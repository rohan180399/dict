/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.operation.data;

import com.ibatis.sqlmap.client.SqlMapClient;  
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.billing.business.BillingTO;
import ets.domain.programmingfree.excelexamples.calculateDistance;
import ets.domain.operation.business.OperationTO;    
import ets.domain.operation.business.TripAllowanceTO;
import ets.domain.operation.business.TripFuelDetailsTO;
import ets.domain.trip.business.TripTO;
import ets.domain.util.FPLogUtils;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;    
import java.util.List;
import java.util.List;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
//import com.Service;
//import com.ServiceSoap;
//import com.GetDriverAdvanceResponse.GetDriverAdvanceResult;

/**
 *
 * @author vijay
 */
public class OperationDAO extends SqlMapClientDaoSupport {

    public OperationDAO() {
    }
    private final int errorStatus = 4;
    private final static String CLASS = "OperationDAO";

    public ArrayList getServiceTypes() {
        Map map = new HashMap();
        ArrayList serviceTypes = new ArrayList();
        try {
            serviceTypes = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getServiceTypes", map);
            //////System.out.println("serviceTypes size=" + serviceTypes.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getServiceTypes Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getServiceTypes List", sqlException);
        }

        return serviceTypes;
    }

    public ArrayList getServiceTypesForWo() {
        Map map = new HashMap();
        ArrayList serviceTypes = new ArrayList();
        try {
            serviceTypes = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getServiceTypesForWo", map);
            //////System.out.println("serviceTypes size=" + serviceTypes.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getServiceTypes Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getServiceTypes List", sqlException);
        }

        return serviceTypes;
    }

    public ArrayList getExistingTechnicians(String jobCardId, String probId) {
        Map map = new HashMap();
        map.put("jobCardId", jobCardId);
        map.put("probId", probId);
        ArrayList existingTechnicians = new ArrayList();
        try {
            existingTechnicians = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getExistingTechnicians", map);
            System.out.println("existingTechnicians size=" + existingTechnicians.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("existingTechnicians Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "existingTechnicians List", sqlException);
        }

        return existingTechnicians;
    }

    public ArrayList getJobCardTechnicians(String jobCardId) {
        Map map = new HashMap();
        map.put("jobCardId", jobCardId);

        ArrayList existingTechnicians = new ArrayList();
        try {
            existingTechnicians = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getJobCardTechnicians", map);
            //////System.out.println("getJobCardTechnicians size=" + existingTechnicians.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJobCardTechnicians Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getJobCardTechnicians List", sqlException);
        }

        return existingTechnicians;
    }
//    bala

    public ArrayList getCustomerTypes() {
        Map map = new HashMap();
        ArrayList customerTypes = new ArrayList();
        try {
            customerTypes = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCustomerTypes", map);
            //////System.out.println("customerTypes size=" + customerTypes.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getServiceTypes Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getServiceTypes List", sqlException);
        }

        return customerTypes;
    }
//    bala ends

    public ArrayList getServicePoints() {
        Map map = new HashMap();
        ArrayList serviceTypes = new ArrayList();
        try {
            serviceTypes = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getServicePoints", map);
            //////System.out.println("getServicePoints size=" + serviceTypes.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getServicePoints Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getServicePoints List", sqlException);
        }
        return serviceTypes;
    }

    public ArrayList getServiceVendors() {
        Map map = new HashMap();
        ArrayList serviceVendors = new ArrayList();
        try {
            serviceVendors = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getServiceVendors", map);
            //////System.out.println("serviceVendors size=" + serviceVendors.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("serviceVendors Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "serviceVendors List", sqlException);
        }
        return serviceVendors;
    }

    public ArrayList getMfrList() {
        Map map = new HashMap();
        ArrayList MfrList = new ArrayList();
        try {

            MfrList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.MfrList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "MfrList", sqlException);
        }
        return MfrList;

    }

    public int insertWorkOrder(OperationTO operationTO) {
        Map map = new HashMap();
        int status = 0;
        //////System.out.println("dao regno=" + operationTO.getRegno());
        map.put("regno", operationTO.getRegno());
        map.put("serviceTypeId", operationTO.getServicetypeId());
        map.put("priority", operationTO.getPriority());
        map.put("fromOperationId", operationTO.getCompId());
        map.put("toServiceId", operationTO.getServiceLocationId());
        map.put("reqdate", operationTO.getReqDate());
        map.put("driverName", operationTO.getDriverName());
        map.put("dateOfIssue", operationTO.getDateOfIssue());
        map.put("km", operationTO.getKmReading());
        map.put("currentLocation", operationTO.getLocation());
        map.put("hourMeter", operationTO.getHourMeter());
        map.put("userId", operationTO.getUserId());
        map.put("remark", operationTO.getRemarks());

        try {
            //  status = (Integer) getSqlMapClientTemplate().update("operation.insertKmHistory", map);
            status = (Integer) getSqlMapClientTemplate().insert("operation.insertWorkOrder", map);
            // status = (Integer) getSqlMapClientTemplate().queryForObject("operation.lastinsertWOId", map);
            //////System.out.println("Success=" + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertWorkOrder Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertWorkOrder", sqlException);
        }

        return status;
    }

    public int insertWorkOrderProblems(int secId, int probId, String symptoms, int Severity, int workorderId) {
        Map map = new HashMap();
        int status = 0;
        map.put("probId", probId);
        map.put("secId", secId);
        map.put("symptom", symptoms);
        map.put("severity", Severity);
        map.put("woId", workorderId);

        try {
            status = (Integer) getSqlMapClientTemplate().update("operation.insertWorkOrderProblems", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertWorkOrderProblems Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertWorkOrderProblems", sqlException);
        }

        return status;

    }

    public String getVehicleNos(String regNo, int compId, int compTypeId) {
        Map map = new HashMap();

        OperationTO operationTO = null;
        map.put("regNo", '%' + regNo);
        map.put("compId", compId);
        map.put("compTypeId", compTypeId);
        //////System.out.println("compTypeId" + compTypeId);
        //////System.out.println("regNo" + regNo);
        String suggestions = "";
        System.out.println("map = " + map);
        try {
            ArrayList getItemList = new ArrayList();
            getItemList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getVehicleNos", map);
            Iterator itr = getItemList.iterator();
            while (itr.hasNext()) {
                operationTO = (OperationTO) itr.next();
                suggestions = operationTO.getRegno() + "~" + suggestions;
            }
            System.out.println("suggestions:" + suggestions);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleNos Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehicleNos", sqlException);
        }
        return suggestions;

    }

    public ArrayList scheduleWODetails(int compId) {
        Map map = new HashMap();
        ArrayList scheduleWODetails = new ArrayList();
        map.put("compId", compId);
        //////System.out.println("map = " + map);
        try {
            scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.scheduleWODetails", map);
            //////System.out.println("scheduleWODetails size=" + scheduleWODetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("scheduleWODetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "scheduleWODetails", sqlException);
        }

        return scheduleWODetails;
    }

    public ArrayList getVehicleDetails(int vehicleId, int jobcardId) {
        Map map = new HashMap();
        ArrayList scheduleWODetails = new ArrayList();
        map.put("vehicleId", vehicleId);
        map.put("jobcardId", jobcardId);
        //////System.out.println("vehicleid 1:" + vehicleId);
        try {
            if (vehicleId != 0) {
                //////System.out.println("am here 1");
                scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getVehicleDetails", map);
            } else {
                //////System.out.println("am here 2");
                scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.nonExistVehicleDetails", map);
            }
            //////System.out.println("getVehicleDetails size=" + scheduleWODetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleDetails", sqlException);
        }

        return scheduleWODetails;
    }

    public ArrayList getTrailerDetails(int vehicleId, int jobcardId) {
        Map map = new HashMap();
        ArrayList scheduleWODetails = new ArrayList();
        map.put("trailerId", vehicleId);
        map.put("jobcardId", jobcardId);
        //////System.out.println("vehicleid 1:" + vehicleId);
        try {

            //////System.out.println("am here 1");
            scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getTrailerDetails", map);

            //////System.out.println("getVehicleDetails size=" + scheduleWODetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleDetails", sqlException);
        }

        return scheduleWODetails;
    }

    public ArrayList getWorkOrderDetails(int workOrderId) {
        Map map = new HashMap();
        ArrayList scheduleWODetails = new ArrayList();
        map.put("workOrderId", workOrderId);
        try {
            scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getWorkOrderDetails", map);
            //////System.out.println("getWorkOrderDetails size=" + scheduleWODetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("scheduleWODetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getWorkOrderDetails", sqlException);
        }

        return scheduleWODetails;
    }

    public ArrayList getProblemDetails(int workOrderId, int jobcardId) {
        Map map = new HashMap();
        ArrayList scheduleWODetails = new ArrayList();
        map.put("workOrderId", workOrderId);
        if (jobcardId == 0) {
            String jocId = "";
            map.put("jobcardId", jocId);
        } else {

            map.put("jobcardId", jobcardId);
        }
        //////System.out.println("map = " + map);
        try {
            scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getProblemDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getProblemDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getProblemDetails", sqlException);
        }

        return scheduleWODetails;
    }

    public ArrayList getWoProblemDetails(int workOrderId, int jobcardId) {
        Map map = new HashMap();
        ArrayList scheduleWODetails = new ArrayList();
        map.put("workOrderId", workOrderId);
        if (jobcardId == 0) {
            String jocId = "";
            map.put("jobcardId", jocId);
        } else {

            map.put("jobcardId", jobcardId);
        }
        //////System.out.println("workOrderId" + workOrderId);
        try {
            scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getWoProblemDetails", map);
            //////System.out.println("getWoProblemDetails size=" + scheduleWODetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getProblemDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getProblemDetails", sqlException);
        }

        return scheduleWODetails;
    }

    public ArrayList scheduledWODetails(String date) {
        Map map = new HashMap();
        ArrayList scheduledWODetails = new ArrayList();
        map.put("date", date);

        try {
            scheduledWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.scheduledWODetails", map);
            //////System.out.println("scheduledWODetails size=" + scheduledWODetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("scheduledWODetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "scheduledWODetails", sqlException);
        }

        return scheduledWODetails;

    }

    public ArrayList getProblemMaster() {
        Map map = new HashMap();
        ArrayList problemMaster = new ArrayList();

        try {
            problemMaster = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getProblemMaster", map);
            //////System.out.println("problemMaster size=" + problemMaster.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getProblemMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getProblemMaster", sqlException);
        }

        return problemMaster;

    }

    public int insertWorkOrderSchdule(int workOrderId, String scheduledate, int userId, String deliDate, String serviceVendor) {
        Map map = new HashMap();
        int status = 0;
        map.put("workOrderId", workOrderId);
        map.put("scheduledate", scheduledate);
        map.put("deliDate", deliDate);
        map.put("userId", userId);
        map.put("serviceVendorId", serviceVendor);
        int jobCardId = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("operation.insertWorkOrderSchdule", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertWorkOrderSchdule Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertWorkOrderSchdule", sqlException);
        }

        return status;
    }

    public int getWorkOrderOperationPointId(int workOrderId) {
        Map map = new HashMap();
        int operationPointId = 0;
        map.put("workOrderId", workOrderId);
        try {
            //////System.out.println("map = " + map);
            operationPointId = (Integer) getSqlMapClientTemplate().queryForObject("operation.getWorkOrderOperationPointId", map);
            //////System.out.println("operationPointId= " + operationPointId);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWorkOrderOperationPointId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getWorkOrderOperationPointId", sqlException);
        }

        return operationPointId;
    }

    public ArrayList searchWorkOrderStatus(String fromDate, String toDate, String regNo, int compId, String status, String workorderId, int roleId) {
        Map map = new HashMap();
        ArrayList scheduleWODetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("compId", compId);
        map.put("status", status);
        map.put("workorderId", workorderId);
        map.put("roleId", String.valueOf(roleId));
        //////System.out.println("fromDate" + fromDate);
        //////System.out.println("toDate" + toDate);
        //////System.out.println("compId" + compId);
        try {
            //////System.out.println("map int the dao ==== " + map);
//            if (status.equalsIgnoreCase("woCreated")) {
//                //////System.out.println("in if");
//                scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.searchWorkOrderStatus", map);
//            } else {
            //////System.out.println("in else");
            scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.workOrderStatus", map);
//            }
            //////System.out.println("searchWorkOrderStatus size=" + scheduleWODetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("searchWorkOrderStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "searchWorkOrderStatus", sqlException);
        }

        return scheduleWODetails;
    }

    public ArrayList searchJobCardStatus(String jcno, String regno, String status, int operId, String fromDate, String toDate, int compId, int startIndex, int endIndex, String custType) {
        Map map = new HashMap();
        ArrayList scheduleWODetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("compId", compId);
//        map.put("compId", 1023);
        map.put("regno", regno);
        map.put("jcno", jcno);
        map.put("operId", operId);
        map.put("status", status);
        map.put("sIndex", startIndex);
        map.put("eIndex", endIndex);
        map.put("custType", custType);
        if (custType == null) {
            custType = "0";
        }
        System.out.println("map Arul here = " + map);
        try {
            if ("0".equals(custType)) {
//                map.put("compId", 1022);
                //////System.out.println("map 0 = " + map);
                scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.searchJobCardStatus", map);
            } else if ("I".equals(custType)) {
//                map.put("compId", 1022);
                //////System.out.println("map 1 = " + map);
                scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.searchJobCardStatusInt", map);
            } else if ("E".equals(custType)) {
//                map.put("compId", 1022);
                //////System.out.println("map  2 = " + map);
                scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.searchJobCardStatusExt", map);
            }

            System.out.println("searchJobCardStatus size=" + scheduleWODetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("searchJobCardStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "searchJobCardStatus", sqlException);
        }

        return scheduleWODetails;
    }

    public ArrayList getJobcardList(int vehicleId, int jobcardid) {
        Map map = new HashMap();
        ArrayList scheduleWODetails = new ArrayList();
        map.put("vehicleId", vehicleId);
        map.put("jobcardid", jobcardid);

        try {

            scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getJobcardList", map);

            //////System.out.println("getJobcardList size=" + scheduleWODetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJobcardList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getJobcardList", sqlException);
        }

        return scheduleWODetails;
    }

    public ArrayList getProblemList(int vehicleId, int workOrderId) {
        Map map = new HashMap();
        ArrayList scheduleWODetails = new ArrayList();
        map.put("vehicleId", vehicleId);
        map.put("workOrderId", workOrderId);
        System.out.println("getProblemList map=" + map);
        try {

            scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getProblemList", map);

            //////System.out.println("getProblemList size=" + scheduleWODetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getProblemList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getProblemList", sqlException);
        }

        return scheduleWODetails;
    }

    public ArrayList getServiceList(int vehicleId, int km, int jobcardId) {
        Map map = new HashMap();
        ArrayList scheduleWODetails = new ArrayList();
        ArrayList scheduleAllWODetails = new ArrayList();
        ArrayList ServiceList = new ArrayList();
        OperationTO operationTO = null;
        OperationTO allTO = null;

        try {
            map.put("vehicleId", vehicleId);
            map.put("km", km);
            map.put("jobcardId", jobcardId);
            System.out.println(" map details....:" + map);
            if (vehicleId != 0) {
                scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getServiceList", map);

                System.out.println("scheduleWODetails" + scheduleWODetails.size());

                /*                scheduleAllWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getAllModelServiceList", map);
                System.out.println("scheduleWODetails" + scheduleWODetails.size());
                //////System.out.println("scheduleAllWODetails" + scheduleAllWODetails.size());

                if (scheduleWODetails.size() != 0) {
                Iterator ser = scheduleAllWODetails.iterator();
                while (ser.hasNext()) {
                operationTO = new OperationTO();
                operationTO = (OperationTO) ser.next();
                Iterator newSer = scheduleWODetails.iterator();
                int count = 0;
                while (newSer.hasNext()) {
                allTO = new OperationTO();
                allTO = (OperationTO) newSer.next();
                System.out.println(operationTO.getServiceId() + ":" + allTO.getServiceId());
                if (operationTO.getServiceId() == allTO.getServiceId()) {
                count++;
                //////System.out.println("configTO.getServiceId()" + operationTO.getServiceId());
                //////System.out.println("newconfigTO.getServiceId()" + allTO.getServiceId());
                //////System.out.println("count" + count);
                }
                }
                //to add new service
                if (count == 0) {
                ServiceList.add(operationTO);
                }
                }
                ServiceList.addAll(scheduleWODetails);
                } else {
                ServiceList.addAll(scheduleAllWODetails);

                }
                }
                 * */
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getServiceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getServiceList", sqlException);
        }

        return scheduleWODetails;
    }

    public ArrayList getNonGracePeriodServices(int vehicleId, int km) {
        Map map = new HashMap();
        ArrayList nonGracePeriodServices = new ArrayList();
        try {
            map.put("vehicleId", vehicleId);
            map.put("km", km);
            System.out.println("getNonGracePeriodServices: " + map);
            nonGracePeriodServices = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getNonGracePeriodServices", map);
            System.out.println("getNonGracePeriodServices size=" + nonGracePeriodServices.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getNonGracePeriodServices Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getNonGracePeriodServices", sqlException);
        }
        return nonGracePeriodServices;
    }

    public ArrayList getNonGraceSchedServices(int vehicleId, int jobCardId, int km, int hm) {
        Map map = new HashMap();
        ArrayList jobCardMrsDetails = new ArrayList();
        try {
            map.put("vehicleId", vehicleId);
            map.put("jobCardId", jobCardId);
            map.put("km", km);
            map.put("hm", hm);
            jobCardMrsDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getNonGraceSchedServices", map);
            //////System.out.println("getNonGraceSchedServices size=" + jobCardMrsDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getNonGracePeriodServices Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getNonGracePeriodServiinsertJobCardScheduleDetailsces", sqlException);
        }
        return jobCardMrsDetails;
    }

    public String getDueHmKm(OperationTO operationTO) {
        Map map = new HashMap();
        String mess = "";
        try {
            map.put("vehicleId", operationTO.getVehicleId());
            map.put("serviceId", operationTO.getServiceId());
            map.put("km", operationTO.getKmReading());
            map.put("hm", operationTO.getHourMeter());
            mess = (String) getSqlMapClientTemplate().queryForObject("operation.getbalHmKm", map);
            if (mess == null) {
                mess = "";
            }
            //////System.out.println("mess=" + mess);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDueHmKm Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDueHmKm", sqlException);
        }
        return mess;
    }

    public int insertJobCardScheduleDetails(int jobcardId, int pId, String pcDate, int techId, String stat, String remarks, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("probId", pId);
        map.put("jobcardId", jobcardId);
        map.put("pcDate", pcDate);
        map.put("techId", techId);
        map.put("status", stat);
        map.put("remarks", remarks);
        map.put("userId", userId);

        try {

            String isExist = (String) getSqlMapClientTemplate().queryForObject("operation.isThisProbExist", map);
            if (isExist == null) {
                status = (Integer) getSqlMapClientTemplate().update("operation.insertJobCardScheduleDetails", map);
            } else {
                status = (Integer) getSqlMapClientTemplate().update("operation.updateJobCardScheduleDetails", map);

            }
            //update PCD in job card master 
            status = (Integer) getSqlMapClientTemplate().update("operation.updateJobCardPCD", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCardScheduleDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertJobCardScheduleDetails", sqlException);
        }

        return status;
    }

    public int updateJobCardScheduleDetails(String pcDate, int jobcardId, int pId, int techId, String stat, String remarks, String cau, String rema) {
        Map map = new HashMap();
        int status = 0;
        map.put("probId", pId);
        map.put("jobcardId", jobcardId);
        map.put("techId", techId);
        map.put("status", stat);
        map.put("remarks", remarks);
        map.put("pcDate", pcDate);

        try {
            status = (Integer) getSqlMapClientTemplate().update("operation.closeJobcardProblem", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateJobCardScheduleDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateJobCardScheduleDetails", sqlException);
        }

        return status;
    }

    public int insertJobCardProblems(int secId, int probId, String symptoms, int Severity, int jobcardId, int temp) {
        Map map = new HashMap();
        int status = 0;
        map.put("probId", probId);
        map.put("secId", secId);
        map.put("symptom", symptoms);
        map.put("severity", Severity);
        map.put("jobcardId", jobcardId);
        String IdentifiedBy = "Technician";
        if (temp == 0) {
            IdentifiedBy = "Customer";
        }
        map.put("IdentifiedBy", IdentifiedBy);
        try {
            status = (Integer) getSqlMapClientTemplate().update("operation.insertJobCardProblems", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCardProblems Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertJobCardProblems", sqlException);
        }

        return status;
    }

    public ArrayList jobCardProblemDetails(int jobcardId) {
        Map map = new HashMap();
        ArrayList scheduleWODetails = new ArrayList();

        map.put("jobcardId", jobcardId);

        try {
            scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.jobCardProblemDetails", map);
            //////System.out.println("jobCardProblemDetails size=" + scheduleWODetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("jobCardProblemDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "jobCardProblemDetails", sqlException);
        }

        return scheduleWODetails;
    }

    public ArrayList getServiceDetails(int jobcardId) {
        Map map = new HashMap();
        ArrayList Details = new ArrayList();

        map.put("jobCardId", jobcardId);
        //////System.out.println("jobcard id service" + jobcardId);
        try {
            Details = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getServiceDetails", map);
            //////System.out.println("Details size=" + Details.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("jobCardProblemDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "jobCardProblemDetails", sqlException);
        }

        return Details;
    }

    public ArrayList getActivity(int probId, int jobcardId) {
        Map map = new HashMap();
        ArrayList Details = new ArrayList();

        map.put("jobCardId", jobcardId);
        map.put("problemId", probId);
        //////System.out.println("jobcard id service" + jobcardId);
        try {
            Details = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getServiceDetails", map);
            //////System.out.println("Details size=" + Details.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("jobCardProblemDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "jobCardProblemDetails", sqlException);
        }

        return Details;
    }

    public int getTotalJobcards(String jcno, String regno, String status, int operId, String fromDate, String toDate, int compId, String custType) {
        Map map = new HashMap();
        int totalRecords = 0;

        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("rno", regno);
        map.put("compId", compId);
        map.put("status", status);
        map.put("operId", operId);
        map.put("jcno", jcno);

        //////System.out.println("regno in tot" + regno);
        //////System.out.println("jcno in tot" + jcno);
        //////System.out.println("fromDate in tot" + fromDate);
        //////System.out.println("compId in tot" + compId);
        //////System.out.println("status in tot" + status);
        //////System.out.println("operId in tot" + operId);
        try {
            if ("0".equals(custType)) {
                totalRecords = (Integer) getSqlMapClientTemplate().queryForObject("operation.getTotalJobcards", map);
            } else if ("I".equals(custType)) {
                totalRecords = (Integer) getSqlMapClientTemplate().queryForObject("operation.getTotalJobcardsInt", map);
            } else if ("E".equals(custType)) {
                totalRecords = (Integer) getSqlMapClientTemplate().queryForObject("operation.getTotalJobcardsExt", map);
            }

            System.out.println("getTotalJobcards " + totalRecords);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTotalJobcards Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTotalJobcards", sqlException);
        }

        return totalRecords;

    }

    public ArrayList getVendorDetails() {
        Map map = new HashMap();
        ArrayList getVendorDetails = new ArrayList();
        try {
            getVendorDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getVendorDetails", map);
            //////System.out.println("getVendorDetails size=" + getVendorDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVendorDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVendorDetails List", sqlException);
        }

        return getVendorDetails;
    }

    public int insertBodyPartsWO(int jobcardId, String remarks, int userId, int vendor) {
        Map map = new HashMap();
        int status = 0;
        map.put("vendorId", vendor);
        map.put("userId", userId);
        map.put("jobcardId", jobcardId);
        map.put("remarks", remarks);

        try {
            status = (Integer) getSqlMapClientTemplate().insert("operation.insertBodyPartsWO", map);
            // status = (Integer) getSqlMapClientTemplate().queryForObject("operation.bodyPartWOId", map);
            //////System.out.println("last insert first Id BP" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertBodyPartsWO Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertBodyPartsWO", sqlException);
        }

        return status;

    }

    public int insertWOProblems(int workOrderId, int pId) {
        Map map = new HashMap();
        int status = 0;
        map.put("workOrderId", workOrderId);
        map.put("probId", pId);

        //////System.out.println("workOrderId last" + workOrderId);
        try {
            status = (Integer) getSqlMapClientTemplate().update("operation.insertWOProblems", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertWOProblems Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertWOProblems", sqlException);
        }

        return status;
    }

    public int insertExternalTechnician(int jobcardId, int probId, String techName, String compName, String contactNo, String labourCharge) {
        Map map = new HashMap();
        int status = 0;
        map.put("jobcardId", jobcardId);
        map.put("probId", probId);
        map.put("techName", techName);
        map.put("compName", compName);
        map.put("contactNo", contactNo);
        //////System.out.println("labourCharge" + labourCharge);

        float lc = Float.parseFloat(labourCharge);
        //////System.out.println("labourCharge" + lc);
        map.put("labourCharge", lc);

        try {
            status = (Integer) getSqlMapClientTemplate().update("operation.insertExternalTechnician", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertExternalTechnician Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertExternalTechnician", sqlException);
        }

        return status;
    }

    public int getVehicleId(String regNo) {
        Map map = new HashMap();
        String status = "";
        map.put("regno", regNo);
        int vehicleId = 0;
        try {

            status = (String) getSqlMapClientTemplate().queryForObject("operation.getVehicleId", map);
            if (status != null) {
                vehicleId = Integer.parseInt(status);
            }

            //////System.out.println("vechileId BP" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleId", sqlException);
        }

        return vehicleId;
    }

    public String checkVehicleRegNo(String regNo) {
        Map map = new HashMap();
        String status = "";

        String mess = "";
        try {
            regNo = regNo.replace(" ", "");
            //////System.out.println("trimmed regno=" + regNo);
            map.put("regNo", regNo);
            status = (String) getSqlMapClientTemplate().queryForObject("operation.checkVehicleExists", map);
            if (status != null) {
                mess = status;
            }

            //////System.out.println("checkVehicleRegNo" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkVehicleRegNo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkVehicleRegNo", sqlException);
        }

        return mess;
    }

    public String getJobCardOwner(int jobCardId) {
        Map map = new HashMap();
        String jobCardOwner = "";

        try {
            //////System.out.println("jobCardId=" + jobCardId);
            map.put("jobCardId", jobCardId);
            jobCardOwner = (String) getSqlMapClientTemplate().queryForObject("operation.jobCardOwner", map);

            //////System.out.println("jobCardOwner:" + jobCardOwner);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("jobCardOwner Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "jobCardOwner", sqlException);
        }

        return jobCardOwner;
    }

    public int insertOtherJobCardWorkOrder(OperationTO operationTO) {
        Map map = new HashMap();
        int status = 0;

        map.put("serviceTypeId", operationTO.getServicetypeId());

        map.put("priority", operationTO.getPriority());

        map.put("fromOperationId", operationTO.getCompId());

        map.put("toServiceId", operationTO.getServiceLocationId());

        map.put("reqdate", operationTO.getReqDate());

        map.put("driverName", operationTO.getDriverName());

        map.put("dateOfIssue", operationTO.getDateOfIssue());

        map.put("km", operationTO.getKmReading());
        map.put("totalkm", operationTO.getKmReading());
        map.put("hm", operationTO.getHourMeter());

        map.put("userId", operationTO.getUserId());

        map.put("remark", operationTO.getRemarks());

        map.put("cust", operationTO.getCust());
        map.put("eCost", operationTO.getTotalAmount());

        int custId = 0;
        if (operationTO.getMfrName() == null || operationTO.getMfrName() == "") {
            map.put("mfrName", 0);
            map.put("modName", 0);
            map.put("usageName", 0);
            //map.put("regno",0);
        } else {
            map.put("mfrName", operationTO.getMfrName());
            map.put("modName", operationTO.getModelName());
            map.put("usageName", operationTO.getUsageName());

        }
        String temp = "";
        int jobcardId = 0;
        int vehicleId = 0;
        String temp1 = "";
        try {

            map.put("regno", operationTO.getRegno());
            //////System.out.println("regno in DAO-->" + operationTO.getRegno());
            temp = (String) getSqlMapClientTemplate().queryForObject("operation.getVehicleId", map);
            //////System.out.println("temp" + temp);
            if (temp != null) {
                vehicleId = Integer.parseInt(temp);
                //////System.out.println("vId" + vehicleId);
            }
            if (vehicleId != 0) {
                map.put("regno", operationTO.getRegno());
            }
            jobcardId = (Integer) getSqlMapClientTemplate().insert("operation.insertJobCardWork", map);
            //jobcardId = (Integer) getSqlMapClientTemplate().queryForObject("operation.lastinsertJobCardId", map);
            //////System.out.println("last job card Id" + jobcardId);

            if (vehicleId != 0) {
                map.put("vehicleId", vehicleId);
                // status = (Integer) getSqlMapClientTemplate().update("operation.insertKmHistory", map);
                temp1 = (String) getSqlMapClientTemplate().queryForObject("operation.getCustId", map);
                if (temp1 != null) {

                    custId = Integer.parseInt(temp1);
                    //////System.out.println("cId" + custId);
                }
            } else {

                map.put("vehicleId", vehicleId);

            }

            map.put("custId", custId);
            map.put("jobcardId", jobcardId);
            //////System.out.println("jobcardId" + jobcardId);
            if (jobcardId != 0) {
                map.put("vehicleId", 0);
                map.put("custId", 0);
                //////System.out.println("vehicleId in DAO-->" + vehicleId + "custId" + custId);
                status = (Integer) getSqlMapClientTemplate().update("operation.insertOtherJobCardWorkOrder", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertOtherJobCardWorkOrder Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertOtherJobCardWorkOrder", sqlException);
        }

        return jobcardId;
    }

    public int insertJobCardWorkOrder(OperationTO operationTO) {
        Map map = new HashMap();
        int status = 0;

        map.put("serviceTypeId", operationTO.getServicetypeId());
        map.put("priority", operationTO.getPriority());
        map.put("fromOperationId", operationTO.getCompId());
        map.put("toServiceId", operationTO.getServiceLocationId());
        map.put("serviceLocation", operationTO.getServiceLocation());
        map.put("reqdate", operationTO.getReqDate());
        map.put("driverName", operationTO.getDriverName());
        map.put("dateOfIssue", operationTO.getDateOfIssue());
        map.put("km", operationTO.getKmReading());
        map.put("totalkm", operationTO.getTotalKm());
        map.put("hm", operationTO.getHourMeter());
        map.put("bayNo", operationTO.getBayNo());
        map.put("workOrderId", operationTO.getWorkOrderId());
        map.put("location", operationTO.getLocation());
        map.put("type", operationTO.getJobCardType());
        map.put("jobCardTypeNew", operationTO.getJobCardTypeNew());
        String sVendor = operationTO.getServiceVendor();
        if (sVendor.equals("")) {
            sVendor = "0";
        }
        map.put("serviceVendor", operationTO.getServiceVendor());

        map.put("userId", operationTO.getUserId());

        map.put("remark", operationTO.getRemarks());

        map.put("cust", operationTO.getCust());
        map.put("eCost", operationTO.getTotalAmount());

        int custId = 0;
        if (operationTO.getMfrName() == null || operationTO.getMfrName() == "") {
            map.put("mfrName", 0);
            map.put("modName", 0);
            map.put("usageName", 0);
            //map.put("regno",0);
        } else {
            map.put("mfrName", operationTO.getMfrName());
            map.put("modName", operationTO.getModelName());
            map.put("usageName", operationTO.getUsageName());

        }
        String temp = "";
        int jobcardId = 0;
        int vehicleId = 0;
        String temp1 = "";
        try {

            map.put("trailerId", operationTO.getTrailerId());
            map.put("jobCardFor", operationTO.getJobCardFor());
            map.put("regno", operationTO.getRegno());
            //////System.out.println("map = " + map);
            temp = (String) getSqlMapClientTemplate().queryForObject("operation.getVehicleId", map);
            //////System.out.println("temp" + temp);
            if (temp != null) {
                vehicleId = Integer.parseInt(temp);
                //////System.out.println("vId" + vehicleId);
            }
            if (vehicleId != 0) {
                map.put("regno", operationTO.getRegno());
            }
            //////System.out.println("map = " + map);
            jobcardId = (Integer) getSqlMapClientTemplate().insert("operation.insertJobCardWork", map);
            //jobcardId = (Integer) getSqlMapClientTemplate().queryForObject("operation.lastinsertJobCardId", map);
            //////System.out.println("last job card Id" + jobcardId);

            if (vehicleId != 0) {
                map.put("vehicleId", vehicleId);
                // status = (Integer) getSqlMapClientTemplate().update("operation.insertKmHistory", map);
                //////System.out.println("map = " + map);
                temp1 = (String) getSqlMapClientTemplate().queryForObject("operation.getCustId", map);
                if (temp1 != null) {

                    custId = Integer.parseInt(temp1);
                    //////System.out.println("cId" + custId);
                }
            } else {

                map.put("vehicleId", vehicleId);

            }

            map.put("custId", custId);
            map.put("jobcardId", jobcardId);
            //////System.out.println("jobcardId" + jobcardId);
            //////System.out.println("map = " + map);
            if (jobcardId != 0) {
                status = (Integer) getSqlMapClientTemplate().update("operation.insertJobCardWorkOrder", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("insertJobCardWorkOrder Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertJobCardWorkOrder", sqlException);
        }

        return jobcardId;
    }

    public int insertJobCardServices(int userId, int jobcardId, int service, int kiloMeter, int hM, int vehicleId) {
        Map map = new HashMap();
        int status = 0;
        map.put("jobcardId", jobcardId);
        map.put("service", service);
        map.put("kiloMeter", kiloMeter);
        map.put("hM", hM);
        map.put("vehicleId", vehicleId);
        map.put("userId", userId);

        try {
            String isExist = (String) getSqlMapClientTemplate().queryForObject("operation.isThisServiceClosed", map);
            if (isExist == null) {
                status = (Integer) getSqlMapClientTemplate().update("operation.insertJobCardServices", map);
            } else {
                status = (Integer) getSqlMapClientTemplate().update("operation.updateClosedService", map);

            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCardServices Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertJobCardServices", sqlException);
        }

        return status;

    }

    public int insertServicedDetails(int userId, int jobcardId, int service, int kiloMeter, int hM, int vehicleId, String dateofIssue) {
        Map map = new HashMap();
        int status = 0;
        map.put("jobcardId", jobcardId);
        map.put("service", service);
        map.put("kiloMeter", kiloMeter);
        map.put("hM", hM);
        map.put("vehicleId", vehicleId);
        map.put("userId", userId);
        map.put("servicedDate", dateofIssue);

        try {
            status = (Integer) getSqlMapClientTemplate().update("operation.insertServicedDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertServicedDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertServicedDetails", sqlException);
        }

        return status;
    }

    public int insertJobCardServiceDetails(int jobcardId, int sId, String dateofIssue, int techId, String stat, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("serviceId", sId);
        map.put("jobcardId", jobcardId);
        map.put("pcDate", dateofIssue);
        map.put("techId", techId);
        map.put("status", stat);
        map.put("userId", userId);

        try {

            String isExist = (String) getSqlMapClientTemplate().queryForObject("operation.isThisServiceExist", map);
            if (isExist == null) {
                status = (Integer) getSqlMapClientTemplate().update("operation.insertJobCardServiceDetails", map);
            } else {
                status = (Integer) getSqlMapClientTemplate().update("operation.updateJobCardServiceDetails", map);

            }
            //update PCD in job card master 
            status = (Integer) getSqlMapClientTemplate().update("operation.updateJobCardPCD", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCardScheduleDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertJobCardScheduleDetails", sqlException);
        }

        return status;
    }

    public ArrayList getScheduledServices(int vehicleId, int jobcardId) {
        Map map = new HashMap();
        ArrayList scheduledServices = new ArrayList();

        try {

            map.put("vehicleId", vehicleId);
            map.put("jobcardId", jobcardId);

            scheduledServices = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getScheduledServices", map);

            //////System.out.println("scheduledServices size=" + scheduledServices.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getScheduledServices Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getScheduledServices", sqlException);
        }

        return scheduledServices;
    }

    public ArrayList getBodyWorksList(int jobcardId) {
        Map map = new HashMap();
        ArrayList scheduledServices = new ArrayList();

        try {

            map.put("jobcardId", jobcardId);
            scheduledServices = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getBodyWorksList", map);

            //////System.out.println("getBodyWorksList size=" + scheduledServices.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyWorksList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBodyWorksList", sqlException);
        }

        return scheduledServices;
    }

    public int getTotalFSList(String date, int compId) {
        Map map = new HashMap();
        int totalRecords = 0;
        map.put("compId", compId);
        map.put("date", date);
        //////System.out.println("date" + date);

        try {
            totalRecords = (Integer) getSqlMapClientTemplate().queryForObject("operation.getTotalFSList", map);

            //////System.out.println("totalRecords " + totalRecords);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTotalFSList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTotalFSList", sqlException);
        }

        return totalRecords;

    }

    public ArrayList getFSList(String date, int compId, int sIndex, int noOfRecords) {
        Map map = new HashMap();
        int totalRecords = 0;
        map.put("compId", compId);
        map.put("sIndex", sIndex);
        map.put("noOfRecords", noOfRecords);
        map.put("date", date);
        ArrayList getFSList = new ArrayList();

        try {
            getFSList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getFSList", map);

            //////System.out.println("getFSList" + getFSList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFSList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFSList", sqlException);
        }

        return getFSList;

    }

    public int insertFreeSevicedVehicles(int vehicleId, int freeServiceId, String dateofIssue, int km, int hm, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("freeServiceId", freeServiceId);
        map.put("vehicleId", vehicleId);
        map.put("serviceddate", dateofIssue);
        map.put("km", km);
        map.put("hm", hm);
        map.put("userId", userId);

        try {

            status = (Integer) getSqlMapClientTemplate().update("operation.insertFreeSevicedVehicles", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertFreeSevicedVehicles Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertFreeSevicedVehicles", sqlException);
        }

        return status;
    }

    public ArrayList periodicServiceList(int compId, String date) {
        Map map = new HashMap();
        int totalRecords = 0;
        map.put("compId", compId);
        map.put("date", date);

        ArrayList periodicServiceList = new ArrayList();

        try {
            periodicServiceList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.periodicServiceList", map);

            //////System.out.println("periodicServiceList" + periodicServiceList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("periodicServiceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "periodicServiceList", sqlException);
        }

        return periodicServiceList;
    }

    public int periodicServiceStatus(int vehicleId, String psDate) {
        Map map = new HashMap();
        int totalRecords = 0;
        map.put("vehicleId", vehicleId);
        map.put("psDate", psDate);

        ArrayList periodicServiceList = new ArrayList();

        try {
            totalRecords = (Integer) getSqlMapClientTemplate().queryForObject("operation.periodicServiceStatus", map);

            //////System.out.println("periodicServiceStatus" + periodicServiceList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("periodicServiceStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "periodicServiceStatus", sqlException);
        }

        return totalRecords;
    }

    public ArrayList getDirectJCList(int compId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList directJCList = new ArrayList();
        map.put("compId", compId);

        try {
            directJCList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.directJCList", map);

        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("directJCList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "directJCList", sqlException);
        }
        return directJCList;
    }

    public int insertDirectJCStatus(int status1, int jobCardId, int userId) {
        Map map = new HashMap();
        int status = 0;

        map.put("jobCardId", jobCardId);
        map.put("userId", userId);
        String temp = "";
        if (status1 == 0) {
            temp = "Approved";
        } else {
            temp = "Rejected";
        }

        map.put("status", temp);
        try {

            status = (Integer) getSqlMapClientTemplate().update("operation.insertDirectJCStatus", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertDirectJCStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertDirectJCStatus", sqlException);
        }

        return status;
    }

    public ArrayList getJobCardList() {
        Map map = new HashMap();
        ArrayList getJobCardList = new ArrayList();
        try {
            getJobCardList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getExternalServiceJcList", map);
            //////System.out.println("getJobCardList size=" + getJobCardList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJobCardList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getJobCardList List", sqlException);
        }

        return getJobCardList;
    }

    public ArrayList getBodyBillList(int jobcardId) {
        Map map = new HashMap();
        ArrayList getJobCardList = new ArrayList();
        try {
            map.put("jobcardId", jobcardId);
            getJobCardList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getBodyBillList", map);
            //////System.out.println("getBodyBillList size=" + getJobCardList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBodyBillList List", sqlException);
        }

        return getJobCardList;
    }

    public int saveBodyWorksBill(String woId,
            String spareAmount, String vatAmount, String serviceAmount,
            String serviceTaxAmount, String remarks,
            String invoiceNo, String total, int userId) {
        Map map = new HashMap();
        int status = 0;

        map.put("woId", Integer.parseInt(woId));
        map.put("invoiceNo", invoiceNo);
        map.put("spareAmount", spareAmount);
        map.put("vatAmount", vatAmount);
        map.put("serviceAmount", serviceAmount);
        map.put("serviceTaxAmount", serviceTaxAmount);
        map.put("remarks", remarks);
        map.put("total", Float.parseFloat(total));
        map.put("userId", userId);

        try {

            status = (Integer) getSqlMapClientTemplate().update("operation.saveBodyWorksBill", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveBodyWorksBill Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveBodyWorksBill", sqlException);
        }

        return status;
    }

    public int saveJobCardTechnicians(String jobCardId, String probId,
            String technicianId, String estimatedHrs,
            String actualHrs, int userId) {
        Map map = new HashMap();
        int status = 0;

        map.put("jobCardId", jobCardId);
        map.put("probId", probId);
        map.put("technicianId", technicianId);
        map.put("estimatedHrs", estimatedHrs);
        map.put("actualHrs", actualHrs);
        map.put("userId", userId);
        try {

            status = (Integer) getSqlMapClientTemplate().update("operation.updateJobCardTechnicians", map);
            if (status == 0) {
                status = (Integer) getSqlMapClientTemplate().update("operation.insertJobCardTechnicians", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveBodyWorksBill Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveBodyWorksBill", sqlException);
        }

        return status;
    }

    public int updateCompletionDate(int jobcardId, String dateofIssue, String jremarks) {
        Map map = new HashMap();
        int status = 0;

        map.put("jobcardId", jobcardId);
        map.put("dateofIssue", dateofIssue);
        map.put("jremarks", jremarks);

        //////System.out.println("jremarks:" + jremarks);
        try {

            status = (Integer) getSqlMapClientTemplate().update("operation.updateCompletionDate", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateCompletionDate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateCompletionDate", sqlException);
        }

        return status;
    }

    public String getcompDate(int jobcardId) {
        Map map = new HashMap();
        String status = "";

        ArrayList compDate = new ArrayList();
        map.put("jobcardId", jobcardId);
        OperationTO operationTO = null;
        try {

            compDate = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getcompDate", map);
            Iterator itr = compDate.iterator();
            //////System.out.println("compDate:" + compDate.size());
            while (itr.hasNext()) {
                operationTO = (OperationTO) itr.next();
                status = operationTO.getScheduledDate() + "~" + operationTO.getRemarks();
                //////System.out.println("status:" + status);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getcompDate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getcompDate", sqlException);
        }

        return status;
    }

    public ArrayList getJcVehDetail(int jobcardId) {
        Map map = new HashMap();

        ArrayList list = new ArrayList();

        map.put("jcId", jobcardId);
        //////System.out.println("jcid=" + jobcardId);
        try {

            list = (ArrayList) getSqlMapClientTemplate().queryForList("operation.jcVehicleDetail", map);
            //////System.out.println("jcVehicleDetail" + list.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getcompDate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getJcVehDetail", sqlException);
        }

        return list;
    }

    public int isThisProblemExist(int jobcardId, int probId) {
        Map map = new HashMap();
        String status = "";
        map.put("jobcardId", jobcardId);
        map.put("probId", probId);
        int temp = 0;
        try {

            status = (String) getSqlMapClientTemplate().queryForObject("operation.isThisProblemExist", map);
            if (status != null) {
                temp = Integer.parseInt(status);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("isThisProblemExist Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "isThisProblemExist", sqlException);
        }

        return temp;

    }

    public ArrayList jobCardMrsDetails(int jobcardId) {
        Map map = new HashMap();
        ArrayList jobCardMrsDetails = new ArrayList();
        try {
            map.put("jobcardId", jobcardId);
            jobCardMrsDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.jobCardMrsDetails", map);
            //////System.out.println("jobCardMrsDetails size=" + jobCardMrsDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "jobCardMrsDetails List", sqlException);
        }
        return jobCardMrsDetails;
    }

    public String getActualKm(String vehicleNo) {
        Map map = new HashMap();
        String vehicleKm = "";
        String trailerNo = "0";
        try {
            String[] temp = vehicleNo.split("~");
            vehicleNo = temp[0];
            if (temp.length > 1) {
                trailerNo = temp[1];
            }
            vehicleNo = vehicleNo.replace(" ", "");
            System.out.println("get Veh no=" + vehicleNo);
            System.out.println("get trailerNo no=" + trailerNo);
            map.put("vehicleNo", vehicleNo);
            map.put("trailerNo", trailerNo);
            if (!"0".equals(vehicleNo) && !"".equals(vehicleNo)) {
                System.out.println("am here...1");
                vehicleKm = (String) getSqlMapClientTemplate().queryForObject("operation.getActualKm", map);
            } else {
                System.out.println("am here...2");
                vehicleKm = (String) getSqlMapClientTemplate().queryForObject("operation.getTrailerActualKm", map);
            }
            System.out.println("vehicleKm=:" + vehicleKm);
            if (vehicleKm == null) {
                vehicleKm = "0~0";
            }
            //////System.out.println("vehicleKm =" + vehicleKm);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return vehicleKm;
    }

    public ArrayList getWoDetail(String woId) {
        Map map = new HashMap();
        ArrayList woDetail = new ArrayList();
        try {
            map.put("woId", woId);
            woDetail = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getContractorWODetails", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return woDetail;
    }

    public ArrayList getContractJobcards() {
        Map map = new HashMap();
        ArrayList jobcardList = new ArrayList();
        try {
            jobcardList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getContractJobcards", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return jobcardList;
    }

    public ArrayList getContractWos(int jobCardId) {
        Map map = new HashMap();
        ArrayList woList = new ArrayList();
        try {
            map.put("jobCardId", jobCardId);
            woList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getContractwo", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return woList;
    }

    //-------------------------------- Trip Details Starts
//    public int saveTripDetails(String routeId, String vehicleId, String tripCode, String tripDate, String departureDate, String arrivalDate,
//            String driverNameId, String kmsOut, String kmsIn, String totalallowance, String totalliters, String totalfuelamount, String totalexpenses, String balanceamount, String totalkms, String status,
//            String tonnage, String companyId, String tripRevenue, String cleanerStatus, String tripType,
//            String pinkSlip, String orderNo, String bags, String gpsKm, String billStatus,
//            String vehicleType, String productId, String gpno,
//            String invoiceNo, int userId, String customertype, String twoLpsStatus, String tripRevenueOne, String tripRevenueTwo, String companyName, String selectedBillStatus, String extraKm) {
//        Map map = new HashMap();
//        int lastInsertedId = 0;
//        int count = 0;
//        int lpsStatus = 0;
//        int PinkSlipStatus = 0;
//        String[] tempRoute;
//        String[] lpsSplit;
//        String[] routeName;
//        String custVal = "";
//        try {
//            System.out.println("---------------------------------xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx------------------------------------------------");
//            map.put("billStatus", billStatus);
//            map.put("pinkSlip", pinkSlip);
//            map.put("orderNo", orderNo);
//            map.put("bags", bags);
//            map.put("gpsKm", gpsKm);
//            map.put("productId", productId);
//            if (tonnage.equals("")) {
//                tonnage = "0";
//            }
//            map.put("tonnage", tonnage);
//            map.put("companyId", companyId);
//            map.put("tripCode", tripCode);
////            if(twoLpsStatus == "Y"){
////            routeName = tripCode.split(",");
////            String routeVal = "'"+routeName[0]+"','"+routeName[1]+"'";
////            map.put("routeVal",routeVal);
////                custVal = (String) getSqlMapClientTemplate().queryForObject("operation.getRouteValues", map);
////                //////System.out.println("routeVal ::::= " + routeVal);
////
////            }
//
//            if (tripRevenue.equals("")) {
//                tripRevenue = "0";
//            }
//            map.put("tripRevenue", tripRevenue);
//            tempRoute = routeId.split(",");
//            map.put("routeId", tempRoute[0]);
//            map.put("identityNo", "01092012");
//            map.put("vehicleId", vehicleId);
//            map.put("tripDate", tripDate);
//            map.put("departureDate", departureDate);
//            map.put("arrivalDate", arrivalDate);
//            map.put("driverNameId", driverNameId);
//            map.put("kmsOut", kmsOut);
//            map.put("kmsIn", kmsIn);
//            map.put("totalallowance", totalallowance);
//            map.put("totalliters", totalliters);
//            map.put("totalfuelamount", totalfuelamount);
//            map.put("totalexpenses", totalexpenses);
//            map.put("balanceamount", balanceamount);
//            map.put("totalkms", totalkms);
//            map.put("extraKm", extraKm);
//            map.put("status", status);
//            map.put("cleanerStatus", cleanerStatus);
//            map.put("tripType", tripType);
//            map.put("settleFlag", "N");
//            map.put("gpno", gpno);
//            map.put("invoiceNo", invoiceNo);
//            map.put("createdBy", userId);
//            map.put("twoLpsStatus", twoLpsStatus);
//            map.put("tripRevenueOne", tripRevenueOne);
//            map.put("tripRevenueTwo", tripRevenueTwo);
//            map.put("companyName", companyName);
//            map.put("selectedBillStatus", selectedBillStatus);
//
//            //////System.out.println("map in Trip  *& -------> " + map);
//            count = (Integer) getSqlMapClientTemplate().queryForObject("operation.getTripExistCount", map);
//            //////System.out.println("count 1= " + count);
//            if (count == 0) {
//                //////System.out.println("count 2= " + count);
//                lastInsertedId = (Integer) getSqlMapClientTemplate().insert("operation.insertTripDetails", map);
//                //////System.out.println("lastInsertedId in Trip DAO" + lastInsertedId);
//                if (lastInsertedId > 0) {
//                    map.put("lastInsertedId", lastInsertedId);
//                    lpsSplit = orderNo.split(",");
//
//                    for (int i = 0; i < lpsSplit.length; i++) {
//                        map.put("lpsNo", lpsSplit[i]);
//                        //////System.out.println("lpsSplit[i] =:::::::::::> " + lpsSplit[i]);
//                        //////System.out.println("map ==> " + map);
//                        lpsStatus = (Integer) getSqlMapClientTemplate().update("operation.updateTripLps", map);
//                        //////System.out.println("lpsStatus = " + lpsStatus);
//                        PinkSlipStatus = (Integer) getSqlMapClientTemplate().update("operation.updateTripPinkSlip", map);
//                        //////System.out.println("PinkSlipStatus = " + PinkSlipStatus);
//                    }
//
//                }
//                //trip accountEntry posting
//                String[] temp;
//                String code2 = "";
//                String getTripNo = "";
//                int status1 = 0;
//                int status2 = 0;
//                String tempBillStatus[] = null;
//                String tempRevenueStatus[] = null;
//                if (selectedBillStatus.contains(",")) {
//                    float tripRevenueTotal = 0.0f;
//                    String billStatusPaid = "";
//                    String billStatusToPay = "";
//                    tempBillStatus = selectedBillStatus.split(",");
//                    String tempCompId[] = null;
//                    tempCompId = companyId.split(",");
//                    tempRevenueStatus = tripRevenueOne.split(",");
//                    for (int i = 0; i < tempBillStatus.length; i++) {
//                        //////System.out.println("tempBillStatus[i] in the DAO = " + tempBillStatus[i]);
//                        if (tempBillStatus[i].equalsIgnoreCase("topay")) {
//                            billStatus = tempBillStatus[i];
//                        } else {
//                            billStatus = tempBillStatus[i];
//                        }
//                        if ("topay".equalsIgnoreCase(billStatus) && "2".equals(vehicleType)) {
//                            tripRevenueTotal = Float.parseFloat(tempRevenueStatus[i]);
//                        } else {
//                            tripRevenueTotal = Float.parseFloat(tempRevenueStatus[i]);
//                        }
//                        companyId = tempCompId[i];
//                        //Multiple LPS Case
//                        if ("topay".equalsIgnoreCase(billStatus) && "2".equals(vehicleType)) {//when mkt vehicle and to pay
//                            //  --------------------------------- acc 1st row start --------------------------
//                            map.put("billStatus", billStatus);
//                            map.put("userId", userId);
//                            map.put("DetailCode", "1");
//                            map.put("voucherType", "%SALES%");
//                            code2 = (String) getSqlMapClientTemplate().queryForObject("operation.getTripVoucherCode", map);
//                            temp = code2.split("-");
//                            int codeval2 = Integer.parseInt(temp[1]);
//                            int codev2 = codeval2 + 1;
//                            String voucherCode = "SALES-" + codev2;
//                            //////System.out.println("voucherCode = " + voucherCode);
//                            map.put("voucherCode", voucherCode);
//                            map.put("mainEntryType", "VOUCHER");
//                            map.put("entryType", "SALES");
//                            map.put("ledgerId", "52");
//                            map.put("particularsId", "LEDGER-40");
//                            float tripRevenueNew = tripRevenueTotal * 8 / 100;  //8% Crossing
//                            //////System.out.println("Arularasan Here --------------------------------------------------------------------------- 1");
//                            //////System.out.println("tripRevenueNew " + map.get("particularsId") + " = " + tripRevenueNew);
//                            map.put("amount", tripRevenueNew);
//                            map.put("Accounts_Type", "DEBIT");
//                            map.put("Remark", "Freight Crossing Income");
//                            map.put("Reference", "Trip");
//                            getTripNo = (String) getSqlMapClientTemplate().queryForObject("operation.getTripNo", map);
//                            //////System.out.println("tripId = " + getTripNo);
//                            map.put("SearchCode", getTripNo.trim());
//                            //////System.out.println("map1 =---------------------> " + map);
//                            status1 = (Integer) getSqlMapClientTemplate().update("operation.insertTripAccountEntry", map);
//                            //////System.out.println("status1 = " + status1);
//                            //--------------------------------- acc 2nd row start --------------------------
//                            if (status1 > 0) {
//                                map.put("DetailCode", "2");
//                                map.put("ledgerId", "169");
//                                map.put("particularsId", "LEDGER-155");
//                                map.put("Accounts_Type", "CREDIT");
//                                //////System.out.println("Arularasan Here --------------------------------------------------------------------------- 2");
//                                //////System.out.println("tripRevenueNew " + map.get("particularsId") + " = " + tripRevenueNew);
//                                //////System.out.println("map2 =---------------------> " + map);
//                                status2 = (Integer) getSqlMapClientTemplate().update("operation.insertTripAccountEntry", map);
//                                //////System.out.println("status2 = " + status2);
//                            }
//                            //--------------------------------- acc 2nd row end --------------------------
//                        } else {
//                            //  --------------------------------- acc 1st row start --------------------------
//                            map.put("billStatus", billStatus);
//                            map.put("userId", userId);
//                            map.put("DetailCode", "1");
//                            map.put("voucherType", "%SALES%");
//                            code2 = (String) getSqlMapClientTemplate().queryForObject("operation.getTripVoucherCode", map);
//                            temp = code2.split("-");
//                            int codeval2 = Integer.parseInt(temp[1]);
//                            int codev2 = codeval2 + 1;
//                            String voucherCode = "SALES-" + codev2;
//                            //////System.out.println("voucherCode = " + voucherCode);
//                            map.put("voucherCode", voucherCode);
//                            map.put("mainEntryType", "VOUCHER");
//                            map.put("entryType", "SALES");
//                            //////System.out.println("billStatus:" + billStatus);
//                            //////System.out.println("companyId:" + companyId);
//                            if ("paid".equalsIgnoreCase(billStatus)) {
//                                map.put("ledgerId", "48");
//                                map.put("particularsId", "LEDGER-36");
//                            } else {
//                                //get ledger info
//                                map.put("customer", companyId);
//                                String ledgerInfo = (String) getSqlMapClientTemplate().queryForObject("operation.getCustomerLedgerInfo", map);
//                                //////System.out.println("ledgerInfo:" + ledgerInfo);
//                                temp = ledgerInfo.split("~");
//                                String ledgerId = temp[0];
//                                String particularsId = temp[1];
//                                map.put("ledgerId", ledgerId);
//                                map.put("particularsId", particularsId);
//                            }
//
//                            map.put("amount", tripRevenueTotal);
//                            map.put("Accounts_Type", "DEBIT");
//                            map.put("Remark", "Freight Charges");
//                            map.put("Reference", "Trip");
//                            //////System.out.println("Arularasan Here ---------------------------------------------------------------------------3 ");
//                            //////System.out.println("tripRevenueNew " + map.get("particularsId") + " = " + tripRevenue);
//                            getTripNo = (String) getSqlMapClientTemplate().queryForObject("operation.getTripNo", map);
//                            //////System.out.println("tripId = " + getTripNo);
//                            map.put("SearchCode", getTripNo.trim());
//                            //////System.out.println("map1 =---------------------> " + map);
//                            status1 = (Integer) getSqlMapClientTemplate().update("operation.insertTripAccountEntry", map);
//                            //////System.out.println("status1 = " + status1);
//                            //--------------------------------- acc 2nd row start --------------------------
//                            if (status1 > 0) {
//                                map.put("DetailCode", "2");
//                                map.put("ledgerId", "51");
//                                map.put("particularsId", "LEDGER-39");
//                                map.put("Accounts_Type", "CREDIT");
//                                //////System.out.println("Arularasan Here ---------------------------------------------------------------------------4 ");
//                                //////System.out.println("tripRevenueNew " + map.get("particularsId") + " = " + tripRevenue);
//                                //////System.out.println("map2 =---------------------> " + map);
//                                status2 = (Integer) getSqlMapClientTemplate().update("operation.insertTripAccountEntry", map);
//                                //////System.out.println("status2 = " + status2);
//                            }
//                            //--------------------------------- acc 2nd row end --------------------------
//                        }
//                    }
//                } else {
//                    //Single LPS Case
//                    if ("topay".equalsIgnoreCase(billStatus) && "2".equals(vehicleType)) {//when mkt vehicle and to pay
//                        //  --------------------------------- acc 1st row start --------------------------
//                        map.put("billStatus", billStatus);
//                        map.put("userId", userId);
//                        map.put("DetailCode", "1");
//                        map.put("voucherType", "%SALES%");
//                        code2 = (String) getSqlMapClientTemplate().queryForObject("operation.getTripVoucherCode", map);
//                        temp = code2.split("-");
//                        int codeval2 = Integer.parseInt(temp[1]);
//                        int codev2 = codeval2 + 1;
//                        String voucherCode = "SALES-" + codev2;
//                        //////System.out.println("voucherCode = " + voucherCode);
//                        map.put("voucherCode", voucherCode);
//                        map.put("mainEntryType", "VOUCHER");
//                        map.put("entryType", "SALES");
//                        map.put("ledgerId", "52");
//                        map.put("particularsId", "LEDGER-40");
//                        float tripRevenueNew = Float.parseFloat(tripRevenue) * 8 / 100;  //8% Crossing
//                        //////System.out.println("Arularasan Here --------------------------------------------------------------------------- 1");
//                        //////System.out.println("tripRevenueNew " + map.get("particularsId") + " = " + tripRevenueNew);
//                        map.put("amount", tripRevenueNew);
//                        map.put("Accounts_Type", "DEBIT");
//                        map.put("Remark", "Freight Crossing Income");
//                        map.put("Reference", "Trip");
//                        getTripNo = (String) getSqlMapClientTemplate().queryForObject("operation.getTripNo", map);
//                        //////System.out.println("tripId = " + getTripNo);
//                        map.put("SearchCode", getTripNo.trim());
//                        //////System.out.println("map1 =---------------------> " + map);
//                        status1 = (Integer) getSqlMapClientTemplate().update("operation.insertTripAccountEntry", map);
//                        //////System.out.println("status1 = " + status1);
//                        //--------------------------------- acc 2nd row start --------------------------
//                        if (status1 > 0) {
//                            map.put("DetailCode", "2");
//                            map.put("ledgerId", "169");
//                            map.put("particularsId", "LEDGER-155");
//                            map.put("Accounts_Type", "CREDIT");
//                            //////System.out.println("Arularasan Here --------------------------------------------------------------------------- 2");
//                            //////System.out.println("tripRevenueNew " + map.get("particularsId") + " = " + tripRevenueNew);
//                            //////System.out.println("map2 =---------------------> " + map);
//                            status2 = (Integer) getSqlMapClientTemplate().update("operation.insertTripAccountEntry", map);
//                            //////System.out.println("status2 = " + status2);
//                        }
//                        //--------------------------------- acc 2nd row end --------------------------
//                    } else {
//                        //  --------------------------------- acc 1st row start --------------------------
//                        map.put("billStatus", billStatus);
//                        map.put("userId", userId);
//                        map.put("DetailCode", "1");
//                        map.put("voucherType", "%SALES%");
//                        code2 = (String) getSqlMapClientTemplate().queryForObject("operation.getTripVoucherCode", map);
//                        temp = code2.split("-");
//                        int codeval2 = Integer.parseInt(temp[1]);
//                        int codev2 = codeval2 + 1;
//                        String voucherCode = "SALES-" + codev2;
//                        //////System.out.println("voucherCode = " + voucherCode);
//                        map.put("voucherCode", voucherCode);
//                        map.put("mainEntryType", "VOUCHER");
//                        map.put("entryType", "SALES");
//                        //////System.out.println("billStatus:" + billStatus);
//                        //////System.out.println("companyId:" + companyId);
//                        if ("paid".equalsIgnoreCase(billStatus)) {
//                            map.put("ledgerId", "48");
//                            map.put("particularsId", "LEDGER-36");
//                        } else {
//                            //get ledger info
//                            map.put("customer", companyId);
//                            String ledgerInfo = (String) getSqlMapClientTemplate().queryForObject("operation.getCustomerLedgerInfo", map);
//                            //////System.out.println("ledgerInfo:" + ledgerInfo);
//                            temp = ledgerInfo.split("~");
//                            String ledgerId = temp[0];
//                            String particularsId = temp[1];
//                            map.put("ledgerId", ledgerId);
//                            map.put("particularsId", particularsId);
//                        }
//
//                        map.put("amount", tripRevenue);
//                        map.put("Accounts_Type", "DEBIT");
//                        map.put("Remark", "Freight Charges");
//                        map.put("Reference", "Trip");
//                        //////System.out.println("Arularasan Here ---------------------------------------------------------------------------3 ");
//                        //////System.out.println("tripRevenueNew " + map.get("particularsId") + " = " + tripRevenue);
//                        getTripNo = (String) getSqlMapClientTemplate().queryForObject("operation.getTripNo", map);
//                        //////System.out.println("tripId = " + getTripNo);
//                        map.put("SearchCode", getTripNo.trim());
//                        //////System.out.println("map1 =---------------------> " + map);
//                        status1 = (Integer) getSqlMapClientTemplate().update("operation.insertTripAccountEntry", map);
//                        //////System.out.println("status1 = " + status1);
//                        //--------------------------------- acc 2nd row start --------------------------
//                        if (status1 > 0) {
//                            map.put("DetailCode", "2");
//                            map.put("ledgerId", "51");
//                            map.put("particularsId", "LEDGER-39");
//                            map.put("Accounts_Type", "CREDIT");
//                            //////System.out.println("Arularasan Here ---------------------------------------------------------------------------4 ");
//                            //////System.out.println("tripRevenueNew " + map.get("particularsId") + " = " + tripRevenue);
//                            //////System.out.println("map2 =---------------------> " + map);
//                            status2 = (Integer) getSqlMapClientTemplate().update("operation.insertTripAccountEntry", map);
//                            //////System.out.println("status2 = " + status2);
//                        }
//                        //--------------------------------- acc 2nd row end --------------------------
//                    }
//                }
//            }
//        } catch (Exception sqlException) {
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
//        }
//        return lastInsertedId;
//    }

    public int saveTripClosureAccountDetails(ArrayList etmExpDetails, String vehicleType, String driverType,
            String vehicleVendorId, String driverVendorId,
            String driverNameId, int userId, String settlementType) {
        Map map = new HashMap();
        int lastInsertedId = 0;
        try {

            map.put("createdBy", userId);

            //trip accountEntry postiong
            String[] temp;
            String code2 = "";
            String getTripNo = "";
            int status1 = 0;
            int status2 = 0;
            map.put("driverId", driverNameId);
            Iterator itr = etmExpDetails.iterator();
            TripFuelDetailsTO optTo = null;
            while (itr.hasNext()) {
                optTo = new TripFuelDetailsTO();
                optTo = (TripFuelDetailsTO) itr.next();

                //  --------------------------------- acc 1st row start --------------------------
                map.put("userId", userId);
                map.put("DetailCode", "1");
                map.put("voucherType", "%PAYMENT%");
                code2 = (String) getSqlMapClientTemplate().queryForObject("operation.getTripVoucherCode", map);
                temp = code2.split("-");
                int codeval2 = Integer.parseInt(temp[1]);
                int codev2 = codeval2 + 1;
                String voucherCode = "PAYMENT-" + codev2;
                //////System.out.println("voucherCode = " + voucherCode);
                map.put("voucherCode", voucherCode);
                map.put("mainEntryType", "VOUCHER");
                map.put("entryType", "PAYMENT");
                map.put("ledgerId", optTo.getLedgerId());
                map.put("particularsId", optTo.getLedgerCode());
                map.put("amount", optTo.getAmount());
                map.put("Accounts_Type", "DEBIT");
                map.put("Remark", "Expenses");
                map.put("Reference", optTo.getReturnTripId());
                map.put("SearchCode", optTo.getTripSheetId());

                //////System.out.println("map1 =---------------------> " + map);
                status1 = (Integer) getSqlMapClientTemplate().update("operation.insertTripAccountEntry", map);
                //////System.out.println("status1 = " + status1);
                //--------------------------------- acc 2nd row start --------------------------
                String ledgerId = "";
                String particularsId = "";
                String ledgerInfo = "";
                if (status1 > 0) {
                    map.put("DetailCode", "2");
                    if ("1".equals(vehicleType)) {//own Vehicle
                        if ("NO".equals(driverType)) {//own Driver, CREDIT TO KARIKALI CASH A/C
                            ledgerId = "52";
                            particularsId = "LEDGER-40";
                        } else {
                            //credit to contract/vendor of driver
                            map.put("vendorId", driverVendorId);
                            ledgerInfo = (String) getSqlMapClientTemplate().queryForObject("operation.getVendorLedgerInfo", map);
                            temp = ledgerInfo.split("~");
                            ledgerId = temp[0];
                            particularsId = temp[1];
                        }
                    } else {//credit to contract/vendor of vehicle
                        map.put("vendorId", vehicleVendorId);
                        ledgerInfo = (String) getSqlMapClientTemplate().queryForObject("operation.getVendorLedgerInfo", map);
                        temp = ledgerInfo.split("~");
                        ledgerId = temp[0];
                        particularsId = temp[1];
                    }
                    map.put("ledgerId", ledgerId);
                    map.put("particularsId", particularsId);

                    map.put("Accounts_Type", "CREDIT");
                    //////System.out.println("map2 =---------------------> " + map);
                    status2 = (Integer) getSqlMapClientTemplate().update("operation.insertTripAccountEntry", map);
                    //////System.out.println("status2 = " + status2);
                }
                //--------------------------------- acc 2nd row end --------------------------

                //if settlement type is cash - start
                if (!"1".equals(vehicleType) && "1".equals(settlementType)) {//mkt vehicle and settlement type is cash
                    //////System.out.println("mkt vehicle and settlement type is cash");
                    map.put("userId", userId);
                    map.put("voucherType", "%RECEIPT%");
                    code2 = (String) getSqlMapClientTemplate().queryForObject("operation.getTripVoucherCode", map);
                    temp = code2.split("-");
                    codeval2 = Integer.parseInt(temp[1]);
                    codev2 = codeval2 + 1;
                    voucherCode = "RECEIPT-" + codev2;
                    //////System.out.println("voucherCode = " + voucherCode);
                    map.put("voucherCode", voucherCode);
                    map.put("mainEntryType", "VOUCHER");
                    map.put("entryType", "RECEIPT");
                    map.put("DetailCode", "1");
                    map.put("ledgerId", ledgerId);
                    map.put("particularsId", particularsId);
                    map.put("Accounts_Type", "DEBIT");
                    status1 = (Integer) getSqlMapClientTemplate().update("operation.insertTripAccountEntry", map);
                    //////System.out.println("status1 = " + status1);
                    if (status1 > 0) {
                        map.put("DetailCode", "2");
                        ledgerId = "52";
                        particularsId = "LEDGER-40";
                        map.put("ledgerId", ledgerId);
                        map.put("particularsId", particularsId);
                        map.put("Accounts_Type", "CREDIT");
                        //////System.out.println("map2 =---------------------> " + map);
                        status2 = (Integer) getSqlMapClientTemplate().update("operation.insertTripAccountEntry", map);
                        //////System.out.println("status2 = " + status2);
                    }
                }
                //if settlement type is cash - end
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripClosureAccountDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripClosureAccountDetails ", sqlException);
        }
        return lastInsertedId;
    }

    public int saveTripClosureAdvanceAccountDetails(ArrayList tripAllowanceDetails, String vehicleType, String driverType,
            String vehicleVendorId, String driverVendorId,
            String driverNameId, int userId) {
        Map map = new HashMap();
        int lastInsertedId = 0;
        try {

            map.put("createdBy", userId);

            //trip accountEntry postiong
            String[] temp;
            String code2 = "";
            String getTripNo = "";
            int status1 = 0;
            int status2 = 0;
            map.put("driverId", driverNameId);
            Iterator itr = tripAllowanceDetails.iterator();
            TripAllowanceTO optTo = null;
            while (itr.hasNext()) {
                optTo = new TripAllowanceTO();
                optTo = (TripAllowanceTO) itr.next();

                //  --------------------------------- acc 1st row start --------------------------
                map.put("userId", userId);
                map.put("DetailCode", "1");
                map.put("voucherType", "%RECEIPT%");
                code2 = (String) getSqlMapClientTemplate().queryForObject("operation.getTripVoucherCode", map);
                temp = code2.split("-");
                int codeval2 = Integer.parseInt(temp[1]);
                int codev2 = codeval2 + 1;
                String voucherCode = "RECEIPT-" + codev2;
                //////System.out.println("voucherCode = " + voucherCode);
                map.put("voucherCode", voucherCode);
                map.put("mainEntryType", "VOUCHER");
                map.put("entryType", "RECEIPT");
                map.put("ledgerId", "52");
                map.put("particularsId", "LEDGER-40");
                map.put("amount", optTo.getTripAllowanceAmount());
                map.put("Accounts_Type", "DEBIT");
                map.put("Remark", "Advance Reversal");
                map.put("Reference", optTo.getReturnTripId());
                map.put("SearchCode", optTo.getTripId());
                //////System.out.println("map1 =---------------------> " + map);
                status1 = (Integer) getSqlMapClientTemplate().update("operation.insertTripAccountEntry", map);
                //////System.out.println("status1 = " + status1);
                //--------------------------------- acc 2nd row start --------------------------
                String ledgerId = "";
                String particularsId = "";
                String ledgerInfo = "";
                if (status1 > 0) {
                    map.put("DetailCode", "2");
                    map.put("vendorId", driverVendorId);
                    if ("NO".equals(driverType)) {//own Driver
                        ledgerInfo = (String) getSqlMapClientTemplate().queryForObject("operation.getDriverLedgerInfo", map);
                        temp = ledgerInfo.split("~");
                        ledgerId = temp[0];
                        particularsId = temp[1];
                    } else {//contract driver
                        ledgerInfo = (String) getSqlMapClientTemplate().queryForObject("operation.getVendorLedgerInfo", map);
                        temp = ledgerInfo.split("~");
                        ledgerId = temp[0];
                        particularsId = temp[1];
                    }

                    map.put("ledgerId", ledgerId);
                    map.put("particularsId", particularsId);

                    map.put("Accounts_Type", "CREDIT");
                    //////System.out.println("map2 =---------------------> " + map);
                    status2 = (Integer) getSqlMapClientTemplate().update("operation.insertTripAccountEntry", map);
                    //////System.out.println("status2 = " + status2);
                }
                //--------------------------------- acc 2nd row end --------------------------
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripClosureAccountDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripClosureAccountDetails ", sqlException);
        }
        return lastInsertedId;
    }

    public int reverseToPayRevenue(String tripSheetIdParam, String tripRevenue, String customerId, int userId) {
        Map map = new HashMap();
        int lastInsertedId = 0;
        try {
            map.put("createdBy", userId);
            //trip accountEntry postiong
            String[] temp;
            String code2 = "";
            String getTripNo = "";
            int status1 = 0;
            int status2 = 0;
            //  --------------------------------- acc 1st row start --------------------------
            map.put("userId", userId);
            map.put("DetailCode", "1");
            map.put("voucherType", "%RECEIPT%");
            code2 = (String) getSqlMapClientTemplate().queryForObject("operation.getTripVoucherCode", map);
            temp = code2.split("-");
            int codeval2 = Integer.parseInt(temp[1]);
            int codev2 = codeval2 + 1;
            String voucherCode = "RECEIPT-" + codev2;
            //////System.out.println("voucherCode = " + voucherCode);
            map.put("voucherCode", voucherCode);
            map.put("mainEntryType", "VOUCHER");
            map.put("entryType", "RECEIPT");
            map.put("amount", tripRevenue);
            map.put("Accounts_Type", "DEBIT");
            map.put("Remark", "To Pay Receipt");
            map.put("Reference", "");
            map.put("SearchCode", tripSheetIdParam);
            map.put("ledgerId", "52");
            map.put("particularsId", "LEDGER-40");
            //////System.out.println("map1 =---------------------> " + map);
            status1 = (Integer) getSqlMapClientTemplate().update("operation.insertTripAccountEntry", map);
            //////System.out.println("status1 = " + status1);

            //--------------------------------- acc 2nd row start --------------------------
            if (status1 > 0) {
                String ledgerId = "";
                String particularsId = "";
                String ledgerInfo = "";
                map.put("customer", customerId);
                map.put("DetailCode", "2");
                map.put("Accounts_Type", "CREDIT");
                ledgerInfo = (String) getSqlMapClientTemplate().queryForObject("operation.getCustomerLedgerInfo", map);
                temp = ledgerInfo.split("~");
                ledgerId = temp[0];
                particularsId = temp[1];
                map.put("ledgerId", ledgerId);
                map.put("particularsId", particularsId);
                //////System.out.println("map2 =---------------------> " + map);
                status2 = (Integer) getSqlMapClientTemplate().update("operation.insertTripAccountEntry", map);
                //////System.out.println("status2 = " + status2);
            }
            //--------------------------------- acc 2nd row end --------------------------

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("reverseToPayRevenue Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "reverseToPayRevenue ", sqlException);
        }
        return lastInsertedId;
    }

//    public int updateTripDetails(String tripSheetId, String routeId, String vehicleId, String tripCode, String tripDate, String departureDate, String arrivalDate,
//            String driverNameId, String kmsOut, String kmsIn, String totalallowance, String totalliters, String totalfuelamount, String totalexpenses, String balanceamount, String totalkms, String status,
//            String loadedTonnage, String deliveredTonnage, String cleanerStatus, String tripType, String fromLocation, String toLocation, String productName, String returnAmount, String returnExpenses,
//            String tripReceivedAmount, String returnLoadedTonnage, String returnLoadedDate, String returnDeliveredTonnage, String returnDeliveredDate, String shortageTonnage,
//            String totalExpensesReturn, String balanceAmountRetuen, int userId) {
    public int updateTripDetails(String tripSheetId, String routeId, String vehicleId, String tripCode, String tripDate, String departureDate, String arrivalDate,
            String driverNameId, String kmsOut, String kmsIn, String totalallowance, String totalliters, String totalfuelamount, String totalexpenses, String balanceamount, String totalkms, String status,
            String loadedTonnage, String deliveredTonnage, String cleanerStatus, String tripType,
            String totalExpensesReturn, String balanceAmountRetuen, int userId, String mileage, String settlementFlag) {
        Map map = new HashMap();
        int updstatus = 0;
        try {
            map.put("mileage", mileage);
            map.put("settlementFlag", settlementFlag);
            map.put("tripSheetId", tripSheetId);
            map.put("routeId", routeId);
            map.put("vehicleId", vehicleId);
            map.put("tripCode", tripCode);
            map.put("tripDate", tripDate);
            map.put("departureDate", departureDate);
            map.put("arrivalDate", arrivalDate);
            map.put("driverNameId", driverNameId);
            map.put("kmsOut", kmsOut);
            map.put("kmsIn", kmsIn);
            map.put("totalallowance", totalallowance);
            map.put("totalliters", totalliters);
            map.put("totalfuelamount", totalfuelamount);
            map.put("totalexpenses", totalexpenses);
            map.put("balanceamount", balanceamount);
            map.put("totalkms", totalkms);
            map.put("status", status);
            map.put("cleanerStatus", cleanerStatus);
            map.put("tripType", tripType);
            //            clpl return trip
            if (loadedTonnage == null || ("").equals(loadedTonnage)) {
                loadedTonnage = "0";
            }
            map.put("loadedTonnage", loadedTonnage);
            //////System.out.println("deliveredTonnage---------- =>111--> " + deliveredTonnage);
            if (deliveredTonnage == null || ("").equals(deliveredTonnage)) {
                deliveredTonnage = "0";
                //////System.out.println("deliveredTonnage---------- =>222--> " + deliveredTonnage);
            }
            map.put("deliveredTonnage", deliveredTonnage);
//            if (toLocation == null ) {toLocation = "";}
//            map.put("toLocation", toLocation);
//            if (fromLocation == null) {fromLocation = "";}
//            map.put("fromLocation", fromLocation);
//            if (productName == null) {productName = "";}
//            map.put("productName", productName);
//            if (returnAmount == null || ("").equals(returnAmount)) {returnAmount = "0";}
//            map.put("returnAmount", returnAmount);
//            if (returnExpenses == null || ("").equals(returnExpenses)) {returnExpenses = "0";}
//            map.put("returnExpenses", returnExpenses);
//            if (tripReceivedAmount == null || ("").equals(tripReceivedAmount)) {tripReceivedAmount = "0";}
//            map.put("tripReceivedAmount", tripReceivedAmount);
//            if (returnLoadedTonnage == null || ("").equals(returnLoadedTonnage)) {returnLoadedTonnage = "0";}
//            map.put("returnLoadedTonnage", returnLoadedTonnage);
//            if (returnLoadedDate == null) {returnLoadedDate = "";}
//            map.put("returnLoadedDate", returnLoadedDate);
//            if (returnDeliveredTonnage == null || ("").equals(returnDeliveredTonnage)) {returnDeliveredTonnage = "0";}
//            map.put("returnDeliveredTonnage", returnDeliveredTonnage);
//            if (returnDeliveredDate == null) {returnDeliveredDate = "";}
//            map.put("returnDeliveredDate", returnDeliveredDate);
//            if (shortageTonnage == null || ("").equals(shortageTonnage)) {shortageTonnage = "0";}
//            map.put("shortageTonnage", shortageTonnage);
            if (balanceAmountRetuen == null || ("").equals(balanceAmountRetuen)) {
                balanceAmountRetuen = "0";
            }
            map.put("balanceAmountRetuen", balanceAmountRetuen);
            if (totalExpensesReturn == null || ("").equals(totalExpensesReturn)) {
                totalExpensesReturn = "0";
            }
            map.put("totalExpensesReturn", totalExpensesReturn);

            map.put("userId", userId);
            //////System.out.println("updateTripDetails DAO map: " + map);
            /*//////System.out.println("tripSheetId: "+tripSheetId);
            //////System.out.println("routeId: "+routeId);
            //////System.out.println("vehicleId: "+vehicleId);
            //////System.out.println("tripCode: "+tripCode);
            //////System.out.println("tripDate: "+tripDate);
            //////System.out.println("departureDate: "+departureDate);
            //////System.out.println("arrivalDate: "+arrivalDate);
            //////System.out.println("driverNameId: "+driverNameId);
            //////System.out.println("kmsOut: "+kmsOut);
            //////System.out.println("kmsIn: "+kmsIn);
            //////System.out.println("totalallowance: "+totalallowance);
            //////System.out.println("totalliters: "+totalliters);
            //////System.out.println("totalfuelamount: "+totalfuelamount);
            //////System.out.println("totalexpenses: "+totalexpenses);
            //////System.out.println("balanceamount: "+balanceamount);
            //////System.out.println("totalkms: "+totalkms);
            //////System.out.println("status: "+status);
            //////System.out.println("loadedTonnage: "+loadedTonnage);
            //////System.out.println("deliveredTonnage: "+deliveredTonnage);
            //////System.out.println("cleanerStatus: "+cleanerStatus);
            //////System.out.println("tripType: "+tripType);*/

            // //////System.out.println("map in Trip "+map);
            if (status.equals("Open")) {
                updstatus = (Integer) getSqlMapClientTemplate().update("operation.updateTripDetails", map);
                //////System.out.println("Opern");

                //trip edit accountEntry postiong
                int accStatus = 0;
                int accStatus1 = 0;
                map.put("DetailCode", "1");
                map.put("amount", totalexpenses);
                //////System.out.println("map ======-----------------IIII--> " + map);
                accStatus = (Integer) getSqlMapClientTemplate().update("operation.updateTripAccountEntry", map);
                //////System.out.println("accStatus = " + accStatus);
                //--------------------------------- expenses 2nd row start --------------------------
                if (accStatus > 0) {
                    map.put("DetailCode", "2");
                    accStatus1 = (Integer) getSqlMapClientTemplate().update("operation.updateTripAccountEntry", map);
                    //////System.out.println("accStatus1 = " + accStatus1);
                }
                //--------------------------------- expenses 2nd row end --------------------------
            } else {
                //////System.out.println("map0000000000000000000 => " + map);
//                int delAllowance = (Integer) getSqlMapClientTemplate().update("operation.deleteAllowanceDetails", map);
//                int delExpenses = (Integer) getSqlMapClientTemplate().update("operation.deleteExpensesDetails", map);
//                int delFuelDetails = (Integer) getSqlMapClientTemplate().update("operation.deleteFuelDetails", map);
                updstatus = (Integer) getSqlMapClientTemplate().update("operation.updateTripDetails", map);

                /* srini commented on 21 march 2013

                //trip edit accountEntry postiong
                String[] temp;
                String[] temp1;
                String code2 = "";
                String code3 = "";
                String getTripNo = "";
                int status1 = 0;
                int status2 = 0;
                int status3 = 0;
                int status4 = 0;
                //  --------------------------------- expenses 1st row start --------------------------
                map.put("userId", userId);
                map.put("DetailCode", "3");
                code2 = (String) getSqlMapClientTemplate().queryForObject("operation.getTripVoucherCode", map);
                temp = code2.split("-");
                int codeval2 = Integer.parseInt(temp[1]);
                int codev2 = codeval2 + 1;
                String voucherCode = "PAYMENT-" + codev2;
                //////System.out.println("voucherCode = " + voucherCode);
                map.put("voucherCode", voucherCode);
                map.put("mainEntryType", "VOUCHER");
                map.put("entryType", "PAYMENT");
                map.put("particularsId", "LEDGER-15");
                map.put("amount", totalExpensesReturn);
                map.put("Accounts_Type", "DEBIT");
                map.put("Remark", "Trip Expenses");
                map.put("Reference", "Trip");
                map.put("SearchCode", tripSheetId);
                //////System.out.println("map1 =---------------------> " + map);
                status1 = (Integer) getSqlMapClientTemplate().update("operation.insertTripAccountEntry", map);
                //////System.out.println("status1 = " + status1);
                //--------------------------------- expenses 2nd row start --------------------------
                if (status1 > 0) {
                map.put("DetailCode", "4");
                map.put("particularsId", "LEDGER-1");
                map.put("Accounts_Type", "CREDIT");
                //////System.out.println("map2 =---------------------> " + map);
                status2 = (Integer) getSqlMapClientTemplate().update("operation.insertTripAccountEntry", map);
                //////System.out.println("status2 = " + status2);
                }
                //--------------------------------- expenses 2nd row end --------------------------

                //  --------------------------------- Balance 1st row start --------------------------
                map.put("DetailCode", "1");
                code3 = (String) getSqlMapClientTemplate().queryForObject("operation.getTripBalVoucherCode", map);
                temp = code3.split("-");
                int codeval3 = Integer.parseInt(temp[1]);
                int codev3 = codeval3 + 1;
                String voucherCode1 = "RECEIPT-" + codev3;
                //////System.out.println("voucherCode1 = " + voucherCode1);
                map.put("voucherCode", voucherCode1);
                map.put("mainEntryType", "VOUCHER");
                map.put("entryType", "RECEIPT");
                map.put("particularsId", "LEDGER-15");
                map.put("amount", balanceAmountRetuen);
                map.put("Accounts_Type", "DEBIT");
                map.put("Remark", "Trip Balance Amount");
                map.put("Reference", "Trip");
                map.put("SearchCode", tripSheetId);
                //////System.out.println("map1 =---------------------> " + map);
                status1 = (Integer) getSqlMapClientTemplate().update("operation.insertTripAccountEntry", map);
                //////System.out.println("status1 = " + status1);
                //--------------------------------- expenses 2nd row start --------------------------
                if (status1 > 0) {
                map.put("DetailCode", "2");
                map.put("particularsId", "LEDGER-1");
                map.put("Accounts_Type", "CREDIT");
                //////System.out.println("map2 =---------------------> " + map);
                status2 = (Integer) getSqlMapClientTemplate().update("operation.insertTripAccountEntry", map);
                //////System.out.println("status2 = " + status2);
                }
                //--------------------------------- expenses 2nd row end --------------------------


                 */
                //////System.out.println("Close");
            }
            //////System.out.println("updateTripDetails -->" + updstatus);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return updstatus;
    }

    public int insertTripAllowence(int userId, String returnTripId, int tripId, String stageId, String tripAllowanceDate, String tripAllowanceAmount,
            String tripAllowancePaidBy, String tripAllowanceRemarks, String driverId, String driverType, String driverVendorId,
            String vehicleType, String vehicleVendorId) {
        Map map = new HashMap();
        int lastInsertedId = 0;
        try {
            map.put("tripId", tripId);
            map.put("returnTripId", returnTripId);
            map.put("tripSheetId", tripId);
            map.put("stageId", stageId);
            map.put("tripAllowanceDate", tripAllowanceDate);
            map.put("tripAllowanceAmount", tripAllowanceAmount);
            map.put("tripAllowancePaidBy", tripAllowancePaidBy);
            map.put("tripAllowanceRemarks", tripAllowanceRemarks);
            map.put("createdBy", userId);
            //////System.out.println("map in tripAllowance " + map);

//            String IdentNo = (String) getSqlMapClientTemplate().queryForObject("operation.getIdentityNo", map);
//            //////System.out.println("insertTripAllowence -- IdentNo: " + IdentNo);
//            if (!IdentNo.equals("01092012")) {
//                //////System.out.println("if...IdentNo: " + IdentNo);
//                map.put("identityNo", IdentNo);
//            } else {
//                //////System.out.println("else...IdentNo: " + IdentNo);
//                map.put("identityNo", "01092012");
//            }
            lastInsertedId = (Integer) getSqlMapClientTemplate().update("operation.insertTripAllowance", map);

            //////System.out.println("insertTripAllowence -->" + lastInsertedId);
            //  --------------------------------- acc 1st row start --------------------------
            map.put("userId", userId);
            map.put("DetailCode", "1");
            map.put("voucherType", "%PAYMENT%");
            String code2 = (String) getSqlMapClientTemplate().queryForObject("operation.getTripVoucherCode", map);
            String[] temp = code2.split("-");
            int codeval2 = Integer.parseInt(temp[1]);
            int codev2 = codeval2 + 1;
            String voucherCode = "PAYMENT-" + codev2;
            //////System.out.println("voucherCode = " + voucherCode);
            map.put("voucherCode", voucherCode);
            map.put("mainEntryType", "VOUCHER");
            map.put("entryType", "PAYMENT");
            map.put("ledgerId", "52");
            map.put("particularsId", "LEDGER-40");
            map.put("amount", tripAllowanceAmount);
            map.put("Accounts_Type", "CREDIT");
            map.put("Remark", "Trip Allowance");
            map.put("Reference", returnTripId);
            map.put("SearchCode", tripId);
            map.put("driverId", driverId);

            //////System.out.println("map1 =---------------------> " + map);
            int status1 = (Integer) getSqlMapClientTemplate().update("operation.insertTripAccountEntry", map);
            //////System.out.println("status1 = " + status1);
            //--------------------------------- acc 2nd row start --------------------------
            if (status1 > 0) {
                //identify debit account
                //if own driver, debit account is driver account
                //if contract driver, the debit account is contract vendor account
                String ledgerId = "";
                String particularsId = "";
                if ("1".equals(vehicleType)) {//own Vehicle
                    if ("NO".equals(driverType)) {//own Driver
                        String ledgerInfo = (String) getSqlMapClientTemplate().queryForObject("operation.getDriverLedgerInfo", map);
                        temp = ledgerInfo.split("~");
                        ledgerId = temp[0];
                        particularsId = temp[1];
                    } else {//contract driver
                        map.put("vendorId", driverVendorId);
                        String ledgerInfo = (String) getSqlMapClientTemplate().queryForObject("operation.getVendorLedgerInfo", map);
                        temp = ledgerInfo.split("~");
                        ledgerId = temp[0];
                        particularsId = temp[1];
                    }
                } else {
                    map.put("vendorId", vehicleVendorId);
                    String ledgerInfo = (String) getSqlMapClientTemplate().queryForObject("operation.getVendorLedgerInfo", map);
                    temp = ledgerInfo.split("~");
                    ledgerId = temp[0];
                    particularsId = temp[1];
                }

                map.put("DetailCode", "2");
                map.put("ledgerId", ledgerId);
                map.put("particularsId", particularsId);
                map.put("Accounts_Type", "DEBIT");
                //////System.out.println("map2 =---------------------> " + map);
                int status2 = (Integer) getSqlMapClientTemplate().update("operation.insertTripAccountEntry", map);
                //////System.out.println("status2 = " + status2);
            }
            //--------------------------------- acc 2nd row end --------------------------

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return lastInsertedId;
    }

    public int resetTripAllowence(String returnTripId, int tripId) {
        Map map = new HashMap();
        int lastInsertedId = 0;
        try {
            map.put("tripId", tripId);
            map.put("tripSheetId", tripId);
            //////System.out.println("map in tripAllowance " + map);
            if ("0".equals(returnTripId)) {  //for edit tripsheet case alone
                int advUpdate = (Integer) getSqlMapClientTemplate().update("operation.deleteAllowanceDetails", map);
                advUpdate = (Integer) getSqlMapClientTemplate().update("operation.deleteAllowanceAccountEntryDetails", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return lastInsertedId;
    }

    public int insertTripExpenses(int userId, String returnTripId, int tripId, String stageIdExp, String tripExpensesDate, String tripExpensesAmount,
            String tripExpensesPaidBy, String tripExpensesRemarks, String tripFuelLitre) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripId);
            map.put("tripSheetId", tripId);
            map.put("returnTripId", returnTripId);
            String[] temp = stageIdExp.split("~");

            map.put("expenseId", temp[0]);
            map.put("stageIdExp", temp[1]);
            map.put("tripExpensesDate", tripExpensesDate);
            map.put("tripFuelLitre", tripFuelLitre);
            double temp1 = Double.parseDouble(tripExpensesAmount);
            //////System.out.println("temp1 ======>>>>>> " + temp1);
            double roundoffAmt = (Math.round(temp1));
            //////System.out.println("roundoffAmt =======>>>>>> " + roundoffAmt);
            map.put("tripExpensesAmount", roundoffAmt);

            map.put("tripExpensesPaidBy", tripExpensesPaidBy);
            map.put("tripExpensesRemarks", tripExpensesRemarks);
            map.put("createdBy", userId);
            //////System.out.println("map in insertTripExpenses " + map);
//            int upExp = (Integer) getSqlMapClientTemplate().update("operation.deleteExpensesDetails", map);
            status = (Integer) getSqlMapClientTemplate().update("operation.insertTripExpenses", map);
//            //////System.out.println("insertTripExpenses Update Flag -->" + upExp);
            //////System.out.println("insertTripExpenses -->" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return status;
    }

    public int insertRreturnTrip(int tripId, String fromLocation, String returnLoadedDate, String toLocation, String returnDeliveredDate,
            String returnProductName, String returnKM, String returnFuel, String returnTon,
            String returnExpenses, String returnAmount) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripId);
            map.put("tripSheetId", tripId);
            map.put("fromLocation", fromLocation);
            map.put("returnLoadedDate", returnLoadedDate);
            map.put("toLocation", toLocation);
            map.put("returnDeliveredDate", returnDeliveredDate);
            map.put("returnProductName", returnProductName);
            map.put("returnKM", returnKM);
            map.put("returnFuel", returnFuel);
            map.put("returnTon", returnTon);
            map.put("returnExpenses", returnExpenses);
            map.put("returnAmount", returnAmount);
            //////System.out.println("map in insert Return Trip" + map);
            //status = (Integer) getSqlMapClientTemplate().update("operation.insertReturnTrip", map);
            //////System.out.println("insert Return Trip -->" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertReturnTrip Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertReturnTrip ", sqlException);
        }
        return status;
    }

    public int insertTripFuel(int userId, String returnTripId, int tripId, String bunkName, String bunkPlace, String fuelDate, String fuelAmount,
            String fuelLtrs, String fuelRemarks, String driverId) {
        Map map = new HashMap();
        int lastInsertedId = 0;
        try {
            String[] tempBunk = bunkName.split("~");
            //////System.out.println("tempBunk[0] = " + tempBunk[0]);
            //////System.out.println("tempBunk[1] = " + tempBunk[1]);
            //////System.out.println("tempBunk[2] = " + tempBunk[2]);
            map.put("tripId", tripId);
            map.put("returnTripId", returnTripId);
            map.put("tripSheetId", tripId);
            map.put("bunkName", tempBunk[0]);
            map.put("bunkPlace", bunkPlace);
            map.put("fuelDate", fuelDate);
            map.put("fuelAmount", fuelAmount);
            map.put("fuelLtrs", fuelLtrs);
            map.put("fuelRemarks", fuelRemarks);
            map.put("createdBy", userId);
            //////System.out.println("map in tripFuel " + map);

            lastInsertedId = (Integer) getSqlMapClientTemplate().update("operation.insertTripFuel", map);
//            //////System.out.println("insertTripFuel Update Flag-->" + upFuel);
            //////System.out.println("insertTripFuel -->" + lastInsertedId);

            //  --------------------------------- acc 1st row start --------------------------
            map.put("userId", userId);
            map.put("DetailCode", "1");
            map.put("voucherType", "%PAYMENT%");
            String code2 = (String) getSqlMapClientTemplate().queryForObject("operation.getTripVoucherCode", map);
            String[] temp = code2.split("-");
            int codeval2 = Integer.parseInt(temp[1]);
            int codev2 = codeval2 + 1;
            String voucherCode = "PAYMENT-" + codev2;
            //////System.out.println("voucherCode = " + voucherCode);
            map.put("voucherCode", voucherCode);
            map.put("mainEntryType", "VOUCHER");
            map.put("entryType", "PAYMENT");
            map.put("ledgerId", "37");
            map.put("particularsId", "LEDGER-29");
            map.put("amount", fuelAmount);
            map.put("Accounts_Type", "DEBIT");
            map.put("Remark", "Diesel Expense");
            map.put("Reference", returnTripId);
            map.put("SearchCode", tripId);
            map.put("driverId", driverId);

            //////System.out.println("map1 =---------------------> " + map);
            int status1 = (Integer) getSqlMapClientTemplate().update("operation.insertTripAccountEntry", map);
            //////System.out.println("status1 = " + status1);
            //--------------------------------- acc 2nd row start --------------------------
            if (status1 > 0) {
                //identify debit account
                //if own driver, debit account is driver account
                //if contract driver, the debit account is contract vendor account
                String ledgerId = tempBunk[1];
                String particularsId = tempBunk[2];

                map.put("DetailCode", "2");
                map.put("ledgerId", ledgerId);
                map.put("particularsId", particularsId);
                map.put("Accounts_Type", "CREDIT");
                //////System.out.println("map2 =---------------------> " + map);
                int status2 = (Integer) getSqlMapClientTemplate().update("operation.insertTripAccountEntry", map);
                //////System.out.println("status2 = " + status2);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return lastInsertedId;
    }

    public int resetTripFuel(String returnTripId, int tripId) {
        Map map = new HashMap();
        int lastInsertedId = 0;
        try {
            map.put("tripId", tripId);
            map.put("returnTripId", returnTripId);
            map.put("tripSheetId", tripId);
            //////System.out.println("map in tripFuel " + map);
            if ("0".equals(returnTripId)) {  //for edit tripsheet case alone
                int upFuel = (Integer) getSqlMapClientTemplate().update("operation.deleteFuelDetails", map);
                upFuel = (Integer) getSqlMapClientTemplate().update("operation.deleteFuelAccountEntryDetails", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return lastInsertedId;
    }
    //status = operationDAO.updateFixedTripExpenses(userId,tripSheetIdParam, expExpId,expExpAmt);

    public int updateFixedTripExpenses(int userId, String tripSheetIdParam, String expExpId, String expExpAmt) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripSheetIdParam);
            map.put("userId", userId);
            map.put("expExpId", expExpId);
            map.put("expExpAmt", expExpAmt);
            //////System.out.println("map in updateFixedTripExpenses " + map);

            status = (Integer) getSqlMapClientTemplate().update("operation.updateFixedTripExpenses", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateFixedTripExpenses Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateFixedTripExpenses ", sqlException);
        }
        return status;
    }

    public String getTripIDPrint() {
        Map map = new HashMap();
        String TripID = "";
        try {
            if (getSqlMapClientTemplate().queryForObject("operation.getTripIDPrint", map) != null) {
                TripID = (String) getSqlMapClientTemplate().queryForObject("operation.getTripIDPrint", map);
            }
        } catch (NullPointerException ne) {
            FPLogUtils.fpDebugLog("getTripIDPrint Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripIDPrint", ne);
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog("getTripIDPrint Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripIDPrint", sqlException);
        }
        return TripID;
    }

    public ArrayList getTripDetails(String searchTripDate) {
        Map map = new HashMap();
        map.put("searchTripDate", searchTripDate);

        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getTripDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJobCardTechnicians Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getJobCardTechnicians List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getTripDetailsEdit(String tripSheetId) {
        Map map = new HashMap();
        map.put("tripSheetId", tripSheetId);

        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getTripDetailsEdit", map);
            //////System.out.println("tripDetails View size 3 ====" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJobCardTechnicians Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getJobCardTechnicians List", sqlException);
        }

        return tripDetails;
    }

//    public ArrayList getTwoLpsStutas(String tripSheetId) {
//        Map map = new HashMap();
//        map.put("tripSheetId", tripSheetId);
//
//        ArrayList twoLpsStutas = new ArrayList();
//        try {
//            twoLpsStutas = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getTwoLpsStutas", map);
//            //////System.out.println("getTwoLpsStutas View size 3 ====" + twoLpsStutas.size());
//        } catch (Exception sqlException) {
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("getTwoLpsStutas Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTwoLpsStutas List", sqlException);
//        }
//
//        return twoLpsStutas;
//    }

    public ArrayList gettripDetailsView(String tripSheetId) {
        Map map = new HashMap();
        map.put("tripSheetId", tripSheetId);
        //////System.out.println("map33333333333333 = " + map);
        ArrayList tripDetailsView = new ArrayList();
        try {
            tripDetailsView = (ArrayList) getSqlMapClientTemplate().queryForList("operation.gettripDetailsView", map);
            //////System.out.println("tripDetails View size----  3----=" + tripDetailsView.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJobCardTechnicians Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getJobCardTechnicians List", sqlException);
        }

        return tripDetailsView;
    }

    public ArrayList getgpsList(String tripSheetId) {
        //////System.out.println("31");
        Map map = new HashMap();
        //////System.out.println("32");
        map.put("tripSheetId", tripSheetId);
        //////System.out.println("map===>" + map);

        ArrayList gpsList = new ArrayList();
        //////System.out.println("35");
        try {
            //////System.out.println("36");
            gpsList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getgpsList", map);
            //////System.out.println("getgpsList View size [[[[[[[=>" + gpsList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getgpsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getgpsList List", sqlException);
        }

        return gpsList;
    }

    public ArrayList getAllowanceDetails(String tripSheetId, String returnTripId) {
        Map map = new HashMap();
        map.put("tripSheetId", tripSheetId);
        map.put("returnTripId", returnTripId);
        //////System.out.println("=====>>>>>> map = " + map);
        ArrayList allowanceDetails = new ArrayList();
        try {
            if ("2".equals(returnTripId)) {
                allowanceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getAllAllowanceDetails", map);
            } else {
                allowanceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getAllowanceDetails", map);
            }

            //////System.out.println("tripDetails View size=" + allowanceDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJobCardTechnicians Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getJobCardTechnicians List", sqlException);
        }

        return allowanceDetails;
    }

    public ArrayList getFuelDetails(String tripSheetId, String returnTripId) {
        Map map = new HashMap();
        map.put("tripSheetId", tripSheetId);
        map.put("returnTripId", returnTripId);

        ArrayList fuelDetails = new ArrayList();
        try {
            fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getFuelDetails", map);
            //////System.out.println("fuelDetails View size=" + fuelDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJobCardTechnicians Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getJobCardTechnicians List", sqlException);
        }

        return fuelDetails;
    }

    public ArrayList getEtmExpenses(String tripId, String returnTripId) {
        Map map = new HashMap();
        map.put("tripId", tripId);
        map.put("returnTripId", returnTripId);
        //////System.out.println("etmExpDetails  ->" + map);
        ArrayList etmExpDetails = new ArrayList();
        try {
            if ("2".equals(returnTripId)) {
                etmExpDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getAllEtmExpenses", map);
            } else {
                etmExpDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getEtmExpenses", map);
            }

            //////System.out.println("getEtmExpenses View size=" + etmExpDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEtmExpenses Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEtmExpenses List", sqlException);
        }

        return etmExpDetails;
    }

    public ArrayList getPaymentVoucherDetails(String tripSheetId) {
        Map map = new HashMap();
        map.put("tripSheetId", tripSheetId);

        ArrayList getPaymentVoucherDetails = new ArrayList();
        try {
            getPaymentVoucherDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getPaymentVoucherDetails", map);
            //////System.out.println("tripDetails View size 3 ====" + getPaymentVoucherDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPaymentVoucherDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPaymentVoucherDetails List", sqlException);
        }

        return getPaymentVoucherDetails;
    }

    //Rathimeena Code Start
    public ArrayList searchProDriverSettlement(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList proDriverSettlement = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);

        //////System.out.println("searchProDriverSettlement....: " + map);
        //////System.out.println("fromDate" + fromDate);
        //////System.out.println("toDate" + toDate);
        try {
            if (getSqlMapClientTemplate().queryForList("operation.proDriverSettlement", map) != null) {
                proDriverSettlement = (ArrayList) getSqlMapClientTemplate().queryForList("operation.proDriverSettlement", map);
            }
            //////System.out.println("searchProDriverSettlement size=" + proDriverSettlement.size());
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("searchProDriverSettlement Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "searchProDriverSettlement", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("searchProDriverSettlement Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "searchProDriverSettlement", sqlException);
        }
        return proDriverSettlement;
    }

    public ArrayList cleanerTripDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList cleanerTrip = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        try {
            if (getSqlMapClientTemplate().queryForList("operation.cleanerTripDetails", map) != null) {
                cleanerTrip = (ArrayList) getSqlMapClientTemplate().queryForList("operation.cleanerTripDetails", map);
            }
            //////System.out.println("searchProDriverSettlement size=" + cleanerTrip.size());
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("cleanerTripDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "cleanerTripDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("cleanerTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "cleanerTripDetails", sqlException);
        }
        return cleanerTrip;
    }

    public ArrayList getFixedExpDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList fixedExpDetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);

        try {
            if (getSqlMapClientTemplate().queryForList("operation.getFixedExpDetails", map) != null) {
                fixedExpDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getFixedExpDetails", map);
                //////System.out.println("fixedExpDetails.size().. DAO : " + fixedExpDetails.size());
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getAdvDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getAdvDetails", sqlException);
        }
        return fixedExpDetails;
    }

    public ArrayList getDriverExpDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList driverExpDetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        try {
            if (getSqlMapClientTemplate().queryForList("operation.getDriverExpDetails", map) != null) {
                driverExpDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getDriverExpDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getAdvDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getAdvDetails", sqlException);
        }
        return driverExpDetails;
    }

    public ArrayList getAdvDetails(String fromDate, String toDate, String regNo, int driId, String tripIds) {
        Map map = new HashMap();
        ArrayList AdvDetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        try {
            if (getSqlMapClientTemplate().queryForList("operation.getAdvDetails", map) != null) {
                AdvDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getAdvDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getAdvDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getAdvDetails", sqlException);
        }
        return AdvDetails;
    }

    public ArrayList getFuelDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList fuelDetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        try {
            /*if (getSqlMapClientTemplate().queryForList("operation.getAdvDetails", map) != null) {
            fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getFuelDetails", map);
            }*/
            if (getSqlMapClientTemplate().queryForList("operation.getFuelsDetail", map) != null) {
                fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getFuelsDetail", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFuelDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFuelDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFuelDetails", sqlException);
        }
        return fuelDetails;
    }

    public ArrayList getHaltDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList fuelDetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        try {
            /*if (getSqlMapClientTemplate().queryForList("operation.getAdvDetails", map) != null) {
            fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getFuelDetails", map);
            }*/
            if (getSqlMapClientTemplate().queryForList("operation.getHaltDetails", map) != null) {
                fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getHaltDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return fuelDetails;
    }

    public ArrayList getRemarkDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList fuelDetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        try {
            /*if (getSqlMapClientTemplate().queryForList("operation.getAdvDetails", map) != null) {
            fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getFuelDetails", map);
            }*/
            if (getSqlMapClientTemplate().queryForList("operation.getRemarkDetails", map) != null) {
                fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getRemarkDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRemarkDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRemarkDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRemarkDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRemarkDetails", sqlException);
        }
        return fuelDetails;
    }

    public int insertEposTripExpenses(String tripId, String expenseDesc, String expenseAmt, String expenseDate, String expenseRemarks, int userId) {
        Map map = new HashMap();
        int lastInsertedId = 0;
        try {
            //map.put("tripId", tripId);
            map.put("tripId", tripId);
            map.put("expenseDesc", expenseDesc);
            map.put("expenseAmt", expenseAmt);
            map.put("expenseDate", expenseDate);
            map.put("expenseRemarks", expenseRemarks);
            map.put("userId", userId);
            map.put("flag", "0");
            //////System.out.println("map in insertEposTripExpenses " + map);
            lastInsertedId = (Integer) getSqlMapClientTemplate().update("operation.insertEposTripExpenses", map);
            //////System.out.println("lastInsertedId -->" + lastInsertedId);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return lastInsertedId;
    }

    public int getTripCountDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int tripCount = 0;
        try {
            if (getSqlMapClientTemplate().queryForObject("operation.getTripCountDetails", map) != null) {
                tripCount = (Integer) getSqlMapClientTemplate().queryForObject("operation.getTripCountDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return tripCount;
    }

    public int getTotalTonnageDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList settleDetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int totalTonnage = 0;
        try {
            if (getSqlMapClientTemplate().queryForObject("operation.getTotalTonnageDetails", map) != null) {
                totalTonnage = (Integer) getSqlMapClientTemplate().queryForObject("operation.getTotalTonnageDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return totalTonnage;
    }

    public int getOutKmDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int outKm = 0;
        try {
            if (getSqlMapClientTemplate().queryForObject("operation.getOutKmDetails", map) != null) {
                outKm = (Integer) getSqlMapClientTemplate().queryForObject("operation.getOutKmDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return outKm;
    }

    public int getInKmDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int inKm = 0;
        try {
            if (getSqlMapClientTemplate().queryForObject("operation.getInKmDetails", map) != null) {
                inKm = (Integer) getSqlMapClientTemplate().queryForObject("operation.getInKmDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return inKm;
    }

    public ArrayList getTotFuelDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList totalFuelDetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int totFuel = 0;
        try {
            if (getSqlMapClientTemplate().queryForList("operation.getTotFuelDetails", map) != null) {
                totalFuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getTotFuelDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return totalFuelDetails;
    }

    public int getDriverExpenseDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int driExpense = 0;
        try {
            if (getSqlMapClientTemplate().queryForObject("operation.getDriverExpenseDetails", map) != null) {
                driExpense = (Integer) getSqlMapClientTemplate().queryForObject("operation.getDriverExpenseDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return driExpense;
    }

    public int getDriverSalaryDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int driExpense = 0;
        try {
            if (getSqlMapClientTemplate().queryForObject("operation.getDriverSalaryDetails", map) != null) {
                driExpense = (Integer) getSqlMapClientTemplate().queryForObject("operation.getDriverSalaryDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return driExpense;
    }

    public int getGeneralExpenseDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int genExpense = 0;
        try {
            if (getSqlMapClientTemplate().queryForObject("operation.getGeneralExpenseDetails", map) != null) {
                genExpense = (Integer) getSqlMapClientTemplate().queryForObject("operation.getGeneralExpenseDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return genExpense;
    }

    public int getDriverAdvanceDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int driAdvance = 0;
        try {
            if (getSqlMapClientTemplate().queryForObject("operation.getDriverAdvanceDetails", map) != null) {
                driAdvance = (Integer) getSqlMapClientTemplate().queryForObject("operation.getDriverAdvanceDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return driAdvance;
    }

    public int getDriverBataDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int driAdvance = 0;
        try {
            if (getSqlMapClientTemplate().queryForObject("operation.getDriverBataDetails", map) != null) {
                driAdvance = (Integer) getSqlMapClientTemplate().queryForObject("operation.getDriverBataDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return driAdvance;
    }

    public int insertTripDriverSettlement(Map map) {
        int status = 0;
        try {
            //////System.out.println("map in insertTripDriverSettlement " + map);
            status = (Integer) getSqlMapClientTemplate().update("operation.insertTripDriverSettlement", map);
            //////System.out.println("insertTripDriverSettlement -->" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return status;
    }

    public int updateDriverSettlement(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int status = 0;
        try {
            //////System.out.println("map in updateDriverSettlement " + map);
            status = (Integer) getSqlMapClientTemplate().update("operation.updateDriverSettlement", map);
            //////System.out.println("updateDriverSettlement -->" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return status;
    }

    public ArrayList getOperationLocation() {
        Map map = new HashMap();
        ArrayList opLocation = new ArrayList();
        try {
            if (getSqlMapClientTemplate().queryForList("operation.getOperationLocation", map) != null) {
                opLocation = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getOperationLocation", map);
                //////System.out.println("opLocation 777777777777---------=> " + opLocation.size());
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOperationLocation", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOperationLocation", sqlException);
        }
        return opLocation;
    }

    public ArrayList getExpensesList() {
        Map map = new HashMap();
        ArrayList expensesList = new ArrayList();
        try {
            if (getSqlMapClientTemplate().queryForList("operation.getExpensesList", map) != null) {
                expensesList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getExpensesList", map);
                //////System.out.println("getExpensesList " + expensesList.size());
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getExpensesList Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getExpensesList", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getExpensesList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getExpensesList", sqlException);
        }
        return expensesList;
    }

    public ArrayList getBunkNameLocation() {
        Map map = new HashMap();
        ArrayList bunkLocation = new ArrayList();
        try {
            if (getSqlMapClientTemplate().queryForList("operation.getBunkNameLocation", map) != null) {
                bunkLocation = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getBunkNameLocation", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBunkNameLocation", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBunkNameLocation", sqlException);
        }
        return bunkLocation;
    }

    public ArrayList getAdvancePaidBy() {
        Map map = new HashMap();
        ArrayList paidBy = new ArrayList();
        try {
            if (getSqlMapClientTemplate().queryForList("operation.getAdvancePaidBy", map) != null) {
                paidBy = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getAdvancePaidBy", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBunkNameLocation", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBunkNameLocation", sqlException);
        }
        return paidBy;
    }

    public ArrayList getBunkList() {
        Map map = new HashMap();
        ArrayList bunkList = new ArrayList();
        try {
            if (getSqlMapClientTemplate().queryForList("operation.getBunkList", map) != null) {
                bunkList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getBunkList", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBunkNameLocation", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBunkNameLocation", sqlException);
        }
        return bunkList;
    }

    public ArrayList getReturnTripList(String tripId, String returnTripId) {
        Map map = new HashMap();
        map.put("tripId", tripId);
        map.put("returnTripId", returnTripId);
        ArrayList returnTripList = new ArrayList();
        try {
            if (getSqlMapClientTemplate().queryForList("operation.getReturnTripList", map) != null) {
                returnTripList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getReturnTripList", map);
            }
            //////System.out.println("returnTripList size:" + returnTripList.size());
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getReturnTripList Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getReturnTripList", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getReturnTripList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getReturnTripList", sqlException);
        }
        return returnTripList;
    }

    public ArrayList getDriverName() {
        Map map = new HashMap();
        ArrayList driverName = new ArrayList();
        try {
            if (getSqlMapClientTemplate().queryForList("operation.getDriverName", map) != null) {
                driverName = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getDriverName", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverName", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverName", sqlException);
        }
        return driverName;
    }

    public ArrayList getCardNos() {
        Map map = new HashMap();
        ArrayList cardNos = new ArrayList();
        try {
            if (getSqlMapClientTemplate().queryForList("operation.getCardNos", map) != null) {
                cardNos = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCardNos", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverName", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverName", sqlException);
        }
        return cardNos;
    }

    public ArrayList getVehicleNos() {
        Map map = new HashMap();
        ArrayList vehicleNos = new ArrayList();
        try {
            if (getSqlMapClientTemplate().queryForList("operation.getVehicleNoList", map) != null) {
                vehicleNos = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getVehicleNoList", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverName", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverName", sqlException);
        }
        return vehicleNos;
    }

    public ArrayList getVehicleNosWithDriver() {
        Map map = new HashMap();
        ArrayList vehicleNos = new ArrayList();
        try {
            if (getSqlMapClientTemplate().queryForList("operation.getVehicleNosWithDriver", map) != null) {
                vehicleNos = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getVehicleNosWithDriver", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleNosWithDriver Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleNosWithDriver", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleNosWithDriver Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleNosWithDriver", sqlException);
        }
        return vehicleNos;
    }

    public ArrayList getCustName() {
        Map map = new HashMap();
        ArrayList custName = new ArrayList();
        try {
            if (getSqlMapClientTemplate().queryForList("operation.getCustName", map) != null) {
                custName = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCustName", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustName", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustName", sqlException);
        }
        return custName;
    }

    public ArrayList searchOverAllTripDetails(String tripId, String regno, String driId, String status, String locId, String fromDate, String toDate, String ownership) {
        Map map = new HashMap();
        String[] temp = null;
        String frDate = "", tDt = "";
        /*if (fromDate != "" && toDate != "") {
        temp = fromDate.split("-");
        String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
        frDate = sDate;
        temp = toDate.split("-");
        String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
        tDt = eDate;
        }*/
        if (tripId.equals("") || tripId == "null" || tripId == "") {
            tripId = "0";
        }
        map.put("tripId", tripId);
        if (regno.equals("") || regno == "null" || regno == "") {
            regno = "0";
        }
        map.put("regno", regno);
        map.put("driId", driId);
        map.put("status", status);
        map.put("locId", locId);
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("ownership", ownership);
        //////System.out.println("tripId: " + tripId);
        //////System.out.println("regno: " + regno);
        //////System.out.println("driId: " + driId);
        //////System.out.println("status: " + status);
        //////System.out.println("locId: " + locId);
        //////System.out.println("fromDate: " + frDate);
        //////System.out.println("toDate: " + tDt);
        //////System.out.println("ownership: " + ownership);
        ArrayList tripDetails = new ArrayList();
        try {
            //////System.out.println("searchOverAllTripDetails...DAO map: " + map);
            if (getSqlMapClientTemplate().queryForList("operation.getOverAllTripDetails", map) != null) {
                tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getOverAllTripDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustName", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustName", sqlException);
        }
        return tripDetails;
    }

    public ArrayList searchDriverDetails(String driName) {
        Map map = new HashMap();
        String dName[] = null;
        String driId = "";
        if (!"".equals(driName)) {
            dName = driName.split("-");
            driId = dName[1];
        }
        map.put("driId", driId);
        ArrayList cardDetails = new ArrayList();
        try {
            //////System.out.println("searchDriverDetails...DAO map: " + map);
            if (getSqlMapClientTemplate().queryForList("operation.searchDriverDetails", map) != null) {
                cardDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.searchDriverDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustName", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustName", sqlException);
        }
        return cardDetails;
    }

    public ArrayList alterCardMapping(String driId) {
        Map map = new HashMap();
        map.put("driId", driId);
        ArrayList cardDetails = new ArrayList();
        try {
            //////System.out.println("alterCardMapping...DAO map: " + map);
            if (getSqlMapClientTemplate().queryForList("operation.searchDriverDetails", map) != null) {
                cardDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.searchDriverDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustName", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustName", sqlException);
        }
        return cardDetails;
    }

    public int saveCardVehicleDriver(String cardId, String vehicleId, String driverId, String remark) {
        int status = 0;
        int insertStatus = 0;
        Map map = new HashMap();
        map.put("cardId", cardId);
        map.put("vehicleId", vehicleId);
        map.put("driverId", driverId);
        map.put("remark", remark);
        map.put("actInd", "Y");
        map.put("modifiedBy", "1081");
        try {
            String vehicleNo = "";
            if (getSqlMapClientTemplate().queryForObject("operation.getVehicleNo", map) != null) {
                vehicleNo = (String) getSqlMapClientTemplate().queryForObject("operation.getVehicleNo", map);
            }
            //////System.out.println("vehicleNo: " + vehicleNo);
            map.put("vehicleNo", vehicleNo);
            status = (Integer) getSqlMapClientTemplate().update("operation.updateCardMapping", map);
            //////System.out.println("status: " + status);

            if (status == 1) {
                insertStatus = (Integer) getSqlMapClientTemplate().update("operation.insertCardMapping", map);
            } else {
                insertStatus = (Integer) getSqlMapClientTemplate().update("operation.insertCardMapping", map);
            }
            //////System.out.println("saveCardVehicleDriver DAO" + insertStatus);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveCardVehicleDriver Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return insertStatus;
    }
    //Rathimeena Code End

//CLPL code start
    //-----------trip sheet ajax starts here---------------
    public String getPinkSlipDetail(String vehicleNo) {
        //////System.out.println("pink ajax DAO.............");
        //////System.out.println("vehicleNo = " + vehicleNo);
//        List<String> list = new ArrayList<String>();
//        List<String> list1 = new ArrayList<String>();
//        ArrayList arraylilstAdd = new ArrayList();
        Map map = new HashMap();
        String pinkSlipDetails = "";
        String pinkSlipValue = "";
        String[] temp;
        String val = "";
        try {
            map.put("vehicleNo", vehicleNo);
            //////System.out.println("map = " + map);
            pinkSlipValue = (String) getSqlMapClientTemplate().queryForObject("operation.pinkSlipDeails", map);
            //////System.out.println("pinkSlipValue = " + pinkSlipValue);

            if (pinkSlipValue == null) {
                pinkSlipDetails = (String) getSqlMapClientTemplate().queryForObject("operation.pinkSlipValue", map);
                //////System.out.println("pinkSlipDetails =-----22222----> " + pinkSlipDetails);
            } else {
                pinkSlipDetails = "";
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPinkSlipDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPinkSlipDetail", sqlException);
        }

        return pinkSlipDetails;
    }

    public String pinkSlipVechicelStatus(String vehicleNo) {
        //////System.out.println("pink ajax DAO ashok.............");
        Map map = new HashMap();
        String status = "";
        try {
            map.put("vehicleNo", vehicleNo);
            //////System.out.println("map = " + map);
//            code1 = (String) getSqlMapClientTemplate().queryForObject("operation.getLedgerCode", map);
            status = (String) getSqlMapClientTemplate().queryForObject("operation.pinkSlipVechicelStatus", map);
            //////System.out.println("status ashok= " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPinkSlipDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPinkSlipDetail", sqlException);
        }

        return status;
    }

    public String getReturnTripDetails(String tripId) {
        //////System.out.println("getReturnTripDetails DAO.............");
        Map map = new HashMap();
        String val = "";
        try {
            map.put("tripId", tripId);
            //////System.out.println("map = " + map);
            val = (String) getSqlMapClientTemplate().queryForObject("operation.getReturnTripDetails", map);
            //////System.out.println("val = " + val);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getReturnTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getReturnTripDetails", sqlException);
        }

        return val;
    }

    /**
     * This method used to Get lps list.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getLpsList(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList lpsList = new ArrayList();
        map.put("fromDate", operationTO.getFromDate());
        map.put("toDate", operationTO.getToDate());
        map.put("lpsNo", operationTO.getLpsNo());
        map.put("status", operationTO.getStatus());
        map.put("destination", operationTO.getDestination());

        try {
            lpsList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getLpsList", map);
            //////System.out.println("lpsList = " + lpsList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("lpsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "lps List", sqlException);
        }
        return lpsList;
    }

    public String getLpsSummary(OperationTO operationTO) {
        Map map = new HashMap();
        String lpsSummary = "";
        map.put("fromDate", operationTO.getFromDate());
        map.put("toDate", operationTO.getToDate());
        map.put("lpsNo", operationTO.getLpsNo());
        map.put("status", operationTO.getStatus());
        map.put("destination", operationTO.getDestination());

        try {
            lpsSummary = (String) getSqlMapClientTemplate().queryForObject("operation.getLPSSummary", map);
            //////System.out.println("lpsSummary = " + lpsSummary);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("lpsSummary Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "lpsSummary", sqlException);
        }
        return lpsSummary;
    }

    public int saveLPS(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        String code = "";
        String delimiter = "-";
        String[] temp;
        int status = 0;
        //CryptoLibrary cl = new CryptoLibrary()
        map.put("userId", userId);
        map.put("lpsNumber", operationTO.getLpsNumber());
        map.put("partyName", operationTO.getPartyName());
        map.put("lpsDate", operationTO.getLpsDate());
        map.put("orderNumber", operationTO.getOrderNumber());
        map.put("contractor", operationTO.getContractor());
        map.put("packerNumber", operationTO.getPackerNumber());
        map.put("quantity", operationTO.getQuantity());
        map.put("bags", operationTO.getBags());
        map.put("destination", operationTO.getDestination());
        String destID[] = operationTO.getDestination().split("-");
        //////System.out.println("destID = " + destID[0]);
        //////System.out.println("destID = " + destID[1]);
        map.put("destID", destID[1]);
        map.put("productName", operationTO.getProductName());
        map.put("productId", operationTO.getProductId());
        map.put("packing", operationTO.getPacking());
        map.put("clplPriority", operationTO.getClplPriority());
        map.put("lorryNo", operationTO.getLorryNo());
        map.put("gatePassNo", operationTO.getGatePassNo());
        map.put("watchandward", operationTO.getWatchandward());
        map.put("billStatus", operationTO.getBillStatus());
        //////System.out.println("map =-////------------> " + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("operation.insertLPS", map);
            //////System.out.println("status = " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertLPS", sqlException);
        }
        return status;
    }

    public String getCustomerSuggestion(String customer) {
        Map map = new HashMap();
        map.put("customer", customer);
        String suggestions = "";
        OperationTO repTO = new OperationTO();
        try {
            ArrayList customers = new ArrayList();
            //////System.out.println("map = " + map);
            customers = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCustomerSuggestion", map);
            Iterator itr = customers.iterator();
            while (itr.hasNext()) {
                repTO = new OperationTO();
                repTO = (OperationTO) itr.next();
                suggestions = repTO.getCustomerName() + "^" + repTO.getCustomerId() + "~" + suggestions;
                //////System.out.println("suggestions = " + suggestions);
            }
            if ("".equals(suggestions)) {
                suggestions = "no match found";
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerSuggestion Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCustomerSuggestion", sqlException);
        }
        return suggestions;
    }

    public ArrayList getCustomerList() {
        Map map = new HashMap();
        map.put("customer", "0");
        ArrayList customers = new ArrayList();
        try {

            //////System.out.println("map = " + map);
            customers = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCustomerSuggestion", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCustomerList", sqlException);
        }
        return customers;
    }

    public String getFromLocation(String formLocation) {
        Map map = new HashMap();
        map.put("formLocation", formLocation);
        String suggestions = "";
        OperationTO repTO = new OperationTO();
        try {
            ArrayList formLoc = new ArrayList();
            //////System.out.println("map = " + map);
            formLoc = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getFromLocation", map);
            Iterator itr = formLoc.iterator();
            while (itr.hasNext()) {
                repTO = new OperationTO();
                repTO = (OperationTO) itr.next();
                suggestions = repTO.getFromLocation() + "^" + repTO.getFromLocationid() + "~" + suggestions;
                //////System.out.println("suggestions = " + suggestions);
            }
            if ("".equals(suggestions)) {
                suggestions = "no match found";
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerSuggestion Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCustomerSuggestion", sqlException);
        }
        return suggestions;
    }

    public String getToLocation(String toLocation, String customer, String fromLocation) {
        Map map = new HashMap();
        map.put("toLocation", toLocation);
        map.put("customer", customer);
        map.put("fromLocation", fromLocation);
        String suggestions = "";
        OperationTO repTO = new OperationTO();
        try {
            ArrayList toLoc = new ArrayList();
            //////System.out.println("map = " + map);
            if ("0".equals(customer)) {
                toLoc = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getToLocation", map);
            } else {
                toLoc = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getToLocationOfCustomer", map);
            }
            Iterator itr = toLoc.iterator();
            while (itr.hasNext()) {
                repTO = new OperationTO();
                repTO = (OperationTO) itr.next();
                suggestions = repTO.getToLocation() + "^" + repTO.getToLocationid() + "^" + repTO.getTotalKm() + "^" + repTO.getTonnageRate() + "~" + suggestions;
                //suggestions = suggestions + "~" + repTO.getToLocation() + "^" + repTO.getToLocationid()+ "^" + repTO.getTotalKm()+ "^" + repTO.getTonnageRate();
                //////System.out.println("suggestions = " + suggestions);
            }
            if ("".equals(suggestions)) {
                suggestions = "no match found";
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerSuggestion Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCustomerSuggestion", sqlException);
        }
        return suggestions;
    }

    public String getProductSuggestion(String product) {
        Map map = new HashMap();
        map.put("product", product);
        String suggestions = "";
        OperationTO repTO = new OperationTO();
        try {
            ArrayList products = new ArrayList();
            //////System.out.println("map = " + map);
            products = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getProductSuggestion", map);
            Iterator itr = products.iterator();
            while (itr.hasNext()) {
                repTO = new OperationTO();
                repTO = (OperationTO) itr.next();
                suggestions = repTO.getProductName() + "^" + repTO.getProductId() + "~" + suggestions;
                //////System.out.println("suggestions = " + suggestions);
            }
            if ("".equals(suggestions)) {
                suggestions = "no match found";
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerSuggestion Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCustomerSuggestion", sqlException);
        }
        return suggestions;
    }

    public ArrayList processLPSalterList(String lpsId) {
        Map map = new HashMap();
        ArrayList lpsList = new ArrayList();
        try {
            map.put("lpsId", lpsId);
            //////System.out.println("map = " + map);
            lpsList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getalterLPS", map);
            //////System.out.println("lpsList = " + lpsList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "lpsList", sqlException);
        }
        return lpsList;
    }

    public ArrayList getLPSProductList() {
        Map map = new HashMap();
        ArrayList productList = new ArrayList();
        try {
            productList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getLPSProductList", map);
            //////System.out.println("productList = " + productList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "lpsList", sqlException);
        }
        return productList;
    }

    public ArrayList getProductList(String lpsId) {
        Map map = new HashMap();
        ArrayList lpsList = new ArrayList();
        try {
            map.put("lpsId", lpsId);
            lpsList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getProductList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "lpsList", sqlException);
        }
        return lpsList;
    }

    public int saveAlterLPS(OperationTO operationTO, String lpsID, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", userId);
        map.put("lpsNumber", operationTO.getLpsNumber());
        map.put("partyName", operationTO.getPartyName());
        map.put("lpsDate", operationTO.getLpsDate());
        map.put("orderNumber", operationTO.getOrderNumber());
        map.put("contractor", operationTO.getContractor());
        map.put("packerNumber", operationTO.getPackerNumber());
        map.put("quantity", operationTO.getQuantity());
        map.put("bags", operationTO.getBags());
        map.put("destination", operationTO.getDestination());
        map.put("productName", operationTO.getProductName());
        map.put("packing", operationTO.getPacking());
        map.put("clplPriority", operationTO.getClplPriority());
        map.put("lorryNo", operationTO.getLorryNo());
        map.put("gatePassNo", operationTO.getGatePassNo());
        map.put("watchandward", operationTO.getWatchandward());
        map.put("billStatus", operationTO.getBillStatus());
        map.put("lpsID", lpsID);
        //////System.out.println("map =---> " + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("operation.updateLPS", map);
            //////System.out.println("status 2= " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "Update LPS", sqlException);
        }

        return status;
    }

    /**
     * This method used to Get PinkSlip list.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String pinkSlipVehicleNo(String regno) {
        Map map = new HashMap();
        map.put("regno", "%" + regno + "%");
        String suggestions = "";
        OperationTO repTO = new OperationTO();
        try {
            ArrayList vehicleNo = new ArrayList();
            //////System.out.println("map = " + map);
            vehicleNo = (ArrayList) getSqlMapClientTemplate().queryForList("operation.pinkSlipVehicleNo", map);
            Iterator itr = vehicleNo.iterator();
            while (itr.hasNext()) {
                repTO = new OperationTO();
                repTO = (OperationTO) itr.next();
                //////System.out.println("repTO.getRegisterNo() = " + repTO.getRegisterNo());
                suggestions = repTO.getRegisterNo() + "~" + suggestions;
                //////System.out.println("suggestions = " + suggestions);
            }
            if ("".equals(suggestions)) {
                suggestions = "no match found";
            } else if (!"".equals(suggestions)) {
                suggestions = regno + "~" + suggestions;
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleNo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehicleNo", sqlException);
        }
        return suggestions;
    }

    public String pinkSlipDriver(String driName) {
        Map map = new HashMap();
        map.put("driName", driName);
        String suggestions = "";
        OperationTO repTO = new OperationTO();
        try {
            ArrayList driverNames = new ArrayList();
            //////System.out.println("map = " + map);
            driverNames = (ArrayList) getSqlMapClientTemplate().queryForList("operation.pinkSlipDriver", map);
            Iterator itr = driverNames.iterator();
            while (itr.hasNext()) {
                repTO = new OperationTO();
                repTO = (OperationTO) itr.next();
                suggestions = repTO.getDriName() + "~" + suggestions;
            }
            if ("".equals(suggestions)) {
                suggestions = "no match found";
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverNames Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getDriverNames", sqlException);
        }
        return suggestions;
    }

    public String getVehicleDetailsPinkSlip(String regNo) {
        Map map = new HashMap();
        String vehicleDetailsPinkSlip = "";
        try {
            map.put("regNo", regNo);
            //////System.out.println("map = " + map);
            vehicleDetailsPinkSlip = (String) getSqlMapClientTemplate().queryForObject("operation.getVehicleDetailsPinkSlip", map);

            //////System.out.println("vehicleDetailsPinkSlip in DAO = " + vehicleDetailsPinkSlip);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkVehicleTyre Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleDetailsPinkSlip", sqlException);
        }

        return vehicleDetailsPinkSlip;
    }

    public ArrayList getLPSOrderList() {
        Map map = new HashMap();
        ArrayList LPSOrderList = new ArrayList();

        try {
            LPSOrderList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.LPSOrderList", map);
            //////System.out.println("LPSOrderList = " + LPSOrderList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("LPSOrderList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "LPSOrder List", sqlException);
        }
        return LPSOrderList;
    }

    public ArrayList getlorryDetailList(OperationTO operationTO) {
        Map map = new HashMap();
        map.put("regno", operationTO.getRegno());
        //////System.out.println("map <========> " + map);
        ArrayList lorryDetailList = new ArrayList();
        try {
            lorryDetailList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.lorryDetailList", map);
            //////System.out.println("lorryDetailList = " + lorryDetailList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("lorryDetailList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "lorryDetailList", sqlException);
        }
        return lorryDetailList;
    }

    public ArrayList getPinkSlipList() {
        Map map = new HashMap();
        ArrayList pinkSlipList = new ArrayList();
        try {
            pinkSlipList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getPinkSlipList", map);
            //////System.out.println("pinkSlipList ======== " + pinkSlipList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("pinkSlipList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "pinkSlip List", sqlException);
        }
        return pinkSlipList;
    }

    public ArrayList getpinkSlipSummaryDetail(OperationTO operationTo) {
        Map map = new HashMap();
        map.put("lpsNo", operationTo.getLpsNo());
        map.put("vehicleNo", operationTo.getVehicleNo());
        map.put("pinkSlipNo", operationTo.getPinkSlip());
        map.put("pinkSlipStatus", operationTo.getStatus());
        ArrayList pinkSlipList = new ArrayList();
        try {
            pinkSlipList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getpinkSlipSummaryDetail", map);
            //////System.out.println("pinkSlipList ======== " + pinkSlipList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("pinkSlipList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "pinkSlip List", sqlException);
        }
        return pinkSlipList;
    }

    public String getpinkSlipSummary(OperationTO operationTo) {
        Map map = new HashMap();
        map.put("lpsNo", operationTo.getLpsNo());
        map.put("vehicleNo", operationTo.getVehicleNo());
        map.put("pinkSlipNo", operationTo.getPinkSlip());
        map.put("status", operationTo.getStatus());
        String pinkSlipSummary = "";
        try {
            pinkSlipSummary = (String) getSqlMapClientTemplate().queryForObject("operation.getPinkSlipSummary", map);
            //////System.out.println("pinkSlipSummary ======== " + pinkSlipSummary);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("pinkSlipSummary Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "pinkSlipSummary", sqlException);
        }
        return pinkSlipSummary;
    }

    public ArrayList getViewPinkSlip(String pinkSlipID, String lpsID) {
        Map map = new HashMap();
        ArrayList pinkSlipList = new ArrayList();

        map.put("pinkSlipID", pinkSlipID);
        map.put("lpsID", lpsID);

        try {
            pinkSlipList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getViewPinkSlip", map);
            //////System.out.println("pinkSlipList ======== " + pinkSlipList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getViewPinkSlip Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getViewPinkSlip List", sqlException);
        }
        return pinkSlipList;
    }

    public ArrayList getSuggestedLPSList(OperationTO operationTO) {
        Map map = new HashMap();
        String vehicleType = "";
        String selectedRouteName1 = "";

        map.put("regno", operationTO.getRegno());
        map.put("driName", operationTO.getDriName());
        map.put("routeId", operationTO.getRouteId());
//      map.put("ownership", operationTO.getOwnership());

        //////System.out.println("map SuggestedLPSList  ===>" + map);
        selectedRouteName1 = (String) getSqlMapClientTemplate().queryForObject("operation.getRouteName", map);
        //////System.out.println(" selectedRouteName1================" + selectedRouteName1);
        String selectedRouteName = '%' + selectedRouteName1 + '%';
        map.put("selectedRouteName", selectedRouteName);
        //////System.out.println("map = " + map);

        //         String selectedRouteN = "";
        //        Map map = new HashMap();
        //
        ////        selectedRouteN = (String) getSqlMapClientTemplate().queryForObject("operation.getRouteName", map);
        ////        //////System.out.println(" selectedRouteN================" + selectedRouteN);
        ////        map.put("selectedRouteN", selectedRouteN);
        ////        //////System.out.println("map = " + map);
        ArrayList SuggestedLPSList = new ArrayList();
        try {
            SuggestedLPSList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.suggestedLPSList", map);
            //////System.out.println("SuggestedLPSList in DAO= " + SuggestedLPSList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("SuggestedLPSList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "SuggestedLPSList List", sqlException);
        }
        return SuggestedLPSList;
    }

    public ArrayList getSuggestedPinkSlip(OperationTO operationTO) {
        Map map = new HashMap();
        String vehicleType = "";
        String selectedRouteName1 = "";
        String selectedRouteName = "";
        map.put("regno", operationTO.getRegno());
        map.put("driName", operationTO.getDriName());
        map.put("routeId", operationTO.getRouteId());
        //////System.out.println("map =-----> " + map);
        selectedRouteName1 = (String) getSqlMapClientTemplate().queryForObject("operation.getRouteName", map);
        //////System.out.println("selectedRouteName1 = " + selectedRouteName1);
        selectedRouteName = "%" + selectedRouteName1 + "%";
        //////System.out.println(" selectedRouteName================" + selectedRouteName);
        map.put("selectedRouteName", selectedRouteName);
        //////System.out.println("map = " + map);

        //        vehicleType = (String) getSqlMapClientTemplate().queryForObject("operation.getVehicleType", map);
        //        //////System.out.println(" vehicleType ================" + vehicleType);
        //
        //        if(vehicleType.equalsIgnoreCase("Own")){
        //            //////System.out.println("11111111");
        //        }else{
        //            //////System.out.println("222222");
        //        }
        ArrayList SuggestedPinkSlip = new ArrayList();
        try {
            SuggestedPinkSlip = (ArrayList) getSqlMapClientTemplate().queryForList("operation.suggestedPinkSlip", map);
            //////System.out.println("SuggestedPinkSlip in DAO= " + SuggestedPinkSlip.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("SuggestedPinkSlip Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "suggestedPinkSlip List", sqlException);
        }
        return SuggestedPinkSlip;
    }

    public int savePinkSlip(OperationTO operationTO, int userId, String selectedLpsIds, String selecteddestination, String selectedBags, String selectedTon, String twoLPSStatus, String selectedTonRate, String selectedTonRateMarket, String ownership, String selecteddestinationName, String selectedbillStatus, String selectedPacking, String selectedProductName) {
        Map map = new HashMap();
        int status = 0;
        int status1 = 0;
        String[] temp;
        String Rid1 = "";
        String[] Rid;
        String code = "";
        //CryptoLibrary cl = new CryptoLibrary()
        map.put("userId", userId);
        map.put("regno", operationTO.getRegno());
        map.put("driName", operationTO.getDriName());
        String driverName = operationTO.getDriName();
        String[] temp1 = driverName.split("-");
        String driverId = "";
        if (temp1.length >= 1) {
            driverId = temp1[1];
        }
        map.put("driverId", driverId);
//        //////System.out.println("operationTO.getRouteId() = " + operationTO.getRouteId());
//        Rid = operationTO.getRouteId().split("-");
//        //////System.out.println("Rid[1] = " + Rid[1]);
//        map.put("routeId", Rid[1]);
        map.put("billStatus", operationTO.getBillStatus());
        map.put("selectedLpsIds", selectedLpsIds);
        map.put("selecteddestination", selecteddestination);
        map.put("twoLPSStatus", twoLPSStatus);
        map.put("selectedBillStatus", operationTO.getSelectedBillStatus());
        map.put("selectedTon", selectedTon);
        //////System.out.println("selectedTon = " + selectedTon);
        map.put("selectedBags", selectedBags);
        //////System.out.println("selectedBags = " + selectedBags);
        map.put("selectedTonRate", selectedTonRate);
        //////System.out.println("selectedTonRate = " + selectedTonRate);
        map.put("selectedTonRateMarket", selectedTonRateMarket);
        //////System.out.println("selectedTonRateMarket = " + selectedTonRateMarket);
//        map.put("SelectedRouteId", operationTO.getSelectedRouteId());
        map.put("ownership", ownership);
        //////System.out.println("ownership = " + ownership);
        map.put("selecteddestinationName", selecteddestinationName);
        //////System.out.println("selecteddestinationName= " + selecteddestinationName);
        map.put("selectedbillStatus", selectedbillStatus);
        //////System.out.println(", selectedbillStatus= " + selectedbillStatus);
        map.put("selectedPacking", selectedPacking);
        //////System.out.println("selectedPacking = " + selectedPacking);
        map.put("selectedProductName", selectedProductName);
        //////System.out.println("selectedProductName= " + selectedProductName);
        //////System.out.println("map =----------ashok------------------> " + map);
        try {
            status1 = (Integer) getSqlMapClientTemplate().update("operation.insertPinkSlip", map);

            if (status1 > 0) {
                code = (String) getSqlMapClientTemplate().queryForObject("operation.getpinkSlipCode", map);
                map.put("pinkSlipID", code);
                temp = selectedLpsIds.split(",");
                for (int i = 0; i < temp.length; i++) {
                    map.put("selectedLpsIds", temp[i]);
                    status = (Integer) getSqlMapClientTemplate().update("operation.updateLpsList", map);
                }
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertPinkSlip", sqlException);
        }
        return status;
    }

    public ArrayList getpinkSliplist() {
        Map map = new HashMap();
//        String pinkSlipList = "";
//        pinkSlipList = (String) getSqlMapClientTemplate().queryForObject("operation.getRouteName", map);
//        //////System.out.println(" selectedRouteName================" + pinkSlipList);

        ArrayList pinkSlipList = new ArrayList();
        try {
            pinkSlipList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getDisplayPinkSliplist", map);
            //////System.out.println("pinkSlipList in DAO= " + pinkSlipList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("pinkSlipList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "pinkSlipList List", sqlException);
        }
        return pinkSlipList;
    }

    public ArrayList getpinkSlipSummarylist(OperationTO operationTO) {
        Map map = new HashMap();
        map.put("pnikSlipID", operationTO.getPinkSlipID());
        //////System.out.println("map =1234567==== " + map);
//        String pinkSlipList = "";
//        pinkSlipList = (String) getSqlMapClientTemplate().queryForObject("operation.getRouteName", map);
//        //////System.out.println(" selectedRouteName================" + pinkSlipList);

        ArrayList pinkSlipList = new ArrayList();
        try {
            pinkSlipList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getDisplayPinkSlipSummarylist", map);
            //////System.out.println("pinkSlipList in DAO= " + pinkSlipList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("pinkSlipList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "pinkSlipList List", sqlException);
        }
        return pinkSlipList;
    }

    public int getpinkSlipDelete(OperationTO operationTO) {
        Map map = new HashMap();
        map.put("pnikSlipID", operationTO.getPinkSlipID());
        map.put("lpsID", operationTO.getLpsID());
        //////System.out.println("map = " + map);
        String code = "";
        int status = 0;
        String[] temp;
        ArrayList pinkSlipList = new ArrayList();
        try {
            int pinkSlipDelete = (Integer) getSqlMapClientTemplate().update("operation.deletepinkSlip", map);
            //////System.out.println("pinkSlipDelete = " + pinkSlipDelete);

            if (pinkSlipDelete > 0) {
                code = operationTO.getLpsID();
                temp = code.split(",");
                for (int i = 0; i < temp.length; i++) {
                    map.put("selectedLpsIds", temp[i]);
                    //////System.out.println("map = " + map);
                    status = (Integer) getSqlMapClientTemplate().update("operation.updateLpsSummaryList", map);
                }
            }

            //pinkSlipList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getDisplayPinkSlipSummarylist", map);
            //////System.out.println("pinkSlipList in DAO= " + pinkSlipList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("pinkSlipList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "pinkSlipList List", sqlException);
        }
        return status;
    }

    public List getPinkSlipLPSList() {
        List<String> list = new ArrayList<String>();
        List pinkSlipLPSList = new ArrayList();
        Map map = new HashMap();
        String getLpsId = "";
        getLpsId = (String) getSqlMapClientTemplate().queryForObject("operation.getLpsId", map);
        //////System.out.println(" getLpsId================" + getLpsId);
        String[] temp;
        temp = getLpsId.split(",");
        for (int i = 0; i < temp.length; i++) {
            list.add(temp[i]);
        }
        //////System.out.println("List ========> " + list);
        try {
            pinkSlipLPSList = getSqlMapClientTemplate().queryForList("operation.getPinkSlipLPSList", list);
            //////System.out.println("objs in DAO= " + pinkSlipLPSList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("pinkSlipList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "pinkSlipList List", sqlException);
        }
        return pinkSlipLPSList;
    }

    public List getpinkSlipSummaryLPSList(OperationTO operationTO) {
        List<String> list = new ArrayList<String>();
        List pinkSlipSummaryLPSList = new ArrayList();
        Map map = new HashMap();
        String getLpsId = operationTO.getLpsID();
//        getLpsId = (String) getSqlMapClientTemplate().queryForObject("operation.getLpsId", map);
        //////System.out.println(" getLpsId================" + getLpsId);
        String[] temp;
        temp = getLpsId.split(",");
        for (int i = 0; i < temp.length; i++) {
            list.add(temp[i]);
        }
        //////System.out.println("List ========> " + list);
        try {
            pinkSlipSummaryLPSList = getSqlMapClientTemplate().queryForList("operation.getPinkSlipLPSList", list);
            //////System.out.println("objs in DAO= " + pinkSlipSummaryLPSList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("pinkSlipList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "pinkSlipList List", sqlException);
        }
        return pinkSlipSummaryLPSList;
    }

    public ArrayList getViewPinkSlipLPSList(String lpsID) {
        //////System.out.println("iiiiiiiiiiiiiiiiiii");
        Map map = new HashMap();

        map.put("lpsID", lpsID);
        //////System.out.println("map ========> " + map);
        ArrayList pinkSlipLPSList = new ArrayList();
        try {
            pinkSlipLPSList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getViewPinkSlipLPSList", map);
            //////System.out.println("pinkSlipLPSList in DAO= " + pinkSlipLPSList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("pinkSlipList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "pinkSlipList List", sqlException);
        }
        return pinkSlipLPSList;
    }

    /**
     * This method used to Get Product list.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getProductList() {
        Map map = new HashMap();
        ArrayList productList = new ArrayList();

        try {
            productList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getProductList", map);
            //////System.out.println("productList = " + productList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("productList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "productList", sqlException);
        }
        return productList;
    }

    public int saveProductMaster(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        String code = "";
        String delimiter = "-";
        String[] temp;
        int status = 0;
        //CryptoLibrary cl = new CryptoLibrary()
        map.put("userId", userId);
        map.put("productCode", operationTO.getProductCode());
        map.put("productName", operationTO.getProductName());
        map.put("remarks", operationTO.getRemarks());

        //////System.out.println("map =---> " + map);
        try {
            // status = (Integer) getSqlMapClientTemplate().update("operation.insertLPS", map);
            status = (Integer) getSqlMapClientTemplate().update("operation.insertProductMaster", map);
            //////System.out.println("status = " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertProductMaster", sqlException);
        }
        return status;
    }

    public ArrayList processAlterProductMasterList(String productId) {
        Map map = new HashMap();
        ArrayList productList = new ArrayList();
        try {
            map.put("productId", productId);
            productList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getalterProductMaster", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "productList", sqlException);
        }
        return productList;
    }

    public int saveAlterProductMaster(OperationTO operationTO, String productId, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", userId);
        map.put("productCode", operationTO.getProductCode());
        map.put("productName", operationTO.getProductName());
        map.put("remarks", operationTO.getRemarks());
        map.put("productId", productId);
        //////System.out.println("map =---> " + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("operation.updateProductMaster", map);
            //////System.out.println("status 2= " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "Update ProductMaster", sqlException);
        }
        return status;
    }

    /**
     * This method used to Invoice .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getLastInvoiceDate(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList LastInvoiceDate = new ArrayList();
        try {
            LastInvoiceDate = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getLastInvoiceDate", map);
            //////System.out.println("LastInvoiceDate in DAO= " + LastInvoiceDate.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("tripList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "tripList List", sqlException);
        }
        return LastInvoiceDate;
    }

    public ArrayList getTripList(String fromDate, String toDate, String customer, String ownership, String billType) {
        Map map = new HashMap();
        String[] temp = null;
        String frDate = "", tDt = "";
        if (fromDate != "" && toDate != "") {
            temp = fromDate.split("-");
            String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
            frDate = sDate;
            temp = toDate.split("-");
            String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
            tDt = eDate;
        }
        map.put("fromDate", frDate);
        map.put("toDate", tDt);
        map.put("customer", customer);
        map.put("billType", billType);
        map.put("ownership", ownership);
        //////System.out.println("map = " + map);
        ArrayList tripList = new ArrayList();
        try {
            tripList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getTripList", map);
            //////System.out.println("getTripList in DAO= " + tripList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("tripList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "tripList List", sqlException);
        }
        return tripList;
    }

    public ArrayList gettotalList(String fromDate, String toDate, String customer) {
        Map map = new HashMap();
        String[] temp = null;
        String frDate = "", tDt = "";
        if (fromDate != "" && toDate != "") {
            temp = fromDate.split("-");
            String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
            frDate = sDate;
            temp = toDate.split("-");
            String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
            tDt = eDate;
        }
        map.put("fromDate", frDate);
        map.put("toDate", tDt);
        map.put("customer", customer);
        //////System.out.println("map = " + map);
        ArrayList totalList = new ArrayList();
        try {
            totalList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.gettotalList", map);
            //////System.out.println("getTripList in DAO=--------------------->/////--> " + totalList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("tripList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "tripList List", sqlException);
        }
        return totalList;
    }

    public int saveInvoiceDetail(OperationTO operationTO, int userId) {
        Map map = new HashMap();
//            OperationTO operation = new OperationTO();
        String[] temp;
        String[] temp1;
        String code = "";
        String code1 = "";
        int lastInsertedId = 0;
        try {
            map.put("userId", userId);
            map.put("FromDate", operationTO.getFromDate());
            map.put("ToDate", operationTO.getToDate());
            map.put("taxAmount", operationTO.getTaxAmount());
            map.put("NoOfTrip", operationTO.getNoOfTrip());
            map.put("TotalTonnage", operationTO.getTotalTonnage());
            map.put("DeliveredTonnage", operationTO.getDeliveredTonnage());
            map.put("TotalTonAmount", operationTO.getTotalTonAmount());
            map.put("Totalamount", operationTO.getTotalInvAmount());
            map.put("Totalexpenses", operationTO.getTotalexpenses());
            map.put("Balanceamount", operationTO.getBalanceamount());
            map.put("Remark", operationTO.getRemark());
            temp = operationTO.getCustomerId().split("~");
            //////System.out.println("temp[0] = " + temp[0]);
            //////System.out.println("temp[1] = " + temp[1]);
            map.put("customerId", temp[0]);
            map.put("customerName", temp[1]);
            //////System.out.println("map = " + map);
            code = (String) getSqlMapClientTemplate().queryForObject("operation.getInvoiceCode", map);
            temp = code.split("-");
            int codeval = Integer.parseInt(temp[1]);
            int codev = codeval + 1;
            String invoiceCode = "INV-" + codev;
            //////System.out.println("invoiceCode = " + invoiceCode);
            map.put("invoiceCode", invoiceCode);

            code1 = (String) getSqlMapClientTemplate().queryForObject("operation.getInvRefCode", map);
            temp1 = code1.split("-");
            int codeInvVal = Integer.parseInt(temp1[1].trim());
            int codev1 = codeInvVal + 1;
            String invRefCode = "CLPL-" + codev1;
            map.put("invRefCode", invRefCode);
            //////System.out.println("map in Invoice  *& -------> " + map);
            lastInsertedId = (Integer) getSqlMapClientTemplate().insert("operation.insertInvoiceDetails", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return lastInsertedId;
    }

    public String getInvoiceCode() {
        Map map = new HashMap();
        String invoiceCode = "";
        try {
            invoiceCode = (String) getSqlMapClientTemplate().queryForObject("operation.getInvoiceCode", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCode Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getInvoiceCode", sqlException);
        }
        return invoiceCode;
    }

    public int saveInvoice(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        String InvoiceCode = "";
//        String invRefCode = "";
        String[] temp;
        String[] temp1;
        String[] temp2;
        String code = "";
        String code1 = "";
        int codev = 0;
        int codeval = 0;
        String invoiceCode = "";
        map.put("userId", userId);
        if ((String) getSqlMapClientTemplate().queryForObject("operation.getInvoiceCode", map) != null && !"".equals((String) getSqlMapClientTemplate().queryForObject("operation.getInvoiceCode", map))) {
            code = (String) getSqlMapClientTemplate().queryForObject("operation.getInvoiceCode", map);
            temp = code.split("-");
            codeval = Integer.parseInt(temp[1]);
            codev = codeval + 1;
            invoiceCode = "INV-" + codev;
        } else {
            codev = 1;
            invoiceCode = "INV-" + codev;
        }
        if (operationTO.getSno() == 0) {
            map.put("invoiceCode", invoiceCode);
        } else {
            map.put("invoiceCode", code);
        }

        int codeInvVal = 0;
        int codev1 = 0;
        String invRefCode = "";
        if ((String) getSqlMapClientTemplate().queryForObject("operation.getInvRefCode", map) != null && !"".equals((String) getSqlMapClientTemplate().queryForObject("operation.getInvRefCode", map))) {
            code1 = (String) getSqlMapClientTemplate().queryForObject("operation.getInvRefCode", map);
            temp1 = code1.split("-");
            codeInvVal = Integer.parseInt(temp1[1].trim());
            codev1 = codeInvVal + 1;
            invRefCode = "CLPL-" + codev1;
        } else {
            codev1 = 1;
            invRefCode = "CLPL-" + codev1;
        }
        if (operationTO.getSno() == 0) {
            map.put("invRefCode", invRefCode);
        } else {
            map.put("invRefCode", code1);
        }
        map.put("TripId", operationTO.getTripId());
        map.put("TripDate", operationTO.getTripDate());
        map.put("Regno", operationTO.getRegno());
        map.put("RouteName", operationTO.getRouteName());
        map.put("CustName", operationTO.getCustName());
        map.put("Totalamount", operationTO.getTotalamount());
        temp2 = operationTO.getCustomerId().split("~");
        map.put("customerId", temp2[0]);
        map.put("gpno", operationTO.getGpno());
        map.put("invoiceNo", operationTO.getInvoiceNo());
        map.put("totalTonnage", operationTO.getTotalTonnage());
        map.put("returnTripNo", "0");
        //////System.out.println("map = " + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("operation.insertTripWiseDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertInvoice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertInvoice", sqlException);
        }
        return status;
    }

    public int alterTripStatus(String tripId) {
        Map map = new HashMap();
        int status = 0;
        map.put("tripId", tripId);
        //////System.out.println("map = " + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("operation.updateTripStatus", map);
            //////System.out.println(" Status in Invoice DAO======>" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertInvoice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertInvoice", sqlException);
        }
        return status;
    }

    public int saveaccountEntry(OperationTO operationTO, int userId) {
        Map map = new HashMap();
//            OperationTO operation = new OperationTO();
        String[] temp;
        String[] temp1;
        String code = "";
        String code1 = "";
        String code2 = "";
        String invRefCode = "";
        int status = 0;
        int status1 = 0;
        int status2 = 0;
        try {
//                    --------------------------------- acc 1st row start --------------------------
            map.put("userId", userId);
            map.put("DetailCode", "1");
            code2 = (String) getSqlMapClientTemplate().queryForObject("operation.getVoucherCode", map);
            temp = code2.split("-");
            int codeval2 = Integer.parseInt(temp[1]);
            int codev2 = codeval2 + 1;
            String voucherCode = "SALES-" + codev2;
            //////System.out.println("voucherCode = " + voucherCode);
            map.put("voucherCode", voucherCode);
            map.put("mainEntryType", "SALES");
            map.put("entryType", "SALES");
            map.put("particularsId", "LEDGER-3");
            double taxAmount = Double.parseDouble(operationTO.getTaxAmount());
            double totalAmount = Double.parseDouble(operationTO.getTotalInvAmount());
            double amount = taxAmount + totalAmount;
            //////System.out.println("amount = " + amount);
            map.put("amount", amount);
            map.put("Accounts_Type", "DEBIT");
            map.put("Remark", operationTO.getRemark());
            invRefCode = (String) getSqlMapClientTemplate().queryForObject("operation.getInvRefCode", map);
            map.put("invRefCode", invRefCode.trim());
            //////System.out.println("map =---------------------> " + map);
            status = (Integer) getSqlMapClientTemplate().update("operation.insertAccountEntry", map);
            //////System.out.println("status = " + status);
//                  lastInsertedId = (Integer) getSqlMapClientTemplate().insert("operation.insertInvoiceDetails", map);
//                    --------------------------------- acc 1st row end --------------------------

//                    --------------------------------- acc 2nd row start --------------------------
            if (status > 0) {
                map.put("DetailCode", "2");
                map.put("particularsId", "LEDGER-2");
                map.put("amount", operationTO.getTotalInvAmount());
                map.put("Accounts_Type", "CREDIT");
                //////System.out.println("map2 =---------------------> " + map);
                status1 = (Integer) getSqlMapClientTemplate().update("operation.insertAccountEntry", map);
                //////System.out.println("status1 = " + status1);
            }
//                    --------------------------------- acc 2nd row end --------------------------
//                    --------------------------------- acc 3rd row start --------------------------
            if (status1 > 0) {
                map.put("DetailCode", "3");
                map.put("particularsId", "LEDGER-4");
                map.put("amount", operationTO.getTaxAmount());
                map.put("Accounts_Type", "CREDIT");
                //////System.out.println("map3 =---------------------> " + map);
                status2 = (Integer) getSqlMapClientTemplate().update("operation.insertAccountEntry", map);
                //////System.out.println("status2 = " + status2);
            }
//                    --------------------------------- acc 2nd row end --------------------------

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return status2;
    }

    public ArrayList getInvoiceDetailList(OperationTO operationTO) {
        Map map = new HashMap();
        String Summary = operationTO.getSummary();
        if (Summary != "Summary") {
            String invoiceId = "";
            invoiceId = (String) getSqlMapClientTemplate().queryForObject("operation.getInvoiceId", map);
            map.put("invoiceId", invoiceId.trim());
        } else {
            map.put("invoiceId", operationTO.getInvoiceCode());
        }
        //////System.out.println("map = " + map);
        ArrayList invoiceDetailList = new ArrayList();
        try {
            invoiceDetailList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getInvoiceHeaderList", map);
            //////System.out.println("invoiceDetailList in DAO= " + invoiceDetailList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("invoiceDetailList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "invoiceDetailList List", sqlException);
        }
        return invoiceDetailList;
    }

    public ArrayList getinvoiceTripDetailList(OperationTO operationTO) {
        Map map = new HashMap();
        String Summary = operationTO.getSummary();
        if (Summary != "Summary") {
            String invoiceId = "";
            invoiceId = (String) getSqlMapClientTemplate().queryForObject("operation.getInvoiceId", map);
            map.put("invoiceId", invoiceId.trim());
        } else {
            map.put("invoiceId", operationTO.getInvoiceCode());
        }
        //////System.out.println("map = " + map);
        ArrayList invoiceTripDetailList = new ArrayList();
        try {
            invoiceTripDetailList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getInvoiceDetailList", map);
            //////System.out.println("invoiceTripDetailList in DAO= " + invoiceTripDetailList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("invoiceTripDetailList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "invoiceTripDetailList List", sqlException);
        }
        return invoiceTripDetailList;
    }

    public ArrayList getinvoiceHeaderList() {
        Map map = new HashMap();
        ArrayList invoiceHeaderList = new ArrayList();
        try {
            invoiceHeaderList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getinvHeaderList", map);
            //////System.out.println("invoiceHeaderList in DAO-----------=>" + invoiceHeaderList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("invoiceHeaderList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "invoiceHeaderList List", sqlException);
        }
        return invoiceHeaderList;
    }

    public ArrayList getDisplyInvoiceSummary(String fromDate, String toDate, String customer) {
        Map map = new HashMap();
        String[] temp;
        ArrayList displyInvoiceSummary = new ArrayList();

        if (fromDate != "" && toDate != "") {
            temp = fromDate.split("-");
            String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
            map.put("sDate", sDate);
            temp = toDate.split("-");
            String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
            map.put("eDate", eDate);
        }
        map.put("customer", customer);
        //////System.out.println("map = " + map);
        try {
            displyInvoiceSummary = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getDisplyInvoiceSummary", map);
            //////System.out.println("invoiceHeaderList in DAO-----------=>" + displyInvoiceSummary.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("invoiceHeaderList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "invoiceHeaderList List", sqlException);
        }
        return displyInvoiceSummary;
    }

    /**
     * This method used to Market Vehicle settlement.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getVendorList() {
        Map map = new HashMap();
        ArrayList vendorList = new ArrayList();
        try {
            vendorList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getVendorList", map);
            //////System.out.println("vendorList = " + vendorList.size());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "vendorList", sqlException);
        }
        return vendorList;
    }

    public ArrayList getMarketVehicleList(String fromDate, String toDate, String regno, String vendorId) {
        Map map = new HashMap();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("Regno", regno);
        map.put("vendorId", vendorId);
        //////System.out.println("map =-----------------> " + map);
        ArrayList marketVehicleList = new ArrayList();
        try {
            if (getSqlMapClientTemplate().queryForList("operation.getMarketVehicleList", map) != null) {
                marketVehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getMarketVehicleList", map);
            }
            //////System.out.println("marketVehicleList size=" + marketVehicleList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("marketVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "marketVehicleList", sqlException);
        }
        return marketVehicleList;
    }

    public ArrayList getTotalMarketVehicleList(String fromDate, String toDate, String regno, String vendorId) {
        Map map = new HashMap();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("Regno", regno);
        map.put("vendorId", vendorId);
        //////System.out.println("map =-----------------> " + map);
        ArrayList totalMarketVehicleList = new ArrayList();
        try {
            totalMarketVehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getTotalMarketVehicleList", map);
            //////System.out.println("totalMarketVehicleList size=" + totalMarketVehicleList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTotalMarketVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTotalMarketVehicleList", sqlException);
        }
        return totalMarketVehicleList;
    }

    public ArrayList getCommissionVal() {
        Map map = new HashMap();
        ArrayList commissionVal = new ArrayList();
        try {
            commissionVal = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCommissionVal", map);
            //////System.out.println("totalMarketVehicleList size=" + commissionVal.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCommissionVal Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCommissionVal", sqlException);
        }
        return commissionVal;
    }

    public int saveVendorSettlementHeader(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int lastInsertedId = 0;
        try {
            map.put("userId", userId);
            map.put("FromDate", operationTO.getFromDate());
            map.put("ToDate", operationTO.getToDate());
            map.put("NoOfTrip", operationTO.getNoOfTrip());
            map.put("VendorId", operationTO.getVendorId());
            map.put("Totalregno", operationTO.getTotalregno());
            map.put("TotalTonnage", operationTO.getTotalTonnage());
            map.put("TotalDeliveredTonnage", operationTO.getTotalDeliveredTonnage());
            map.put("Totalshortage", operationTO.getTotalshortage());
            map.put("commissionPercentage", operationTO.getCommissionPercentage());
            map.put("totalamount", operationTO.getTotalamount());
            map.put("commissionAmount", operationTO.getCommissionAmount());
            map.put("settlementAmount", operationTO.getSettlementAmount());
            //////System.out.println("map in Invoice  *& -------> " + map);
            lastInsertedId = (Integer) getSqlMapClientTemplate().insert("operation.insertVendorSettlement", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return lastInsertedId;
    }

    public int alterTripSettlement(String tripId) {
        //////System.out.println("-------in DAO-------");
        Map map = new HashMap();
        int status = 0;
        map.put("tripId", tripId);
        //////System.out.println("map = " + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("operation.updateTripSettlement", map);
            //////System.out.println(" Status in Invoice DAO======>" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertInvoice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertInvoice", sqlException);
        }
        return status;
    }

    public int saveVendorSettlementAccEntry(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        String code = "";
        String code1 = "";
        String settlementId = "";
        int status = 0;
        int status1 = 0;
        String[] temp;
        int lastInsertedId = 0;
        try {
            //  --------------------------------- acc 1st row start --------------------------
            map.put("userId", userId);
            map.put("DetailCode", "1");
            code = (String) getSqlMapClientTemplate().queryForObject("operation.getSettlementVoucherCode", map);
            temp = code.split("-");
            int codeval = Integer.parseInt(temp[1]);
            int codev = codeval + 1;
            String voucherCode = "PAYMENT-" + codev;
            //////System.out.println("voucherCode = " + voucherCode);
            map.put("voucherCode", voucherCode);
            map.put("mainEntryType", "PAYMENT");
            map.put("entryType", "PAYMENT");
            map.put("vendorName", operationTO.getVendorName());
            code1 = (String) getSqlMapClientTemplate().queryForObject("operation.getLedgerCode", map);
            //////System.out.println("code = " + code1);
            map.put("particularsId", code1);
            map.put("amount", operationTO.getSettlementAmount());
            map.put("Accounts_Type", "DEBIT");
            map.put("Remark", "VendorSettlement");
            settlementId = (String) getSqlMapClientTemplate().queryForObject("operation.getSettlementCode", map);
            map.put("invRefCode", settlementId.trim());
            //////System.out.println("map in Invoice  *& 1-------> " + map);
            status = (Integer) getSqlMapClientTemplate().update("operation.insertAccountEntry", map);
//  --------------------------------- acc 1st row end --------------------------
//  --------------------------------- acc 2st row start --------------------------

            map.put("DetailCode", "2");
            map.put("particularsId", "LEDGER-1");
            map.put("Accounts_Type", "CREDIT");
            //////System.out.println("map in Invoice  *& 2-------> " + map);
            status1 = (Integer) getSqlMapClientTemplate().update("operation.insertAccountEntry", map);

//  --------------------------------- acc 2st row end --------------------------
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return status1;
    }

    public int saveVendorSettlementDetail(OperationTO operationTO, int userId) {
        //////System.out.println("-------in DAO-------");
        Map map = new HashMap();
        int status = 0;
        String getSettlementID = "";
        map.put("userId", userId);
        getSettlementID = (String) getSqlMapClientTemplate().queryForObject("operation.getSettlementID", map);
        map.put("getSettlementID", getSettlementID.trim());
        map.put("TripId", operationTO.getTripId());
        map.put("Regno", operationTO.getRegno());
        map.put("routeName", operationTO.getRouteName());
        map.put("OutKM", operationTO.getOutKM());
        map.put("outDateTime", operationTO.getOutDateTime());
        map.put("InKM", operationTO.getInKM());
        map.put("inDateTime", operationTO.getInDateTime());
        map.put("TotalTonnage", operationTO.getTotalTonnage());
        map.put("deliveredTonnage", operationTO.getDeliveredTonnage());
        map.put("shortage", operationTO.getShortage());
        map.put("Totalamount", operationTO.getTotalamaount());
        //////System.out.println("map =========[[[[=== " + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("operation.insertTripWiseSettlement", map);
            //////System.out.println(" Status in Invoice DAO======>" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertInvoice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertInvoice", sqlException);
        }
        return status;
    }

    public ArrayList getsettlementHeader() {
        String getSettlementID = "";
        Map map = new HashMap();
        getSettlementID = (String) getSqlMapClientTemplate().queryForObject("operation.getSettlementID", map);
        //////System.out.println("getSettlementID = " + getSettlementID);
        map.put("getSettlementID", getSettlementID.trim());
        ArrayList settlementHeader = new ArrayList();
        try {
            settlementHeader = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getsettlementHeader", map);
            //////System.out.println("totalMarketVehicleList size=" + settlementHeader.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("settlementHeader Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "settlementHeader", sqlException);
        }
        return settlementHeader;
    }

    public ArrayList getsettlementDetail() {
        String getSettlementID = "";
        Map map = new HashMap();
        getSettlementID = (String) getSqlMapClientTemplate().queryForObject("operation.getSettlementID", map);
        //////System.out.println("getSettlementID = " + getSettlementID);
        map.put("getSettlementID", getSettlementID.trim());
        ArrayList settlementDetail = new ArrayList();
        try {
            settlementDetail = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getSettlementDetail", map);
            //////System.out.println("totalMarketVehicleList size=" + settlementDetail.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("settlementDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "settlementDetail", sqlException);
        }
        return settlementDetail;
    }

    public String saveReturnMovement(int userId, String tripCode, String tripDate, String customer, String fromLocation, String fromLocationId, String toLocation, String toLocationId,
            String returnTripType, String returnProductName, String returnLoadedDate, String returnDeliveredDate, String loadedTon, String deliveredTon, String shortageTon, String loadedSlipNo, String deliveredDANo, String returnKM, String inKM, String outKM, String returnAmount) {
        Map map = new HashMap();
        OperationTO operationTO = null;
        int returnTripId = 0;
        String ReturnRouteCode = "";
        map.put("userId", userId);
        map.put("tripCode", tripCode);
        map.put("tripDate", tripDate);
        map.put("fromLocation", fromLocation);
        map.put("fromLocationId", fromLocationId);
        map.put("toLocation", toLocation);
        map.put("toLocationId", toLocationId);
        map.put("returnTripType", returnTripType);
        map.put("returnProductName", returnProductName);
        map.put("returnLoadedDate", returnLoadedDate);
        map.put("returnDeliveredDate", returnDeliveredDate);
        map.put("customer", customer);
        //////System.out.println("customer = " + customer);
        map.put("loadedTon", loadedTon);
        map.put("deliveredTon", deliveredTon);
        map.put("shortageTon", shortageTon);
        map.put("loadedSlipNo", loadedSlipNo);
        map.put("deliveredDANo", deliveredDANo);

        map.put("returnKM", returnKM);
        map.put("inKM", inKM);
        map.put("outKM", outKM);
        map.put("returnAmount", returnAmount);
        //////System.out.println("map in saveReturnMovement DAO= " + map);
        String suggestions = "";
        try {

            ReturnRouteCode = (String) getSqlMapClientTemplate().queryForObject("operation.getReturnRouteCode", map);
            //////System.out.println("map in returnRouteCode DAO= " + ReturnRouteCode);
            map.put("returnRouteCode", ReturnRouteCode);

            returnTripId = (Integer) getSqlMapClientTemplate().insert("operation.saveReturnMovement", map);
            //////System.out.println("returnTripId= " + returnTripId);

            suggestions = Integer.toString(returnTripId);
            //////System.out.println("suggestions = " + suggestions);
            if ("loaded".equalsIgnoreCase(returnTripType)) {
                //make account entry
                //trip accountEntry postiong

                String[] temp;
                String code2 = "";
                String getTripNo = "";
                int status1 = 0;
                int status2 = 0;

                //  --------------------------------- acc 1st row start --------------------------
                map.put("userId", userId);
                map.put("DetailCode", "1");
                map.put("voucherType", "%SALES%");
                code2 = (String) getSqlMapClientTemplate().queryForObject("operation.getTripVoucherCode", map);
                temp = code2.split("-");
                int codeval2 = Integer.parseInt(temp[1]);
                int codev2 = codeval2 + 1;
                String voucherCode = "SALES-" + codev2;
                //////System.out.println("voucherCode = " + voucherCode);
                map.put("voucherCode", voucherCode);
                map.put("mainEntryType", "VOUCHER");
                map.put("entryType", "SALES");
                //select ledger code of the return load customer
                String ledgerInfo = (String) getSqlMapClientTemplate().queryForObject("operation.getCustomerLedgerInfo", map);
                temp = ledgerInfo.split("~");
                String ledgerId = temp[0];
                String particularsId = temp[1];

                map.put("ledgerId", ledgerId);
                map.put("particularsId", particularsId);

                map.put("amount", returnAmount);
                map.put("Accounts_Type", "DEBIT");
                map.put("Remark", "Freight Charges");
                map.put("Reference", returnTripId);
                map.put("SearchCode", tripCode);
                //////System.out.println("map1 =---------------------> " + map);
                status1 = (Integer) getSqlMapClientTemplate().update("operation.insertTripAccountEntry", map);
                //////System.out.println("status1 = " + status1);
                //--------------------------------- acc 2nd row start --------------------------
                if (status1 > 0) {
                    map.put("DetailCode", "2");
                    map.put("ledgerId", "51");
                    map.put("particularsId", "LEDGER-39");
                    map.put("Accounts_Type", "CREDIT");
                    //////System.out.println("map2 =---------------------> " + map);
                    status2 = (Integer) getSqlMapClientTemplate().update("operation.insertTripAccountEntry", map);
                    //////System.out.println("status2 = " + status2);
                }
                //--------------------------------- acc 2nd row end --------------------------
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getToDestination Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getToDestination", sqlException);
        }
        return suggestions;
    }

    public String getFLocation(String fromLocation) {
        Map map = new HashMap();
        map.put("fromLocation", fromLocation);
        String suggestions = "";
        OperationTO operationTO = new OperationTO();
        try {
            ArrayList FLocation = new ArrayList();
            //////System.out.println("map =-----> " + map);
            FLocation = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getFLocation", map);
            //////System.out.println("FLocation.size() = " + FLocation.size());
            Iterator itr = FLocation.iterator();
            while (itr.hasNext()) {
                operationTO = new OperationTO();
                operationTO = (OperationTO) itr.next();
                suggestions = operationTO.getfLocationName() + "^" + operationTO.getfLocationId() + "~" + suggestions;
                //suggestions = operationTO.getToLocation() + "~" + suggestions;
                //////System.out.println("suggestions: " + suggestions);
            }
            if ("".equals(suggestions)) {
                suggestions = "no match found";
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getToDestination Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getToDestination", sqlException);
        }
        return suggestions;
    }

    public String getTLocation(String toLocation) {
        Map map = new HashMap();
        map.put("toLocation", toLocation);
        String suggestions = "";
        OperationTO operationTO = new OperationTO();
        try {
            ArrayList TLocation = new ArrayList();
            //////System.out.println("map =-----> " + map);
            TLocation = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getTLocation", map);
            //////System.out.println("FLocation.size() = " + TLocation.size());
            Iterator itr = TLocation.iterator();
            while (itr.hasNext()) {
                operationTO = new OperationTO();
                operationTO = (OperationTO) itr.next();
                suggestions = operationTO.gettLocationName() + "^" + operationTO.gettLocationId() + "~" + suggestions;
                //suggestions = operationTO.getToLocation() + "~" + suggestions;
                //////System.out.println("suggestions: " + suggestions);
            }
            if ("".equals(suggestions)) {
                suggestions = "no match found";
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getToDestination Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getToDestination", sqlException);
        }
        return suggestions;
    }

    public ArrayList consignmentPrint(String tripSheetId) {
        Map map = new HashMap();
        map.put("tripSheetId", tripSheetId);

        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.consignmentPrint", map);
            //////System.out.println("tripDetails View size 3 ====" + tripDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJobCardTechnicians Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getJobCardTechnicians List", sqlException);
        }

        return tripDetails;
    }

    //CLPL code end
//    Brattle Foods Starts Here
    public String getCurrentFuelPrice() {
        Map map = new HashMap();
        String currentFuelPrice = "";
        try {
            //////System.out.println("map = " + map);

            currentFuelPrice = (String) getSqlMapClientTemplate().queryForObject("operation.getCurrentFuelPrice", map);
            //////System.out.println("CurrentFuelPrice " + currentFuelPrice);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCurrentFuelPrice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPinkSlipDetail", sqlException);
        }

        return currentFuelPrice;
    }

    public String getCurrentCNGPrice() {
        Map map = new HashMap();
        String currentCNGPrice = "";
        try {
            //////System.out.println("map = " + map);

            currentCNGPrice = (String) getSqlMapClientTemplate().queryForObject("operation.getCurrentCNGPrice", map);
            //////System.out.println("currentCNGPrice " + currentCNGPrice);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCurrentCNGPrice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCurrentCNGPrice", sqlException);
        }

        return currentCNGPrice;
    }

    /**
     * This method used to Get Config Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String getConfigDetails(OperationTO operationTO) {
        Map map = new HashMap();
        String configDetails = "";
        try {
            configDetails = (String) getSqlMapClientTemplate().queryForObject("operation.getConfigDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConfigDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConfigDetails", sqlException);
        }

        return configDetails;
    }

    public ArrayList getMileageConfigList(String customerId) {
        Map map = new HashMap();
        ArrayList mileageConfigList = new ArrayList();
        map.put("customerId", customerId);
        //////System.out.println("map for:" + map);
        try {
            mileageConfigList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getMilleageConfigList", map);
            //////System.out.println("mileageConfigList " + mileageConfigList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMileageConfigList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getMileageConfigList List", sqlException);
        }

        return mileageConfigList;
    }

    public ArrayList getSellingCostList(String customerId) {
        Map map = new HashMap();
        ArrayList sellingCostList = new ArrayList();
        map.put("customerId", customerId);
        //////System.out.println("map for:" + map);
        try {
            sellingCostList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getSellingCostList", map);
            //////System.out.println("getSellingCostList " + sellingCostList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSellingCostList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSellingCostList List", sqlException);
        }

        return sellingCostList;
    }

    public ArrayList getSellingCostDetailsList(String routeId) {
        Map map = new HashMap();
        ArrayList sellingCostDetailsList = new ArrayList();
        map.put("routeId", routeId);
        //////System.out.println("map for:" + map);
        try {
            sellingCostDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getSellingCostDetailsList", map);
            //////System.out.println("getSellingCostList " + sellingCostDetailsList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSellingCostList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSellingCostList List", sqlException);
        }

        return sellingCostDetailsList;
    }

    public ArrayList getBorderListDetails(String routeId) {
        Map map = new HashMap();
        ArrayList borderDetailsList = new ArrayList();
        map.put("routeId", routeId);
        //////System.out.println("map for:" + map);
        try {
            borderDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getBorderListDetails", map);
            //////System.out.println("getBorderListDetails " + borderDetailsList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBorderListDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBorderListDetails List", sqlException);
        }

        return borderDetailsList;
    }

    public String checkRouteCode(OperationTO operationTO) {
        Map map = new HashMap();
        String checkRouteCode = "";
        try {
            System.out.println("map = " + map);
            map.put("routeCode", operationTO.getRouteCode());
            checkRouteCode = (String) getSqlMapClientTemplate().queryForObject("operation.checkRouteCode", map);
            System.out.println("checkRouteCode " + checkRouteCode);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkRouteCode Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkRouteCode", sqlException);
        }

        return checkRouteCode;
    }

    public String getNextRouteCode() {
        Map map = new HashMap();
        String nextRouteCode = "";
        try {
            nextRouteCode = (String) getSqlMapClientTemplate().queryForObject("operation.nextRouteCode", map);
            System.out.println("nextRouteCode " + nextRouteCode);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("nextRouteCode Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "nextRouteCode", sqlException);
        }

        return nextRouteCode;
    }

    public String checkRoute(OperationTO operationTO) {
        Map map = new HashMap();
        String checkRoute = "";
        map.put("cityFromId", operationTO.getCityFromId());
        map.put("cityToId", operationTO.getCityToId());
        try {
            checkRoute = (String) getSqlMapClientTemplate().queryForObject("operation.checkRoute", map);
            System.out.println("checkRoute " + checkRoute);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkRoute Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkRoute", sqlException);
        }

        return checkRoute;
    }

    /**
     * This method used to Save Route .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int saveRoute(OperationTO operationTO, String[] rtoId, int userId) {
        Map map = new HashMap();
        int routeId = 0;
        int insertStatus = 0;
        try {
            try {
                String vehTypeId[] = null;
                String vehMileage[] = null;
                String reefMileage[] = null;
                String fuelCostPerKms[] = null;
                String fuelCostPerKmsEmpCon[] = null;
                String fuelCostPerKms20ftCon[] = null;
                String fuelCostPerKms2_20ftCon[] = null;
                String fuelCostPerKms40ftCon[] = null;
                String fuelCostPerHrs[] = null;
                String tollAmounts[] = null;
                String miscCostKm[] = null;
                String driverIncenKm[] = null;
                String varExpense[] = null;
                String factor[] = null;
                String vehExpense[] = null;
                String vehExpenseEmpCon[] = null;
                String vehExpense20ftCon[] = null;
                String vehExpense2_20ftCon[] = null;
                String vehExpense40ftCon[] = null;
                String referExpense[] = null;
                String fuelWithReefer[] = null;
                String fuelWOReefer[] = null;

                String totExpense[] = null;
                map.put("routeCode", operationTO.getRouteCode());
                map.put("cityFromId", operationTO.getCityFromId());
                map.put("cityToId", operationTO.getCityToId());
                map.put("distance", operationTO.getDistance());
                map.put("travelHour", operationTO.getTravelHour());
                map.put("travelMinute", operationTO.getTravelMinute());
                map.put("reeferHour", operationTO.getReeferHour());
                map.put("reeferMinute", operationTO.getReeferMinute());
                map.put("tollAmountType", operationTO.getTollAmountType());
                map.put("roadType", operationTO.getRoadType());
                map.put("fuelCost", operationTO.getFuelCost());
                map.put("incentiveStatus", operationTO.getIncentiveStatus());
                map.put("tollStatus", operationTO.getTollStatus());
                map.put("miscStatus", operationTO.getMiscStatus());
                map.put("userId", userId);
                System.out.println("map 1 = " + map);
                routeId = (Integer) getSqlMapClientTemplate().insert("operation.insertRouteCode", map);
                System.out.println("routeId = " + routeId);
                if (routeId > 0) {
                    map.put("routeId", routeId);
                    vehTypeId = operationTO.getVehTypeId();
                    vehMileage = operationTO.getVehMileage();
                    reefMileage = operationTO.getReefMileage();
                    fuelCostPerKms = operationTO.getFuelCostPerKms();
                    fuelCostPerKmsEmpCon = operationTO.getFuelCostPerKmsEmpCon();
                    fuelCostPerKms20ftCon = operationTO.getFuelCostPerKms20ftCon();
                    fuelCostPerKms2_20ftCon = operationTO.getFuelCostPerKms2_20ftCon();
                    fuelCostPerKms40ftCon = operationTO.getFuelCostPerKms40ftCon();
                    fuelCostPerHrs = operationTO.getFuelCostPerHrs();
                    tollAmounts = operationTO.getTollAmounts();
                    miscCostKm = operationTO.getMiscCostKm();
                    driverIncenKm = operationTO.getDriverIncenKm();
                    varExpense = operationTO.getVarExpense();
                    vehExpenseEmpCon = operationTO.getVehExpenseEmpCon();
                    vehExpense20ftCon = operationTO.getVehExpense20ftCon();
                    vehExpense2_20ftCon = operationTO.getVehExpense2_20ftCon();
                    vehExpense40ftCon = operationTO.getVehExpense40ftCon();
                    factor = operationTO.getFactor();
                    vehExpense = operationTO.getVehExpense();
                    referExpense = operationTO.getReeferExpense();
                    totExpense = operationTO.getTotExpense();
                    fuelWOReefer = operationTO.getFuelWOReefer();
                    fuelWithReefer = operationTO.getFuelWithReefer();
                    for (int i = 0; i < vehTypeId.length; i++) {
                        map.put("vehTypeId", vehTypeId[i]);
                        map.put("vehMileage", vehMileage[i]);
                        map.put("reefMileage", reefMileage[i]);
                        map.put("fuelCostPerKms", fuelCostPerKms[i]);
                        map.put("fuelCostPerKmsEmpCon", "0");
                        map.put("fuelCostPerKms20ftCon", "0");
                        map.put("fuelCostPerKms2_20ftCon", "0");
                        map.put("fuelCostPerKms40ftCon", "0");
                        map.put("fuelCostPerHrs", fuelCostPerHrs[i]);
                        map.put("tollAmounts", tollAmounts[i]);
                        map.put("miscCostKm", miscCostKm[i]);
                        map.put("driverIncenKm", driverIncenKm[i]);
                        map.put("varExpense", varExpense[i]);
                        map.put("factor", factor[i]);
                        map.put("vehExpense", vehExpense[i]);
                        map.put("vehExpenseEmpCon", "0");
                        map.put("vehExpense20ftCon", "0");
                        map.put("vehExpense2_20ftCon", "0");
                        map.put("vehExpense40ftCon", "0");
                        map.put("referExpense", referExpense[i]);
                        map.put("totExpense", totExpense[i]);
                        map.put("fuelWOReefer", fuelWOReefer[i]);
                        map.put("fuelWithReefer", fuelWithReefer[i]);
                        System.out.println("map 2= " + map);
                        insertStatus = (Integer) getSqlMapClientTemplate().update("operation.insertRouteCost", map);
                    }
                    // insert rto details
                    if (rtoId != null && rtoId.length > 0) {

                        for (int i = 0; i < rtoId.length; i++) {
                            map.put("userId", userId);
                            map.put("rtoId", rtoId[i]);
                            System.out.println("map for rto:" + map);
                            insertStatus = (Integer) getSqlMapClientTemplate().insert("operation.insertRtoRouteMapping", map);
                        }
                    }
                }
            } catch (Exception ex) {
                Writer writer = new StringWriter();
                PrintWriter printWriter = new PrintWriter(writer);
                ex.printStackTrace(printWriter);
                String s = writer.toString();
                System.out.println("s = " + s);
            }
        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveRoute Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveRoute", sqlException);
        }

        return insertStatus;
    }

    /**
     * This method used to Get Route List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getRouteList(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList routeList = new ArrayList();
        map.put("routeNameFrom", operationTO.getRouteFrom());
        map.put("routeNameTo", operationTO.getRouteTo());
        map.put("routeFromId", operationTO.getRouteFromId());
        map.put("routeToId", operationTO.getRouteToId());
        map.put("routeId", operationTO.getRouteId());
        map.put("routeCode", operationTO.getRouteCode());
        //        map.put("tollAmountType", operationTO.getTollAmountType());
        map.put("editRouteId", operationTO.getEditRouteId());
        try {
            routeList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getRouteList", map);
            System.out.println("routeList " + routeList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRouteList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRouteList List", sqlException);
        }

        return routeList;
    }

    /**
     * This method used to Get Route Name From.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getRouteNameFrom(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList routeNameList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("routeFrom", operationTO.getRouteFrom() + "%");
        map.put("routeTo", operationTO.getRouteTo());
        //        System.out.println("password " + loginTO.getPassword());
        System.out.println("map = " + map);

        try {
            routeNameList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getRouteNameFrom", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRouteNameFrom Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getRouteNameFrom", sqlException);
        }
        return routeNameList;
    }

    /**
     * This method used to Get Route Name To.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getRouteNameTo(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList routeNameList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("routeTo", operationTO.getRouteTo() + "%");
        map.put("routeFrom", operationTO.getRouteFrom());
        //        System.out.println("password " + loginTO.getPassword());
        System.out.println("map = " + map);

        try {
            routeNameList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getRouteNameTo", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRouteNameTo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getRouteNameTo", sqlException);
        }
        return routeNameList;
    }
    public ArrayList getRouteDetailsList(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList routeDetailsList = new ArrayList();
        if (operationTO.getEditRouteId() != null && !"".equals(operationTO.getEditRouteId())) {
            map.put("routeId", operationTO.getEditRouteId());
        }
        try {
            routeDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getRouteDetailsList", map);
            System.out.println("routeDetailsList " + routeDetailsList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRouteDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRouteDetailsList List", sqlException);
        }

        return routeDetailsList;
    }

    /**
     * This method used to Update Route Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int updateRoute(OperationTO operationTO, String[] rtoIds, int userId) {
        Map map = new HashMap();
        int routeId = 0;
        int updateStatus = 0;
        int insertStatus = 0;
        try {
            String vehTypeId[] = null;
            String vehMileage[] = null;
            String reefMileage[] = null;
            String fuelCostPerKms[] = null;
            String fuelCostPerKmsEmpCon[] = null;
            String fuelCostPerKms20ftCon[] = null;
            String fuelCostPerKms2_20ftCon[] = null;
            String fuelCostPerKms40ftCon[] = null;

            String fuelCostPerHrs[] = null;
            String tollAmounts[] = null;
            String miscCostKm[] = null;
            String driverIncenKm[] = null;
            String varExpense[] = null;
            String vehExpenseEmpCon[] = null;
            String vehExpense20ftCon[] = null;
            String vehExpense2_20ftCon[] = null;
            String vehExpense40ftCon[] = null;
            String factor[] = null;
            String vehExpense[] = null;
            String referExpense[] = null;
            String totExpense[] = null;
            String routeCostId[] = null;
            String fuelWithReefer[] = null;
            String fuelWOReefer[] = null;

            map.put("routeId", operationTO.getRouteId());
            map.put("routeCode", operationTO.getRouteCode());
            map.put("cityFromId", operationTO.getCityFromId());
            map.put("cityToId", operationTO.getCityToId());
            map.put("distance", operationTO.getDistance());
            map.put("travelHour", operationTO.getTravelHour());
            map.put("travelMinute", operationTO.getTravelMinute());
            map.put("reeferHour", operationTO.getReeferHour());
            map.put("reeferMinute", operationTO.getReeferMinute());
            map.put("roadType", operationTO.getRoadType());
            map.put("fuelCost", operationTO.getFuelCost());
            map.put("status", operationTO.getStatus());
            map.put("advance", operationTO.getRequestedadvance());
            map.put("userId", userId);
            System.out.println("map 1 = " + map);
            routeId = (Integer) getSqlMapClientTemplate().update("operation.updateRouteCode", map);
            System.out.println("routeId = " + routeId);
            if (routeId > 0) {
                map.put("routeId", operationTO.getRouteId());
                routeCostId = operationTO.getRouteCostIds();
                vehTypeId = operationTO.getVehTypeId();
                vehMileage = operationTO.getVehMileage();
                reefMileage = operationTO.getReefMileage();
                fuelCostPerKms = operationTO.getFuelCostPerKms();
                fuelCostPerKmsEmpCon = operationTO.getFuelCostPerKmsEmpCon();
                fuelCostPerKms20ftCon = operationTO.getFuelCostPerKms20ftCon();
                fuelCostPerKms2_20ftCon = operationTO.getFuelCostPerKms2_20ftCon();
                fuelCostPerKms40ftCon = operationTO.getFuelCostPerKms40ftCon();
                fuelCostPerHrs = operationTO.getFuelCostPerHrs();
                tollAmounts = operationTO.getTollAmounts();
                miscCostKm = operationTO.getMiscCostKm();
                driverIncenKm = operationTO.getDriverIncenKm();
                varExpense = operationTO.getVarExpense();
                vehExpenseEmpCon = operationTO.getVehExpenseEmpCon();
                vehExpense20ftCon = operationTO.getVehExpense20ftCon();
                vehExpense2_20ftCon = operationTO.getVehExpense2_20ftCon();
                vehExpense40ftCon = operationTO.getVehExpense40ftCon();
                factor = operationTO.getFactor();
                vehExpense = operationTO.getVehExpense();
                referExpense = operationTO.getReeferExpense();
                totExpense = operationTO.getTotExpense();
                fuelWOReefer = operationTO.getFuelWOReefer();
                fuelWithReefer = operationTO.getFuelWithReefer();
                for (int i = 0; i < vehTypeId.length; i++) {
                    map.put("vehTypeId", vehTypeId[i]);
                    map.put("vehMileage", vehMileage[i]);
                    map.put("reefMileage", reefMileage[i]);
                    map.put("fuelCostPerKms", fuelCostPerKms[i]);
                    map.put("fuelCostPerHrs", fuelCostPerHrs[i]);
                    map.put("fuelCostPerKmsEmpCon", fuelCostPerKmsEmpCon[i]);
                    map.put("fuelCostPerKms20ftCon", fuelCostPerKms20ftCon[i]);
                    map.put("fuelCostPerKms2_20ftCon", fuelCostPerKms2_20ftCon[i]);
                    map.put("fuelCostPerKms40ftCon", fuelCostPerKms40ftCon[i]);
                    map.put("tollAmounts", tollAmounts[i]);
                    map.put("miscCostKm", miscCostKm[i]);
                    map.put("driverIncenKm", driverIncenKm[i]);
                    map.put("varExpense", "0");
                    map.put("factor", factor[i]);
                    map.put("vehExpense", vehExpense[i]);
                    map.put("vehExpenseEmpCon", "0");
                    map.put("vehExpense20ftCon", "0");
                    map.put("vehExpense2_20ftCon", "0");
                    map.put("vehExpense40ftCon", "0");
                    map.put("referExpense", "0");
                    map.put("totExpense", totExpense[i]);
                    map.put("routeCostId", routeCostId[i]);
                    map.put("fuelWOReefer", fuelWOReefer[i]);
                    map.put("fuelWithReefer", fuelWithReefer[i]);
                    System.out.println("map 2= " + map);
                    if (!routeCostId[i].equals("0")) {
                        updateStatus += (Integer) getSqlMapClientTemplate().update("operation.updateRouteCost", map);
                        System.out.println("updateStatus = " + updateStatus);
                    } else {
                        insertStatus += (Integer) getSqlMapClientTemplate().update("operation.insertRouteCost", map);
                        System.out.println("updateStatus = " + updateStatus);
                    }
                }
                if (rtoIds != null && rtoIds.length > 0) {

                    insertStatus = (Integer) getSqlMapClientTemplate().delete("operation.deleteRouteRtoMapping", map);
                    for (int i = 0; i < rtoIds.length; i++) {
                        map.put("userId", userId);
                        map.put("rtoId", rtoIds[i]);
                        insertStatus = (Integer) getSqlMapClientTemplate().insert("operation.insertRtoRouteMapping", map);
                    }
                }
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateRoute Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateRoute", sqlException);
        }

        return updateStatus;
    }

    public ArrayList getRouteCode(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList routeCodeList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("routeCode", operationTO.getRouteCode() + "%");
        System.out.println("map = " + map);

        try {
            routeCodeList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getRouteCode", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRouteCode Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getRouteCode", sqlException);
        }
        return routeCodeList;
    }

    public ArrayList getCity(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList cityList = new ArrayList();
        //////System.out.println("map = " + map);
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("city", operationTO.getCity() + "%");
        map.put("cityId", operationTO.getCityId());
        //////System.out.println("map = " + map);

        try {
            cityList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCity", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCity Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCity", sqlException);
        }
        return cityList;
    }

    public ArrayList getLatitudeLongitudeList(OperationTO operationTO, int cityId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList latLonList = new ArrayList();
        //////System.out.println("map = " + map);
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("cityid", cityId);

        try {
            latLonList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getLatitudeLongitudeList", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCity Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCity", sqlException);
        }
        return latLonList;
    }

    public int saveStandardCharge(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int saveStandardCharge = 0;
        int user = userId;
        try {
            map.put("userId", user);
            map.put("name", operationTO.getStChargeName());
            map.put("description", operationTO.getStChargeDesc());
            map.put("unites", operationTO.getStChargeUnit());
            map.put("status", operationTO.getStatus());
            saveStandardCharge = (Integer) getSqlMapClientTemplate().update("operation.saveStandardCharge", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveStandardCharge Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveStandardCharge ", sqlException);
        }
        return saveStandardCharge;
    }

    /**
     * This method used to Get Unit List .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getUnitList() {
        Map map = new HashMap();
        ArrayList unitList = new ArrayList();

        try {
            unitList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getUnitList", map);
            //////System.out.println(" unitList =" + unitList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUnitList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getUnitList", sqlException);
        }
        return unitList;
    }

    /**
     * This method used to Get standardCharge .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getStandardChargeList() {
        Map map = new HashMap();
        ArrayList standardChargeList = new ArrayList();

        try {
            //////System.out.println("this is standard charges");
            standardChargeList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getStandardChargeList", map);
            //////System.out.println(" StandardChargeList =" + standardChargeList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getStandardChargeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getStandardChargeList", sqlException);
        }
        return standardChargeList;
    }

    /**
     * This method used to Modify standardCharge .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int updateStandardCharge(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int user = userId;

        map.put("name", operationTO.getStChargeName());
        map.put("description", operationTO.getStChargeDesc());
        map.put("unites", operationTO.getStChargeUnit());
        map.put("stChargeId", operationTO.getStChargeId());
        map.put("status", operationTO.getStatus());

        try {

            status = (Integer) getSqlMapClientTemplate().update("operation.updateStandardCharge", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateJobCardScheduleDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStandardCharge", sqlException);
        }

        return status;
    }

    /**
     * This method used to Get Vehicle Mfr Model List .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getVehicleMfrModelList() {
        Map map = new HashMap();
        ArrayList vehicleMfrList = new ArrayList();

        try {
            vehicleMfrList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getVehicleMfrModelList", map);
            //////System.out.println(" vehicleMfrList =" + vehicleMfrList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleMfrModelList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleMfrModelList", sqlException);
        }
        return vehicleMfrList;
    }

    /**
     * This method used to Get Vehicle Type List .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getVehicleTypeList() {
        Map map = new HashMap();
        ArrayList vehicleTypeList = new ArrayList();

        try {
            vehicleTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getVehicleTypeList", map);
            //////System.out.println(" vehicleTypeList =" + vehicleTypeList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleTypeList", sqlException);
        }
        return vehicleTypeList;
    }

    public ArrayList getHubList() {
        Map map = new HashMap();
        ArrayList hubList = new ArrayList();

        try {
            hubList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getHubList", map);
            //////System.out.println(" hubList =" + hubList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("gethubList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "gethubList", sqlException);
        }
        return hubList;
    }

    public ArrayList getVehicleTypeForActualKM(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList vehicleTypeList = new ArrayList();
        map.put("contractId", operationTO.getContractId());
        try {
            vehicleTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getVehicleTypeForActualKM", map);
            //////System.out.println(" vehicleTypeList =" + vehicleTypeList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleTypeForActualKM Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleTypeForActualKM", sqlException);
        }
        return vehicleTypeList;
    }

    /**
     * This method used to Get Billing Type List .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getBillingTypeList() {
        Map map = new HashMap();
        ArrayList billingTypeList = new ArrayList();

        try {
            billingTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getBillingTypeList", map);
            //////System.out.println(" billingTypeList =" + billingTypeList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBillingTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBillingTypeList", sqlException);
        }
        return billingTypeList;
    }

    public ArrayList getPickupPoint(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList pickupPoint = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("routeFrom", operationTO.getElementValue() + "%");
        //        //////System.out.println("password " + loginTO.getPassword());
        //////System.out.println("map = " + map);

        try {
            pickupPoint = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getPickupPoint", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPickupPoint Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getPickupPoint", sqlException);
        }
        return pickupPoint;
    }

    public ArrayList getInterimPoint(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList interimPoint = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("cityId", operationTO.getCityId());
        map.put("interimPoint", operationTO.getElementValue() + "%");
        if (operationTO.getRouteFrom().contains(",")) {
            String[] routeFrom = operationTO.getRouteFrom().split(",");
            List routeFromId = new ArrayList(routeFrom.length);
            for (int i = 0; i < routeFrom.length; i++) {
                //////System.out.println("value:" + routeFrom[i]);
                routeFromId.add(routeFrom[i]);
            }
            map.put("routeFromId", routeFromId);
        } else {
            map.put("routeFromId", operationTO.getRouteFrom());
        }
        //////System.out.println("map = " + map);

        try {
            interimPoint = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getInterimPoint", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInterimPoint Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getInterimPoint", sqlException);
        }
        return interimPoint;
    }

    /**
     * This method used to Customer Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getCustomerDetails(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList customerDetails = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("customerName", operationTO.getCustomerName() + "%");
        map.put("customerCode", operationTO.getCustomerCode() + "%");
        map.put("userId", operationTO.getUserId());
        map.put("roleId", operationTO.getRoleId());

        String empId = (String) getSqlMapClientTemplate().queryForObject("customer.getEmployeeId", map);
        map.put("empId", empId);

        //////System.out.println("map = " + map);
        try {
            if (operationTO.getCustomerName() != null) {
                customerDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCustomerNameDetails", map);
            } else if (operationTO.getCustomerCode() != null) {
                customerDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCustomerCodeDetails", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCustomerDetails", sqlException);
        }
        return customerDetails;
    }

    /**
     * This method used to Insert Customer Contract.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertCustomerContractDetails(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int insertStatus = 0;
        int index = 0;
        int stChargeId = 0;
        int stChargeAmount = 0;
        int stChargeDate = 0;
        map.put("userId", userId);
        map.put("contractNo", operationTO.getContractNo());
        map.put("customerId", operationTO.getCustomerId());
        map.put("contractFrom", operationTO.getContractFrom());
        map.put("contractTo", operationTO.getContractTo());
        map.put("billingTypeId", operationTO.getBillingTypeId());
        map.put("companyId", operationTO.getCompanyID1());
        //////System.out.println("map = " + map);
        try {
            insertStatus = (Integer) getSqlMapClientTemplate().insert("operation.insertCustomerContract", map);
            //////System.out.println("insertStatus = " + insertStatus);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertCustomerContract Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertCustomerContract", sqlException);
        }

        return insertStatus;
    }

    /**
     * This method used to Insert Contract Standard Charge Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertContractStandardChargeDetails(OperationTO operationTO, String[] stChargeId, String[] stChargeAmount, String[] stChargeDate, String[] stChargeSelect, int userId) {
        Map map = new HashMap();
        int insertStandardCharge = 0;
        int index = 0;
        map.put("userId", userId);
        map.put("contractId", operationTO.getContractId());
        //////System.out.println("stChargeId.length = " + stChargeId.length);
        String[] selectStChargeId = new String[stChargeId.length];
        String[] selectStChargeAmount = new String[stChargeAmount.length];
        String[] selectStChargeDate = new String[stChargeDate.length];
        try {
            for (int i = 0; i < stChargeId.length; i++) {
                for (int j = 0; j < stChargeSelect.length; j++) {
                    index = Integer.parseInt(stChargeSelect[j]);
                    selectStChargeId[index] = stChargeId[index];
                    selectStChargeAmount[index] = stChargeAmount[index];
                    selectStChargeDate[index] = stChargeDate[index];
                }
            }

            for (int i = 0; i < selectStChargeId.length; i++) {
//                //////System.out.println("selectStChargeId[i] = " + selectStChargeId[i]);
//                //////System.out.println("selectStChargeAmount[i] = " + selectStChargeAmount[i]);
//                //////System.out.println("selectStChargeDate[i] = " + selectStChargeDate[i]);
                if (selectStChargeId[i] != null && selectStChargeAmount[i] != null && selectStChargeDate[i] != null) {
                    map.put("stChargeId", selectStChargeId[i]);
                    map.put("stChargeAmount", selectStChargeAmount[i]);
                    map.put("stChargeDate", selectStChargeDate[i]);
                    insertStandardCharge = (Integer) getSqlMapClientTemplate().update("operation.insertContractStandardCharge", map);
                }
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractStandardCharge Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertContractStandardCharge", sqlException);
        }

        return insertStandardCharge;
    }

    /**
     * This method used to Insert Contract PTP Billing Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String[] insertRouteContractPTPBillingDetails(OperationTO operationTO, String[] ptpRouteCode, String[] ptpVehicleTypeId, String[] ptpPickupPoint, String[] ptpInterimPoint1, String[] ptpInterimPoint2, String[] ptpInterimPoint3, String[] ptpInterimPoint4, String[] ptpDropPoint, String[] ptpPickupPointId, String[] ptpInterimPointId1, String[] ptpInterimPointId2, String[] ptpInterimPointId3, String[] ptpInterimPointId4, String[] ptpDropPointId, String[] ptpInterimPoint1Km, String[] ptpInterimPoint2Km, String[] ptpInterimPoint3Km, String[] ptpInterimPoint4Km, String[] ptpDropPointKm, String[] interimPoint1Hrs, String[] interimPoint2Hrs, String[] interimPoint3Hrs, String[] interimPoint4Hrs, String[] ptpDropPointHrs, String[] interimPoint1Minutes, String[] interimPoint2Minutes, String[] interimPoint3Minutes, String[] interimPoint4Minutes, String[] ptpDropPointMinutes, String[] interimPoint1RouteId, String[] interimPoint2RouteId, String[] interimPoint3RouteId, String[] interimPoint4RouteId, String[] ptpDropPointRouteId, String[] ptpTotalKm, String[] ptpTotalHours, String[] ptpTotalMinutes, String[] ptpRateWithReefer, String[] ptpRateWithoutReefer, String[] loadTypeId, String[] containerTypeId, String[] containerQty, String[] ptpFromDate, String[] ptpToDate, int userId, String[] fuelVehicle, String[] fuelDg, String[] totalFuel, String[] toll, String[] driverBachat, String[] dala, String[] misc, String[] approvalFlag, int reqId, String[] marketHireVehicle, String[] marketHireWithReefer, SqlMapClient session) throws FPRuntimeException {
        Map map = new HashMap();
        int insertBillingDetails = 0;
        String contractRateIds[] = null;
        int index = 0;
        map.put("userId", userId);
        map.put("contractId", operationTO.getContractId());
        int pointCount = 0;
        int update = 0;
        try {

            for (int i = 0; i < ptpRouteCode.length; i++) {
                String[] temp = null;
                map.put("routeCode", ptpRouteCode[i]);
                if (ptpPickupPoint != null) {
                    if (ptpPickupPoint.length == 0) {
                        map.put("ptpPickupPoint", "");
                    } else if (ptpPickupPoint.length > 0) {
                        if (ptpPickupPoint[i] != null && !"".equals(ptpPickupPoint[i])) {
                            map.put("ptpPickupPoint", ptpPickupPoint[i]);
                        } else {
                            map.put("ptpPickupPoint", "");
                        }
                    }
                }
                if (loadTypeId != null) {
                    if (loadTypeId.length == 0) {
                        map.put("loadTypeId", "");
                    } else if (loadTypeId.length > 0) {
                        if (loadTypeId[i] != null && !"".equals(loadTypeId[i])) {
                            map.put("loadTypeId", loadTypeId[i]);
                        } else {
                            map.put("loadTypeId", "0");
                        }
                    }
                }
                if (containerTypeId != null) {
                    if (containerTypeId.length == 0) {
                        map.put("containerTypeId", "");
                    } else if (containerTypeId.length > 0) {
                        if (containerTypeId[i] != null && !"".equals(containerTypeId[i])) {
                            map.put("containerTypeId", containerTypeId[i]);
                        } else {
                            map.put("containerTypeId", "0");
                        }
                    }
                }
                if (containerQty != null) {
                    if (containerQty.length == 0) {
                        map.put("containerQty", "");
                    } else if (containerQty.length > 0) {
                        if (containerQty[i] != null && !"".equals(containerQty[i])) {
                            map.put("containerQty", containerQty[i]);
                        } else {
                            map.put("containerQty", "0");
                        }
                    }
                }
                if (ptpInterimPoint1 != null) {
                    if (ptpInterimPoint1.length == 0) {
                        map.put("interimPoint1", "");
                    } else if (ptpInterimPoint1.length > 0) {
                        if (ptpInterimPoint1[i] != null && !"".equals(ptpInterimPoint1[i])) {
                            map.put("interimPoint1", ptpInterimPoint1[i]);
                        } else {
                            map.put("interimPoint1", "");
                        }
                    }
                }
                if (ptpInterimPoint2 != null) {
                    if (ptpInterimPoint2.length == 0) {
                        map.put("interimPoint2", "");
                    } else if (ptpInterimPoint2.length > 0) {
                        if (ptpInterimPoint2[i] != null && !"".equals(ptpInterimPoint2[i])) {
                            map.put("interimPoint2", ptpInterimPoint2[i]);
                        } else {
                            map.put("interimPoint2", "");
                        }
                    }
                }

                if (ptpInterimPoint3 != null) {
                    if (ptpInterimPoint3.length == 0) {
                        map.put("interimPoint3", "");
                    } else if (ptpInterimPoint3.length > 0) {
                        if (ptpInterimPoint3[i] != null && !"".equals(ptpInterimPoint3[i])) {
                            map.put("interimPoint3", ptpInterimPoint3[i]);
                        } else {
                            map.put("interimPoint3", ptpInterimPoint3[i]);
                        }
                    }
                }
                if (ptpInterimPoint4 != null) {
                    if (ptpInterimPoint4.length == 0) {
                        map.put("interimPoint4", "");
                    } else if (ptpInterimPoint4.length > 0) {
                        if (ptpInterimPoint4[i] != null && !"".equals(ptpInterimPoint4[i])) {
                            map.put("interimPoint4", ptpInterimPoint4[i]);
                        } else {
                            map.put("interimPoint4", "");
                        }
                    }
                }
                if (ptpDropPoint != null) {
                    if (ptpDropPoint.length == 0) {
                        map.put("ptpDropPoint", "");
                    } else if (ptpDropPoint.length > 0) {
                        if (ptpDropPoint[i] != null && !"".equals(ptpDropPoint[i])) {
                            map.put("ptpDropPoint", ptpDropPoint[i]);
                        } else {
                            map.put("ptpDropPoint", "");
                        }
                    }
                }

                if (ptpPickupPointId != null) {
                    if (ptpPickupPointId.length == 0) {
                        map.put("ptpPickupPointId", 0);
                    } else if (ptpPickupPointId.length > 0) {
                        if (ptpPickupPointId[i] != null && !"".equals(ptpPickupPointId[i])) {
                            map.put("ptpPickupPointId", Integer.parseInt(ptpPickupPointId[i]));
                        } else {
                            map.put("ptpPickupPointId", 0);
                        }
                    }
                }
                if (ptpInterimPointId1 != null) {
                    if (ptpInterimPointId1.length == 0) {
                        map.put("interimPointId1", 0);
                    } else if (ptpInterimPointId1.length > 0) {
                        if (ptpInterimPointId1[i] != null && !"".equals(ptpInterimPointId1[i])) {
                            map.put("interimPointId1", Integer.parseInt(ptpInterimPointId1[i]));
                        } else {
                            map.put("interimPointId1", 0);
                        }
                    }
                }
                if (ptpInterimPointId2 != null) {
                    if (ptpInterimPointId2.length == 0) {
                        map.put("interimPointId2", 0);
                    } else if (ptpInterimPointId2.length > 0) {
                        if (ptpInterimPointId2[i] != null && !"".equals(ptpInterimPointId2[i])) {
                            map.put("interimPointId2", Integer.parseInt(ptpInterimPointId2[i]));
                        } else {
                            map.put("interimPointId2", 0);
                        }
                    }
                }
                if (ptpInterimPointId3 != null) {
                    if (ptpInterimPointId3.length == 0) {
                        map.put("interimPointId3", 0);
                    } else if (ptpInterimPointId3.length > 0) {
                        if (ptpInterimPointId3[i] != null && !"".equals(ptpInterimPointId3[i])) {
                            map.put("interimPointId3", Integer.parseInt(ptpInterimPointId3[i]));
                        } else {
                            map.put("interimPointId3", 0);
                        }
                    }
                }
                if (ptpInterimPointId4 != null) {
                    if (ptpInterimPointId4.length == 0) {
                        map.put("interimPointId4", 0);
                    } else if (ptpInterimPointId4.length > 0) {
                        if (ptpInterimPointId4[i] != null && !"".equals(ptpInterimPointId4[i])) {
                            map.put("interimPointId4", Integer.parseInt(ptpInterimPointId4[i]));
                        } else {
                            map.put("interimPointId4", 0);
                        }
                    }
                }
                if (ptpDropPointId != null) {
                    if (ptpDropPointId.length == 0) {
                        map.put("ptpDropPointId", 0);
                    } else if (ptpDropPointId.length > 0) {
                        if (ptpDropPointId[i] != null && !"".equals(ptpDropPointId[i])) {
                            map.put("ptpDropPointId", Integer.parseInt(ptpDropPointId[i]));
                        } else {
                            map.put("ptpDropPointId", 0);
                        }
                    }
                }

                if (ptpInterimPoint1Km != null) {
                    if (ptpInterimPoint1Km.length == 0) {
                        map.put("interimPoint1Km", 0.0f);
                    } else if (ptpInterimPoint1Km.length > 0) {
                        if (ptpInterimPoint1Km[i] != null && !"".equals(ptpInterimPoint1Km[i])) {
                            map.put("interimPoint1Km", Double.parseDouble(ptpInterimPoint1Km[i]));
                        } else {
                            map.put("interimPoint1Km", 0.0f);
                        }
                    }
                }
                if (ptpInterimPoint2Km != null) {
                    if (ptpInterimPoint2Km.length == 0) {
                        map.put("interimPoint2Km", 0.0f);
                    } else if (ptpInterimPoint2Km.length > 0) {
                        if (ptpInterimPoint2Km[i] != null && !"".equals(ptpInterimPoint2Km[i])) {
                            map.put("interimPoint2Km", Double.parseDouble(ptpInterimPoint2Km[i]));
                        } else {
                            map.put("interimPoint2Km", 0.0f);
                        }
                    }
                }
                if (ptpInterimPoint3Km != null) {
                    if (ptpInterimPoint3Km.length == 0) {
                        map.put("interimPoint3Km", 0.0f);
                    } else if (ptpInterimPoint3Km.length > 0) {
                        if (ptpInterimPoint3Km[i] != null && !"".equals(ptpInterimPoint3Km[i])) {
                            map.put("interimPoint3Km", Double.parseDouble(ptpInterimPoint3Km[i]));
                        } else {
                            map.put("interimPoint3Km", 0.0f);
                        }
                    }
                }
                if (ptpInterimPoint4Km != null) {
                    if (ptpInterimPoint4Km.length == 0) {
                        map.put("interimPoint4Km", 0.0f);
                    } else if (ptpInterimPoint4Km.length > 0) {
                        if (ptpInterimPoint4Km[i] != null && !"".equals(ptpInterimPoint4Km[i])) {
                            map.put("interimPoint4Km", Double.parseDouble(ptpInterimPoint4Km[i]));
                        } else {
                            map.put("interimPoint4Km", 0.0f);
                        }
                    }
                }
                if (ptpDropPointKm != null) {
                    if (ptpDropPointKm.length == 0) {
                        map.put("ptpDropPointKm", 0.0f);
                    } else if (ptpDropPointKm.length > 0) {
                        if (ptpDropPointKm[i] != null && !"".equals(ptpDropPointKm[i])) {
                            map.put("ptpDropPointKm", Double.parseDouble(ptpDropPointKm[i]));
                        } else {
                            map.put("ptpDropPointKm", 0.0f);
                        }
                    }
                }

                if (interimPoint1Hrs != null) {
                    if (interimPoint1Hrs.length == 0) {
                        map.put("interimPoint1Hrs", 0.0f);
                    } else if (interimPoint1Hrs.length > 0) {
                        if (interimPoint1Hrs[i] != null && !"".equals(interimPoint1Hrs[i])) {
                            map.put("interimPoint1Hrs", Double.parseDouble(interimPoint1Hrs[i]));
                        } else {
                            map.put("interimPoint1Hrs", 0.0f);
                        }
                    }
                }

                if (interimPoint2Hrs != null) {
                    if (interimPoint2Hrs.length == 0) {
                        map.put("interimPoint2Hrs", 0);
                    } else if (interimPoint2Hrs.length > 0) {
                        if (interimPoint2Hrs[i] != null && !"".equals(interimPoint2Hrs[i])) {
                            map.put("interimPoint2Hrs", interimPoint2Hrs[i]);
                        } else {
                            map.put("interimPoint2Hrs", 0);
                        }
                    }
                }

                if (interimPoint3Hrs != null) {
                    if (interimPoint3Hrs.length == 0) {
                        map.put("interimPoint3Hrs", 0);
                    } else if (interimPoint3Hrs.length > 0) {
                        if (interimPoint3Hrs[i] != null && !"".equals(interimPoint3Hrs[i])) {
                            map.put("interimPoint3Hrs", interimPoint3Hrs[i]);
                        } else {
                            map.put("interimPoint3Hrs", 0);
                        }
                    }
                }
                if (interimPoint4Hrs != null) {
                    if (interimPoint4Hrs.length == 0) {
                        map.put("interimPoint4Hrs", 0);
                    } else if (interimPoint4Hrs.length > 0) {
                        if (interimPoint4Hrs[i] != null && !"".equals(interimPoint4Hrs[i])) {
                            map.put("interimPoint4Hrs", interimPoint4Hrs[i]);
                        } else {
                            map.put("interimPoint4Hrs", 0);
                        }
                    }
                }
                if (ptpDropPointHrs != null) {
                    if (ptpDropPointHrs.length == 0) {
                        map.put("ptpDropPointHrs", 0);
                    } else if (ptpDropPointHrs.length > 0) {
                        if (ptpDropPointHrs[i] != null && !"".equals(ptpDropPointHrs[i])) {
                            map.put("ptpDropPointHrs", ptpDropPointHrs[i]);
                        } else {
                            map.put("ptpDropPointHrs", 0);
                        }
                    }
                }

                if (interimPoint1Minutes != null) {
                    if (interimPoint1Minutes.length == 0) {
                        map.put("interimPoint1Minutes", 0);
                    } else if (interimPoint1Minutes.length > 0) {
                        if (interimPoint1Minutes[i] != null && !"".equals(interimPoint1Minutes[i])) {
                            map.put("interimPoint1Minutes", interimPoint1Minutes[i]);
                        } else {
                            map.put("interimPoint1Minutes", 0);
                        }
                    }
                }
                if (interimPoint2Minutes != null) {
                    if (interimPoint2Minutes.length == 0) {
                        map.put("interimPoint2Minutes", 0);
                    } else if (interimPoint2Minutes.length > 0) {
                        if (interimPoint2Minutes[i] != null && !"".equals(interimPoint2Minutes[i])) {
                            map.put("interimPoint2Minutes", interimPoint2Minutes[i]);
                        } else {
                            map.put("interimPoint2Minutes", 0);
                        }
                    }
                }
                if (interimPoint3Minutes != null) {
                    if (interimPoint3Minutes.length == 0) {
                        map.put("interimPoint3Minutes", 0);
                    } else if (interimPoint3Minutes.length > 0) {
                        if (interimPoint3Minutes[i] != null && !"".equals(interimPoint3Minutes[i])) {
                            map.put("interimPoint3Minutes", interimPoint3Minutes[i]);
                        } else {
                            map.put("interimPoint3Minutes", 0);
                        }
                    }
                }
                if (interimPoint4Minutes != null) {
                    if (interimPoint4Minutes.length == 0) {
                        map.put("interimPoint4Minutes", 0);
                    } else if (interimPoint4Minutes.length > 0) {
                        if (interimPoint4Minutes[i] != null && !"".equals(interimPoint4Minutes[i])) {
                            map.put("interimPoint4Minutes", interimPoint4Minutes[i]);
                        } else {
                            map.put("interimPoint4Minutes", 0);
                        }
                    }
                }
                if (ptpDropPointMinutes != null) {
                    if (ptpDropPointMinutes.length == 0) {
                        map.put("ptpDropPointMinutes", 0);
                    } else if (ptpDropPointMinutes.length > 0) {
                        if (ptpDropPointMinutes[i] != null && !"".equals(ptpDropPointMinutes[i])) {
                            map.put("ptpDropPointMinutes", ptpDropPointMinutes[i]);
                        } else {
                            map.put("ptpDropPointMinutes", 0);
                        }
                    }
                }

                if (interimPoint1RouteId != null) {
                    if (interimPoint1RouteId.length == 0) {
                        map.put("interimPoint1RouteId", 0);
                    } else if (interimPoint1RouteId.length > 0) {
                        if (interimPoint1RouteId[i] != null && !"".equals(interimPoint1RouteId[i])) {
                            map.put("interimPoint1RouteId", interimPoint1RouteId[i]);
                            pointCount++;
                        } else {
                            map.put("interimPoint1RouteId", 0);
                        }
                    }
                }
                if (interimPoint2RouteId != null) {
                    if (interimPoint2RouteId.length == 0) {
                        map.put("interimPoint2RouteId", 0);
                    } else if (interimPoint2RouteId.length > 0) {
                        if (interimPoint2RouteId[i] != null && !"".equals(interimPoint2RouteId[i])) {
                            map.put("interimPoint2RouteId", interimPoint2RouteId[i]);
                            pointCount++;
                        } else {
                            map.put("interimPoint2RouteId", 0);
                        }
                    }
                }
                if (interimPoint3RouteId != null) {
                    if (interimPoint3RouteId.length == 0) {
                        map.put("interimPoint3RouteId", 0);
                    } else if (interimPoint3RouteId.length > 0) {
                        if (interimPoint3RouteId[i] != null && !"".equals(interimPoint3RouteId[i])) {
                            map.put("interimPoint3RouteId", interimPoint3RouteId[i]);
                            pointCount++;
                        } else {
                            map.put("interimPoint3RouteId", 0);
                        }
                    }
                }
                if (interimPoint4RouteId != null) {
                    if (interimPoint4RouteId.length == 0) {
                        map.put("interimPoint4RouteId", 0);
                    } else if (interimPoint4RouteId.length > 0) {
                        if (interimPoint4RouteId[i] != null && !"".equals(interimPoint4RouteId[i])) {
                            map.put("interimPoint4RouteId", interimPoint4RouteId[i]);
                            pointCount++;
                        } else {
                            map.put("interimPoint4RouteId", 0);
                        }
                    }
                }
                if (ptpDropPointRouteId != null) {
                    if (ptpDropPointRouteId.length == 0) {
                        map.put("ptpDropPointRouteId", 0);
                    } else if (ptpDropPointRouteId.length > 0) {
                        if (ptpDropPointRouteId[i] != null && !"".equals(ptpDropPointRouteId[i])) {
                            map.put("ptpDropPointRouteId", ptpDropPointRouteId[i]);
                            pointCount++;
                        } else {
                            map.put("ptpDropPointRouteId", 0);
                        }
                    }
                }
                if (ptpTotalKm != null) {
                    if (ptpTotalKm.length == 0) {
                        map.put("ptpTotalKm", 0.0f);
                    } else if (ptpTotalKm.length > 0) {
                        if (ptpTotalKm[i] != null && !"".equals(ptpTotalKm[i])) {
                            map.put("ptpTotalKm", ptpTotalKm[i]);
                        } else {
                            map.put("ptpTotalKm", 0.0f);
                        }
                    }
                }
                if (ptpTotalHours != null) {
                    if (ptpTotalHours.length == 0) {
                        map.put("ptpTotalHours", 0.0f);
                    } else if (ptpTotalHours.length > 0) {
                        if (ptpTotalHours[i] != null && !"".equals(ptpTotalHours[i])) {
                            map.put("ptpTotalHours", ptpTotalHours[i]);
                        } else {
                            map.put("ptpTotalHours", 0.0f);
                        }
                    }
                }
                if (ptpTotalMinutes != null) {
                    if (ptpTotalMinutes.length == 0) {
                        map.put("ptpTotalMinutes", 0.0f);
                    } else if (ptpTotalMinutes.length > 0) {
                        if (ptpTotalMinutes[i] != null && !"".equals(ptpTotalMinutes[i])) {
                            map.put("ptpTotalMinutes", ptpTotalMinutes[i]);
                        } else {
                            map.put("ptpTotalMinutes", 0.0f);
                        }
                    }
                }
                Date today = new Date();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                if (ptpFromDate != null) {
                    if (ptpFromDate.length == 0) {
                        map.put("ptpFromDate", df.format(today));
                    } else if (ptpFromDate.length > 0) {
                        if (ptpFromDate[i] != null && !"".equals(ptpFromDate[i])) {
                            map.put("ptpFromDate", ptpFromDate[i]);
                        } else {
                            map.put("ptpFromDate", df.format(today));
                        }
                    }
                }

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(today);

                calendar.add(Calendar.MONTH, 1);
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                calendar.add(Calendar.DATE, -1);

                Date lastDayOfMonth = calendar.getTime();
                if (ptpToDate != null) {
                    if (ptpToDate.length == 0) {
                        map.put("ptpToDate", df.format(lastDayOfMonth));
                    } else if (ptpToDate.length > 0) {
                        if (ptpToDate[i] != null && !"".equals(ptpToDate[i])) {
                            map.put("ptpToDate", ptpToDate[i]);
                        } else {
                            map.put("ptpToDate", df.format(lastDayOfMonth));
                        }
                    }
                }
                map.put("pointCount", pointCount);
                System.out.println("operationTO.getContractId() = " + operationTO.getContractId());
                System.out.println("map routes  in the DAO = " + map);
                insertBillingDetails = (Integer) getSqlMapClientTemplate().insert("operation.insertRouteContractPTPBillingDetails", map);
                if (insertBillingDetails > 0) {
                    operationTO.setRouteContractId(insertBillingDetails);
                    if (operationTO.getBillingTypeId().equals("4")) {
                        contractRateIds[i] = insertPTPContractRates(operationTO, ptpVehicleTypeId[i], "0", "0", userId, "0", "0", "0", "0", "0", "0", "0", approvalFlag[i], reqId, "0", "0", session);
                    } else {
                        contractRateIds[i] = insertPTPContractRates(operationTO, ptpVehicleTypeId[i], ptpRateWithReefer[i], ptpRateWithoutReefer[i], userId, fuelVehicle[i], fuelDg[i], totalFuel[i], toll[i], driverBachat[i], dala[i], misc[i], approvalFlag[i], reqId, marketHireVehicle[i], marketHireWithReefer[i], session);
                    }
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractStandardCharge Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertContractStandardCharge", sqlException);
        }

        return contractRateIds;
    }

    /**
     * This method used to Insert Contract PTPW Billing Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertRouteContractPTPWBillingDetails(OperationTO operationTO, String[] ptpwRouteContractCode, String[] ptpwVehicleTypeId, String[] ptpwPickupPointId, String[] ptpwPickupPoint, String[] ptpwDropPoint, String[] ptpwDropPointId, String[] ptpwPointRouteId, String[] ptpwTotalKm, String[] ptpwTotalHrs, String[] ptpwTotalMinutes, String[] ptpwRateWithReefer, String[] ptpwRateWithoutReefer, String[] loadTypeId, String[] containerTypeId, String[] containerQty, int userId) {
        Map map = new HashMap();
        int insertBillingDetails = 0;
        int index = 0;
        map.put("userId", userId);
        map.put("contractId", operationTO.getContractId());
        int pointCount = 2;
        int update = 0;
        try {
            try {
                for (int i = 0; i < ptpwRouteContractCode.length; i++) {
                    String[] temp = null;
                    map.put("routeCode", ptpwRouteContractCode[i]);
                    map.put("loadTypeId", loadTypeId[i]);
                    map.put("containerTypeId", containerTypeId[i]);
                    map.put("containerQty", containerQty[i]);
                    map.put("ptpwPickupPointId", ptpwPickupPointId[i]);
                    map.put("ptpwPickupPoint", ptpwPickupPoint[i]);
                    map.put("ptpwDropPoint", ptpwDropPoint[i]);
                    map.put("ptpwDropPointId", ptpwDropPointId[i]);
                    map.put("ptpwPointRouteId", ptpwPointRouteId[i]);
                    if (ptpwPickupPointId[i] != null && !"".equals(ptpwPickupPointId[i])) {
                        map.put("ptpwPickupPointId", Integer.parseInt(ptpwPickupPointId[i]));
                    } else {
                        map.put("ptpwPickupPointId", 0);
                    }
                    if (ptpwTotalKm[i] != null && !"".equals(ptpwTotalKm[i])) {
                        map.put("ptpwDropPointKm", Double.parseDouble(ptpwTotalKm[i]));
                        map.put("ptpwTotalKm", Double.parseDouble(ptpwTotalKm[i]));
                    } else {
                        map.put("ptpwDropPointKm", 0.0f);
                        map.put("ptpwTotalKm", 0.0f);
                    }
                    if (ptpwTotalHrs[i] != null && !"".equals(ptpwTotalHrs[i])) {
                        map.put("ptpwTotalHrs", Integer.parseInt(ptpwTotalHrs[i]));
                    } else {
                        map.put("ptpwTotalHrs", 0);
                    }
                    if (ptpwTotalMinutes[i] != null && !"".equals(ptpwTotalMinutes[i])) {
                        map.put("ptpwTotalMinutes", Integer.parseInt(ptpwTotalMinutes[i]));
                    } else {
                        map.put("ptpwTotalMinutes", 0);
                    }

                    map.put("pointCount", pointCount);
                    System.out.println("map routes in the dao = " + map);
                    insertBillingDetails = (Integer) getSqlMapClientTemplate().insert("operation.insertRouteContractPTPWBillingDetails", map);
                    if (insertBillingDetails > 0) {
                        operationTO.setRouteContractId(insertBillingDetails);
                        update = insertPTPWContractRates(operationTO, ptpwVehicleTypeId[i], ptpwRateWithReefer[i], ptpwRateWithoutReefer[i], userId);
                    }
                }
            } catch (Exception ex) {
                Writer writer = new StringWriter();
                PrintWriter printWriter = new PrintWriter(writer);
                ex.printStackTrace(printWriter);
                String s = writer.toString();
                System.out.println("s = " + s);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractPTPWBillingDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertContractPTPWBillingDetails", sqlException);
        }

        return update;
    }

    /**
     * This method used to Insert PTP Contract Rates.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String insertPTPContractRates(OperationTO operationTO, String ptpVehicleTypeId, String ptpRateWithReefer, String ptpRateWithoutReefer, int userId, String fuelVehicle, String fuelDg, String totalFuel, String toll, String driverBachat, String dala, String misc, String approvalFlag, int reqId, String marketHireVehicle, String marketHireWithReefer, SqlMapClient session) {
        Map map = new HashMap();
        int insertBillingDetails = 0;
        String contractRateIds = "";
        int index = 0;
        map.put("userId", userId);
        map.put("contractId", operationTO.getContractId());
        map.put("routeContractId", operationTO.getRouteContractId());
        map.put("approvalFlag", approvalFlag);
        map.put("reqId", reqId);

        try {

            map.put("vehicleTypeId", ptpVehicleTypeId);
            if (marketHireVehicle != null && !"".equals(marketHireVehicle)) {
                map.put("marketHireVehicle", Double.parseDouble(marketHireVehicle));
            } else {
                map.put("marketHireVehicle", 0.0f);
            }
            if (marketHireWithReefer != null && !"".equals(marketHireWithReefer)) {
                map.put("marketHireWithReefer", Double.parseDouble(marketHireWithReefer));
            } else {
                map.put("marketHireWithReefer", 0.0f);
            }
            if (ptpRateWithReefer != null && !"".equals(ptpRateWithReefer)) {
                map.put("rateWithReefer", Double.parseDouble(ptpRateWithReefer));
            } else {
                map.put("rateWithReefer", 0.0f);
            }
            if (ptpRateWithoutReefer != null && !"".equals(ptpRateWithoutReefer)) {
                map.put("rateWithoutReefer", Double.parseDouble(ptpRateWithoutReefer));
            } else {
                map.put("rateWithoutReefer", 0.0f);
            }
            map.put("fuelVehicle", fuelVehicle.equals("") || fuelVehicle == null ? 0 : fuelVehicle);
            map.put("fuelDg", fuelDg.equals("") || fuelDg == null ? 0 : fuelDg);
            map.put("totalFuel", totalFuel.equals("") || totalFuel == null ? 0 : totalFuel);
            map.put("toll", toll.equals("") || toll == null ? 0 : toll);
            map.put("driverBachat", driverBachat.equals("") || driverBachat == null ? 0 : driverBachat);
            map.put("dala", dala.equals("") || dala == null ? 0 : dala);
            map.put("misc", misc.equals("") || misc == null ? 0 : misc);

            System.out.println("map Contract Rates In the DAO= " + map);
            contractRateIds = (String) session.insert("operation.insertPTPContractRates", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractPTPWBillingDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertContractPTPWBillingDetails", sqlException);
        }

        return contractRateIds;
    }

    public int insertFixedContractRates(OperationTO operationTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int insertBillingDetails = 0;
        int index = 0;
        map.put("userId", userId);
        map.put("contractId", operationTO.getContractId());
        map.put("routeContractId", operationTO.getRouteContractId());
        try {
            for (int i = 0; i < operationTO.getFixedKmvehicleTypeId().length; i++) {
                map.put("vehicleTypeId", operationTO.getFixedKmvehicleTypeId()[i]);
                map.put("vehicleNos", operationTO.getVehicleNos()[i]);
                map.put("totalKm", operationTO.getFixedTotalKm()[i]);
                map.put("totalHm", operationTO.getFixedTotalHm()[i]);
                map.put("fixedRateWithReefer", operationTO.getFixedRateWithReefer()[i]);
                map.put("fixedExtraKmRateWithReefer", operationTO.getFixedExtraKmRateWithReefer()[i]);
                map.put("fixedExtraHmRateWithReefer", operationTO.getFixedExtraHmRateWithReefer()[i]);
                map.put("fixedRateWithoutReefer", operationTO.getFixedRateWithoutReefer()[i]);
                map.put("fixedExtraKmRateWithoutReefer", operationTO.getFixedExtraKmRateWithoutReefer()[i]);
                map.put("status", operationTO.getFixedKmStatus()[i]);
                if (operationTO.getEditId()[i].equals("0")) {
                    //////System.out.println("map Contract Rates In the DAO insert = " + map);
                    insertBillingDetails = (Integer) session.update("operation.insertFixedContractRates", map);
                } else {
                    //////System.out.println("map Contract Rates In the DAO update = " + map);
                    map.put("editId", operationTO.getEditId()[i]);
                    insertBillingDetails = (Integer) session.update("operation.updateFixedContractRates", map);
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractPTPWBillingDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertContractPTPWBillingDetails", sqlException);
        }

        return insertBillingDetails;
    }

    /**
     * This method used to Insert PTPW Contract Rates.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertPTPWContractRates(OperationTO operationTO, String ptpwVehicleTypeId, String ptpwRateWithReefer, String ptpwRateWithoutReefer, int userId) {
        Map map = new HashMap();
        int insertBillingDetails = 0;
        int index = 0;
        map.put("userId", userId);
        map.put("contractId", operationTO.getContractId());
        map.put("routeContractId", operationTO.getRouteContractId());
        try {

            map.put("vehicleTypeId", ptpwVehicleTypeId);
            if (ptpwRateWithReefer != null && !"".equals(ptpwRateWithReefer)) {
                map.put("rateWithReefer", Double.parseDouble(ptpwRateWithReefer));
            } else {
                map.put("rateWithReefer", 0.0f);
            }
            if (ptpwRateWithoutReefer != null && !"".equals(ptpwRateWithoutReefer)) {
                map.put("rateWithoutReefer", Double.parseDouble(ptpwRateWithoutReefer));
            } else {
                map.put("rateWithoutReefer", 0.0f);
            }

            //////System.out.println("map = " + map);
            insertBillingDetails = (Integer) getSqlMapClientTemplate().update("operation.insertPTPWContractRates", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractPTPWBillingDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertContractPTPWBillingDetails", sqlException);
        }

        return insertBillingDetails;
    }

    /**
     * This method used to Insert Actual Km Billing Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertActualKmBillingDetails(OperationTO operationTO, String[] actualKmVehicleTypeId, String[] actualVehicleRsPerKm, String[] actualReeferRsPerHour, String[] actualKmSelect, int userId, String[] approvalFlag, int reqId) {
        Map map = new HashMap();
        int insertBillingDetails = 0;
        int index = 0;
        map.put("userId", userId);
        map.put("contractId", operationTO.getContractId());
        map.put("reqId", reqId);
        try {
            try {
                for (int i = 0; i < actualKmVehicleTypeId.length; i++) {
                    map.put("approvalFlag", approvalFlag[i]);
                    if (actualKmVehicleTypeId[i] != null && actualVehicleRsPerKm[i] != null && actualReeferRsPerHour[i] != null) {
                        map.put("actualKmVehicleTypeId", actualKmVehicleTypeId[i]);
                        if (actualVehicleRsPerKm[i] != null && !"".equals(actualVehicleRsPerKm[i])) {
                            map.put("vehicleRate", Double.parseDouble(actualVehicleRsPerKm[i]));
                        } else {
                            map.put("vehicleRate", 0.0f);
                        }
                        if (actualReeferRsPerHour[i] != null && !"".equals(actualReeferRsPerHour[i])) {
                            map.put("reeferRate", Double.parseDouble(actualReeferRsPerHour[i]));
                        } else {
                            map.put("reeferRate", 0.0f);
                        }
                        insertBillingDetails = (Integer) getSqlMapClientTemplate().update("operation.insertActualKmBillingDetails", map);
                    }
                }

            } catch (Exception ex) {
                Writer writer = new StringWriter();
                PrintWriter printWriter = new PrintWriter(writer);
                ex.printStackTrace(printWriter);
                String s = writer.toString();
                //////System.out.println("s = " + s);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertActualKmBillingDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertActualKmBillingDetails", sqlException);
        }

        return insertBillingDetails;
    }

    /**
     * This method used to Get Customer Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String getCustomer(OperationTO operationTO) {
        Map map = new HashMap();
        String customerDetails = "";
        try {
            map.put("customerId", operationTO.getCustomerId());
            customerDetails = (String) getSqlMapClientTemplate().queryForObject("operation.getCustomer", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomer Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomer", sqlException);
        }

        return customerDetails;
    }

    public String getCompanyDetails(OperationTO operationTO) {
        Map map = new HashMap();
        String customerDetails = "";
        try {
            map.put("companyId", operationTO.getCompanyID1());
            System.out.println(" companyId map is" + map);
            customerDetails = (String) getSqlMapClientTemplate().queryForObject("operation.getCompanyDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomer Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomer", sqlException);
        }

        return customerDetails;
    }

    /**
     * This method used to Get Contract Standard Charge List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getContractStandardChargeList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList contractStandardChargeList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("customerId", operationTO.getCustomerId());
        map.put("contractId", operationTO.getContractId());
        System.out.println("charge map = " + map);

        try {
            contractStandardChargeList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getContractStandardChargeList", map);
            System.out.println("contractStandardChargeList = " + contractStandardChargeList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractStandardChargeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getContractStandardChargeList", sqlException);
        }
        return contractStandardChargeList;
    }

    /**
     * This method used to Get Customer Contract Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String getCustomerContractDetails(OperationTO operationTO) {
        Map map = new HashMap();
        String customerContractDetails = "";
        try {
            map.put("customerId", operationTO.getCustomerId());
            customerContractDetails = (String) getSqlMapClientTemplate().queryForObject("operation.getCustomerContractDetails", map);
            System.out.println("customerContractDetails = " + customerContractDetails);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerContractDetails", sqlException);
        }

        return customerContractDetails;
    }

    /**
     * This method used to Get Contract Billing List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getContractBillingList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList contractBillingList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("contractId", operationTO.getContractId());
        map.put("routeContractId", operationTO.getRouteContractId());
        //////System.out.println("operationTO.getBillingTypeId() = " + operationTO.getBillingTypeId());
        System.out.println("map = " + map);

        try {
            if (operationTO.getBillingTypeId().equals("1")) {
                contractBillingList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getPTPContractBillingList", map);

            } else if (operationTO.getBillingTypeId().equals("2")) {
                contractBillingList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getPTPWContractBillingList", map);
            } else if (operationTO.getBillingTypeId().equals("3")) {
                contractBillingList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getActualKmContractBillingList", map);
            } else if (operationTO.getBillingTypeId().equals("4")) {
                contractBillingList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getFixedContractBillingList", map);
            }
            //////System.out.println("contractBillingList.size() = " + contractBillingList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPTPContractBillingList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getPTPContractBillingList", sqlException);
        }
        return contractBillingList;
    }

    public ArrayList getPTPInactiveContractBillingList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList contractBillingList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("contractId", operationTO.getContractId());
        //////System.out.println("operationTO.getBillingTypeId() = " + operationTO.getBillingTypeId());
        System.out.println("map = " + map);

        try {
            if (operationTO.getBillingTypeId().equals("1")) {
                contractBillingList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getPTPInactiveContractBillingList", map);

            }
            System.out.println("getPTPInactiveContractBillingList.size() = " + contractBillingList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPTPContractBillingList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getPTPContractBillingList", sqlException);
        }
        return contractBillingList;
    }

    public ArrayList getContractFixedRateList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList contractFixedRateList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("contractId", operationTO.getContractId());
        //////System.out.println("operationTO.getBillingTypeId() = " + operationTO.getBillingTypeId());
        //////System.out.println("map = " + map);

        try {
            if (operationTO.getBillingTypeId().equals("4")) {
                contractFixedRateList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getContractFixedRateList", map);
            }
            //////System.out.println("contractFixedRateList.size() = " + contractFixedRateList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractFixedRateList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getContractFixedRateList", sqlException);
        }
        return contractFixedRateList;
    }

    /**
     * This method used to Edit Contract Standard Charge List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList editContractStandardChargeList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList editContractStandardChargeList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("customerId", operationTO.getCustomerId());
        map.put("contractId", operationTO.getContractId());
        //////System.out.println("map = " + map);

        try {
            editContractStandardChargeList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.editContractStandardChargeList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("editContractStandardChargeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "editContractStandardChargeList", sqlException);
        }
        return editContractStandardChargeList;
    }

    /**
     * This method used to Update Customer Contract.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int updateCustomerContractDetails(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int updateStatus = 0;
        map.put("userId", userId);
        map.put("contractId", operationTO.getContractId());
        map.put("contractTo", operationTO.getContractTo());
        map.put("billingTypeId", operationTO.getBillingTypeId());
        System.out.println(" updateCustomerContractDetails ----------- map = " + map);
        try {
            updateStatus = (Integer) getSqlMapClientTemplate().update("operation.updateCustomerContract", map);
            //////System.out.println("updateStatus = " + updateStatus);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateCustomerContract Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateCustomerContract", sqlException);
        }

        return updateStatus;
    }

    /**
     * This method used to Update Contract Standard Charge Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int updateContractStandardChargeDetails(OperationTO operationTO, String[] stChargeId, String[] stChargeAmount, String[] stChargeDate, String[] stChargeSelect, int userId) {
        Map map = new HashMap();
        int insertStandardCharge = 0;
        int updateStandardCharge = 0;
        int index = 0;
        map.put("userId", userId);
        map.put("contractId", operationTO.getContractId());
        //////System.out.println("stChargeId.length = " + stChargeId.length);
        String[] selectStChargeId = new String[stChargeId.length];
        String[] selectStChargeAmount = new String[stChargeAmount.length];
        String[] selectStChargeDate = new String[stChargeDate.length];
        try {
            for (int i = 0; i < stChargeId.length; i++) {
                for (int j = 0; j < stChargeSelect.length; j++) {
                    index = Integer.parseInt(stChargeSelect[j]);
                    selectStChargeId[index] = stChargeId[index];
                    selectStChargeAmount[index] = stChargeAmount[index];
                    selectStChargeDate[index] = stChargeDate[index];
                }
            }
            String checkStandardCharge = null;
            for (int i = 0; i < selectStChargeId.length; i++) {
                if (selectStChargeId[i] != null && selectStChargeAmount[i] != null && selectStChargeDate[i] != null) {
                    map.put("stChargeId", selectStChargeId[i]);
                    map.put("stChargeAmount", selectStChargeAmount[i]);
                    map.put("stChargeDate", selectStChargeDate[i]);
                    checkStandardCharge = (String) getSqlMapClientTemplate().queryForObject("operation.checkStandardCharge", map);
                    if (checkStandardCharge != null) {
                        updateStandardCharge = (Integer) getSqlMapClientTemplate().update("operation.updateContractStandardCharge", map);
                    } else {
                        insertStandardCharge = (Integer) getSqlMapClientTemplate().update("operation.insertContractStandardCharge", map);
                    }
                } else {
                    map.put("stChargeId", selectStChargeId[i]);
                    map.put("stChargeAmount", selectStChargeAmount[i]);
                    map.put("stChargeDate", selectStChargeDate[i]);
                    checkStandardCharge = (String) getSqlMapClientTemplate().queryForObject("operation.checkStandardCharge", map);
                    if (checkStandardCharge != null) {
                        updateStandardCharge = (Integer) getSqlMapClientTemplate().update("operation.updateContractStandardCharge", map);
                    } else {
                        insertStandardCharge = (Integer) getSqlMapClientTemplate().update("operation.insertContractStandardCharge", map);
                    }
                }
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateContractStandardChargeDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateContractStandardChargeDetails", sqlException);
        }

        return insertStandardCharge;
    }

    /**
     * This method used to Get Consignment Order No.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String getConsignmentOrderNo(OperationTO operationTO) {
        Map map = new HashMap();
        String consignmentOrderNo = "";
        try {
            map.put("customerId", operationTO.getCustomerId());
            consignmentOrderNo = (String) getSqlMapClientTemplate().queryForObject("operation.getConsignmentOrderNo", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentOrderNo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConsignmentOrderNo", sqlException);
        }

        return consignmentOrderNo;
    }

    /**
     * This method used to Get Customer Type List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getProductCategoryList() throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList productCategoryList = new ArrayList();
        //////System.out.println("map = " + map);
        try {
            productCategoryList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getProductCategoryList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getProductCategoryList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getProductCategoryList", sqlException);
        }
        return productCategoryList;
    }

    public int insertConsignmentNote(OperationTO operationTO, int userId, SqlMapClient session) throws Exception {
        Map map = new HashMap();
        int insertConsignmentNote = 0;
        int updateStandardCharge = 0;
        int index = 0;
        map.put("userId", userId);
//        try {
        System.out.println("operationTO.getCnoteCount() = " + operationTO.getCnoteCount());
        try {
            map.put("shippingLineTwo", operationTO.getShippingLineTwo());
            int retVal = Double.compare(operationTO.getCreditLimitAmount(), operationTO.getOutStanding());
            map.put("consignmentStatusId", "5");
            operationTO.setStatus("5");
            map.put("customerTypeId", operationTO.getCustomerTypeId());
            map.put("consigneeId", operationTO.getConsigneeId());
            map.put("consignorId", operationTO.getConsignorId());
            map.put("billingTypeId", operationTO.getBillingTypeId());
            map.put("orderType", operationTO.getOrderType());
            map.put("entryType", operationTO.getEntryType());
            map.put("consignmentNoteNo", operationTO.getConsignmentNoteNo());
            map.put("consignmentDate", operationTO.getConsignmentDate());
            map.put("orderReferenceNo", operationTO.getOrderReferenceNo());
            map.put("billingParty", operationTO.getBillingParty());
            map.put("movementType", operationTO.getMovementType());
            map.put("tripType", operationTO.getTripType());
            map.put("orderReferenceRemarks", operationTO.getOrderReferenceRemarks());
            map.put("secCustId", operationTO.getSecCustId());
            if (operationTO.getPaymentType() != "") {

                map.put("paymentType", operationTO.getPaymentType());
            } else {
                map.put("paymentType", "0");

            }
            if (!"".equals(operationTO.getShipingLineNo())) {

                map.put("shipingLineNo", operationTO.getShipingLineNo());
            }
//            else {
//                map.put("shipingLineNo", "0");
//
//            }
            if (!"".equals(operationTO.getBillOfEntry())) {

                map.put("billOfEntry", operationTO.getBillOfEntry());
            }
//             else {
//                map.put("billOfEntry", "0");
//
//            }
            if (operationTO.getProductCategoryId() != "") {
                map.put("productCategoryId", operationTO.getProductCategoryId());
            } else {
                map.put("productCategoryId", 0);
            }
            map.put("origin", operationTO.getOrigin());
            map.put("destinaion", operationTO.getDestination());
            if (operationTO.getBusinessType() != "") {
                map.put("businessType", operationTO.getBusinessType());
            } else {
                map.put("businessType", 0);
            }
            if (operationTO.getMultiPickup() != null) {
                map.put("multiPickup", operationTO.getMultiPickup());
            }
            if (operationTO.getMultiDelivery() != null) {
                map.put("multiDelivery", operationTO.getMultiDelivery());
            }
            map.put("consignmentOrderInstruction", operationTO.getConsignmentOrderInstruction());
            if (operationTO.getTotalPackage() != "") {
                map.put("totalPackage", operationTO.getTotalPackage());
            } else {
                map.put("totalPackage", 0);
            }
            if (operationTO.getTotalWeightage() != "") {
                map.put("totalWeightage", operationTO.getTotalWeightage());
            } else {
                map.put("totalWeightage", 0);
            }
            if (!"".equals(operationTO.getTotalVolumes())) {
                map.put("totalVolumes", operationTO.getTotalVolumes());
            } else {
                map.put("totalVolumes", 0);
            }
            if (operationTO.getServiceType() != "") {
                map.put("serviceType", operationTO.getServiceType());
            } else {
                map.put("serviceType", 0);
            }
            if (operationTO.getVehicleTypeId() != "") {
                map.put("vehicleTypeId", operationTO.getVehicleTypeId());
            } else {
                map.put("vehicleTypeId", 0);
            }

            if (operationTO.getReeferRequired() != "") {

                map.put("reeferRequired", operationTO.getReeferRequired());
            } else {

                map.put("reeferRequired", "No");
            }
            if (operationTO.getTotalKm() != "") {
                map.put("totalKm", operationTO.getTotalKm());
            } else {
                map.put("totalKm", 0);
            }
            if (operationTO.getTotalHours() != "") {
                map.put("totalHours", operationTO.getTotalHours());
            } else {
                map.put("totalHours", 0);
            }
            if (operationTO.getTotalMinutes() != "") {
                map.put("totalMinutes", operationTO.getTotalMinutes());
            } else {
                map.put("totalMinutes", 0);
            }
            map.put("vehicleRequiredDate", operationTO.getVehicleRequiredDate());
            String vehicleRequiredHour = "00";
            String vehicleRequiredMinute = "00";
            if (!"".equals(operationTO.getVehicleRequiredHour())) {
                vehicleRequiredHour = operationTO.getVehicleRequiredHour();
            }
            if (!"".equals(operationTO.getVehicleRequiredMinute())) {
                vehicleRequiredMinute = operationTO.getVehicleRequiredMinute();
            }
            map.put("vehicleRequiredTime", vehicleRequiredHour + ":" + vehicleRequiredMinute + ":00");
            map.put("vehicleInstruction", operationTO.getVehicleInstruction());
//            if (operationTO.getVehicleInstruction() != "") {
//            } else {
//
//                map.put("vehicleInstruction", "no");
//            }
            map.put("consignorName", operationTO.getConsignorName());
            map.put("consignorPhoneNo", operationTO.getConsignorPhoneNo());
            map.put("consignorAddress", operationTO.getConsignorAddress());
            map.put("consigneeName", operationTO.getConsigneeName());
            map.put("consigneePhoneNo", operationTO.getConsigneePhoneNo());
            map.put("consigneeAddress", operationTO.getConsigneeAddress());
            map.put("invoiceType", "0");
            if (operationTO.getTotFreightAmount() != "") {
                map.put("totalFreightAmount", operationTO.getTotFreightAmount());
                map.put("outStandingAmount", operationTO.getTotFreightAmount());
            } else {
                map.put("totalFreightAmount", 0);
                map.put("outStandingAmount", "0");
            }
            if (operationTO.getSubTotal() != "") {
                map.put("standardChargeTotal", operationTO.getSubTotal());
            } else {
                map.put("standardChargeTotal", 0);
            }
            if (operationTO.getTotalCharges() != "" && !"NaN".equals(operationTO.getTotalCharges())) {
                map.put("totalCharges", operationTO.getTotalCharges());
            } else {
                map.put("totalCharges", 0);
            }
            map.put("totalCharges", 0);
            if (operationTO.getLocationId() != "" && !"NaN".equals(operationTO.getLocationId())) {
                map.put("locationId", operationTO.getLocationId());
            } else {
                map.put("locationId", "");
            }
//            if (operationTO.getContainerNO() != null && !"NaN".equals(operationTO.getContainerNO())) {
//                map.put("containerNo", operationTO.getContainerNO());
//            } else {
//            }
            map.put("containerNo", "");
//            if (operationTO.getContainerId() != "" && !"NaN".equals(operationTO.getContainerId())) {
//                map.put("containerId", operationTO.getContainerId());
//            } else {
//            }
            map.put("containerId", "");
//            String[] volume = null;
//            volume = operationTO.getProductVolume();
//            System.out.println("volume:"+volume+":");
//
//            System.out.println("volume is" + volume[0]);
//            map.put("ordVolume", volume[0]);

            map.put("ordVolume", "0");

            //cfs details
//             map.put("billEntryDate", operationTO.getBillEntryDate());
//             map.put("billEntryNo", operationTO.getBillEntryNo());
//             map.put("occDate", operationTO.getOccDate());
//             map.put("dutyPaymentDate", operationTO.getDutyPaymentDate());
//             map.put("cfsPersonName", operationTO.getCfsPersonName());
//             map.put("cfsPersonNo", operationTO.getCfsPersonNo());
            if (operationTO.getCustomerTypeId().equals("1")) {
                if (operationTO.getContractRateId() != "") {
                    map.put("contractRateId", operationTO.getContractRateId());
                } else {
                    map.put("contractRateId", 0);
                }

//                map.put("vehicleTypeIds", operationTO.getVehicleTypeId());
//                map.put("loadType", operationTO.getLoadType());
//                map.put("containerTypes", operationTO.getContainerTypes());
//                map.put("containerQtys", operationTO.getContainerQtys());
                map.put("customerId", operationTO.getCustomerId());
                map.put("customerName", operationTO.getCustomerName());
                map.put("customerCode", operationTO.getCustomerCode());
                map.put("customerAddress", operationTO.getCustomerAddress());
                map.put("pincode", operationTO.getPincode());
                map.put("customerMobileNo", operationTO.getCustomerMobileNo());
                map.put("mailId", operationTO.getMailId());
                map.put("customerPhoneNo", operationTO.getCustomerPhoneNo());
                map.put("contractId", operationTO.getContractId());
                float rateWithReefer = 0.00F;
                float rateWithoutReefer = 0.00F;
                if (operationTO.getRateWithReefer() != "") {
                    rateWithReefer = Float.parseFloat(operationTO.getRateWithReefer());
                } else {
                    rateWithReefer = 0;
                }
                if (operationTO.getRateWithoutReefer() != "") {
                    rateWithoutReefer = Float.parseFloat(operationTO.getRateWithoutReefer());
                } else {
                    rateWithoutReefer = 0;
                }
                if (operationTO.getBillingTypeId().equals("1")) {
                    map.put("routeId", 0);
                    map.put("routeContractId", operationTO.getRouteContractId());
                    if (operationTO.getReeferRequired().equals("Yes")) {
                        map.put("fixedRate", rateWithReefer);
                    } else if (operationTO.getReeferRequired().equals("No")) {
                        map.put("fixedRate", rateWithoutReefer);
                    } else {
                        map.put("fixedRate", "0");
                    }
                    map.put("ratePerKg", "0");
                    map.put("ratePerKm", "0");

                    System.out.println("map1: = " + map);

                    insertConsignmentNote = (Integer) session.insert("operation.insertConsignmentNote", map);
                } else if (operationTO.getBillingTypeId().equals("4")) {
                    map.put("routeId", 0);
                    map.put("routeContractId", operationTO.getRouteContractId());
                    if (operationTO.getReeferRequired().equals("Yes")) {
                        map.put("fixedRate", rateWithReefer);
                    } else if (operationTO.getReeferRequired().equals("No")) {
                        map.put("fixedRate", rateWithoutReefer);
                    } else {
                        map.put("fixedRate", "0");
                    }
                    map.put("ratePerKg", "0");
                    map.put("ratePerKm", "0");
                    System.out.println("map = " + map);

                    insertConsignmentNote = (Integer) session.insert("operation.insertConsignmentNote", map);
                } else if (operationTO.getBillingTypeId().equals("2")) {
                    map.put("routeId", 0);
                    map.put("routeContractId", operationTO.getRouteContractId());
                    if (operationTO.getReeferRequired().equals("Yes")) {
                        map.put("ratePerKg", rateWithReefer);
                    } else if (operationTO.getReeferRequired().equals("No")) {
                        map.put("ratePerKg", rateWithoutReefer);
                    } else {
                        map.put("ratePerKg", "0");
                    }
                    map.put("fixedRate", "0");
                    map.put("ratePerKm", "0");
                    System.out.println("map = " + map);
                    insertConsignmentNote = (Integer) session.insert("operation.insertConsignmentNote", map);
                } else if (operationTO.getBillingTypeId().equals("3")) {
                    map.put("routeId", operationTO.getRouteId());
                    map.put("routeContractId", 0);
                    if (operationTO.getReeferRequired().equals("Yes")) {
                        map.put("ratePerKm", rateWithReefer);
                    } else if (operationTO.getReeferRequired().equals("No")) {
                        map.put("ratePerKm", rateWithoutReefer);
                    } else {
                        map.put("ratePerKm", "0");
                    }
                    map.put("fixedRate", "0");
                    map.put("ratePerKg", "0");
                    System.out.println("map = " + map);
                    insertConsignmentNote = (Integer) session.insert("operation.insertConsignmentNote", map);

                }
                
                
                System.out.println("operationTO.getCustTypeId()++++++++++++++++++" + operationTO.getCustTypeId());
                map.put("consignmentOrderId", insertConsignmentNote);
                map.put("custType", operationTO.getCustTypeId());
                map.put("outstandingAmount", operationTO.getOutStanding());
                map.put("invoiceId", "0");
                map.put("creditNoteInvoiceId", "0");
                map.put("supplymentryInvoiceId", "0");
                
                
                int updateCnoteCount, updateOutstanding, updateCreditLimit, updateUsedLimit, updateOutstandingLog = 0;
                String availBlockedAmount = "";
                String availBlockedAmountTemp[] = null;
                map.put("transactionName", "Blocked");
                
                if ("1".equalsIgnoreCase(operationTO.getCustTypeId())) {   //for credit Customers Only
                    updateOutstanding = (Integer) session.update("operation.updateOutstanding", map);
                    updateCnoteCount = (Integer) session.update("operation.updateCnoteCount", map);
                    updateCreditLimit = (Integer) session.update("operation.updateCreditLimit", map);
                    availBlockedAmount = (String) session.queryForObject("operation.getAvailandBlockedAmount", map);
                    System.out.println("availBlockedAmount------" + availBlockedAmount);
                    availBlockedAmountTemp = availBlockedAmount.split("~");
                    map.put("fixedLimit", availBlockedAmountTemp[0]);
                    map.put("availAmount", availBlockedAmountTemp[1]);
                    map.put("blockedAmount", availBlockedAmountTemp[2]);
                    map.put("usedLimit", availBlockedAmountTemp[3]);
                    
                    map.put("transactionType", "Debit");  //debit
                    map.put("debitAmount", operationTO.getTotFreightAmount());
                    System.out.println(" operationTO.getOutStanding() :  "+operationTO.getOutStanding());
                    map.put("creditAmount", "0.00");
                    System.out.println(" consignment insert map ::::::::"+map);
                    
                    updateOutstandingLog = (Integer) session.update("operation.updateOutstandingLog", map);
                    
                    map.put("usedAmt", operationTO.getTotFreightAmount());
                    map.put("usedStatus", "1");

                    int updateTempCreditLimit = (Integer) session.update("operation.updateTempCreditLimit", map);
                    System.out.println("updateTempCreditLimit---"+updateTempCreditLimit);
                    
                    
                } else if ("2".equalsIgnoreCase(operationTO.getCustTypeId())) {
                    updateOutstanding = (Integer) session.update("operation.updateOutstanding", map);
                    updateCnoteCount = (Integer) session.update("operation.updateCnoteCount", map);
                    int updatepda = (Integer) session.update("operation.updatePdaAmount", map);
                    availBlockedAmount = (String) session.queryForObject("operation.getAvailandBlockedAmount", map);
                    System.out.println("availBlockedAmount------" + availBlockedAmount);
                    availBlockedAmountTemp = availBlockedAmount.split("~");
                    map.put("fixedLimit", availBlockedAmountTemp[0]);
                    map.put("availAmount", availBlockedAmountTemp[1]);
                    map.put("blockedAmount", availBlockedAmountTemp[2]);
                    map.put("usedLimit", availBlockedAmountTemp[3]);
                    
                    map.put("transactionType", "Debit"); //debit
                    map.put("debitAmount", operationTO.getTotFreightAmount());
                    map.put("creditAmount", "0.00");
                    updateOutstandingLog = (Integer) session.update("operation.updateOutstandingLog", map);
                }
                
            } else if (operationTO.getCustomerTypeId().equals("2")) {
                map.put("customerName", operationTO.getWalkinCustomerName());
                map.put("routeContractId", 0);
                map.put("customerCode", operationTO.getWalkinCustomerCode());
                map.put("customerAddress", operationTO.getWalkinCustomerAddress());
                map.put("pincode", operationTO.getWalkinPincode());
                map.put("customerMobileNo", operationTO.getWalkinCustomerMobileNo());
                map.put("mailId", operationTO.getWalkinMailId());
                map.put("customerPhoneNo", operationTO.getWalkinCustomerPhoneNo());
                map.put("billingTypeId", operationTO.getWalkInBillingTypeId());
                System.out.println("check map:" + map);
                if (operationTO.getWalkInBillingTypeId().equals("1")) {
                    map.put("invoiceType", "1");
                } else {
                    map.put("invoiceType", "2");
                }
                map.put("contractId", 0);
                if (operationTO.getWalkInBillingTypeId().equals("1")) {
                    if (operationTO.getReeferRequired().equals("Yes")) {
                        map.put("fixedRate", operationTO.getWalkinFreightWithReefer());
                    } else if (operationTO.getReeferRequired().equals("No")) {
                        map.put("fixedRate", operationTO.getWalkinFreightWithoutReefer());
                    } else {
                        map.put("fixedRate", "0");
                    }
                    map.put("ratePerKg", "0");
                    map.put("ratePerKm", "0");
                } else if (operationTO.getWalkInBillingTypeId().equals("2")) {
                    if (operationTO.getReeferRequired().equals("Yes")) {
                        map.put("ratePerKg", operationTO.getWalkinRateWithReeferPerKg());
                    } else if (operationTO.getReeferRequired().equals("No")) {
                        map.put("ratePerKg", operationTO.getWalkinRateWithoutReeferPerKg());
                    } else {
                        map.put("ratePerKg", "0");
                    }
                    map.put("fixedRate", "0");
                    map.put("ratePerKm", "0");
                } else if (operationTO.getWalkInBillingTypeId().equals("3")) {
                    if (operationTO.getReeferRequired().equals("Yes")) {
                        map.put("ratePerKm", operationTO.getWalkinRateWithReeferPerKm());
                    } else if (operationTO.getReeferRequired().equals("No")) {
                        map.put("ratePerKm", operationTO.getWalkinRateWithoutReeferPerKm());
                    } else {
                        map.put("ratePerKm", "0");
                    }
                    map.put("fixedRate", "0");
                    map.put("ratePerKg", "0");
                }

                if (operationTO.getRouteId() != "") {
                    map.put("routeId", operationTO.getRouteId());
                } else {
                    map.put("routeId", 0);
                }
                map.put("contractRateId", 0);
                int insertCustomer = 0;
                insertCustomer = (Integer) session.insert("operation.insertCustomerDetails", map);
                if (insertCustomer > 0) {
                    map.put("customerId", insertCustomer);
                }
                System.out.println("map for walk in = " + map);
                insertConsignmentNote = (Integer) session.insert("operation.insertConsignmentNote", map);
                System.out.println("insertConsignmentNote = " + insertConsignmentNote);
            }
            //

            if (map.get("consignmentStatusId").equals("3")) {
                map.put("consignmentStatusId", "3");
                map.put("consignmentOrderId", insertConsignmentNote);
                map.put("updateType", "System Update");
                map.put("remarks", "Waiting For Approval due TO Credit Limit");
                System.out.println("map consignment order status  = " + map);
                int insertConsignmentStatus = (Integer) session.update("operation.insertConsignmentStatus", map);
            }
//            } catch (Exception ex) {
//                Writer writer = new StringWriter();
//                PrintWriter printWriter = new PrintWriter(writer);
//                ex.printStackTrace(printWriter);
//                String s = writer.toString();
//                System.out.println("s = " + s);
//            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertConsignmentNote Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertConsignmentNote", sqlException);
        }

        return insertConsignmentNote;
    }

    /**
     * This method used to Insert Consignment Note.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    /**
     * This method used to Insert Multi Pickup And Multi Origin Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertConsignmentArticle(OperationTO operationTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int index = 0;
        map.put("userId", userId);
        String hr = "";
        String min = "";
        int insertConsignmentArticle = 0;
        try {
            map.put("consignmentId", operationTO.getConsignmentOrderId());
            String[] productCodes = null;
            String[] productNames = null;
            String[] packageNos = null;
            String[] weights = null;
            String[] batch = null;
            String[] uom = null;
            String[] volume = null;
            productCodes = operationTO.getProductCodes();
            volume = operationTO.getProductVolume();
            packageNos = operationTO.getPackagesNos();
            productNames = operationTO.getProductNames();
            weights = operationTO.getWeights();
            batch = operationTO.getBatchCode();
            uom = operationTO.getUom();
            if(productNames!=null){
            for (int i = 0; i < productNames.length; i++) {
                if ("".equals(productCodes[i]) || productCodes[i] == null) {
                    map.put("productCode", " ");
                } else {
                    map.put("productCode", productCodes[i]);
                }
                if ("".equals(packageNos[i]) || packageNos[i] == null) {
                    map.put("packageNos", " ");
                } else {
                    map.put("packageNos", packageNos[i]);
                }
                if ("".equals(weights[i]) || weights[i] == null) {
                    map.put("weights", " ");
                } else {
                    map.put("weights", weights[i]);
                }
                if ("".equals(batch[i]) || batch[i] == null) {
                    map.put("batch", " ");
                } else {
                    map.put("batch", batch[i]);
                }
                if ("".equals(uom[i]) || uom[i] == null) {
                    map.put("uom", " ");
                } else {
                    map.put("uom", uom[i]);
                }
                if ("".equals(volume[i]) || volume[i] == null) {
                    map.put("volume", " ");
                } else {
                    map.put("volume", volume[i]);
                }
                map.put("producName", productNames[i]);

//                map.put("weights", weights[i]);
//                map.put("batch", batch[i]);
//                map.put("uom", uom[i]);
//                map.put("volume", volume[i]);
                System.out.println("map for Articles:" + map);
                insertConsignmentArticle = (Integer) session.update("operation.insertConsignmentArticle", map);
            }
            }

//            } catch (Exception ex) {
//                Writer writer = new StringWriter();
//                PrintWriter printWriter = new PrintWriter(writer);
//                ex.printStackTrace(printWriter);
//                String s = writer.toString();
//                System.out.println("s = " + s);
//            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertConsignmentArticle Error" + sqlException.toString());
            sqlException.printStackTrace();
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertConsignmentArticle", sqlException);
        }

        return insertConsignmentArticle;
    }

    /**
     * This method used to Insert Multi Pickup And Multi Origin Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertMultiplePoints(OperationTO operationTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int insertMultiplePoints = 0;
        int index = 0;
        map.put("userId", userId);
        String hr = "";
        String min = "";
        try {
//            try {
            String consignmentId = operationTO.getConsignmentOrderId();
            String[] pointId = operationTO.getPointId();
            String[] pointRouteId = operationTO.getPointRouteId();
            String[] pointType = operationTO.getPointType();
            String[] pointSequence = operationTO.getOrder();
            String[] pointAddress = operationTO.getPointAddresss();
            String[] routeId = operationTO.getMultiplePointRouteId();
            String[] pointPlanDate = operationTO.getPointPlanDate();
            String[] pointPlanHour = operationTO.getPointPlanHour();
            String[] pointPlanMinute = operationTO.getPointPlanMinute();
//                String endPointId = operationTO.getEndPointId();
//                String endPointType = operationTO.getEndPointType();
//                String endOrder = operationTO.getEndOrder();
//                String finalRouteId = operationTO.getFinalRouteId();
//                String endPointAddress = operationTO.getEndPointAddresss();
//                String endPointPlanDate = operationTO.getEndPointPlanDate();
//                String endPointPlanHour = operationTO.getEndPointPlanHour();
//                String endPointPlanMinute = operationTO.getEndPointPlanMinute();
            map.put("consignmentId", consignmentId);
            map.put("userId", userId);
            //////System.out.println("pointId.length:" + pointId.length);
            if (pointId.length > 0) {
                for (int i = 0; i < pointId.length; i++) {
                    map.put("pointId", pointId[i]);
                    map.put("pointRouteId", pointRouteId[i]);
                    map.put("pointType", pointType[i]);
                    map.put("pointSequence", pointSequence[i]);
                    map.put("pointAddress", pointAddress[i]);
                    map.put("routeId", 0);
                    map.put("pointPlanDate", pointPlanDate[i]);

                    hr = pointPlanHour == null || ("").equals(pointPlanHour[i]) ? "00" : pointPlanHour[i];
                    min = pointPlanMinute == null || ("").equals(pointPlanMinute[i]) ? "00" : pointPlanMinute[i];
                    if ("".equals(hr)) {
                        hr = "00";
                    }
                    if ("".equals(min)) {
                        min = "00";
                    }
                    map.put("pointPlanTime", hr + ":" + min + ":00");
                    //////System.out.println("map:" + map);
                    insertMultiplePoints = (Integer) session.update("operation.insertMultiplePoints", map);
                }
//                        map.put("pointId", endPointId);
//                        map.put("pointType", endPointType);
//                        map.put("pointSequence", endOrder);
//                        map.put("pointAddress", endPointAddress);
//                        map.put("routeId", finalRouteId);
//                        map.put("pointPlanDate", endPointPlanDate);
//                        map.put("pointPlanTime", endPointPlanHour+":"+endPointPlanMinute+":00");
//                        insertMultiplePoints = (Integer) getSqlMapClientTemplate().update("operation.insertMultiplePoints", map);
            }

//            } catch (Exception ex) {
//                Writer writer = new StringWriter();
//                PrintWriter printWriter = new PrintWriter(writer);
//                ex.printStackTrace(printWriter);
//                String s = writer.toString();
//                //////System.out.println("s = " + s);
//            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("insertConsignmentNote Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertConsignmentNote", sqlException);
        }

        return insertMultiplePoints;
    }

    /**
     * This method used to Get Customer Type List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getCustomerTypeList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList customerTypeList = new ArrayList();
        //////System.out.println("map = " + map);
        try {
            customerTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCustomerTypeList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCustomerTypeList", sqlException);
        }
        return customerTypeList;
    }

    /**
     * This method used to Get Contract Route List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getContractRouteList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList contractRouteList = new ArrayList();
        map.put("contractId", operationTO.getContractId());
        map.put("billingTypeId", operationTO.getBillingTypeId());
        //////System.out.println("map = " + map);
        try {
            contractRouteList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getActualKmRouteList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractRouteList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getContractRouteList", sqlException);
        }
        return contractRouteList;
    }

    public ArrayList fetchCustomerProducts(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList list = new ArrayList();
        map.put("customerId", operationTO.getCustomerId());
        //////System.out.println("map = " + map);
        try {
            list = (ArrayList) getSqlMapClientTemplate().queryForList("operation.fetchCustomerProducts", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("fetchCustomerProducts Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "fetchCustomerProducts", sqlException);
        }
        return list;
    }

    public ArrayList getContractRoutes(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList contractRouteList = new ArrayList();
        map.put("contractId", operationTO.getContractId());
        map.put("billingTypeId", operationTO.getBillingTypeId());
        //////System.out.println("map = " + map);
        try {
            contractRouteList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getContractRoutes", map);
            //////System.out.println("getContractRoutes size:" + contractRouteList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractRouteList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getContractRouteList", sqlException);
        }
        return contractRouteList;
    }

    public ArrayList getCustContractRoutes(String routeContractId, String contractId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList contractRouteList = new ArrayList();
        map.put("routeContractId", routeContractId);
        if (contractId != null && !"".equals(contractId)) {
            map.put("contractId", contractId);
        }
        try {
            contractRouteList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCustContractRoutes", map);
            //////System.out.println("getContractRoutes size:" + contractRouteList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractRouteList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getContractRouteList", sqlException);
        }
        return contractRouteList;
    }

    public ArrayList getContractRoutesOrigin(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList contractRouteList = new ArrayList();
        map.put("contractId", operationTO.getContractId());
        map.put("billingTypeId", operationTO.getBillingTypeId());
        System.out.println("map = " + map);
        try {
            contractRouteList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getContractRoutesOrigin", map);
            System.out.println("getContractRoutes size:" + contractRouteList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractRouteList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getContractRouteList", sqlException);
        }
        return contractRouteList;
    }

    public ArrayList getContractRoutesOriginDestination(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList contractRouteList = new ArrayList();
        map.put("contractId", operationTO.getContractId());
        map.put("contractRouteOrigin", operationTO.getContractRouteOrigin());
        if (!"".equals(operationTO.getContractRouteInteream()) || operationTO.getContractRouteInteream() != null || !"null".equals(operationTO.getContractRouteInteream())) {
            map.put("contractRouteInteram", operationTO.getContractRouteInteream());
        } else {
            map.put("contractRouteInteram", "0");
        }
        map.put("movementType", operationTO.getMovementType());
        System.out.println("map = " + map);
        try {
            contractRouteList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getContractRoutesOriginDestination", map);
            System.out.println("getContractRoutes size:" + contractRouteList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractRouteList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getContractRouteList", sqlException);
        }
        return contractRouteList;
    }

    public ArrayList getContractRoutesOriginOtherICD(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList contractRouteOtherICDList = new ArrayList();
        map.put("contractId", operationTO.getContractId());
        map.put("billingTypeId", operationTO.getBillingTypeId());
        System.out.println("map = " + map);
        try {
            contractRouteOtherICDList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getContractRoutesOriginOtherICD", map);
            System.out.println("getContractRoutesOriginOtherICD size:" + contractRouteOtherICDList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractRoutesOriginOtherICD Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getContractRoutesOriginOtherICD", sqlException);
        }
        return contractRouteOtherICDList;
    }

    public ArrayList getContractRoutesOriginInteream(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList contractRouteList = new ArrayList();
        map.put("contractId", operationTO.getContractId());
        map.put("contractRouteOrigin", operationTO.getContractRouteOrigin());

        System.out.println("map = " + map);
        try {
            contractRouteList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getContractRoutesOriginInteream", map);
            System.out.println("getContractRoutes size....:" + contractRouteList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractRouteList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getContractRouteList", sqlException);
        }
        return contractRouteList;
    }

    public ArrayList getContractRoutesOriginIntereamOtherICD(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList contractRouteOtherICDList = new ArrayList();
        map.put("contractId", operationTO.getContractId());
        map.put("contractRouteOrigin", operationTO.getContractRouteOrigin());

        System.out.println("map = " + map);
        try {
            contractRouteOtherICDList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getContractRoutesOriginIntereamOtherICD", map);
            System.out.println("getContractRoutesOriginIntereamOtherICD size....:" + contractRouteOtherICDList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractRoutesOriginIntereamOtherICD Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getContractRoutesOriginIntereamOtherICD", sqlException);
        }
        return contractRouteOtherICDList;
    }

    public ArrayList getContractRouteCourse(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList contractRouteList = new ArrayList();
        map.put("routeContractId", operationTO.getRouteContractId());
        //////System.out.println("map = " + map);
        try {
            contractRouteList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getContractRouteCourse", map);
            //////System.out.println("getContractRoutes size:" + contractRouteList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractRouteList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getContractRouteList", sqlException);
        }
        return contractRouteList;
    }

    /**
     * This method used to Get Contract Route Destination List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception Model
     */
    public ArrayList getRouteDestinationList(int routeId, String customerTypeId) {
        Map map = new HashMap();
        ArrayList routeContractList = new ArrayList();
        map.put("routeId", routeId);
        map.put("customerTypeId", customerTypeId);
        //////System.out.println("map = " + map);
        try {

            routeContractList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getRouteDestinationList", map);

            //////System.out.println("routeContractList.size() = " + routeContractList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractRouteDestinationList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getContractRouteDestinationList", sqlException);
        }
        return routeContractList;

    }

    /**
     * This method used to Get Contract Route Destination List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception Model
     */
    public ArrayList getContractRouteDestinationList(int originId, String billingTypeId, String contractId) {
        Map map = new HashMap();
        ArrayList routeContractList = new ArrayList();
        map.put("originId", originId);
        map.put("billingTypeId", billingTypeId);
        map.put("contractId", contractId);
        //////System.out.println("map = " + map);
        String cistyName = "";
        try {
            routeContractList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getActualKmRouteDestinationList", map);

//            if (billingTypeId.equals("1") || billingTypeId.equals("2")) {
//                routeContractList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getPtpContractRouteDestinationList", map);
//            } else {
//                cistyName = (String) getSqlMapClientTemplate().queryForObject("operation.getCityName", map);
//                map.put("cityName", cistyName + "%");
//                routeContractList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getActualKmRouteDestinationList", map);
//            }
            //////System.out.println("routeContractList.size() = " + routeContractList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractRouteDestinationList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getContractRouteDestinationList", sqlException);
        }
        return routeContractList;

    }

    /**
     * This method used to Get Contract Vehicle Type List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception Model
     */
    public ArrayList getContractVehicleTypeList(int contractId, String billingTypeId, String customerTypeId, String origin, String destination) {
        Map map = new HashMap();
        ArrayList contractVehicleTypeList = new ArrayList();
        map.put("contractId", contractId);
        map.put("billingTypeId", billingTypeId);
        map.put("customerTypeId", customerTypeId);
        map.put("origin", origin);
        map.put("destination", destination);
        //////System.out.println("map = " + map);
        try {
            contractVehicleTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getContractVehicleTypeList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractVehicleTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getContractVehicleTypeList", sqlException);
        }
        return contractVehicleTypeList;

    }

    public ArrayList getContractVehicleTypeForPointsList(int contractId,
            String firstPointId, String point1Id, String point2Id, String point3Id, String point4Id, String finalPointId,
            String vehicleTypeIds, String loadType, String containerTypes, String containerQty) {
        Map map = new HashMap();
        ArrayList contractVehicleTypeList = new ArrayList();
        map.put("contractId", contractId);
        map.put("firstPointId", firstPointId);
        map.put("point1Id", point1Id);
        map.put("point2Id", point2Id);
        map.put("point3Id", point3Id);
        map.put("point4Id", point4Id);
        map.put("finalPointId", finalPointId);
        map.put("loadTypeId", loadType);
//        int qty = Integer.parseInt(containerQty);
        if (containerTypes != null) {
            if (containerTypes.contains(",")) {
                String[] containerTypeIds = containerTypes.split(",");
                System.out.println("containerTypeIds" + containerTypeIds.length);
                map.put("containerTypes", containerTypeIds);
            } else {
                map.put("containerType", containerTypes);
            }
        }
        if (vehicleTypeIds != null) {
            if (vehicleTypeIds.contains(",")) {
                String[] vehicleTypeIds1 = vehicleTypeIds.split(",");
                System.out.println("vehicleTypeIds1" + vehicleTypeIds1.length);
                map.put("vehicleTypeIds", vehicleTypeIds1);
            } else {
                map.put("vehicleTypeId", vehicleTypeIds);
            }
        }
//        if(qty == 1){
//        map.put("containerQty", 1);
//        }else if(qty >= 2){
//        map.put("containerQty", 2);
//        }

        try {
            System.out.println("map = " + map);
            contractVehicleTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getContractVehicleTypeForPointsList", map);
            System.out.println("getContractVehicleTypeForPointsList size ++++++++++++++++++= " + contractVehicleTypeList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractVehicleTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getContractVehicleTypeList", sqlException);
        }
        return contractVehicleTypeList;

    }

    public ArrayList getContractVehicleTypeForPointsList1(int contractId,
            String firstPointId, String point1Id, String point2Id, String point3Id, String point4Id, String finalPointId,
            String vehicleTypeIds, String loadType, String containerTypes, String containerQty) {
            Map map = new HashMap();
            ArrayList contractVehicleTypeList = new ArrayList();
            map.put("contractId", contractId);
            map.put("firstPointId", firstPointId);
            map.put("point1Id", point1Id);
            map.put("point2Id", point2Id);
            map.put("point3Id", point3Id);
            map.put("point4Id", point4Id);
            map.put("finalPointId", finalPointId);
            map.put("loadTypeId", loadType);
            map.put("containerQty", containerQty);
//        int qty = Integer.parseInt(containerQty);
        if (containerTypes != null) {
            if (containerTypes.contains(",")) {
                String[] containerTypeIds = containerTypes.split(",");
                System.out.println("containerTypeIds" + containerTypeIds.length);
                map.put("containerTypes", containerTypeIds);
            } else {
                map.put("containerType", containerTypes);
            }
        }
        if (vehicleTypeIds != null) {
            if (vehicleTypeIds.contains(",")) {
                String[] vehicleTypeIds1 = vehicleTypeIds.split(",");
                System.out.println("vehicleTypeIds1" + vehicleTypeIds1.length);
                map.put("vehicleTypeIds", vehicleTypeIds1);
            } else {
                map.put("vehicleTypeId", vehicleTypeIds);
            }
        }
//        if(qty == 1){
//        map.put("containerQty", 1);
//        }else if(qty >= 2){
//        map.put("containerQty", 2);
//        }

        try {
            System.out.println("map = " + map);
            contractVehicleTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getContractVehicleTypeForPointsList1", map);
            System.out.println("getContractVehicleTypeForPointsList size = " + contractVehicleTypeList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractVehicleTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getContractVehicleTypeList", sqlException);
        }
        return contractVehicleTypeList;

    }

    public ArrayList getContractDistanceForPointsList(int contractId,
            String toDistance, String fromDistance, String vehicleTypeIds, String loadType, String containerTypes, String containerQty) {
        Map map = new HashMap();
        ArrayList contractVehicleTypeList = new ArrayList();
        map.put("contractId", contractId);
        map.put("toDistance", toDistance);
        map.put("fromDistance", fromDistance);
        map.put("loadTypeId", loadType);
        map.put("containerQty", containerQty);
//        int qty = Integer.parseInt(containerQty);
        if (containerTypes != null) {
            if (containerTypes.contains(",")) {
                String[] containerTypeIds = containerTypes.split(",");
                System.out.println("containerTypeIds" + containerTypeIds.length);
                map.put("containerTypes", containerTypeIds);
            } else {
                map.put("containerType", containerTypes);
            }
        }
        if (vehicleTypeIds != null) {
            if (vehicleTypeIds.contains(",")) {
                String[] vehicleTypeIds1 = vehicleTypeIds.split(",");
                System.out.println("vehicleTypeIds1" + vehicleTypeIds1.length);
                map.put("vehicleTypeIds", vehicleTypeIds1);
            } else {
                map.put("vehicleTypeId", vehicleTypeIds);
            }
        }

        try {
            System.out.println("map = " + map);
            contractVehicleTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getContractDistanceForPointsList", map);
            System.out.println("getContractDistanceForPointsList size = " + contractVehicleTypeList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractDistanceForPointsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getContractVehicleTypeList", sqlException);
        }
        return contractVehicleTypeList;

    }

    /**
     * This method is used to View Fuel Price.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getfuelPriceMaster() {
        Map map = new HashMap();
        ArrayList viewfuelPriceMaster = new ArrayList();

        try {

            viewfuelPriceMaster = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getfuelPriceMaster", map);

            //////System.out.println("viewfuelPriceMaster size=" + viewfuelPriceMaster.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getfuelPriceMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getfuelPriceMaster", sqlException);
        }
        return viewfuelPriceMaster;
    }

    /**
     * This method is used to Get Save Fuel Price.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int saveFuelPriceMaster(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int savefuelPriceMaster = 0;
        int insertAvgFuelPrice = 0;
        int updateAvgFuelPrice = 0;
        String fuelDetails = "";
        double viewFuelDetails = 0.0;
        String avgFuelPrice = "";
        String[] temp = null;
        String tripIds = "";
        int fuelPriceId = 0;
        double difffuelPrice = 0;
        int updateFuelPrice = 0;
        int updateBunkPrice = 0;
        int approvedStatus = 0;
        map.put("userId", userId);
        map.put("cityId", operationTO.getCityId());
        map.put("bunkId", operationTO.getBunkId());
        map.put("approvedStatus", operationTO.getApproveStatus());
        map.put("status", operationTO.getStatus());
        String[] tempfueldDate = operationTO.getEffectiveDate().split("~");
        ArrayList fuelPriceMaster = new ArrayList();
        try {

            fuelDetails = (String) getSqlMapClientTemplate().queryForObject("operation.getCurrrentFuelPrice", map);
            System.out.println("fuelDetails@" + fuelDetails);

            if (fuelDetails != null) {
                temp = fuelDetails.split("-");
                fuelPriceId = Integer.parseInt(temp[0]);
                difffuelPrice = Double.parseDouble(operationTO.getFuelPrice()) - Double.parseDouble(temp[1]);
                map.put("endDate", tempfueldDate[0]);
                map.put("diffFuelPrice", difffuelPrice);
                map.put("fuelPriceId", fuelPriceId);
                map.put("activeInd", "Y");
                map.put("newFuelPrice", Double.parseDouble(operationTO.getFuelPrice()));
                System.out.println("Map@@@" + map);

                updateFuelPrice = getSqlMapClientTemplate().update("operation.updateFuelPrice", map);
                System.out.println("updateFuelPrice" + updateFuelPrice);
                if (updateFuelPrice > 0) {
                    map.put("startDate", tempfueldDate[0]);
                    map.put("startTime", tempfueldDate[1] + ":" + "00");

                    map.put("diffFuelPrice", difffuelPrice);
                    map.put("lastPrice", Double.parseDouble(temp[1]));
                    savefuelPriceMaster = (Integer) getSqlMapClientTemplate().insert("operation.savefuelPriceMaster", map);
                }

            } else {
                map.put("lastPrice", 0);
                map.put("diffFuelPrice", 0);
                map.put("newFuelPrice", Double.parseDouble(operationTO.getFuelPrice()));
                map.put("startDate", tempfueldDate[0]);
                map.put("startTime", tempfueldDate[1] + ":" + "00");

                savefuelPriceMaster = (Integer) getSqlMapClientTemplate().insert("operation.savefuelPriceMaster", map);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return savefuelPriceMaster;
    }

    public int updateFuelPriceMaster(OperationTO operationTO) {
        Map map = new HashMap();
        int updateStatus = 0;
        int updateFuelPrice = 0;
        int updateBunkPrice = 0;
        String oldFuel = operationTO.getOldFuelPriceId();
        System.out.println("oldFuel" + oldFuel);
        map.put("userId", operationTO.getUserId());
        map.put("newFuelPrice", Double.parseDouble(operationTO.getFuelPrice()));
        map.put("status", operationTO.getStatus());
        System.out.println("ggggg" + operationTO.getOldFuelPriceId());
        map.put("oldFuelPriceId", operationTO.getOldFuelPriceId());
        String fuelDetails = "";
        String[] temp = null;
        try {
            map.put("bunkId", operationTO.getBunkId());
            map.put("cityId", "641");
            System.out.println("map@@" + map);
            fuelDetails = (String) getSqlMapClientTemplate().queryForObject("operation.getCurrrentFuelPrice", map);
            System.out.println("fuelDetails@" + fuelDetails);
            if ("1".equals(operationTO.getStatus())) {

                if (fuelDetails != null) {
                    temp = fuelDetails.split("-");
                    int fuelPriceId = Integer.parseInt(temp[0]);
                    map.put("oldFuelId", fuelPriceId);
                    System.out.println("oldFuelId" + fuelPriceId);
                    updateStatus = getSqlMapClientTemplate().update("operation.updateOldFuelPriceInactive", map);
                }
                updateStatus = getSqlMapClientTemplate().update("operation.updateFuelPriceMaster", map);
                System.out.println("updateFuelPrice" + updateStatus);

                updateBunkPrice = getSqlMapClientTemplate().update("operation.updateBunkPrice", map);
                updateFuelPrice = getSqlMapClientTemplate().update("operation.updateTripFuelEpos", map);
                System.out.println("updateBunkPrice" + updateBunkPrice);
            } else {

                updateStatus = getSqlMapClientTemplate().update("operation.deleteAvgFuelPriceMaster", map);
                System.out.println("DELETEAvgFuelPrice" + updateStatus);

                updateStatus = getSqlMapClientTemplate().update("operation.rejectFuelPriceMaster", map);
                System.out.println("rejectFuelPrice" + updateStatus);

                updateStatus = getSqlMapClientTemplate().update("operation.rejectAvgFuelPriceMaster", map);
                System.out.println("rejectAvgFuelPrice" + updateStatus);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertCustomerContract Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertCustomerContract", sqlException);
        }

        return updateStatus;
    }

    public ArrayList getFuleType() {
        Map map = new HashMap();
        ArrayList fuleTypeList = new ArrayList();
        try {
            fuleTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getFuleType", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFuleType Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFuleType", sqlException);
        }

        return fuleTypeList;
    }

    /**
     * This method used to Get Average Current Fuel Cost.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String getAverageCurrentFuelCost(OperationTO operationTO) {
        Map map = new HashMap();
        String avgCurrentFuelCost = "";
        try {
            avgCurrentFuelCost = (String) getSqlMapClientTemplate().queryForObject("operation.getAvgCurrrentFuelPrice", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAvgCurrrentFuelPrice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getAvgCurrrentFuelPrice", sqlException);
        }

        return avgCurrentFuelCost;
    }

    /**
     * This method used to Get Account Manager Name.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getAccountManager(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList accountManagerList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("accountManagerName", operationTO.getAccountManagerName() + "%");
        //////System.out.println("map = " + map);
        try {
            accountManagerList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getAccountManager", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAccountManager Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getAccountManager", sqlException);
        }
        return accountManagerList;
    }

    /**
     * This method used to Get City Name.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getrtoList() {
        Map map = new HashMap();
        ArrayList Vehicleno = new ArrayList();
        try {
            // System.out.println("map = " + map);
            Vehicleno = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getRtoList", map);
            System.out.println("VehiclenoList size():" + Vehicleno.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getrtoList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getrtoList", sqlException);
        }
        return Vehicleno;

    }

    public ArrayList getCityList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList cityList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("cityName", operationTO.getCityName() + "%");
        map.put("pointId", operationTO.getPrevPointId());
        //////System.out.println("map = " + map);
        try {
            cityList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCityList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCityList", sqlException);
        }
        return cityList;
    }

    public ArrayList getPreOrderList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList preOrderList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        //////System.out.println("map = " + map);
        try {
            preOrderList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getPreOrderList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCityList", sqlException);
        }
        return preOrderList;
    }

    public ArrayList getPoint1CityList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList cityList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("cityName", operationTO.getCityName() + "%");
        map.put("contractId", operationTO.getContractId());
        map.put("pointId", operationTO.getPrevPointId());
        //////System.out.println("map = " + map);
        try {
            cityList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getPoint1CityList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCityList", sqlException);
        }
        return cityList;
    }

    public ArrayList getPoint4CityList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList cityList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("cityName", operationTO.getCityName() + "%");
        map.put("contractId", operationTO.getContractId());
        map.put("pointId", operationTO.getPrevPointId());
        //////System.out.println("map = " + map);
        try {
            cityList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getPoint4CityList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCityList", sqlException);
        }
        return cityList;
    }

    public ArrayList getPoint2CityList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList cityList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("cityName", operationTO.getCityName() + "%");
        map.put("contractId", operationTO.getContractId());
        map.put("pointId", operationTO.getPrevPointId());
        System.out.println("map getPoint2CityList = " + map);
        try {
            cityList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getPoint2CityList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCityList", sqlException);
        }
        return cityList;
    }

    public ArrayList getPoint3CityList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList cityList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("cityName", operationTO.getCityName() + "%");
        map.put("contractId", operationTO.getContractId());
        map.put("pointId", operationTO.getPrevPointId());
        System.out.println("map = " + map);
        try {
            cityList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getPoint3CityList", map);
             System.out.println("cityList--DAO---"+cityList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCityList", sqlException);
        }
        return cityList;
    }

    /**
     * This method used to Get Consignment List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getConsignmentList(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList consignmentList = new ArrayList();
        String customerId = "";
        String orderReferenceNo = "";
        String consignmentNoteNo = "";
        String status = "";
        String fromDate = "";
        String toDate = "";
        String productCategoryId = "";
        String origin = "";
        String destination = "";
        String destinationRadius = "";
        String pickupRadius = "";
        String pickupStartDate = "";
        String pickupEndDate = "";
        String arrivalStartDate = "";
        String arrivalEndDate = "";
        String vehicleRequiredDate = "";
        String zoneId = "";
        if (operationTO.getVehicleRequiredDate() != null) {
            vehicleRequiredDate = operationTO.getVehicleRequiredDate();
        } else {
            vehicleRequiredDate = "";
        }
        if (operationTO.getZoneId() != null) {
            zoneId = operationTO.getZoneId();
        } else {
            zoneId = "";
        }
        if (operationTO.getPickupStartDate() != null) {
            pickupStartDate = operationTO.getPickupStartDate();
        } else {
            pickupStartDate = "";
        }
        if (operationTO.getPickupEndDate() != null) {
            pickupEndDate = operationTO.getPickupEndDate();
        } else {
            pickupEndDate = "";
        }
        if (operationTO.getArrivalStartDate() != null) {
            arrivalStartDate = operationTO.getArrivalStartDate();
        } else {
            arrivalStartDate = "";
        }
        if (operationTO.getArrivalEndDate() != null) {
            arrivalEndDate = operationTO.getArrivalEndDate();
        } else {
            arrivalEndDate = "";
        }

        if (operationTO.getProductCategoryId() != null) {
            productCategoryId = operationTO.getProductCategoryId();
        } else {
            productCategoryId = "";
        }
        if (operationTO.getPickupRadius() != null) {
            pickupRadius = operationTO.getPickupRadius();
        } else {
            pickupRadius = "";
        }

        if (operationTO.getDestinationRadius() != null) {
            destinationRadius = operationTO.getDestinationRadius();
        } else {
            destinationRadius = "";
        }
        if (operationTO.getDestination() != null) {
            destination = operationTO.getDestination();
        } else {
            destination = "";
        }
        if (operationTO.getOrigin() != null) {
            origin = operationTO.getOrigin();
        } else {
            origin = "";
        }
        if (operationTO.getCustomerId() != null) {
            customerId = operationTO.getCustomerId();
        } else {
            customerId = "";
        }
        if (operationTO.getOrderReferenceNo() != null) {
            orderReferenceNo = operationTO.getOrderReferenceNo();
        } else {
            orderReferenceNo = "";
        }
        if (operationTO.getConsignmentNoteNo() != null) {
            consignmentNoteNo = operationTO.getConsignmentNoteNo();
        } else {
            consignmentNoteNo = "";
        }
        if (operationTO.getStatus() != null) {
            status = operationTO.getStatus();
        } else {
            status = "";
        }
        if (operationTO.getFromDate() != null) {
            fromDate = operationTO.getFromDate();
        } else {
            fromDate = "";
        }
        if (operationTO.getToDate() != null) {
            toDate = operationTO.getToDate();
        } else {
            toDate = "";
        }
        map.put("zoneId", zoneId);
        map.put("vehicleRequiredDate", vehicleRequiredDate);
        map.put("pickupStartDate", pickupStartDate);
        map.put("pickupEndDate", pickupEndDate);
        map.put("arrivalStartDate", arrivalStartDate);
        map.put("arrivalEndDate", arrivalEndDate);
        map.put("origin", origin);
        map.put("pickType", operationTO.getPickupType());
        map.put("destType", operationTO.getDestinationType());
        map.put("destination", destination);
        map.put("pickupRadius", pickupRadius);
        map.put("destinationRadius", destinationRadius);
        map.put("productCategoryId", productCategoryId);
        map.put("customerId", customerId);
        //            map.put("hubId", operationTO.getHubId());
        map.put("customerOrderReferenceNo", orderReferenceNo);
        map.put("consignmentOrderReferenceNo", consignmentNoteNo);
        map.put("status", status);
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        System.out.println("lisssss map is" + map);
        try {
//        calculateDistance cd = new calculateDistance();
//        double lat1 = 0.00;
//        double long1 = 0.00;
//        double lat2 = 0.00;
//        double long2 = 0.00;
//        double distance = 0.00;

//        String originLatLong = "";
//        String destLatLong = "";
//        if (!"".equals(origin)) {
//            originLatLong = (String) getSqlMapClientTemplate().queryForObject("operation.getOriginLatLong", map);
//        }
//        if (!"".equals(destination)) {
//            pickupRadius = destinationRadius;
//            map.put("origin", destination);
//            destLatLong = (String) getSqlMapClientTemplate().queryForObject("operation.getOriginLatLong", map);
//            map.put("origin", origin);
//        }
            //////System.out.println("originoriginorigin map" + map);
//        if (!"".equals(originLatLong) && originLatLong != null) {
//            String temp[] = originLatLong.split("~");
//            lat1 = Double.parseDouble(temp[0]);
//            long1 = Double.parseDouble(temp[1]);
//        }
//        List consignmentOrderIds = new ArrayList();
            //////System.out.println("map for getConsignmentOriginLists:" + map);
//        ArrayList consignmentOriginList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getConsignmentOriginLists", map);
            //////System.out.println("consignmentOriginList size:" + consignmentOriginList.size());
//        boolean pickQualified = false;
//        boolean dropQualified = false;
//
//        Iterator itr1 = consignmentOriginList.iterator();
//        OperationTO opTO = new OperationTO();
//        while (itr1.hasNext()) {
            //////System.out.println("am here.....");
//            opTO = new OperationTO();
//            opTO = (OperationTO) itr1.next();
//            if (!"".equals(pickupRadius) && pickupRadius != null && !"0".equals(pickupRadius)) {
//                if (!"".equals(origin)) {
//                    map.put("origin", opTO.getOrigin());
//                }
//                String destinationLatLong = (String) getSqlMapClientTemplate().queryForObject("operation.getOriginLatLong", map);
//                if (!"".equals(destinationLatLong) && destinationLatLong != null) {
//                    String temp[] = destinationLatLong.split("~");
//                    lat2 = Double.parseDouble(temp[0]);
//                    long2 = Double.parseDouble(temp[1]);
//                }
//
//                distance = cd.distance(lat1, long1, lat2, long2, 'K');
//                if (Double.parseDouble(pickupRadius) >= distance) {
//                    double smallValue = 10000;
//                    if (smallValue > distance) {
//                        smallValue = distance;
//                    }
//                    pickQualified = true;
//                }
//            } else {
//                pickQualified = true;
//            }
            //check for destination compliance if applicable
//            if (!"".equals(destinationRadius) && destinationRadius != null && !"0".equals(destinationRadius)) {
//                if (!"".equals(origin)) {
//                    map.put("origin", opTO.getDestination());
//                }
//
//                String destinationLatLong = (String) getSqlMapClientTemplate().queryForObject("operation.getOriginLatLong", map);
//                if (!"".equals(destinationLatLong) && destinationLatLong != null) {
//                    String temp[] = destinationLatLong.split("~");
//                    lat2 = Double.parseDouble(temp[0]);
//                    long2 = Double.parseDouble(temp[1]);
//                }
//
//                distance = cd.distance(lat1, long1, lat2, long2, 'K');
//                if (Double.parseDouble(destinationRadius) >= distance) {
//                    double smallValue = 10000;
//                    if (smallValue > distance) {
//                        smallValue = distance;
//                    }
//                    dropQualified = true;
//                }
//            } else {
//                dropQualified = true;
//            }
//            if (dropQualified && pickQualified) {
//                consignmentOrderIds.add(opTO.getConsignmentOrderId());
//            }
//            distance = 0.00;
//            lat2 = 0.00;
//            long2 = 0.00;
//            dropQualified = false;
//            pickQualified = false;
//        }
            //////System.out.println("consignmentOrderIds:" + consignmentOrderIds);
//        try {
//            map.put("origin", origin);
//            map.put("destination", destination);
//            map.put("pickupType", operationTO.getPickupType());
//            map.put("destinationType", operationTO.getDestinationType());
//            //////System.out.println("value:" + consignmentOrderIds);
//            map.put("consignmentOrderIds", consignmentOrderIds);
//
//            if (operationTO.getParameterName() != null && operationTO.getParameterName().equals("ExcelExport")) {
//                if (operationTO.getConsignmentOrderId().contains(",")) {
//                    String[] consignmentId = operationTO.getConsignmentOrderId().split(",");
//                    List cnoteId = new ArrayList(consignmentId.length);
//                    for (int i = 0; i < consignmentId.length; i++) {
//                        //////System.out.println("value:" + consignmentId[i]);
//                        cnoteId.add(consignmentId[i]);
//                    }
//                    map.put("cNoteCount", "1");
//                    map.put("cnoteId", cnoteId);
//                } else {
//                    map.put("cNoteCount", "2");
//                    map.put("cnoteId", operationTO.getConsignmentOrderId());
//                }
//
//                //////System.out.println("mpa excel loop= " + map);
//                consignmentList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getConsignmentListExcelExport", map);
//            }
//            if (operationTO.getParameterName() != null && (operationTO.getParameterName().equals("0")
//                    || operationTO.getParameterName().equals("1") || operationTO.getParameterName().equals("2")
//                    || operationTO.getParameterName().equals("3"))) {
//                //////System.out.println("my if executed succesfull");
//                if (operationTO.getParameterName().equals("1")) {
//                    //////System.out.println("operationTO.11111111111111111111111111111111111111111111111ap value is= " + map);
//                    consignmentList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getConsignmentListForPick", map);
//                } else {
//                    //////System.out.println("444444444444444444444444444() map value is= " + map);
//                    consignmentList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getConsignmentList", map);
//                }
//
//            } else {
//                //////System.out.println("regular map list loop= " + map);
//            }
            consignmentList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getConsignmentList", map);
            //////System.out.println("consignmentList " + consignmentList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRouteDetailsList List", sqlException);
        }

        return consignmentList;
    }

    public ArrayList getConsignmentViewList(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList consignmentList = new ArrayList();
        String customerId = "";
        String orderReferenceNo = "";
        String consignmentNoteNo = "";
        String status = "";
        String fromDate = "";
        String toDate = "";
        if (operationTO.getCustomerId() != null) {
            customerId = operationTO.getCustomerId();
        } else {
            customerId = "";
        }
        if (operationTO.getOrderReferenceNo() != null) {
            orderReferenceNo = operationTO.getOrderReferenceNo();
        } else {
            orderReferenceNo = "";
        }
        if (operationTO.getConsignmentNoteNo() != null) {
            consignmentNoteNo = operationTO.getConsignmentNoteNo();
        } else {
            consignmentNoteNo = "";
        }
        if (operationTO.getStatus() != null) {
            status = operationTO.getStatus();
        } else {
            status = "";
        }
        Date today = new Date();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        if (operationTO.getFromDate() != null) {
            fromDate = operationTO.getFromDate();
        } else {
            fromDate = df.format(today);
        }
        if (operationTO.getToDate() != null) {
            toDate = operationTO.getToDate();
        } else {
            toDate = df.format(today);
        }
        map.put("customerId", customerId);
        map.put("customerOrderReferenceNo", orderReferenceNo + "%");
        map.put("consignmentOrderReferenceNo", consignmentNoteNo + "%");
        map.put("status", status);
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);

        map.put("roleId", operationTO.getRoleId());
        map.put("userId", operationTO.getUserId());
        String empId = (String) getSqlMapClientTemplate().queryForObject("customer.getEmployeeId", map);
        map.put("empId", empId);
        System.out.println("mpa = " + map);

        try {
            consignmentList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getConsignmentViewList", map);
            System.out.println("routeDetailsList " + consignmentList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRouteDetailsList List", sqlException);
        }

        return consignmentList;
    }

    /**
     * This method used to Get Route Course
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getRouteCourse(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList routeCourse = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("cityName", operationTO.getCityName() + "%");
        map.put("lastPointId", operationTO.getCityId());
        map.put("endPointId", operationTO.getFinalPointId());
        //////System.out.println("map = " + map);
        try {
            routeCourse = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getRouteCourse", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRouteCourse Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getRouteCourse", sqlException);
        }
        return routeCourse;
    }

    public ArrayList getConsignmentDetails(String id) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList response = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("consignmentOrderId", id);
        //////System.out.println("map = " + map);
        try {
            response = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getConsignmentDetails", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getConsignmentDetails", sqlException);
        }
        return response;
    }

    /**
     * This method used to Check Route Exists For CNote Route Course.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String checkIfRouteExists(String pointIdValue, String pointIdPrev, String pointIdNext) {
        Map map = new HashMap();
        OperationTO operationTO = null;
        map.put("fromId", pointIdPrev);
        map.put("toId", pointIdValue);

        String checkIfRouteExists = "";
        //////System.out.println("map = " + map);
        String result = "0~0~0";
        String prevRouteId = "";
        try {

            checkIfRouteExists = (String) getSqlMapClientTemplate().queryForObject("operation.checkIfRouteExists", map);
            if (checkIfRouteExists != null && !"".equals(checkIfRouteExists)) {
                prevRouteId = checkIfRouteExists;
                result = "1~" + checkIfRouteExists + "~0";
                map.put("fromId", pointIdValue);
                map.put("toId", pointIdNext);
                checkIfRouteExists = (String) getSqlMapClientTemplate().queryForObject("operation.checkIfRouteExists", map);
                if (checkIfRouteExists != null && !"".equals(checkIfRouteExists)) {
                    result = "2~" + prevRouteId + "~" + checkIfRouteExists;
                }
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkIfRouteExists Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkIfRouteExists", sqlException);
        }
        return result;

    }

//    Brattle Foods Ends Here
//    Brattle Foods Start Arun 7 dec Here
    /**
     * This method used to Get Consignment List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList viewConsignmentList(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList consignmentList = new ArrayList();
        try {
            map.put("consignmentOrderId", operationTO.getConsignmentOrderId());
            //////System.out.println("map = " + map);
            consignmentList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.viewConsignmentList", map);
            //////System.out.println("consignmentList " + consignmentList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getConsignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRouteDetailsList List", sqlException);
        }

        return consignmentList;
    }

    /**
     * This method used to Get Consignment Articles.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getConsignmentArticles(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList consignmentArticles = new ArrayList();
        try {
            map.put("consignmentOrderId", operationTO.getConsignmentOrderId());
            //////System.out.println("map = " + map);
            consignmentArticles = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getConsignmentArticles", map);
            //////System.out.println("consignmentArticles " + consignmentArticles.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentArticles Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConsignmentArticles List", sqlException);
        }

        return consignmentArticles;
    }

    /**
     * This method used to Get Consignment Points.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getConsignmentPoints(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList consignmentPoints = new ArrayList();
        try {
            map.put("consignmentOrderId", operationTO.getConsignmentOrderId());
            //////System.out.println("map = " + map);
            consignmentPoints = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getConsignmentPoints", map);
            //////System.out.println("consignmentPoints " + consignmentPoints.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentPoints Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConsignmentPoints List", sqlException);
        }

        return consignmentPoints;
    }

    /**
     * This method used to Cancel Consignment Note.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int cancelConsignmentNote(String consignmentOrderId, String consignmentStatusId, String cancelRemarks, int userId) {
        Map map = new HashMap();
        int cancelConsignmentNote = 0;
        int unHoldConsignmentNote = 0;
        int index = 0;
        map.put("userId", userId);
        try {
            try {
                map.put("consignmentOrderId", consignmentOrderId);
                map.put("consignmentStatusId", consignmentStatusId);
                map.put("remarks", cancelRemarks);
                map.put("updateType", "System Update");
                map.put("userId", userId);
                //////System.out.println("mpa = " + map);
                if ("4".equals(consignmentStatusId)) {
                    cancelConsignmentNote = (Integer) getSqlMapClientTemplate().update("operation.cancelConsignmentNote", map);
                } else if ("5".equals(consignmentStatusId)) {
                    cancelConsignmentNote = (Integer) getSqlMapClientTemplate().update("operation.unHoldConsignmentNote", map);
                } else if ("3".equals(consignmentStatusId)) {
                    cancelConsignmentNote = (Integer) getSqlMapClientTemplate().update("operation.onHoldConsignmentNote", map);
                }

                int insertStatusDetails = (Integer) getSqlMapClientTemplate().update("operation.insertConsignmentStatus", map);
            } catch (Exception ex) {
                Writer writer = new StringWriter();
                PrintWriter printWriter = new PrintWriter(writer);
                ex.printStackTrace(printWriter);
                String s = writer.toString();
                //////System.out.println("s = " + s);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertConsignmentNote Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertConsignmentNote", sqlException);
        }

        return cancelConsignmentNote;
    }

    /**
     * This method is used to View Vehicle And Driver Planning.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getVehicleDriverMapping(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList vehicleDriverMapping = new ArrayList();
        try {
            vehicleDriverMapping = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getVehicleDriverMapping", map);
            //////System.out.println("routeDetailsList " + vehicleDriverMapping.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleDriverMapping Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleDriverMapping List", sqlException);
        }

        return vehicleDriverMapping;
    }

    /**
     * This method used to Get Vehicle Reg No List
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getVehicleNo(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList vehicleNo = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("vehicleNo", "%" + operationTO.getVehicleNo() + "%");
        //System.out.println("map = " + map);
        try {
            vehicleNo = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getVehicleList", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleNo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehicleNo", sqlException);
        }
        return vehicleNo;
    }

    public ArrayList getVehicleAdvanceVehicleNo(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList vehicleNo = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("vehicleNo", operationTO.getVehicleNo() + "%");
        map.put("usageTypeId", operationTO.getUsageTypeId());
        //////System.out.println("map = " + map);
        try {
            vehicleNo = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getVehicleAdvanceList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleAdvanceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehicleAdvanceList", sqlException);
        }
        return vehicleNo;
    }

    /**
     * This method used to Get Driver Name List
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getDriverName(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList driverName = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("driverName", operationTO.getDriverName() + "%");
        //////System.out.println("map = " + map);
        try {
            driverName = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getDriverList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getDriverList", sqlException);
        }
        return driverName;
    }

    public ArrayList getTechinicanDriverName(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList driverName = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("driverName", operationTO.getDriverName() + "%");
        //////System.out.println("map = " + map);
        try {
            driverName = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getTechinicanDriverName", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getDriverList", sqlException);
        }
        return driverName;
    }

    /**
     * This method used to Insert Vehicle Driver Mapping.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertVehicleDriverMapping(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int insertVehicleDriverMapping = 0;
        int index = 0;
        map.put("userId", userId);
        try {
            try {
                map.put("vehicleId", operationTO.getVehicleId());
                if ("".equals(operationTO.getPrimaryDriverId())) {
                    map.put("primaryDriverId", 0);
                } else {
                    map.put("primaryDriverId", operationTO.getPrimaryDriverId());
                }
                if (!"".equals(operationTO.getSecondaryDriverIdOne())) {
                    map.put("secondaryDriverIdOne", operationTO.getSecondaryDriverIdOne());
                } else {
                    map.put("secondaryDriverIdOne", "0");
                }
                if (!"".equals(operationTO.getSecondaryDriverIdTwo())) {
                    map.put("secondaryDriverIdTwo", operationTO.getSecondaryDriverIdTwo());
                } else {
                    map.put("secondaryDriverIdTwo", "0");
                }
                map.put("userId", userId);
                map.put("mappingId", operationTO.getMappingId());
                //////System.out.println("mpa = " + map);
                if ("0".equals(operationTO.getMappingId())) {
                    insertVehicleDriverMapping = (Integer) getSqlMapClientTemplate().update("operation.insertVehicleDriverMapping", map);
                } else {
                    insertVehicleDriverMapping = (Integer) getSqlMapClientTemplate().update("operation.updateVehicleDriverMapping", map);
                }
            } catch (Exception ex) {
                Writer writer = new StringWriter();
                PrintWriter printWriter = new PrintWriter(writer);
                ex.printStackTrace(printWriter);
                String s = writer.toString();
                //////System.out.println("s = " + s);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVehicleDriverMapping Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertVehicleDriverMapping", sqlException);
        }

        return insertVehicleDriverMapping;
    }

    //Arul Starts 11-12-2013
    public String checkVehicleDriverMapping(String vehicleId) {
        Map map = new HashMap();
        OperationTO operationTO = null;
        map.put("vehicleId", vehicleId);
        String checkVehicleDriverMapping = "";
        //////System.out.println("map = " + map);
        try {
            checkVehicleDriverMapping = (String) getSqlMapClientTemplate().queryForObject("operation.checkVehicleDriverMapping", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkVehicleDriverMapping Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkVehicleDriverMapping", sqlException);
        }
        return checkVehicleDriverMapping;

    }
    //Arul Ends 11-12-2013

//    Brattle Foods Ends  Arun 7 dec Here
    /**
     * This method used to Get VehicleList .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getVehicleList(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList vehicleList = new ArrayList();
        map.put("regno", "%" + operationTO.getRegno() + "%");
        try {
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getVehicleListForAvailability", map);
            //////System.out.println(" getVehicleList =" + vehicleList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getZoneList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleList", sqlException);
        }

        return vehicleList;
    }

    /**
     * This method used to Get Route List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getInvoiceDetails(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList invoiceDetails = new ArrayList();
        map.put("consignmentNo", operationTO.getConsignmentOrderId());
        //////System.out.println("map = " + map);
        try {
            String invoiceId = (String) getSqlMapClientTemplate().queryForObject("operation.getInvoiceIds", map);
            //////System.out.println("this is the invoice no" + invoiceId);
            if (invoiceId != null) {
                map.put("invoiceId", invoiceId);
                invoiceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getInvoiceDetails", map);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getInvoiceDetails", sqlException);
        }
        return invoiceDetails;
    }

    public ArrayList getCompanyList() {
        Map map = new HashMap();
        ArrayList companyList = new ArrayList();
        try {
            companyList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCompanyList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "GradeList", sqlException);
        }
        return companyList;
    }

    public ArrayList getVehicleAvailabilityList(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList vehicleAvailabilityList = new ArrayList();
//        map.put("vehicleid", operationTO.getVehicleid());
        map.put("regno", operationTO.getRegno());
        map.put("vehicleTypeId", operationTO.getVehicleID());
        map.put("fromDate", operationTO.getFromDate());
        map.put("toDate", operationTO.getToDate());
        map.put("status", operationTO.getStatus());
        map.put("zoneId", operationTO.getZoneId());
        map.put("fleetCenterId", operationTO.getFleetCenterId());
        map.put("roleId", operationTO.getRoleId());
        map.put("companyId", operationTO.getCompId());
        map.put("isActive", operationTO.getIsactive());
        map.put("cityFromId", operationTO.getCityId());
        if ("1".equals(operationTO.getTripType())) {
            map.put("usageType", 2);
        } else if ("2".equals(operationTO.getTripType())) {
            map.put("usageType", 1);
        }

        System.out.println("map = " + map);

        try {
            System.out.println("this is vehicle Availability");
            vehicleAvailabilityList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getVehicleAvailabilityList", map);
            System.out.println(" vehicleAvailabilityList =" + vehicleAvailabilityList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleAvailabilityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleAvailabilityList ", sqlException);
        }

        return vehicleAvailabilityList;
    }

    public ArrayList getTripStatus() {
        Map map = new HashMap();
        ArrayList statusDetails = new ArrayList();
        try {
            statusDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getTripStatus", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("gettripClosureRequest Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "gettripClosureRequest", sqlException);
        }
        return statusDetails;
    }
//Senthil start 7 dec

    /**
     * This method used to get closed trips.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getTripCustomerList() {
        Map map = new HashMap();
        ArrayList customerList = null;
        try {
            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCustomerList", map);
            System.out.println("getCustomerList.size()------- = " + customerList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerList List", sqlException);
        }
        return customerList;
    }

    /**
     * This method used to get trip details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getCustomerTripDetails(String tripId, int userId) {
        Map map = new HashMap();
        ArrayList tripList = null;
        try {
            map.put("userId", userId);
            map.put("tripId", tripId);
            tripList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCustomerTripDetails", map);
            //////System.out.println("getCustomerList.size() = " + tripList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerList List", sqlException);
        }
        return tripList;
    }

    /**
     * This method used to get last invoice Id.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int getLastInvoceId() {
        Map map = new HashMap();
        int lastInvoiceId = 0;
        try {

            lastInvoiceId = (Integer) getSqlMapClientTemplate().queryForObject("operation.getCustomerTripDetails", map);
            //////System.out.println("Last INvoice Id = " + lastInvoiceId);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerList List", sqlException);
        }
        return lastInvoiceId;
    }

    /**
     * This method used to get last invoice Id.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertInvoiceHeader(String customerName, String customerId, String billingType, String fromdate, String todate, int userId) {
        Map map = new HashMap();
        int lastInvoiceId = 0;
        try {
            map.put("customerName", customerName);
            map.put("customerId", customerId);
            map.put("billingType", billingType);
            map.put("fromdate", fromdate);
            map.put("todate", todate);
            map.put("userId", userId);
            //////System.out.println("header map::::" + map);
            lastInvoiceId = (Integer) getSqlMapClientTemplate().insert("operation.insertInvoiceHeader", map);
            //////System.out.println("Last INvoice Id = " + lastInvoiceId);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerList List", sqlException);
        }
        return lastInvoiceId;
    }

    /**
     * This method used to insert invoice details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertInvoiceDetails(String invoiceCode, String tripId, String cnoteNo, String totalweights, String totalweight, String customerName, String custId, String tripDate, String destination, int userId, String invoiceRefCode, int lastinsertId, String freightamt, String cnoteId) {
        Map map = new HashMap();
        int lastInvoiceId = 0;
        try {
            map.put("invoiceCode", invoiceCode);
            map.put("tripId", tripId);
            map.put("cnoteNo", cnoteNo);
            map.put("totalweights", totalweight);
            map.put("customerName", customerName);
            map.put("custId", custId);
            map.put("tripDate", tripDate);
            map.put("destination", destination);
            map.put("userId", userId);
            map.put("invoiceRefCode", invoiceRefCode);
            map.put("lastinsertId", lastinsertId);
            map.put("freightamt", freightamt);
            map.put("cnoteId", cnoteId);
            //////System.out.println("insert header deati;ls map" + map);
            lastInvoiceId = (Integer) getSqlMapClientTemplate().insert("operation.insertInvoiceHeaderDetails", map);
            lastInvoiceId = (Integer) getSqlMapClientTemplate().update("operation.updateTripStatusForBilling", map);
            lastInvoiceId = (Integer) getSqlMapClientTemplate().update("operation.saveTripStatusDetails", map);

            //////System.out.println("Invoice Detailsls Table = " + lastInvoiceId);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerList List", sqlException);
        }
        return lastInvoiceId;
    }

    /**
     * This method used to insert invoice details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int updateInvoiceHeader(double lastinsertId, String invoiceCode, String invoiceRefCode, double totalamt) {
        Map map = new HashMap();
        int lastInvoiceId = 0;
        try {
            map.put("lastinsertId", lastinsertId);
            map.put("invoiceCode", invoiceCode);
            map.put("invoiceRefCode", invoiceRefCode);
            map.put("totalamt", totalamt);

            //////System.out.println("update header deati;ls map" + map);
            int status = (Integer) getSqlMapClientTemplate().update("operation.updateInvoiceHeaderDetails", map);
            //////System.out.println("Invoice Deatls Table = " + lastInvoiceId);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerList List", sqlException);
        }
        return lastInvoiceId;
    }

    /**
     * This method used to get customer invoice details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public OperationTO handleCustomerDetails(String customerId) {
        Map map = new HashMap();
        OperationTO opto = null;
        ArrayList list = null;
        try {
            map.put("customerId", customerId);
            //////System.out.println("customer header deati;ls map" + map);
            list = (ArrayList) getSqlMapClientTemplate().queryForList("operation.handleCustomerDetails", map);
            //////System.out.println("Invoice Deatls Table = " + list.size());
            opto = (OperationTO) list.get(0);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerList List", sqlException);
        }
        return opto;
    }

    /**
     * This method used to get closed trips.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getClosedTrips(String fromDate, String toDate, String custName, int userId) {
        Map map = new HashMap();
        ArrayList clisedTrips = null;

        try {
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            if (!"".equals(custName)) {
                custName = custName + "%";
            }
            map.put("custName", custName);
            map.put("userId", userId);
            //////System.out.println("closed Trip map is::" + map);
            clisedTrips = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getclosedTrips", map);
            //////System.out.println("getClosedTrips.size() = " + clisedTrips.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getClosedTrips Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getClosedTrips List", sqlException);
        }
        return clisedTrips;
    }

    public ArrayList getClosedLeasedTrips(String fromDate, String toDate, String custName, int userId) {
        Map map = new HashMap();
        ArrayList clisedTrips = null;

        try {
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            if (!"".equals(custName)) {
                custName = custName + "%";
            }
            map.put("custName", custName);
            map.put("userId", userId);
            System.out.println("closed Trip map is::" + map);
            clisedTrips = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getclosedLeasedTrips", map);
            //////System.out.println("getClosedTrips.size() = " + clisedTrips.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getClosedTrips Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getClosedTrips List", sqlException);
        }
        return clisedTrips;
    }
//Senthil end 7 dec
//Arun 11 Dec Starts

    public ArrayList getfinanceAdviceDetails(String dateval, String type, String tripType) {
        Map map = new HashMap();
        ArrayList financeAdviceDetails = new ArrayList();
        map.put("dateval", dateval);
        map.put("type", type);
        map.put("tripType", tripType);
        //////System.out.println("map = " + map);
        try {
            financeAdviceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getfinanceAdviceDetails", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("financeAdviceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getfinanceAdviceDetails", sqlException);
        }
        return financeAdviceDetails;
    }

    public ArrayList viewAdviceDetails(String tripAdvaceId) {
        Map map = new HashMap();
        ArrayList viewfinanceAdviceDetails = new ArrayList();
        map.put("tripAdvaceId", tripAdvaceId);
        try {
            viewfinanceAdviceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.viewfinanceApprovalDetails", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("viewfinanceAdviceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "viewgetfinanceAdviceDetails", sqlException);
        }
        return viewfinanceAdviceDetails;
    }

    public ArrayList getfinanceAdvice(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList financeAdvice = new ArrayList();
        map.put("fromdate", operationTO.getFromDate());
        map.put("todate", operationTO.getToDate());
        map.put("tripType", operationTO.getTripType());
        //////System.out.println("financeAdvice  map = " + map);
        try {
            financeAdvice = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getfinanceAdvice", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("financeAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getfinanceAdvice", sqlException);
        }
        return financeAdvice;
    }

    public ArrayList getfinanceRequest(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList financeRequest = new ArrayList();
        map.put("fromdate", operationTO.getFromDate());
        map.put("todate", operationTO.getToDate());
        try {
            //////System.out.println("map:" + map);
            financeRequest = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getfinanceRequest", map);
            //////System.out.println("financeRequest size:" + financeRequest.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("financeRequest Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "financeRequest", sqlException);
        }
        return financeRequest;
    }

    public int addAdvanceRequest(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int addAdvanceRequest = 0;

        try {
            map.put("advancerequestamt", operationTO.getAdvancerequestamt());
            map.put("requeston", operationTO.getRequeston());
            //map.put("requestby", operationTO.getRequestby());
            map.put("requestremarks", operationTO.getRequestremarks());
            map.put("requeststatus", operationTO.getRequeststatus());
            map.put("tripid", operationTO.getTripid());
            map.put("tobepaidtoday", operationTO.getTobepaidtoday());
            //map.put("estimatedrevenue", operationTO.getEstimatedrevenue());
//            map.put("tripday", operationTO.getTripday());
            map.put("advicedate", operationTO.getAdvicedate());
            map.put("batchType", operationTO.getBatchType());
            map.put("userId", userId);

            System.out.println(map + "===map");
            addAdvanceRequest = (Integer) getSqlMapClientTemplate().update("operation.addAdvanceRequest", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Approval Request Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "ApprovalRequest", sqlException);
        }
        return addAdvanceRequest;
    }

    public int saveAdvanceApproval(OperationTO operationTO, int userId, String batchType) {
        Map map = new HashMap();
        int saveAdvanceApproval = 0;

        try {
            map.put("advanceapproveamt", operationTO.getAdvancerequestamt());
            map.put("remarks", operationTO.getApproveremarks());
            map.put("status", operationTO.getApprovalstatus());
            map.put("tripid", operationTO.getTripid());
            map.put("userId", userId);
            map.put("advicedate", operationTO.getAdvicedate());
            map.put("batchType", batchType);

            System.out.println(map + "===map");
            saveAdvanceApproval = (Integer) getSqlMapClientTemplate().update("operation.saveAdvanceApproval", map);
//            updateDriverAdvanceToEFS(operationTO.getTripId());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveAdvanceApproval Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveAdvanceApproval", sqlException);
        }
        return saveAdvanceApproval;
    }

    public int saveTripAmount(OperationTO operationTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int addAdvanceRequest = 0;
        int updatetripamount = 0;
        try {
            map.put("paidamt", operationTO.getPaidamt());
            map.put("paidstatus", operationTO.getPaidstatus());
            map.put("tripid", operationTO.getTripid());
            map.put("tripAdvaceId", operationTO.getTripAdvaceId());
            map.put("tobepaidtoday", operationTO.getTobepaidtoday());
            map.put("tripday", operationTO.getTripday());
            map.put("advicedate", operationTO.getAdvicedate());
            map.put("userId", userId);
            System.out.println("saveTripAmount " + map);
            addAdvanceRequest = (Integer) session.update("operation.saveTripAmount", map);
            //////System.out.println("addAdvanceRequest = " + addAdvanceRequest);
            int TripActualAmount = 0;
            if (addAdvanceRequest > 0) {
                TripActualAmount = (Integer) session.queryForObject("operation.getActualPaidVal", map);
                map.put("TripActualAmount", TripActualAmount);
                //////System.out.println("TripActualAmount" + map);
                updatetripamount = (Integer) session.update("operation.updateTripActualAmount", map);
                //Finance Entry Starts Here
                //Account Entry Start
                String code2 = "";
                String[] temp = null;
                int insertStatus = 0;
                map.put("userId", userId);
                map.put("DetailCode", "1");
                map.put("voucherType", "%PAYMENT%");
                code2 = (String) session.queryForObject("trip.getTripVoucherCode", map);
                temp = code2.split("-");
                int codeval2 = Integer.parseInt(temp[1]);
                int codev2 = codeval2 + 1;
                String voucherCode = "PAYMENT-" + codev2;
                System.out.println("voucherCode = " + voucherCode);
                map.put("voucherCode", voucherCode);
                map.put("mainEntryType", "VOUCHER");
                map.put("entryType", "PAYMENT");
                String tripDriverLedgerDetails = "";
                String tripAdvanceVehicleId = "";
                tripAdvanceVehicleId = (String) session.queryForObject("trip.tripAdvanceVehicleId", map);
                System.out.println("tripAdvanceVehicleId:" + tripAdvanceVehicleId);
                map.put("vehicleId", tripAdvanceVehicleId);
                System.out.println("map::::" + map);
                tripDriverLedgerDetails = (String) session.queryForObject("trip.tripDriverLedgerDetails", map);
                System.out.println("tripDriverLedgerDetails:" + tripDriverLedgerDetails);
                String[] ledgerTemp = null;
                ledgerTemp = tripDriverLedgerDetails.split("~");
                map.put("ledgerId", ledgerTemp[0]);
                map.put("particularsId", ledgerTemp[1]);
                map.put("amount", operationTO.getPaidamt());
                map.put("Accounts_Type", "DEBIT");
                map.put("Remark", "Trip Advance");
                map.put("Reference", "Trip");
                System.out.println("tripId = " + operationTO.getTripid());
                map.put("SearchCode", operationTO.getTripid());
                System.out.println("map1 =---------------------> " + map);
                insertStatus = (Integer) session.update("trip.insertTripAccountEntry", map);
                System.out.println("status1 = " + insertStatus);
                //--------------------------------- acc 2nd row start --------------------------
                if (insertStatus > 0) {
                    map.put("DetailCode", "2");
                    String[] tmp = null;
                    map.put("ledgerId", "1");
                    map.put("particularsId", "LEDGER-0");
                    map.put("Accounts_Type", "CREDIT");
                    System.out.println("map2 =---------------------> " + map);
                    insertStatus = (Integer) session.update("trip.insertTripAccountEntry", map);
                    System.out.println("status2 = " + insertStatus);
                }

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Approval Request Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "ApprovalRequest", sqlException);
        }

        return addAdvanceRequest;
    }

//Arun 11 Dec End
//Mathan Starts 19-12-2013
    /**
     * This method used to save City List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int saveCityMaster(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int zoneId = 0;
        int saveCityMaster = 0;
        int user = userId;
        try {
            map.put("zoneId", operationTO.getZoneId());
            map.put("cityName", operationTO.getCityName());

            map.put("cityCode", operationTO.getCityCode());
            map.put("countryId", operationTO.getCountryId());
            //map.put("countryName", operationTO.getCountryName());
            map.put("latitude", operationTO.getLatitude());
            map.put("longitute", operationTO.getLongitute());
            map.put("icdLocation", operationTO.getIcdLocation());
            map.put("googleCityName", operationTO.getGoogleCityName());
            System.out.println("map 1= " + map);
            saveCityMaster = (Integer) getSqlMapClientTemplate().insert("operation.saveCity", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveCity Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveCity", sqlException);
        }
        return saveCityMaster;
    }

    public ArrayList getZoneList(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList zoneList = new ArrayList();
        map.put("zoneName", operationTO.getZoneName() + "%");
        System.out.println("getZoneList==" + map);
        try {
            zoneList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getZoneList", map);
            //////System.out.println(" getZoneList =" + zoneList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getZoneList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getZoneList List", sqlException);
        }

        return zoneList;
    }

    /**
     * This method used to Get Route List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getCityMaster(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList cityMasterList = new ArrayList();
        map.put("cityId", operationTO.getCityId());

        //////System.out.println("map = " + map);
        try {
            //////System.out.println("this is City Master");
            cityMasterList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCityMaster", map);
            //////System.out.println(" cityMasterList =" + cityMasterList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getcityMaster List", sqlException);
        }

        return cityMasterList;
    }

    /**
     * This method used to Modify standardCharge .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int updateCityMaster(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int user = userId;

        map.put("cityName", operationTO.getCityName());
        map.put("zoneName", operationTO.getZoneName());
        map.put("zoneId", operationTO.getZoneId());
        map.put("cityId", operationTO.getCityId());
        map.put("status", operationTO.getStatus());

        map.put("cityCode", operationTO.getCityCode());
        map.put("countryId", operationTO.getCountryId());
        map.put("latitude", operationTO.getLatitude());
        map.put("longitute", operationTO.getLongitute());
        map.put("borderStatus", operationTO.getBorderStatus());
//        map.put("icdLocation", operationTO.getIcdLocation());
        map.put("googleCityName", operationTO.getGoogleCityName());
        try {

            status = (Integer) getSqlMapClientTemplate().update("operation.updateCityMaster", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateCityMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateCityMaster", sqlException);
        }

        return status;
    }

    /**
     * This method used to get Unit Master .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
//    public int saveCheckList(OperationTO operationTO, int userId) {
//        Map map = new HashMap();
//        int saveCheckList = 0;
//        int user = userId;
//        try {
//            map.put("userId", user);
//            map.put("name", operationTO.getCheckListName());
//            map.put("stage", operationTO.getCheckListStage());
//            map.put("effectiveDate", operationTO.getCheckListDate());
//            map.put("status", operationTO.getCheckListStatus());
//            saveCheckList = (Integer) getSqlMapClientTemplate().update("operation.saveCheckList", map);
//
//        } catch (Exception sqlException) {
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("saveCheckList Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveCheckList ", sqlException);
//        }
//        return saveCheckList;
//    }

    /**
     * This method used to Get Check List .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
//    public ArrayList getCheckList() {
//        Map map = new HashMap();
//        ArrayList checkList = new ArrayList();
//
//        try {
//            //////System.out.println("this is Check List");
//            checkList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCheckList", map);
//        } catch (Exception sqlException) {
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("getCheckList Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCheckList", sqlException);
//        }
//        return checkList;
//    }

    /**
     * This method used to Update standardCharge .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
//    public int updateCheckList(OperationTO operationTO, int userId) {
//        Map map = new HashMap();
//        int status = 0;
//        int user = userId;
//
//        map.put("name", operationTO.getCheckListName());
//        map.put("stage", operationTO.getCheckListStage());
//        map.put("effectiveDate", operationTO.getCheckListDate());
//        map.put("checkListId", operationTO.getCheckListId());
//        map.put("status", operationTO.getCheckListStatus());
//
//        try {
//
//            status = (Integer) getSqlMapClientTemplate().update("operation.updateCheckList", map);
//
//        } catch (Exception sqlException) {
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("updateCheckList Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateCheckList", sqlException);
//        }
//
//        return status;
//    }

    /**
     * This method used to get Stage Master .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getStageMasterList() {
        Map map = new HashMap();
        ArrayList stageList = new ArrayList();
        try {
            stageList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getStageMasterList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getStageMasterList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getStageMasterList", sqlException);
        }

        return stageList;
    }

    /**
     * This method used to saveAlterList .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int saveAlterList(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int saveAlterList = 0;
        int user = userId;
        try {
            map.put("userId", user);
            map.put("alertid", operationTO.getAlertid());
            map.put("name", operationTO.getAlertName());
            map.put("description", operationTO.getAlertDes());
            map.put("basedon", operationTO.getAlertBased());
            map.put("alertraised", operationTO.getAlertRaised());
            map.put("alertto", operationTO.getAlerttoEmail());
            map.put("alertcc", operationTO.getAlertccEmail());
            map.put("mailsubject", operationTO.getAlertsemailSub());
            map.put("alertfrequency", operationTO.getAlertsfrequently());
            map.put("escalateunit", operationTO.getAlertRaised1());
            map.put("escalateto", operationTO.getEscalationtoEmail1());
            map.put("escalatecc", operationTO.getEscalationccEmail1());
            map.put("escalatesubject", operationTO.getEscalationemailSub1());
            map.put("alertTime", operationTO.getAlertTime());
            map.put("escalateAlertTime", operationTO.getEscalationAlertTime());
            map.put("repeat", operationTO.getRepeat());
            map.put("status", operationTO.getAlertstatus());
            System.out.println("mapalert==" + map);

            saveAlterList = (Integer) getSqlMapClientTemplate().update("operation.saveAlterList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveAlterList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveAlterList ", sqlException);
        }
        return saveAlterList;
    }

    /**
     * This method used to Get AlertList .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getAlertList() {
        Map map = new HashMap();
        ArrayList alertList = new ArrayList();

        try {
            //////System.out.println("this is get Alert List");
            alertList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getAlertList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAlertList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getAlertList", sqlException);
        }
        return alertList;
    }

    /**
     * This method used to Update standardCharge .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int updateAlertList(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int user = userId;

        map.put("userId", user);
        map.put("alertid", operationTO.getAlertid());
        map.put("name", operationTO.getAlertName());
        map.put("description", operationTO.getAlertDes());
        map.put("basedon", operationTO.getAlertBased());
        map.put("alertraised", operationTO.getAlertRaised());
        map.put("alertto", operationTO.getAlerttoEmail());
        map.put("alertcc", operationTO.getAlertccEmail());
        map.put("mailsubject", operationTO.getAlertsemailSub());
        map.put("alertfrequency", operationTO.getAlertsfrequently());
        map.put("escalateunit", operationTO.getAlertRaised1());
        map.put("escalateto", operationTO.getEscalationtoEmail1());
        map.put("escalatecc", operationTO.getEscalationccEmail1());
        map.put("escalatesubject", operationTO.getEscalationemailSub1());
        map.put("alertTime", operationTO.getAlertTime());
        map.put("escalateAlertTime", operationTO.getEscalationAlertTime());
        map.put("repeat", operationTO.getRepeat());
        map.put("status", operationTO.getAlertstatus());
        System.out.println("map update==" + map);

        try {

            status = (Integer) getSqlMapClientTemplate().update("operation.updateAlertList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateAlertList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateAlertList", sqlException);
        }

        return status;
    }

    /**
     * This method used to get Alert Master .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getalertfrequency() {
        Map map = new HashMap();
        ArrayList frequencyList = new ArrayList();
        try {
            frequencyList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getalertfrequency", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getalertfrequency Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getalertfrequency", sqlException);
        }

        return frequencyList;
    }

    //    configdetails
    /**
     * This method used to Get Unit List .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int saveConfigDetails(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int saveConfigDetails = 0;
        int user = userId;
        try {
            map.put("userId", user);
            map.put("parameterName", operationTO.getParameterName());
            map.put("parameterValue", operationTO.getParameterValue());
            map.put("parameterUnit", operationTO.getParameterUnit());
            map.put("id", operationTO.getId());
            map.put("parameterDescription", operationTO.getParameterDescription());
            map.put("status", operationTO.getStatus());
            //////System.out.println("map = " + map);  //checking the insert value map
            saveConfigDetails = (Integer) getSqlMapClientTemplate().update("operation.saveConfigDetails", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveConfigDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveConfigDetails ", sqlException);
        }
        return saveConfigDetails;
    }

    /**
     * This method used to GetProductCategory .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getConfigDetailsLists() {
        Map map = new HashMap();
        ArrayList productCategoryList = new ArrayList();

        try {
            //////System.out.println("this is standard charges");
            productCategoryList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getConfigDetailsLists", map);
            //////System.out.println(" productCategoryList =" + productCategoryList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConfigDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConfigDetails", sqlException);
        }
        return productCategoryList;
    }

    /**
     * This method used to Modify ProductCategory .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int updateConfigDetails(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int user = userId;

        map.put("userId", user);
        map.put("parameterName", operationTO.getParameterName());
        map.put("parameterValue", operationTO.getParameterValue());
        map.put("parameterUnit", operationTO.getParameterUnit());
        map.put("id", operationTO.getId());
        map.put("parameterDescription", operationTO.getParameterDescription());
        map.put("status", operationTO.getStatus());

        try {

            status = (Integer) getSqlMapClientTemplate().update("operation.updateConfigDetails", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateConfigDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateConfigDetails", sqlException);
        }

        return status;
    }

    /**
     * This method used to Get Unit List .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int saveProductCategory(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int saveProductCategory = 0;
        int user = userId;
        try {
            map.put("userId", user);
            map.put("productCategoryName", operationTO.getProductCategoryName());
            map.put("reeferRequired", operationTO.getReeferRequired());
            map.put("reeferMinimumTemperature", operationTO.getReeferMinimumTemperature());
            map.put("reeferMaximumTemperature", operationTO.getReeferMaximumTemperature());
            map.put("status", operationTO.getStatus());
            saveProductCategory = (Integer) getSqlMapClientTemplate().update("operation.saveProductCategory", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveProductCategory Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveProductCategory ", sqlException);
        }
        return saveProductCategory;
    }

    /**
     * This method used to GetProductCategory .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getProductCategory() {
        Map map = new HashMap();
        ArrayList productCategoryList = new ArrayList();

        try {
            //////System.out.println("this is standard charges");
            productCategoryList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getProductCategory", map);
            //////System.out.println(" productCategoryList =" + productCategoryList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getProductCategory Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getProductCategory", sqlException);
        }
        return productCategoryList;
    }

    public ArrayList getCountry() {
        Map map = new HashMap();
        ArrayList getCountryList = new ArrayList();

        try {
            System.out.println("this is country");
            getCountryList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCountryList", map);
            System.out.println(" getCountryList =" + getCountryList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getProductCategory Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getProductCategory", sqlException);
        }
        return getCountryList;
    }

    /**
     * This method used to Modify ProductCategory .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int updateProductCategory(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int user = userId;

        map.put("userId", user);
        map.put("productCategoryName", operationTO.getProductCategoryName());
        map.put("reeferRequired", operationTO.getReeferRequired());
        map.put("reeferMinimumTemperature", operationTO.getReeferMinimumTemperature());
        map.put("reeferMaximumTemperature", operationTO.getReeferMaximumTemperature());
        map.put("productCategoryId", operationTO.getProductCategoryId());
        map.put("status", operationTO.getStatus());

        try {

            status = (Integer) getSqlMapClientTemplate().update("operation.updateProductCategory", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateProductCategory Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateProductCategory", sqlException);
        }

        return status;
    }

    public int insertCurrencyMaster(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int user = userId;

        map.put("userId", user);
        map.put("fromcountryId", operationTO.getFromcurrency());
        map.put("currencyVal1", operationTO.getFromcurrencyvalue());
        map.put("tocountryId", operationTO.getTocurrency());
        map.put("currencyVal2", operationTO.getTocurrencyvalue());
        map.put("fromdate", operationTO.getFromdate());
        map.put("todate", operationTO.getTodate());
        map.put("status", operationTO.getStatus());

        try {

            status = (Integer) getSqlMapClientTemplate().insert("operation.inserCurrencyMaster", map);

        } catch (Exception sqlException) {

            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateProductCategory Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateProductCategory", sqlException);
        }

        return status;
    }

    public int updateCurrencyMaster(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int user = userId;

        map.put("userId", user);
        map.put("fromcountryId", operationTO.getFromcurrency());
        map.put("currencyId", Integer.parseInt(operationTO.getTocurrencyId()));
        map.put("currencyVal1", operationTO.getFromcurrencyvalue());
        map.put("tocountryId", operationTO.getTocurrency());
        map.put("currencyVal2", operationTO.getTocurrencyvalue());
        map.put("fromdate", operationTO.getFromdate());
        map.put("todate", operationTO.getTodate());
        map.put("status", operationTO.getStatus());

        try {
            ArrayList getCurrencyList = new ArrayList();
            String fdate, todate;
            getCurrencyList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCurrencyList", map);
            //////System.out.println(" getCurrencyList =" + getCurrencyList.size());
            if (getCurrencyList != null) {
                Iterator itr = getCurrencyList.iterator();
                if (itr.hasNext()) {
                    operationTO = new OperationTO();
                    operationTO = (OperationTO) itr.next();
                    fdate = operationTO.getFromdate();
                    todate = operationTO.getTodate();
                    if (map.get("fromdate").toString().equals(fdate) && map.get("todate").toString().equals(todate)) {
                        status = (Integer) getSqlMapClientTemplate().update("operation.updateCurrencyMaster", map);
                    } else {
                        status = (Integer) getSqlMapClientTemplate().insert("operation.inserCurrencyMaster", map);
                        status = (Integer) getSqlMapClientTemplate().update("operation.updateCurrencyMasterInactive", map);
                        //////System.out.println("My Status Is: " + status);
                        status = 5;
                    }
                }
            }

        } catch (Exception sqlException) {

            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateProductCategory Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateProductCategory", sqlException);
        }

        return status;
    }
//Mathan Ends 10-12-2013

//Senthil Starts 10-12-2013
    /**
     * This method used to get closed trips.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getClosedTripList(int userId) {
        Map map = new HashMap();
        ArrayList list = null;
        try {
            map.put("userId", userId);
            //////System.out.println("getClosedTripList map" + map);
            list = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getClosedTripList", map);
            //////System.out.println("getClosedTripList  size is ::::" + list.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getClosedTripList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getClosedTripList", sqlException);
        }
        return list;
    }

    /**
     * This method used to get trip expense.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public double getTripExpense(String tripId) {
        Map map = new HashMap();
        double totalExpense = 0.0;
        try {
            map.put("tripId", tripId);
            //////System.out.println("getTripExpense map" + map);
            totalExpense = (Double) getSqlMapClientTemplate().queryForObject("operation.getTotalExpense", map);
            //////System.out.println("getTripExpense size is ::::" + totalExpense);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripExpense", sqlException);
        }
        return totalExpense;
    }

    /**
     * This method used to get trip expense.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public double getTotalFreaghtAmt(String cnoteNo) {
        Map map = new HashMap();
        double totalFreight = 0.0;
        try {
            map.put("cnoteNo", cnoteNo);
            //////System.out.println("getCnote Value map" + map);
            totalFreight = (Double) getSqlMapClientTemplate().queryForObject("operation.getTotalFreight", map);
            //////System.out.println("getTotalFreaghtAmt size is ::::" + totalFreight);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTotalFreaghtAmt Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTotalFreaghtAmt", sqlException);
        }
        return totalFreight;
    }

    /**
     * This method used to total drivers.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int getTotalDrivers(String tripId) {
        Map map = new HashMap();
        int totalDrivers = 0;
        try {
            map.put("tripId", tripId);
            //////System.out.println("getCnote Value map" + map);
            totalDrivers = (Integer) getSqlMapClientTemplate().queryForObject("operation.getTotalDrivers", map);
            //////System.out.println("getTotalDrivers size is ::::" + totalDrivers);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTotalDrivers Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTotalDrivers", sqlException);
        }
        return totalDrivers;
    }

    /**
     * This method used to total drivers.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getInvoiceCustomerDetails(String headerTripId) {
        Map map = new HashMap();
        OperationTO opto = null;
        ArrayList invoiceCustomerList = null;
        try {
            map.put("tripId", headerTripId);
            //////System.out.println("getCnote Value map" + map);
            invoiceCustomerList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getTripDetailList", map);
            //////System.out.println("getInvoiceCustomerDetails size is ::::" + invoiceCustomerList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return invoiceCustomerList;
    }

    /**
     * This method used to insert invoice details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int updateInvoiceHeader(double lastinsertId, String invoiceCode, String invoiceRefCode) {
        Map map = new HashMap();
        int lastInvoiceId = 0;
        try {
            map.put("lastinsertId", lastinsertId);
            map.put("invoiceCode", invoiceCode);
            map.put("invoiceRefCode", invoiceRefCode);

            //////System.out.println("update header deati;ls map" + map);
            int status = (Integer) getSqlMapClientTemplate().update("operation.updateInvoiceHeaderDetails", map);
            //////System.out.println("Invoice Deatls Table = " + lastInvoiceId);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerList List", sqlException);
        }
        return lastInvoiceId;
    }

    /**
     * This method used to total drivers.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public OperationTO getTripInvoiceCustomerDetails(String headerTripId) {
        Map map = new HashMap();
        OperationTO opto = null;
        ArrayList invoiceCustomerList = null;
        try {
            map.put("tripId", headerTripId);
            //////System.out.println("getCnote Value map" + map);
            invoiceCustomerList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getTripDetailList", map);
            //////System.out.println("getInvoiceCustomerDetails size is ::::" + invoiceCustomerList.size());
            opto = (OperationTO) invoiceCustomerList.get(0);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return opto;
    }

    /**
     * This method used to update trip.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int updateTripStatus(String tripId) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripId);
            //////System.out.println("getCnote Value map" + map);
            status = (Integer) getSqlMapClientTemplate().update("operation.updateBilledTripStatus", map);
            //////System.out.println("getInvoiceCustomerDetails size is ::::" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return status;
    }

    /**
     * This method used to get closed bill.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getClosedBilledList(String customerId, String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList closedBillList = null;
        try {
            map.put("customerId", customerId);
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            //////System.out.println("getClosedbill Value map" + map);
            closedBillList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getClosedBilledList", map);
            //////System.out.println("getbil details size is ::::" + closedBillList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return closedBillList;
    }

    public ArrayList viewBillForSubmission(String customerId, String fromDate, String toDate, String tripType) {
        Map map = new HashMap();
        ArrayList closedBillList = null;
        try {
            map.put("customerId", customerId);
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("tripType", tripType);
            //////System.out.println("getClosedbill Value map" + map);
            closedBillList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.viewBillForSubmission", map);
            //////System.out.println("getbil details size is ::::" + closedBillList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return closedBillList;
    }

    /**
     * This method used to get header Details
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public OperationTO getInvoiceHeaderDetails(String invoicecode) {
        Map map = new HashMap();
        ArrayList invoiceList = null;
        OperationTO opto = null;

        try {
            map.put("invoicecode", invoicecode);

            //////System.out.println("getinvoice deatails Value map" + map);
            invoiceList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getInvoiceHeaderDetails", map);
            //////System.out.println("getbil details size is ::::" + invoiceList.size());
            opto = (OperationTO) invoiceList.get(0);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return opto;
    }

    /**
     * This method used to get headersub Details
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getInvoiceHeaderSubDeatil(String invoicecode) {
        Map map = new HashMap();
        ArrayList invoicedetailList = null;

        try {
            map.put("invoicecode", invoicecode);
            //////System.out.println("getinvoice deatails Value map" + map);
            invoicedetailList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getInvoiceHeaderSubDeatil", map);
            //////System.out.println("getbil details size is ::::" + invoicedetailList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return invoicedetailList;
    }

    /**
     * This method used to get last invoice Id.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertInvoiceHeader(String customerName, String customerId, String billingType, int userId, String remark, String grandTotal) {
        Map map = new HashMap();
        int lastInvoiceId = 0;
        try {
            map.put("customerName", customerName);
            map.put("customerId", customerId);
            map.put("billingType", billingType);
            map.put("userId", userId);
            map.put("remark", remark);
            map.put("grandTotal", grandTotal);
            //////System.out.println("header map::::" + map);
            lastInvoiceId = (Integer) getSqlMapClientTemplate().insert("operation.insertInvoiceHeader", map);
            //////System.out.println("Last INvoice Id = " + lastInvoiceId);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerList List", sqlException);
        }
        return lastInvoiceId;
    }

    public int submitBill(String invoiceId, int userId) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("invoiceId", invoiceId);
            map.put("userId", userId);
            //////System.out.println("map::::" + map);
            status = (Integer) getSqlMapClientTemplate().update("operation.updateTripStatusForBillingSubmission", map);

            status = (Integer) getSqlMapClientTemplate().update("operation.saveTripStatusDetailsForBillingSubmission", map);
            status = (Integer) getSqlMapClientTemplate().update("operation.updateBillSubmitStatusFinanceHeader", map);

            //////System.out.println("status = " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerList List", sqlException);
        }
        return status;
    }

    /**
     * This method used to insert invoice details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertInvoiceDetails(String invoiceCode, String tripId, String cnoteId, String customerName, String custonerId, String tripStartDate, String destination, String invoiceRefCode, String freightamt, String totalweights, int userId) {
        Map map = new HashMap();
        int lastInvoiceId = 0;
        try {
            map.put("invoiceCode", invoiceCode);
            map.put("tripId", tripId);
            map.put("cnoteId", cnoteId);
            map.put("totalweights", totalweights);
            map.put("customerName", customerName);
            map.put("custId", custonerId);
            map.put("tripDate", tripStartDate);
            map.put("destination", destination);
            map.put("userId", userId);
            map.put("invoiceRefCode", invoiceRefCode);
            map.put("freightamt", freightamt);

            //////System.out.println("insert header deati;ls map" + map);
            lastInvoiceId = (Integer) getSqlMapClientTemplate().insert("operation.insertInvoiceHeaderDetails", map);
            //////System.out.println("Invoice Deatls Table = " + lastInvoiceId);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerList List", sqlException);
        }
        return lastInvoiceId;
    }

    /**
     * This method used to get vehicle Id
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int handleVehicleId(String vehicleRegNo) {
        Map map = new HashMap();
        int vehicleId = 0;

        try {
            map.put("vehicleNo", vehicleRegNo);
            //////System.out.println("vehicle No map---" + map);
            vehicleId = (Integer) getSqlMapClientTemplate().queryForObject("operation.handleVehicleId", map);
            //////System.out.println("vehicle No" + vehicleId);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("handleVehicleId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "handleVehicleId", sqlException);
        }
        return vehicleId;

    }

    /**
     * This method used to insert trip planning
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertTripPlanning(OperationTO opTo) {
        Map map = new HashMap();
        int status = 0;

        try {
            map.put("userId", opTo.getUserId());
            map.put("consigmentOrderId", opTo.getConsigmentId());
            map.put("vehicleId", opTo.getVehicleId());
            map.put("tripPlanningRemakr", opTo.getTripPlanningRemark());

            //////System.out.println("insert Trip Planning map---" + map);
            status = (Integer) getSqlMapClientTemplate().insert("operation.insertTripPlanning", map);
            //////System.out.println("insert Trip Planning status" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertTripPlanning Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertTripPlanning", sqlException);
        }
        return status;

    }

    /**
     * This method used to handleCnotexl Details
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList handleCnotexlDetail(int userId, String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList consignmentList = null;
        try {
            map.put("userId", userId);
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            //////System.out.println("cnote  Xl Deatils map is---" + map);

            consignmentList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.handleCnotexlDetail", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("handleCnotexlDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "handleCnotexlDetail", sqlException);
        }
        return consignmentList;
    }

//Senthil Ends 10-12-2013
    public ArrayList gettripClosureRequest(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList gettripClosureRequest = new ArrayList();
        map.put("fromdate", operationTO.getFromDate());
        map.put("todate", operationTO.getToDate());
        map.put("tripType", operationTO.getTripType());
        try {
            gettripClosureRequest = (ArrayList) getSqlMapClientTemplate().queryForList("operation.gettripClosureRequest", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("gettripClosureRequest Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "gettripClosureRequest", sqlException);
        }
        return gettripClosureRequest;
    }

    public ArrayList viewTripClosureRequest(String tripid, String tripclosureid) {
        Map map = new HashMap();
        ArrayList viewTripClosureRequest = new ArrayList();
        map.put("tripid", tripid);
        map.put("tripclosureid", tripclosureid);

        try {
            viewTripClosureRequest = (ArrayList) getSqlMapClientTemplate().queryForList("operation.viewTripClosureRequest", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("viewTripClosureRequest Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "viewTripClosureRequest", sqlException);
        }
        return viewTripClosureRequest;
    }

    public int saveTripClosureapprove(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int saveTripClosureapprove = 0;

        try {

            map.put("remarks", operationTO.getApproveremarks());
            map.put("status", operationTO.getApprovalstatus());
            map.put("tripid", operationTO.getTripid());
            map.put("tripclosureid", operationTO.getTripclosureid());
            map.put("userId", userId);

            System.out.println(map + "===map");
            saveTripClosureapprove = (Integer) getSqlMapClientTemplate().update("operation.saveTripClosureapprove", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripClosureapprove Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripClosureapprove", sqlException);
        }
        return saveTripClosureapprove;
    }

    public String checkCheckListName(OperationTO operationTO) {
        Map map = new HashMap();
        String checkCheckListName = "";
        try {
            //////System.out.println("map = " + map);
            map.put("checkListName", operationTO.getCheckListName());
            checkCheckListName = (String) getSqlMapClientTemplate().queryForObject("operation.checkCheckListName", map);
            //////System.out.println("checkCheckListName " + checkCheckListName);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkStandardChargeName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkStandardChargeName", sqlException);
        }

        return checkCheckListName;
    }

    public String checkCityName(OperationTO operationTO) {
        Map map = new HashMap();
        String checkCityName = "";
        try {
            //////System.out.println("map = " + map);
            map.put("cityName", operationTO.getCityName());
            checkCityName = (String) getSqlMapClientTemplate().queryForObject("operation.checkCityName", map);
            //////System.out.println("checkCityName " + checkCityName);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkCityName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkCityName", sqlException);
        }

        return checkCityName;
    }

    public String checkParameterName(OperationTO operationTO) {
        Map map = new HashMap();
        String checkParameterName = "";
        try {
            //////System.out.println("map = " + map);
            map.put("parameterName", operationTO.getParameterName());
            checkParameterName = (String) getSqlMapClientTemplate().queryForObject("operation.checkParameterName", map);
            //////System.out.println("checkParameterName " + checkParameterName);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkParameterName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkParameterName", sqlException);
        }

        return checkParameterName;
    }

    public String checkproductCategoryName(OperationTO operationTO) {
        Map map = new HashMap();
        String checkproductCategoryName = "";
        try {
            //////System.out.println("map = " + map);
            map.put("productCategoryName", operationTO.getProductCategoryName());
            checkproductCategoryName = (String) getSqlMapClientTemplate().queryForObject("operation.checkproductCategoryName", map);
            //////System.out.println("checkproductCategoryName " + checkproductCategoryName);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkproductCategoryName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkproductCategoryName", sqlException);
        }

        return checkproductCategoryName;
    }

    public String checkStandardChargeName(OperationTO operationTO) {
        Map map = new HashMap();
        String checkStandardChargeName = "";
        try {
            //////System.out.println("map = " + map);
            map.put("stChargeName", operationTO.getStChargeName());
            checkStandardChargeName = (String) getSqlMapClientTemplate().queryForObject("operation.checkStandardChargeName", map);
            //////System.out.println("checkStandardChargeName " + checkStandardChargeName);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkStandardChargeName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkStandardChargeName", sqlException);
        }

        return checkStandardChargeName;
    }

    public String getCnoteCodeSequence() {
        Map map = new HashMap();
        int cnoteCodeSequence = 0;
        try {
            cnoteCodeSequence = (Integer) getSqlMapClientTemplate().insert("operation.getCnoteCodeSequence", map);

            //////System.out.println("cnoteCodeSequence=" + cnoteCodeSequence);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCnoteCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCnoteCodeSequence", sqlException);
        }
        return cnoteCodeSequence + "";
    }

    public String getCnoteCodeSequence(SqlMapClient session) {
        Map map = new HashMap();
        int cnoteCodeSequence = 0;
        String cnoteCodeSequences = "";
        try {
            cnoteCodeSequence = (Integer) session.insert("operation.getCnoteCodeSequence", map);
            cnoteCodeSequences = cnoteCodeSequence + "";
            if (cnoteCodeSequences.length() == 1) {
                cnoteCodeSequences = "0000" + cnoteCodeSequences;
                System.out.println("invoice no 1.." + cnoteCodeSequences);
            } else if (cnoteCodeSequences.length() == 2) {
                cnoteCodeSequences = "000" + cnoteCodeSequences;
                System.out.println("invoice lenght 2.." + cnoteCodeSequences);
            } else if (cnoteCodeSequences.length() == 3) {
                cnoteCodeSequences = "00" + cnoteCodeSequences;
                System.out.println("invoice lenght 3.." + cnoteCodeSequences);
            } else if (cnoteCodeSequences.length() == 4) {
                cnoteCodeSequences = "0" + cnoteCodeSequences;
            }
            //////System.out.println("cnoteCodeSequence=" + cnoteCodeSequence);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCnoteCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCnoteCodeSequence", sqlException);
        }
        return cnoteCodeSequences;
    }

    public String getVehicleRegNo(String vehicleId) {
        Map map = new HashMap();
        map.put("vehicleId", vehicleId);
        String result = "";
        try {
            result = (String) getSqlMapClientTemplate().queryForObject("operation.getVehicleRegNo", map);
            //////System.out.println("result=" + result);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleRegNo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleRegNo", sqlException);
        }
        return result;
    }

    /**
     * This method used to Get Consignor Name For Ajax.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getConsignorName(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList consignorName = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("cosignorName", operationTO.getConsignorName() + "%");
        map.put("customerId", operationTO.getCustomerId());
        //////System.out.println("map = " + map);
        try {
            consignorName = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getConsignorName", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignorName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getConsignorName", sqlException);
        }
        return consignorName;
    }

    /**
     * This method used to Get Consignee Name For Ajax.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getConsigneeName(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList consigneeName = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("cosigneeName", operationTO.getConsigneeName() + "%");
        map.put("customerId", operationTO.getCustomerId());
        //////System.out.println("map = " + map);
        try {
            consigneeName = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getConsigneeName", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsigneeName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getConsigneeName", sqlException);
        }
        return consigneeName;
    }

    /**
     * This method used to User Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getUserDetails(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList userDetails = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userName", operationTO.getUserName() + "%");
        //////System.out.println("map = " + map);
        try {
            userDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getUserDetails", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserDetails", sqlException);
        }
        return userDetails;
    }

    /**
     * This method used to Get Customer List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getCustomerList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList customerList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", operationTO.getUserId());
        //////System.out.println("map = " + map);
        try {
            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCustomersList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomersList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCustomersList", sqlException);
        }
        return customerList;
    }

    /**
     * This method used to Get User Customer List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getUserCustomer(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList userCustomerList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", operationTO.getUserId());
        //////System.out.println("map = " + map);
        try {
            userCustomerList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getUserCustomer", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserCustomer Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserCustomer", sqlException);
        }
        return userCustomerList;
    }

    /**
     * This method used to Get User Customer List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int deleteUserCustomer(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int delete = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("user", operationTO.getUserId());
        map.put("userId", userId);
        //////System.out.println("map = " + map);
        try {
            delete = (Integer) getSqlMapClientTemplate().update("operation.deleteUserCustomer", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateUserCustomer Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateUserCustomer", sqlException);
        }
        return delete;
    }

    public int saveUserCustomer(String[] customerId, OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        int status = 0;
        try {
            map.put("user", operationTO.getUserId());
            map.put("userId", userId);
            status = (Integer) getSqlMapClientTemplate().update("operation.deleteUserCustomer", map);
            for (int i = 0; i < customerId.length; i++) {
                ArrayList getUserCustomers = new ArrayList();
                map.put("customerId", customerId[i]);
                getUserCustomers = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getUserCustomers", map);
                //////System.out.println("size is " + getUserCustomers.size());
                if (getUserCustomers.size() != 0) {
                    //////System.out.println("i am in update");
                    status = (Integer) getSqlMapClientTemplate().update("operation.updateUserCustomer", map);
                } else {
                    //////System.out.println("i am in insert");
                    status = (Integer) getSqlMapClientTemplate().update("operation.insertUserCustomer", map);
                }
            }
            //////System.out.println("status is in deleterolesuser " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("delete&save userroles Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "delete&save userroles", sqlException);
        }
        return status;
    }

    public String getConsignmentOrderId(String consignmentNo) {
        Map map = new HashMap();
        OperationTO operationTO = null;
        map.put("consignmentNo", consignmentNo);
        String consignmentId = "";
        //////System.out.println("map = " + map);
        try {
            consignmentId = (String) getSqlMapClientTemplate().queryForObject("operation.getConsignmentOrderId", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentOrderId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getConsignmentOrderId", sqlException);
        }
        return consignmentId;

    }

    public String getConsignmentStatus(String consignmentId) {
        Map map = new HashMap();
        OperationTO operationTO = null;
        map.put("consignmentId", consignmentId);
        String consignmentStatus = "";
        //////System.out.println("map = " + map);
        try {
            consignmentStatus = (String) getSqlMapClientTemplate().queryForObject("operation.getConsignmentStatus", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getConsignmentStatus", sqlException);
        }
        return consignmentStatus;

    }

    public String getConsignmentTripPlanStatus(String consignmentId) {
        Map map = new HashMap();
        OperationTO operationTO = null;
        map.put("consignmentId", consignmentId);
        String consignmentStatus = "";
        //////System.out.println("map = " + map);
        try {
            consignmentStatus = (String) getSqlMapClientTemplate().queryForObject("operation.getConsignmentTripPlanStatus", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentTripPlanStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getConsignmentTripPlanStatus", sqlException);
        }
        return consignmentStatus;

    }

    public String getVehicleTripPlanStatus(String consignmentId, String vehicleId) {
        Map map = new HashMap();
        OperationTO operationTO = null;
        map.put("consignmentId", consignmentId);
        map.put("vehicleId", vehicleId);
        String consignmentStatus = "";
        //////System.out.println("map = " + map);
        try {
            consignmentStatus = (String) getSqlMapClientTemplate().queryForObject("operation.getVehicleTripPlanStatus", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleTripPlanStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehicleTripPlanStatus", sqlException);
        }
        return consignmentStatus;

    }

    public String getTripPlanVehicleId(String vehicleNo) {
        Map map = new HashMap();
        OperationTO operationTO = null;
        map.put("vehicleNo", vehicleNo);
        String tripPlanVehicleId = "";
        //////System.out.println("map = " + map);
        try {
            tripPlanVehicleId = (String) getSqlMapClientTemplate().queryForObject("operation.getTripPlanVehicleId", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripPlanVehicleId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTripPlanVehicleId", sqlException);
        }
        return tripPlanVehicleId;

    }

    public int insertUploadTripPlanning(String[] cNoteId, String[] vehId, int userId) {
        Map map = new HashMap();
        OperationTO operationTO = null;
        int insertStatus = 0;
        String checkConsignment = "";
        //////System.out.println("map = " + map);
        try {
            map.put("userId", userId);
            for (int i = 0; i < cNoteId.length; i++) {
                map.put("consignmentId", cNoteId[i]);
                map.put("vehicleId", vehId[i]);
                //////System.out.println("map = " + map);
                if (!"0".equals(cNoteId[i]) && !"0".equals(vehId[i])) {
                    checkConsignment = (String) getSqlMapClientTemplate().queryForObject("operation.checkConsignment", map);
                    //////System.out.println("checkConsignment = " + checkConsignment);
                    if (checkConsignment == null) {
                        insertStatus = (Integer) getSqlMapClientTemplate().update("operation.insertUploadTripPlanning", map);
                    } else {
                        insertStatus = (Integer) getSqlMapClientTemplate().update("operation.updateUploadTripPlanning", map);
                    }
                }
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertUploadTripPlanning Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertUploadTripPlanning", sqlException);
        }
        return insertStatus;

    }

    /**
     * This method used to Get Trip Planned Vehicle List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getTripPlannedVehicleList(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList consignmentList = new ArrayList();
        String customerId = "";
        String orderReferenceNo = "";
        String consignmentNoteNo = "";
        String status = "";
        String fromDate = "";
        String toDate = "";
        if (operationTO.getCustomerId() != null) {
            customerId = operationTO.getCustomerId();
        } else {
            customerId = "";
        }
        if (operationTO.getOrderReferenceNo() != null) {
            orderReferenceNo = operationTO.getOrderReferenceNo();
        } else {
            orderReferenceNo = "";
        }
        if (operationTO.getConsignmentNoteNo() != null) {
            consignmentNoteNo = operationTO.getConsignmentNoteNo();
        } else {
            consignmentNoteNo = "";
        }
        if (operationTO.getStatus() != null) {
            status = operationTO.getStatus();
        } else {
            status = "";
        }
        if (operationTO.getFromDate() != null) {
            fromDate = operationTO.getFromDate();
        } else {
            fromDate = "";
        }
        if (operationTO.getToDate() != null) {
            toDate = operationTO.getToDate();
        } else {
            toDate = "";
        }
        map.put("customerId", customerId);
        map.put("customerOrderReferenceNo", orderReferenceNo);
        map.put("consignmentOrderReferenceNo", consignmentNoteNo);
        map.put("status", status);
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        //////System.out.println("mpa = " + map);
        try {
            consignmentList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getTripPlannedVehicleList", map);
            //////System.out.println("routeDetailsList " + consignmentList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripPlannedVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripPlannedVehicleList List", sqlException);
        }

        return consignmentList;
    }

    public int insertConsignmentRoutePlan(String consigmentId, String pointId, String pointType, String pointSequence, String pointPlanDate, String pointPlanTime, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", userId);
        map.put("consignmentId", consigmentId);
        map.put("pointId", pointId);
        map.put("pointRouteId", "0");
        map.put("pointType", pointType);
        map.put("pointSequence", pointSequence);
        map.put("pointAddress", "");
        map.put("routeId", 0);
        map.put("pointPlanDate", pointPlanDate);
        map.put("pointPlanTime", pointPlanTime);
        //////System.out.println("map:" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("operation.insertMultiplePoints", map);
            //////System.out.println("Success=" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertConsignmentRoutePlan Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertConsignmentRoutePlan", sqlException);
        }

        return status;
    }

    /**
     * This method used to get Customer Details
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList checkCustomerAvailability(String customerName, String customerCode) {
        Map map = new HashMap();
        ArrayList customerList = null;
        try {
            map.put("customerName", customerName + "%");
            map.put("customerCode", customerCode);
            System.out.println(map + "===map");
            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.checkCustomerAvailability", map);
            //////System.out.println("customerList size is :::" + customerList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripClosureapprove Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripClosureapprove", sqlException);
        }
        return customerList;
    }

    /**
     * This method used to check vehile Type
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList checkConsignmentVehicleType(String vehicleType) {
        Map map = new HashMap();
        ArrayList vehicleList = null;
        try {
            map.put("vehicleType", "%" + vehicleType + "%");
            System.out.println(map + "===vehicle list map");
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.checkConsignmentVehicleType", map);
            //////System.out.println("vehicleList size is :::" + vehicleList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripClosureapprove Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripClosureapprove", sqlException);
        }
        return vehicleList;
    }

    /**
     * This method used to city ids
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String getConsignmentCity(String cityName) {
        Map map = new HashMap();
        String cityId = "";
        try {
            map.put("cityName", cityName);
            System.out.println(map + "===map");
            if (getSqlMapClientTemplate().queryForList("operation.getConsignmentCity", map) != null) {
                cityId = (String) getSqlMapClientTemplate().queryForObject("operation.getConsignmentCity", map);
            }
            //////System.out.println("vehicleList size is :::" + cityId);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripClosureapprove Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripClosureapprove", sqlException);
        }
        return cityId;
    }

    public String getEmailToList(String custId) {
        Map map = new HashMap();
        String result = "";
        try {
            map.put("custId", custId);
            System.out.println(map + "===map");
            result = (String) getSqlMapClientTemplate().queryForObject("operation.getEmailToList", map);
            //////System.out.println("getEmailToListe is :::" + result);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmailToList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEmailToList", sqlException);
        }
        return result;
    }

    public String getEmailToListForConsignment(String custId) {
        Map map = new HashMap();
        String result = "";
        try {
            map.put("custId", custId);
            System.out.println(map + "===map");
            result = (String) getSqlMapClientTemplate().queryForObject("operation.getEmailToListForConsignment", map);
            //////System.out.println("getEmailToListe is :::" + result);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmailToList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEmailToList", sqlException);
        }
        return result;
    }

    public String getConfirmationMail(String confirmationCode) {
        Map map = new HashMap();
        String result = "";
        try {
            map.put("confirmationCode", confirmationCode);
            System.out.println(map + "===map");
            result = (String) getSqlMapClientTemplate().queryForObject("operation.getConfirmationMail", map);
            //////System.out.println("getConfirmationMail is :::" + result);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConfirmationMail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConfirmationMail", sqlException);
        }
        return result;
    }

    /**
     * This method used to get routeContractId
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public OperationTO handleRouteContractDetails(String point1RouteId, String point2RouteId, String point3RouteId, String point4RouteId, String finalPoint) {
        Map map = new HashMap();
        OperationTO opto = null;
        ArrayList routeList = null;
        try {
            map.put("point1RouteId", point1RouteId);
            map.put("point2RouteId", point2RouteId);
            map.put("point3RouteId", point3RouteId);
            map.put("point4RouteId", point4RouteId);
            map.put("finalPoint", finalPoint);
            System.out.println(map + "===map");
            if (getSqlMapClientTemplate().queryForObject("operation.getContractRouteDetails", map) != null) {
                routeList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getContractRouteDetails", map);
                //////System.out.println("getConsignmentRouteContract is  :::" + routeList);
                opto = (OperationTO) routeList.get(0);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentRouteContract Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConsignmentRouteContract", sqlException);
        }
        return opto;
    }

    public OperationTO handleRouteDetailsNew(String point1RouteId, String point2RouteId, String point3RouteId, String point4RouteId, String finalPoint) {
        Map map = new HashMap();
        OperationTO opto = null;
        ArrayList routeList = null;
        try {
            map.put("point1RouteId", point1RouteId);
            map.put("point2RouteId", point2RouteId);
            map.put("point3RouteId", point3RouteId);
            map.put("point4RouteId", point4RouteId);
            map.put("finalPoint", finalPoint);
            System.out.println(map + "===map");
            if (getSqlMapClientTemplate().queryForObject("operation.getContractDetailsNew", map) != null) {
                routeList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getContractDetailsNew", map);
                //////System.out.println("getConsignmentRouteContract is  :::" + routeList);
                opto = (OperationTO) routeList.get(0);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentRouteContract Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConsignmentRouteContract", sqlException);
        }
        return opto;
    }

    public OperationTO handleRouteContractDetailsNew(String originId, String point1Id, String point2Id, String point3Id,
            String point4Id, String finalpointId, String contractId) {
        Map map = new HashMap();
        OperationTO opto = null;
        ArrayList routeList = null;
        try {
            map.put("originId", originId);
            map.put("point1Id", point1Id);
            map.put("point2Id", point2Id);
            map.put("point3Id", point3Id);
            map.put("point4Id", point4Id);
            map.put("finalpointId", finalpointId);
            map.put("contractId", contractId);
            System.out.println(map + "===srini map");

            routeList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getContractRouteDetailsNew", map);
            //////System.out.println("getConsignmentRouteContract is  :::" + routeList.size());
            if (routeList.size() > 0) {
                opto = (OperationTO) routeList.get(0);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentRouteContract Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConsignmentRouteContract", sqlException);
        }
        return opto;
    }

    /**
     * This method used to get route Id
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String handleConsignRoute(String fromCityId, String toCityId) {
        Map map = new HashMap();
        String routeId = "";
        try {
            map.put("fromCityId", fromCityId);
            map.put("toCityId", toCityId);

            System.out.println(map + "===map");
            if (getSqlMapClientTemplate().queryForList("operation.handleConsignRoute", map) != null) {
                routeId = (String) getSqlMapClientTemplate().queryForObject("operation.handleConsignRoute", map);
            }
            //////System.out.println("handleConsignRoute size is :::" + routeId);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripClosureapprove Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripClosureapprove", sqlException);
        }
        return routeId;
    }

    /**
     * This method used to get product Ctaegory
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList checkProductCategory(String prodCategory) {
        Map map = new HashMap();
        ArrayList prodList = null;
        OperationTO opto = null;
        try {
            map.put("prodCategory", prodCategory + "%");
            System.out.println(map + "===map");

            prodList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.handleProductCategory", map);
            //////System.out.println("handleConsignRoute size is :::" + prodList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkProductCategory Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkProductCategory", sqlException);
        }
        return prodList;
    }

    /**
     * This method used to get contract Route Details
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public OperationTO handleContractRateDetails(String vehicleTypeId, int contractId, String routeContractId) {
        Map map = new HashMap();
        ArrayList prodList = null;
        OperationTO opto = null;
        try {
            map.put("vehicleTypeId", vehicleTypeId);
            map.put("contractId", contractId);
            map.put("routeContractId", routeContractId);

            System.out.println(map + "===map");

            prodList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.handleContractRateDetails", map);
            //////System.out.println("handleContractRateDetails size is :::" + prodList.size());
            if (prodList.size() > 0) {
                opto = (OperationTO) prodList.get(0);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkProductCategory Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkProductCategory", sqlException);
        }
        return opto;
    }

    /**
     * This method used to insert cnote
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int saveImportConsignmentNote(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int insertConsignmentNote = 0;
        int updateStandardCharge = 0;
        int index = 0;
        map.put("userId", userId);
        try {
            map.put("consignmentNoteNo", operationTO.getConsignmentNoteNo());
            map.put("consignmentRefNo", operationTO.getConsignmentRefNo());
            map.put("consignmentOrderRefRemarks", operationTO.getConsignmentOrderRefRemarks());
            map.put("customerType", operationTO.getCustomerTypeName());
            map.put("productCategory", operationTO.getProductCategory());
            map.put("entryType", "2");
            map.put("customerId", operationTO.getCustomerId());
            map.put("customerName", operationTO.getCustomerName());
            map.put("customerAddress", operationTO.getCustomerAddress());
            map.put("customerPhone", operationTO.getCustomerPhone());
            map.put("customerMobile", operationTO.getCustomerMobile());
            map.put("customerEmail", operationTO.getCustomerEmail());
            map.put("billingTypeId", operationTO.getBillingTypeId());
            map.put("routeContractId", operationTO.getRouteContractId());
            map.put("routeId", "0");
            map.put("contractRateId", "0");
            map.put("fromLocation", operationTO.getFromLocation());
            map.put("toLocation", operationTO.getToLocation());
            map.put("businessType", "1");
            map.put("multipickup", "N");
            map.put("multiDelivery", "N");
            map.put("consignmentOrderInstruction", operationTO.getConsignmentOrderRefRemarks());
            map.put("serviceType", "1");
            map.put("vehicleTypeId", operationTO.getVehicleId());
            if (operationTO.getReeferRequired().equalsIgnoreCase("Y"));
            map.put("reeferRequired", "YES");
            if (operationTO.getReeferRequired().equalsIgnoreCase("N")) {
                map.put("reeferRequired", "NO");
            }
            map.put("requiredDate", operationTO.getPickupdate());
            map.put("requiredTime", operationTO.getPickupTime());
            map.put("vehicleRemark", "NA");
            map.put("consignorName", operationTO.getConsignorName());
            map.put("consignorAddress", operationTO.getConsignorAddress());
            map.put("consignorPhoneNo", operationTO.getConsignorPhoneNo());
            map.put("consigneeName", operationTO.getConsigneeName());
            map.put("consigneeAddress", operationTO.getConsigneeAddress());
            map.put("consigneePhoneNo", operationTO.getConsigneePhoneNo());
            map.put("totalPackages", "0");
            map.put("totalWeight", "0");
            map.put("totalDistance", operationTO.getTotalKm());
            map.put("totalHours", operationTO.getTotalHours());
            map.put("freightCharge", "0");
            map.put("standardCharge", "0");
            //////System.out.println("cnote dat map " + map);

            insertConsignmentNote = (Integer) getSqlMapClientTemplate().insert("operation.saveImportConsignmentNote", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkProductCategory Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkProductCategory", sqlException);
        }
        return insertConsignmentNote;
    }

    public int checkCustomerOrdRefNo(String orderNo) {
        Map map = new HashMap();
        OperationTO operationTO = null;
        map.put("orderNo", orderNo);
        int status = 0;
        //////System.out.println("map = " + map);
        try {
            status = (Integer) getSqlMapClientTemplate().queryForObject("operation.checkCustomerOrdRefNo", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkCustomerOrdRefNo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkCustomerOrdRefNo", sqlException);
        }
        return status;

    }

    public int updateCustomerContract(String routeContractId, String contractRateId, String ptpRateWithReefer, String ptpRateWithoutReefer, String validStatus, String fromDate, String toDate,
            String ptpRateWithReeferOld, String ptpRateWithoutReeferOld, String validStatusOld, OperationTO operationTO,
            String fuelVehicle, String fuelDg, String totalFuel, String toll, String driverBachat, String dala, String misc,
            String fuelVehicleOld, String fuelDgOld, String totalFuelOld, String tollOld, String driverBachatOld, String dalaOld, String miscOld, String approvalFlag,
            String marketHire11) {
        Map map = new HashMap();
        String withReefer = "";
        String withoutReefer = "";
        String activeInd = "";

//      new values
        map.put("routeContractId", routeContractId);
        map.put("approvalFlag", approvalFlag);
        map.put("contractRateId", contractRateId);
        map.put("ptpRateWithReefer", ptpRateWithReefer);
        map.put("ptpRateWithoutReefer", ptpRateWithoutReefer);
        map.put("customerId", operationTO.getCustId());
        map.put("userId", operationTO.getUserId());
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("fuelVehicleOld", fuelVehicleOld);
        map.put("fuelDgOld", fuelDgOld);
        map.put("totalFuelOld", totalFuelOld);
        map.put("tollOld", tollOld);
        map.put("driverBachatOld", driverBachatOld);
        map.put("dalaOld", dalaOld);
        map.put("miscOld", miscOld);
        map.put("fuelVehicle", fuelVehicle);
        map.put("fuelDg", fuelDg);
        map.put("totalFuel", totalFuel);
        map.put("toll", toll);
        map.put("driverBachat", driverBachat);
        map.put("dala", dala);
        map.put("misc", misc);
        map.put("marketHire", marketHire11);
        //
        map.put("validStatus", validStatus);
        System.out.println("updateCustomerContract 111111 ------------- map --------------- " + map);

        int status = 0;
        int tripRevenueUpdate = 0;
        try {
            //withreefer
            if (ptpRateWithReeferOld.equals(ptpRateWithReefer)) {
                withReefer = "0";
                map.put("ptpRateWithReeferOld", "0.00");
                map.put("ptpRateWithReeferNew", "0.00");
                System.out.println("withReefer=" + withReefer);
            } else {
                withReefer = ptpRateWithReefer;
                map.put("ptpRateWithReeferOld", ptpRateWithReeferOld);
                map.put("ptpRateWithReeferNew", ptpRateWithReefer);
            }
            //withoutreefer
            if (ptpRateWithoutReeferOld.equals(ptpRateWithoutReefer)) {
                withoutReefer = "0";
                map.put("ptpRateWithoutReeferOld", "0.00");
                map.put("ptpRateWithoutReeferNew", "0.00");
                System.out.println("withoutReefer=" + withoutReefer);
            } else {
                withoutReefer = ptpRateWithoutReefer;
                map.put("ptpRateWithoutReeferOld", ptpRateWithoutReeferOld);
                map.put("ptpRateWithoutReeferNew", ptpRateWithoutReefer);
            }
            //activeInd
            if (validStatusOld.equals(validStatus)) {
                activeInd = "0";
                map.put("validStatusOld", "0");
                map.put("validStatusNew", "0");
                System.out.println("activeInd=" + activeInd);
            } else {
                System.out.println("DAO else active Ind");
                activeInd = validStatus;
                System.out.println("activeIndDAO+" + activeInd);
                map.put("validStatusOld", validStatusOld);
                map.put("validStatusNew", validStatus);
            }
            System.out.println("map@@@@" + map);

            if ("0".equals(withReefer) && "0".equals(withoutReefer) && "0".equals(activeInd)) {
                System.out.println("IFLOG!!0=");
            } else {
                status = (Integer) getSqlMapClientTemplate().update("operation.insertCustomerContractRateLog", map);
                System.out.println("LOG=" + status);
            }

            status = (Integer) getSqlMapClientTemplate().update("operation.updateCustomerContractRate", map);
            System.out.println("update" + status);

            // update all trip revnue whose bill has not created yet. For DICT New Case 08/04/2016
            String ambientTripIds = (String) getSqlMapClientTemplate().queryForObject("operation.ambientTripIds", map);
            String[] tempTripIds = null;
            if (ambientTripIds != null && !"".equals(ambientTripIds) && ambientTripIds.length() > 0) {
                tempTripIds = ambientTripIds.split(",");
                List tripIds = new ArrayList(tempTripIds.length);
                for (int i = 0; i < tempTripIds.length; i++) {
                    System.out.println("tripId:" + tempTripIds[i]);
                    tripIds.add(tempTripIds[i]);
                }
                map.put("ambientTripIds", tripIds);

                System.out.println("map for ambient:" + map);
                if (!"0".equals(ptpRateWithoutReefer) && !"0.00".equals(ptpRateWithoutReefer)) {
                    //update trip carry ambient product
                    tripRevenueUpdate = (Integer) getSqlMapClientTemplate().update("operation.updateAmbientTripRevenue", map);
                }
            }
            String frozenTripId = (String) getSqlMapClientTemplate().queryForObject("operation.frozenTripIds", map);
            String[] tempFrozenTripIds = null;
            if (frozenTripId != null && !"".equals(frozenTripId) && frozenTripId.length() > 0) {
                tempFrozenTripIds = frozenTripId.split(",");
                List frozenTripIds = new ArrayList(tempFrozenTripIds.length);
                for (int i = 0; i < tempFrozenTripIds.length; i++) {
                    System.out.println("tripId:" + tempFrozenTripIds[i]);
                    frozenTripIds.add(tempFrozenTripIds[i]);
                }
                map.put("frozenTripIds", frozenTripIds);
                System.out.println("map for frozenTripIds" + map);
                if (!"0".equals(ptpRateWithReefer) && !"0.00".equals(ptpRateWithReefer)) {
                    //update trip carry chiller/frozen product
                    tripRevenueUpdate = (Integer) getSqlMapClientTemplate().update("operation.updateFrozenTripRevenue", map);
                }
            }
            int revenueUpdateStatus = (Integer) getSqlMapClientTemplate().update("operation.approveBillingRevenue", map);
            System.out.println("revenueUpdateStatus:" + revenueUpdateStatus);
            if ("N".equals(validStatus)) {
                status = (Integer) getSqlMapClientTemplate().update("operation.updateCustomerContractRoute", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkCustomerOrdRefNo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkCustomerOrdRefNo", sqlException);
        }
        return status;

    }

    public int updateCustomerContract(String routeContractId, String contractRateId, String ptpRateWithReefer, String ptpRateWithoutReefer, String validStatus, String fromDate, String toDate,
            OperationTO operationTO,
            String fuelVehicle, String fuelDg, String totalFuel, String toll, String driverBachat, String dala, String misc,
            String approvalFlag, String marketHire11, String marketHireWithReefer1, SqlMapClient session) {
        Map map = new HashMap();
        String withReefer = "";
        String withoutReefer = "";
        String activeInd = "";

//      new values
        map.put("routeContractId", routeContractId);
        map.put("approvalFlag", approvalFlag);
        map.put("contractRateId", contractRateId);
        map.put("ptpRateWithReefer", ptpRateWithReefer);
        map.put("ptpRateWithoutReefer", ptpRateWithoutReefer);
        map.put("customerId", operationTO.getCustId());
        map.put("userId", operationTO.getUserId());
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("fuelVehicle", fuelVehicle);
        map.put("fuelDg", fuelDg);
        map.put("totalFuel", totalFuel);
        map.put("toll", toll);
        map.put("driverBachat", driverBachat);
        map.put("dala", dala);
        map.put("misc", misc);
        map.put("marketHire", marketHire11);
        map.put("marketHireWithReefer", marketHireWithReefer1);
        //
        map.put("validStatus", validStatus);

        System.out.println("updateCustomerContractRateeeeeeeeeeee ---------  *********** " + map);
        int status = 0;
        int tripRevenueUpdate = 0;
        try {
            //withreefer

            status = (Integer) session.update("operation.updateCustomerContractRate", map);
            System.out.println("update" + status);
            // update all trip revnue whose bill has not created yet. For DICT New Case 08/04/2016
            String ambientTripIds = (String) session.queryForObject("operation.ambientTripIds", map);
            String[] tempTripIds = null;
            if (ambientTripIds != null && !"".equals(ambientTripIds) && ambientTripIds.length() > 0) {
                tempTripIds = ambientTripIds.split(",");
                List tripIds = new ArrayList(tempTripIds.length);
                for (int i = 0; i < tempTripIds.length; i++) {
                    System.out.println("tripId:" + tempTripIds[i]);
                    tripIds.add(tempTripIds[i]);
                }
                map.put("ambientTripIds", tripIds);

                System.out.println("map for ambient:" + map);
                if (!"0".equals(ptpRateWithoutReefer) && !"0.00".equals(ptpRateWithoutReefer)) {
                    //update trip carry ambient product
                    tripRevenueUpdate = (Integer) session.update("operation.updateAmbientTripRevenue", map);
                }
            }
            String frozenTripId = (String) session.queryForObject("operation.frozenTripIds", map);
            String[] tempFrozenTripIds = null;
            if (frozenTripId != null && !"".equals(frozenTripId) && frozenTripId.length() > 0) {
                tempFrozenTripIds = frozenTripId.split(",");
                List frozenTripIds = new ArrayList(tempFrozenTripIds.length);
                for (int i = 0; i < tempFrozenTripIds.length; i++) {
                    System.out.println("tripId:" + tempFrozenTripIds[i]);
                    frozenTripIds.add(tempFrozenTripIds[i]);
                }
                map.put("frozenTripIds", frozenTripIds);
                System.out.println("map for frozenTripIds" + map);
                if (!"0".equals(ptpRateWithReefer) && !"0.00".equals(ptpRateWithReefer)) {
                    //update trip carry chiller/frozen product
                    tripRevenueUpdate = (Integer) session.update("operation.updateFrozenTripRevenue", map);
                }
            }
            int revenueUpdateStatus = (Integer) session.update("operation.approveBillingRevenue", map);
            System.out.println("revenueUpdateStatus:" + revenueUpdateStatus);
            if ("N".equals(validStatus)) {
                status = (Integer) session.update("operation.updateCustomerContractRoute", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkCustomerOrdRefNo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkCustomerOrdRefNo", sqlException);
        }
        return status;

    }

    public int updateCustomerContract1(String routeContractId, String contractRateId, String ptpRateWithReefer, String ptpRateWithoutReefer, String validStatus) {
        Map map = new HashMap();
        OperationTO operationTO = null;
        map.put("routeContractId", routeContractId);
        map.put("contractRateId", contractRateId);
        map.put("ptpRateWithReefer", ptpRateWithReefer);
        map.put("ptpRateWithoutReefer", ptpRateWithoutReefer);
        if ("0".equals(validStatus)) {
            validStatus = "N";
        } else {
            validStatus = "Y";

        }
        map.put("validStatus", validStatus);
        int status = 0;
        //////System.out.println("map = " + map);
        try {

            status = (Integer) getSqlMapClientTemplate().update("operation.updateCustomerContractRate1", map);
            if ("N".equals(validStatus)) {
                status = (Integer) getSqlMapClientTemplate().update("operation.updateCustomerContractRoute", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkCustomerOrdRefNo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkCustomerOrdRefNo", sqlException);
        }
        return status;

    }

    public int updateCustomerContract2(String contractRateId, String vehicleRatePerKm, String reeferRatePerHour, String validStatus) {
        Map map = new HashMap();
        OperationTO operationTO = null;
        map.put("contractRateId", contractRateId);
        map.put("vehicleRatePerKm", vehicleRatePerKm);
        map.put("reeferRatePerHour", reeferRatePerHour);
        if ("0".equals(validStatus)) {
            validStatus = "N";
        } else {
            validStatus = "Y";

        }
        map.put("validStatus", validStatus);
        int status = 0;
        //////System.out.println("map = " + map);
        try {

            status = (Integer) getSqlMapClientTemplate().update("operation.updateCustomerContractRate2", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkCustomerOrdRefNo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkCustomerOrdRefNo", sqlException);
        }
        return status;

    }
    /////////////////////////senthil ends 17-12-2013///////////////////////////

    public ArrayList getEmailDetails(String activitycode) {
        ArrayList EmailDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("actcode", activitycode);
            EmailDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getEmailDetails", map);
            //////System.out.println("EmailDetails size=" + EmailDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("EmailDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "EmailDetails List", sqlException);
        }

        return EmailDetails;
    }

    public int manualAdvanceRequest(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int manualAdvanceRequest = 0;

        try {

            map.put("advancerequestamt", operationTO.getAdvancerequestamt());
            map.put("requeston", operationTO.getRequeston());
            map.put("requestremarks", operationTO.getRequestremarks());
            map.put("requeststatus", operationTO.getRequeststatus());
            map.put("tripid", operationTO.getTripid());
            map.put("tobepaidtoday", operationTO.getTobepaidtoday());
            map.put("batchType", operationTO.getBatchType());
            map.put("tripday", operationTO.getTripday());
            map.put("currencyId", operationTO.getCurrencyid());
            map.put("userId", userId);
            System.out.println("map val" + map);

            manualAdvanceRequest = (Integer) getSqlMapClientTemplate().insert("operation.manualAdvanceRequest", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Manual Approval Request Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "ManualApprovalRequest", sqlException);
        }
        return manualAdvanceRequest;
    }

    public int payManualAdvanceRequest(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int manualAdvanceRequest = 0;

        try {

            map.put("advancerequestamt", operationTO.getAdvancerequestamt());
            map.put("requeston", operationTO.getRequeston());
            map.put("requestremarks", operationTO.getRequestremarks());
            map.put("requeststatus", operationTO.getRequeststatus());
            map.put("tripid", operationTO.getTripid());
            map.put("tobepaidtoday", operationTO.getTobepaidtoday());
            map.put("batchType", operationTO.getBatchType());
            map.put("tripday", operationTO.getTripday());
            map.put("userId", userId);
            //////System.out.println("map val" + map);

            manualAdvanceRequest = (Integer) getSqlMapClientTemplate().insert("operation.payManualAdvanceRequest", map);

            int TripActualAmount = (Integer) getSqlMapClientTemplate().queryForObject("operation.getActualPaidVal", map);
            map.put("TripActualAmount", TripActualAmount);
            System.out.println(map);
            int updatetripamount = (Integer) getSqlMapClientTemplate().update("operation.updateTripActualAmount", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Manual Approval Request Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "ManualApprovalRequest", sqlException);
        }
        return manualAdvanceRequest;
    }

    public double getPreStartExpense(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        double preStartExpense = 0;
        try {
            map.put("tripId", operationTO.getTripId());
            //////System.out.println("preStartExpense = " + map);
            preStartExpense = (Double) getSqlMapClientTemplate().queryForObject("operation.getPreStartExpense", map);
            //////System.out.println("preStartExpense = " + preStartExpense);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPreStartExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPreStartExpense", sqlException);
        }

        return preStartExpense;
    }

    public ArrayList getmanualfinanceAdvDetails(String tripid) {
        ArrayList manualfinanceAdvDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("tripid", tripid);
            manualfinanceAdvDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getmanualfinanceAdvDetails", map);
            //////System.out.println("manualfinanceAdvDetails size=" + manualfinanceAdvDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("manualfinanceAdvDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "manualfinanceAdvDetails List", sqlException);
        }

        return manualfinanceAdvDetails;
    }

    public double getfcRequestamount(int reqid) {
        Map map = new HashMap();
        double getfcRequestamount = 0;

        try {

            map.put("reqid", reqid);
            //////System.out.println("map get request amount = " + map);
            getfcRequestamount = (Double) getSqlMapClientTemplate().queryForObject("operation.getfcRequestamount", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Fc Request Amount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getfcRequestamount", sqlException);
        }
        return getfcRequestamount;
    }

    public double gettotalReqamount(String tripid, String advdate) {
        Map map = new HashMap();
        double gettotalReqamount = 0;

        try {
            map.put("tripid", tripid);
            map.put("advdate", advdate);
            //////System.out.println("map:" + map);
            gettotalReqamount = (Double) getSqlMapClientTemplate().queryForObject("operation.gettotalReqamount", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Today Total Request Amount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "gettotalReqamount", sqlException);
        }
        return gettotalReqamount;
    }

    public ArrayList getapprovalValueDetails(double actualval, String tripId) {
        ArrayList getapprovalValueDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("actualval", actualval);
            map.put("tripId", tripId);
            //////System.out.println("map:" + map);
            getapprovalValueDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getapprovalValueDetails", map);
            //////System.out.println("getapprovalValueDetails size=" + getapprovalValueDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getapprovalValueDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getapprovalValueDetails List", sqlException);
        }

        return getapprovalValueDetails;
    }

    public ArrayList getTripDeviationDetails(String tripId, int manualAdvanceRequest) {
        ArrayList tripDeviationDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("tripId", tripId);
            map.put("tripAdvanceId", manualAdvanceRequest);
            //////System.out.println("map:" + map);
            tripDeviationDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getTripDeviationDetails", map);
            //////System.out.println("getTripDeviationDetails size=" + tripDeviationDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDeviationDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDeviationDetails List", sqlException);
        }

        return tripDeviationDetails;
    }

    public int manualAdvanceApprove(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int manualAdvanceApprove = 0;

        try {

            map.put("advancerequestamt", operationTO.getAdvancerequestamt());
            map.put("requeston", operationTO.getRequeston());
            map.put("requestremarks", operationTO.getRequestremarks());
            map.put("tripid", operationTO.getTripid());
            map.put("tobepaidtoday", operationTO.getTobepaidtoday());
            map.put("batchType", operationTO.getBatchType());
            map.put("tripday", operationTO.getTripday());
            map.put("currencyId", operationTO.getCurrencyid());
            map.put("userId", userId);

            manualAdvanceApprove = (Integer) getSqlMapClientTemplate().update("operation.manualAdvanceApprove", map);
//            updateDriverAdvanceToEFS(operationTO.getTripid());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("manual Advance Approve Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "manualAdvanceApprove", sqlException);
        }
        return manualAdvanceApprove;
    }

    public ArrayList getapprovalFCValueDetails() {
        ArrayList getapprovalFCValueDetails = new ArrayList();
        Map map = new HashMap();
        try {
            getapprovalFCValueDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getapprovalFCValueDetails", map);
            //////System.out.println("getapprovalFCValueDetails size=" + getapprovalFCValueDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getapprovalFCValueDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getapprovalFCValueDetails List", sqlException);
        }

        return getapprovalFCValueDetails;
    }

    public int saveManualAdvanceApproval(OperationTO operationTO, int userId, String advid) {
        Map map = new HashMap();
        int saveManualAdvanceApproval = 0;

        try {
            map.put("advanceapproveamt", operationTO.getAdvancerequestamt());
            map.put("remarks", operationTO.getApproveremarks());
            map.put("status", operationTO.getApprovalstatus());
            map.put("tripid", operationTO.getTripid());
            map.put("userId", userId);
            map.put("adviceid", advid);
            map.put("mailId", operationTO.getMailId());
            map.put("fuelTypeId", operationTO.getFuelTypeId());

            System.out.println(map + "===map");
            if (operationTO.getFuelTypeId().equals("1002")) {
                saveManualAdvanceApproval = (Integer) getSqlMapClientTemplate().update("operation.saveManualAdvanceApproval", map);
            } else if (operationTO.getFuelTypeId().equals("1003")) {
                saveManualAdvanceApproval = (Integer) getSqlMapClientTemplate().update("operation.saveManualAdvanceApprovalCNG", map);
            }
//            updateDriverAdvanceToEFS(operationTO.getTripid());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveManualAdvanceApproval Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveManualAdvanceApproval", sqlException);
        }
        return saveManualAdvanceApproval;
    }

    public int getManualAdvanceApprovalStatus(String advid) {
        Map map = new HashMap();
        int responseValue = 0;

        try {
            map.put("adviceid", advid);
            System.out.println(map + "===map");
            responseValue = (Integer) getSqlMapClientTemplate().queryForObject("operation.getManualAdvanceApprovalStatus", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getManualAdvanceApprovalStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getManualAdvanceApprovalStatus", sqlException);
        }
        return responseValue;
    }

    public int updateConsignmentDetails(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int updateConsignmentDetails = 0;
        try {
            try {
                map.put("consignmentId", operationTO.getConsignmentOrderId());
                map.put("consignmentRefNo", operationTO.getOrderReferenceNo());
                map.put("consignmentRefRemarks", operationTO.getOrderReferenceRemarks());
                map.put("productCategoryId", operationTO.getProductCategoryId());
                map.put("consignmentOrderInstruction", operationTO.getConsignmentOrderInstruction());
                map.put("vehicleTypeId", operationTO.getVehicleTypeId());
                map.put("vehicleInstruction", operationTO.getVehicleInstruction());
                map.put("consignorName", operationTO.getConsignorName());
                map.put("consignorPhoneNo", operationTO.getConsignorPhoneNo());
                map.put("consignorAddress", operationTO.getConsignorAddress());
                map.put("consigneeName", operationTO.getConsigneeName());
                map.put("consigneePhoneNo", operationTO.getConsigneePhoneNo());
                map.put("consigneeAddress", operationTO.getConsigneeAddress());
                map.put("reeferRequired", operationTO.getReeferRequired());
                map.put("totalKm", operationTO.getTotalKm());
                map.put("totalHours", operationTO.getTotalHours());
                map.put("routeContractId", operationTO.getRouteContractId());
                map.put("routeId", operationTO.getRouteId());
                map.put("contractRateId", operationTO.getContractRateId());
                map.put("origin", operationTO.getOrigin());
                map.put("destination", operationTO.getDestination());
                map.put("totalMinutes", operationTO.getTotalMinutes());
                map.put("totalPackage", operationTO.getTotalPackage());
                map.put("totalWeightage", operationTO.getTotalWeightage());
                map.put("freightAmount", operationTO.getTotFreightAmount());
                //cfs
                map.put("billEntryDate", operationTO.getBillEntryDate());
                map.put("billEntryNo", operationTO.getBillEntryNo());
                map.put("occDate", operationTO.getOccDate());
                map.put("dutyPaymentDate", operationTO.getDutyPaymentDate());
                map.put("cfsPersonName", operationTO.getCfsPersonName());
                map.put("cfsPersonNo", operationTO.getCfsPersonNo());

                String[] volume = null;
//             volume=operationTO.getProductVolume();
//             //////System.out.println("volume is"+volume[0]);
                map.put("ordVolume", operationTO.getProductVolume());

                if (operationTO.getLocationId() != "" && !"NaN".equals(operationTO.getLocationId())) {
                    map.put("locationId", operationTO.getLocationId());
                } else {
                    map.put("locationId", "");
                }
                if (operationTO.getContainerNo() != "" && !"NaN".equals(operationTO.getContainerNo())) {
                    map.put("containerNo", operationTO.getContainerNo());
                } else {
                    map.put("containerNo", "");
                }
                if (operationTO.getContainerId() != "" && !"NaN".equals(operationTO.getContainerId())) {
                    map.put("containerId", operationTO.getContainerId());
                } else {
                    map.put("containerId", "");
                }
                map.put("userId", userId);
                if (operationTO.getPointPlanDate()[0] != "" || operationTO.getPointPlanDate()[0] != null) {

                    map.put("pointPlanDate", operationTO.getPointPlanDate()[0]);
                } else {
                    map.put("pointPlanDate", "2015-04-10");

                }
                if (operationTO.getPointPlanHour()[0] != "" || operationTO.getPointPlanHour()[0] != null) {

                    map.put("pointPlanTime", operationTO.getPointPlanHour()[0] + ":" + operationTO.getPointPlanMinute()[0]);
                } else {
                    map.put("pointPlanTime", operationTO.getPointPlanTimeTemp());
                }

                //////System.out.println("map for update = " + map);
                updateConsignmentDetails = (Integer) getSqlMapClientTemplate().update("operation.updateConsignmentDetails", map);
            } catch (Exception ex) {
                ex.printStackTrace();
                Writer writer = new StringWriter();
                PrintWriter printWriter = new PrintWriter(writer);
                ex.printStackTrace(printWriter);
                String s = writer.toString();
                //////System.out.println("s = " + s);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertConsignmentNote Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertConsignmentNote", sqlException);
        }

        return updateConsignmentDetails;
    }

    public int updateConsignmentArticles(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int updateConsignmentArticles = 0;
        try {
            try {
                map.put("consignmentId", operationTO.getConsignmentOrderId());
                map.put("userId", userId);
                for (int i = 0; i < operationTO.getArticleId().length; i++) {
                    if (!"0".equals(operationTO.getArticleId()[i])) {
                        //update
                        map.put("consignmentArticleId", operationTO.getArticleId()[i]);
                        map.put("productCodes", operationTO.getProductCodes()[i]);
                        map.put("batchCode", operationTO.getBatchCode()[i]);
                        map.put("uom", operationTO.getUom()[i]);
                        map.put("productName", operationTO.getProductNames()[i]);
                        if ("".equals(operationTO.getPackagesNos()[i])) {
                            map.put("packageNos", 0);
                        } else {
                            map.put("packageNos", operationTO.getPackagesNos()[i]);
                        }
                        if ("".equals(operationTO.getWeights()[i])) {
                            map.put("weights", 0);
                        } else {
                            map.put("weights", operationTO.getWeights()[i]);
                        }
                        //////System.out.println("map = " + map);
                        updateConsignmentArticles = (Integer) getSqlMapClientTemplate().update("operation.updateConsignmentArticles", map);
                    } else {
                        //insert
                        map.put("productCode", operationTO.getProductCodes()[i]);
                        map.put("producName", operationTO.getProductNames()[i]);
                        map.put("batch", operationTO.getBatchCode()[i]);
                        map.put("uom", operationTO.getUom()[i]);
                        map.put("packageNos", operationTO.getPackagesNos()[i]);
                        map.put("weights", operationTO.getWeights()[i]);
                        //////System.out.println("map = " + map);
                        if (!"".equals(operationTO.getPackagesNos()[i]) && !"".equals(operationTO.getWeights()[i]) || !"".equals(operationTO.getProductNames()[i])) {
                            updateConsignmentArticles = (Integer) getSqlMapClientTemplate().update("operation.insertConsignmentArticle", map);
                        }
                    }
                }

            } catch (Exception ex) {
                Writer writer = new StringWriter();
                PrintWriter printWriter = new PrintWriter(writer);
                ex.printStackTrace(printWriter);
                String s = writer.toString();
                //////System.out.println("s = " + s);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertConsignmentNote Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertConsignmentNote", sqlException);
        }

        return updateConsignmentArticles;
    }

    public int deleteMultiplePoints(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int deletePoints = 0;
        try {
            try {
                map.put("consignmentId", operationTO.getConsignmentOrderId());
                map.put("userId", userId);
                deletePoints = (Integer) getSqlMapClientTemplate().update("operation.deletePoints", map);
            } catch (Exception ex) {
                Writer writer = new StringWriter();
                PrintWriter printWriter = new PrintWriter(writer);
                ex.printStackTrace(printWriter);
                String s = writer.toString();
                //////System.out.println("s = " + s);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertConsignmentNote Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertConsignmentNote", sqlException);
        }

        return deletePoints;
    }

    public double gettotalPaidamount(String tripid, String advdate) {
        Map map = new HashMap();
        double gettotalPaidamount = 0;

        try {
            map.put("tripid", tripid);
            map.put("advdate", advdate);
            gettotalPaidamount = (Double) getSqlMapClientTemplate().queryForObject("operation.gettotalPaidamount", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Today Total Paid Amount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "gettotalPaidamount", sqlException);
        }
        return gettotalPaidamount;
    }

    public double getovertotalPaidamount(String tripid) {
        Map map = new HashMap();
        double getovertotalPaidamount = 0;

        try {
            map.put("tripid", tripid);
            getovertotalPaidamount = (Double) getSqlMapClientTemplate().queryForObject("operation.getovertotalPaidamount", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Today Total Over Paid Amount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "gettotalOverPaidamount", sqlException);
        }
        return getovertotalPaidamount;
    }

    public double getEstimatedamount(String tripid, String advdate) {
        Map map = new HashMap();
        double getEstimatedamount = 0;

        try {
            map.put("tripid", tripid);
            map.put("advdate", advdate);
            getEstimatedamount = (Double) getSqlMapClientTemplate().queryForObject("operation.getEstimatedamount", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Estimated Amount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEstimatedamount", sqlException);
        }
        return getEstimatedamount;
    }

    public ArrayList getCityNameList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList cityNameList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("cityName", operationTO.getCityName() + "%");
        //////System.out.println("map = " + map);
        try {
            cityNameList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCityNameList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityNameList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCityNameList", sqlException);
        }
        return cityNameList;
    }

    public int updateCustomerDetails(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int updateCustomerDetails = 0;
        int index = 0;
        int insertStatus = 0;
        int updateStatus = 0;
        try {
            map.put("custId", operationTO.getCustId());
            map.put("custContactPerson", operationTO.getCustContactPerson());
            map.put("custPhone", operationTO.getCustPhone());
            map.put("custMobile", operationTO.getCustMobile());
            map.put("custEmail", operationTO.getCustEmail());
            map.put("accountManagerId", operationTO.getAccountManagerId());
            map.put("creditDays", operationTO.getCreditDays());
            map.put("creditLimit", operationTO.getCreditLimit());
            map.put("userId", userId);
            map.put("paymentType", operationTO.getPaymentType());
            //////System.out.println("map 2 = " + map);
            updateStatus = (Integer) getSqlMapClientTemplate().update("operation.updateCustomer", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateCustomerDetails", sqlException);
        }

        return updateStatus;
    }

    public String getTripId(String consignmentOrderId) {
        Map map = new HashMap();
        map.put("consignmentOrderId", consignmentOrderId);
        String tripId = "";
        //////System.out.println("map = " + map);
        try {
            tripId = (String) getSqlMapClientTemplate().queryForObject("operation.getTripId", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTripId", sqlException);
        }
        return tripId;

    }

    public String getTripStatusAndWFUDay(String tripId) {
        Map map = new HashMap();
        map.put("tripId", tripId);

        //////System.out.println("map = " + map);
        try {
            tripId = (String) getSqlMapClientTemplate().queryForObject("operation.getTripStatusAndWFUDay", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripStatusAndWFUDay Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTripStatusAndWFUDay", sqlException);
        }
        return tripId;

    }

    public int updateTripMaster(OperationTO operationTO, String tripId, int userId) {
        Map map = new HashMap();
        int updateStatus = 0;
        int index = 0;
        try {
            map.put("userId", userId);
            map.put("tripId", tripId);
            map.put("originId", operationTO.getOrigin());
            map.put("destinationId", operationTO.getDestination());
            map.put("customerId", operationTO.getCustomerId());
            String routeInfo = "";
            routeInfo = (String) getSqlMapClientTemplate().queryForObject("operation.getRouteInfo", map);
            map.put("routeInfo", routeInfo);
            map.put("productCategoryId", operationTO.getProductCategoryId());
            String productInfo = "";
            productInfo = (String) getSqlMapClientTemplate().queryForObject("operation.getProductInfo", map);
            map.put("productInfo", productInfo);
            map.put("vehicleTypeId", operationTO.getVehicleTypeId());
            String vehicleTypeName = "";
            vehicleTypeName = (String) getSqlMapClientTemplate().queryForObject("operation.getVehicleTypeName", map);
            map.put("vehicleTypeName", vehicleTypeName);
            map.put("totalRevenue", operationTO.getTotalRevenue());
            map.put("totalExpense", operationTO.getTotalExp());
            map.put("reeferRequired", operationTO.getReeferRequired());
            //////System.out.println("map update trip master= " + map);
            updateStatus = (Integer) getSqlMapClientTemplate().update("operation.updateTripMaster", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTripMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateTripMaster", sqlException);
        }
        return updateStatus;
    }

    public int deleteTripPoints(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int deletePoints = 0;
        try {
            try {
                map.put("consignmentId", operationTO.getConsignmentOrderId());
                map.put("tripId", operationTO.getTripId());
                map.put("userId", userId);
                //////System.out.println("map delete trip Points= " + map);
                deletePoints = (Integer) getSqlMapClientTemplate().update("operation.deleteTripPoints", map);
            } catch (Exception ex) {
                Writer writer = new StringWriter();
                PrintWriter printWriter = new PrintWriter(writer);
                ex.printStackTrace(printWriter);
                String s = writer.toString();
                //////System.out.println("s = " + s);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("deleteTripPoints Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "deleteTripPoints", sqlException);
        }

        return deletePoints;
    }

    public int insertTripPoints(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int insertTripPoints = 0;
        int index = 0;
        map.put("userId", userId);
        String hr = "";
        String min = "";
        try {
            try {
                String consignmentId = operationTO.getConsignmentOrderId();
                String[] pointId = operationTO.getPointId();
                String[] pointRouteId = operationTO.getPointRouteId();
                String[] pointType = operationTO.getPointType();
                String[] pointSequence = operationTO.getOrder();
                String[] pointAddress = operationTO.getPointAddresss();
                String[] routeId = operationTO.getMultiplePointRouteId();
                String[] pointPlanDate = operationTO.getPointPlanDate();
                String[] pointPlanHour = operationTO.getPointPlanHour();
                String[] pointPlanMinute = operationTO.getPointPlanMinute();
                //                String endPointId = operationTO.getEndPointId();
                //                String endPointType = operationTO.getEndPointType();
                //                String endOrder = operationTO.getEndOrder();
                //                String finalRouteId = operationTO.getFinalRouteId();
                //                String endPointAddress = operationTO.getEndPointAddresss();
                //                String endPointPlanDate = operationTO.getEndPointPlanDate();
                //                String endPointPlanHour = operationTO.getEndPointPlanHour();
                //                String endPointPlanMinute = operationTO.getEndPointPlanMinute();
                map.put("consignmentId", consignmentId);
                map.put("tripId", operationTO.getTripId());
                map.put("userId", userId);
                //////System.out.println("pointId.length:" + pointId.length);
                if (pointId.length > 0) {
                    for (int i = 0; i < pointId.length; i++) {
                        map.put("pointId", pointId[i]);
                        map.put("pointRouteId", pointRouteId[i]);
                        map.put("pointType", pointType[i]);
                        map.put("pointSequence", pointSequence[i]);
                        map.put("pointAddress", pointAddress[i]);
                        map.put("routeId", 0);
                        map.put("pointPlanDate", pointPlanDate[i]);
                        hr = pointPlanHour[i];
                        min = pointPlanMinute[i];
                        if ("".equals(hr)) {
                            hr = "00";
                        }
                        if ("".equals(min)) {
                            min = "00";
                        }
                        map.put("pointPlanTime", hr + ":" + min + ":00");
                        //////System.out.println("map: insert trip point details===" + map);
                        insertTripPoints = (Integer) getSqlMapClientTemplate().update("operation.insertTripPoints", map);
                    }
                    //                        map.put("pointId", endPointId);
                    //                        map.put("pointType", endPointType);
                    //                        map.put("pointSequence", endOrder);
                    //                        map.put("pointAddress", endPointAddress);
                    //                        map.put("routeId", finalRouteId);
                    //                        map.put("pointPlanDate", endPointPlanDate);
                    //                        map.put("pointPlanTime", endPointPlanHour+":"+endPointPlanMinute+":00");
                    //                        insertMultiplePoints = (Integer) getSqlMapClientTemplate().update("operation.insertMultiplePoints", map);
                }

            } catch (Exception ex) {
                Writer writer = new StringWriter();
                PrintWriter printWriter = new PrintWriter(writer);
                ex.printStackTrace(printWriter);
                String s = writer.toString();
                //////System.out.println("s = " + s);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertTripPoints Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertTripPoints", sqlException);
        }

        return insertTripPoints;
    }

    public int updateTripConsignmentArticles(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int updateConsignmentArticles = 0;
        try {
            try {
                map.put("tripId", operationTO.getTripId());
                map.put("consignmentId", operationTO.getConsignmentOrderId());
                map.put("userId", userId);
                for (int i = 0; i < operationTO.getArticleId().length; i++) {
                    if (!"0".equals(operationTO.getArticleId()[i])) {
                        //update
                        map.put("consignmentArticleId", operationTO.getArticleId()[i]);
                        map.put("productCodes", operationTO.getProductCodes()[i]);
                        map.put("batchCode", operationTO.getBatchCode()[i]);
                        map.put("uom", operationTO.getUom()[i]);
                        map.put("productName", operationTO.getProductNames()[i]);
                        if ("".equals(operationTO.getPackagesNos()[i])) {
                            map.put("packageNos", 0);
                        } else {
                            map.put("packageNos", operationTO.getPackagesNos()[i]);
                        }
                        if ("".equals(operationTO.getWeights()[i])) {
                            map.put("weights", 0);
                        } else {
                            map.put("weights", operationTO.getWeights()[i]);
                        }
                        //////System.out.println("map = " + map);
                        updateConsignmentArticles = (Integer) getSqlMapClientTemplate().update("operation.updateTripConsignmentArticles", map);
                    } else {
                        //insert
                        map.put("productCode", operationTO.getProductCodes()[i]);
                        map.put("producName", operationTO.getProductNames()[i]);
                        map.put("batch", operationTO.getBatchCode()[i]);
                        map.put("uom", operationTO.getUom()[i]);
                        map.put("packageNos", operationTO.getPackagesNos()[i]);
                        map.put("weights", operationTO.getWeights()[i]);
                        //////System.out.println("map = " + map);
                        if (!"".equals(operationTO.getPackagesNos()[i]) && !"".equals(operationTO.getWeights()[i]) || !"".equals(operationTO.getProductNames()[i])) {
                            updateConsignmentArticles = (Integer) getSqlMapClientTemplate().update("operation.insertTripConsignmentArticle", map);
                        }
                    }
                }

            } catch (Exception ex) {
                Writer writer = new StringWriter();
                PrintWriter printWriter = new PrintWriter(writer);
                ex.printStackTrace(printWriter);
                String s = writer.toString();
                //////System.out.println("s = " + s);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertConsignmentNote Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertConsignmentNote", sqlException);
        }

        return updateConsignmentArticles;
    }

    public int insertcontractProductDetails(OperationTO operationTO, int userId) {
//      String productCategoryUnchecked) {
        Map map = new HashMap();
        int insertcontractProductDetails = 0;
        int status = 0;
        int insertStatus = 0;
        int updateStatus = 0;
        try {
            String[] productCategory = null, productCategoryUnchecked = null, tempMinimumTemp = null, tempMaximumTemperature = null, tempReeferRequired = null, temp1MinimumTemp = null, temp1MaximumTemperature = null, temp1ReeferRequired = null;
            map.put("customerId", operationTO.getCustomerId());
            map.put("userId", userId);

            productCategory = operationTO.getProductCategory().split(",");
            tempMinimumTemp = operationTO.getTempMinimumTemp().split(",");
            tempMaximumTemperature = operationTO.getTempMaximumTemperature().split(",");
            tempReeferRequired = operationTO.getTempReeferRequired().split(",");

//            String productCategoryId[] = operationTO.getProductCategoryIds();
            for (int i = 0; i < productCategory.length; i++) {
                if (!"0".equals(productCategory[i])) {
                    map.put("productCategoryId", productCategory[i]);
//                //////System.out.println("productCategory" + productCategory);
                    //////System.out.println("productCategoryId" + productCategory[i]);
                    map.put("refeerRequired", tempReeferRequired[i]);
                    map.put("reeferMaximumtemperature", tempMaximumTemperature[i]);
                    map.put("reeferMinimumtemperature", tempMinimumTemp[i]);
                    map.put("activeInd", "Y");
                    //////System.out.println("map ==> " + map);
                    if ((String) getSqlMapClientTemplate().queryForObject("operation.checkCustomerProduct", map) != null) {
                        String configId = (String) getSqlMapClientTemplate().queryForObject("operation.checkCustomerProduct", map);
                        map.put("configId", configId);
                        insertStatus = (Integer) getSqlMapClientTemplate().update("operation.updatecontractProductDetails", map);
                    } else {
                        insertStatus = (Integer) getSqlMapClientTemplate().update("operation.insertcontractProductDetails", map);
                    }
                }
            }
            productCategoryUnchecked = operationTO.getProductCategoryUnchecked().split(",");
            temp1MinimumTemp = operationTO.getTemp1MinimumTemp().split(",");
            temp1MaximumTemperature = operationTO.getTemp1MaximumTemperature().split(",");
            temp1ReeferRequired = operationTO.getTemp1ReeferRequired().split(",");
            for (int i = 0; i < productCategoryUnchecked.length; i++) {
                if (!"0".equals(productCategoryUnchecked[i])) {
                    map.put("productCategoryId", productCategoryUnchecked[i]);
                    //////System.out.println("productCategoryId ==> " + productCategoryUnchecked[i]);
                    map.put("refeerRequired", temp1ReeferRequired[i]);
                    map.put("reeferMaximumtemperature", temp1MaximumTemperature[i]);
                    map.put("reeferMinimumtemperature", temp1MinimumTemp[i]);
                    map.put("activeInd", "N");
                    //////System.out.println("map ==> " + map);
                    if ((String) getSqlMapClientTemplate().queryForObject("operation.checkCustomerProduct", map) != null) {
                        String configId = (String) getSqlMapClientTemplate().queryForObject("operation.checkCustomerProduct", map);
                        map.put("configId", configId);
                        insertStatus = (Integer) getSqlMapClientTemplate().update("operation.updatecontractProductDetails", map);
                    } else {
                        insertStatus = (Integer) getSqlMapClientTemplate().update("operation.insertcontractProductDetails", map);
                    }
                }
            }
//	    insertStatus = (Integer) getSqlMapClientTemplate().insert("operation.insertcontractProductDetails", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertcontractProductDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertcontractProductDetails", sqlException);
        }

        return insertcontractProductDetails;
    }

    public ArrayList getCustomerProductCategoryList(OperationTO operationTO) {
        Map map = new HashMap();
        map.put("customerId", operationTO.getCustomerId());
        ArrayList customerProductCategoryList = new ArrayList();
        ;
        try {
            customerProductCategoryList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCustomerProductCategoryList", map);
            //////System.out.println("map 2 = " + map);
            //////System.out.println("customerProductCategoryList " + customerProductCategoryList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerProductCategoryList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerProductCategoryList", sqlException);
        }

        return customerProductCategoryList;
    }

    public String getRouteId(OperationTO operationTO) {
        Map map = new HashMap();
        map.put("originId", operationTO.getOrigin());
        map.put("destinationId", operationTO.getDestination());
        String routeId = "";
        //////System.out.println("map = " + map);
        try {
            routeId = (String) getSqlMapClientTemplate().queryForObject("operation.getRouteId", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRouteId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getRouteId", sqlException);
        }
        return routeId;

    }

    public int insertConsignmentNoteForEmptyTrip(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int insertConsignmentNote = 0;
        int updateStandardCharge = 0;
        int index = 0;
        map.put("userId", userId);
        try {
            try {
                map.put("consignmentStatusId", "5");
                map.put("orderType", "1");
                map.put("customerTypeId", operationTO.getCustomerTypeId());
                map.put("billingTypeId", operationTO.getBillingTypeId());
                map.put("entryType", operationTO.getEntryType());
                map.put("consignmentNoteNo", operationTO.getConsignmentNoteNo());
                map.put("consignmentDate", operationTO.getConsignmentDate());
                map.put("orderReferenceNo", operationTO.getOrderReferenceNo());
                map.put("orderReferenceRemarks", operationTO.getOrderReferenceRemarks());
                if (operationTO.getProductCategoryId() != "") {
                    map.put("productCategoryId", operationTO.getProductCategoryId());
                } else {
                    map.put("productCategoryId", 0);
                }
                map.put("origin", operationTO.getOrigin());
                map.put("destinaion", operationTO.getDestination());
                if (operationTO.getBusinessType() != "") {
                    map.put("businessType", operationTO.getBusinessType());
                } else {
                    map.put("businessType", 0);
                }
                if (operationTO.getMultiPickup() != null) {
                    map.put("multiPickup", operationTO.getMultiPickup());
                }
                if (operationTO.getMultiDelivery() != null) {
                    map.put("multiDelivery", operationTO.getMultiDelivery());
                }
                map.put("consignmentOrderInstruction", operationTO.getConsignmentOrderInstruction());
                if (operationTO.getTotalPackage() != "") {
                    map.put("totalPackage", operationTO.getTotalPackage());
                } else {
                    map.put("totalPackage", 0);
                }
                if (operationTO.getTotalWeightage() != "") {
                    map.put("totalWeightage", operationTO.getTotalWeightage());
                } else {
                    map.put("totalWeightage", 0);
                }
                if (operationTO.getServiceType() != "") {
                    map.put("serviceType", operationTO.getServiceType());
                } else {
                    map.put("serviceType", 0);
                }
                if (operationTO.getVehicleTypeId() != "") {
                    map.put("vehicleTypeId", operationTO.getVehicleTypeId());
                } else {
                    map.put("vehicleTypeId", 0);
                }
                map.put("reeferRequired", operationTO.getReeferRequired());
                if (operationTO.getTotalKm() != "") {
                    map.put("totalKm", operationTO.getTotalKm());
                } else {
                    map.put("totalKm", 0);
                }
                if (operationTO.getTotalHours() != "") {
                    map.put("totalHours", operationTO.getTotalHours());
                } else {
                    map.put("totalHours", 0);
                }
                if (operationTO.getTotalMinutes() != "") {
                    map.put("totalMinutes", operationTO.getTotalMinutes());
                } else {
                    map.put("totalMinutes", 0);
                }
                map.put("vehicleRequiredDate", operationTO.getVehicleRequiredDate());
                map.put("vehicleRequiredTime", operationTO.getVehicleRequiredHour() + ":" + operationTO.getVehicleRequiredMinute() + ":00");
                map.put("vehicleInstruction", operationTO.getVehicleInstruction());
                map.put("consignorName", operationTO.getConsignorName());
                map.put("consignorPhoneNo", operationTO.getConsignorPhoneNo());
                map.put("consignorAddress", operationTO.getConsignorAddress());
                map.put("consigneeName", operationTO.getConsigneeName());
                map.put("consigneePhoneNo", operationTO.getConsigneePhoneNo());
                map.put("consigneeAddress", operationTO.getConsigneeAddress());
                map.put("invoiceType", "");
                if (operationTO.getTotFreightAmount() != "") {
                    map.put("totalFreightAmount", operationTO.getTotFreightAmount());
                } else {
                    map.put("totalFreightAmount", 0);
                }
                if (operationTO.getSubTotal() != "") {
                    map.put("standardChargeTotal", operationTO.getSubTotal());
                } else {
                    map.put("standardChargeTotal", 0);
                }
                if (operationTO.getTotalCharges() != "" && !"NaN".equals(operationTO.getTotalCharges())) {
                    map.put("totalCharges", operationTO.getTotalCharges());
                } else {
                    map.put("totalCharges", 0);
                }
                map.put("totalCharges", 0);
                if (operationTO.getCustomerTypeId().equals("1")) {
                    if (operationTO.getBusinessType().equals("1")) {
                        //Primary Operation Starts
                        if (operationTO.getContractRateId() != "") {
                            map.put("contractRateId", operationTO.getContractRateId());
                        } else {
                            map.put("contractRateId", 0);
                        }

                        map.put("customerId", operationTO.getCustomerId());
                        map.put("customerName", operationTO.getCustomerName());
                        map.put("customerCode", operationTO.getCustomerCode());
                        map.put("customerAddress", operationTO.getCustomerAddress());
                        map.put("pincode", operationTO.getPincode());
                        map.put("customerMobileNo", operationTO.getCustomerMobileNo());
                        map.put("mailId", operationTO.getMailId());
                        map.put("customerPhoneNo", operationTO.getCustomerPhoneNo());
                        map.put("contractId", operationTO.getContractId());
                        float rateWithReefer = 0.00F;
                        float rateWithoutReefer = 0.00F;
                        if (operationTO.getRateWithReefer() != "") {
                            rateWithReefer = Float.parseFloat(operationTO.getRateWithReefer());
                        } else {
                            rateWithReefer = 0;
                        }
                        if (operationTO.getRateWithoutReefer() != "") {
                            rateWithoutReefer = Float.parseFloat(operationTO.getRateWithoutReefer());
                        } else {
                            rateWithoutReefer = 0;
                        }
                        if (operationTO.getBillingTypeId().equals("1")) {
                            map.put("routeId", 0);
                            map.put("routeContractId", operationTO.getRouteContractId());
                            if (operationTO.getReeferRequired().equals("Yes")) {
                                map.put("fixedRate", rateWithReefer);
                            } else if (operationTO.getReeferRequired().equals("No")) {
                                map.put("fixedRate", rateWithoutReefer);
                            } else {
                                map.put("fixedRate", "0");
                            }
                            map.put("ratePerKg", "0");
                            map.put("ratePerKm", "0");
                            //////System.out.println("map = " + map);

                            insertConsignmentNote = (Integer) getSqlMapClientTemplate().insert("operation.insertConsignmentNote", map);
                        } else if (operationTO.getBillingTypeId().equals("2")) {
                            map.put("routeId", 0);
                            map.put("routeContractId", operationTO.getRouteContractId());
                            if (operationTO.getReeferRequired().equals("Yes")) {
                                map.put("ratePerKg", rateWithReefer);
                            } else if (operationTO.getReeferRequired().equals("No")) {
                                map.put("ratePerKg", rateWithoutReefer);
                            } else {
                                map.put("ratePerKg", "0");
                            }
                            map.put("fixedRate", "0");
                            map.put("ratePerKm", "0");
                            //////System.out.println("map = " + map);
                            insertConsignmentNote = (Integer) getSqlMapClientTemplate().insert("operation.insertConsignmentNote", map);
                        } else if (operationTO.getBillingTypeId().equals("3")) {
                            map.put("routeId", operationTO.getRouteId());
                            map.put("routeContractId", 0);
                            if (operationTO.getReeferRequired().equals("Yes")) {
                                map.put("ratePerKm", rateWithReefer);
                            } else if (operationTO.getReeferRequired().equals("No")) {
                                map.put("ratePerKm", rateWithoutReefer);
                            } else {
                                map.put("ratePerKm", "0");
                            }
                            map.put("fixedRate", "0");
                            map.put("ratePerKg", "0");
                            //////System.out.println("map = " + map);
                            insertConsignmentNote = (Integer) getSqlMapClientTemplate().insert("operation.insertConsignmentNote", map);
                        }
                        //Primary Operation Ends

                    } else if (operationTO.getBusinessType().equals("2")) {
                        //Secondary Operation Starts
                        System.out.println("i m in secondary operation");
                        System.out.println("operationTO.getBillingTypeId():" + operationTO.getBillingTypeId());
                        if (operationTO.getContractRateId() != "") {
                            map.put("contractRateId", operationTO.getContractRateId());
                        } else {
                            map.put("contractRateId", 0);
                        }

                        map.put("customerId", operationTO.getCustomerId());
                        map.put("customerName", operationTO.getCustomerName());
                        map.put("customerCode", operationTO.getCustomerCode());
                        map.put("customerAddress", operationTO.getCustomerAddress());
                        map.put("pincode", operationTO.getPincode());
                        map.put("customerMobileNo", operationTO.getCustomerMobileNo());
                        map.put("mailId", operationTO.getMailId());
                        map.put("customerPhoneNo", operationTO.getCustomerPhoneNo());
                        map.put("contractId", operationTO.getContractId());
                        float rateWithReefer = 0.00F;
                        float rateWithoutReefer = 0.00F;
                        if (operationTO.getRateWithReefer() != "") {
                            rateWithReefer = Float.parseFloat(operationTO.getRateWithReefer());
                        } else {
                            rateWithReefer = 0;
                        }
                        if (operationTO.getRateWithoutReefer() != "") {
                            rateWithoutReefer = Float.parseFloat(operationTO.getRateWithoutReefer());
                        } else {
                            rateWithoutReefer = 0;
                        }
                        if (operationTO.getBillingTypeId().equals("1")) {
                            System.out.println("i m in secondary billing type 1");
                            map.put("routeId", 0);
                            map.put("routeContractId", operationTO.getRouteContractId());
                            if (operationTO.getReeferRequired().equals("Yes")) {
                                map.put("fixedRate", rateWithReefer);
                            } else if (operationTO.getReeferRequired().equals("No")) {
                                map.put("fixedRate", rateWithoutReefer);
                            } else {
                                map.put("fixedRate", "0");
                            }
                            map.put("ratePerKg", "0");
                            map.put("ratePerKm", "0");
                            //////System.out.println("map = " + map);

                            insertConsignmentNote = (Integer) getSqlMapClientTemplate().insert("operation.insertConsignmentNote", map);
                        } else if (operationTO.getBillingTypeId().equals("2")) {
                            System.out.println("i m in secondary billing type 2");
                            map.put("routeId", 0);
                            map.put("routeContractId", operationTO.getRouteContractId());
                            if (operationTO.getReeferRequired().equals("Yes")) {
                                map.put("ratePerKg", rateWithReefer);
                            } else if (operationTO.getReeferRequired().equals("No")) {
                                map.put("ratePerKg", rateWithoutReefer);
                            } else {
                                map.put("ratePerKg", "0");
                            }
                            map.put("fixedRate", "0");
                            map.put("ratePerKm", "0");
                            //////System.out.println("map = " + map);
                            insertConsignmentNote = (Integer) getSqlMapClientTemplate().insert("operation.insertConsignmentNote", map);
                        } else if (operationTO.getBillingTypeId().equals("3")) {
                            System.out.println("i m in secondary billing type 3");
                            map.put("routeId", operationTO.getRouteId());
                            map.put("routeContractId", operationTO.getRouteContractId());
                            if (operationTO.getReeferRequired().equals("Yes")) {
                                map.put("ratePerKm", rateWithReefer);
                            } else if (operationTO.getReeferRequired().equals("No")) {
                                map.put("ratePerKm", rateWithoutReefer);
                            } else {
                                map.put("ratePerKm", "0");
                            }
                            map.put("fixedRate", "0");
                            map.put("ratePerKg", "0");
                            //////System.out.println("map = " + map);
                            insertConsignmentNote = (Integer) getSqlMapClientTemplate().insert("operation.insertConsignmentNote", map);
                        }
                        //Secondary Operation Ends
                    }

                } else if (operationTO.getCustomerTypeId().equals("2")) {
                    map.put("customerName", operationTO.getWalkinCustomerName());
                    map.put("customerCode", operationTO.getWalkinCustomerCode());
                    map.put("customerAddress", operationTO.getWalkinCustomerAddress());
                    map.put("pincode", operationTO.getWalkinPincode());
                    map.put("customerMobileNo", operationTO.getWalkinCustomerMobileNo());
                    map.put("mailId", operationTO.getWalkinMailId());
                    map.put("customerPhoneNo", operationTO.getWalkinCustomerPhoneNo());
                    map.put("billingTypeId", operationTO.getWalkInBillingTypeId());
                    map.put("contractId", 0);
                    if (operationTO.getWalkInBillingTypeId().equals("1")) {
                        if (operationTO.getReeferRequired().equals("Yes")) {
                            map.put("fixedRate", operationTO.getWalkinFreightWithReefer());
                        } else if (operationTO.getReeferRequired().equals("No")) {
                            map.put("fixedRate", operationTO.getWalkinFreightWithoutReefer());
                        } else {
                            map.put("fixedRate", "0");
                        }
                        map.put("ratePerKg", "0");
                        map.put("ratePerKm", "0");
                    } else if (operationTO.getWalkInBillingTypeId().equals("2")) {
                        if (operationTO.getReeferRequired().equals("Yes")) {
                            map.put("ratePerKg", operationTO.getWalkinRateWithReeferPerKg());
                        } else if (operationTO.getReeferRequired().equals("No")) {
                            map.put("ratePerKg", operationTO.getWalkinRateWithoutReeferPerKg());
                        } else {
                            map.put("ratePerKg", "0");
                        }
                        map.put("fixedRate", "0");
                        map.put("ratePerKm", "0");
                    } else if (operationTO.getWalkInBillingTypeId().equals("3")) {
                        if (operationTO.getReeferRequired().equals("Yes")) {
                            map.put("ratePerKm", operationTO.getWalkinRateWithReeferPerKm());
                        } else if (operationTO.getReeferRequired().equals("No")) {
                            map.put("ratePerKm", operationTO.getWalkinRateWithoutReeferPerKm());
                        } else {
                            map.put("ratePerKm", "0");
                        }
                        map.put("fixedRate", "0");
                        map.put("ratePerKg", "0");
                    }

                    if (operationTO.getRouteId() != "") {
                        map.put("routeId", operationTO.getRouteId());
                    } else {
                        map.put("routeId", 0);
                    }
                    map.put("contractRateId", 0);
                    int insertCustomer = 0;
                    insertCustomer = (Integer) getSqlMapClientTemplate().insert("operation.insertCustomerDetails", map);
                    if (insertCustomer > 0) {
                        map.put("customerId", insertCustomer);
                    }
                    //////System.out.println("map = " + map);
                    insertConsignmentNote = (Integer) getSqlMapClientTemplate().insert("operation.insertConsignmentNote", map);
                }

            } catch (Exception ex) {
                Writer writer = new StringWriter();
                PrintWriter printWriter = new PrintWriter(writer);
                ex.printStackTrace(printWriter);
                String s = writer.toString();
                //////System.out.println("s = " + s);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertConsignmentNote Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertConsignmentNote", sqlException);
        }

        return insertConsignmentNote;
    }

    public ArrayList getVehicleBPCLCardMapping(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList vehicleBPCLCardMapping = new ArrayList();
        map.put("vehicleNo", "");
        try {
            vehicleBPCLCardMapping = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getVehicleBPCLCardMapping", map);
            //////System.out.println("vehicleBPCLCardMapping " + vehicleBPCLCardMapping.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleBPCLCardMapping Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleBPCLCardMapping List", sqlException);
        }

        return vehicleBPCLCardMapping;
    }

    public ArrayList getVehicleNoBPCLCardNo(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList vehicleNoBPCLCardNo = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("vehicleNo", operationTO.getVehicleNo() + "%");
        //////System.out.println("map = " + map);
        try {
            vehicleNoBPCLCardNo = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getVehicleBPCLCardMapping", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleNoBPCLCardNo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehicleNoBPCLCardNo", sqlException);
        }
        return vehicleNoBPCLCardNo;
    }

    public int insertVehicleBPCLCardMapping(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int insertVehicleBPCLCardMapping = 0;
        int updateBPCLCardMapping = 0;
        int index = 0;
        map.put("userId", userId);
        try {
            try {
                map.put("vehicleId", operationTO.getVehicleId());
                map.put("bpclTransactionId", operationTO.getBpclTransactionId());
                map.put("userId", userId);
                //////System.out.println("mpa = " + map);
                String checkBPCLCardMapping = "";
                String mappingId = "";
                checkBPCLCardMapping = (String) getSqlMapClientTemplate().queryForObject("operation.checkBPCLCardMapping", map);
                //////System.out.println("checkBPCLCardMapping = " + checkBPCLCardMapping);
                if (checkBPCLCardMapping == null) {
                    insertVehicleBPCLCardMapping = (Integer) getSqlMapClientTemplate().update("operation.updateVehicleBPCLCardMapping", map);
                    //////System.out.println("insertVehicleBPCLCardMapping = " + insertVehicleBPCLCardMapping);
                    if (insertVehicleBPCLCardMapping > 0) {
                        mappingId = (String) getSqlMapClientTemplate().queryForObject("operation.getMappingId", map);
                        if (mappingId != null) {
                            map.put("mappingId", mappingId);
                            updateBPCLCardMapping = (Integer) getSqlMapClientTemplate().update("operation.updateBPCLCardMapping", map);
                        }
                        insertVehicleBPCLCardMapping = (Integer) getSqlMapClientTemplate().update("operation.insertVehicleBPCLCardMapping", map);
                    }
                }
            } catch (Exception ex) {
                Writer writer = new StringWriter();
                PrintWriter printWriter = new PrintWriter(writer);
                ex.printStackTrace(printWriter);
                String s = writer.toString();
                //////System.out.println("s = " + s);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVehicleDriverMapping Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertVehicleDriverMapping", sqlException);
        }

        return insertVehicleBPCLCardMapping;
    }

    public ArrayList getPaymentMode() throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList paymentMode = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        //////System.out.println("map = " + map);
        try {
            paymentMode = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getPaymentMode", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRouteCourse Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getRouteCourse", sqlException);
        }
        return paymentMode;
    }

    public int insertPaymentDetails(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        map.put("userId", userId);
        map.put("consignmentId", operationTO.getConsignmentOrderId());
        map.put("paymentType", operationTO.getPaymentType());
        map.put("paymentModeId", operationTO.getPaymentModeId());
        if ("2".equals(operationTO.getPaymentModeId())) {
            map.put("approvalStatus", "1");
        } else {
            map.put("approvalStatus", "0");
        }
        map.put("rtgsNo", operationTO.getRtgsNo());
        map.put("rtgsRemarks", operationTO.getRtgsRemarks());
        map.put("chequeNo", operationTO.getChequeNo());
        map.put("chequeRemarks", operationTO.getChequeRemarks());
        map.put("draftNo", operationTO.getDraftNo());
        map.put("draftRemarks", operationTO.getDraftRemarks());
        map.put("freightAmount", operationTO.getFreightAmount());
        map.put("paidAmount", operationTO.getPaidAmount());
        int status = 0;
        //////System.out.println("map = " + map);
        try {

            status = (Integer) getSqlMapClientTemplate().update("operation.insertPaymentDetails", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertPaymentDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertPaymentDetails", sqlException);
        }
        return status;

    }

    public ArrayList getPaymentDetails(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList paymentDetails = new ArrayList();
        map.put("consignmentId", operationTO.getConsignmentOrderId());
        map.put("paymentTypeId", operationTO.getPaymentTypeId());
        //////System.out.println("map = " + map);
        try {
            paymentDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getPaymentDetails", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRouteCourse Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getRouteCourse", sqlException);
        }
        return paymentDetails;
    }

    public int updateRequestDetails(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        map.put("consignmentId", operationTO.getConsignmentOrderId());
        map.put("requestStatus", operationTO.getRequeststatus());
        map.put("paymentTypeId", operationTO.getPaymentTypeId());
        int status = 0;
        //////System.out.println("map = " + map);
        try {

            status = (Integer) getSqlMapClientTemplate().update("operation.updateRequestDetails", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertPaymentDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertPaymentDetails", sqlException);
        }
        return status;

    }

    public double totalConsignmentPaid(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        map.put("consignmentId", operationTO.getConsignmentOrderId());
        double status = 0.0f;
        //////System.out.println("map = " + map);
        try {

            status = (Double) getSqlMapClientTemplate().queryForObject("operation.totalConsignmentPaid", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("totalConsignmentPaid Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "totalConsignmentPaid", sqlException);
        }
        return status;

    }

    public double totalAmountWaitingForApproval(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        map.put("consignmentId", operationTO.getConsignmentOrderId());
        double totalAmountWaitingForApproval = 0.0f;
        //////System.out.println("map = " + map);
        try {

            totalAmountWaitingForApproval = (Double) getSqlMapClientTemplate().queryForObject("operation.totalAmountWaitingForApproval", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("totalAmountWaitingForApproval Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "totalAmountWaitingForApproval", sqlException);
        }
        return totalAmountWaitingForApproval;

    }

    public ArrayList getVehicleDriverMappingForVehicleId(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList vehicleDriverMapping = new ArrayList();
        map.put("vehicleId", operationTO.getVehicleId());
        try {
            //////System.out.println("map:" + map);
            vehicleDriverMapping = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getVehicleDriverMappingForVehicleId", map);
            //////System.out.println("routeDetailsList " + vehicleDriverMapping.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleDriverMapping Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleDriverMapping List", sqlException);
        }

        return vehicleDriverMapping;
    }

    public ArrayList getapprovalEmptyTripDetails(int totalKM) {
        Map map = new HashMap();
        ArrayList emptyTripDetails = new ArrayList();
        map.put("totalKM", totalKM);
        try {
            //////System.out.println("map:" + map);
            emptyTripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getapprovalEmptyTripDetails", map);
            //////System.out.println("getapprovalEmptyTripDetails " + emptyTripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("emptyTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "emptyTripDetails List", sqlException);
        }

        return emptyTripDetails;
    }

    public ArrayList getVehicleRegNoForJobCard() {
        Map map = new HashMap();
        ArrayList vehicleRegNoForJobCard = new ArrayList();
        try {
            vehicleRegNoForJobCard = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getVehicleRegNoForJobCard", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleRegNoForJobCard Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleRegNoForJobCard", sqlException);
        }

        return vehicleRegNoForJobCard;
    }

    public ArrayList trailerNos() {
        Map map = new HashMap();
        ArrayList trailerNos = new ArrayList();
        try {
            trailerNos = (ArrayList) getSqlMapClientTemplate().queryForList("operation.trailerNos", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("trailerNos Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "trailerNos", sqlException);
        }

        return trailerNos;
    }

    public ArrayList getVehicleRegNoForJobCardList(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList vehicleRegNoForJobCard = new ArrayList();
        map.put("regNo", operationTO.getVehicleNo() + "%");
        //////System.out.println("map = " + map);
        try {
            vehicleRegNoForJobCard = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getVehicleRegNoForJobCardList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleRegNoForJobCard Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleRegNoForJobCard", sqlException);
        }

        return vehicleRegNoForJobCard;
    }

    public ArrayList getJobCardDetails(int jobcardId) {
        Map map = new HashMap();
        map.put("jobcardId", jobcardId);
        ArrayList jobCardList = new ArrayList();
        try {
            jobCardList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getJobCardDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleRegNoForJobCard Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleRegNoForJobCard", sqlException);
        }

        return jobCardList;
    }

    public ArrayList getClosedJobCardDetails(int jobcardId) {
        Map map = new HashMap();
        map.put("jobcardId", jobcardId);
        ArrayList jobCardList = new ArrayList();
        try {
            jobCardList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getClosedJobCardDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getClosedJobCardDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getClosedJobCardDetails", sqlException);
        }

        return jobCardList;
    }

    public String getJobcardEmail() {
        Map map = new HashMap();
        String jobCardEmailList = "";
        try {
            jobCardEmailList = (String) getSqlMapClientTemplate().queryForObject("operation.getJobcardEmail", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJobcardEmail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getJobcardEmail", sqlException);
        }

        return jobCardEmailList;
    }

    public ArrayList getPrimaryDriverName(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList driverName = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("driverName", operationTO.getDriverName() + "%");
        //////System.out.println("map = " + map);
        try {
            driverName = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getPrimaryDriverList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getDriverList", sqlException);
        }
        return driverName;
    }

    public ArrayList getServiceTypeSection(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList serviceTypeSection = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("serviceTypeId", operationTO.getServiceTypeId());
        //////System.out.println("map = " + map);
        try {
            serviceTypeSection = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getServiceTypeSection", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getServiceTypeSection Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getServiceTypeSection", sqlException);
        }
        return serviceTypeSection;
    }

    public String getTripTypeId(String tripId) {
        Map map = new HashMap();
        map.put("tripId", tripId);
        String tripTypeId = "";
        //////System.out.println("map = " + map);
        try {
            tripTypeId = (String) getSqlMapClientTemplate().queryForObject("operation.getTripTypeId", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripTypeId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTripTypeId", sqlException);
        }
        return tripTypeId;

    }

    public String getFuelTypeId(String tripId) {
        Map map = new HashMap();
        map.put("tripId", tripId);
        String fuelTypeId = "";
        //////System.out.println("map = " + map);
        try {
            fuelTypeId = (String) getSqlMapClientTemplate().queryForObject("operation.getFuelTypeId", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFuelTypeId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getFuelTypeId", sqlException);
        }
        return fuelTypeId;

    }

    public String getSecondaryApprovalPerson(String tripId) {
        Map map = new HashMap();
        map.put("tripId", tripId);

        //////System.out.println("map = " + map);
        try {
            tripId = (String) getSqlMapClientTemplate().queryForObject("operation.getSecondaryApprovalPerson", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryApprovalPerson Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getSecondaryApprovalPerson", sqlException);
        }
        return tripId;

    }

    public String getSecondaryApprovalPersonForEmpty(String tripId) {
        Map map = new HashMap();
        map.put("tripId", tripId);

        //////System.out.println("map = " + map);
        try {
            tripId = (String) getSqlMapClientTemplate().queryForObject("operation.getSecondaryApprovalPersonForEmpty", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryApprovalPerson Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getSecondaryApprovalPerson", sqlException);
        }
        return tripId;

    }

    public ArrayList getCreditLimitEmailDetails(String activitycode) {
        ArrayList creditLimitEmail = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("actcode", activitycode);
            creditLimitEmail = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCreditLimitEmailDetails", map);
            //////System.out.println("EmailDetails size=" + creditLimitEmail.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCreditLimitEmailDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCreditLimitEmailDetails List", sqlException);
        }

        return creditLimitEmail;
    }

    public int updateConsignmentApprovalMailCount(OperationTO operationTO, int count) {
        Map map = new HashMap();
        map.put("consignmentId", operationTO.getConsignmentOrderId());
        map.put("count", count);
        int status = 0;
        //////System.out.println("map = " + map);
        try {

            status = (Integer) getSqlMapClientTemplate().update("operation.updateConsignmentApprovalMailCount", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateConsignmentApprovalMailCount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateConsignmentApprovalMailCount", sqlException);
        }
        return status;

    }

    public String getSecondaryRouteApprovalPerson(String customerId) {
        Map map = new HashMap();
        map.put("customerId", customerId);

        //////System.out.println("map = " + map);
        try {
            customerId = (String) getSqlMapClientTemplate().queryForObject("operation.getSecondaryRouteApprovalPerson", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryApprovalPerson Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getSecondaryApprovalPerson", sqlException);
        }
        return customerId;

    }

    public String getSecondaryEmptyTripApprovalPerson(String vehicleId) {
        Map map = new HashMap();
        map.put("vehicleId", vehicleId);

        //////System.out.println("map = " + map);
        try {
            vehicleId = (String) getSqlMapClientTemplate().queryForObject("operation.getSecondaryEmptyTripApprovalPerson", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryApprovalPerson Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getSecondaryApprovalPerson", sqlException);
        }
        return vehicleId;

    }

    public String getSecondaryFcPerson(String vehicleId) {
        Map map = new HashMap();
        map.put("vehicleId", vehicleId);

        //////System.out.println("map = " + map);
        try {
            vehicleId = (String) getSqlMapClientTemplate().queryForObject("operation.getSecondaryFcPerson", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryApprovalPerson Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getSecondaryApprovalPerson", sqlException);
        }
        return vehicleId;

    }

    public int updateConteCount(OperationTO operationTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        int user = userId;

        map.put("customerId", operationTO.getCustomerId());
        map.put("userID", userId);

        try {

            status = (Integer) session.update("operation.updateCnoteCount", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateCityMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateCityMaster", sqlException);
        }

        return status;
    }

    public ArrayList getVehicleRegNoForTyer() {
        Map map = new HashMap();
        ArrayList vehicleRegNoForJobCard = new ArrayList();
        try {
            vehicleRegNoForJobCard = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getVehicleRegNoForTyer", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleRegNoForJobCard Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleRegNoForJobCard", sqlException);
        }

        return vehicleRegNoForJobCard;
    }

    public ArrayList getContainerTypeList() {
        Map map = new HashMap();
        ArrayList containerTypeList = new ArrayList();

        try {
            containerTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getContainerTypeList", map);
            //////System.out.println(" containerTypeList =" + containerTypeList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleTypeList", sqlException);
        }
        return containerTypeList;
    }

    public ArrayList getCityName(String cityname) {
        Map map = new HashMap();
        map.put("cityName", cityname + "%");

        ArrayList citynameList = new ArrayList();
        try {
            citynameList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCityNameList", map);
            //////System.out.println("existingTechnicians size=" + citynameList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("existingTechnicians Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "existingTechnicians List", sqlException);
        }

        return citynameList;
    }

    public ArrayList getTrailerAvailabilityList(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList trailerAvailabilityList = new ArrayList();
//        map.put("vehicleid", operationTO.getVehicleid());
        map.put("trailerId", operationTO.getTrailerId());
        map.put("trailerNo", operationTO.getTrailerNo());
        // map.put("vehicleTypeId", operationTO.getVehicleID());
        map.put("fromDate", operationTO.getFromDate());
        map.put("toDate", operationTO.getToDate());
        map.put("status", operationTO.getStatus());
        map.put("zoneId", operationTO.getZoneId());
        map.put("fleetCenterId", operationTO.getFleetCenterId());
        map.put("roleId", operationTO.getRoleId());
        map.put("companyId", operationTO.getCompId());
        map.put("isActive", operationTO.getIsactive());
        map.put("cityFromId", operationTO.getCityId());
        if ("1".equals(operationTO.getTripType())) {
            map.put("usageType", 2);
        } else if ("2".equals(operationTO.getTripType())) {
            map.put("usageType", 1);
        }

        //////System.out.println("map = " + map);
        try {
            //////System.out.println("this is Trailer Availability");
            trailerAvailabilityList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getTrailerAvailabilityList", map);
            //////System.out.println(" trailerAvailabilityList =" + trailerAvailabilityList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTrailerAvailabilityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTrailerAvailabilityList ", sqlException);
        }

        return trailerAvailabilityList;
    }

//    public void updateDriverAdvanceToEFS(String tripId) {
//
//        Map map = new HashMap();
//        try {
//            String employee_code = "";
//            String tripCode = "", localAmount = "", throttle_uid = "", currency_code = "", orderNo = "";
//
//            map.put("tripId", tripId);
//
//            String orderReferenceNo = (String) getSqlMapClientTemplate().queryForObject("trip.getOrderReferenceNoForTripId", map);
//            // String tripStatus = (String) getSqlMapClientTemplate().queryForObject("trip.getStausName", map);
//            map.put("orderReferenceNo", orderReferenceNo);
//            //////System.out.println("map::::" + map);
//            ArrayList driverAdvanceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getDriverAdvanceDetailsForEFS", map);
//            Iterator itr1 = driverAdvanceDetails.iterator();
//            OperationTO opTO = new OperationTO();
//            while (itr1.hasNext()) {
//                opTO = new OperationTO();
//                opTO = (OperationTO) itr1.next();
//                employee_code = String.valueOf(opTO.getEmpId());
//                tripCode = opTO.getTripCode();
//                localAmount = opTO.getRequestedadvance();
//                throttle_uid = opTO.getTripAdvanceId();
//                currency_code = opTO.getCurrencyCode();
//                orderNo = opTO.getCustomerOrderReferenceNo();
//                //////System.out.println("employee_code" + employee_code + "tripCode" + tripCode + "localAmount" + localAmount + "throttle_uid" + throttle_uid + "currency_code" + currency_code + "order_No" + orderNo);
//
//            }
//            // TODO code application logic here
//
//            Service srv = new Service();
//            ServiceSoap srvs = srv.getServiceSoap();
//
//            //UpdateOrderStatusResult result = srvs.updateOrderStatus(orderReferenceNo, tripStatusId, "04-05-2015 21:12:12");
//            String[] temp = orderReferenceNo.split(",");
//            GetDriverAdvanceResult result = null;
//            //  for (int i = 0; i < temp.length; i++) {
//            //     System.out.println(temp[i]);
//            result = srvs.getDriverAdvance(employee_code, tripCode, localAmount, throttle_uid, currency_code, orderNo);
//            //////System.out.println("result.getContent() = " + result.getContent());
//            List resultList = result.getContent();
//            for (int k = 0; k < resultList.size(); k++) {
//                System.out.println(resultList.get(k));
//            }
//
//            //    }
//        } catch (Exception sqlException) {
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            sqlException.printStackTrace();
//            FPLogUtils.fpDebugLog("updateOrderStatusToEFS Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            //throw new FPRuntimeException("EM-MRS-01", CLASS, "updateOrderStatusToEFS List", sqlException);
//        }
//    }
    public String ownVehicleTypeRate(String vehicleTypeId, String routePoints) {
        Map map = new HashMap();
        String ratePerKm = "";
        String[] originData = null;
        String[] destinationData = null;
        //////System.out.println("routePoints:" + routePoints);
        String[] routeData = routePoints.split("@");
        originData = routeData[0].split("~");
        map.put("sourceId", originData[0]);
        destinationData = routeData[routeData.length - 1].split("~");
        map.put("destinationId", destinationData[0]);
        map.put("vehicleTypeId", vehicleTypeId);

        //////System.out.println("map = " + map);
        try {
            ratePerKm = (String) getSqlMapClientTemplate().queryForObject("operation.ownVehicleTypeRate", map);
            //////System.out.println("ratePerKm:" + ratePerKm);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ownVehicleTypeRate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "ownVehicleTypeRate", sqlException);
        }
        return ratePerKm;

    }

    public ArrayList dedicatedVehicleTypeVendorRates(String vehicleTypeId, String routePoints) {
        Map map = new HashMap();
        ArrayList dedicatedVehicleTypeVendorRates = new ArrayList();
        map.put("vehicleTypeId", vehicleTypeId);

        String[] originData = null;
        String[] destinationData = null;
        //////System.out.println("routePoints:" + routePoints);
        String[] routeData = routePoints.split("@");
        originData = routeData[0].split("~");
        map.put("sourceId", originData[0]);
        destinationData = routeData[routeData.length - 1].split("~");
        map.put("destinationId", destinationData[0]);
        //////System.out.println("map = " + map);
        try {
            dedicatedVehicleTypeVendorRates = (ArrayList) getSqlMapClientTemplate().queryForList("operation.dedicatedVehicleTypeVendorRates", map);
            //////System.out.println(" dedicatedVehicleTypeVendorRates =" + dedicatedVehicleTypeVendorRates.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("dedicatedVehicleTypeVendorRates Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "dedicatedVehicleTypeVendorRates ", sqlException);
        }
        return dedicatedVehicleTypeVendorRates;
    }

    public ArrayList hireVehicleTypeVendorRates(String vehicleTypeId, String routePoints) {
        Map map = new HashMap();
        ArrayList hireVehicleTypeVendorRates = new ArrayList();
        map.put("vehicleTypeId", vehicleTypeId);
        String[] originData = null;
        String[] destinationData = null;
        //////System.out.println("routePoints:" + routePoints);
        String[] routeData = routePoints.split("@");
        originData = routeData[0].split("~");
        map.put("sourceId", originData[0]);
        destinationData = routeData[routeData.length - 1].split("~");
        map.put("destinationId", destinationData[0]);
        //////System.out.println("map = " + map);
        try {
            hireVehicleTypeVendorRates = (ArrayList) getSqlMapClientTemplate().queryForList("operation.hireVehicleTypeVendorRates", map);
            //////System.out.println(" hireVehicleTypeVendorRates =" + hireVehicleTypeVendorRates.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("hireVehicleTypeVendorRates Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "hireVehicleTypeVendorRates ", sqlException);
        }
        return hireVehicleTypeVendorRates;
    }

    public ArrayList freightRate(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList freightRate = new ArrayList();
        //   map.put("consignmentOrderIds", consignmentOrderIds);
        if (operationTO.getConsignmentOrderId().contains(",")) {
            String[] consignmentId = operationTO.getConsignmentOrderId().split(",");
            List cOrderId = new ArrayList(consignmentId.length);
            for (int i = 0; i < consignmentId.length; i++) {
                //////System.out.println("value:" + consignmentId[i]);
                cOrderId.add(consignmentId[i]);
            }
            map.put("cOrderId", cOrderId);
        } else {
            map.put("cOrderId", operationTO.getConsignmentOrderId());
        }

        //////System.out.println("map = " + map);
        try {
            freightRate = (ArrayList) getSqlMapClientTemplate().queryForList("operation.freightRate", map);
            //////System.out.println(" freightRate =" + freightRate.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("freightRate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "freightRate ", sqlException);
        }
        return freightRate;
    }

    public ArrayList getCountryName(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList countryList = new ArrayList();
        map.put("countryName", operationTO.getCountryName() + "%");
        System.out.println("countryName" + map);
        try {
            countryList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getcountryList", map);
            System.out.println(" getcountry =" + countryList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getcountryList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getZoneList List", sqlException);
        }

        return countryList;
    }

//    gulshan
    public int insertOrEditZoneMaster(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int Status = 0;
        int user = userId;

        map.put("zoneId", operationTO.getZoneId());
        map.put("zoneName", operationTO.getZoneName());
        map.put("countryId", operationTO.getCountryId());
        map.put("status", operationTO.getStatus());
        System.out.println("map zone= " + map);

        try {
            if (operationTO.getZoneId() != null && operationTO.getZoneId() == "") {
                Status = (Integer) getSqlMapClientTemplate().update("operation.saveZone", map);

            } else {
                Status = (Integer) getSqlMapClientTemplate().update("operation.updateZoneMaster", map);

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveZone Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveZone", sqlException);
        }
        return Status;
    }

    public ArrayList getZonelist() {
        Map map = new HashMap();
        ArrayList getZonelist = new ArrayList();

        try {
            getZonelist = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getZoneNames", map);
            //////System.out.println(" getZonelist =" + getZonelist.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getProductCategory Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getProductCategory", sqlException);
        }
        return getZonelist;
    }

    public String checkZoneName(OperationTO operationTO) {
        Map map = new HashMap();
        String checkZoneName = "";
        try {
            //////System.out.println("map = " + map);
            map.put("zoneName", operationTO.getZoneName());
            checkZoneName = (String) getSqlMapClientTemplate().queryForObject("operation.checkZoneName", map);
            //////System.out.println("checkZoneName " + checkZoneName);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkZoneName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkZoneName", sqlException);
        }

        return checkZoneName;
    }

//    slab master
    public ArrayList getSlablist() {
        Map map = new HashMap();
        ArrayList getSlablist = new ArrayList();

        try {
            getSlablist = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getSlabNames", map);
            //////System.out.println(" getSlablist =" + getSlablist.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getProductCategory Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSlablist", sqlException);
        }
        return getSlablist;
    }

    public int insertOrEditSlabMaster(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int Status = 0;
        int user = userId;

        map.put("slabId", operationTO.getSlabId());
        map.put("slabName", operationTO.getSlabName());
        map.put("maxTonnage", operationTO.getMaxTonnage());
        map.put("status", operationTO.getStatus());
        //////System.out.println("map Slab= " + map);

        try {
            if (operationTO.getSlabId() != null && operationTO.getSlabId() == "") {
                Status = (Integer) getSqlMapClientTemplate().update("operation.saveSlabName", map);

            } else {
                Status = (Integer) getSqlMapClientTemplate().update("operation.updateSlabName", map);

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveZone Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveZone", sqlException);
        }
        return Status;
    }

    public String checkslabName(OperationTO operationTO) {
        Map map = new HashMap();
        String checkslabName = "";
        try {
            map.put("slabName", operationTO.getSlabName());
            //////System.out.println("map = " + map);

            checkslabName = (String) getSqlMapClientTemplate().queryForObject("operation.checkslabName", map);
            //////System.out.println("checkslabName " + checkslabName);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkslabName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkslabName", sqlException);
        }

        return checkslabName;
    }

//    symbol master over
    public int insertOrEditCurrencySymbolMaster(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int Status = 0;
        int user = userId;

        map.put("userId", userId);
        map.put("currencyId", operationTO.getCurrencyId());
        map.put("currencyName", operationTO.getCurrencyName());
        map.put("currencySymbol", operationTO.getCurrencySymbol());
        map.put("status", operationTO.getStatus());
        //////System.out.println("map zone= " + map);

        try {
            if (operationTO.getCurrencyId() != null && operationTO.getCurrencyId() == "") {
                Status = (Integer) getSqlMapClientTemplate().update("operation.saveCurrencySymbol", map);

            } else {
                Status = (Integer) getSqlMapClientTemplate().update("operation.updateCurrencySymbol", map);

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveZone Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveZone", sqlException);
        }
        return Status;
    }

    public ArrayList getSymbolNameList() {
        Map map = new HashMap();
        ArrayList getSymbolNameList = new ArrayList();

        try {
            getSymbolNameList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getSymbolList", map);
            //////System.out.println(" getZonelist =" + getSymbolNameList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSymbolList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSymbolList", sqlException);
        }
        return getSymbolNameList;
    }

    public String checkCurrencyName(OperationTO operationTO) {
        Map map = new HashMap();
        String checkCurrencyName = "";
        try {
            map.put("currencyName", operationTO.getCurrencyName());
            //////System.out.println("map = " + map);
            checkCurrencyName = (String) getSqlMapClientTemplate().queryForObject("operation.checkCurrencyName", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkCurrencyName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkCurrencyName", sqlException);
        }

        return checkCurrencyName;
    }

//    currency symbol over
    public ArrayList getBorderCity(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList borderCityList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("city", operationTO.getCity() + "%");
        //////System.out.println("map = " + map);

        try {
            borderCityList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getBorderCity", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBorderCity Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getBorderCity", sqlException);
        }
        return borderCityList;
    }

    public String checkCountryName(OperationTO operationTO) {
        Map map = new HashMap();
        String checkCountryName = "";
        try {
            map.put("countryName", operationTO.getCountryName());
            //////System.out.println("map = " + map);
            checkCountryName = (String) getSqlMapClientTemplate().queryForObject("operation.checkCountryName", map);
            //////System.out.println("checkCountryName " + checkCountryName);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkCountryName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkCountryName", sqlException);
        }

        return checkCountryName;
    }

    public int insertDetaintionChargeDetails(OperationTO operationTO, String[] dcVehicleTypeId, String[] dcRate, int userId) {
        Map map = new HashMap();
        int insertBillingDetails = 0;
        int index = 0;
        map.put("userId", userId);
        map.put("contractId", operationTO.getContractId());
        map.put("customerId", operationTO.getCustomerId());
        try {
            try {
                for (int i = 0; i < dcVehicleTypeId.length; i++) {
                    if (dcVehicleTypeId[i] != null && dcRate[i] != null) {
                        map.put("dcVehicleTypeId", dcVehicleTypeId[i]);
                        if (dcRate[i] != null && !"".equals(dcRate[i])) {
                            map.put("dcRate", Double.parseDouble(dcRate[i]));
                        } else {
                            map.put("dcRate", 0.0f);
                        }
                        //////System.out.println("map for Dc:" + map);
                        insertBillingDetails = (Integer) getSqlMapClientTemplate().update("operation.insertDetaintionChargeDetails", map);
                    }
                }

            } catch (Exception ex) {
                Writer writer = new StringWriter();
                PrintWriter printWriter = new PrintWriter(writer);
                ex.printStackTrace(printWriter);
                String s = writer.toString();
                //////System.out.println("s = " + s);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertActualKmBillingDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertActualKmBillingDetails", sqlException);
        }

        return insertBillingDetails;
    }

    public ArrayList getDetaintionRateList(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList detaintionRateList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("contractId", operationTO.getContractId());
        map.put("customerId", operationTO.getCustomerId());
        //////System.out.println("operationTO.getBillingTypeId() = " + operationTO.getBillingTypeId());
        //////System.out.println("map = " + map);

        try {
            detaintionRateList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getDetaintionRateList", map);

            //////System.out.println("contractFixedRateList.size() = " + detaintionRateList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractFixedRateList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getContractFixedRateList", sqlException);
        }
        return detaintionRateList;
    }

    public int updateDetaintion(OperationTO operationTo, String[] dcVehicleTypeId, String[] dcRate, int userId) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("customerId", operationTo.getCustId());
            for (int i = 0; i < dcVehicleTypeId.length; i++) {
                if (dcVehicleTypeId[i] != null && dcRate[i] != null) {
                    map.put("dcVehicleTypeId", dcVehicleTypeId[i]);
                    if (dcRate[i] != null && !"".equals(dcRate[i])) {
                        map.put("dcRate", Double.parseDouble(dcRate[i]));
                    } else {
                        map.put("dcRate", 0.0f);
                    }

                    //////System.out.println("map for Dc update:" + map);
                    status = (Integer) getSqlMapClientTemplate().update("operation.updateDetaintion", map);
                }
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertWorkOrderSchdule Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertWorkOrderSchdule", sqlException);
        }

        return status;
    }

//    country master-17/12/15
    public int insertOrEditCountryMaster(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int Status = 0;
        int user = userId;

        map.put("userId", userId);
        map.put("countryId", operationTO.getCountryId());
        map.put("countryCode", operationTO.getCountryCode());
        map.put("countryName", operationTO.getCountryName());
        map.put("currencyCode", operationTO.getCurrencyCode());
        map.put("status", operationTO.getStatus());
        //////System.out.println("map zone= " + map);

        try {
            if (operationTO.getCountryId() != null && operationTO.getCountryId() == "") {
                Status = (Integer) getSqlMapClientTemplate().update("operation.saveCountryMaster", map);
            } else {
                Status = (Integer) getSqlMapClientTemplate().update("operation.updateCountryMaster", map);

            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveZone Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveZone", sqlException);
        }
        return Status;
    }

    public ArrayList getCountryNameList() {
        Map map = new HashMap();
        ArrayList getCountryNameList = new ArrayList();

        try {
            getCountryNameList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getSymbolNameList", map);
            //////System.out.println(" getZonelist =" + getCountryNameList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCountryNameList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCountryNameList", sqlException);
        }
        return getCountryNameList;
    }

    public String checkCountryNameList(OperationTO operationTO) {
        Map map = new HashMap();
        String checkCountryNameList = "";
        try {
            map.put("countryName", operationTO.getCountryName());
            //////System.out.println("map = " + map);
            checkCountryNameList = (String) getSqlMapClientTemplate().queryForObject("operation.checkCountryNameList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkCountryNameList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkCountryNameList", sqlException);
        }

        return checkCountryNameList;
    }

    public ArrayList getcurrencySymbolList(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList currencySymbolList = new ArrayList();
        map.put("currencyId", operationTO.getCurrencyId());
        map.put("currencyCode", operationTO.getCurrencyCode() + "%");
        System.out.println("currency map==" + map);
        try {
            currencySymbolList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getcurrencySymbol", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("currencySymbolList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "currencySymbolList List", sqlException);
        }

        return currencySymbolList;
    }

//    currencyROE
    public int insertOrEditCurrencyRateExchange(OperationTO operationTO, int userId) {
        System.out.println("insertOrEditCurrencyRateExchange");
        Map map = new HashMap();
        int Status = 0;
        int user = userId;

        map.put("userId", user);
        map.put("currencyId", operationTO.getCurrencyId());
        map.put("fromcountryId", operationTO.getFromcurrencyId());
        map.put("currencyVal1", operationTO.getFromcurrencyvalue());
        map.put("tocountryId", operationTO.getTocurrencyId());
        map.put("currencyVal2", operationTO.getTocurrencyvalue());
        map.put("fromdate", operationTO.getFromdate());
        map.put("todate", operationTO.getTodate());
        map.put("status", operationTO.getStatus());
        System.out.println("exchangecurrency map" + map);

        try {
            System.out.println("start insert");
            if (operationTO.getCurrencyId() != null && operationTO.getCurrencyId() == "") {
                Status = (Integer) getSqlMapClientTemplate().update("operation.insertCurrencyRateExchange", map);
                System.out.println("insert status==" + Status);
            } else {
                Status = (Integer) getSqlMapClientTemplate().update("operation.updateCurrencyRateExchange", map);
                System.out.println("update status==" + Status);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Status Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "Status", sqlException);
        }
        return Status;
    }

    public ArrayList getCurrencyList() {
        Map map = new HashMap();
        ArrayList getCurrencyList = new ArrayList();
        map.put("currencyId", "");
        try {
            System.out.println("this is currency List");
            getCurrencyList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCurrencyList", map);
            System.out.println(" getCurrencyList =" + getCurrencyList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCurrencyList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCurrencyList", sqlException);
        }
        return getCurrencyList;
    }

//    05/01/16
    public String getZoneCountryName(String zoneName) {
        Map map = new HashMap();
        String ZoneCountryName = "";
        try {
            map.put("zoneName", zoneName);
            System.out.println("zoneName==" + map);
            ZoneCountryName = (String) getSqlMapClientTemplate().queryForObject("operation.getZoneCountryName", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkVehicleTyre Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleDetails", sqlException);
        }
        return ZoneCountryName;
    }

    //08-01-16   
    public ArrayList getavailTollList(String zoneName, String routeId) {
        Map map = new HashMap();
        ArrayList getavailTollList = new ArrayList();
        map.put("zoneName", zoneName);
        map.put("routeId", routeId);
        try {
            System.out.println("this is toll List");
            getavailTollList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getavailTollList", map);
            System.out.println(" getavailTollList =" + getavailTollList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getavailTollList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getavailTollList", sqlException);
        }
        return getavailTollList;
    }

    public ArrayList getassignedTollList(String zoneName, String routeId) {
        Map map = new HashMap();
        ArrayList getassignedTollList = new ArrayList();
        map.put("zoneName", zoneName);
        map.put("routeId", routeId);
        try {
            System.out.println("this is get assigned Toll List");
            getassignedTollList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getassignedTollList", map);
            System.out.println(" getassignedTollList =" + getassignedTollList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getassignedTollList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getassignedTollList", sqlException);
        }
        return getassignedTollList;
    }

    public int updateRouteTollDetails(OperationTO operationTO, String[] assignedRole, int userId) {

        System.out.println("updateRouteTollDetails");

        Map map = new HashMap();
        int Status = 0;
        int user = userId;
        String assigtoll = "";

        try {

            map.put("userId", user);
            map.put("routeId", operationTO.getRouteId());
            map.put("zoneId", operationTO.getZoneId());

            //
            Status = (Integer) getSqlMapClientTemplate().update("operation.updateRouteTollDetails", map);

            for (int i = 0; i < assignedRole.length; i++) {

                assigtoll = assignedRole[i];

                System.out.println("assignedRole " + assignedRole[i]);

                map.put("tollId", assigtoll);

                Status = (Integer) getSqlMapClientTemplate().update("operation.insertRouteTollDetails", map);

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Status Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "Status", sqlException);
        }
        return Status;
    }

    public ArrayList getCurrency() {
        Map map = new HashMap();
        ArrayList currencyList = new ArrayList();
        try {
            currencyList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCurrency", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCurrency Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCurrency List", sqlException);
        }

        return currencyList;
    }

    public ArrayList getContainerList(OperationTO operationTO) {
        Map map = new HashMap();
//        map.put("customerId", operationTO.getCustomerId());
        ArrayList getContainerList = new ArrayList();

        try {
            getContainerList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getContainerList", map);
            //////System.out.println("map 2 = " + map);
            System.out.println("getContainerList " + getContainerList);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContainerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getContainerList", sqlException);
        }

        return getContainerList;
    }

    public ArrayList getDailyContractedCustomerDetails(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList customerDetails = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("customerName", operationTO.getCustomerName() + "%");
        map.put("customerCode", operationTO.getCustomerCode() + "%");
        map.put("userId", operationTO.getUserId());
        map.put("roleId", operationTO.getRoleId());

        String empId = (String) getSqlMapClientTemplate().queryForObject("customer.getEmployeeId", map);
        map.put("empId", empId);

        System.out.println("map = " + map);
        try {
            if (operationTO.getCustomerName() != null) {
                customerDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getDailyContratcedCustomerNameDetails", map);
            } else if (operationTO.getCustomerCode() != null) {
                customerDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCustomerCodeDetails", map);
            }
            System.out.println("customerDetails size:" + customerDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCustomerDetails", sqlException);
        }
        return customerDetails;
    }

    public ArrayList getDailyCustomerDetails(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList customerDetails = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */

        map.put("quotationId", operationTO.getQuotationId());
        map.put("userId", operationTO.getUserId());
        map.put("roleId", operationTO.getRoleId());

        System.out.println("map = " + map);
        try {
            customerDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getDailyContratcedCustomerNameDetails", map);
            System.out.println("customerDetails size:" + customerDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCustomerDetails", sqlException);
        }
        return customerDetails;
    }

    public int updateQuotationStatus(String quotationId, String trailerTypeId, String vehicleTypeId, int userId) {

        Map map = new HashMap();
        int Status = 0;
        int user = userId;
        String assigtoll = "";

        try {

            map.put("quotationId", quotationId);
            map.put("trailerTypeId", trailerTypeId);
            map.put("vehicleTypeId", vehicleTypeId);
            System.out.println("map..:" + map);
            //
            Status = (Integer) getSqlMapClientTemplate().update("operation.updateQuotationStatus", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Status Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "Status", sqlException);
        }
        return Status;
    }

    public ArrayList getVehicleTypeList(String containerTypeId) {
        Map map = new HashMap();
        ArrayList vehicleTypeList = new ArrayList();

        try {
            map.put("conatainerTypeId", containerTypeId);
            System.out.println("map:" + map);
            vehicleTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getVehicleTypeListBasedOnContainer", map);
            System.out.println(" getVehicleTypeListBasedOnContainer =" + vehicleTypeList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleTypeList", sqlException);
        }
        return vehicleTypeList;
    }

    public int insertTripFuel(int userId, String returnTripId, int tripId, String bunkName, String bunkPlace, String fuelDate, String fuelAmount,
            String fuelLtrs, String fuelRemarks, String driverId, String slipNo, String tripFuelId, String vehicleId, String hireVehicleNo) {
        Map map = new HashMap();
        int lastInsertedId = 0;
        try {
            String[] tempBunk = bunkName.split("~");
//            System.out.println("tempBunk[0] = " + tempBunk[0]);
//            System.out.println("tempBunk[1] = " + tempBunk[1]);
//            System.out.println("tempBunk[2] = " + tempBunk[2]);
            map.put("tripId", tripId);
            map.put("returnTripId", returnTripId);
            map.put("tripSheetId", tripId);
            map.put("bunkName", tempBunk[0]);
            map.put("bunkPlace", bunkPlace);
            map.put("fuelDate", fuelDate);
            map.put("fuelAmount", fuelAmount);
            map.put("fuelLtrs", fuelLtrs);
            map.put("fuelRemarks", fuelRemarks);
            map.put("createdBy", userId);
            map.put("slipNo", slipNo);
            map.put("tripFuelId", tripFuelId);
            map.put("vehicleId", vehicleId);
            if ("0".equals(vehicleId)) {
                map.put("hireVehilceNo", hireVehicleNo);
            }
            System.out.println("map in tripFuel " + map);
            if (tripFuelId == null || "".equals(tripFuelId)) {
                lastInsertedId = (Integer) getSqlMapClientTemplate().update("operation.insertTripFuel", map);
                System.out.println("insertTripFuel -->" + lastInsertedId);

                //  --------------------------------- acc 1st row start --------------------------
                map.put("userId", userId);
                map.put("DetailCode", "1");
                map.put("voucherType", "%PAYMENT%");
                String code2 = (String) getSqlMapClientTemplate().queryForObject("operation.getTripVoucherCode", map);
                String[] temp = code2.split("-");
                int codeval2 = Integer.parseInt(temp[1]);
                int codev2 = codeval2 + 1;
                String voucherCode = "PAYMENT-" + codev2;
                System.out.println("voucherCode = " + voucherCode);
                map.put("voucherCode", voucherCode);
                map.put("mainEntryType", "VOUCHER");
                map.put("entryType", "PAYMENT");
                map.put("ledgerId", "37");
                map.put("particularsId", "LEDGER-29");
                map.put("amount", fuelAmount);
                map.put("Accounts_Type", "DEBIT");
                map.put("Remark", "Diesel Expense");
                map.put("Reference", returnTripId);
                map.put("SearchCode", tripId);
                map.put("driverId", driverId);

                System.out.println("map1 =---------------------> " + map);
                int status1 = (Integer) getSqlMapClientTemplate().update("operation.insertTripAccountEntry", map);
                System.out.println("status1 = " + status1);
                //--------------------------------- acc 2nd row start --------------------------
                if (status1 > 0) {
                    //identify debit account
                    //if own driver, debit account is driver account
                    //if contract driver, the debit account is contract vendor account
                    String ledgerId = tempBunk[1];
                    String particularsId = tempBunk[2];

                    map.put("DetailCode", "2");
                    map.put("ledgerId", ledgerId);
                    map.put("particularsId", particularsId);
                    map.put("Accounts_Type", "CREDIT");
                    System.out.println("map2 =---------------------> " + map);
                    int status2 = (Integer) getSqlMapClientTemplate().update("operation.insertTripAccountEntry", map);
                    System.out.println("status2 = " + status2);
                }
            } else {
                lastInsertedId = (Integer) getSqlMapClientTemplate().update("operation.updateTripFuel", map);
                System.out.println("update:" + lastInsertedId);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return lastInsertedId;
    }

    public int updateTripGr(int tripId, String containerNo, String linerName, String sealNo, String grNo, String consignmentOrderId, String uniqueId, String tripContainerId, String grDate, String vehicleNo, String driverName, String vehicleId, String challanNo, String grHours, String grMins) {
        Map map = new HashMap();
        int lastInsertedId = 0;
        try {
//            SimpleDateFormat dt=new SimpleDateFormat("yyyy-mm-dd");
            map.put("tripId", tripId);
            map.put("containerNo", containerNo);
            map.put("linerName", linerName);
            map.put("sealNo", sealNo);
            map.put("grNo", grNo);
            map.put("consignmentOrderId", consignmentOrderId);
            map.put("uniqueId", uniqueId);
            map.put("tripContainerId", tripContainerId);
            map.put("grDate", grDate + " " + grHours + ":" + grMins + ":" + "00");
            map.put("vehicleNo", vehicleNo);
            map.put("driverName", driverName);
            map.put("vehicleId", vehicleId);
            map.put("challanNo", challanNo);

            System.out.println("map in tripgr Update " + map);

            lastInsertedId = (Integer) getSqlMapClientTemplate().update("operation.updateConsignmentContainer", map);
            System.out.println("updateConsignmentContainer -->" + lastInsertedId);
            lastInsertedId = (Integer) getSqlMapClientTemplate().update("operation.updateTripContainer", map);
            System.out.println("updateTripconatiner:" + lastInsertedId);
            lastInsertedId = (Integer) getSqlMapClientTemplate().update("operation.updateTripGrDetails", map);
            System.out.println("updateTripGrDetails -->" + lastInsertedId);
            if ("0".equals(vehicleId)) {
                lastInsertedId = (Integer) getSqlMapClientTemplate().update("operation.updateTripVehicle", map);
                System.out.println("updateTripVehicle -->" + lastInsertedId);
                lastInsertedId = (Integer) getSqlMapClientTemplate().update("operation.updateTripDriver", map);
                System.out.println("updateTripDriver -->" + lastInsertedId);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */

            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return lastInsertedId;
    }

    public int insertpenalitycharges(OperationTO operationTO, String penality, String chargeamount, String pcmremarks, String pcmunit) {
        Map map = new HashMap();
        int insertStatus = 0;

        map.put("penality", penality);
        map.put("chargeamount", chargeamount);
        map.put("pcmunit", pcmunit);
        map.put("Pcmremarks", pcmremarks);
        map.put("userId", operationTO.getUserId());
        map.put("contractId", operationTO.getContractId());
        System.out.println("map = " + map);

        try {
            if (penality != null || !"".equals(penality)) {
                insertStatus = (Integer) getSqlMapClientTemplate().update("operation.insertpenalitycharges", map);
                System.out.println("insertStatus==" + insertStatus);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertpenalitycharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertpenalitycharges", sqlException);
        }
        return insertStatus;
    }

    public ArrayList getLinerlist() {
        Map map = new HashMap();
        ArrayList Linerlist = new ArrayList();
        try {
            // System.out.println("map = " + map);
            Linerlist = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getLinerlist", map);
            System.out.println("Linerlist size():" + Linerlist.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Linerlist Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "Linerlist", sqlException);
        }
        return Linerlist;

    }

    public ArrayList getviewpenalitycharge(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList viewpenalitycharge = new ArrayList();
        map.put("contractId", operationTO.getContractId());
        System.out.println("operationTO.getBillingTypeId() = " + operationTO.getBillingTypeId());
        System.out.println("map = " + map);
        try {
            viewpenalitycharge = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getviewpenalitycharge", map);
            System.out.println("getviewpenalitycharge size():===" + viewpenalitycharge.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getviewpenalitycharge Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "viewpenalitycharge", sqlException);
        }
        return viewpenalitycharge;
    }

    public int insertdetentioncharges(OperationTO operationTO, String detention, String dcmunit, String chargeamt, String dcmremarks) {
        Map map = new HashMap();
        int insertStatus = 0;

        map.put("detention", detention);
        map.put("dcmunit", dcmunit);
        map.put("chargeamt", chargeamt);
        map.put("dcmremarks", dcmremarks);
        map.put("userId", operationTO.getUserId());
        map.put("contractId", operationTO.getContractId());
        System.out.println("map = " + map);

        try {
            if (detention != null || !"".equals(detention)) {
                insertStatus = (Integer) getSqlMapClientTemplate().update("operation.insertdetentioncharges", map);
                System.out.println("insertStatus==" + insertStatus);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertdetentioncharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertdetentioncharges", sqlException);
        }
        return insertStatus;
    }

    public ArrayList getviewdetentioncharge(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList viewdetentioncharge = new ArrayList();
        map.put("contractId", operationTO.getContractId());
        System.out.println("operationTO.getBillingTypeId() = " + operationTO.getBillingTypeId());
        System.out.println("map = " + map);

        try {

            viewdetentioncharge = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getviewdetentioncharge", map);
            System.out.println("getviewdetentioncharge size():===" + viewdetentioncharge.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getviewdetentioncharge Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "viewdetentioncharge", sqlException);
        }
        return viewdetentioncharge;
    }

    public int Updatepenalitycharges(OperationTO operationTO, String penalitycharge, String id) {
        Map map = new HashMap();
        int insertStatus = 0;
        try {

            if (penalitycharge != null || !"".equals(penalitycharge)) {
                map.put("penalitycharge", penalitycharge);
                map.put("userId", operationTO.getUserId());
                map.put("Id", id);
                insertStatus = (Integer) getSqlMapClientTemplate().update("operation.updatePenalityCharges", map);
                System.out.println("insertStatus==" + insertStatus);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Updatepenalitycharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "Updatepenalitycharges", sqlException);
        }
        return insertStatus;
    }

    public int updatedetentioncharges(OperationTO operationTO, String detentioncharge, String denid) {
        Map map = new HashMap();
        int insertStatus = 0;
        try {

            if (detentioncharge != null || !"".equals(detentioncharge)) {
                map.put("detentioncharge", detentioncharge);
                map.put("userId", operationTO.getUserId());
                map.put("Id", denid);
                insertStatus = (Integer) getSqlMapClientTemplate().update("operation.updatedetentioncharges", map);
                System.out.println("insertStatus==" + insertStatus);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updatedetentioncharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "updatedetentioncharges", sqlException);
        }
        return insertStatus;
    }

    public int insertOrEditlinermaster(OperationTO operationTO) {
        Map map = new HashMap();
        int insertStatus = 0;
        map.put("linername", operationTO.getLinername());
        map.put("status", operationTO.getStatus());
        map.put("userId", operationTO.getUserId());
        map.put("Linerid", operationTO.getLinerid());
        System.out.println("map = " + map);

        try {

            if (operationTO.getLinerid() != null && operationTO.getLinerid() == "") {
                insertStatus = (Integer) getSqlMapClientTemplate().update("operation.insertlinermaster", map);

            } else {
                insertStatus = (Integer) getSqlMapClientTemplate().update("operation.editSavelinermaster", map);
                System.out.println("updateStatus" + insertStatus);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertlinermaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertlinermaster", sqlException);
        }
        return insertStatus;
    }

    public String checklinerName(OperationTO operationTO) {
        Map map = new HashMap();
        String checklinerName = "";
        try {
            System.out.println("map = " + map);
            map.put("linername", operationTO.getLinername());
            checklinerName = (String) getSqlMapClientTemplate().queryForObject("operation.checklinerName", map);
            System.out.println("checklinerName " + checklinerName);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checklinerName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checklinerName", sqlException);
        }

        return checklinerName;
    }

    public String[] insertContractPTPBillingDetails(OperationTO operationTO, String[] ptpRouteCode, String[] ptpVehicleTypeId, String[] ptpPickupPoint, String[] ptpInterimPoint1, String[] ptpInterimPoint2, String[] ptpInterimPoint3, String[] ptpInterimPoint4, String[] ptpDropPoint, String[] ptpPickupPointId, String[] ptpInterimPointId1, String[] ptpInterimPointId2, String[] ptpInterimPointId3, String[] ptpInterimPointId4, String[] ptpDropPointId, String[] ptpInterimPoint1Km, String[] ptpInterimPoint2Km, String[] ptpInterimPoint3Km, String[] ptpInterimPoint4Km, String[] ptpDropPointKm, String[] interimPoint1Hrs, String[] interimPoint2Hrs, String[] interimPoint3Hrs, String[] interimPoint4Hrs, String[] ptpDropPointHrs, String[] interimPoint1Minutes, String[] interimPoint2Minutes, String[] interimPoint3Minutes, String[] interimPoint4Minutes, String[] ptpDropPointMinutes, String[] interimPoint1RouteId, String[] interimPoint2RouteId, String[] interimPoint3RouteId, String[] interimPoint4RouteId, String[] ptpDropPointRouteId, String[] ptpTotalKm, String[] ptpTotalHours, String[] ptpTotalMinutes, String[] ptpRateWithReefer, String[] ptpRateWithoutReefer, String[] loadTypeId, String[] containerTypeId, String[] containerQty, String[] ptpFromDate, String[] ptpToDate, int userId, String[] fuelVehicle, String[] fuelDg, String[] totalFuel, String[] toll,
            String[] driverBachat, String[] dala, String[] misc, String[] approvalFlag,
            int reqId, String[] marketHireVehicle, String[] marketHireWithReefer, SqlMapClient session) throws FPRuntimeException {
        Map map = new HashMap();
        int insertBillingDetails = 0;
        String[] contractRateIds = new String[ptpPickupPoint.length];
        String[] contractRouteIds = new String[ptpPickupPoint.length];
        int index = 0;
        map.put("userId", userId);
        map.put("contractId", operationTO.getContractId());
        int pointCount = 0;
        int update = 0;
        System.out.println("------- DAO --------- ptpRouteCode ********** ----------  " + ptpRouteCode.length);
        try {
            if (ptpRouteCode != null) {
                for (int i = 0; i < ptpPickupPoint.length; i++) {
                    String[] temp = null;
                    map.put("routeCode", ptpRouteCode[i]);
                    if (ptpPickupPoint != null) {
                        if (ptpPickupPoint.length == 0) {
                            map.put("ptpPickupPoint", "");
                        } else if (ptpPickupPoint.length > 0) {
                            if (ptpPickupPoint[i] != null && !"".equals(ptpPickupPoint[i])) {
                                map.put("ptpPickupPoint", ptpPickupPoint[i]);
                            } else {
                                map.put("ptpPickupPoint", "");
                            }
                        }
                    }
                    if (loadTypeId != null) {
                        if (loadTypeId.length == 0) {
                            map.put("loadTypeId", "");
                        } else if (loadTypeId.length > 0) {
                            if (loadTypeId[i] != null && !"".equals(loadTypeId[i])) {
                                map.put("loadTypeId", loadTypeId[i]);
                            } else {
                                map.put("loadTypeId", "0");
                            }
                        }
                    }
                    if (containerTypeId != null) {
                        if (containerTypeId.length == 0) {
                            map.put("containerTypeId", "");
                        } else if (containerTypeId.length > 0) {
                            if (containerTypeId[i] != null && !"".equals(containerTypeId[i])) {
                                map.put("containerTypeId", containerTypeId[i]);
                            } else {
                                map.put("containerTypeId", "0");
                            }
                        }
                    }
                    if (containerQty != null) {
                        if (containerQty.length == 0) {
                            map.put("containerQty", "");
                        } else if (containerQty.length > 0) {
                            if (containerQty[i] != null && !"".equals(containerQty[i])) {
                                map.put("containerQty", containerQty[i]);
                            } else {
                                map.put("containerQty", "0");
                            }
                        }
                    }
                    if (ptpInterimPoint1 != null) {
                        if (ptpInterimPoint1.length == 0) {
                            map.put("interimPoint1", "");
                        } else if (ptpInterimPoint1.length > 0) {
                            if (ptpInterimPoint1[i] != null && !"".equals(ptpInterimPoint1[i])) {
                                map.put("interimPoint1", ptpInterimPoint1[i]);
                            } else {
                                map.put("interimPoint1", "");
                            }
                        }
                    }
                    if (ptpInterimPoint2 != null) {
                        if (ptpInterimPoint2.length == 0) {
                            map.put("interimPoint2", "");
                        } else if (ptpInterimPoint2.length > 0) {
                            if (ptpInterimPoint2[i] != null && !"".equals(ptpInterimPoint2[i])) {
                                map.put("interimPoint2", ptpInterimPoint2[i]);
                            } else {
                                map.put("interimPoint2", "");
                            }
                        }
                    }

                    if (ptpInterimPoint3 != null) {
                        if (ptpInterimPoint3.length == 0) {
                            map.put("interimPoint3", "");
                        } else if (ptpInterimPoint3.length > 0) {
                            if (ptpInterimPoint3[i] != null && !"".equals(ptpInterimPoint3[i])) {
                                map.put("interimPoint3", ptpInterimPoint3[i]);
                            } else {
                                map.put("interimPoint3", ptpInterimPoint3[i]);
                            }
                        }
                    }
                    if (ptpInterimPoint4 != null) {
                        if (ptpInterimPoint4.length == 0) {
                            map.put("interimPoint4", "");
                        } else if (ptpInterimPoint4.length > 0) {
                            if (ptpInterimPoint4[i] != null && !"".equals(ptpInterimPoint4[i])) {
                                map.put("interimPoint4", ptpInterimPoint4[i]);
                            } else {
                                map.put("interimPoint4", "");
                            }
                        }
                    }
                    if (ptpDropPoint != null) {
                        if (ptpDropPoint.length == 0) {
                            map.put("ptpDropPoint", "");
                        } else if (ptpDropPoint.length > 0) {
                            if (ptpDropPoint[i] != null && !"".equals(ptpDropPoint[i])) {
                                map.put("ptpDropPoint", ptpDropPoint[i]);
                            } else {
                                map.put("ptpDropPoint", "");
                            }
                        }
                    }

                    if (ptpPickupPointId != null) {
                        if (ptpPickupPointId.length == 0) {
                            map.put("ptpPickupPointId", 0);
                        } else if (ptpPickupPointId.length > 0) {
                            if (ptpPickupPointId[i] != null && !"".equals(ptpPickupPointId[i])) {
                                map.put("ptpPickupPointId", Integer.parseInt(ptpPickupPointId[i]));
                            } else {
                                map.put("ptpPickupPointId", 0);
                            }
                        }
                    }
                    if (ptpInterimPointId1 != null) {
                        if (ptpInterimPointId1.length == 0) {
                            map.put("interimPointId1", 0);
                        } else if (ptpInterimPointId1.length > 0) {
                            if (ptpInterimPointId1[i] != null && !"".equals(ptpInterimPointId1[i])) {
                                map.put("interimPointId1", Integer.parseInt(ptpInterimPointId1[i]));
                            } else {
                                map.put("interimPointId1", 0);
                            }
                        }
                    }
                    if (ptpInterimPointId2 != null) {
                        if (ptpInterimPointId2.length == 0) {
                            map.put("interimPointId2", 0);
                        } else if (ptpInterimPointId2.length > 0) {
                            if (ptpInterimPointId2[i] != null && !"".equals(ptpInterimPointId2[i])) {
                                map.put("interimPointId2", Integer.parseInt(ptpInterimPointId2[i]));
                            } else {
                                map.put("interimPointId2", 0);
                            }
                        }
                    }
                    if (ptpInterimPointId3 != null) {
                        if (ptpInterimPointId3.length == 0) {
                            map.put("interimPointId3", 0);
                        } else if (ptpInterimPointId3.length > 0) {
                            if (ptpInterimPointId3[i] != null && !"".equals(ptpInterimPointId3[i])) {
                                map.put("interimPointId3", Integer.parseInt(ptpInterimPointId3[i]));
                            } else {
                                map.put("interimPointId3", 0);
                            }
                        }
                    }
                    if (ptpInterimPointId4 != null) {
                        if (ptpInterimPointId4.length == 0) {
                            map.put("interimPointId4", 0);
                        } else if (ptpInterimPointId4.length > 0) {
                            if (ptpInterimPointId4[i] != null && !"".equals(ptpInterimPointId4[i])) {
                                map.put("interimPointId4", Integer.parseInt(ptpInterimPointId4[i]));
                            } else {
                                map.put("interimPointId4", 0);
                            }
                        }
                    }
                    if (ptpDropPointId != null) {
                        if (ptpDropPointId.length == 0) {
                            map.put("ptpDropPointId", 0);
                        } else if (ptpDropPointId.length > 0) {
                            if (ptpDropPointId[i] != null && !"".equals(ptpDropPointId[i])) {
                                map.put("ptpDropPointId", Integer.parseInt(ptpDropPointId[i]));
                            } else {
                                map.put("ptpDropPointId", 0);
                            }
                        }
                    }

                    if (ptpInterimPoint1Km != null) {
                        if (ptpInterimPoint1Km.length == 0) {
                            map.put("interimPoint1Km", 0.0f);
                        } else if (ptpInterimPoint1Km.length > 0) {
                            if (ptpInterimPoint1Km[i] != null && !"".equals(ptpInterimPoint1Km[i])) {
                                map.put("interimPoint1Km", Double.parseDouble(ptpInterimPoint1Km[i]));
                            } else {
                                map.put("interimPoint1Km", 0.0f);
                            }
                        }
                    }
                    if (ptpInterimPoint2Km != null) {
                        if (ptpInterimPoint2Km.length == 0) {
                            map.put("interimPoint2Km", 0.0f);
                        } else if (ptpInterimPoint2Km.length > 0) {
                            if (ptpInterimPoint2Km[i] != null && !"".equals(ptpInterimPoint2Km[i])) {
                                map.put("interimPoint2Km", Double.parseDouble(ptpInterimPoint2Km[i]));
                            } else {
                                map.put("interimPoint2Km", 0.0f);
                            }
                        }
                    }
                    if (ptpInterimPoint3Km != null) {
                        if (ptpInterimPoint3Km.length == 0) {
                            map.put("interimPoint3Km", 0.0f);
                        } else if (ptpInterimPoint3Km.length > 0) {
                            if (ptpInterimPoint3Km[i] != null && !"".equals(ptpInterimPoint3Km[i])) {
                                map.put("interimPoint3Km", Double.parseDouble(ptpInterimPoint3Km[i]));
                            } else {
                                map.put("interimPoint3Km", 0.0f);
                            }
                        }
                    }
                    if (ptpInterimPoint4Km != null) {
                        if (ptpInterimPoint4Km.length == 0) {
                            map.put("interimPoint4Km", 0.0f);
                        } else if (ptpInterimPoint4Km.length > 0) {
                            if (ptpInterimPoint4Km[i] != null && !"".equals(ptpInterimPoint4Km[i])) {
                                map.put("interimPoint4Km", Double.parseDouble(ptpInterimPoint4Km[i]));
                            } else {
                                map.put("interimPoint4Km", 0.0f);
                            }
                        }
                    }
                    if (ptpDropPointKm != null) {
                        if (ptpDropPointKm.length == 0) {
                            map.put("ptpDropPointKm", 0.0f);
                        } else if (ptpDropPointKm.length > 0) {
                            if (ptpDropPointKm[i] != null && !"".equals(ptpDropPointKm[i])) {
                                map.put("ptpDropPointKm", Double.parseDouble(ptpDropPointKm[i]));
                            } else {
                                map.put("ptpDropPointKm", 0.0f);
                            }
                        }
                    }

                    if (interimPoint1Hrs != null) {
                        if (interimPoint1Hrs.length == 0) {
                            map.put("interimPoint1Hrs", 0.0f);
                        } else if (interimPoint1Hrs.length > 0) {
                            if (interimPoint1Hrs[i] != null && !"".equals(interimPoint1Hrs[i])) {
                                map.put("interimPoint1Hrs", Double.parseDouble(interimPoint1Hrs[i]));
                            } else {
                                map.put("interimPoint1Hrs", 0.0f);
                            }
                        }
                    }

                    if (interimPoint2Hrs != null) {
                        if (interimPoint2Hrs.length == 0) {
                            map.put("interimPoint2Hrs", 0);
                        } else if (interimPoint2Hrs.length > 0) {
                            if (interimPoint2Hrs[i] != null && !"".equals(interimPoint2Hrs[i])) {
                                map.put("interimPoint2Hrs", interimPoint2Hrs[i]);
                            } else {
                                map.put("interimPoint2Hrs", 0);
                            }
                        }
                    }

                    if (interimPoint3Hrs != null) {
                        if (interimPoint3Hrs.length == 0) {
                            map.put("interimPoint3Hrs", 0);
                        } else if (interimPoint3Hrs.length > 0) {
                            if (interimPoint3Hrs[i] != null && !"".equals(interimPoint3Hrs[i])) {
                                map.put("interimPoint3Hrs", interimPoint3Hrs[i]);
                            } else {
                                map.put("interimPoint3Hrs", 0);
                            }
                        }
                    }
                    if (interimPoint4Hrs != null) {
                        if (interimPoint4Hrs.length == 0) {
                            map.put("interimPoint4Hrs", 0);
                        } else if (interimPoint4Hrs.length > 0) {
                            if (interimPoint4Hrs[i] != null && !"".equals(interimPoint4Hrs[i])) {
                                map.put("interimPoint4Hrs", interimPoint4Hrs[i]);
                            } else {
                                map.put("interimPoint4Hrs", 0);
                            }
                        }
                    }
                    if (ptpDropPointHrs != null) {
                        if (ptpDropPointHrs.length == 0) {
                            map.put("ptpDropPointHrs", 0);
                        } else if (ptpDropPointHrs.length > 0) {
                            if (ptpDropPointHrs[i] != null && !"".equals(ptpDropPointHrs[i])) {
                                map.put("ptpDropPointHrs", ptpDropPointHrs[i]);
                            } else {
                                map.put("ptpDropPointHrs", 0);
                            }
                        }
                    }

                    if (interimPoint1Minutes != null) {
                        if (interimPoint1Minutes.length == 0) {
                            map.put("interimPoint1Minutes", 0);
                        } else if (interimPoint1Minutes.length > 0) {
                            if (interimPoint1Minutes[i] != null && !"".equals(interimPoint1Minutes[i])) {
                                map.put("interimPoint1Minutes", interimPoint1Minutes[i]);
                            } else {
                                map.put("interimPoint1Minutes", 0);
                            }
                        }
                    }
                    if (interimPoint2Minutes != null) {
                        if (interimPoint2Minutes.length == 0) {
                            map.put("interimPoint2Minutes", 0);
                        } else if (interimPoint2Minutes.length > 0) {
                            if (interimPoint2Minutes[i] != null && !"".equals(interimPoint2Minutes[i])) {
                                map.put("interimPoint2Minutes", interimPoint2Minutes[i]);
                            } else {
                                map.put("interimPoint2Minutes", 0);
                            }
                        }
                    }
                    if (interimPoint3Minutes != null) {
                        if (interimPoint3Minutes.length == 0) {
                            map.put("interimPoint3Minutes", 0);
                        } else if (interimPoint3Minutes.length > 0) {
                            if (interimPoint3Minutes[i] != null && !"".equals(interimPoint3Minutes[i])) {
                                map.put("interimPoint3Minutes", interimPoint3Minutes[i]);
                            } else {
                                map.put("interimPoint3Minutes", 0);
                            }
                        }
                    }
                    if (interimPoint4Minutes != null) {
                        if (interimPoint4Minutes.length == 0) {
                            map.put("interimPoint4Minutes", 0);
                        } else if (interimPoint4Minutes.length > 0) {
                            if (interimPoint4Minutes[i] != null && !"".equals(interimPoint4Minutes[i])) {
                                map.put("interimPoint4Minutes", interimPoint4Minutes[i]);
                            } else {
                                map.put("interimPoint4Minutes", 0);
                            }
                        }
                    }
                    if (ptpDropPointMinutes != null) {
                        if (ptpDropPointMinutes.length == 0) {
                            map.put("ptpDropPointMinutes", 0);
                        } else if (ptpDropPointMinutes.length > 0) {
                            if (ptpDropPointMinutes[i] != null && !"".equals(ptpDropPointMinutes[i])) {
                                map.put("ptpDropPointMinutes", ptpDropPointMinutes[i]);
                            } else {
                                map.put("ptpDropPointMinutes", 0);
                            }
                        }
                    }

                    if (interimPoint1RouteId != null) {
                        if (interimPoint1RouteId.length == 0) {
                            map.put("interimPoint1RouteId", 0);
                        } else if (interimPoint1RouteId.length > 0) {
                            if (interimPoint1RouteId[i] != null && !"".equals(interimPoint1RouteId[i])) {
                                map.put("interimPoint1RouteId", interimPoint1RouteId[i]);
                                pointCount++;
                            } else {
                                map.put("interimPoint1RouteId", 0);
                            }
                        }
                    }
                    if (interimPoint2RouteId != null) {
                        if (interimPoint2RouteId.length == 0) {
                            map.put("interimPoint2RouteId", 0);
                        } else if (interimPoint2RouteId.length > 0) {
                            if (interimPoint2RouteId[i] != null && !"".equals(interimPoint2RouteId[i])) {
                                map.put("interimPoint2RouteId", interimPoint2RouteId[i]);
                                pointCount++;
                            } else {
                                map.put("interimPoint2RouteId", 0);
                            }
                        }
                    }
                    if (interimPoint3RouteId != null) {
                        if (interimPoint3RouteId.length == 0) {
                            map.put("interimPoint3RouteId", 0);
                        } else if (interimPoint3RouteId.length > 0) {
                            if (interimPoint3RouteId[i] != null && !"".equals(interimPoint3RouteId[i])) {
                                map.put("interimPoint3RouteId", interimPoint3RouteId[i]);
                                pointCount++;
                            } else {
                                map.put("interimPoint3RouteId", 0);
                            }
                        }
                    }
                    if (interimPoint4RouteId != null) {
                        if (interimPoint4RouteId.length == 0) {
                            map.put("interimPoint4RouteId", 0);
                        } else if (interimPoint4RouteId.length > 0) {
                            if (interimPoint4RouteId[i] != null && !"".equals(interimPoint4RouteId[i])) {
                                map.put("interimPoint4RouteId", interimPoint4RouteId[i]);
                                pointCount++;
                            } else {
                                map.put("interimPoint4RouteId", 0);
                            }
                        }
                    }
                    if (ptpDropPointRouteId != null) {
                        if (ptpDropPointRouteId.length == 0) {
                            map.put("ptpDropPointRouteId", 0);
                        } else if (ptpDropPointRouteId.length > 0) {
                            if (ptpDropPointRouteId[i] != null && !"".equals(ptpDropPointRouteId[i])) {
                                map.put("ptpDropPointRouteId", ptpDropPointRouteId[i]);
                                pointCount++;
                            } else {
                                map.put("ptpDropPointRouteId", 0);
                            }
                        }
                    }
                    if (ptpTotalKm != null) {
                        if (ptpTotalKm.length == 0) {
                            map.put("ptpTotalKm", 0.0f);
                        } else if (ptpTotalKm.length > 0) {
                            if (ptpTotalKm[i] != null && !"".equals(ptpTotalKm[i])) {
                                map.put("ptpTotalKm", ptpTotalKm[i]);
                            } else {
                                map.put("ptpTotalKm", 0.0f);
                            }
                        }
                    }
                    if (ptpTotalHours != null) {
                        if (ptpTotalHours.length == 0) {
                            map.put("ptpTotalHours", 0.0f);
                        } else if (ptpTotalHours.length > 0) {
                            if (ptpTotalHours[i] != null && !"".equals(ptpTotalHours[i])) {
                                map.put("ptpTotalHours", ptpTotalHours[i]);
                            } else {
                                map.put("ptpTotalHours", 0.0f);
                            }
                        }
                    }
                    if (ptpTotalMinutes != null) {
                        if (ptpTotalMinutes.length == 0) {
                            map.put("ptpTotalMinutes", 0.0f);
                        } else if (ptpTotalMinutes.length > 0) {
                            if (ptpTotalMinutes[i] != null && !"".equals(ptpTotalMinutes[i])) {
                                map.put("ptpTotalMinutes", ptpTotalMinutes[i]);
                            } else {
                                map.put("ptpTotalMinutes", 0.0f);
                            }
                        }
                    }
                    Date today = new Date();
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    if (ptpFromDate != null) {
                        if (ptpFromDate.length == 0) {
                            map.put("ptpFromDate", df.format(today));
                        } else if (ptpFromDate.length > 0) {
                            if (ptpFromDate[i] != null && !"".equals(ptpFromDate[i])) {
                                map.put("ptpFromDate", ptpFromDate[i]);
                            } else {
                                map.put("ptpFromDate", df.format(today));
                            }
                        }
                    }

                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(today);

                    calendar.add(Calendar.MONTH, 1);
                    calendar.set(Calendar.DAY_OF_MONTH, 1);
                    calendar.add(Calendar.DATE, -1);

                    Date lastDayOfMonth = calendar.getTime();
                    if (ptpToDate != null) {
                        if (ptpToDate.length == 0) {
                            map.put("ptpToDate", df.format(lastDayOfMonth));
                        } else if (ptpToDate.length > 0) {
                            if (ptpToDate[i] != null && !"".equals(ptpToDate[i])) {
                                map.put("ptpToDate", ptpToDate[i]);
                            } else {
                                map.put("ptpToDate", df.format(lastDayOfMonth));
                            }
                        }
                    }
                    map.put("pointCount", pointCount);
                    map.put("ptpVehicleTypeId", ptpVehicleTypeId[i]);
                    System.out.println(" map issss" + map);
                    Float orgRateWithReefer = 0.0f;
                    Float orgRateWithoutReefer = 0.0f;
                    String fuelVehicle1, fuelDg1, totalFuel1, toll1, driverBachat1, dala1, misc1 = "";
                    String ptVt, ptpRw, ptpRwo, appF, mkv, mkwv = "";
                    String rate = (String) session.queryForObject("operation.getContractVehicleTypeForPointsCheck", map);
                    System.out.println(" rate" + rate);
                    String[] temp7 = null;
                    if (rate != null) {
                        temp7 = rate.split("~");

                        if (temp7.length > 0) {
                            orgRateWithReefer = Float.parseFloat(temp7[0]);
                            orgRateWithoutReefer = Float.parseFloat(temp7[1]);
                        }
                    }

                    if (Float.parseFloat(ptpRateWithReefer[i]) >= orgRateWithReefer && Float.parseFloat(ptpRateWithoutReefer[i]) >= orgRateWithoutReefer) {
                        appF = "1";
                    } else {
                        appF = "2";
                    }

                    if (approvalFlag != null && !"".equals(approvalFlag)) {
                        map.put("approvalFlag", approvalFlag[i]);
                    }
                    map.put("reqId", reqId);
                    map.put("pointCount", pointCount);
                    System.out.println("operationTO.getContractId() = " + operationTO.getContractId());
                    System.out.println("map routes  in the DAO = " + map);
                    //System.out.println("approvalFlag[i] --------- ***** DAO &&&&&&&&&& "+approvalFlag[i]);
                    insertBillingDetails = (Integer) session.insert("operation.insertContractPTPBillingDetails", map);
                    contractRouteIds[i] = insertBillingDetails + "";

                    if (insertBillingDetails > 0) {
                        operationTO.setRouteContractId(insertBillingDetails);

                        fuelVehicle1 = fuelVehicle == null || fuelVehicle.equals("") ? "0" : fuelVehicle[i];
                        fuelDg1 = fuelDg == null || fuelDg.equals("") ? "0" : fuelDg[i];
                        totalFuel1 = totalFuel == null || totalFuel.equals("") ? "0" : totalFuel[i];
                        toll1 = toll == null || toll.equals("") ? "0" : toll[i];
                        driverBachat1 = driverBachat == null || driverBachat.equals("") ? "0" : driverBachat[i];
                        dala1 = dala == null || dala.equals("") ? "0" : dala[i];
                        misc1 = misc == null || misc.equals("") ? "0" : misc[i];

                        if (operationTO.getBillingTypeId().equals("4")) {
                            contractRateIds[i] = insertPTPContractRates(operationTO, ptpVehicleTypeId[i], "0", "0", userId, "0", "0", "0", "0", "0", "0", "0", approvalFlag[i], reqId, "0", "0", session);
                        } else {

                            ptVt = ptpVehicleTypeId == null || ptpVehicleTypeId.equals("") ? "" : ptpVehicleTypeId[i];
                            System.out.println("ptpRateWithReefer[i] " + ptpRateWithReefer[i]);
                            ptpRw = ptpRateWithReefer == null || ptpRateWithReefer.equals("") ? "" : ptpRateWithReefer[i];
                            ptpRwo = ptpRateWithoutReefer == null || ptpRateWithoutReefer.equals("") ? "" : ptpRateWithoutReefer[i];
                            mkv = marketHireVehicle == null || marketHireVehicle.equals("") ? "" : marketHireVehicle[i];
                            mkwv = marketHireWithReefer == null || marketHireWithReefer.equals("") ? "" : marketHireWithReefer[i];
                            contractRateIds[i] = insertPTPContractRates(operationTO, ptVt, ptpRw, ptpRwo, userId,
                                    fuelVehicle1, fuelDg1, totalFuel1, toll1, driverBachat1, dala1, misc1, appF, reqId, mkv, mkwv, session);
                        }
                    }
                }

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractStandardCharge Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertContractStandardCharge", sqlException);
        }

        return contractRateIds;
    }

    public int insertContractPTPWBillingDetails(OperationTO operationTO, String[] ptpwRouteContractCode, String[] ptpwVehicleTypeId, String[] ptpwPickupPointId, String[] ptpwPickupPoint, String[] ptpwDropPoint, String[] ptpwDropPointId, String[] ptpwPointRouteId, String[] ptpwTotalKm, String[] ptpwTotalHrs, String[] ptpwTotalMinutes, String[] ptpwRateWithReefer, String[] ptpwRateWithoutReefer, String[] loadTypeId, String[] containerTypeId, String[] containerQty, int userId) {
        Map map = new HashMap();
        int insertBillingDetails = 0;
        int index = 0;
        map.put("userId", userId);
        map.put("contractId", operationTO.getContractId());
        int pointCount = 2;
        int update = 0;
        try {
            try {
                for (int i = 0; i < ptpwRouteContractCode.length; i++) {
                    String[] temp = null;
                    map.put("routeCode", ptpwRouteContractCode[i]);
                    map.put("loadTypeId", loadTypeId[i]);
                    map.put("containerTypeId", containerTypeId[i]);
                    map.put("containerQty", containerQty[i]);
                    map.put("ptpwPickupPointId", ptpwPickupPointId[i]);
                    map.put("ptpwPickupPoint", ptpwPickupPoint[i]);
                    map.put("ptpwDropPoint", ptpwDropPoint[i]);
                    map.put("ptpwDropPointId", ptpwDropPointId[i]);
                    map.put("ptpwPointRouteId", ptpwPointRouteId[i]);
                    if (ptpwPickupPointId[i] != null && !"".equals(ptpwPickupPointId[i])) {
                        map.put("ptpwPickupPointId", Integer.parseInt(ptpwPickupPointId[i]));
                    } else {
                        map.put("ptpwPickupPointId", 0);
                    }
                    if (ptpwTotalKm[i] != null && !"".equals(ptpwTotalKm[i])) {
                        map.put("ptpwDropPointKm", Double.parseDouble(ptpwTotalKm[i]));
                        map.put("ptpwTotalKm", Double.parseDouble(ptpwTotalKm[i]));
                    } else {
                        map.put("ptpwDropPointKm", 0.0f);
                        map.put("ptpwTotalKm", 0.0f);
                    }
                    if (ptpwTotalHrs[i] != null && !"".equals(ptpwTotalHrs[i])) {
                        map.put("ptpwTotalHrs", Integer.parseInt(ptpwTotalHrs[i]));
                    } else {
                        map.put("ptpwTotalHrs", 0);
                    }
                    if (ptpwTotalMinutes[i] != null && !"".equals(ptpwTotalMinutes[i])) {
                        map.put("ptpwTotalMinutes", Integer.parseInt(ptpwTotalMinutes[i]));
                    } else {
                        map.put("ptpwTotalMinutes", 0);
                    }

                    map.put("pointCount", pointCount);
                    System.out.println("map routes in the dao = " + map);
                    insertBillingDetails = (Integer) getSqlMapClientTemplate().insert("operation.insertContractPTPWBillingDetails", map);
                    if (insertBillingDetails > 0) {
                        operationTO.setRouteContractId(insertBillingDetails);
                        update = insertPTPWContractRates(operationTO, ptpwVehicleTypeId[i], ptpwRateWithReefer[i], ptpwRateWithoutReefer[i], userId);
                    }
                }
            } catch (Exception ex) {
                Writer writer = new StringWriter();
                PrintWriter printWriter = new PrintWriter(writer);
                ex.printStackTrace(printWriter);
                String s = writer.toString();
                System.out.println("s = " + s);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractPTPWBillingDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertContractPTPWBillingDetails", sqlException);
        }

        return update;
    }

    public ArrayList getLinerList() {
        Map map = new HashMap();
        ArrayList linerList = new ArrayList();

        try {
            linerList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getLinerListNEW", map);
            System.out.println("linerList =" + linerList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getLinerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getLinerList", sqlException);
        }
        return linerList;
    }

    public ArrayList getMovementTypeList() {
        Map map = new HashMap();
        ArrayList containerTypeList = new ArrayList();

        try {
            containerTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getMovementTypeList", map);
            System.out.println(" getMovementTypeList =" + containerTypeList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleTypeList Error" + sqlException.toString());

            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleTypeList", sqlException);
        }
        return containerTypeList;
    }

    public ArrayList getConsignorAddressDetails(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList ConsignorAddressDetails = new ArrayList();

        try {
            map.put("customerId", operationTO.getCustomerId());
//             System.out.println("customerId==="+map);

            ConsignorAddressDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getConsignorAddressDetails", map);
            System.out.println(" ConsignorAddressDetails =" + ConsignorAddressDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ConsignorAddressDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "ConsignorAddressDetails", sqlException);
        }
        return ConsignorAddressDetails;
    }

    public int insertContainer(OperationTO operationTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int insertMultiplePoints = 0;
        map.put("userId", userId);
        try {
            String consignmentId = operationTO.getConsignmentOrderId();
            String[] vehTypeIdValue = operationTO.getVehTypeIdValue();
            String[] containerTypeValue = operationTO.getContainerTypeValue();
            String[] containerName = operationTO.getContainerName1();
            String[] containerLinerName = operationTO.getContainerLinerName();
            String[] containerFreightCharges = operationTO.getContainerFreightCharges();
            String[] consignmentContainerIds = operationTO.getConsignmentContainerIds();

            map.put("consignmentId", consignmentId);
            map.put("userId", userId);
            System.out.println("containerTypeValue.length:" + containerTypeValue.length);
            
            if (containerTypeValue.length > 0) {
                for (int j = 0; j < containerTypeValue.length; j++) {
                    System.out.println("containerTypeValue[j]:" + containerTypeValue[j]);
                    map.put("vehicleTypeId", vehTypeIdValue[j]);
                    map.put("containerType", containerTypeValue[j]);
                    map.put("containerNo", containerName[j]);
                    map.put("containerLinerId", containerLinerName[j]);
                    map.put("containerFreightCharges", containerFreightCharges[j]);
                    map.put("consignmentContainerIds", consignmentContainerIds[j]);
                    map.put("quantity", 1);
                    if (consignmentContainerIds[j] != "") {
                        System.out.println("map for update Container:" + map);
                        insertMultiplePoints = (Integer) session.update("operation.updateContainer", map);
                    } else {
                        System.out.println("map for insert Container:" + map);
                        insertMultiplePoints = (Integer) session.update("operation.insertContainer", map);
                    }
                }
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("insertContainer Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertContainer", sqlException);
        }

        return insertMultiplePoints;
    }

    public String getApprovedStatus(OperationTO operationTO) {
        Map map = new HashMap();
        String status = "";

        String approvedStatus = "";
        try {
//            regNo = regNo.replace(" ", "");
//            System.out.println("trimmed regno=" + regNo);
            map.put("bunkId", operationTO.getBunkId());
            status = (String) getSqlMapClientTemplate().queryForObject("operation.getApprovedStatus", map);
            if (status != null) {
                approvedStatus = status;
            }

            System.out.println("getApprovedStatus" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getApprovedStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getApprovedStatus", sqlException);
        }

        return approvedStatus;
    }

    public ArrayList getBunkLists() {
        Map map = new HashMap();
        ArrayList bunkList = new ArrayList();
        try {
            if (getSqlMapClientTemplate().queryForList("operation.getBunkLists", map) != null) {
                bunkList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getBunkLists", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBunkNameLocation", ne);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBunkNameLocation", sqlException);
        }
        return bunkList;
    }

    public ArrayList getBunkCurrentPrice() {
        Map map = new HashMap();
        ArrayList bunkList = new ArrayList();
        try {
            bunkList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getBunkCurrentPrice", map);
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBunkNameLocation", ne);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBunkNameLocation", sqlException);
        }
        return bunkList;
    }

    public ArrayList getTotalRtolist() {
        Map map = new HashMap();
        ArrayList tortalRtoList = new ArrayList();
        try {
            System.out.println("map = " + map);
            tortalRtoList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getRtoList", map);
            System.out.println("tortalRtoList size():" + tortalRtoList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("moventTypeMasterList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "moventTypeMasterList", sqlException);
        }
        return tortalRtoList;

    }

    public ArrayList getAvailableRtolist(String routeId) {
        Map map = new HashMap();
        ArrayList tortalRtoList = new ArrayList();
        try {
            map.put("routeId", routeId);
            System.out.println("map for= " + map);
            tortalRtoList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getAvailableRtoList", map);
            System.out.println("getAvailableRtolist size():" + tortalRtoList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("moventTypeMasterList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "moventTypeMasterList", sqlException);
        }
        return tortalRtoList;

    }

    public ArrayList getRouteMappedRtolist(String routeId) {
        Map map = new HashMap();
        ArrayList tortalRtoList = new ArrayList();
        try {
            map.put("routeId", routeId);
            System.out.println("map for= " + map);
            tortalRtoList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getRouteMappedesRtoList", map);
            System.out.println("getAvailableRtolist size():" + tortalRtoList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("moventTypeMasterList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "moventTypeMasterList", sqlException);
        }
        return tortalRtoList;

    }

    public ArrayList getContractRoutesOriginDestinationOtherICD(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList contractRouteOtherICDList = new ArrayList();
        map.put("contractId", operationTO.getContractId());
        map.put("contractRouteOrigin", operationTO.getContractRouteOrigin());
        if (!"".equals(operationTO.getContractRouteInteream()) || operationTO.getContractRouteInteream() != null || !"null".equals(operationTO.getContractRouteInteream())) {
            map.put("contractRouteInteram", operationTO.getContractRouteInteream());
        } else {
            map.put("contractRouteInteram", "0");
        }
        System.out.println("map = " + map);
        try {
            contractRouteOtherICDList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getContractRoutesOriginDestinationOtherICD", map);
            System.out.println("getContractRoutesOriginDestinationOtherICD size:" + contractRouteOtherICDList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractRoutesOriginDestinationOtherICD Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getContractRoutesOriginDestinationOtherICD", sqlException);
        }
        return contractRouteOtherICDList;
    }

    public ArrayList getRtonamelist() {
        Map map = new HashMap();
        ArrayList rtonamelist = new ArrayList();

        try {

            rtonamelist = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getRtonamelist", map);
            System.out.println("VehiclenoList size():" + rtonamelist.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("rtonamelist Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "rtonamelist", sqlException);
        }
        return rtonamelist;
    }

    public ArrayList getTripTypeMasterList() {
        Map map = new HashMap();
        ArrayList tripTypeMasterList = new ArrayList();
        try {
            System.out.println("map = " + map);
            tripTypeMasterList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getTripTypeMasterList", map);
            System.out.println("tripTypeMasterList size():" + tripTypeMasterList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripTypeMasterList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getTripTypeMasterList", sqlException);
        }
        return tripTypeMasterList;

    }

    public ArrayList getMoventTypeMasterList() {
        Map map = new HashMap();
        ArrayList moventTypeMasterList = new ArrayList();
        try {
            System.out.println("map = " + map);
            moventTypeMasterList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getMoventTypeMasterList", map);
            System.out.println("moventTypeMasterList size():" + moventTypeMasterList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("moventTypeMasterList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "moventTypeMasterList", sqlException);
        }
        return moventTypeMasterList;

    }

    public int insertRtocharges(OperationTO operationTO) {
        Map map = new HashMap();
        int insertStatus = 0;

        map.put("rtoChargeId", operationTO.getRtoChargeId());
        map.put("rtoId", operationTO.getRtoName());
        map.put("VehicleNo", operationTO.getVehicleNo());
        map.put("amounts", operationTO.getAmounts());
        map.put("status", operationTO.getStatus());
        map.put("fromDate", operationTO.getFromDate());
        map.put("toDate", operationTO.getToDate());
        map.put("userId", operationTO.getUserId());
        System.out.println("map = " + map);

        try {

            if (operationTO.getRtoChargeId() != null && "".equals(operationTO.getRtoChargeId())) {
                insertStatus = (Integer) getSqlMapClientTemplate().update("operation.insertRtocharges", map);
                System.out.println("insertStatus==" + insertStatus);
            } else {
                insertStatus = (Integer) getSqlMapClientTemplate().update("operation.editInsertRtocharges", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertRtoCharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertRtomaster", sqlException);
        }
        return insertStatus;
    }

    public ArrayList getchargesmaster() {
        Map map = new HashMap();
        ArrayList chargesmaster = new ArrayList();

        try {

            chargesmaster = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getchargesmaster", map);
            System.out.println("VehiclenoList size():" + chargesmaster.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("rtonamelist Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "rtonamelist", sqlException);
        }
        return chargesmaster;
    }

    public ArrayList getCashList() {
        Map map = new HashMap();
        ArrayList cashList = new ArrayList();
        try {
            cashList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.cashList", map);
            System.out.println("cashList size=" + cashList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("citylist Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "citylist List", sqlException);
        }
        return cashList;
    }

    public int insertCity(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("cashDate", operationTO.getCashDate());
        map.put("cash", operationTO.getCash());
        map.put("userId", operationTO.getUserId());
        System.out.println("insertCity map = " + map);
        int status = 0;
        try {
// status = (Integer) getSqlMapClientTemplate().insert("operation.insertWorkOrder", map);
            status = (Integer) getSqlMapClientTemplate().update("operation.insertCashLog", map);
            String balance = (String) getSqlMapClientTemplate().queryForObject("operation.getBalance", map);
            if ("".equals(balance) || balance == null) {

                balance = operationTO.getCash();
                map.put("balance", balance);
                System.out.println("map for cash insert:" + map);
                status = (Integer) getSqlMapClientTemplate().update("operation.insertCashReceived", map);
            } else {
                balance = String.valueOf(Double.parseDouble(balance) + Double.parseDouble(operationTO.getCash()));
                map.put("balance", balance);
                System.out.println("map for cash update:" + map);
                status = (Integer) getSqlMapClientTemplate().update("operation.updateCashReceived", map);
            }

            System.out.println("status is in insertdao " + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertCity Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertCity", sqlException);
        }
        return status;
    }

    public ArrayList getMenulist() {
        Map map = new HashMap();
        ArrayList Menulist = new ArrayList();
        try {
            // System.out.println("map = " + map);
            Menulist = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getMenulist", map);
            System.out.println("Menulist size():" + Menulist.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Menulist Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "Menulist", sqlException);
        }
        return Menulist;

    }

    public ArrayList getsubMenulist() {
        Map map = new HashMap();
        ArrayList subMenulist = new ArrayList();
        try {
            // System.out.println("map = " + map);
            subMenulist = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getsubMenulist", map);
            System.out.println("subMenulist size():" + subMenulist.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("subMenulist Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "subMenulist", sqlException);
        }
        return subMenulist;

    }

    public ArrayList getmodulelist() {
        Map map = new HashMap();
        ArrayList modulelist = new ArrayList();
        try {
            // System.out.println("map = " + map);
            modulelist = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getmodulelist", map);
            System.out.println("modulelist size():" + modulelist.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("modulelist Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "modulelist", sqlException);
        }
        return modulelist;
    }

    public int saveFunctionMaster(OperationTO operationTO) {
        Map map = new HashMap();
        int insertStatus = 0;
        map.put("menuname", operationTO.getFunctionmenu());
        map.put("submenu", operationTO.getSubmodule());
        map.put("module", operationTO.getModule());
        map.put("function", operationTO.getFunction());
        map.put("URI", operationTO.getFunctionuri());
        map.put("status", operationTO.getStatus());
        map.put("userId", operationTO.getUserId());
        System.out.println("map = " + map);

        try {

//            if (operationTO.getMenuname() != null && operationTO.getMenuname() == "") {
            insertStatus = (Integer) getSqlMapClientTemplate().update("operation.saveFunctionMaster", map);
//            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveFunctionMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveFunctionMaster", sqlException);
        }
        return insertStatus;
    }

    public ArrayList getDetaintionTimeSlot() {
        Map map = new HashMap();
        ArrayList detaintionTimeSlot = new ArrayList();

        try {
            detaintionTimeSlot = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getDetaintionTimeSlotList", map);
            System.out.println("detaintionTimeSlot size():===" + detaintionTimeSlot.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleContainerContractQtyList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getDetaintionTimeSlotList", sqlException);
        }
        return detaintionTimeSlot;
    }

    public ArrayList getInsuranceList() {
        Map map = new HashMap();
        ArrayList Insurance = new ArrayList();
        try {
            // System.out.println("map = " + map);
            Insurance = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getInsuranceList", map);
            System.out.println("InsuranceList size():" + Insurance.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getrtoList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getrtoList", sqlException);
        }
        return Insurance;

    }

    //    symbol master
    public int updateConsignmetFreight(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int insertMultiplePoints = 0;
        int updatetrip = 0;
        double tripRevenue = 0.0;
        map.put("userId", userId);
        try {
            String consignmentId = operationTO.getConsignmentOrderId();
            map.put("consignmentId", consignmentId);
            map.put("userId", userId);
            map.put("freightCharges", operationTO.getTotFreightAmount());
            System.out.println("map for update freight:" + map);
            insertMultiplePoints = (Integer) getSqlMapClientTemplate().update("operation.updateConsignmentFreight", map);
            int noOfContainer = (Integer) getSqlMapClientTemplate().queryForObject("operation.getTotalNoOfContainer", map);
            if (noOfContainer != 0) {
                tripRevenue = (double) (Integer.parseInt(operationTO.getTotFreightAmount()) / noOfContainer);
            }
            System.out.println("tripRevenue:" + tripRevenue);
            String tripIds = (String) getSqlMapClientTemplate().queryForObject("operation.getConsignmentTripIds", map);
            System.out.println("tripIds.....:" + tripIds);
            if (tripIds != null && tripIds.length() > 0) {
                String tripId[] = tripIds.split(",");
                for (int i = 0; i < tripId.length; i++) {
                    map.put("tripId", tripId[i]);
                    map.put("tripRevenue", tripRevenue);
                    System.out.println("map for update order:" + map);
                    //    updatetrip = (Integer) getSqlMapClientTemplate().update("operation.updateTripFreight", map);
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("insertContainer Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertContainer", sqlException);
        }

        return insertMultiplePoints;
    }

    public int insertOrEditinsurancemaster(OperationTO operationTO) {
        Map map = new HashMap();
        int insertStatus = 0;
        map.put("companyId", operationTO.getCompanyID1());
        map.put("companyName", operationTO.getCompanyName());
        map.put("insuranceName", operationTO.getInsId());
        map.put("contactNo", operationTO.getContactNo());
        map.put("userId", operationTO.getUserId());
        map.put("status", operationTO.getStatus());
        map.put("agentName", operationTO.getAgentName());
        map.put("agentCode", operationTO.getAgentCode());
        map.put("agentNo", operationTO.getAgentNo());
        System.out.println("map = " + map);

        try {

            if (operationTO.getInsId() != null && operationTO.getInsId() == "") {
                insertStatus = (Integer) getSqlMapClientTemplate().update("operation.insertInsurancemaster", map);
                System.out.println("insertStatus----" + insertStatus);

            } else {
                insertStatus = (Integer) getSqlMapClientTemplate().update("operation.editUpdateInsmaster", map);
                System.out.println("updateStatus" + insertStatus);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertRtomaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertRtomaster", sqlException);
        }
        return insertStatus;

    }

    public ArrayList getmasterlist() {
        Map map = new HashMap();
        ArrayList masterlist = new ArrayList();
        try {
            // System.out.println("map = " + map);
            masterlist = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getmasterlist", map);
            System.out.println("masterlist size():" + masterlist.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("masterlist Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "masterlist", sqlException);
        }
        return masterlist;
    }

    public int SaveMenuEntryMaster(OperationTO operationTO) {
        Map map = new HashMap();
        int insertStatus = 0;
        map.put("entrymenu", operationTO.getEntrymenu());
        map.put("entrytype", operationTO.getEntrytype());
        map.put("userId", operationTO.getUserId());
        System.out.println("map = " + map);

        try {

//            if (operationTO.getMenuname() != null && operationTO.getMenuname() == "") {
            insertStatus = (Integer) getSqlMapClientTemplate().update("operation.SaveMenuEntryMaster", map);
//            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveFunctionMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveFunctionMaster", sqlException);
        }
        return insertStatus;
    }

    public ArrayList getMenuentrylist() {
        Map map = new HashMap();
        ArrayList Menuentrylist = new ArrayList();
        try {
            // System.out.println("map = " + map);
            Menuentrylist = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getMenuentrylist", map);
            System.out.println("masterlist size():" + Menuentrylist.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("masterlist Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "masterlist", sqlException);
        }
        return Menuentrylist;
    }

    public String checkBillingPartyId(String customerId) {
        Map map = new HashMap();
        String status = "";

        String billingParty = "";
        try {
//            regNo = regNo.replace(" ", "");
//            System.out.println("trimmed regno=" + regNo);
            map.put("customerId", customerId);
            status = (String) getSqlMapClientTemplate().queryForObject("operation.checkBillingPartyId", map);
            if (status != null) {
                billingParty = status;
            }

            System.out.println("checkBillingPartyId" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkBillingPartyId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkBillingPartyId", sqlException);
        }

        return billingParty;
    }

    public ArrayList getVehicleContainerContractList(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList vehicleContainerContractList = new ArrayList();
        map.put("contractId", operationTO.getConsignmentOrderId());
//        System.out.println("operationTO.getBillingTypeId() = " + operationTO.getBillingTypeId());
        System.out.println("map = " + map);
        try {
            vehicleContainerContractList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getVehicleContainerContractList", map);
            System.out.println("getVehicleContainerContractList size():===" + vehicleContainerContractList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleContainerContractList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getVehicleContainerContractList", sqlException);
        }
        return vehicleContainerContractList;
    }

    public ArrayList getVehicleContainerContractQtyList(OperationTO operationTO) {
        Map map = new HashMap();
        ArrayList vehicleContainerContractQtyList = new ArrayList();
        map.put("contractId", operationTO.getConsignmentOrderId());
        System.out.println("map = " + map);
        try {
            vehicleContainerContractQtyList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getVehicleContainerContractQtyList", map);
            System.out.println("getVehicleContainerContractQtyList size():===" + vehicleContainerContractQtyList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleContainerContractQtyList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getVehicleContainerContractQtyList", sqlException);
        }
        return vehicleContainerContractQtyList;
    }

    /**
     * This method used to Get User Customer List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int deleteConsignmentContainer(OperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int delete = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("contractId", operationTO.getContractId());
        map.put("consignmentContainerId", operationTO.getConsignmentContainerId());
        map.put("userId", userId);
        System.out.println("map = " + map);
        try {
            delete = (Integer) getSqlMapClientTemplate().update("operation.deleteConsignmentContainer", map);
            System.out.println("delete" + delete);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("deleteconsignmentContainer Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "deleteconsignmentContainer", sqlException);
        }
        return delete;
    }

    public ArrayList getCitylist() {
        Map map = new HashMap();
        ArrayList Citylist = new ArrayList();
        try {
            // System.out.println("map = " + map);
            Citylist = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCitylist", map);
            System.out.println("Citylist size():" + Citylist.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Citylist Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "Citylist", sqlException);
        }
        return Citylist;

    }

    public ArrayList getCustomerDetailsWithoutContract(OperationTO operationTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList customerDetails = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("customerName", operationTO.getCustomerName() + "%");
        map.put("customerCode", operationTO.getCustomerCode() + "%");
        map.put("userId", operationTO.getUserId());
        map.put("roleId", operationTO.getRoleId());

        String empId = (String) getSqlMapClientTemplate().queryForObject("customer.getEmployeeId", map);
        map.put("empId", empId);

        System.out.println("map = " + map);
        try {
            if (operationTO.getCustomerName() != null) {
                customerDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCustomerNameDetailsWithoutContract", map);
            } else if (operationTO.getCustomerCode() != null) {
                customerDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCustomerCodeDetails", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCustomerDetails", sqlException);
        }
        return customerDetails;
    }

    public int insertOrEditrtomaster(OperationTO operationTO) {
        Map map = new HashMap();
        int insertStatus = 0;

        map.put("rtoCode", operationTO.getRtoCode());
        map.put("rtoName", operationTO.getRtoName());
        map.put("rtoId", operationTO.getRtoId());
        map.put("userId", operationTO.getUserId());
        map.put("status", operationTO.getStatus());
        map.put("cityId", operationTO.getCityId());
        System.out.println("map = " + map);

        try {

            if (operationTO.getRtoId() != null && operationTO.getRtoId() == "") {
                insertStatus = (Integer) getSqlMapClientTemplate().update("operation.insertRtomaster", map);

            } else {
                insertStatus = (Integer) getSqlMapClientTemplate().update("operation.editSavertomaster", map);
                System.out.println("updateStatus" + insertStatus);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertRtomaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertRtomaster", sqlException);
        }
        return insertStatus;

    }

    public ArrayList getFuelPriceApprovalList() {
        Map map = new HashMap();
        ArrayList viewfuelPriceMaster = new ArrayList();

        try {
            viewfuelPriceMaster = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getFuelPriceApprovalList", map);

            //////System.out.println("viewfuelPriceMaster size=" + viewfuelPriceMaster.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getfuelPriceMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getfuelPriceMaster", sqlException);
        }
        return viewfuelPriceMaster;
    }

    public int getCompanyID() {
        Map map = new HashMap();
        ArrayList compList = new ArrayList();
        int orgID = 0;
        String organisationId = "";

        try {
            organisationId = (String) getSqlMapClientTemplate().queryForObject("operation.getCompanyID", map);
            if (organisationId != null) {
                orgID = Integer.parseInt(organisationId);
                System.out.println("orgID ------------- ************ " + orgID);
            } else {
                System.out.println("ELSe ------------- ************ ");
            }

            //////System.out.println("viewfuelPriceMaster size=" + viewfuelPriceMaster.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCompanyID Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCompanyID", sqlException);
        }
        return orgID;
    }

    public int updateCustApprovedStatus(OperationTO operationTO) {
        Map map = new HashMap();
        int updateStatus = 0;
        int status1 = 0;
        map.put("userId", operationTO.getUserId());
        map.put("status", operationTO.getStatus());
        try {
            map.put("contractRateId", operationTO.getContractRateId());
            System.out.println("map@@" + map);
            int status = getSqlMapClientTemplate().update("operation.updateCustApprovedStatus", map);
            System.out.println("approveCustomerContractRate" + status);
            System.out.println("operationTO.getStatus()" + operationTO.getStatus());
            if ("1".equals(operationTO.getStatus()) && status == 1) {
                status1 = 1;
            } else if ("3".equals(operationTO.getStatus()) && status == 1) {
                status1 = 3;
            }

            String rateWithReefer = "", rateWithoutReefer = "", finalPointId = "", interim4 = "", interim3 = "", interim2 = "", interim1 = "", firstPickPointId = "";
            String customerName = "", vechTypId = "", containerTypId = "", lodTypId = "", routeContractId = "", approvalstatus = "", contractStatus = "", requestBy = "";
            String contractRateDetails = (String) getSqlMapClientTemplate().queryForObject("operation.getContractRateDetails", map);

            String temp[] = contractRateDetails.split("~");
            routeContractId = temp[0];
            rateWithReefer = temp[1];
            rateWithoutReefer = temp[2];
            approvalstatus = temp[3];
            vechTypId = temp[4];
            requestBy = temp[5];
            customerName = temp[6];
            if ("1".equals(approvalstatus)) {
                contractStatus = "Approved";
            } else {
                contractStatus = "Rejected";
            }
            map.put("userId", requestBy);
            String email = (String) getSqlMapClientTemplate().queryForObject("operation.getRequestByEmail", map);
            map.put("routeContractId", routeContractId);
            String contractRoutesDetails = (String) getSqlMapClientTemplate().queryForObject("operation.getContractRoutesDetails", map);
            String temp1[] = contractRoutesDetails.split("~");
            firstPickPointId = temp1[0];
            interim1 = temp1[1];
            interim2 = temp1[2];
            interim3 = temp1[3];
            interim4 = temp1[4];
            finalPointId = temp1[5];
            lodTypId = temp1[6];
            containerTypId = temp1[7];
            String emailFormat = "<html>"
                    + "<body><table border='1' align='center' width='80%' cellpadding='0' cellspacing='0'>"
                    + "<tr>"
                    + "<th colspan='2'>Contract Rate Approval for Customer &nbsp;&nbsp:&nbsp;&nbsp " + customerName + " </th>"
                    + "</tr>"
                    + "<tr><td>&nbsp;&nbsp;Vehicle Type</td><td>&nbsp;&nbsp;" + vechTypId + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Container Type</td><td>&nbsp;&nbsp;" + containerTypId + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Load Type</td><td>&nbsp;&nbsp;" + lodTypId + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Pickup point</td><td>&nbsp;&nbsp;" + firstPickPointId + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Interim Point1</td><td>&nbsp;&nbsp;" + interim1 + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Interim Point2</td><td>&nbsp;&nbsp;" + interim2 + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Interim Point3</td><td>&nbsp;&nbsp;" + interim3 + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Interim Point4</td><td>&nbsp;&nbsp;" + interim4 + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Drop Point</td><td>&nbsp;&nbsp;" + finalPointId + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Rate With Reefer</td><td>&nbsp;&nbsp;" + rateWithReefer + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Rate Without Reefer</td><td>&nbsp;&nbsp;" + rateWithoutReefer + "</td></tr>"
                    + "<tr><td>&nbsp;&nbsp;Status</td><td>&nbsp;&nbsp;" + contractStatus + "</td></tr>"
                    + "<tr height='25'><td></td><td></td></tr>"
                    + "<tr><td colspan='2' align='center'>"
                    + "</td></tr>"
                    + "</table><br/><br/><br/><br/><br/></body>"
                    + "<script></script></html>";

            String subject = "Contract Rate Approval Status";
            String content = emailFormat;

            TripTO tripTO = new TripTO();
            tripTO.setMailTypeId("2");
            tripTO.setMailSubjectTo(subject);
            tripTO.setMailSubjectCc(subject);
            tripTO.setMailSubjectBcc("");
            tripTO.setMailContentTo(content);
            tripTO.setMailContentCc(content);
            tripTO.setMailContentBcc("");
            tripTO.setMailIdTo(email);
            tripTO.setMailIdCc("rohanb.ampsolutions@gmail.com,arun.ampsolutions@gmail.com,sachink@jmbaxi.com");
            tripTO.setMailIdBcc("");
            int insertMailDetails = 0;
            map.put("mailTypeId", tripTO.getMailTypeId());
            map.put("mailSubjectTo", tripTO.getMailSubjectTo());
            map.put("mailSubjectCc", tripTO.getMailSubjectCc());
            map.put("mailSubjectBcc", tripTO.getMailSubjectBcc());
            map.put("mailContentTo", tripTO.getMailContentTo());
            map.put("mailContentCc", tripTO.getMailContentCc());
            map.put("mailContentBcc", tripTO.getMailContentBcc());
            map.put("mailTo", tripTO.getMailIdTo());
            map.put("mailCc", tripTO.getMailIdCc());
            map.put("mailBcc", tripTO.getMailIdBcc());
            map.put("userId", "0");
            System.out.println("map####### = " + map);

            insertMailDetails = (Integer) getSqlMapClientTemplate().insert("trip.insertMailDetails", map);
            System.out.println("return values" + status1);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertCustomerContract Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertCustomerContract", sqlException);
        }

        return status1;
    }

    public int insertMultiplePoints(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int insertMultiplePoints = 0;
        int index = 0;
        map.put("userId", userId);
        String hr = "";
        String min = "";
        try {
//            try {
            String consignmentId = operationTO.getConsignmentOrderId();
            String[] pointId = operationTO.getPointId();
            String[] pointRouteId = operationTO.getPointRouteId();
            String[] pointType = operationTO.getPointType();
            String[] pointSequence = operationTO.getOrder();
            String[] pointAddress = operationTO.getPointAddresss();
            String[] routeId = operationTO.getMultiplePointRouteId();
            String[] pointPlanDate = operationTO.getPointPlanDate();
            String[] pointPlanHour = operationTO.getPointPlanHour();
            String[] pointPlanMinute = operationTO.getPointPlanMinute();
//                String endPointId = operationTO.getEndPointId();
//                String endPointType = operationTO.getEndPointType();
//                String endOrder = operationTO.getEndOrder();
//                String finalRouteId = operationTO.getFinalRouteId();
//                String endPointAddress = operationTO.getEndPointAddresss();
//                String endPointPlanDate = operationTO.getEndPointPlanDate();
//                String endPointPlanHour = operationTO.getEndPointPlanHour();
//                String endPointPlanMinute = operationTO.getEndPointPlanMinute();
            map.put("consignmentId", consignmentId);
            map.put("userId", userId);
            System.out.println("pointId.length:" + pointId.length);
            if (pointId.length > 0) {
                for (int i = 0; i < pointId.length; i++) {
                    map.put("pointId", pointId[i]);
                    map.put("pointRouteId", pointRouteId[i]);
                    map.put("pointType", pointType[i]);
                    map.put("pointSequence", pointSequence[i]);
                    map.put("pointAddress", pointAddress[i]);
                    map.put("routeId", 0);
                    map.put("pointPlanDate", pointPlanDate[i]);
                    hr = pointPlanHour[i];
                    min = pointPlanMinute[i];
                    if ("".equals(hr)) {
                        hr = "00";
                    }
                    if ("".equals(min)) {
                        min = "00";
                    }
                    map.put("pointPlanTime", hr + ":" + min + ":00");
                    System.out.println("map:" + map);
                    insertMultiplePoints = (Integer) getSqlMapClientTemplate().update("operation.insertMultiplePoints", map);
                }
//                        map.put("pointId", endPointId);
//                        map.put("pointType", endPointType);
//                        map.put("pointSequence", endOrder);
//                        map.put("pointAddress", endPointAddress);
//                        map.put("routeId", finalRouteId);
//                        map.put("pointPlanDate", endPointPlanDate);
//                        map.put("pointPlanTime", endPointPlanHour+":"+endPointPlanMinute+":00");
//                        insertMultiplePoints = (Integer) getSqlMapClientTemplate().update("operation.insertMultiplePoints", map);
            }

//            } catch (Exception ex) {
//                Writer writer = new StringWriter();
//                PrintWriter printWriter = new PrintWriter(writer);
//                ex.printStackTrace(printWriter);
//                String s = writer.toString();
//                System.out.println("s = " + s);
//            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("insertConsignmentNote Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertConsignmentNote", sqlException);
        }

        return insertMultiplePoints;
    }

    public int insertContainer(OperationTO operationTO, String[] vehTypeIdValue, String[] containerTypeValue, String[] containerName, String[] containerLinerName, String[] containerFreightCharges, String[] consignmentContainerIds, int userId) {
        Map map = new HashMap();
        int insertMultiplePoints = 0;
        map.put("userId", userId);
        try {
            String consignmentId = operationTO.getConsignmentOrderId();
//            String[] containerType = operationTO.getContainerType();
//            String[] quantity = operationTO.getContainerQty();
//            String[] containerName = operationTO.getContainerNO();
//            String vehicleTypeId = operationTO.getVehicleTypeId();
//            String[] vehicleTypeIds = vehicleTypeId.split(",");
            map.put("consignmentId", consignmentId);
            map.put("userId", userId);
            System.out.println("containerTypeValue.length:" + containerTypeValue.length);
            if (containerTypeValue.length > 0) {
                for (int j = 0; j < containerTypeValue.length; j++) {
                    System.out.println("containerTypeValue[j]:" + containerTypeValue[j]);
                    map.put("vehicleTypeId", vehTypeIdValue[j]);
                    map.put("containerType", containerTypeValue[j]);
                    map.put("containerNo", containerName[j]);
                    map.put("containerLinerId", containerLinerName[j]);
                    map.put("containerFreightCharges", containerFreightCharges[j]);
                    map.put("consignmentContainerIds", consignmentContainerIds[j]);
                    map.put("quantity", 1);
                    if (consignmentContainerIds[j] != "") {
                        System.out.println("map for update Container:" + map);
                        insertMultiplePoints = (Integer) getSqlMapClientTemplate().update("operation.updateContainer", map);
                    } else {
                        System.out.println("map for insert Container:" + map);
                        insertMultiplePoints = (Integer) getSqlMapClientTemplate().update("operation.insertContainer", map);
                    }
                }
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("insertContainer Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertContainer", sqlException);
        }

        return insertMultiplePoints;
    }

    public int saveGSTCategoryMaster(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int zoneId = 0;
        int saveCityMaster = 0;
        int saveGSTCategoryMaster = 0;
        int saveGSTRateMaster = 0;
        int user = userId;
        try {
            map.put("categoryCode", operationTO.getCategoryCode());
            map.put("categoryName", operationTO.getCategoryName());
            map.put("sacCode", operationTO.getSacCode());
            map.put("sacDescription", operationTO.getSacDescription());
            map.put("hsnCode", operationTO.getHsnCode());
            map.put("hsnDescription", operationTO.getHsnDescription());
            map.put("activeInd", operationTO.getActiveInd());
            map.put("gstType", operationTO.getGstType());
            System.out.println("map 1= " + map);
            saveGSTCategoryMaster = (Integer) getSqlMapClientTemplate().insert("operation.saveGSTCategoryMaster", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveCity Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveGSTCategory", sqlException);
        }
        return saveGSTCategoryMaster;
    }

    public ArrayList getGSTCategoryMasterList() {
        Map map = new HashMap();
        ArrayList GSTCategoryMaster = new ArrayList();
        //        map.put("gstCategoryId", operationTO.getGstCategoryId());

        //////System.out.println("map = " + map);
        try {
            //////System.out.println("this is City Master");
            GSTCategoryMaster = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getGSTCategoryMasterList", map);
            //////System.out.println(" cityMasterList =" + cityMasterList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("GSTCategoryMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "GSTCategoryMaster List", sqlException);
        }

        return GSTCategoryMaster;
    }

    public int updateGSTCategoryMaster(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int user = userId;

        map.put("gstCategoryId", operationTO.getGstCategoryId());
        map.put("categoryCode", operationTO.getCategoryCode());
        map.put("categoryName", operationTO.getCategoryName());
        if ("1".equals(operationTO.getGstType())) {
            map.put("sacCode", operationTO.getSacCode());
            map.put("sacDescription", operationTO.getSacDescription());
        } else {
            map.put("hsnCode", operationTO.getHsnCode());
            map.put("hsnDescription", operationTO.getHsnDescription());
        }
        map.put("activeInd", operationTO.getActiveInd());
        map.put("gstType", operationTO.getGstType());

        try {
            System.out.println("map = " + map);
            status = (Integer) getSqlMapClientTemplate().update("operation.updateGSTCategoryMaster", map);

        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateGSTCategoryMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateGSTCategoryMaster", sqlException);
        }

        return status;
    }

    public ArrayList getCategoryName() {
        Map map = new HashMap();
        ArrayList getCategoryName = new ArrayList();
        //        map.put("gstCategoryId", operationTO.getGstCategoryId());

        //////System.out.println("map = " + map);
        try {
            //////System.out.println("this is City Master");
            getCategoryName = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCategoryName", map);
            //////System.out.println(" cityMasterList =" + cityMasterList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCategoryName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCategoryName List", sqlException);
        }

        return getCategoryName;
    }

    public int updateCategoryMaster(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int user = userId;

        map.put("categoryId", operationTO.getCategoryId());
        map.put("categoryName", operationTO.getCategoryName());
        map.put("categoryCode", operationTO.getCategoryCode());
        map.put("activeInd", operationTO.getActiveInd());
        map.put("gstCategoryName", operationTO.getGstCategoryName());
        map.put("description", operationTO.getDescription());
        map.put("userId", userId);

        try {
            System.out.println("map = " + map);
            status = (Integer) getSqlMapClientTemplate().update("operation.updateCategoryMaster", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateCategoryMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateGSTCategoryMaster", sqlException);
        }

        return status;
    }

    public ArrayList getCategoryMasterList() {
        Map map = new HashMap();
        ArrayList getCategoryMasterList = new ArrayList();
        //        map.put("gstCategoryId", operationTO.getGstCategoryId());

        //////System.out.println("map = " + map);
        try {
            //////System.out.println("this is City Master");
            getCategoryMasterList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCategoryMasterList", map);
            //////System.out.println(" cityMasterList =" + cityMasterList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCategoryMasterList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCategoryMasterList List", sqlException);
        }

        return getCategoryMasterList;
    }

    public int saveCategoryMaster(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int zoneId = 0;
        int saveCityMaster = 0;
        int saveCategoryMaster = 0;
        int user = userId;
        try {
            map.put("categoryName", operationTO.getCategoryName());
            map.put("categoryCode", operationTO.getCategoryCode());
            map.put("gstCategoryName", operationTO.getGstCategoryName());
            map.put("activeInd", operationTO.getActiveInd());
            map.put("description", operationTO.getDescription());
            map.put("userId", userId);
            System.out.println("map 1= " + map);
            saveCategoryMaster = (Integer) getSqlMapClientTemplate().insert("operation.saveCategoryMaster", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveCategoryMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveCategoryMaster", sqlException);
        }
        return saveCategoryMaster;
    }

    public ArrayList gstCode() {
        Map map = new HashMap();
        ArrayList gstCode = new ArrayList();
        //        map.put("gstCategoryId", operationTO.getGstCategoryId());

        //////System.out.println("map = " + map);
        try {
            //////System.out.println("this is City Master");
            gstCode = (ArrayList) getSqlMapClientTemplate().queryForList("operation.gstCode", map);
            //////System.out.println(" cityMasterList =" + cityMasterList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("gstCode Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "gstCode List", sqlException);
        }

        return gstCode;
    }

    public ArrayList getRateMasterList() {
        Map map = new HashMap();
        ArrayList getRateMasterList = new ArrayList();
        //        map.put("gstRateDetailId", gstRateDetailId);
        //        map.put("gstCategoryId", operationTO.getGstCategoryId());

        //////System.out.println("map = " + map);
        try {
            //////System.out.println("this is City Master");
            getRateMasterList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getRateMasterList", map);
            //////System.out.println(" cityMasterList =" + cityMasterList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                     * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRateMasterList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRateMasterList List", sqlException);
        }

        return getRateMasterList;
    }

    public ArrayList gstRatemaster() {
        Map map = new HashMap();
        ArrayList gstRatemaster = new ArrayList();
        //        map.put("gstCategoryId", operationTO.getGstCategoryId());

        //////System.out.println("map = " + map);
        try {
            //////System.out.println("this is City Master");
            gstRatemaster = (ArrayList) getSqlMapClientTemplate().queryForList("operation.gstRatemaster", map);
            //////System.out.println(" cityMasterList =" + cityMasterList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                     * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("gstRatemaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "gstRatemaster List", sqlException);
        }

        return gstRatemaster;
    }

    public ArrayList getRateMasterDetails(int gstRateDetailId) {
        Map map = new HashMap();
        ArrayList getRateMasterDetails = new ArrayList();
        map.put("gstRateDetailId", gstRateDetailId);

        System.out.println("map = " + map);
        try {
            //////System.out.println("this is City Master");
            getRateMasterDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getRateMasterDetails", map);
            //////System.out.println(" cityMasterList =" + cityMasterList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                     * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRateMasterDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRateMasterDetails List", sqlException);
        }

        return getRateMasterDetails;
    }

    public int updateRateMaster(OperationTO operationTO, String gstCode, String gstPercentage, String validFrom, String validTo, int userId, String gstRateDetailId, int gstRateId) {
        Map map = new HashMap();
        int updateRateMaster = 0;
        int user = userId;
        try {
            map.put("categoryName", operationTO.getCategoryName());
            map.put("productName", operationTO.getProductName());
            map.put("state", operationTO.getState());
            map.put("gstRateId", gstRateId);
            //            map.put("gstPercentage", operationTO.getGstPercentage());
            //            map.put("validFrom", operationTO.getValidFrom());
            //            map.put("validTo", operationTO.getValidTo());
            updateRateMaster = (Integer) getSqlMapClientTemplate().update("operation.updateRateMaster", map);
            //            map.put("gstRateId",updateRateMaster);
            System.out.println("updateRateMaster" + updateRateMaster);
            map.put("userId", userId);
            //                map.put("gstRateId", operationTO.getGstRateId());
            //                for(int i=0;i<gstCode.length;i++){
            map.put("gstRateDetailId", gstRateDetailId);
            map.put("gstCode", gstCode);
            map.put("gstPercentage", gstPercentage);
            String fromDate = validFrom.replace('/', '-');
            String toDate = validTo.replace('/', '-');
            map.put("validFrom", fromDate);
            map.put("validTo", toDate);
            System.out.println("mapgstCode" + map);
            updateRateMaster = (Integer) getSqlMapClientTemplate().update("operation.updateRateDetails", map);
            System.out.println("updateRateMaster" + updateRateMaster);
            //                }
            //                System.out.println("map 1= " + map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                     * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateRateMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateRateMaster", sqlException);
        }
        return updateRateMaster;
    }

    public int saveProductCategoryList(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int zoneId = 0;
        int saveCityMaster = 0;
        int saveProductCategoryList = 0;
        int user = userId;
        try {
            map.put("hsnName", operationTO.getHsnName());
            map.put("hsnCode", operationTO.getHsnCode());
            map.put("hsnDescription", operationTO.getHsnDescription());
            map.put("userId", userId);
            System.out.println("map 1= " + map);
            saveProductCategoryList = (Integer) getSqlMapClientTemplate().update("operation.saveProductCategoryList", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveProductCategoryList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveProductCategoryList", sqlException);
        }
        return saveProductCategoryList;
    }

    public ArrayList getProductCategoryDetails() {
        Map map = new HashMap();
        ArrayList getProductCategoryDetails = new ArrayList();
        //        map.put("gstCategoryId", operationTO.getGstCategoryId());

        //////System.out.println("map = " + map);
        try {
            //////System.out.println("this is City Master");
            getProductCategoryDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getProductCategoryDetails", map);
            //////System.out.println(" cityMasterList =" + cityMasterList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getProductCategoryDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getProductCategoryDetails List", sqlException);
        }
        return getProductCategoryDetails;
    }

    public int updateProductCategoryList(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int user = userId;

        map.put("gstProductId", operationTO.getGstProductId());
        map.put("hsnName", operationTO.getHsnName());
        map.put("hsnCode", operationTO.getHsnCode());
        map.put("hsnDescription", operationTO.getHsnDescription());
        map.put("userId", userId);

        try {
            System.out.println("map = " + map);
            status = (Integer) getSqlMapClientTemplate().update("operation.updateProductCategoryList", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateProductCategoryList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateProductCategoryList", sqlException);
        }

        return status;
    }

    public ArrayList getProductName() {
        Map map = new HashMap();
        ArrayList getProductName = new ArrayList();

        //////System.out.println("map = " + map);
        try {
            getProductName = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getProductName", map);
            //////System.out.println(" cityMasterList =" + cityMasterList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getProductName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getProductName List", sqlException);
        }
        return getProductName;
    }

    public ArrayList getAllCategoryName() {
        Map map = new HashMap();
        ArrayList getAllCategoryName = new ArrayList();
        //        map.put("gstCategoryId", operationTO.getGstCategoryId());

        //////System.out.println("map = " + map);
        try {
            //////System.out.println("this is City Master");
            getAllCategoryName = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getAllCategoryName", map);
            //////System.out.println(" cityMasterList =" + cityMasterList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAllCategoryName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getAllCategoryName List", sqlException);
        }

        return getAllCategoryName;
    }

    public int checkRateMaster(OperationTO operationTO) {
        Map map = new HashMap();
        int checkRateMaster = 0;
        map.put("gstType", operationTO.getGstType());
        map.put("state", operationTO.getState());
        map.put("categoryName", operationTO.getCategoryName());
        map.put("productName", operationTO.getProductName());
        try {
            System.out.println("map = " + map);
            checkRateMaster = (Integer) getSqlMapClientTemplate().queryForObject("operation.checkRateMaster", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkRateMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkRateMaster", sqlException);
        }

        return checkRateMaster;
    }

    public int saveRateDetails(OperationTO operationTO, String gstCode, String gstPercentage, String validFrom, String validTo, int userId, int gstRateId) {
        Map map = new HashMap();
        //                Map map = new HashMap();
        int saveRateMaster = 0;
        int user = userId;
        try {

            System.out.println("saveRateMaster" + saveRateMaster);
            if (gstRateId == 0 && gstRateId != 0) {
                map.put("GSTRateId", saveRateMaster);
            } else {
                map.put("GSTRateId", gstRateId);
            }
            map.put("userId", userId);
            //                    for(int i=0;i<gstCode.length;i++){
            map.put("gstCode", gstCode);
            map.put("gstPercentage", gstPercentage);
            map.put("validFrom", validFrom);
            map.put("validTo", validTo);
            saveRateMaster = (Integer) getSqlMapClientTemplate().update("operation.saveRateDetails", map);
            //                    }
            System.out.println("map 2= " + map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                         * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveRateMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveRateMaster", sqlException);
        }
        return saveRateMaster;

    }

    public int saveRateMaster(OperationTO operationTO) {
        Map map = new HashMap();
        int saveRateMaster = 0;
        map.put("categoryName", operationTO.getCategoryName());
        map.put("productName", operationTO.getProductName());
        map.put("state", operationTO.getState());
        map.put("gstType", operationTO.getGstType());

        System.out.println("map 1= " + map);
        saveRateMaster = (Integer) getSqlMapClientTemplate().insert("operation.saveRateMaster", map);
        return saveRateMaster;
    }

    public String checkFuelApprovalStatus() {
        String saveRateMaster = "";
        Map map = new HashMap();

        saveRateMaster = (String) getSqlMapClientTemplate().queryForObject("operation.checkPreviousFuelStatus", map);
        System.out.println("checkPreviousFuelStatus ->= " + saveRateMaster);
        return saveRateMaster;
    }

    public String getUserName(String userId) {
        Map map = new HashMap();
        String userName = "";

        String mess = "";
        try {

            map.put("userId", userId);
            System.out.println("map for user name:" + map);
            userName = (String) getSqlMapClientTemplate().queryForObject("operation.getUserName", map);

            System.out.println("userName" + userName);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkVehicleRegNo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkVehicleRegNo", sqlException);
        }

        return userName;
    }

    public ArrayList getBankList() {
        Map map = new HashMap();
        ArrayList bankList = null;
        try {
            bankList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getBankList", map);
            System.out.println("bankList.size() = " + bankList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerList List", sqlException);
        }
        return bankList;
    }

  public int updateInvoiceOustanding(BillingTO billingTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        int user = userId;
        map.put("invoiceId", billingTO.getInvoiceId());
        map.put("invoiceNo", billingTO.getInvoiceNo());
        map.put("customerId", billingTO.getCustomerId());
        map.put("invoiceAmount", billingTO.getGrandTotal());

        map.put("consignmentOrderId", "0");
        map.put("custType", "1");
        map.put("outstandingAmount", billingTO.getGrandTotal());
       
        map.put("creditNoteInvoiceId", "0");
        map.put("supplymentryInvoiceId", "0");
        map.put("paymentId", "0");

        map.put("userId", userId);
        System.out.println("updateInvoiceOustanding----" + map);
        try {
            int updateCnoteCount, updateOutstanding, updateCreditLimit, updateUsedLimit, updateOutstandingLog, updatepda = 0;
            String getCustomerType = (String) session.queryForObject("operation.getCustomerType", map);
            String availBlockedAmount = "";
            String availBlockedAmountTemp[] = null;
            map.put("custType", getCustomerType);
            
            if ("1".equalsIgnoreCase(getCustomerType)) {   //for credit Customers Only
                System.out.println("getCustomerType--if---"+getCustomerType);
                availBlockedAmount = (String) session.queryForObject("operation.getAvailandBlockedAmount", map);
                System.out.println("availBlockedAmount------" + availBlockedAmount);
                availBlockedAmountTemp = availBlockedAmount.split("~");
                map.put("fixedLimit", availBlockedAmountTemp[0]);
                
                String releaseAmount = (String) session.queryForObject("operation.getReleasedFreight", map);
                System.out.println("releaseAmount--"+releaseAmount);
                
                String getNonOutsatnadingCnoteAmt="";
                String temp = (String) session.queryForObject("operation.getNonOutsatnadingCnoteAmt", map);
                
                if("".equals(temp) || temp == null){
                    getNonOutsatnadingCnoteAmt = "0";
                }else{
                    getNonOutsatnadingCnoteAmt = temp;
                }
                System.out.println("getNonOutsatnadingCnoteAmt--"+getNonOutsatnadingCnoteAmt);
                
                double releaseAmt = Double.parseDouble(getNonOutsatnadingCnoteAmt) - Double.parseDouble(releaseAmount);
                System.out.println("releaseAmt--"+releaseAmt);
                
                double releaseBal = Double.parseDouble(availBlockedAmountTemp[1]) + releaseAmt;
                System.out.println("releaseBal--"+releaseBal);
                
                map.put("totalFreightAmount", "0");
                map.put("availAmount", releaseBal);
                map.put("blockedAmount", "0");
                map.put("transactionName", "Released"); 
                map.put("transactionType", "Credit");   // credit
                map.put("creditAmount", releaseAmount);
                map.put("debitAmount", "0.00");
                
                updateOutstandingLog = (Integer) session.update("operation.updateOutstandingLog", map);
                
                double usedLimit = Double.parseDouble(billingTO.getGrandTotal())+Double.parseDouble(availBlockedAmountTemp[3]);
                map.put("usedLimit", usedLimit);
                double blockedLimit = Double.parseDouble(availBlockedAmountTemp[2])-Double.parseDouble(billingTO.getGrandTotal());
                
                double avail = Double.parseDouble(availBlockedAmountTemp[1]);
                double othexp = Double.parseDouble(billingTO.getOtherExpense());
                
                double actAvail = avail - othexp;
                
                if(blockedLimit<0){
                //avail = Double.parseDouble(availBlockedAmountTemp[1])+blockedLimit;    
                map.put("availAmount", actAvail);
                map.put("blockedAmount", "0");
                }else{
                map.put("availAmount", actAvail);
                map.put("blockedAmount", blockedLimit);
                }
                
                updateCreditLimit = (Integer) session.update("operation.updateInvoiceCreditLimit", map);               
                
                map.put("totalFreightAmount", billingTO.getGrandTotal());                
                map.put("transactionName", "Invoice"); 
                map.put("transactionType", "Debit");   // debit
                map.put("debitAmount", billingTO.getGrandTotal());
                map.put("creditAmount", "0.00");
                updateOutstandingLog = (Integer) session.update("operation.updateOutstandingLog", map);
                
            } else if ("2".equalsIgnoreCase(getCustomerType)) {
                System.out.println("getCustomerType--else---"+getCustomerType);
                availBlockedAmount = (String) session.queryForObject("operation.getAvailandBlockedAmount", map);
                System.out.println("availBlockedAmount------" + availBlockedAmount);
                availBlockedAmountTemp = availBlockedAmount.split("~");
                map.put("fixedLimit", availBlockedAmountTemp[0]);
                
                String releaseAmount = (String) session.queryForObject("operation.getReleasedFreight", map);
                System.out.println("releaseAmount--"+releaseAmount);
                String getNonOutsatnadingCnoteAmt="";
                String temp = (String) session.queryForObject("operation.getNonOutsatnadingCnoteAmt", map);
                if("".equals(temp) || temp == null){
                    getNonOutsatnadingCnoteAmt = "0";
                }else{
                    getNonOutsatnadingCnoteAmt = temp;
                }
                System.out.println("getNonOutsatnadingCnoteAmt--"+getNonOutsatnadingCnoteAmt);
                double releaseAmt = Double.parseDouble(getNonOutsatnadingCnoteAmt) - Double.parseDouble(releaseAmount);
                System.out.println("releaseAmt--"+releaseAmt);
                
                double releaseBal = Double.parseDouble(availBlockedAmountTemp[1]) + releaseAmt;
                System.out.println("releaseBal--"+releaseBal);
                
//                double releaseBal = Double.parseDouble(availBlockedAmountTemp[1])+Double.parseDouble(releaseAmount);
                
                map.put("totalFreightAmount", "0");
                map.put("availAmount", releaseBal);
                map.put("blockedAmount", "0");
                map.put("transactionName", "Released"); 
                map.put("transactionType", "Credit");   // credit
                map.put("creditAmount", releaseAmount);
                map.put("debitAmount", "0.00");
                
                
                
                updateOutstandingLog = (Integer) session.update("operation.updateOutstandingLog", map);
                
                double usedLimit = Double.parseDouble(billingTO.getGrandTotal())+Double.parseDouble(availBlockedAmountTemp[3]);
                map.put("usedLimit", usedLimit);
                double blockedLimit = Double.parseDouble(availBlockedAmountTemp[2])-Double.parseDouble(billingTO.getGrandTotal());
                
                double avail = Double.parseDouble(availBlockedAmountTemp[1]);
                double othexp = Double.parseDouble(billingTO.getOtherExpense());
                
                double actAvail = avail - othexp;
                
                if(blockedLimit<0){
                //avail = Double.parseDouble(availBlockedAmountTemp[1])+blockedLimit;    
                map.put("availAmount", actAvail);
                map.put("blockedAmount", "0");
                }else{
                map.put("availAmount", actAvail);
                map.put("blockedAmount", blockedLimit);
                }
                
                updateCreditLimit = (Integer) session.update("operation.updateInvoiceCreditLimit", map);               
                
                map.put("totalFreightAmount", billingTO.getGrandTotal());                
                map.put("transactionName", "Invoice"); 
                map.put("transactionType", "Debit");   // debit
                map.put("debitAmount", billingTO.getGrandTotal());
                map.put("creditAmount", "0.00");
                updateOutstandingLog = (Integer) session.update("operation.updateOutstandingLog", map);
                
            }
            
            
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateCityMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateCityMaster", sqlException);
        }

        return status;
    }

     public int updateSuppInvoiceOustanding(BillingTO billingTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        int user = userId;
        map.put("invoiceId", billingTO.getInvoiceNo());
        map.put("customerId", billingTO.getCustomerId());
        map.put("invoiceAmount", billingTO.getGrandTotal());

        map.put("consignmentOrderId", "0");
        map.put("outstandingAmount", billingTO.getGrandTotal());
        map.put("totalFreightAmount", billingTO.getGrandTotal());
        map.put("creditNoteInvoiceId", "0");
        map.put("supplymentryInvoiceId", billingTO.getSuppInvoiceId());
        map.put("paymentId", "0");

        map.put("userId", userId);
        System.out.println("updateInvoiceOustanding----" + map);
        try {
            int updateCnoteCount, updateOutstanding, updateCreditLimit, updateUsedLimit, updateOutstandingLog = 0;
            String getCustomerType = (String) session.queryForObject("operation.getCustomerType", map);
            String availBlockedAmount = "";
            String availBlockedAmountTemp[] = null;
            map.put("custType", getCustomerType);
            map.put("transactionName", "Supp.Invoice");
            if ("1".equalsIgnoreCase(getCustomerType)) {   //for credit Customers Only
                
                availBlockedAmount = (String) session.queryForObject("operation.getAvailandBlockedAmount", map);
                System.out.println("availBlockedAmount------" + availBlockedAmount);
                availBlockedAmountTemp = availBlockedAmount.split("~");
                map.put("fixedLimit", availBlockedAmountTemp[0]);
                
                double usedLimit = Double.parseDouble(billingTO.getGrandTotal())+Double.parseDouble(availBlockedAmountTemp[3]);
                map.put("usedLimit", usedLimit);
                map.put("blockedAmount", availBlockedAmountTemp[2]);
                
                double availLimit = Double.parseDouble(availBlockedAmountTemp[1]) - Double.parseDouble(billingTO.getGrandTotal());
                
                map.put("availAmount", availLimit);
                
                updateCreditLimit = (Integer) session.update("operation.updateInvoiceCreditLimit", map);
                 
                map.put("transactionType", "Debit");   // debit
                map.put("debitAmount", billingTO.getGrandTotal());
                map.put("creditAmount", "0.00");
                updateOutstandingLog = (Integer) session.update("operation.updateOutstandingLog", map);
                
//                map.put("transactionName", "Released"); 
//                map.put("transactionType", "Credit");   // credit
//                map.put("creditAmount", billingTO.getGrandTotal());
//                map.put("debitAmount", "0.00");
//                updateOutstandingLog = (Integer) session.update("operation.updateOutstandingLog", map);
                
            } else if ("2".equalsIgnoreCase(getCustomerType)) {
                availBlockedAmount = (String) session.queryForObject("operation.getAvailandBlockedAmount", map);
                System.out.println("availBlockedAmount------" + availBlockedAmount);
                availBlockedAmountTemp = availBlockedAmount.split("~");
                map.put("fixedLimit", availBlockedAmountTemp[0]);
                
                double usedLimit = Double.parseDouble(billingTO.getGrandTotal())+Double.parseDouble(availBlockedAmountTemp[3]);
                map.put("usedLimit", usedLimit);
                map.put("blockedAmount", availBlockedAmountTemp[2]);
                
                double availLimit = Double.parseDouble(availBlockedAmountTemp[1]) - Double.parseDouble(billingTO.getGrandTotal());
                
                map.put("availAmount", availLimit);
                
                updateCreditLimit = (Integer) session.update("operation.updateInvoiceCreditLimit", map);
                
                map.put("transactionType", "Debit");   // debit
                map.put("debitAmount", billingTO.getGrandTotal());
                map.put("creditAmount", "0.00");
                updateOutstandingLog = (Integer) session.update("operation.updateOutstandingLog", map);
//                
//                map.put("transactionName", "Released"); 
//                map.put("transactionType", "Credit");   // credit
//                map.put("creditAmount", billingTO.getGrandTotal());
//                map.put("debitAmount", "0.00");
//                updateOutstandingLog = (Integer) session.update("operation.updateOutstandingLog", map);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateCityMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateCityMaster", sqlException);
        }

        return status;
    }

     public String existRouteContractData(int contractId, String loadTypeId, String ptpVehicleTypeId, String containerTypeId,
            String containerQty,
            String ptpPickupPointId, String ptpInterimPointId1, String ptpInterimPointId2, String ptpInterimPointId3,
            String ptpInterimPointId4, String ptpDropPointId) {
        Map map = new HashMap();
        String status = "";

        try {
            map.put("contractId", contractId);
            map.put("loadTypeId", loadTypeId);
            map.put("ptpVehicleTypeId", ptpVehicleTypeId);
            map.put("containerTypeId", containerTypeId);
            map.put("containerQty", containerQty);
            map.put("ptpPickupPointId", ptpPickupPointId);
            map.put("ptpInterimPointId1", ptpInterimPointId1);
            map.put("ptpInterimPointId2", ptpInterimPointId2);
            map.put("ptpInterimPointId3", ptpInterimPointId3);
            map.put("ptpInterimPointId4", ptpInterimPointId4);
            map.put("ptpDropPointId", ptpDropPointId);

            System.out.println("map value is = " + map);
            status = (String) getSqlMapClientTemplate().queryForObject("operation.existRouteContractData", map);

            if (status == null) {
                status = "";
}
            System.out.println("existRouteContractData" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("existRouteContractData Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "existRouteContractData", sqlException);
        }

        return status;
    }
    
public ArrayList getCreditCustomerList() {
        Map map = new HashMap();
        ArrayList customerList = null;
        try {
            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCreditCustomerList", map);
            System.out.println("getCustomerList.size()------- = " + customerList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerList List", sqlException);
        }
        return customerList;
    }        
   
   public ArrayList getCommodityDetails() {
        Map map = new HashMap();
        ArrayList getCommodityDetails = new ArrayList();

        try {
            //////System.out.println("this is standard charges");
            getCommodityDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCommodityDetails", map);
            System.out.println(" getCommodityDetails =" + getCommodityDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
                    
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCommodityDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCommodityDetails", sqlException);
        }
        return getCommodityDetails;
    }

   public String checkCommodityName(OperationTO operationTO) {
        Map map = new HashMap();
        String checkCommodityName = "";
        try {
            //////System.out.println("map = " + map);
            map.put("commodityName", operationTO.getCommodityName());
            System.out.println("operationTO.getCommodityName()----"+operationTO.getCommodityName());
            checkCommodityName = (String) getSqlMapClientTemplate().queryForObject("operation.checkCommodityName", map);
            System.out.println("checkCommodityName " + checkCommodityName);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkCommodityName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkCommodityName", sqlException);
        }

        return checkCommodityName;
    }
   
   public int saveCommodityMaster(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int saveProductCategory = 0;
        try {
            map.put("userId", userId);
            map.put("commodityId", operationTO.getCommodityId());
            map.put("commodityName", operationTO.getCommodityName());
            map.put("gstApplicable", operationTO.getGstApplicable());
            map.put("hsnCode", operationTO.getHsnCode());
            map.put("commodityDesc", operationTO.getCommodityDesc());
            map.put("activeInd", operationTO.getActiveInd());
            System.out.println("map----"+map);
            saveProductCategory = (Integer) getSqlMapClientTemplate().update("operation.saveCommodityMaster", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveProductCategory Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveProductCategory ", sqlException);
        }
        return saveProductCategory;
    }
   
   public int updateCommodityMaster(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", userId);
        map.put("commodityId", operationTO.getCommodityId());
        map.put("commodityName", operationTO.getCommodityName());
        map.put("gstApplicable", operationTO.getGstApplicable());
        map.put("hsnCode", operationTO.getHsnCode());
        map.put("commodityDesc", operationTO.getCommodityDesc());
        map.put("activeInd", operationTO.getActiveInd());
        System.out.println("map----"+map);

        try {

            status = (Integer) getSqlMapClientTemplate().update("operation.updateCommodityMaster", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateCommodityMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateCommodityMaster", sqlException);
        }

        return status;
    }
       public String checkContainerNo(String containerNo) {
        Map map = new HashMap();
        String status = "";

//        String mess = "";
        try {
//            containerName = containerName.replace(" ", "");
            //////System.out.println("trimmed regno=" + regNo);
            map.put("containerNo", containerNo);
            System.out.println("-" + containerNo + "-");
            status = (String) getSqlMapClientTemplate().queryForObject("operation.checkContainerExists", map);
            System.out.println("----------------------------------------------------------" + map);
            System.out.println("status-" + status);
//            if (status != null) {
//                mess = status;
//            }

            //////System.out.println("checkVehicleRegNo" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkContainerNo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkContainerNo", sqlException);
        }

        return status;
    }
   
    public int insertdetentionchargesNew(OperationTO operationTO, String detention, String dcmunit, String dcmunitTo, String chargeamt, String dcmremarks, String Teriftype) {
        Map map = new HashMap();
        int insertStatus = 0;

        map.put("detention", detention);
        map.put("dcmunit", dcmunit);
        map.put("dcmunitTo", dcmunitTo);
        map.put("chargeamt", chargeamt);
        map.put("dcmremarks", dcmremarks);
        map.put("Teriftype", Teriftype);
        map.put("userId", operationTO.getUserId());
        map.put("contractId", operationTO.getContractId());
        System.out.println("map = " + map);

        try {
            if (detention != null || !"".equals(detention)) {
                insertStatus = (Integer) getSqlMapClientTemplate().update("operation.insertdetentionchargesNew", map);
                System.out.println("insertStatus==" + insertStatus);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertdetentioncharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertdetentioncharges", sqlException);
        }
        return insertStatus;
    }

    public int updateDetentionChargesNew(OperationTO operationTO, String detentioncharge, String denid, String detFrom, String detTo, String TariftypeName) {
        Map map = new HashMap();
        int insertStatus = 0;

        map.put("detentioncharge", detentioncharge);
        map.put("detFrom", detFrom);
        map.put("detTo", detTo);
        map.put("Id", denid);
        map.put("TariftypeName", TariftypeName);
        map.put("userId", operationTO.getUserId());
        map.put("contractId", operationTO.getContractId());
        System.out.println("map = " + map);

        try {
            if (detentioncharge != null || !"".equals(detentioncharge)) {
                insertStatus = (Integer) getSqlMapClientTemplate().update("operation.updatedetentionchargesNew", map);
                System.out.println("insertStatus==" + insertStatus);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertdetentioncharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertdetentioncharges", sqlException);
        }
        return insertStatus;
    }


}
