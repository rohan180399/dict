
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>
 <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script>
        <title>BUS</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
            <script language="javascript" src="/throttle/js/validate.js"></script>
    </head>
    <script language="javascript">
        function submitPage(value)  {
            if(isEmpty(document.jobCardBill.invoiceNo.value)){
                alert("please enter invoice no");
                document.jobCardBill.invoiceNo.focus();
            }else if(isEmpty(document.jobCardBill.invoiceRemarks.value)){
                alert("please enter invoice invoiceRemarks");
                document.jobCardBill.invoiceRemarks.focus();
            }else if(confirm("Are you sure to submit")){
                document.jobCardBill.action = "extJobCardBillStore.do";
                document.jobCardBill.submit();
            }
        }
        function calcluateTotalAmt(){
            document.getElementById("totalValue").innerHTML =
                    parseFloat(document.jobCardBill.sparesAmt.value) + parseFloat(document.jobCardBill.consumableAmt.value) + parseFloat(document.jobCardBill.laborAmt.value) +
                    parseFloat(document.jobCardBill.othersAmt.value) + parseFloat(document.jobCardBill.vatAmt.value) + parseFloat(document.jobCardBill.serviceTaxAmt.value);
            document.getElementById("totalValue").innerHTML= parseFloat(document.getElementById("totalValue").innerHTML).toFixed(2);
            document.jobCardBill.totalAmt.value = parseFloat(document.getElementById("totalValue").innerHTML).toFixed(2);
        }

    </script>

    <body>

        <form method="post" name="jobCardBill" action= "jobCardBillStore.do">
            <!-- copy there from end -->
        <div id="print" >
            <div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
                <div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
                    <!-- pointer table -->
                    <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                        <tr>
                            <td >
                                <%@ include file="/content/common/path.jsp" %>
                    </td></tr></table>
                    <!-- pointer table -->

                </div>
            </div>

            <!-- message table -->
            <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
                <tr>
                    <td >
                        <%@ include file="/content/common/message.jsp" %>
                    </td>
                </tr>
            </table>
            <!-- message table -->
<!-- copy there  end -->
                      <%
            int c = 0;
                            %>
            <c:if test = "${jcList != null}" >
                <c:forEach items="${jcList}" var="list">

                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="90%" class="border">
                        <tbody><tr>
                                <td class="contenthead" colspan="6" height="30"><div class="contenthead">Record Vendor Bill Details</div></td>
                            </tr>

                            <tr>

                                <td class="text1" height="30"><b>JobCardNo</b></td>
                                <td class="text1" height="30"><c:out value="${list.jcMYFormatNo}"/>
                                <input type="hidden" name="jcNo" value='<c:out value="${list.jcMYFormatNo}"/>'/>
                                <input type="hidden" name="jcCreatedDate" value='<c:out value="${list.createdDate}"/>'/>
                                <input type="hidden" name="jcModelName" value='<c:out value="${list.modelName}"/>'/>
                                <input type="hidden" name="jcMfrName" value='<c:out value="${list.mfrName}"/>'/>
                                <input type="hidden" name="jcVehicleNo" value='<c:out value="${list.vehicleNo}"/>'/>
                                <input type="hidden" name="jcCustomerName" value='<c:out value="${list.customerName}"/>'/>

                                </td>

                                <td class="text1" height="30"><b>Vehicle No</b></td>
                                <td class="text1" height="30"><c:out value="${list.vehicleNo}"/></td>
                                <td class="text1" height="30"><b>Customer</b></td>
                                <td class="text1" height="30"><c:out value="${list.customerName}"/></td>
                            </tr>

                    </tbody></table>
                    <input type="hidden" name="jobCardId" value='<c:out value="${list.jobCardId}"/>'/>


                </c:forEach >

            </c:if>
    <br>
        <table width="90%" cellpadding="0" cellspacing="0" border="0" align="center" class="table5">
            <tr>
            <td class="bottom" width="8%" align="left"><img src="/throttle/images/left_status.jpg" alt=""  /></td>

            <td  width="12%" align="right"><h2>Total SAR.</h2></td>
            <td  width="10%" align="left"><h2><div id="totalValue">0.00</div></h2></td>

            </tr>

        </table>

        <br>

           <table align="center" border="0" cellpadding="0" cellspacing="0" width="90%" class="border">
		<tbody><tr>
		<td class="text2" align="center" colspan="8" height="30"><strong>Vendor Bill Particulars</strong></td>
		</tr>
		<tr height="30">
                    <td class="text1" align="left"><b>Invoice No</b></td>
                    <td class="text1" align="left"><input type="text" name="invoiceNo" value="" class="form-control" /> </td>
                    <td class="text1" align="left"><b>Invoice Remarks</b></td>
                    <td class="text1" align="left"><textarea name="invoiceRemarks" rowspan="3" colspan="6" class="form-control"></textarea></td>
                    <td class="text1" align="left"><b>Invoice Date</b></td>
                    <td class="text1" align="left">

                        <input type="text" name="invoiceDate" id="invoiceDate" value="<%=session.getAttribute("currentDate")%>" >
                                    <img   src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.jobCardBill.invoiceDate,'dd-mm-yyyy',this);"  style="cursor:default; "/>
                    </td>
		</tr>
		<tr height="30">
                    <td class="text2" align="left"><b>Spares Value</b></td>
                    <td class="text2" align="left"><input type="text" name="sparesAmt" value="0" class="form-control" onChange="calcluateTotalAmt();" /> </td>
                    <td class="text2" align="left"><b>Spares Remarks / Details</b></td>
                    <td class="text2" align="left"><textarea name="sparesRemarks" rowspan="3" colspan="6" class="form-control" >-</textarea></td>
                    <td class="text2" align="left">&nbsp;</td>
                    <td class="text2" align="left">&nbsp;</td>
		</tr>
		<tr height="30">
                    <td class="text1" align="left"><b>Consumables Value</b></td>
                    <td class="text1" align="left"><input type="text" name="consumableAmt" value="0" onChange="calcluateTotalAmt();" class="form-control" /> </td>
                    <td class="text1" align="left"><b>Consumables Remarks / Details</b></td>
                    <td class="text1" align="left"><textarea name="consumableRemarks" rowspan="3" colspan="6" class="form-control" >-</textarea></td>
                    <td class="text1" align="left">&nbsp;</td>
                    <td class="text1" align="left">&nbsp;</td>
		</tr>
		<tr height="30">
                    <td class="text2" align="left"><b>Labour Value</b></td>
                    <td class="text2" align="left"><input type="text" name="laborAmt" value="0"  onChange="calcluateTotalAmt();"  class="form-control" /> </td>
                    <td class="text2" align="left"><b>Labour Remarks / Details</b></td>
                    <td class="text2" align="left"><textarea name="laborRemarks" rowspan="3" colspan="6" class="form-control" >-</textarea></td>
                    <td class="text2" align="left">&nbsp;</td>
                    <td class="text2" align="left">&nbsp;</td>
		</tr>
		<tr height="30">
                    <td class="text1" align="left"><b>Others Value</b></td>
                    <td class="text1" align="left"><input type="text" name="othersAmt" value="0"  onChange="calcluateTotalAmt();" class="form-control" /> </td>
                    <td class="text1" align="left"><b>Others Remarks / Details</b></td>
                    <td class="text1" align="left"><textarea name="othersRemarks" rowspan="3" colspan="6" class="form-control" >-</textarea></td>
                    <td class="text1" align="left">&nbsp;</td>
                    <td class="text1" align="left">&nbsp;</td>
		</tr>
		<tr height="30">
                    <td class="text1" align="left"><b>Vat %age</b></td>
                    <td class="text1" align="left"><input type="text" name="vatPercent" value="0"  class="form-control" /> </td>
                    <td class="text1" align="left"><b>Vat Value</b></td>
                    <td class="text1" align="left"><input type="text" name="vatAmt" value="0"  onChange="calcluateTotalAmt();"  class="form-control" /> </td>
                    <td class="text1" align="left">&nbsp;</td>
                    <td class="text1" align="left">&nbsp;</td>
		</tr>
                <tr>
                    <td class="text2" align="left"><b>Service Tax %age</b></td>
                    <td class="text2" align="left"><input type="text" name="serviceTaxPercent" value="0" class="form-control" /> </td>
                    <td class="text2" align="left"><b>Service Tax Value</b></td>
                    <td class="text2" align="left"><input type="text" name="serviceTaxAmt" value="0"  onChange="calcluateTotalAmt();" class="form-control" /> </td>
                    <td class="text2" align="left">&nbsp;</td>
                    <td class="text2" align="left">&nbsp;</td>
		</tr>

                </table>


             <input type="hidden" name="totalAmt" value="0" />

            <br>

            </div>

            <center>
                <br>

                    <div id="saveBill" style="visibility:visible;" align="center" >

                        <input class="button" type="button" value="Save Bill" onClick="submitPage();"> </div>


            </center>
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
