
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">

        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {

                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function() {
                //	alert("cv");
                $( ".datepicker" ).datepicker({

                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });
        </script>
    </head>

    <script>
        function newWindow(){
            date='<%=session.getAttribute("currentDate")%>';
            window.open('/throttle/checkWorkOrder.do?scheduleDate='+date, 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
  
        }
    
        function submitPage()
        {
            var chek=validation();
            if(chek==1){
                if(document.getElementById("serviceVendor").value == 0){
                alert("Please select service vendor");
                document.getElementById("serviceVendor").focus();
                }else{
                document.workOrder.action = '/throttle/scheduleWorkOrder.do';
                document.workOrder.submit();
                }
            }
        }
        function validation(){
            if(textValidation(document.workOrder.scheduleDate,'scheduleDate')){
                return 0;
            }
            if(textValidation(document.workOrder.deliveryDate,'deliveryDate')){
                return 0;
            }
            return 1;
        }
    </script>



    <body>
        <form name="workOrder" method="post">
            <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                <tr>
                    <td >
                        <%@ include file="/content/common/path.jsp" %>
                    </td></tr></table>
            <!-- pointer table -->

            <!-- message table -->
            <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
                <tr>
                    <td >
                        <%@ include file="/content/common/message.jsp"%>
                    </td>
                </tr>
            </table>

            <c:if test = "${vehicleDetails != null}" >
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="750" id="bg" class="border">

                    <tr>
                        <td class="contenthead" colspan="4" align="center" height="30"><div class="contenthead">Vehicle Details</div></td>
                    </tr>
                    <c:forEach items="${vehicleDetails}" var="fservice">

                        <tr>
                            <td class="text2" height="30">Vehicle Number</td>
                            <td class="text2" height="30"><c:out value="${fservice.regno}"/></td>
                            <td class="text2" height="30">Vehicle Type:</td>
                            <td class="text2" height="30"><c:out value="${fservice.vehicleTypeName}"/></td>
                        </tr>

                        <tr>
                            <td class="text1" height="30">MFG:</td>
                            <td class="text1" height="30"><c:out value="${fservice.mfrName}"/></td>
                            <td class="text1" height="30">Use Type:</td>
                            <td class="text1" height="30"><c:out value="${fservice.usageName}"/></td>
                        </tr>

                        <tr>
                            <td class="text2" height="30">Model:</td>
                            <td class="text2" height="30"><c:out value="${fservice.modelName}"/></td>
                            <td class="text2" height="30">Engine No:</td>
                            <td class="text2" height="30"><c:out value="${fservice.engineNo}"/></td>
                        </tr>

                        <tr>
                            <td class="text1" height="30">Chasis No:</td>
                            <td class="text1" height="30"><c:out value="${fservice.chassNo}"/></td>
                            <td class="text1" colspan="2" height="30">&nbsp;</td>
                        </tr>
                    </c:forEach>
                </table
                ></c:if>
                <br>
                <div  align="right">  </div>
                <br>
            <c:if test = "${workOrderDetails != null}" >
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="750" id="bg" class="border">
                    <tr>
                        <td colspan="4" class="contenthead" height="30"><div class="contenthead">Work Order</div></td>
                    </tr>
                    <c:forEach items="${workOrderDetails}" var="service">
                        <tr>
                            <td class="text2" height="30">Work Order No:</td>
                            <td class="text2" height="30"><%=request.getAttribute("workOrderId")%><input name="workOrderId" type="hidden" value='<%=request.getAttribute("workOrderId")%>'></td>
                            <td class="text2" height="30">Date of Issue</td>
                            <td class="text2" height="30"><c:out value="${service.dateOfIssue}"/></td>
                        </tr>

                        <tr>
                            <td class="text1" height="30">Place of Issue</td>
                            <td class="text1" height="30"><c:out value="${service.compName}"/></td>
                            <td class="text1" height="30">Required Date/Time of delivery</td>
                            <td class="text1" height="30"><c:out value="${service.reqDate}"/></td>
                        </tr>
                        <tr>
                            <td class="text2" height="30">Km Reading Before leaving</td>
                            <td class="text2" height="30"><c:out value="${service.kmReading}"/></td>
                            <td class="text2" height="30">Service Location</td>
                            <td class="text2" height="30"><%=session.getAttribute("companyName")%></td>
                        </tr>

                        <tr>
                            <td class="text2" height="30">Schedule Date </td>
                            <td class="text2" height="20" width="150" > <input name="scheduleDate" readonly type="text" class="datepicker" value=""></td>
                            <td class="text2" height="30">Delivery Date </td>
                            <td class="text2" height="20" width="150" > <input name="deliveryDate" readonly type="text" class="datepicker" value=""></td>
                        </tr>
                        <tr>
                            <td class="text1" height="30">Driver </td>
                            <td class="text1" height="30"><c:out value="${service.driverName}"/></td>
                            <td class="text1" height="30">Current Location</td>
                            <td class="text1" height="30"><c:out value="${service.location}"/></td>
                            
                        </tr>
                        <tr>
                           <td class="text1" height="30">Service Vendor</td>
                            <td class="text1" height="30"><select class="form-control" style="width:125px;" name="serviceVendor" id="serviceVendor">
                                    <option selected   value=0>NA</option>
                                    <c:if test = "${serviceVendors != null}" >
                                        <c:forEach items="${serviceVendors}" var="sv">
                                            <option  value='<c:out value="${sv.vendorId}"/>'>
                                                <c:out value="${sv.vendorName}" />
                                            </c:forEach >
                                        </c:if>
                                </select>  </td>
                            <td class="text1" height="30" colspan="2">&nbsp;</td>

                        </tr>
                    </c:forEach>
                </table
                ></c:if>
                <br>
            <%

                        int index = 0;
            %>
            <c:if test = "${workOrderProblemDetails != null}" >
                <table align="center"  border="0" cellpadding="0" cellspacing="0" width="750" class="border" >
                    <tr>
                        <td colspan="8" align="center" class="text2" height="30"><strong>Complaints</strong></td>
                    </tr>

                    <tr>
                        <td  height="30" class="contentsub"><div class="contentsub">S.No</div></td>
                        <td  height="30" class="contentsub"><div class="contentsub">Section</div></td>
                        <td  height="30" class="contentsub"><div class="contentsub">Fault</div></td>
                        <td  height="30" class="contentsub"><div class="contentsub">Description</div></td>
                        <td  height="30" class="contentsub"><div class="contentsub">Symptoms</div></td>
                        <td  height="30" class="contentsub"><div class="contentsub">Severity</div></td>
                        <td height="30" class="contentsub"><div class="contentsub">&nbsp;</div></td>
                        <td height="30" class="contentsub"><div class="contentsub">&nbsp;</div></td>
                    </tr>
                    <c:forEach items="${workOrderProblemDetails}" var="prob">
                        <%
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text1";
                                    } else {
                                        classText = "text2";
                                    }
                        %>
                        <tr>
                            <td height="30" class="<%=classText%>"><%=index + 1%></td>
                            <td height="30" class="<%=classText%>"><c:out value="${prob.secName}"/></td>
                            <td height="30" class="<%=classText%>"><c:out value="${prob.probName}"/></td>
                            <td height="30" class="<%=classText%>"><c:out value="${prob.desc}"/></td>
                            <td height="30" class="<%=classText%>"><c:out value="${prob.symptoms}"/></td>
                            <c:if test = "${prob.severity ==1}" >
                                <td height="30" class="<%=classText%>">Low</td>
                            </c:if>
                            <c:if test = "${prob.severity ==2}" >
                                <td height="30" class="<%=classText%>">Medium</td>
                            </c:if>
                            <c:if test = "${prob.severity ==3}" >
                                <td height="30" class="<%=classText%>">High</td>
                            </c:if>
                        </tr>
                        <%
                                    index++;
                        %>
                    </c:forEach>

                </table>
            </c:if>
            <br>
            <center>
                <input type="button" value="Schedule" name="schedule" class="button" onclick="submitPage();">
                <input type="hidden" value="viewWorkOrder" name="reqfor">
            </center>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
        <br>
    </body>
</html>
