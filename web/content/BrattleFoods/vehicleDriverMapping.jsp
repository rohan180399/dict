<%--
    Document   : standardChargesMaster
    Created on : Oct 29, 2013, 11:32:08 AM
    Author     : srinivasan
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
    <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>
<script>

    var httpRequest;
    function checkDriverMapped(vehicleId) {
        if (vehicleId != '') {
            var url = '/throttle/checkVehicleDriverMapping.do?vehicleId=' + vehicleId;
            if (window.ActiveXObject) {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest) {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("GET", url, true);
            httpRequest.onreadystatechange = function() {
                processRequest();
            };
            httpRequest.send(null);
        }
    }
    function processRequest() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var val = httpRequest.responseText.valueOf();
                //alert(val.trim());
                if (val.trim() !== "") {
                    var temp = val.split("~");
                    document.getElementById("mappingId").value = temp[0];
                    document.getElementById("primaryDriverId").value = temp[1];
                    document.getElementById("primaryDriver").value = temp[2];
                    document.getElementById("secondaryDriverIdOne").value = temp[3];
                    document.getElementById("secondaryDriverOne").value = temp[4];
                    document.getElementById("secondaryDriverIdTwo").value = temp[5];
                    document.getElementById("secondaryDriverTwo").value = temp[6];
                    if(temp[7] == '0'){
                            $("#tripStatus").text("");
                            $("#tripStatusShow").hide();
                        }else if(temp[7] != '0'){
                            //alert(temp[7]);
                            $("#tripStatusShow").show();
                            if(temp[8] == 8){
                            //alert(temp[8]);
                            $("#tripStatus").text('Now Vehicle is assigned to a trip (pre-start status) and trip sheet no is '+temp[7]);
                            }else if(temp[8] == 9){
                            $("#tripStatus").text('Now Vehicle is assigned to a trip (start status) and trip sheet no is '+temp[7]);
                            }else if(temp[8] == 10){
                            $("#tripStatus").text('Now Vehicle is assigned to a trip (in progress status) and trip sheet no is '+temp[7]);
                            }
                        }
                } else {
                    document.getElementById("mappingId").value = 0;
                    document.getElementById("primaryDriverId").value = '';
                    document.getElementById("primaryDriver").value = '';
                    document.getElementById("secondaryDriverIdOne").value = '';
                    document.getElementById("secondaryDriverOne").value = '';
                    document.getElementById("secondaryDriverIdTwo").value = '';
                    document.getElementById("secondaryDriverTwo").value = '';
                    $("#tripStatus").text("");
                }
            } else {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }


    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#vehicleNo').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getVehicleNo.do",
                    dataType: "json",
                    data: {
                        vehicleNo: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if(items == ''){
                        alert("Invalid Vehicle No");
                        $('#vehicleNo').val('');
                        $('#vehicleId').val('');    
                        $('#vehicleNo').fous();
                        }else{
                        response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var id = ui.item.Id;
                $('#vehicleNo').val(value);
                $('#vehicleId').val(id);
                 $('#primaryDriver').focus();
                checkDriverMapped(id);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };



        // Use the .autocomplete() method to compile the list based on input from user
        $('#primaryDriver').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getDriverName.do",
                    dataType: "json",
                    data: {
                        driverName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        var primaryDriver = $('#primaryDriver').val();
                        if(items == '' && primaryDriver != ''){
                        alert("Invalid Primary Driver Name");
                        $('#primaryDriver').val('');
                        $('#primaryDriverId').val(''); 
                        $('#primaryDriver').focus();
                        }else{
                        }
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var id = ui.item.Id;
                $('#primaryDriver').val(value);
                $('#primaryDriverId').val(id);
                 $('#secondaryDriverOne').focus();
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };



        // Use the .autocomplete() method to compile the list based on input from user
        $('#secondaryDriverOne').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getDriverName.do",
                    dataType: "json",
                    data: {
                        driverName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if(items == ''){
                        alert("Invalid Secondary Driver Name");
                        $('#secondaryDriverOne').val('');
                        $('#secondaryDriverIdOne').val(''); 
                        $('#secondaryDriverOne').focus();
                        }else{
                        response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var id = ui.item.Id;
                $('#secondaryDriverOne').val(value);
                $('#secondaryDriverIdOne').val(id);
                $('#secondaryDriverTwo').focus();
                return false;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };



        // Use the .autocomplete() method to compile the list based on input from user
        $('#secondaryDriverTwo').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getDriverName.do",
                    dataType: "json",
                    data: {
                        driverName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if(items == ''){
                        alert("Invalid Tertiary Driver Name");
                        $('#secondaryDriverTwo').val('');
                        $('#secondaryDriverIdTwo').val(''); 
                        $('#secondaryDriverTwo').focus();
                        }else{
                        response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var id = ui.item.Id;
                $('#secondaryDriverTwo').val(value);
                $('#secondaryDriverIdTwo').val(id);
                return false;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };


    });




    //Update Vehicle Planning
    function setValues(mappingId, vehicleId, vehicleNo, primaryDriverId, primaryDriverName, secondaryDriverIdOne, secondaryDriverNameOne, secondaryDriverIdTwo, secondaryDriverNameTwo, status,sno) {
        var count = parseInt(document.getElementById("count").value);
        //document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("mappingId").value = mappingId;
        document.getElementById("vehicleId").value = vehicleId;
        document.getElementById("vehicleNo").value = vehicleNo;
        document.getElementById("vehicleNo").readOnly = true;
        document.getElementById("primaryDriverId").value = primaryDriverId;
        document.getElementById("primaryDriver").value = primaryDriverName;
        document.getElementById("secondaryDriverIdOne").value = secondaryDriverIdOne;
        document.getElementById("secondaryDriverOne").value = secondaryDriverNameOne;
        document.getElementById("secondaryDriverIdTwo").value = secondaryDriverIdTwo;
        document.getElementById("secondaryDriverTwo").value = secondaryDriverNameTwo;
//        document.getElementById("status").value = status;
    }

    //savefunction
    function submitPage() {
        if (document.getElementById("vehicleNo").value == '' || document.getElementById("vehicleId").value == '') {
            alert("Please Select Valid Vehicle No");
            document.getElementById("vehicleNo").focus();
         }else if($("#tripStatus").text() != "") {
            alert("This Vehicle Should not be in  Trip in progress  ");
        } else if (document.getElementById("primaryDriver").value != '' && document.getElementById("primaryDriverId").value == '') {
            alert("Please Select Valid Primary Driver Name");
            document.getElementById("primaryDriver").focus();
        } else if (document.getElementById("secondaryDriverOne").value != '' && document.getElementById("secondaryDriverIdOne").value == '') {
            alert("Please Select Valid Secondary Driver Name");
            document.getElementById("secondaryDriverOne").focus();
        } else if (document.getElementById("secondaryDriverTwo").value != '' && document.getElementById("secondaryDriverIdTwo").value == '') {
            alert("Please Select Valid Tertiary Driver Name");
            document.getElementById("secondaryDriverTwo").focus();
        } else {
            $("#save").hide();
            document.vehicleDriverPlanning.action = '/throttle/saveDriverVehicle.do';
            document.vehicleDriverPlanning.submit();
        }
    }
    
    
    
    function viewSearchViewEmp(primaryDriverName) {
            
            window.open('/throttle/searchViewEmp.do?staffName='+primaryDriverName, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
     function viewSearchViewEmp1(secondaryDriverNameOne) {
            
            window.open('/throttle/searchViewEmp.do?staffName='+secondaryDriverNameOne, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
     function viewSearchViewEmp2(secondaryDriverNameTwo) {
            
            window.open('/throttle/searchViewEmp.do?staffName='+secondaryDriverNameTwo, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
</script>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <div class="pageheader">
      <h2><i class="fa fa-edit"></i> <h2><spring:message code="operations.label.VehicleDriverMapping"  text="default text"/></h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="operations.label.DailyOperation"  text="default text"/></a></li>
          <li class="active"><spring:message code="operations.label.VehicleDriverMapping"  text="default text"/></li>
        </ol>
      </div>
      </div>
 <div class="contentpanel">
<div class="panel panel-default">
 <div class="panel-body">
    <body onload="document.vehicleDriverPlanning.vehicleNo.focus()">
        <form name="vehicleDriverPlanning"  method="post" >
           
            
            <%@ include file="/content/common/message.jsp" %>
            
            

           <table class="table table-info mb30 table-hover" >
                <input type="hidden" name="stChargeId" id="stChargeId" value=""  />
                <thead>
                <tr>
                    <th  colspan="4" ><spring:message code="operations.label.VehicleDriverMapping"  text="default text"/></th>
                </tr>
                </thead>
                <tr>
                    <td  colspan="4" align="center" id="tripStatusShow" style="display: none;"><label id="tripStatus" style="color: red"></label></td>
                </tr>
                <tr>
                    <td >&nbsp;&nbsp;<font color="red">*</font><spring:message code="operations.label.VehicleNo"  text="default text"/></td>
                    <td ><input type="hidden" name="mappingId" id="mappingId" class="textbox"  ><input type="hidden" name="vehicleId" id="vehicleId" class="textbox"  ><input type="text" name="vehicleNo" id="vehicleNo" style="width:260px;height:40px;"  class="form-control" ></td>
                    <td >&nbsp;&nbsp;<font color="red">*</font><spring:message code="operations.label.PrimaryDriver"  text="default text"/></td>
                    <td ><input type="hidden" name="primaryDriverId" id="primaryDriverId" class="textbox"  ><input type="text" name="primaryDriver" id="primaryDriver" style="width:260px;height:40px;"  class="form-control" onchange="setPrimaryDriverEmpty()" onKeyPress="return onKeyPressBlockNumbers(event);"></td>
                </tr>
                <tr>
                    <td >&nbsp;&nbsp;<font color="red"></font><spring:message code="operations.label.SecondaryDriver"  text="default text"/></td>
                    <td ><input type="hidden" name="secondaryDriverIdOne" id="secondaryDriverIdOne" class="textbox"  ><input type="text" name="secondaryDriverOne" id="secondaryDriverOne" style="width:260px;height:40px;"  class="form-control" onchange="setSecondaryDriverEmpty()" onKeyPress="return onKeyPressBlockNumbers(event);"></td>
                    <td >&nbsp;&nbsp;<font color="red"></font><spring:message code="operations.label.TertiaryDriver"  text="default text"/></td>
                    <td ><input type="hidden" name="secondaryDriverIdTwo" id="secondaryDriverIdTwo" class="textbox"  ><input type="text" name="secondaryDriverTwo" id="secondaryDriverTwo" style="width:260px;height:40px;"  class="form-control" onchange="setTertiaryDriverEmpty()" onKeyPress="return onKeyPressBlockNumbers(event);"></td>
                </tr>
                <tr>
                    <td  colspan="4" align="center"><input type="button" class="btn btn-success" value="<spring:message code="operations.label.SAVE"  text="default text"/>" id="save" onClick="submitPage();"   /></td>
                </tr>
                
                <script>
                    function setPrimaryDriverEmpty(){
                       var primaryDriver = $("#primaryDriver").val(); 
                       var mappingId = $("#mappingId").val(); 
                       if(primaryDriver == ''){
                           $("#primaryDriverId").val('');
                       }
                    }
                    function setSecondaryDriverEmpty(){
                       var secondaryDriverOne = $("#secondaryDriverOne").val(); 
                       var mappingId = $("#mappingId").val(); 
                       if(secondaryDriverOne == ''){
                           $("#secondaryDriverIdOne").val('');
                       }
                    }
                    function setTertiaryDriverEmpty(){
                       var secondaryDriverTwo = $("#secondaryDriverTwo").val(); 
                       var mappingId = $("#mappingId").val(); 
                       if(secondaryDriverTwo == ''){
                           $("#secondaryDriverIdTwo").val('');
                       }
                    }
                </script>
            </table>
            
            
           <table class="table table-info mb30 table-hover" id="table">
                <thead>
                    <tr height="30">
                        <th><spring:message code="operations.label.SNo"  text="default text"/></th>
                        <th><spring:message code="operations.label.VehicleNo"  text="default text"/> </th>
                        <th><spring:message code="operations.label.PrimaryDriver"  text="default text"/></th>
                        <th><spring:message code="operations.label.SecondaryDriver"  text="default text"/></th>
                        <th><spring:message code="operations.label.TertiaryDriver"  text="default text"/></th>
                        <th><spring:message code="operations.label.Select"  text="default text"/></th>
                    </tr>
                </thead>
                <tbody>
                    <% int sno = 0;%>
                    <c:if test = "${vehicleDriverMapping != null}">
                        <c:forEach items="${vehicleDriverMapping}" var="vehicleDriver">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td   align="left"> <%= sno + 1%> </td>
                                <td   align="left"><input type="hidden" name="vehicleIds" id="vehicleIds" value="<c:out value="${vehicleDriver.vehicleId}" />"/> <c:out value="${vehicleDriver.regNo}" /></td>
                                 <td   align="left"><a href="" onclick="viewSearchViewEmp('<c:out value="${vehicleDriver.primaryDriverName}"/>')"><c:out value="${vehicleDriver.primaryDriverName}" /></a></td>
                                <td   align="left"><a href="" onclick="viewSearchViewEmp1('<c:out value="${vehicleDriver.secondaryDriverNameOne}"/>')"><c:out value="${vehicleDriver.secondaryDriverNameOne}"/></a></td>
                                <td   align="left"><a href="" onclick="viewSearchViewEmp2('<c:out value="${vehicleDriver.secondaryDriverNameTwo}"/>')"><c:out value="${vehicleDriver.secondaryDriverNameTwo}"/></a></td>
                                <td > <input type="checkbox" id="edit<%=sno%>" onclick="setValues('<c:out value="${vehicleDriver.mappingId}" />', '<c:out value="${vehicleDriver.vehicleId}" />', '<c:out value="${vehicleDriver.regNo}" />', '<c:out value="${vehicleDriver.primaryDriverId}" />', '<c:out value="${vehicleDriver.primaryDriverName}" />', '<c:out value="${vehicleDriver.secondaryDriverIdOne}" />', '<c:out value="${vehicleDriver.secondaryDriverNameOne}" />', '<c:out value="${vehicleDriver.secondaryDriverIdTwo}" />', '<c:out value="${vehicleDriver.secondaryDriverNameTwo}" />', '<c:out value="${vehicleDriver.status}" />','<%=sno%>');" /></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </c:if>
            </table>
            <input type="hidden" name="count" id="count" value="<%=sno%>" />
            
            
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");</script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span><spring:message code="operations.label.EntriesPerPage"  text="default text"/></span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text"><spring:message code="operations.label.DisplayingPage"  text="default text"/> <span id="currentpage"></span> <spring:message code="operations.label.of"  text="default text"/> <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
