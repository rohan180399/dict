<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE>Parveen Auto Care</TITLE>
<META NAME="Description" CONTENT="">
<link href="css/admin1.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
<!--

function resize_iframe()
{
	//document.getElementById("info").innerHTML='iframe offsetTop: <b> '+document.getElementById("glu").offsetTop+"</b><br>body.offsetHeight:<b>"+document.body.offsetHeight+"</b>";//display some information on the screen

	var height=window.innerWidth;//Firefox
	if (document.body.clientHeight)
	{
		height=document.body.clientHeight;//IE
	}

	document.getElementById("glu").style.height=parseInt(height-document.getElementById("glu").offsetTop -190)+"px";//resize the iframe according to the size of the window
	document.getElementById("glu").height=document.body.offsetHeight-document.getElementById("glu").offsetTop- 0;

}

/*
	//Here is another way to define the function (this function reloads the page whenever the user resizes the page)
	window.onresize=
	function (e) 
	{
		location.reload();
	};
*/

window.onresize=resize_iframe; //instead of using this you can use: <BODY onresize="resize_iframe()">
//-->
</script>
</HEAD>

<BODY style="overflow:hidden; " scroll="no">
<!- Header-->
<table width="100%" height="100" cellpadding="0" cellspacing="0" align="center" bgcolor="#FFFFFF" style="border:1px solid #86DDFE; ">
<tr>
<td width="200" rowspan="2"><img src="images/logo.gif" alt="logo"></td>
<td width="500" style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:18px; font-weight:bold; padding-left:5px; vertical-align:middle; color:#741F51; ">Parveen Auto Care</td>
<td width="57" height="70" align="center" style="background-position:center; vertical-align:bottom; "><img src="images/home_03.gif"></td>
<td width="57" height="70" align="center" style="background-position:center; vertical-align:bottom; "><img src="images/logout_05.gif"></td>
<td width="57" height="70" align="center" style="background-position:center; vertical-align:bottom; "><img src="images/logout_05.gif"></td>
<td width="59" height="70" align="center" style="background-position:center; vertical-align:bottom; "><img src="images/help_07.gif"></td>
</tr>
<tr>

  <td width="500" style="font-family:Arial; font-size:12px; padding-left:20px; vertical-align:bottom; color:#000000; ">Welcome <span> <%= session.getAttribute("UserName")%>,&nbsp;<%= session.getAttribute("DesigName")%>,&nbsp;<%= session.getAttribute("companyName")%></span></td>
  <td class="home"><a href="/BharathUniv/userServlet?reqfor=home">Home</a></td>
  <td class="home"><a href="/BharathUniv/userServlet?reqfor=logout" target="_top">Logout</a></td>
  <td class="home"><a href="">Reset Password</a></td>
  <td class="home">Help</td>
</tr>
</table>
<!- Header -->

<table width="100%" height="30" cellpadding="0" cellspacing="0" align="center" bgcolor="#FFFFFF" id="menu" style="border:1px solid #86DDFE; border-top-style:none; ">
<tr>
<td align="center"   ><%@ include file="menu.jsp" %></td>
</tr>
</table>


<!-- content table -->
<table width="100%" cellpadding="0" cellspacing="0" align="center" bgcolor="#CFF2FF">
<tr>
<td valign="middle" align="center" style=" ">
	<!---iframe table---->

<iframe name="content"  allowtransparency="true" scrolling="auto"  id='glu' width="85%"  onload='resize_iframe()'  frameborder="0" style="background-color:#FFFFFF; padding-top:30px; "  ></iframe></td>

	<!---inside---->
</td>
</tr>
</table>
<!-- iframe table -->


<!----Footer----->
<table width="100%" height="30" cellpadding="0" cellspacing="0" align="center" bgcolor="#86DDFE">
<tr>
<td style="font-family:Arial, Helvetica, sans-serif; font-size:10px; color:#02465F; text-align:center; ">(C) All rights reserved with Entitle Tech Solutions Pvt Ltd.</td>
</tr>
</table>
<!----Footer----->
</BODY>
</HTML>
