
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
       
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
</head>
<script language="javascript">
    function submitPage(){
      var chek=validation();
      if(chek=='true'){
        document.tank.action='/throttle/addTankDetail.do';      
        document.tank.submit();
        }
        
        }
        function calculate(){
            if(document.tank.previous.value != "") {
                if(document.tank.fuelFilled.value != ""){
            var filled=document.tank.fuelFilled.value;
            var previous=document.tank.previous.value;            
            document.tank.present.value=parseFloat(filled)+parseFloat(previous);
            }
            }
            }
     function validation(){
         if(textValidation(document.tank.date,'Date')){
             return 'false';
             }
         if(textValidation(document.tank.fuelFilled,'filled Fuel')){
             return 'false';
             }
         if(textValidation(document.tank.previous,'Previous Tank Reading')){
             return 'false';
             }
         if(textValidation(document.tank.present,'Current Reading')){
             return 'false';
             }
         if(textValidation(document.tank.rate,'Total Amount')){
             return 'false';
             }
             if(document.tank.companyId.value=='0'){
                 alert("please Select Filling Station");
                 return 'false';
                 }
                 return 'true';
}         
</script>
<body>
<form name="tank" method="post">
     <%@ include file="/content/common/path.jsp" %>                 
        <%@ include file="/content/common/message.jsp" %>
        <br>
            
<table align="center" width="600" border="0" class="border" cellspacing="0" cellpadding="0">
 <tr height="30">
  <Td colspan="4" class="contenthead">Tank Details</Td>
 </tr>
 
  <tr height="30">
    <td class="text1">Date</td>  
    <td class="text1"><input name="date" class="form-control" type="textbox" value="" size="20">
        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.tank.date,'dd-mm-yyyy',this)"/></td>    
  </tr>
  <tr height="30">
    <td class="text2">Filling Station</td>
     <td class="text2"><select class="form-control" name="companyId" style="width:125px">
                        <option value="0">---Select---</option>
                        <c:if test = "${CompanyList != null}" >
                            <c:forEach items="${CompanyList}" var="comp"> 
                                 <c:choose>
                            <c:when test="${comp.companyTypeId==1012}" >
                                <option value='<c:out value="${comp.cmpId}" />'><c:out value="${comp.name}" /></option>
                            </c:when>
                            </c:choose>
                            </c:forEach>
                        </c:if>  	
                </select></td>
    
  </tr>
  <tr height="30">
    <td class="text1">Filled Fuel</td>
     <td class="text1"><input name="fuelFilled" class="form-control" onfocusOut="calculate();" type="textbox" value="" size="20"></td>
    
  </tr>
 
  <tr height="30">
    <td class="text2">PreviousReading(Ltrs)</td>
     <td class="text2"><input name="previous" class="form-control" onfocusOut="calculate();" type="textbox" value="" size="20"></td>    
  </tr>
 
  <tr height="30">
    <td class="text1">CurrentReading(Ltrs)</td>
     <td class="text1"><input name="present" class="form-control" readonly type="textbox" value="" size="20"></td>
    
  </tr> 
  <tr height="30">
    <td class="text2">Total Amount</td>
     <td class="text2"><input name="rate" class="form-control" type="textbox" value="" size="20"></td>    
  </tr>
  <tr height="30">
    <td class="text2">Tank Minimun Level</td>
     <td class="text2"><input name="minLevel" class="form-control" type="textbox" value='<c:out value="${minLevel}"/>' size="20"></td>    
  </tr>
  
</table>

<br><br>
    <center><input type="button" class="button" value="Save" onclick="submitPage();"/>

</center>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
