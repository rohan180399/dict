<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">
    function validatePanNo() {
        var nPANNo = document.getElementById("panNo").value;
        if (nPANNo != "") {
            document.getElementById("panNo").value = nPANNo.toUpperCase();
            var ObjVal = nPANNo;
            var pancardPattern = /^([ABCFGHLJPT]{1})([A-Z]{2})([PCFH]{1})([A-Z]{1})(\d{4})([A-Z]{1})$/;
            var patternArray = ObjVal.match(pancardPattern);
            if (patternArray == null) {
                alert("PAN Card No Invalid ");
                return false;
            } else {
                return true;
            }
        } else {
             alert("Please enter pan no");
            return false;
        }
    }
</script>
<script>
    $(document).ready(function () {
        $('#vendorName').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getVendorNameDetails.do",
                    dataType: "json",
                    data: {
                        vendorName: request.term
                    },
                    success: function (data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function (data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };



    });


    function checkVendorNameExists() {
        var vendorName = $("#vendorName").val();
        var vendorTypeId = $("#vendorTypeId").val();
        if (vendorName != '' && vendorTypeId != '') {
            $.ajax({
                url: "/throttle/checkVendorNameExists.do",
                dataType: "json",
                data: {
                    vendorName: vendorName,
                    vendorTypeId: vendorTypeId
                },
                success: function (data, textStatus, jqXHR) {
                    var count = data.VendorCount;
                    if (count == 0) {
                        $("#vendorStatus").text("");
                        $("#saveButton").show();
                    } else {
                        $("#vendorStatus").text("Vendor Already Exists");
                        $("#saveButton").hide();
                    }
                },
                error: function (data, type) {
                    console.log(type);
                }
            });
        }

    }


    function setSettlementDiv() {
        var vendorTypeId = document.dept.vendorTypeIdTemp.value;
//        alert(vendorTypeId);
        $("#vendorTypeId").val(vendorTypeId);
        if (vendorTypeId == 1001) {
            document.getElementById("settlementDiv").style.display = "block";
            document.getElementById("settlementDiv1").style.display = "block";
        } else {
            document.getElementById("settlementDiv").style.display = "none";
            document.getElementById("settlementDiv1").style.display = "none";
        }
        checkVendorNameExists();
    }


    function onKeyPressBlockCharacters(e) {
        var sts = false;
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /[a-zA-Z-`~!@#$%^&*() _|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
        sts = !reg.test(keychar);
        return sts;
    }


</script>
<script language="javascript">
    function submitPage() {
      //  var check = validatePanNo();
     //   if (check == true) {
            document.dept.action = '/throttle/addVendor.do';
            document.dept.submit();
     //   }
    }
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Vendor</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Vendor</a></li>
            <li class="active">Add Vendor</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="addRow();">
                <%@ include file="/content/common/message.jsp" %>
                <form name="dept" id="addVendor"  method="post" enctype="multipart/form-data" action="/throttle/addVendor.do" >
                    <center><span id="vendorStatus" style="color: red"></span></center>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="90%" id="bg" class="border">
                        <tr>
                            <td colspan="4" style="background-color:#5BC0DE;">Add Vendor</td>
                        </tr>
                        <tr>
                            <td><font color="red">*</font>Vendor Name </td>

                            <td>
                                <input type="hidden" name="vendorId" value='<c:out value="${list.vendorId}"/>'>
                                <input name="vendorName" id="vendorName" type="text" class="form-control" value="" placeholder="Type vendor name..."  required maxlength="100" onchange="checkVendorNameExists()"></td>

                            <td><font color="red">*</font>TIN No / Vendor Reg No</td>

                            <td>
                                <input name="tinNo" type="text" class="form-control" value="" placeholder="Type vendor TIN No..."  required maxlength="45"></td>
                        </tr>

                        <tr>
                            <td><font color="red">*</font>Vendor Type</td>
                            <td>
                                <input name="vendorTypeId" id="vendorTypeId"  type="hidden" class="form-control" >
                                <select name="vendorTypeIdTemp" id="vendorTypeIdTemp" onChange="setSettlementDiv();"  class="form-control" required data-placeholder="Choose One">
                                    <option value="">Choose One</option>
                                    <c:if test = "${VendorTypeList != null}" >
                                        <c:forEach items="${VendorTypeList}" var="Type">
                                            <option value='<c:out value="${Type.vendorTypeId}" />'><c:out value="${Type.vendorTypeValue}" /></option>
                                        </c:forEach >
                                    </c:if>
                                </select>
                            </td>

                            <td><div id="settlementDiv" style="display: none;"><font color="red">*</font>Settlement Type</div></td>
                            <td>
                                <div id="settlementDiv1" style="display: none;">
                                    <select class="form-control" name="settlementType"  >
                                        <option value="1">Cash Settlement</option>
                                        <option value="2">Credit Settlement</option>
                                    </select>
                                </div>
                            </td>

                        <input type="hidden" name="priceType" value="M"/>
                        </tr>
                        <tr>
                            <td>Vendor Address</td>
                            <td><textarea name="vendorAddress" rows="2" class="form-control" placeholder="Type vendor address....." maxlength="200"></textarea></td>
                            <td>Vendor Phone No/Fax No</td>
                            <td><input name="vendorPhoneNo" maxlength="11" type="text"  onkeypress="return onKeyPressBlockCharacters(event)" class="form-control" value="" placeholder="Type vendor phone no / fax no....."></td>
                        </tr>
                        <tr>
                            <td>Vendor Mail Id</td>
                            <td><input name="vendorMailId" type="text" class="form-control" value="" placeholder="Type vendor mailid......" maxlength="50"></td>
                            <td><font color="red">*</font>Credit (Days)</td>
                            <td><input name="creditDays" type="text" class="form-control" class="form-control" value="" onkeypress="return onKeyPressBlockCharacters(event)" maxlength="3" placeholder="Type credit days...."></td>
                        </tr>
                        <tr>
                            <td colspan="4" style="background-color:#5BC0DE;">Contact Details</td>
                        </tr>
                        <tr>
                            <td><font color="red">*</font>Name</td>
                            <td>
                                <input type="text" id="contactName" name="contactName" value=""  class="form-control" placeholder="Type contact person name..." required maxlength="45">
                            </td>
                            <td>Designation</td>
                            <td>
                                <input type="text" id="designation" name="designation" value=""  class="form-control" maxlength="45" >
                            </td>
                        </tr>
                        <tr>
                            <td>Email Id</td>
                            <td>
                                <input type="text" id="emailId" name="emailId" value="" class="form-control" maxlength="45">
                            </td>
                            <td><font color="red">*</font>Telephone No</td>
                            <td>
                                <input type="text" id="teleNo" name="teleNo" value="" class="form-control" maxlength="10" onkeypress="return onKeyPressBlockCharacters(event)" placeholder="Type Tel No....." required >
                            </td>
                        </tr>
                        <tr>
                            <td><font color="red">*</font>Mobile NO</td>
                            <td>
                                <input type="text" id="mobileNo" name="mobileNo" value="" maxlength="10" onkeypress="return onKeyPressBlockCharacters(event)" class="form-control"  placeholder="Type MobileNo....." required>
                            </td>
                            <td>Fax No </td>
                            <td>
                                <input type="text" id="faxNo" name="faxNo" value="" onkeypress="return onKeyPressBlockCharacters(event)"  class="form-control" maxlength="10">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="background-color:#5BC0DE;">Bank Details</td>
                        </tr>
                        <tr>
                            <td><font color="red">*</font>Bank Name</td>
                            <td>
                                <input type="text" id="bankName" name="bankName" value=""  class="form-control"  placeholder="Type BankName....." required maxlength="45">
                            </td>
                            <td><font color="red">*</font>Branch</td>
                            <td>
                                <input type="text" id="branch" name="branch" value=""  class="form-control"   placeholder="Type BranchName....." required maxlength="45">
                            </td>
                        </tr>
                        <tr>
                            <td><font color="red">*</font>Branch Code</td>
                            <td>
                                <input type="text" id="branchCode" name="branchCode" value=""  class="form-control"   placeholder="Type BankCode....." required maxlength="45">
                            </td>
                            <td><font color="red">*</font>Account No</td>
                            <td>
                                <input type="text" id="accountNo" name="accountNo" value=""  class="form-control" onkeypress="return onKeyPressBlockCharacters(event)"  placeholder="Type AccountNo....." required maxlength="45">
                            </td>
                        </tr>
                        <tr>
                            <td>IFSC Code</td>
                            <td>
                                <input type="text" id="ifscCode" name="ifscCode" value=""   class="form-control" maxlength="45">
                            </td>
                            <td>Micr No</td>
                            <td>
                                <input type="text" id="micrNo" name="micrNo" value=""  class="form-control"  onkeypress="return onKeyPressBlockCharacters(event)" maxlength="45">
                            </td>

                        </tr>
                        <tr>
                            <td colspan="4">Registration No Details</td>
                        </tr>
                        <tr>
                            <td>M.S.M.E</td>
                            <td>
                                <input type="text" id="msmeId" name="msmeId" value=""    onkeypress="return onKeyPressBlockCharacters(event)"  class="form-control" maxlength="45">
                            </td>
                            <td>GST</td>
                            <td>
                                <input type="text" id="gstId" name="gstId" value=""   onkeypress=""  class="form-control" maxlength="45">
                            </td>
                        </tr>
                        <tr>
                            <td><font color="red">*</font>Service Tax No</td>
                            <td>
                                <input type="text" id="serViceTax" name="serViceTax" value=""  class="form-control" onkeypress="return onKeyPressBlockCharacters(event)"    placeholder="Type ServiceTax No....." required maxlength="45">
                            </td>
                            <td>Excise duty</td>
                            <td>
                                <input type="text" id="exciseDuty" name="exciseDuty" value=""  class="form-control"  onkeypress="return onKeyPressBlockCharacters(event)"  maxlength="45">
                            </td>
                        </tr>
                        <tr>
                            <td>VAT - TIN</td>
                            <td>
                                <input type="text" id="vatId" name="vatId" value=""  class="form-control"  onkeypress="return onKeyPressBlockCharacters(event)"  maxlength="45">
                            </td>
                            <td><font color="red">*</font>Pan No</td>
                            <td>
                                <input type="text" id="panNo" name="panNo" value="" maxlength="10" onBlur="validatePanNo()" class="form-control" placeholder="Type Pan No....." required maxlength="45">
                            </td>
                        </tr>
                    </table>

                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="90%" id="POD1">
                        <tr  id="tableDesingTD">
                            <td align="center"  >Sno</td>
                            <td>Attachment</td>
                            <td></td>
                        </tr>
                        <% int index = 1;%>
                        <tr>
                            <td colspan="4" align="center">
                                <input type="hidden" name="selectedRowCount" id="selectedRowCount" value="1"/>
                                <input type="hidden" name="tripSheetId1" id="tripSheetId1" value="<%=request.getParameter("tripSheetId1")%>" />
                                <input type="button" name="add"  id="buttonDesign" value="Add File" onclick="addRow();" class="btn btn-info"/>
                            </td>
                        </tr>
                        <script>
                            var podRowCount1 = 1;
                            var podSno1 = '';
                            function addRow() {
                                podSno1 = document.getElementById('selectedRowCount').value;
                                if (podSno1 < 6) {
                                    var tab = document.getElementById("POD1");
                                    var newrow = tab.insertRow(podSno1);

                                    newrow.id = 'rowId' + podSno1;

                                    var cell = newrow.insertCell(0);
                                    var cell0 = "<td class='text1' height='25' >" + podSno1 + "</td>";
                                    cell.innerHTML = cell0;


                                    cell = newrow.insertCell(1);
                                    var cell0 = "<td class='text1' height='25' ><input type='file'   id='podFile" + podSno1 + "' name='podFile' class='form-control' value='' onchange='checkName(" + podSno1 + ");'><br/><font size='2' color='blue'> Allowed file type:pdf & image</td>";
                                    cell.innerHTML = cell0;


                                    if (podSno1 == 1) {
                                        cell = newrow.insertCell(2);
                                        cell0 = "<td class='text1' height='25' ></td>";
                                        cell.innerHTML = cell0;
                                    } else {
                                        cell = newrow.insertCell(2);
                                        cell0 = "<td class='text1' height='25' align='left'><input type='button' class='btn btn-info'  style='width:90px;height:30px;font-weight: bold;padding: 1px;'  name='delete'  id='delete" + podSno1 + "'   value='Delete Row' onclick='deleteRow(this);' ></td>";
                                        cell.innerHTML = cell0;
                                    }
                                    podSno1++;
                                    if (podSno1 > 0) {
                                        document.getElementById('selectedRowCount').value = podSno1;
                                    }

                                } else {
                                    alert("Only 5 Files Can Be added");
                                }
                            }
                            function deleteRow(src) {
                                podSno1--;
                                var oRow = src.parentNode.parentNode;
                                var dRow = document.getElementById('POD1');
                                dRow.deleteRow(oRow.rowIndex);
                                document.getElementById("selectedRowCount").value--;
                            }

                        </script>
                        <%index++;%>
                    </table>







                    <script type="text/javascript">

                        var ar_ext = ['pdf', 'txt', 'gif', 'jpeg', 'jpg', 'png', 'Gif', 'GIF', 'Png', 'PNG', 'JPG', 'Jpg'];        // array with allowed extensions

                        function checkName(sno) {
                            var name = document.getElementById("podFile" + sno).value;
                            var ar_name = name.split('.');

                            var ar_nm = ar_name[0].split('\\');
                            for (var i = 0; i < ar_nm.length; i++)
                                var nm = ar_nm[i];

                            var re = 0;
                            for (var i = 0; i < ar_ext.length; i++) {
                                if (ar_ext[i] == ar_name[ar_name.length - 1]) {
                                    re = 1;
                                    break;
                                }
                            }

                            if (re == 1) {
                            } else {
                                alert('".' + ar_name[ar_name.length - 1] + '" is not an file type allowed for upload');
                                document.getElementById("podFile" + sno).value = '';
                            }
                        }
                    </script>



                    </table>
                    <br>




                    <center>
                        <button class="btn btn-info" id="saveButton" type="submit">Submit</button>
                        &emsp;<input type="reset" class="btn btn-info" value="Clear"  id="buttonDesign">
                    </center>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
<script>
    jQuery(document).ready(function () {
        "use strict";
        // Select2
        jQuery(".select2").select2({
            width: '100%',
            minimumResultsForSearch: -1
        });
    });
</script>

