<%-- 
    Document   : displyVehicleWisePandL
    Created on : Jan 18, 2013, 10:18:29 AM
    Author     : Entitle
--%>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->

        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
         <%@ page import="ets.domain.report.business.ReportTO" %>
        <%@ page import="ets.domain.security.business.SecurityTO" %>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script src="/throttle/js/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script src="/throttle/js/TableSort.js" language="javascript" type="text/javascript"></script>
        <link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />

        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    </head>
    <script type="text/javascript">

        function submitPage(value)
        {

            if(textValidation(document.saveInvoice.remark,'Remark')){
                return;
            }
            document.saveInvoice.action="/throttle/saveInvoice.do";
            document.saveInvoice.submit();
        }

    </script>

    <%
                String companyId = (String) request.getAttribute("companyId");
                String fromDate = (String) request.getAttribute("fromDate");
                String toDate = (String) request.getAttribute("toDate");
                String vehicleId = (String) request.getAttribute("vehicleId");



    %>

    <body>
        <form name="saveInvoice" action=""  method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br/>
            <table cellpadding="0" cellspacing="2" align="center" border="0" width="700" id="report" bgcolor="#97caff" style="margin-top:0px;">
                <tr>
                    <td><b>Vehicle Wise Profit and Loss</b></td>
                    <td align="right"><span id="openClose" onclick="displayCollapse();" style="cursor: pointer;">Close</span>&nbsp;</td>
                </tr>
                <tr id="exp_table"  style="display: block;">
                    <td colspan="2" style="padding:15px;" align="right">
                        <div class="tabs" align="center" style="width:900px">
                            <table width="100%" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                                <tr>
                                    <td  height="30">From Date :</td>
                                    <td  height="30"><input type="text" name="fromDate" id="fromDate" value="<%=fromDate%>" />  </td>

                                    <td  height="30">To Date : </td>
                                    <td  height="30"><input type="text" name="toDate" id="toDate" value="<%=toDate%>" /></td>
                                </tr>
                                <tr>
                                    <td  height="30">Vehicle No :   </td>
                                    <td  height="30">
                                        <input type="hidden" name="vehicleId" id="vehicleId" value="<%=vehicleId%>"/>
                                        <input type="text" name="vehicleId1" id="vehicleId1" value="<c:out value="${sVehicleNo}"/>"/>
                                    </td>
                                    <td  height="30">Location :   </td>
                                    <td  height="30">
                                        <input type="hidden" name="companyId" id="companyId" value="<%=companyId%>"/>
                                        <input type="text" name="companyId1" id="companyId1" value="<c:out value="${sCompanyID}"/>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td  height="30">Vehicle Type :   </td>
                                    <td  height="30">
                                        <input type="text" name="sVehicleType" id="sVehicleType" value="<c:out value="${sVehicleType}"/>"/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <br>
            <table cellpadding="0" cellspacing="2" align="center" border="0" width="700" id="report" bgcolor="#97caff" style="margin-top:0px;">
                <c:if test="${vehicleWiseTotal != null}">
                    <c:forEach items="${vehicleWiseTotal}" var="vwt">
                        <c:set var="noOfTrips" value="${vwt.noOfTrips}"/>
                        <c:set var="pandL" value="${vwt.pandL}"/>
                        <c:set var="expenses" value="${vwt.expenses}"/>
                        <c:set var="revenue" value="${vwt.revenue}"/>
                    </c:forEach>
                </c:if>

               <tr>
                    <td width="25%" height="30">
                        <b>Number Of Trip : </b></td>
                    <td align="left"><b><c:out value="${noOfTrips}"/></b></td>
                    <td width="25%" height="30">
                        <b>Total Revenue : </b></td>
                    <td align="left"><b><c:out value="${pandL}"/></b></td>
                </tr>
                <tr>
                    <td width="25%" height="30">
                        <b>Total Expenses : </b></td>
                    <td align="left"><b><c:out value="${expenses}"/></b></td>
                    <td width="25%" height="30">
                        <b>Total Profit/Loss : </b></td>
                    <td align="left"><b><c:out value="${revenue}"/></b></td>
                </tr>
            </table>
            <br/>

                <table  border="0" class="border" align="center" width="700" cellpadding="0" cellspacing="0" id="bg">
                    <%int index = 0;%>
                <c:if test="${vehicleWiseList != null}">
                <tr>
                    <td class="contentsub" height="30">S.No</td>
                    <td class="contentsub" height="30">Trip No</td>
                    <td class="contentsub" height="30">Vehicle number</td>
                    <td class="contentsub" height="30">Route</td>
                    <td class="contentsub" height="30">Location</td>
                    <td class="contentsub" height="30">Revenue</td>
                    <td class="contentsub" height="140">Expenses</td>
                    <td class="contentsub" height="30">Profit/Loss</td>

                </tr>
                    <c:forEach items="${vehicleWiseList}" var="vwl">
                        <tr>
                            <td class="text1"  height="30"><%=index +1%></td>
                            <td class="text1"  height="30"><c:out value="${vwl.tripId}"/></td>
                            <td class="text1"  height="30"><c:out value="${vwl.regno}"/></td>
                            <td class="text1"  height="30"><c:out value="${vwl.routeName}"/></td>
                            <td class="text1"  height="30"><c:out value="${vwl.location}"/></td>
                            <td class="text1"  height="30"><c:out value="${vwl.revenue}"/></td>
                            <td class="text1"  height="30"><c:out value="${vwl.totalexpenses}"/></td>
                            <td class="text1"  height="30"><c:out value="${vwl.pandL}"/></td>
                        </tr>
                        <%index++;%>
                    </c:forEach>
                </c:if>
                     
                </table>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

    </body>

</html>