<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>
<script type="text/javascript">


    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#vehicleNo').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getVehicleNo.do",
                    dataType: "json",
                    data: {
                        vehicleNo: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            alert("Invalid Vehicle No");
                            $('#vehicleNo').val('');
                            $('#vehicleId').val('');
                            $('#vehicleNo').fous();
                        } else {
                            response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var id = ui.item.Id;
                $('#vehicleNo').val(value);
                $('#vehicleId').val(id);
                return false;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

        $('#primaryDriver').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getDriverName.do",
                    dataType: "json",
                    data: {
                        driverName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        var primaryDriver = $('#primaryDriver').val();
                        if (items == '' && primaryDriver != '') {
                            alert("Invalid Primary Driver Name");
                            $('#primaryDriver').val('');
                            $('#primaryDriverId').val('');
                            $('#primaryDriver').focus();
                        } else {
                        }
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var id = ui.item.Id;
                $('#primaryDriver').val(value);
                $('#primaryDriverId').val(id);
                $('#secondaryDriverOne').focus();
                return false;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });


    function submitPage(value) {
        if (document.getElementById('fromDate').value == '') {
            alert("please select from date");
            document.getElementById('fromDate').focus();
        } else {
            if (value == 'ExportExcel') {
                document.accountReceivable.action = '/throttle/handleDriverSettlementReport.do?param=ExportExcel';
                document.accountReceivable.submit();
            } else {
                document.accountReceivable.action = '/throttle/handleDriverSettlementReport.do?param=Search';
                document.accountReceivable.submit();
            }
        }
    }

    function setValue() {
        if ('<%=request.getAttribute("page")%>' != 'null') {
            var page = '<%=request.getAttribute("page")%>';
            if (page == 1) {
                submitPage('search');
            }
        }
    }


</script>

<style type="text/css">





    .container {width: 960px; margin: 0 auto; overflow: hidden;}
    .content {width:800px; margin:0 auto; padding-top:50px;}
    .contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

    /* STOP ANIMATION */



    /* Second Loadin Circle */

    .circle1 {
        background-color: rgba(0,0,0,0);
        border:5px solid rgba(100,183,229,0.9);
        opacity:.9;
        border-left:5px solid rgba(0,0,0,0);
        /*	border-right:5px solid rgba(0,0,0,0);*/
        border-radius:50px;
        /*box-shadow: 0 0 15px #2187e7; */
        /*	box-shadow: 0 0 15px blue;*/
        width:40px;
        height:40px;
        margin:0 auto;
        position:relative;
        top:-50px;
        -moz-animation:spinoffPulse 1s infinite linear;
        -webkit-animation:spinoffPulse 1s infinite linear;
        -ms-animation:spinoffPulse 1s infinite linear;
        -o-animation:spinoffPulse 1s infinite linear;
    }

    @-moz-keyframes spinoffPulse {
        0% { -moz-transform:rotate(0deg); }
        100% { -moz-transform:rotate(360deg);  }
    }
    @-webkit-keyframes spinoffPulse {
        0% { -webkit-transform:rotate(0deg); }
        100% { -webkit-transform:rotate(360deg);  }
    }
    @-ms-keyframes spinoffPulse {
        0% { -ms-transform:rotate(0deg); }
        100% { -ms-transform:rotate(360deg);  }
    }
    @-o-keyframes spinoffPulse {
        0% { -o-transform:rotate(0deg); }
        100% { -o-transform:rotate(360deg);  }
    }



</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="operations.reports.label.CustomerWiseProfit" text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="operations.reports.label.Report" text="default text"/></a></li>
            <li class="active"><spring:message code="operations.reports.label.CustomerWiseProfit" text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="">
                <form name="accountReceivable" action=""  method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>
                    <!--            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                                    <tr id="exp_table" >
                                        <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                                            <div class="tabs" align="left" style="width:850;">
                                                <ul class="tabNavigation">
                                                    <li style="background:#76b3f1">Customer Wise Profit</li>
                                                </ul>
                                                <div id="first">-->
                    <table class="table table-info mb30 table-hover" >
                        <thead><tr><th colspan="6"><spring:message code="operations.reports.label.CustomerWiseProfit" text="default text"/></th></tr></thead>
                        <tr>
                            <td  height="30"><spring:message code="operations.reports.label.PrimaryDriverName" text="default text"/></td>
                            <td  height="30">
                                <input type="text" style="width:260px;height:40px;" class="form-control" id="primaryDriver"  name="primaryDriver" autocomplete="off" value="<c:out value="${primaryDriverName}"/>"/>
                                <input type="hidden" class="form-control" id="primaryDriverId"  name="primaryDriverId" autocomplete="off" value="<c:out value="${primaryDriverId}"/>"/>
                            </td>
                            <td  height="30"><spring:message code="operations.reports.label.VehicleNo" text="default text"/></td>
                            <td  height="30">
                                <input type="text" style="width:260px;height:40px;" class="form-control" id="vehicleNo"  name="vehicleNo" autocomplete="off" value="<c:out value="${vehicleNo}"/>"/>
                                <input type="hidden" class="form-control" id="vehicleId"  name="vehicleId" autocomplete="off" value="<c:out value="${vehicleId}"/>"/>
                            </td>
                            <td  height="30"><spring:message code="operations.reports.label.TripCode" text="default text"/></td>
                            <td  height="30"><input type="text" style="width:260px;height:40px;" class="form-control" id="tripId"  name="tripId" autocomplete="off" value="<c:out value="${tripId}"/>"/></td>
                        </tr>
                        <tr>
                            <td  height="30"><spring:message code="operations.reports.label.SettlementFromDate" text="default text"/></td>
                            <td  height="30"><input type="text" style="width:260px;height:40px;" class="datepicker form-control" id="fromDate" name="fromDate" autocomplete="off" value="<c:out value="${fromDate}"/>"/></td>
                            <td  height="30"><spring:message code="operations.reports.label.SettlementToDate" text="default text"/></td>
                            <td  height="30"><input type="text" style="width:260px;height:40px;" class="datepicker form-control" id="toDate"  name="toDate" autocomplete="off" value="<c:out value="${toDate}"/>"/></td>
                            <td  height="30" colspan="2"></td>
                        </tr>
                    </table>

                    <tr>


                    <center><td  ><input type="button" class="btn btn-success" name="ExportExcel"   value="<spring:message code="operations.reports.label.EXPORTEXCEL" text="default text"/>" onclick="submitPage(this.name);"></td>
                        <td  ><input type="button" class="btn btn-success" value="<spring:message code="operations.reports.label.SEARCH" text="default text"/>" name="search" onClick="submitPage(this.name)"/></td></center>
                    </tr>
                    <br>
                    <br>
                    <!--                            </div></div>
                                        </td>
                                    </tr>
                                </table>-->
                    <c:if test = "${driverSettlementDetails == null}" >
                        <center><font color="red"><spring:message code="operations.reports.label.NoRecordsFound" text="default text"/></font></center>
                        </c:if>

                    <c:if test = "${driverSettlementDetails != null}" >
                        <table class="table table-info mb30 table-hover sortable">
                            <thead>
                                <tr height="80" >
                                    <th><h3 align="center"><spring:message code="operations.reports.label.SNo" text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="operations.reports.label.TripCode" text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="operations.reports.label.VehicleNo" text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="operations.reports.label.SettlementDate" text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="operations.reports.label.DriverName" text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="operations.reports.label.RunKm" text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="operations.reports.label.RunHour" text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="operations.reports.label.FuelPrice" text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="operations.reports.label.DieselUsed" text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="operations.reports.label.RCMAllocation" text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="operations.reports.label.BPCLAllocation" text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="operations.reports.label.ExtraExpense" text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="operations.reports.label.TotalMisc" text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="operations.reports.label.Bhatta" text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="operations.reports.label.TotalExpense" text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="operations.reports.label.Balance" text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="operations.reports.label.StartBalance" text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="operations.reports.label.EndBalance" text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="operations.reports.label.PayMode" text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="operations.reports.label.Remarks" text="default text"/></h3></th>
                            </tr>
                            </thead>
                            <tbody>
                                <% int index = 0, sno = 1;%>
                                <c:forEach items="${driverSettlementDetails}" var="dsList">

                                    <tr height="30">

                                        <td align="center"><%=sno%></td>
                                        <td align="left"><c:out value="${dsList.tripCode}"/></td>
                                        <td align="left"><c:out value="${dsList.vehicleNo}"/></td>
                                        <td align="left"><c:out value="${dsList.settlementDate}"/></td>
                                        <td align="left"><c:out value="${dsList.empName}"/></td>
                                        <td align="left"><c:out value="${dsList.distance}"/></td>
                                        <td align="left"><c:out value="${dsList.runHour}"/></td>
                                        <td align="left"><c:out value="${dsList.fuelPrice}"/></td>
                                        <td align="left"><c:out value="${dsList.dieselUsed}"/></td>
                                        <td align="left"><c:out value="${dsList.rcmAllocation}"/></td>
                                        <td align="left"><c:out value="${dsList.bpclAllocation}"/></td>
                                        <td align="left"><c:out value="${dsList.extraExpense}"/></td>
                                        <td align="left"><c:out value="${dsList.totalMiscellaneous}"/></td>
                                        <td align="left"><c:out value="${dsList.bhatta}"/></td>
                                        <td align="left"><c:out value="${dsList.totalExpense}"/></td>
                                        <td align="left"><c:out value="${dsList.balance}"/></td>
                                        <td align="left"><c:out value="${dsList.startingBalance}"/></td>
                                        <td align="left"><c:out value="${dsList.endingBalance}"/></td>
                                        <td align="left"><c:out value="${dsList.payMode}"/></td>
                                        <td align="left"><c:out value="${dsList.remarks}"/></td>
                                    </tr>
                                    <%
                                                index++;
                                                sno++;
                                    %>
                                </c:forEach>

                            </tbody>
                        </table>

                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5" selected="selected">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span><spring:message code="operations.reports.label.EntriesPerPage" text="default text"/></span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text"><spring:message code="operations.reports.label.DisplayingPage" text="default text"/> <span id="currentpage"></span> <spring:message code="operations.reports.label.of" text="default text"/> <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 0);
                        </script>

                        <!--                <table>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                            </tr>
                        
                                        </table>-->
                        <c:if test = "${customerWiseProfitList != null}" >
                            <table border="2" style="border: 1px solid #666666;"  align="center"  cellpadding="0" cellspacing="1" >
                                <tr height="25" align="right">
                                    <td style="background-color: #6374AB; color: #ffffff"><spring:message code="operations.reports.label.TotalTripsCarriedOut" text="default text"/></td>
                                    <td width="150" align="right"><fmt:formatNumber type="number"  value="${totalTripNos}" /></td>
                                </tr>
                                <tr height="25" >
                                    <td style="background-color: #6374AB; color: #ffffff"><spring:message code="operations.reports.label.TotalIncome" text="default text"/></td>
                                    <td width="150" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalRevenue}" /></td>
                                </tr>
                                <tr height="25" >
                                    <td style="background-color: #6374AB; color: #ffffff"><spring:message code="operations.reports.label.TotalFixedExpenses" text="default text"/></td>
                                    <td width="150" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${expenses}" /></td>
                                </tr>
                                <tr height="25" >
                                    <td style="background-color: #6374AB; color: #ffffff"><spring:message code="operations.reports.label.TotalProfit" text="default text"/> </td>
                                    <td width="150" align="right">
                                        <c:if test="${profit > 0}">
                                    <font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profit}" /></font>
                                </c:if>
                                <c:if test="${profit <= 0}">
                                    <font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profit}" /></font>
                                </c:if>
                                </td>
                                </tr>
                                <tr height="25">
                                    <td style="background-color: #6374AB; color: #ffffff"><spring:message code="operations.reports.label.TotalProfit" text="default text"/> % </td>
                                    <td width="150" align="right">
                                        <c:if test="${perc/count > 0}">
                                    <font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font>
                                </c:if>
                                <c:if test="${perc/count <= 0}">
                                    <font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font>
                                </c:if>
                                </td>
                                </tr>
                            </table>
                        </c:if>
                    </c:if>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>    
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>