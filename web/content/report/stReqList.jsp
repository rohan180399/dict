
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
 
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
       <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
        <title>MRSList</title>
    </head>
    
    
    
    <script>

function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}

        function addRow(val){
            //if(document.getElementById(val).innerHTML == ""){
            document.getElementById(val).innerHTML = "<input type='text' size='7' class='form-control' >";
            //}else{
            //document.getElementById(val).innerHTML = "";
            //}
        }
        
        function showTable()
        {
            var selectedMrs = document.getElementsByName("selectedIndex");
            var counter=0;
            for(var i=0;i<selectedMrs.length;i++){
                if(selectedMrs[i].checked == 1){
                    counter++;
                    break;
                }       
            }    
            if(counter ==0){
                alert("Please Select MRS");    
            }else{
            document.bill.action = "/throttle/mrsSummary.do";
            document.bill.submit();
        }
    }
    function newWO(){
        window.open('/throttle/content/stores/OtherServiceStockAvailability.html', 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
    }
    function categoryWindow(toId){
         var fromId=document.bill.fromId.value;
         var fromDate=document.bill.fromDate.value;
         var toDate=document.bill.toDate.value;
         var url='/throttle/categorywiseGD.do?toId='+toId+'&fromId='+fromId+'&fromDate='+fromDate+'&toDate='+toDate;
         
        window.open(url, 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
    }
    function submitPag(val){
        if(validate() == '0'){
            return;
        }else if(val=='purchaseOrder'){
        document.bill.purchaseType.value="1012"
        document.bill.action = '/throttle/generateMpr.do'
        document.bill.submit();
    }else if(val == 'localPurchase'){
    document.bill.purchaseType.value="1011"
    document.bill.action = '/throttle/generateMpr.do'
    document.bill.submit();    
}
}   


function validation(){
    
    if(document.bill.toDate.value==''){
        alert("Please Enter From Date");
        return 'false';
    }else if(document.bill.fromDate.value==''){
    alert("Please Enter to Date");
    return 'false';
} 
if(document.bill.type.value==''){
    alert("Please Select Type");
    document.bill.type.focus();
    return 'false';
}
return 'true';   
}
function searchSubmit(){
    var chek= validation();  
    if(chek=='true'){  
        document.bill.action = '/throttle/stList.do'
        document.bill.submit();    
    }
}    




function setDate(fDate,tDate,fromId,toId){
    if(fDate != 'null'){
        document.bill.fromDate.value=fDate;
    }
    if(tDate != 'null'){
        document.bill.toDate.value=tDate;
    }
    if(fromId != 'null'){
        document.bill.fromId.value=fromId;
    }    
    if(toId != 'null'){
        document.bill.toId.value=toId;
    }      
    if('<%=request.getAttribute("type")%>' != 'null'){
        document.bill.type.value='<%=request.getAttribute("type")%>';
    } else{
    document.bill.type.value='GD';
}
}


function details(indx){
    
    //gdId=document.getElementsByName("gdId");
    //alert(gdId[indx].value);
    
    var url = '/throttle/handleGdDetail.do?gdId='+indx;    
    window.open(url , 'PopupPage', 'height=450,width=650,scrollbars=yes,resizable=yes');
}
function grdetails(indx){
    
    //requestId=document.getElementsByName("requemenustIds");
    
    var url = '/throttle/stDetail.do?reqId='+indx;
    
    window.open(url , 'PopupPage', 'height=450,width=650,scrollbars=yes,resizable=yes');    
    
}

    </script>
    
    <body onLoad="setDate('<%= request.getAttribute("fromDate") %>','<%= request.getAttribute("toDate") %>','<%= request.getAttribute("fromId") %>','<%= request.getAttribute("toId") %>' )">        
        
        <form name="bill"  method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>

<table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:850;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">Stock Transfer Report</li>
    </ul>
    <div id="first">
    <table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
  <tr>
                    <td height="30">From Service Point</td>
                    <td  >
                        <select class="form-control" name="fromId"  style="width:125px;">
                            <option value="0">---Select---</option>
                            <c:if test = "${operationPointList != null}" >
                                <c:forEach items="${operationPointList}" var="Dept">
                                    <option value='<c:out value="${Dept.spId}" />'> <c:out value="${Dept.spName}" /></option>
                                </c:forEach >

                            </c:if>
                        </select>
                    </td>
                    <td height="30">To Service point</td>
                    <td  >
                        <select class="form-control" name="toId"  style="width:125px;">
                            <option value="0">---Select---</option>
                            <c:if test = "${operationPointList != null}" >
                                <c:forEach items="${operationPointList}" var="Dept">
                                    <option value='<c:out value="${Dept.spId}" />'> <c:out value="${Dept.spName}" /></option>
                                </c:forEach >
                            </c:if>
                        </select>
                    </td>
                    <td height="30">Type</td>
                    <td  >
                        <select class="form-control" name="type"  style="width:125px;">
                            <option selected value="GD">GD</option>
                            <option value="GR">GR</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td height="30" width="200">From Date</td>
                    <td height="30" width="200">
                        <input type="text" class="form-control" name="fromDate" value="" >
                        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.bill.fromDate,'dd-mm-yyyy',this)"/>
                    </td>

                    <td height="30" width="200">TO Date</td>
                    <td height="30" width="200">
                        <input type="text" class="form-control" readonly name="toDate" value="" >
                        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.bill.toDate,'dd-mm-yyyy',this)"/>
                    </td>
                    <td><input type="button" class="button" readonly name="search" value="search" onClick="searchSubmit();" ></td>
                </tr>
    </table>
    </div></div>
    </td>
    </tr>
    </table>

             
             
              
            <center>  
            </center>
            <br>
            <br>
            
            <% int index = 0;
            String classText = "";
            int oddEven = 0;
            %>    
            <c:if test = "${stReqList != null}" >



                <table align="right" border="0" cellpadding="0" cellspacing="0" width="250" id="bg" class="border">

                  <c:set var="tot" value="0"></c:set>
                        <c:if test = "${operationPointList != null}" >
                            <tr>
                                <td class="contentsub" height="30">Service Point</td>
                                 <td class="contentsub" height="30">Stock Transfer</td>
                            </tr>
                                    <c:forEach items="${operationPointList}" var="spoint">
                                    <c:if test = "${spoint.spId!=fromId}" >
                                        <tr>

                                              <c:set var="tot" value="${tot+spoint.amount}"></c:set>
                                              
                                              <td class="text1" height="30"><a href="" onClick="categoryWindow('<c:out value="${spoint.spId}"/>')"> <c:out value="${spoint.spName}"/></a></td>
                                            <td class="text1" height="30">   <fmt:setLocale value="en_US" /><b>SAR:<fmt:formatNumber value="${spoint.amount}" pattern="##.00"/></b></td>


                                            </tr>
                                             </c:if>
                                    </c:forEach>
                                            <tr>
                                              
                                            <td class="text2" height="30">Total Stock Transfer</td>
                                            <td class="text2" height="30">   <fmt:setLocale value="en_US" /><b>SAR:<fmt:formatNumber value="${tot}" pattern="##.00"/></b></td>

                                            </tr>
                                   </c:if>
                </table>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" id="bg" class="border">
                    <tr>
                        <td class="contentsub" height="30">Sno</td>
                        <td class="contentsub" height="30">Gr Id</td>
                        <td class="contentsub" height="30">GD Id</td>
                        <td class="contentsub" height="30">From SP</td>
                        <td class="contentsub" height="30">To SP</td>
                        <td class="contentsub" height="30">Remarks</td>
                        <td class="contentsub" height="30">Required Date</td>
                        <td class="contentsub" height="30">Issued Date</td>
                        <td class="contentsub" height="30">Stock Worth(SAR)</td>
                    </tr>
                    <c:forEach items="${stReqList}" var="bill">    
                        <%

            oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        
            
                        <tr>
                            <td class="<%=classText %>" height="30"> <%= index + 1 %> </td>
                            <td class="<%=classText %>" height="30">
                                <input type="hidden" name="requemenustIds" value='<c:out value="${bill.grId}"/>' >
                                
                                <a href="" onClick="grdetails( <c:out value="${bill.grId}"/>)" > <c:out value="${bill.grId}"/> </a>
                                
                            </td>
                            <input type="hidden" name="gdId" value='<c:out value="${bill.gdId}"/>' >
                            
                            <td class="<%=classText %>" height="30"><a href="" onClick="details('<c:out value="${bill.gdId}"/> ')" > <c:out value="${bill.gdId}"/> </a></td>
                            
                            <td class="<%=classText %>" height="30">
                                <c:if test = "${operationPointList != null}" >
                                    <c:forEach items="${operationPointList}" var="Dept">                             
                                        <c:if test = "${Dept.spId==bill.fromSpId }" >
                                            <c:out value="${Dept.spName}"/>
                                        </c:if>
                                    </c:forEach>
                                </c:if>    
                            </td>
                            <td class="<%=classText %>" height="30">
                                <c:if test = "${operationPointList != null}" >
                                    <c:forEach items="${operationPointList}" var="Dept">                             
                                        <c:if test = "${Dept.spId==bill.toSpId }" >
                                            <c:out value="${Dept.spName}"/>
                                        </c:if>
                                    </c:forEach>
                                </c:if>                                                         
                            </td>
                            <input type="hidden" name="gdId" value='<c:out value="${bill.gdId}"/>'>                           
                            <td class="<%=classText %>" height="30"><c:out value="${bill.remarks}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${bill.requiredDate}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${bill.issuedDate}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${bill.amount}"/></td>
                        </tr>
                       
                        
                        
                        <%
            index++;
                        %>
                    </c:forEach>
                    
                    
                        <tr>
                            <td colspan="10">&nbsp;</td>

                        </tr>
                          
                    

                        
                </table>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
        <style>
.text1 {
font-family:Tahoma;
font-size:12px;
color:#333333;
background-color:#E9FBFF;
padding-left:10px;
}
.text2 {
font-family:Tahoma;
font-size:12px;
color:#333333;
background-color:#FFFFFF;
padding-left:10px;
}
.contentsub {
background-image:url(/throttle/images/button.gif);
background-repeat:repeat-x;
height:15px;
font-family:Tahoma;
padding-left:5px;
font-size:11px;
font-weight:bold;
color:#0070D4;
text-align:left;
padding-bottom:8px;
}

.contenthead {
background-image:url(/throttle/images/button.gif);
background-repeat:repeat-x;
height:15px;
font-family:Tahoma;
padding-left:5px;
font-size:11px;
font-weight:bold;
color:#0070D4;
text-align:center;
padding-bottom:8px;
}

</style>  
        
    </body>
</html>
