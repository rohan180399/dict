<%--
Document   : BPCLTransactionReport
Created on : Oct 31, 2013, 1:48:05 PM
Author     : Arul
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        $(".datepicker").datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true, changeYear: true
        });
    });
</script>


<script type="text/javascript">
    function submitPage(value) {
        var invoiceId = document.getElementById("invoiceId").value;
        var gstStatusType= document.getElementById("gstStatusType").value;
        if (document.getElementById('fromDate').value == '') {
            alert("Please select from Date");
            document.getElementById('fromDate').focus();
        } else if (document.getElementById('toDate').value == '') {
            alert("Please select to Date");
            document.getElementById('toDate').focus();
        } else if (document.getElementById('sbBillBoeBill').value != '' && document.getElementById('movTypeId').value == '0') {
            alert("Please select Movement Type");
            document.getElementById('movTypeId').focus();
        } else if (document.getElementById('invoiceId').value == '0') {
            alert("Please select Invoice");
            document.getElementById('invoiceId').focus();
        } else if (document.getElementById("invoiceId").value == '2' && document.getElementById('invoiceType').value == '0') {

            alert("Please select Invoice Type");
            document.getElementById('invoiceType').focus();
        } else {
            if (value == "ExportExcel") {
                document.BPCLTransaction.action = '/throttle/handlePendingGrForInvoiceReport.do?param=ExportExcel&gstStatusType='+gstStatusType;
                document.BPCLTransaction.submit();
            } else if (value == "search") {
                document.BPCLTransaction.action = '/throttle/handlePendingGrForInvoiceReport.do?param=search &invoiceId=' + invoiceId+'&gstStatusType='+gstStatusType;
                document.BPCLTransaction.submit();
            }
        }

    }
</script> 

<script>
    function saveThis(name) {
        var invoice = document.getElementById("invoiceId").value;
        var x = 0;
        var inputs = document.getElementsByName("selectedIndex");
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].type == "checkbox") {
                if (inputs[i].checked == true) {
                    x++;
                }
            }
        }

        if (name == "save") {
            if (x > 0) {
                if (document.getElementById('commodityIds').value == '') {
                    alert("Please select Commodity name");
                    document.getElementById('commodityIds').focus();
                } else {
                    document.BPCLTransaction.action = "/throttle/savePendingGrForInvoiceReport.do?param=" + name + "&invoice=" + invoice;
                    document.BPCLTransaction.submit();
                }
            } else {
                alert("Please Select any one Check Box")
            }

        }

    }



</script>
<script>
    $(document).ready(function () {
        $('#grNos').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getGRNoLists.do",
                    dataType: "json",
                    data: {
                        grNos: request.term
                    },
                    success: function (data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            $('#grIds').val('');
                            $('#grNos').val('');
                            $('#tripIds').val('');
                        }
                        response(items);
                    },
                    error: function (data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                var value = ui.item.Name;
                var tmp = value.split(',');
                $('#grIds').val(tmp[0]);
                $('#grNos').val(tmp[1]);
                $('#tripIds').val(tmp[2]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split(',');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });


</script>   

<script>
    $(document).ready(function () {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#customerName').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getConsignorName.do",
                    dataType: "json",
                    data: {
                        consignorName: request.term,
                        customerId: document.getElementById('customerId').value

                    },
                    success: function (data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function (data, type) {
                        console.log(type);
                    }

                });
            },
            minLength: 1,
            select: function (event, ui) {
                var value = ui.item.Name;
                $('#customerName').val(value);
//                        $('#consignorPhoneNo').val(ui.item.Mobile);
//                        $('#consignorAddress').val(ui.item.Address);
                $('#customerId').val(ui.item.custId);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });
</script>

<script>
    function checkAll() {
        var selectAll = document.getElementById("selectAll").checked;
        var functionStatus = document.getElementsByName("selectedIndex");
        if (selectAll == true) {
            for (var i = 0; i < functionStatus.length; i++) {
                document.getElementById("selectedIndex" + i).checked = true;
                $("#selectedId" + i).val("1");
            }
        } else {
            for (var i = 0; i < functionStatus.length; i++) {
                document.getElementById("selectedIndex" + i).checked = false;
                $("#selectedId" + i).val("0");
            }
        }
    }
</script>
<!--
  <script>
                        function checkAll() {
                            var input = document.getElementsByName("selectAll");
                            var inputs = document.getElementsByName("selectedIndex");
                            for (var i = 0; i < input.length; i++) {
                                if (inputs[i].type == "checkbox") {
                                    if (inputs[i].checked == true) {
                                        inputs[i].checked = false;
                                        $("#selectedId"+i).val("1");
                                    } else if (inputs[i].checked == false) {
                                        inputs[i].checked = true;
                                        $("#selectedId"+i).val("0");
                                    }
                                }
                            }
                        }
                    </script>-->

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Billing</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Billing</a></li>
            <li class="active">GR Invoice Pending</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="checkvalue('<c:out value="${setInvoice}"/>')">
                <form name="BPCLTransaction" method="post">
                    <%@ include file="/content/common/message.jsp"%>

                    <table  class="table table-info mb30 table-hover" style="width: 50%">
                        <tr>
                            <td><font color="red">*</font>From Date</td>
                            <td height="30"><input name="fromDate" id="fromDate" type="text" class="form-control datepicker" value="<c:out value="${fromDate}"/>" style="width: 90px;" ></td>
                            <td><font color="red">*</font>To Date</td>
                            <td height="30"><input name="toDate" id="toDate" type="text" class="form-control datepicker" value="<c:out value="${toDate}"/>" style="width: 90px;"></td>
                        </tr>
                        <tr>
                            <td><font color="red">*</font>Party Name</td>
                            <td><input type="hidden" name="customerId" id="customerId" class="form-control" value="<c:out value="${customerId}"/>"/>
                                <input type="text" class="form-control" name="customerName"   id="customerName" value="<c:out value="${customerName}"/>" style="width:240px;height:40px;" onKeyPress="return onKeyPressBlockNumbers(event);"/></td>

                            <td><font color="red">*</font>Gr No</td>
                            <td height="30">
                                <input name="tripIds" id="tripIds" type="hidden" class=" form-control" style="width:240px;height:40px;" value="<c:out value="${tripIds}"/>">
                                <input name="grIds" id="grIds" type="hidden" class=" form-control" style="width:240px;height:40px;" value="<c:out value="${grIds}"/>">
                                <input name="grNos" id="grNos" type="text" class=" form-control" style="width:240px;height:40px;" value="<c:out value="${grNos}"/>">
                            </td>
                        </tr>

                        <tr>  
                            <td>Movement Type</td>
                            <td><select name="movTypeId" id="movTypeId" class="form-control" style="width:240px;height:40px;">
                                    <option value="0" selected>--select--</option>
                                    <option value="1" >Shipping Bill</option>
                                    <option value="2">BOE</option>
                                </select> 
                            </td> 
                    <script>
                        $("#movTypeId").val('<c:out value="${movTypeId}" />');
                    </script>
                            <td><font color="red">*</font>Shipping/BOE</td>
                            <td><input type="text" name="sbBillBoeBill" id="sbBillBoeBill" class="form-control" value="<c:out value="${sbBillBoeBill}"/>"/></td>
                        </tr>
                        <tr >   

                            <td><font color="red">*</font>Invoice</td>
                            <td><select name="invoiceId" id="invoiceId" class="form-control" onchange='checkvalue(this.value)'  style="width:240px;height:40px;">
                                    <option value="0" selected>--select--</option>
                                    <option value="1">Before Invoice</option>
                                    <option value="2">After Invoice</option>
                                </select> 
                            </td> 
                        <script>
                            
                            document.getElementById("invoiceId").value = '<c:out value="${setInvoice}"/>';
                        </script>

                        <td name="type" id="type" style='width:250px;height:40px;display:none;'><font color="red">*</font>Invoice Type</td>
                        <td height="30">
                            <select  name="invoiceType" id="invoiceType" style='width:250px;height:40px;display:none;' >
                                <option value="0">--Select--</option>
                                <option value="1">Main Invoice</option>
                                <option value="2">Credit Note Invoice - Main</option>
                                <option value="3">Supplementary Invoice</option>
                                <option value="4">Credit Note Invoice - Supplementary</option>
                            </select> 

                        </td>

                        <script>
                            document.getElementById("invoiceType").value = '<c:out value="${invoiceType}"/>';
                        </script>


                        <script>
                            function checkvalue(val) {
//                                alert(val)
                                if (val == 2) {
                                    document.getElementById('invoiceType').style.display = 'block';
                                    document.getElementById('type').style.display = 'block';
                                } else {
                                    document.getElementById('invoiceType').value = '0';
                                    document.getElementById('invoiceType').style.display = 'none';
                                    document.getElementById('type').style.display = 'none';
                                }
                            }
                        </script>                         


                        <%--<c:forEach items="${invoiceReport}" var="com">--%>  
                            <td width="30" class="form-control"  style="display:none"><input type="hidden" id="gstStatuss" readonly name="gstStatuss" value="<c:out value="${gstType}"/>"/></td>
                            <td width="30" class="form-control" style="display:none">
                                <input type="hidden" id="commoditys" name="commoditys" value="<c:out value="${commodityId}"/>"/>
                                <input type="hidden" id="gstTypes" name="gstTypes" value="<c:out value="${gstType}"/>"/>
                                <input type="hidden" id="gstStatusType" name="gstStatusType" value="<c:out value="${gstType}"/>"/>
                                <input type="hidden" id="commodityNames" name="commodityNames" value="<c:out value="${commodityName}"/>"/></td>
                            <%--</c:forEach>--%> 

                        </tr>

                        <tr>     <td colspan="3"></td>
                            <td><input type="button" class="btn btn-info" name="search" onclick="submitPage(this.name);" value="Search">
                                &nbsp;&nbsp;<input type="button" class="btn btn-info" name="ExportExcel" onclick="submitPage(this.name);" value="ExportExcel">
                            </td>


                        </tr>

                    </table>
                    <table align="left">
                        <td>  
                        <tr>  
                            <td>Commodity</td>
                            <td><select id="commodityIds" name="commodityIds"  class="form-control" style="width:240px;height:40px;" onChange="setGstTypeMain();">
                                    <c:if test="${comodityDetails != null}">
                                        <option value="">---select-----</option>
                                        <c:forEach items="${comodityDetails}" var="coms">
                                            <option value='<c:out value="${coms.commodityId}" />~<c:out value="${coms.commodityName}" />~<c:out value="${coms.gstType}" />'><c:out value="${coms.commodityName}" /></option>
                                        </c:forEach> 
                                    </c:if>
                                            <script>   
                                                $("#commodityIds").val('<c:out value="${commodityId}" />~<c:out value="${commodityName}" />~<c:out value="${gstType}" />');
                                            </script>      
                            </td>
                        </tr>
                    </table>
                        <br>
                    
                    <br>
                    <br>
                    <c:if test="${invoiceReport != null}">
                        <table class="table table-info mb30 table-bordered"  id="table" >

                            <thead>
                                <tr>
                                    <th  > S.No </th>
                                    <th  > Bill of Entry </th>
                                    <th  > Shipping Bill No </th>
                                    <th  > GR No </th>
                                    <th  > GR Date </th>
                                    <th  > Movement Type </th>
                                    <th  > Invoice No </th>
                                    <th  > Invoice Date </th>
                                    <th  > Billed Party Name </th>
                                    <th  > Route  </th>
                                    <th  > Container No </th>
                                    <th  > Container Size </th>
                                    <th  > Freight Amount </th>
                                    <th  > Detention Amount </th>
                                    <th  > Toll Tax </th>
                                    <th  > Weightment </th>
                                    <th  > other Expense </th>
                                    <th  > Total Amount </th>
                                    <th  > Creation Date </th>
                                    <th  > Creator Name </th>
                                    <th > Commodity </th>
                                    <th  > GST Applicable </th>
                                    <th  > Pending GR Remarks </th>
                                    <th>Select<input type="checkbox" name="selectAll" id="selectAll" style="width:20px;height:20px;" onclick="checkAll();"/>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int index1 = 1;%>
                                <% int index = 0;%>

                                <c:forEach items="${invoiceReport}" var="Invoice">
                                    <%
                                                String classText = "";
                                                int oddEven = index % 2;
                                                if (oddEven > 0) {
                                                    classText = "text2";
                                                } else {
                                                    classText = "text1";
                                                }
                                    %>
                                    <tr>
                                        <td ><%=index1%></td>
                                        <td  width="30"><c:out value="${Invoice.billOfEntryNo}"/></td>
                                        <td  width="30"><c:out value="${Invoice.shipingBillNo}"/></td>
                                        <td  width="30"><c:out value="${Invoice.grNo}"/></td>
                                        <td  width="30"><c:out value="${Invoice.grDate}"/></td>
                                        <td  width="30"><c:out value="${Invoice.movementType}"/></td>
                                        <td  width="30"><c:out value="${Invoice.invoiceCode}"/>
                                        <td width="30"><c:out value="${Invoice.invoiceDate}"/></td>
                                        <td width="30" ><c:out value="${Invoice.billedPartyname}"/></td>
                                        <td  width="30"><c:out value="${Invoice.routeInfo}"/></td>
                                        <td width="30"  ><c:out value="${Invoice.containerNo}"/></td>
                                        <td width="30"  ><c:out value="${Invoice.containerSize}"/></td>
                                        <td width="30"  ><c:out value="${Invoice.frieghtAmount}"/></td>
                                        <td width="30"  ><c:out value="${Invoice.detentionAmount}"/>
                                        </td>
                                        <td width="30"  ><c:out value="${Invoice.tollAmount}"/></td>
                                        <td width="30"  ><c:out value="${Invoice.weightMent}"/></td>
                                        <td width="30"  ><c:out value="${Invoice.otherExpense}"/></td>

                                        <td width="30"  ><c:out value="${Invoice.otherExpense + Invoice.tollAmount + Invoice.detentionAmount + Invoice.frieghtAmount}"/></td>
                                        <td width="30"  ><c:out value="${Invoice.createdDate}"/></td>
                                        <td width="30"  ><c:out value="${Invoice.creatorName}"/></td>
                                        <td ><c:out value="${Invoice.commodityName}"/>
<!--                                             <select disabled id="commodityId<%=index%>"  name="commodityId" style="width:180px;height:40px;" onChange="setGstType(<%=index%>);"/>
                                                           <option value="0~0~N">---select-----</option>
                                            <c:forEach items="${comodityDetails}" var="com">
                                                <option value='<c:out value="${com.commodityId}" />~<c:out value="${com.commodityName}" />~<c:out value="${com.gstType}" />'><c:out value="${com.commodityName}" /></option>
                                            </c:forEach>
                                    </select>-->
                                        </td>
                                        <td width="30"  ><input type="text" id="gstStatus<%=index%>" readonly name="gstStatus" value="<c:out value="${Invoice.gstType}"/>"/></td>
                                        <td width="30"  >
                                            <input type="hidden" id="commodity<%=index%>" name="commodity" value="<c:out value="${Invoice.commodityId}"/>"/>
                                            <input type="hidden" name="selectedIndexs" id="selectedIndexs<%=index%>" value="0"/>
                                            <input type="hidden" id="selectedId<%=index%>" name="selectedId" value="0"/>
                                            <input type="hidden" id="grId<%=index%>" name="grId" value="<c:out value="${Invoice.grId}"/>"/>
                                            <input type="hidden" id="gstType<%=index%>" name="gstType" value="<c:out value="${Invoice.gstType}"/>"/>
                                            <input type="hidden" id="commodityName<%=index%>" name="commodityName" value="<c:out value="${Invoice.commodityName}"/>"/>
                                            <input type="hidden" id="tripId<%=index%>" name="tripId" value="<c:out value="${Invoice.tripId}"/>"/>
                                            <textarea  id="pendingGrRemarks<%=index%>" name="pendingGrRemarks" value="<c:out value="${Invoice.remarks}"/>" rows="4" cols="20"><c:out value="${Invoice.remarks}"/></textarea></td>


                                        <td width="30"  >
                                            <input type="checkbox" name="selectedIndex" id="selectedIndex<%=index%>" value="1"  style="margin-left: 30px" onclick="setValue(<%=index%>);"/> </td> 
                                            <%index++;%>
                                            <%index1++;%>
                                    </tr>

                                <script>

//                                    function setGstType(sno) {
//                                        var gstType = document.getElementById("commodityId" + sno).value;
//                                        var temp = gstType.split("~");
//                                        document.getElementById("gstType" + sno).value = temp[2];
//                                        document.getElementById("gstStatus" + sno).value = temp[2];
//                                        document.getElementById("commodityName" + sno).value = temp[1];
//                                        document.getElementById("commodity" + sno).value = temp[0];
//                                    }
                                </script>
                            </c:forEach>
                            <input type="hidden" name="selInd" id="selInd" value="<%=index%>">
                            </tbody>
                            <script>

                                function  setGstTypeMain() {
                                    var gstType = document.getElementById("commodityIds").value;
                                    var invoiceId = document.getElementById("invoiceId").value;
                                    var invoiceType = document.getElementById("invoiceType").value;
                                    var temp = gstType.split("~");
                                    document.getElementById("gstTypes").value = temp[2];
                                    document.getElementById("gstStatuss").value = temp[2];
                                    document.getElementById("commodityNames").value = temp[1];
                                    document.getElementById("commoditys").value = temp[0];
                                    document.getElementById("gstStatusType").value = temp[2];
                                    var gstStatusType = document.getElementById("gstStatusType").value;
                                    if(invoiceId == "2" && invoiceType !="0"){
//                                        alert(gstStatusType)
                                        document.BPCLTransaction.action = '/throttle/handlePendingGrForInvoiceReport.do?param=search &invoiceId=' + invoiceId+'&gstStatusType='+gstStatusType;
                                        document.BPCLTransaction.submit();
                                    }
                                    
                                }
                            </script>

                            <script>
//                                function setCommodity() {
////                                        alert("Sssss")
//                                    var commodityId = document.getElementsByName("commodity");
//                                    var commodityName = document.getElementsByName("commodityName");
//                                    var gstType = document.getElementsByName("gstType");
//                                    var temp = "";
////                                        alert(commodityId.length)
//                                    for (var i = 0; i <= commodityId.length; i++) {
//                                        temp = commodityId[i].value + "~" + commodityName[i].value + "~" + gstType[i].value;
//                                        document.getElementById("commodityId" + i).value = temp;
//                                        document.getElementById("commodity" + i).value = commodityId[i].value;
//                                        document.getElementById("commodityName" + i).value = commodityName[i].value;
//                                        document.getElementById("gstType" + i).value = gstType[i].value;
////                                        alert(document.getElementById("commodityId"+i).value())
//                                    }
//                                }

                            </script>
                        </table>
                        <table align="center">
                            <tr>
                                <td><input type="button" class="btn btn-info" name="save" onclick="saveThis(this.name);" value="save"></td>
                            </tr>
                        </table>

                    </c:if>
                    <br>
                    <br>

                    <script language="javascript" type="text/javascript">
                        function setValue(sno) {
                            var selectedIndex = document.getElementsByName('selectedIndex');
                            if (document.getElementById("selectedIndex" + sno).checked == true) {
                                $("#selectedIndexs" + sno).val("1");
                                $("#selectedId" + sno).val("1");
                            } else {
                                $("#selectedIndexs" + sno).val("0");
                                $("#selectedId" + sno).val("0");
                            }
                        }
                    </script>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <!--                    <div id="controls">
                                            <div id="perpage">
                                                <select onchange="sorter.size(this.value)">
                                                    <option value="5" selected="selected">5</option>
                                                    <option value="10">10</option>
                                                    <option value="20">20</option>
                                                    <option value="50">50</option>
                                                    <option value="100">100</option>
                                                </select>
                                                <span>Entries Per Page</span>
                                            </div>
                                            <div id="navigation">
                                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                            </div>
                                            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                                        </div>-->
                    <!--                    <script type="text/javascript">
                                            var sorter = new TINY.table.sorter("sorter");
                                            sorter.head = "head";
                                            sorter.asc = "asc";
                                            sorter.desc = "desc";
                                            sorter.even = "evenrow";
                                            sorter.odd = "oddrow";
                                            sorter.evensel = "evenselected";
                                            sorter.oddsel = "oddselected";
                                            sorter.paginate = true;
                                            sorter.currentid = "currentpage";
                                            sorter.limitid = "pagelimit";
                                            sorter.init("table", 0);
                                        </script>-->
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

