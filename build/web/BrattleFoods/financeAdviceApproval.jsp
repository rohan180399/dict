<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $( "#datepicker" ).datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $( ".datepicker" ).datepicker({

            /*altField: "#alternate",
                        altFormat: "DD, d MM, yy"*/
            changeMonth: true,changeYear: true
        });

    });
</script>

    </head>
    <script language="javascript">
        function submitPage(){
            if(textValidation(document.approve.advancerequestamt,'Request Amount')){
                return;
            }
            if(textValidation(document.approve.requeststatus,'Request Status')) {

                return;
            }
            if(textValidation(document.approve.requeston,'Request on')){

                return;
            }            

            document.approve.action = '/throttle/addAdvanceRequest.do';
            document.approve.submit();
        }
        function setFocus(){
            document.approve.approveamt.focus();
        }
    </script>


    <body onload="setFocus();">
        <form name="approve"  method="post" >
            <%
            request.setAttribute("menuPath","Advance >> Approval Request");
            String tripid = request.getParameter("tripid");
            String tripday = request.getParameter("tripday");                        
            String estimatedadvance = request.getParameter("estimatedadvance");
            String advicedate = request.getParameter("advicedate");
            String cnoteName = request.getParameter("cnoteName");
            String vehicleTypeName = request.getParameter("vehicleTypeName");
            String routeName = request.getParameter("routeName");
            String driverName = request.getParameter("driverName");
            String planneddate = request.getParameter("planneddate");
            String estimatedexpense = request.getParameter("estimatedexpense");
            String actualadvancepaid = request.getParameter("actualadvancepaid");
            String vegno = request.getParameter("vegno");
            String type = request.getParameter("billtype");
            
            %>
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="50%" id="bg" class="border">
                <tr align="center">
                    <td colspan="2" align="center" class="contenthead" height="30">
                        <div class="contenthead">Advance Approval Request</div></td>
                </tr>
                
                <tr>
                    <td class="text1" height="30">Cnote Name</td>
                    <td class="text1" height="30"><%=cnoteName%>
                    <input type="hidden" name="cnote" value="<%=cnoteName%>"/>
                    </td>
                </tr>
                <tr>
                    <td class="text2" height="30">Vehicle Type</td>
                    <td class="text2" height="30"><%=vehicleTypeName%>
                        <input type="hidden" name="vehicletype" value="<%=vehicleTypeName%>"/>
                    </td>
                </tr>
                <tr>
                    <td class="text1" height="30">Vehicle No</td>
                    <td class="text1" height="30"><%=vegno%>
                    <input type="hidden" name="vegno" value="<%=vegno%>"/>
                    </td>
                </tr>
                <tr>
                    <td class="text2" height="30">Route Name</td>
                    <td class="text2" height="30"><%=routeName%>
                    <input type="hidden" name="routename" value="<%=routeName%>"/>
                    </td>
                </tr>
                <tr>
                    <td class="text1" height="30">Driver Name</td>
                    <td class="text1" height="30"><%=driverName%>
                        <input type="hidden" name="drivername" value="<%=driverName%>"/>
                    </td>
                </tr>
                <tr>
                    <td class="text2" height="30">Planned Date</td>
                    <td class="text2" height="30"><%=planneddate%>
                        <input type="hidden" name="planneddate" value="<%=planneddate%>"/>
                    </td>
                </tr>
                <tr>
                    <td class="text1" height="30">Actual Advance Paid</td>
                    <td class="text1" height="30"><%=actualadvancepaid%>
                        <input type="hidden" name="actadvancepaid" value="<%=actualadvancepaid%>"/>
                    </td>
                </tr>
                
                <tr>
                    <td class="text2" height="30">Estimated Expense</td>
                    <td class="text2" height="30"><%=estimatedexpense%>
                        <input type="hidden" name="estimatedexpense" value="<%=estimatedexpense%>"/>
                    </td>
                </tr>

                <tr>
                    <td class="text1" height="30">To Be Paid</td>
                    <td class="text1" height="30"><%=estimatedadvance%>
                        <input type="hidden" name="estimatedadvance" value="<%=estimatedadvance%>"/>
                    </td>
                </tr>
                <tr>
                    <td class="text2" height="30">Trip Day</td>
                    <td class="text2" height="30"><%=tripday%>
                       <input type="hidden" name="tripday" value="<%=tripday%>"/>
                    </td>
                </tr>
                <tr>
                    <td class="text1" height="30"><font color="red">*</font>Req.Advance Amt.</td>
                    <td class="text1" height="30"><input name="advancerequestamt" type="text" class="form-control" value=""></td>
                </tr>

                <input type="hidden" name="requeststatus" value="0"/>
                <input type="hidden" name="tripid" value="<%=tripid%>"/>
                <input type="hidden" name="batchType" value="<%=type%>"/>
                <input type="hidden" name="advicedate" value="<%=advicedate%>"/>
                <input name="tobepaidtoday" type="hidden" class="form-control" value="<%=estimatedadvance%>" readonly>
                <tr>
                    <td class="text2" height="30"><font color="red">*</font>Request On</td>
                    <td class="text2" height="30">
                        <input name="requeston" type="text" class="datepicker" value="">
                    </td>
                </tr>

               
                <tr>
                    <td class="text1" height="30"><font color="red">*</font>Request Remarks</td>
                    <td class="text1" height="30"><textarea class="form-control" name="requestremarks"></textarea></td>
                </tr>
                
            </table>
            <br>
            <center>
                <input type="button" value="save" class="button" onClick="submitPage();">
                &emsp;
<!--                <input type="reset" class="button" value="Clear">-->
            </center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
