
<%@ include file="/content/common/NewDesign/header.jsp" %>
   <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>

<%@ page import="java.util.*" %>

<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
            setValues();
            getVehicleNos();">
    </c:if>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>PAPL</title>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<div class="pageheader">
      <h2><i class="fa fa-edit"></i><spring:message code="hrms.label.AddGrade" text="default text"/></h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="default text"/>:</span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.HRMS" text="default text"/></a></li>
          <li class=""><spring:message code="hrms.label.Designation" text="default text"/></li>
          <li class=""><spring:message code="hrms.label.Grade" text="default text"/></li>
          <li class=""><spring:message code="hrms.label.AddGrade" text="default text"/></li>
        </ol>
      </div>
      </div>
<div class="contentpanel">
            <div class="panel panel-default">
             <div class="panel-body">
<body>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script language="javascript">
function validate(value){
if(isEmpty(document.addGrade.gradeName.value)){
alert("Grade Name Field Should Not Be Empty");
document.addGrade.gradeName.focus();
}
else if(isEmpty(document.addGrade.description.value)){
alert("Description Should Not Be Empty");
document.addGrade.description.focus();
}
else if(isSpecialCharacter(document.addGrade.gradeName.value)){
alert("Special Characters are not Allowed");
document.addGrade.gradeName.focus();
}
else if(value == 'add'){
document.addGrade.gradeName.value = trim(document.addGrade.gradeName.value);
document.addGrade.description.value = trim(document.addGrade.description.value);
document.addGrade.action = '/throttle/AddGrade.do';
document.addGrade.submit();
}
}


</script>

<!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/ 
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->

<form name="addGrade"  method="post">


<%@ include file="/content/common/message.jsp" %>

  <table class="table table-info mb30 table-hover" >
      <thead>
<tr>
<th  height="30"><div ><spring:message code="hrms.label.DesignationName" text="default text"/></div></th>
<th  height="30"><div ><%= request.getAttribute("desigName") %></div></th>
<input type="hidden" name="desigName" value="<%= request.getAttribute("desigName") %>" >
</tr>
</thead>
<tr >
<td  height="30"><font color=red>*</font><spring:message code="hrms.label.GradeName" text="default text"/></td>
<td  height="30"><input type="text" name="gradeName" class="form-control" style="width:220px;height:40px;"></td>
</tr>
<tr >
<td  height="30"><font color=red>*</font><spring:message code="hrms.label.GradeDescription" text="default text"/></td>
<td  height="30"><textarea  name="description" class="form-control" style="width:220px;height:40px;"></textarea></td>
</tr>

<input type="hidden" name="designationId" value='<%= request.getAttribute("DesigId") %>'>
</table>

<center>
<br>
<input type="button" value="<spring:message code="hrms.label.ADD" text="default text"/>" name="add" onClick="validate(this.name)" class="btn btn-success">
&emsp;<input type="reset" class="btn btn-success" value="<spring:message code="hrms.label.CLEAR" text="default text"/>">
</center>
<br>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
