<%-- 
    Document   : Product
    Created on : Jul 27, 2014, 5:14:32 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<table id="table" class="sortable" cellpadding="0" cellspacing="0">
    <caption>Product Details</caption>
    <tr></tr>
    <tr>
        <th>Sno</th>
        <th>Product Code</th>
        <th>Product Name</th>
        <th>Supplier</th>
        <th>Action</th>
    </tr>
    <tr class="text2">
        <td align="center">1</td>
        <td>BSF/ICTK1</td>
        <td>HIV (Rapid) Kits – 1st Antigen</td>
        <td>Hetero Labs Limited </td>
        <td>Edit | Delete</td>
    </tr>
    <tr class="text1">
        <td align="center">2</td>
        <td>BSF/HBVE1</td>
        <td>HBV (Elisa) Kits</td>
        <td>Hetero Labs Limited </td>
        <td>Edit | Delete</td>
    </tr>
    <tr class="text2">
        <td align="center">3</td>
        <td>BSF/ICTK2</td>
        <td>HIV (Rapid) Kits – 2nd Antigen</td>
        <td>Hetero Labs Limited, Ranbaxy</td>
        <td>Edit | Delete</td>
    </tr>
    <tr class="text1">
        <td align="center">4</td>
        <td>BSF/ICTK3</td>
        <td>HIV (Rapid) Kits – 3rd Antigen</td>
        <td>Hetero Labs Limited, Ranbaxy</td>
        <td>Edit | Delete</td>
    </tr>
    <tr class="text2">
        <td align="center">5</td>
        <td>BSF/WBFP</td>
        <td>Whole Blood Finger Prick Test Kits</td>
        <td>Shriram</td>
        <td>Edit | Delete</td>
    </tr>
     <tr class="text2">
        <td align="center">6</td>
        <td>BSF/HBVR1</td>
        <td>HBV (Rapid) Kits</td>
        <td>Hetero Labs Limited, Ranbaxy</td>
        <td>Edit | Delete</td>
    </tr>
    <tr class="text1">
        <td align="center">7</td>
        <td>BSF/HCVE1</td>
        <td>HCV (Elisa) Kits</td>
        <td>Hetero Labs Limited, Shriram</td>
        <td>Edit | Delete</td>
    </tr>
    <tr class="text2">
        <td align="center">8</td>
        <td>BSF/HCVR1</td>
        <td>HCV (Rapid) Kits</td>
        <td>Hetero Labs Limited, Ranbaxy</td>
        <td>Edit | Delete</td>
    </tr>
    <tr class="text1">
        <td align="center">9</td>
        <td>ARV/ATZ</td>
        <td>Atazanavir 300mg capsules</td>
        <td>Shriram, Ranbaxy</td>
        <td>Edit | Delete</td>
    </tr>
    <tr class="text2">
        <td align="center">10</td>
        <td>ARV/EFV2</td>
        <td>Efavirenz 200 mg tablet</td>
        <td>Hetero Labs Limited, Ranbaxy</td>
        <td>Edit | Delete</td>
    </tr>



</table>

<script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="../images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="../images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="../images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="../images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page 1 of 5</div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>