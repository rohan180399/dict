/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.secondaryOperation.business;

import java.io.Serializable;

/**
 * extraExpense
 *
 * @author Arul
 */
public class SecondaryOperationTO implements Serializable {

     private String companyId = "";
     private String loadType = "";
     private String distanceContractId = "";
     private String toDistance = "";
     private String fromDistance = "";
     private String containerQty = "";
     private String containerTypeName = "";
     private String referenceName = "";
     private String customerDetails = "";
     private String quantity = "";
     private String ratePerKm = "";
     private String tariffType = "";
     private String conditions = "";
    private String userName = "";
    private String quotationDetailsId = "";
    private String quotationId = "";
    private String validity = "";
    private String quotationStatus = "";
    private String salesManager = "";
    private String quotationType = "";
    private String quotationNo = "";
    private String quotationDate = "";
    private String scheduleId = "";
    private String jan = "";
    private String feb = "";
    private String mar = "";
    private String apr = "";
    private String may = "";
    private String jun = "";
    private String jul = "";
    private String aug = "";
    private String sep = "";
    private String oct = "";
    private String nov = "";
    private String dec = "";
    private String startReading = "";
    private String vehicleRegNo = "";
    private String trailerId = "";
    private String agreedDate = "";
    private String mfrId = "";
    private String modelId = "";
    private String mfr = "";
    private String model = "";
    private String modelName = "";
    private String trailerNo = "";
    private String trailerRemarks = "";
    private String[] selectedIndex = null;
    private String existingTrailerUnit = "";
    private String existingVehicleUnit = "";
    private String trailerContractId = "";
    private String vehicleContractId = "";
    private String costPerTrailerPerMonth = "";
    private String trailerTypeName = "";
    private String typeName = "";
    private String trailerTypeId = "";
    private String trailerUnit = "";
    private String extraRunKmCost = "";
    private String driverResponsibility = "";
    private String driverResponsibilityName = "";
    private String fuelResponsibilityName = "";
    private String billingKmCalculationId = "";
    private String billingKmCalculationName = "";
    private String vehicleUnit = "";
    private String contractType = "";
    private String contractTypeName = "";
    private String costPerVehiclePerMonth = "";
    private String totalCost = "";
    private String maxAllowedKm = "";
    private String fuelResponsibility = "";
    private String axleName = "";
    private String singleJourney = "";
    private String returnJourney = "";
    private String monthlyPass = "";
    private String localVehicleCost = "";
    private String googleCityName = "";
    private String tollRateId = "";

    private String freezeInDate = "";
    private String freezeInTime = "";
    private String freezeOutDate = "";
    private String freezeOutTime = "";
    private String incidentId = "";
    private String incidentName = "";

    private String tripSheetId = "";
    private String tripType = "";
    private String vehicleNo = "";
    private String time = "";
    private String currentLocation = "";
    private String remarks = "";
    private String vendorName = "";
    private String routeCostId = "";
    private String slabId = "";
    private String slabName = "";
    private String vendorId = "";
    private String slabRate = "";
    private String slabRateId = "";

    private String settledDate = "";
    private String name = "";
    private String seccompId = "";
    private String approvestatus = "";
    private String totalReferMin = "";
    private String fuelCostPerMin = "";
    private String companyName = "";
    private String parkingCost = "";
    private String preCollingCost = "";
    private String additionalTollCost = "";
    private String driverName = "";
    private String empId = "";
    private String empName = "";
    private String tripEndDate = "";
    private String tripStartDate = "";
    private String totalAmount = "";
    private String billYear = "";
    private String billedMonth = "";
    private String advancePaid = "";
    private String rcmCost = "";
    private String tripStartKm = "";
    private String tripEndKm = "";
    private String vehicleCount = "";
    private String invoiceAmount = "";
    private String extraKmCalculation = "";
    private String contractCngCost = "";
    private String contractDieselCost = "";
    private String rateChangeOfCng = "";
    private String rateChangeOfDiesel = "";
    private String fuelTypeId = "";
    private String vehicleTonnage = "";
    private String activeStatus = "";
    private String vehicleMileage = "";
    private String reeferMileage = "";
    private String fuelTypeName = "";
    private String averageKM = "";
    private String humanId = "";
    // private String pointId = "";
    private String contractId = "";
    private String preCoolingHr = "";
    private String loadingHr = "";
    private String id = "";
    private String cfAmount = "";
    private String payAmount = "";
    private String tripSettlementId = "";
    private String settlementDate = "";
    private String tripBalance = "";
    private String totalRcmAllocation = "";
    private String totalBpclAllocation = "";
    private String totalExtraExpense = "";
    private String totalMiscellaneousExpense = "";
    private String totalDriverBhatta = "";
    private String totalExpenses = "";
    private String balanceAmount = "";
    private String startingBalance = "";
    private String endingBalance = "";
    private String paymentMode = "";
    private String settlementRemarks = "";
    private String extraExpense = "";
    private String reeferMilleage = "";
    private String vehicleMilleage = "";
    private String fuelConsumption = "";
    private String tollCostPerKm = "";
    private String fuelExpense = "";
    private String driverIncentivePerKm = "";
    private String driverBattaPerDay = "";
    private String miscExpense = "";
    private String systemExpense = "";
    private String totalExpense = "";
    private String totalKm = "";
    private String addlTollAmounts = "";
    private String invoiceId = "";
    private String invoiceCode = "";
    private String invoiceNo = "";
    private String invoiceDate = "";
    private String creditLimit = "";
    private String grandTotal = "";
    private String invoiceStatus = "";
    private String billType = "";
    private String secFactor = "";
    private String startTime = "";
    private String endTime = "";
    private String expense = "";
    private String advanceAmount = "";
    private String vehicleTypeId = "";
    private String milleage = "";
    private String reeferConsumption = "";
    private String tollAmount = "";
    private String driverBatta = "";
    private String driverIncentive = "";
    private String fuelPrice = "";
    private String consignmentNo = "";
    private String billingType = "";
    private String customerType = "";
    private String routeInfo = "";
    private String reeferRequired = "";
    private String vehicleType = "";
    private String estimatedRevenue = "";
    private String rcmExpense = "0";
    private String statusId = "";
    private String statusName = "";
    private String regNo = "";
    private String vehicleId = "";
    private String originId = "";
    private String destinationId = "";
    private String tripCode = "";
    private String tripId = "";
    private String startDate = "";
    private String endDate = "";
    private String totalKmRun = "";
    private String totalReeferRun = "";
    private String totalDays = "";
    private String totalWeight = "";
    private String primaryDriverId = "";
    private String primaryDriverName = "";
    private String pointAddress = "";
    private String status = "";
    private String latitudePosition = "";
    private String longitudePosition = "";
    private String destination = "";
    private String routeId = "";
    private String fixedKm = "";
    private String fixedKmCharge = "";
    private String routeContractId = "";
    private String customerTypeId = "0";
    private String entryType = "";
    private String consignmentNoteNo = "";
    private String consignmentDate = "";
    private String orderReferenceNo = "";
    private String orderReferenceRemarks = "";
    private String productCategoryId = "0";
    private String customerAddress = "";
    private String pincode = "";
    private String customerMobileNo = "";
    private String mailId = "";
    private String customerPhoneNo = "";
    private String origin = "";
    private String businessType = "0";
    private String multiPickup = "";
    private String multiDelivery = "";
    private String consignmentOrderInstruction = "";
    private String serviceType = "0";
    private String contractRateId = "0";
    private String noOfVehicles = "";
    private String vehicleRequiredDate = "";
    private String vehicleRequiredHour = "";
    private String vehicleRequiredMinute = "";
    private String vehicleInstruction = "";
    private String consignorName = "";
    private String consignorPhoneNo = "";
    private String consignorAddress = "";
    private String consigneeName = "";
    private String consigneePhoneNo = "";
    private String consigneeAddress = "";
    private String totFreightAmount = "";
    private String secondaryRouteDetailId = "";
    private String secondaryRouteCostId = "";
    private String vehicleTypeName = "";
    private String pointId = "";
    private String pointName = "";
    private String secondaryRouteId = "";
    private String secondaryRouteCode = "";
    private String secondaryRouteName = "";
    private String totalWaitHours = "";
    private String activeInd = "";
    private String totalTravelKm = "";
    private String totalTravelHour = "";
    private String totalTravelMinute = "";
    private String billingTypeName = "";
    private String phone = "";
    private String email = "";
    private String mobileNo = "";
    private String billingTypeId = "";
    private String totalWaitMinutes = "";
    private String totalReeferMinutes = "";
    private String totalReeferHours = "";
    private String scheduleStatus = "";
    private String scheduleDate = "";
    private String fromDate = "";
    private String toDate = "";
    private int secondaryTripscheduleSatus;
    private String custId = "";
    private String custName = "";
    private String custContactPerson = "";
    private String custAddress = "";
    private String custCity = "";
    private String custStatus = "";
    private String customerCode = "";
    private String date = "";
    private String dateName = "";
    private String customerName = "";
    private String nextWeek = "";
    private String[] secRouteId = null;
    private String[] scheduledate = null;
    private int userId = 0;
    private String cityId = "";
    private String cityName = "";
    private String customerId = "";
    private String routeName = "";
    private String fuelCost = "";
    private String fixedKmPerMonth = "";
    private String fixedReeferHours = "";
    private String fixedReeferMinutes = "";
    private String routeValidFrom = "";
    private String routeValidTo = "";
    private String distance = "";
    private String totalHours = "";
    private String totalMinutes = "";
    private String pointType = "";
    private String pointAddresss = "";
    private String pointSequence = "";
    private String latitude = "";
    private String longitude = "";
    private String secondaryContractId = "";
    private String hrId = "";
    private String hrName = "";
    private String noOfPersons = "";
    private String fixedAmount = "";
    private String travelKm = "";
    private String travelHour = "";
    private String travelMinute = "";
    private String reeferHour = "";
    private String reeferMinute = "";
    private String waitMinute = "";
    private String vehTypeId = "";
    private String fuelCostPerKms = "";
    private String fuelCostPerHrs = "";
    private String tollAmounts = "";
    private String miscCostKm = "";
    private String driverIncenKm = "";
    private String factor = "";
    private String varExpense = "";
    private String vehExpense = "";
    private String reeferExpense = "";
    private String totExpense = "";
    private String rateWithReefer = "";
    private String rateWithoutReefer = "";
    private String extraKmCharge = "";
    private String extraReeferHourCharge = "";
    private String noOfVehicle = "";

    private String tollName = "";
    private String tollId = "";
    private String zoneId = "";
    private String zoneName = "";
    private String countryName = "";
    private String countryId = "";
    private String currencyCode = "";
    //pavi
    private String levelID = "";
    private String ledgerId = "";
    private String levelGroupName = "";
    private String expenseId = "";
    private String groupName = "";
    private String expenseName = "";
       private String createdDate = "";
    private String description = "";
    private String alertFor = "";
      private String dept = "";
     private String contactPerson = "";

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAlertFor() {
        return alertFor;
    }

    public void setAlertFor(String alertFor) {
        this.alertFor = alertFor;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getLevelID() {
        return levelID;
    }

    public void setLevelID(String levelID) {
        this.levelID = levelID;
    }

    public String getLedgerId() {
        return ledgerId;
    }

    public void setLedgerId(String ledgerId) {
        this.ledgerId = ledgerId;
    }

    public String getLevelGroupName() {
        return levelGroupName;
    }

    public void setLevelGroupName(String levelGroupName) {
        this.levelGroupName = levelGroupName;
    }

    public String getExpenseId() {
        return expenseId;
    }

    public void setExpenseId(String expenseId) {
        this.expenseId = expenseId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getExpenseName() {
        return expenseName;
    }

    public void setExpenseName(String expenseName) {
        this.expenseName = expenseName;
    }
    

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getTollName() {
        return tollName;
    }

    public void setTollName(String tollName) {
        this.tollName = tollName;
    }

    public String getTollId() {
        return tollId;
    }

    public void setTollId(String tollId) {
        this.tollId = tollId;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getFuelCost() {
        return fuelCost;
    }

    public void setFuelCost(String fuelCost) {
        this.fuelCost = fuelCost;
    }

    public String getFixedKmPerMonth() {
        return fixedKmPerMonth;
    }

    public void setFixedKmPerMonth(String fixedKmPerMonth) {
        this.fixedKmPerMonth = fixedKmPerMonth;
    }

    public String getFixedReeferHours() {
        return fixedReeferHours;
    }

    public void setFixedReeferHours(String fixedReeferHours) {
        this.fixedReeferHours = fixedReeferHours;
    }

    public String getFixedReeferMinutes() {
        return fixedReeferMinutes;
    }

    public void setFixedReeferMinutes(String fixedReeferMinutes) {
        this.fixedReeferMinutes = fixedReeferMinutes;
    }

    public String getRouteValidFrom() {
        return routeValidFrom;
    }

    public void setRouteValidFrom(String routeValidFrom) {
        this.routeValidFrom = routeValidFrom;
    }

    public String getRouteValidTo() {
        return routeValidTo;
    }

    public void setRouteValidTo(String routeValidTo) {
        this.routeValidTo = routeValidTo;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(String totalHours) {
        this.totalHours = totalHours;
    }

    public String getTotalMinutes() {
        return totalMinutes;
    }

    public void setTotalMinutes(String totalMinutes) {
        this.totalMinutes = totalMinutes;
    }

    public String getTotalReeferHours() {
        return totalReeferHours;
    }

    public void setTotalReeferHours(String totalReeferHours) {
        this.totalReeferHours = totalReeferHours;
    }

    public String getTotalReeferMinutes() {
        return totalReeferMinutes;
    }

    public void setTotalReeferMinutes(String totalReeferMinutes) {
        this.totalReeferMinutes = totalReeferMinutes;
    }

    public String getTotalWaitMinutes() {
        return totalWaitMinutes;
    }

    public void setTotalWaitMinutes(String totalWaitMinutes) {
        this.totalWaitMinutes = totalWaitMinutes;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getPointType() {
        return pointType;
    }

    public void setPointType(String pointType) {
        this.pointType = pointType;
    }

    public String getPointAddresss() {
        return pointAddresss;
    }

    public void setPointAddresss(String pointAddresss) {
        this.pointAddresss = pointAddresss;
    }

    public String getPointSequence() {
        return pointSequence;
    }

    public void setPointSequence(String pointSequence) {
        this.pointSequence = pointSequence;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getTravelKm() {
        return travelKm;
    }

    public void setTravelKm(String travelKm) {
        this.travelKm = travelKm;
    }

    public String getTravelHour() {
        return travelHour;
    }

    public void setTravelHour(String travelHour) {
        this.travelHour = travelHour;
    }

    public String getTravelMinute() {
        return travelMinute;
    }

    public void setTravelMinute(String travelMinute) {
        this.travelMinute = travelMinute;
    }

    public String getReeferHour() {
        return reeferHour;
    }

    public void setReeferHour(String reeferHour) {
        this.reeferHour = reeferHour;
    }

    public String getReeferMinute() {
        return reeferMinute;
    }

    public void setReeferMinute(String reeferMinute) {
        this.reeferMinute = reeferMinute;
    }

    public String getWaitMinute() {
        return waitMinute;
    }

    public void setWaitMinute(String waitMinute) {
        this.waitMinute = waitMinute;
    }

    public String getVehTypeId() {
        return vehTypeId;
    }

    public void setVehTypeId(String vehTypeId) {
        this.vehTypeId = vehTypeId;
    }

    public String getFuelCostPerKms() {
        return fuelCostPerKms;
    }

    public void setFuelCostPerKms(String fuelCostPerKms) {
        this.fuelCostPerKms = fuelCostPerKms;
    }

    public String getFuelCostPerHrs() {
        return fuelCostPerHrs;
    }

    public void setFuelCostPerHrs(String fuelCostPerHrs) {
        this.fuelCostPerHrs = fuelCostPerHrs;
    }

    public String getTollAmounts() {
        return tollAmounts;
    }

    public void setTollAmounts(String tollAmounts) {
        this.tollAmounts = tollAmounts;
    }

    public String getMiscCostKm() {
        return miscCostKm;
    }

    public void setMiscCostKm(String miscCostKm) {
        this.miscCostKm = miscCostKm;
    }

    public String getDriverIncenKm() {
        return driverIncenKm;
    }

    public void setDriverIncenKm(String driverIncenKm) {
        this.driverIncenKm = driverIncenKm;
    }

    public String getFactor() {
        return factor;
    }

    public void setFactor(String factor) {
        this.factor = factor;
    }

    public String getVarExpense() {
        return varExpense;
    }

    public void setVarExpense(String varExpense) {
        this.varExpense = varExpense;
    }

    public String getVehExpense() {
        return vehExpense;
    }

    public void setVehExpense(String vehExpense) {
        this.vehExpense = vehExpense;
    }

    public String getReeferExpense() {
        return reeferExpense;
    }

    public void setReeferExpense(String reeferExpense) {
        this.reeferExpense = reeferExpense;
    }

    public String getTotExpense() {
        return totExpense;
    }

    public void setTotExpense(String totExpense) {
        this.totExpense = totExpense;
    }

    public String getRateWithReefer() {
        return rateWithReefer;
    }

    public void setRateWithReefer(String rateWithReefer) {
        this.rateWithReefer = rateWithReefer;
    }

    public String getRateWithoutReefer() {
        return rateWithoutReefer;
    }

    public void setRateWithoutReefer(String rateWithoutReefer) {
        this.rateWithoutReefer = rateWithoutReefer;
    }

    public String getExtraKmCharge() {
        return extraKmCharge;
    }

    public void setExtraKmCharge(String extraKmCharge) {
        this.extraKmCharge = extraKmCharge;
    }

    public String getExtraReeferHourCharge() {
        return extraReeferHourCharge;
    }

    public void setExtraReeferHourCharge(String extraReeferHourCharge) {
        this.extraReeferHourCharge = extraReeferHourCharge;
    }

    public String getDateName() {
        return dateName;
    }

    public void setDateName(String dateName) {
        this.dateName = dateName;
    }

    public String getNextWeek() {
        return nextWeek;
    }

    public void setNextWeek(String nextWeek) {
        this.nextWeek = nextWeek;
    }

    public String[] getSecRouteId() {
        return secRouteId;
    }

    public void setSecRouteId(String[] secRouteId) {
        this.secRouteId = secRouteId;
    }

    public String[] getScheduledate() {
        return scheduledate;
    }

    public void setScheduledate(String[] scheduledate) {
        this.scheduledate = scheduledate;
    }

    public String getSecondaryRouteId() {
        return secondaryRouteId;
    }

    public void setSecondaryRouteId(String secondaryRouteId) {
        this.secondaryRouteId = secondaryRouteId;
    }

    public String getSecondaryRouteCode() {
        return secondaryRouteCode;
    }

    public void setSecondaryRouteCode(String secondaryRouteCode) {
        this.secondaryRouteCode = secondaryRouteCode;
    }

    public String getSecondaryRouteName() {
        return secondaryRouteName;
    }

    public void setSecondaryRouteName(String secondaryRouteName) {
        this.secondaryRouteName = secondaryRouteName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getSecondaryTripscheduleSatus() {
        return secondaryTripscheduleSatus;
    }

    public void setSecondaryTripscheduleSatus(int secondaryTripscheduleSatus) {
        this.secondaryTripscheduleSatus = secondaryTripscheduleSatus;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCustContactPerson() {
        return custContactPerson;
    }

    public void setCustContactPerson(String custContactPerson) {
        this.custContactPerson = custContactPerson;
    }

    public String getCustAddress() {
        return custAddress;
    }

    public void setCustAddress(String custAddress) {
        this.custAddress = custAddress;
    }

    public String getCustCity() {
        return custCity;
    }

    public void setCustCity(String custCity) {
        this.custCity = custCity;
    }

    public String getCustStatus() {
        return custStatus;
    }

    public void setCustStatus(String custStatus) {
        this.custStatus = custStatus;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getScheduleStatus() {
        return scheduleStatus;
    }

    public void setScheduleStatus(String scheduleStatus) {
        this.scheduleStatus = scheduleStatus;
    }

    public String getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(String scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public String getTotalTravelKm() {
        return totalTravelKm;
    }

    public void setTotalTravelKm(String totalTravelKm) {
        this.totalTravelKm = totalTravelKm;
    }

    public String getTotalTravelHour() {
        return totalTravelHour;
    }

    public void setTotalTravelHour(String totalTravelHour) {
        this.totalTravelHour = totalTravelHour;
    }

    public String getTotalWaitHours() {
        return totalWaitHours;
    }

    public void setTotalWaitHours(String totalWaitHours) {
        this.totalWaitHours = totalWaitHours;
    }

    public String getTotalTravelMinute() {
        return totalTravelMinute;
    }

    public void setTotalTravelMinute(String totalTravelMinute) {
        this.totalTravelMinute = totalTravelMinute;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getPointId() {
        return pointId;
    }

    public void setPointId(String pointId) {
        this.pointId = pointId;
    }

    public String getPointName() {
        return pointName;
    }

    public void setPointName(String pointName) {
        this.pointName = pointName;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    public String getBillingTypeId() {
        return billingTypeId;
    }

    public void setBillingTypeId(String billingTypeId) {
        this.billingTypeId = billingTypeId;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getBillingTypeName() {
        return billingTypeName;
    }

    public void setBillingTypeName(String billingTypeName) {
        this.billingTypeName = billingTypeName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSecondaryRouteDetailId() {
        return secondaryRouteDetailId;
    }

    public void setSecondaryRouteDetailId(String secondaryRouteDetailId) {
        this.secondaryRouteDetailId = secondaryRouteDetailId;
    }

    public String getSecondaryRouteCostId() {
        return secondaryRouteCostId;
    }

    public void setSecondaryRouteCostId(String secondaryRouteCostId) {
        this.secondaryRouteCostId = secondaryRouteCostId;
    }

    public String getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(String customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    public String getEntryType() {
        return entryType;
    }

    public void setEntryType(String entryType) {
        this.entryType = entryType;
    }

    public String getConsignmentNoteNo() {
        return consignmentNoteNo;
    }

    public void setConsignmentNoteNo(String consignmentNoteNo) {
        this.consignmentNoteNo = consignmentNoteNo;
    }

    public String getConsignmentDate() {
        return consignmentDate;
    }

    public void setConsignmentDate(String consignmentDate) {
        this.consignmentDate = consignmentDate;
    }

    public String getOrderReferenceNo() {
        return orderReferenceNo;
    }

    public void setOrderReferenceNo(String orderReferenceNo) {
        this.orderReferenceNo = orderReferenceNo;
    }

    public String getOrderReferenceRemarks() {
        return orderReferenceRemarks;
    }

    public void setOrderReferenceRemarks(String orderReferenceRemarks) {
        this.orderReferenceRemarks = orderReferenceRemarks;
    }

    public String getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerMobileNo() {
        return customerMobileNo;
    }

    public void setCustomerMobileNo(String customerMobileNo) {
        this.customerMobileNo = customerMobileNo;
    }

    public String getMailId() {
        return mailId;
    }

    public void setMailId(String mailId) {
        this.mailId = mailId;
    }

    public String getCustomerPhoneNo() {
        return customerPhoneNo;
    }

    public void setCustomerPhoneNo(String customerPhoneNo) {
        this.customerPhoneNo = customerPhoneNo;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getMultiPickup() {
        return multiPickup;
    }

    public void setMultiPickup(String multiPickup) {
        this.multiPickup = multiPickup;
    }

    public String getMultiDelivery() {
        return multiDelivery;
    }

    public void setMultiDelivery(String multiDelivery) {
        this.multiDelivery = multiDelivery;
    }

    public String getConsignmentOrderInstruction() {
        return consignmentOrderInstruction;
    }

    public void setConsignmentOrderInstruction(String consignmentOrderInstruction) {
        this.consignmentOrderInstruction = consignmentOrderInstruction;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getReeferRequired() {
        return reeferRequired;
    }

    public void setReeferRequired(String reeferRequired) {
        this.reeferRequired = reeferRequired;
    }

    public String getContractRateId() {
        return contractRateId;
    }

    public void setContractRateId(String contractRateId) {
        this.contractRateId = contractRateId;
    }

    public String getVehicleRequiredDate() {
        return vehicleRequiredDate;
    }

    public void setVehicleRequiredDate(String vehicleRequiredDate) {
        this.vehicleRequiredDate = vehicleRequiredDate;
    }

    public String getVehicleRequiredHour() {
        return vehicleRequiredHour;
    }

    public void setVehicleRequiredHour(String vehicleRequiredHour) {
        this.vehicleRequiredHour = vehicleRequiredHour;
    }

    public String getVehicleRequiredMinute() {
        return vehicleRequiredMinute;
    }

    public void setVehicleRequiredMinute(String vehicleRequiredMinute) {
        this.vehicleRequiredMinute = vehicleRequiredMinute;
    }

    public String getVehicleInstruction() {
        return vehicleInstruction;
    }

    public void setVehicleInstruction(String vehicleInstruction) {
        this.vehicleInstruction = vehicleInstruction;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public void setConsignorName(String consignorName) {
        this.consignorName = consignorName;
    }

    public String getConsignorPhoneNo() {
        return consignorPhoneNo;
    }

    public void setConsignorPhoneNo(String consignorPhoneNo) {
        this.consignorPhoneNo = consignorPhoneNo;
    }

    public String getConsignorAddress() {
        return consignorAddress;
    }

    public void setConsignorAddress(String consignorAddress) {
        this.consignorAddress = consignorAddress;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getConsigneePhoneNo() {
        return consigneePhoneNo;
    }

    public void setConsigneePhoneNo(String consigneePhoneNo) {
        this.consigneePhoneNo = consigneePhoneNo;
    }

    public String getConsigneeAddress() {
        return consigneeAddress;
    }

    public void setConsigneeAddress(String consigneeAddress) {
        this.consigneeAddress = consigneeAddress;
    }

    public String getTotFreightAmount() {
        return totFreightAmount;
    }

    public void setTotFreightAmount(String totFreightAmount) {
        this.totFreightAmount = totFreightAmount;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getTotalKm() {
        return totalKm;
    }

    public void setTotalKm(String totalKm) {
        this.totalKm = totalKm;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getRouteContractId() {
        return routeContractId;
    }

    public void setRouteContractId(String routeContractId) {
        this.routeContractId = routeContractId;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getFixedKm() {
        return fixedKm;
    }

    public void setFixedKm(String fixedKm) {
        this.fixedKm = fixedKm;
    }

    public String getFixedKmCharge() {
        return fixedKmCharge;
    }

    public void setFixedKmCharge(String fixedKmCharge) {
        this.fixedKmCharge = fixedKmCharge;
    }

    public String getNoOfVehicle() {
        return noOfVehicle;
    }

    public void setNoOfVehicle(String noOfVehicle) {
        this.noOfVehicle = noOfVehicle;
    }

    public String getLatitudePosition() {
        return latitudePosition;
    }

    public void setLatitudePosition(String latitudePosition) {
        this.latitudePosition = latitudePosition;
    }

    public String getLongitudePosition() {
        return longitudePosition;
    }

    public void setLongitudePosition(String longitudePosition) {
        this.longitudePosition = longitudePosition;
    }

    public String getPointAddress() {
        return pointAddress;
    }

    public void setPointAddress(String pointAddress) {
        this.pointAddress = pointAddress;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAdvanceAmount() {
        return advanceAmount;
    }

    public void setAdvanceAmount(String advanceAmount) {
        this.advanceAmount = advanceAmount;
    }

    public String getBillingType() {
        return billingType;
    }

    public void setBillingType(String billingType) {
        this.billingType = billingType;
    }

    public String getConsignmentNo() {
        return consignmentNo;
    }

    public void setConsignmentNo(String consignmentNo) {
        this.consignmentNo = consignmentNo;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(String destinationId) {
        this.destinationId = destinationId;
    }

    public String getDriverBatta() {
        return driverBatta;
    }

    public void setDriverBatta(String driverBatta) {
        this.driverBatta = driverBatta;
    }

    public String getDriverIncentive() {
        return driverIncentive;
    }

    public void setDriverIncentive(String driverIncentive) {
        this.driverIncentive = driverIncentive;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getEstimatedRevenue() {
        return estimatedRevenue;
    }

    public void setEstimatedRevenue(String estimatedRevenue) {
        this.estimatedRevenue = estimatedRevenue;
    }

    public String getExpense() {
        return expense;
    }

    public void setExpense(String expense) {
        this.expense = expense;
    }

    public String getFuelPrice() {
        return fuelPrice;
    }

    public void setFuelPrice(String fuelPrice) {
        this.fuelPrice = fuelPrice;
    }

    public String getMilleage() {
        return milleage;
    }

    public void setMilleage(String milleage) {
        this.milleage = milleage;
    }

    public String getOriginId() {
        return originId;
    }

    public void setOriginId(String originId) {
        this.originId = originId;
    }

    public String getPrimaryDriverId() {
        return primaryDriverId;
    }

    public void setPrimaryDriverId(String primaryDriverId) {
        this.primaryDriverId = primaryDriverId;
    }

    public String getPrimaryDriverName() {
        return primaryDriverName;
    }

    public void setPrimaryDriverName(String primaryDriverName) {
        this.primaryDriverName = primaryDriverName;
    }

    public String getRcmExpense() {
        return rcmExpense;
    }

    public void setRcmExpense(String rcmExpense) {
        this.rcmExpense = rcmExpense;
    }

    public String getReeferConsumption() {
        return reeferConsumption;
    }

    public void setReeferConsumption(String reeferConsumption) {
        this.reeferConsumption = reeferConsumption;
    }

    public String getRouteInfo() {
        return routeInfo;
    }

    public void setRouteInfo(String routeInfo) {
        this.routeInfo = routeInfo;
    }

    public String getSecFactor() {
        return secFactor;
    }

    public void setSecFactor(String secFactor) {
        this.secFactor = secFactor;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getTollAmount() {
        return tollAmount;
    }

    public void setTollAmount(String tollAmount) {
        this.tollAmount = tollAmount;
    }

    public String getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(String totalDays) {
        this.totalDays = totalDays;
    }

    public String getTotalKmRun() {
        return totalKmRun;
    }

    public void setTotalKmRun(String totalKmRun) {
        this.totalKmRun = totalKmRun;
    }

    public String getTotalReeferRun() {
        return totalReeferRun;
    }

    public void setTotalReeferRun(String totalReeferRun) {
        this.totalReeferRun = totalReeferRun;
    }

    public String getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(String totalWeight) {
        this.totalWeight = totalWeight;
    }

    public String getTripCode() {
        return tripCode;
    }

    public void setTripCode(String tripCode) {
        this.tripCode = tripCode;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getNoOfVehicles() {
        return noOfVehicles;
    }

    public void setNoOfVehicles(String noOfVehicles) {
        this.noOfVehicles = noOfVehicles;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public String getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(String creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    public String getFixedAmount() {
        return fixedAmount;
    }

    public void setFixedAmount(String fixedAmount) {
        this.fixedAmount = fixedAmount;
    }

    public String getHrId() {
        return hrId;
    }

    public void setHrId(String hrId) {
        this.hrId = hrId;
    }

    public String getHrName() {
        return hrName;
    }

    public void setHrName(String hrName) {
        this.hrName = hrName;
    }

    public String getNoOfPersons() {
        return noOfPersons;
    }

    public void setNoOfPersons(String noOfPersons) {
        this.noOfPersons = noOfPersons;
    }

    public String getSecondaryContractId() {
        return secondaryContractId;
    }

    public void setSecondaryContractId(String secondaryContractId) {
        this.secondaryContractId = secondaryContractId;
    }

    public String getAddlTollAmounts() {
        return addlTollAmounts;
    }

    public void setAddlTollAmounts(String addlTollAmounts) {
        this.addlTollAmounts = addlTollAmounts;
    }

    public String getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(String balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getDriverBattaPerDay() {
        return driverBattaPerDay;
    }

    public void setDriverBattaPerDay(String driverBattaPerDay) {
        this.driverBattaPerDay = driverBattaPerDay;
    }

    public String getDriverIncentivePerKm() {
        return driverIncentivePerKm;
    }

    public void setDriverIncentivePerKm(String driverIncentivePerKm) {
        this.driverIncentivePerKm = driverIncentivePerKm;
    }

    public String getEndingBalance() {
        return endingBalance;
    }

    public void setEndingBalance(String endingBalance) {
        this.endingBalance = endingBalance;
    }

    public String getExtraExpense() {
        return extraExpense;
    }

    public void setExtraExpense(String extraExpense) {
        this.extraExpense = extraExpense;
    }

    public String getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(String fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public String getFuelExpense() {
        return fuelExpense;
    }

    public void setFuelExpense(String fuelExpense) {
        this.fuelExpense = fuelExpense;
    }

    public String getMiscExpense() {
        return miscExpense;
    }

    public void setMiscExpense(String miscExpense) {
        this.miscExpense = miscExpense;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getReeferMilleage() {
        return reeferMilleage;
    }

    public void setReeferMilleage(String reeferMilleage) {
        this.reeferMilleage = reeferMilleage;
    }

    public String getSettlementRemarks() {
        return settlementRemarks;
    }

    public void setSettlementRemarks(String settlementRemarks) {
        this.settlementRemarks = settlementRemarks;
    }

    public String getStartingBalance() {
        return startingBalance;
    }

    public void setStartingBalance(String startingBalance) {
        this.startingBalance = startingBalance;
    }

    public String getSystemExpense() {
        return systemExpense;
    }

    public void setSystemExpense(String systemExpense) {
        this.systemExpense = systemExpense;
    }

    public String getTollCostPerKm() {
        return tollCostPerKm;
    }

    public void setTollCostPerKm(String tollCostPerKm) {
        this.tollCostPerKm = tollCostPerKm;
    }

    public String getTotalBpclAllocation() {
        return totalBpclAllocation;
    }

    public void setTotalBpclAllocation(String totalBpclAllocation) {
        this.totalBpclAllocation = totalBpclAllocation;
    }

    public String getTotalDriverBhatta() {
        return totalDriverBhatta;
    }

    public void setTotalDriverBhatta(String totalDriverBhatta) {
        this.totalDriverBhatta = totalDriverBhatta;
    }

    public String getTotalExpense() {
        return totalExpense;
    }

    public void setTotalExpense(String totalExpense) {
        this.totalExpense = totalExpense;
    }

    public String getTotalExpenses() {
        return totalExpenses;
    }

    public void setTotalExpenses(String totalExpenses) {
        this.totalExpenses = totalExpenses;
    }

    public String getTotalExtraExpense() {
        return totalExtraExpense;
    }

    public void setTotalExtraExpense(String totalExtraExpense) {
        this.totalExtraExpense = totalExtraExpense;
    }

    public String getTotalMiscellaneousExpense() {
        return totalMiscellaneousExpense;
    }

    public void setTotalMiscellaneousExpense(String totalMiscellaneousExpense) {
        this.totalMiscellaneousExpense = totalMiscellaneousExpense;
    }

    public String getTotalRcmAllocation() {
        return totalRcmAllocation;
    }

    public void setTotalRcmAllocation(String totalRcmAllocation) {
        this.totalRcmAllocation = totalRcmAllocation;
    }

    public String getVehicleMilleage() {
        return vehicleMilleage;
    }

    public void setVehicleMilleage(String vehicleMilleage) {
        this.vehicleMilleage = vehicleMilleage;
    }

    public String getCfAmount() {
        return cfAmount;
    }

    public void setCfAmount(String cfAmount) {
        this.cfAmount = cfAmount;
    }

    public String getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(String payAmount) {
        this.payAmount = payAmount;
    }

    public String getSettlementDate() {
        return settlementDate;
    }

    public void setSettlementDate(String settlementDate) {
        this.settlementDate = settlementDate;
    }

    public String getTripBalance() {
        return tripBalance;
    }

    public void setTripBalance(String tripBalance) {
        this.tripBalance = tripBalance;
    }

    public String getTripSettlementId() {
        return tripSettlementId;
    }

    public void setTripSettlementId(String tripSettlementId) {
        this.tripSettlementId = tripSettlementId;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getHumanId() {
        return humanId;
    }

    public void setHumanId(String humanId) {
        this.humanId = humanId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParkingCost() {
        return parkingCost;
    }

    public void setParkingCost(String parkingCost) {
        this.parkingCost = parkingCost;
    }

    public String getAverageKM() {
        return averageKM;
    }

    public void setAverageKM(String averageKM) {
        this.averageKM = averageKM;
    }

    public String getLoadingHr() {
        return loadingHr;
    }

    public void setLoadingHr(String loadingHr) {
        this.loadingHr = loadingHr;
    }

    public String getPreCoolingHr() {
        return preCoolingHr;
    }

    public void setPreCoolingHr(String preCoolingHr) {
        this.preCoolingHr = preCoolingHr;
    }

    public String getVehicleMileage() {
        return vehicleMileage;
    }

    public void setVehicleMileage(String vehicleMileage) {
        this.vehicleMileage = vehicleMileage;
    }

    public String getReeferMileage() {
        return reeferMileage;
    }

    public void setReeferMileage(String reeferMileage) {
        this.reeferMileage = reeferMileage;
    }

    public String getFuelTypeName() {
        return fuelTypeName;
    }

    public void setFuelTypeName(String fuelTypeName) {
        this.fuelTypeName = fuelTypeName;
    }

    public String getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(String activeStatus) {
        this.activeStatus = activeStatus;
    }

    public String getFuelTypeId() {
        return fuelTypeId;
    }

    public void setFuelTypeId(String fuelTypeId) {
        this.fuelTypeId = fuelTypeId;
    }

    public String getVehicleTonnage() {
        return vehicleTonnage;
    }

    public void setVehicleTonnage(String vehicleTonnage) {
        this.vehicleTonnage = vehicleTonnage;
    }

    public String getExtraKmCalculation() {
        return extraKmCalculation;
    }

    public void setExtraKmCalculation(String extraKmCalculation) {
        this.extraKmCalculation = extraKmCalculation;
    }

    public String getContractCngCost() {
        return contractCngCost;
    }

    public void setContractCngCost(String contractCngCost) {
        this.contractCngCost = contractCngCost;
    }

    public String getContractDieselCost() {
        return contractDieselCost;
    }

    public void setContractDieselCost(String contractDieselCost) {
        this.contractDieselCost = contractDieselCost;
    }

    public String getRateChangeOfCng() {
        return rateChangeOfCng;
    }

    public void setRateChangeOfCng(String rateChangeOfCng) {
        this.rateChangeOfCng = rateChangeOfCng;
    }

    public String getRateChangeOfDiesel() {
        return rateChangeOfDiesel;
    }

    public void setRateChangeOfDiesel(String rateChangeOfDiesel) {
        this.rateChangeOfDiesel = rateChangeOfDiesel;
    }

    public String getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(String invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public String getVehicleCount() {
        return vehicleCount;
    }

    public void setVehicleCount(String vehicleCount) {
        this.vehicleCount = vehicleCount;
    }

    public String getAdvancePaid() {
        return advancePaid;
    }

    public void setAdvancePaid(String advancePaid) {
        this.advancePaid = advancePaid;
    }

    public String getRcmCost() {
        return rcmCost;
    }

    public void setRcmCost(String rcmCost) {
        this.rcmCost = rcmCost;
    }

    public String getTripStartKm() {
        return tripStartKm;
    }

    public void setTripStartKm(String tripStartKm) {
        this.tripStartKm = tripStartKm;
    }

    public String getTripEndKm() {
        return tripEndKm;
    }

    public void setTripEndKm(String tripEndKm) {
        this.tripEndKm = tripEndKm;
    }

    public String getBilledMonth() {
        return billedMonth;
    }

    public void setBilledMonth(String billedMonth) {
        this.billedMonth = billedMonth;
    }

    public String getBillYear() {
        return billYear;
    }

    public void setBillYear(String billYear) {
        this.billYear = billYear;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTripStartDate() {
        return tripStartDate;
    }

    public void setTripStartDate(String tripStartDate) {
        this.tripStartDate = tripStartDate;
    }

    public String getTripEndDate() {
        return tripEndDate;
    }

    public void setTripEndDate(String tripEndDate) {
        this.tripEndDate = tripEndDate;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getAdditionalTollCost() {
        return additionalTollCost;
    }

    public void setAdditionalTollCost(String additionalTollCost) {
        this.additionalTollCost = additionalTollCost;
    }

    public String getPreCollingCost() {
        return preCollingCost;
    }

    public void setPreCollingCost(String preCollingCost) {
        this.preCollingCost = preCollingCost;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getFuelCostPerMin() {
        return fuelCostPerMin;
    }

    public void setFuelCostPerMin(String fuelCostPerMin) {
        this.fuelCostPerMin = fuelCostPerMin;
    }

    public String getTotalReferMin() {
        return totalReferMin;
    }

    public void setTotalReferMin(String totalReferMin) {
        this.totalReferMin = totalReferMin;
    }

    public String getApprovestatus() {
        return approvestatus;
    }

    public void setApprovestatus(String approvestatus) {
        this.approvestatus = approvestatus;
    }

    public String getSeccompId() {
        return seccompId;
    }

    public void setSeccompId(String seccompId) {
        this.seccompId = seccompId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSettledDate() {
        return settledDate;
    }

    public void setSettledDate(String settledDate) {
        this.settledDate = settledDate;
    }

    public String getSlabId() {
        return slabId;
    }

    public void setSlabId(String slabId) {
        this.slabId = slabId;
    }

    public String getSlabName() {
        return slabName;
    }

    public void setSlabName(String slabName) {
        this.slabName = slabName;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getSlabRate() {
        return slabRate;
    }

    public void setSlabRate(String slabRate) {
        this.slabRate = slabRate;
    }

    public String getSlabRateId() {
        return slabRateId;
    }

    public void setSlabRateId(String slabRateId) {
        this.slabRateId = slabRateId;
    }

    public String getRouteCostId() {
        return routeCostId;
    }

    public void setRouteCostId(String routeCostId) {
        this.routeCostId = routeCostId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getTripSheetId() {
        return tripSheetId;
    }

    public void setTripSheetId(String tripSheetId) {
        this.tripSheetId = tripSheetId;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getFreezeInDate() {
        return freezeInDate;
    }

    public void setFreezeInDate(String freezeInDate) {
        this.freezeInDate = freezeInDate;
    }

    public String getFreezeInTime() {
        return freezeInTime;
    }

    public void setFreezeInTime(String freezeInTime) {
        this.freezeInTime = freezeInTime;
    }

    public String getFreezeOutDate() {
        return freezeOutDate;
    }

    public void setFreezeOutDate(String freezeOutDate) {
        this.freezeOutDate = freezeOutDate;
    }

    public String getFreezeOutTime() {
        return freezeOutTime;
    }

    public void setFreezeOutTime(String freezeOutTime) {
        this.freezeOutTime = freezeOutTime;
    }

    public String getIncidentId() {
        return incidentId;
    }

    public void setIncidentId(String incidentId) {
        this.incidentId = incidentId;
    }

    public String getIncidentName() {
        return incidentName;
    }

    public void setIncidentName(String incidentName) {
        this.incidentName = incidentName;
    }

    public String getGoogleCityName() {
        return googleCityName;
    }

    public void setGoogleCityName(String googleCityName) {
        this.googleCityName = googleCityName;
    }

    public String getAxleName() {
        return axleName;
    }

    public void setAxleName(String axleName) {
        this.axleName = axleName;
    }

    public String getSingleJourney() {
        return singleJourney;
    }

    public void setSingleJourney(String singleJourney) {
        this.singleJourney = singleJourney;
    }

    public String getReturnJourney() {
        return returnJourney;
    }

    public void setReturnJourney(String returnJourney) {
        this.returnJourney = returnJourney;
    }

    public String getMonthlyPass() {
        return monthlyPass;
    }

    public void setMonthlyPass(String monthlyPass) {
        this.monthlyPass = monthlyPass;
    }

    public String getLocalVehicleCost() {
        return localVehicleCost;
    }

    public void setLocalVehicleCost(String localVehicleCost) {
        this.localVehicleCost = localVehicleCost;
    }

    public String getTollRateId() {
        return tollRateId;
    }

    public void setTollRateId(String tollRateId) {
        this.tollRateId = tollRateId;
    }

    public String getBillingKmCalculationId() {
        return billingKmCalculationId;
    }

    public void setBillingKmCalculationId(String billingKmCalculationId) {
        this.billingKmCalculationId = billingKmCalculationId;
    }

    public String getFuelResponsibility() {
        return fuelResponsibility;
    }

    public void setFuelResponsibility(String fuelResponsibility) {
        this.fuelResponsibility = fuelResponsibility;
    }

    public String getMaxAllowedKm() {
        return maxAllowedKm;
    }

    public void setMaxAllowedKm(String maxAllowedKm) {
        this.maxAllowedKm = maxAllowedKm;
    }

    public String getBillingKmCalculationName() {
        return billingKmCalculationName;
    }

    public void setBillingKmCalculationName(String billingKmCalculationName) {
        this.billingKmCalculationName = billingKmCalculationName;
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public String getContractTypeName() {
        return contractTypeName;
    }

    public void setContractTypeName(String contractTypeName) {
        this.contractTypeName = contractTypeName;
    }

    public String getCostPerVehiclePerMonth() {
        return costPerVehiclePerMonth;
    }

    public void setCostPerVehiclePerMonth(String costPerVehiclePerMonth) {
        this.costPerVehiclePerMonth = costPerVehiclePerMonth;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }

    public String getVehicleUnit() {
        return vehicleUnit;
    }

    public void setVehicleUnit(String vehicleUnit) {
        this.vehicleUnit = vehicleUnit;
    }

    public String getExtraRunKmCost() {
        return extraRunKmCost;
    }

    public void setExtraRunKmCost(String extraRunKmCost) {
        this.extraRunKmCost = extraRunKmCost;
    }

    public String getDriverResponsibility() {
        return driverResponsibility;
    }

    public void setDriverResponsibility(String driverResponsibility) {
        this.driverResponsibility = driverResponsibility;
    }

    public String getDriverResponsibilityName() {
        return driverResponsibilityName;
    }

    public void setDriverResponsibilityName(String driverResponsibilityName) {
        this.driverResponsibilityName = driverResponsibilityName;
    }

    public String getFuelResponsibilityName() {
        return fuelResponsibilityName;
    }

    public void setFuelResponsibilityName(String fuelResponsibilityName) {
        this.fuelResponsibilityName = fuelResponsibilityName;
    }

    public String getCostPerTrailerPerMonth() {
        return costPerTrailerPerMonth;
    }

    public void setCostPerTrailerPerMonth(String costPerTrailerPerMonth) {
        this.costPerTrailerPerMonth = costPerTrailerPerMonth;
    }

    public String getTrailerTypeId() {
        return trailerTypeId;
    }

    public void setTrailerTypeId(String trailerTypeId) {
        this.trailerTypeId = trailerTypeId;
    }

    public String getTrailerTypeName() {
        return trailerTypeName;
    }

    public void setTrailerTypeName(String trailerTypeName) {
        this.trailerTypeName = trailerTypeName;
    }

    public String getTrailerUnit() {
        return trailerUnit;
    }

    public void setTrailerUnit(String trailerUnit) {
        this.trailerUnit = trailerUnit;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getTrailerContractId() {
        return trailerContractId;
    }

    public void setTrailerContractId(String trailerContractId) {
        this.trailerContractId = trailerContractId;
    }

    public String getVehicleContractId() {
        return vehicleContractId;
    }

    public void setVehicleContractId(String vehicleContractId) {
        this.vehicleContractId = vehicleContractId;
    }

    public String getExistingTrailerUnit() {
        return existingTrailerUnit;
    }

    public void setExistingTrailerUnit(String existingTrailerUnit) {
        this.existingTrailerUnit = existingTrailerUnit;
    }

    public String getExistingVehicleUnit() {
        return existingVehicleUnit;
    }

    public void setExistingVehicleUnit(String existingVehicleUnit) {
        this.existingVehicleUnit = existingVehicleUnit;
    }

    public String getAgreedDate() {
        return agreedDate;
    }

    public void setAgreedDate(String agreedDate) {
        this.agreedDate = agreedDate;
    }

    public String getMfr() {
        return mfr;
    }

    public void setMfr(String mfr) {
        this.mfr = mfr;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String[] getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(String[] selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public String getTrailerNo() {
        return trailerNo;
    }

    public void setTrailerNo(String trailerNo) {
        this.trailerNo = trailerNo;
    }

    public String getTrailerRemarks() {
        return trailerRemarks;
    }

    public void setTrailerRemarks(String trailerRemarks) {
        this.trailerRemarks = trailerRemarks;
    }

    public String getVehicleRegNo() {
        return vehicleRegNo;
    }

    public void setVehicleRegNo(String vehicleRegNo) {
        this.vehicleRegNo = vehicleRegNo;
    }

    public String getMfrId() {
        return mfrId;
    }

    public void setMfrId(String mfrId) {
        this.mfrId = mfrId;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getTrailerId() {
        return trailerId;
    }

    public void setTrailerId(String trailerId) {
        this.trailerId = trailerId;
    }

    public String getStartReading() {
        return startReading;
    }

    public void setStartReading(String startReading) {
        this.startReading = startReading;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getJan() {
        return jan;
    }

    public void setJan(String jan) {
        this.jan = jan;
    }

    public String getFeb() {
        return feb;
    }

    public void setFeb(String feb) {
        this.feb = feb;
    }

    public String getMar() {
        return mar;
    }

    public void setMar(String mar) {
        this.mar = mar;
    }

    public String getApr() {
        return apr;
    }

    public void setApr(String apr) {
        this.apr = apr;
    }

    public String getMay() {
        return may;
    }

    public void setMay(String may) {
        this.may = may;
    }

    public String getJun() {
        return jun;
    }

    public void setJun(String jun) {
        this.jun = jun;
    }

    public String getJul() {
        return jul;
    }

    public void setJul(String jul) {
        this.jul = jul;
    }

    public String getAug() {
        return aug;
    }

    public void setAug(String aug) {
        this.aug = aug;
    }

    public String getSep() {
        return sep;
    }

    public void setSep(String sep) {
        this.sep = sep;
    }

    public String getOct() {
        return oct;
    }

    public void setOct(String oct) {
        this.oct = oct;
    }

    public String getNov() {
        return nov;
    }

    public void setNov(String nov) {
        this.nov = nov;
    }

    public String getDec() {
        return dec;
    }

    public void setDec(String dec) {
        this.dec = dec;
    }

    public String getQuotationDate() {
        return quotationDate;
    }

    public void setQuotationDate(String quotationDate) {
        this.quotationDate = quotationDate;
    }

    public String getQuotationStatus() {
        return quotationStatus;
    }

    public void setQuotationStatus(String quotationStatus) {
        this.quotationStatus = quotationStatus;
    }

    public String getQuotationType() {
        return quotationType;
    }

    public void setQuotationType(String quotationType) {
        this.quotationType = quotationType;
    }

    public String getSalesManager() {
        return salesManager;
    }

    public void setSalesManager(String salesManager) {
        this.salesManager = salesManager;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public String getQuotationNo() {
        return quotationNo;
    }

    public void setQuotationNo(String quotationNo) {
        this.quotationNo = quotationNo;
    }

    public String getQuotationId() {
        return quotationId;
    }

    public void setQuotationId(String quotationId) {
        this.quotationId = quotationId;
    }

    public String getQuotationDetailsId() {
        return quotationDetailsId;
    }

    public void setQuotationDetailsId(String quotationDetailsId) {
        this.quotationDetailsId = quotationDetailsId;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTariffType() {
        return tariffType;
    }

    public void setTariffType(String tariffType) {
        this.tariffType = tariffType;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getRatePerKm() {
        return ratePerKm;
    }

    public void setRatePerKm(String ratePerKm) {
        this.ratePerKm = ratePerKm;
    }

    public String getCustomerDetails() {
        return customerDetails;
    }

    public void setCustomerDetails(String customerDetails) {
        this.customerDetails = customerDetails;
    }

    public String getContainerQty() {
        return containerQty;
    }

    public void setContainerQty(String containerQty) {
        this.containerQty = containerQty;
    }

    public String getContainerTypeName() {
        return containerTypeName;
    }

    public void setContainerTypeName(String containerTypeName) {
        this.containerTypeName = containerTypeName;
    }

    public String getFromDistance() {
        return fromDistance;
    }

    public void setFromDistance(String fromDistance) {
        this.fromDistance = fromDistance;
    }

    public String getReferenceName() {
        return referenceName;
    }

    public void setReferenceName(String referenceName) {
        this.referenceName = referenceName;
    }

    public String getToDistance() {
        return toDistance;
    }

    public void setToDistance(String toDistance) {
        this.toDistance = toDistance;
    }

    public String getDistanceContractId() {
        return distanceContractId;
    }

    public void setDistanceContractId(String distanceContractId) {
        this.distanceContractId = distanceContractId;
    }

    public String getLoadType() {
        return loadType;
    }

    public void setLoadType(String loadType) {
        this.loadType = loadType;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }


}
