
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ page import="ets.domain.mrs.business.MrsTO" %> 
        <title>MRSList</title>
    </head>
    <body>
        
<script>
    
function viewMpr(mprId)
{
    document.mpr.action="/throttle/alterMprPage.do";
    document.mpr.submit();    
}    
    
    
</script>    


        
        <form name="mpr" method="post" >                    
            <%@ include file="/content/common/path.jsp" %>            
            <!-- pointer table -->
           
            <!-- message table -->           
            <%@ include file="/content/common/message.jsp" %>    
             
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" id="bg" class="border">
               
                <tr>
                    <td class="contentsub" height="30"><div class="contentsub">MPR/LPR NO</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Vendor Name</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Purchase Type</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Raised Date</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Elapsed Days</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Status</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Edit</div></td>
                </tr>
                <% int index = 0;
            String classText = "";
            int oddEven = 0;
                %>
                <c:if test = "${mprList != null}" >
                    <c:forEach items="${mprList}" var="mpr"> 
                        <%

            oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr>
                            <td class="<%=classText %>" height="30"><c:out value="${mpr.mprId}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${mpr.vendorName}"/></td>                       
                            <td class="<%=classText %>" height="30"><c:out value="${mpr.purchaseType}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${mpr.mprDate}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${mpr.elapsedDays}"/></td>                                                                        
                            <td class="<%=classText %>" height="30"><c:out value="${mpr.status}"/></td>                                                                                                    
                            <td class="<%=classText %>" height="30"><c:out value="${mpr.status}"/></td>                                                                                                    
                            <td class="<%=classText %>" height="30"><a href="" onClick="viewMpr(<c:out value="${mpr.mprId}"/>)" > </a> </td>                                                                                                    
                            
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach>
                </c:if>                  
            </table>                        
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
