<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        $("#tabs").tabs();
    });
</script>
<style>

    .loading {

        border:10px solid red;

        display:none;

    }

    .button1{border: 1px black solid ; color:#000 !important;  }



    .spinner{

        position: fixed;

        top: 50%;

        left: 50%;

        margin-left: -50px; /* half width of the spinner gif */

        margin-top: -50px; /* half height of the spinner gif */

        text-align:center;

        z-index:1234;

        overflow: auto;

        width: 300px; /* width of the spinner gif */

        height: 300px; /*hight of the spinner gif +2px to fix IE8 issue */

    }

    .ui-loader-background {

        width:100%;

        height:100%;

        top:0;

        padding: 0;

        margin: 0;

        background: rgba(0, 0, 0, 0.3);

        display:none;

        position: fixed;

        z-index:100;

    }

    .ui-loader-background {

        display:block;



    }

</style>

</head>


    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> Billing party change</h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Sales</a></li>
            <li class="active">Billing party change</li>
        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body>
                <% String menuPath = "Operations >>  Create New Consignment Note";
                            request.setAttribute("menuPath", menuPath);
                %>
                <form name="cNote" method="post">

                    <%@include file="/content/common/message.jsp" %>
                    <%
                                Date today = new Date();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                                String startDate = sdf.format(today);
                    %>
                    <div id="spinner" class="spinner" style="display:none;" >
                        <img id="img-spinner" src="images/page-loader2.gif" alt="Loading" />
                        <div style="margin-top: 10px; color: white">
                            <b>Please wait...</b>
                        </div>
                    </div>
                    <input type="hidden" name="contractApprovalStatus" value='' />
                    <input type="hidden" name="startDate" value='<%=startDate%>' />

                    
                    <table class="table table-info mb30 table-hover" style="width:100%">
                         <tr>
                            <font color="red" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; ">
                            <div align="center" id="Status">&nbsp;&nbsp;</div>
                            </font>
                            <td><font color="red">*</font>Gr No</td>
                                    <td height="30">
                                       <input name="grId" id="grId" type="hidden" class=" form-control" style="width:240px;height:40px;" value="">
                                       <input name="grNo" id="grNo" type="text" class=" form-control" style="width:240px;height:40px;" value="">
                                    </td>
                             </tr>
                         <tr>
                            <font color="red" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; ">
                            <div align="center" id="Status">&nbsp;&nbsp;</div>
                            </font>
                            <td><font color="red">*</font>Old Billing Party Name </td>
                            <td><input type="hidden" name="customerIdO" id="customerIdO" class="form-control"  /> 
                                <input type="text" name="customerNameO"   id="customerNameO" class="form-control" readonly  onKeyPress="return onKeyPressBlockNumbers(event);" onkeyup="return checkKeyForCustomer(this, event, 'customerId')"/></td>
                            <td><font color="red"></font>New Billing Party Name</td>
                            <td><input type="hidden" name="customerIdN" id="customerIdN" class="form-control" />
                                <input type="text" name="customerNameN"   id="customerNameN" class="form-control"   onKeyPress="return onKeyPressBlockNumbers(event);" onkeyup="return checkKeyForCustomer(this, event, 'customerId')"/></td>
                        </tr>
                         <tr>
                            <td><font color="red">*</font>Route</td>
                            <td>
                                <input type="hidden" name="tripId" id="tripId" class="form-control" value="" /> 
                                <input type="hidden" name="contractId" id="contractId" class="form-control"  value=""/> 
                                <input type="hidden" name="contractRateId" id="contractRateId" class="form-control"  value=""/> 
                                <input type="hidden" name="routeContractId" id="routeContractId" class="form-control"  value=""/> 
                                <input type="text" name="routeInfo" id="routeInfo" class="form-control" readonly /> </td>
                            <td><font color="red"></font></td>
                            <td><font color="red"></font></td>
                        </tr>
                         <tr>
                            <td><font color="red"></font>Estimated Revenue</td>
                            <td><input type="text" name="estimatedRevenueO" id="estimatedRevenueO" class="form-control" readonly/></td>
                           
                             <td><font color="red"></font>Estimated Revenue</td>
                            <td><input type="text" name="estimatedRevenueN" id="estimatedRevenueN" class="form-control" readonly/></td>
                        </tr>
                    </table>
                    <center>
                        <!--                        <input type="button" class="button" name="Save" value="Estimate Freight" onclick="estimateFreight();" >-->
                        <div id="orderButton" >
                            <input type="button" class="btn btn-info" name="Save" value="Update" id="Update" onclick="submitPage(this.value);" >
                        </div>
                    </center>
                    <!--                </div>-->
                    <!--- pop up div -->
                    <script>
                        $(document).ready(function() {
                           $('#grNo').autocomplete({
                          source: function(request, response) {
                              $.ajax({
                                  url: "/throttle/getGRNoDetails.do",
                                  dataType: "json",
                                  data: {
                                      grNo: request.term
                                  },
                                  success: function(data, textStatus, jqXHR) {
                                      var items = data;
                                      if (items == '') {
                                          $('#grId').val('');
                                          $('#grNo').val('');
                                      }
                                      response(items);
                                  },
                                  error: function(data, type) {
                                      console.log(type);
                                  }
                              });
                          },
                          minLength: 1,
                          select: function(event, ui) {
                              var value = ui.item.Name;
                              var tmp = value.split('~');
                              $('#grId').val(tmp[0]);
                              $('#grNo').val(tmp[1]);
                              $('#customerIdO').val(tmp[2]);
                              $('#customerNameO').val(tmp[3]);
                              $('#tripId').val(tmp[4]);
                              $('#routeInfo').val(tmp[5]);
                              $('#estimatedRevenueO').val(tmp[6]);
                              $('#consignmentOrderId').val(tmp[7]);
                              $('#contractId').val(tmp[8]);
                              $('#contractRateId').val(tmp[9]);
                              $('#routeContractId').val(tmp[10]);
                              return false;
                          }
                      }).data("ui-autocomplete")._renderItem = function(ul, item) {
                          var itemVal = item.Name;
                          var temp = itemVal.split('~');
                          itemVal = '<font color="green">' + temp[1] + '</font>';
                          return $("<li></li>")
                                  .data("item.autocomplete", item)
                                  .append("<a>" + itemVal + "</a>")
                                  .appendTo(ul);
                      };
                  });
                  
                  
                        $(document).ready(function() {
                           $('#customerNameN').autocomplete({
                          source: function(request, response) {
                              $.ajax({
                                  url: "/throttle/getCustomerNameDetails.do",
                                  dataType: "json",
                                  data: {
                                      customerName: request.term
                                  },
                                  success: function(data, textStatus, jqXHR) {
                                      var items = data;
                                      if (items == '') {
                                          $('#customerIdN').val('');
                                          $('#customerNameN').val('');
                                      }
                                      response(items);
                                  },
                                  error: function(data, type) {
                                      console.log(type);
                                  }
                              });
                          },
                          minLength: 1,
                          select: function(event, ui) {
                              var value = ui.item.Name;
                              var tmp = value.split('~');
                              $('#customerIdN').val(tmp[0]);
                              $('#customerNameN').val(tmp[1]);
                              if(tmp[2] > 0){
                                  $('#estimatedRevenueN').val(tmp[2]);
                              }else{
                                  $('#estimatedRevenueN').val(tmp[3]);
                              }
                              return false;
                          }
                      }).data("ui-autocomplete")._renderItem = function(ul, item) {
                          var itemVal = item.Name;
                          var temp = itemVal.split('~');
                          itemVal = '<font color="green">' + temp[1] + '</font>';
                          return $("<li></li>")
                                  .data("item.autocomplete", item)
                                  .append("<a>" + itemVal + "</a>")
                                  .appendTo(ul);
                      };
                  });
                    </script> 
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

                <div id="loader"></div>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>



<!--#@@@@@@@@@@@@@@@@@-->
