<%--
    Document   : tripPlanningExcel
    Created on : Nov 4, 2013, 10:56:05 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
             <%
            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
            //System.out.println("Current Date: " + ft.format(dNow));
            String curDate = ft.format(dNow);
            String expFile = "View_Employee_"+curDate+".xls";

            String fileName = "attachment;filename=" + expFile;
            response.setContentType("application/vnd.ms-excel;charset=UTF-8");
            response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
        <c:if test = "${employeeDetails != null}" >
            <table style="width: 1100px" align="center" border="1">
                <thead>
                    <tr height="45">
                        <th class="contentsub"><h3><h3>Sno</h3></th>
                        <th class="contentsub"><h3>Employee Code</h3></th>
                        <th class="contentsub"><h3>Employee Name</h3></th>
                        <th class="contentsub"><h3>Date of Birth</h3></th>
                        <th class="contentsub"><h3>Date of Joining </h3></th>
                        <th class="contentsub"><h3>Gender </h3></th>
                        <th class="contentsub"><h3>Blood Group </h3></th>
                        <th class="contentsub"><h3>Marital Status </h3></th>
                        <th class="contentsub"><h3>Father Name</h3></th>
                        <th class="contentsub"><h3>Mobile No</h3></th>
                        <th class="contentsub"><h3>Home Phone</h3></th>
                        <th class="contentsub"><h3>Qualification</h3></th>
                        <th class="contentsub"><h3>Email Id</h3></th>
                        <th class="contentsub"><h3>Department Name</h3></th>
                        <th class="contentsub"><h3>Designation Name</h3></th>
                        <th class="contentsub"><h3>Grade Name</h3></th>
                        <th class="contentsub"><h3>Company Name</h3></th>
                        <th class="contentsub"><h3>Reference Name</h3></th>
                        <th class="contentsub"><h3>Contract Employee</h3></th>
                        <th class="contentsub"><h3>Driver License No</h3></th>
                        <th class="contentsub"><h3>Valid Upto</h3></th>
                        <th class="contentsub"><h3>License Type</h3></th>
                        <th class="contentsub"><h3>License State</h3></th>
                        <th class="contentsub"><h3>Present Address</h3></th>
                        <th class="contentsub"><h3>Present City</h3></th>
                        <th class="contentsub"><h3>Present State</h3></th>
                        <th class="contentsub"><h3>Present Pincode</h3></th>
                        <th class="contentsub"><h3>Permanent Address</h3></th>
                        <th class="contentsub"><h3>Permanent City</h3></th>
                        <th class="contentsub"><h3>Permanent State</h3></th>
                        <th class="contentsub"><h3>Permanent Pincode</h3></th>
                        <th class="contentsub"><h3>Salary Type</h3></th>
                        <th class="contentsub"><h3>Consolidated Salary</h3></th>
                        <th class="contentsub"><h3>Is Eligible for ESI</h3></th>
                        <th class="contentsub"><h3>Is Eligible for PF</h3></th>
                        <th class="contentsub"><h3>Bank Account Number</h3></th>
                        <th class="contentsub"><h3>Bank Name</h3></th>
                        <th class="contentsub"><h3>Branch Name</h3></th>
                        <th class="contentsub"><h3>A/c Holder Name</h3></th>
                    </tr>
                </thead>
                <% int index = 0;int sno = 1;%>
                <tbody>
                    <c:forEach items="${employeeDetails}" var="employee">
                        <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                        <tr height="30">
                            <td align="left" class="text1" ><%=sno%></td>
                            <td align="left" class="text1" ><c:out value="${employee.empCode}"/></a></td>
                            <td align="left" class="text1" ><c:out value="${employee.name}"/></a></td>
                            <td align="left" class="text1" ><c:out value="${employee.dateOfBirth}"/></a></td>
                            <td align="left" class="text1" ><c:out value="${employee.DOJ}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.gender}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.bloodGrp}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.martialStatus}"/></td>
                          
                            <td align="left" class="text1" ><c:out value="${employee.fatherName}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.mobile}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.phone}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.qualification}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.email}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.deptName}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.desigName}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.gradeName}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.companyName}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.referenceName}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.contractDriver}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.drivingLicenseNo}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.licenseDate}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.licenseType}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.licenseState}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.addr}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.city}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.state}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.pincode}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.addr1}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.city1}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.state1}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.pincode1}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.salaryType}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.basicSalary}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.esiEligible}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.pfEligible}"/></td>
                            <td align="left" class="text1" ><c:out value="${employee.bankAccountNo}"/></td>
                            <td align="left" class="text1" >&nbsp;</td>
                            <td align="left" class="text1" >&nbsp;</td>
                            <td align="left" class="text1" ><c:out value="${employee.nomineeName}"/></td>
                          
                        <%index++;%>
                        <%sno++;%>
                    </c:forEach>
                </tbody>
            </table>
        </c:if>

    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
