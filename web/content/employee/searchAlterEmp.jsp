<%--
    Document   : alterLecture
    Created on : Nov 7, 2008, 11:34:46 AM
    Author     : vijay
--%>

<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <title>PAPL</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
            <script src="/throttle/js/jquery.ui.core.js"></script>
            <script src="/throttle/js/jquery.ui.datepicker.js"></script>
            <script type="text/javascript">
    $(document).ready(function() {
        //alert('hai');
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            yearRange: '1900:2030', changeMonth: true, changeYear: true
        });

    });
    /*$( ".datepicker" ).datepicker({changeMonth: true,changeYear: true, minDate: 0, maxDate: "+10Y +10D" });
 
     }*/
            </script>
            <script language="javascript">



                function submitPage(value) {
                    if (value == "search") {
                        if (isEmpty(document.lecture.staffCode.value) && isEmpty(document.lecture.staffName.value)) {
                            alert("please enter the staff code or staff name and search");
                            document.lecture.staffCode.focus();
                        } else if (document.lecture.staffCode.value != "" && document.lecture.staffName.value != "") {
                            alert("please enter the any one and serach");
                            document.lecture.staffCode.value = "";
                            document.lecture.staffName.value = "";
                        } else {
                            document.lecture.action = '/throttle/getAlterEmpPage.do';
                            document.lecture.submit();
                            return;
                        }
                    } else if (value == "add") {
                        document.lecture.action = '/throttle/addEmployeePage.do';
                        document.lecture.submit();
                        return;
                    }


                    else if (isEmpty(document.lecture.staffNames.value)) {
                        alert("Enter Valid Character for Employee Name ");
                        document.lecture.staffNames.focus();
                        return;
                    } else if (isEmpty(document.lecture.qualification.value)) {
                        alert("Qualification Field Should Not Be Empty");
                        document.lecture.qualification.focus();
                        return;
                    } else if (isChar(document.lecture.qualification.value)) {
                        alert("Enter Valid Character for Qualification ");
                        document.lecture.qualification.focus();
                        return;
                    } else if (isEmpty(document.lecture.dateOfBirth.value)) {
                        alert("Date of Birth Field Should Not Be Empty");
                        document.lecture.dateOfBirth.focus();
                        return;
                    } else if (isEmpty(document.lecture.dateOfJoining.value)) {
                        alert("Date Of Joining Field Should Not Be Empty");
                        document.lecture.dateOfJoining.focus();
                        return;
                    } else if (isEmpty(document.lecture.bloodGrp.value)) {
                        alert("Blood Group Field Should Not Be Empty");
                        document.lecture.bloodGrp.focus();
                        return;
                    } else if (isEmpty(document.lecture.fatherName.value)) {
                        alert("Father Field Should Not Be Empty");
                        document.lecture.fatherName.focus();
                        return;
                    } else if (isEmpty(document.lecture.mobile.value)) {
                        alert("Mobile Field Should Not Be Empty");
                        document.lecture.mobile.focus();
                        return;
                    } else if (isDigit(document.lecture.mobile.value)) {
                        alert("Mobile Field Should Be Digit");
                        document.lecture.mobile.focus();
                        return;
                    } else if (isEmpty(document.lecture.phone.value)) {
                        alert("Phone No Field Should Be Digit");
                        document.lecture.phone.focus();
                        return;
                    } else if (isEmpty(document.lecture.email.value)) {
                        alert("Email Field Should Not Be Empty");
                        document.lecture.email.focus();
                        return;
                    } else if (isEmpty(document.lecture.addr.value)) {
                        alert("Present Address Field Should Not Be Empty");
                        document.lecture.addr.focus();
                        return;
                    } else if (isEmpty(document.lecture.city.value)) {
                        alert("Present City Field Should Not Be Empty");
                        document.lecture.city.focus();
                        return;
                    } else if (isEmpty(document.lecture.state.value)) {
                        alert("Present State Field Should Not Be Empty");
                        document.lecture.state.focus();
                        return;
                    } else if (isEmpty(document.lecture.pincode.value)) {
                        alert("Present Pincode Field Should Not Be Empty");
                        document.lecture.pincode.focus();
                        return;
                    } else if (isEmpty(document.lecture.addr1.value)) {
                        alert("Permanant Address Field Should Not Be Empty");
                        document.lecture.addr1.focus();
                        return;
                    } else if (isEmpty(document.lecture.city1.value)) {
                        alert("Permanant City Field Should Not Be Empty");
                        document.lecture.city1.focus();
                        return;
                    } else if (isEmpty(document.lecture.state1.value)) {
                        alert("Permanant State Field Should Not Be Empty");
                        document.lecture.state1.focus();
                        return;
                    } else if (isEmpty(document.lecture.pincode1.value)) {
                        alert("Permanant Pincode Field Should Not Be Empty");
                        document.lecture.pincode1.focus();
                        return;
                    } else if (document.lecture.deptid.value == "0") {
                        alert("Select any one Department");
                        document.lecture.deptid.focus();
                        return;
                    } else if (document.lecture.designationId.value == "0") {
                        alert("Select any one Designation");
                        document.lecture.designationId.focus();
                        return;
                    } else if (document.lecture.gradeId.value == "0") {
                        alert("Select any one Grade");
                        document.lecture.gradeId.focus();
                        return;
                    }
                    if (document.lecture.userStatus.checked == true) {
                        if (isEmpty(document.lecture.userName.value)) {
                            alert("User Name Field Should Not Be Empty");
                            document.lecture.userName.focus();
                            return;
                        }

                        if (document.lecture.userCheck.value == 'exists') {
                            alert("User Name Already Exists");
                            document.lecture.userName.focus();
                            document.lecture.userName.select();
                            return;
                        }

                        if (!isName(document.lecture.userName.value)) {
                            alert("Enter Valid Character for User Name ");
                            document.lecture.userName.focus();
                            return;
                        }

                        if (isEmpty(document.lecture.password.value)) {
                            alert("Please Enter Password");
                            document.lecture.password.focus();
                            return;
                        }

                        if (document.lecture.roleId.value == "0") {
                            alert("Please Select Employee Role ");
                            document.lecture.roleId.focus();
                            return;
                        }

                    }
                    if (value == 'save')
                    {
                        document.lecture.action = '/throttle/alterEmployee.do';
                        document.lecture.submit();
                    }
                }


                function copyAddress(value)
                {
                    if (value == true) {
                        var addAddr;
                        var addCity;
                        var addState;
                        var pinCode;

                        addAddr = document.lecture.addr.value;
                        addCity = document.lecture.city.value;
                        addState = document.lecture.state.value;
                        pinCode = document.lecture.pincode.value;

                        document.lecture.addr1.value = addAddr;
                        document.lecture.city1.value = addCity;
                        document.lecture.state1.value = addState;
                        document.lecture.pincode1.value = pinCode;
                    }
                    else
                    {
                        document.lecture.addr1.value = "";
                        document.lecture.city1.value = "";
                        document.lecture.state1.value = "";
                        document.lecture.pincode1.value = "";

                    }
                }



                function isChar(s) {
                    if (!(/^-?\d+$/.test(s))) {
                        return false;
                    }
                    return true;
                }

                function isEmail(s)
                {

                    if (/[^@]+@[^@]+\.(com)|(co.in)$/.test(s))
                        return false;
                    alert("Email not in valid form!");
                    return true;
                }

                function initCs() {
                    var desig = document.getElementById('desigId');
                    //alert(desig.value);
                    desig.onchange = function() {
                        if (this.value != "") {
                            var list = document.getElementById("gradeId");
                            while (list.childNodes[0]) {
                                list.removeChild(list.childNodes[0])
                            }
                            fillSelect(this.value);
                        }
                    }

                    fillSelect(document.getElementById('desigId').value);
                }

                function go() {

                    if (httpRequest.readyState == 4) {
                        if (httpRequest.status == 200) {
                            var response = httpRequest.responseText;
                            var list = document.getElementById("gradeId");
                            var grade = response.split(',');
                            for (i = 1; i < grade.length; i++) {
                                var gradeDetails = grade[i].split('-');
                                var gradeId = gradeDetails[0];
                                var gradeName = gradeDetails[1];
                                var x = document.createElement('option');
                                var name = document.createTextNode(gradeName);
                                x.appendChild(name);
                                x.setAttribute('value', gradeId)
                                list.appendChild(x);
                            }
                        }
                    }
                }


                function fillSelect(desigId) {
                    //alert(desigId +" design Id ");
                    var url = '/throttle/getGradeForDesig.do?desigId=' + desigId;

                    if (window.ActiveXObject)
                    {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)
                    {
                        httpRequest = new XMLHttpRequest();
                    }

                    httpRequest.open("POST", url, true);
                    httpRequest.onreadystatechange = function() {
                        go();
                    };
                    httpRequest.send(null);
                }



                function showTab() {
                    if (document.lecture.userStatus.checked == true) {
                        document.getElementById("userStat").style.visibility = "visible";
                        document.getElementById("userStat1").style.visibility = "visible";
                    } else {
                        document.getElementById("userStat").style.visibility = "hidden";
                        document.getElementById("userStat1").style.visibility = "hidden";
                    }
                }




                var httpRequest;
                function getProfile(userName)
                {

                    var url = '/throttle/checkUserName.do?userName=' + userName;

                    if (window.ActiveXObject)
                    {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)
                    {
                        httpRequest = new XMLHttpRequest();
                    }

                    httpRequest.open("GET", url, true);

                    httpRequest.onreadystatechange = function() {
                        processRequest();
                    };

                    httpRequest.send(null);
                }


                function processRequest()
                {
                    if (httpRequest.readyState == 4)
                    {
                        if (httpRequest.status == 200)
                        {
                            if (trim(httpRequest.responseText) != "") {

                                document.getElementById("userNameStatus").innerHTML = httpRequest.responseText;
                                document.lecture.password.value = '';
                                document.lecture.userCheck.value = 'exists';
                                document.lecture.password.disabled = true;
                                //document.lecture.password.disabled=true;
                                document.lecture.userName.select();
                            } else {
                                document.getElementById("userNameStatus").innerHTML = "";
                                document.lecture.userCheck.value = 'notExists';
                                document.lecture.password.disabled = false;
                                document.lecture.password.focus();
                            }
                        }
                        else
                        {
                            alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
                        }
                    }
                }

                function nameSearch() {
                    var oTextbox = new AutoSuggestControl(document.getElementById("staffName"),
                            new ListSuggestions("staffName", "/throttle/EmpNameSuggests.do?"));
                }
    //            var httpRequest;
    //            function nameSearch(userName)
    //            {
    //
    //                var url = '/throttle/EmpNameSuggests.do?staffName='+ userName;
    //
    //                if (window.ActiveXObject)
    //                {
    //                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
    //                }
    //                else if (window.XMLHttpRequest)
    //                {
    //                    httpRequest = new XMLHttpRequest();
    //                }
    //
    //                httpRequest.open("GET", url, true);
    //
    //                httpRequest.onreadystatechange = function() { processRequestName(); } ;
    //
    //                httpRequest.send(null);
    //            }
    //
    //
    //            function processRequestName()
    //            {
    //                if (httpRequest.readyState == 4)
    //                {
    //                    if(httpRequest.status == 200)
    //                    {
    //                        if(trim(httpRequest.responseText)!="") {
    //                               alert(httpRequest.responseText);
    //                               var temp = httpRequest.responseText;
    //                    }
    //                    else
    //                    {
    //                        alert("Error loading page\n"+ httpRequest.status +":"+ httpRequest.statusText);
    //                    }
    //                }
    //            }
    //            }

                function addPage()
                {
                    document.lecture.action = '/throttle/addEmployeePage.do';
                    document.lecture.submit();
                }

                function viewPage()
                {
                    document.lecture.action = '/throttle/EmpViewSearchPage.do';
                    document.lecture.submit();
                }


                function codeSearch(mode) {
                    if (mode == 0) {
                        var oTextbox = new AutoSuggestControl(document.getElementById("staffCode"), new ListSuggestions("staffCode", "/throttle/handleGetEmpCode.do?"));
                    } else {
                        var oTextbox = new AutoSuggestControl(document.getElementById("staffName"), new ListSuggestions("staffName", "/throttle/EmpNameSuggests.do?"));
                    }

                }

                function setValues() {
                    var staffCode = '<%=request.getAttribute("staffCode")%>';
                    var staffName = '<%=request.getAttribute("staffName")%>';
    //                alert(staffCode);
    //                alert(staffName);
                    if (staffCode != "null" && staffCode != "") {
                        document.lecture.staffCode.value = staffCode;
                    }
                    if (staffCode == "null") {
                        document.lecture.staffCode.value = "";
                    }
                    if (staffName != "null" && staffName != "") {
                        document.lecture.staffName.value = staffName;
                    }
                    if (staffName == "null") {
                        document.lecture.staffName.value = "";
                    }
                }

function viewEmployeePhotos(staffId) {
            window.open('/throttle/displayBlobData.jsp?staffId='+staffId+'&empPhoto=1', 'PopupPage', 'height = 500, width = 500, scrollbars = yes, resizable = yes');
        }
            </script>
    </head>

    <!--[if lte IE 7]>
    <style type="text/css">

    #fixme {display:block;
    top:0px; left:0px;  position:fixed;  }
    </style>
    <![endif]-->

    <!--[if lte IE 6]>
    <style type="text/css">
    body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
    #fixme {display:block;
    top:0px; left:0px;  position:fixed;  }
    * html #fixme  {position:absolute;}
    </style>
    <![endif]-->

    <!--[if lte IE 6]>
    <style type="text/css">
    /*<![CDATA[*/
    html {overflow-x:auto; overflow-y:hidden;}
    /*]]>*/
    </style>
    <![endif]-->

    <body onload="showIfDriver();
            checkStatus();
            setImages(1, 0, 0, 0, 0, 0);
            setValues();
            document.lecture.staffId.focus();">
        <form name="lecture" method="post" enctype="multipart/form-data">

            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>



            <table  border="0" align="center" width="600" cellpadding="0" cellspacing="0" class="border" bgcolor="#FFFFFF" style="display: none;" >
                <tr>
                    <td class="text2" height="30">Employee Code</td>
                    <td class="text2" height="30"><input type="text" class="textbox" id="staffCode" name="staffCode" autocomplete="off" onFocus="codeSearch(0)" /></td>
                    <td class="text2" height="30">Employee Name</td>
                    <td class="text2" height="30"><input type="text" class="textbox" id="staffName"  name="staffName" autocomplete="off" onFocus="nameSearch(1)"/></td>
                    <td class="text2" height="30"><input type="button" class="button" value="Search" name="search" onClick="submitPage(this.name)"/></td>

                </tr>

                <tr>
                    <td colspan="5" align="center" > &nbsp; </td> </tr>

                <!--
                <tr>
                <td colspan="5" align="center" >
                <input type="button" class="button" name="Add" onClick="addPage();" value="Add" > &nbsp;
                <input type="button" class="button" name="View" onClick="viewPage();" value="View" >
                </td>
                </tr>
                -->
            </table>

            <center>

                <input type="hidden" value="" name="reqfor"/>
            </center>
            <%
                        int snumber = 1;
                        int status = 0;
                        if (request.getAttribute("StaffList") != null) {
            %>
            <table width="800" align="center" border="0" cellpadding="0" cellspacing="0" class="border">
                <c:choose>
                    <c:when test = "${StaffList != null}">
                        <c:forEach items="${StaffList}" var="lec">


                            <tr>
                                <td class="contenthead" height="30" colspan="2"><div class="contenthead">Personal Details</div></td>
                                <td class="contenthead" height="30" >Employee Code</td>
                                <td class="contenthead" height="30">
                                    <a onclick="viewEmployeePhotos('<c:out value="${lec.staffId}"/>')" href=""><c:out value="${staffCode}"/></a>
                                </td>
                            </tr>


                            <tr>
                                <td class="text1" height="30"><font color="red">*</font>Employee Photo</td>
                                <td class="text1" height="30"><input type="hidden" name="staffid" value='<c:out value="${lec.staffId}"/>'/>
                                    <input type="file" id="employeePhoto" name="employeePhoto" size="20" class="textbox" maxlength="10" value="0">
                                </td>
                                <td class="text1" height="30"><font color="red">*</font>Name</td>
                                <td class="text1" height="30"><input type="text" name="staffNames" class="textbox" onKeyPress="return onKeyPressBlockNumbers(event);" value='<c:out value="${lec.name}"/>' maxlength="50"></td>
                            </tr>

                            <tr>
                                <td class="text2" height="30">Qualification </td>
                                <td class="text2" height="30"><input type="text" name="qualification" size="20" maxlength="30" class="textbox" value="<c:out value="${lec.qualification}"/>"></td>
                                <td class="text2" height="30"><font color="red">*</font>Date Of Birth</td>
                                <td class="text2" height="30"><input type="text" id="dateOfBirth" name="dateOfBirth" value='<c:out value="${lec.dateOfBirth}"/>' size="20" class="datepicker" > </td>
                                <script type="text/javascript">
                                    $("#dateOfBirth").datepicker({
                                        yearRange: '1900:1995', changeMonth: true, changeYear: true
                                    });
                                </script> 
                            </tr>

                            <tr>

                                <td class="text1" height="30"><font color="red">*</font>Date Of Joining </td>
                                <td class="text1" height="30"><input type="text" id="dateOfJoining" name="dateOfJoining" value='<c:out value="${lec.DOJ}"/>' size="20" class="datepicker"> </td>
                                <script type="text/javascript">
                                    $("#dateOfJoining").datepicker({
                                        yearRange: '1970', changeMonth: true, changeYear: true
                                    });
                                </script> 
                                <td height="30" class="text1"><font color="red">*</font>Gender</td>
                                <td height="30" class="text1">
                                    <c:choose>
                                        <c:when test="${lec.gender== 'Male'}">
                                            <input type="radio" name="gender" checked value="Male">Male <input type="radio" name="gender" value="Female">Female</td>
                                                </c:when>
                                                <c:otherwise>
                                                    <input type="radio" name="gender" checked value="Female">Female <input type="radio" name="gender" value="Male">Male</td>
                                                        </c:otherwise>
                                                    </c:choose>
                                                    </tr>

                                                    <tr>
                                                        <td class="text2" height="30">Blood Group</td>
                                                        <td class="text2" height="30"><input type="text" name="bloodGrp" size="20" maxlength="7" class="textbox" value="<c:out value="${lec.bloodGrp}"/>"/></td>
                                                        <td height="30" class="text2">Marital Status</td>
                                                        <td class="text2" height="30">

                                                            <c:if test="${lec.martialStatus =='Married'}">
                                                                <input type="radio" name="martialStatus" checked value="Married"/> Married <input type="radio" name="martialStatus" value="UnMarried"/>UnMarried <input type="radio" name="martialStatus" value="Widow"/>Widow
                                                            </c:if>
                                                            <c:if test="${lec.martialStatus =='UnMarried'}">
                                                                <input type="radio" name="martialStatus" value="Married"/> Married <input type="radio" name="martialStatus" checked value="UnMarried"/>UnMarried <input type="radio" name="martialStatus" value="Widow"/>Widow
                                                            </c:if>
                                                            <c:if test="${lec.martialStatus =='Widow'}">
                                                                <input type="radio" name="martialStatus" value="Married"/> Married <input type="radio" name="martialStatus" value="UnMarried"/>UnMarried <input type="radio" checked name="martialStatus" value="Widow"/>Widow</td>
                                                            </c:if>
                                                            <c:if test="${lec.martialStatus ==''}">
                                                            <input type="radio" name="martialStatus" value="Married"/> Married <input type="radio" name="martialStatus" value="UnMarried" checked=""/>UnMarried <input type="radio" name="martialStatus" value="Widow"/>Widow</td>
                                                        </c:if>

                                                    </tr>

                                                    <tr>
                                                        <td class="text1" height="30"><font color="red">*</font>Father Name</td>
                                                        <td class="text1" height="30"><input name="fatherName" type="text" class="textbox" onKeyPress="return onKeyPressBlockNumbers(event);" maxlength="50" size="20" value="<c:out value="${lec.fatherName}"/>"/></td>
                                                        <td class="text1" height="30"><font color="red">*</font>Mobile No </td>
                                                        <td class="text1" height="30"><input type="text" name="mobile" size="20" onKeyPress="return onKeyPressBlockCharacters(event);" class="textbox" maxlength="12" value="<c:out value="${lec.mobile}"/>" /></td>
                                                    </tr>

                                                    <tr>

                                                        <td class="text2" height="30">Home No </td>
                                                        <td class="text2" height="30"><input type="text" name="phone" size="20" onKeyPress="return onKeyPressBlockCharacters(event);" class="textbox" maxlength="12" value="<c:out value="${lec.phone}"/>" ></td>
                                                        <td class="text2" height="30">Email-Id</td>
                                                        <td class="text2" height="30" colspan="4"><input type="text" name="email" size="20" maxlength="35" class="textbox" value="<c:out value="${lec.email}"/>"></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="contenthead" height="30" colspan="4"><div class="contenthead">Present Address</div></td>
                                                    </tr>
                                                    <tr >
                                                        <td class="text1" height="30">Address</td>
                                                        <td class="text1" height="30"> <textarea name="addr" size="20" class="textbox"><c:out value="${lec.addr}"/> </textarea></td>
                                                        <td class="text1" height="30">City</td>
                                                        <td class="text1" height="30"><input type="text" name="city" size="20" onKeyPress="return onKeyPressBlockNumbers(event);" class="textbox" maxlength="30" value="<c:out value="${lec.city}"/>" ></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text2" height="30">State</td>
                                                        <td class="text2" height="30"><input type="text" name="state" size="20" onKeyPress="return onKeyPressBlockNumbers(event);" class="textbox" maxlength="30" value="<c:out value="${lec.state}"/>" ></td>
                                                        <td class="text2" height="30">Pincode</td>
                                                        <td class="text2" height="30"><input type="text" name="pincode" size="20" onKeyPress="return onKeyPressBlockCharacters(event);" class="textbox" maxlength="6" value="<c:out value="${lec.pincode}"/>" ></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="contenthead" height="30" colspan="2"><b>Permanent Address</b> </td>
                                                        <td colspan="2" class="contenthead" height="30">
                                                            <c:if test="${lec.chec == 'Y'}">
                                                                <input type="checkbox" name="chec" onClick="copyAddress(this.checked)" checked value="Y"/> 
                                                            </c:if>
                                                            <c:if test="${lec.chec == 'N'}">
                                                                <input type="checkbox" name="chec" onClick="copyAddress(this.checked)" />
                                                            </c:if>If  Temporary Address is Same As Permanent Address Click Here </td>
                                                    </tr>
                                                    <tr >
                                                        <td class="text1" height="30">Address </td>
                                                        <td class="text1" height="30"> <textarea name="addr1" class="textbox" ><c:out value="${lec.addr1}"/></textarea></td>
                                                        <td class="text1" height="30">City</td>
                                                        <td class="text1" height="30"><input type="text" name="city1" size="20" onKeyPress="return onKeyPressBlockNumbers(event);" class="textbox" maxlength="30" value="<c:out value="${lec.city1}"/>" ></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text2" height="30">State</td>
                                                        <td class="text2" height="30"><input type="text" name="state1" size="20" onKeyPress="return onKeyPressBlockNumbers(event);" class="textbox" maxlength="30" value="<c:out value="${lec.state1}"/>" ></td>
                                                        <td class="text2" height="30">Pincode</td>
                                                        <td class="text2" height="30"><input type="text" name="pincode1" size="20" onKeyPress="return onKeyPressBlockCharacters(event);" class="textbox" maxlength="6" value="<c:out value="${lec.pincode1}"/>" ></td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="4" height="30" class="contenthead"><div class="contenthead">Official Details</div></td>
                                                    </tr>

                                                    <tr>


                                                        <td class="text1" height="30"><font color="red">*</font>Department Name</td>
                                                        <td class="text1" height="30"><select class="textbox" name="deptid" style="width:125px;">
                                                                <option>---Select---</option>
                                                                <c:if test = "${DeptList != null}" >
                                                                    <c:forEach items="${DeptList}" var="Dept">
                                                                        <c:choose>
                                                                            <c:when test="${Dept.departmentId==lec.deptId}">
                                                                                <option selected value='<c:out value="${Dept.departmentId}" />'><c:out value="${Dept.name}" />
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                    <option value='<c:out value="${Dept.departmentId}" />'><c:out value="${Dept.name}" /></option>
                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                        </c:forEach >
                                                                    </c:if>
                                                            </select></td>

                                                        <td class="text1" height="30"><font color="red">*</font>Designation Name </td>
                                                        <td class="text1" height="30"><select class="textbox" id="desigId" name="designationId" onchange="showIfDriver();
                                        initCs();" style="width:125px;" >
                                                                <option value="1">---Select---</option>
                                                                <c:if test = "${DesignaList != null}" >
                                                                    <c:forEach  items="${DesignaList}" var="desig">
                                                                        <c:choose>
                                                                            <c:when test="${lec.desigId==desig.desigId}">
                                                                                <option selected value='<c:out value="${desig.desigId}" />'><c:out value="${desig.desigName}" /></option>
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <option  value='<c:out value="${desig.desigId}" />'><c:out value="${desig.desigName}" /></option>
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </c:forEach >
                                                                </c:if>
                                                            </select></td>
                                                    </tr>

                                                    <tr>
                                                        <td class="text2" height="30"><font color="red">*</font>Grade Name </td>
                                                        <td class="text2" height="30"><select class="textbox" id="gradeId" name="gradeId" style="width:125px;">
                                                                <option value="1">---Select---</option>
                                                                <c:if test = "${GradeList != null}" >
                                                                    <c:forEach  items="${GradeList}" var="Grade">
                                                                        <c:choose>
                                                                            <c:when test="${lec.gradeId==Grade.gradeId}">
                                                                                <option selected value='<c:out value="${Grade.gradeId}" />'><c:out value="${Grade.gradeName}" /></option>
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <option value='<c:out value="${Grade.gradeId}" />'><c:out value="${Grade.gradeName}" /></option>
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </c:forEach >
                                                                </c:if>
                                                            </select></td>
                                                        <td height="30" class="text2">Status</td>
                                                        <td height="30" class="text2"><select name="activeInd" class="textbox" style="width:125px;">
                                                                  <c:if test="${lec.status == 'Y'}">
                                                                        <option  value="Y" selected>Active</option>
                                                                        <option  value="N">InActive</option>
                                                                        <option  value="B"><p style="color:#F00;">Blacklisted</p></option>
                                                                    </c:if>
                                                                    <c:if test="${lec.status == 'N'}">    
                                                                        <option  value="Y">Active</option>
                                                                        <option value="N" selected>InActive</option>
                                                                        <option value="B" >Blacklisted</option>
                                                                    </c:if>     
                                                                    <c:if test="${lec.status == 'B'}">    
                                                                        <option  value="Y">Active</option>
                                                                        <option  value="N" >InActive</option>
                                                                        <option  value="B" selected>Blacklisted</option>
                                                                    </c:if>     
                                                                    <c:if test="${lec.status == ''}">    
                                                                        <option value="Y">Active</option>
                                                                        <option value="N" selected>InActive</option>
                                                                        <option value="B">Blacklisted</option>
                                                                    </c:if>     
                                                            </select></td>
                                                    </tr>

                                                    <tr>
                                                        <td class="text1" height="30"><font color="red">*</font>Company </td>
                                                        <td class="text1" height="30"><select class="textbox" name="cmpId" style="width:125px;">
                                                                <option value="0">-Select-</option>
                                                                <c:if test = "${companyList!= null}" >
                                                                    <c:forEach  items="${companyList}" var="comp">
                                                                        <c:choose>
                                                                            <c:when test="${lec.cmpId == comp.cmpId}">
                                                                                <option selected value='<c:out value="${comp.cmpId}" />'><c:out value="${comp.name}" />(<c:out value="${comp.companyType}" />)</option>
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <option value='<c:out value="${comp.cmpId}" />'><c:out value="${comp.name}" />(<c:out value="${comp.companyType}" />)</option>
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </c:forEach >
                                                                </c:if>
                                                            </select></td>
                                                        <td class="text1" height="30">Reference Name</td>
                                                        <td class="text1" height="30"><input type="text" name="referenceName" onKeyPress="return onKeyPressBlockNumbers(event);" id="referenceName" maxlength="35" class="textbox" value="<c:out value="${lec.referenceName}"/>" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text2" height="30">Hourly Rate</td>
                                                        <td class="text2" height="30">
                                                            <input type="text" name="hourlyRate" maxlength="5" id="hourlyRate" class="textbox" value="<c:out value="${lec.hourlyRate}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" />
                                                        </td>
                                                        <td class="text2" height="30">Terminate</td>
                                                        <td class="text2" height="30"><select name="terminate" id="terminate" class="textbox" style="width:125px;" >
                                                                <c:if test="${lec.terminateFlag == 'Y'}">
                                                                    <option value="Y" selected>Active</option>
                                                                    <option value="N" >InActive</option>
                                                                </c:if>
                                                                <c:if test="${lec.terminateFlag == 'N'}">
                                                                    <option value="N" selected>InActive</option>
                                                                    <option value="Y" >Active</option>
                                                                </c:if>
                                                            </select></td>
                                                        <td class="text2" height="30">&nbsp;</td>
                                                        <td class="text2" height="30">&nbsp;</td>
                                                    </tr>

                                                    <script>
                                                        function showIfDriver() {
                                                            var desig = $("#desigId").val();
                                                            if (desig == "1034") {
                                                                $("#drivingLicenceInfo").show();
                                                                $("#drivingLicenceInfo1").show();
                                                                $("#drivingLicenceInfo2").show();
                                                                $("#drivingLicenceInfo3").show();
                                                            } else {
                                                                $("#drivingLicenceInfo").hide();
                                                                $("#drivingLicenceInfo1").hide();
                                                                $("#drivingLicenceInfo2").hide();
                                                                $("#drivingLicenceInfo3").hide();
                                                            }

                                                        }
                                                    </script>
                                                    <tr id="drivingLicenceInfo2">
                                                        <td colspan="4" height="30" class="contenthead"><B>Driving License Detail</B></td>
                                                    </tr>
                                                    <tr id="drivingLicenceInfo"><td class="text1">Driving License No</td>
                                                        <td class="text1"><input type="text" name="drivingLicenseNo" class="textbox" maxlength="30" value="<c:out value="${lec.drivingLicenseNo}"/>" /></td>
                                                        <td class="text1">Valid Upto</td>
                                                        <td class="text1"><input type="text" id="licenseExpDate" name="licenseExpDate" class="datepicker" value="<c:out value="${lec.licenseDate}"/>" /></td>
                                                        <script type="text/javascript">
                                                            $("#licenseExpDate").datepicker({
                                                                yearRange: '2014:2030', changeMonth: true, changeYear: true
                                                            });
                                                        </script> 
                                                    </tr>

                                                    <tr id="drivingLicenceInfo1"><td class="text2">License Type</td>
                                                        <td class="text2"><select class="textbox" name="licenseType" style="width:125px;">
                                                                <c:if test="${lec.licenseType == ''}">
                                                                    <option value="" selected> -- Select -- </option>
                                                                    <option value="LMV" >LMV</option>
                                                                    <option value="LMV With Batch" >LMV With Batch</option>
                                                                    <option value="Heavy" >Heavy</option>
                                                                </c:if>
                                                                <c:if test="${lec.licenseType == 'LMV'}">
                                                                    <option value="LMV" selected>LMV</option>
                                                                    <option value="LMV With Batch" >LMV With Batch</option>
                                                                    <option value="Heavy" >Heavy</option>
                                                                </c:if>
                                                                <c:if test="${lec.licenseType == 'LMV With Batch'}">
                                                                    <option value="LMV">LMV</option>
                                                                    <option value="LMV With Batch" selected>LMV With Batch</option>
                                                                    <option value="Heavy" >Heavy</option>
                                                                </c:if>
                                                                <c:if test="${lec.licenseType == 'Heavy'}">
                                                                    <option value="LMV">LMV</option>
                                                                    <option value="LMV With Batch">LMV With Batch</option>
                                                                    <option value="Heavy" selected>Heavy</option>
                                                                </c:if>
                                                            </select>
                                                        </td>
                                                        <td class="text2">State</td>
                                                        <td class="text2"><input type="text" name="licenseState" onKeyPress="return onKeyPressBlockNumbers(event);" maxlength="35" class="textbox" value="<c:out value="${lec.licenseState}"/>" /></td>
                                                    </tr>
                                                    <tr id="drivingLicenceInfo3">
                                                        <td class="text1" height="30"><font color="red">*</font>License Photo</td>
                                                        <td class="text1" height="30">
                                                            <input type="file" id="employeeLicense" name="employeeLicense" size="20" class="textbox" maxlength="10" value="0">
                                                        </td>
                                                        <td class="text1" colspan="2">&nbsp;</td>
                                                    </tr>
                                                    <!--
                                                                                <tr id="drivingLicenceInfo2"><td class="text1" style="display: none">Driver Type</td>
                                                                                    <td class="text1" style="display: none"><select class="textbox" name="driverType" style="width:125px;">
                                                    <c:if test="${lec.driverType == ''}">
                                                        <option value="" selected> -- Select -- </option>
                                                        <option value="PRR" >PRR</option>
                                                        <option value="ATT" >ATT</option>
                                                    </c:if>
                                                    <c:if test="${lec.driverType == 'PRR'}">
                                                        <option value="PRR" selected>PRR</option>
                                                        <option value="ATT" >ATT</option>
                                                    </c:if>
                                                    <c:if test="${lec.driverType == 'ATT'}">
                                                        <option value="PRR" >PRR</option>
                                                        <option value="ATT" selected>ATT</option>
                                                    </c:if>
                                                </select>
                                            </td>
                                            <td class="text1" style="display: none">Vendor Company/Introducer</td>
                                            <td class="text1" style="display: none"><input type="text" name="vendorCompany" id="vendorCompany" maxlength="45"class="textbox" value="<c:out value="${lec.vendorCompany}"/>"/></td>
                                        </tr>-->

                                                    <tr>
                                                        <td colspan="4" height="30" class="contenthead"><B>Salary Details</B></td>
                                                    </tr>
                                                    <tr><td class="text1">Salary Type</td>
                                                        <td class="text1"><select class="textbox" name="salaryType" style="width:125px;">
                                                                <c:if test="${lec.salaryType == ''}">
                                                                    <option value="" selected> -- Select -- </option>
                                                                    <option value="Daily" >Daily</option>
                                                                    <option value="Monthly" >Monthly</option>
                                                                </c:if>
                                                                <c:if test="${lec.salaryType == 'Daily'}">
                                                                    <option value="Daily" selected>Daily</option>
                                                                    <option value="Monthly" >Monthly</option>
                                                                </c:if>
                                                                <c:if test="${lec.salaryType == 'Monthly'}">
                                                                    <option value="Daily" >Daily</option>
                                                                    <option value="Monthly" selected>Monthly</option>
                                                                </c:if>

                                                            </select>
                                                        </td>
                                                        <td class="text1">Basic Salary</td>
                                                        <td class="text1"><input type="text" name="basicSalary" class="textbox" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10" value="<c:out value="${lec.basicSalary}"/>" /></td>
                                                    </tr>
                                                    <tr><td class="text2">Is Eligible For ESI</td>
                                                        <td class="text2">
                                                            <c:if test="${lec.esiEligible == 'Y'}">
                                                                <input type="radio" name="esiEligible" checked value="Y" /> Yes <input type="radio" name="esiEligible" value="N" /> No
                                                            </c:if>
                                                            <c:if test="${lec.esiEligible == 'N'}">
                                                                <input type="radio" name="esiEligible" value="Y" /> Yes <input type="radio" name="esiEligible" value="N" checked/> No
                                                            </c:if>
                                                        </td>
                                                        <td class="text2">Is Eligible For PF</td>
                                                        <td class="text2">
                                                            <c:if test="${lec.pfEligible == 'Y'}">
                                                                <input type="radio" name="pfEligible" checked value="Y" /> Yes <input type="radio" name="pfEligible" value="N" /> No
                                                            </c:if>
                                                            <c:if test="${lec.pfEligible == 'N'}">
                                                                <input type="radio" name="pfEligible" value="Y" /> Yes <input type="radio" name="pfEligible" value="N" checked /> No
                                                            </c:if>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text1">Bank Account No</td>
                                                        <td class="text1"><input type="text" name="bankAccountNo" id="bankAccountNo" maxlength="35" value="<c:out value="${lec.bankAccountNo}"/>"/></td>
                                                        <td class="text1">Bank Name</td>
                                                        <td class="text1"><input type="text" name="bankName" maxlength="25" class="textbox" value="<c:out value="${lec.bankName}"/>"  /></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text2">Bank Branch</td>
                                                        <td class="text2"><input type="text" name="bankBranchName" maxlength="25" class="textbox" value="<c:out value="${lec.bankBranchName}"/>"  /></td>
                                                        <td class="text2">A/C Holder Name</td>
                                                        <td class="text2"><input type="text" name="accountHolderName" maxlength="25" class="textbox" value="<c:out value="${lec.accountHolderName}"/>"  /></td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="4" height="30" class="contenthead"><B>Nominee Details</B></td>
                                                    </tr>
                                                    <tr><td class="text1">Name</td>
                                                        <td class="text1"><input type="text" name="nomineeName" class="textbox" onKeyPress="return onKeyPressBlockNumbers(event);" value="<c:out value="${lec.nomineeName}"/>" maxlength="35" />
                                                        </td>
                                                        <td class="text1">Relation</td>
                                                        <td class="text1"><select class="textbox" name="nomineeRelation" style="width:125px;">
                                                                <option value="0">-Select-</option>
                                                                <c:if test = "${relationList!= null}" >
                                                                    <c:forEach  items="${relationList}" var="rel">
                                                                        <option value='<c:out value="${rel.relationId}" />'<c:if test="${lec.nomineeRelation == rel.relationId}">selected</c:if>><c:out value="${rel.relationName}" /></option>
                                                                    </c:forEach >
                                                                </c:if>
                                                            </select></td>
                                                    </tr>
                                                    <tr><td class="text2">Date Of Birth</td>
                                                        <td class="text2"><input type="text" id="nomineeDob" name="nomineeDob" readonly="readOnly" size="20" class="datepicker" onchange="getNomineeAge(this.value)" value="<c:out value="${lec.nomineeDob}"/>" ></td>
                                                        <script type="text/javascript">
                                                            $("#nomineeDob").datepicker({
                                                                yearRange: '1900', changeMonth: true, changeYear: true
                                                            });
                                                        </script> 
                                                        <td class="text2">Age</td>
                                                        <td class="text2"><input type="text" name="nomineeAge" id="nomineeAge" class="textbox" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${lec.nomineeAge}"/>" /></td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="4" height="30" class="contenthead"><B>Previous Experience and Employer Details</B></td>
                                                    </tr>
                                                    <tr><td class="text1">Years Of Experience </td>
                                                        <td class="text1"><input type="text" name="yearOfExperience" class="textbox" maxlength="2" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${lec.yearOfExperience}"/>"  />
                                                        </td>
                                                        <td class="text1">Designation</td>
                                                        <td class="text1"><input type="text" name="preEmpDesignation" class="textbox" onKeyPress="return onKeyPressBlockNumbers(event);" maxlength="35" value="<c:out value="${lec.prevEmpDesignation}"/>" /></td>
                                                    </tr>
                                                    <tr><td class="text2">Company</td>
                                                        <td class="text2">
                                                            <input type="text" name="prevCompanyName" maxlength="35" onKeyPress="return onKeyPressBlockNumbers(event);" class="textbox" value="<c:out value="${lec.prevCompanyName}"/>" />
                                                        </td>
                                                        <td class="text2">Address</td>
                                                        <td class="text2"><input type="text" name="prevCompanyAddress" maxlength="45" class="textbox" value="<c:out value="${lec.prevCompanyAddress}"/>" /></td>
                                                    </tr>
                                                    <tr><td class="text1">City</td>
                                                        <td class="text1"><input type="text" name="prevCompanyCity" onKeyPress="return onKeyPressBlockNumbers(event);" maxlength="30" class="textbox" value="<c:out value="${lec.prevCompanyCity}"/>" />
                                                        </td>
                                                        <td class="text1">State</td>
                                                        <td class="text1"><input type="text" name="preEmpState" onKeyPress="return onKeyPressBlockNumbers(event);" class="textbox" maxlength="30" value="<c:out value="${lec.prevEmpState}"/>" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text2">Pin code</td>
                                                        <td class="text2"><input type="text" name="preEmpPincode" class="textbox" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="6" value="<c:out value="${lec.prevEmployeePincode}"/>"/></td>
                                                        <td class="text2">Contact No</td>
                                                        <td class="text2"><input type="text" name="prevCompanyContact" class="textbox" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="12" value="<c:out value="${lec.prevCompanyContact}"/>" />
                                                        </td>

                                                    </tr>
                                                    </table>
                                                    <table id="allowanceTBL" border="0" align="center" width="800" cellpadding="0" cellspacing="0"  ><center>Family Details</center>
                                                        <tr>
                                                            <td class="contenthead">S.No</td>
                                                            <td class="contenthead">Name</td>
                                                            <td class="contenthead">Relation</td>
                                                            <td class="contenthead">Date Of Birth</td>
                                                            <td class="contenthead">Age</td>
                                                            <td class="contenthead">Delete <input type="hidden" name="empRelationSize" id="empRelationSize" class="textbox" value="<c:out value="${familyListSize}"/>" /></td>
                                                        </tr>
                                                        <c:if test="${familyList != null}">
                                                            <tr>
                                                                <c:forEach items="${familyList}" var="fam">
                                                                    <td><%=snumber++%></td>

                                                                    <td><input type="hidden" name="empRelationId" id="empRelationId<%=snumber%>" class="textbox" value="<c:out value="${fam.empRelationId}"/>" /><input type="text" name="relationName" id="relationName<%=snumber%>" class="textbox" value="<c:out value="${fam.relationNames}"/>" /></td>
                                                                    <td><select class="textbox" name="relation" id="relation<%=snumber%>" style="width:125px;">
                                                                            <option value='0'>-Select-</option>
                                                                            <c:if test = "${relationList!= null}" >
                                                                                <c:forEach  items="${relationList}" var="rel">
                                                                                    <option value='<c:out value="${rel.relationId}" />'<c:if test="${rel.relationId == fam.relationId}">selected</c:if>><c:out value="${rel.relationName}" /></option>
                                                                                </c:forEach >
                                                                            </c:if>
                                                                        </select></td>
                                                                    <td><input type="text" name="relationDOB" id="relationDOB<%=snumber%>"  class="datepicker" value="<c:out value="${fam.relationDobs}"/>" onchange="showAge(<%=snumber%>)"/></td>
                                                                    <td><input type="text" name="relationAge" id="relationAge<%=snumber%>" class="textbox" readonly value="<c:out value="${fam.relationAges}"/>" /></td>
                                                                    <td><input type="checkbox" name="delete" id="delete<%=snumber%>"/></td>
                                                                </tr>
                                                            </c:forEach>
                                                        </c:if>
                                                    </table>
                                                    <script>
                                                        addRow();
                                                    </script>    
                                                    <center>
                                                        <input type="button" value="Add Row" onclick="addRow()" />
                                                        <input type="button" value="Delete Row" onclick="deleteRow()" />
                                                    </center>
                                                    <table border="0" align="center" width="800" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td height="30" colspan='4' align="center" class="text1"><font color="#FF0033" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; "><div align="center" id="userNameStatus" style="height:25px; "></div></font>  </td>
                                                        </tr>


                                                        <c:if test="${lec.userId != 0}" >
                                                            <%status++;%>
                                                            <c:if test="${userDetails != null}">
                                                                <c:forEach items="${userDetails}" var="usr">
                                                                    <tr>
                                                                        <td height="30" class="text2">Click here if need user privilege</td>
                                                                        <td height="30" class="text2"><input type="checkbox" class="textbox" name="userStatus" id="userStatus" onClick="showTab();"  /></td>
                                                                        <td height="30" class="text2">&nbsp;</td>
                                                                        <td height="30" class="text2">&nbsp;</td>
                                                                    </tr>


                                                                    <tr id="userStat" style="visibility:hidden;">
                                                                        <td height="30" class="text2"><font color="red">*</font>User Name :</td>
                                                                        <td height="30" class="text2"><input type="hidden" class="textbox" name="staffUserId" id="staffUserId"  value="<c:out value="${usr.userId}"/>" /><input type="text" class="textbox" name="userName"  onchange="getProfile(this.value)" value="<c:out value="${usr.userName}"/>" /></td>
                                                                        <c:set var="pass" value="${usr.password}"/>
                                                                        <%
                                                                                                    Object pass = (Object) pageContext.getAttribute("pass");

                                                                                                    String password = (String) pass;
                                                                                                    CryptoLibrary cl = new CryptoLibrary();
                                                                                                    password = cl.decrypt(password);
                                                                        %>
                                                                        <td height="30" class="text2"><font color="red">*</font> Password :</td>
                                                                        <td height="30" class="text2"><input type="password" maxlength='10' name="password" class="textbox"  value="<%=password%>"  /></td>
                                                                    </tr>
                                                                    <tr id="userStat1"  style="visibility:hidden;">
                                                                        <td class="text1" height="30"><font color="red">*</font>User Role</td>
                                                                        <td class="text1" height="30"><select class="textbox" name="roleId" style="width:125px;">
                                                                                <option value="0">-Select-</option>
                                                                                <c:if test = "${roleList != null}" >
                                                                                    <c:forEach  items="${roleList}" var="role">
                                                                                        <option value='<c:out value="${role.roleId}" />'<c:if test="${role.roleId == usr.roleId}">selected</c:if>><c:out value="${role.roleName}" /></option>
                                                                                    </c:forEach >
                                                                                </c:if>
                                                                            </select></td>

                                                                        <td class="text1" height="30">&nbsp;</td>
                                                                        <td class="text1" height="30">&nbsp;</td>
                                                                    </tr>
                                                                </c:forEach>
                                                            </c:if>

                                                        </c:if>

                                                        <c:if test="${lec.userId == 0}" >
                                                            <%status--;%>
                                                            <input type="checkbox" style="visibility:hidden;" class="textbox" name="userStatus"  />
                                                        </c:if>

                                                    </table>
                                                    <br>
                                                        <center>
                                                            <input type="button" class="button" value="Save" name="save" onClick="submitPage(this.name)"/>
                                                        </center>

                                                    </c:forEach>
                                                </c:when>
                                            </c:choose>
                                            <%            }
                                            %>

                                            <bR>

                                                <input type="hidden" name="userCheck" value='' >
                                                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                                                    <SCRIPT language="javascript">
                                                        function checkStatus() {
                                                        <%if (status == 1) {%>
                                                            document.getElementById("userStatus").checked = true
                                                            showTab();
                                                        <%}%>
                                                        }

                                                        var rowCount = <%=snumber%>
                                                        var snumber = 1;
                                                        //  rowCount = len;
                                                        function addRow()
                                                        {
                                                            var tab = document.getElementById("allowanceTBL");
                                                            rowCount = tab.rows.length;

                                                            //alert(rowCount);
                                                            snumber = (rowCount) - 1;
                                                            //alert(snumber+1);
                                                            var newrow = tab.insertRow((snumber) + 1);
                                                            newrow.height = "30px";
                                                            var sno = snumber + 1;
                                                            snumber = snumber + 2;
                                                            // var temp = sno1-1;
                                                            var cell = newrow.insertCell(0);
                                                            var cell0 = "<td class='text1'> " + sno + "</td>";
                                                            //cell.setAttribute(cssAttributeName,"text1");
                                                            cell.innerHTML = cell0;

                                                            cell = newrow.insertCell(1);
                                                            cell0 = "<td class='text1'><input type='text' name='relationName' onKeyPress='return onKeyPressBlockNumbers(event);' maxlength='35' id='relationName" + snumber + "' class='textbox' /></td>";
                                                            //cell.setAttribute(cssAttributeName,"text1");
                                                            cell.innerHTML = cell0;

                                                            cell = newrow.insertCell(2);
                                                            cell0 = "<td class='text1'><select class='textbox' name='relation' id='relation" + snumber + "' style='width:125px;'><option value='0'>-Select-</option><c:if test = "${relationList!= null}" ><c:forEach  items="${relationList}" var="rel"><option value='<c:out value="${rel.relationId}" />'><c:out value="${rel.relationName}" /></option></c:forEach ></c:if></select></td>";
                                                            //cell.setAttribute(cssAttributeName,"text1");
                                                            cell.innerHTML = cell0;

                                                            cell = newrow.insertCell(3);
                                                            cell0 = "<td class='text1'><input type='text' name='relationDOB' id='relationDOB" + snumber + "'  class='datepicker' onchange='showAge(" + snumber + ")' /></td>";
                                                            //cell.setAttribute(cssAttributeName,"text1");
                                                            cell.innerHTML = cell0;
                                                            $("#relationDOB" + snumber).datepicker({
                                                                yearRange: '1900', changeMonth: true, changeYear: true
                                                            });

                                                            cell = newrow.insertCell(4);
                                                            cell0 = "<td class='text1'><input type='text' name='relationAge' id='relationAge" + snumber + "'  class='textbox' /></td>";
                                                            //cell.setAttribute(cssAttributeName,"text1");
                                                            cell.innerHTML = cell0;
                                                            if (rowCount == 1) {
                                                                cell = newrow.insertCell(5);
                                                                cell0 = "<td class='text1'</td>";
                                                                //cell.setAttribute(cssAttributeName,"text1");
                                                                cell.innerHTML = cell0;
                                                            } else {
                                                                cell = newrow.insertCell(5);
                                                                cell0 = "<td class='text1'><input type='checkbox' name='delete' id='delete" + snumber + "'  /></td>";
                                                                //cell.setAttribute(cssAttributeName,"text1");
                                                                cell.innerHTML = cell0;
                                                            }
                                                            rowCount++;



                                                            $(document).ready(function() {
                                                                //alert('hai');
                                                                $("#datepicker").datepicker({
                                                                    showOn: "button",
                                                                    buttonImage: "calendar.gif",
                                                                    buttonImageOnly: true

                                                                });



                                                            });

                                                            $(function() {
                                                                //	alert("cv");
                                                                $(".datepicker").datepicker({
                                                                    /*altField: "#alternate",
                                                                     altFormat: "DD, d MM, yy"*/
                                                                    changeMonth: true, changeYear: true
                                                                });

                                                            });
                                                        }

                                                        function deleteRow() {
                                                            try {
                                                                var table = document.getElementById("allowanceTBL");
                                                                rowCount = table.rows.length;
                                                                for (var i = 0; i < rowCount; i++) {
                                                                    var row = table.rows[i];
                                                                    var chkbox = row.cells[5].childNodes[0];
                                                                    if (null != chkbox && true == chkbox.checked) {
                                                                        if (rowCount <= 1) {
                                                                            alert("Cannot delete all the rows.");
                                                                            break;
                                                                        }
                                                                        table.deleteRow(i);
                                                                        rowCount--;
                                                                        i--;
                                                                        snumber = i;
                                                                    }
                                                                    //alert(snumber);
                                                                }
                                                            } catch (e) {
                                                                alert(e);
                                                            }
                                                        }
                                                        function showAge(value) {
                                                            //                            alert(value);
                                                            var dob = $('#relationDOB' + value).val();
                                                            if (dob != '') {
                                                                var today = new Date();
                                                                today = today.getFullYear();
                                                                //                        alert(today);
                                                                var dobYear = dob.split("-");
                                                                //                        alert(dobYear[2]);

                                                                var age = Math.floor((today - dobYear[2]));

                                                                //                        alert(age);
                                                                $('#relationAge' + value).val(age);

                                                            }


                                                        }

                                                        function getNomineeAge(value) {
                                                            if (value != '') {
                                                                var today = new Date();
                                                                today = today.getFullYear();
                                                                //                        alert(today);
                                                                var dobYear = value.split("-");
                                                                //                        alert(dobYear[2]);

                                                                var age = Math.floor((today - dobYear[2]));

                                                                //                        alert(age);
                                                                $('#nomineeAge').val(age);

                                                            }
                                                        }
                                                    </SCRIPT>
                                                    </body>
                                                    </html>
