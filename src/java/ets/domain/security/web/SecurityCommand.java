package ets.domain.security.web;

/**
 *
 * @author vidya
 */
public class SecurityCommand {

    public SecurityCommand() {
    }
    String currentDate = "";
    String woDate = "";
    String vno = "";
    String woId = "";
    String vehicleOperationPoint = "";
    String vehicleOperationPointId = "";
    String vehicleTypeName = "";
    String vehicleTypeId = "";
    String modelId = "";
    String vehicleNumber = "";
    String vehicleKm = "";
    String vehicleHm = "";
        
    
    String remarks = "";
    String vehicleId = "";
    String[] approvedIds;
    String[] issuedQtys;
    String[] woIds = null;
    String[] vehicleIds = null;
    String[] vehicleNumbers = null;
    String[] in = null;
    String[] out = null;
    String userId = "";
    String[] inKm = null;
    String[] inHm = null;
    String[] compId = null;
    String[] purposeId = null;
    String[] vioId = null;
    String[] vehicleRemarks = null;
    String purpose = "";
    String vehicleDate = "";
    String vehicleFromDate  = "";
    String vehicleToDate = "";
    String vehicleInPurpose  = "";
    String vehicleStatus = "";

    public String getVehicleFromDate() {
        return vehicleFromDate;
    }

    public void setVehicleFromDate(String vehicleFromDate) {
        this.vehicleFromDate = vehicleFromDate;
    }

    public String getVehicleInPurpose() {
        return vehicleInPurpose;
    }

    public void setVehicleInPurpose(String vehicleInPurpose) {
        this.vehicleInPurpose = vehicleInPurpose;
    }

    public String getVehicleStatus() {
        return vehicleStatus;
    }

    public void setVehicleStatus(String vehicleStatus) {
        this.vehicleStatus = vehicleStatus;
    }

    public String getVehicleToDate() {
        return vehicleToDate;
    }

    public void setVehicleToDate(String vehicleToDate) {
        this.vehicleToDate = vehicleToDate;
    }
    
    

    public String getVehicleDate() {
        return vehicleDate;
    }

    public void setVehicleDate(String vehicleDate) {
        this.vehicleDate = vehicleDate;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }


    public String[] getOut() {
        return out;
    }

    public void setOut(String[] out) {
        this.out = out;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String[] getIn() {
        return in;
    }

    public void setIn(String[] in) {
        this.in = in;
    }

    public String[] getVehicleIds() {
        return vehicleIds;
    }

    public void setVehicleIds(String[] vehicleIds) {
        this.vehicleIds = vehicleIds;
    }

    public String[] getVehicleNumbers() {
        return vehicleNumbers;
    }

    public void setVehicleNumbers(String[] vehicleNumbers) {
        this.vehicleNumbers = vehicleNumbers;
    }

    public String[] getWoIds() {
        return woIds;
    }

    public void setWoIds(String[] woIds) {
        this.woIds = woIds;
    }

    public String[] getApprovedIds() {
        return approvedIds;
    }

    public void setApprovedIds(String[] approvedIds) {
        this.approvedIds = approvedIds;
    }

    public String[] getIssuedQtys() {
        return issuedQtys;
    }

    public void setIssuedQtys(String[] issuedQtys) {
        this.issuedQtys = issuedQtys;
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getVehicleOperationPoint() {
        return vehicleOperationPoint;
    }

    public void setVehicleOperationPoint(String vehicleOperationPoint) {
        this.vehicleOperationPoint = vehicleOperationPoint;
    }

    public String getVehicleOperationPointId() {
        return vehicleOperationPointId;
    }

    public void setVehicleOperationPointId(String vehicleOperationPointId) {
        this.vehicleOperationPointId = vehicleOperationPointId;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    public String getWoDate() {
        return woDate;
    }

    public void setWoDate(String woDate) {
        this.woDate = woDate;
    }

    public String getWoId() {
        return woId;
    }

    public void setWoId(String woId) {
        this.woId = woId;
    }

    public String[] getCompId() {
        return compId;
    }

    public void setCompId(String[] compId) {
        this.compId = compId;
    }

    public String[] getInHm() {
        return inHm;
    }

    public void setInHm(String[] inHm) {
        this.inHm = inHm;
    }

    public String[] getInKm() {
        return inKm;
    }

    public void setInKm(String[] inKm) {
        this.inKm = inKm;
    }

    public String[] getPurposeId() {
        return purposeId;
    }

    public void setPurposeId(String[] purposeId) {
        this.purposeId = purposeId;
    }

    public String[] getVioId() {
        return vioId;
    }

    public void setVioId(String[] vioId) {
        this.vioId = vioId;
    }

    public String[] getVehicleRemarks() {
        return vehicleRemarks;
    }

    public void setVehicleRemarks(String[] vehicleRemarks) {
        this.vehicleRemarks = vehicleRemarks;
    }

    public String getVno() {
        return vno;
    }

    public void setVno(String vno) {
        this.vno = vno;
    }

    public String getVehicleHm() {
        return vehicleHm;
    }

    public void setVehicleHm(String vehicleHm) {
        this.vehicleHm = vehicleHm;
    }

    public String getVehicleKm() {
        return vehicleKm;
    }

    public void setVehicleKm(String vehicleKm) {
        this.vehicleKm = vehicleKm;
    }
    
}
