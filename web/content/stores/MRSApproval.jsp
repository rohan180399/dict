<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<!--<title>PAPL</title>-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
                    format: "dd-mm-yyyy",
                    autoclose: true,
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
                    format: "dd-mm-yyyy",
                    autoclose: true
        });

    });


</script>

<script>



function newWO(){
        window.open('/throttle/content/stores/OtherServiceStockAvailability.html', 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
    }

function showTable()
{
    document.getElementById("showTable").style.visibility = "visible";
}


function submt()
{
    if(validate() == '0'){
        return
    }
    
    else{        
    document.spareIssue.action = "/throttle/handleMrsApprovalSummary.do";    
    document.spareIssue.submit();
    }
}

function submitPag(val){
    if(val =='search'){
        if(document.spareIssue.toDate.value==''){
            alert("Please Enter From Date");
        }else if(document.spareIssue.fromDate.value==''){
            alert("Please Enter to Date");
        }    
        
        document.spareIssue.action = '/throttle/MRSApproval.do'
        document.spareIssue.submit();    
    }
} 


            function setDate(fDate,tDate){
                if(fDate != 'null'){
                document.spareIssue.fromDate.value=fDate;
                }
                if(tDate != 'null'){
                document.spareIssue.toDate.value=tDate;
                }
                                
            }


function validate()
{
var selectedMrs = document.getElementsByName("selectedIndex");
var counter=0;

    for(var i=0;i<selectedMrs.length;i++){
        if(selectedMrs[i].checked == 1){
            counter++;                        
        }       
    }
    if(counter==0){
        alert("Please Select Any One Mrs");
        return '0';
    }     
    return counter;
}

</script>



<script>
   function changePageLanguage(langSelection){
   if(langSelection== 'ar'){
   document.getElementById("pAlign").style.direction="rtl";
   }else if(langSelection== 'en'){
   document.getElementById("pAlign").style.direction="ltr";
   }
   }
 </script>

<div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.mrsApproval"  text="MRS Approval"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="stores.label.stores"  text="Stores"/></a></li>
          <li class="active"><spring:message code="stores.label.mrsApproval"  text="MRS Approval"/></li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">




<body onLoad="setDate('<%= request.getAttribute("fromDate") %>','<%= request.getAttribute("toDate") %>')">        

<form name="spareIssue" method="post" >

<% int index=0;
int oddEven =0;
    String classText = ""; %>    

            <!--<div class="panel panel-default">-->
<!--                <div class="panel-heading">
                    <div class="panel-btns">
                      <a href="" class="panel-close">&times;</a>-->
                        <!--<a href="" class="minimize">&minus;</a>-->
                    <!--</div> panel-btns -->
                <!--</div>-->
            <!--<div class="panel-body">-->
<td colspan="4"  style="background-color:#5BC0DE;">
            <table class="table table-info mb30 table-hover">
                <thead>
                    <tr>
                        <th colspan="5"><spring:message code="stores.label.mrsApproval"  text="MRS Approval"/>
                        </th>
                    </tr>
                    </thead>
                    
                <tr>
                    <td ><spring:message code="stores.label.FromDate"  text="default text"/>
</td>
                    <td >
                        <input type="text"  style="width:260px;height:40px;" class="form-control pull-right datepicker"  name="fromDate" value="" >
<!--                        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.spareIssue.fromDate,'dd-mm-yyyy',this)"/>    -->
                    </td>               
                    <td ><spring:message code="stores.label.TODate"  text="default text"/></td>
                    <td >
                        <input type="text"  style="width:260px;height:40px;"class="form-control pull-right"   readonly name="toDate" value="" >
<!--                        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.spareIssue.toDate,'dd-mm-yyyy',this)"/>-->
                    </td>
<td></td>
                </tr>  
                <tr >
                    <td></td>
                    <td></td>
                    <td align="center" >
                        <input type="button" class="btn btn-success" readonly name="search" value="<spring:message code="stores.label.SEARCH"  text="default text"/>" onClick="submitPag('search');" >
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
</td>



    <c:if test = "${mrsLists != null}" >
            <table class="table table-info mb30 table-hover" id="table">
            <thead>
               
                <tr>
                    <th ><spring:message code="stores.label.QueueNo"  text="default text"/></th>
                    <th ><spring:message code="stores.label.JobCardNo"  text="default text"/></th>
                    <th ><spring:message code="stores.label.MRSNo"  text="default text"/></th>
                    <th ><spring:message code="stores.label.VehicleNo"  text="default text"/></th>
                    <th ><spring:message code="stores.label.CreatedDate/Time"  text="default text"/></th>
                    <th ><spring:message code="stores.label.Approval"  text="default text"/></th>
                    <th ><spring:message code="sales.label.Action"  text="Action"/></th>
                    
                </tr>
            </thead>


      <c:forEach items="${mrsLists}" var="mrs"> 
<%
    oddEven = index % 2;
    if (oddEven > 0) {
    classText = "text2";
    } else {
    classText = "text1";
    }
    %>
                <tr>
                    <td ><%=index+1%></td>
                    <td ><c:out value="${mrs.mrsJobCardNumber}"/></td>
                    <td ><a href="/throttle/MRSDetail.do?mrsId=<c:out value="${mrs.mrsNumber}"/>" ><c:out value="${mrs.mrsNumber}"/></a></td>
                    <input type="hidden" name="mrsIds" value="<c:out value="${mrs.mrsNumber}"/>" >
                    <td ><c:out value="${mrs.mrsVehicleNumber}"/></td>
                    <td ><c:out value="${mrs.mrsCreatedDate}"/></td>
                    <c:if test="${mrs.mrsStatus=='Hold'}" >
                    <td ><a href="/throttle/MRSApprovalScreen.do?mrsId=<c:out value="${mrs.mrsNumber}"/>" ><spring:message code="sales.label.Approve"  text="Action"/></a></td>
                    </c:if>
                    <c:if test="${mrs.mrsStatus!='Hold'}" >
                    <td ><a href="/throttle/mrsStatus.do?status=<c:out value="${mrs.mrsStatus}"/>&mrsId=<c:out value="${mrs.mrsNumber}"/>&mrsJobCardNumber=<c:out value="${mrs.mrsJobCardNumber}"/>"><spring:message code="sales.label.View"  text="Action"/></a></td>
                    </c:if>                    
                    <td ><input type="checkbox" name="selectedIndex" value="<%= index %>" class="form-control" > </td>
                    <input type="hidden" name="directToPage" value="mrsApprovalList" > 
                 </tr>
 <%
   index++;
 %>
</c:forEach>
              
            </table>
            <br>
            <center> <input type="button" name="show" class="btn btn-success" value="<spring:message code="stores.label.SHOW" text="default text"/>" onClick="submt();"> </center>
</c:if>    
<br>
    
<c:if test="${ mrsSummaryList!=null }" >
            <table class="table table-info mb30 table-hover" id="table">
                <tr>
                    <td colspan="12" align="center" class="contenthead" height="30"><spring:message code="stores.label.RequiredItems" text="default text"/>
                    </td>
                </tr>
                <thead>
                <tr>
                    <th ><spring:message code="stores.label.Item" text="default text"/><br> <spring:message code="stores.label.Code" text="default text"/></th>
                    <th ><spring:message code="stores.label.PAPL" text="default text"/><br> <spring:message code="stores.label.Code" text="default text"/></th>
                    <th ><spring:message code="stores.label.Item" text="default text"/> <br><spring:message code="stores.label.Name" text="default text"/></th>
                    <th ><spring:message code="stores.label.New" text="default text"/><br><spring:message code="stores.label.Stock" text="default text"/></th>
                    <th ><spring:message code="stores.label.RC" text="default text"/><br><spring:message code="stores.label.Stock" text="default text"/></th>
                    <th ><spring:message code="stores.label.OtherServPointStock" text="default text"/></th>
                    <th ><spring:message code="stores.label.Required" text="default text"/><br> <spring:message code="stores.label.Qty" text="default text"/></th>
                    <th ><spring:message code="stores.label.PORaised" text="default text"/><br> <spring:message code="stores.label.Qty" text="default text"/></th>
                    <th ><spring:message code="stores.label.Local" text="default text"/><br> <spring:message code="stores.label.PurchQty" text="default text"/></th>
                    
                </tr>
                </thead>
<%  index = 0; %>                

<c:forEach items="${mrsSummaryList}" var="mrsSumm">    
<%    
    oddEven = index % 2;
    if (oddEven > 0) {
    classText = "text2";
    } else {
    classText = "text1";
    }
    %>

                <tr>
                    <input type="hidden" name="itemIds" value=<c:out value="${mrsSumm.mrsItemId}" /> >
                    <td > <c:out value="${mrsSumm.mrsItemMfrCode }" /> </td>
                    <td ><c:out value="${mrsSumm.mrsPaplCode}" /></td>
                    <td ><c:out value="${mrsSumm.mrsItemName}" /></td>
                    <td ><c:out value="${mrsSumm.mrsLocalServiceItemSum}" /></td>
                    <td ><c:out value="${mrsSumm.rcQty}" /></td>
                    <td ><c:out value="${mrsSumm.mrsOtherServiceItemSum}" /></td>
                    <td ><c:out value="${mrsSumm.requiredQty}" /></td>
                    <td ><c:out value="${mrsSumm.vendorPoQty}" /></td>
                    <td ><c:out value="${mrsSumm.localPoQty}" /></td>                    
                </tr>    
<% index++; %>    
</c:forEach>   
   
</table>
<input type="hidden" name="purchaseType" value="" >
<br>   
</c:if> 
    
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
        </div>


        </div>
        </div>


<%@ include file="/content/common/NewDesign/settings.jsp" %>
