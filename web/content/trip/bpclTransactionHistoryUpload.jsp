<%-- 
    Document   : bpclTransactionHistoryUpload
    Created on : Jan 04, 2014, 12:38:56 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<script type="text/javascript" src="/throttle/js/suest"></script>
<script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
<script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
    function submitPage(value) {
        if (value == 'Proceed') {
            document.upload.action = '/throttle/saveBpclTransactionHistory.do';
            document.upload.submit();
        } else {
            document.upload.action = '/throttle/bpclTransactionHistoryUpload.do';
            document.upload.submit();
        }
    }
</script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>    
    <body>
        <form name="upload" method="post" enctype="multipart/form-data">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <br>

            <%
            if(request.getAttribute("errorMessage")!=null){
            String errorMessage=(String)request.getAttribute("errorMessage");                
            %>
            <center><b><font color="red" size="1"><%=errorMessage%></font></b></center>
                        <%}%>
            <table border="0" cellpadding="0" cellspacing="0" width="780" align="center">
                <tr>
                    <td class="contenthead" colspan="4">BPCL Transaction History Upload</td>
                </tr>
                <tr>
                    <td class="text2">Select file</td>
                    <td class="text2"><input type="file" name="importBpclTransaction" id="importBpclTransaction" class="importBpclTransaction"></td>                             
                    <td class="text2">Last Transaction Date</td>
                    <td class="text2"><c:out value="${lastBpclTxDate}"/></td>                             
                </tr>
                <tr>
                    <td colspan="4" class="text1" align="center" ><input type="button" value="Submit" name="Submit" onclick="submitPage(this.value)" >
                </tr>
            </table>
            <br>
            <br>
            <br>
            <% int i = 1 ;%>
            <% int index = 0;%> 
            <%int oddEven = 0;%>
            <%  String classText = "";%>    
            <c:if test="${shippingBillNoList != null}">
                <table>
                    <th class="contenthead" colspan="15">File Uploaded Details&nbsp;:</th>
                    <tr>
                        <td class="contenthead">S No</td>
                        <td class="contenthead">ContainerNo</td>
                        <td class="contenthead">ContainerSize</td>
                        <td class="contenthead">ContainerType</td>
                        <td class="contenthead">ShippingBillNo</td>
                        <td class="contenthead">RequestNo</td>
                        <td class="contenthead">ShippingBillDate</td>
                        <td class="contenthead">GateInDate</td>
                        <td class="contenthead">VehicleNo</td>
                        <td class="contenthead">NoOfContainer</td>
                    </tr>
                    <c:forEach items="${shippingBillNoList}" var="bpcl">
                        <%
                      oddEven = index % 2;
                      if (oddEven > 0) {
                          classText = "text2";
                      } else {
                          classText = "text1";
                        }
                        %>
                        <tr>
                                <td class="<%=classText%>" ><font color="red"><%=i++%></font></td>
                                <td class="<%=classText%>" ><font color="red"><input type="hidden" name="containerNo" id="containerNo" value="<c:out value="${bpcl.containerNo}"/>" /><c:out value="${bpcl.containerNo}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><input type="hidden" name="containerSize" id="containerSize" value="<c:out value="${bpcl.containerTypeName}"/>" /><c:out value="${bpcl.containerTypeName}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><input type="hidden" name="containerType" id="containerType" value="<c:out value="${bpcl.containerTypeId}"/>" /><c:out value="${bpcl.containerTypeId}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><input type="hidden" name="shippingBillNo" id="shippingBillNo" value="<c:out value="${bpcl.shipingLineNo}"/>" /><c:out value="${bpcl.shipingLineNo}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><input type="hidden" name="requestNo" id="requestNo" value="<c:out value="${bpcl.requestNo}"/>" /><c:out value="${bpcl.requestNo}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><input type="hidden" name="shippingBillDate" id="shippingBillDate" value="<c:out value="${bpcl.shippingBillDate}"/>" /><c:out value="${bpcl.shippingBillDate}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><input type="hidden" name="gateInDate" id="gateInDate" value="<c:out value="${bpcl.gateInDate}"/>" /><c:out value="${bpcl.gateInDate}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><input type="hidden" name="vehicleNo" id="vehicleNo" value="<c:out value="${bpcl.vehicleNo}"/>" /><c:out value="${bpcl.vehicleNo}"/></font></td>
                                <td class="<%=classText%>" ><font color="red"><input type="hidden" name="containerQty" id="containerQty" value="<c:out value="${bpcl.containerQty}"/>" /><c:out value="${bpcl.containerQty}"/></font></td>

                        </tr>
                        <%index++;%>
                    </c:forEach>
                </table>
                <br>
                <br>
                <br>
                <center>
                    <input type="button" class="button" value="Proceed" onclick="submitPage(this.value)"/>
                </center>

            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>