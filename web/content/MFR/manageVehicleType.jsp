
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
     <%@page language="java" contentType="text/html; charset=UTF-8"%>
 <%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

    <script language="javascript">
        function submitPage(value)
        {
            if (value=='add')
                {
                    document.desigDetail.action ='/throttle/addTypePage.do';
                }else if(value == 'modify'){
                
                document.desigDetail.action ='/throttle/alterTypePage.do';
            }
            document.desigDetail.submit();
        }
    </script>
    <div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="trucks.label.VehicleType"  text="VehicleType"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="stores.label.Vehicle"  text="Vehicle"/></a></li>
          <li class="active"><spring:message code="trucks.label.VehicleType"  text="VehicleType"/></li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">
    <body>
        
        <form method="post" name="desigDetail">
<%@ include file="/content/common/message.jsp" %>
 <% int index = 0;  %>    
            <br>
            <c:if test = "${TypeList != null}" >
                <table class="table table-info mb30 table-hover">
                    <thead>
                   
                    <tr align="center">
                        <th height="20" ><div ><spring:message code="trucks.label.SNo"  text="default text"/></div></th>
                        <th height="20" ><div ><spring:message code="trucks.label.VehicleType"  text="default text"/></div></th>
                        <th height="20" ><div ><spring:message code="trucks.label.VehicleTonnage"  text="default text"/></div></th>
                        <th height="20" ><div ><spring:message code="trucks.label.VehicleCapacity(CBM)"  text="default text"/></div></th>
                        <th height="20" ><div ><spring:message code="trucks.label.Description"  text="default text"/></div></th>
                        <th height="20" ><div ><spring:message code="trucks.label.Status"  text="default text"/></div></th>
                       
                    </tr>
                    </thead>
                    <%

                    %>
                    
                    <c:forEach items="${TypeList}" var="list"> 	
                        <%

            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr  width="420" height="20" >
                            <td><%=index + 1%></td>
                            <td><input type="hidden" name="typeId" value='<c:out value="${list.typeId}"/>'> <c:out value="${list.typeName}"/></td>
                            <td>&nbsp;&nbsp;<c:out value="${list.tonnage}"/></td>
                            <td><c:out value="${list.capacity}"/></td>
                            <td><c:out value="${list.description}"/></td>
                            <td>                                
                                <c:if test = "${list.activeInd == 'Y'}" >
                                    <spring:message code="trucks.label.Active"  text="default text"/>
                                </c:if>
                                <c:if test = "${list.activeInd == 'N'}" >
                                    <spring:message code="trucks.label.InActive"  text="default text"/>
                                </c:if>
                            </td>
                             
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach >
                    
                </table>
            </c:if> 
            <center>
                <br>
                <input type="button" name="add" value="<spring:message code="trucks.label.Add"  text="default text"/>" onClick="submitPage(this.name)" class="btn btn-success" style="display: none">
                <c:if test = "${TypeList != null}" >
                    <input type="button" value="<spring:message code="trucks.label.Alter"  text="default text"/>" name="modify" onClick="submitPage(this.name)" class="btn btn-success" style="display: none">
                </c:if>
                <input type="hidden" name="reqfor" value="">
            </center>
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>


    </div>
    </div>





<%@ include file="/content/common/NewDesign/settings.jsp" %>