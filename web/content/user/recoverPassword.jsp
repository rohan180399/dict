<%-- 
Document   : recoverPassword
Created on : Dec 27, 2008, 2:56:39 PM
Author     : Sabareesh
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
   <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import = "ets.domain.users.business.LoginTO" %>
<%@ page import="java.util.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<title>JSP Page</title>
<script src="/throttle/js/validate.js"></script>
<script>
function submitPage(value){

if(isEmpty(document.modifyuser.userName.value)){
alert("User Name Should Not be Empty");
document.modifyuser.userName.focus();
}else if(value == "recover"){
document.modifyuser.action='/throttle/recoverPass.do';
document.modifyuser.submit();
}

}

function setValue(){
document.modifyuser.userName.focus();
var userName = '<%=request.getAttribute("userName") %>';
if(userName != 'null'){
document.modifyuser.userName.value = userName;
}
}
</script>
<!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/ 
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->

<div class="pageheader">
      <h2><i class="fa fa-edit"></i><spring:message code="settings.label.RecoverPassword" text="default text"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="header.label.YouAreHere" text="default text"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="header.label.Home" text="default text"/></a></li>
          <li><a href="general-forms.html"><spring:message code="header.label.Setting" text="default text"/></a></li>
          <li class=""><spring:message code="settings.label.RecoverPassword" text="default text"/></li>
                  </ol>
      </div>
      </div>
<div class="contentpanel">
            <div class="panel panel-default">
             <div class="panel-body">
<body onLoad="setValue()">
<form method="post" name="modifyuser">
<!-- copy there from end -->
<!-- copy there from end -->
<!-- pointer table -->
<!-- pointer table -->

<% 
if(request.getAttribute("userPassword") !=null){
%>
<div align="center"  style="height:30px;color:green;font-size: 18px;"><center><b><spring:message code="settings.label.Thepasswordis" text="default text"/> <%=request.getAttribute("userPassword")  %></b></center></div>
<%
}
%>
            <!--<table class="table table-bordered">-->
<%@ include file="/content/common/message.jsp" %>
  <table  class="table table-info mb30 table-hover" align="center">
 
<tr>
<input type="hidden" name="userId" value='<%= session.getAttribute("userId")%>'/> 
<td width="176"  height="30" style="border-top-color:#5BC0DE;" align="center"> <spring:message code="settings.label.Username" text="default text"/>  </td>
<td width="172"  height="30" style="border-top-color:#5BC0DE;"><input type="text" style="width:260px;height:40px;"  class="form-control"  name="userName" value=""> </td>   
</tr>
</table>
<!--</table>-->
<center>
    <input type="button"  class="btn btn-success" value="<spring:message code="settings.label.RECOVER" text="default text"/>" name="recover" onClick="submitPage(this.name)" class="button"/>
</center>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
