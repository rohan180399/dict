    /*----------------------------------------------------------------------------
      * FPBaseController.java
      * Jun 19, 2008
      *
      * Copyright (c) ES Systems.
      * All Rights Reserved.
      *
      * This software is the confidential and proprietary information of
      * ES Systems ("Confidential Information"). You shall
      * not disclose such Confidential Information and shall use it only in
      * accordance with the terms of the license agreement you entered into
      * with ES Systems.
      --------------------------------------------------------------------------*/

    package ets.arch.web;
    /**
    *
    * Modification Log
    * ---------------------------------------------------------------------
    * Ver   Date              Modified By               Description
    * ---------------------------------------------------------------------
    * 1.0   19 Mar 2008       Satheeshkumar P             Initial Version
    *
    ***********************************************************************/

    import java.util.ArrayList;
    import javax.servlet.http.HttpServletRequest;
    import javax.servlet.http.HttpSession;
    import org.apache.commons.logging.Log;
    import org.apache.commons.logging.LogFactory;
    import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
    import ets.arch.util.PaginationConstants;
    import ets.domain.util.FPLogUtils;
    import java.util.Locale;
    import javax.servlet.http.HttpServletResponse;
    import org.springframework.beans.propertyeditors.LocaleEditor;
    import org.springframework.web.bind.ServletRequestUtils;
    import org.springframework.web.servlet.LocaleResolver;
    import org.springframework.web.servlet.support.RequestContextUtils;



    /*******************************************************************************
     * FPBaseController: Enhanced base Action. See the documentation for the <code>execute</code>
     * method for operational details. A <code>perform</code> method is also
     * provided for backwards compatibility with 1_0.
    * @author  Satheeshkumar P
    * @version 1.0 19 Mar 2008
    * @since   event Portal Iteration 0
     ******************************************************************************/


    public class BaseController extends MultiActionController{

         public void BaseController() {

         }

       //  EventBP eventBP;

         /**
          * @return EventBP - returns the eventBP.

         public EventBP getEventBP() {
              return this.eventBP;
         }*/

         /**
          * @param eventBP
          * The eventBP to set.

         public void setEventBP(EventBP eventBP) {
              this.eventBP = eventBP;
         }*/

         public void initialize (HttpServletRequest request){
            //for highlighting sidemenu
             System.out.println("am in base controller initialize");
             HttpSession session = request.getSession();
             String url = request.getRequestURI();
             System.out.println("url:" + url);
             url = url.substring((url.indexOf("/", 1) + 1));
             System.out.println("url:" + url);
             String menuClickStatus = request.getParameter("menuClick");
             System.out.println("menuClickStatus:" + menuClickStatus);
             if (menuClickStatus != null && !"".equals(menuClickStatus)) {
                 session.setAttribute("menuUrl", url);
                 session.setAttribute("menuClick", menuClickStatus);
                 System.out.println("menuUrl:" + session.getAttribute("menuUrl"));
             }
             //for highlighting sidemenu
         }

         public void setLocale(HttpServletRequest request, HttpServletResponse response){

             try{
                 HttpSession session = request.getSession();
    //             String userName=(String)session.getAttribute("UserName");
    //             String password=(String)session.getAttribute("password");
    //             System.out.println("userName:"+userName +"pwd:"+password);
                LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

                if (localeResolver != null) {
                    // get current locale
                    Locale locale = localeResolver.resolveLocale(request);

                    if (locale != null) {
                        // if current locale is available
                        // set the old locale name
    //                    map.put("oldLocaleNamecale.toString());

                    }

                    // get the locale resolver class name and set it in the model
                    String localeResolverClassName = localeResolver.getClass().getName();
    //                map.put("localeResolverClassNamecaleResolverClassName);

                    // get the new locale name from the request param, if possible
                    //String newLocaleName = ServletRequestUtils.getStringParameter(request, "paramName");
                    String newLocaleName = (String)session.getAttribute("paramName");
                   // String username = ServletRequestUtils.getStringParameter(request, "UserName");
                    System.out.println("newLocaleName:"+newLocaleName);
                   // System.out.println("userName:"+username);
                    if (newLocaleName != null) {
                        LocaleEditor localeEditor = new LocaleEditor();
                        localeEditor.setAsText(newLocaleName);

                        // set the new locale
                        localeResolver.setLocale(request, response,
                                (Locale) localeEditor.getValue());
                    }
                    String language="";
                    if("ar".equals(newLocaleName)){
                    request.setAttribute("language", "ar");

                    }else if ("en".equals(newLocaleName)){
                    request.setAttribute("language", "en");
                    }
                }

             }catch (Exception e){
                 e.printStackTrace();
             }
         }


    }





