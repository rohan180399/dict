<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ page import="ets.domain.racks.business.RackTO" %>  

<script>
function submitPage(value){
if(value == "add"){
document.manageRocks.action = '/throttle/handleViewAdd.do';
document.manageRocks.submit();
}else if(value == 'alter'){
document.manageRocks.action = '/throttle/handleViewRackAlter.do';
document.manageRocks.submit();
}
}
</script>

<script>
   function changePageLanguage(langSelection){

            if(langSelection== 'ar'){
            document.getElementById("pAlign").style.direction="rtl";
            }else if(langSelection== 'en'){
            document.getElementById("pAlign").style.direction="ltr";
            }
        }

    </script>

     <div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.Racks"  text="Racks"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="general.label.stores"  text="Stores"/></a></li>
          <li class="active"><spring:message code="stores.label.Racks"  text="Racks"/></li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">
<body onLoad="setValues();getVehicleNos();">


<form name="manageRocks" method="post" >    


<table class="table table-info mb30 table-hover" id="table">
<thead>
                        <tr >
                            <th><spring:message code="stores.label.RackName"  text="default text"/></th>
                            <th><spring:message code="stores.label.RackDescription"  text="default text"/></th>
                            <th><spring:message code="stores.label.Status"  text="default text"/></th>
                        </tr>
</thead>

<% int index=0; %>
<c:if test = "${rackLists != null}" >
      <c:forEach items="${rackLists}" var="rack"> 

<tr>
<td  width="90"  height="30" style="padding-left:30px; "><c:out value="${rack.rackName}"/></td>
<td  width="265" height="30" style="padding-left:30px; "><c:out value="${rack.rackDescription}"/></td>
<td  width="65" height="30" style="padding-left:20px; ">
<c:if test="${(rack.rackStatus=='n') || (rack.rackStatus=='N')}" >
InActive                      
 </c:if>   
<c:if test="${(rack.rackStatus=='y') || (rack.rackStatus=='Y')}" >
Active
</c:if>
</td>
</tr>
<%
   index++;
 %>
</c:forEach>
  </c:if>  
</table>
<br>
<center>
<input type="button" class="btn btn-success" value="<spring:message code="stores.label.ADD"  text="default text"/>" name="add" onClick="submitPage(this.name)">
<input type="button" class="btn btn-success" value="<spring:message code="stores.label.ALTER"  text="default text"/>" name="alter" onClick="submitPage(this.name)">
</center>
<input type="hidden" value="" name="reqfor">

<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
      </div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
