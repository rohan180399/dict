<%--
    Document   : customerContractMaster
    Created on : Nov 16, 2013, 12:18:29 PM
    Author     : Arul
--%>



<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>

<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>




<script type="text/javascript">
    function submitPage(value) {
        document.customerContract.action = "/throttle/saveCustomerContract.do";
        document.customerContract.submit();
    }

</script>
<div class="pageheader">
    <c:if test="${customerId != '0'}">
        <h2><i class="fa fa-edit"></i> Sales</h2>
    </c:if>
    <c:if test="${customerId == '0'}">
        <h2><i class="fa fa-edit"></i> Company Route Tariff</h2>
    </c:if>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li data-toggle="tab"><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li data-toggle="tab"><a href="general-forms.html">Sales</a></li>
            <li data-toggle="tab" data-toggle="tab"class="active">View Customer Contract</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="">
                <form name="customerContract"  method="post" >

                    <br>


                    <table class="table table-info mb30 table-hover" style="width:60%;">
                        <c:if test="${customerId != '0'}">
                            <tr id="tableDesingTD">
                                <td  colspan="4" >Customer Contract Master</td>
                            </tr>

                            <tr>
                                <td >Customer Name</td>
                                <td ><input type="hidden" name="customerId" id="customerId" value="<c:out value="${customerId}"/>" class="form-control"><input type="hidden" name="customerName" id="customerName" value="<c:out value="${customerName}"/>" class="form-control" readonly=""><c:out value="${customerName}"/></td>
                                <td >Customer Code</td>
                                <td ><input type="hidden" name="customerCode" id="customerCode" value="<c:out value="${customerCode}"/>" class="form-control" readonly><c:out value="${customerCode}"/></td>
                            </tr>
                            <tr>
                                <td >Contract From</td>
                                <td ><input type="hidden" name="contractFrom" id="contractFrom" value="<c:out value="${contractFrom}"/>" class="datepicker form-control"><c:out value="${contractFrom}"/></td>
                                <td >Contract To</td>
                                <td ><input type="hidden" name="contractTo" id="contractTo" value="<c:out value="${contractTo}"/>" class="datepicker form-control"><c:out value="${contractTo}"/></td>
                            </tr>
                            <tr>
                                <td >Contract No</td>
                                <td ><input type="hidden" name="contractNo" id="contractNo" value="<c:out value="${contractNo}"/>" class="form-control" ><c:out value="${contractNo}"/></td>
                                <td >Billing Type</td>
                                <td ><input type="hidden" name="billingTypeId" id="billingTypeId" value="<c:out value="${billingTypeId}"/>"/>
                                    <c:if test="${billingTypeList != null}">
                                        <c:forEach items="${billingTypeList}" var="btl">
                                            <c:if test="${billingTypeId == btl.billingTypeId}">
                                                <c:out value="${btl.billingTypeName}"/>
                                            </c:if>
                                        </c:forEach>
                                    </c:if>
                                </td>
                            </tr>
                        </c:if>
                        <c:if test="${customerId == '0'}">
                            <tr id="tableDesingTD">
                                <td colspan="4" >Company Contract Details</td>
                            </tr>
                            <tr>
                                <td width="25%">Company Name</td>
                                <td width="25%">
                                    <c:out value="${companyName}"/>
                                </td>
                                <td colspan="2">&nbsp;
                                </td>
                            </tr>
                        </c:if>
                    </table>
                    <br>

                    <div id="tabs" >
                        <ul class="nav nav-tabs">
                            <c:if test="${billingTypeId == 1}">
                                <li data-toggle="tab" class="active"  id="pp" style="display: block"><a href="#ptp"><span>Point to Point - Based </span></a></li>
                                
                                </c:if>
                            <c:if test="${billingTypeId == 1}">
                                <li data-toggle="tab" class="active"  id="ip" style="display: block"><a href="#ipp"><span>Point to Point - Based(In-active) </span></a></li>
                                
                                </c:if>
                                <c:if test="${billingTypeId == 1}">
                                <li data-toggle="tab" id="pc" style="display: block"><a href="#pcm"><span> Penality charges </span></a></li>
                                </c:if>
                                <c:if test="${billingTypeId == 1}">
                                <li data-toggle="tab" id="dc" style="display: block"><a href="#dcm"><span> Detention charges </span></a></li>
                                </c:if>
                                <c:if test="${billingTypeId == 2}">
                                <li data-toggle="tab" id="pw" style="display: block"><a href="#ptpw"><span>Point to Point Weight - Based </span></a></li>
                                </c:if>
                                <c:if test="${billingTypeId == 3}">
                                <li data-toggle="tab" id="akm" style="display: block"><a href="#mfr"><span>Actual KM - Based </span></a></li>
                                </c:if>
                             <%--   <c:if test="${billingTypeId == 5}">
                                <li data-toggle="tab" id="dkm" style="display: block"><a href="#mfr"><span>distance Based-Rate Details </span></a></li>
                                </c:if>  --%>
                                <c:if test="${billingTypeId == 4}">
                                <li data-toggle="tab"><a href="#fkmRate"><span>Fixed KM - Based - Rate Details </span></a></li>
                                <li data-toggle="tab"><a href="#fkmRoute"><span>Fixed KM - Based - Route Details </span></a></li>
                                </c:if>
                        </ul>

                      <%--  <c:if test="${billingTypeId == 5}">
                             <div id="ptp" class="tab-pane active" style="overflow: auto">
                                 <div class="inpad">
                                         <c:if test="${contractListSize > 0}">
                               <table class="table table-info mb30 table-hover sortable" width="98%" >
                                        <thead>
                                            <tr height="40" >
                                                <th>Sno</th>

                                                <th  height="80" style="width: 10px;"><font color="red"></font>Reference name</th>


                                                <th  height="30" style="width: 10px;"><font color="red"></font>Vehicle Type</th>
                                                <th  height="30" style="width: 10px;"><font color="red"></font>Container Type</th>
                                                <th  height="30" style="width: 10px;"><font color="red"></font>Container Qty</th>
                                                <th  height="30" style="width: 10px;"><font color="red"></font>Load Type</th>
                                                <th  height="30" style="width: 10px;"><font color="red"></font>From Distance</th>

                                                <th  height="30" ><font color="red"></font>To Distance</th>

                                                <th  height="30" style="width: 90px;" ><font color="red">*</font>Rate With Reefer</th>
                                                <th  height="30" style="width: 90px;" ><font color="red">*</font>Rate Without Reefer</th>
                                                <th  height="30" style="width: 90px;" ><font color="red">*</font>status</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%
                                                   int sno = 0;
                                            %>
                                           <c:forEach items="${contractList}" var="cList">
                                               <%sno++;%>
                                               <tr>
                                                   <td><%=sno%><input type="hidden" id="distanceContractId<%=sno%>" name="distanceContractId" value="<c:out value="${cList.distanceContractId}"/>"/></td>
                                                   <td><c:out value="${cList.referenceName}"/></td>
                                                   <td><c:out value="${cList.vehicleType}"/></td>
                                                   <td><c:out value="${cList.containerTypeName}"/></td>
                                                   <td><c:out value="${cList.containerQty}"/></td>
                                                   <td><c:out value="${cList.loadType}"/></td>
                                                   <td><c:out value="${cList.fromDistance}"/></td>
                                                   <td><c:out value="${cList.toDistance}"/></td>
                                                 <td> <c:out value="${cList.rateWithReefer}"/>
                                                  </td>
                                                 <td> <c:out value="${cList.rateWithoutReefer}"/>
                                                  </td>

                                                   <td> <c:out value="${cList.activeStatus}"/>
                                                       </td>
                                               </tr>
                                            </c:forEach>
                                        </tbody>
                               </table>
                            </c:if>

                                 </div>

                             </div>
                        </c:if>   --%>



                        <c:if test="${billingTypeId == 1}">
                            <!--<div  style="overflow: auto">-->
                            <div id="ptp" class="tab-pane active" style="overflow: auto">
                                <div class="inpad">

                                    <c:if test = "${ptpBillingList != null}">
                                        <table class="table table-info mb30 table-hover" width="98%" id="itemsTable">
                                            <thead>
                                                <% int sno1 = 1;%>
                                                <tr>
                                                    <th>S.No</th>
                                                    <c:if test="${customerId == '0'}">
                                                        <th>View</th>
                                                        </c:if>
                                                    <th>Action</th>    
                                                        <c:if test="${customerId != '0'}">
                                                        <th>Vehicle Route Contract Code</th>
                                                        <th>From Date</th>
                                                        <th>To Date</th>
                                                        </c:if>
                                                    <th>Vehicle Type</th>
                                                    <th>Load Type</th>
                                                    <th>Container Type</th>
                                                    <th>Container Qty</th>
                                                    <th>First Pick UP</th>
                                                    <th>Interim Point 1</th>
                                                    <th>Interim Point 2</th>
                                                    <th>Interim Point 3</th>
                                                    <th>Interim Point 4</th>
                                                    <th>Final Drop Point</th>
                                                    <th>Travel Km</th>
                                                    <th>Travel Time</th>
                                                    <th>Rate with Reefer</th>
                                                    <th>Rate without Reefer</th>
                                                        <c:if test="${customerId == '0'}">
<!--                                                        <th>Fuel Vehicle</th>
                                                        <th>Fuel DG</th>
                                                        <th>Total Fuel</th>-->
                                                        <th>Toll</th>
                                                        <th>Driver Bachat</th>
                                                        <th>Dala</th>
                                                        <th>Misc</th>
                                                        <th>MarketHireWithoutReefer</th>
                                                        <th>MarketHireWithReefer</th>
                                                        </c:if>
                                                    <th>Status</th>
                                                    <th>Created By</th>
                                                        <c:if test="${customerId != '0'}">
                                                        <th>Approval Status</th>
                                                        <th>Approved By</th>
                                                        </c:if>
                                                        
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach items="${ptpBillingList}" var="ptpl">
                                                    <tr>
                                                        <td><%=sno1%></td>
                                                        
                                                        <c:if test="${customerId == '0'}">
                                                            <td>
                                                                <a href="#" name="configure" id="configure" data-toggle="modal" data-target="#myModal"  onclick="viewModelFuel('<c:out value="${ptpl.vehicleTypeId}"/>', '<c:out value="${ptpl.contractRateId}"/>', '2')">VIEW</a> 
                                                            </td>
                                                        </c:if>
                                                            
                                                            <td   align="left" > <a href='/throttle/editCustomerContract.do?companyId=1188&custId=<c:out value="${customerId}" />&routeContractId=<c:out value="${ptpl.routeContractId}" />'>edit/add</a></td>    
                                                            
                                                        
                                                        
                                                        <c:if test="${customerId != '0'}">
                                                            <td><input type="hidden" name="ptpRouteContractCode" id="ptpRouteContractCode" value="<c:out value="${ptpl.routeCode}" />" class="tInput"  style="width: 60px;"/><c:out value="${ptpl.routeCode}" /></td>
                                                            <td> <c:out value="${ptpl.fromDate}" /></td>
                                                            <td> <c:out value="${ptpl.toDate}" /></td>
                                                        </c:if>
                                                        <td>
                                                            <input type="hidden" name="ptpVehTypeId" id="ptpVehTypeId" value="<c:out value="${ptpl.vehicleTypeId}" />" class="tInput"  style="width: 60px;"/>
                                                            <input type="hidden" name="ptpRouteContractId" id="ptpRouteContractId" value="<c:out value="${ptpl.routeContractId}" />" class="tInput"  style="width: 60px;"/>
                                                            <c:out value="${ptpl.vehicleTypeName}" />
                                                        </td>
                                                        <td>
                                                            <c:if test="${ptpl.loadTypeId =='1'}">
                                                                Empty Trip
                                                            </c:if>
                                                            <c:if test="${ptpl.loadTypeId =='2'}">
                                                                Load Trip
                                                            </c:if>
                                                        </td>
                                                        <td>
                                                            <c:if test="${ptpl.containerTypeId =='1'}">
                                                                20'
                                                            </c:if>
                                                            <c:if test="${ptpl.containerTypeId =='2'}">
                                                                40'
                                                            </c:if>

                                                        </td>
                                                        <td>
                                                            <c:if test="${ptpl.containerQty1 =='1'}">
                                                                1
                                                            </c:if>
                                                            <c:if test="${ptpl.containerQty1 =='2'}">
                                                                2
                                                            </c:if>
                                                        </td>
                                                        <td><input type="hidden" name="ptpFirstPickupId" value="<c:out value="${ptpl.firstPickupId}" />" class="tInput" id="ptpFirstPickupId"  style="width: 90px;"/><c:out value="${ptpl.firstPickupName}" /></td>
                                                        <td><input type="hidden" name="ptpPoint1Id" value="<c:out value="${ptpl.point1Id}" />" class="tInput" id="ptpPoint1Id"  style="width: 90px;"/><input type="hidden" name="ptpPoint1Km" value="<c:out value="${ptpl.point1Km}" />" class="tInput" id="ptpPoint1Km"  style="width: 90px;"/><input type="hidden" name="ptpPoint1Hrs" value="<c:out value="${ptpl.point1Hrs}" />" class="tInput" id="ptpPoint1Hrs"  style="width: 90px;"/><input type="hidden" name="ptpPoint1Minutes" value="<c:out value="${ptpl.point1Minutes}" />" class="tInput" id="ptpPoint1Minutes"  style="width: 90px;"/><input type="hidden" name="ptpPoint1RouteId" value="<c:out value="${ptpl.point1RouteId}" />" class="tInput" id="ptpPoint1RouteId"  style="width: 90px;"/><c:out value="${ptpl.point1Name}" /></td>
                                                        <td><input type="hidden" name="ptpPoint2Id" value="<c:out value="${ptpl.point2Id}" />" class="tInput" id="ptpPoint2Id"  style="width: 90px;"/><input type="hidden" name="ptpPoint2Km" value="<c:out value="${ptpl.point2Km}" />" class="tInput" id="ptpPoint2Km"  style="width: 90px;"/><input type="hidden" name="ptpPoint2Hrs" value="<c:out value="${ptpl.point2Hrs}" />" class="tInput" id="ptpPoint2Hrs"  style="width: 90px;"/><input type="hidden" name="ptpPoint2Minutes" value="<c:out value="${ptpl.point2Minutes}" />" class="tInput" id="ptpPoint2Minutes"  style="width: 90px;"/><input type="hidden" name="ptpPoint2RouteId" value="<c:out value="${ptpl.point2RouteId}" />" class="tInput" id="ptpPoint2RouteId"  style="width: 90px;"/><c:out value="${ptpl.point2Name}" /></td>
                                                        <td><input type="hidden" name="ptpPoint3Id" value="<c:out value="${ptpl.point3Id}" />" class="tInput" id="ptpPoint3Id"  style="width: 90px;"/><input type="hidden" name="ptpPoint3Km" value="<c:out value="${ptpl.point3Km}" />" class="tInput" id="ptpPoint3Km"  style="width: 90px;"/><input type="hidden" name="ptpPoint3Hrs" value="<c:out value="${ptpl.point3Hrs}" />" class="tInput" id="ptpPoint3Hrs"  style="width: 90px;"/><input type="hidden" name="ptpPoint3Minutes" value="<c:out value="${ptpl.point3Minutes}" />" class="tInput" id="ptpPoint3Minutes"  style="width: 90px;"/><input type="hidden" name="ptpPoint3RouteId" value="<c:out value="${ptpl.point3RouteId}" />" class="tInput" id="ptpPoint3RouteId"  style="width: 90px;"/><c:out value="${ptpl.point3Name}" /></td>
                                                        <td><input type="hidden" name="ptpPoint4Id" value="<c:out value="${ptpl.point4Id}" />" class="tInput" id="ptpPoint4Id"  style="width: 90px;"/><input type="hidden" name="ptpPoint4Km" value="<c:out value="${ptpl.point4Km}" />" class="tInput" id="ptpPoint4Km"  style="width: 90px;"/><input type="hidden" name="ptpPoint4Hrs" value="<c:out value="${ptpl.point4Hrs}" />" class="tInput" id="ptpPoint4Hrs"  style="width: 90px;"/><input type="hidden" name="ptpPoint4Minutes" value="<c:out value="${ptpl.point4Minutes}" />" class="tInput" id="ptpPoint4Minutes"  style="width: 90px;"/><input type="hidden" name="ptpPoint4RouteId" value="<c:out value="${ptpl.point4RouteId}" />" class="tInput" id="ptpPoint4RouteId"  style="width: 90px;"/><c:out value="${ptpl.point4Name}" /></td>
                                                        <td><input type="hidden" name="ptpFinalPointId" value="<c:out value="${ptpl.finalPointId}" />" class="tInput" id="ptpFinalPointId"  style="width: 90px;"/><input type="hidden" name="ptpFinalPointKm" value="<c:out value="${ptpl.finalPointKm}" />" class="tInput" id="ptpFinalPointKm"  style="width: 90px;"/><input type="hidden" name="ptpFinalPointHrs" value="<c:out value="${ptpl.finalPointHrs}" />" class="tInput" id="ptpFinalPointHrs"  style="width: 90px;"/><input type="hidden" name="ptpFinalPointMinutes" value="<c:out value="${ptpl.finalPointMinutes}" />" class="tInput" id="ptpFinalPointMinutes"  style="width: 90px;"/><input type="hidden" name="ptpFinalPointRouteId" value="<c:out value="${ptpl.finalPointRouteId}" />" class="tInput" id="ptpFinalPointRouteId"  style="width: 90px;"/><c:out value="${ptpl.finalPointName}" /></td>

                                                        <td><input type="hidden" name="ptpTotalKm" value="<c:out value="${ptpl.totalKm}" />" class="tInput" id="ptpTotalKm"  style="width: 90px;"/><c:out value="${ptpl.totalKm}" /></td>
                                                        <td><input type="hidden" name="ptpTravelTime" value="<c:out value="${ptpl.totalHours}" />:<c:out value="${ptpl.totalMinutes}" />:00" class="tInput" id="<c:out value="${ptpl.totalHours}" />"  style="width: 90px;"/><c:out value="${ptpl.totalHours}" />:<c:out value="${ptpl.totalMinutes}" />:00</td>
                                                        <td><input type="hidden" name="ptpRateWithReefer" value="<c:out value="${ptpl.rateWithReeferPerKm}" />" class="tInput" id="ptpRateWithReefer"  style="width: 60px;" /><c:out value="${ptpl.rateWithReefer}" /></td>
                                                        <td><input type="hidden" name="ptpRateWithoutReefer" value="<c:out value="${ptpl.rateWithoutReeferPerKm}" />" class="tInput" id="ptpRateWithoutReefer" style="width: 60px;" /><c:out value="${ptpl.rateWithoutReefer}" /></td>
                                                            <c:if test="${customerId == '0'}">
                                                            <td><c:out value="${ptpl.toll}" /></td>
                                                            <td><c:out value="${ptpl.driverBachat}" /></td>
                                                            <td><c:out value="${ptpl.dala}" /></td>
                                                            <td><c:out value="${ptpl.misc}" /></td>
                                                            <td><c:out value="${ptpl.marketHire}" /></td>
                                                            <td><c:out value="${ptpl.marketHireWithReefer}" /></td>
                                                        </c:if>
                                                        <td><input type="hidden" name="status" value="<c:out value="${ptpl.activeInd}" />" class="tInput" id="status" style="width: 60px;" /><c:out value="${ptpl.activeInd}" /></td>
                                                        <td><c:out value="${ptpl.userName}" /></td>
                                                        <c:if test="${customerId != '0'}">
                                                            <c:if test="${ptpl.approvalStatus == '2' || ptpl.approvalStatus == null}">
                                                                <td   align="left" > <span class="label label-warning">pending</span></td>
                                                                    </c:if>
                                                                    <c:if test="${ptpl.approvalStatus =='1'}">
                                                                <td   align="left" > <span class="label label-success">approved</span></td>
                                                                    </c:if>
                                                                    <c:if test="${ptpl.approvalStatus =='3'}">
                                                                <td   align="left" ><span class="label label-danger">rejected</span></td>
                                                                    </c:if>
                                                                   <td><c:out value="${ptpl.approvedBy}" /></td>
                                                                </c:if>
                                  
                                                    </tr>
                                                    <%sno1++;%>
                                                </c:forEach >
                                            </tbody>
                                        </table>
                                    </c:if>
                                    <br>

                                    <script language="javascript" type="text/javascript">
                                        setFilterGrid("itemsTable");
                                    </script>
                                    <div id="controls">
                                        <div id="perpage">
                                            <select onchange="sorter.size(this.value)">
                                                <option value="5" selected="selected">5</option>
                                                <option value="10">10</option>
                                                <option value="20">20</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                            </select>
                                            <span>Entries Per Page</span>
                                        </div>
                                        <div id="navigation">
                                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                        </div>
                                        <div id="text">Displaying Page 1 of 1</div>
                                    </div>
                                    <script type="text/javascript">
                                        var sorter = new TINY.table.sorter("sorter");
                                        sorter.head = "head";
                                        sorter.asc = "asc";
                                        sorter.desc = "desc";
                                        sorter.even = "evenrow";
                                        sorter.odd = "oddrow";
                                        sorter.evensel = "evenselected";
                                        sorter.oddsel = "oddselected";
                                        sorter.paginate = true;
                                        sorter.currentid = "currentpage";
                                        sorter.limitid = "pagelimit";
                                        sorter.init("itemsTable", 1);
                                    </script>
                                </div>
                                <br>
                                <br>
                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:90px;height:30px;font-weight: bold;padding: 1px;"/></a>
                                </center>
                                <br>
                                <br>

                            </div>
                            
                            <div id="ipp" class="tab-pane active" style="overflow: auto">
                                <div class="inpad">

                                    <c:if test = "${inactiveContractBillingList != null}">
                                        <table class="table table-info mb30 table-hover" width="98%" id="itemsTable1">
                                            <thead>
                                                <% int sno1 = 1;%>
                                                <tr>
                                                    <th>S.No</th>
                                                        <c:if test="${customerId != '0'}">
                                                        <th>Vehicle Route Contract Code</th>
                                                        <th>From Date</th>
                                                        <th>To Date</th>
                                                        </c:if>
                                                    <th>Vehicle Type</th>
                                                    <th>Load Type</th>
                                                    <th>Container Type</th>
                                                    <th>Container Qty</th>
                                                    <th>First Pick UP</th>
                                                    <th>Interim Point 1</th>
                                                    <th>Interim Point 2</th>
                                                    <th>Interim Point 3</th>
                                                    <th>Interim Point 4</th>
                                                    <th>Final Drop Point</th>
                                                    <th>Travel Km</th>
                                                    <th>Travel Time</th>
                                                    <th>Rate with Reefer</th>
                                                    <th>Rate without Reefer</th>
                                                        <c:if test="${customerId == '0'}">
<!--                                                        <th>Fuel Vehicle</th>
                                                        <th>Fuel DG</th>
                                                        <th>Total Fuel</th>-->
                                                        <th>Toll</th>
                                                        <th>Driver Bachat</th>
                                                        <th>Dala</th>
                                                        <th>Misc</th>
                                                        <th>MarketHireWithoutReefer</th>
                                                        <th>MarketHireWithReefer</th>
                                                        </c:if>
                                                    <th>Status</th>
                                                    <th>Created By</th>
                                                    <th>Inactive By</th>
                                                    <th>Inactive On</th>
                                                     <%--   <c:if test="${customerId != '0'}">
                                                        <th>Approval Status</th>
                                                        <th>Approved By</th>
                                                        </c:if>  --%>
                                                        <c:if test="${customerId == '0'}">
                                                        <th>View</th>
                                                        </c:if>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach items="${inactiveContractBillingList}" var="ptpl">
                                                    <tr>
                                                        <td><%=sno1%></td>
                                                        <c:if test="${customerId != '0'}">
                                                            <td><input type="hidden" name="ptpRouteContractCode" id="ptpRouteContractCode" value="<c:out value="${ptpl.routeCode}" />" class="tInput"  style="width: 60px;"/><c:out value="${ptpl.routeCode}" /></td>
                                                            <td> <c:out value="${ptpl.fromDate}" /></td>
                                                            <td> <c:out value="${ptpl.toDate}" /></td>
                                                        </c:if>
                                                        <td>
                                                            <input type="hidden" name="ptpVehTypeId" id="ptpVehTypeId" value="<c:out value="${ptpl.vehicleTypeId}" />" class="tInput"  style="width: 60px;"/>
                                                            <input type="hidden" name="ptpRouteContractId" id="ptpRouteContractId" value="<c:out value="${ptpl.routeContractId}" />" class="tInput"  style="width: 60px;"/>
                                                            <c:out value="${ptpl.vehicleTypeName}" />
                                                        </td>
                                                        <td>
                                                            <c:if test="${ptpl.loadTypeId =='1'}">
                                                                Empty Trip
                                                            </c:if>
                                                            <c:if test="${ptpl.loadTypeId =='2'}">
                                                                Load Trip
                                                            </c:if>
                                                        </td>
                                                        <td>
                                                            <c:if test="${ptpl.containerTypeId =='1'}">
                                                                20'
                                                            </c:if>
                                                            <c:if test="${ptpl.containerTypeId =='2'}">
                                                                40'
                                                            </c:if>

                                                        </td>
                                                        <td>
                                                            <c:if test="${ptpl.containerQty1 =='1'}">
                                                                1
                                                            </c:if>
                                                            <c:if test="${ptpl.containerQty1 =='2'}">
                                                                2
                                                            </c:if>
                                                        </td>
                                                        <td><input type="hidden" name="ptpFirstPickupId" value="<c:out value="${ptpl.firstPickupId}" />" class="tInput" id="ptpFirstPickupId"  style="width: 90px;"/><c:out value="${ptpl.firstPickupName}" /></td>
                                                        <td><input type="hidden" name="ptpPoint1Id" value="<c:out value="${ptpl.point1Id}" />" class="tInput" id="ptpPoint1Id"  style="width: 90px;"/><input type="hidden" name="ptpPoint1Km" value="<c:out value="${ptpl.point1Km}" />" class="tInput" id="ptpPoint1Km"  style="width: 90px;"/><input type="hidden" name="ptpPoint1Hrs" value="<c:out value="${ptpl.point1Hrs}" />" class="tInput" id="ptpPoint1Hrs"  style="width: 90px;"/><input type="hidden" name="ptpPoint1Minutes" value="<c:out value="${ptpl.point1Minutes}" />" class="tInput" id="ptpPoint1Minutes"  style="width: 90px;"/><input type="hidden" name="ptpPoint1RouteId" value="<c:out value="${ptpl.point1RouteId}" />" class="tInput" id="ptpPoint1RouteId"  style="width: 90px;"/><c:out value="${ptpl.point1Name}" /></td>
                                                        <td><input type="hidden" name="ptpPoint2Id" value="<c:out value="${ptpl.point2Id}" />" class="tInput" id="ptpPoint2Id"  style="width: 90px;"/><input type="hidden" name="ptpPoint2Km" value="<c:out value="${ptpl.point2Km}" />" class="tInput" id="ptpPoint2Km"  style="width: 90px;"/><input type="hidden" name="ptpPoint2Hrs" value="<c:out value="${ptpl.point2Hrs}" />" class="tInput" id="ptpPoint2Hrs"  style="width: 90px;"/><input type="hidden" name="ptpPoint2Minutes" value="<c:out value="${ptpl.point2Minutes}" />" class="tInput" id="ptpPoint2Minutes"  style="width: 90px;"/><input type="hidden" name="ptpPoint2RouteId" value="<c:out value="${ptpl.point2RouteId}" />" class="tInput" id="ptpPoint2RouteId"  style="width: 90px;"/><c:out value="${ptpl.point2Name}" /></td>
                                                        <td><input type="hidden" name="ptpPoint3Id" value="<c:out value="${ptpl.point3Id}" />" class="tInput" id="ptpPoint3Id"  style="width: 90px;"/><input type="hidden" name="ptpPoint3Km" value="<c:out value="${ptpl.point3Km}" />" class="tInput" id="ptpPoint3Km"  style="width: 90px;"/><input type="hidden" name="ptpPoint3Hrs" value="<c:out value="${ptpl.point3Hrs}" />" class="tInput" id="ptpPoint3Hrs"  style="width: 90px;"/><input type="hidden" name="ptpPoint3Minutes" value="<c:out value="${ptpl.point3Minutes}" />" class="tInput" id="ptpPoint3Minutes"  style="width: 90px;"/><input type="hidden" name="ptpPoint3RouteId" value="<c:out value="${ptpl.point3RouteId}" />" class="tInput" id="ptpPoint3RouteId"  style="width: 90px;"/><c:out value="${ptpl.point3Name}" /></td>
                                                        <td><input type="hidden" name="ptpPoint4Id" value="<c:out value="${ptpl.point4Id}" />" class="tInput" id="ptpPoint4Id"  style="width: 90px;"/><input type="hidden" name="ptpPoint4Km" value="<c:out value="${ptpl.point4Km}" />" class="tInput" id="ptpPoint4Km"  style="width: 90px;"/><input type="hidden" name="ptpPoint4Hrs" value="<c:out value="${ptpl.point4Hrs}" />" class="tInput" id="ptpPoint4Hrs"  style="width: 90px;"/><input type="hidden" name="ptpPoint4Minutes" value="<c:out value="${ptpl.point4Minutes}" />" class="tInput" id="ptpPoint4Minutes"  style="width: 90px;"/><input type="hidden" name="ptpPoint4RouteId" value="<c:out value="${ptpl.point4RouteId}" />" class="tInput" id="ptpPoint4RouteId"  style="width: 90px;"/><c:out value="${ptpl.point4Name}" /></td>
                                                        <td><input type="hidden" name="ptpFinalPointId" value="<c:out value="${ptpl.finalPointId}" />" class="tInput" id="ptpFinalPointId"  style="width: 90px;"/><input type="hidden" name="ptpFinalPointKm" value="<c:out value="${ptpl.finalPointKm}" />" class="tInput" id="ptpFinalPointKm"  style="width: 90px;"/><input type="hidden" name="ptpFinalPointHrs" value="<c:out value="${ptpl.finalPointHrs}" />" class="tInput" id="ptpFinalPointHrs"  style="width: 90px;"/><input type="hidden" name="ptpFinalPointMinutes" value="<c:out value="${ptpl.finalPointMinutes}" />" class="tInput" id="ptpFinalPointMinutes"  style="width: 90px;"/><input type="hidden" name="ptpFinalPointRouteId" value="<c:out value="${ptpl.finalPointRouteId}" />" class="tInput" id="ptpFinalPointRouteId"  style="width: 90px;"/><c:out value="${ptpl.finalPointName}" /></td>

                                                        <td><input type="hidden" name="ptpTotalKm" value="<c:out value="${ptpl.totalKm}" />" class="tInput" id="ptpTotalKm"  style="width: 90px;"/><c:out value="${ptpl.totalKm}" /></td>
                                                        <td><input type="hidden" name="ptpTravelTime" value="<c:out value="${ptpl.totalHours}" />:<c:out value="${ptpl.totalMinutes}" />:00" class="tInput" id="<c:out value="${ptpl.totalHours}" />"  style="width: 90px;"/><c:out value="${ptpl.totalHours}" />:<c:out value="${ptpl.totalMinutes}" />:00</td>
                                                        <td><input type="hidden" name="ptpRateWithReefer" value="<c:out value="${ptpl.rateWithReeferPerKm}" />" class="tInput" id="ptpRateWithReefer"  style="width: 60px;" /><c:out value="${ptpl.rateWithReefer}" /></td>
                                                        <td><input type="hidden" name="ptpRateWithoutReefer" value="<c:out value="${ptpl.rateWithoutReeferPerKm}" />" class="tInput" id="ptpRateWithoutReefer" style="width: 60px;" /><c:out value="${ptpl.rateWithoutReefer}" /></td>
                                                            <c:if test="${customerId == '0'}">
                                                            <td><c:out value="${ptpl.toll}" /></td>
                                                            <td><c:out value="${ptpl.driverBachat}" /></td>
                                                            <td><c:out value="${ptpl.dala}" /></td>
                                                            <td><c:out value="${ptpl.misc}" /></td>
                                                            <td><c:out value="${ptpl.marketHire}" /></td>
                                                            <td><c:out value="${ptpl.marketHireWithReefer}" /></td>
                                                        </c:if>
                                                        <td><input type="hidden" name="status" value="<c:out value="${ptpl.activeInd}" />" class="tInput" id="status" style="width: 60px;" /><c:out value="${ptpl.activeInd}" /></td>
                                                        <td><c:out value="${ptpl.userName}" /></td>
                                                   <%--     <c:if test="${customerId != '0'}">
                                                            <c:if test="${ptpl.approvalStatus == '2' || ptpl.approvalStatus == null}">
                                                                <td   align="left" > <span class="label label-warning">pending</span></td>
                                                                    </c:if>
                                                                    <c:if test="${ptpl.approvalStatus =='1'}">
                                                                <td   align="left" > <span class="label label-success">approved</span></td>
                                                                    </c:if>
                                                                    <c:if test="${ptpl.approvalStatus =='3'}">
                                                                <td   align="left" ><span class="label label-danger">rejected</span></td>
                                                                    </c:if>
                                                                   <td><c:out value="${ptpl.approvedBy}" /></td>
</c:if>  --%>
                                                     <td><c:out value="${ptpl.approvedBy}" /></td>
                                                     <td><c:out value="${ptpl.approvalStatus}" /></td>
                                                                <c:if test="${customerId == '0'}">
                                                            <td>
                                                                <a href="#" name="configure" id="configure" data-toggle="modal" data-target="#myModal"  onclick="viewModelFuel('<c:out value="${ptpl.vehicleTypeId}"/>', '<c:out value="${ptpl.contractRateId}"/>', '2')">VIEW</a> 
                                                            </td>
                                                        </c:if>
                                                    </tr>
                                                    <%sno1++;%>
                                                </c:forEach >
                                            </tbody>
                                        </table>
                                    </c:if>
                                    <br>

                                    <script language="javascript" type="text/javascript">
                                        setFilterGrid("itemsTable1");
                                    </script>
                                    <div id="controls">
                                        <div id="perpage">
                                            <select onchange="sorter.size(this.value)">
                                                <option value="5" selected="selected">5</option>
                                                <option value="10">10</option>
                                                <option value="20">20</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                            </select>
                                            <span>Entries Per Page</span>
                                        </div>
                                        <div id="navigation">
                                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                        </div>
                                        <div id="text">Displaying Page 1 of 1</div>
                                    </div>
                                    <script type="text/javascript">
                                        var sorter = new TINY.table.sorter("sorter");
                                        sorter.head = "head";
                                        sorter.asc = "asc";
                                        sorter.desc = "desc";
                                        sorter.even = "evenrow";
                                        sorter.odd = "oddrow";
                                        sorter.evensel = "evenselected";
                                        sorter.oddsel = "oddselected";
                                        sorter.paginate = true;
                                        sorter.currentid = "currentpage";
                                        sorter.limitid = "pagelimit";
                                        sorter.init("itemsTable1", 1);
                                    </script>
                                </div>
                                <br>
                                <br>
                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:90px;height:30px;font-weight: bold;padding: 1px;"/></a>
                                </center>
                                <br>
                                <br>

                            </div>
                            
                            <div id="pcm" class="tab-pane">
                                <div class="inpad">
                                    <c:if test = "${viewpenalitycharge != null}">
                                        <table class="table table-info mb30 table-hover" width="98%" id="POD1">
                                            <thead>
                                                <!--<tr id="rowId0">-->
                                                <tr id="tableDesingTH">
                                                    <th  height="30">S No</th>
                                                    <th  height="30">Charges names</td>
                                                    <th  height="30">Unit</th>
                                                    <th  height="30">Amount</th>
                                                    <th  height="30"> Remark</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <% int sno = 0;%>
                                                <c:forEach items="${viewpenalitycharge}" var="vpc">
                                                    <%
                                                        sno++;
                                                        String className = "text1";
                                                        if ((sno % 1) == 0) {
                                                            className = "text1";
                                                        } else {
                                                            className = "text2";
                                                        }
                                                    %>
                                                    <tr>
                                                        <td   align="left"> <%= sno%>  </td>
                                                        <td   align="left"> <c:out value="${vpc.penality}" /></td>
                                                        <td   align="left"> <c:out value="${vpc.pcmunit}"/></td>
                                                        <td   align="left"> <c:out value="${vpc.chargeamount}" /></td>
                                                        <td   align="left"> <c:out value="${vpc.pcmremarks}"/></td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                        <!--                                    <div id="controls">
                                                                                <div id="perpage">
                                                                                    <select onchange="sorter.size(this.value)">
                                                                                        <option value="5" selected="selected">5</option>
                                                                                        <option value="10">10</option>
                                                                                        <option value="20">20</option>
                                                                                        <option value="50">50</option>
                                                                                        <option value="100">100</option>
                                                                                    </select>
                                                                                    <span>Entries Per Page</span>
                                                                                </div>
                                                                                <div id="navigation">
                                                                                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                                                                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                                                                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                                                                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                                                                </div>
                                                                                <div id="text">Displaying Page 1 of 1</div>
                                                                            </div>-->
                                    </c:if>
                                </div>
                                <br>
                                <br>
                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                </center>
                                <br>
                                <br>
                            </div>
                            <br>
                            <br>
                            <div id="dcm" >
                                <div class="inpad">
                                    <c:if test = "${viewdetentioncharge != null}">
                                        <table class="table table-info" width="98%" id="POD2">
                                            <thead>
                                                <tr id="tableDesingTH">
                                                    <!--<tr id="rowId1">-->
                                                    <th>S.No</th>
                                                    <th   height="30">Vehicle type</th>
                                                    <th   height="30">Time Slot</th>
                                                    <th   height="30">Amount</th>
                                                    <th   height="30">Remarks</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <% int sno = 0;%>
                                                <c:forEach items="${viewdetentioncharge}" var="vdc">
                                                    <%
                                                        sno++;
                                                        String className = "text1";
                                                        if ((sno % 1) == 0) {
                                                            className = "text1";
                                                        } else {
                                                            className = "text2";
                                                        }
                                                    %>
                                                    <tr>
                                                        <td   align="left"> <%= sno%>  </td>
                                                        <td   align="left"> <c:out value="${vdc.vehicleTypeName}" /></td>
                                                        <td   align="left"> <c:out value="${vdc.timeSlot}" /></td>
                                                        <td   align="left"> <c:out value="${vdc.chargeamt}" /></td>
                                                        <td   align="left"> <c:out value="${vdc.dcmremarks}"/></td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>

                                        </table>
                                        <!--                                    <div id="controls">
                                                                                <div id="perpage">
                                                                                    <select onchange="sorter.size(this.value)">
                                                                                        <option value="5" selected="selected">5</option>
                                                                                        <option value="10">10</option>
                                                                                        <option value="20">20</option>
                                                                                        <option value="50">50</option>
                                                                                        <option value="100">100</option>
                                                                                    </select>
                                                                                    <span>Entries Per Page</span>
                                                                                </div>
                                                                                <div id="navigation">
                                                                                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                                                                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                                                                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                                                                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                                                                </div>
                                                                                <div id="text">Displaying Page 1 of 1</div>
                                                                            </div>-->
                                    </c:if>
                                </div>
                                <br>
                                <br>
                                <center>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                </center>
                                <br>
                                <br>
                            </div>
                            <!--</div>-->
                            
                            
                        </c:if>

                        <c:if test="${billingTypeId == 4}">
                            <div id="fkmRate" class="tab-pane">
                                <div class="inpad">

                                    <c:if test = "${contractFixedRateList != null}">
                                        <table class="table table-info mb30 table-hover sortable" width="98%"  id="itemsTable">
                                            <thead>
                                                <% int sno1 = 1;%>
                                                <tr height="110" id="tableDesingTH">
                                                    <th>S.No</th>
                                                    <th>Vehicle Type Name</th>
                                                    <th>Vehicle Count</th>
                                                    <th>Total Km</th>
                                                    <th>Total Reefer Hm</th>
                                                    <th>Rate with Reefer</th>
                                                    <th>Extra Km Rate/Km with Reefer</th>
                                                    <th>Extra Reefer Hm Rate/Hm with Reefer</th>
                                                    <th>Rate without Reefer</th>
                                                    <th>Extra Km Rate/Km without Reefer</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach items="${contractFixedRateList}" var="ptpl">
                                                    <tr>
                                                        <td><%=sno1%></td>
                                                        <td>
                                                            <input type="hidden" name="fixedVehTypeId" id="fixedVehTypeId" value="<c:out value="${ptpl.vehicleTypeId}" />" class="tInput"  style="width: 60px;"/>
                                                            <c:out value="${ptpl.vehicleTypeName}" />
                                                        </td>
                                                        <td><input type="hidden" name="contractVehicleNos" value="<c:out value="${ptpl.contractVehicleNos}" />" class="tInput" id="contractVehicleNos"  style="width: 90px;"/><c:out value="${ptpl.contractVehicleNos}" /></td>
                                                        <td><input type="hidden" name="fixedTotalKm" value="<c:out value="${ptpl.totalKm}" />" class="tInput" id="fixedTotalKm"  style="width: 90px;"/><c:out value="${ptpl.totalKm}" /></td>
                                                        <td><input type="hidden" name="fixedTotalHm" value="<c:out value="${ptpl.totalHm}" />" class="tInput" id="fixedTotalHm"  style="width: 90px;"/><c:out value="${ptpl.totalHm}" /></td>
                                                        <td><input type="hidden" name="fixedRateWithReefer" value="<c:out value="${ptpl.rateWithReefer}" />" class="tInput" id="fixedRateWithReefer"  style="width: 60px;" /><c:out value="${ptpl.rateWithReefer}" /></td>
                                                        <td><input type="hidden" name="extraKmRatePerKmRateWithReefer" value="<c:out value="${ptpl.extraKmRatePerKmRateWithReefer}" />" class="tInput" id="extraKmRatePerKmRateWithReefer"  style="width: 60px;" /><c:out value="${ptpl.extraKmRatePerKmRateWithReefer}" /></td>
                                                        <td><input type="hidden" name="extraHmRatePerHmRateWithReefer" value="<c:out value="${ptpl.extraHmRatePerHmRateWithReefer}" />" class="tInput" id="extraHmRatePerHmRateWithReefer"  style="width: 60px;" /><c:out value="${ptpl.extraHmRatePerHmRateWithReefer}" /></td>
                                                        <td><input type="hidden" name="fixedRateWithoutReefer" value="<c:out value="${ptpl.rateWithoutReefer}" />" class="tInput" id="fixedRateWithoutReefer" style="width: 60px;" /><c:out value="${ptpl.rateWithoutReefer}" /></td>
                                                        <td><input type="hidden" name="extraKmRatePerKmRateWithoutReefer" value="<c:out value="${ptpl.extraKmRatePerKmRateWithoutReefer}" />" class="tInput" id="extraKmRatePerKmRateWithoutReefer" style="width: 60px;" /><c:out value="${ptpl.extraKmRatePerKmRateWithoutReefer}" /></td>
                                                        <td><input type="hidden" name="status" value="<c:out value="${ptpl.activeInd}" />" class="tInput" id="status" style="width: 60px;" /><c:out value="${ptpl.activeInd}" /></td>
                                                    </tr>
                                                    <%sno1++;%>
                                                </c:forEach >
                                            </tbody>
                                        </table>
                                    </c:if>
                                    <br>
                                    <script language="javascript" type="text/javascript">
                                        setFilterGrid("itemsTable");
                                    </script>
                                    <div id="controls">
                                        <div id="perpage">
                                            <select onchange="sorter.size(this.value)">
                                                <option value="5" selected="selected">5</option>
                                                <option value="10">10</option>
                                                <option value="20">20</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                            </select>
                                            <span>Entries Per Page</span>
                                        </div>
                                        <div id="navigation">
                                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                        </div>
                                        <div id="text">Displaying Page 1 of 1</div>
                                    </div>
                                    <script type="text/javascript">
                                        var sorter = new TINY.table.sorter("sorter");
                                        sorter.head = "head";
                                        sorter.asc = "asc";
                                        sorter.desc = "desc";
                                        sorter.even = "evenrow";
                                        sorter.odd = "oddrow";
                                        sorter.evensel = "evenselected";
                                        sorter.oddsel = "oddselected";
                                        sorter.paginate = true;
                                        sorter.currentid = "currentpage";
                                        sorter.limitid = "pagelimit";
                                        sorter.init("itemsTable", 1);
                                    </script>
                                </div>
                                <br>
                                <br>
                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                </center>
                                <br>
                                <br>
                            </div>
                            <div id="fkmRoute" class="tab-pane">
                                <div class="inpad">

                                    <c:if test = "${fixedKmBillingList != null}">
                                        <table class="table table-info mb30 table-hover sortable" width="98%"  id="itemsTable" >
                                            <thead>
                                                <% int sno1 = 1;%>
                                                <tr height="110" id="tableDesingTH">
                                                    <th>S.No</th>
                                                    <th>Vehicle Route Contract Code</th>
                                                    <th>Vehicle Type</th>
                                                    <th>First Pick UP</th>
                                                    <th>Interim Point 1</th>
                                                    <th>Interim Point 2</th>
                                                    <th>Interim Point 3</th>
                                                    <th>Interim Point 4</th>
                                                    <th>Final Drop Point</th>
                                                    <th>Travel Km</th>
                                                    <th>Travel Time</th>
                                                    <th>Rate with Reefer</th>
                                                    <th>Rate without Reefer</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach items="${fixedKmBillingList}" var="ptpl">
                                                    <tr>
                                                        <td><%=sno1%></td>
                                                        <td><input type="hidden" name="ptpRouteContractCode" id="ptpRouteContractCode" value="<c:out value="${ptpl.routeCode}" />" class="tInput"  style="width: 60px;"/><c:out value="${ptpl.routeCode}" /></td>
                                                        <td>
                                                            <input type="hidden" name="ptpVehTypeId" id="ptpVehTypeId" value="<c:out value="${ptpl.vehicleTypeId}" />" class="tInput"  style="width: 60px;"/>
                                                            <input type="hidden" name="ptpRouteContractId" id="ptpRouteContractId" value="<c:out value="${ptpl.routeContractId}" />" class="tInput"  style="width: 60px;"/>
                                                            <c:out value="${ptpl.vehicleTypeName}" />
                                                        </td>
                                                        <td><input type="hidden" name="ptpFirstPickupId" value="<c:out value="${ptpl.firstPickupId}" />" class="tInput" id="ptpFirstPickupId"  style="width: 90px;"/><c:out value="${ptpl.firstPickupName}" /></td>
                                                        <td><input type="hidden" name="ptpPoint1Id" value="<c:out value="${ptpl.point1Id}" />" class="tInput" id="ptpPoint1Id"  style="width: 90px;"/><input type="hidden" name="ptpPoint1Km" value="<c:out value="${ptpl.point1Km}" />" class="tInput" id="ptpPoint1Km"  style="width: 90px;"/><input type="hidden" name="ptpPoint1Hrs" value="<c:out value="${ptpl.point1Hrs}" />" class="tInput" id="ptpPoint1Hrs"  style="width: 90px;"/><input type="hidden" name="ptpPoint1Minutes" value="<c:out value="${ptpl.point1Minutes}" />" class="tInput" id="ptpPoint1Minutes"  style="width: 90px;"/><input type="hidden" name="ptpPoint1RouteId" value="<c:out value="${ptpl.point1RouteId}" />" class="tInput" id="ptpPoint1RouteId"  style="width: 90px;"/><c:out value="${ptpl.point1Name}" /></td>
                                                        <td><input type="hidden" name="ptpPoint2Id" value="<c:out value="${ptpl.point2Id}" />" class="tInput" id="ptpPoint2Id"  style="width: 90px;"/><input type="hidden" name="ptpPoint2Km" value="<c:out value="${ptpl.point2Km}" />" class="tInput" id="ptpPoint2Km"  style="width: 90px;"/><input type="hidden" name="ptpPoint2Hrs" value="<c:out value="${ptpl.point2Hrs}" />" class="tInput" id="ptpPoint2Hrs"  style="width: 90px;"/><input type="hidden" name="ptpPoint2Minutes" value="<c:out value="${ptpl.point2Minutes}" />" class="tInput" id="ptpPoint2Minutes"  style="width: 90px;"/><input type="hidden" name="ptpPoint2RouteId" value="<c:out value="${ptpl.point2RouteId}" />" class="tInput" id="ptpPoint2RouteId"  style="width: 90px;"/><c:out value="${ptpl.point2Name}" /></td>
                                                        <td><input type="hidden" name="ptpPoint3Id" value="<c:out value="${ptpl.point3Id}" />" class="tInput" id="ptpPoint3Id"  style="width: 90px;"/><input type="hidden" name="ptpPoint3Km" value="<c:out value="${ptpl.point3Km}" />" class="tInput" id="ptpPoint3Km"  style="width: 90px;"/><input type="hidden" name="ptpPoint3Hrs" value="<c:out value="${ptpl.point3Hrs}" />" class="tInput" id="ptpPoint3Hrs"  style="width: 90px;"/><input type="hidden" name="ptpPoint3Minutes" value="<c:out value="${ptpl.point3Minutes}" />" class="tInput" id="ptpPoint3Minutes"  style="width: 90px;"/><input type="hidden" name="ptpPoint3RouteId" value="<c:out value="${ptpl.point3RouteId}" />" class="tInput" id="ptpPoint3RouteId"  style="width: 90px;"/><c:out value="${ptpl.point3Name}" /></td>
                                                        <td><input type="hidden" name="ptpPoint4Id" value="<c:out value="${ptpl.point4Id}" />" class="tInput" id="ptpPoint4Id"  style="width: 90px;"/><input type="hidden" name="ptpPoint4Km" value="<c:out value="${ptpl.point4Km}" />" class="tInput" id="ptpPoint4Km"  style="width: 90px;"/><input type="hidden" name="ptpPoint4Hrs" value="<c:out value="${ptpl.point4Hrs}" />" class="tInput" id="ptpPoint4Hrs"  style="width: 90px;"/><input type="hidden" name="ptpPoint4Minutes" value="<c:out value="${ptpl.point4Minutes}" />" class="tInput" id="ptpPoint4Minutes"  style="width: 90px;"/><input type="hidden" name="ptpPoint4RouteId" value="<c:out value="${ptpl.point4RouteId}" />" class="tInput" id="ptpPoint4RouteId"  style="width: 90px;"/><c:out value="${ptpl.point4Name}" /></td>
                                                        <td><input type="hidden" name="ptpFinalPointId" value="<c:out value="${ptpl.finalPointId}" />" class="tInput" id="ptpFinalPointId"  style="width: 90px;"/><input type="hidden" name="ptpFinalPointKm" value="<c:out value="${ptpl.finalPointKm}" />" class="tInput" id="ptpFinalPointKm"  style="width: 90px;"/><input type="hidden" name="ptpFinalPointHrs" value="<c:out value="${ptpl.finalPointHrs}" />" class="tInput" id="ptpFinalPointHrs"  style="width: 90px;"/><input type="hidden" name="ptpFinalPointMinutes" value="<c:out value="${ptpl.finalPointMinutes}" />" class="tInput" id="ptpFinalPointMinutes"  style="width: 90px;"/><input type="hidden" name="ptpFinalPointRouteId" value="<c:out value="${ptpl.finalPointRouteId}" />" class="tInput" id="ptpFinalPointRouteId"  style="width: 90px;"/><c:out value="${ptpl.finalPointName}" /></td>

                                                        <td><input type="hidden" name="ptpTotalKm" value="<c:out value="${ptpl.totalKm}" />" class="tInput" id="ptpTotalKm"  style="width: 90px;"/><c:out value="${ptpl.totalKm}" /></td>
                                                        <td><input type="hidden" name="ptpTravelTime" value="<c:out value="${ptpl.totalHours}" />:<c:out value="${ptpl.totalMinutes}" />:00" class="tInput" id="<c:out value="${ptpl.totalHours}" />"  style="width: 90px;"/><c:out value="${ptpl.totalHours}" />:<c:out value="${ptpl.totalMinutes}" />:00</td>
                                                        <td><input type="hidden" name="ptpRateWithReefer" value="<c:out value="${ptpl.rateWithReeferPerKm}" />" class="tInput" id="ptpRateWithReefer"  style="width: 60px;" /><c:out value="${ptpl.rateWithReefer}" /></td>
                                                        <td><input type="hidden" name="ptpRateWithoutReefer" value="<c:out value="${ptpl.rateWithoutReeferPerKm}" />" class="tInput" id="ptpRateWithoutReefer" style="width: 60px;" /><c:out value="${ptpl.rateWithoutReefer}" /></td>
                                                        <td><input type="hidden" name="status" value="<c:out value="${ptpl.activeInd}" />" class="tInput" id="status" style="width: 60px;" /><c:out value="${ptpl.activeInd}" /></td>
                                                    </tr>
                                                    <%sno1++;%>
                                                </c:forEach >
                                            </tbody>
                                        </table>
                                    </c:if>
                                    <br>
                                    <script language="javascript" type="text/javascript">
                                        setFilterGrid("itemsTable");
                                    </script>
                                    <div id="controls">
                                        <div id="perpage">
                                            <select onchange="sorter.size(this.value)">
                                                <option value="5" selected="selected">5</option>
                                                <option value="10">10</option>
                                                <option value="20">20</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                            </select>
                                            <span>Entries Per Page</span>
                                        </div>
                                        <div id="navigation">
                                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                        </div>
                                        <div id="text">Displaying Page 1 of 1</div>
                                    </div>
                                    <script type="text/javascript">
                                        var sorter = new TINY.table.sorter("sorter");
                                        sorter.head = "head";
                                        sorter.asc = "asc";
                                        sorter.desc = "desc";
                                        sorter.even = "evenrow";
                                        sorter.odd = "oddrow";
                                        sorter.evensel = "evenselected";
                                        sorter.oddsel = "oddselected";
                                        sorter.paginate = true;
                                        sorter.currentid = "currentpage";
                                        sorter.limitid = "pagelimit";
                                        sorter.init("itemsTable", 1);
                                    </script>
                                </div>
                                <br>
                                <br>
                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                </center>
                                <br>
                                <br>
                            </div>

                        </c:if>

                        <c:if test="${billingTypeId == 2}">
                            <div id="ptpw">

                                <div class="inpad">
                                    <c:if test = "${ptpwBillingList != null}">
                                        <table class="table table-info mb30 table-hover sortable" width="98%"  id="itemsTable1" >
                                            <thead>
                                                <% int sno1 = 1;%>
                                                <tr height="60" id="tableDesingTH">
                                                    <th>S.No</th>
                                                    <th>Vehicle Route Contract Code</th>
                                                    <th>Vehicle Type</th>
                                                    <th>Vehicle Type</th>
                                                    <th>Load Type</th>
                                                    <th>Container Type</th>
                                                    <th>First Pick UP</th>
                                                    <th>Final Drop Point</th>
                                                    <th>Total Km</th>
                                                    <th>Rate with Reefer / KG</th>
                                                    <th>Rate without Reefer / KG</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach items="${ptpwBillingList}" var="ptpwl">
                                                    <tr>
                                                        <td><%=sno1%></td>
                                                        <td><input type="hidden" name="ptpwRouteContractCode" id="ptpwRouteContractCode" value="<c:out value="${ptpwl.routeCode}" />" class="tInput" style="width:120px"/><c:out value="${ptpwl.routeCode}" /></td>
                                                        <td><input type="hidden" name="vehicleTypeId" id="vehicleTypeId" value="<c:out value="${ptpwl.vehicleTypeId}" />" class="tInput"  style="width: 60px;"/><c:out value="${ptpwl.vehicleTypeName}" /></td>
                                                        <td><input type="hidden" name="loadTypeId" id="loadTypeId" value="<c:out value="${ptpwl.loadTypeId}" />" class="tInput"  style="width: 60px;"/>
                                                            <%--<c:out value="${ptpwl.loadTypeId}" />--%>
                                                            <c:if test="${ptpwl.loadTypeId == 1}" >
                                                                Empty Trip
                                                            </c:if>
                                                            <c:if test="${ptpwl.loadTypeId == 2}" >
                                                                Load Trip
                                                            </c:if>
                                                        </td>
                                                        <td><input type="hidden" name="containerTypeName" id="containerTypeName" value="<c:out value="${ptpwl.containerTypeName}" />" class="tInput"  style="width: 60px;"/><c:out value="${ptpwl.containerTypeName}" /></td>
                                                        <td><input type="hidden" name="containerQty1" id="containerQty1" value="<c:out value="${ptpwl.containerQty1}" />" class="tInput"  style="width: 60px;"/><c:out value="${ptpwl.containerQty1}" /></td>
                                                        <td><input type="hidden" name="ptpwFirstPickupId" id="ptpwFirstPickupId" value="<c:out value="${ptpwl.firstPickupId}" />" class="tInput"  style="width: 120px;"/><c:out value="${ptpwl.firstPickupName}" /></td>
                                                        <td><input type="hidden" name="ptpwFinalPointId" id="ptpwFinalPointId" value="<c:out value="${ptpwl.finalPointId}" />" class="tInput"  style="width: 120px;"/> <c:out value="${ptpwl.finalPointName}" /></td>
                                                        <td><input type="hidden" name="ptpwTotalKm" id="ptpwTotalKm" value="<c:out value="${ptpwl.totalKm}" />" class="tInput"  style="width: 120px;"/><input type="hidden" name="ptpwPointId" id="ptpwPointId" value="<c:out value="${ptpwl.finalPointId}" />" class="tInput"  style="width: 120px;"/><c:out value="${ptpwl.finalPointKm}" /></td>
                                                        <td><input type="hidden" name="ptpwRateWithReefer"  id="ptpwRateWithReefer" value="<c:out value="${ptpwl.rateWithReeferPerKg}" />" class="tInput" style="width: 120px;"/><c:out value="${ptpwl.rateWithReeferPerKg}" /></td>
                                                        <td><input type="hidden" name="ptpwRateWithoutReefer" id="ptpwRateWithoutReefer"  value="<c:out value="${ptpwl.rateWithoutReeferPerKg}" />" class="tInput" style="width: 120px;"/><c:out value="${ptpwl.rateWithoutReeferPerKg}" /></td>
                                                        <td><input type="hidden" name="status" id="status"  value="<c:out value="${ptpwl.status}" />" class="tInput" style="width: 120px;"/><c:out value="${ptpwl.activeInd}" /></td>
                                                    </tr>
                                                    <%sno1++;%>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                        <!--                                <center>
                                                                            <a  class="nexttab" href="#" ><input type="button" class="button" value="Next" name="Save" style="width: 120px"/></a>
                                                                        </center>-->
                                    </c:if>
                                    <br>
                                    <script language="javascript" type="text/javascript">
                                        setFilterGrid("itemsTable1");
                                    </script>
                                    <div id="controls">
                                        <div id="perpage">
                                            <select onchange="sorter.size(this.value)">
                                                <option value="5" selected="selected">5</option>
                                                <option value="10">10</option>
                                                <option value="20">20</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                            </select>
                                            <span>Entries Per Page</span>
                                        </div>
                                        <div id="navigation">
                                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                        </div>
                                        <div id="text">Displaying Page 1 of 1</div>
                                    </div>
                                    <script type="text/javascript">
                                        var sorter = new TINY.table.sorter("sorter");
                                        sorter.head = "head";
                                        sorter.asc = "asc";
                                        sorter.desc = "desc";
                                        sorter.even = "evenrow";
                                        sorter.odd = "oddrow";
                                        sorter.evensel = "evenselected";
                                        sorter.oddsel = "oddselected";
                                        sorter.paginate = true;
                                        sorter.currentid = "currentpage";
                                        sorter.limitid = "pagelimit";
                                        sorter.init("itemsTable1", 1);
                                    </script>

                                </div>
                                <!--                        <center>
                                                            <input type="button" class="button" value="Save" style="width: 120px" onclick="submitPage(this.value)"/>
                                                        </center>-->
                                <br>
                                <br>
                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                </center>
                                <br>
                                <br>
                            </div>
                        </c:if>




                        <c:if test="${billingTypeId == 3}">
                            <div id="mfr">
                                <c:if test="${actualKmBillingList != null}">
                                    <% int sno1 = 1;%>
                                    <table class="table table-info mb30 table-hover sortable" width="98%"  id="table2" >
                                        <thead>
                                            <tr height="30" id="tableDesingTH">
                                                <th>S.No</th>
                                                <th>Vehicle Type (Mfr/Model)</th>
                                                <th>Vehicle INR/Km</th>
                                                <th>Reefer INR/Hrs</th>
                                                <th>Select</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${actualKmBillingList}" var="mfrList">
                                                <tr height="30">
                                                    <td align="left"><%=sno1%></td>
                                                    <td align="left">
                                                        <input type="hidden" name="actualVehicleTypeId" value="<c:out value="${mfrList.vehicleTypeId}"/>" /><c:out value="${mfrList.vehicleTypeName}"/></td>
                                                    <td align="left"><input type="hidden" name="vehicleRatePerKm" class="form-control" value="<c:out value="${mfrList.vehicleRatePerKm}"/>"><c:out value="${mfrList.vehicleRatePerKm}"/></td>
                                                    <td align="left"><input type="hidden" name="reeferRatePerHour" class="form-control" value="<c:out value="${mfrList.reeferRatePerHour}"/>"><c:out value="${mfrList.reeferRatePerHour}"/></td>
                                                    <td align="left"><input type="hidden" name="status" value="<c:out value="${mfrList.activeInd}"/>"/><c:out value="${mfrList.activeInd}"/></td>
                                                </tr>
                                                <%sno1++;%>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </c:if>
                                <br>
                                <!--                        <center>
                                                            <input type="button" class="button" value="Save" style="width: 120px" onclick="submitPage(this.value)"/>
                                                        </center>-->
                                <br>
                                <script language="javascript" type="text/javascript">
                                    setFilterGrid("table2");
                                </script>
                                <div id="controls">
                                    <div id="perpage">
                                        <select onchange="sorter.size(this.value)">
                                            <option value="5" selected="selected">5</option>
                                            <option value="10">10</option>
                                            <option value="20">20</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select>
                                        <span>Entries Per Page</span>
                                    </div>
                                    <div id="navigation">
                                        <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                        <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                        <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                        <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                    </div>
                                    <div id="text">Displaying Page 1 of 1</div>
                                </div>
                                <script type="text/javascript">
                                    var sorter = new TINY.table.sorter("sorter");
                                    sorter.head = "head";
                                    sorter.asc = "asc";
                                    sorter.desc = "desc";
                                    sorter.even = "evenrow";
                                    sorter.odd = "oddrow";
                                    sorter.evensel = "evenselected";
                                    sorter.oddsel = "oddselected";
                                    sorter.paginate = true;
                                    sorter.currentid = "currentpage";
                                    sorter.limitid = "pagelimit";
                                    sorter.init("table2", 1);
                                </script>
                                <br>
                                <br>
                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                </center>
                                <br>
                                <br>
                            </div>
                        </c:if>

                        <script>
                            $('.btnNext').click(function() {
                                $('.nav-tabs > .active').next('li').find('a').trigger('click');
                            });
                            $('.btnPrevious').click(function() {
                                $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                            });
                        </script>

                    </div>
                    <script>
                        function viewModelFuel(typeId, contractRateId, status) {
//                             alert(typeId);               
                            $.ajax({
                                url: "/throttle/configModelFuel.do?",
                                data: {
                                    typeId: typeId, contractRateId: contractRateId, status: status

                                },
                                type: "GET",
                                success: function(response) {
                                    $("#modelFuelDetailsTab").html(response);
                                }
                            });
                        }

                    </script>
                    <br>
                    <br>
                    <div class="modal fade" id="myModal" role="dialog" style="width: auto;height: auto;">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" onclick="resetTheDetails1()">&times;</button>
                                    <h4 class="modal-title">View Model Fuel Details</h4>
                                </div>
                                <div class="modal-body" id="modelFuelDetailsTab" style="width: auto;height:auto;">
                                </div>
                                <div class="modal-footer">
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <br>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>


