

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Vendor</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Vendor</a></li>
            <li class="active">View Fleet Vendor Contract</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>

                <style>
                    body {
                        font:13px verdana;
                        font-weight:normal;
                    }
                </style>


                <form name="vehicleVendorContract" method="post" >
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>
                    <br>
                    <%--   <c:if test="${vendorVehicleContractLists != null}">
                        <c:forEach items="${vendorVehicleContractLists}" var="vendorContract">--%>
                    <%--    </c:forEach></c:if>--%>
                    <table class="table table-info mb30"  >
                        <tr id="tableDesingTD" height="30">
                            <td  colspan="4" >View Vendor Contract Info</td>
                        </tr>
                        <tr height="30">
                            <td class="text1">Vendor Name</td>
                            <td class="text1"><input type="hidden" name="vendorId" id="vendorId" value="<c:out value="${vendorId}"/>" class="textbox"><c:out value="${vendorName}"/></td>
                            <td class="text1">Contract Type</td>
                            <td class="text1"><input type="hidden" name="contractTypeId" id="contractTypeId" value="<c:out value="${contractTypeId}"/>" style="height:22px;width:150px;"/>
                                <c:if test="${contractTypeId == '1'}">
                                    Dedicated
                                </c:if>
                                <c:if test="${contractTypeId == '2'}">
                                    Hired
                                </c:if>
                                <c:if test="${contractTypeId == '3'}">
                                    Both
                                </c:if>
                            </td>
                        </tr>
                        <tr height="30">
                            <td class="text2">Contract From</td>
                            <td class="text2"><input type="hidden" name="startDate" id="startDate" value="<c:out value="${startDate}"/>" class="datepicker"><c:out value="${startDate}"/></td>
                            <td class="text2">Contract To</td>
                            <td class="text2"><input type="hidden" name="endDate" id="endDate" value="<c:out value="${endDate}"/>" class="datepicker"><c:out value="${endDate}"/></td>

                        </tr>
                        <tr height="30">
                            <td class="text1">Payment Type </td>
                            <td class="text1"><input type="hidden" name="paymentType" id="paymentType" value="<c:out value="${paymentType}"/>"/>
                                <c:if test="${paymentType =='1'}">
                                    Monthly
                                </c:if>
                                <c:if test="${paymentType == '2'}">
                                    Advance
                                </c:if>
                                <c:if test="${paymentType == '3'}">
                                    FortNight
                                </c:if>
                            </td>
                            <td class="text1" colspan="2"></td>
                        </tr>
                    </table>
                    <br>
                    <br>
                    <div id="tabs" >
                        <ul class="nav nav-tabs">

                            <%--  <c:if test="${contractTypeId == 1}">--%>
                            <li class="active" data-toggle="tab"><a href="#deD"><span>DEDICATED</span></a></li>
                                <%--     </c:if>--%>
                                <%--  <c:if test="${contractTypeId == 2}">--%>
                            <li data-toggle="tab"><a href="#fullTruck"><span>ON DEMAND</span></a></li>
                                <%--   </c:if>--%>
                        </ul>

                        <%--   <c:if test="${contractTypeId == 2}">--%>
                        <div id="fullTruck" class="tab-pane active">
                            <div id="routeFullTruck">
                                <c:if test="${hireList != null}">
                                    <table  id="table" class="table table-info mb30" style="width:100%" >
                                        <tr style="height: 40px;" id="tableDesingTH">
                                            <th align="center">S.No</th>
                                            <th align="center">Vehicle Type</th>
                                            <!--<th align="center">Vehicle Units<br><br></th>-->
                                            <th align="center">Origin</th>
                                            <th align="center">Interim Point1</th>
                                            <th align="center">Interim Point2</th>
                                            <th align="center">Interim Point3</th>
                                            <th align="center">Interim Point4</th>
                                            <th align="center">Destination</th>
                                            <!--                                        <th align="center">Trailer Type</th>
                                                                                    <th align="center">Trailer Units</th>-->
                                            <th align="center">Load Type</th>
                                            <th align="center">Container Type</th>
                                            <th align="center">Container Qty</th>
                                            <th align="center">Rate with Reefer</th>
                                            <th align="center">Rate without Reefer</th>
<!--                                             <td align="center">Container Type</td>
                                            <td align="center">Container Qty</td>-->
                                            <td align="center">Current Status</td>
                                            <!--<td align="center">Market Rate</td>-->
<!--                                            <th align="center">Travel Kms</th>
                                            <th align="center">Travel Hours</th>
                                            <th align="center">Travel Minutes</th>-->
                                            <th align="center">Active Status</th>
                                        </tr>
                                        <tbody>
                                            <% int index = 1;%>
                                            <c:forEach items="${hireList}" var="route">
                                                <%
                                                    String classText = "";
                                                    int oddEven = index % 2;
                                                    if (oddEven > 0) {
                                                        classText = "text2";
                                                    } else {
                                                        classText = "text1";
                                                    }
                                                %>
                                                <tr height="30">
                                                    <td class="<%=classText%>"><%=index++%></td>
                                                    <td class="<%=classText%>"><c:out value="${route.vehicleTypeId}"/></td>
                                                    <!--<td class="<%=classText%>"><c:out value="${route.vehicleUnits}"/></td>-->
                                                    <td class="<%=classText%>"><c:out value="${route.originNameFullTruck}"/></td>
                                                    <td class="<%=classText%>"><c:out value="${route.point1Name}"/></td>
                                                    <td class="<%=classText%>"><c:out value="${route.point2Name}"/></td>
                                                    <td class="<%=classText%>"><c:out value="${route.point3Name}"/></td>
                                                    <td class="<%=classText%>"><c:out value="${route.point4Name}"/></td>
                                                    <td class="<%=classText%>"><c:out value="${route.destinationNameFullTruck}"/></td>

<!--                                            <td class="<%=classText%>"><c:out value="${route.trailerType}"/></td>
                                            <td class="<%=classText%>"   ><c:out value="${route.trailorTypeUnits}"/></td>-->
                                                    <td class="<%=classText%>"   >
                                                        <c:if test="${route.loadTypeId =='1'}">
                                                            Empty Trip
                                                        </c:if>
                                                        <c:if test="${route.loadTypeId =='2'}">
                                                            Load Trip
                                                        </c:if>
                                                    </td>
                                                    <td class="<%=classText%>">
                                                        <c:forEach items="${containerTypeList}" var="conType">
                                                            <c:if test="${route.containerTypeId ==conType.containerId}">                                                    
                                                        <c:out value="${conType.containerName}"/>
                                                    </option>
                                                </c:if>
                                            </c:forEach>
                                            </td>
                                            <td class="<%=classText%>">
                                                <c:if test="${route.containerQty =='1'}">
                                                    1
                                                </c:if>
                                                <c:if test="${route.containerQty =='2'}">
                                                    2
                                                </c:if>
                                            </td>
                                            <td class="<%=classText%>"   ><c:out value="${route.spotCost}"/></td>
                                            <td class="<%=classText%>"   ><c:out value="${route.additionalCost}"/></td>
<!--                                            <td class="<%=classText%>"   ><c:out value="${route.travelKmFullTruck}"/></td>
                                            <td class="<%=classText%>"   ><c:out value="${route.travelHourFullTruck}"/></td>
                                            <td class="<%=classText%>"   ><c:out value="${route.travelMinuteFullTruck}"/></td>
                                           <td><c:if test="${containerTypeList != null}">
                                                            <select name="containerTypeId" id="containerTypeId" style="height:22px;width:130px;" disabled>
                                                         <option value="0">-Select-</option>
                                                         <c:forEach items="${containerTypeList}" var="conType">
                                                             <option value="<c:out value="${conType.containerId}"/>"><c:out value="${conType.containerName}"/></option>
                                                         </c:forEach>
                                                     </c:if>
                                                     </select>
                                                    </td>
                                                    <td><select name="containerQty" id="containerQty" style="height:22px;width:130px;" disabled>
                                                            <option value="0">-Select-</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                        </select>
                                                    </td>-->
                                                    <script>
                                                        document.getElementById("containerTypeId").value =<c:out value="${route.containerTypeId}"/>
                                                        document.getElementById("containerQty").value =<c:out value="${route.containerQty}"/>
                                                    </script>
                                                    <c:if test="${route.approvalStatus == 1}">
                                                    <td class="<%=classText%>"   ><span class="label label-success">approved</span></td>
                                                    </c:if>
                                                     <c:if test="${route.approvalStatus == 2}">
                                                    <td class="<%=classText%>"   ><span class="label label-warning">pending</span></td>
                                                    </c:if>
                                                     <c:if test="${route.approvalStatus == 3}">
                                                    <td class="<%=classText%>"   ><span class="label label-danger">rejected</span></td>
                                                    </c:if>
                                                    
                                            <td class="<%=classText%>"   >
                                                <c:if test="${route.activeInd == 'Y'}">
                                                    Active
                                                </c:if>
                                                <c:if test="${route.activeInd == 'N'}">
                                                    In Active
                                                </c:if>
                                            </td>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </c:if>
                            </div>
                            <br>
                            <br>
                            <center>
                                <a><input type="button" class="btn btn-info btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                            </center>
                            <br>
                            <br>
                        </div>
                        <%--   </c:if>--%>



                        <%--   <c:if test="${contractTypeId == 1}">--%>
                        <div id="deD"  class="tab-pane active" style="padding-left: 1px;">
                            <div id="routeWeightBreak">
                                <c:if test="${dedicateList != null}">
                                    <table  id="table" class="table table-info mb30" style="width:100%;" >
                                        <tr style="height: 40px;" id="tableDesingTH">
                                            <th align="center">S.No</th>
                                            <th align="center">Vehicle Type</th>
                                            <th align="center">Vehicle Units</th>
                                            <th align="center">Trailer Type</th>
                                            <th align="center">Trailer Units</th>
                                            <th align="center">Contract Category</th>
                                            <th align="center">Fixed Cost Per Vehicle & Month</th>
                                            <th colspan="2" align="center">Fixed Duration Per day<br>&emsp14;
                                        <table   border="0"><tr  id="tableDesingTD"><td width="250px;"><center>Hours</center></td><td width="212px;"><center>Minutes</center></td></tr></table></th>
                                        <th align="center">Total Fixed Cost Per Month</th>
                                        <th align="center">Rate Per KM</th>
                                        <th align="center">Rate Exceeds Limit KM</th>
                                        <th align="center">Max Allowable KM Per Month</th>
                                        <th align="center" colspan="2">Over Time Cost Per HR<br>&emsp14;
                                        <table   border="0"><tr  id="tableDesingTD"><td width="250px;"><center>Work Days</center></td><td width="212px;"><center>Holidays</center></td></tr></table></th>
                                        <th align="center">Additional Cost</th>
                                        <th align="center">Active Status</th>

                                        </tr>
                                        <tbody>
                                            <% int index1 = 1;%>
                                            <c:forEach items="${dedicateList}" var="weight">
                                                <%
                                                    String classText1 = "";
                                                    int oddEven1 = index1 % 2;
                                                    if (oddEven1 > 0) {
                                                        classText1 = "text2";
                                                    } else {
                                                        classText1 = "text1";
                                                    }
                                                %>
                                                <tr height="30">
                                                    <td class="<%=classText1%>"  ><%=index1++%></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.vehicleTypeIdDedicate}"/></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.vehicleUnitsDedicate}"/></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.trailorTypeDedicate}"/></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.trailorUnitsDedicate}"/></td>
                                                    <td class="<%=classText1%>"   >
                                                        <c:if test="${weight.contractCategory == '1'}">
                                                            Fixed </c:if>
                                                        <c:if test="${weight.contractCategory == '2'}">
                                                            Actual
                                                        </c:if></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.fixedCost}"/></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.fixedHrs}"/></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.fixedMin}"/></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.totalFixedCost}"/></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.rateCost}"/></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.rateLimit}"/></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.maxAllowableKM}"/></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.workingDays}"/></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.holidays}"/></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.addCostDedicate}"/></td>
                                                    <td class="<%=classText1%>"   >
                                                        <c:if test="${weight.activeInd == 'Y'}">
                                                            Active
                                                        </c:if>
                                                        <c:if test="${weight.activeInd == 'N'}">
                                                            In Active
                                                        </c:if>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table
                                </c:if>
                            </div>

                            <br>
                            <br>
                            <center>
                                <a><input type="button" class="btn btn-info btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                            </center>
                            <br>
                            <br>
                        </div>
                        <%--       </c:if>--%>
                        <script>
                            $('.btnNext').click(function() {
                                $('.nav-tabs > .active').next('li').find('a').trigger('click');
                            });
                            $('.btnPrevious').click(function() {
                                $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                            });
                        </script>


                    </div>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
