<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>C F S</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/CFS/css/main.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="/CFS/js/mootools.js"></script>
<style type="text/css">
<!--
a:link {
	text-decoration: none;
}
a:visited {
	text-decoration: none;
}
a:hover {
	text-decoration: underline;
}
a:active {
	text-decoration: none;
}
-->
</style></head>
<script type="text/javascript">

function screenSize() {
 var viewportwidth;
 var viewportheight;

 // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight

 if (typeof window.innerWidth != 'undefined')
 {

      viewportwidth = window.innerWidth,
      viewportheight = window.innerHeight
	  //alert(viewportwidth);
	  //alert(viewportheight);
 }

// IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)

 else if (typeof document.documentElement != 'undefined'
     && typeof document.documentElement.clientWidth !=
     'undefined' && document.documentElement.clientWidth != 0)
 {
       viewportwidth = document.documentElement.clientWidth,
       viewportheight = document.documentElement.clientHeight

       document.getElementById('frameSize').height = viewportheight-200;

 }

 // older versions of IE

 else
 {
       viewportwidth = document.getElementsById('wrapper')[0].clientWidth,
       viewportheight = document.getElementsById('wrapper')[0].clientHeight
       document.getElementById('frameSize').height = viewportheight-200;
 }

 document.getElementById('wrapper').style.height=viewportheight+'px';
  document.getElementById('wrapper').style.width=viewportwidth+'px';



}
</script>
<style type="text/css">
<!--
html,body {
height:100%;
margin:0;
padding:0;
}
iframe {
height:100%;
}
-->
.expand {
font-family:Arial, Helvetica, sans-serif;
font-size:12px;
font-weight:bold;
color:#FFFFFF;
}

.status {
font-family:Arial, Helvetica, sans-serif;
font-size:12px;
font-weight:bold;
color:#36BCF1;
}

.statusText {
font-family:Arial, Helvetica, sans-serif;
font-size:12px;
font-weight:bold;
color:#FFEA00;

}
</style>

<!-- slide menu script -->
<script type="text/javascript">
window.addEvent('domready', function() {
	var status = {
		'true': 'open',
		'false': 'close'
	};
	var myHorizontalSlide = new Fx.Slide('horizontal_slide', {mode: 'horizontal'});
	$('h_toggle').addEvent('click', function(e){
		e.stop();
		myHorizontalSlide.toggle();
	});
	myHorizontalSlide.addEvent('complete', function() {
		$('horizontal_status').set('html', status[myHorizontalSlide.open]);
	});
});
</script>
<!-- slide menu script -->

<body onLoad="screenSize();"  style="overflow:hidden; " scroll="no">
<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
<tr>
<td  align="center" valign="middle" id="wrapper">

					<table class="table" width="100%" cellpadding="0"  cellspacing="0" style="border:1px solid #0B92C9; " border="0" align="center" id="main_table" >
					<tr>
					<td height="58" colspan="2" >


					<!-- header Table -->

					<table width="100%" cellpadding="0"  cellspacing="0" border="0" align="center">
					<tr>
					<td width="33%" align="left"><img src="/CFS/images/logo.gif" alt="" /></td>
					<td width="44%" align="center" class="menu_bg_center" valign="bottom"><img src="/CFS/images/icon_home.gif" alt="Home" align="absmiddle" /><a href="/CFS/home.do" class="hdr_text"  target="frame">Home</a>&nbsp;&nbsp;<img src="/CFS/images/icon_logout.gif" alt="Home"  align="absmiddle"/><a href="/CFS/logout.do" class="hdr_text" >Logout</a>&nbsp;&nbsp;<img src="/CFS/images/icon_help.gif" alt="Home" align="absmiddle"/><a href="" class="hdr_text" >Help</a></td>
					<td width="33%" align="right"><img src="/CFS/images/header_right_bg.gif" alt=""/></td>
					</tr>
					</table>

					<!-- header Table -->


					</td>
					</tr>
					<tr>
					<td width="100%" height="51" colspan="2" class="menu_table_bg">
					<!-- menu table outer -->

					<table width="100%" cellpadding="0"  cellspacing="0" border="0">
					<tr>
					<td height="25" id="footerText" width="20%" align="left">Welcome : <%= session.getAttribute("userName")%></td>
					<td height="25" id="footerText" width="80%" align="left" style="padding:0px;"></td>
					</tr>
					<tr>
					<td height="26" align="left" style="padding-left:10px; ">
					<div style="width:200px; float:left; "  >

											<a id="h_toggle" href="" class="expand">Expand</a>
											<span class="status">| <strong>status</strong>&nbsp;: </span><span id="horizontal_status" class="statusText">open</span>
											</div>

					</td>
					<td height="26" valign="bottom" align="center">

					<%@include file="/content/common/menu22.jsp"%>


					</td>
					</tr>
					</table>

					<!-- menu table outer-->
					</td>
					</tr>
					<tr>
					<td id="frameSize" colspan="2" width="100%" bgcolor="#EDEDED" valign="top"><!-- content table -->

											<table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin:0px; ">
											<tr>
											<td   valign="top" >

											<div id="horizontal_slide" style="width:225px; overflow-y:auto; overflow-x:hidden; height:440px; "><%@include file="/content/common/subMenu.jsp"%></div></td>
											<td width="100%" bgcolor="#FFFFFF">
											<iframe id="frame" src="/throttle/content/common/welcome.htm" style="background-color:#FFFFFF; " name="frame" frameborder="0" width="100%"  scrolling="auto" marginwidth="0" marginheight="0" >  </iframe>
											<script type="text/javascript">
											function resizeIframe() {
												var height = document.documentElement.clientHeight;
												height -= 142;
												document.getElementById('frame').style.height = height +"px";
											};
											document.getElementById('frame').onload = resizeIframe;
											window.onresize = resizeIframe;
											</script>
											</td>
											</tr>
											</table>
					</td>
					</tr>
					<tr>
					
					<td width="50%" id="footerText" align="right" style="padding:0px 10px 0px 0px;">powered by entitle</td>
					</tr>
					</table>

</td>
</tr>
</table>
</body>
</html>
