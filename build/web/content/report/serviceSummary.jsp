<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="ets.domain.operation.business.OperationTO" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->
<script language="JavaScript" src="FusionCharts.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/prettify.js"></script>
<script type="text/javascript" src="js/json2.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
</script>

<script>
    function show_src() {
        document.getElementById('exp_table').style.display = 'none';
    }
    function show_exp() {
        document.getElementById('exp_table').style.display = 'block';
    }
    function show_close() {
        document.getElementById('exp_table').style.display = 'none';
    }

    function addRow(val) {
        //if(document.getElementById(val).innerHTML == ""){
        document.getElementById(val).innerHTML = "<input type='text' size='7' class='form-control' >";
        //}else{
        //document.getElementById(val).innerHTML = "";
        //}
    }

    function showTable()
    {
        var selectedMrs = document.getElementsByName("selectedIndex");
        var counter = 0;
        for (var i = 0; i < selectedMrs.length; i++) {
            if (selectedMrs[i].checked == 1) {
                counter++;
                break;
            }
        }
        if (counter == 0) {
            alert("Please Select MRS");
        } else {
            document.bill.action = "/throttle/mrsSummary.do";
            document.bill.submit();
        }
    }
    function newWO() {
        window.open('/throttle/content/stores/OtherServiceStockAvailability.html', 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
    }

    function submitPag(val) {
        if (validate() == '0') {
            return;
        } else if (val == 'purchaseOrder') {
            document.bill.purchaseType.value = "1012"
            document.bill.action = '/throttle/generateMpr.do'
            document.bill.submit();
        } else if (val == 'localPurchase') {
            document.bill.purchaseType.value = "1011"
            document.bill.action = '/throttle/generateMpr.do'
            document.bill.submit();
        }
    }



    function searchSubmit() {

        if (document.bill.fromDate.value == '') {
            alert("Please Enter From Date");
            return;
        } else if (document.bill.toDate.value == '') {
            alert("Please Enter to Date");
            return;
        }
        document.bill.action = '/throttle/serviceSummary.do'
        document.bill.submit();

    }


    function validate()
    {
        var selectedMrs = document.getElementsByName("selectedIndex1");
        var reqQtys = document.getElementsByName("reqQtys");
        var vendorIds = document.getElementsByName("vendorIds");
        var counter = 0;
        for (var i = 0; i < selectedMrs.length; i++) {
            if (selectedMrs[i].checked == 1) {
                counter++;
                if (parseInt(reqQtys[i].value) == 0 || isDigit(reqQtys[i].value)) {
                    alert("Please Enter Valid Required Quantity");
                    reqQtys[i].select();
                    reqQtys[i].focus();
                    return '0'
                }
                if (vendorIds[i].value == '0') {
                    alert("Please Select Vendor");
                    vendorIds[i].focus();
                    return '0'
                }
            }
        }
        if (counter == 0) {
            alert("Please Select Atleast one item");
            return '0';
        }
        return counter;
    }


    function selectBox(val) {
        var checkBoxs = document.getElementsByName("selectedIndex1");
        checkBoxs[val].checked = 1;
    }



    function setDate(fDate, tDate, serviceType, opId) {
        if (fDate != 'null') {
            document.bill.fromDate.value = fDate;
        }
        if (tDate != 'null') {
            document.bill.toDate.value = tDate;
        }
        if (serviceType != 'null') {
            document.bill.serviceType.value = serviceType;
        }
        if (opId != 'null') {
            document.bill.opId.value = opId;
        }
        if ('<%= request.getAttribute("usageTypeId") %>' != 'null') {
            document.bill.usageId.value = '<%= request.getAttribute("usageTypeId") %>';
        }
        if ('<%= request.getAttribute("custId") %>' != 'null') {
            document.bill.custId.value = '<%= request.getAttribute("custId") %>';
        }
    }



</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="fmsrenderservice.label.ServiceSummary" text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="fmsrenderservice.label.FMSRenderService" text="default text"/></a></li>
            <li class="active"><spring:message code="fmsrenderservice.label.ServiceSummary" text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body onLoad="setDate('<%= request.getAttribute("fromDate") %>', '<%= request.getAttribute("toDate") %>', '<%= request.getAttribute("serviceType") %>', '<%= request.getAttribute("opId") %>')">        

                <form name="bill"  method="post" >
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>

                    <!--<table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                        <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                        <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                        </tr>
                        <tr id="exp_table" >
                        <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                            <div class="tabs" align="left" style="width:850;">
                        <ul class="tabNavigation">
                                <li style="background:#76b3f1">Service Summary</li>
                        </ul>
                        <div id="first">-->
                    <table class="table table-info mb30 table-hover">
                        <thead><tr><th colspan="4"><spring:message code="fmsrenderservice.label.ServiceSummary"  text="default text"/></th></tr></thead>
                        <tr>
                            <td height="30"><spring:message code="fmsrenderservice.label.UsageType"  text="default text"/></td>
                            <td><select class="form-control" style="width:260px;height:40px;" name="usageId"  >
                                    <option value="0">---<spring:message code="fmsrenderservice.label.Select"  text="default text"/>---</option>
                                    <c:if test = "${UsageList != null}" >
                                        <c:forEach items="${UsageList}" var="Dept">
                                            <option value='<c:out value="${Dept.usageId}" />'><c:out value="${Dept.usageName}" /></option>
                                        </c:forEach >
                                    </c:if>
                                </select></td>
                            <td height="30"><spring:message code="fmsrenderservice.label.ServiceType"  text="default text"/></td>
                            <td height="30"><select class="form-control" style="width:260px;height:40px;" name="serviceType">
                                    <option selected   value=0>---<spring:message code="fmsrenderservice.label.Select"  text="default text"/>---</option>
                                    <c:if test = "${serviceTypes != null}" >
                                        <c:forEach items="${serviceTypes}" var="mfr">
                                            <c:if test = "${mfr.servicetypeId ==1 }" >
                                                <option value='<c:out value="${mfr.servicetypeId}" />'>
                                                    <c:out value="${mfr.servicetypeName}" />
                                                </c:if>
                                                <c:if test = "${mfr.servicetypeId !=1 }" >
                                                <option  value='<c:out value="${mfr.servicetypeId}" />'>
                                                    <c:out value="${mfr.servicetypeName}" />
                                                </c:if>
                                            </c:forEach >
                                        </c:if>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td height="30"><spring:message code="fmsrenderservice.label.Customer"  text="default text"/></td>
                            <td height="30">
                                <select class="form-control" style="width:260px;height:40px;" name="custId"  >
                                    <c:if test = "${customerList != null}" >
                                        <c:forEach items="${customerList}" var="cust">
                                            <option value='<c:out value="${cust.custId}" />'><c:out value="${cust.custName}" /></option>
                                        </c:forEach >
                                    </c:if>
                                </select>
                            </td>
                            <td height="30"><spring:message code="fmsrenderservice.label.Select"  text="default text"/></td>
                            <td><select  class="form-control" style="width:260px;height:40px;" name="opId"  >
                                    <option value="0">---<spring:message code="fmsrenderservice.label.Select"  text="default text"/>---</option>
                                    <option value='EXTERNAL SERVICE' > <spring:message code="fmsrenderservice.label.EXTERNALSERVICE"  text="default text"/> </option>
                                    <c:if test = "${operationPointList != null}" >
                                        <c:forEach items="${operationPointList}" var="Dept">
                                            <option value='<c:out value="${Dept.opId}" />'> <c:out value="${Dept.opName}" /></option>
                                        </c:forEach >

                                    </c:if>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td height="30"><spring:message code="fmsrenderservice.label.FromDate"  text="default text"/> </td>
                            <td height="30"><input type="text" name="fromDate" class="datepicker form-control" style="width:260px;height:40px;" autocomplete="off" >
                                <!--<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.bill.fromDate,'dd-mm-yyyy',this)"/> </td>-->
                            <td height="30"><spring:message code="fmsrenderservice.label.ToDate"  text="default text"/> </td>
                            <td height="30"><input name="toDate" type="text" class="datepicker form-control" style="width:260px;height:40px;" autocomplete="off" >
                                <!--<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.bill.toDate,'dd-mm-yyyy',this)"/></td>-->

                        </tr>
                        <tr>
                            <td colspan="4" align="center" ><input type="button" class="btn btn-success" readonly name="search" value="<spring:message code="fmsrenderservice.label.Search"  text="default text"/>" onClick="searchSubmit();" ></td>
                        </tr>
                    </table>
                    <!--    </div></div>
                        </td>
                        </tr>
                        </table>-->

                    <!--<br>-->

                    <% int index = 0;
                    String classText = "";
                    int oddEven = 0;
                    %>    
                    <c:if test = "${serviceList != null}" >


                        <table class="table table-info mb30 table-hover" id="bg" >
                            <thead>

                                <tr >
                                    <th  ><spring:message code="fmsrenderservice.label.Sno"  text="default text"/></th>
                                    <th  height="30" ><spring:message code="fmsrenderservice.label.VehicleNumber"  text="default text"/> </th>
                                    <th  ><spring:message code="fmsrenderservice.label.ServiceType"  text="default text"/> </th>
                                    <th  ><spring:message code="fmsrenderservice.label.Location"  text="default text"/> </th>
                                    <th  height="30" ><spring:message code="fmsrenderservice.label.Customer"  text="default text"/></th>
                                    <th  ><spring:message code="fmsrenderservice.label.UsageType"  text="default text"/></th>
                                    <th  height="30" ><spring:message code="fmsrenderservice.label.Total"  text="default text"/><br><spring:message code="fmsrenderservice.label.Complaints"  text="default text"/>  </th>
                                    <th  height="30" ><spring:message code="fmsrenderservice.label.Attended"  text="default text"/><br> <spring:message code="fmsrenderservice.label.Complaints"  text="default text"/> </th>
                                    <th  height="30" > <spring:message code="fmsrenderservice.label.Status"  text="default text"/> </th>                        
                                    <th  > <spring:message code="fmsrenderservice.label.TotalDays"  text="default text"/><br> <spring:message code="fmsrenderservice.label.Taken"  text="default text"/> </th>
                                </tr>
                            </thead>

                            <c:forEach items="${serviceList}" var="service">    
                                <%

                    oddEven = index % 2;
                    if (oddEven > 0) {
                        classText = "text2";
                    } else {
                        classText = "text1";
                    }
                                %>


                                <tr>
                                    <td  height="30"> <%= index+1 %> </td>
                                    <td  height="30"><c:out value="${service.regNo}"/></td>
                                    <td  height="30"><c:out value="${service.serviceName}"/></td>
                                    <td  height="30"><c:out value="${service.companyName}"/></td>
                                    <td  height="30"><c:out value="${service.custName}"/></td>
                                    <td  height="30"><c:out value="${service.usageType}"/></td>
                                    <td  height="30"><c:out value="${service.raisedProp}"/></td>
                                    <td  height="30"><c:out value="${service.completedProp}"/></td>
                                    <td  height="30"><c:out value="${service.problemStatus}"/></td>
                                    <td  height="30"><c:out value="${service.day}"/></td>
                                </tr>
                                <%
                    index++;
                                %>
                            </c:forEach>
                        </table>
                    </c:if>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
