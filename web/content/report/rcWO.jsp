

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    

    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
    <%@ page import="ets.domain.report.business.ReportTO" %> 
 <%@ page import="java.util.*" %> 
    <title>RCWO</title>




</head>
<body>


<script>
    function submitPage(val){
        if(val=='approve'){
            document.mpr.status.value="APPROVED"
            document.mpr.action="/throttle/approveMpr.do"     
            document.mpr.submit();
        }else if(val=='reject'){
        document.mpr.status.value="REJECTED"                
        document.mpr.action="/throttle/approveMpr.do"                
        document.mpr.submit();
    }
}            

    
    function print()
    {       
        var DocumentContainer = document.getElementById('print');
        var WindowObject = window.open('', "TrackHistoryData", 
            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        //WindowObject.close();   
    }            


</script>

<form name="mpr"  method="post" >                        
    <br>
    
  
<%
int  index=0;
ArrayList woDetail = new ArrayList();
woDetail  = (ArrayList) request.getAttribute("RCWODetail");
ReportTO purch = new ReportTO();
ReportTO headContent = new ReportTO();
int itemNameLimit = 30;
int mfrCodeLimit = 10;
headContent = (ReportTO)woDetail.get(0);
String[] address = headContent.getAddressSplit();
String itemName = "";
String mfrCode = "";
int listSize=10;

for(int i=0; i<woDetail.size(); i=i+listSize) {
%>
<div id="print" >
    
<style type="text/css">
.header {font-family:Arial;
font-size:15px;
color:#000000;
text-align:center;
 padding-top:10px;
 font-weight:bold;
}
.border {border:1px;
border-color:#000000;
border-style:solid;
}
.text1 {
font-family:Arial, Helvetica, sans-serif;
font-size:14px;
color:#000000;
}
.text2 {
font-family:Arial, Helvetica, sans-serif;
font-size:16px;
color:#000000;
}
.text3 {
font-family:Arial, Helvetica, sans-serif;
font-size:18px;
color:#000000;
}

</style>  

<table width="700" align="center" border="0" cellpadding="0" cellspacing="0" >
<tr>
<td valign="top" colspan="2" height="30" align="center" class="border"><strong> WORK ORDER</strong></td>
</tr>
<tr>
<td height="125" valign="top">
<table width="350" height="131" border="0" cellpadding="0" cellspacing="0" class="border">
<tr>
<Td height="80" width="400" class="text3" align="center">
<strong> your company name.
</Td>
</tr>
<tr>
<Td align="center" class="text1" style="padding-left:10px; ">address 1,<br>
address 2.<br>
PHONE-xxxxxx.
</Td>
</tr>
</table>
</td>
<td height="125"  valign="top" >
   <%  index = 0;%>                 
<table width="350" height="131" border="0" cellpadding="0" cellspacing="0">
<tr>
<Td height="125" width="300" valign="top">
<table height="131" width="350" align="left" border="0" cellpadding="0" cellspacing="0" class="border" >
<tr>
<td colspan="2" class="text1" style="padding-left:10px;" ><strong>To</strong></td>
</tr>
     
 <% if (index == 0) {%>     
<tr>
<td colspan="2" class="text2" style="line-height:20px;"  height="107" style="padding-left:10px;">
    
                                    <p><strong>M/S&nbsp;&nbsp; <%= headContent.getVendorName() %></strong> <br>                                
                                    <% for(int k=0;k<address.length ; k++){ %>
                                      <%= address[k] %>  <br>
                                    <% } %>         
  
                                    Phno &nbsp;:&nbsp; <%= headContent.getPhone() %> </p>  
    </td>
</tr>
<tr>
<td class="text1" width="137" style="padding-left:10px;"> WO.No.:<strong><span class="text3" >  <%= headContent.getRcWoId() %></span></strong> </td>
<td class="text1" width="161"  style="padding-left:10px;">Date:<strong> <span class="text3" >  <%= headContent.getCreatedDate() %></span></strong> </td>
</tr>

                        <% index++;
            }%>                
                  
               
</table>
</Td>
</tr>
</table>
</td>
</tr>

<tr>
<td valign="top" colspan="2">
<table width="700" border="0" cellpadding="0" cellspacing="0">    
<tr>
<td class="text1" width="350"  style="padding-left:10px;">Make:<span class="text3" ><%= headContent.getMfrName() %></span> </td>
<td class="text1" width="350"  style="padding-left:10px;"> Vehicle No :<strong><span class="text3" >  <%= headContent.getRegNo() %></span></strong> </td>
</tr>    
</table>            
</td>
</tr>




<tr>
<Td valign="top" colspan="2">
    
    

<table width="700" align="center" border="0" cellpadding="0" cellspacing="0" class="border" style="margin-bottom:25px; ">
      
       
        <tr>
        <Td class="text2" width="20" height="28" valign="top" class="border" style="padding-left:10px; border:1px; border-color:#000000; border-style:solid;"> <strong>S.NO</strong></Td>
        <Td class="text2" valign="top" width="110" style="padding-left:10px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>FOLIO NO</strong></Td>
        <Td class="text2"  valign="top" width="400" style="padding-left:10px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>NOMENCLATURE</strong></Td>
        <Td class="text2"  valign="top" width="150" style="padding-left:10px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>REF NO</strong></Td>
        <Td class="text2"  valign="top" width="60" style="padding-left:10px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>UOM</strong></Td>
        <Td class="text2"  valign="top" width="60" style="padding-left:10px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>QUANTITY</strong></Td>
        </tr>
         <%	index = 0;
	for(int j=i; ( j < (listSize+i) ) && (j<woDetail.size() ) ; j++){	
	purch = new ReportTO();	
		purch = (ReportTO)woDetail.get(j);                
                mfrCode = purch.getMfrCode();
                itemName  = purch.getItemName();
                
                if(purch.getMfrCode().length() > mfrCodeLimit ){
                    mfrCode = mfrCode.substring(0,mfrCodeLimit-1);
                }if(purch.getItemName().length() > itemNameLimit ){
                    itemName = itemName.substring(0,itemNameLimit-1);
                }
                System.out.println("j=="+j);		
%>     

           
             
                <tr>
                    <Td valign="top" HEIGHT="20" class="text1" width="25"  style="border:1px;  border-right-style:solid; border-left-style:solid;" align="right"   ><%= j+1 %></td>                                     
                    <Td valign="top" HEIGHT="20"  class="text1" width="90"  style="border:1px;  border-right-style:solid; border-left-style:solid;" align="center"><%= mfrCode %>&nbsp; </td>                                     
                    <Td valign="top" HEIGHT="20"  class="text1" width="400"  style="border:1px; border-right-style:solid; border-left-style:solid;" align="left"> <%= itemName %></td>
                    <Td valign="top" HEIGHT="20"  class="text1" width="60"  style="border:1px;  border-right-style:solid; border-left-style:solid;" align="center"><%= purch.getRcCode() %> </td>
                    <Td valign="top" HEIGHT="20"  class="text1" width="60"  style="border:1px;  border-right-style:solid; border-left-style:solid;" align="center"><%= purch.getUomName() %></td>                
                    <Td valign="top" HEIGHT="20"  class="text1" width="60"  style="border:1px;  border-right-style:solid; border-left-style:solid;" align="right"><%= purch.getQuantity() %></td>                
                </tr>
                <%
            index++;}
                %>
           
            <% while(index <= 10 ){ %>
            <tr>
                    <Td valign="top" HEIGHT="20"  class="text1" width="20"  style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;"  > &nbsp; </td>                                     
                    <Td valign="top" HEIGHT="20"  class="text1" width="80"  style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="left">   &nbsp;</td>                                     
                    <Td valign="top" HEIGHT="20"  class="text1" width="200"  style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="left"> &nbsp;</td>
                    <Td valign="top" HEIGHT="20"  class="text1" width="60"  style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="center"> &nbsp;</td>
                    <Td valign="top" HEIGHT="20"  class="text1" width="60"  style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right"> &nbsp; </td>                            
                    <Td valign="top" HEIGHT="20"  class="text1" width="60"  style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right"> &nbsp; </td>                            
                                             
                </tr>
            <% index++; } %>            
                   
    </table>   
</td>

<Tr>
<Td valign="top" colspan="3">
<table width="700" height="50" align="center" border="0" cellpadding="0" cellspacing="0" class="border">

<Tr>
<Td height="30" class="text1" ALIGN="left" ><strong>REPORT</strong></Td>
<Td width="600" height="30" ALIGN="left" >
            <p>                        
                                    <%

                                    for(int l=0;l< headContent.getRemarksSplit().length ; l++){ %>
                                      <%= headContent.getRemarksSplit()[l] %>  <br>
                                    <% }
                                    %>
</Td>
</tr>
<Tr>
<Td >&nbsp;</Td>
<Td >&nbsp;</Td>
</Tr>

</table>

</td>
</Tr>




<Tr>
<Td width="110" class="text1" >&nbsp;</Td>
<Td width="336">&nbsp;</Td>
<Td width="154" class="text1" align="right" >&nbsp;</Td>
</Tr>


<Tr>
<Td valign="top" colspan="3">
<table width="700" height="30" align="center" border="0" cellpadding="0" cellspacing="0" class="border">

<Tr>
<Td width="110" class="text1" height="30" > &nbsp; </Td>
<Td width="336" class="text1" height="30" >&nbsp;</Td>
<Td width="154" class="text1" height="30" >&nbsp;</Td>
</Tr>

<Tr>
<Td width="110" class="text1"><strong>Inspected by</strong></Td>
<Td width="336" class="text1">&nbsp;</Td>
<Td width="154" class="text1"><strong>Authorised Signatory</strong></Td>
</Tr>
</table>
</Td>
</Tr>
</table>
</div>
    <center>   
        <input type="button" class="button" name="Print" value="Print" onClick="print();" > &nbsp;        
    </center>
<% } %>  
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
