<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <!--        <script type="text/javascript" src="/throttle/js/suest"></script>
                <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
                <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
                <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
                <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />-->

        <!--        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
                <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
                <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>-->

        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>






        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });



        </script>
        <script  type="text/javascript" src="js/jq-ac-script.js"></script>


        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $("#tabs").tabs();
            });
        </script>


        <script type="text/javascript" language="javascript">




            function getDriverName() {
                var oTextbox = new AutoSuggestControl(document.getElementById("driName"), new ListSuggestions("driName", "/throttle/handleDriverSettlement.do?"));

            }
        </script>
        <script language="">
            function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }

            function popUp(url) {
                var http = new XMLHttpRequest();
                http.open('HEAD', url, false);
                http.send();
                if (http.status != 404) {
                    popupWindow = window.open(
                    url, 'popUpWindow', 'height=400,width=500,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes');
                }
                else {
                    var url1 = "/throttle/content/trip/fileNotFound.jsp";
                    popupWindow = window.open(
                    url1, 'popUpWindow', 'height=400,width=500,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes');
                }
            }

        </script>





    </head>


    <body>

        <form name="trip" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <div id="tabs" >
                <ul>
                    <li><a href="#vehicleDetail"><span>Trailer Details</span></a></li>
                    <li><a href="#oemDetail"><span>OEM Details</span></a></li>
                    <li><a href="#insuranceDetail"><span>Insurance</span></a></li>
                    <li><a href="#taxDetail"><span>Road Tax</span></a></li>
                </ul>
                <div id="vehicleDetail">
                    <c:if test="${vehicleDetail != null}" >
                        <table  border="0" class="border" align="center" width="80%" cellpadding="0" cellspacing="0" id="bg">
                            <c:forEach items="${vehicleDetail}" var="veh" >
                                <tr>
                                    <td colspan="4" height="30" class="contenthead">View Trailer</td>
                                </tr>
                                <tr>
                                    <td class="text2" height="30">Trailer Number</td>
                                    <td class="text2" height="30"><c:out value="${veh.trailerNo}" /></td>
                                    <td class="text2" height="30">Registration Date</td>
                                    <td class="text2" height="30"><c:out value="${veh.dateOfSale}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text1" height="30">Warranty (Days)</td>
                                <td class="text1" height="30"><c:out value="${veh.war_period}" />
                                    <td class="text2" height="30"><font color="red">*</font>Warranty Date</td>
                                    <td class="text2" height="30"><c:out value="${veh.warrantyDate}"/></td>
                                </tr>
                                <tr>
                                                               <tr>
                                    <td class="text2" height="30">MFRs</td>
                                    <td class="text2"  >
                                        <c:if test = "${MfrList != null}" >
                                            <c:forEach items="${MfrList}" var="Dept">
                                                <c:choose>
                                                    <c:when test="${Dept.mfrId == veh.mfrId}" >
                                                        <c:out value="${Dept.mfrName}" />
                                                    </c:when>

                                                </c:choose>
                                            </c:forEach >
                                        </c:if>
                                    </td>
                                <td class="text1" height="30">Ownership</td>
                            <td class="text1" height="30">

                                    <c:if test = "${leasingCustList != null}" >
                                        <c:forEach items="${leasingCustList}" var="lcl">
                                            <c:choose>
                                                <c:when test="${lcl.vendorId == veh.vendorId}">
                                                    <c:out value="${lcl.vendorName}" />
                                                </c:when>
                                                
                                            </c:choose>
                                        </c:forEach>
                                    </c:if>
                               
                            </td>
                                    
                                </tr>

                                <tr>
                                    <td class="text1" height="30">Model </td>
                                    <td class="text1" height="30">
                                        <c:if test = "${modelList != null}" >
                                            <c:forEach items="${modelList}" var="Dept">
                                                <c:if test="${Dept.modelId == veh.modelId}" >
                                                    <c:out value="${Dept.modelName}" />
                                                </c:if>
                                            </c:forEach >
                                        </c:if>
                                    </td>
                                    <td class="text1" height="30">Tonnage</td>
                                    <td class="text1" height="30"><c:out value="${veh.seatCapacity}" /></td>
                                </tr>
                                <tr>
                                </tr>
                              
                              
                                <!--                    <td class="text1" height="30">Next FC Date</td>
                                                    <td class="text1" height="30"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.addVehicle.nextFCDate,'dd-mm-yyyy',this)"/></td>                    -->
                              
                                <tr>


                                    <td class="text1" height="30">Status </td>
                                    <td class="text1"  >
                                        <c:choose>
                                            <c:when test="${veh.activeInd == 'Y'}" >
                                                Active
                                            </c:when>
                                            <c:otherwise>
                                                InActive
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td class="text1" height="30" colspan="2" >&nbsp;</td>
                                </tr>
                            </c:forEach>
                        </table>
                    </c:if>

                    <center>
                        <a  class="nexttab" href=""><input type="button" class="button" value="Next" name="Next" /></a>
                    </center>

                </div>
       <!--         <div id="oemDetail">
                    <c:if test="${vehTyreList != null}" >
                        <% int index = 0;
                                    String classText = "";
                        %>
                        <table border="0" class="border" align="center" width="80%" cellpadding="0" cellspacing="0" id="addTyres">
                            <tr >
                                <td  class="contenthead" align="center" height="30" ><div class="contenthead">Sno</div></td>
                                <td  class="contenthead" height="30" ><div class="contenthead">Item</div></td>
                                <td  class="contenthead" height="30" ><div class="contenthead">Position</div> </td>
                                <td  class="contenthead" height="30" ><div class="contenthead">Tyre Number</div></td>
                                <td  class="contenthead" height="30" ><div class="contenthead">Date</div></td>
                            </tr>


                            <c:forEach items="${vehTyreList}" var="veh">
                                <%

                                            int oddEven = index % 2;
                                            if (oddEven > 0) {
                                                classText = "text1";
                                            } else {
                                                classText = "text2";
                                            }
                                %>
                                <tr>
                                    <td class="<%=classText%>" height="30" > <%= index + 1%> </td>

                                    <td class="<%=classText%>" height="30" >

                                        <c:if test="${tyreItemList != null}" >
                                            <c:forEach items="${tyreItemList}" var="item">
                                                <c:choose>
                                                    <c:when test="${item.itemId == veh.itemId }" >
                                                        <c:out value="${item.itemName}" />
                                                    </c:when>
                                                </c:choose>
                                            </c:forEach>
                                        </c:if>
                                    </td>


                                    <td class="<%=classText%>" height="30" >
                                        <c:if test="${positionList != null}" >
                                            <c:forEach items="${positionList}" var="item">
                                                <c:choose>
                                                    <c:when test="${item.posId == veh.posId }" >
                                                        <c:out value="${item.posName}" />
                                                    </c:when>
                                                </c:choose>

                                            </c:forEach>
                                        </c:if>
                                    </td>

                                    <td class="<%=classText%>" height="30" >
                                        <c:out value="${veh.tyreNo}" />
                                        <input type="hidden" name="tyreIds" value='<c:out value="${veh.tyreId}" />' >
                                    </td>
                                    <td class="<%=classText%>" height="30" >
                                        <c:out value="${veh.tyreD}" />
                                    </td>


                                </tr>
                                <% index++;%>
                            </c:forEach>
                        </table>
                    </c:if>

                    <center>
                        <a  class="nexttab" href=""><input type="button" class="button" value="Next" name="Next" /></a>
                    </center>
                </div>
                <div id="insuranceDetail">
                    <c:if test="${vehicleAMC != null}">
                        <c:forEach items="${vehicleAMC}" var="vAmc" >
                            <tr>
                                <td class="texttitle1">AMC Company</td>
                                <td class="text1"><c:out value="${vAmc.amcCompanyName}" /></td>
                                <td class="texttitle1">AMC Amount</td>
                                <td class="text1"><c:out value="${vAmc.amcAmount}" /></td>
                            </tr>
                            <tr>
                                <td class="texttitle2">AMC Duration</td>
                                <td class="text2"><c:out value="${vAmc.amcDuration}" /></td>
                                <td class="texttitle2">AMC Cheque Date</td>
                                <td class="text2"><c:out value="${vAmc.amcChequeDate}" /></td>
                            </tr>
                            <tr>
                                <td class="texttitle1">AMC Cheque No</td>
                                <td class="text1"><c:out value="${vAmc.amcChequeNo}" /></td>
                                <td class="texttitle1">AMC From Date Date</td>
                                <td class="text1"><c:out value="${vAmc.amcFromDate}" /></td>
                            </tr>
                            <tr>
                                <td class="texttitle2">AMC To Date</td>
                                <td class="text2"><c:out value="${vAmc.amcToDate}" /></td>
                                <td class="texttitle2">Remarks</td>
                                <td class="text2"><c:out value="${vAmc.remarks}" /></td>
                            </tr>
                        </c:forEach>
                    </c:if>
                    <center>
                        <a  class="nexttab" href=""><input type="button" class="button" value="Next" name="Next" /></a>
                    </center>
                </div>
                <div id="taxDetail">
                    <c:if test="${RoadTaxList == null }" >
                        <br>
                        <center><font color="red" size="2"> no records found </font></center>
                    </c:if>
                    <c:if test="${RoadTaxList != null }" >
                        <table align="center" width="700" cellpadding="0" cellspacing="0"  class="border">

                            <tr>

                                <td class="contentsub" height="30"><div class="contentsub">Sno</div></td>
                                <td class="contentsub" height="30"><div class="contentsub">Vehicle Number</div></td>
                                <td class="contentsub" height="30"><div class="contentsub">Tax Receipt No</div></td>
                                <td class="contentsub" height="30"><div class="contentsub">Tax Receipt Date</div></td>
                                <td class="contentsub" height="30"><div class="contentsub">Tax Paid Location</div></td>
                                <td class="contentsub" height="30"><div class="contentsub">Tax Amount</div></td>
                                <td class="contentsub" height="30"><div class="contentsub">Next Tax Date</div></td>
                            </tr>
                            <br>
                            <%
                                String style = "text1";
                                int index1 = 1;%>
                            <c:forEach items="${RoadTaxList}" var="Rtl" >
                                <%
                                            if ((index1 % 2) == 0) {
                                                style = "text1";
                                            } else {
                                                style = "text2";
                                    }%>
                                <tr>
                                    <td class="<%= style%>" height="30" style="padding-left:30px; "> <%= index1%> </td>
                                    <td class="<%= style%>" height="30" style="padding-left:30px; "> <c:out value="${Rtl.regNo}" /></td>
                                    <td class="<%= style%>" height="30" style="padding-left:30px; "><c:out value="${Rtl.roadtaxreceiptno}" /></td>
                                    <td class="<%= style%>" height="30" style="padding-left:30px; "><c:out value="${Rtl.roadtaxreceiptdate}" /></td>
                                    <td class="<%= style%>" height="30" style="padding-left:30px; "><c:out value="${Rtl.roadtaxpaidlocation}" /></td>
                                    <td class="<%= style%>" height="30" style="padding-left:30px; "><c:out value="${Rtl.roadtaxamount}" /></td>
                                    <td class="<%= style%>" height="30" style="padding-left:30px; "><c:out value="${Rtl.nextRoadTaxDate}" /></td>
                                </tr>
                                <% index1++;%>
                            </c:forEach>
                        </table>
                    </c:if>

                </div>   -->






                <script>
                    $(".nexttab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected + 1);
                    });
                </script>

            </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>