<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <!--        <script type="text/javascript" src="/throttle/js/suest"></script>
                <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
                <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
                <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
                <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />-->

        <!--        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
                <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
                <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>-->

        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>






        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });



        </script>
        <script  type="text/javascript" src="js/jq-ac-script.js"></script>


        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $("#tabs").tabs();
            });
        </script>


        <script type="text/javascript" language="javascript">




            function getDriverName() {
                var oTextbox = new AutoSuggestControl(document.getElementById("driName"), new ListSuggestions("driName", "/throttle/handleDriverSettlement.do?"));

            }
        </script>
        <script language="">
            function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }

            function popUp(url) {
                var http = new XMLHttpRequest();
                http.open('HEAD', url, false);
                http.send();
                if (http.status != 404) {
                    popupWindow = window.open(
                    url, 'popUpWindow', 'height=400,width=500,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes');
                }
                else {
                    var url1 = "/throttle/content/trip/fileNotFound.jsp";
                    popupWindow = window.open(
                    url1, 'popUpWindow', 'height=400,width=500,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes');
                }
            }

        </script>





    </head>


    <body >

        <form name="trip" method="post">
            <br>
            <center><h1>Estimated expense details</h1></center>
            <br>
            <center><h2><b>Vehicle Type:</b><c:out value="${vehicleTypeName}"/></h2></center>
            <br>

                        <table  border="1" align="center" width="80%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center"><b>Sno</b></td>
                                <td align="center"><b>Vehicle Ownership Type</b></td>
                                <td align="center"><b>Estimated Expense (INR)</b></td>
                                <td align="center"><b>Estimated Revenue</b></td>
                            </tr>
                            <% int cntr = 1; %>
                    <c:if test="${ownVehicleTypeRate != null}" >
                        <tr>
                                <td><%=cntr%></td>
                                <td>Own Vehicle</td>
                                <td  align="right" >
                                    <br>
                                   <table border="1">
                                        <tr>
                                            <td><b>Estimate (INR)</b></td>
                                            <td></td>
                                            <td><b>Profit Margin (%)</b></td>
                                        </tr>
                                        <tr>
                                            <td align="right" >
                                               <fmt:formatNumber type="number"  maxFractionDigits="2" value="${ownVehicleTypeRate * routeDistance  * noOfVehicles }" />
                                            </td>
                                            <td></td>
                                             <td>
                                                 <c:forEach items="${freightRate}" var="conOrderId" >
                                                    <fmt:formatNumber type="number"  maxFractionDigits="2" value="${((conOrderId.freightCharges-ownVehicleTypeRate * routeDistance  * noOfVehicles)/conOrderId.freightCharges)*100}" />
                                                </c:forEach>
                                             </td>
                                        </tr>
                                      </table>
                                <td align="right">
                                    <c:if test="${freightRate != null}" >
                                      <c:forEach items="${freightRate}" var="conOrderId" >
                                            <c:out value="${conOrderId.freightCharges}"/>
                                      </c:forEach>
                                      </c:if>
                                </td>

                        </tr>
                    </c:if>
                            <% cntr++;%>
                    <c:if test="${dedicatedVehicleTypeVendorRates != null}" >
                                <tr>
                                    <td><%=cntr%></td>
                                    <td>Dedicated Vehicle</td>
                                    <td align="right" >
                                        <br>
                                            <table border="1">
                                                <tr>
                                                    <td><b>Vendor</b></td>
                                                    <td><b>Estimate (INR)</b></td>
                                                    <td></td>
                                                    <td><b>Profit Margin (%)</b></td>
                                                </tr>
                                                <c:forEach items="${dedicatedVehicleTypeVendorRates}" var="dediRates" >
                                                 <tr>
                                                     <td><c:out value="${dediRates.vendorName}" /></td>
                                                     <td  align="right" >
                                                        <fmt:formatNumber type="number"  maxFractionDigits="2" value="${dediRates.vehicleRatePerKm * routeDistance  * noOfVehicles }" />
                                                     </td>
                                                     <td></td>
                                                     <td>
                                                         <c:if test="${freightRate != null}" >
                                                         <c:forEach items="${freightRate}" var="conOrderId" >
                                                            <fmt:formatNumber type="number"  maxFractionDigits="2" value="${((conOrderId.freightCharges-dediRates.vehicleRatePerKm * routeDistance  * noOfVehicles )/conOrderId.freightCharges)*100}" />
                                                          </c:forEach>
                                                         </c:if>
                                                     </td>
                                                  </tr>
                                                </c:forEach>
                                            </table>
                                            <br>
                                    </td>
                                </tr>
                    </c:if>
                            <% cntr++;%>
                    <c:if test="${hireVehicleTypeVendorRates != null}" >
                                <tr>
                                    <td><%=cntr%></td>
                                    <td>On Hire Vehicle</td>
                                    <td align="right" >
                                        <br>
                                            <table border="1">
                                                <tr>
                                                    <td><b>Vendor</b></td>
                                                    <td><b>Estimate (INR)</b></td>
                                                    <td></td>
                                                     <td><b>Profit Margin (%)</b></td>
                                                </tr>
                                                <c:forEach items="${hireVehicleTypeVendorRates}" var="hireRates" >
                                                    <tr>
                                                        <td><c:out value="${hireRates.vendorName}" /></td>
                                                        <td align="right" >
                                                            <fmt:formatNumber type="number"  maxFractionDigits="2" value="${hireRates.vehicleRatePerKm * routeDistance  * noOfVehicles }" />
                                                        </td>
                                                        <td></td>
                                                         <td>
                                                            <c:forEach items="${freightRate}" var="conOrderId" >
                                                               <fmt:formatNumber type="number"  maxFractionDigits="2" value="${((conOrderId.freightCharges-hireRates.vehicleRatePerKm * routeDistance  * noOfVehicles)/conOrderId.freightCharges)*100}" />
                                                            </c:forEach>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </table>
                                        <br>
                                    </td>
                                </tr>
                    </c:if>
                        </table>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>