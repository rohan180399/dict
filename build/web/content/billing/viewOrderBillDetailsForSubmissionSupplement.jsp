<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">
    var currencyCode = 'INR';
    function checkInvoiceNo() {
        var invoiceNo = document.trip.invoiceNo.value;
        if (invoiceNo == '' || invoiceNo == '0') {
            alert('please enter invoice No');
            document.trip.invoiceNo.focus();
            return false;
        } else {
            return true;
        }
    }
    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });



</script>
<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        $("#tabs").tabs();
    });

    function submitOrderBill(invoiceId) {
        $("#SubmitBill").hide();
        document.trip.action = "/throttle/submitOrderBill.do?invoiceId=" + invoiceId;
        document.trip.submit();

    }
    function numberWithCommas(x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }
</script>



<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body  onload="checkSpace();">
                <form name="trip" method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <br>
                    <%int sno = 1;%>
                    <div id="tabs" >
                        <ul>
                            <!--                    <li><a href="#tripDetail"><span>Billing Trip Details</span></a></li>-->
                            <li><a href="#invoiceDetail"><span>Billing Invoice Details</span></a></li>
                        </ul>
                        <c:set var="totalRevenue" value="0" />
                        <c:set var="totalExpToBeBilled" value="0" />


                        <%--          <div id="tripDetail">
                                      <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                          <tr>
                                              <td class="contenthead" >Sno</td>
                                              <td class="contenthead" >Customer</td>
                                              <td class="contenthead" >C Note</td>
                                              <td class="contenthead" >Trip Code</td>
                                              <td class="contenthead" >InvoiceCode</td>
                                              <td class="contenthead" >FreightAmount</td>
                                              <td class="contenthead" >OtherExp</td>
                                              <td class="contenthead" >TotalAmount</td>
                                              <td class="contenthead" >PodStatus</td>
                                          </tr>
                                          <% int submissionStatus = 0 ;%>
                                          <c:if test = "${tripDetails != null}" >
                                              <c:forEach items="${tripDetails}" var="trip">
                                                  <tr>
                                                      <td class="text1" ><%=sno%></td>
                                                      <td class="text1" ><c:out value="${trip.customerName}"/></td>
                                                      <td class="text1" ><c:out value="${trip.consignmentNoteNo}"/></td>
                                                      <td class="text1" ><c:out value="${trip.tripCode}"/></td>
                                                      <td class="text1" ><c:out value="${trip.invoiceCode}"/></td>
                                                      <td class="text1"  align="right"><c:out value="${trip.freightAmount}"/></td>
                                                      <td class="text1"  align="right"><c:out value="${trip.otherExpenseAmount}"/></td>
                                                      <td class="text1"  align="right"><c:out value="${trip.totalAmount}"/></td>
                                                      <td class="text1"  align="right">
                                                      <c:if test="${trip.podCount == 1}">
                                                          <a href="viewTripPod.do?tripSheetId=<c:out value="${trip.tripId}"/>">  <img src="images/Podactive.png" alt="Y"   title="click to upload pod"/></a>
                                                      </c:if>
                                                      <c:if test="${trip.podCount == 0}">
                                                          <a href="viewTripPod.do?tripSheetId=<c:out value="${trip.tripId}"/>">  <img src="images/Podinactive.png" alt="Y"   title="click to upload pod"/></a>
                                                      </c:if>
                                                      </td>
                                                      <c:if test="${trip.podCount > 0}">
                                                          <%submissionStatus = 1 ;%>

                                    </c:if>

                                <%sno++;%>
                            </c:forEach>
                        </c:if>
                    </table>
                    <br>
                    <center>
                        <% if(submissionStatus == 1){ %>
                        <c:if test="${submitStatus == 0}">
                        <input type="button" class="button" value="SubmitBill" id="SubmitBill" name="Next" onclick="submitOrderBill('<c:out value="${invoiceId}"/>');" />
                        </c:if>
                        <%}else{%>
                        Waiting for POD Upload, we cant submit bill.
                        <%}%>
                    </center>
                </div>  --%>
                        <div id="invoiceDetail" >
                            <div id="print" >

                                <table  align="center" cellpadding="0" cellspacing="0" border="2"  rules="all" frame="hsides"
                                        style="border-collapse: collapse;width:1000px;border-color:#000000;border-style:solid;font-size:12px;font-family:Arial;height:800px;border-left: none;border-right: none;">

                                    <tr>
                                        <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border" colspan="3" >
                                            <table align="left" >
                                                <tr>
                                                    <td align="left"><img src="images/dict-logo11.png" width="100" height="50"/></td>
                                                    <td style="padding-left: 75px; padding-right:42px;">
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<% String date3 = "01-11-2021";
                                                    String date4 = (String)request.getAttribute("billDate");
                                                   
                                                    SimpleDateFormat formatter2 = new SimpleDateFormat ("dd-MM-yyyy");
                                                    Date d3 = formatter2.parse(date3);
                                                    Date d4 = formatter2.parse(date4);
                                                if(d4.before(d3)) {
                                                %>
                                                        <font size="3"><b>International Cargo Terminals And Rail Infrastructure Pvt.Ltd.</b></font><br>
                                                <% }else{  %>
                                                
                                                <font size="3" ><b>Delhi International Cargo Terminal Pvt.Ltd.</b></font><br>

                                                <%}%>
                                                <font size="2" >(Formerly Known As INTERNATIONAL CARGO TERMINALS & RAIL INFRASTRUCTURE PVT. LTD.)</font><br>
                                           

                                                <center> <%-- <font size="2" style="padding-left: 85px" >  (Formely Known As Boxtrans Logistics (India) Services Pvt Ltd.)</font><br>  --%>
                                                    <font size="2" style="padding-left: 85px" >   Panchi Gujran,Tehsil-Ganaur, Dist-Sonepat-131101<br>
                                                    </font>
                                                    <font size="2" style="padding-left: 85px">CIN No:U63040MH2006PTC159885</font>
                                                </center>
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                                </td>
                                </tr>

                                <tr>
                                    <!--                                <td height="60" align="left" style="border-right:none;"><img src="images/Dict_logo.png" width="100" height="50"/></td>-->
                                <td  height="30" colspan="3" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse">    
                                    <c:if test="${gstType == 'Y'}">
                                        <!--<center><h3><font size="3"><b>SUPPLEMENTARY TAX INVOICE</b></font></h3></center>-->
                                    </c:if>
                                    <c:if test="${gstType != 'Y'}">
                                        <center><h3><font size="3"><b>BILL OF SUPPLY</b></font></h3></center>
                                    </c:if>
                                </td>

                                </tr>
                                <tr>
                                    <td style="width: 460px;">
                                        <table width="100%">

                                            <!--                                        <tr>
                                                                                        <td style="border-bottom-style: solid;border-right-style: solid;border-collapse: collapse;border-bottom-color:#000000;border-right-color:#000000;border-right-width: 1px;border-bottom-width: 1px;padding: 5px;">
                                                                                            <font size="2">Billed to:</font>
                                                                                        </td>
                                                                                    </tr>-->
                                            <tr>
                                                <td style="border-bottom-style: none;border-right-style: none;border-collapse: collapse;border-bottom-color:#000000;border-right-color:#000000;border-right-width: 1px;border-bottom-width: 1px;padding: 5px;">
                                                    <font size="2">Billed to:-</font> <br>

                                                    <font size="2">  <b><c:out value="${billingParty}"/></b></font><br>
                                                    <font size="2">  <c:out value="${customerAddress}"/></font><br>
                                                    <font size="2"> Billing State: <c:out value="${billingState}"/></font><br>
                                                    <font size="2"> PAN No: <c:out value="${panNo}"/></font><br>
                                                    <font size="2"> GST No: <c:out value="${gstNo}"/></font>
                                                </td>
                                            </tr>

                                        </table>
                                    </td>
                                    <td style="width: 260px;">
                                        <table width="100%" >


                                            <tr>
                                                <td style="border-bottom-style: solid;border-right-style: none;border-collapse: collapse;border-bottom-color:#000000;border-right-color:#000000;border-right-width: 1px;border-bottom-width: 1px;padding: 5px;text-align:center;padding-bottom:6px;">
                                                    <font size="2"><B>Invoice No:<c:out value="${invoicecode}"/> 
                                                         <c:if test="${workOrderNumber != null}">
                                                        <br> Work Order No:<c:out value="${workOrderNumber}"/>
                                                         </c:if>
                                                    </B></font></td>

                                            </tr>
                                           <tr>
                                            <td style="border-bottom-style: solid;border-right-style: 
                                                none;border-collapse: collapse;border-bottom-color:#000000;
                                                border-right-color:#000000;border-right-width: 1px;border-bottom-width: 
                                                1px;padding: 5px;text-align:left;">
                                            <font size="2" style="padding-left: 15px;"><b><c:out value="${billingType}"/></b></font>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border-bottom-style: none;border-right-style: 
                                                none;border-collapse: collapse;border-bottom-color:#000000;
                                                border-right-color:#000000;border-right-width: 1px;border-bottom-width: 
                                                1px;padding: 5px;text-align:left;">
                                            <font size="2" style="padding-left: 15px;"><b>SAC Code: 996511</b></font>
                                            </td>
                                        </tr>



                                        </table>
                                    </td>
                                    <td style="width: 260px;">
                                        <table width="100%" >


                                            <tr>
                                                <td style="border-bottom-style: solid;border-right-style: none;border-collapse: collapse;border-bottom-color:#000000;border-right-color:#000000;border-right-width: 1px;border-bottom-width: 1px;padding: 6px;text-align:left;">
                                                    <b> <font size="2">Bill Date:<c:out value="${billDate}"/></font> </b></td>
                                            </tr>
                                            <tr>
                                                <td style="border-bottom-style: none;border-right-style: none;border-collapse: collapse;border-bottom-color:#000000;border-right-color:#000000;border-right-width: 1px;border-bottom-width: 1px;padding: 5px;text-align:left;">
                                                    <b>  <font size="2">Dispatched Through:</font></b>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="30"  colspan="3"><font size="2"><center><b>PARTICULARS</b></center></font></td>
                                <!--<td height="30" style="border-bottom-style: none;border-right-color:#000000;border-right-width: 1px;padding: 0px;" colspan="3"><font size="2"><center><b>PARTICULARS</b></center></font></td>-->
                                </tr>
                                <c:if test="${invoiceDetailsList != null}">
                                    <tr  >
                                        <td height="80" colspan="3" >
                                            <table border=1 style='' >
                                                <tr>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 48px;">GR.No</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 65px;">Date</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 275px;">Description</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 89px;">No.Of Container</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 67px;">Commodity</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 67px;">Commodity Category</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 55px;">Freight</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 59px;">Toll Tax</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 92px;">Detention Charges</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 70px;">Weightment</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 79px;">Other Charges</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 98px;">Total Amount(Rs.)</th>


                                                </tr>
                                                <% int index = 0;%>
                                                <c:set var="totalFreightCharge" value="0.00"/>
                                                <c:set var="totalFreightChargeOfRow" value="0.00"/>
                                                <c:set var="totalContainer" value="0"/>


                                                <c:forEach begin="0" end="16" items="${invoiceDetailsList}" var="invoiceDetailsList">

                                                    <tr>
                                                        <!--<font size="2">-->

                                                        <td  height="30" style="font-size: 12px;width: 48px;"><c:out value="${invoiceDetailsList.grNumber}"/></td>
                                                        <td  height="40" style="font-size: 12px;width: 65px;"> <c:out value="${invoiceDetailsList.grDate}"/></td>
                                                        <td  height="30" style="font-size: 12px;width: 275px;"> <c:out value="${invoiceDetailsList.destination}"/></td>
                                                        <td  height="30" style="font-size: 12px;width: 89px;text-align: center;"><c:out value="${invoiceDetailsList.containerQty}"/>
                                                            <c:set var="totalContainer" value='${invoiceDetailsList.containerQty+totalContainer}'/>
                                                        </td>
                                                        <td  height="30" style="font-size: 12px;width: 67px;text-align: center;"><c:out value="${invoiceDetailsList.articleName}"/></td>
                                                        <td  height="30" style="font-size: 12px;width: 67px;text-align: center;"><c:out value="${invoiceDetailsList.commodityCategory}"/></td>
                                                        <td  height="30" style="font-size: 12px;text-align:right;width: 55px;"><span id="initialFreightAmountSpan<%=index%>"></span>
                                                            <script>
                                                                var freightAmount = '<c:out value="${invoiceDetailsList.freightAmount}"/>';
                                                                var formattedAmount = parseFloat(freightAmount).toFixed(2);
                                                                $("#initialFreightAmountSpan<%=index%>").text(numberWithCommas(formattedAmount));
//
                                                            </script>
                                                        </td>

                                                        <td  height="30" style="font-size: 12px;text-align:right;width: 59px;"><span id="otherExpenseSpan<%=index%>"></span>
                                                            <script>
                                                                var freightAmount = '<c:out value="${invoiceDetailsList.tollTax}"/>';
                                                                if (freightAmount != '') {
                                                                    var formattedAmount = parseFloat(freightAmount).toFixed(2);
                                                                    $("#otherExpenseSpan<%=index%>").text(numberWithCommas(formattedAmount));
                                                                } </script></font>
                                                        </td>
                                                        <td  height="30" style="font-size: 12px;text-align:right;width: 92px;"><span id="detaintionSpan<%=index%>"></span>
                                                            <script>
                                                                var freightAmount = '<c:out value="${invoiceDetailsList.detaintion}"/>';
                                                                if (freightAmount != '') {
                                                                    var formattedAmount = parseFloat(freightAmount).toFixed(2);
                                                                    $("#detaintionSpan<%=index%>").text(numberWithCommas(formattedAmount));
                                                                }
                                                            </script>
                                                        </td>
                                                        <td style="font-size: 12px;text-align:right;width: 70px;"><c:out value="${invoiceDetailsList.weightmentExpense}"/></td>
                                                        <td style="font-size: 12px;text-align:right;width: 79px;"><c:out value="${invoiceDetailsList.otherExpense}"/></td>
                                                        <td  height="30" style="font-size: 12px;text-align:right;width: 98px;"> <span id="prefreightAmountSpan<%=index%>"></span>
                                                            <c:set var="totalFreightChargeOfRow" value='${invoiceDetailsList.freightAmount+invoiceDetailsList.tollTax+invoiceDetailsList.otherExpense+invoiceDetailsList.weightmentExpense+invoiceDetailsList.detaintion+totalFreightChargeOfRow}'/></font>
                                                            <c:set var="totalFreightCharge" value='${invoiceDetailsList.freightAmount+invoiceDetailsList.otherExpense+invoiceDetailsList.tollTax+invoiceDetailsList.detaintion+totalFreightCharge+invoiceDetailsList.weightmentExpense}'/></font>
                                                              <c:set var="totalFreightCharge" value='${totalFreightCharge+taxValue}'/>
                                                            <script>
                                                                var freightAmount = '<c:out value="${totalFreightChargeOfRow}"/>';
                                                                var formattedAmount = parseFloat(freightAmount).toFixed(2);
                                                                $("#prefreightAmountSpan<%=index%>").text(numberWithCommas(formattedAmount));

//
                                                            </script>
                                                        </td>

                                                    </tr>
                                                    <c:set var="totalFreightChargeOfRow" value="0"/>
                                                    <%index++;%>
                                                </c:forEach>
                                                <input type="hidden" name="count1" id="count1" value="<%=index%>"/>

                                            </table>
                                        </td>

                                    </tr>


                                    <tr style="border-left: 0px;border-right: 0px;display: none;border-bottom: none;border-top:  none;" id="breakOne">
                                        <td height="150" style="width: 300px;border-left: 0px;border-right: 0px;" colspan="3">
                                            <table align="center"  width="100%" height="20%" style="font-size: 10px;border:none;" id="spaceTable">

                                                <tr style="border:none">
                                                    <td style="border:none">
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>


                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>


                                    <tr id="tableTwo">
                                        <td height="150" colspan="3">
                                            <table border=1 >
                                                <tr id="headerOne">
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 48px;">GR.No</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 65px;">Date</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 275px;">Description</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 89px;">No.Of Container</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 67px;">Commodity</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 67px;">Commodity Category</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 55px;">Freight</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 59px;">Toll Tax</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 92px;">Detention Charges</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 70px;">Weightment</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 79px;">Other Charges</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 98px;">Total Amount(Rs.)</th>

                                                </tr>

                                                <% int index1 = 20;%>
                                                <c:forEach begin="17" end="39" items="${invoiceDetailsList}" var="invoiceDetailsList">
                                                    <%--<c:forEach items="${invoiceDetailsList}" var="invoiceDetailsList" >--%>
                                                    <tr>
                                                        <!--<font size="2">-->

                                                        <td  height="30" style="font-size: 12px;width: 48px;"><c:out value="${invoiceDetailsList.grNumber}"/></td>
                                                        <td  height="30" style="font-size: 12px;width: 65px;"> <c:out value="${invoiceDetailsList.grDate}"/></td>
                                                        <td  height="30" style="font-size: 12px;width: 275px;"> <c:out value="${invoiceDetailsList.destination}"/></td>
                                                        <td  height="30" style="font-size: 12px;width: 89px;text-align: center;"><c:out value="${invoiceDetailsList.containerQty}"/>
                                                            <c:set var="totalContainer" value='${invoiceDetailsList.containerQty+totalContainer}'/>
                                                        </td>
                                                        <td  height="30" style="font-size: 12px;width: 67px;text-align: center;"><c:out value="${invoiceDetailsList.articleName}"/></td>
                                                        <td  height="30" style="font-size: 12px;width: 67px;text-align: center;"><c:out value="${invoiceDetailsList.commodityCategory}"/></td>
                                                        <td  height="30" style="font-size: 12px;text-align:right;width: 55px;"><span id="initialFreightAmountSpan<%=index1%>"></span>
                                                            <script>
                                                                var freightAmount = '<c:out value="${invoiceDetailsList.freightAmount}"/>';
                                                                var formattedAmount = parseFloat(freightAmount).toFixed(2);
                                                                $("#initialFreightAmountSpan<%=index1%>").text(numberWithCommas(formattedAmount));
//
                                                            </script>
                                                        </td>

                                                        <td  height="30" style="font-size: 12px;text-align:right;width: 59px;"><span id="otherExpenseSpan<%=index1%>"></span>
                                                            <script>
                                                                var freightAmount = '<c:out value="${invoiceDetailsList.tollTax}"/>';
                                                                if (freightAmount != '') {
                                                                    var formattedAmount = parseFloat(freightAmount).toFixed(2);
                                                                    $("#otherExpenseSpan<%=index1%>").text(numberWithCommas(formattedAmount));
                                                                }
//
                                                            </script></font>
                                                        </td>
                                                        <td  height="30" style="font-size: 12px;text-align:right;width: 92px;"><span id="detaintionSpan<%=index1%>"></span>
                                                            <script>
                                                                var freightAmount = '<c:out value="${invoiceDetailsList.detaintion}"/>';
                                                                if (freightAmount != '') {
                                                                    var formattedAmount = parseFloat(freightAmount).toFixed(2);
                                                                    $("#detaintionSpan<%=index1%>").text(numberWithCommas(formattedAmount));
                                                                }
//
                                                            </script>
                                                        </td>
                                                        <td style="font-size: 12px;text-align:right;width: 70px;"><c:out value="${invoiceDetailsList.weightmentExpense}"/></td>
                                                        <td style="font-size: 12px;text-align:right;width: 79px;"><c:out value="${invoiceDetailsList.otherExpense}"/></td>
                                                        <td  height="30" style="font-size: 12px;text-align:right;width: 98px;"> <span id="prefreightAmountSpan<%=index1%>"></span>
                                                            <c:set var="totalFreightChargeOfRow" value='${invoiceDetailsList.freightAmount+invoiceDetailsList.tollTax+invoiceDetailsList.otherExpense+invoiceDetailsList.weightmentExpense+invoiceDetailsList.detaintion+totalFreightChargeOfRow}'/></font>
                                                            <c:set var="totalFreightCharge" value='${invoiceDetailsList.freightAmount+invoiceDetailsList.tollTax+invoiceDetailsList.otherExpense+invoiceDetailsList.weightmentExpense+invoiceDetailsList.detaintion+totalFreightCharge}'/></font>
                                                            <script>
                                                                var freightAmount = '<c:out value="${totalFreightChargeOfRow}"/>';
                                                                var formattedAmount = parseFloat(freightAmount).toFixed(2);
                                                                $("#prefreightAmountSpan<%=index1%>").text(numberWithCommas(formattedAmount));

//
                                                            </script>
                                                        </td>

                                                    </tr>

                                                    <c:set var="totalFreightChargeOfRow" value="0"/>
                                                    <%index1++;%>
                                                </c:forEach>
                                                <!--<input type="text" name="count2" id="count2" value="<%=index1%>"/>-->

                                            </table>
                                        </td>
                                    </tr>
                                    <!--                            <tr style="border-left: 0px;border-right: 0px;display: none;" id="breakTwo">
                                                                    <td height="150" style="width: 300px;border-left: 0px;border-right: 0px;" colspan="3">
                                                                   <table align="center"  width="100%" height="20%" style="font-size: 10px;border:none;" id="spaceTable">
                                                            <tr style="border:none">
                                                                <td style="border:none">
                                    <%--<c:if test="${index1 <= 34}">--%>
                                  <br>
                                  <br>
                                    <%--</c:if>--%>
                              </td>
                          </tr>
                      </table>
                                      </td>
                          </tr>-->
                                    <tr style="border-left: 0px;border-right: 0px;display: none;border-bottom: none;border-top:  none;" id="breakTwo">
                                        <td height="150" style="width: 300px;border-left: 0px;border-right: 0px;" colspan="3">
                                            <table border=1  align="center"  width="100%" height="20%" style="font-size: 10px;border:none;" id="spaceTable">

                                                <tr style="border:none">
                                                    <td style="border:none">
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>

                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="tableThree">
                                        <td height="150" colspan="3">
                                            <table border=1 >
                                                <tr id="headerTwo">
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 48px;">GR.No</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 65px;">Date</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 275px;">Description</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 89px;">No.Of Container</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 67px;">Commodity</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 67px;">Commodity Category</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 55px;">Freight</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 59px;">Toll Tax</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 92px;">Detention Charges</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 70px;">Weightment</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 79px;">Other Charges</th>
                                                    <th height="30" style="font-size: 12px;text-align: center;width: 98px;">Total Amount(Rs.)</th>

                                                </tr>
                                                <% int index2 = 40;%>
                                                <c:forEach begin="40" end="59" items="${invoiceDetailsList}" var="invoiceDetailsList">
                                                    <%--<c:forEach items="${invoiceDetailsList}" var="invoiceDetailsList" >--%>
                                                    <tr>
                                                        <!--<font size="2">-->

                                                        <td  height="30" style="font-size: 12px;width: 48px;"><c:out value="${invoiceDetailsList.grNumber}"/></td>
                                                        <td  height="40" style="font-size: 12px;width: 65px;"> <c:out value="${invoiceDetailsList.grDate}"/></td>
                                                        <td  height="30" style="font-size: 12px;width: 275px;"> <c:out value="${invoiceDetailsList.destination}"/></td>
                                                        <td  height="30" style="font-size: 12px;width: 89px;text-align: center;"><c:out value="${invoiceDetailsList.containerQty}"/>
                                                            <c:set var="totalContainer" value='${invoiceDetailsList.containerQty+totalContainer}'/>
                                                        </td>
                                                        <td  height="30" style="font-size: 12px;width: 67px;text-align: center;"><c:out value="${invoiceDetailsList.articleName}"/></td>
                                                        <td  height="30" style="font-size: 12px;width: 67px;text-align: center;"><c:out value="${invoiceDetailsList.commodityCategory}"/></td>
                                                        <td  height="30" style="font-size: 12px;text-align:right;width: 55px;"><span id="initialFreightAmountSpan<%=index2%>"></span>
                                                            <script>
                                                                var freightAmount = '<c:out value="${invoiceDetailsList.freightAmount}"/>';
                                                                var formattedAmount = parseFloat(freightAmount).toFixed(2);
                                                                $("#initialFreightAmountSpan<%=index2%>").text(numberWithCommas(formattedAmount));
//
                                                            </script>
                                                        </td>

                                                        <td  height="30" style="font-size: 12px;text-align:right;width: 59px;"><span id="otherExpenseSpan<%=index2%>"></span>
                                                            <script>
                                                                var freightAmount = '<c:out value="${invoiceDetailsList.tollTax}"/>';
                                                                if (freightAmount != '') {
                                                                    var formattedAmount = parseFloat(freightAmount).toFixed(2);
                                                                    $("#otherExpenseSpan<%=index2%>").text(numberWithCommas(formattedAmount));
                                                                }
//
                                                            </script></font>
                                                        </td>
                                                        <td  height="30" style="font-size: 12px;text-align:right;width: 92px;"><span id="detaintionSpan<%=index2%>"></span>
                                                            <script>
                                                                var freightAmount = '<c:out value="${invoiceDetailsList.detaintion}"/>';
                                                                if (freightAmount != '') {
                                                                    var formattedAmount = parseFloat(freightAmount).toFixed(2);
                                                                    $("#detaintionSpan<%=index2%>").text(numberWithCommas(formattedAmount));
                                                                }
//
                                                            </script>
                                                        </td>
                                                        <td style="font-size: 12px;text-align:right;width: 70px;"><c:out value="${invoiceDetailsList.weightmentExpense}"/></td>
                                                        <td style="font-size: 12px;text-align:right;width: 79px;"><c:out value="${invoiceDetailsList.otherExpense}"/></td>
                                                        <td  height="30" style="font-size: 12px;text-align:right;width: 98px;"> <span id="prefreightAmountSpan<%=index2%>"></span>
                                                            <c:set var="totalFreightChargeOfRow" value='${invoiceDetailsList.freightAmount+invoiceDetailsList.tollTax+invoiceDetailsList.otherExpense+invoiceDetailsList.weightmentExpense+invoiceDetailsList.detaintion+totalFreightChargeOfRow}'/></font>
                                                            <c:set var="totalFreightCharge" value='${invoiceDetailsList.freightAmount+invoiceDetailsList.tollTax+invoiceDetailsList.otherExpense+invoiceDetailsList.detaintion+invoiceDetailsList.weightmentExpense+totalFreightCharge}'/></font>
                                                            <script>
                                                                var freightAmount = '<c:out value="${totalFreightChargeOfRow}"/>';
                                                                var formattedAmount = parseFloat(freightAmount).toFixed(2);
                                                                $("#prefreightAmountSpan<%=index2%>").text(numberWithCommas(formattedAmount));

//
                                                            </script>
                                                        </td>

                                                    </tr>
                                                    <c:set var="totalFreightChargeOfRow" value="0"/>
                                                    <%index2++;%>
                                                </c:forEach>
                                                <!--<input type="text" name="count2" id="count2" value="<%=index2%>"/>-->

                                            </table>
                                        </td>
                                    </tr>


                                    <tr id="tableTotal">
                                        <td height="30" colspan="3">
                                            <table border=1 >
                                                <c:set var="taxableAmount" value='${(totalFreightCharge*30)/100}'/>
                                                <c:set var="serviceTaxAmount" value='${(taxableAmount*14)/100}'/>
                                                <c:set var="abatement" value='${totalFreightCharge - taxableAmount}'/>
                                                <c:set var="swachhaBharat" value='${(taxableAmount*0.5)/100}'/>
                                                <c:set var="test12" value="12000"/>

                                                <c:if test = "${firstRemarks != '' && firstRemarks != null}" >
                                                <tr id="firstRemarks">

                                                    <td  height="30" style="width: 48px; " ></td>
                                                    <td  height="30" style="width: 65px;">
                                                        <!--                                                 <span id="freightAmountSpan"></span>-->

                                                    </td>
                                                    <td  height="30" style="font-size: 12px;width: 275px;">

                                                        <c:out value="${firstRemarks}"/>
                                                    </td>
                                                    <td  height="30" style="width:89px;"></td>
                                                    <td  height="30" style="width:67px;"></td>
                                                    <td  height="30" style="width:55px;"></td>
                                                    <td  height="30" style="width:59px;"></td>
                                                    <td  height="30" style="width:92px;"></td>
                                                    <td  height="30" style="width:70px;"></td>
                                                    <td  height="30" style="width:79px;"></td>
                                                    <td  height="30" style="width:98px;">
                                                    </td>
                                                </tr></c:if>
                                                            <c:if test = "${secondRemarks != '' && secondRemarks != null}" >
                                                <tr id="secondRemarks">

                                                    <td  height="30" style="width: 50px; " ></td>
                                                    <td  height="30" style="width: 65px;">
                                                        <!--                                                 <span id="freightAmountSpan"></span>-->

                                                    </td>
                                                    <td  height="30" style="font-size: 12px;width: 275px;">
                                                        <c:out value="${secondRemarks}"/>

                                                    </td>
                                                    <td  height="30" style="width:89px;"></td>
                                                    <td  height="30" style="width:67px;"></td>
                                                    <td  height="30" style="width:55px;"></td>
                                                    <td  height="30" style="width:59px;"></td>
                                                    <td  height="30" style="width:92px;"></td>
                                                    <td  height="30" style="width:70px;"></td>
                                                    <td  height="30" style="width:79px;"></td>
                                                    <td  height="30" style="width:98px;">
                                                    </td>
                                                </tr> </c:if>
                                                <c:if test = "${invoiceTaxDetails != null}" >

                                    <c:forEach items="${invoiceTaxDetails}" var="invoiceTax">
                                            <td  height="30" style="width: 48px;"></td>
                                            <td  height="30" style="text-align:right;width: 65px;">
                                       </td>
                                            <td  height="30" style="width:275px;">


                                            </td>
                                            <td  height="30" style="width:89px;text-align: center;"></td>
                                            <td  height="30" style="width:67px;">&nbsp;</td>
                                            <td  height="30" style="width:55px;">&nbsp;</td>
                                            <td  height="30" style="width:59px;">&nbsp;</td>
                                            <td  height="30" style="width:92px;">&nbsp;</td>
                                            <td  height="30" align="right" style="width:70px;"><c:out value="${invoiceTax.gstName}"/></td>
                                            <td  height="30"  align="right" style="width:70px;"><c:out value="${invoiceTax.gstPercentage}"/>%</td>
                                            <td  height="30" align="right" style="width:70px;"><c:out value="${invoiceTax.totalTax}"/></td>
                                        </tr>
 </c:forEach>
                                        </c:if>
                                                <tr>

                                                    <td  height="30" style="width: 48px;"></td>
                                                    <td  height="30" style="text-align:right;width: 65px;">
                                                        <!--                                                 <span id="freightAmountSpan"></span>-->
                                                        <script>
                                                            var freightAmount = '<c:out value="${totalFreightCharge}"/>';
                                                            var formattedAmount = parseFloat(freightAmount).toFixed(2);
                                                            $("#freightAmountSpan").text(numberWithCommas(formattedAmount));

//                                            $("#freightAmountSpan").text(parseFloat(9000.0).toFixed(2));
                                                        </script></td>
                                                    <td  height="30" style="width:275px;">


                                                    </td>
                                                    <td  height="30" style="width:89px;text-align: center;"><c:out value="${totalContainer}"/></td>
                                                    <td  height="30" style="width:67px;">&nbsp;</td>
                                                    <td  height="30" style="width:55px;">&nbsp;</td>
                                                    <td  height="30" style="width:59px;">&nbsp;</td>
                                                    <td  height="30" style="width:92px;">&nbsp;</td>
                                                    <td  height="30" style="width:70px;"></td>
                                                    <td  height="30" style="width:79px;">&nbsp;</td>
                                                   <td  height="30" style="text-align:right;width:98px;"><span id="totFreightAmountSpan"></span>
                                                        <script>
                                                            var freightAmount = '<c:out value="${totalFreightCharge}"/>';
                                                            var formattedAmount = parseFloat(freightAmount).toFixed(2);
                                                            $("#totFreightAmountSpan").text(numberWithCommas(formattedAmount));
//
                                                        </script>
                                                    </td>
                                                </tr>
                                            </table>

                                        </td>
                                    </tr>
                                </c:if>
                                <tr style="border-left: 0px;border-right: 0px;display: none;" id="remarksBreak">
                                    <td height="10" style="width: 300px;border-left: 0px;border-right: 0px;" colspan="3">
                                        <table border=0 rules=ALL FRAME=VOID align="center"  width="100%" height="20%" style="font-size: 10px;border:none;" id="spaceTable">
                                            <!--                            <td colspan="1"  id="displayrow11" align="right" style="margin-right: 190px;border:none;" border="0"><b>Page 01 of 02</b></td>
                                                                        <td colspan="1"  id="displayrow12" align="right" style="margin-right: 190px;border:none;" border="0"><b>Page 01 of 03</b></td>-->
                                            <!--                        <tr style="border:none">
                                                                        <td style="border:none">
                                                                            <br>
                                                                        <br><br>
                                                                        <br><br>
                                                                        <br><br>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                            
                                            
                                            
                                                                        </td> -->
                                            <!--</tr>-->
                                        </table>
                                    </td>
                                </tr>


                                <!--                        <tr id="teableThree">
                                                               <td height="" colspan="">
                                                                    <table  border="1" >
                                
                                
                                <% int index = 3;%>
        
        
                                <c:forEach begin="40" end="59" items="${invoiceDetailsList}" var="invoiceDetailsList">

                                <tr>
                                <font size="2">

                                 <td  height="30" style="font-size: 12px;width: 48px;"><c:out value="${invoiceDetailsList.grNumber}"/></td>
                                 <td  height="40" style="font-size: 12px;width: 65px;"> <c:out value="${invoiceDetailsList.grDate}"/></td>
                                 <td  height="30" style="font-size: 12px;width: 275px;"> <c:out value="${invoiceDetailsList.destination}"/></td>
                                 <td  height="30" style="font-size: 12px;width: 89px;text-align: center;"><c:out value="${invoiceDetailsList.containerQty}"/>
                                    <c:set var="totalContainer" value='${invoiceDetailsList.containerQty+totalContainer}'/>
                                    </td>
                                    <td  height="30" style="font-size: 12px;width: 67px;text-align: center;"><c:out value="${invoiceDetailsList.articleName}"/></td>
                                    <td  height="30" style="font-size: 12px;text-align:right;width: 55px;"><span id="initialFreightAmountSpan<%=index%>"></span>
                                     <script>
                                    var freightAmount = '<c:out value="${invoiceDetailsList.freightAmount}"/>';
                                      var formattedAmount=parseFloat(freightAmount).toFixed(2);
                                    $("#initialFreightAmountSpan<%=index%>").text(numberWithCommas(formattedAmount));
//
                                </script>
                                    </td>

                                    <td  height="30" style="font-size: 12px;text-align:right;width: 59px;"><span id="otherExpenseSpan<%=index%>"></span>
                                      <script>
                                    var freightAmount = '<c:out value="${invoiceDetailsList.otherExpense}"/>';
                                      if(freightAmount != ''){
                                      var formattedAmount=parseFloat(freightAmount).toFixed(2);
                                    $("#otherExpenseSpan<%=index%>").text(numberWithCommas(formattedAmount));
                                     }
//
                                </script></font>
                                    </td>
                                    <td  height="30" style="font-size: 12px;text-align:right;width: 92px;"><span id="detaintionSpan<%=index%>"></span>
                                     <script>
                                    var freightAmount = '<c:out value="${invoiceDetailsList.detaintion}"/>';
                                       if(freightAmount != ''){
                                    var formattedAmount=parseFloat(freightAmount).toFixed(2);
                                  $("#detaintionSpan<%=index%>").text(numberWithCommas(formattedAmount));
                                       }
//
                                </script>
                                    </td>
                                    <td style="font-size: 12px;text-align:right;width: 70px;"></td>
                                    <td style="font-size: 12px;text-align:right;width: 79px;"></td>
                                    <td  height="30" style="font-size: 12px;text-align:right;width: 98px;"> <span id="prefreightAmountSpan<%=index%>"></span>
                                    <c:set var="totalFreightChargeOfRow" value='${invoiceDetailsList.freightAmount+invoiceDetailsList.tollTax+invoiceDetailsList.otherExpense+invoiceDetailsList.detaintion+totalFreightChargeOfRow}'/></font>
                                    <c:set var="totalFreightCharge" value='${invoiceDetailsList.freightAmount+invoiceDetailsList.tollTax+invoiceDetailsList.otherExpense+invoiceDetailsList.detaintion+totalFreightCharge}'/></font>
                              <script>
                                var freightAmount = '<c:out value="${totalFreightChargeOfRow}"/>';
                                  var formattedAmount=parseFloat(freightAmount).toFixed(2);
                                $("#prefreightAmountSpan<%=index%>").text(numberWithCommas(formattedAmount));

//
                            </script>
                                </td>

                            </tr>
                                    <c:set var="totalFreightChargeOfRow" value="0"/>
                                    <%index++;%>
                                </c:forEach>
                                 <input type="hidden" name="count1" id="count1" value="<%=index%>"/>

                                </table>
                        </td>

                    </tr>
                    <tr style="border-left: 0px;border-right: 0px;display: none;" id="breakTwo">
                        <td height="150" style="width: 300px;border-left: 0px;border-right: 0px;" colspan="3">
                       <table align="center"  width="100%" height="20%" style="font-size: 10px;border:none;" id="spaceTable">
                <tr>
                    <td colspan="1"  id="displayrow11" align="right" style="margin-right: 190px;border:none;" border="0"><b>Page 01 of 02</b></td>
                    <td colspan="1"  id="displayrow12" align="right" style="margin-right: 190px;border:none;" border="0"><b>Page 01 of 03</b></td>
                </tr>
                <tr style="border:none">
                    <td style="border:none">
                        <br>
                     <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>

                    </td>
                </tr>
            </table>
                            </td>
                </tr>-->



                                <tr>
                                    <td colspan="3" height="30">
                                        <!--<div colspan="3" style="background-image: url(images/Cancelled.jpg);  height: 200px; width: 800px; border: 1px solid black;"></div>-->
                                        <!--<div style="background-image: url(../images/test-background.gif); height: 200px; width: 400px; border: 1px solid black;"> </div>-->
                                        <font size="2"> Amount Chargeable(In words)</font><br>
                                        <font size="3"> <b>Rs.
                                            <jsp:useBean id="spareTotalRound"   class="ets.domain.report.business.NumberWordsIndianRupees" >
                                                <% spareTotalRound.setRoundedValue(String.valueOf(pageContext.getAttribute("totalFreightCharge")));%>
                                                <% spareTotalRound.setNumberInWords(spareTotalRound.getRoundedValue());%>
                                                <b><jsp:getProperty name="spareTotalRound" property="numberInWords" />&nbsp; Only</b>
                                            </jsp:useBean>
                                        </b></font>
                                        <br>
                                        <br>
                                        <br>
                                         <% int i=1;%>
                                    <font size="3"><b>Remarks</b></font><br>
                                    <font size="2">Being Road Transportation Charges Of <c:out value="${containerTypeName}"/> <c:out value="${movementType}"/> Containers<br></font> <br>
                                    <font size="2">Declaration<br></font>
                                    <font size="3"><%=i%>)All diputes are subject to Delhi Jurisdition</font><br>
                                    <% i++; %>
                                    <font size="3"><%=i%>)Input credit on inputs, capital goods and input services, used for providing the taxable service, has not been taken.</font><br>
                                    <% i++; %>
                                   <% if((Integer)request.getAttribute("invoiceTaxDetailsSize") > 0){%>
                                    <font size="3"><%=i%>)Goods and Service tax(GST), in respect of services provided by a goods transport agency in respect of transportation of goods by road, will be paid by service receiver.</font><br>
                                    <% i++; %>
                                    <%}%>
                                    <font size="3"><%=i%>)PAN No.AACCB8054G</font><br>
                                    <% i++; %>
                                    <font size="3"><%=i%>)GST Ref No.06AACCB8054G1ZT</font><br>
                                    <% if((Integer)request.getAttribute("invoiceTaxDetailsSize") == 0){%>
                                    <% i++; %>
                                    <c:if test="${gstType == 'Y'}">
                                        <font size="3"><%=i%>)Tax is payable under RCM.</font><br>
                                    </c:if>
                                     <c:if test="${gstType != 'Y'}">
                                        <font size="3"><%=i%>)RCM not applicable.</font><br>
                                    </c:if>
                                    <%}%><%i++;%>
 
                                    <font size="3"><%=i%>)We declare that GST input credit on inputs, capital goods and input services used for providing the subject taxable service has not been taken by us under the provisions of the Input tax credit rules of GST. (Notification No.11/2017-Central Tax-rate dt. 28th June 2017 and Notification No.11/2017-State Tax -rate dt. 29th June 2017 ).
                                    </font><br/><%i++;%>
<font size="3"><%=i%>)In case of any discrepancy in this invoice, kindly intimate within 7 days from receipt or else it will be deemed to be accepted</font>
                                    <br/>
                                    <br/>
                                    
                                    
                                    
<!--                                        <table style="float: right;">
                                            <tr>
                                                <td align="right" colspan="3">
                                                    <font size="3" ><b>For International Cargo Terminals And Rail Infrastructure Pvt.Ltd.</b></font><br>
                                                    <font size="3" >(Formerly Known As Boxtrans Logistics (India) Services Pvt Ltd)</font><br><br><br><br><br>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"></td>
                                                <td></td>
                                                <td align="right">
                                                    <font size="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Authorised Signature </font><br>
                                                </td>
                                            </tr>
                                        </table>
                                        <br>
                                        <table style="float: left;">
                                            <td colspan="2"></td>
                                            <td style="padding-top: 95px;"> <font size="1"> Ref code:<c:out value="${userName}"/></font> </td>
                                        </table>-->
                                       <table style="float: left;">
                                            <tr align="left" >
                                                <td colspan="3" > <font size="2" ><b>BENEFICIERY DETAILS</b></font><br>
                                           </td>
                                            </tr>
                                            <% String invDt="", invDate="";
                                            Date dates = new Date();
                                            java.sql.Date invBef = null;
                                            java.sql.Date invAft = null; %>
                                          <c:set var="invoiceDate" value="${billDate}"/>
                                           <% String curDate   = "2020-03-05";   // Bank last date
                                              invDt = (String) pageContext.getAttribute("invoiceDate");
                                              System.out.println("pre---"+invDt);
                                              dates=new SimpleDateFormat("dd-MM-yyyy").parse(invDt);
                                              SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                              invDate = sdf.format(dates);
                                              invBef = java.sql.Date.valueOf(invDate);
                                              invAft = java.sql.Date.valueOf(curDate);
                                              System.out.println("date1 : " + invBef);
                                              System.out.println("date2 : " + invAft);
                                               if(invBef.before(invAft) || invBef.equals(invAft)){
                                                     System.out.println("before pre: " );
                                               }else if(invBef.after(invAft)){
                                                   System.out.println("after pre: " );
                                               }
                                            %>

                                              <%if(invBef.before(invAft) || invBef.equals(invAft)){%>
                                                   <tr>
                                                <td align="left"></td>
                                                <td >
                                                    <font size="2"><b>IN FAVOUR&nbsp;:-</b>International Cargo Terminals & Rail Infrastructure Pvt. Ltd.</font><br>
                                                <font size="2"><b>BANKER &nbsp;:-</b>&nbsp;YES BANK LTD</font><br>
                                                <font size="2"><b>BRANCH  &nbsp;:-</b>&nbsp;Ground Floor, Sheela Shoppee Sanjay Chowk , Panipat - 132103</font><br>
                                                <font size="2"><b>A/C NO   &nbsp;:-</b>&nbsp;&emsp;007081400000019</font><br>
                                                <font size="2"><b>IFSC CODE   &nbsp; :-</b>&nbsp;YESB0000070</font>
                                                <br><br>
                                                <c:if test="${creditNoteNo != null}">
                                              <font size="1"> Debit Note No:<c:out value="${creditNoteNo}"/></font> &nbsp;
                                              <font size="1"> Debit Note Date:<c:out value="${creditNoteDate}"/></font> &nbsp;
                                              </c:if>
                                              <font size="1"> Ref code:<c:out value="${userName}"/></font> </td>
                                                
<!--                                                <td align="right">
                                                    <font size="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Authorised Signature </font><br>
                                                </td>-->
                                            </tr>
                                               <%}else if(invBef.after(invAft)){%>
                                                <tr>
                                                <td align="left"></td>
                                                <td >
                                              
<% String date5 = "01-11-2021";
                                                    String date6 = (String)request.getAttribute("billDate");
                                                   
                                                    SimpleDateFormat formatter3 = new SimpleDateFormat ("dd-MM-yyyy");
                                                    Date d5 = formatter3.parse(date5);
                                                    Date d6 = formatter3.parse(date6);
                                                if(d6.before(d5)) {
                                                %>
                                                
                                                    <font size="2"><b>IN FAVOUR&nbsp;:-</b>International Cargo Terminals & Rail Infrastructure Pvt. Ltd.</font><br>
                                                <font size="2"><b>BANKER &nbsp;:-</b>&nbsp;PUNJAB NATIONAL BANK</font><br>
                                                <font size="2"><b>BRANCH  &nbsp;:-</b>&nbsp;PANCHI GUJRAN, SONEPAT, HARYANA – 131101</font><br>
                                                <font size="2"><b>A/C NO   &nbsp;:-</b>&nbsp;&emsp;7818002100000081</font><br>
                                                <font size="2"><b>IFSC CODE   &nbsp; :-</b>&nbsp;PUNB0781800</font>
                                                <% }else{  %>
                                                

                                                    <font size="2"><b>IN FAVOUR&nbsp;:-</b>  DELHI INTERNATIONAL CARGO TERMINAL PVT LTD</font><br>
                                                <font size="2"><b>BANKER &nbsp;:-</b>&nbsp;YES Bank</font><br>
                                                <font size="2"><b>BRANCH  &nbsp;:-</b>&nbsp;GROUND FLOOR, SHEELA SHOPPEE SANJAY CHOWK, PANIPAT 132103</font><br>
                                                <font size="2"><b>A/C NO   &nbsp;:-</b>&nbsp;&emsp;007081400000021</font><br>
                                                <font size="2"><b>IFSC CODE   &nbsp; :-</b>&nbsp;YESB0000070</font>
                                                <%}%>      
                                                
                                                
                                                <br><br>
                                                <c:if test="${creditNoteNo != null}">
                                              <font size="1"> Debit Note No:<c:out value="${creditNoteNo}"/></font> &nbsp;
                                              <font size="1"> Debit Note Date:<c:out value="${creditNoteDate}"/></font> &nbsp;
                                              </c:if>
                                              <font size="1"> Ref code:<c:out value="${userName}"/></font> </td>
                                                
<!--                                                <td align="right">
                                                    <font size="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Authorised Signature </font><br>
                                                </td>-->
                                            </tr>
                                                <%}%>

                                        </table>
                                        <table style="float: right;">
                                            <tr>
                                                <td align="right" colspan="3">
                                                           <% String date9 = "01-11-2021";
                                                    String date10 = (String)request.getAttribute("billDate");
                                                   
                                                    SimpleDateFormat formatter3 = new SimpleDateFormat ("dd-MM-yyyy");
                                                    Date d9 = formatter3.parse(date9);
                                                    Date d10 = formatter3.parse(date10);
                                                if(d10.before(d9)) {
                                                %>
                                                        <font size="3"><b>For International Cargo Terminals And Rail Infrastructure Pvt.Ltd.</b></font><br>
                                                <% }else{  %>
                                                
                                    <font size="3" ><b>For Delhi International Cargo Terminal Pvt. Ltd.</b></font><br>

                                                <%}%>
                                                    <font size="3" >(Formerly Known As INTERNATIONAL CARGO TERMINALS & RAIL INFRASTRUCTURE PVT. LTD.)</font><br>
                                                </td>
                                            </tr>
                                           <tr>
                                            <td align=""></td>
                                            <td align="right">
                                                <img src="images/authsign.jpg" width="200" height="120"/> <br>
                                                <font size="3">&nbsp;Authorised Signature &emsp;&emsp;&nbsp;&nbsp;</font>
                                            </td>
                                        </tr>
                                        </table>
                                        <br>

                                    </td>
                                </tr>
                                <tr>
                                <table>

                                    <font >   <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Regd.Office:</b> Godrej Coliseum,Office No. 801,8th Floor,C-Wing,Behind Everard Nagar,off Somaiya Hospital Road,Sion (East)Mumbai-400022 <br> </font>
                                </table>
                                </tr>
                                </table>

                            </div>
                            <br>
                            <br>
                            <center>
                                <input type="button" class="button"  value="Print" onClick="print('print');" >
                                <c:if test="${repoOrder != null}">
                                <input type="button" class="button"  value="Generate New Bill" onClick="goBack('<c:out value="${repoOrder}"/>');" style="width: 160px;">
                                </c:if>
                                &nbsp;&nbsp;&nbsp;
                                <%--<%=String status = '<%=request.getAttribute("status")%>';%>--%>
                                <br>
                                <br>
                                <!--<span id="cancel" style="display:none"><input type="button" class="button"  value="cancel" onClick="cancel('<c:out value="${invoicecode}"/>');" ></span>-->

                                <c:if test="${billSubmitStatus == '0'}">
                                    <input type="button" class="button"  value="Edit Invoice Amount" onClick="editInvoice(<c:out value="${tripId}"/>);" style="width: 180px">
                                </c:if>
                            </center>
                        </div>
                        <script>
                            function goBack(repoOrder) {
                                //window.history.back();
                                //alert(repoOrder);
                                if(repoOrder == 'N'){
                                document.trip.action = '/throttle/handleSupplementInvoice.do?menuClick=1';
                                document.trip.submit();
                                }else if(repoOrder == 'Y'){
                                document.trip.action = '/throttle/handleSupplementInvoice.do?menuClick=1';
                                document.trip.submit();
                                }
                            }
                        </script>
                        <script type="text/javascript">
                            function print(val)
                            {
                                var DocumentContainer = document.getElementById(val);
                                var WindowObject = window.open('', "TrackHistoryData",
                                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                                WindowObject.document.writeln(DocumentContainer.innerHTML);
                                WindowObject.document.close();
                                WindowObject.focus();
                                WindowObject.print();
                                WindowObject.close();
                            }
                        </script>
                        <script>
                            $(".nexttab").click(function () {
                                var selected = $("#tabs").tabs("option", "selected");
                                $("#tabs").tabs("option", "selected", selected + 1);
                            });
                        </script>
                        <script type="text/javascript">
                            function cancel(billingNo) {
                                var submitStatus = <%=request.getAttribute("submitStatus")%>;
                                //                    alert(submitStatus);
                                //                    alert(billingNo);
                                var confirms = confirm("Do You want to Cancel the Invoice?");
                                if (confirms == true) {
                                    alert("You Pressed Yes")
                                    if (submitStatus == '0') {
                                        document.trip.action = '/throttle/viewOrderBillForSubmit.do?cancelStatus=' + 1 + '&billingNo=' + billingNo
                                        document.trip.submit();
                                    }
                                } else {
                                    alert("You Pressed No")
                                }
                            }


                            function checkSpace() {

                                var invoiceSize = <%=request.getAttribute("invoiceDetailsListSize")%>;
                                //                   alert(invoiceSize)

                                if (16 <= invoiceSize) {
                                    //alert("1");
                                    $("#breakOne").show();
                                    $("#tableTwo").show();
                                    $("#breakTwo").hide();
                                    $("#remarksBreak").show();
                                    //                        $("#headerOne").show();
                                    $("#headerTwo").hide();
                                    $("#tableThree").hide();
                                } else if (40 <= invoiceSize) {
                                    // alert("2");
                                    $("#breakOne").show();
                                    $("#breakTwo").show();
                                    $("#tableThree").show();
                                    $("#remarksBreak").show();
                                    $("#headerOne").show();
                                    //                        $("#headerTwo").hide();
                                } else if (60 <= invoiceSize) {
                                    // alert("3");
                                    $("#breakOne").show();
                                    $("#breakTwo").show();
                                    $("#remarksBreak").show();
                                    $("#headerOne").show();
                                    $("#headerTwo").show();
                                } else {
                                    // alert("4");
                                    $("#breakOne").hide();
                                    $("#breakTwo").hide();
                                    $("#headerOne").hide();
                                    $("#headerTwo").hide();
                                    $("#tableTwo").hide();
                                    $("#tableThree").hide();
                                    $("#remarksBreak").hide();
                                }
                                var firstRemarks = <%=request.getAttribute("firstRemarks")%>;

                                //alert("first:"+firstRemarks );
                                if (firstRemarks != '' && firstRemarks != null && firstRemarks != 'null') {
                                    $("#firstRemarks").show();

                                } else {
                                    $("#firstRemarks").hide();
                                }
                                var secondRemarks = <%=request.getAttribute("secondRemarks")%>;
                                if (secondRemarks != '' && secondRemarks != null && secondRemarks != 'null') {
                                    $("#secondRemarks").show();

                                } else {
                                    $("#secondRemarks").hide();
                                }

                                //            function changes(){
                                //              //  alert(<%=request.getAttribute("invoiceDetailsListSize")%>);
                                //                var count = <%=request.getAttribute("invoiceDetailsListSize")%> ;
                                //                for(i=0;i<=count;i++){
                                //                    if(i == 20){
                                ////                        break;
                                //                    twenty();
                                //                }
                                //                    else if(i == 40){
                                ////                        break;
                                //                    forty();
                                //                }   else if(i == 60){
                                ////                        break;
                                //                    sixty();
                                //                }else{
                                //                    $("#breakOne").hide();
                                //                    $("#breakTwo").hide();
                                //                    $("#headerOne").hide();
                                //                    $("#headerTwo").hide();
                                //                    $("#tableTwo").hide();
                                //                    $("#tableThree").hide();
                                //                    $("#remarksBreak").hide();
                                //                }
                                //            }
                                //        }
                                //
                                //            function twenty(){
                                //                        $("#breakOne").show();
                                //                        $("#tableTwo").show();
                                //                        $("#breakTwo").hide();
                                //                        $("#remarksBreak").show();
                                //                        $("#headerTwo").hide();
                                //                        $("#headerOne").show();
                                //            }
                                //            function forty(){
                                //                        $("#breakOne").show();
                                //                        $("#breakTwo").show();
                                //                        $("#tableThree").show();
                                //                        $("#remarksBreak").show();
                                //                        $("#headerOne").show();
                                //                        $("#headerTwo").show();
                                //
                                //            }
                                //            function sixty(){
                                //                        $("#breakOne").show();
                                //                        $("#breakTwo").show();
                                //                        $("#remarksBreak").show();
                                //                        $("#headerOne").show();
                                //                        $("#headerTwo").show();
                                //
                                //            }

                                var status = <%=request.getAttribute("status")%>;
                                if (status == '1') {
                                    //                        alert(status)
                                    document.getElementById('cancel').style.display = 'block';
                                } else {
                                    document.getElementById('cancel').style.display = 'none';
                                }
                            }
                        </script>
                    </div>
                    <%@ include file="../common/NewDesign/commonParameters.jsp" %></form>


            </body>
        </div>
    </div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>
