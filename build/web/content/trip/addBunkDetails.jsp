
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        //alert('hai');
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>
</head>
<style type="text/css">
    .blink {
        font-family:Tahoma;
        font-size:11px;
        color:#333333;
        padding-left:10px;
        background-color:#CC3333;
    }
    .blink1 {
        font-family:Tahoma;
        font-size:11px;
        color:#333333;
        padding-left:10px;
        background-color:#F2F2F2;
    }
</style>
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>
<script type="text/javascript">
    function  savePage() {
        var message = "";
        if (document.getElementById("bunkName").value == "") {
            message += "Please Enter Bunk Name \n";
        }
        if (document.getElementById("fuelType").value == "") {
            message += "Please Enter Fuel Type \n";
        }
        if (document.getElementById("currRate").value == "") {
            message += "Please Enter Current Rate \n";
        }
        if (document.getElementById("location").value == "") {
            message += "Please Enter the Location \n";
        }
        if (document.getElementById("state").value == "") {
            message += "Please Enter the State \n";
        }
        if (document.getElementById("activeStatus").value == "") {
            message += "Please select the Active Status \n";
        }
        /*if(document.getElementById("remarks").value == ""){
         message += "Please Enter the Remarks \n";
         }*/
        //alert(message);
        if (message == "") {
            var alterBunk = document.getElementById("alterBunk").value;
            if (alterBunk == '0') {
                document.addBunkDetails.action = "/throttle/handleTripAddBunkDetails.do";
                document.addBunkDetails.submit();
            } else {
            var bunkId = document.getElementById("bunkId").value;
                document.addBunkDetails.action = "/throttle/UpdateTripBunkDetails.do?bunkId=" + bunkId;
                document.addBunkDetails.submit();
            }
        } else {
            alert(message);
        }
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Master</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Master</a></li>
            <li class="active">Bunk Master Add</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="addBunkDetails" method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp"%>
                    <center><font color="blue">&nbsp;</font></center>

                    <table class="table table-info mb30 table-hover" style="width:100%">
                        <tr height="30"   ><td colSpan="6" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">Bunk Master Add</td></tr>
                        <c:if test="${bunkalterList != null}">
                            <c:forEach items="${bunkalterList}" var="bal">
                                <tr>
                                <input type="hidden" name="alterBunk" id="alterBunk" value="1">
                                <input type="hidden" name="bunkId" id="bunkId" value="<c:out value="${bal.bunkId}"/>">
                                </tr>
                                <tr>
                                    <td>Bunk Name</td>
                                    <td  height="30"><input name="bunkName" id="bunkName" class="form-control" style="width:240px;height:40px;" type="text" size="20" value="<c:out value="${bal.bunkName}"/>" autocomplete="off"></td>
                                    <td>Fuel Type</td>
                                    <td height="30">
                                        <!--<input name="fuelType" id="fuelType" class="form-control" style="width:240px;height:40px;" type="text" size="20" value="<c:out value="${bal.fuelType}" />"  autocomplete="off"></td>-->
                                    <select name="fuelType" id="fuelType" class="form-control" style="width:240px;height:40px;">
                                            <option value="Diesel" selected>Diesel</option>
                                            <option value="CNG">CNG</option>
                                    </select>
                                    <script>
                                      $("#fuelType").val(<c:out value="${bal.fuelType}" />);
                                    </script>
                                    <td>Current Rate</td>
                                    <td height="30"><input name="currRate" id="currRate" class="form-control" style="width:240px;height:40px;" type="text" size="20" value="<c:out value="${bal.currRate}"/>" autocomplete="off"></td>
                                </tr>
                                <tr>
                                    <td>Location</td>
                                    <td><input name="location" id="location" type="text" class="form-control" style="width:240px;height:40px;" size="20" value="<c:out value="${bal.currlocation}"/>" autocomplete="off"></td>
                                    <td>State</td>
                                    <td><input name="state" id="state" type="text" class="form-control" style="width:240px;height:40px;" size="20" value="<c:out value="${bal.bunkState}"/>" autocomplete="off"></td>
                                    <td>Active Status</td>
                                    <td><select name="activeStatus" id="activeStatus" class="form-control" style="width:240px;height:40px;">
                                            <c:if test="${bal.bunkStatus == 'Y'}">
                                                <option value="Y" selected>Yes</option>
                                                <option value="N">No</option>
                                            </c:if>
                                            <c:if test="${bal.bunkStatus == 'N'}">
                                                <option value="N" selected>No</option>
                                                <option value="Y">Yes</option>
                                            </c:if>
                                        </select>
                                </tr>
                                <tr>
                                    <td>Remarks</td>
                                    <td ><textarea cols="50%" name="remarks" id="remarks" class="form-control" style="width:240px;height:40px;"><c:out value="${bal.remarks}"></c:out></textarea></td>
                                   <td colspan="3">
                                    <input type="button" class="btn btn-info"  onclick="savePage();" value="Save">
                                    <input type="reset" class="btn btn-info" value="Clear">
                                </td>
                                </tr>
                            </c:forEach>            
                        </c:if>
                                
                          <c:if test="${bunkalterList == null}">
<!--                            <tr>
-->                            <input type="hidden" name="alterBunk" id="alterBunk" value="0"><!--
                            <input type="hidden" name="bunkId" id="bunkId" value="">
                            </tr>-->
                            <tr>
                                <td>Bunk Name</td>
                                <td  height="30"><input name="bunkName" id="bunkName" class="form-control" style="width:240px;height:40px;" type="text" size="20" value="" autocomplete="off"></td>
                                <td>Fuel Type</td>
                                <td height="30">
                                    <!--<input name="fuelType" id="fuelType" class="form-control" style="width:240px;height:40px;" type="text" size="20" value="" autocomplete="off"></td>-->
                                <select name="fuelType" id="fuelType" class="form-control" style="width:240px;height:40px;">
                                            <option value="Diesel" selected>Diesel</option>
                                            <option value="CNG">CNG</option>
                                    </select>
                                <td>Current Rate</td>
                                <td height="30"><input name="currRate" id="currRate" class="form-control" style="width:240px;height:40px;" type="text" size="20" value="" autocomplete="off"></td>
                            </tr>
                            <tr>
                                <td>Location</td>
                                <td><input name="location" id="location" type="text" class="form-control" style="width:240px;height:40px;" size="20" value="" autocomplete="off"></td>
                                <td>State</td>
                                <td><input name="state" id="state" type="text" class="form-control" style="width:240px;height:40px;" size="20" value="" autocomplete="off"></td>
                                <td>Active Status</td>
                                <td><select name="activeStatus" id="activeStatus" class="form-control" style="width:240px;height:40px;">
                                        <option value="">--Select--</option>
                                        <option value="Y">Yes</option>
                                        <option value="N">No</option>
                                    </select>
                            </tr>
                            <tr>
                                <td>Remarks</td>
                                <td ><textarea cols="50%" name="remarks" id="remark" class="form-control" style="width:240px;height:40px;"></textarea></td>
                                <td></td>
                                <td colspan="3">
                                    <input type="button" class="btn btn-info"  onclick="savePage();" value="Save">
                                    <input type="reset" class="btn btn-info" value="Clear">
                                </td>
                            </tr>
                        </c:if>

                    </table>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>