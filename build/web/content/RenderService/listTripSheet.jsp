<%-- 
    Document   : viewTripSheet
    Created on : Mar 7, 2012, 6:59:45 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" import="java.util.ArrayList,java.util.Iterator,ets.domain.operation.business.OperationTO"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script><!-- External script -->
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        <title>Trips Sheet Edit</title>
    </head>
    <body>
     <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
        <table width="100%" align="center" border="0" class="table2">
            <tr height="30">
                <td colspan="4" align="left" class="texttitle"><h2> Edit Trip Sheet </h2></td>
            </tr>
        </table>
        <table width="90%" id="table" class="sortable" align="center" border="0">
            <thead>
            <tr height="30">
                <th><h3>S.No</h3></th>
                <th><h3>Trip Id</h3></th>
                <th><h3>Route</h3></th>
                <th><h3>Vehicle No</h3></th>
                <th><h3>Trip Date</h3></th>
                <th><h3>Status</h3></th>
            </tr>
            </thead>
            <tbody>
            <%
                        int sno = 0;
                        String rowClass = "text1";
                        ArrayList tripDetails = new ArrayList();
                        if (request.getAttribute("tripDetails") != null) {
                            tripDetails = (ArrayList) request.getAttribute("tripDetails");
                        }
                        OperationTO operationTo;
                        Iterator it = tripDetails.iterator();
                        while (it.hasNext()) {

                            operationTo = new OperationTO();
                            operationTo = (OperationTO) it.next();
                            sno++;
                            if ((sno % 2) == 0) {
                                rowClass = "text2";
                            } else {
                                rowClass = "text1";
                            }
            %>
            <tr height="30">

                <td align="left" class="<%=rowClass%>"><%=sno%></td>
                <td align="left" class="<%=rowClass%>"><a href="viewEditTripSheet.do?tripSheetIdParam=<%=operationTo.getTripSheetId()%>" ><%=operationTo.getTripSheetId()%></a></td>
                <td align="left" class="<%=rowClass%>"><%=operationTo.getRouteName()%></td>
                <td align="left" class="<%=rowClass%>"><%=operationTo.getRegno()%></td>
                <td align="left" class="<%=rowClass%>"><%=operationTo.getTripDate()%></td>
                <td align="left" class="<%=rowClass%>"><%=operationTo.getTripStatus()%></td>

            </tr>
            <%

                        }

            %>
        </tbody>

        </table>
        <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>

        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5" selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1,true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1,true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table",1);
        </script>


    </body>
</html>
