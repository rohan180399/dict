

<%--
    Document   : newBank
    Created on : 20 Oct, 2012, 5:49:25 PM
    Author     : ASHOK
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title></title>
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>

        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    </head>
    <script>
        function submitPage()
        {

            if (textValidation(document.add.bankName, 'bankName')) {
                return;
            }
            if (textValidation(document.add.bankCode, 'bankCode')) {
                return;
            }
            if (textValidation(document.add.description, 'description')) {
                return;
            }
            if($("#bankNameStatus").text() == ''){
            document.add.action = '/throttle/saveNewBank.do';
            document.add.submit();
            }else {
                alert("Bank Name is Already Exists");
                $("#bankName").focus();
            }
        }
        function setFocus() {
                $("#bankName").focus();
        }


        var httpRequest;
        function checkBankAccountName() {
            var bankName = document.getElementById('bankName').value;
            if (bankName != '' ) {
                var url = '/throttle/checkBankAccountName.do?bankName='+bankName;
                if (window.ActiveXObject) {
                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                } else if (window.XMLHttpRequest) {
                    httpRequest = new XMLHttpRequest();
                }
                httpRequest.open("GET", url, true);
                httpRequest.onreadystatechange = function() {
                    processRequest();
                };
                httpRequest.send(null);
            }
        }


        function processRequest() {
            if (httpRequest.readyState == 4) {
                if (httpRequest.status == 200) {
                    var val = httpRequest.responseText.valueOf();
                    alert(val);
                    if (val != 'null') {
                    alert(val);
                        $("#bankNameStatus").text('Bank Name is Already  Exists :' + val);
                    } else {
                        $("#bankNameStatus").text('');
                    }
                } else {
                    alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
                }
            }
        }

    </script>
    <body onload="setFocus();">
        <form name="add" method="post">
            <%@ include file="/content/common/path.jsp" %>

            <%@ include file="/content/common/message.jsp" %>

            <table align="center" width="700" border="0" cellspacing="0" cellpadding="0" class="border">
                <tr height="30">
                    <Td colspan="2" class="contenthead">Add Bank</Td>&nbsp;&nbsp;&nbsp;
                    <font color="red" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; ">
                        <div align="center" id="bankNameStatus" height="20" >&nbsp;&nbsp;</div>
                    </font>
                </tr>
                <tr height="30">
                    <td class="text2"><font color="red">*</font>Name</td>
                    <td class="text2"><input name="bankName" id="bankName" type="text" class="form-control" value="" size="50" onchange="checkBankAccountName();" ></td>
                </tr>
                <tr height="30">
                    <td class="text1"><font color="red">*</font>Code</td>
                    <td class="text1"><input name="bankCode" type="text" class="form-control" value="" size="50"  ></td>
                </tr>
                <tr height="30">
                    <td class="text2"><font color="red">*</font>Address</td>
                    <td class="text2">
                        <textarea class="form-control" name="address"></textarea>
                    </td>
                </tr>
                <tr height="30">
                    <td class="text1"><font color="red">*</font>Phone No</td>
                    <td class="text1"><input name="phoneNo" type="text" class="form-control" value="" size="50" maxlength="12"  onKeyPress='return onKeyPressBlockCharacters(event);' ></td>
                </tr>
                <tr height="30">
                    <td class="text2"><font color="red">*</font>Account No</td>
                    <td class="text2"><input name="accntCode" type="text" class="form-control" value="" size="50"  ></td>
                </tr>
                <tr height="30">
                    <td class="text1"><font color="red">*</font>Description</td>
                    <td class="text1"><input name="description" type="text" class="form-control" value="" size="50"  ></td>
                </tr>
            </table>
            <br>
            <br>
            <center>
                <input type="button" class="button" value="Save" onclick="submitPage();" />
                &emsp;<input type="reset" class="button" value="Clear">
            </center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
