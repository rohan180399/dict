/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.programmingfree.excelexamples;

/**
 *
 * @author Arul
 */
//import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import com.oreilly.servlet.multipart.Part;
import ets.arch.util.SendMail;

import java.io.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.plot.PlotOrientation;
import java.util.Iterator;
import java.sql.*;
import java.io.*;
//import static java.lang.ProcessBuilder.Redirect.to;
import java.math.BigDecimal;
//import java.util.*;
import java.util.Calendar;
import java.text.*;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;
import javax.activation.DataHandler;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import static org.apache.poi.hssf.usermodel.HeaderFooter.date;
import org.jfree.chart.ChartColor;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.data.time.Hour;
import org.jfree.data.time.TimeSeries;
import ets.domain.trip.business.TripTO;
import java.util.ArrayList;
import java.util.Iterator;

 public class CreateBarChartExample {
    
    public CreateBarChartExample(String tripId , String filePath,ArrayList vehicleLogDetails) {
        //////System.out.println("tripId"+tripId);
        //////System.out.println("filePath"+filePath);
        
         // connection
                Statement statement = null;
                Connection connection = null;
        
        String url = "jdbc:mysql://localhost:3306/";
//        String url = "jdbc:mysql://localhost/";
//        String dbName = "throttlebrattle_100314";
        String dbName = "throttlebrattle";
        String driver = "com.mysql.jdbc.Driver";
        String userName = "root";
        String password = "etsadmin";


//        File theDir = new File("C:/Apps1/");
//
//        if (!theDir.exists()) {
//            boolean result = theDir.mkdir();
//            if (result) {
//                //////System.out.println("theDir");
//            }
//
//        }
        try {
//            connection = DriverManager.getConnection(url + dbName, userName, password);

            Calendar cal = Calendar.getInstance();
            String rangeStartDate1 = "";
            String trip_Id = tripId;
            SimpleDateFormat processDateFormat1 = new SimpleDateFormat("dd-MM-yyyy");
                            String tempPath = filePath+ "/"  + "dataLogExcel"+trip_Id+".xls" ;
                            String actPath = filePath  + "/" + "dataLogExcel"+trip_Id+".xls";
                            //////System.out.println("tempPath..." + tempPath);
                            //////System.out.println("actPath..." + actPath);
            String filename = filePath + "/" + "dataLogExcel"+trip_Id+".xls";
             
            //////System.out.println("filename = " + filename);
            //////System.out.println("processDateFormat1 = " + processDateFormat1);

            //first my_sheet start
//            HSSFWorkbook my_workbook1 = new HSSFWorkbook();
            XSSFWorkbook my_workbook1 = new XSSFWorkbook();
            String sheetName = "Report";
            Sheet my_sheet1 = (Sheet) my_workbook1.createSheet(sheetName);
            Row s1Row1 = my_sheet1.createRow((short) 23);
            s1Row1.setHeightInPoints(50); // row hight
            s1Row1.createCell((short) 0).setCellValue("S.No");
            my_sheet1.setColumnWidth((short) 0, (short) 4000); // cell width
            s1Row1.createCell((short) 1).setCellValue("Log Date Time");
            my_sheet1.setColumnWidth((short) 1, (short) 4000); // cell width
            s1Row1.createCell((short) 2).setCellValue("Current Temperature");
            my_sheet1.setColumnWidth((short) 2, (short) 4000); // cell width
            s1Row1.createCell((short) 3).setCellValue("Log Time");
            my_sheet1.setColumnWidth((short) 3, (short) 4000); // cell width
            s1Row1.createCell((short) 4).setCellValue("Vehicle No");
            my_sheet1.setColumnWidth((short) 4, (short) 4000); // cell width
            s1Row1.createCell((short) 5).setCellValue("Current  location");
            my_sheet1.setColumnWidth((short) 5, (short) 4000); // cell width
            s1Row1.createCell((short) 6).setCellValue("Distance travelled");
            my_sheet1.setColumnWidth((short) 6, (short) 4000); // cell width

            Class.forName(driver).newInstance();
            Connection conn = DriverManager.getConnection(url + dbName, userName, password);
            Statement st = conn.createStatement();
            Statement st1 = conn.createStatement();
            String logDateTime = "";
            String logDate = "";
            String logTime = "";
            String currentTemperature = "";
            String regNo = "";
            String location = "";
            String dstanceTravelled = "";
//            ResultSet res1 = st1.executeQuery(" select `date` as logDateTime, \n"
//                    + "DATE_FORMAT(`date`,'%d-%m-%Y') as logDate, \n"
//                    + "TIME_FORMAT(`date`, '%H:%i:%s' ) as logtime, \n"
//                    + "current_temperature, \n"
//                    + "b.reg_no,a.location,a.distance_travelled \n"
//                    + "from \n"
//                    + "ts_vehicle_location_log a, \n"
//                    + "papl_vehicle_reg_no b \n"
//                    + "where a.vehicle_Id = b.vehicle_Id \n"
//                    + "and b.Active_Ind = 'Y' \n"
//                    + "and a.trip_Id = "+trip_Id+" \n"
//                    + "order by `date` ");
            int cntr = 24;
              Iterator itr = vehicleLogDetails.iterator();
            TripTO tripTO = null;


             while (itr.hasNext()) {
//                    //////System.out.println("cntr = " + cntr);

                 tripTO = new TripTO();
                tripTO = (TripTO) itr.next();
                logDateTime = tripTO.getLogDateTime();
                //////System.out.println("logDateTime" +logDateTime);
                logDate = tripTO.getLogDate();
                logTime = tripTO.getLogTime();
                currentTemperature = tripTO.getTemperature();
                regNo = tripTO.getRegNo();
                location = tripTO.getLocation();
                dstanceTravelled = tripTO.getDistance();
                s1Row1 = my_sheet1.createRow((short) cntr);
                s1Row1.createCell((short) 0).setCellValue(cntr);
                s1Row1.createCell((short) 1).setCellValue(logDateTime);
                s1Row1.createCell((short) 2).setCellValue(currentTemperature);
//                s1Row1.createCell((short) 3).setCellValue(logDate);
                s1Row1.createCell((short) 3).setCellValue(logTime);
                s1Row1.createCell((short) 4).setCellValue(regNo);
                s1Row1.createCell((short) 5).setCellValue(location);
                s1Row1.createCell((short) 6).setCellValue(dstanceTravelled);
                cntr++;
            }

            FileOutputStream fileOut = new FileOutputStream(filename);
            my_workbook1.write(fileOut);
            FileInputStream chart_file_input = new FileInputStream(new File(filePath  + "/" + "dataLogExcel"+trip_Id+".xls"));
            String fileName = "dataLogExcel"+trip_Id+".xls";

            XSSFWorkbook my_workbook = new XSSFWorkbook(chart_file_input);
            XSSFSheet my_sheet = my_workbook.getSheetAt(0);
            DefaultCategoryDataset my_bar_chart_dataset = new DefaultCategoryDataset();
            //////System.out.println("conn = " + conn);
//            ResultSet res = st.executeQuery(" SELECT \n"
//                    + "DATE_FORMAT(`date`,'%d-%m-%Y') as logDate,\n"
//                    + "TIME_FORMAT(`date`, '%H:%i:%s' ) as logtime,\n"
//                    + "current_temperature FROM ts_vehicle_location_log\n"
//                    + "where trip_Id = "+trip_Id+" \n"
//                    + "group by DATE_FORMAT(`date`,'%d-%m-%Y')\n"
//                    + "order by `date` ");
//
//            while (res.next()) {
//                logDate = res.getString("logDate");
//            }
            // setting connection autocommit false
//                        connection.setAutoCommit(false);
                        // Getting reference to connection object
//                        statement = connection.createStatement();
                        // setting connection autocommit false
//                        connection.setAutoCommit(false);
                        // Getting reference to connection object
//                        statement = connection.createStatement();
                        // creating Query String
                        
//                        String updateQuery = "UPDATE ts_trip_master SET templogfile='madhan' WHERE trip_Id=400";
//                        int result = statement.executeUpdate(updateQuery);
//                        if (result == 1) {
//                                //////System.out.println("Table Updated Successfully.......");
//                        }
                        PreparedStatement ps=conn.prepareStatement("UPDATE ts_trip_master SET templogfile=? WHERE trip_Id=?");
                        ps.setString(1,fileName);
                        ps.setString(2,tripId);
                        ps.executeUpdate();
                        
                        
                         
                                

//            PreparedStatement updateEXP = conn.prepareStatement( "update`ts_trip_master` set `temp_log_file` = 'madhan'  where `trip_Id` = '"+trip_Id+"'");
//            ResultSet updateEXP_done = updateEXP.executeQuery();

            /* Read the bar chart data from the excel file */
            /* XSSFWorkbook object reads the full Excel document. We will manipulate this object and
             write it back to the disk with the chart */
            /* Read chart data worksheet */
            /* Create Dataset that will take the chart data */
            /* We have to load bar chart data now */
            /* Begin by iterating over the worksheet*/
            /* Create an Iterator object */
            Iterator<Row> rowIterator = my_sheet.iterator();
            /* Loop through worksheet data and populate bar chart dataset */
            String chart_label = "";
            Number chart_data = 3;
//            java.util.Date d = new Date();
            String seriesLabel = "";
//            int chart_data=0;
            TimeSeries series = new TimeSeries("", Hour.class);
            SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss aa");
            DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            String inputText = "2012-11-17 23:00:00";
            java.util.Date date = null;

            String outputText = "";

            while (rowIterator.hasNext()) {
                //Read Rows from Excel document
                Row row = rowIterator.next();
                //Read cells in Rows and get chart data
                Iterator<Cell> cellIterator = row.cellIterator();
                int columnIndex = 0;
                int checkRow = 0;
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    checkRow = cell.getRowIndex();
//                    //////System.out.println("checkRow = " + checkRow);
                    if (checkRow > 23) {
                        columnIndex = cell.getColumnIndex();
                        if (columnIndex == 1) {
                            chart_label = cell.getStringCellValue();
                            date = inputFormat.parse(chart_label);
                            outputText = outputFormat.format(date);
                            //////System.out.println("outputText = " + outputText);
                        }
                        if (columnIndex == 2) {
                            chart_data = Float.parseFloat(cell.getStringCellValue());
//                        //////System.out.println("chart_data = " + chart_data);
                        }
                        if (columnIndex == 3) {
                            seriesLabel = cell.getStringCellValue();
                        }
                        my_bar_chart_dataset.addValue(chart_data.doubleValue(), seriesLabel, outputText);

                    }

//                    switch (cell.getCellType()) {
//                        case Cell.CELL_TYPE_NUMERIC:
//                            chart_data = cell.getNumericCellValue();
//                            break;
//                        case Cell.CELL_TYPE_STRING:
//                            chart_label = cell.getStringCellValue();
//                            break;
//                    }
                }
                /* Add data to the data set */
                /* We don't have grouping in the bar chart, so we put them in fixed group */

            }
            /* Create a logical chart object with the chart data collected */
//            JFreeChart BarChartObject = ChartFactory.createXYBarChart("Time Vs Temperature", "Time", true, "Temperature",my_bar_chart_dataset, PlotOrientation.VERTICAL, true, true, false);
            JFreeChart BarChartObject = ChartFactory.createLineChart("Time Vs Temperature", "Time", "Temperature", my_bar_chart_dataset, PlotOrientation.VERTICAL, true, true, false);
            CategoryPlot plot = (CategoryPlot) BarChartObject.getPlot();
            CategoryAxis xAxis = (CategoryAxis) plot.getDomainAxis();
            xAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_90);
            xAxis.setAxisLineVisible(false);
            BarChartObject.setBackgroundPaint(ChartColor.WHITE);
            /* Dimensions of the bar chart */
            int width = logDate.length() * 125; /* Width of the chart */

            int height = 490; /* Height of the chart */
            /* We don't want to create an intermediate file. So, we create a byte array output stream 
             and byte array input stream
             And we pass the chart data directly to input stream through this */
            /* Write chart as PNG to Output Stream */

            ByteArrayOutputStream chart_out = new ByteArrayOutputStream();
            ChartUtilities.writeChartAsPNG(chart_out, BarChartObject, width, height);
            /* We can now read the byte data from output stream and stamp the chart to Excel worksheet */
            int my_picture_id = my_workbook.addPicture(chart_out.toByteArray(), Workbook.PICTURE_TYPE_PNG);
            /* we close the output stream as we don't need this anymore */
            chart_out.close();
            /* Create the drawing container */
            XSSFDrawing drawing = my_sheet.createDrawingPatriarch();
            /* Create an anchor point */
            ClientAnchor my_anchor = new XSSFClientAnchor();
            /* Define top left corner, and we can resize picture suitable from there */
            my_anchor.setCol1(0);
            my_anchor.setRow1(0);
            /* Invoke createPicture and pass the anchor point and ID */
            XSSFPicture my_picture = drawing.createPicture(my_anchor, my_picture_id);
            /* Call resize method, which resizes the image */
            my_picture.resize();
            /* Close the FileInputStream */
            chart_file_input.close();
            /* Write changes to the workbook */
            FileOutputStream out = new FileOutputStream(new File( filePath +"/" + "dataLogExcel"+trip_Id+".xls"));
            my_workbook.write(out);
            
            out.close();
            conn.close();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




   
}
