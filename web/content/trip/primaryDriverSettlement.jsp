<%--
    Document   : driverSettlementReport
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/layout-styles.css";
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });



        });

        $(function() {
            //	alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });

        });
    </script>
    <script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
    </script>
    <script type="text/javascript">


    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user

        $('#primaryDriver').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getPrimaryDriver.do",
                    dataType: "json",
                    data: {
                        driverName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        var primaryDriver = $('#primaryDriver').val();
                        if(items == '' && primaryDriver != ''){
                            alert("Invalid Primary Driver Name");
                            $('#primaryDriver').val('');
                            $('#primaryDriverId').val('');
                            $('#primaryDriver').focus();
                        }else{
                        }
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var id = ui.item.Id;
                var settledDate = ui.item.settledDate;
                $('#primaryDriver').val(value);
                $('#primaryDriverId').val(id);
                $('#fromDate').val(settledDate);
                return false;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
            .data("item.autocomplete", item)
            .append("<a>" + itemVal + "</a>")
            .appendTo(ul);
        };
    });


    function submitPage(value) {
        if(value == "search"){
            if((document.getElementById('primaryDriver').value == '') && (document.getElementById('primaryDriverId').value= '')){
                alert("driver name should not empty");
                document.getElementById('primaryDriver').focus();
            }else if(document.getElementById('fromDate').value == ''){
                alert("from date should not empty");
                document.getElementById('fromDate').focus();
            }else if(document.getElementById('toDate').value == ''){
                alert("to date should not empty");
                document.getElementById('toDate').focus();
            }else{
                document.driverSettlement.action = '/throttle/viewPrimaryDriverSettlement.do';
                document.driverSettlement.submit();
            }
        }else{
            if(document.getElementById("payAmount").value == ''){
            alert("pay amount should not empty");
            document.getElementById("payAmount").focus();
            }else if(document.getElementById("settlementRemarks").value == ''){
            alert("remarks should not empty");
            document.getElementById("settlementRemarks").focus();
            }else{
            document.driverSettlement.action = '/throttle/savePrimaryDriverSettlement.do';
            document.driverSettlement.submit();
            }
        }
    }

    function setValues(){
        if('<%=request.getAttribute("primaryDriverId")%>' != 'null'){
            document.getElementById('primaryDriverId').value='<%=request.getAttribute("primaryDriverId")%>';
        }
        if('<%=request.getAttribute("primaryDriver")%>' != 'null'){
            document.getElementById('primaryDriver').value='<%=request.getAttribute("primaryDriver")%>';
        }
        if('<%=request.getAttribute("fromDate")%>' != 'null'){
            document.getElementById('fromDate').value='<%=request.getAttribute("fromDate")%>';
        }
        if('<%=request.getAttribute("toDate")%>' != 'null'){
            document.getElementById('toDate').value='<%=request.getAttribute("toDate")%>';
        }
    }

    function viewTripDetails(tripId) {
        window.open('/throttle/viewTripSheetDetails.do?tripId='+tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    function viewVehicleDetails(vehicleId) {
        window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }

    </script>
    <style>
        .ui-autocomplete { cursor:pointer; height:120px; overflow-y:scroll }
    </style>

</head>
<body onload="setValues();calculatePayAmount();">
    <form name="driverSettlement" action=""  method="post">
        <%@ include file="/content/common/path.jsp" %>
        <%@ include file="/content/common/message.jsp" %>
        <br>
        <br>
        <br>

        <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
            <tr id="exp_table" >
                <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                    <div class="tabs" align="left" style="width:850;">
                        <ul class="tabNavigation">
                            <li style="background:#76b3f1">Primary Driver Settlement</li>
                        </ul>
                        <div id="first">
                            <table width="800" cellpadding="0" cellspacing="0" border="0" align="center" class="table4" >
                                <tr>

                                    <td class="text1" align="center" height="30">Primary Driver Name</td>
                                    <td class="text1" height="30">
                                        <input name="primaryDriver" id="primaryDriver" class="textbox" />
                                        <input type="hidden" name="primaryDriverId" id="primaryDriverId" class="textbox" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text1" align="center" height="30"> <font color="red">*</font>From Date</td>
                                    <td class="text1" height="30"><input type="text" class="datepicker" id="fromDate" name="fromDate" autocomplete="off" value="<c:out value="${fromDate}"/>" readonly/></td>
                                    <td class="text1" align="center" height="30"> <font color="red">*</font>To Date</td>
                                    <td class="text1" height="30"><input type="text" class="datepicker" id="toDate"  name="toDate" autocomplete="off" value="<c:out value="${toDate}"/>"/></td>
                                </tr>

                                <tr>

                                    <td class="text1" height="30" colspan="2" align="center"></td>
                                    <td class="text1" height="30"  align="center"><input type="button" class="button" value="Search" name="search" onClick="submitPage(this.name)"/></td>

                                </tr>
                            </table>
                        </div></div>
                </td>
            </tr>
        </table>
        <br>
        <br>
        <%--        <c:if test="${settlementTripsSize > 0 || driverIdleBhattaSize > 0}"> --%>
        <table>
            <tr class="contenthead">
                <td colspan="2">Last Settled Date : <c:out value="${driverLastBalanceDate}"/></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td colspan="2">Last Balance Amount : <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${driverLastBalanceAmount}" />
                    <input type="hidden" name="driverLastBalanceAmount" id="driverLastBalanceAmount" value="<c:out value="${driverLastBalanceAmount}"/>"/>
                </td>
            </tr>
        </table>
        <div id="tabs" >
            <ul>

                <li><a href="#tripDetails"><span>Trip Details</span></a></li>
                <li><a href="#vehicleAdvance"><span>Vehicle Advance</span></a></li>
                <li><a href="#idleBhatta"><span>Idle Bhatta</span></a></li>
                <li><a href="#summary"><span>summary</span></a></li>
            </ul>
            <br>
            <div id="tripDetails">

                <c:if test = "${settlementTrips != null}" >
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr height="45" >
                            <td class="contenthead" align="center">S.No</td>
                            <td class="contenthead" align="center">Trip Code</td>
                            <td class="contenthead" align="center">Vehicle No</td>
                            <td class="contenthead" align="center" style="width: 80px">Start Date</td>
                            <td class="contenthead" align="center" style="width: 80px">End Date</td>
                            <td class="contenthead" align="center">Total Run Km</td>
                            <td class="contenthead" align="center">Total Hours</td>
                            <td class="contenthead"  align="center">Trip Settled Amount</td>
                            <td class="contenthead"  align="center">Trip UnCleared Amount</td>
                            <td class="contenthead"  align="center">Driver Count</td>
                            <td class="contenthead"  align="center">Amount</td>
                        </tr>
                        <% int index = 0, sno = 1;%>
                        <c:set var="totalRunKms" value="${0}"/>
                        <c:set var="totalRunHms" value="${0}"/>
                        <c:set var="totalUnclearedAmount" value="${0}"/>
                        <c:forEach items="${settlementTrips}" var="settlement">

                            <input type="hidden" name="tripId" id="tripId" value="<c:out value="${settlement.tripId}"/>"/>
                            <input type="hidden" name="vehicleIds" id="vehicleIds" value="<c:out value="${settlement.vehicleId}"/>"/>
                            <input type="hidden" name="runKMs" id="runKM" value="<c:out value="${settlement.totalKm}"/>"/>
                            <input type="hidden" name="runHours" id="runHours" value="<c:out value="${settlement.totalHm}"/>"/>
                            <input type="hidden" name="driverCount" id="driverCount" value="<c:out value="${settlement.driverCount}"/>"/>
                            <input type="text" name="endingBalance" id="endingBalance" value="<c:out value="${settlement.endingBalance}"/>"/>
                            <input type="hidden" name="unclearedAmount" id="unclearedAmount" value="<c:out value="${settlement.unclearedAmount}"/>"/>
                            <input type="hidden" name="amount" id="amount" value="<c:out value="${settlement.amount}"/>"/>
                            <c:set var="totalRunKms" value="${totalRunKms+settlement.totalKm}"/>
                            <c:set var="totalRunHms" value="${totalRunHms+settlement.totalHm}"/>
                            <c:set var="tripAmount" value="${tripAmount+settlement.amount}"/>
                            <c:set var="totalUnclearedAmount" value="${totalUnclearedAmount+settlement.unclearedAmount}"/>
                            <%
                                        String classText3 = "";
                                        int oddEven = sno % 2;
                                        if (oddEven > 0) {
                                            classText3 = "text1";
                                        } else {
                                            classText3 = "text2";
                                        }
                            %>
                            <tr height="30">

                                <td class="<%=classText3%>" align="center"><%=sno%></td>
                                <td class="<%=classText3%>" align="left">
                                    <a href="#" onclick="viewTripDetails('<c:out value="${settlement.tripId}"/>');"><c:out value="${settlement.tripCode}"/></a></td>
                                <td class="<%=classText3%>" align="left">
                                    <a href="#" onclick="viewVehicleDetails('<c:out value="${settlement.vehicleId}"/>')"><c:out value="${settlement.regNo}"/></a></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${settlement.startDate}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${settlement.endDate}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${settlement.totalKm}"/></td>
                                <td class="<%=classText3%>"  align="left"><c:out value="${settlement.totalHm}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${settlement.endingBalance}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${settlement.unclearedAmount}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${settlement.driverCount}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${settlement.amount}"/></td>

                            </tr>
                            <%
                                        index++;
                                        sno++;
                            %>
                        </c:forEach>
                        <tr>
                            <td colspan="11">&nbsp;</td>
                        </tr>
                        <tr class="contenthead">
                            <td colspan="10" align="center">Total Amount</td>
                            <td>
                                <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${tripAmount}" />
                                <input type="hidden" name="tripAmount" id="tripAmount" value="<c:out value="${tripAmount}"/>"/></td>

                        </tr>

                    </table>



                    <br/>
                    <br/>

                </c:if>
                <center>
                    <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Next" /></a>
                </center>

            </div>
            <div id="vehicleAdvance">
                <c:set var="advAmount" value="${0}"/>
                <c:if test = "${vehicleAdvance != null}" >
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr height="45" >
                            <td class="contenthead" align="center">S.No</td>
                            <td class="contenthead" align="center">Vehicle No</td>
                            <td class="contenthead" align="center">Advance Code</td>
                            <td class="contenthead" align="center">Advance Date</td>
                            <td class="contenthead" align="center">Advance Amount</td>
                            <td class="contenthead" align="center">No Of Drivers</td>
                            <td class="contenthead"  align="center">Amount</td>
                        </tr>
                        <% int index = 0, sno = 1;%>
                        <c:forEach items="${vehicleAdvance}" var="advance">
                            <input type="hidden" name="vehicleAdvanceId" id="vehicleAdvanceId" value="<c:out value="${advance.vehicleAdvanceId}"/>"/>
                            <input type="hidden" name="advanceVehicleId" id="advanceVehicleId" value="<c:out value="${advance.vehicleId}"/>"/>
                            <input type="hidden" name="advanceDriverCount" id="advanceDriverCount" value="<c:out value="${advance.driverCount}"/>"/>
                            <input type="hidden" name="paidAdvance" id="paidAdvance" value="<c:out value="${advance.paidAdvance}"/>"/>
                            <input type="hidden" name="advanceAmount" id="advanceAmount" value="<c:out value="${advance.amount}"/>"/>

                            <c:set var="advAmount" value="${advAmount+advance.amount}"/>
                            <%
                                        String classText3 = "";
                                        int oddEven = sno % 2;
                                        if (oddEven > 0) {
                                            classText3 = "text1";
                                        } else {
                                            classText3 = "text2";
                                        }
                            %>
                            <tr height="30">
                                <td class="<%=classText3%>" align="center"><%=sno%></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${advance.regNo}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${advance.advanceCode}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${advance.advanceDate}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${advance.paidAdvance}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${advance.driverCount}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${advance.amount}"/></td>


                            </tr>
                            <%
                                        index++;
                                        sno++;
                            %>
                        </c:forEach>
                        <tr>
                            <td colspan="7">&nbsp;</td>
                        </tr>
                        <tr class="contenthead">
                            <td colspan="6" align="center">Total Amount</td>
                            <td><c:out value="${advAmount}"/>
                                <input type="hidden" name="advAmount" id="advAmount" value="<c:out value="${advAmount}"/>"/></td>

                        </tr>
                    </c:if>
                </table>
                <br/>
                <br/>
                <center>
                    <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Next" /></a>
                </center>
            </div>
            <div id="idleBhatta">
                <c:set var="bhattaAmount" value="${0}"/>
                <c:if test = "${driverIdleBhatta != null}" >
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr height="45" >
                            <td  class="contenthead"  align="center">S.No</td>
                            <td class="contenthead"  align="center">Vehicle No</td>
                            <td class="contenthead"  align="center">Bhatta Date</td>
                            <td class="contenthead"  align="center">Amount</td>
                        </tr>
                        <% int index = 0, sno = 1;%>
                        <c:forEach items="${driverIdleBhatta}" var="idleBhatta">
                            <c:set var="bhattaAmount" value="${bhattaAmount + idleBhatta.amount}"></c:set>
                            <input type="hidden" name="bhattaAmount" id="bhattaAmount" value="<c:out value="${bhattaAmount}"/>"
                                   <%
                                               String classText3 = "";
                                               int oddEven = sno % 2;
                                               if (oddEven > 0) {
                                                   classText3 = "text1";
                                               } else {
                                                   classText3 = "text2";
                                               }
                                   %>
                                   <tr height="30">
                                <td class="<%=classText3%>" align="center"><%=sno%>
                                    <input type="hidden" name="idleBhattaId" id="idleBhattaId" value="<c:out value="${idleBhatta.idleBhattaId}"/>"
                                </td>
                                <td class="<%=classText3%>" align="left"><c:out value="${idleBhatta.regNo}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${idleBhatta.bhattaDate}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${idleBhatta.amount}"/></td>

                            </tr>
                            <%
                                        index++;
                                        sno++;
                            %>
                        </c:forEach>
                        <tr height="30">
                            <td  align="left">&nbsp;</td>
                            <td  align="left">&nbsp;</td>
                            <td  align="left">Total Idle Bhatta</td>
                            <td  align="left">
                                <input type="hidden" name="bhattaAmount" id="bhattaAmount" value="<c:out value="${bhattaAmount}"/>"  />
                                <input type="hidden" name="idleDays" id="idleDays" value="<c:out value="${driverIdleBhattaSize}"/>"  />
                                <c:out value="${bhattaAmount}"/>
                            </td>

                        </tr>
                    </c:if>

                </table>
                <br/>
                <br/>
                <center>
                    <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Next" /></a>
                </center>
            </div>

            <div id="summary">

                <table  border="1" class="border" align="center" width="800px" cellpadding="0" cellspacing="0" id="bg">
                    <tr>
                        <td class="contenthead" colspan="4" >Driver Settlement Details
                        </td>
                    </tr>


                    <tr height="25">
                        <td class="text1">Driver Name</td>
                        <td class="text1"><label><c:out value="${primaryDriver}"/></label></td>
                        <td class="text1">Total No of Trips</td>
                        <td class="text1"><label >
                                <c:out value="${settlementTripsSize}"/>
                                <input type="hidden" name="settlementTripsSize" id="settlementTripsSize" value="<c:out value="${settlementTripsSize}"/>"  />
                            </label></td>
                    </tr>

                    <tr height="25">
                        <td class="text2">Total Run Kms</td>
                        <td class="text2"><label><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalRunKms}" /></label></td>
                        <td class="text2">Total Run Hours</td>
                        <td class="text2"><label><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalRunHms}" /></label></td>
                    </tr>

                    <tr height="25">
                        <td class="text1">Trip Settled Amount</td>
                        <td class="text1"><label><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${tripAmount}" /></label></td>
                        <td class="text1">Vehicle Advance Paid </td>
                        <td class="text1"><label><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${advAmount}" /></label></td>

                    </tr>

                    <tr height="25">
                        <td class="text2">Total Idle Bhatta</td>
                        <td class="text2"><label><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${bhattaAmount}" /></label></td>
                        <td class="text2">Last Settled Balance Amount</td>
                        <td class="text2"><label><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${driverLastBalanceAmount}" /></label></td>


                    </tr>
                    <tr height="25">
                        <td class="text1">Uncleared Amount </td>
                        <td class="text1"><input type="hidden" name="totalUnclearedAmount" id="totalUnclearedAmount" value="<c:out value="${totalUnclearedAmount}"/>"/><c:out value="${totalUnclearedAmount}"/></td>
                        <td class="text1">Settle Amount</td>
                        <td class="text1"><label>
                                <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${tripAmount+advAmount+bhattaAmount+driverLastBalanceAmount-totalUnclearedAmount}" />
                                <input type="hidden" name="settleAmount" id="settleAmount" value="<c:out value="${tripAmount+advAmount+bhattaAmount+driverLastBalanceAmount-totalUnclearedAmount}"/>"  />
                            </label></td>

                    </tr>

                    <script>
                    function calculatePayAmount(){
                        var settleAmount = document.getElementById("settleAmount").value;
                        if(document.getElementById("primaryDriverId").value != 0){
                            if(settleAmount < 0){
                                document.getElementById("payAmount").value = 0;
                                document.getElementById("balanceAmount").value =  parseFloat(settleAmount).toFixed(2);
                            }else{
                                var payAmount =  document.getElementById("payAmount").value;
                                if(payAmount != ''){
                                if(parseFloat(payAmount) <= parseFloat(settleAmount)){
                                    document.getElementById("balanceAmount").value = (parseFloat(settleAmount) - parseFloat(payAmount)).toFixed(2);
                                }else{
                                    alert("Pay amount should less than the settle amount");
                                    document.getElementById("payAmount").value = "";
                                    document.getElementById("payAmount").focus();
                                }
                                }
                            }
                        }else{

                        }
                                
                    }
                    </script>

                    <tr height="25">
                        <td class="text2">Pay Amount</td>
                        <td class="text2"><input type="text" name="payAmount" id="payAmount" onchange="calculatePayAmount()" /></td>
                        <td class="text2">Balance Amount </td>
                        <td class="text2"><input type="text" name="balanceAmount" id="balanceAmount" value=""/></td>
                    </tr>
                    <tr>
                        <td class="text1">DICT Account</td>
                        <td class="text1">
                            <select name="paymentMode" id="paymentMode">
                                <option value="Credit">Credit</option>
                                <option value="Debit">Debit</option>
                              
                            </select>
                        </td>
                        <td class="text1">Remarks for Extra Expenses</td>
                        <td class="text1"><textarea name="settlementRemarks" id="settlementRemarks" cols="40" rows=""></textarea></td>
                    </tr>
                </table>
                <br/>
                <br/>
                <center>
                    <input type="button" class="button" name="save" id="save" value="save" onclick="submitPage();"/>
                </center>

                <br/>
                <br/>


            </div>
        </div>
        <script>
        $(".nexttab").click(function() {
            var selected = $("#tabs").tabs("option", "selected");
            $("#tabs").tabs("option", "selected", selected + 1);
        });
        </script>
        <%--
             </c:if>
     <c:if test="${settlementTripsSize == 0 || driverIdleBhattaSize == 0}">
                 <center>
                     <font color="red">No Records Found</font>
                 </center>
             </c:if> --%>
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>