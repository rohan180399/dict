
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
    function submitPage(val) {
        if (val == 'add') {
            document.manageDepartment.action = "/throttle/addDepartmentPage.do";
            document.manageDepartment.submit();
        } else if (val == 'alter') {
            document.manageDepartment.action = "/throttle/viewAlterDept.do";
            document.manageDepartment.submit();
        }
    }
</script>



<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
            setValues();
            getVehicleNos();">
    </c:if>

    <!--	  <span style="float: right">
                    <a href="?paramName=en">English</a>
                    |
                    <a href="?paramName=ar">???????
    </a>
              </span>-->
<style>
    #index td {
        color:white;
    }
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="hrms.label.Department" text="default text"/></h2>
    <!--    <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="default text"/>:</span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.HRMS" text="default text"/></a></li>
                <li class="active"><spring:message code="hrms.label.Department" text="default text"/></li>
            </ol>
        </div>-->
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">


            <body onload="setImages(1, 1, 0, 0, 0, 0);
                    document.manageDepartment.name.focus();">
                <form name="manageDepartment" action="file:///F|/Tomcat%205.5/webapps/throttle/content/Department/addDepartment.do" method="post" >
                    <%@ include file="/content/common/message.jsp" %>
                    <table align="right" width="8%">
                       
                    </table>
                    <br><br>
                    <c:if test = "${DepartmentLists != null}" >
                        <table class="table table-dark mb30 table-hover" id="table" >
                            <thead>
                                <tr align="center" height="43">
                                    <th width="60" align="center" class="contenthead" scope="col"><center><spring:message code="hrms.label.SNo" text="default text"/></center></th>
                                    <th width="92" align="center" class="contenthead" scope="col"><center><spring:message code="hrms.label.Name" text="default text"/></center></th>
                                    <th width="83" align="center" class="contenthead" scope="col"><center><spring:message code="hrms.label.Description" text="default text"/></center></th>
                                    <th width="67" align="center" class="contenthead" scope="col"><center>To Mail ID</center></th>
                                    <th width="67" align="center" class="contenthead" scope="col"><center>CC Mail ID</center></th>
                                    <th width="67" align="center" class="contenthead" scope="col"><center><spring:message code="hrms.label.Status" text="default text"/></center></th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int index = 0;%>
                                <c:forEach items="${DepartmentLists}" var="department">
                                    <%
                        String classText = "";
                        int oddEven = index % 2;
                        if (oddEven > 0) {
                            classText = "text2";
                        } else {
                            classText = "text1";
                        }
                                    %>
                                    <tr  width="208" height="38" align="center">
                                        <td align="center" class="<%=classText %>" height="30"><%= index + 1  %></td>
                                        <td align="center" class="<%=classText %>" height="30"><c:out value="${department.name}"/></td>
                                        <td align="center" class="<%=classText %>" height="30"><c:out value="${department.description}"/></td>
                                        <td align="center" class="<%=classText %>" height="30"><c:out value="${department.toMail}"/></td>
                                        <td align="center" class="<%=classText %>" height="30"><c:out value="${department.ccMail}"/></td>
                                        <td align="center" class="<%=classText %>" height="30">
                                            <c:if test="${(department.status=='y') || (department.status=='Y')}" >
                                                <i style="font-size:20px;color:green" class="glyphicon glyphicon-ok-circle"></i>
                                            </c:if>
                                            <c:if test="${(department.status=='n') || (department.status=='N')}" >
                                                <i style="font-size:20px;color:red" class="glyphicon glyphicon-remove-circle"></i>
                                            </c:if>
                                        </td>
                                    </tr>
                                    <%
                        index++;
                                    %>
                                </c:forEach>
                            </c:if>
                        </tbody>
                    </table>
                     <center><input type="button" class="btn btn-success" value="<spring:message code="hrms.label.Add" text="default text"/>"  onclick="submitPage('add')"  /> &nbsp;&nbsp;<input type="button" class="btn btn-success" value="<spring:message code="hrms.label.ALTER" text="default text"/>" onClick="submitPage('alter');"/></center>
 

                    <!--<center><input type="button" class="btn btn-primary" value="<spring:message code="hrms.label.Add" text="default text"/>"  onclick="submitPage('add')"  /> &nbsp;&nbsp;<input type="button" class="btn btn-primary" value="<spring:message code="hrms.label.ALTER" text="default text"/>" onClick="submitPage('alter');"/></center>-->
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <br><br>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span><spring:message code="hrms.label.EntriesPerPage" text="default text"/></span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text"><spring:message code="hrms.label.DisplayingPage" text="default text"/> <span id="currentpage"></span><spring:message code="hrms.label.of" text="default text"/>  <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 1);
                    </script>

                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
