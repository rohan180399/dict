

<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
  <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
  <%@ page import="java.util.*" %>
</head>

<script>
function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}


function searchSubmit(){
    
    if(document.serviceCost.fromDate.value==''){
        alert("Please Enter From Date");
        return;
    }else if(document.serviceCost.toDate.value==''){
    alert("Please Enter to Date");
    return;
} 
document.serviceCost.action = '/throttle/taxWisePODetails.do'
document.serviceCost.submit();    

}    

 function setValues(){            
            if('<%=request.getAttribute("fromDate")%>'!='null'){
            document.serviceCost.fromDate.value ='<%=request.getAttribute("fromDate")%>';
            document.serviceCost.toDate.value ='<%=request.getAttribute("toDate")%>';            
            }if('<%=request.getAttribute("custId")%>' != 'null'){
            document.serviceCost.custId.value='<%=request.getAttribute("custId")%>';
            }
            }
  
    function printPage()
    {       
        var DocumentContainer = document.getElementById("printPage");
        var WindowObject = window.open('', "TrackHistoryData", 
            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        //WindowObject.close();   
    }     



    </script>
    
    <body onload="setValues();">
        
        <form name="serviceCost"  method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>



  <table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:850;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">TaxWise Vehicle Purchase Report</li>
    </ul>
    <div id="first">
    <table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
  <tr>
        <td height="30">Vendor</td>
        <td height="30">
            <select class="form-control" name="custId"  style="width:125px;">
                <option value="">---Select---</option>
                <c:if test = "${vendorList != null}" >
                    <c:forEach items="${vendorList}" var="cust">
                        <option value='<c:out value="${cust.vendorId}" />'><c:out value="${cust.vendorName}" /></option>
                    </c:forEach >
                </c:if>
            </select>
        </td>

        <td height="30">From Date </td>
        <td height="30"><input type="text" name="fromDate" class="form-control" ><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.serviceCost.fromDate,'dd-mm-yyyy',this)"/> </td>
        <td height="30">To Date </td>
        <td height="30"><input name="toDate" type="text" class="form-control" ><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.serviceCost.toDate,'dd-mm-yyyy',this)"/></td>
        <td><input type="button" class="button" readonly name="search" value="search" onClick="searchSubmit();" ></td>
    </tr>
                
    </table>
    </div></div>
    </td>
    </tr>
    </table>
            
            <br>
            <% int index = 0;
            String classText = "";
            int oddEven = 0;
            %>    
            <c:if test = "${serviceCostList != null}" >
                
<center> <input type="button" class="button" name="print" value="print" onClick="printPage();" > </center>
<br>
        <div id="printPage" >                
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="850" id="bg" class="border">
                    
                    <tr class="contenthead">
                        <td  class="contentsub">Sno</td>
                        <td  height="30" class="contentsub">PO No</td>
                        <td  height="30" class="contentsub">Vendor Name</td>
                        <td  height="30" class="contentsub">GRN No</td>
                        <td  height="30" class="contentsub">Invoice No</td>
                        <td  height="30" class="contentsub">Invoice Date</td>
                        <c:forEach items="${VatValues}" var="ser">                         
                        <td  height="30" class="contentsub">Vat@<c:out value="${ser.acd}"/></td>
                        <td  height="30" class="contentsub">Nett </td> 
                         </c:forEach>                        
                        <td  height="30" class="contentsub"> Total</td>                        
                        
                    </tr>
                    
                    <c:set var="totJc" value="0" />
                    
                    <c:forEach items="${serviceCostList}" var="service">    
                        <%

            oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        
            
                        <tr>
                            <td class="<%=classText %>" height="30"> <%= index+1 %> </td>
                            <td class="<%=classText %>" height="30"><c:out value="${service.poId}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${service.vendorName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${service.supplyId}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${service.invoiceNo}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${service.invoiceDate}"/></td>                                                    
                             <c:forEach items="${service.vatValues}"  var="vat">
                                   <td class="<%=classText %>" height="30" ><fmt:formatNumber value="${vat.taxAmount}" pattern="##.00"/></td>
                            <td class="<%=classText %>" height="30"  ><c:out value="${vat.amount}"/></td>
                           
                            </c:forEach>                                                     
                            <td class="<%=classText %>" height="30" ><c:out value="${service.totalAmount}"/></td>    
                        </tr>
                        
                        
                        <%
            index++;
                        %>
                    </c:forEach>
                    
                </table>

<style>
.text1 {
font-family:Tahoma;
font-size:12px;
color:#333333;
background-color:#E9FBFF;
padding-left:10px;
}
.text2 {
font-family:Tahoma;
font-size:12px;
color:#333333;
background-color:#FFFFFF;
padding-left:10px;
}        
.contentsub {
background-image:url(/throttle/images/button.gif);
background-repeat:repeat-x;
height:15px;
font-family:Tahoma;
padding-left:5px;
font-size:11px;
font-weight:bold;
color:#0070D4;
text-align:left;
padding-bottom:8px;
}

.contenthead {
background-image:url(/throttle/images/button.gif);
background-repeat:repeat-x;
height:15px;
font-family:Tahoma;
padding-left:5px;
font-size:11px;
font-weight:bold;
color:#0070D4;
text-align:center;
padding-bottom:8px;
}

</style>
</div>
               <!-- 
                <br>
                 <table  align="center" border="0" cellpadding="0" cellspacing="0" width="500" id="bg" class="border">
                     <tr>
                     <td class="text1" height="30"> Total Jobcards </td>
                     <td class="text1" height="30"> <b><c:out value="${totJc}"/> </b> </td>
                     </tr>
                     <tr>
                     <td class="text1" height="30"> Spare Amount </td>
                     <td class="text1" height="30"> <fmt:setLocale value="en_US" /><b>SAR: <fmt:formatNumber value="${spareTotal}" pattern="##.00"/></b> </td>
                     </tr>
                     <tr>
                     <td class="text1" height="30"> Labour Amount </td>
                     <td class="text1" height="30"> <fmt:setLocale value="en_US" /><b>SAR: <fmt:formatNumber value="${laborTotal}" pattern="##.00"/></b></td>
                     </tr>
                     <tr>
                     <td class="text1" height="30"> Contract Amount </td>
                     <td class="text1" height="30"> <fmt:setLocale value="en_US" /><b>SAR: <fmt:formatNumber value="${contractTotal}" pattern="##.00"/></b></td>
                     </tr>
                     <tr>
                     <td class="text1" height="30"> Total Amount </td>
                     <td class="text1" height="30"> <fmt:setLocale value="en_US" /><b>SAR: <fmt:formatNumber value="${spareTotal + laborTotal + contractTotal}" pattern="##.00"/></b></td>
                     </tr>

                 </table>                     -->
            </c:if>
             
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
        
        
    </body>
</html>
