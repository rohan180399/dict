<%--
    Document   : vehicleDriverAdvance
    Created on : Dec 30, 2013, 01:59:08 PM
    Author     : Madhan
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>
<script>

    var httpRequest;
    function checkDriverAdvance(vehicleId) {
        if (vehicleId != '') {
            var url = '/throttle/checkVehicleDriverAdvance.do?vehicleId=' + vehicleId;
            if (window.ActiveXObject) {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest) {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("GET", url, true);
            httpRequest.onreadystatechange = function() {
                processRequest();
            };
            httpRequest.send(null);
        }
    }
    function processRequest() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var val = httpRequest.responseText.valueOf();
                //alert(val.trim());
                if (val.trim() !== "") {
                    var temp = val.split("~");
                    //alert('am here 0');
                    if(temp[1] != '' && parseInt(temp[1])  > 0){
                        document.getElementById("mappingId").value = temp[0];
                        document.getElementById("primaryDriverId").value = temp[1];
                        document.getElementById("primaryDriver").value = temp[2];
                        document.getElementById("secondaryDriverIdOne").value = temp[3];
                        document.getElementById("secondaryDriverOne").value = temp[4];
                        document.getElementById("secondaryDriverIdTwo").value = temp[5];
                        document.getElementById("secondaryDriverTwo").value = temp[6];
                        if(temp[7] == '0'){
                            $("#tripStatus").text("");
                            $("#tripStatusShow").hide();
                        }else if(temp[7] != '0'){
                            //alert(temp[7]);
                            $("#tripStatusShow").show();
                            if(temp[8] == 8){
                            //alert(temp[8]);
                            $("#tripStatus").text('Now Vehicle is assigned to a trip (pre-start status) and trip sheet no is '+temp[7]);
                            }else if(temp[8] == 9){
                            $("#tripStatus").text('Now Vehicle is assigned to a trip (start status) and trip sheet no is '+temp[7]);
                            }else if(temp[8] == 10){
                            $("#tripStatus").text('Now Vehicle is assigned to a trip (in progress status) and trip sheet no is '+temp[7]);
                            }
                        }
                    }else{
                        //alert('am here 1');
                        $("#tripStatusShow").show();
                        $("#tripStatus").text('No Driver Mapping to the chosen vehicle no '+$('#vehicleNo').val());
                        $('#vehicleNo').val('');
                        $('#vehicleId').val('');
                    }
                } else {
                    document.getElementById("mappingId").value = 0;
                    document.getElementById("primaryDriverId").value = '';
                    document.getElementById("primaryDriver").value = '';
                    document.getElementById("secondaryDriverIdOne").value = '';
                    document.getElementById("secondaryDriverOne").value = '';
                    document.getElementById("secondaryDriverIdTwo").value = '';
                    document.getElementById("secondaryDriverTwo").value = '';
                    $("#tripStatus").text("");
                }
            } else {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }


    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#vehicleNo').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getVehicleAdvanceVehicleNo.do",
                    dataType: "json",
                    data: {
                        vehicleNo: request.term,
                        usageTypeId: $("#usageTypeId").val()
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if(items == ''){
                        alert("Invalid Vehicle No");
                        $('#vehicleNo').val('');
                        $('#vehicleId').val('');    
                        $('#vehicleNo').fous();
                        }else{
                        response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var id = ui.item.Id;
                $('#vehicleNo').val(value);
                $('#vehicleId').val(id);
                 $('#primaryDriver').focus();
                checkDriverAdvance(id);
                return false;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    });




   

    //savefunction
    function submitPage() {
        if (document.getElementById("vehicleNo").value == '' || document.getElementById("vehicleId").value == '') {
            alert("Please Select Valid Vehicle No");
            document.getElementById("vehicleNo").focus();
        }else if($("#tripStatus").text() != "") {
            alert("This Vehicle Should not be in Trip Pre-Start and Trip Start Service  ");
        }else if (document.getElementById("advanceAmount").value == '') {
            alert("Please Enter AdvanceAmount");
            document.getElementById("advanceAmount").focus();
        }else if (document.getElementById("advanceDate").value == '') {
            alert("Please select AdvanceDate");
            document.getElementById("advanceDate").focus();
        }else if (document.getElementById("primaryDriverId").value == '') {
            alert("Please Select Valid Primary Driver Name");
            document.getElementById("primaryDriver").focus();
        } else if (document.getElementById("expenseType").value == '0') {
            alert("Please select expense type");
            document.getElementById("expenseType").focus();
        } else if (document.getElementById("advanceRemarks").value == '') {
            alert("Please enter advance remarks");
            document.getElementById("advanceRemarks").focus();
        } else {
            $("#save").hide();
            document.vehicleDriverAdvance.action = '/throttle/saveVehicleDriverAdvance.do';
            document.vehicleDriverAdvance.submit();
        }
    }
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body onload="document.vehicleDriverAdvance.vehicleNo.focus()">
        <form name="vehicleDriverAdvance"  method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>

            <table width="980" align="center" class="table2" cellpadding="0" cellspacing="0">
<!--                <input type="hidden" name="stChargeId" id="stChargeId" value=""  />-->
                <tr>
                    <td class="contenthead" colspan="4" >Vehicle Driver Advance</td>
                </tr>
                <tr>
                    <td class="text1" colspan="4" align="center" id="tripStatusShow" style="display: none;"><label id="tripStatus" style="color: red"></label></td>
                </tr>
                <tr>
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Vehicle No</td>
                    <td class="text1"><input type="hidden" name="mappingId" id="mappingId" class="textbox"  ><input type="hidden" name="vehicleId" id="vehicleId" class="textbox"  ><input type="text" name="vehicleNo" id="vehicleNo" class="textbox" ></td>
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Advance Amount</td>
                    <td class="text1"><input type="text" name="advanceAmount" id="advanceAmount" class="textbox"  onKeyPress="return onKeyPressBlockCharacters(event);"></td>
                </tr>
                <tr>
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Advance Date</td>
                    <td class="text1"><input type="text" name="advanceDate" id="advanceDate" class="datepicker">
                    <td class="text1">&nbsp;&nbsp;&nbsp;&nbsp;Primary Driver</td>
                    <td class="text1"><input type="hidden" name="primaryDriverId" id="primaryDriverId" class="textbox"  ><input type="text"  name="primaryDriver" id="primaryDriver" class="textbox" onchange="setPrimaryDriverEmpty()" onKeyPress="return onKeyPressBlockNumbers(event);" readonly></td>
                </tr>
                <tr>
                    <td class="text1">&nbsp;&nbsp;&nbsp;&nbsp;Secondary Driver</td>
                    <td class="text1"><input type="hidden" name="secondaryDriverIdOne" id="secondaryDriverIdOne" class="textbox"  ><input type="text" name="secondaryDriverOne" id="secondaryDriverOne" class="textbox" onchange="setSecondaryDriverEmpty()" onKeyPress="return onKeyPressBlockNumbers(event);" readonly></td>
                    <td class="text1">&nbsp;&nbsp;&nbsp;&nbsp;Tertiary Driver</td>
                    <td class="text1"><input type="hidden" name="secondaryDriverIdTwo" id="secondaryDriverIdTwo" class="textbox"  ><input type="text" name="secondaryDriverTwo" id="secondaryDriverTwo" class="textbox" onchange="setTertiaryDriverEmpty()" onKeyPress="return onKeyPressBlockNumbers(event);" readonly></td>
                </tr>
                <tr>
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Expense Type</td>
                    <td class="text1"><select name="expenseType" id="expenseType" class="textbox">
                            <option value="0">--Select--</option>
                            <option value="Food">Food Allowances</option>
                            <option value="Repair">Repair&amp;Maintenance</option>
                        </select></td>
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Advance Remarks</td>
                    <td class="text1">
                        <textarea rows="3" cols="10"  name="advanceRemarks" id="advanceRemarks" class="textbox"></textarea>
                    </td>
                </tr>
                <tr>
                    <td class="text1" colspan="4" align="center">
                        <input type="button" class="button" value="Save" id="save" onClick="submitPage();"   />
                        <input type="hidden" class="button" value="<c:out value="${usageTypeId}"/>" id="usageTypeId" name="usageTypeId"   />
                    </td>
                </tr>
                
                <script>
                    function setPrimaryDriverEmpty(){
                       var primaryDriver = $("#primaryDriver").val(); 
                       var mappingId = $("#mappingId").val(); 
                       if(primaryDriver == ''){
                           $("#primaryDriverId").val('');
                       }
                    }
                    function setSecondaryDriverEmpty(){
                       var secondaryDriverOne = $("#secondaryDriverOne").val(); 
                       var mappingId = $("#mappingId").val(); 
                       if(secondaryDriverOne == ''){
                           $("#secondaryDriverIdOne").val('');
                       }
                    }
                    function setTertiaryDriverEmpty(){
                       var secondaryDriverTwo = $("#secondaryDriverTwo").val(); 
                       var mappingId = $("#mappingId").val(); 
                       if(secondaryDriverTwo == ''){
                           $("#secondaryDriverIdTwo").val('');
                       }
                    }
                </script>
            </table>
            <br>
            
            <table width="815" align="center" border="0" id="table" class="sortable">
                <thead>
                    <tr height="30">
                        <th><h3>S.No</h3></th>
                        <th><h3>Vehicle No </h3></th>
                        <th><h3>Advance Amount</h3></th>
                        <th><h3>Advance Date</h3></th>
                        <th><h3>Advance Remarks</h3></th>
                        <th><h3>Primary Driver</h3></th>
                        <th><h3>Secondary Driver</h3></th>
                        <th><h3>Tertiary Driver</h3></th>
                    </tr>
                </thead>
                <tbody>
                    <% int sno = 0;%>
                    <c:if test = "${statusList != null}">
                        <c:forEach items="${statusList}" var="vehicleDriver">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td class="<%=className%>"  align="left"> <%= sno + 1%> </td>
                                <td class="<%=className%>"  align="left"> <c:out value="${vehicleDriver.regNo}" /></td>
                                <td class="<%=className%>"  align="left"><c:out value="${vehicleDriver.advanceAmount}" /></td>
                                <td class="<%=className%>"  align="left"><c:out value="${vehicleDriver.advanceDate}" /></td>
                                <td class="<%=className%>"  align="left"><c:out value="${vehicleDriver.advanceRemarks}" /></td>
                                <td class="<%=className%>"  align="left"><c:out value="${vehicleDriver.primaryDriverName}" /></td>
                                <td class="<%=className%>"  align="left"><c:out value="${vehicleDriver.secondaryDriverNameOne}"/></td>
                                <td class="<%=className%>"  align="left"><c:out value="${vehicleDriver.secondaryDriverNameTwo}"/></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </c:if>
            </table>
            <input type="hidden" name="count" id="count" value="<%=sno%>" />
            <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");</script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>