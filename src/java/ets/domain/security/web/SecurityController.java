package ets.domain.security.web;

import ets.arch.web.BaseController;
import ets.domain.users.business.LoginBP;
import ets.domain.util.FPLogUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.ModelAndView;
import java.util.ArrayList;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.racks.business.RackBP;
import ets.domain.security.business.SecurityBP;
import ets.domain.operation.business.OperationBP;
import ets.domain.security.business.SecurityTO;
import ets.domain.util.ParveenErrorConstants;
//import ets.domain.vendor.business.VendorBP;
import ets.domain.vehicle.business.VehicleBP;
//import ets.domain.vendor.business.VendorTO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;   

public class SecurityController extends BaseController {

    SecurityCommand securityCommand;
    OperationBP operationBP;
    LoginBP loginBP;
    SecurityBP securityBP;
    RackBP rackBP;
    VehicleBP vehicleBP;

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    public RackBP getRackBP() {
        return rackBP;
    }

    public void setRackBP(RackBP rackBP) {
        this.rackBP = rackBP;
    }

    public SecurityBP getSecurityBP() {
        return securityBP;
    }

    public void setSecurityBP(SecurityBP securityBP) {
        this.securityBP = securityBP;
    }

    public SecurityCommand getSecurityCommand() {
        return securityCommand;
    }

    public void setSecurityCommand(SecurityCommand securityCommand) {
        this.securityCommand = securityCommand;
    }

    public VehicleBP getVehicleBP() {
        return vehicleBP;
    }

    public void setVehicleBP(VehicleBP vehicleBP) {
        this.vehicleBP = vehicleBP;
    }

    public OperationBP getOperationBP() {
        return operationBP;
    }

    public void setOperationBP(OperationBP operationBP) {
        this.operationBP = operationBP;
    }

      protected void bind(HttpServletRequest request, Object command) throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        System.out.println("request.getRequestURI() = " + request.getRequestURI());
        HttpSession session = request.getSession();
//        FacesContext context = FacesContext.getCurrentInstance();
//       HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
//        if (request.getSession().isNew()) {
//        System.out.println("request.getSession().isNew() = " + request.getSession().isNew());
//        }
//        else {
//        System.out.println("request.getSession().isold() = " + request.getSession().isNew());
        int userId = (Integer) session.getAttribute("userId");
//
        System.out.println("userId" + userId);
//        System.out.println("loginRecordId" + loginRecordId);
        String getUserFunctionsActive = "";
        String uri = request.getRequestURI();
//        getUserFunctionsActive = loginBP.getUserFunctionsActive(userId, uri);
        if (getUserFunctionsActive != "" && getUserFunctionsActive != null) {
            String temp[] = getUserFunctionsActive.split("~");
            System.out.println("function Id" + temp[0]);
            System.out.println("function Name  " + temp[0]);
            int status1 = 0;
            int functionId = Integer.parseInt(temp[0]);
            String functionName = temp[1];
//            status1 = loginBP.insertUserFunctionAccessDetails(functionId, functionName, userId, loginRecordId);
        }
//        }
        binder.closeNoCatch();
          initialize(request);
    }

    /**
     * This method used to View Vendor Page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView manageSecurityPage(HttpServletRequest request, HttpServletResponse response, SecurityCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        securityCommand = command;
        SecurityTO securityTO = new SecurityTO();
        String menuPath = "Manage Security  >>  View ";
        path = "content/security/manageSecurity.jsp";
        String currentDate = "";
        int userId = (Integer) session.getAttribute("userId");
        
        try {
//            if (!loginBP.checkAuthorisation(userFunctions, "Security-View", userId, loginRecordId)) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {

                if (securityCommand.getVehicleFromDate() != null && securityCommand.getVehicleFromDate() != "") {
                    securityTO.setVehicleFromDate(securityCommand.getVehicleFromDate());
                } else {
                    currentDate = (String) session.getAttribute("currentDate");
                    System.out.println("currentDate:" + currentDate);
                    securityTO.setVehicleFromDate(currentDate);
                }
                if (securityCommand.getVehicleToDate() != null && securityCommand.getVehicleToDate() != "") {
                    securityTO.setVehicleToDate(securityCommand.getVehicleToDate());
                } else {
                    securityTO.setVehicleToDate(securityTO.getVehicleFromDate());
                }
                if (securityCommand.getVehicleInPurpose() != null && securityCommand.getVehicleInPurpose() != "") {
                    securityTO.setVehicleInPurpose(securityCommand.getVehicleInPurpose());
                }else{
                    securityTO.setVehicleInPurpose("0");

                }
                if (securityCommand.getVno() != null && securityCommand.getVno() != "") {
                    securityTO.setVno(securityCommand.getVno());
                }
                if (securityCommand.getVehicleStatus() != null && securityCommand.getVehicleStatus() != "") {
                    securityTO.setVehicleStatus(securityCommand.getVehicleStatus());
                } else {
                    securityTO.setVehicleStatus("0");
                }
                String vehicleStatus = securityTO.getVehicleStatus();
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

                String pageTitle = "View Security";
                request.setAttribute("pageTitle", pageTitle);
                String status;

                //get data for purpose list start
                ArrayList vehicleInPurposeList = new ArrayList();
                vehicleInPurposeList = securityBP.getVehicleInPurposeList();
                request.setAttribute("VehicleInPurpose", vehicleInPurposeList);
                //get data for purpose list end

                int compId = Integer.parseInt((String) session.getAttribute("companyId"));

                //get data for purpose list start
                ArrayList vehicleList = new ArrayList();
                vehicleList = securityBP.getVehicleList(compId, securityTO);

                request.setAttribute("vehicleFromDate", securityTO.getVehicleFromDate());
                request.setAttribute("vehicleToDate", securityTO.getVehicleToDate());
                request.setAttribute("vehicleInPurpose", securityTO.getVehicleInPurpose());
                request.setAttribute("vehicleStatus", vehicleStatus);
                request.setAttribute("vno", securityTO.getVno());

                request.setAttribute("vehicleList", vehicleList);
                System.out.println("vehicleList" + vehicleList.size());
            //get data for purpose list end               

//            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Security data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

// manageSaveSecurity 
    /**
     * This method used to Modify Designation Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView manageSaveSecurity(HttpServletRequest request, HttpServletResponse response, SecurityCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        ModelAndView mv = null;
        String path = "";
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        securityCommand = command;

        path = "content/security/manageSecurity.jsp";

        try {
//            if (!loginBP.checkAuthorisation(userFunctions, "Security-Save", userId, loginRecordId)) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                String menuPath = "HRMS  >>  manageSaveSecurity    ";
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

                int index = 0;
                int modify = 0;
                String[] woIds = securityCommand.getWoIds();
                String[] vehicleNumbers = securityCommand.getVehicleNumbers();
                String[] in = securityCommand.getIn();
                String[] inKm = securityCommand.getInKm();
                String[] inHm = securityCommand.getInHm();
                String[] out = securityCommand.getOut();
                String[] remarks = securityCommand.getVehicleRemarks();

                String[] compId = securityCommand.getCompId();
                String[] purposeId = securityCommand.getPurposeId();
                String[] vioId = securityCommand.getVioId();

                int UserId = (Integer) session.getAttribute("userId");
                System.out.println("userId" + UserId);
                ArrayList List = new ArrayList();
                SecurityTO securityTO = null;
                System.out.println("IN manageSaveSecurity");

                if (in != null) {
                    for (int i = 0; i < in.length; i++) {
                        securityTO = new SecurityTO();
                        index = Integer.parseInt(in[i]);
                        System.out.println("index" + index);
                        securityTO.setWoId(woIds[index]);
                        securityTO.setVehicleNumber(vehicleNumbers[index]);
                        securityTO.setKm(inKm[index]);
                        securityTO.setHourMeter(Integer.parseInt(inHm[index]));
                        securityTO.setCompId(compId[index]);
                        securityTO.setPurposeId(Integer.parseInt(purposeId[index]));
                        securityTO.setRemarks(remarks[index]);
                        System.out.println("vehicleNumbers[index]=" + vehicleNumbers[index]);
                        System.out.println("inKm[index]=" + inKm[index]);
                        System.out.println(" inHm[index]=" + inHm[index]);
                        System.out.println("operationBP.getVehicleId( vehicleNumbers[index]=" + operationBP.getVehicleId(vehicleNumbers[index]));
                        vehicleBP.vehicleKmUpdate(operationBP.getVehicleId(vehicleNumbers[index]), inKm[index], inHm[index], userId);
                        List.add(securityTO);
                    }
                    modify = securityBP.processSaveVehicle(List, UserId);
                }

                //security page jsp???
                    String[] activeInd = request.getParameterValues("activeInd");
                    String status = request.getParameter("status1");
                    if(status.equals("1")){
                    for (int j = 0; j < activeInd.length; j++) {
                        securityTO = new SecurityTO();
                        if(activeInd[j].equals("Y")){
                        System.out.println("index" + j);
                        securityTO.setVioId(Integer.parseInt(vioId[j]));
                        securityTO.setRemarks(remarks[j]);

                        List.add(securityTO);
                        }
                    }
                    modify = securityBP.processSaveVehicleOutTime(List, UserId);
            }


                 mv = manageSecurityPage(request, response, command);


                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Update Successful");
//            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve designation --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
      return mv;
    }

    /**
     * This method used to View Vendor Page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView manageAddSecurity(HttpServletRequest request, HttpServletResponse response, SecurityCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        securityCommand = command;
        SecurityTO securityTO = new SecurityTO();
        String menuPath = "Manage Security  >>  Add ";
        path = "content/security/addVehicleSecurity.jsp";
        int userId = (Integer) session.getAttribute("userId");
        
        try {
//            if (!loginBP.checkAuthorisation(userFunctions, "Security-Add", userId, loginRecordId)) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                int compId = Integer.parseInt((String) session.getAttribute("companyId"));

                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                String pageTitle = "Add Vehicle Entry";
                request.setAttribute("pageTitle", pageTitle);
                //get data for purpose list start
                ArrayList vehicleInPurposeList = new ArrayList();
                vehicleInPurposeList = securityBP.getVehicleInPurposeList();
                request.setAttribute("VehicleInPurpose", vehicleInPurposeList);
                //get data for purpose list end

                //get data for operation list start
                ArrayList opList = new ArrayList();
                opList = securityBP.getOpList();
                request.setAttribute("opList", opList);
            //get data for operation list end                

//            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Security data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //handleAddVehicleSecurity
    /**
     * This method used to Insert Parts Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAddVehicleSecurity(HttpServletRequest request, HttpServletResponse response, SecurityCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        securityCommand = command;
        ModelAndView mv = null;
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        try {
            path = "content/security/manageSecurity.jsp";
            String menuPath = "Security  >>  Add ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            SecurityTO securityTO = new SecurityTO();
            int insertStatus = 0;
            securityTO.setWoId("0");
            securityTO.setVehicleNumber(securityCommand.getVno());
            securityTO.setPurposeId(Integer.parseInt(securityCommand.getVehicleInPurpose()));
            securityTO.setCompId(securityCommand.getVehicleOperationPointId());
            securityTO.setKm(securityCommand.getVehicleKm());
            securityTO.setHourMeter(Integer.parseInt(securityCommand.getVehicleHm()));
            securityTO.setRemarks(securityCommand.getRemarks());
            ArrayList List = new ArrayList();
            String currentDate = "";
            List.add(securityTO);

            int modify = securityBP.processSaveVehicle(List, userId);
            
                int compId = 0;


                 if (securityCommand.getVehicleFromDate() != null && securityCommand.getVehicleFromDate() != "") {
                    securityTO.setVehicleFromDate(securityCommand.getVehicleFromDate());
                } else {
                    currentDate = (String) session.getAttribute("currentDate");
                    System.out.println("currentDate:" + currentDate);
                    securityTO.setVehicleFromDate(currentDate);
                }
                if (securityCommand.getVehicleToDate() != null && securityCommand.getVehicleToDate() != "") {
                    securityTO.setVehicleToDate(securityCommand.getVehicleToDate());
                } else {
                    securityTO.setVehicleToDate(securityTO.getVehicleFromDate());
                }
                if (securityCommand.getVehicleInPurpose() != null && securityCommand.getVehicleInPurpose() != "") {
                    securityTO.setVehicleInPurpose(securityCommand.getVehicleInPurpose());
                }else{
                    securityTO.setVehicleInPurpose("0");

                }
                    securityTO.setVno("");

                if (securityCommand.getVehicleStatus() != null && securityCommand.getVehicleStatus() != "") {
                    securityTO.setVehicleStatus(securityCommand.getVehicleStatus());
                } else {
                    securityTO.setVehicleStatus("0");
                }

                ArrayList vehicleInPurposeList = new ArrayList();
                vehicleInPurposeList = securityBP.getVehicleInPurposeList();
                request.setAttribute("VehicleInPurpose", vehicleInPurposeList);

                 ArrayList vehicleList = new ArrayList();
                vehicleList = securityBP.getVehicleList(compId, securityTO);
                request.setAttribute("vehicleList", vehicleList);

                request.setAttribute("vehicleFromDate", securityTO.getVehicleFromDate());
                request.setAttribute("vehicleToDate", securityTO.getVehicleToDate());
                request.setAttribute("vehicleInPurpose", "0");
                request.setAttribute("vehicleStatus", "0");
                request.setAttribute("vno", "");
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Add Successful");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to View Vendor Page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleSearchSecurity(HttpServletRequest request, HttpServletResponse response, SecurityCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        securityCommand = command;
        SecurityTO securityTO = new SecurityTO();
        String menuPath = "Manage Security  >>  View ";
        path = "content/security/manageSecurity.jsp";
        int userId = (Integer) session.getAttribute("userId");
        
        try {
//            if (!loginBP.checkAuthorisation(userFunctions, "Security-View", userId, loginRecordId)) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                System.out.println("securityCommand.getVehicleDate()" + securityCommand.getVehicleDate());
                System.out.println("i am in manageSecurityPage ");
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

                String pageTitle = "View Security";
                request.setAttribute("pageTitle", pageTitle);
                String status;

                // get values from DB to display in manage Screen
                status = securityBP.processGetVendorList();
                System.out.println("Got current Date " + status);
                //request.setAttribute("securityList", securityList);

                // To get workorder date List
                ArrayList workorderDate = new ArrayList();
                int compId = (Integer) session.getAttribute("companyId");
                workorderDate = securityBP.processWorkorderDate(compId);
                System.out.println("workorderDateu" + workorderDate.size());
                request.setAttribute("GetWoDetailUnique", workorderDate);

                ArrayList dateList = new ArrayList();
                dateList = securityBP.processDateList();
                request.setAttribute("DateList", dateList);
                System.out.println("list Size" + dateList.size());

                ArrayList outList = new ArrayList();
                outList = securityBP.processOutList();
                request.setAttribute("OutList", outList);
                System.out.println("outList Size" + outList.size());

                ArrayList inAndOut = new ArrayList();
                inAndOut = securityBP.processInAndOut();
                request.setAttribute("InAndOut", inAndOut);
                System.out.println("inoutList Size" + inAndOut.size());

                workorderDate.addAll(inAndOut);
                System.out.println("after Size" + workorderDate.size());
                request.setAttribute("Alldata", workorderDate);

//            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Security data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void isThisVehicleIn(HttpServletRequest request, HttpServletResponse response, SecurityCommand command) throws IOException, FPBusinessException {
        System.out.println("i am in ajax ");
        HttpSession session = request.getSession();

        String regNo = request.getParameter("regNo");
        System.out.println("regNo" + regNo);
        String suggestions = "";
        suggestions = securityBP.isThisVehicleIn(regNo);
        System.out.println("suggestions" + suggestions);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.print(suggestions);
        writer.close();

    }
}
