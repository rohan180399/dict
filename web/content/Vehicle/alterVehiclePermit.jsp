<%-- 
    Document   : alterVehiclePermit
    Created on : Sep 26, 2012, 11:02:21 AM
    Author     : entitle
--%>

<%@page contentType="text/html" import="java.sql.*,java.text.DecimalFormat" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <head>
        <title>Vehicle Insurance Update</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                //alert('hai');
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function() {
                //	alert("cv");
                $( ".datepicker" ).datepicker({

                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });
        </script>

        <script language="javascript">

            function show_src() {
                document.getElementById('exp_table').style.display='none';
            }
            function show_exp() {
                document.getElementById('exp_table').style.display='block';
            }
            function show_close() {
                document.getElementById('exp_table').style.display='none';
            }



            function validate(){
                var errMsg = "";
                if(document.VehicleInsurance.regNo.value){
                    errMsg = errMsg+"Vehicle Registration No is not filled\n";
                }
                if(document.VehicleInsurance.insuranceCompanyName.value){
                    errMsg = errMsg+"Insurance Company is not filled\n";
                }
                if(document.VehicleInsurance.premiumNo.value){
                    errMsg = errMsg+"Premium No is not filled\n";
                }
                if(document.VehicleInsurance.premiumPaidAmount.value){
                    errMsg = errMsg+"Premium paid Amount is not filled\n";
                }
                if(document.VehicleInsurance.premiumPaidDate.value){
                    errMsg = errMsg+"Premium paid Date is not filled\n";
                }
                if(document.VehicleInsurance.vehicleValue.value){
                    errMsg = errMsg+"Vehicle Value is not filled\n";
                }
                if(document.VehicleInsurance.premiumExpiryDate.value){
                    errMsg = errMsg+"Premium expiry Date is not filled\n";
                }
                if(errMsg != "") {
                    alert(errMsg);
                    return false;
                }else {
                    return true;
                }
            }


            var httpRequest;
            function getVehicleDetails(regNo)
            {

                if(regNo != "") {
                    var url = "/throttle/getVehicleDetailsInsurance.do?regNo1="+ regNo;
                    url = url+"&sino="+Math.random();
                    if (window.ActiveXObject)  {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)  {
                        httpRequest = new XMLHttpRequest();
                    }
                    httpRequest.open("GET", url, true);
                    httpRequest.onreadystatechange = function() { processRequest(); } ;
                    httpRequest.send(null);
                }
            }


            function processRequest()
            {
                if (httpRequest.readyState == 4)  {

                    if(httpRequest.status == 200) {
                        if(httpRequest.responseText.valueOf()!=""){
                            var detail = httpRequest.responseText.valueOf();
                            var vehicleValues = detail.split("~");
                            document.VehicleInsurance.vehicleId.value = vehicleValues[0]
                            document.VehicleInsurance.chassisNo.value = vehicleValues[2]
                            document.VehicleInsurance.engineNo.value = vehicleValues[2]
                            document.VehicleInsurance.vehicleMake.value = vehicleValues[4]
                            document.VehicleInsurance.vehicleModel.value = vehicleValues[5]

                        }
                    }
                    else
                    {
                        alert("Error loading page\n"+ httpRequest.status +":"+ httpRequest.statusText);
                    }
                }
            }
        </script>

    </head>

    <body>
        <form name="VehicleInsurance" action="/throttle/alterVehiclePermit.do" >
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <h2 align="center">Vehicle Permit Details</h2>
            <table width="800" align="center" class="table2" cellpadding="0" cellspacing="0">

                <tr>
                    <td class="contenthead" colspan="4">Vehicle Details</td>
                </tr>
                <c:if test="${vehicleDetail != null}">
                    <c:forEach items="${vehicleDetail}" var="VDet" >
                        <tr>
                            <td class="texttitle1">Vehicle No</td>
                            <td class="text1"><input type="text" name="regNo" id="regNo" class="form-control" value='<c:out value="${VDet.regno}" />' readonly ><input type="hidden" name="vehicleId" id="vehicleId" value='<c:out value="${VDet.vehicleId}" />' readonly ></td>
                            <td class="texttitle1">Make</td>
                            <td class="text1"><input type="text" name="vehicleMake" id="vehicleMake" class="form-control" value='<c:out value="${VDet.mfrName}" />' readonly ></td>
                        </tr>
                        <tr>
                            <td class="texttitle2">Model</td>
                            <td class="text2"><input type="text" name="vehicleModel" id="vehicleModel" class="form-control" value='<c:out value="${VDet.modelName}" />' readonly ></td>
                            <td class="texttitle2">Usage</td>
                            <td class="text2"><input type="text" name="vehicleUsage" id="vehicleUsage" class="form-control" readonly></td>
                        </tr>
                        <tr>
                            <td class="texttitle1">Engine No</td>
                            <td class="text1"><input type="text" name="engineNo" id="engineNo" class="form-control" value='<c:out value="${VDet.engineNo}" />' readonly ></td>
                            <td class="texttitle1">Chassis No</td>
                            <td class="text1"><input type="text" name="chassisNo" id="chassisNo" class="form-control" value='<c:out value="${VDet.chassisNo}" />' readonly ></td>
                        </tr>
                    </c:forEach>
                </c:if>
                <tr>
                    <td class="contenthead" colspan="4">Permit Details</td>
                </tr>
                <c:if test="${vehiclePermit != null}">
                    <c:forEach items="${vehiclePermit}" var="VIns" >
                        <tr>
                            <td class="texttitle1">Permit Type</td>
                            <td class="text1">

                                <select name="permitType" id="permitType" class="form-control" style="width:100px;">
                                    <c:choose>
                                         <c:when test="${VIns.permitType == 'State permit'}" >
                                            <option value="State permit" selected>State permit</option>
                                            <option value="National permit">National permit</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="State permit" >State permit</option>
                                            <option value="National permit" selected>National permit</option>
                                        </c:otherwise>
                                    </c:choose>
                                </select>
                                <input type="hidden" name="permitid" id="permitid" value='<c:out value="${VIns.permitid}" />'>
                            </td>
                            <td class="texttitle1">Permit No</td>
                            <td class="text1"><input type="text" name="permitNo" id="permitNo" class="form-control" value='<c:out value="${VIns.permitNo}" />' ></td>
                        </tr>
                        <tr>
                            <td class="texttitle2">Permit Paid Date</td>
                            <td class="text2"><input type="text" name="permitPaidDate" id="permitPaidDate" class="datepicker"  value='<c:out value="${VIns.permitPaidDate}" />' ></td>
                            <td class="texttitle2">Permit Amount</td>
                            <td class="text2"><input type="text" name="permitAmount" id="permitAmount"  class="form-control"  value='<c:out value="${VIns.permitAmount}" />' ></td>
                        </tr>
                        <tr>
                            <td class="texttitle1">Permit Expiry Date</td>
                            <td class="text1"><input type="text" name="permitExpiryDate" id="permitExpiryDate" class="datepicker"  value='<c:out value="${VIns.permitExpiryDate}" />' ></td>
                            <td class="texttitle1">remarks</td>
                            <td class="text1"><input type="text" name="remarks" id="remarks"  class="form-control"  value='<c:out value="${VIns.remarks}" />' ></td>
                        </tr>

                    </c:forEach>
                </c:if>
                <tr>
                    <td align="center" class="texttitle1" colspan="4"></td>
                </tr>
            </table>
            <br>
            <center>
                Upload Copy Of Bills&emsp;<input type="file" />
                <br>
                <br>
                <input type="submit" class="button" value=" Update " />
            </center>
    </body>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</html>

