<%-- 
Document   : userCustomer
Created on : Jul 22, 2008, 3:35:04 PM
Author     : vidya
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import = "ets.domain.users.business.LoginTO" %>
<%@ page import="java.util.*" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"></head>
<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#userName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getUserDetails.do",
                    dataType: "json",
                    data: {
                        userName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var name = ui.item.Name;
                var id = ui.item.Id;
                $('#userId').val(id);
                $('#userName').val(name);
                return false;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });
</script>
<script language="javascript">
/*window.onload = nameSearch;
function nameSearch() {
var oTextbox = new AutoSuggestControl(document.getElementById("userName"),
    new ListSuggestions("userName","/throttle/getUserNameList.do?"));
}
*/

//window.onload = nameSearch;
//function nameSearch(){
//    //alert("value");
//    var oTextbox = new AutoSuggestControl(document.getElementById("userName"),new ListSuggestions("userName","/throttle/getUserNameList.do?"));
//} 

function submitPage(value)    {
if(value=='search'){
if(document.userCustomer.userName.value==""){
alert("Required field cannot be left blank");
document.userCustomer.userName.focus();
}else{
document.userCustomer.action='/throttle/searchUserCustomer.do';
document.userCustomer.reqfor.value='searchUserRoles';
document.userCustomer.submit();
}
}else if(value=='save'){
var length = document.userCustomer.roleId.length;
var counter = document.userCustomer.assignedRole.length;


for(var j = 0; j < counter; j++){			
//alert(document.userCustomer.assignedRole.options[j].value);
document.userCustomer.assignedRole.options[j].selected = true;                   
}             
document.userCustomer.action='/throttle/saveUserCustomer.do';
document.userCustomer.reqfor.value='saveUserRoles';
document.userCustomer.submit();
}
}

function copyAddress(roleId)
{
var avilableFunction = document.getElementById("availableFunc");
var index = 0;
var selectedLength = 0;
if(avilableFunction.length != 0){
for (var i=0; i<avilableFunction.options.length ; i++) {    
    if(avilableFunction.options[i].selected == true){
            selectedLength++;
    }    
  }
for (var j=0; j<selectedLength ; j++) {
        for (var i=0; i<avilableFunction.options.length ; i++) {
            if(avilableFunction.options[i].selected == true){
                
                    var optionCounter = document.userCustomer.assignedfunc.length;
                    document.userCustomer.assignedfunc.length = document.userCustomer.assignedfunc.length +1;
                    document.userCustomer.assignedfunc.options[optionCounter].text = avilableFunction.options[i].text ;
                    document.userCustomer.assignedfunc.options[optionCounter].value =  avilableFunction.options[i].value;
                    avilableFunction.options[i] = null;
                    break;
            }
          }
}  
}
else {
alert("Please Select any Value");
}
}



function copyAddress1(assRole)
{
var selectedValue = document.getElementById('assignedfunc');
var index = 0;
var selectedLength = 0;

if(selectedValue.length != 0){
for (var i=0; i<selectedValue.options.length ; i++) {    
    if(selectedValue.options[i].selected == true){
            selectedLength++;
    }    
  }  
for (var j=0; j<selectedLength ; j++) {
        for (var i=0; i<selectedValue.options.length ; i++) {
            if(selectedValue.options[i].selected == true){
                
                    var optionCounter = document.userCustomer.availableFunc.length;
                    document.userCustomer.availableFunc.length = document.userCustomer.availableFunc.length +1;
                    document.userCustomer.availableFunc.options[optionCounter].text = selectedValue.options[i].text ;
                    document.userCustomer.availableFunc.options[optionCounter].value =  selectedValue.options[i].value;
                    selectedValue.options[i] = null;
                   break;
            }
          }
}  
}
else {
alert("Please Select any Value");
}
}


function setValue(userName,userId) {
if(userName != 'null'){
document.userCustomer.userName.value="";
document.userCustomer.userName.value= userName; 
document.userCustomer.userId.value= userId; 
}
document.userCustomer.userName.focus();
}
window.onload = setValue;
</script>

<!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/ 
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->

<body onLoad="setValue('<%= request.getAttribute("userName") %>','<%= request.getAttribute("userId") %>');">
<form name="userCustomer" method="post">
<!-- copy there from end -->
<!-- pointer table -->

<%@ include file="/content/common/path.jsp" %>

<!-- pointer table -->


<%@ include file="/content/common/message.jsp" %>

<br>
<br>
<table align="center" width="400" border="0" cellspacing="0" cellpadding="0" id="bg" class="border">

<tr>              
<td height="30" class="text1">Enter the UserName :&nbsp;
    <input type="hidden" class="textbox" align="right" id="userId" name="userId" value="" >
    <input type="text" class="textbox" align="right" id="userName" name="userName" value="" >
</td>
<td align="center" height="30" class="text1"><input type="button" align="center" name="search" value="Go" onClick="submitPage(this.name)" class="button"></td>
</tr>
</table><br><br>
<% int index = 0;
ArrayList empTotalDays = (ArrayList) request.getAttribute("customerList");
if(request.getAttribute("customerList") != null){

%> 

<table align="center" border="1" cellpadding="0" cellspacing="0" width="500" id="bg"> 
<tr>
<td class="contenthead" valign="center" height="35" >Available Customers </td>
<td class="contenthead" height="35"></td>
<td class="contenthead" valign="center" height="35">Assigned Customers </td>
</tr>
<tr>
<td valign="top">
<table width="150" height="150" cellpadding="0" cellspacing="0" align="center" id="bg">
<tr>  
<td width="150"><select style="width:200px;"  id="availableFunc" multiple size="10" name="roleId">
<c:if test = "${customerList != null}" >
<c:forEach items="${customerList}" var="cust"> 
<option value='<c:out value="${cust.customerId}" />'><c:out value="${cust.customerName}" /></option>
</c:forEach>
</c:if>
</select></td>
</tr>
</table>                       

</td>   

<td height="100" valign="center">
<table width="20" height="70" cellpadding="2" cellspacing="2" align="center" id="bg">
<tr>
<td height="15" align="center" bgcolor="#F4F4F4" style=" border:1px; border-bottom-style:solid; "><input type="button" class="textbox" value=">" onClick="copyAddress(roleId.value)"></td></tr>
<tr >
<td height="15" align="center" bgcolor="#F4F4F4" style=" border:1px; "><input type="button" class="textbox" value="<" onClick="copyAddress1(assignedRole.value)"></td></tr>


</table></td>
<td valign="top">

<table width="150" height="150" cellpadding="0" cellspacing="0" align="center" id="bg">
<tr>
<td width="150"><select style="width:200px;" id="assignedfunc" multiple size="10" name="assignedRole" >
<c:if test = "${userCustomer != null}" >
<c:forEach items="${userCustomer}" var="usr"> 
<option value='<c:out value="${usr.customerId}" />'><c:out value="${usr.customerName}" /></option>
</c:forEach >
</c:if>
</select></td>
</tr>
</table></td>
</tr> 
</table>
<br><center>  
<input type="button" name="save" value="Save" onClick="submitPage(this.name)" class="button">
</center>
<% } %>
<input type="hidden" value="" name="assignRoleId">
<input type="hidden" value="" name="reqfor">
<br>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
