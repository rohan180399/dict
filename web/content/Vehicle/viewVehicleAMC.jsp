<%-- 
    Document   : viewVehicleAMC
    Created on : Sep 18, 2012, 3:11:25 PM
    Author     : entitle
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>

        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script type="text/javascript">

            function show_src() {
                document.getElementById('exp_table').style.display='none';
            }

            function show_exp() {
                document.getElementById('exp_table').style.display='block';
            }

            function show_close() {
                document.getElementById('exp_table').style.display='none';
            }
        </script>

    </head>
    <!--setImages(1,0,0,0,0,0);-->
    <body onLoad="getVehicleNos();setImages(1,0,0,0,0,0);setDefaultVals('<%= request.getAttribute("regNo")%>','<%= request.getAttribute("typeId")%>','<%= request.getAttribute("mfrId")%>','<%= request.getAttribute("usageId")%>','<%= request.getAttribute("groupId")%>');">
        <form name="viewVehicleDetails"  method="post" >
            <%@ include file="/content/common/path.jsp" %>


            <%@ include file="/content/common/message.jsp" %>


            <table width="700" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Export" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:700px;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">View Vehicle AMC Search</li>
                            </ul>
                            <div id="first">
                                <table width="100%" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                                    <tr>
                                        <td>Vehicle Number</td><td><input type="text" id="regno" name="regNo" value="" class="form-control" autocomplete="off"></td>

                                    </tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><td>&nbsp;</td>
                                        <td><input type="button" class="button" onclick="submitPage(this.name);" name="search" value="Go"></td>
                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>
            <br>
            
<%
            int index = 1;
           
%>


            <c:if test="${vehicleAmcList == null }" >
                <br>
                <center><font color="red" size="2"> no records found </font></center>
            </c:if>
            <c:if test="${vehicleAmcList != null }" >
                <table align="center" width="700" cellpadding="0" cellspacing="0"  class="border">

                    <tr>

                        <td class="contentsub" height="30"><div class="contentsub">Sno</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">Vehicle Number</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">AMC Company Name</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">AMC Amount</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">AMC From Date</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">AMC To Date</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">&nbsp;</div></td>
                    </tr>
                    <%
            String style = "text1";%>
                    <c:forEach items="${vehicleAmcList}" var="veh" >
                        <%
            if ((index % 2) == 0) {
                style = "text1";
            } else {
                style = "text2";
            }%>
                        <tr>
                            <td class="<%= style %>" height="30" style="padding-left:30px; "> <%= index %> </td>
                            <td class="<%= style %>" height="30" style="padding-left:30px; "> <c:out value="${veh.regNo}" /></td>
                            <td class="<%= style %>" height="30" style="padding-left:30px; "><c:out value="${veh.amcCompanyName}" /></td>
                            <td class="<%= style %>" height="30" style="padding-left:30px; "><c:out value="${veh.amcAmount}" /></td>
                            <td class="<%= style %>" height="30" style="padding-left:30px; "><c:out value="${veh.amcFromDate}" /></td>
                            <td class="<%= style %>" height="30" style="padding-left:30px; "><c:out value="${veh.amcToDate}" /></td>
                            <td class="<%= style %>" height="30" style="padding-left:30px; "><a href='/throttle/vehicleAMCDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />&amcid=<c:out value="${veh.amcId}" />'>alter </a> </td>
                        </tr>
                        <% index++; %>
                    </c:forEach>
                </table>
            </c:if>
            <br>

            
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    <script type="text/javascript">
        function submitPage(value){
            //alert(value);
            if(value == 'search' || value == 'Prev' || value == 'Next' || value == 'GoTo' || value =='First' || value =='Last'){
                if(value=='GoTo'){
                    var temp=document.viewVehicleDetails.GoTo.value;
                    document.viewVehicleDetails.pageNo.value=temp;
                    document.viewVehicleDetails.button.value=value;
                    document.viewVehicleDetails.action = '/throttle/vehicleAMCList.do';
                    document.viewVehicleDetails.submit();
                }else if(value == "First"){
                    temp ="1";
                    document.viewVehicleDetails.pageNo.value = temp;
                    value='GoTo';
                }else if(value == "Last"){
                    temp =document.viewVehicleDetails.last.value;
                    document.viewVehicleDetails.pageNo.value = temp;
                    value='GoTo';
                }
                //document.viewVehicleDetails.button.value=value;
                document.viewVehicleDetails.action = '/throttle/vehicleAMCList.do';
                document.viewVehicleDetails.submit();
              
            }else if(value == 'add'){
                document.viewVehicleDetails.action = '/throttle/handleVehicleAMC.do';
                document.viewVehicleDetails.submit();
            }else{
                document.viewVehicleDetails.action='/throttle/vehicleAMCList.do'
                document.viewVehicleDetails.submit();
            }
        }


        function setDefaultVals(regNo,typeId,mfrId,usageId,groupId){

            if( regNo!='null'){
                document.viewVehicleDetails.regNo.value=regNo;
            }
            if( typeId!='null'){
                document.viewVehicleDetails.typeId.value=typeId;
            }
            if( mfrId!='null'){
                document.viewVehicleDetails.mfrId.value=mfrId;
            }
            if( usageId!='null'){
                document.viewVehicleDetails.usageId.value=usageId;
            }
            if( groupId!='null'){
                document.viewVehicleDetails.groupId.value=groupId;
            }
        }


        function getVehicleNos(){
            //onkeypress='getList(sno,this.id)'
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
        }

    </script>
</html>

