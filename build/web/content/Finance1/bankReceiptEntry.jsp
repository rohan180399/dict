<%--  
    Document   : bankReceiptEntry
    Created on : May 13, 2013, 7:53:47 PM
    Author     : Entitle
--%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <%@ page import="ets.domain.contract.business.ContractTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });
        </script>

    </head>
    <script language="javascript">
        function checkLedger(sno)
        {
             if(sno >= 1){
             document.getElementById('addrow').style.visibility='hidden';
             }else{
             document.getElementById('addrow').style.visibility='visible';
             }

//            var Lid = document.getElementById("bankid1"+sno).value;
//                var temp = Lid.split("~");
//            if(temp[0] == "52") {
//                alert("Select Other then Karikkali cash")
//                document.getElementById("bankid1"+sno).value=0;
//            }
        }
        window.onload = function()
        {
            var currentDate = new Date();
            var day = currentDate.getDate();
            var month = currentDate.getMonth() + 1;
            var year = currentDate.getFullYear();
            var myDate= day + "-" + month + "-" + year;
            document.getElementById("date").value=myDate;
        }


        function sumReceiptAmt(){
            var sumAmt=0;
            var totAmt=0;
            sumAmt= document.getElementsByName('amount');
            for(i=0;i<sumAmt.length;i++){
                if(sumAmt[i].value != ""){
                totAmt += parseInt(sumAmt[i].value);
                document.getElementById('totalDebitAmt').value=parseInt(totAmt);
                }else{
                document.getElementById('totalDebitAmt').value=0;
                }
            }
        }

        function bankIdVal()
        {
            var bankid1 = document.getElementsByName("bankid1");
            var bankid2 = document.getElementsByName("bankid2");
            //            alert("bankid1=== "+textValidation(bankid1[i]);
            // alert("bankid2=== "+bankid2);
        }
        function setSelectbox(i)
        {
            var selected=document.getElementsByName("selectedIndex");
            alert("selected=="+selected[i]);
            selected[i].checked = 1;

        }
        function submitPage(value){
            //var checValidate = selectedItemValidation();
            if(value == "Save"){
               if(document.getElementById('date').value == ""){
                    alert("please select bank payment date");
                    document.getElementById('date').focus();
               }else if(document.getElementById('BankHead').value == "0"){
                    alert("please select bank");
                    document.getElementById('BankHead').focus();
                }else if(document.getElementById('chequeNo').value == ""){
                    alert("please enter cheque no");
                    document.getElementById('chequeNo').focus();
                }else if(document.getElementById('chequeDate').value == ""){
                    alert("please select cheque date");
                    document.getElementById('chequeDate').focus();
                }else{
                    var checValidate = selectedItemValidation();
                }if(checValidate == 'SubmitForm'){
                document.manufacturer.action = '/throttle/insertBankReceiptEntry.do';
                document.manufacturer.submit();
                }
            }
        }
       function selectedItemValidation(){
            var index = document.getElementsByName("selectedIndex");
            var bankId = document.getElementById("bankId");
            var chec=0;
            var mess = "SubmitForm";
            for(var i=1;(i<=index.length && index.length!=0);i++){
                chec++;
                if(document.getElementById('bankId'+i).value == 0){
                    alert("select bank for row "+i);
                    document.getElementById('bankId'+i).focus();
                    mess =  'NotSubmit';
                }else{
                    mess =  'SubmitForm';
                }
                if(document.getElementById('amount'+i).value == ""){
                    alert("select amount for row "+i);
                    mess =  'NotSubmit';
                }else {
                    mess =  'SubmitForm';
                }
            }
            if(chec == 0){
                alert("Please click ADDROW And Then Proceed");
                mess =  'NotSubmit';
            }
            return mess;
        }

        var rowCount=1;
        var sno=0;
        var style="text2";
        function showRow()
        {
            if(rowCount%2==0){
                style="text2";
            }else{
                style="text1";
            }
            sno++;
            var tab = document.getElementById("addRow");
            var newrow = tab.insertRow(rowCount);

            var cell = newrow.insertCell(0);
            var cell1 = "<td class='text1' height='25' >"+sno+"</td>";
            cell.setAttribute("className",style);
            cell.innerHTML = cell1;

            cell = newrow.insertCell(1);
            var cell2 = "<td class='text1' height='30'><select class='form-control' id='bankId"+sno+"' onChange='checkLedger("+sno+")' style='width:225px'  name='bankid1'><option selected value=0>---Select---</option><c:if test = "${bankPaymentLedgerList != null}" ><c:forEach items="${bankPaymentLedgerList}" var="JLL"><option  value='<c:out value="${JLL.ledgerID}" />~<c:out value="${JLL.groupCode}" />~<c:out value="${JLL.levelID}" />~<c:out value="${JLL.ledgerCode}" />'><c:out value="${JLL.ledgerName}" /></option></c:forEach ></c:if></select></td>";
            cell.setAttribute("className",style);
            cell.innerHTML = cell2;

            cell = newrow.insertCell(2);
            var cell3 = "<td class='text1' height='30'><input type='text' name='amount' id='amount"+sno+"' maxlength='13' onkeyup='sumReceiptAmt();' onclick='bankIdVal(sno-1)'  size='20' class='form-control' /></td>";
            cell.setAttribute("className",style);
            cell.innerHTML = cell3;

            cell = newrow.insertCell(3);
            var cell4 = "<td height='30' class='tex1'> <div align='center'><select name='accType' id='accType"+sno+"' class='form-control'><option value='CREDIT'>CREDIT</option></select></div> </td>";
            cell.setAttribute("className",style);
            cell.innerHTML = cell4;

            cell = newrow.insertCell(4);
            var cell5 = "<td class='text1' height='30'><input type='text' name='narration' id='narration"+sno+"' size='20' class='form-control' /></td>";
            cell.setAttribute("className",style);
            cell.innerHTML = cell5;

            cell = newrow.insertCell(5);
            var cell6 = "<td class='text1' height='30'><input type='checkbox' name='selectedIndex' value='"+sno+"'/></td>";
            cell.setAttribute("className",style);
            cell.innerHTML = cell6;

//            cell = newrow.insertCell(5);
//            var cell6 = "<td alain='left' >3tretrt<input type='checkbox' name='deleteItem' value='"+snumber+"'/> </td>";
//            cell.setAttribute("className",style);
//            cell.innerHTML = cell6;

            var temp=sno-1;

            rowCount++;
        }


         function DeleteRow() {
                try {
                    var table = document.getElementById("addRow");
                    rowCount = table.rows.length;
                    for(var i=1; i<rowCount; i++) {
                        var row = table.rows[i];
                        var checkbox = row.cells[5].childNodes[0];
                        if(null != checkbox && true == checkbox.checked) {
                            if(rowCount <= 1) {
                                alert("Cannot delete all the rows");
                                break;
                            }
                            table.deleteRow(i);
                            rowCount--;
                            i--;
                            sno--;
                            // snumber--;
                        }
                    }sumPaymentAmt();checkLedger();
                }catch(e) {
                    alert(e);
                }
            }

        function printBankReceiptEntry(val){
                document.manufacturer.action = '/throttle/printBankReceiptEntry.do?voucherCode='+val;
                document.manufacturer.submit();
       }
    </script>
            <body>
        <form name="manufacturer" method="post" >
            <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                <tr>
                    <td>
                        <%@ include file="/content/common/path.jsp" %>
                    </td></tr></table>
            <!-- pointer table -->
            <!-- message table -->
            <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
                <tr>
                    <td >
                        <%@ include file="/content/common/message.jsp"%>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="683" id="bg" class="border">
                <tr>
                    <td colspan="6" class="contenthead" height="30">
                        <div class="contenthead" align="center">Bank Receipt </div>
                    </td>
                </tr>
                <tr>
                    <td class="texttitle1" height="30">Date </td>
                    <td class="text1" height="30">
                        <input type="textbox" id="date" name="date" value=""  size="20" class="datepicker">
                    </td>
                    <td class="texttitle1" height="30">Bank Head</td>
                    <td class="text1" height="30">
                        <select class="textbox" id="BankHead" style="width:225px"  name="BankHead">
                            <option selected value=0>---Select---</option>
                            <c:if test = "${bankLedgerList != null}" >
                                <c:forEach items="${bankLedgerList}" var="BLL">
                                    <option  value="<c:out value="${BLL.ledgerID}" />~<c:out value="${BLL.groupCode}" />~
                                             <c:out value="${BLL.levelID}" />~<c:out value="${BLL.ledgerCode}" />">
                                    <c:out value="${BLL.ledgerName}" />
                                    </c:forEach >
                                </c:if>
                        </select>
<!--                        <input type="text" id="cashHeadName" name="cashHeadName" value="Karikali Cash"  size="20" class="textbox" readonly>-->
                    </td>
                    <td class="texttitle1" height="30">
                        <input type="text" id="totalDebitAmt" name="totalDebitAmt" value=""  size="20" class="textbox" readonly>
                        Debit
                    </td>
                </tr>
                <tr>
                    <td class="texttitle1" height="30">Cheque No</td>
                    <td class="texttitle1" height="30">
                        <input type="text" id="chequeNo" name="chequeNo" value=""  size="20" class="textbox"  >
                    </td>
                    <td class="texttitle1" height="30">Cheque Date </td>
                    <td class="text1" height="30">
                        <input type="textbox" id="chequeDate" name="chequeDate" value=""  size="20" class="datepicker" autocomplete="off">
                    </td>
                </tr>

            </table>
            <p>&nbsp;</p>
            <div align="center">
                <table width="780" class="border" border="3" id="addRow">
                    <tr class="contentsub">
                        <td class="contentsub" ><div class="contentsub">Sno</div></td>
                        <td class="contentsub" ><div class="contentsub">Ledger Name</div></td>
                        <td class="contentsub" ><div class="contentsub">Amount</div></td>
                        <td class="contentsub" ><div class="contentsub">Account Type</div></td>
                        <td class="contentsub" ><div class="contentsub">Narration</div></td>
                        <td class="contentsub" ><div class="contentsub">Select</div></td>
                    </tr>
                </table>
            </div>
            <p><br>
            </p>
            <center>
                <input type="button" id="addrow" name="addrow" value="AddRow" class="button" onClick="showRow();">&nbsp;&nbsp;
                <input type="button" value="DeleteRow" class="button" onClick="DeleteRow();">&nbsp;&nbsp;
                <input type="button" value="Save" class="button" onClick="submitPage(this.value);">
            </center>
            <br>

            <c:if test = "${bankReceiptList != null}" >

                <br>
                <table align="center" width="100%" border="0" id="table" class="sortable">
                    <thead>
                        <tr>
                            <th><h3>S.No</h3></th>
                            <th><h3>Entry Date</h3></th>
                            <th><h3>Voucher Code</h3></th>
                            <th><h3>Debit Ledger</h3></th>
                            <th><h3>Debit Amount</h3></th>
                            <th><h3>Credit Ledger</h3></th>
                            <th><h3>Credit Amount</h3></th>
                            <th><h3>Print</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 0;%>
                        <c:forEach items="${bankReceiptList}" var="BRL">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text1";
                                        } else {
                                            classText = "text2";
                                        }
                            %>
                            <tr>
                                <td class="<%=classText%>"  align="left"> <%= index + 1%> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${BRL.accountEntryDate}"/> </td>
                                <td class="<%=classText%>" align="left"> <c:out value="${BRL.voucherCode}" /></td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${BRL.debitLedgerName}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${BRL.debitAmount}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${BRL.creditLedgerName}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${BRL.creditAmount}"/> </td>
                                <td class="<%=classText%>"  align="left"><a href="" onclick="printBankReceiptEntry('<c:out value="${BRL.voucherCode}" />')">Print</a></td>
                            </tr>

                            <% index++;%>
                        </c:forEach>
                    </c:if>
                </tbody>
            </table>
            <br>
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                    <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5" selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1,true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1,true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table",1);
        </script>
    </body>
</html>