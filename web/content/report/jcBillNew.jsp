

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


    <%@ page import="ets.domain.renderservice.business.RenderServiceTO" %>
    <%@ page import="ets.domain.renderservice.business.JobCardItemTO" %>
    <%@ page import="ets.domain.problemActivities.business.ProblemActivitiesTO" %>
    <%@ page import="ets.domain.report.business.NumberWordsIndianRupees" %>


    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
    <%@ page import="java.util.*" %>
    <title>Job Card Bill</title>

</head>







<script>



    function print(val)
    {
        var DocumentContainer = document.getElementById("print");
        var WindowObject = window.open('', "TrackHistoryData",
            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        WindowObject.close();
    }


</script>


<body>
<div id="print">
<%

int itemNameLength = 35,uomLength=10;
JobCardItemTO itemTO=null;
ProblemActivitiesTO activityTO = null;
int pageSize = 24;
String[] activity = null;



ArrayList billRecord =new ArrayList();
ArrayList billheader =new ArrayList();
ArrayList itemList =new ArrayList();
ArrayList labourList =new ArrayList();
ArrayList taxList =new ArrayList();
RenderServiceTO rto =null;
RenderServiceTO billrto =null;
NumberWords nm=null;
float laborTotal = 0;
float spareTotal = 0;
float taxTotal = 0;
double total = 0;
int index = 0;
int loopCntr = 0;

int totalRecords = 0;
int itemSize = 0;
int laborSize =  0;
int taxSize = 0;
int count=0;

ArrayList billIds = (ArrayList) request.getAttribute("billIds");
Iterator itr=billIds.iterator();
while(itr.hasNext()){
    nm=new NumberWords();
         rto = new RenderServiceTO();
        rto = (RenderServiceTO) itr.next();
count++;
billRecord =rto.getBillRecord();
billheader=rto.getBillHeaderInfo();
itemList=rto.getBillItems();
labourList=rto.getBillActivities();
taxList=rto.getBillTaxDetails();

 totalRecords = billRecord.size();
 itemSize = itemList.size();

 laborSize =  labourList.size();
 taxSize = taxList.size();

 laborTotal = 0;
 spareTotal = 0;
 taxTotal = 0;
 total = 0;
 index = 0;
 loopCntr = 0;



for(int i=0;i< billRecord.size(); i=i+pageSize){
    
loopCntr = i;
%>



<style>


.text1 {
font-family:Tahoma;
font-size:14px;
color:#333333;
background-color:#E9FBFF;
padding-left:10px;
line-height:15px;
}
.text2 {
font-family:Tahoma;
font-size:12px;
color:#333333;
background-color:#FFFFFF;
padding-left:10px;
line-height:15px;
}


</style>



      <%

      Iterator billitr=billheader.iterator();
  while(billitr.hasNext()){
          billrto = new RenderServiceTO();
          billrto = (RenderServiceTO) billitr.next();

    %>
    <table width="725" border="1" cellspacing="0" cellpadding="0"  style="border:1px; border-color:#E8E8E8; border-style:solid;" >
           <tr>
               <td height="27" colspan="4" class="text2"  align="center"  scope="row" style="border:1px; border-bottom-color:##CCCCCC; border-bottom-style:solid;  ">
                  <b> <%=billrto.getInvoiceType()%> INVOICE </b>
               </td>
           </tr>

           <c:set var="jobcardNo"   value='billrto.getJcMYFormatNo()'/>
           <tr>
               <td class="text2" colspan="2" width="427"  align="left" rowspan="2" scope="col" style="border:1px; border-bottom-color:##CCCCCC; border-bottom-style:solid; border-right-color:##CCCCCC; border-right-style:solid; " >
                   <p> <b> your company name. </b><br>
                       address 1,<br>
                       address 2,<br>
                       Pin Code xxx xxx<br>
               E-mail : abc@xyz.com</p></td>
               <td  class="text2"  align="left"  width="150" height="53" scope="col"  style="border:1px; border-bottom-color:##CCCCCC; border-bottom-style:solid; border-right-color:##CCCCCC; border-right-style:solid; " >
                   <p>Invoice No<br>

                   <b>  <%=billrto.getInvoiceNo()%> </b>


                   </p>
               </td>
               <td  class="text2"  align="left"  width="148" scope="col"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  " >
                   <p>Dated<br>
                   <b> <%=billrto.getBillDate()%>  </b> </p>
               </td>
           </tr>
           <tr>
               <td class="text2"  align="left" scope="col"  style=" padding-bottom:3px; padding-top:3px; border:1px; border-bottom-color:##CCCCCC; border-bottom-style:solid; border-right-color:##CCCCCC; border-right-style:solid; ">
                   <p>Vehicle No.<br>
                   <b> <%=billrto.getVehicleNo()%>  </b> </p>
               </td>
               <td  class="text2"  align="left" scope="col"  style="padding-bottom:3px; padding-top:3px; border:1px; border-bottom-color:##CCCCCC; border-bottom-style:solid;  ">
                   <p>Other Reference<br>
                   <b> <%=billrto.getCompanyName()%></b> </p>
               </td>
           </tr>
           <tr>
               <td colspan="2" rowspan="4" align="left" valign="top" class="text2" scope="col"  style="border:1px; border-right-color:##CCCCCC; border-right-style:solid; ">
                   <p>Buyer<br>
                   <b><%=billrto.getCustomerName()%></b> <br>
                    <%=billrto.getAddress()%> <br>
                       <%=billrto.getCity()%><br>
                       <%=billrto.getState()%><br>
                       <%=billrto.getPhone()%><br>
                       <%=billrto.getEmail()%><p>
               </td>
               <td class="text2"  align="left"  style="border:1px; border-right-color:##CCCCCC; border-right-style:solid;border-bottom-color:##CCCCCC; border-bottom-style:solid; ">
                   <p>Buyer's Order No.<br>
                   &nbsp;</p>
               </td>
               <td class="text2"  align="left"  style="border:1px; border-bottom-color:##CCCCCC; border-bottom-style:solid;">
                   <p>Dated<br>
                   &nbsp;</p>
               </td>
           </tr>
           <tr>
               <td class="text2"  align="left"  style="border:1px; border-bottom-color:##CCCCCC; border-bottom-style:solid; border-right-color:##CCCCCC; border-right-style:solid; ">
                   <p>Despatch Document  No.<br>
                   &nbsp;</p>
               </td>
               <td class="text2"  align="left"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; ">
                   <p>Dated<br>
                   &nbsp;</p>
               </td>
           </tr>
           <tr>
               <td class="text2"  align="left"  style="border:1px; border-bottom-color:##CCCCCC; border-bottom-style:solid; border-right-color:##CCCCCC; border-right-style:solid; ">
                   <p>Despatched through <br>
                   &nbsp;</p>
               </td>
               <td class="text2"  align="left"  style="border:1px; border-bottom-color:##CCCCCC; border-bottom-style:solid; ">
                   <p>Destination<br>
                   &nbsp;</p>
               </td>
           </tr>
           <tr>
               <td  class="text2"  colspan="2" align="left"  style="border:1px;">
                   <p>Terms of Delivery <br>
                   <p> &nbsp;
               </td>
           </tr>
       </table>

                    <%}%>

            <table width="725" height="99"  border="0" cellpadding="0"  style="border:1px; border-left-color:##E8E8E8; border-left-style:solid; border-right-color:##E8E8E8; border-right-style:solid;"  cellspacing="0">
        <tr>
	    <th width="30" class="text2"  height="30" style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid; " scope="col" >Sno</th>
            <th class="text2" width="407"  height="30"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; " scope="col" >Description of Goods </th>
            <th width="65"  class="text2"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; "  scope="col">Price</th>
            <th width="50" class="text2"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; "  scope="col">VAT %</th>
            <th width="50"  class="text2" style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; "  scope="col">Qty.</th>
            <th width="50"  class="text2"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; " scope="col">UOM</th>
            <th width="65"  class="text2"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; " scope="col">Amount</th>
        </tr>

<% if(loopCntr <= itemSize ){ %>
<%

        for(int j=loopCntr; ( j < (itemSize) ) && (j < i + pageSize  ) ; j++){

       ++loopCntr;
       itemTO = new JobCardItemTO();
       itemTO = (JobCardItemTO) billRecord.get(j);
String itemName = "";
String uom = "";
       itemName = itemTO.getItemName();
       uom = itemTO.getUom();
       if(itemName.length() > itemNameLength){
           itemName = itemName.substring(0,itemNameLength-1);
       }
       if(uom.length() > uomLength){
           uom = uom.substring(0,uomLength-1);
       }

%>



        <tr>
            <td  class="text2"  align="left"  style="border:1px; border-color:#FFFFFF; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                <% index++; %> <%= index %>
            </td>
            <td  class="text2"   align="left"   style="border:1px; border-color:#FFFFFF; border-left-color:#CCCCCC; border-left-style:solid; "  scope="row" >
                <%= itemName %>
            </td>
            <td  class="text2"   style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid; "  align="right" >
                <%= itemTO.getPrice() %>
            </td>
            <td  class="text2"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;"   align="center" >
                <%= itemTO.getTax() %>
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-left-color:#CCCCCC; border-left-style:solid; "  align="center" >
                <%= itemTO.getQuantity() %>
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;border-left-color:#CCCCCC; border-left-style:solid; "  align="center" >
                <%= uom %>
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; "  align="right" >
                <%= itemTO.getAmount() %>
            </td>
            <% spareTotal =(float) spareTotal + Float.parseFloat(itemTO.getAmount()); %>
        </tr>
    <% } %>

    <% if( (loopCntr < i+pageSize ) && (loopCntr == itemSize) ) { %>

        <tr>
            <td  class="text2"  align="right"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
               &nbsp;
            </td>
            <td  class="text2"  align="right"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                <b>Spare Net Amount</b>
            </td>
            <td  class="text2" align="right"  style="border:1px; border-color:#FFFFFF; border-left-color:#CCCCCC; border-left-style:solid; "  scope="row" >
                &nbsp;
            </td>
            <td  class="text2"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid; " align="right" >
                &nbsp;
            </td>
            <td  class="text2"   style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" align="right" >
                &nbsp;
            </td>
            <td  class="text2"   style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" align="right" >
                &nbsp;
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;border-left-color:#CCCCCC; border-left-style:solid; "   align="right" >
                
                <jsp:useBean id="spareTotalRound"   class="ets.domain.report.business.NumberWordsIndianRupees" >
 			<% spareTotalRound.setRoundedValue(String.valueOf(spareTotal)); %>
                        <b><jsp:getProperty name="spareTotalRound" property="roundedValue" /> </b>
                    </jsp:useBean>
	
            </td>
        </tr>

        <% } %>


<% } %>






<%

    for(int k=loopCntr; ( k < ( itemSize + taxSize) ) && ( k < i+ pageSize ) && (k<totalRecords ) ; k++){

       itemTO = new JobCardItemTO();
       itemTO = (JobCardItemTO) billRecord.get(k);
       ++loopCntr;
%>

        <tr>
            <td  class="text2"  align="right"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
               &nbsp;
            </td>
            <td  class="text2"  align="right" style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
               	 OUTPUT VAT @ <%= itemTO.getTax() %>
            </td>
            <td  class="text2" align="right"  style="border:1px; border-color:#FFFFFF; border-left-color:#CCCCCC; border-left-style:solid; "  scope="row" >
                &nbsp;
            </td>
            <td  class="text2"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid; "  align="right" >
                &nbsp;
            </td>
            <td  class="text2"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;"  align="right" >
                &nbsp;
            </td>
            <td  class="text2"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;"  align="right" >
                &nbsp;
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;border-left-color:#CCCCCC; border-left-style:solid; "   align="right" >
                <%= itemTO.getAmount() %> <c:set var="taxTotal" value="0" />
            </td>
        </tr>
        <% taxTotal = (float) taxTotal + Float.parseFloat(itemTO.getAmount()); %>
        <% } %>


<%

if(loopCntr > (itemSize+taxSize-1) && loopCntr < i+pageSize ) { %>

        <tr>
            <th width="30" class="text2"  height="30" style="border:1px; border-color:#FFFFFF; border-bottom-color:#CCCCCC; border-bottom-style:solid;  border-top-color:#CCCCCC; border-top-style:solid;  border-left-color:#CCCCCC; border-left-style:solid;" scope="col" >&nbsp;</th>
            <th colspan="5" width="625" class="text2" style="border:1px; border-color:#FFFFFF; border-bottom-color:#CCCCCC; border-bottom-style:solid;  border-top-color:#CCCCCC; border-top-style:solid;" scope="col" >   Labour Charges</th>
            <th  width="65"  class="text2"  style="border:1px; border-color:#FFFFFF; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; border-top-color:#CCCCCC; border-top-style:solid;"  scope="col">&nbsp;</th>
        </tr>

<%
    for(int l=loopCntr; ( l < ( billRecord.size() ) && ( loopCntr < i+pageSize )  ) ; l++ ) {
       activityTO = new ProblemActivitiesTO();
       activityTO = (ProblemActivitiesTO) billRecord.get(l);
       activity = activityTO.getActivitySplt();
       ++loopCntr;
%>




        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
		<% index++; %>
                 <%= index %>
            </td>
            <td class="text2" colspan="5"  align="left"   style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; " scope="row" >
                    <% for(int k=0;k<activity.length ; k++){ %>
                      <%= activity[k] %>   <br>
                    <% } %>
            </td>
            <td class="text2"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                <%= activityTO.getAmount() %>
                <% laborTotal = (float) laborTotal + Float.parseFloat(activityTO.getAmount()); %>
            </td>

        </tr>
<% } %>

<% if( (loopCntr <= i + pageSize) && (loopCntr == (itemSize+taxSize+laborSize) ) ) { %>

        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
				&nbsp;
            </td>
            <td class="text2" colspan="5" align="left"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                <b> Labour Total </b>
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                    <jsp:useBean id="laborTotalRound"   class="ets.domain.report.business.NumberWordsIndianRupees" >
 			<% laborTotalRound.setRoundedValue(String.valueOf(laborTotal)); %>
                         <b> <jsp:getProperty name="laborTotalRound" property="roundedValue" />  </b>
                    </jsp:useBean>
            </td>
        </tr>

<% } %>


<% } %>

<% total = (double) (spareTotal + laborTotal + taxTotal ); %>

            <% for(int m=loopCntr; ( (m%pageSize) != 0); m++){ %>

        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;"  scope="row" >
                &nbsp;
            </td>
            <td class="text2" colspan="5"  align="left" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
               &nbsp;
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                &nbsp;
            </td>
        </tr>
            <% } %>

<% if(loopCntr != (totalRecords) ) { %>

        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                &nbsp;
            </td>
            <td class="text2" colspan="5"  align="left"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
               &nbsp;
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                &nbsp;
            </td>
        </tr>

                <tr>
	            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
	                &nbsp;
	            </td>
	            <td class="text2" colspan="5"  align="left"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
	               &nbsp;
	            </td>
	            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
	                &nbsp;
	            </td>
        </tr>

        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                &nbsp;
            </td>
            <td class="text2" colspan="5"  align="left" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
               &nbsp;
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid; border-bottom-color:#CCCCCC; border-bottom-style:solid;" scope="row" >
               &nbsp;
            </td>
            <td class="text2" colspan="5"  align="left" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid; border-bottom-color:#CCCCCC; border-bottom-style:solid;" scope="row" >
               &nbsp;
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-bottom-color:#CCCCCC; border-bottom-style:solid; "  align="right" >
                &nbsp;
            </td>
        </tr>


        <% } if(loopCntr == (totalRecords) ) { %>


        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
				&nbsp;
            </td>
            <td class="text2" colspan="5" align="right"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                Spare Net Amount
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                  <jsp:getProperty name="spareTotalRound" property="roundedValue" />
            </td>
        </tr>


        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
				&nbsp;
            </td>
            <td class="text2" colspan="5" align="right"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                Total Vat
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                <%System.out.println("taxTotal"+taxTotal);%>
                    <jsp:useBean id="totalVatt"   class="ets.domain.report.business.NumberWordsIndianRupees" >
                        <% totalVatt.setRoundedValue(String.valueOf(taxTotal)); %>
                        <jsp:getProperty name="totalVatt" property="roundedValue" />
                    </jsp:useBean>
            </td>
        </tr>

        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
				&nbsp;
            </td>
            <td class="text2" colspan="5" align="right"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                Labour Total
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                        <jsp:getProperty name="laborTotalRound" property="roundedValue" />
            </td>
        </tr>

        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;border-bottom-style:solid; border-bottom-color:#CCCCCC; " scope="row" >
				&nbsp;
            </td>
            <td class="text2" colspan="5"  align="right"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;border-bottom-style:solid; border-bottom-color:#CCCCCC;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
               <b> Grand Total </b>
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                    <jsp:useBean id="numberWords2"   class="ets.domain.report.business.NumberWordsIndianRupees" >
 			<% numberWords2.setRoundedValue(String.valueOf(total)); %>
                        <b> <jsp:getProperty name="numberWords2" property="roundedValue" /> </b>
                    </jsp:useBean>
            </td>
        </tr>



<% } %>

<% if(loopCntr != (totalRecords) ) { %>

        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;  border-bottom-color:#CCCCCC; border-bottom-style:solid; " scope="row" >
                &nbsp;
            </td>
            <td class="text2" colspan="5"  align="center" style="border:1px; border-color:#FFFFFF;    border-bottom-color:#CCCCCC; border-bottom-style:solid; " scope="row" >
                <b>Contnd..</b>
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF;  border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid;"  align="right" >
               &nbsp;
            </td>
        </tr>






<% } %>

    </table>




<table width="725" height="99" border="0" style="border:1px; border-color:##E8E8E8; border-style:solid;"  cellpadding="0" cellspacing="0">
<tr>
<td width="720">
<div class="text2" >
 <% if(loopCntr == (totalRecords) ) { %>
 Amount Chargeable (in words)<br>
  <jsp:useBean id="numberWords1"   class="ets.domain.report.business.NumberWordsIndianRupees" >
 			<% numberWords1.setRoundedValue(String.valueOf(total)); %>

 			<% numberWords1.setNumberInWords(numberWords1.getRoundedValue()); %>

                    <b>  Rupees <jsp:getProperty name="numberWords1" property="numberInWords" /> Only</b>
                    </jsp:useBean>
<% } %>
 <br>
<br>

Remarks:
<% if(loopCntr == (totalRecords) ) { %>
<% if(request.getAttribute("vendorNames") != null ){ %>
<b> <%= request.getAttribute("vendorNames") %> </b>
<% } %>
<% } %>
<br>
<b>AS PER MADH JC.No.&nbsp; <c:out value="${jobcardNo}" /> </b> <br>
Company's VAT TIN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;<b>33241085632</b><br>
Company's CST No. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;<b>999473 - 28.05.08</b><br>
Buyer's VAT TIN/Sales Tax No.      :<br>
</div>

<table width="720" border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td class="text2" width="380" align="left">Declaration </td>
    <td class="text2" width="325" align="right"><b> for your company name. </b> </td>
    </tr>
    <tr>
    <td class="text2" width="380" align="left">We declare that this invoice shows the actual price of the</td>
    <td class="text2" width="325" align="right"> &nbsp;</td>
    </tr>
    <tr>
    <td class="text2" width="380" align="left">goods described and that all particulars are true and correct</td>
    <td class="text2" width="325" align="right">&nbsp; </td>
    </tr>
    <tr>
    <td class="text2" width="380" align="left">&nbsp; </td>
    <td class="text2" width="325" align="right"> Authorized Signatory </td>
    </tr>

</table>

</div>

</td>
</tr>
</table>



<br>
<div ALIGN="CENTER" class="text2">
SUBJECT TO CHENNAI JURISDICTION<br>
This is a Computer Generated Invoice
</div>

<%
}
}
%>





</div>
<br>
    <center>
        <input type="button" class="button" name="Print" value="Print" onClick="print('print');" > &nbsp;
    </center>
<br>
<br>



</body>
</html>

