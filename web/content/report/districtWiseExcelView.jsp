<%--
    Document   : displayDistrictWiseExcelview
    Created on : 10-Jun-2013, 23:18:35
    Author     : Hari@entitlesolutions.com
--%>
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }

        </style>
    </head>
    <body>
        <form name="districtwise" action=""  method="post">
            <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "Trip_Sheet_Report-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
            %>
            <br/>
              <%
                            int index = 0;
                            ArrayList districtSummaryList = (ArrayList) request.getAttribute("districtSummaryList");
                            if (districtSummaryList.size() != 0) {
                %>
                <table  border="1" class="border" align="center" width="95%" cellpadding="0" cellspacing="0" id="bg">
                    <tr>

                    <td class="contentsub" height="30">S.No</td>
                    <td class="contentsub" height="30">Trip Date</td>
                    <td class="contentsub" height="30">Destination</td>
                    <td class="contentsub" height="30">Distance</td>
                    <td class="contentsub" height="30">Total Tonnage</td>
                    <td class="contentsub" height="30">Freight Amount</td>
                    </tr>
                    <c:forEach items="${districtSummaryList}" var="distList">
                        <c:set var="total" value="${total+1}"></c:set>
                        <c:set var="totalFreight" value="0"/>
                        <c:set var="grandTotal" value="0"/>
                        <c:set var="tripDate" value="0"/>
                        <c:set var="displayStatus" value="0"/>
                        <%
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text2";
                                    } else {
                                        classText = "text1";
                                    }
                        %>
                        <c:if test = "${dsl.tripDate != tripDate}" >
                         <c:set var="tripDate" value="${dsl.tripDate}"/>
                         <c:set var="totalFreight" value="0"/>
                         <c:set var="displayStatus" value="0"/>
                         <c:if test = "${tripDate != '0'}" >
                            <c:set var="displayStatus" value="1"/>
                        </c:if>
                     </c:if>
                     <c:set var="totalFreight" value="${dsl.totalFreightAmount + totalFreight}"/>
                     <c:set var="grandTotal" value="${dsl.totalFreightAmount + grandTotal}"/>
                        <tr>
                            <td class="<%=classText%>"  height="30" align="left"><%=index+1%></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${distList.tripDate}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${distList.destination}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${distList.distance}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${distList.totalTonnage}"/></td>
                            <td class="<%=classText%>"  height="30"><c:out value="${distList.totalFreightAmount}"/></td>

                        </tr>
                        <c:if test = "${displayStatus == 1}" >
                            <tr>
                                <td class="text1" colspan="2">&nbsp;</td>
                                <td class="text1" align="center"  height="30">Subtotal</td>
                                <td class="text1"  colspan="2" align="right"  height="30"><c:out value="${totalFreight}"/></td>
                            </tr>
                        </c:if>
                        <%index++;%>
                    </c:forEach>
                             <tr>
                                <td class="text1" colspan="3" align="right"  height="30">grand Total</td>
                                <td class="text1"  colspan="2" align="right"  height="30"><c:out value="${grandTotal}"/></td>
                            </tr>
                </table>

                <%
                            }%>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
