<%@page import="ets.domain.billing.business.BillingTO"%>
<%@page import="java.util.ArrayList"%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

      <script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        $(".datepicker").datepicker({
            dateFormat:'dd-mm-yy',
            changeMonth: true, changeYear: true
        });
    });
</script>
<script type="text/javascript">
    
    

                function setBillingOrder(sno, obj, value) {
                    var cnoteId = "";
                    if (obj.checked == true) {
                        var temp1 = "";
                        var cntr = 0;
                        var selectedConsignment = document.getElementsByName('consignmentId');
                        var grNoSet = document.getElementsByName('grNoSet');
                        for (var i = 0; i < selectedConsignment.length; i++) {
                            if (selectedConsignment[i].checked == true) {
                                if (cntr == 0) {
                                    temp1 = grNoSet[i].value;
                                } else {
                                    temp1 = temp1 + " , " + grNoSet[i].value;
                                }
                                cntr++;
                            }
                        }
                        document.getElementById("grNoSpan").innerHTML = temp1;


                        document.getElementById("tripIdStatus" + sno).value = value;
                        document.getElementById("tripSheetId").value = value;
                    } else {

                        var temp1 = "";
                        var cntr = 0;
                        var selectedConsignment = document.getElementsByName('consignmentId');
                        var grNoSet = document.getElementsByName('grNoSet');
                        for (var i = 0; i < selectedConsignment.length; i++) {
                            if (selectedConsignment[i].checked == true) {
                                if (cntr == 0) {
                                    temp1 = grNoSet[i].value;
                                } else {
                                    temp1 = temp1 + " , " + grNoSet[i].value;
                                }
                                cntr--;
                            }
                        }

                        document.getElementById("grNoSpan").innerHTML = temp1;
                        document.getElementById("tripIdStatus" + sno).value = 0;
                        document.getElementById("tripSheetId").value = 0;
                    }

                }
    
    function setValues() {
        if ('<%=request.getAttribute("fromDate1")%>' != 'null') {
            document.getElementById('fromDate1111').value = '<%=request.getAttribute("fromDate1")%>';
        }
        if ('<%=request.getAttribute("toDate")%>' != 'null') {
            document.getElementById('toDate1111').value = '<%=request.getAttribute("toDate")%>';
        }
        if ('<%=request.getAttribute("type")%>' != 'null') {
            document.getElementById('type').value = '<%=request.getAttribute("type")%>';
        }
        if ('<%=request.getAttribute("shippingBillNo")%>' != 'null') {
            document.getElementById('shippingBillNo').value = '<%=request.getAttribute("shippingBillNo")%>';
        }
        if ('<%=request.getAttribute("billOfEntryNo")%>' != 'null') {
            document.getElementById('billOfEntryNo').value = '<%=request.getAttribute("billOfEntryNo")%>';
        }
        
        if ('<%=request.getAttribute("customerName")%>' != 'null') {
            document.getElementById('customer').value = '<%=request.getAttribute("customerName")%>';
        }
        if ('<%=request.getAttribute("customerId")%>' != 'null') {
            document.getElementById('billingParty').value = '<%=request.getAttribute("customerId")%>';
        }

        if ('<%=request.getAttribute("movementType")%>' != 'null') {
            document.getElementById('movementType').value = '<%=request.getAttribute("movementType")%>';
        }

    }

    function submitPage() {
        var customerId = document.getElementById("billingParty").value;
//        if (customerId == "") {
//            alert("Please select customer and then proceed");
//            document.getElementById("Customer").focus();
//            return;
//        } else {
            document.enter.action = '/throttle/viewClosedRepoOrderForBilling.do';
            document.enter.submit();
//        }

    }
    function uncheckAll()
        {
           var selectedConsignment = document.getElementsByName('consignmentId');
            for (var i = 0; i < selectedConsignment.length; i++) {
              selectedConsignment[i].checked = false;  
            } 
    } 
    function generateBill() {
        var temp = "";
        var cntr = 0;
        var selectValue = 0;
        var obj=true;
        var selectedConsignment = document.getElementsByName('consignmentId');
        for (var i = 1; i <= selectedConsignment.length; i++) {
            if (document.getElementById("consignmentId"+i).checked == true) {
                    var panNo=document.getElementById("panNo"+i).value; 
                     var organizationId=document.getElementById("organizationId"+i).value;
                    var gstNo=document.getElementById("gstNo"+i).value;
                    if (organizationId == ''){
                        obj.checked = false;
                        alert("Invoice cannot process since Organization for customer is Empty");
                        return;
                    }
                    if (organizationId == '1' && panNo == ''){
                        obj.checked = false;
                        alert("Invoice cannot process since Organization is individual,Pan No for customer is Empty");
                        return;
                    }
                    if (organizationId == '2' && gstNo == '' && panNo == ''){
                        obj.checked = false;
                        alert("Invoice cannot process since Organization is company,GST No,Pan No for customer is Empty");
                        return;
                    }
                    if(panNo== ''){
                    obj = false;
                        alert("Invoice cannot process since PAN no for customer is Empty");
                        uncheckAll();
                        return;}
                    if (obj == true) {
                        var organizationName = "";
                    if (organizationId == '1'){
                      organizationName = "Individual";  
                    }else{
                      organizationName = "Company";     
                    }
                    // alert("Org Name:"+organizationName+"  Pan No:"+panNo+"  GST NO:"+gstNo);
              
                        }
                    
                
                if (cntr == 0) {
                    temp = document.getElementById("consignmentId"+i).value;
                } else {
                    temp = temp + "," + document.getElementById("consignmentId"+i).value;
                }
                selectValue = i;
                cntr++;
            }
        }
         if (obj == true) {
	         alert("Org Name:"+organizationName+"  Pan No:"+panNo+"  GST NO:"+gstNo);
     }
//        alert(selectValue)
//                alert("temp"+temp);
        document.enter.action = '/throttle/gererateRepoOrderBill.do?tripIds=' + temp + '&consignmentOrderId=' + temp;
        document.enter.submit();
    }
    function selectCheckCondition() {
        if (document.getElementById("select").checked = true) {
            document.getElementById("select").value = "selected";
        } else {
            document.getElementById("select").value = "";
        }



    }
    </script>
   <script>
// (function () {
  //                          var grNo = document.getElementById('grNo');
    //                        grNo.addEventListener('keypress', function (event) {
      //                          if (event.keyCode == 13 || event.keyCode == 9) {
        //                            event.preventDefault();
          //                          if (grNo != '') {
            //                            submitPage('FETCH DATA');
              //                      }
                //                }
                  //          });
                    //    }());
                    </script>  
    <script>
    $(document).ready(function () {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#Customer').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getConsignorName.do",
                    dataType: "json",
                    data: {
                        consignorName: request.term,
                        customerId: document.getElementById('Customer').value

                    },
                    success: function (data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function (data, type) {
                        console.log(type);
                    }

                });
            },
            minLength: 1,
            select: function (event, ui) {
                var value = ui.item.Name;
                $('#Customer').val(value);
//                        $('#consignorPhoneNo').val(ui.item.Mobile);
//                        $('#consignorAddress').val(ui.item.Address);
                $('#customerId').val(ui.item.custId);
                $('#billingParty').val(ui.item.custId);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });
</script>
    
    <script type="text/javascript">//
//    $(document).ready(function() {
//        $('#Customer').autocomplete({
//            source: function(request, response) {
//                $.ajax({
//                    url: "/throttle/getCustomerName.do",
//                    dataType: "json",
//                    data: {
//                        customerCode: request.term,
//                        customerName: document.getElementById('Customer').value
//                    },
//                    success: function(data, textStatus, jqXHR) {
//                        var items = data;
//                        response(items);
//                    },
//                    error: function(data, type) {
//                        console.log(type);
//                    }
//                });
//            },
//            minLength: 1,
//            select: function(event, ui) {
//                $("#Customer").val(ui.item.customerName);
//                var $itemrow = $(this).closest('tr');
//                var value = ui.item.customerName;
//                var tmp = value.split('-');
//                $itemrow.find('#customerId').val(tmp[0]);
//                $itemrow.find('#Customer').val(tmp[1]);
//                $("#billingParty").val(tmp[0]);
//                return false;
//            }
//        }).data("autocomplete")._renderItem = function(ul, item) {
//            var itemVal = item.customerName;
//
//            var temp = itemVal.split('-');
//            itemVal = '<font color="green">' + temp[1] + '</font>';
//            return $("<li></li>")
//                    .data("item.autocomplete", item)
//                    .append("<a>" + itemVal + "</a>")
//                    .appendTo(ul);
//        };
//
//
//    });
//
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Primary Billing Order</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Primary Billing Order</a></li>
            <li class="active">Generate Billing (Report& DSO)</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
<body onload="setValues();">
    <form name="enter" action=""  method="post">
        <%--<%@ include file="/content/common/path.jsp" %>--%>
        <%@ include file="/content/common/message.jsp" %>
                            <table class="table table-info mb30 table-hover" style="width:100%">
                                    <tr height="30"   ><td colSpan="4" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">Bill Generation</td></tr>
                                <tr>
                                    <td>Bill of Entry No</td>
                                    <td height="30"><input name="billOfEntryNo" id="billOfEntryNo" type="text" class="form-control" style="width:240px;height:40px;" value="<c:out value="${billOfEntryNo}"/>" ></td>
                                    <td>Shiping Bill No</td>
                                    <td height="30"><input name="shipingBillNo" id="shipingBillNo" type="text" class="form-control" style="width:240px;height:40px;" value="<c:out value="${shippingBillNo}"/>" ></td>
                                <tr>
                                <tr>
                                    <td><font color="red">*</font>Billing Party</td>
                                    <td height="30">
                                        <input type="hidden" name="billingParty" id="billingParty" value="<c:out value="${customerId}"/>"/>
                                        <input type="text" name="Customer" id="Customer" class="form-control" style="width:240px;height:40px;" value="<c:out value="${customerName}"/>" />
                                    </td>
                                    <td>Consignment Order No</td>
                                    <td height="30"><input name="orderNo" id="orderNo" type="text" class="form-control" style="width:240px;height:40px;" value="" ></td>
                                </tr>
                                <tr>
                                    <td>From Date</td>
                                    <td height="30"><input name="fromDate" id="fromDate" type="text"  style="width:240px;height:40px;" class="datepicker" value="" ></td>
                                    <td>To Date</td>
                                    <td height="30"><input name="toDate" id="toDate" type="text"  style="width:240px;height:40px;" class="datepicker" value="" ></td>
                                </tr>
                                <tr>
                                    <td>Priority</td>
                                    <td height="30"><select name="type" id="type" class="form-control" style="width:240px;height:40px;"  >
                                            <option value="">All</option>
                                            <option value="1">urgent</option>
                                            <option value="2">Moderate</option>
                                            <option value="3">Normal</option>
                                        </select></td>
                                    <td>Order Type</td>
                                    <td height="30">
                                        <select name="movementType" id="movementType"  class="form-control" style="width:240px;height:40px;">
                                            <option value="">--Select---</option>
                                            <c:forEach items="${movementTypeList}" var="proList">
                                                <option value="<c:out value="${proList.movementTypeId}"/>"><c:out value="${proList.movementType}"/></option>
                                            </c:forEach>

                                        </select>

                                    </td>
                                </tr>
                                <tr>
                                    <td><font color="red">*</font>Gr No</td>
                            <td height="30">
                                <input name="grNo" id="grNo" type="text" class="form-control"   style="width:240px;height:40px;" value="<c:out value="${grNo}"/>">
                            </td>
                                    <td colspan="2"><center><input type="button" class="btn btn-info"   value="FETCH DATA" onclick="submitPage();"></center></td>
                                </tr>
                            </table>
        <table style="width: 100%" align="center" border="0" id="header" class="sortable">
            <tr>
                <td>
                    <span id="grNoSpan"></span>
                </td>
            </tr>
<input type="hidden" name="checkDate1" id="checkDate1" value="1"/>
        </table>
 <script>
 (function () {
                            var grNo = document.getElementById('grNo');
                            grNo.addEventListener('keypress', function (event) {
                                if (event.keyCode == 13 || event.keyCode == 9) {
                                    event.preventDefault();
                                    if (grNo != '') {
                                        submitPage('FETCH DATA');
                                    }
                                }
                            });
                        }());
                    </script> 

        <%
        if((ArrayList)request.getAttribute("closedTrips")!=null){
            String fromDate=(String)request.getAttribute("fromDate");
            String toDate=(String)request.getAttribute("toDate");
        ArrayList list=(ArrayList)request.getAttribute("closedTrips");
        %>
        <%if(list.size()>0){%>

               <table class="table table-info mb30 table-hover"  id="table" style="width:100%">
                    <thead>
                        <tr>
                    <th>S No</th>
                    <th>Select</th>
                        <%--                        <th>Bill of Entry</th>
                                                <th>Shipping Bill No</th>--%>
                    <th>Order Type</th>
                    <th>Billing Party</th>
                    <th>Customer Name</th>
                        <%--    <th>Revenue</th>  --%>
                    <th>Route Name</th>
                    <th>GR NO</th>
                    <th>C-Notes</th>
                              <th>Container</th> 
                    <th>Trip Code</th>
                        <%--          <th>POD Status</th> --%>

                    <%--                        <th>Tonnage</th> --%>

                    <th>Status</th>
                        <%--        <th>Action</th>  --%>

                </tr>
            </thead>
            <tbody>
                <%int index=0;
                int sno=1;
                String classText ="";
                %>
                <%
                     String customerName="";
                     String billingParty="";
                     String container="";
                     String cnoteNo="";
                     String consignmentOrderId="";
                     String tripId="";
                     String regNo="";
                     String vehicleType="";
                     String routeName="";
                     String vehicleTypeName="";
                     String tripStartDate="";
                     String tripEndDate="";
                     String tripstartkm="";
                     String tripendkm="";
                     String totalWeight="";
                     String destinationName="";
                     String freightAmt="";
                     String billingType="";
                     String customerId="";
                     String cnoteId="";
                     String checkTripId="";
                     String totalkmrun="";
                     String podStatus="";
                     String reeferRequired="";
                     String timeElapsedValue="";
                     String billOfEntry="";
                     String shippingBillNo="";
                     String movementType="";
                     String panNo="";
                     String containerNo="";
                     String organizationId="";
                     String customerType="";
                         String gstNo="";
                     for(int i=0;i<list.size();i++){
                     String tripCodes[] = null;
                     String tripIds[] = null;
                     String tripCode="";
                      String grNo="";
                     int oddEven = index % 2;
                      if (oddEven > 0) {
                                 classText = "text2";
                             } else {
                                 classText = "text1";
                             }
                         BillingTO opto=(BillingTO)list.get(i);
                         customerName=opto.getCustomerName();
                         cnoteNo=opto.getConsignmentNoteNo();
                         tripId=opto.getTripId();
                         if(opto.getTripId().contains(",")){
                         tripCodes = opto.getTripCode().split(",");
                         tripIds=opto.getTripId().split(",");
                            for(int k=0; k<tripCodes.length; k++){
                             if(tripCode == ""){
                             tripCode="<a href='#' onclick='viewTripDetails("+tripIds[k]+")'>"+tripCodes[k]+"</a>,<br>";
                             }else{
                             if(k == tripCodes.length-1){
                             tripCode=tripCode+"<a href='#' onclick='viewTripDetails("+tripIds[k]+")'>"+tripCodes[k]+"</a>";
                             }else{
                             tripCode=tripCode+"<a href='#' onclick='viewTripDetails("+tripIds[k]+")'>"+tripCodes[k]+"</a>,<br>";
                             }
                             }

                            }
                         }else{
                         tripCode="<a href='#' onclick='viewTripDetails("+opto.getTripId()+")'>"+opto.getTripCode()+"</a>";
                         }
                         checkTripId=opto.getTripId();
                         destinationName=opto.getConsigmentDestination();
                         freightAmt=opto.getFreightCharges();
                         billingType=opto.getBillingTypeId();
                         customerId=opto.getCustomerId();
                         totalkmrun=opto.getTotalKmRun();
                         totalWeight=opto.getTotalWeightage();
                         podStatus=opto.getStatus();
                         routeName=opto.getRouteInfo();
                         reeferRequired=opto.getTempReeferRequired();
                         consignmentOrderId=opto.getConsignmentOrderId();
                         billingParty=opto.getBillingParty();
                         container=opto.getContainerTypeName();
                         containerNo=opto.getContainerNo();
                         timeElapsedValue=opto.getTimeElapsedValue();
                         billOfEntry=opto.getBillOfEntryNo();
                         shippingBillNo=opto.getShipingBillNo();
                         movementType=opto.getMovementType();
                         panNo=opto.getPanNo();
                          gstNo=opto.getGstNo();
                         organizationId=opto.getOrganizationId();
                         customerType=opto.getCompanyType();
                         String[] grNos=null;
                       //  grNo=opto.getGrNo();
                         if(opto.getGrNo().contains(",")){
                         grNos = opto.getGrNo().split(",");
                         tripIds=opto.getTripId().split(",");
                            for(int k=0; k<grNos.length; k++){
                             if(grNo == ""){
                             grNo="<a href='#' onclick='printTripGrn("+tripIds[k]+")'>"+grNos[k]+"</a>,<br>";
                             }else{
                             if(k == grNos.length-1){
                             grNo=grNo+"<a href='#' onclick='printTripGrn("+tripIds[k]+")'>"+grNos[k]+"</a>";
                             }else{
                             grNo=grNo+"<a href='#' onclick='printTripGrn("+tripIds[k]+")'>"+grNos[k]+"</a>,<br>";
                             }
                             }

                            }
                         }else{
                         grNo="<a href='#' onclick='printTripGrn("+opto.getTripId()+")'>"+opto.getGrNo()+"</a>";
                         }
                %>

                <tr>
                    <td  height="30">
                        <% if(Integer.parseInt(timeElapsedValue )> -2) {%>
                        <font color="green">   <%=sno%></font>
                            <% }else if (Integer.parseInt(timeElapsedValue )> -5){ %>
                        <font color="orange">    <%=sno%></font>
                            <%}else { %>
                        <font color="red">  <%=sno%></font>
                            <% }%>
                    </td>
                    <td  height="30">
                        <input type="hidden" name="panNo" id="panNo<%=sno%>" value="<%=panNo%>"/>
                        <input type="hidden" name="gstNo<%=sno%>" id="gstNo<%=sno%>"value="<%=gstNo%>"/>
                        <input type="hidden" name="organizationId<%=sno%>" id="organizationId<%=sno%>"value="<%=organizationId%>"/>
                        <input type="hidden" name="customerType<%=sno%>" id="customerType<%=sno%>"value="<%=customerType%>"/>
                        <input type="hidden" name="tripId" id="tripId<%=sno%>" value="<%=tripId%>"/>
                        <input type="hidden" name="customerName<%=sno%>" value="<%=billingParty%>"/>
                        <input type="hidden" name="customerId<%=sno%>" id="customerId<%=sno%>"value="<%=customerId%>"/>
                        <input type="hidden" name="cnoteNo<%=sno%>" value="<%=cnoteNo%>"/>
                        <input type="hidden" name="destinationName<%=sno%>" value="<%=destinationName%>"/>
                        <input type="hidden" name="freightAmt<%=sno%>" value="<%=freightAmt%>"/>
                        <input type="hidden" name="billingType<%=sno%>" value="<%=billingType%>"/>
                        <input type="hidden" name="customerId<%=sno%>" value="<%=customerId%>"/>
                        <input type="hidden" name="totalweights<%=sno%>" value="<%=totalWeight%>"/>
                        <input type="hidden" name="tripDate<%=sno%>" value="<%=tripStartDate%>"/>
                        <input type="checkbox" name="consignmentId" id="consignmentId<%=sno%>" value="<%=tripId%>" onclick="setBillingOrder('<%=sno%>', this, this.value)"/>
                        <input type="hidden" name="grNoSet" id="grNoSet<%=sno%>" value="<%=grNo%>"/>
                        <input type="hidden" name="consignmentOrderIdStatus" id="consignmentOrderIdStatus<%=sno%>" value="0" />
                        <input type="hidden" name="tripIdStatus" id="tripIdStatus<%=sno%>" value="0" />
                        <input type="hidden" name="routeName<%=sno%>" value="<%=routeName%>"/>
                        <input type="hidden" name="rowNo<%=sno%>" value="<%=sno%>"/>
                        <input type="hidden" name="totalkmrun<%=sno%>" value="<%=totalkmrun%>"/>
                    </td>
                    <%--                        <td  height="30" ><%=billOfEntry%></td>

                        <td  height="30" ><%=shippingBillNo%></td>--%>
                    <td  height="30" ><%=movementType%></td>
                    <td  height="30" ><%=billingParty%></td>
                    <td  height="30" ><%=customerName%></td>
                    <%--       <td  height="30" style="width:120px;text-align: right"><%=freightAmt%></td> --%>
                    <td  height="30" style="width:120px"><%=routeName%></td>
                    <td  height="30" style="width:120px"><%=grNo%></td>
                    <td  height="30" style="width:120px">
                        <% if(Integer.parseInt(timeElapsedValue )> -2) {%>

                        <font color="green">   <%=cnoteNo%></font>
                            <% }else if (Integer.parseInt(timeElapsedValue )> -5){ %>
                        <font color="orange">   <%=cnoteNo%></font>
                            <%}else { %>
                        <font color="red">   <%=cnoteNo%></font>
                            <% }%>
                    </td>
                    <td  height="30" style="width:120px"><%=containerNo%></td>

                    <td  height="30" style="width:120px"><%=tripCode%></td>
                    <%--        <td  height="30" >

                            <% if("0".equals(podStatus)){%>
                            <a href="viewTripPod.do?tripSheetId=<%=tripId%>">  <img src="images/Podinactive.png" alt="Y"   title="click to upload pod"/></a>
                                <% } else if("1".equals(podStatus)){%>
                            <a href="viewTripPod.do?tripSheetId=<%=tripId%>">  <img src="images/Podactive.png" alt="Y"   title="click to upload pod"/></a>
                                <% }%>
                        </td>  --%>

                    <%--                        <td  height="30" style="width:120px;text-align: right"><%=totalWeight%></td>--%>

                    <td  height="30"><img src="/throttle/images/icon_closed.png" alt=""/></td>
                        <%--     <td  height="30">Settled</td>  --%>


                </tr>
                <%
                 cnoteId="500000";
                sno++;
                index++;
                %>
                <%
            }
                %>

            </tbody>
        </table>
        <center>
            <input type="hidden" name="consignmentOrderId" id="consignmentOrderId" />
            <input type="hidden" name="custId" id="custId" value="<%=customerId%>" />
            <input type="hidden" name="tripSheetId" id="tripSheetId" value="" />
            <input type="button" class="btn btn-info"  value="GENERATE BILL" onclick="generateBill();">
        </center>

        <input type="hidden" name="billingOrderId" id="billingOrderId" value=""/>
        <input type="hidden" name="invoicefromDate" value="<%=fromDate%>"/>
        <input type="hidden" name="invoicetoDate" value="<%=toDate%>"/>
        <!--          <br>
                <center>
                    <input type="button" class="button" value="Generate Bill" name="Generate Bill" onClick="generateBill();">
                </center>-->
        <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5" selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>
        <%}}%>
        <script type="text/javascript">
                var customerName = '<c:out value="${customerName}"/>';
                var customerId = '<c:out value="${customerId}"/>';

                if (customerId != null & customerId != "") {
                    document.getElementById("customerId").value = customerId;
                }
                if (customerName != null & customerName != "") {
                    document.getElementById("Customer").value = customerName;
                }

                function viewTripDetails(tripId) {
                    window.open('/throttle/viewTripSheetDetails.do?tripId=' + tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
                }

                function setBillingOrder11(sno, obj, value) {
                    var cnoteId = "";
//                    alert(sno)
                  
                  alert("new1111");
                    var panNo=document.getElementById("panNo"+sno).value;
                     var organizationId=document.getElementById("organizationId"+sno).value;
                    var gstNo=document.getElementById("gstNo"+sno).value;
                    var customerType=document.getElementById("customerType"+sno).value;
                    if (customerType == ''){
                        obj.checked = false;
                        alert("Invoice cannot process since companyType for customer is Empty");
                        return;
                    }
                    if (organizationId == ''){
                        obj.checked = false;
                        alert("Invoice cannot process since Organization for customer is Empty");
                        return;
                    }
                    if (organizationId == '' && panNo == '' && gstNo == ''){
                        obj.checked = false;
                        alert("Invoice cannot process since Organization,Pan No,GST No for customer is Empty");
                        return;
                    }
                    if (organizationId == '2' && gstNo == ''){
                        obj.checked = false;
                        alert("Invoice cannot process since Organization is company,GST No for customer is Empty");
                        return;
                    }
                    if(panNo== ''){
                    obj.checked = false;
                        alert("Invoice cannot process since PAN no for customer is Empty");
                        return;}
                 //   if (obj.checked == true) {
                        var organizationName = "";
                    if (organizationId == '1'){
                      organizationName = "Individual";  
                    }else{
                      organizationName = "Company";     
                    }
                     alert("Org Name:"+organizationName+"  Pan No:"+panNo+"  GST NO:"+gstNo);
                        var temp1 = "";
                        var cntr = 0;
                        var selectedConsignment = document.getElementsByName('consignmentId');
                        var grNoSet = document.getElementsByName('grNoSet');
                        for (var i = 0; i < selectedConsignment.length; i++) {
                            if (selectedConsignment[i].checked == true) {
                                if (cntr == 0) {
                                    temp1 = grNoSet[i].value;
                                } else {
                                    temp1 = temp1 + " , " + grNoSet[i].value;
                                }
                                cntr++;
                            }
                        }
                        document.getElementById("grNoSpan").innerHTML = temp1;


                        document.getElementById("tripIdStatus" + sno).value = value;
                        document.getElementById("tripSheetId").value = value;
//                    } else {
//
//                        var temp1 = "";
//                        var cntr = 0;
//                        var selectedConsignment = document.getElementsByName('consignmentId');
//                        var grNoSet = document.getElementsByName('grNoSet');
//                        for (var i = 0; i < selectedConsignment.length; i++) {
//                            if (selectedConsignment[i].checked == true) {
//                                if (cntr == 0) {
//                                    temp1 = grNoSet[i].value;
//                                } else {
//                                    temp1 = temp1 + " , " + grNoSet[i].value;
//                                }
//                                cntr--;
//                            }
//                        }
//
//                        document.getElementById("grNoSpan").innerHTML = temp1;
//                        document.getElementById("tripIdStatus" + sno).value = 0;
//                        document.getElementById("tripSheetId").value = 0;
//                    }

                }



        </script>

    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
