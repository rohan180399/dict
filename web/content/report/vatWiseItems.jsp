

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
         <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
    </head>
    <script>
function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}

       function submitPage(){
            var chek=validation();
            
            if(chek=='true'){  
           document.receivedStock.action='/throttle/handleTaxWiseItems.do';
           document.receivedStock.submit();
           }
           }
       function validation(){

            if( (document.receivedStock.vendorId.value == "") && (document.receivedStock.paplCode.value == "") && (document.receivedStock.itemName.value == "")  ){
                alert("Please enter Vendor Name or Vat or Papl Code or Item Name");
                document.receivedStock.paplCode.focus();
                document.receivedStock.paplCode.select();
                return'false';
            }    
            return 'true';
        }
        function newWindow(indx){
            var supplyId=document.getElementsByName("supplyId");
            window.open('/throttle/viewGRN.do?supplyId='+supplyId[indx].value, 'PopupPage', 'height=450,width=700,scrollbars=yes,resizable=yes');
        }
         function setValues(){           

                 if('<%=request.getAttribute("vendorId")%>'!='null'){
                document.receivedStock.vendorId.value='<%=request.getAttribute("vendorId")%>';                                
                }
                 if('<%=request.getAttribute("vat")%>'!='null'){
                document.receivedStock.vat.value='<%=request.getAttribute("vat")%>';                                
                }
                 if('<%=request.getAttribute("paplCode")%>'!='null'){
                document.receivedStock.paplCode.value='<%=request.getAttribute("paplCode")%>';                                
                }
                 if('<%=request.getAttribute("itemName")%>'!='null'){
                document.receivedStock.itemName.value='<%=request.getAttribute("itemName")%>';                                
                }
            }
            
            
    function printPag()
    {       
        var DocumentContainer = document.getElementById('print');
        var WindowObject = window.open('', "TrackHistoryData", 
            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        WindowObject.close();   
    }                        
            
            
    </script>
    <body onload="setValues();">
        <form name="receivedStock" method="post">
            <%@ include file="/content/common/path.jsp" %>           
            <%@ include file="/content/common/message.jsp" %>
 
<table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:850;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">VAT Wise Item Report</li>
    </ul>
    <div id="first">
    <table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
    <tr>
        <td height="30">Vendor</td>
        <td height="30"><select name="vendorId" class="form-control" style="width:125px">
                            <option value="">-select-</option>
                            <c:if test = "${vendorList != null}" >
                                <c:forEach items="${vendorList}" var="vendor">
                                    <option value="<c:out value="${vendor.vendorId}"/>"><c:out value="${vendor.vendorName}"/></option>
                                </c:forEach>
                            </c:if>

                    </select></td>
        <td height="30">VAT</td>
        <td height="30"><select name="vat" class="form-control" style="width:125px">
                            <option value="">-select-</option>
                            <c:if test = "${vatList != null}" >
                                <c:forEach items="${vatList}" var="vat">
                                    <option value='<c:out value="${vat.vat}"/>' ><c:out value="${vat.vat}"/></option>
                                </c:forEach>
                            </c:if>
                        </select></td>
        <td height="30">PAPL Code</td>
        <td height="30"><input type="text" name="paplCode" class="form-control" value=""></td>
        <td height="30">Item Name</td>
        <td height="30"><input type="text" name="itemName" class="form-control" value="" ></td>
        <td height="30" rowspan="2" valign="middle"><input type="button" name="" value="Search" class="button" height="30" onclick="submitPage();" >  </td>
    </tr>
   
    </table>
    </div></div>
    </td>
    </tr>
    </table>             
           
                <c:set var="Amount" value="0" />
                 <c:if test = "${itemList != null}" >
<div id="print" >
<style>    
.text1 {
font-family:Tahoma;
font-size:10px;
color:#333333;
background-color:#E9FBFF;
padding-left:10px;
}
.text2 {
font-family:Tahoma;
font-size:10px;
color:#333333;
background-color:#FFFFFF;
padding-left:10px;
}    
.contenthead {
background-image:url(/throttle/images/button.gif);
background-repeat:repeat-x;
height:15px;
font-family:Tahoma;
padding-left:5px;
font-size:11px;
font-weight:bold;
color:#0070D4;
text-align:left;
padding-bottom:8px;
}
.contentsub {
background-image:url(/throttle/images/button.gif);
background-repeat:repeat-x;
height:15px;
font-family:Tahoma;
padding-left:5px;
font-size:11px;
font-weight:bold;
color:#0070D4;
text-align:left;
padding-bottom:8px;
}
</style>    
            <table width="800" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" height="30" class="border">
                   <!--DWLayoutTable-->
                   <tr>
                    <td class="contenthead" height="30" colspan="8" align="center"  >VAT Wise Items</td>
                </tr>
                <tr>
                    <td  class="contentsub" height="30" >Sno</td>
                    <td  class="contentsub" height="30" >Mfr Code</td>
                    <td  class="contentsub" height="30" >PAPL Code</td>
                    <td  class="contentsub" height="30" >Item Name</td>
                    <td  class="contentsub" height="30" >Vendor Name</td>
                    <td  class="contentsub" height="30" >Tax</td>
                    <td  class="contentsub" height="30" >Price</td>                                        
                </tr>  
                  <% int index = 0;%> 
                    <c:forEach items="${itemList}" var="stock"> 
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>	
                        
                <tr>
                    <td class="<%=classText%>" height="30" > <%= index+1 %> </td>
                    <td class="<%=classText%>" height="30" ><c:out value="${stock.mfrCode}"/></td>
                    <td class="<%=classText%>" height="30" ><c:out value="${stock.paplCode}"/></td>
                    <td class="<%=classText%>" height="30" ><c:out value="${stock.itemName}"/></td>
                    <td class="<%=classText%>" height="30" ><c:out value="${stock.vendorName}"/></td>
                    <td class="<%=classText%>" height="30" ><c:out value="${stock.tax}"/></td>                                     
                    <td class="<%=classText%>" height="30" ><c:out value="${stock.itemPrice }"/></td>                                     
                                
                </tr>              
             <% index++;%>
                    </c:forEach>
                        
                       
                </table>                
</div>                             
            </c:if>
            <br>
<center>
    <input type="button" class="button" name="print" value="print" onClick="printPag();" >
</center>             

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
