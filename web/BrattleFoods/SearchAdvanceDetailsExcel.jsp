<%-- 
    Document   : SearchAdvanceDetailsExcel
    Created on : Nov 4, 2013, 5:39:57 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.* "%>
<%@ page import="java.text.* "%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "Trip_Sheet_Report-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
            %>
    </head>
    <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }

        </style>
    <body>
        <table width="980"  border="0" id="table" class="sortable">
                <thead>
                    <tr height="30">

                        <td rowspan="2" class="contenthead">S.No</td>
                        <td rowspan="2" class="contenthead">TripSheetId</td>
                        <td rowspan="2" class="contenthead">Trip Date &amp; Time</td>
                        <td rowspan="2" class="contenthead">Estimated Delivery Date</td>
                        <td rowspan="2" class="contenthead">Transit Days</td>
                        <td rowspan="2" class="contenthead">Vechile No</td>
                        <td rowspan="2" class="contenthead">Driver No</td>
                        <td rowspan="2" class="contenthead">Account No</td>
                        <td rowspan="2" class="contenthead">Last Advance </td>
                        <td rowspan="2" class="contenthead">Advance Account</td>
                        <td rowspan="2" class="contenthead">Status</td>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="text1">1</td>
                    <td class="text1">12411</td>
                    <td class="text1">19:11:2013 &amp; 7:00AM</td>
                    <td class="text1">21:11:2013 &amp; 5:00PM</td>
                    <td class="text1">2.5 Days</td>
                    <td class="text1">DL 19 AB 1099</td>
                    <td class="text1">Prem</td>
                    <td class="text1">139785462789456</td>
                    <td class="text1">10000</td>
                    <td class="text1">4500</td>
                    <td class="text1"><a href="/throttle/BrattleFoods/tripSheet.jsp?advance=4">Proceed</a></td>
                </tr>
                <tr>
                    <td class="text2">2</td>
                    <td class="text2">23231</td>
                    <td class="text2">10:11:2013 &amp; 7:00AM</td>
                    <td class="text2">13:11:2013 &amp; 5:00PM</td>
                    <td class="text2">2.5 Days</td>
                    <td class="text2">DL 18 AB 5056</td>
                    <td class="text2">Senthil </td>
                    <td class="text2">156894567894562</td>
                    <td class="text2">50000</td>
                    <td class="text2">1500</td>
                    <td class="text2"><a href="/throttle/BrattleFoods/tipSheet.jsp">Proceed</a></td>
                </tr>
                    <tr>
                        <td class="text1">3</td>
                        <td class="text1">13636</td>
                        <td class="text1">19:10:2013 &amp; 7:00AM</td>
                        <td class="text1">21:10:2013 &amp; 7:00PM</td>
                        <td class="text1">2.0 Days</td>
                        <td class="text1">TN 09 QX 9098</td>
                        <td class="text1">Kumar</td>
                        <td class="text1">896478945612354</td>
                        <td class="text1">12000</td>
                        <td class="text1">3000</td>
                        <td class="text1">Proceeded</td>
                    </tr>
                    <tr>
                        <td class="text2">4</td>
                        <td class="text2">12132</td>
                        <td class="text2">19:11:2013 &amp; 7:00AM</td>
                        <td class="text2">21:11:2013 &amp; 7:00PM</td>
                        <td class="text2">2.5 Days</td>
                        <td class="text2">DL 20 FG 2346</td>
                        <td class="text2">Tamil  </td>
                        <td class="text2">567889546598956</td>
                        <td class="text2">40200</td>
                        <td class="text2">1213</td>
                        <td class="text2"><a href="/throttle/BrattleFoods/tipSheet.jsp">Proceed</a></td>
                    </tr>
                    <tr>
                        <td class="text1">5</td>
                        <td class="text1">11232</td>
                        <td class="text1">19:11:2013 &amp; 7:00AM</td>
                        <td class="text1">21:11:2013 &amp; 10:00PM</td>
                        <td class="text1">2.5 Days</td>
                        <td class="text1">DL 34 RT 2446</td>
                        <td class="text1">selvam</td>
                        <td class="text1">568975468923597</td>
                        <td class="text1">58000</td>
                        <td class="text1">4345</td>
                        <td class="text1"><a href="/throttle/BrattleFoods/tipSheet.jsp">Proceed</a></td>
                    </tr>
                    <tr>
                        <td class="text2">6</td>
                        <td class="text2">12422</td>
                        <td class="text2">10:10:2013 &amp; 8:00AM</td>
                        <td class="text2">13:10:2013 &amp; 8:00AM</td>
                        <td class="text2">3.0 Days</td>
                        <td class="text2">DL 24 CG 5356</td>
                        <td class="text2">madhan</td>
                        <td class="text2">865234987456128</td>
                        <td class="text2">20000</td>
                        <td class="text2">2300</td>
                        <td class="text2">Proceeded</td>
                    </tr>
                </tbody>
            </table>
    </body>
</html>
