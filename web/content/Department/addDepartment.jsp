<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>

<script>
    function submitpage1()
    {
        if (isEmpty(document.dept.name.value))
        {
            alert("please enter the Name");
            document.dept.name.focus();
        } else if (isEmpty(document.dept.description.value))
        {
            alert("please enter the Description");
            document.dept.description.focus();
        }
        else if (isEmpty(document.dept.toMail.value))
        {
            alert("please enter the To Mail ID");
            document.dept.toMail.focus();
        }
        else if (isEmpty(document.dept.ccMail.value))
        {
            alert("please enter the CC Mail ID");
            document.dept.ccMail.focus();
        }
        else {
            document.dept.action = "/throttle/addDepartment.do";
            document.dept.submit();
        }
    }
    function setFocus() {
        document.dept.name.focus();
    }
</script>
<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
            setValues();
            getVehicleNos();">
</c:if>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="hrms.label.Department" text="default text"/></h2>
    <!--    <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="default text"/>:</span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.HRMS" text="default text"/></a></li>
                <li class="active"><spring:message code="hrms.label.Department" text="default text"/></li>
              <li class=""><spring:message code="hrms.label.AddDepartment" text="default text"/></li>


            </ol>
          </div>-->
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="setFocus();">
                <form name="dept"  method="post">
                    <table class="table table-dark mb30 table-hover" >
                        <thead>
                            <!--<tr>
                            <th colspan="2" align="center" height="20"><div ><spring:message code="hrms.label.AddDepartment" text="default text"/></div></th>
                            </tr>-->
                        </thead>
                        <tr>
                            <td  height="20"><font color="red">*</font><spring:message code="hrms.label.DepartmentName" text="default text"/></td>
                            <td  height="20"><input name="name" type="text" style="width:220px;height:40px;" class="form-control" value=""></td>
                            <!--</tr>
                            <tr>-->
                            <td  height="20"><font color="red">*</font><spring:message code="hrms.label.Description" text="default text"/></td>
                            <td  height="20"><textarea class="form-control" style="width:220px;height:40px;" name="description"></textarea></td>
                        </tr>
                        <tr>
                            <td  height="20"><font color="red">*</font>To Mail ID</td>
                            <td  height="20"><input type="text" name="toMail" id="toMail" style="width:220px;height:40px;" class="form-control" value=""></td>
                            <!--</tr>
                            <tr>-->
                            <td  height="20"><font color="red">*</font>CC Mail ID</td>
                            <td  height="20"><input type="text" name="ccMail" id="ccMail" style="width:220px;height:40px;" class="form-control" value=""></td>
                        </tr>
                    </table>
                    <br>
                    <center>
                        <input type="button" value="<spring:message code="hrms.label.ADD" text="default text"/>" class="btn btn-primary" onclick="submitpage1();">
                        &emsp;<input type="reset" class="btn btn-primary" value="<spring:message code="hrms.label.CLEAR" text="default text"/>">
                    </center>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
