<%-- 
    Document   : bankReceiptEntryPrint
    Created on : Oct 22, 2013, 11:15:43 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <link rel="stylesheet" href="/throttle/css/rupees.css"  type="text/css" />

        <%@ page import="ets.domain.renderservice.business.RenderServiceTO" %>
        <%@ page import="ets.domain.renderservice.business.JobCardItemTO" %>
        <%@ page import="ets.domain.problemActivities.business.ProblemActivitiesTO" %>
        <%@ page import="ets.domain.operation.business.OperationTO" %>


        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <%@ page import="java.util.*" %>
        <%@ page import="java.lang.Double" %>

        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>\
        <link rel="stylesheet" href="style.css" />
        <%@ page import="ets.domain.contract.business.ContractTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <title>Cash Payment Voucher</title>
    </head>

    <script>



        function print()
        {
            var DocumentContainer = document.getElementById("printDiv");
            var WindowObject = window.open('', "TrackHistoryData",
            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }

        function back()
        {
            window.history.back()
        }

        function printBankReceiptEntry(val){
            document.suggestedPS.action = '/throttle/printBankReceiptEntry.do?voucherCode='+val;
            document.suggestedPS.submit();
        }

    </script>
    <%--

    --%>

    <body>
        <form name="suggestedPS" method="post">

            <div id="printDiv" style="border: 2px solid">

                <table width="860" cellpadding="0" cellspacing="0" align="center" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; ">
                    <tr>
                        <td height="30" style="text-align:center; text-transform:uppercase;" colspan="4">Chettinad Logistics Private Limited - Karikkali<br>
                            <center><u> PLH-Receipt Voucher<u></center>
                                        </td>

                                        </tr>
                                        <tr>
                                            <td height="30" colspan="2" height="30" style="text-align:left; ">Voucher No:<c:out value="${voucherCode}"/></td>
                                            <%java.text.DateFormat df = new java.text.SimpleDateFormat("dd-MM-yyyy");%>
                                            <td height="30" colspan="2" height="30" style="text-align:left; ">Date:<%= df.format(new java.util.Date())%> </td>
                                        </tr>
                                        <tr>
                                            <td height="30" style="text-align:left; border-bottom:1px dashed;"><b>Account Head</b></td>
                                            <td height="30" style="text-align:left; border-bottom:1px dashed;"><b>Narration</b></td>
                                            <td height="30" style="text-align:left; border-bottom:1px dashed;"><b>Credits</b></td>
                                        </tr>
                                        <c:if test="${bankReceiptEntry != null}">
                                            <c:set var="creditTotal" value="0"/>
                                            <c:forEach items="${bankReceiptEntry}" var="BRE">
                                                <tr>
                                                    <c:if test="${BRE.accountsType == 'CREDIT'}">
                                                        <c:set var="creditTotal" value="${BRE.debitAmmount + creditTotal}"/>
                                                    </c:if>
                                                    <td height="30"><c:out value="${BRE.ledgerName}"/></td>
                                                    <td height="30"><c:out value="${BRE.remarks}"/></td>
                                                    <td height="30"><c:out value="${BRE.debitAmmount}"/></td>
                                                </tr>
                                            </c:forEach>
                                        </c:if>
                                        <tr>
                                            <td height="30" colspan="2"></td>
                                            <td height="30" colspan="2" style="text-align:left; border-bottom:1px dashed;"></td>
                                        </tr>
                                        <tr>
                                            <td height="30" colspan="2" style="text-align:right;">Total :</td>
                                            <td height="30" style="text-align:left; border-bottom:1px dashed;"><c:out value="${creditTotal}"/></td>
                                        </tr>
                                        <tr>
                                            <td height="90" colspan="2">Prepared By</td>
                                            <td height="90" >Checked By</td>
                                            <td height="90">Passed By</td>
                                        </tr>
                                        <tr align="center">
                                            <td colspan="4">
                                                <input align="center" type="button" onclick="print();" value = " Print "   />
                                            </td>
                                        </tr>
                                        </table>

                                        </div>
                                        <br>

                                        <c:if test = "${bankReceiptList != null}" >

                                            <table align="center" width="100%" border="0" id="table" class="sortable">
                                                    <thead>
                                                        <tr>
                                                            <th><h3>S.No</h3></th>
                                                            <th><h3>Entry Date</h3></th>
                                                            <th><h3>Voucher Code</h3></th>
                                                            <th><h3>Debit Ledger</h3></th>
                                                            <th><h3>Debit Amount</h3></th>
                                                            <th><h3>Credit Ledger</h3></th>
                                                            <th><h3>Credit Amount</h3></th>
                                                            <th><h3>Print</h3></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <% int index = 0;%>
                                                        <c:forEach items="${bankReceiptList}" var="BRL">
                                                            <%
                                                                        String classText = "";
                                                                        int oddEven = index % 2;
                                                                        if (oddEven > 0) {
                                                                            classText = "text1";
                                                                        } else {
                                                                            classText = "text2";
                                                                        }
                                                            %>
                                                            <tr>
                                                                <td class="<%=classText%>"  align="left"> <%= index + 1%> </td>
                                                                <td class="<%=classText%>"  align="left"> <c:out value="${BRL.accountEntryDate}"/> </td>
                                                                <td class="<%=classText%>" align="left"> <c:out value="${BRL.voucherCode}" /></td>
                                                                <td class="<%=classText%>"  align="left"> <c:out value="${BRL.debitLedgerName}"/> </td>
                                                                <td class="<%=classText%>"  align="left"> <c:out value="${BRL.debitAmount}"/> </td>
                                                                <td class="<%=classText%>"  align="left"> <c:out value="${BRL.creditLedgerName}"/> </td>
                                                                <td class="<%=classText%>"  align="left"> <c:out value="${BRL.creditAmount}"/> </td>
                                                                <td class="<%=classText%>"  align="left"><a href="" onclick="printBankReceiptEntry('<c:out value="${BRL.voucherCode}" />')">Print</a></td>
                                                            </tr>

                                                            <% index++;%>
                                                        </c:forEach>
                                                    </c:if>
                                                </tbody>
                                            </table>
                                        <br>
                                        <br>
                                        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                                        <div id="controls">
                                            <div id="perpage">
                                                <select onchange="sorter.size(this.value)">
                                                    <option value="5" selected="selected">5</option>
                                                    <option value="10">10</option>
                                                    <option value="20">20</option>
                                                    <option value="50">50</option>
                                                    <option value="100">100</option>
                                                </select>
                                                <span>Entries Per Page</span>
                                            </div>
                                            <div id="navigation">
                                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1,true)" />
                                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1,true)" />
                                            </div>
                                            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                                        </div>
                                        <script type="text/javascript">
                                            var sorter = new TINY.table.sorter("sorter");
                                            sorter.head = "head";
                                            sorter.asc = "asc";
                                            sorter.desc = "desc";
                                            sorter.even = "evenrow";
                                            sorter.odd = "oddrow";
                                            sorter.evensel = "evenselected";
                                            sorter.oddsel = "oddselected";
                                            sorter.paginate = true;
                                            sorter.currentid = "currentpage";
                                            sorter.limitid = "pagelimit";
                                            sorter.init("table",1);
                                        </script>
                                        </body>
                                        </html>