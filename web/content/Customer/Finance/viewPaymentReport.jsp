
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">

<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>


<script type="text/javascript">
    function setValues() {
        var temp = document.getElementById("vendorIdTemp").value;
        //alert(temp);
        var tempStr = temp.split("~");
        document.invoice.vendorId.value = tempStr[0];
        document.invoice.ledgerId.value = tempStr[1];
        submitPage();

    }

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
</script>

<script language="javascript">

    function submitPage() {
        if (document.getElementById("vendorIdTemp").value == "") {
            alert('please select vendor');
            document.getElementById("vendorIdTemp").focus();
        }
        if (document.getElementById("vendorIdTemp").value != "") {

            var vendorName = document.getElementById('vendorIdTemp').options[document.getElementById('vendorIdTemp').selectedIndex].text;
            document.invoice.vendorName.value = vendorName;
            //alert(document.invoice.customerId.value);
            document.invoice.action = "/throttle/paymentPurchaseDetails.do";
            document.invoice.submit();
        }
    }


</script>
<style>
    #index td {
        color:white;
    }
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="finance.label.Payment"  text="default text"/></h2>
    <div class="breadcrumb-wrapper">
       <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance"  text="default text"/></a></li>
            <li class="active"><spring:message code="finance.label.Payment"  text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="invoice" method="post" class="form-horizontal form-bordered">
                    <table class="table table-bordered" >
                        <tr id="index">
                            <td colspan="6"  height="30" align="center" style="background-color:#5BC0DE;">
                                <div  align="center"  > <spring:message code="finance.label.PAYMENT"  text="default text"/></div></td>
                        </tr>
                        <tr>

                            <td colspan="6" height="30" align="center" style="border-color:#5BC0DE;">
                                <input type="hidden" name="vendorId" id="customerId" value="0" />
                                <input type="hidden" name="ledgerId" id="ledgerId" value="0" />
                               <spring:message code="finance.label.Vendor"  text="default text"/> <select name="vendorIdTemp" id="vendorIdTemp"  class="form-control" style="width:280px;height:40px;" onchange="setValues();">
                                    <c:if test="${vendorList != null}">
                                        <option value="" selected>--<spring:message code="finance.label.Select"  text="default text"/>--</option>
                                        <c:forEach items="${vendorList}" var="vendor">
                                            <option value='<c:out value="${vendor.vendorId}"/>~<c:out value="${vendor.ledgerId}"/>'><c:out value="${vendor.vendorName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                                <input type="hidden" name="vendorName" id="vendorName" value="" >    
                            </td>
                        </tr>

                    </table>
                    <br/>
                    <!--             <table align="center" width="100%" border="0">
                                    <tr >
                                       <td colspan="2" align="center"> <input type="button" id="Search" value="Search" class="button" onClick="setValues();">
                                    </tr>
                                </table>-->


            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>