<html>
<head>
   <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
   
        
<script type="text/javascript">
     $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });

    </script>
<script type="text/javascript">
    function showTextBox() {
    var ele = document.getElementById("company");
    if ($('#newTyerType').val() == 'Newtyer') {
        
        //$('#company').style.display = "block";
                 ele.style.display = 'table-row';
        // $('#tyerCompanyName').css({'visibility':'visible'});
    }
    else
    {
      ele.style.display = "none";  
    }
    }
</script>
<SCRIPT>
    
   

       
function getVehicleNos(){
    var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
    //getVehicleDetails(document.getElementById("regno"));
} 


var httpReq;
function callAjax(){
    
   
          if( trim(document.workOrder.regno.value) == '' ){
            alert("Please Enter Vehicle Registration Number");
            document.workOrder.regno.value = '';
            document.workOrder.regno.select();
            document.workOrder.regno.focus();
            document.workOrder.kmReading.value='';
            return;
       }    
        var vehicleNo=document.workOrder.regno.value;          
        var url='/throttle/checkActualKm.do?vehicleNo='+vehicleNo;
        if ( vehicleNo != '') {
            if (window.ActiveXObject){
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest){
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function() { processAjax();};
            httpReq.send(null);
        }
    
}   
function processAjax()
{
    if (httpReq.readyState == 4)
        {
            if(httpReq.status == 200)
                {
                    temp = httpReq.responseText.valueOf();
                    var kminfo=temp.split('~');
                    document.workOrder.actualKm.value=kminfo[0];
                    document.workOrder.totalKm.value=kminfo[1];
                    document.workOrder.kmReading.focus();
                    
                }
                else
                    {
                        alert("Error loading page\n"+ httpReq.status +":"+ httpReq.statusText);
                    }
                }
}

          function submitPage()
          {
               //if(textValidation(document.workOrder.regno,'Vehicle No'));
              // else if(numberValidation(document.workOrder.kmReading,'kmReading'));
              // else  if(textValidation(document.workOrder.reason,'Reason'));
               //else{
                    document.workOrder.action = '/throttle/handleTyerDetails.do';
                    document.workOrder.submit();
               //}
          }


</SCRIPT>


<body onload="document.workOrder.regno.focus();getVehicleNos();" >
<form name="workOrder" method="post">



<%@ include file="/content/common/path.jsp" %>

<!-- pointer table -->

<!-- message table -->

<%@ include file="/content/common/message.jsp"%> 
<table align="center" border="0" cellpadding="0" cellspacing="0" width="400" id="bg" class="border"style="display:block">
<tr>
<td colspan="2" class="contenthead" height="30"><div class="contenthead">Enter Tyer Details </div></td>
</tr>
<tr>    
    <td class="text2" height="30"><font color="red">*</font>Vehicle No</td>
<td class="text2" height="30">
    <c:if test="${vehicleNos!= null}">
                                                 <select name="vehicleId" id="vehicleId" >
                                <option value='0~~0~~~'>--select--</option>
                                <c:forEach items="${vehicleNos}" var="vehicle">
                                  <option value='<c:out value="${vehicle.vehicleId}"/>'> <c:out value="${vehicle.vehicleNo}"/> </option>
                                </c:forEach>
                                  
                               
                            </select>
                                            </c:if>
    
</td>
</tr>

<tr>
<td class="text2" height="30">Old Tyer No</td>
<td class="text2" height="30"><input name="oldTyerNo" id="oldTyerNo" style="width:125px;" maxlength="20" type="text"  class="form-control" value=""></td>
</tr>

<tr>
<td class="text1" height="30"> New Tyer No </td>
<td class="text1" height="30"><input name="newTyerNo" id="newTyerNo" style="width:125px;" maxlength="20" type="text"  class="form-control" value=""></td>
</tr>

<tr>
<td class="text2" height="30"><font color="red">*</font>Odometer Reading </td>
<td class="text2" height="30"><input name="odometerReading" id="odometerReading" style="width:125px;" maxlength="20" type="text"  class="form-control" value=""></td>
</tr>
<tr>
<td class="text2" height="30"><font color="red">*</font>Change Date </td>
<td class="text2" height="30"> <input name="changeDate"  type="text" class="datepicker" value=""></td>
</tr>
<tr>
<td class="text2" height="30"><font color="red">*</font>Tyer Amount </td>
<td class="text2" height="30"> <input name="tyerAmount" id="tyerAmount"  type="text" class="form-control"  value=""></td>
</tr>
<tr>
<td class="text2" height="30"><font color="red">*</font>New Tyer Type </td>
<td class="text2" height="30">
    <select id="newTyerType" name="newTyerType" onchange="showTextBox();"> 
        <option value='Rethread'>Rethread</option>
        <option value='Newtyer'>New tyer</option>
        
        
    </select></td>
</tr>

<tr id="company" style="display: none" >
<td class="text2" height="30"><font color="red">*</font>Company Name </td>
<td class="text2" height="30"><input name="tyerCompanyName" id="tyerCompanyName" style="width:125px;" maxlength="20" type="text"  class="form-control" value=""></td>
</tr>

<input name="cust" type="hidden" value="Existing Customer" >
<tr>
<td class="text1" height="70"> <font color="red">*</font>Remarks </td>
<td class="text1" height="70">
<textarea name="remarks" id="remarks" onkeyup="maxlength(this.form.remark,300)"  style="width:125px;" class="text1"></textarea>
</td>
</tr>

</table>

<br>
<br>

<br>
<center>

<input type="button" value="Submit" name="generate" id="generate" class="button" onclick="submitPage();">


</center>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
<br>
    <br>
        <br>
    <br>
</body>
</html>
