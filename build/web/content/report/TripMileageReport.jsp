<%--
    Document   : TripMileageReport
    Created on : Jul 29, 2012, 12:08:35 PM
    Author     : admin
--%>



<%@page contentType="text/html" import="java.sql.*,java.text.DecimalFormat" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Mileage Report</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript">

        function show_src() {
            document.getElementById('exp_table').style.display='none';
        }
        function show_exp() {
            document.getElementById('exp_table').style.display='block';
        }
        function show_close() {
            document.getElementById('exp_table').style.display='none';
        }
            function submitpage(){

                document.viewTripSheet.action="TripMileageReport.jsp";
                document.viewTripSheet.method = "post";
                document.viewTripSheet.submit();
            }
        </script>

    </head>
    <body>

        <form name="viewTripSheet" >

<%!

DecimalFormat df2 = new DecimalFormat("#0.00");

double getFleetExpenses(Statement stm, String vehicleNo, String tripDate) {
    double fleetExpenses = 0;

    try{

        ResultSet res = stm.executeQuery("select c.total_amount  as fleetExp "
                + "from papl_vehicle_master a, papl_direct_jobcard b, papl_jobcard_bill_master c, "
                + "papl_vehicle_reg_no d where a.vehicle_id = b.vehicle_id and  "
                + "a.vehicle_id = d.vehicle_id and d.active_ind='Y' and b.job_card_id = c.job_card_id  "
                + " and date_format(c.Created_On,'%d-%m-%Y')  = '"+tripDate+"' and d.reg_no in ('"+vehicleNo+"')");
        while(res.next()) {
            fleetExpenses = res.getDouble("fleetExp");
        }
    }catch(Exception e){
        System.out.println("Exception in getting Fleet Amount"+e.getMessage());

    }
    return fleetExpenses;

}


String getRegNo(Statement stm, String vehicleId) {
    String regNo = "";

    try{
    
        ResultSet res = stm.executeQuery("select reg_no FROM papl_vehicle_reg_no where vehicle_id in ('"+vehicleId+"')");
        while(res.next()) {
            regNo = res.getString("reg_no");
        }
    }catch(Exception e){
        System.out.println("Exception in getting reg_no"+e.getMessage());

    }
    return regNo;
    
}



%>


<%
DecimalFormat df2 = new DecimalFormat("0.00");
Class.forName("com.mysql.jdbc.Driver").newInstance();
Connection connThtottle = DriverManager.getConnection("jdbc:mysql://localhost:3306/throttlerathimeena", "root", "etsadmin");
Statement stm = connThtottle.createStatement();
Statement stmfun = connThtottle.createStatement();
ResultSet resVehicle = stm.executeQuery("SELECT distinct vr.vehicle_id,vr.reg_no FROM papl_vehicle_reg_no vr, tripsheet ts WHERE ts.vehicleid = vr.vehicle_id AND vr.active_ind='Y'");

%>

 <table width="900" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
        <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
        </h2></td>
        <td align="right"><div style="height:17px;margin-top:0px;"><img src="/throttle/images/icon_report.png" alt="Export" onclick="show_exp();" class="arrow" />&nbsp;<img src="/throttle/images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
        </tr>
        <tr id="exp_table" >
        <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
            <div class="tabs" align="left" style="width:850;">
        <ul class="tabNavigation">
		<li style="background:#76b3f1">Mileage Report</li>
	</ul>
        <div id="first">
        <table width="900" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
        <tr>
            <td>Vehicle No</td><td>
                <select name="vehicleId"  id="vehicleId" class="form-control" style="width:120px;">

                    <option value=""> -Select- </option>
                    <%
                    while(resVehicle.next()){
                    %>
                    <option value="<%=resVehicle.getString("vr.vehicle_id")%>"><%=resVehicle.getString("vr.reg_no")%></option>
                    <%
                    }
                    %>
                </select>
            </td>
            <td>Route</td><td>
                 <select name="routeId" id="routeId" class="form-control" style="width:120px;">
                    <option value="" selected="selected">Select</option>
                    <option value="Bangalore -Kalasipalayam - Chennai (via - Hosur )">Bangalore -Kalasipalayam - Chennai (via - Hosur )</option>
                    <option value="Chennai - Calicut (via - Vellore-Krishnagiri-Coimbatore-mannarkad-perunthalmanna-Malapuram )">Chennai - Calicut (via - Vellore-Krishnagiri-Coimbatore-mannarkad-perunthalmanna-Malapuram )</option>
                    <option value="Chennai - Madurai (via - Trichy )">Chennai - Madurai (via - Trichy )</option>
                </select>
            </td>
            <td>&nbsp;</td><td>
                &nbsp;</td>
            </tr>
            <tr>
            <td><font color="red">*</font>From Date</td><td><input name="tripFromDate" id="tripFromDate" readonly  class="form-control"  type="text" value="" size="20"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.viewTripSheet.tripFromDate,'dd-mm-yyyy',this)"/></td>
            <td><font color="red">*</font>To Date</td><td><input name="tripToDate" id="tripToDate" readonly  class="form-control"  type="text" value="" size="20"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.viewTripSheet.tripToDate,'dd-mm-yyyy',this)"/></td>
            <td colspan="2"><input type="button" name="search" value="View Report" onClick="submitpage(this.name)" class="button" /></td>
        </tr>
        </table>
        </div></div>
        </td>
        </tr>
        </table>
<%

String rowClass = "text1";
String tripFromDate = request.getParameter("tripFromDate");
if(tripFromDate == null) {
    tripFromDate = "";
}
String tripToDate = request.getParameter("tripToDate");
if(tripToDate == null) {
    tripToDate = "";
}
String vehicleId = request.getParameter("vehicleId");
if(vehicleId == null) {
    vehicleId = "";
}
String routeId = request.getParameter("routeId");
if(routeId == null) {
    routeId = "";
}

String status = request.getParameter("status");
if(status == null) {
    status = "";
}


String queryConcat = "";
int sno = 0;

if(vehicleId.length() > 0){
    queryConcat = queryConcat + " AND  ts.vehicleid = '"+vehicleId+"'";
}

if(routeId.length() > 0){
    queryConcat = queryConcat + " AND  ts.routeid = '"+routeId+"'";
}

if(status.equals("Open")){
    queryConcat = queryConcat + " AND  ts.status = 'Y'";
}
if(status.equals("Close")){
    queryConcat = queryConcat + " AND  ts.status <> 'Y'";
}

if(tripFromDate.length() > 0 && tripToDate.length() > 0){
    //queryConcat = queryConcat + " AND  status <> 'Y'";





String qry= " SELECT ts.tripsheetid,date_format(ts.tripdate,'%d-%m-%Y') as tdate,ts.routeid,"
        + "ts.vehicleid,ts.totalkms as km,sum(ifnull(fd.totalliters,0)) as ltr,sum(ifnull(fd.totalamount,0)) as amt"
        + " FROM tripfueldetails fd ,tripsheet ts WHERE ts.tripsheetid=fd.tripsheetid  AND fd.active_ind='Y' "+queryConcat+" GROUP BY fd.tripsheetid ";
//out.println(qry);

ResultSet res = stm.executeQuery(qry);


%>


<script language="javascript">
    document.getElementById("tripFromDate").value = "<%=tripFromDate%>";
    document.getElementById("tripToDate").value = "<%=tripToDate%>";
    document.getElementById("vehicleId").value = "<%=vehicleId%>";
    document.getElementById("routeId").value = "<%=routeId%>";
</script>



        <table width="100%" cellpadding="5" cellspacing="0">
            <tr>
                <td  class="contenthead">S.No</td>
                <td  class="contenthead">Date</td>
                <td  class="contenthead">vehicle</td>
                <td  class="contenthead">Vehicle Type</td>
                <td  class="contenthead">Fuel Consumed</td>
                <td  class="contenthead">Run Km</td>
                <td  class="contenthead">KMPL</td>
                <td  class="contenthead">Amount</td>
                <td  class="contenthead">Cost/Km</td>

            </tr>
            <%
            String rNo = "";
            float km = 0.0f;
            float ltrs = 0.0f;
            float amount = 0.0f;
            float kmpl = 0.0f;
            float costPerKm = 0.0f;
            double totalKM = 0;
            double totalAmount = 0;
            double totalLtrs = 0;


            while(res.next()) {
            sno++;
	    rNo = getRegNo(stmfun, res.getString("ts.vehicleid"));
            km = res.getFloat("km");
            ltrs = res.getFloat("ltr");
            amount = res.getFloat("amt");
            totalKM += km;
            totalAmount += amount;
            totalLtrs += ltrs;
            if(km > 0) {
            costPerKm = amount / km;
            kmpl =  km / ltrs;
            } else {
            costPerKm = 0;
            kmpl = 0;
            }
    %>
      <tr>
        <td class="<%=rowClass%>"><%=sno%></td>
        <td class="<%=rowClass%>"><%=res.getString("tdate")%></td>
        <td class="<%=rowClass%>"><%=rNo%></td>
        <td class="<%=rowClass%>">&nbsp;</td>
        <td class="<%=rowClass%>"><%=ltrs%></td>
        <td class="<%=rowClass%>"><%=km%></td>
        <td class="<%=rowClass%>"><%=df2.format(kmpl)%></td>
        <td class="<%=rowClass%>"><%=df2.format(amount)%></td>
        <td class="<%=rowClass%>"><%=df2.format(costPerKm)%></td>
    </tr>

<%
}


%>
</table>



<br>
<br>
<table width="500" cellpadding="5" cellspacing="0">
<tr>
<td class="contenthead">Overall Total KM</td>
<td class="contenthead">Overall Fuel Total(Ltrs) </td>
<td class="contenthead">Average KMPL </td>
<td class="contenthead">Overall Fuel Total Amount</td>
<td class="contenthead">Average Cost/Km </td>
</tr>

<tr>
<td class="text1"><%=totalKM%></td>
<td class="text1"><%=totalLtrs%></td>
<td class="text1"><%=(totalLtrs > 0)?df2.format((totalKM/totalLtrs)):0%> </td>
<td class="text1"><%=totalAmount%></td>
<td class="text1"><%=(totalKM > 0)?df2.format((totalAmount/totalKM)):0%></td>
</tr>


</table>

<%
}
%>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

    </body>
</html>
