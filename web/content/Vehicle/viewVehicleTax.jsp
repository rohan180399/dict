<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
        <script type="text/javascript">

            function show_src() {
                document.getElementById('exp_table').style.display='none';
            }

            function show_exp() {
                document.getElementById('exp_table').style.display='block';
            }

            function show_close() {
                document.getElementById('exp_table').style.display='none';
            }
        </script>
            <style>
                        #index td {
   color:white;
}
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.VehicleRoadPermit"  text="Vehicle Road Permit"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.Vehicle"  text="Vehicle"/></a></li>
            <li class="active"><spring:message code="stores.label.VehicleRoadPermit"  text="Vehicle Road Permit"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">

    <!--setImages(1,0,0,0,0,0);-->
    <body onLoad="getVehicleNos();setImages(1,0,0,0,0,0);setDefaultVals('<%= request.getAttribute("regNo")%>','<%= request.getAttribute("typeId")%>','<%= request.getAttribute("mfrId")%>','<%= request.getAttribute("usageId")%>','<%= request.getAttribute("groupId")%>');">
        <form name="viewVehicleDetails"  method="post" >
            <%@ include file="/content/common/path.jsp" %>


            <%@ include file="/content/common/message.jsp" %>

            
                  <table class="table table-info">
                              <tr height="30" id="index">
                             <td colspan="4"  style="background-color:#5BC0DE;"><b>Vehicle Road Permit</b></td>&nbsp;&nbsp;&nbsp;                    
                    </tr>
                            <tr>
                                <td>Vehicle Number</td>
                                <td>
                                   <input type="textbox" id="regno" name="regno" value="<c:out value="${regNo}" />" class="form-control" style="width:260px;height: 38px;">
                                </td>
                                <td></td>
                                <td>
<!--                                    <select class="form-control" name="vendorId" id="vendorId" type="textbox" style="width:260px;height: 38px;">
                                              <option value="">---Select---</option>
                                                <c:if test = "${vendorListCompliance != null}" >
                                                    <c:forEach items="${vendorListCompliance}" var="list">
                                                        <option value='<c:out value="${list.vendorId}" />'><c:out value="${list.vendorName}" /></option>
                                                    </c:forEach >
                                                </c:if>
                                            </select>
                                             <script>
                                                 document.getElementById("vendorId").value ='<c:out value="${vendorId}"/>'
                                             </script>-->
                                </td>  
                            </tr>
                            <tr>
                                <td><font color="red">*</font>From Date</td>
                                <td height="30"><input name="fromDate" id="fromDate" value="<c:out value="${fromDate}"/>" type="textbox" class="form-control datepicker" style="width:260px;height: 38px;" onclick="ressetDate(this);"></td>
                                <td><font color="red">*</font>To Date</td>
                                <td height="30"><input name="toDate" id="toDate" value="<c:out value="${toDate}"/>" type="textbox" class="form-control datepicker" style="width:260px;height: 38px;" onclick="ressetDate(this);"></td>
                            </tr>
                            <tr>
                                <!--<td></td>--> 
                                <td colspan="4" align="center">&emsp;&emsp;&emsp;<input type="button" class="button" name="Search" value="Search" onclick="submitPage(this.name);">
                                </td>  
                            </tr>
                        </table>
              
            <br>

<%
            int index = 1;

%>


            <c:if test="${RoadTaxList == null }" >
                <br>
                <center><font color="red" size="2"> no records found </font></center>
            </c:if>
            <c:if test="${RoadTaxList != null }" >
                  <table class="table table-info mb30 table-hover" id="table">
                     <thead>
                             <th>S.No</th>
                             <th>Vehicle Number</th>
                             <th>Tax Receipt No</th>
                             <th>Tax Receipt Date</th>
                             <th>Tax Paid Location</th>
                             <th>Tax Amount</th>
                             <th>Next Tax Date</th>
                              <th>Road Tax Action</th>
                    </thead>
                
                    <c:forEach items="${RoadTaxList}" var="Rtl" >
           
                        <tr>
                            <td  height="30" style="padding-left:30px; "> <%= index %> </td>
                            <td  height="30" style="padding-left:30px; "> <c:out value="${Rtl.regNo}" /></td>
                            <td  height="30" style="padding-left:30px; "><c:out value="${Rtl.roadtaxreceiptno}" /></td>
                            <td  height="30" style="padding-left:30px; "><c:out value="${Rtl.roadtaxreceiptdate}" /></td>
                            <td  height="30" style="padding-left:30px; "><c:out value="${Rtl.roadtaxpaidlocation}" /></td>
                            <td  height="30" style="padding-left:30px; "><c:out value="${Rtl.roadtaxamount}" /></td>
                            <td  height="30" style="padding-left:30px; "><c:out value="${Rtl.nextRoadTaxDate}" /></td>
                            <%--<td  height="30" style="padding-left:30px; "><a href='/throttle/vehicleTaxDetail.do?vehicleId=<c:out value="${Rtl.vehicleId}" />&id=<c:out value="${Rtl.id}" />'>alter </a> </td>--%>
                        <td  height="30" style="padding-left:30px; ">
                                <a href='/throttle/vehicleDetail.do?vehicleId=<c:out value="${Rtl.vehicleId}" />&regNo=<c:out value="${Rtl.regNo}" />&listId=<c:out value="${listId}" />&fleetTypeId=1'><img src="/throttle/images/addlogos.png" height="20px;"/> </a>
                                &nbsp;
                                &nbsp;
                            </td>
                        </tr>
                        <% index++; %>
                    </c:forEach>
                </table>
            </c:if>
            <br>
  <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5"  selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 7);
        </script>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    <script type="text/javascript">
        function submitPage1(value){
           // alert(value);
            if(value == 'search' || value == 'Prev' || value == 'Next' || value == 'GoTo' || value =='First' || value =='Last'){
                if(value=='GoTo'){
                    var temp=document.viewVehicleDetails.GoTo.value;
                    document.viewVehicleDetails.pageNo.value=temp;
                    document.viewVehicleDetails.button.value=value;
                    document.viewVehicleDetails.action = '/throttle/vehicleTaxList.do';
                    document.viewVehicleDetails.submit();
                }else if(value == "First"){
                    temp ="1";
                    document.viewVehicleDetails.pageNo.value = temp;
                    value='GoTo';
                }else if(value == "Last"){
                    temp =document.viewVehicleDetails.last.value;
                    document.viewVehicleDetails.pageNo.value = temp;
                    value='GoTo';
                }
                //document.viewVehicleDetails.button.value=value;
                document.viewVehicleDetails.action = '/throttle/vehicleTaxList.do';
                document.viewVehicleDetails.submit();

            }else if(value == 'add'){
                document.viewVehicleDetails.action = '/throttle/handleVehicleRoadTax.do';
                document.viewVehicleDetails.submit();
            }else{
                document.viewVehicleDetails.action='/throttle/vehicleTaxList.do'
                document.viewVehicleDetails.submit();
            }
        }
        function submitPage(value) {
         
                    document.viewVehicleDetails.action = '/throttle/searchVehiclePage.do?listId=2';
                    document.viewVehicleDetails.submit();
         
        }


        function setDefaultVals(regNo,typeId,mfrId,usageId,groupId){

            if( regNo!='null'){
                document.viewVehicleDetails.regNo.value=regNo;
            }
            if( typeId!='null'){
                document.viewVehicleDetails.typeId.value=typeId;
            }
            if( mfrId!='null'){
                document.viewVehicleDetails.mfrId.value=mfrId;
            }
            if( usageId!='null'){
                document.viewVehicleDetails.usageId.value=usageId;
            }
            if( groupId!='null'){
                document.viewVehicleDetails.groupId.value=groupId;
            }
        }


        function getVehicleNos(){
            //onkeypress='getList(sno,this.id)'
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
        }

    </script>
</div>
    </div>
    </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

