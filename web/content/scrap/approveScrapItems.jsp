<html>
    <head>
        <%@page language="java" contentType="text/html; charset=UTF-8"%>
        <%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
        <%@page import="java.util.Locale"%>

        <!--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script>       
        <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
        
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>        
        <title></title>
    </head>
    
    
    
    <script>

        function show_src() {
            document.getElementById('exp_table').style.display='none';
        }

        function show_exp() {
            document.getElementById('exp_table').style.display='block';
        }

        function show_close() {
            document.getElementById('exp_table').style.display='none';
        }
    
    function newWO(){
        window.open('/throttle/content/stores/OtherServiceStockAvailability.html', 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
    }
    
    function submitPage(val){    
    if(document.bill.fromDate.value==''){
        alert("Please Enter From Date");
        document.bill.fromDate.focus();
        document.bill.fromDate.select();
        return 'fail';
    }    
    if(document.bill.toDate.value==''){
        alert("Please Enter To Date");
        document.bill.toDate.focus();
        document.bill.toDate.select();
        return 'fail';
    }    
    document.bill.action = '/throttle/scrapApproval.do'
    document.bill.submit();    
    }   
    
    
    function saveScraps(val){    
    var selectedIndex = document.getElementsByName("selectedIndex");
    var previousQty = document.getElementsByName("previousQty");
    var reqQty = document.getElementsByName("reqQty");
    var stkQty = document.getElementsByName("stkQty");
    var approvedQty = document.getElementsByName("approvedQty");
    
    
    
    for(var i=0;i<selectedIndex.length;i++){        
        if(selectedIndex[i].checked == 1){
            if(isFloat(approvedQty[i].value)){
                alert("Please Enter Valid quantity");
                approvedQty[i].focus();
                approvedQty[i].select();                
                return 'fail';
            }
            if( parseFloat(reqQty[i].value) < ( parseFloat(previousQty[i].value ) + parseFloat(approvedQty[i].value ) )  ){
                alert("Approved qunatity should not exceeds reauested quantity");
                approvedQty[i].focus();
                approvedQty[i].select();                
                return 'fail';
            }
            if( parseFloat(stkQty[i].value) < ( parseFloat(previousQty[i].value ) + parseFloat(approvedQty[i].value ) )  ){
                alert("Quantity should not exceeds stock balance");
                approvedQty[i].focus();
                approvedQty[i].select();                
                return 'fail';
            }
            
        }    
    }
    if( trim(document.bill.approver.value) == ''){
        alert("Please enter approver name");
        document.bill.approver.focus(); 
        return 'fail';
    }    
    if(confirm("Are you sure to submit")){
        document.getElementById("subButtons").style.visibility = "hidden";
        document.bill.status.value=val;
        document.bill.action = '/throttle/handleScrapItems.do'
        document.bill.submit(); 
    }
    
    }   
    
    function scrapReturns(){ 
    var selectedIndex = document.getElementsByName("selectedIndex");
    var previousQty = document.getElementsByName("previousQty");
    var reqQty = document.getElementsByName("reqQty");
    var stkQty = document.getElementsByName("stkQty");
    var approvedQty = document.getElementsByName("approvedQty");
    var approverName = document.getElementsByName("approver");
    
    
    for(var i=0;i<selectedIndex.length;i++){
        if(selectedIndex[i].checked == 1){
            if(isFloat(approvedQty[i].value)){
                alert("Please Enter Valid quantity");
                approvedQty[i].focus();
                approvedQty[i].select();                
                return 'fail';
            }   
            if( parseFloat(previousQty[i].value ) <  parseFloat(approvedQty[i].value )  ){
                alert("Approved qunatity should not exceeds reauested quantity");
                approvedQty[i].focus();
                approvedQty[i].select();                
                return 'fail';
            }
        }    
    }
    if(trim(document.bill.approver.value) == ''){
        alert("Please enter approver name");
        document.bill.approver.focus(); 
        return 'fail';
    }         
        
    if(confirm("Are you sure to submit")){
        document.getElementById("subButtons").style.visibility = "hidden";        
        document.bill.action = '/throttle/handleReturnScraps.do';
        document.bill.submit();    
    }
    }   

    function setValues(){
     if( '<%= request.getAttribute("fromDate") %>' != 'null' )   {
         document.bill.fromDate.value = '<%= request.getAttribute("fromDate") %>';
     }   
     if( '<%= request.getAttribute("toDate") %>' != 'null' )   {
         document.bill.toDate.value = '<%= request.getAttribute("toDate") %>';
     }   
     if( '<%= request.getAttribute("paplCode") %>' != 'null' )   {
         document.bill.paplCode.value = '<%= request.getAttribute("paplCode") %>';
     }   
     if( '<%= request.getAttribute("itemName") %>' != 'null' )   {
         document.bill.itemName.value = '<%= request.getAttribute("itemName") %>';
     }   
     }    


    </script>
    <script>
   function changePageLanguage(langSelection){
   if(langSelection== 'ar'){
   document.getElementById("pAlign").style.direction="rtl";
   }else if(langSelection== 'en'){
   document.getElementById("pAlign").style.direction="ltr";
   }
   }
    </script>

  <c:if test="${jcList != null}">
  <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
  </c:if>
      
  <span style="float: right">
	<a href="?paramName=en">English</a>
	|
	<a href="?paramName=ar">Arabic</a>
  </span>

    <body onLoad="setValues();" >        
        
        <form name="bill"  method="post" >
            <%@ include file="/content/common/path.jsp" %>
           
            
            <%@ include file="/content/common/message.jsp" %>

            <table width="90%" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
        <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
        </h2></td>
        <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Export" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
        </tr>
        <tr id="exp_table" >
        <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
            <div class="tabs" align="left" style="width:850;">
        <ul class="tabNavigation">
		<li style="background:#76b3f1"><spring:message code="stores.label.SearchParts"  text="default text"/>
                </li>
	</ul>
        <div id="first">
        <table width="100%" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
        <tr>
            <td><spring:message code="stores.label.CompanyCode"  text="default text"/></td><td><input type="text" class="form-control" name="paplCode" value="" > </td>
            <td><spring:message code="stores.label.ItemName"  text="default text"/></td><td><input type="text" class="form-control" name="itemName" value="" ></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
                 <td><spring:message code="stores.label.FromDate"  text="default text"/></td><td><input type="text" class="form-control" name="fromDate" value="" ><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.bill.fromDate,'dd-mm-yyyy',this)"/></td>
            <td><spring:message code="stores.label.TODate"  text="default text"/></td><td><input type="text" class="form-control" readonly name="toDate" value="" > <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.bill.toDate,'dd-mm-yyyy',this)"/></td>
            <td><input type="button" class="button" name="Search" value="<spring:message code="stores.label.SEARCH"  text="default text"/>" onClick="submitPage(this.value)" ></td>
        </tr>
        </table>
        </div></div>
        </td>
        </tr>
        </table>
                   
            <br>
            <br>
                
            <c:if test = "${scrapItems != null}" >
             <% 
                int oddEven=0,index=0;
                String classText="";
             %>   
                
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="800" id="bg" class="border">
               
                    <tr>
                        <td class="contentsub" height="30"><spring:message code="stores.label.Sno"  text="default text"/></td>
                        <td class="contentsub" height="30">PAPL CODE</td>
                        <td class="contentsub" height="30"><spring:message code="stores.label.ItemName"  text="default text"/></td>
                        <td class="contentsub" height="30">Uom</td>
                        <td class="contentsub" height="30"><spring:message code="stores.label.REQQTY"  text="default text"/></td>
                        <td class="contentsub" height="30"><spring:message code="stores.label.ApprovedQty"  text="default text"/></td>
                        <td class="contentsub" height="30"><spring:message code="stores.label.Stock"  text="default text"/></td>
                        <td class="contentsub" height="30"><spring:message code="stores.label.SCRAP/RETURN"  text="default text"/></td>
                        <td class="contentsub" height="30"><spring:message code="stores.label.Status"  text="default text"/></td>
                        <td class="contentsub" height="30"><spring:message code="stores.label.APPROVER"  text="default text"/></td>
                        <td class="contentsub" height="30"><spring:message code="stores.label.Select"  text="default text"/></td>
                    </tr>
                    
                    <c:forEach items="${scrapItems}" var="bill">    
                        <%

            oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                                
            
                        <tr>
                            <input type="hidden" name="approveId" value='<c:out value="${bill.approveId}"/>' >
                            <input type="hidden" name="priceId" value='<c:out value="${bill.priceId}"/>' >
                            <input type="hidden" name="itemId" value='<c:out value="${bill.itemId}"/>' >
                            <td class="<%=classText %>" height="30"> <%= index+1 %> </td>
                            <td class="<%=classText %>" height="30"><c:out value="${bill.paplCode}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${bill.itemName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${bill.uomName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${bill.requestedQty}"/>
                            <input type="hidden" class="form-control" size="6" name="reqQty" value='<c:out value="${bill.requestedQty}"/>' >
                            </td>
                            <td class="<%=classText %>" height="30"><c:out value="${bill.quandity}"/>
                            <input type="hidden" class="form-control" size="6" name="previousQty" value='<c:out value="${bill.quandity}"/>' >
                            </td>
                            <td class="<%=classText %>" height="30">
                            <c:out value="${bill.stkQty}"/>
                            <input type="hidden" class="form-control" size="6" name="stkQty" value='<c:out value="${bill.stkQty}"/>' >
                            </td>
                            <td class="<%=classText %>" height="30">
                            <input type="text" class="form-control" size="6" name="approvedQty" value='<c:out value="${bill.quandity}"/>' >
                            </td>
                            <td class="<%=classText %>" height="30"><c:out value="${bill.status}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${bill.approvedBy}"/></td>                                                        
                            <td class="<%=classText %>" height="30">
                            <input type="checkbox" class="form-control" name="selectedIndex" value="<%= index %>" >
                            </td>                                                        
                        </tr>
                        
                        
                        <%
            index++;
                        %>
                    </c:forEach>                
                </table>

<br>
<div align="center" class="text2">
    Approver Name &nbsp;&nbsp;:&nbsp;&nbsp;<input  type="text" name="approver" value="" class="form-control" >
        <input type="hidden" name="status" value="" >
</div>
<br>
<div align="center" id="subButtons" style="visibility:visible;"> 
<input type="button" class="button" readonly name="search" value="<spring:message code="stores.label.APPROVE"  text="default text"/>" onClick="saveScraps('APPROVE');" >
&nbsp; <input type="button" class="button" readonly name="search" value="<spring:message code="stores.label.REJECT"  text="default text"/>" onClick="saveScraps('REJECT');" >
&nbsp; <input type="button" class="button" readonly name="search" value="<spring:message code="stores.label.SCRAPRETURN"  text="default text"/>" onClick="scrapReturns();" >
    </div>
<br>
            </c:if>    
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>