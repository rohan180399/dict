/*---------------------------------------------------------------------------
 * RenderServiceTO.java
 * Mar 5, 2009
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
-------------------------------------------------------------------------*/
package ets.domain.renderservice.business;

import java.util.ArrayList;

/****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver              Date                      Author                    Change
 * ---------------------------------------------------------------------------
 * 1.0           Mar 15, 2009              Srinivasan.R			       Created
 *
 ******************************************************************************/
public class RenderServiceTO {

    private String labourCount = "";
    private String labourHours = "";
    private String labourExpenseAmount = "";
    private String consumbalesAmount = "";
    private String tyerAmount = "";
    private String oldTyerNo = "";
    private String newTyerNo = "";
    private String odometerReading = "";
    private String changeDate = "";
    private String newTyerype = "";
    private String tyerCompanyName = "";
    private String cost1 = "";
    private String groupRemarks = "";
    private String labour1 = "";
    private String quantity1 = "";
    private String total1 = "";
    private String vendorName = "";
    private String[] quantity = null;
    private String desc = "";
    private String serviceTypeId = "";
    private String serviceTypeName = "";
    private String serviceList1[] = null;
    private String groupList[] = null;
    private String subGroupList[] = null;
    private String expenseRemarks[] = null;
    private String cost[] = null;
    private String labour[] = null;
    private String total[] = null;
    private String labourRemarks = "";
    private String invoiceAmount = "";
    private String vehicleDetails = "";
    private String totalCost = "";
    private String totalLabour = "";
    private String totalAmount = "";
    private String vatAmount = "";
    private String vatRemarks = "";
    private String serviceTaxAmount = "";
    private String serviceTaxRemarks = "";
    private String billSubGroupDetails = "";
    private String billSubGroupId = "";
    private String billSubGroupName = "";
    private String billSubGroupDesc = "";
    private String billGroupId = "";
    private String billGroupName = "";
    private String activeInd = "";
    private String operationPointId = "";
    private String hm = "";
    private String workOrderId = "";
    private String workTypeId = "";
    private String workTypeName = "";
    private String jobCardId = "";
    private String jcMYFormatNo = "";
    private String discount = "";
    private String vehicleNo = "";
    private String jobCardScheduledDate = "";
    private String jobCardPCD = "";
    private String customerName = "";
    private int custId = 0;
    private int serviceId = 0;
    private String serviceName = "";
    private String className = "";
    private String closedBy = "";
    private String closedOn = "";
    private String modelName = "";
    private String modelId = "";
    private String mfrName = "";
    private String mfrId = "";
    private String vehicleTypeName = "";
    private String usageTypeName = "";
    private String status = "";
    private String billNo = "";
    private String sectionId = "";
    private String sectionName = "";
    private ArrayList problemActivityList = new ArrayList();
    private ArrayList activityList = new ArrayList();
    private ArrayList workOrderActivityList = new ArrayList();
    private ArrayList problemCauseList = new ArrayList();
    private ArrayList itemsList = new ArrayList();
    private ArrayList serviceList = new ArrayList();
    private String sparesPercentage = "";
    private String labourPercentage = "";
    private String[] activityId = null;
    private String activtyId = "";
    private String activtyCode = "";
    private String activtyName = "";
    private String time = "";
    private String ratePerHour = "";
    private String amount = "";
    private int vehicleId = 0;
    private String address = "";
    private String city = "";
    private String state = "";
    private String phone = "";
    private String mobile = "";
    private String email = "";
    private String billDate = "";
    private String invoiceType = "";
    private String companyName = "";
    private String[] addressSplit = null;
    int startIndex = 0;
    int endIndex = 0;
    String regNo = "";
    String jcId = "";
    String compId = "";
    String invoiceNo = "";
    ArrayList billHeaderInfo = new ArrayList();
    ArrayList billItems = new ArrayList();
    ArrayList billActivities = new ArrayList();
    ArrayList billTaxDetails = new ArrayList();
    String vendorNames = "";
    ArrayList billRecord = new ArrayList();
    int itemSize = 0;
    int taxSize = 0;
    int laborSize = 0;
    //Hari
    private String date = null;
    private String servicePtName = null;
    private String createdDate = null;
    private String remarks = null;
    private String serviceVendor = null;
    //private String invoiceNo = null;
    private String invoiceDate = null;
    private String labourAmt = null;
    private String invoiceRemarks = null;
    private String sparesAmt = null;
    private String sparesRemarks = null;
    private String consumableAmt = null;
    private String consumableRemarks = null;
    private String laborAmt = null;
    private String laborRemarks = null;
    private String othersAmt = null;
    private String othersRemarks = null;
    private String totalAmt = null;
    private String vatPercent = null;
    private String vatAmt = null;
    private String serviceTaxPercent = null;
    private String serviceTaxAmt = null;
    private String km = null;
    private String driver = null;
    private String serviceType = null;
    private String location = null;

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getKm() {
        return km;
    }

    public void setKm(String km) {
        this.km = km;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getConsumableAmt() {
        return consumableAmt;
    }

    public void setConsumableAmt(String consumableAmt) {
        this.consumableAmt = consumableAmt;
    }

    public String getConsumableRemarks() {
        return consumableRemarks;
    }

    public void setConsumableRemarks(String consumableRemarks) {
        this.consumableRemarks = consumableRemarks;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getInvoiceRemarks() {
        return invoiceRemarks;
    }

    public void setInvoiceRemarks(String invoiceRemarks) {
        this.invoiceRemarks = invoiceRemarks;
    }

    public String getLaborAmt() {
        return laborAmt;
    }

    public void setLaborAmt(String laborAmt) {
        this.laborAmt = laborAmt;
    }

    public String getLaborRemarks() {
        return laborRemarks;
    }

    public void setLaborRemarks(String laborRemarks) {
        this.laborRemarks = laborRemarks;
    }

    public String getLabourAmt() {
        return labourAmt;
    }

    public void setLabourAmt(String labourAmt) {
        this.labourAmt = labourAmt;
    }

    public String getOthersAmt() {
        return othersAmt;
    }

    public void setOthersAmt(String othersAmt) {
        this.othersAmt = othersAmt;
    }

    public String getOthersRemarks() {
        return othersRemarks;
    }

    public void setOthersRemarks(String othersRemarks) {
        this.othersRemarks = othersRemarks;
    }

    public String getServiceTaxAmt() {
        return serviceTaxAmt;
    }

    public void setServiceTaxAmt(String serviceTaxAmt) {
        this.serviceTaxAmt = serviceTaxAmt;
    }

    public String getServiceTaxPercent() {
        return serviceTaxPercent;
    }

    public void setServiceTaxPercent(String serviceTaxPercent) {
        this.serviceTaxPercent = serviceTaxPercent;
    }

    public String getSparesAmt() {
        return sparesAmt;
    }

    public void setSparesAmt(String sparesAmt) {
        this.sparesAmt = sparesAmt;
    }

    public String getSparesRemarks() {
        return sparesRemarks;
    }

    public void setSparesRemarks(String sparesRemarks) {
        this.sparesRemarks = sparesRemarks;
    }

    public String getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(String totalAmt) {
        this.totalAmt = totalAmt;
    }

    public String getVatAmt() {
        return vatAmt;
    }

    public void setVatAmt(String vatAmt) {
        this.vatAmt = vatAmt;
    }

    public String getVatPercent() {
        return vatPercent;
    }

    public void setVatPercent(String vatPercent) {
        this.vatPercent = vatPercent;
    }

    public String getServiceVendor() {
        return serviceVendor;
    }

    public void setServiceVendor(String serviceVendor) {
        this.serviceVendor = serviceVendor;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getServicePtName() {
        return servicePtName;
    }

    public void setServicePtName(String servicePtName) {
        this.servicePtName = servicePtName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String[] getActivityId() {
        return activityId;
    }

    public void setActivityId(String[] activityId) {
        this.activityId = activityId;
    }

    public String getJobCardId() {
        return jobCardId;
    }

    public void setJobCardId(String jobCardId) {
        this.jobCardId = jobCardId;
    }

    public String getJobCardPCD() {
        return jobCardPCD;
    }

    public void setJobCardPCD(String jobCardPCD) {
        this.jobCardPCD = jobCardPCD;
    }

    public String getJobCardScheduledDate() {
        return jobCardScheduledDate;
    }

    public void setJobCardScheduledDate(String jobCardScheduledDate) {
        this.jobCardScheduledDate = jobCardScheduledDate;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMfrName() {
        return mfrName;
    }

    public void setMfrName(String mfrName) {
        this.mfrName = mfrName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    public ArrayList getProblemActivityList() {
        return problemActivityList;
    }

    public void setProblemActivityList(ArrayList problemActivityList) {
        this.problemActivityList = problemActivityList;
    }

    public String getUsageTypeName() {
        return usageTypeName;
    }

    public void setUsageTypeName(String usageTypeName) {
        this.usageTypeName = usageTypeName;
    }

    public ArrayList getProblemCauseList() {
        return problemCauseList;
    }

    public void setProblemCauseList(ArrayList problemCauseList) {
        this.problemCauseList = problemCauseList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public ArrayList getItemsList() {
        return itemsList;
    }

    public void setItemsList(ArrayList itemsList) {
        this.itemsList = itemsList;
    }

    public int getCustId() {
        return custId;
    }

    public void setCustId(int custId) {
        this.custId = custId;
    }

    public String getLabourPercentage() {
        return labourPercentage;
    }

    public void setLabourPercentage(String labourPercentage) {
        this.labourPercentage = labourPercentage;
    }

    public String getSparesPercentage() {
        return sparesPercentage;
    }

    public void setSparesPercentage(String sparesPercentage) {
        this.sparesPercentage = sparesPercentage;
    }

    public ArrayList getServiceList() {
        return serviceList;
    }

    public void setServiceList(ArrayList serviceList) {
        this.serviceList = serviceList;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public String getJcMYFormatNo() {
        return jcMYFormatNo;
    }

    public void setJcMYFormatNo(String jcMYFormatNo) {
        this.jcMYFormatNo = jcMYFormatNo;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String[] getAddressSplit() {
        return addressSplit;
    }

    public void setAddressSplit(String[] addressSplit) {
        this.addressSplit = addressSplit;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(int endIndex) {
        this.endIndex = endIndex;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public String getJcId() {
        return jcId;
    }

    public void setJcId(String jcId) {
        this.jcId = jcId;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getCompId() {
        return compId;
    }

    public void setCompId(String compId) {
        this.compId = compId;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public ArrayList getBillActivities() {
        return billActivities;
    }

    public void setBillActivities(ArrayList billActivities) {
        this.billActivities = billActivities;
    }

    public ArrayList getBillHeaderInfo() {
        return billHeaderInfo;
    }

    public void setBillHeaderInfo(ArrayList billHeaderInfo) {
        this.billHeaderInfo = billHeaderInfo;
    }

    public ArrayList getBillItems() {
        return billItems;
    }

    public void setBillItems(ArrayList billItems) {
        this.billItems = billItems;
    }

    public ArrayList getBillTaxDetails() {
        return billTaxDetails;
    }

    public void setBillTaxDetails(ArrayList billTaxDetails) {
        this.billTaxDetails = billTaxDetails;
    }

    public int getItemSize() {
        return itemSize;
    }

    public void setItemSize(int itemSize) {
        this.itemSize = itemSize;
    }

    public int getLaborSize() {
        return laborSize;
    }

    public void setLaborSize(int laborSize) {
        this.laborSize = laborSize;
    }

    public int getTaxSize() {
        return taxSize;
    }

    public void setTaxSize(int taxSize) {
        this.taxSize = taxSize;
    }

    public ArrayList getBillRecord() {
        return billRecord;
    }

    public void setBillRecord(ArrayList billRecord) {
        this.billRecord = billRecord;
    }

    public String getVendorNames() {
        return vendorNames;
    }

    public void setVendorNames(String vendorNames) {
        this.vendorNames = vendorNames;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public String getActivtyCode() {
        return activtyCode;
    }

    public void setActivtyCode(String activtyCode) {
        this.activtyCode = activtyCode;
    }

    public String getActivtyId() {
        return activtyId;
    }

    public void setActivtyId(String activtyId) {
        this.activtyId = activtyId;
    }

    public String getActivtyName() {
        return activtyName;
    }

    public void setActivtyName(String activtyName) {
        this.activtyName = activtyName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRatePerHour() {
        return ratePerHour;
    }

    public void setRatePerHour(String ratePerHour) {
        this.ratePerHour = ratePerHour;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMfrId() {
        return mfrId;
    }

    public void setMfrId(String mfrId) {
        this.mfrId = mfrId;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public ArrayList getActivityList() {
        return activityList;
    }

    public void setActivityList(ArrayList activityList) {
        this.activityList = activityList;
    }

    public ArrayList getWorkOrderActivityList() {
        return workOrderActivityList;
    }

    public void setWorkOrderActivityList(ArrayList workOrderActivityList) {
        this.workOrderActivityList = workOrderActivityList;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getClosedBy() {
        return closedBy;
    }

    public void setClosedBy(String closedBy) {
        this.closedBy = closedBy;
    }

    public String getClosedOn() {
        return closedOn;
    }

    public void setClosedOn(String closedOn) {
        this.closedOn = closedOn;
    }

    public String getWorkTypeId() {
        return workTypeId;
    }

    public void setWorkTypeId(String workTypeId) {
        this.workTypeId = workTypeId;
    }

    public String getWorkTypeName() {
        return workTypeName;
    }

    public void setWorkTypeName(String workTypeName) {
        this.workTypeName = workTypeName;
    }

    public String getHm() {
        return hm;
    }

    public void setHm(String hm) {
        this.hm = hm;
    }

    public String getOperationPointId() {
        return operationPointId;
    }

    public void setOperationPointId(String operationPointId) {
        this.operationPointId = operationPointId;
    }

    public String getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(String workOrderId) {
        this.workOrderId = workOrderId;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getBillGroupId() {
        return billGroupId;
    }

    public void setBillGroupId(String billGroupId) {
        this.billGroupId = billGroupId;
    }

    public String getBillGroupName() {
        return billGroupName;
    }

    public void setBillGroupName(String billGroupName) {
        this.billGroupName = billGroupName;
    }

    public String getBillSubGroupId() {
        return billSubGroupId;
    }

    public void setBillSubGroupId(String billSubGroupId) {
        this.billSubGroupId = billSubGroupId;
    }

    public String getBillSubGroupName() {
        return billSubGroupName;
    }

    public void setBillSubGroupName(String billSubGroupName) {
        this.billSubGroupName = billSubGroupName;
    }

    public String getBillSubGroupDesc() {
        return billSubGroupDesc;
    }

    public void setBillSubGroupDesc(String billSubGroupDesc) {
        this.billSubGroupDesc = billSubGroupDesc;
    }

    public String getBillSubGroupDetails() {
        return billSubGroupDetails;
    }

    public void setBillSubGroupDetails(String billSubGroupDetails) {
        this.billSubGroupDetails = billSubGroupDetails;
    }

    public String getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(String invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public String getServiceTaxAmount() {
        return serviceTaxAmount;
    }

    public void setServiceTaxAmount(String serviceTaxAmount) {
        this.serviceTaxAmount = serviceTaxAmount;
    }

    public String getServiceTaxRemarks() {
        return serviceTaxRemarks;
    }

    public void setServiceTaxRemarks(String serviceTaxRemarks) {
        this.serviceTaxRemarks = serviceTaxRemarks;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }

    public String getTotalLabour() {
        return totalLabour;
    }

    public void setTotalLabour(String totalLabour) {
        this.totalLabour = totalLabour;
    }

    public String getVatAmount() {
        return vatAmount;
    }

    public void setVatAmount(String vatAmount) {
        this.vatAmount = vatAmount;
    }

    public String getVatRemarks() {
        return vatRemarks;
    }

    public void setVatRemarks(String vatRemarks) {
        this.vatRemarks = vatRemarks;
    }

    public String[] getCost() {
        return cost;
    }

    public void setCost(String[] cost) {
        this.cost = cost;
    }

    public String[] getExpenseRemarks() {
        return expenseRemarks;
    }

    public void setExpenseRemarks(String[] expenseRemarks) {
        this.expenseRemarks = expenseRemarks;
    }

    public String[] getGroupList() {
        return groupList;
    }

    public void setGroupList(String[] groupList) {
        this.groupList = groupList;
    }

    public String[] getLabour() {
        return labour;
    }

    public void setLabour(String[] labour) {
        this.labour = labour;
    }

    public String[] getSubGroupList() {
        return subGroupList;
    }

    public void setSubGroupList(String[] subGroupList) {
        this.subGroupList = subGroupList;
    }

    public String[] getTotal() {
        return total;
    }

    public void setTotal(String[] total) {
        this.total = total;
    }

    public String getVehicleDetails() {
        return vehicleDetails;
    }

    public void setVehicleDetails(String vehicleDetails) {
        this.vehicleDetails = vehicleDetails;
    }

    public String getLabourRemarks() {
        return labourRemarks;
    }

    public void setLabourRemarks(String labourRemarks) {
        this.labourRemarks = labourRemarks;
    }

    public String[] getServiceList1() {
        return serviceList1;
    }

    public void setServiceList1(String[] serviceList1) {
        this.serviceList1 = serviceList1;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(String serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public String getServiceTypeName() {
        return serviceTypeName;
    }

    public void setServiceTypeName(String serviceTypeName) {
        this.serviceTypeName = serviceTypeName;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String[] getQuantity() {
        return quantity;
    }

    public void setQuantity(String[] quantity) {
        this.quantity = quantity;
    }

    public String getCost1() {
        return cost1;
    }

    public void setCost1(String cost1) {
        this.cost1 = cost1;
    }

    public String getLabour1() {
        return labour1;
    }

    public void setLabour1(String labour1) {
        this.labour1 = labour1;
    }

    public String getQuantity1() {
        return quantity1;
    }

    public void setQuantity1(String quantity1) {
        this.quantity1 = quantity1;
    }

    public String getTotal1() {
        return total1;
    }

    public void setTotal1(String total1) {
        this.total1 = total1;
    }

    public String getGroupRemarks() {
        return groupRemarks;
    }

    public void setGroupRemarks(String groupRemarks) {
        this.groupRemarks = groupRemarks;
    }

    public String getChangeDate() {
        return changeDate;
    }

    public void setChangeDate(String changeDate) {
        this.changeDate = changeDate;
    }

    public String getNewTyerNo() {
        return newTyerNo;
    }

    public void setNewTyerNo(String newTyerNo) {
        this.newTyerNo = newTyerNo;
    }

    public String getNewTyerype() {
        return newTyerype;
    }

    public void setNewTyerype(String newTyerype) {
        this.newTyerype = newTyerype;
    }

    public String getOdometerReading() {
        return odometerReading;
    }

    public void setOdometerReading(String odometerReading) {
        this.odometerReading = odometerReading;
    }

    public String getOldTyerNo() {
        return oldTyerNo;
    }

    public void setOldTyerNo(String oldTyerNo) {
        this.oldTyerNo = oldTyerNo;
    }

    public String getTyerCompanyName() {
        return tyerCompanyName;
    }

    public void setTyerCompanyName(String tyerCompanyName) {
        this.tyerCompanyName = tyerCompanyName;
    }

    public String getTyerAmount() {
        return tyerAmount;
    }

    public void setTyerAmount(String tyerAmount) {
        this.tyerAmount = tyerAmount;
    }

    public String getConsumbalesAmount() {
        return consumbalesAmount;
    }

    public void setConsumbalesAmount(String consumbalesAmount) {
        this.consumbalesAmount = consumbalesAmount;
    }

    public String getLabourCount() {
        return labourCount;
    }

    public void setLabourCount(String labourCount) {
        this.labourCount = labourCount;
    }

    public String getLabourExpenseAmount() {
        return labourExpenseAmount;
    }

    public void setLabourExpenseAmount(String labourExpenseAmount) {
        this.labourExpenseAmount = labourExpenseAmount;
    }

    public String getLabourHours() {
        return labourHours;
    }

    public void setLabourHours(String labourHours) {
        this.labourHours = labourHours;
    }
    
}
