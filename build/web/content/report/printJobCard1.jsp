<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<title>JobCard</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ page import="ets.domain.operation.business.OperationTO" %> 
        <%@ page import="ets.domain.mrs.business.MrsTO" %> 
        <%@ page import="java.util.*" %> 
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
</head>
<style type="text/css">
body {
font:normal 12px arial;
}

h1 {
font-size:18px;
font-weight:normal;
padding:0px;
margin:0px;
}
.dashed {
border:1px dashed #000;
border-bottom-style:dashed;
border-top-style:none;
border-right-style:none;
border-left-style:none;
}
.line {
border:1px solid #000;
border-bottom-style:solid;
border-top-style:none;
border-right-style:none;
border-left-style:none;
}

.rgline {
border:1px solid #000;
border-bottom-style:solid;
border-top-style:none;
border-right-style:solid;
border-left-style:none;
}
</style>

        <script>
            
            
            
            function print(ind)
            {       
                var DocumentContainer = document.getElementById('print'+ind);
                var WindowObject = window.open('', "TrackHistoryData", 
                    "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                //WindowObject.close();   
            }            
            
            
        </script>

<body>
<form name="mpr"  method="post" >                        
            <br>
            
            
            <%
            int index = 0;
            int sno = 0;

            int service = 0;
            ArrayList cardDetail = new ArrayList();
            cardDetail = (ArrayList) request.getAttribute("DetailsList");
            String[] problemNames,symptoms;            
            OperationTO operationTO = new OperationTO();

            int jlistSize = 36;
            int secNameLimit = 30;
            int probLimit = 30;

            System.out.println("jobcard in jsp=" + cardDetail.size());

            String secName = "";
            String probName = "";




            int cardDet = cardDetail.size();
            if (cardDet != 0) {
                for (int i = 0; i < cardDet; i = i + jlistSize) {
            %>
			
<div id="print<%= i %>" >
<table width="576" height="785" cellpadding="0" cellspacing="0" border="0" align="center">
<tr>
<td height="85"><!-- header logo print empty sapce --></td>
</tr>
<tr>
<td align="center" height="35">
					<!-- Address Print -->
					address 1, <br>
					address 2. 

</td>
</tr>
<tr>
<td valign="top" height="30">
					<!-- job card &amp; reg no -->
					<table width="550"  cellpadding="0" cellspacing="0" border="0" align="center" style="padding:5px; border:1px solid #000; margin-top:5px; ">
					<tr>
					<td width="400" height="30" align="center"><h1>JOB CARD:11856</h1></td>
					<td width="176" align="center"><h1>Reg: TN-02 J 8888</h1></td>
					</tr>
					</table>


</td>
</tr>
		<%
		
		ArrayList details = new ArrayList();
		details = (ArrayList) request.getAttribute("details");
		OperationTO opTO = new OperationTO();
		OperationTO headContent = new OperationTO();
		headContent = (OperationTO) details.get(0);
		%>
<tr>
<td valign="top" height="75">
					<!-- Customer Details -->
					<table width="550"  cellpadding="0" cellspacing="0" border="0" align="center" style="padding:5px; border:1px solid #000; margin-top:5px; ">
					<tr>
					<td width="120" height="25" valign="bottom">Customer Name :</td>
					<td width="187" height="25" class="dashed"></td>
					<td width="100" height="25">Date :</td>
					<td width="147" height="25" class="dashed"> <%=headContent.getCreatedDate()%></td>
					</tr>
					<tr>
					<td width="120" height="25" valign="bottom">Customer Address :</td>
					<td width="187" class="dashed"></td>
					<td width="100" height="25" valign="bottom">Time :</td>
					<td width="147" class="dashed"></td>
					</tr>
					<tr>
					<td width="120" height="30" valign="bottom">Operation Point :</td>
					<td width="187" class="dashed"><%= headContent.getUsageName() %> </td>
					<td width="100" height="30" valign="bottom"></td>
					<td width="147" class="dashed"></td>
					</tr>
					</table>
</td>
</tr>
<tr>
<td valign="top" >
					<!-- vehicle details -->
					<table width="550"  cellpadding="0" cellspacing="0" border="0" align="center" style="padding:0px; border:1px solid #000; margin-top:15px; ">
					<tr >
					<td colspan="2" class="rgline" align="left" height="35" style="padding-left:5px; " ><h1>Vehicle Details</h1></td>
					<td colspan="2" class="line"  align="left" style="padding-left:5px; "><h1>Job Card Details</h1></td>
					</tr>
					<tr >
					<td width="110" height="25" class="line" style="padding-left:5px; " >Model No:</td>
					<td width="165" height="25" class="rgline" style="padding-left:5px; "><b>4015</b></td>
					<td width="100" height="25" class="line" style="padding-left:5px; ">Current KM :</td>
					<td width="175" height="25" class="line" style="padding-left:5px; "><b><%= request.getAttribute("km") %> </b></td>
					</tr>
					<tr >
					<td width="110" height="25" class="line" style="padding-left:5px; " >Registration No:</td>
					<td width="165" height="25" class="rgline" style="padding-left:5px; "><b>TN28AC4488</b></td>
					<td width="100" height="25" class="line" style="padding-left:5px; ">Total KM :</td>
					<td width="175" height="25" class="line" style="padding-left:5px; "><b>168764 KM</b></td>
					</tr>
					<tr >
					<td width="110" height="25" class="line" style="padding-left:5px; " >Registration Date:</td>
					<td width="165" height="25" class="rgline" style="padding-left:5px; "><b>29-10-2010</b></td>
					<td width="100" height="25" class="line" style="padding-left:5px; ">Service Type:</td>
					<td width="175" height="25" class="line" style="padding-left:5px; "><b>Normal</b></td>
					</tr>
					<tr >
					<td width="110" height="25" class="line" style="padding-left:5px; " >Chassis No:</td>
					<td width="165" height="25" class="rgline" style="padding-left:5px; "><b>PNAOB68O1</b></td>
					<td width="100" height="25" class="line" style="padding-left:5px; ">Driver :</td>
					<td width="175" height="25" class="line" style="padding-left:5px; "><b><%= headContent.getDriverName() %></b></td>
					</tr>
					<tr >
					<td width="110" height="25" class="line" style="padding-left:5px; " >Engine No:</td>
					<td width="165" height="25" class="rgline" style="padding-left:5px; "><b>NP H606943</b></td>
					<td width="100" height="25" class="line" style="padding-left:5px; ">EST Delivery :</td>
					<td width="175" height="25" class="line" style="padding-left:5px; "><b>03/12/2010</b></td>
					</tr>
					<tr>
					<td colspan="4" height="60" valign="top" style="padding:5px; ">
					<div><b>TECHINICAN NAME:</b></div>
					<p></p>
					</td>
					</tr>
					</table>
</td>
</tr>
<tr>
<td valign="top">
					<!-- complaints -->
					<table width="550"  cellpadding="0" cellspacing="0" border="0" align="center" style="padding:0px; border:1px solid #000; margin-top:15px; ">
					<tr >
					<td colspan="3" class="line" align="left" height="35" style="padding-left:5px; " ><h1>Complaints</h1></td>
					</tr>
					<tr >
					<td width="40" height="25" class="rgline" style="padding-left:5px; " ><b>S.No</b></td>
					<td width="205" height="25" class="rgline" align="center" style="padding-left:5px; "><b>Complaints</b></td>
					<td width="205" height="25" class="line" align="center" style="padding-left:5px; "><b>Descriptions</b></td>
					</tr>
					 <%
                                    for (int j = i; (j < cardDetail.size() ) && (j < jlistSize + i) ; j++) {
                                        operationTO = new OperationTO();
                                        operationTO = (OperationTO) cardDetail.get(j);
                                        secName = operationTO.getSecName();
                                        problemNames = operationTO.getProblemNameSplit();
                                        symptoms = operationTO.getSymptomsSplit();
                                        if (operationTO.getSecName().length() > secNameLimit) {
                                            secName = secName.substring(0, secNameLimit - 1);
                                        }

                                        
                                        if( operationTO.getProbName().equals("WORKORDERCOMPLAINT") ){ %>
					<tr>
					<tr >
					<td width="40" height="25" class="rgline" style="padding-left:5px; " ><%= sno %></td>
					<td width="205" height="25" class="rgline" align="center" style="padding-left:5px; ">
					<% for(int k=0; k<problemNames.length; k++ ) {%>
                                      <%= problemNames[k] %>  <br>
                                    <%}%>  </td>
					<td width="205" height="25" class="line" align="center" style="padding-left:5px; ">Radiator service forn</td>
					</tr>
					 <%  
                        } 
                            index++;  
                            sno++;
                        }
                  
                  while( (index % jlistSize) != 0 ) { %>
				  
				  <% index++; }  %>  
					<tr>
					<tr >
					<td width="40" height="25" class="rgline" style="padding-left:5px; " >1</td>
					<td width="205" height="25" class="rgline" align="center" style="padding-left:5px; ">All Wheel blow and adjust</td>
					<td width="205" height="25" class="line" align="center" style="padding-left:5px; ">Radiator service forn</td>
					</tr>
					<tr>
					<tr >
					<td width="40" height="25" class="rgline" style="padding-left:5px; " >1</td>
					<td width="205" height="25" class="rgline" align="center" style="padding-left:5px; ">All Wheel blow and adjust</td>
					<td width="205" height="25" class="line" align="center" style="padding-left:5px; ">Radiator service forn</td>
					</tr>
					<tr>
					<tr >
					<td width="40" height="25" class="rgline" style="padding-left:5px; " >1</td>
					<td width="205" height="25" class="rgline" align="center" style="padding-left:5px; ">All Wheel blow and adjust</td>
					<td width="205" height="25" class="line" align="center" style="padding-left:5px; ">Radiator service forn</td>
					</tr>
					<tr>
					<tr >
					<td width="40" height="25" class="rgline" style="padding-left:5px; " >1</td>
					<td width="205" height="25" class="rgline" align="center" style="padding-left:5px; ">All Wheel blow and adjust</td>
					<td width="205" height="25" class="line" align="center" style="padding-left:5px; ">Radiator service forn</td>
					</tr>
					<tr>
					<tr >
					<td width="40" height="25" class="rgline" style="padding-left:5px; " >1</td>
					<td width="205" height="25" class="rgline" align="center" style="padding-left:5px; ">All Wheel blow and adjust</td>
					<td width="205" height="25" class="line" align="center" style="padding-left:5px; ">Radiator service forn</td>
					</tr>
					<tr>
					<tr >
					<td width="40" height="25" class="rgline" style="padding-left:5px; " >1</td>
					<td width="205" height="25" class="rgline" align="center" style="padding-left:5px; ">All Wheel blow and adjust</td>
					<td width="205" height="25" class="line" align="center" style="padding-left:5px; ">Radiator service forn</td>
					</tr>
					<tr>
					<td colspan="3" height="60" valign="top" style="padding:5px; ">
					<div><b>Remarks</b></div>
					<p></p>
					</td>
					</tr>
					</table>
</td>
</tr>
<tr>
<td><center>   
                <input type="button" class="button" name="Print" value="Print" onClick="print(<%= i %>)" > &nbsp;        
            </center>
            <br>    
            <br>    
            
            <%  }   %>  
            <%  }   %> </td>
</tr>
<tr>
<td ><!-- empty space height --></td>
</tr>
</table>
</div>
</body>
</html>
