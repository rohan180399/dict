<%-- 
    Document   : displayBlobData
    Created on : Feb 22, 2014, 10:57:49 PM
    Author     : Arul
--%>


<%@page import="java.util.Properties"%>
<%@page import="org.apache.commons.io.FilenameUtils"%>
<%@ page import="java.sql.*"%>

<%@ page import="java.io.*"%>
                    <%
                        Blob image = null;
                        Blob licenseImage = null;

                        Connection con = null;

                        byte[] imgData = null;
                        byte[] licenseImgData = null;

                        Statement stmt = null;

                        ResultSet INR = null;
                        String expenseId = request.getParameter("expenseId");
                        String fileNameNew = "jdbc_url.properties";
                        String fileName = "jdbc_url.properties";
                        Properties dbProps = new Properties();
                        //The forward slash "/" in front of in_filename will ensure that
                        //no package names are prepended to the filename when the Classloader
                        //search for the file in the classpath

                        InputStream is = getClass().getResourceAsStream("/"+fileName);
                        dbProps.load(is);//this may throw IOException
                        String dbClassName = dbProps.getProperty("jdbc.driverClassName");

                        String dbUrl = dbProps.getProperty("jdbc.url");
                        String dbUserName = dbProps.getProperty("jdbc.username");
                        String dbPassword = dbProps.getProperty("jdbc.password");

                        try {

                            Class.forName(dbClassName).newInstance();

                            con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);

                            stmt = con.createStatement();

                            if (!"".equals(expenseId)) {
                                INR = stmt.executeQuery("select bill_copy_file from ts_trip_expense_files where trip_expense_id ="+expenseId);

                                if (INR.next()) {
                                    image = INR.getBlob(1);
 
                                    imgData = image.getBytes(1, (int) image.length());

                                } else {
                                    //out.println("Display Blob Example");

                                    //out.println("image not found for given id>");

                                    return;

                                }

                                // display the image
                                  if(!FilenameUtils.isExtension(fileNameNew,"pdf")){
                                    //JOptionPane.showMessageDialog(null, "Choose an excel file!");
                                    response.setContentType("image/jpeg");
                                  }else{
                                    //JOptionPane.showMessageDialog(null, "Choose an excel file!");
                                    response.setContentType("application/pdf");
                                  }

                                OutputStream o = response.getOutputStream();
                                o.write(imgData);
                                o.flush();

                                o.close();

                            }

                        } catch (Exception e) {

                            out.println("Please Upload File");

                            //out.println("Image Display Error=" + e.getMessage());

                            return;

                        } finally {

                            try {

                                INR.close();

                                stmt.close();

                                con.close();

                            } catch (SQLException e) {

                                e.printStackTrace();

                            }

                        }

                    %> 
