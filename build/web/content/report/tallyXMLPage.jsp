<%-- 
    Document   : tallyXMLPage
    Created on : Oct 27, 2010, 5:56:49 PM
    Author     : Admin
--%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="ets.domain.report.business.ReportTO" %>
        <%@ page import="java.util.*" %>
         </head>
         <script>
     function searchSubmit(){

    if(document.tallyXML.fromDate.value==''){
        alert("Please Enter From Date");
        return false;
    }else if(document.tallyXML.toDate.value==''){
        alert("Please Enter to Date");
        return false;
    }
    document.tallyXML.action = '/throttle/tallyXMLPage.do'
    document.tallyXML.submit();
   }
   function tallyXMLGen()
   {
       document.tallyXML.action = '/throttle/tallyXMLReport.do'
       document.tallyXML.submit();
   }
    function setDate(fromDate,toDate)
    {
        if(fromDate!='null')
            {
                document.tallyXML.fromDate.value = fromDate;
            }
        if(toDate != 'null')
            {
                document.tallyXML.toDate.value = toDate;
            }
    }
         </script>
         <body onload="setDate('<%= request.getAttribute("fromDate")%>','<%= request.getAttribute("toDate")%>');">
        <form name="tallyXML"  method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="500" id="bg" class="border">
                <tr>
                    <td colspan="4" align="center" class="contenthead" height="30"><div class="contenthead">TallyXML Report</div> </td>
                </tr>
                <tr>
                    <td class="text1" height="30">From Date</td>
                    <td class="text1" height="30">
                        <input type="text" class="form-control"  name="fromDate" value="" >
                        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.tallyXML.fromDate,'dd-mm-yyyy',this)"/>
                    </td>
                    <td class="text1" height="30">TO Date</td>
                    <td class="text1" height="30">
                        <input type="text" class="form-control"  readonly name="toDate" value="" >
                        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.tallyXML.toDate,'dd-mm-yyyy',this)"/>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp; </td>
                </tr>
                <tr>
                    <td>&nbsp; </td>
                    <td  align="right" class="text2" height="30">
                        <input type="button" class="button" readonly name="search" value="search" onClick="searchSubmit();" >
                    </td>
                    <td class="text2" height="30">
                        <input type="button" class="button" readonly name="add" value="add" onClick="tallyXMLGen();" >
                    </td>
                </tr>

            </table>
                <br>
                <br>
           <% int index=0;
                String classText="";
                int oddEven=0;
            %>
            <c:if test="${tallyXMLSummary != null}">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" id="bg" class="border">
                    <tr>
                    <td class="contentsub" height="30"><div class="contentsub">S.No</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">FromDate</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">ToDate</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">XMLto be generated Date</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Req.Data Type</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Created Date</div></td>
                    <td class="contentsub" height="30"><div class="contentsub">Action</div></td>
                    </tr>
                    <c:forEach items="${tallyXMLSummary}" var="tallyXML">
                        <%
                        oddEven = index%2;
                        if(oddEven > 0)
                            {
                            classText = "Text2";
                            }
                        else{
                            classText = "Text1";
                            }
                        %>
                        <tr>
                    <td class="<%=classText %>" height="30"><%= index+1 %></td>
                    <td class="<%=classText %>" height="30"><c:out value="${tallyXML.fromDate}"/></td>
                    <td class="<%=classText %>" height="30"><c:out value="${tallyXML.toDate}"/></td>
                    <td class="<%=classText %>" height="30"><c:out value="${tallyXML.requiredDate}"/></td>
                    <td class="<%=classText %>" height="30"><c:out value="${tallyXML.dataType}"/></td>
                    <td class="<%=classText %>" height="30"><c:out value="${tallyXML.createdDate}"/></td>

                    <td class="<%=classText %>" height="30"><input type="hidden" name="xmlId" value="<c:out value='${tallyXML.xmlId}'/>">
                        <a href="/throttle/modifyTallyXMLPage.do?xmlId=<c:out value='${tallyXML.xmlId}'/>" > Alter </a> </td>
                    <%
                        index++;
                    %>
                    </c:forEach>
                </table>
            </c:if>
    </body>
    </html>
