
<%-- 
    Document   : InvoiceDetailsReportExcel
    Created on : Apr 28, 2016, 7:06:41 PM
    Author     : Gulshan kumar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
            <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "GrPrintReport-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
            <c:if test = "${grDetailsReport != null}" >
                <table border="1" bgcolor="#F3FAB6" align="center" width="100%" cellpadding="0" cellspacing="0" >
                    <thead>
                        <tr>
                            <td class="contenthead">S.no</td>
                            <td class="contenthead">GR No</td>
                            <td class="contenthead">Print Date</td>
                            <td class="contenthead">User Name</td>
                            
                        </tr>
                    </thead>
                    <% int index = 0,sno = 1;%>
                    <c:forEach items="${grDetailsReport}" var="csList">
                        <tr>
                            <td align="center"><%=sno%></td>
                            <td align="center"><c:out value="${csList.grNo}"/></td>
                            <td align="center"><c:out value="${csList.grDate}"/></td>
                            <td align="center"><c:out value="${csList.userName}"/></td>
                           
                        </tr>
                        <%  
                   index++;
                     sno++;
                        %>
                    </c:forEach>
                </table>

                    <br>
                <br>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>

           
 

</html>

