<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>
        <script type="text/javascript">
            function setValues(){
                var temp = document.getElementById("customerIdTemp").value;
                //alert(temp);
                var tempStr = temp.split("~");
                document.invoice.customerId.value = tempStr[0];
                document.invoice.ledgerId.value = tempStr[1];
                submitPage();
            }

            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });
        </script>

    </head>
    <script language="javascript">
        
        function submitPage(){
                 if(document.getElementById("customerIdTemp").value == ""){
                    alert('please select customer');
                    document.getElementById("customerIdTemp").focus();
                }
                if(document.getElementById("customerIdTemp").value != ""){
                    
                    var customerName = document.getElementById('customerIdTemp').options[document.getElementById('customerIdTemp').selectedIndex].text;
                    document.invoice.customerName.value = customerName;
                    //alert(document.invoice.customerId.value);
                    document.invoice.action = "/throttle/supplyCreditNoteDetails.do";
                    document.invoice.submit();
                }
            }
            
        
    </script>
    
    
    <style>
    #index td {
   color:white;
}
</style>
             <div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="finance.label.SUPPYINVOICECREDIT"  text="SUPPY CREDIT INVOICE"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance"  text="SUPPY CREDIT INVOICE"/></a></li>
            <li class="active"><spring:message code="finance.label.SUPPYINVOICECREDIT"  text="SUPPY CREDIT INVOICE"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body"> 
    
    
    
    <body>
        <form name="invoice" method="post" >
              <%@ include file="/content/common/message.jsp" %>
          
            <table class="table table-info mb30 table-hover">
                <thead>
                    <tr >
                        <th colspan="1"  align="center">
                        <spring:message code="finance.label.SUPPLYINVOICECREDIT"  text="SUPPY CREDIT INVOICE"/></th>
                </tr>
                </thead>
                <tr >
                    <!--<td width="80" >&nbsp;</td>-->
                         <input type="hidden" name="customerId" id="customerId" value="0" />
                         <input type="hidden" name="ledgerId" id="ledgerId" value="0" />
                         <td width="80" align="center"><b><spring:message code="finance.label.Customer"  text="default text"/></b> &nbsp;&nbsp; <select name="customerIdTemp" id="customerIdTemp" onchange="setValues();" class="form-control" style="width:280px;height:40px;" >
                               <c:if test="${customerList != null}">
                                <option value="" selected>--<spring:message code="finance.label.Select"  text="default text"/>--</option>
                                <c:forEach items="${customerList}" var="customerList">
                                    <option value='<c:out value="${customerList.customerId}"/>~<c:out value="${customerList.ledgerId}"/>'><c:out value="${customerList.customerName}"/></option>
                                </c:forEach>
                            </c:if>
                                </select>
                               <input type="hidden" name="customerName" id="customerName" value="" > </td>
                </tr>
            </table>
            <br/>
             <table align="center" width="100%" border="0">
                <tr >
                   <!--<td colspan="2" align="center"> <input type="button" id="Search" value="Search" class="button" onClick="submitPage();">-->
                </tr>
            </table>

          
    </body>
</div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>