<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<!--<title>PAPL</title>-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script>
    function submitPage()
    {
           
        if(textValidation(document.addRack.rackName,'Rack Name')){
   
        return;
        }  
        if(textValidation(document.addRack.rackDescription,'Rack Description')){
   
        return;
         }  
        document.addRack.action='/throttle/handleAddRack.do';
        document.addRack.submit();
    }
    function setFocus(){
        document.addRack.rackName.focus();
        }
   
</script>

<script>
   function changePageLanguage(langSelection){

            if(langSelection== 'ar'){
            document.getElementById("pAlign").style.direction="rtl";
            }else if(langSelection== 'en'){
            document.getElementById("pAlign").style.direction="ltr";
            }
        }

    </script>

     <div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.Racks"  text="Racks"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="general.label.stores"  text="Stores"/></a></li>
          <li class="active"><spring:message code="stores.label.Racks"  text="Racks"/></li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">
      <div class="panel-body">

<body onload="setFocus();">
<form name="addRack" method="post" >


<!--<table class="table table-bordered table-hover">-->
<td colspan="4"  style="background-color:#5BC0DE;padding:2px;">
<table class="table table-info mb30 table-hover" id="bg" >
    <thead>
<tr>
<th colspan="4"  height="30"><spring:message code="stores.label.AddRackTypes"  text="default text"/></th>
</tr>
    </thead>
<%
    String classText = "";
    classText = "text2";       
   
    %>
<tr>
<td  height="30"><font color="red">*</font><spring:message code="stores.label.RackName"  text="default text"/></td>
<td  height="30"><input name="rackName" type="text" class="form-control" style="width:260px;height:40px;" value=""></td>
<!--</tr>-->
<%
   classText = "text1";
 %>
<!--<tr>-->
<td  height="30"><font color="red">*</font><spring:message code="stores.label.RackDescription"  text="default text"/></td></td>
<td  height="30"><textarea class="form-control" style="width:260px;height:40px;"  name="rackDescription"></textarea></td>
</tr>
</table>
</td>
</table>
<br>
<center>
<input type="button" value="<spring:message code="stores.label.ADD"  text="default text"/>" class="btn btn-success" onclick="submitPage();">
&emsp;<input type="reset" class="btn btn-success" value="<spring:message code="stores.label.Clear"  text="default text"/>">
</center>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

