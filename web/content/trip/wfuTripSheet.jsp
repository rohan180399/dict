
<%@page import="java.text.SimpleDateFormat" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/validate.js"></script>

<!--        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />-->

<!--        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>-->

        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>






        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">

            function computeShortage(val){
                //alert(val);
                var loaded = document.getElementById("loadedpackages"+val).value;
                var unloaded = document.getElementById("unloadedpackages"+val).value;
                if(unloaded == ''){
                    alert('invalid no of unloaded packs. please check');
                    document.getElementById("unloadedpackages"+val).focus();
                }else if(parseFloat(unloaded) > parseFloat(loaded)){
                    alert('invalid no of unloaded packs. please check');
                    document.getElementById("unloadedpackages"+val).focus();
                }else if(parseFloat(unloaded) > 0){
                    document.getElementById("shortage"+val).value = loaded - unloaded;
                }
            }
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });



    </script>
       
        <script  type="text/javascript" src="js/jq-ac-script.js"></script>


        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $("#tabs").tabs();
            });
        </script>


         <script type="text/javascript">
            function submitPage(){
            if(isEmpty(document.getElementById("vehicleactreportdate").value)){
            alert('please enter the vehicle reporting date');
            document.getElementById("vehicleactreportdate").focus();
            }
            if(isEmpty(document.getElementById("vehicleactreporthour").value)){
            alert('please enter  the vehicle reporting time');
            document.getElementById("vehicleactreporthour").focus();
            alert("fdsfsdf");
            }
           else{
            document.endTripSheet.action = '/throttle/saveWFUTripSheet.do';
            document.endTripSheet.submit();
           }
            }
            

        </script>

    </head>


    <body  >

        <form name="endTripSheet" method="post">
             <%
        Date today = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String endDate = sdf.format(today);
        %>
            <%@ include file="/content/common/path.jsp" %>
            <br>

<table width="300" cellpadding="0" cellspacing="0" align="right" border="0" id="report" style="margin-top:0px;">

                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:300;">

                            <div id="first">
                                <c:if test = "${tripDetails != null}" >
                                    <c:forEach items="${tripDetails}" var="trip">
                                <table width="300" cellpadding="0" cellspacing="1" border="0" align="center">
                                    <tr id="exp_table" >
                                        <td> <font color="white"><b>Expected Revenue:</b></font></td>
                                        <td> <c:out value="${trip.orderRevenue}" /></td>

                                     </tr>
                                    <tr id="exp_table" >
                                        <td> <font color="white"><b>Projected Expense:</b></font></td>
                                        <td> <c:out value="${trip.orderExpense}" /></td>

                                     </tr>
                                    <c:set var="profitMargin" value="" />
                                     <c:set var="orderRevenue" value="${trip.orderRevenue}" />
                                     <c:set var="orderExpense" value="${trip.orderExpense}" />
                                     <c:set var="profitMargin" value="${orderRevenue - orderExpense}" />
                                     <%
                                     String profitMarginStr = "" + (Double)pageContext.getAttribute("profitMargin");
                                     String revenueStr = "" + (String)pageContext.getAttribute("orderRevenue");
                                     float profitPercentage = 0.00F;
                                     if(!"".equals(revenueStr) && !"".equals(profitMarginStr)){
                                         profitPercentage = Float.parseFloat(profitMarginStr)*100/Float.parseFloat(revenueStr);
                                     }


                                     %>
                                    <tr id="exp_table" >
                                        <td> <font color="white"><b>Profit Margin:</b></font></td>
                                        <td>  <%=new DecimalFormat("#0.00").format(profitPercentage)%>(%)
                                            <input type="hidden" name="profitMargin" value='<c:out value="${profitMargin}" />'>
                                        <td>

                                        <td>
                                     </tr>
                                </table>
                                </c:forEach>
                             </c:if>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br>


<br>
<br>
<br>
<br>
<table width="100%">
                <% int loopCntr = 0;%>
                <c:if test = "${tripDetails != null}" >
                    <c:forEach items="${tripDetails}" var="trip">
                        <% if(loopCntr == 0) {%>
                        <tr>
                            <td class="contenthead" >Vehicle: <c:out value="${trip.vehicleNo}" /></td>
                            <td class="contenthead" >Trip Code: <c:out value="${trip.tripCode}"/></td>
                            <td class="contenthead" >Customer Name:&nbsp;<c:out value="${trip.customerName}"/></td>
                            <td class="contenthead" >Route: &nbsp;<c:out value="${trip.routeInfo}"/></td>
                            <td class="contenthead" >Status: <c:out value="${trip.status}"/></td>
                            <input type="hidden" name="tripCodeEmail" value='<c:out value="${trip.tripCode}"/>' />
                            <input type="hidden" name="customerNameEmail" value='<c:out value="${trip.customerName}"/>' />
                            <input type="hidden" name="routeInfoEmail" value='<c:out value="${trip.routeInfo}"/>' />
                            <input type="hidden" name="tripType" value='<c:out value="${tripType}"/>' />
                            <input type="hidden" name="statusId" value='<c:out value="${statusId}"/>' />
                        </tr>
                        <% }%>
                        <% loopCntr++;%>
                    </c:forEach>
                </c:if>
            </table>
            <div id="tabs" >
                <ul>

                    <li><a href="#tripWFC"><span>Trip WFC Details</span></a></li>
                    <li><a href="#tripDetail"><span>Trip Details</span></a></li>
                    <li><a href="#routeDetail"><span>Consignment Note(s) / Route Course / Trip Plan </span></a></li>
                    <li><a href="#advance"><span>Advance</span></a></li>
<!--                    <li><a href="#preStart"><span>Trip Pre Start Details </span></a></li>-->
                    <li><a href="#startDetail"><span>Trip Start Details</span></a></li>
                     <li><a href="#podDetail"><span>Trip POD Details</span></a></li>
                      <li><a href="#statusDetail"><span>Status History</span></a></li>
                    <!--
                    <li><a href="#cleanerDetail"><span>Cleaner</span></a></li>-->
<!--                    <li><a href="#advDetail"><span>Advance</span></a></li>-->
                    <!--<li><a href="#expDetail"><span>Expense Details</span></a></li>-->
<!--                    <li><a href="#summary"><span>Remarks</span></a></li>-->
                </ul>

                <div id="tripDetail">
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contenthead" colspan="6" >Trip Details</td>
                        </tr>

                        <c:if test = "${tripDetails != null}" >
                            <c:forEach items="${tripDetails}" var="trip">


                        <tr>
<!--                            <td class="text1"><font color="red">*</font>Trip Sheet Date</td>
                            <td class="text1"><input type="text" name="tripDate" class="datepicker" value=""></td>-->
                            <td class="text1">Consignment No(s)</td>
                            <td class="text1">
                                <c:out value="${trip.cNotes}" />
                                <input type="hidden" name="cNotesEmail" value='<c:out value="${trip.cNotes}"/>' />
                            </td>
                            <td class="text1">Billing Type</td>
                            <td class="text1">
                                <c:out value="${trip.billingType}" />
                            </td>
                        </tr>
                        <tr>
<!--                            <td class="text2">Customer Code</td>
                            <td class="text2">BF00001</td>-->
                            <td class="text2">Customer Name</td>
                            <td class="text2">
                                <c:out value="${trip.customerName}" />
                                <input type="hidden" name="customerName" Id="customerName" class="textbox" value='<c:out value="${customerName}" />'>
                            </td>
                            <td class="text2">Customer Type</td>
                            <td class="text2" colspan="3" >
                                <c:out value="${trip.customerType}" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text1">Route Name</td>
                            <td class="text1">
                                <c:out value="${trip.routeInfo}" />
                            </td>
<!--                            <td class="text1">Route Code</td>
                            <td class="text1" >DL001</td>-->
                            <td class="text1">Reefer Required</td>
                            <td class="text1" >
                                <c:out value="${trip.reeferRequired}" />
                            </td>
                            <td class="text1">Order Est Weight (MT)</td>
                            <td class="text1" >
                                <c:out value="${trip.totalWeight}" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text2">Vehicle Type</td>
                            <td class="text2">
                                <c:out value="${trip.vehicleTypeName}" />
                            </td>
                            <td class="text2"><font color="red">*</font>Vehicle No</td>
                            <td class="text2">
                                <c:out value="${trip.vehicleNo}" />
                                <input type="hidden" name="vehicleno" value="<c:out value="${trip.vehicleNo}" />"/>
                                <input type="hidden" name="vehicleNoEmail" value='<c:out value="${trip.vehicleNo}"/>' />

                            </td>
                            <td class="text2">Vehicle Capacity (MT)</td>
                            <td class="text2">
                                <c:out value="${trip.vehicleTonnage}" />

                            </td>
                        </tr>

                        <tr>
                            <td class="text1">Veh. Cap [Util%]</td>
                            <td class="text1">
                                <c:out value="${trip.vehicleCapUtil}" />
                            </td>
                            <td class="text1">Special Instruction</td>
                            <td class="text1">-</td>
                            <td class="text1">Trip Schedule</td>
                            <td class="text1"><c:out value="${trip.tripScheduleDate}" />  <c:out value="${trip.tripScheduleTime}" />
                             <input type="hidden" name="tripScheduleDate"  id="tripScheduleDate" value='<c:out value="${trip.tripScheduleDate}" />'>
                            <input type="hidden" name="tripScheduleTime" id="tripScheduleTime" value='<c:out value="${trip.tripScheduleTime}" />'></td>
                        </tr>


                        <tr>
                            <td class="text2"><font color="red">*</font>Driver </td>
                            <td class="text2" colspan="5" >
                                 <c:out value="${trip.driverName}" />
                            </td>

                        </tr>
                        <tr>
                            <td class="text1">Product Info </td>
                            <td class="text1" colspan="5" >
                                 <c:out value="${trip.productInfo}" />
                            </td>

                        </tr>
                            </c:forEach>
                       </c:if>
                    </table>
                    <br/>
                    <br/>

                    <c:if test = "${expiryDateDetails != null}" >
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contenthead" colspan="4" >Vehicle Compliance Check</td>
                        </tr>
                          <c:forEach items="${expiryDateDetails}" var="expiryDate">
                        <tr>
                            <td class="text2">Vehicle FC Valid UpTo</td>
                            <td class="text2"><label><font color="green"><c:out value="${expiryDate.fcExpiryDate}" /></font></label></td>
                        </tr>
                        <tr>
                            <td class="text1">Vehicle Insurance Valid UpTo</td>
                            <td class="text1"><label><font color="green"><c:out value="${expiryDate.insuranceExpiryDate}" /></font></label></td>
                        </tr>
                        <tr>
                            <td class="text2">Vehicle Permit Valid UpTo</td>
                            <td class="text2"><label><font color="green"><c:out value="${expiryDate.permitExpiryDate}" /></font></label></td>
                        </tr>
                        <tr>
                            <td class="text2">Road Tax Valid UpTo</td>
                            <td class="text2"><label><font color="green"><c:out value="${expiryDate.roadTaxExpiryDate}" /></font></label></td>
                        </tr>
                        </c:forEach>
                    </table>
                    <br/>
                    <br/>
                    <center>
                        <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Next" /></a>
                    </center>
                       </c:if>

                </div>
                 <div id="podDetail">
                      <c:if test="${viewPODDetails != null}">
                            <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                <tr>
                                    <th width="50" class="contenthead">S No&nbsp;</th>
                                    <th class="contenthead">City Name</th>
                                    <th class="contenthead">POD file Name</th>
                                    <th class="contenthead">LR Number</th>
                                    <th class="contenthead">POD Remarks</th>
                                </tr>
                                <% int index2 = 1;%>
                                <c:forEach items="${viewPODDetails}" var="viewPODDetails">
                                     <%
                                        String classText3 = "";
                                        int oddEven = index2 % 2;
                                        if (oddEven > 0) {
                                            classText3 = "text1";
                                        } else {
                                            classText3 = "text2";
                                        }
                            %>
                                    <tr>
                                        <td class="<%=classText3%>" ><%=index2++%></td>
                                        <td class="<%=classText3%>" ><c:out value="${viewPODDetails.cityName}"/></td>
                                      <td class="<%=classText3%>" ><a  href="JavaScript:popUp('/throttle/uploadFiles/Files/<c:out value="${viewPODDetails.podFile}"/>');"><c:out value="${viewPODDetails.podFile}"/></a></td>
                                        <td class="<%=classText3%>" ><c:out value="${viewPODDetails.lrNumber}"/></td>
                                        <td class="<%=classText3%>" ><c:out value="${viewPODDetails.podRemarks}"/></td>
                                    </tr>
                                </c:forEach>

                            </table>
                        </c:if>
                       </div>

                  <div id="routeDetail">

                    <c:if test = "${tripPointDetails != null}" >
                    <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                        <tr >
                                <td class="contenthead" height="30" >S No</td>
                                <td class="contenthead" height="30" >Point Name</td>
                                <td class="contenthead" height="30" >Type</td>
                                <td class="contenthead" height="30" >Route Order</td>
                                <td class="contenthead" height="30" >Address</td>
                                <td class="contenthead" height="30" >Planned Date</td>
                                <td class="contenthead" height="30" >Planned Time</td>
                      </tr>
                        <% int index2 = 1; %>
                        <c:forEach items="${tripPointDetails}" var="tripPoint">
                         <%
                                        String classText1 = "";
                                        int oddEven = index2 % 2;
                                        if (oddEven > 0) {
                                            classText1 = "text1";
                                        } else {
                                            classText1 = "text2";
                                        }
                            %>
                        <tr >
                                 <td class="<%=classText1%>" height="30" ><%=index2++%></td>
                                <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointName}" /></td>
                                <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointType}" /></td>
                                <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointSequence}" /></td>
                                <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointAddress}" /></td>
                                <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointPlanDate}" /></td>
                                <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointPlanTime}" /></td>
                      </tr>
                        </c:forEach >
                    </table>
                    <br/>

                    <table border="0" class="border" align="centver" width="100%" cellpadding="0" cellspacing="0" >
                            <c:if test = "${tripDetails != null}" >
                                <c:forEach items="${tripDetails}" var="trip">
                                    <tr>
                                        <td class="text1" width="150"> Estimated KM</td>
                                        <td class="text1" width="120" > <c:out value="${trip.estimatedKM}" />&nbsp;</td>
                                        <td class="text1" colspan="4">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="text2" width="150"> Estimated Reefer Hour</td>
                                        <td class="text2" width="120"> <c:out value="${trip.estimatedTransitHours * 60 / 100}" />&nbsp;</td>
                                        <td class="text2" colspan="4">&nbsp;</td>
                                    </tr>

                                </c:forEach>
                            </c:if>
                        </table>
                    <br/>
                    <br/>
                     <center>
                            <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Save" /></a>
                        </center>
                    </c:if>
                    <br>
                    <br>
                </div>
<!--                     <div id="preStart">

                    <c:if test = "${tripPreStartDetails != null}" >
                        <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                            <c:forEach items="${tripPreStartDetails}" var="preStartDetails">
                            <tr>
                            <td class="contenthead" colspan="4" >Trip Pre Start Details</td>
                        </tr>
                                <tr >
                                <td class="text1" height="30" >Trip Pre Start Date</td>
                                    <td class="text1" height="30" ><c:out value="${preStartDetails.preStartDate}" /></td>
                              <td class="text1" height="30" >Trip Pre Start Time</td>
                                    <td class="text1" height="30" ><c:out value="${preStartDetails.preStartTime}" /></td>
                            </tr>
                              <tr>
                                <td class="text2" height="30" >Trip Pre Start Odometer Reading(KM)</td>
                                    <td class="text2" height="30" ><c:out value="${preStartDetails.preOdometerReading}" /></td>
                            <c:if test = "${tripDetails != null}" >
                                    <td class="text2">Trip Pre Start Location / Distance</td>
                                    <c:forEach items="${tripDetails}" var="trip">
                            <td class="text2"> <c:out value="${trip.preStartLocation}" /> / <c:out value="${trip.preStartLocationDistance}" />KM</td>
                                    </c:forEach>
                            </c:if>
                            </tr>
                            <tr>
                                    <td class="text1" height="30" >Trip Pre Start Remarks</td>
                                    <td class="text1" height="30" ><c:out value="${preStartDetails.preTripRemarks}" /></td>
                            </tr>
                            </c:forEach >
                        </table>
                        <br/>
                        <br/>
                        <br/>
                         <center>
                            <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Save" /></a>
                        </center>
                    </c:if>
                    <br>
                    <br>

                </div>-->
                 <div id="startDetail">
                    <c:if test = "${tripStartDetails != null}" >
                        <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                            <c:forEach items="${tripStartDetails}" var="startDetails">
                           <tr>
                            <td class="contenthead" colspan="6" > Trip Start Details</td>
                        </tr>
                         <tr>
                                <td class="text1" height="30" >Trip Planned Start Date</td>
                                    <td class="text1" height="30" ><c:out value="${startDetails.planStartDate}" />&nbsp;</td>
                                <td class="text1" height="30" >Trip Planned Start Time</td>
                                    <td class="text1" height="30" ><c:out value="${startDetails.planStartTime}" />&nbsp;</td>
                                <td class="text1" height="30" >Trip Start Reporting Date</td>
                                    <td class="text1" height="30" ><c:out value="${startDetails.startReportingDate}" />&nbsp;</td>

                        </tr>
                            <tr>
                                    <td class="text2" height="30" >Trip Start Reporting Time</td>
                                    <td class="text2" height="30" ><c:out value="${startDetails.startReportingTime}" />&nbsp;</td>
                                <td class="text2" height="30" >Trip Loading date</td>
                                    <td class="text2" height="30" ><c:out value="${startDetails.loadingDate}" />&nbsp;</td>
                                <td class="text2" height="30" >Trip Loading Time</td>
                                    <td class="text2" height="30" ><c:out value="${startDetails.loadingTime}" />&nbsp;</td>
                            </tr>
                                <tr >
                                <td class="text1" height="30" >Trip Loading Temperature</td>
                                    <td class="text1" height="30" ><c:out value="${startDetails.loadingTemperature}" />&nbsp;</td>
                                <td class="text1" height="30" >Trip Actual Start Date</td>
                                    <td class="text1" height="30" ><c:out value="${startDetails.startDate}" />&nbsp;</td>
                                <td class="text1" height="30" >Trip Actual Start Time</td>
                                    <td class="text1" height="30" ><c:out value="${startDetails.startTime}" />&nbsp;</td>
                            </tr>
                            <tr>
                                    <td class="text2" height="30" >Trip Start Odometer Reading(KM)</td>
                                    <td class="text2" height="30" ><c:out value="${startDetails.startOdometerReading}" />&nbsp;</td>
                                <td class="text2" height="30" >Trip Start Reefer Reading(HM)</td>
                                    <td class="text2" height="30" ><c:out value="${startDetails.startHM}" />&nbsp;</td>
                                    <td class="text2" height="30" colspan="2" ></td>
                            </tr>
                            </c:forEach >
                        </table>

                         <c:if test = "${tripUnPackDetails != null}" >
                    <table border="0" class="border" align="left" width="100%" cellpadding="0" cellspacing="0" id="addTyres1">
                                    <tr>
                                        <td colspan="10" class="contenthead" align="center" height="30" >Consignment Unloading Details</td>
                                    </tr>
                                    <tr>
                                        <td width="20" class="contenthead" align="center" height="30" >Sno</td>
                                        <td class="contenthead" height="30" >Product/Article Code</td>
                                        <td class="contenthead" height="30" >Product/Article Name </td>
                                        <td class="contenthead" height="30" >Batch </td>
                                        <td class="contenthead" height="30" ><font color='red'>*</font>No of Packages</td>
                                        <td class="contenthead" height="30" ><font color='red'>*</font>Uom</td>
                                        <td class="contenthead" height="30" ><font color='red'>*</font>Total Weight (in Kg)</td>
                                        <td class="contenthead" height="30" ><font color='red'>*</font>Loaded Package Nos</td>
                                        <td class="contenthead" height="30" ><font color='red'>*</font>UnLoaded Package Nos</td>
                                        <td class="contenthead" height="30" ><font color='red'>*</font>Shortage</td>
                                    </tr>


                                         <%int i1=1;%>
                                         <c:forEach items="${tripUnPackDetails}" var="tripunpack">
                                            <tr>
                                                <td><%=i1%></td>
                                                <td><input type="text"  name="productCodes" id="productCodes" value="<c:out value="${tripunpack.articleCode}"/>" readonly/></td>
                                                <td><input type="text" name="productNames" id="productNames" value="<c:out value="${tripunpack.articleName}"/>" readonly/></td>
                                                <td><input type="text" name="productbatch" id="productbatch" value="<c:out value="${tripunpack.batch}"/>" readonly/></td>
                                                <td><input type="text" name="packagesNos" id="packagesNos" value="<c:out value="${tripunpack.packageNos}"/>" readonly/></td>
                                                <td><input type="text" name="productuom" id="productuom" value="<c:out value="${tripunpack.uom}"/>" readonly/></td>
                                                <td><input type="text" name="weights" id="weights" value="<c:out value="${tripunpack.packageWeight}"/> " readonly/>
                                                <td><input type="text" name="loadedpackages" id="loadedpackages<%=i1%>" value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly/>
                                                    <input type="hidden" name="consignmentId" value="<c:out value="${tripunpack.consignmentId}"/>"/>
                                                    <input type="hidden" name="tripArticleId" value="<c:out value="${tripunpack.tripArticleid}"/>"/>
                                                </td>
                                                <td><input type="text" name="unloadedpackages" id="unloadedpackages<%=i1%>" onblur="computeShortage(<%=i1%>);"  value="0"  onKeyPress="return onKeyPressBlockCharacters(event);"    /></td>
                                                <td><input type="text" name="shortage" id="shortage<%=i1%>"  value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly /></td>
                                            </tr>
                                            <%i1++;%>
                                         </c:forEach>

                    <br/>
                      </table>
                                         <br/>
                                         <br/>
                         <center>
                            <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Save" /></a>
                        </center>
                      </c:if>
                    <br>
                    <br>
                    </c:if>
                </div>
                  <c:if test="${tripAdvanceDetails != null}">
                          <div id="advance">
                      <c:if test="${tripAdvanceDetails != null}">
                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <tr>
                                <td class="contenthead" width="30">Sno</td>
                                <td class="contenthead" width="90">Advance Date</td>
                                <td class="contenthead" width="90">Trip Day</td>
                                <td class="contenthead" width="120">Estimated Advance</td>
                                <td class="contenthead" width="120">Requested Advance</td>
                                <td class="contenthead" width="90"> Type</td>
                                <td class="contenthead" width="120">Requested By</td>
                                <td class="contenthead" width="120">Requested Remarks</td>
                                <td class="contenthead" width="120">Approved By</td>
                                <td class="contenthead" width="120">Approved Remarks</td>
                                <td class="contenthead" width="120">Paid Advance</td>
                            </tr>
                            <%int index7=1;%>
                            <c:forEach items="${tripAdvanceDetails}" var="tripAdvance">
                                <c:set var="totalAdvancePaid" value="${ totalAdvancePaid + tripAdvance.paidAdvance + totalpaidAdvance}"></c:set>
                                 <%
                                        String classText7 = "";
                                        int oddEven7 = index7 % 2;
                                        if (oddEven7 > 0) {
                                            classText7 = "text1";
                                        } else {
                                            classText7 = "text2";
                                        }
                            %>
                                <tr>
                                    <td class="<%=classText7%>"><%=index7++%></td>
                                    <td class="<%=classText7%>"><c:out value="${tripAdvance.advanceDate}"/></td>
                                    <td class="<%=classText7%>">DAY&nbsp;<c:out value="${tripAdvance.tripDay}"/></td>
                                    <td class="<%=classText7%>"><c:out value="${tripAdvance.estimatedAdance}"/></td>
                                    <td class="<%=classText7%>"><c:out value="${tripAdvance.requestedAdvance}"/></td>
                                      <c:if test = "${tripAdvance.requestType == 'A'}" >
                                         <td class="<%=classText7%>">Adhoc</td>
                                      </c:if>
                                      <c:if test = "${tripAdvance.requestType == 'B'}" >
                                         <td class="<%=classText7%>">Batch</td>
                                      </c:if>
                                      <c:if test = "${tripAdvance.requestType == 'M'}" >
                                         <td class="<%=classText7%>">Manual</td>
                                      </c:if>
                                     <td class="<%=classText7%>"><c:out value="${tripAdvance.approvalRequestBy}"/></td>
                                     <td class="<%=classText7%>"><c:out value="${tripAdvance.approvalRequestRemarks}"/></td>
                                     <td class="<%=classText7%>"><c:out value="${tripAdvance.approvedBy}"/></td>
                                     <td class="<%=classText7%>"><c:out value="${tripAdvance.approvalRemarks}"/></td>
                                    <td class="<%=classText7%>"><c:out value="${tripAdvance.paidAdvance + totalpaidAdvance}"/></td>
                                </tr>
                            </c:forEach>

                                <tr></tr>
                                    <tr>

                                    <td class="text1">&nbsp;</td>
                                    <td class="text1">&nbsp;</td>
                                    <td class="text1">&nbsp;</td>
                                    <td class="text1">&nbsp;</td>
                                    <td class="text1">&nbsp;</td>
                                         <td class="text1">&nbsp;</td>
                                     <td class="text1"></td>
                                     <td class="text1"></td>
                                     <td class="text1" colspan="2">Total Advance Paid</td>
                                    <td class="text1"><c:out value="${totalAdvancePaid}"/></td>
                                </tr>
                        </table>
                        <br/>
                        <c:if test="${tripAdvanceDetailsStatus != null}">
                         <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <tr>
                            <td class="contenthead" colspan="13" > Advance Approval Status Details</td>
                        </tr>
                             <tr>
                                <td class="contenthead" width="30">Sno</td>
                                <td class="contenthead" width="90">Request Date</td>
                                <td class="contenthead" width="90">Trip Day</td>
                                <td class="contenthead" width="120">Estimated Advance</td>
                                <td class="contenthead" width="120">Requested Advance</td>
                                <td class="contenthead" width="90"> Type</td>
                                <td class="contenthead" width="120">Requested By</td>
                                <td class="contenthead" width="120">Requested Remarks</td>
                                <td class="contenthead" width="120">Approval Status</td>
                                <td class="contenthead" width="120">Paid Status</td>
                            </tr>
                            <%int index13=1;%>
                            <c:forEach items="${tripAdvanceDetailsStatus}" var="tripAdvanceStatus">
                                 <%
                                        String classText13 = "";
                                        int oddEven11 = index13 % 2;
                                        if (oddEven11 > 0) {
                                            classText13 = "text1";
                                        } else {
                                            classText13 = "text2";
                                        }
                            %>
                                <tr>

                                    <td class="<%=classText13%>"><%=index13++%></td>
                                    <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.advanceDate}"/></td>
                                    <td class="<%=classText13%>">DAY&nbsp;<c:out value="${tripAdvanceStatus.tripDay}"/></td>
                                    <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.estimatedAdance}"/></td>
                                    <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.requestedAdvance}"/></td>
                                      <c:if test = "${tripAdvanceStatus.requestType == 'A'}" >
                                         <td class="<%=classText13%>">Adhoc</td>
                                      </c:if>
                                      <c:if test = "${tripAdvanceStatus.requestType == 'B'}" >
                                         <td class="<%=classText13%>">Batch</td>
                                      </c:if>
                                      <c:if test = "${tripAdvanceStatus.requestType == 'M'}" >
                                         <td class="<%=classText13%>">Manual</td>
                                      </c:if>

                                     <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.approvalRequestBy}"/></td>
                                     <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.approvalRequestRemarks}"/></td>
                                     <td class="<%=classText13%>">
                                    <c:if test = "${tripAdvanceStatus.approvalStatus== ''}" >
                                      &nbsp
                                    </c:if>
                                    <c:if test = "${tripAdvanceStatus.approvalStatus== '1' }" >
                                        Request Approved
                                    </c:if>
                                    <c:if test = "${tripAdvanceStatus.approvalStatus== '2' }" >
                                        Request Rejected
                                    </c:if>
                                    <c:if test = "${tripAdvanceStatus.approvalStatus== '0'}" >
                                        Approval in  Pending
                                    </c:if>
                                    &nbsp;</td>
                                         <td class="<%=classText13%>">
                                    <c:if test = "${ tripAdvanceStatus.approvalStatus== '1' || tripAdvanceStatus.approvalStatus== 'N'}" >
                                        Yet To Pay
                                    </c:if>
                                        &nbsp;
                                         </td>
                                </tr>
                            </c:forEach>
                         </table>
                                <br/>
                                <br/>
                         </c:if>
                        <br/>
                        <center>
                            <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Save" /></a>
                        </center>
                    </c:if>
                </div>
                    </c:if>
                 <div id="statusDetail">
                    <% int index1 = 1; %>

                    <c:if test = "${statusDetails != null}" >
                        <table border="0endDetail" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                            <tr >
                                <td class="contenthead" height="30" >S No</td>
                                <td class="contenthead" height="30" >Status Name</td>
                                <td class="contenthead" height="30" >Remarks</td>
                                <td class="contenthead" height="30" >Created User Name</td>
                                <td class="contenthead" height="30" >Created Date</td>
                            </tr>
                            <c:forEach items="${statusDetails}" var="statusDetails">
                                 <%
                                        String classText = "";
                                        int oddEven1 = index1 % 2;
                                        if (oddEven1 > 0) {
                                            classText = "text1";
                                        } else {
                                            classText = "text2";
                                        }
                            %>
                                <tr >
                                    <td class="<%=classText%>" height="30" ><%=index1++%></td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.statusName}" /></td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.tripRemarks}" /></td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.userName}" /></td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.tripDate}" /></td>
                                </tr>
                            </c:forEach >
                        </table>
                        <br/>
                        <br/>
                        <br/>
                    </c:if>
                    <br>
                    <br>

                </div>
                <div id="tripWFC">


                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                      <tr>
                            <td class="contenthead" colspan="4" >Vehicle Reporting Details</td>
                        </tr>
                        <tr>
                            <tr>
                            <td class="text2"><font color="red">*</font>Vehicle  Reporting Date</td>
                            <td class="text2"><input type="text" name="vehicleactreportdate" id="vehicleactreportdate" class="datepicker" value="<%=endDate%>"></td>
                            <td class="text2" height="25" ><font color="red">*</font>Vehicle  Reporting Time </td>
                            <td class="text2" colspan="3" align="left" height="25" >HH:<select name="vehicleactreporthour" id="vehicleactreporthour" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                MI:<select name="vehicleactreportmin" id="vehicleactreportmin" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></td>
                        </tr>

                            <td class="text1" >WFU  Remarks</td>
                        <td class="text1" >

                            <textarea rows="3" cols="30" class="textbox" name="wfuRemarks" id="wfuRemarks"   style="width:142px"></textarea>
                            <input type="hidden" name="tripSheetId" value="<%=request.getParameter("tripSheetId")%>" />
                        </tr>
                    </table>
                    <br/>
                    <br/>


                    <br/>
                   

                        <center>
                            <input type="button" class="button" name="Save" value="Save" onclick="submitPage();" />
                        </center>
                    </div>




                <script>
                    $(".nexttab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected + 1);
                    });
                </script>

            </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>