<%-- 
    Document   : driverSettlementReportExcel
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>

        <form name="BPCLTransaction" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "TripExtraExpense-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
<c:if test="${endedTripDetails != null}">
                  <table align="center" border="1" id="table" class="sortable" style="width:100%;" >
                    <thead height="30">
                        <tr id="tableDesingTH" height="30">
                            <th align="center"><h3>S.No</h3></th>
                           <th align="center"><h3>Vehicle No</h3></th>
                           <th align="center"><h3>GR NO</h3></th>
                           <th align="center"><h3>Billing Party</h3></th>
                           <th align="center"><h3>Trip Expense</h3></th>
                            <th align="center"><h3>Trip Advance</h3></th>
                            <!--<th align="center"><h3>Print</h3></th>-->
                             
                        </tr> 
                    </thead>
                            <tbody>
                    <% int index = 1;%>
                            <c:forEach items="${endedTripDetails}" var="eTrip">
                                  <c:set var="days" value="${totalDays}"/>
                                  
                                    <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                                    %>
                                        <tr>
                                            <td  align="right" ><%=index++%></td>
                                            <td align="right"  >
                                               <c:out value="${eTrip.vehicleNo}"/></td>
                                            <td   align="right" ><c:out value="${eTrip.grNo}"/></td>
                                            <td  align="right" ><c:out value="${eTrip.billingParty}"/></td>
                                             <td align="right"  ><c:out value="${eTrip.tripExpense}"/></td>
                                            <td  align="right"><c:out value="${eTrip.paidExpense}"/></td>
                                            <!--<td  align="right"> <a href="getTripExpensePrintDetails.do?tripId=<c:out value="${eTrip.tripId}"/>&param=ExportExcel">Print</a></td>-->
                                             
                                        </tr>

                            </c:forEach>
                </tbody>
                </table>
            </c:if>
</form>
    </body>
</html>