<%-- 
Document   : rolefunctions
Created on : Jul 23, 2008, 11:04:14 AM
Author     : vidya
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import = "ets.domain.users.business.LoginTO" %>
<%@ page import="java.util.*" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="javascript">
            function getfunction() {
                document.rolefunctions.action = '/throttle/getfunc.do';
                document.rolefunctions.reqfor.value = 'getFunctions';
                document.rolefunctions.submit();
            }

            function copyAddress(availableFunc)
            {
                var avilableFunction = document.getElementById("availableFunc");
                var index = 0;
                var selectedLength = 0;
                if(avilableFunction.length != 0){
                    for (var i=0; i<avilableFunction.options.length ; i++) {
                        if(avilableFunction.options[i].selected == true){
                            selectedLength++;
                        }
                    }
                    for (var j=0; j<selectedLength ; j++) {
                        for (var i=0; i<avilableFunction.options.length ; i++) {
                            if(avilableFunction.options[i].selected == true){
                
                                var optionCounter = document.rolefunctions.assignedfunc.length;
                                document.rolefunctions.assignedfunc.length = document.rolefunctions.assignedfunc.length +1;
                                document.rolefunctions.assignedfunc.options[optionCounter].text = avilableFunction.options[i].text ;
                                document.rolefunctions.assignedfunc.options[optionCounter].value =  avilableFunction.options[i].value;
                                avilableFunction.options[i] = null;
                                break;
                            }
                        }
                    }
                    /*if(availableFunc != ""){
        var optionCounter = document.rolefunctions.assignedfunc.length;
        document.rolefunctions.assignedfunc.length = document.rolefunctions.assignedfunc.length +1;
        document.rolefunctions.assignedfunc.options[optionCounter].text = document.rolefunctions.availableFunc.options[document.rolefunctions.availableFunc.selectedIndex].text ;
        document.rolefunctions.assignedfunc.options[optionCounter].value = document.rolefunctions.availableFunc.options[document.rolefunctions.availableFunc.selectedIndex].value ;
        document.rolefunctions.availableFunc.options[document.rolefunctions.availableFunc.selectedIndex]=null;

        }*/
                }
                else {
                    alert("Please Select any Value");
                }
            }

            function copyAddress1(assRole)
            {
                //var value =
                var selectedValue = document.getElementById('assignedfunc');
                var index = 0;
                var selectedLength = 0;
                if(selectedValue.length != 0){
                    for (var i=0; i<selectedValue.options.length ; i++) {
                        if(selectedValue.options[i].selected == true){
                            selectedLength++;
                        }
                    }
                    //alert(selectedLength);
                    //alert(selectedValue.options.length);
                    for (var j=0; j<selectedLength ; j++) {
                        for (var i=0; i<selectedValue.options.length ; i++) {
                            if(selectedValue.options[i].selected == true){
                
                                var optionCounter = document.rolefunctions.availableFunc.length;
                                document.rolefunctions.availableFunc.length = document.rolefunctions.availableFunc.length +1;
                                document.rolefunctions.availableFunc.options[optionCounter].text = selectedValue.options[i].text ;
                                document.rolefunctions.availableFunc.options[optionCounter].value =  selectedValue.options[i].value;
                                selectedValue.options[i] = null;
                                break;
                            }
                        }
                    }
                    //document.rolefunctions.availableFunc.length = document.rolefunctions.availableFunc.length +1;
                    //document.rolefunctions.availableFunc.options[optionCounter].text = document.rolefunctions.assignedfunc.options[document.rolefunctions.assignedfunc.selectedIndex].text ;
                    //document.rolefunctions.availableFunc.options[optionCounter].value = document.rolefunctions.assignedfunc.options[document.rolefunctions.assignedfunc.selectedIndex].value ;
                    //document.rolefunctions.assignedfunc.options[document.rolefunctions.assignedfunc.selectedIndex] = null;

                }
                else {
                    alert("Please Select any Value");
                }
            }

            function setRole() {
                var roleId = '<%= request.getAttribute("rolelist")%>';

                if(roleId != 'null') {
                    document.rolefunctions.rolesId.value=roleId;

                } else {
                    //
                }
            }
            function submitPage() {

                var length = document.rolefunctions.availableFunc.length;
                var counter = document.rolefunctions.assignedfunc.length;
                for(var j = 0; j < counter; j++){
                    document.rolefunctions.assignedfunc.options[j].selected = true;
                }
                document.rolefunctions.action='/throttle/assignfunction.do';
                document.rolefunctions.reqfor.value='assignFunctions';
                document.rolefunctions.submit();
            }
            window.onload = setRole;
        </script>
    </head>
    <!--[if lte IE 7]>
    <style type="text/css">

    #fixme {display:block;
    top:0px; left:0px;  position:fixed;  }
    </style>
    <![endif]-->

    <!--[if lte IE 6]>
    <style type="text/css">
    body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
    #fixme {display:block;
    top:0px; left:0px;  position:fixed;  }
    * html #fixme  {position:absolute;}
    </style>
    <![endif]-->

    <!--[if lte IE 6]>
    <style type="text/css">
    /*<![CDATA[*/
    html {overflow-x:auto; overflow-y:hidden;}
    /*]]>*/
    </style>
    <![endif]-->
    <body>
        <form name="rolefunctions" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <!-- pointer table -->
            <%@ include file="/content/common/message.jsp" %>
            <!-- message table -->
            <!-- copy there  end -->
            <br>
            <br>
            <c:if test = "${roleList != null}">
                <table width="350" align="center" class="border" cellpadding="0" cellspacing="0" id="bg"  >
                    <!--
                    <tr height="30">
                        <td class="contenthead" colspan="4"><div align="center">Assign Role-Functions</div></td>
                    </tr>
                    -->
                    <tr>
                        <td class="text1" align="center" height="45"> Role Types</td>
                        <td align="center" height="30" class="text1">
                            <select name="rolesId" class="form-control">
                                <option value="0">-Select-</option>

                                <c:forEach items="${roleList}" var="role">
                                    <option value='<c:out value="${role.roleId}" />'><c:out value="${role.roleName}" /></option>
                                </c:forEach >

                            </select>  </td>
                        <th height="45" class="text1" ><input type="button" value="Go" class="button" onClick="getfunction()"></th>
                    </tr>
                </table><br><br>
                <table align="center" border="1" cellpadding="0" cellspacing="0" width="500"  id="bg">
                    <tr>
                        <td class="contenthead" valign="center" height="35" >Available Functions </td>
                        <td class="contenthead" height="35"></td>
                        <td class="contenthead" valign="center" height="35">Assigned Functions </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table width="150" height="150" cellpadding="0" cellspacing="0" align="center" id="bg">
                                <tr>
                                    <td width="150"><select style="width:200px;" multiple size="10" id="availableFunc" name="availableFunc" class="form-control">
                                            <c:if test = "${AvailableFunction != null}" >
                                                <c:forEach items="${AvailableFunction}" var="roles">
                                                    <option value='<c:out value="${roles.functionId}" />'><c:out value="${roles.functionName}" /></option>
                                                </c:forEach >
                                            </c:if>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td height="100" valign="center">
                            <table width="20" height="70" cellpadding="2" cellspacing="2" align="center" id="bg">
                                <tr>
                                    <td height="15" align="center" bgcolor="#F4F4F4" style=" border:1px; border-bottom-style:solid; "><input type="button" class="form-control" value=">" onClick="copyAddress(availableFunc.value)"></td></tr>
                                <tr >
                                    <td height="15" align="center" bgcolor="#F4F4F4" style=" border:1px; "><input type="button" class="form-control" value="<" onClick="copyAddress1(assignedfunc.value)">
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top">
                            <table width="150" height="150" cellpadding="0" cellspacing="0" align="center" id="bg">
                                <tr>
                                    <td width="150"><select style="width:200px;" multiple size="10" id="assignedfunc" name="assignedfunc"  class="form-control">
                                            <c:if test = "${AssignedFucntions != null}">
                                                <c:forEach items="${AssignedFucntions}" var="role">
                                                    <option value='<c:out value="${role.functionId}" />'><c:out value="${role.functionName}" /></option>
                                                </c:forEach >
                                            </c:if>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </c:if>
            <br>
            <center>
                <input type="button" name="save" value="Save" onClick="submitPage();" class="button">
                <input type="hidden" value="" name="reqfor">
            </center>
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>  
</html>