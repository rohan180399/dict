<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
       
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
</head>
<script language="javascript">
    function submitPage(value){
      
      if(value=="Add"){
        document.tank.action='/throttle/addTankDetailPage.do';
      
        document.tank.submit();
        }else{
        document.tank.action='/throttle/alterTankDetailPage.do';
      
        document.tank.submit();
        }
        
        }
     function searchPage(){
             document.tank.action='/throttle/searchTankDetailPage.do';      
            document.tank.submit();
         }
         function setValues(){
             if('<%=request.getAttribute("fromDate")%>'!='null'){
             document.tank.fromDate.value='<%=request.getAttribute("fromDate")%>';
             }if('<%=request.getAttribute("toDate")%>'!='null'){
             document.tank.toDate.value='<%=request.getAttribute("toDate")%>';
             }if('<%=request.getAttribute("companyId")%>'!='null'){
             document.tank.companyId.value='<%=request.getAttribute("companyId")%>';
             }
             }
</script>
<body onload="setValues();">
<form name="tank" method="post">
     <%@ include file="/content/common/path.jsp" %>                 
        <%@ include file="/content/common/message.jsp" %>
        <br>
             <table align="center" width="950" border="0"  cellspacing="0" cellpadding="0">
             <tr>
                 <td>
            <table align="center" width="650" border="0" class="border"  cellspacing="0" cellpadding="0">
                <tr>
             <td class="text1"><font color="red">*</font>From Date</td>
                    <td class="text1"><input name="fromDate" class="form-control" type="text"  size="20">
                         <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.tank.fromDate,'dd-mm-yyyy',this)"/></td>
                    <td class="text1" ><font color="red">*</font>TO Date</td>
                    <td class="text1" ><input name="toDate" class="form-control" type="text"  size="20">
                         <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.tank.toDate,'dd-mm-yyyy',this)"/></td>
                         <td class="text1"><font color="red">*</font>Filling Station</td>
                    <td class="text1"><select class="form-control" name="companyId" style="width:125px">
                        <option value="0">---Select---</option>
                        <c:if test = "${CompanyList != null}" >
                            <c:forEach items="${CompanyList}" var="comp"> 
                            <c:choose>
                            <c:when test="${comp.companyTypeId==1012}" >
                                <option value='<c:out value="${comp.cmpId}" />'><c:out value="${comp.name}" /></option>
                            </c:when>
                            </c:choose>
                           </c:forEach>
                        </c:if>  	
                </select></td>
                        </tr>
                     </table>
                     <br>
                     <center><input type="button" class="button" name="Search" value="Fetch Data" onclick="searchPage();"></center>
                     <br>
                 </td>
             </tr>
             <tr>
                 <td>
     <c:if test = "${TankDetails != null}" >
    <c:set var="amount" value="0"/>     
<table align="center" width="650" border="0" class="border" cellspacing="0" cellpadding="0">
 <tr height="30">
  <Td colspan="6" class="contenthead">Tank Details</Td>
 </tr>
 
  <tr height="30">
    <td class="contentsub">Date</td>  
     <td class="contentsub">Filling Station</td>
      <td class="contentsub">Filled Fuel</td>       
        <td class="contentsub">Current Reading(Ltrs)</td>         
         <td class="contentsub">Filed Fuel Amount</td>
        
  </tr>
   <% int index=0;%>
   <c:forEach items="${TankDetails}" var="tank"> 
      <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                    %>	
  <tr height="30">   
    <td class="<%=classText%>"><c:out value="${tank.date}"/></td>    
    <td class="<%=classText%>"><c:out value="${tank.companyName}"/></td>       
    <td class="<%=classText%>"><c:out value="${tank.fuelFilled}"/></td>   
    <td class="<%=classText%>"><c:out value="${tank.present}"/></td>           
    <td class="<%=classText%>"><c:out value="${tank.rate}"/></td>    
    <c:set var="amount" value="${amount + tank.rate}"/>
  </tr>
  <% index++;%>
</c:forEach>
</table>
</c:if>
</td>
<td valign="top">
<table width="250" align="right" border="0" cellpadding="0" cellspacing="0" class="border">
<tr> 
<td colspan="2" height="30" class="blue" align="center" style="border:1px;border-color:#FFFFFF;border-bottom-style:dashed;">Summary</td>
</tr>

<tr> 
<td width="121" height="30" class="bluetext">Fuel Amount</td>

<td width="96" height="30"  class="bluetext"><fmt:formatNumber value="${amount}" pattern="##.00"/>SAR</td>
</tr>
</table>
</td>
</tr>
</table>
<br><br>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
