<!DOCTYPE html>
<%@page import="java.text.SimpleDateFormat" %>
<html>
    <head>

        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>

        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>



        <script language="">
            function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }


            function setValues() {
                var idTemp = document.getElementById('vehicleIdTemp').value;
                if (idTemp != '') {
                    var temp = idTemp.split("~");
                    document.getElementById('vehicleId').value = temp[0];
                    document.getElementById('vehicleTypeId').value = temp[1];
                    document.getElementById('vehicleTypeName').value = temp[2];
                    document.getElementById('vehicleNo').value = temp[3];
                } else {
                    document.getElementById('vehicleId').value = 0;
                    document.getElementById('vehicleTypeId').value = 0;
                    document.getElementById('vehicleTypeName').value = "";
                    document.getElementById('vehicleNo').value = "";
                }
            }
            function submitPage(value) {
            //    setDriverName();
           var temp;
        var selectedIndex= document.getElementsByName("selectedIndex");
        var cntr = 0;
               for(var i=0;i<selectedIndex.length;i++){
                   if(selectedIndex[i].checked==true){
                       if(cntr==0){

                    temp= selectedIndex[i].value;
                       }else{
                           temp=temp + ','+selectedIndex[i].value;
                       }
                       cntr++;
                   }

               }
               alert(temp);
       
                $("#createTrip").hide();
                if (document.getElementById('productCategoryId').value == '') {
                    alert("Please select Product Category");
                    document.getElementById('productCategoryId').focus();
//                } else if (document.getElementById('vehicleIdTemp').value == '0') {
//                    alert("Please select vehicle ");
//                    document.getElementById('vehicleIdTemp').focus();
                } else {
                    document.cNote.action = "/throttle/saveLeasingCustomerTripSheet.do?temp="+temp;
                    document.cNote.submit();
                }
            }
            $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user

        $('#primaryDriver').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getPrimaryDriver.do",
                    dataType: "json",
                    data: {
                        driverName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        var primaryDriver = $('#primaryDriver').val();
                        if(items == '' && primaryDriver != ''){
                            alert("Invalid Primary Driver Name");
                            $('#primaryDriver').val('');
                            $('#primaryDriverId').val('');
                            $('#primaryDriver').focus();
                        }else{
                        }
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var id = ui.item.Id;
                var settledDate = ui.item.settledDate;
                var mobileNo= ui.item.mobileNo;
                $('#primaryDriver').val(value);
                $('#primaryDriverId').val(id);
                $('#fromDate').val(settledDate);
                $('#mobileNo').text(mobileNo);
                return false;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
            .data("item.autocomplete", item)
            .append("<a>" + itemVal + "</a>")
            .appendTo(ul);
        };
    });


        </script>
    </head>


    <body onload="setProductCategoryValues();">
        <% String menuPath = "Secondary Operation >>  Create Trip Sheet";
            request.setAttribute("menuPath", menuPath);
        %>
        <form name="cNote" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <%@include file="/content/common/message.jsp" %>
            <%
                Date today = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                String startDate = sdf.format(today);
            %>
            <input type="hidden" name="startDate" value='<c:out value="${scheduleDate}"/>' />
            <input type="hidden" name="scheduleMonth" value='<c:out value="${scheduleMonth}"/>' />
            <input type="hidden" name="vehicleNo" id="vehicleNo" value='' />


            <br>
            <br>
            <div id="tabs">


                <div id="customerDetail">
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="contractCustomerTable">
                        <tr>
                            <td class="contenthead"  >Vehicle No: </td>
                            <td class="contenthead"  ><b><c:out value="${vehicleNo}"/> </b></td>
                            <td class="contenthead"  >For Month of: </td>
                            <td class="contenthead"  ><b><c:out value="${scheduleMonth}"/> </b></td>
                        </tr>
                        <tr>
                            <td class="text1"><font color="red">*</font>Customer Name</td>
                        <input type="hidden" name="entryType" value="1" >
                        <input type="hidden" name="customerTypeId" id="customerTypeId" value="1" >
                        <input type="hidden" name="consignmentDate" id="consignmentDate"  value="<c:out value="${scheduleDate}"/>" >
                        <input type="hidden" name="orderReferenceNo" id="orderReferenceNo"  value="Secondary" >
                        <input type="hidden" name="orderReferenceRemarks" id="orderReferenceRemarks"  value="Secondary" >
                        <input type="hidden" name="consignmentOrderInstruction" id="consignmentOrderInstruction"  value="Secondary" >
                        <input type="hidden" name="multiPickup" id="multiPickup" value="N"  >
                        <input type="hidden"  name="multiDelivery" id="multiDelivery"  value="N"  >
                        <input type="hidden"  id="totalPackage" name="totalPackage" value="0"  >
                        <input type="hidden"  id="totalWeightage" name="totalWeightage" value="0"  >
                        <input type="hidden"  id="consignorName" name="consignorName" value="<c:out value="customerName"/>"  >
                        <input type="hidden"  id="consignorPhoneNo" name="consignorPhoneNo" value="<c:out value="mobileNo"/>"  >
                        <input type="hidden"  id="consignorAddress" name="consignorAddress" value="<c:out value="customerAddress"/>"  >
                        <input type="hidden"  id="consigneeName" name="consigneeName" value="<c:out value="customerName"/>"  >
                        <input type="hidden"  id="consigneePhoneNo" name="consigneePhoneNo" value="<c:out value="mobileNo"/>"  >
                        <input type="hidden"  id="consigneeAddress" name="consigneeAddress" value="<c:out value="customerAddress"/>"  >
                        <td class="text1"><input type="hidden" name="customerId" id="customerId" class="textbox" value="<c:out value="${customerId}"/>"/>
                            <input type="hidden" name="customerName" onKeyPress="return onKeyPressBlockNumbers(event);"  id="customerName" class="textbox" value="<c:out value="customerName"/>" /><c:out value="${customerName}"/></td>
                        <td class="text1"><font color="red">*</font>Customer Code</td>
                        <td class="text1"><input type="hidden" name="customerCode" id="customerCode" class="textbox" value="<c:out value="${customerCode}"/>" /><c:out value="${customerCode}"/></td>
                        </tr>
                        <tr>
                            <td class="text2"><font color="red">*</font>Address</td>
                            <td class="text2"><input type="hidden" id="customerAddress" name="customerAddress" value="<c:out value="${customerAddress}"/>"><c:out value="${customerAddress}"/></textarea></td>
                            <td class="text2"><font color="red">*</font>Pincode</td>
                            <td class="text2"><input type="hidden"  name="pincode" id="pincode"  onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" value="<c:out value="${pincode}"/>" /><c:out value="${pincode}"/></td>
                        </tr>
                        <tr>
                            <td class="text1"><font color="red">*</font>Mobile No</td>
                            <td class="text1"><input type="hidden"  name="customerMobileNo"  onKeyPress="return onKeyPressBlockCharacters(event);"  id="customerMobileNo" class="textbox"  value="<c:out value="${mobileNo}"/>" /><c:out value="${mobileNo}"/></td>
                            <td class="text1"><font color="red">*</font>E-Mail Id</td>
                            <td class="text1"><input type="hidden"  name="mailId" id="mailId" class="textbox" value="<c:out value="${email}"/>"  /><c:out value="${email}"/></td>
                        </tr>
                        <tr>
                            <td class="text2">Phone No</td>
                            <td class="text2"><input type="hidden" name="customerPhoneNo" id="customerPhoneNo"  onKeyPress="return onKeyPressBlockCharacters(event);"   class="textbox" maxlength="10" value="<c:out value="${phone}"/>"   /><c:out value="${phone}"/></td>

                            <td class="text2">Billing Type</td>
                            <td class="text2" id="billingType"><input type="hidden" name="billingTypeId" id="billingTypeId" class="textbox" value="<c:out value="${billingTypeId}"/>" /><c:out value="${billingTypeName}"/></td>

                        </tr>
                        <tr>
                            <td class="text2">Driver</td>
                            <td class="text2"><input type="text" name="primaryDriver" id="primaryDriver"     class="textbox" value=""   /></td>

                            <td class="text2">Mobile No</td>
                            <td class="text2" id="billingType"><label id="mobileNo"></label></td>

                        </tr>
                    </table>


                    <br>
                    <table class="border" align="left" width="900" cellpadding="0" cellspacing="0" id="routePlan" style="display:none;">
                        <tr>
                            <td class="contenthead" height="30" ><font color="red">*</font>Order Sequence</td>
                            <td class="contenthead" height="30" ><font color="red">*</font>Point Name</td>
                            <td class="contenthead" height="30" ><font color="red">*</font>Point Type</td>
                            <td class="contenthead" height="30" ><font color="red">*</font>Address</td>
                            <!--                            <td class="contenthead" height="30" ><font color="red">*</font>Required Date</td>-->
                            <!--<td class="contenthead" height="30" ><font color="red"></font>Required Time</td>-->
                        </tr>
                        <c:set var="totalKm" value="" />
                        <c:set var="totalHours" value="" />
                        <c:if test="${orderPointDetails != null}">
                              </c:if>
                            <tbody>
                                <c:forEach items="${orderPointDetails}" var="point">
                                </c:forEach>
                                    <tr>
                                        <td class="text1" height="30" ><input type="hidden" name="orderPointName" value="<c:out value="${point.pointName}"/>" /><input type="hidden" name="pointSequence" value="1" /><label>Start Point</label></td>
                                        <td class="text1" height="30" ><input type="hidden" name="orderPointId" value="1" /><label>1</label></td>
                                        <td class="text1" height="30" ><input type="hidden" name="pointType" value="PickUp" /><label>Pickup</label></td>
                                        <td class="text1" height="30" ><input type="hidden" name="pointAddresss" value="<c:out value="${point.pointAddresss}"/>" /><label><c:out value="${point.pointAddresss}"/></label></td>
<!--                                        <td class="text1" height="30" ><input type="hidden" name="pointDate" value="<c:out value="${scheduleDate}"/>" /><label><c:out value="${scheduleDate}"/></label></td>-->
                                    </tr>
                                    <c:set var="totalKm" value="${point.totalKm}" />
                                    <c:set var="totalHours" value="${point.totalHours}" />
                            </tbody>
                      
                        <tr>
                            <td class="text1" height="30" ><b>Total KM</b></td>
                            <td class="text1" height="30" align="left" ><c:out value="${totalKm}"/></td>
                            <td class="text1" height="30" ><b>Total Hours</b></td>
                            <td class="text1" height="30" align="left" ><c:out value="${totalHours}"/> </td>
                        </tr>
                    </table>
                    <br>

                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg" style="display:none;">

                        <tr>
                            <td class="contenthead" colspan="6" >Product Info</td>
                        </tr>
                        <tr>

                            <td class="text1"><font color="red">*</font>Product Category &nbsp;&nbsp;
                                <c:if test="${productCategoryList != null}">
                                    <input type="hidden" name="productCategoryName" id="productCategoryName" class="textbox" />
                                    <input type="hidden" name="productCategoryId" id="productCategoryId" class="textbox" />
                                    <select name="productCategoryIdTemp" id="productCategoryIdTemp" onchange="setProductCategoryValues();"  class="textbox" >
                                        <option value="0">--Select--</option>
                                        <c:forEach items="${productCategoryList}" var="proList">
                                            <c:if test="${proList.customerTypeName == 'Ambient' }">
                                            <option selected value="<c:out value="${proList.productCategoryId}"/>~<c:out value="${proList.customerTypeName}"/>"><c:out value="${proList.customerTypeName}"/></option>
                                            </c:if>
                                        </c:forEach>
                                    </select>
                                </c:if>

                            </td>
                            <td class="text1" colspan="4" align="left" >&nbsp;<label id="temperatureInfo"></label>
                                <input type="hidden" name="origin" value="<c:out value="${0}"/>" />
                                <input type="hidden" name="destination" value="<c:out value="${0}"/>" />
                            </td>
                        </tr>
                    </table>
                    <br>
                    <script>
                        function setProductCategoryValues() {
                            var temp = document.cNote.productCategoryIdTemp.value;
//                            alert(temp);
                            if (temp != 0) {
                                var tempSplit = temp.split('~');
                                document.getElementById("temperatureInfo").innerHTML = 'Reefer Temp (deg Celcius): Min ' + tempSplit[1] + ' Max ' + tempSplit[2];
                                document.getElementById("productCategoryName").value = tempSplit[4]+'Reefer Temp Min ' + tempSplit[1] + ' Max upto' + tempSplit[2]+' deg Celcius';
                                document.cNote.productCategoryId.value = tempSplit[0];
                                var reeferRequired = tempSplit[3];
                                if (reeferRequired == 'Y') {
                                    reeferRequired = 'Yes';
                                } else {
                                    reeferRequired = 'No';
                                }
                                document.cNote.reeferRequired.value = reeferRequired;
                            } else {
                                document.getElementById("temperatureInfo").innerHTML = '';
                                document.cNote.productCategoryId.value = 0;
                                document.cNote.reeferRequired.value = '';
                            }
                        }
                    </script>
              <table  border="0" class="border" align="center" width="30%" cellpadding="0" cellspacing="0" id="bg" style="display:block;border-spacing: 10px;">

                        <tr>
                            <td class="contenthead" colspan="2" >Available Trailer Details</td>
                        </tr>
                        <tr>
                            <td height="30">Trailer No</td>
                            <td height="30">Select</td>
                        </tr>
                        <% int index=1;%>
                        <c:if test="${trailerNos != null}">
                            <c:forEach items="${trailerNos}" var="tNo">
                                <tr>
                                    <td height="30"><c:out value="${tNo.trailerNo}"/></td>
                                    <td height="30"><input type="checkbox" id="selectedIndex<%=index%>" name="selectedIndex" value="<c:out value="${tNo.trailerId}"/>"/></td>
                                </tr>
                                <%index++;%>
                            </c:forEach>
                        </c:if>
              </table>
              <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg" style="display:none;">

                        <tr>
                            <td class="contenthead" colspan="8" >Vehicle (Required) Details</td>
                        </tr>

                        <input type="hidden" name="serviceType" value="1" />
                        <input type="hidden" name="vehTypeId" value="0" />
                        <input type="hidden" name="vehicleTypeName" id="vehicleTypeName" value="<c:out value="${vehicleTypeName}"/>"/>
                        <tr id="vehicleTypeDiv">
<!--                            <td class="text2">Vehicle </td>
                            <td class="text2"  > 
                                <select name="vehicleIdTemp" id="vehicleIdTemp" class="textbox"  style="width:120px;"  onchange="setValues();fetchVehicleDriver();"  >
                                    <c:if test="${vehicleRegNoList != null}">
                                        <option value="0" selected>--Select--</option>
                                        <c:forEach items="${vehicleRegNoList}" var="vehicle">
                                            <option value="<c:out value="${vehicle.vehicleId}"/>~<c:out value="${vehicle.vehicleTypeId}"/>~<c:out value="${vehicle.vehicleTypeName}"/>~<c:out value="${vehicle.regNo}"/>"><c:out value="${vehicle.regNo}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select></td>
                              <td class="text2">Trailer No </td>
                              <td class="text2">
                                <select name="trailerno" id="trailerno" >
                                <option value="0" selected>..Select..</option>
                                <c:if test="${trailerNos != null}">
                                <c:forEach items="${trailerNos}" var="trlNo">
                                <option value='<c:out value="${trlNo.trailerId}"/>-<c:out value="${trlNo.seatCapacity}"/>'><c:out value="${trlNo.trailerNo}"/></option>
                                </c:forEach>
                                </c:if>
                                </select>
                                </td>-->


<!--                            <td class="text2">Driver Name </td>
                            <td class="text2"  > 
                                <select name="primaryDriverId" id="primaryDriverId" class="textbox"  style="width:120px;">
                                    <option value="0">--Select--</option> 
                                </select>
                            </td>-->
                        <input type="hidden" value="" id="primaryDriverId" name="primaryDriverId"/></td>
                        </tr>
                        <script type="text/javascript">
                            function fetchVehicleDriver() {
                                var vehicleId1 = document.cNote.vehicleIdTemp.value;
                                var vehicle = vehicleId1.split("~");
                                var vehicleId = vehicle[0];
                                if (vehicleId != '' && vehicleId != '0') {
                                    $.ajax({
                                        url: '/throttle/fetchVehicleDriver.do',
                                        data: {vehicleId: vehicleId},
                                        dataType: 'json',
                                        success: function(data) {
                                            if (data != '') {

                                                $('#primaryDriverId').empty();
                                                $('#primaryDriverId').append(

                                                        )
                                                $.each(data, function(i, data) {

                                                    $('#primaryDriverId').append(
                                                            $('<option style="width:150px"></option>').val(data.Id).html(data.Name)
                                                            )

                                                });
                                            } else {
                                                $('<option style="width:150px"></option>').val(0).html("--Select--")
                                            }
                                        }
                                    });
                                }

                            }
                            function setDriverName() {
                                var selText2 = primaryDriverId.options[primaryDriverId.selectedIndex].text;
                                document.getElementById("driverName").value = selText2;
                            }
                        </script>
                        <tr>
                            <td class="text1"><font color="red">*</font>Reefer Required</td>
                            <td class="text1" colspan="8" >
                                <input type="text" readonly  name="reeferRequired" id="reeferRequired"  class="textbox"  value="" />
                            </td>
                        </tr>

                        <tr>
                        <input type="hidden" class="textbox" name="routeInfo" id="routeInfo" value="Dummy Route">
                        <input type="hidden" class="textbox" name="originId" id="originId" value="0">
                        <input type="hidden" class="textbox" name="destinationId" id="destinationId" value="0">
                        <input type="hidden" class="textbox" name="routeContractId" id="routeContractId"  value="0">
                        <input type="hidden" class="textbox" name="routeId" id="routeId" value="0">
                        <input type="hidden" class="textbox" name="routeBased" id="routeBased" >
                        <input type="hidden" class="textbox" name="contractRateId" id="contractRateId" value="0">
                        <input type="hidden" class="textbox" name="totalKm" id="totalKm" value="0">
                        <input type="hidden" class="textbox" name="totalHours" id="totalHours" value="0">
                        <input type="hidden" class="textbox" name="totalMinutes" id="totalMinutes" value="0">
                        <input type="hidden" class="textbox" name="totalPoints" id="totalPoints" value="0">
                        <input type="hidden" class="textbox" name="vehicleTypeId" id="vehicleTypeId" value="0">
                        <input type="hidden" class="textbox" name="vehicleId" id="vehicleId" value="<c:out value="${vehicleId}"/>">
                        <input type="hidden" class="textbox" name="rateWithReefer" id="rateWithReefer" value="0">
                        <input type="hidden" class="textbox" name="rateWithoutReefer" id="rateWithoutReefer" value="0">
                        <input type="hidden" readonly class="datepicker" name="vehicleRequiredDate" id="vehicleRequiredDate" value="<c:out value="${scheduleDate}"/>">

                        <input type="hidden" readonly  name="vehicleRequiredHour" id="vehicleRequiredHour" value="00">
                        <input type="hidden" readonly  name="vehicleRequiredMinute" id="vehicleRequiredMinute" value="00">

                        <td class="text2">Special Instruction</td>
                        <td class="text2" colspan="5" ><textarea rows="1" cols="16" name="vehicleInstruction" id="vehicleInstruction"></textarea></td>
                        </tr>
                    </table>     

                    <br/>
                </div>

                <br/>
                <br/>
                <input type="hidden" class="textbox" id="subTotal" name="subTotal"  value="0" readonly="">


                <center>
                    <input type="button" class="button" name="Save" value="Create Trip" id="createTrip" onclick="submitPage(this.value);" >
                </center>
                <!--                </div>-->

            </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>