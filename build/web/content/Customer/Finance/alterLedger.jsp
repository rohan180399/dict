<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<!--<title>PAPL</title>-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->

<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="finance.label.LedgerEdit"  text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance"  text="default text"/></a></li>
            <li class="active"><spring:message code="finance.label.LedgerEdit"  text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <script type="text/javascript">
                $(document).ready(function() {
                    $("#datepicker").datepicker({
                        showOn: "button",
                        buttonImage: "calendar.gif",
                        buttonImageOnly: true

                    });
                });

                $(function() {
                    //alert("cv");
                    $(".datepicker").datepicker({
                        /*altField: "#alternate",
                         altFormat: "DD, d MM, yy"*/
                        changeMonth: true, changeYear: true
                    });
                });
            </script>
            <script>
                function submitPage()
                {
                    //            alert("hi");
                    if (document.alter.openingBalance.value == '') {
                        alert("Please Enter openingBalance");
                        return 'false';
                    }
                    if (document.alter.amountType.value == '0') {
                        alert("Please Enter openingBalance");
                        return 'false';
                    }
                    document.alter.action = '/throttle/saveAlterLedger.do';
                    document.alter.submit();
                }


            </script>
            <body>
                <form name="alter" method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>

                    <%@ include file="/content/common/message.jsp" %>
                    <c:if test="${ledgeralterList != null}">
                        <c:forEach items="${ledgeralterList}" var="LL">
                            <table class="table table-info mb30 table-hover">
                                <thead>
                                    <tr height="30">
                                        <th colspan="4" ><spring:message code="finance.label.EditLedger"  text="default text"/></th>
                                    </tr>
                                </thead>
                                <tr height="30">
                                    <td ><font color="red">*</font><spring:message code="finance.label.LedgerName"  text="default text"/></td>
                                    <td ><c:out value="${LL.ledgerName}"/>
                                        <input type="hidden" name="ledgerID" value="<c:out value="${LL.ledgerID}"/>"> </td>
                                    <!--                        </tr>
                                                            <tr height="30">-->
                                    <td ><font color="red">*</font><spring:message code="finance.label.GroupName"  text="default text"/></td>
                                    <td ><c:out value="${LL.groupname}"/></td>
                                </tr>
                                <tr height="30">
                                    <td ><font color="red">*</font><spring:message code="finance.label.AmountType"  text="default text"/></td>
                                    <td >
                                        <select name="amountType" class="form-control" style="width:260px;height:40px;" >
                                            <c:choose>
                                                <c:when test="${LL.amountType =='CREDIT'}" >
                                                    <option value="CREDIT" selected > <spring:message code="finance.label.CREDIT"  text="default text"/> </option>
                                                    <option value="DEBIT" > <spring:message code="finance.label.DEBIT"  text="default text"/> </option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value="CREDIT" > <spring:message code="finance.label.CREDIT"  text="default text"/> </option>
                                                    <option value="DEBIT" selected > <spring:message code="finance.label.DEBIT"  text="default text"/> </option>
                                                </c:otherwise>
                                            </c:choose>
                                        </select>
                                    </td>
                                    <!--                        </tr>
                                                            <tr height="30">-->
                                    <td ><font color="red">*</font><spring:message code="finance.label.OpeningBalance"  text="default text"/></td>
                                    <td > <input type="text" name="openingBalance" id="openingBalance" class="form-control" style="width:260px;height:40px;" value="<c:out value="${LL.openingBalance}"/>" /> </td>
                                </tr>
                                <tr height="30">

                                    <td ><spring:message code="finance.label.OpeningBalanceDate"  text="default text"/> </td>
                                    <td >  <input type="text" id="date" autocomplete="off" name="date" value="<c:out value="${LL.openingBalanceDate}"/>"   style="width:260px;height:40px;" class="datepicker form-control"></td>
                                    <td></td>
                                    <td></td>
                                </tr>

                            </table>
                        </c:forEach>
                    </c:if>
                    <br>
                    <center><input type="button" class="btn btn-success" value="<spring:message code="finance.label.Save"  text="default text"/>" onclick="submitPage();" /></center>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

