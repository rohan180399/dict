
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import=" javax. servlet. http. HttpServletRequest" %>
<%@page import="java.text.DecimalFormat" %>
<%@page import="java.text.NumberFormat" %>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<script  type="text/javascript" src="js/jq-ac-script.js"></script>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery.flot.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
<!--        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>-->

<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });



</script>
<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>


<script type="text/javascript" language="javascript">
    function getDriverName() {
        var oTextbox = new AutoSuggestControl(document.getElementById("driName"), new ListSuggestions("driName", "/throttle/handleDriverSettlement.do?"));
    }
</script>
<script language="">
    function print(val)
    {
        var DocumentContainer = document.getElementById(val);
        var WindowObject = window.open('', "TrackHistoryData",
                "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        WindowObject.close();
    }

    function popUp(url) {
        var http = new XMLHttpRequest();
        http.open('HEAD', url, false);
        http.send();
        if (http.status != 404) {
            popupWindow = window.open(
                    url, 'popUpWindow', 'height=400,width=500,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes');
        }
        else {
            var url1 = "/throttle/content/trip/fileNotFound.jsp";
            popupWindow = window.open(
                    url1, 'popUpWindow', 'height=400,width=500,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes');
        }
    }

</script>

<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
                    setValues();
                    getVehicleNos();">
    </c:if>

    <!--  <span style="float: right">
            <a href="?paramName=en">English</a>
            |
            <a href="?paramName=ar">???????</a>
      </span>-->
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i>Vehicle Utilization Report</h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="default text"/>:</span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                <li><a href="general-forms.html">Operation</a></li>
                <li class="active">Vehicle Utilization Report</li>
            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">

                <body>
                    <form name="trip" method="post">
                        <div id="tabs" >
                            <ul class="nav nav-tabs">
                                <li class="active" data-toggle="tab"><a href="#vehicleDetail"><span>Vehicle Details</span></a></li>
                                <li data-toggle="tab"><a href="#oemDetail"><span>OEM Details</span></a></li>
                                <li data-toggle="tab"><a href="#insuranceDetail"><span>Insurance</span></a></li>
                                <li data-toggle="tab"><a href="#taxDetail"><span>Road Tax</span></a></li>
                            </ul>
                            <div id="vehicleDetail">
                                <c:if test="${vehicleDetail != null}" >

                                    <table class="table table-bordered" id="bg">
                                        <c:forEach items="${vehicleDetail}" var="veh" >
                                            <tr>
                                                <td colspan="4" height="30" class="contentsub contenthead">View Vehicle</td>
                                            </tr>
                                            <tr>
                                                <td  height="30">Vehicle Number</td>
                                                <td  height="30"><c:out value="${veh.regNo}" /></td>
                                                <td  height="30">Registration Date</td>
                                                <td  height="30"><c:out value="${veh.dateOfSale}" />
                                                </td>
                                            </tr>
                                            <tr>
                                            <input type="hidden" name="vehicleId" value="<c:out value="${veh.vehicleId}" />" >
                                            <td  height="30">Usage</td>
                                            <td>

                                                <c:if test = "${UsageList != null}" >
                                                    <c:forEach items="${UsageList}" var="Dept">
                                                        <c:choose>
                                                            <c:when test="${Dept.usageId == veh.usageId}" >
                                                                <c:out value="${Dept.usageName}" />
                                                            </c:when>
                                                        </c:choose>

                                                    </c:forEach >
                                                </c:if>
                                            </td>
                                            <td  height="30">Warranty(Days)</td>
                                            <td  height="30"><c:out value="${veh.war_period}" />
                                                </tr>
                                            <tr>
                                                <td  height="30">MFRs</td>
                                                <td   >
                                                    <c:if test = "${MfrList != null}" >
                                                        <c:forEach items="${MfrList}" var="Dept">
                                                            <c:choose>
                                                                <c:when test="${Dept.mfrId == veh.mfrId}" >
                                                                    <c:out value="${Dept.mfrName}" />
                                                                </c:when>

                                                            </c:choose>
                                                        </c:forEach >
                                                    </c:if>
                                                </td>
                                                <td  height="30"><font color="red">*</font>Warranty Date</td>
                                                <td  height="30"><c:out value="${veh.warrantyDate}"/></td>
                                            </tr>
                                            <tr>
                                                <td  height="30">Vehicle Type</td>
                                                <td  height="30">
                                                    <!--<option value="1">Own</option>-->
                                                    <c:if test = "${TypeList != null}" >
                                                        <c:forEach items="${TypeList}" var="type">
                                                            <c:choose>
                                                                <c:when test="${type.typeId == veh.typeId}">
                                                            <option  value=<c:out value="${type.typeId}" /> > <c:out value="${type.typeName}" /></option>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <!--<option value=<c:out value="${type.typeId}" /> > <c:out value="${type.typeName}" /></option>-->
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:forEach>
                                            </c:if>
                                            </td>
                                            <td  height="30">Vehicle Class</td>
                                            <td   >
                                                <c:if test = "${ClassList != null}" >
                                                    <c:forEach items="${ClassList}" var="Dept">
                                                        <c:choose>
                                                            <c:when test="${Dept.classId == veh.classId}" >
                                                                <c:out value="${Dept.className}" />
                                                            </c:when>
                                                        </c:choose>
                                                    </c:forEach >
                                                </c:if>
                                            </td>
                                            </tr>
                                            <tr>
                                                <td  height="30">Model </td>
                                                <td  height="30">
                                                    <c:if test = "${modelList != null}" >
                                                        <c:forEach items="${modelList}" var="Dept">
                                                            <c:if test="${Dept.modelId == veh.modelId}" >
                                                                <c:out value="${Dept.modelName}" />
                                                            </c:if>
                                                        </c:forEach >
                                                    </c:if>
                                                </td>
                                                <td  height="30">Tonnage</td>
                                                <td  height="30"><c:out value="${veh.seatCapacity}" /></td>
                                            </tr>
                                            <tr>
                                            </tr>
                                            <tr>
                                                <td  height="30">Engine No</td>
                                                <td  height="30"><c:out value="${veh.engineNo}" /></td>
                                                <td  height="30">Chassis No</td>
                                                <td  height="30"><c:out value="${veh.chassisNo}" /> </td>
                                            </tr>

                                            <tr>
                                                <td  height="30">Operation Point </td>
                                                <td   >
                                                    <c:if test = "${OperationPointList != null}" >
                                                        <c:forEach items="${OperationPointList}" var="Dept">
                                                            <c:choose>
                                                                <c:when test="${Dept.opId == veh.opId}" >
                                                                    <c:out value="${Dept.opName}" />
                                                                </c:when>
                                                            </c:choose>
                                                        </c:forEach>
                                                    </c:if>
                                                </td>

                                                <td  height="30">GPS Tracking System</td>
                                                <td   ><c:out value="${veh.gpsSystem}" />
                                                </td>

                                                <!--                                    <td  height="30">Vehicle Group</td>
                                                                                <input type="hidden" name="custId" value="0"/>
                                                                                <td  height="30">
                                                <c:if test = "${groupList != null}" >
                                                    <c:forEach items="${groupList}" var="grp">
                                                        <c:choose>
                                                            <c:when test="${grp.groupId == veh.groupId}" >
                                                                <c:out value="${grp.groupName}" />
                                                            </c:when>
                                                        </c:choose>
                                                    </c:forEach>
                                                </c:if>
                                            </td>-->

                                                <!--                    <td  height="30">Next FC Date</td>
                                                                    <td  height="30"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.addVehicle.nextFCDate,'dd-mm-yyyy',this)"/></td>                    -->
                                            <input name="nextFCDate" type="hidden" class="form-control" value='<c:out value="${veh.nextFCDate}" />' size="20">
                                            </tr>

                                            <tr>
                                                <td  height="30">KM Reading</td>
                                                <td   ><c:out value="${veh.kmReading}" /></td>
                                                <!--                    <td  height="30">HM Reading</td>
                                                                    <td  height="30"></td>-->
                                            <input name="hmReading" maxlength='20' type="hidden" class="form-control" value='<c:out value="${veh.hmReading}" />' size="20">
                                            <td  height="30">Daily Run KM</td>
                                            <td  height="30"> <c:out value="${veh.dailyKm}" /> </td>
                                            <!--                    <td  height="30"> Daily Run HM</td>
                                                                <td   > </td>-->
                                            <input name="dailyHm" type="hidden" maxlength='20' class="form-control" value='<c:out value="${veh.dailyHm}" />' size="20">
                                            </tr>

                                            <tr>  
                                                <td  height="30">Ownership</td>
                                                <td  height="30">
                                                    <c:if test = "${veh.ownership == '1'}" >
                                                        Own
                                                    </c:if>
                                                    <c:if test = "${veh.ownership == '2'}" >
                                                        Dedicate
                                                    </c:if>
                                                    <c:if test = "${veh.ownership == '3'}" >
                                                        Hire
                                                    </c:if>
                                                    <c:if test = "${veh.ownership == '4'}" >
                                                        Replacement
                                                    </c:if>
                                                    <input type="hidden" name="ownership" id="ownership" value="<c:out value="${veh.ownership}" />"/>

                                                </td>
                                                <td  height="30">vendor Name</td>
                                                <td  height="30">
                                                    <!--<option value="1">Own</option>-->
                                                    <c:if test = "${leasingCustList != null}" >
                                                        <c:forEach items="${leasingCustList}" var="lcl">
                                                            <c:choose>
                                                                <c:when test="${lcl.vendorId == veh.vendorId}">
                                                            <option value=<c:out value="${lcl.vendorId}" /> > <c:out value="${lcl.vendorName}" /></option>
                                                        </c:when>
                                                        <c:otherwise>
        <!--                                                    <option value=<c:out value="${lcl.vendorId}" /> > <c:out value="${lcl.vendorName}" /></option>-->
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:forEach>
                                            </c:if>
                                            </td>
                                            </tr>
                                            <tr>
                                                <td  height="30">Status</td>
                                                <td   >
                                                    <c:choose>
                                                        <c:when test="${veh.activeInd == 'Y'}" >
                                                            Active
                                                        </c:when>
                                                        <c:otherwise>
                                                            InActive
                                                        </c:otherwise>
                                                    </c:choose>
                                                </td>
                                                <td  height="30" colspan="2" >&nbsp;</td>
                                            </tr>
                                        </c:forEach>
                                    </table>
                                </c:if>
                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                </center>
                            </div>
                            <div id="oemDetail">
                                <c:if test="${vehTyreList != null}" >
                                    <% int index = 0;
                                                String classText = "";
                                    %>
                                    <table class="table table-bordered" id="addTyres">
                                        <tr >
                                            <td  class="contenthead contentsub" align="center" height="30" ><div class="contenthead">Sno</div></td>
                                            <td  class="contenthead contentsub" height="30" ><div class="contenthead">Item</div></td>
                                            <td  class="contenthead contentsub" height="30" ><div class="contenthead">Position</div> </td>
                                            <td  class="contenthead contentsub" height="30" ><div class="contenthead">Tyre Number</div></td>
                                            <td  class="contenthead contentsub" height="30" ><div class="contenthead">Date</div></td>
                                        </tr>
                                        <c:forEach items="${vehTyreList}" var="veh">
                                            <%

                                                        int oddEven = index % 2;
                                                        if (oddEven > 0) {
                                                            classText = "text1";
                                                        } else {
                                                            classText = "text2";
                                                        }
                                            %>
                                            <tr>
                                                <td class="<%=classText%>" height="30" > <%= index + 1%> </td>
                                                <td class="<%=classText%>" height="30" >

                                                    <c:if test="${tyreItemList != null}" >
                                                        <c:forEach items="${tyreItemList}" var="item">
                                                            <c:choose>
                                                                <c:when test="${item.itemId == veh.itemId }" >
                                                                    <c:out value="${item.itemName}" />
                                                                </c:when>
                                                            </c:choose>
                                                        </c:forEach>
                                                    </c:if>
                                                </td>

                                                <td class="<%=classText%>" height="30" >
                                                    <c:if test="${positionList != null}" >
                                                        <c:forEach items="${positionList}" var="item">
                                                            <c:choose>
                                                                <c:when test="${item.posId == veh.posId }" >
                                                                    <c:out value="${item.posName}" />
                                                                </c:when>
                                                            </c:choose>

                                                        </c:forEach>
                                                    </c:if>
                                                </td>

                                                <td class="<%=classText%>" height="30" >
                                                    <c:out value="${veh.tyreNo}" />
                                                    <input type="hidden" name="tyreIds" value='<c:out value="${veh.tyreId}" />' >
                                                </td>
                                                <td class="<%=classText%>" height="30" >
                                                    <c:out value="${veh.tyreD}" />
                                                </td>
                                            </tr>
                                            <% index++;%>
                                        </c:forEach>
                                    </table>
                                </c:if>
                                <center>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                    <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                </center>
                            </div>
                            <div id="insuranceDetail">
                                <c:if test="${vehicleAMC != null}">
                                    <c:forEach items="${vehicleAMC}" var="vAmc" >
                                        <tr>
                                            <td class="texttitle1">AMC Company</td>
                                            <td ><c:out value="${vAmc.amcCompanyName}" /></td>
                                            <td class="texttitle1">AMC Amount</td>
                                            <td ><c:out value="${vAmc.amcAmount}" /></td>
                                        </tr>
                                        <tr>
                                            <td class="texttitle2">AMC Duration</td>
                                            <td ><c:out value="${vAmc.amcDuration}" /></td>
                                            <td class="texttitle2">AMC Cheque Date</td>
                                            <td ><c:out value="${vAmc.amcChequeDate}" /></td>
                                        </tr>
                                        <tr>
                                            <td class="texttitle1">AMC Cheque No</td>
                                            <td ><c:out value="${vAmc.amcChequeNo}" /></td>
                                            <td class="texttitle1">AMC From Date Date</td>
                                            <td ><c:out value="${vAmc.amcFromDate}" /></td>
                                        </tr>
                                        <tr>
                                            <td class="texttitle2">AMC To Date</td>
                                            <td ><c:out value="${vAmc.amcToDate}" /></td>
                                            <td class="texttitle2">Remarks</td>
                                            <td ><c:out value="${vAmc.remarks}" /></td>
                                        </tr>
                                    </c:forEach>
                                </c:if>
                                <center>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                    <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                </center>
                            </div>
                            <div id="taxDetail">
                                <c:if test="${RoadTaxList == null }" >
                                    <br>
                                    <center><font color="red" size="2"> No records found </font></center>
                                    </c:if>
                                    <c:if test="${RoadTaxList != null }" >
                                    <table class="table table-bordered" id="table">
                                        <tr height="50">
                                            <td class="contentsub" height="30"><div class="contentsub">Sno</div></td>
                                            <td class="contentsub" height="30"><div class="contentsub">Vehicle Number</div></td>
                                            <td class="contentsub" height="30"><div class="contentsub">Tax Receipt No</div></td>
                                            <td class="contentsub" height="30"><div class="contentsub">Tax Receipt Date</div></td>
                                            <td class="contentsub" height="30"><div class="contentsub">Tax Paid Location</div></td>
                                            <td class="contentsub" height="30"><div class="contentsub">Tax Amount</div></td>
                                            <td class="contentsub" height="30"><div class="contentsub">Next Tax Date</div></td>
                                        </tr>
                                        <br>
                                        <%
                                            String style = "text1";
                                            int index1 = 1;%>
                                        <c:forEach items="${RoadTaxList}" var="Rtl" >
                                            <%
                                                        if ((index1 % 2) == 0) {
                                                            style = "text1";
                                                        } else {
                                                            style = "text2";
                                                }%>
                                            <tr>
                                                <td class="<%= style%>" height="30" style="padding-left:30px; "> <%= index1%> </td>
                                                <td class="<%= style%>" height="30" style="padding-left:30px; "> <c:out value="${Rtl.regNo}" /></td>
                                                <td class="<%= style%>" height="30" style="padding-left:30px; "><c:out value="${Rtl.roadtaxreceiptno}" /></td>
                                                <td class="<%= style%>" height="30" style="padding-left:30px; "><c:out value="${Rtl.roadtaxreceiptdate}" /></td>
                                                <td class="<%= style%>" height="30" style="padding-left:30px; "><c:out value="${Rtl.roadtaxpaidlocation}" /></td>
                                                <td class="<%= style%>" height="30" style="padding-left:30px; "><c:out value="${Rtl.roadtaxamount}" /></td>
                                                <td class="<%= style%>" height="30" style="padding-left:30px; "><c:out value="${Rtl.nextRoadTaxDate}" /></td>
                                            </tr>
                                            <% index1++;%>
                                        </c:forEach>
                                    </table>
                                </c:if>
                                <center>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                </center>
                                <script language="javascript" type="text/javascript">
                                    setFilterGrid("table");
                                </script>
                                <div id="controls">
                                    <div id="perpage">
                                        <select onchange="sorter.size(this.value)">
                                            <option value="5" selected="selected">5</option>
                                            <option value="10">10</option>
                                            <option value="20">20</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select>
                                        <span>Entries Per Page</span>
                                    </div>
                                    <div id="navigation">
                                        <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                        <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                        <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                        <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                    </div>
                                    <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                                </div>
                                <script type="text/javascript">
                                    var sorter = new TINY.table.sorter("sorter");
                                    sorter.head = "head";
                                    sorter.asc = "asc";
                                    sorter.desc = "desc";
                                    sorter.even = "evenrow";
                                    sorter.odd = "oddrow";
                                    sorter.evensel = "evenselected";
                                    sorter.oddsel = "oddselected";
                                    sorter.paginate = true;
                                    sorter.currentid = "currentpage";
                                    sorter.limitid = "pagelimit";
                                    sorter.init("table", 1);
                                </script>
                            </div>

                            <script>
                           //                                        $(".nexttab").click(function() {
                           //                                            var selected = $("#tabs").tabs("option", "selected");
                           //                                            $("#tabs").tabs("option", "selected", selected + 1);
                           //                                        });
                                $('.btnNext').click(function() {
                                    $('.nav-tabs > .active').next('li').find('a').trigger('click');
                                });
                                $('.btnPrevious').click(function() {
                                    $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                                });
                            </script>

                        </div>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>