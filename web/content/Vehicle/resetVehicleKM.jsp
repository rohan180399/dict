<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>

<SCRIPT>



    function getVehicleNos() {
        var oTextbox = new AutoSuggestControl(document.getElementById("regno"), new ListSuggestions("regno", "/throttle/getVehicleNos.do?"));
        //getVehicleDetails(document.getElementById("regno"));
    }


    var httpReq;
    function callAjax() {


        if (trim(document.workOrder.regno.value) == '') {
            alert("Please Enter Vehicle Registration Number");
            document.workOrder.regno.value = '';
            document.workOrder.regno.select();
            document.workOrder.regno.focus();
            document.workOrder.kmReading.value = '';
            return;
        }
        var vehicleNo = document.workOrder.regno.value;
        var url = '/throttle/checkActualKm.do?vehicleNo=' + vehicleNo;
        if (vehicleNo != '') {
            if (window.ActiveXObject) {
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest) {
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function() {
                processAjax();
            };
            httpReq.send(null);
        }

    }
    function processAjax()
    {
        if (httpReq.readyState == 4)
        {
            if (httpReq.status == 200)
            {
                temp = httpReq.responseText.valueOf();
                var kminfo = temp.split('~');
                document.workOrder.actualKm.value = kminfo[0];
                document.workOrder.totalKm.value = kminfo[1];
                document.workOrder.kmReading.focus();

            }
            else
            {
                alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
            }
        }
    }

    function submitPage()
    {
        if (textValidation(document.workOrder.regno, 'Vehicle No'))
            ;
        else if (numberValidation(document.workOrder.kmReading, 'kmReading'))
            ;
        else if (textValidation(document.workOrder.reason, 'Reason'))
            ;
        else {
            document.workOrder.action = '/throttle/saveResetVehicleKM.do';
            document.workOrder.submit();
        }
    }


</SCRIPT>
    <div class="pageheader">
            <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.ResetVehicleKM"  text="Vehicle"/> </h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
                <ol class="breadcrumb">
                    <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
                    <li><a href="general-forms.html"><spring:message code="stores.label.Vehicle"  text="Vehicle"/></a></li>
                    <li class="active"><spring:message code="stores.label.ResetVehicleKM"  text="Vehicle"/></li>
                </ol>
            </div>
        </div>


        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">

<body onload="document.workOrder.regno.focus();
        getVehicleNos();" >
    <form name="workOrder" method="post">
        <%--<%@ include file="/content/common/path.jsp" %>--%>
                     <%@ include file="/content/common/message.jsp"%> 
                        <table class="table table-info mb30 table-hover" id="bg" border="0" >
                            <thead>
                                <tr>
                                    <th colspan="2"  height="30"><div ><spring:message code="trucks.label.ResetVehicleKM"  text="default text"/> </div></th>
                            </tr>
                            </thead>
                            <tr>    
                                <td  height="30"><font color="red">*</font><spring:message code="trucks.label.VehicleNo"  text="default text"/></td>
                                <td  height="30">
                                    <input name="regno" id="regno" 
                                           maxlength="11" type="text"  class="form-control" style="width:260px;height:40px;" size="20" value=""></td>
                            </tr>
                            <tr>
                                <td  height="70"> <font color="red">*</font><spring:message code="trucks.label.ResetReason"  text="default text"/> </td>
                                <td  height="70">
                                    <textarea name="reason" onkeyup="maxlength(this.form.remark, 300)" onChange="callAjax();" class="form-control" style="width:260px;height:40px;"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td  height="30"> <spring:message code="trucks.label.TotalKMRun"  text="default text"/></td>
                                <td  height="30"><input name="totalKm"  maxlength="20" type="text" readonly class="form-control" style="width:260px;height:40px;" value=""></td>
                            </tr>

                            <tr>
                                <td  height="30"> <spring:message code="trucks.label.CurrentKM"  text="default text"/> </td>
                                <td  height="30"><input name="actualKm"  maxlength="20" type="text" readonly class="form-control" style="width:260px;height:40px;" value=""></td>
                            </tr>

                            <tr>
                                <td  height="30"><font color="red">*</font><spring:message code="trucks.label.ResetKM"  text="default text"/> </td>
                                <td  height="30"><input name="kmReading"  maxlength="20" type="text"  class="form-control" style="width:260px;height:40px;" value=""></td>
                                <!--<td></td>
                                <td></td>-->
                            </tr>

                            <input name="cust" type="hidden" value="Existing Customer" >

                        </table>

                    <br>
                    <center>

                        <input type="button" value="<spring:message code="trucks.label.Reset"  text="default text"/>" name="generate" id="generate" class="btn btn-success" onclick="submitPage();">
                        <input type="reset" value="<spring:message code="trucks.label.Clear"  text="default text"/>" class="btn btn-success">

                    </center>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                    <br>
                    </body>
                </div>
            </div>
        </div>
        <%@ include file="/content/common/NewDesign/settings.jsp" %>
