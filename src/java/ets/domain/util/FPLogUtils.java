/*-----------------------------------------------------------------------------
 * FPLogUtils.java
 * Mar 25, 2008
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
---------------------------------------------------------------------------*/
package ets.domain.util;

import java.sql.*;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.DriverManager;
import java.util.Properties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.openxml4j.opc.internal.FileHelper;

/****************************************************************************
 *
 * Modification Log:
 * -----------------------------------------------------------------------------
 * Ver       Date                              Author                 Change
 * -----------------------------------------------------------------------------
 * 1.0       Mar 25, 2008                      ES			      Created
 *
 *
 *****************************************************************************/
/**
 * <br>FPLogUtils object - Event Protal Log Util class.
 * @author Satheeshkumar P
 * @version 1.0 26 Mar 2008
 * @since   Event Portal Iteration 0
 */
public class FPLogUtils {

    private static final Log LOG = LogFactory.getLog(FPLogUtils.class);

    /**
     * This method would log the statement if the log4jproperties
     * is set to Debug mode.
     * @param loggableStatement - Object to be logged in
     */
    public static void fpDebugLog(Object loggableStatement) {
        if (LOG.isDebugEnabled()) {
            LOG.debug(loggableStatement);
        }
    }

    /**
     * This method would log the statement if the log4jproperties
     * is set to Error mode.
     * @param loggableStatement - Object to be logged in
     */
    public static void fpErrorLog(Object loggableStatement)  {
        if (LOG.isErrorEnabled()) {
            LOG.error(loggableStatement);
        }
        Connection con = null;
        String canonicalHostName = "";
        String hostName = "";
        String hostAddress = "";
        int updateStatus = 0;
        try{
        InetAddress inetAddr = InetAddress.getLocalHost();
        //System.out.println("Canonical host name: " + inetAddr.getCanonicalHostName());
        canonicalHostName = inetAddr.getCanonicalHostName();
        //System.out.println("Host Name: " + inetAddr.getHostName());
        hostName = inetAddr.getHostName();
        //System.out.println("Host Address: " + inetAddr.getHostAddress());
        hostAddress = inetAddr.getHostAddress();
        }catch (UnknownHostException e) {
            e.printStackTrace();
        }
        try{
        String smtp = "";
        String subject = "Error Log For Host Name :"+hostName+" and Host Add:"+hostAddress;
        String content = loggableStatement.toString();
        String to = "throttleerror@gmail.com";
        //System.out.println("content = " + content);
        String fileName = "jdbc_url.properties";
        Properties dbProps = new Properties();
        try{
        InputStream is = (InputStream) FileHelper.class.getClassLoader().getResourceAsStream(fileName);
        dbProps.load(is);
        }catch(Exception exp1){
            exp1.printStackTrace();
        }
        String dbClassName = dbProps.getProperty("jdbc.driverClassName");
        String dbUrl = dbProps.getProperty("jdbc.url");
        String dbUserName = dbProps.getProperty("jdbc.username");
        String dbPassword = dbProps.getProperty("jdbc.password");
        try{
        Class.forName(dbClassName).newInstance();
        }catch(Exception exp1){
            exp1.printStackTrace();
        }
        
            System.out.println("dbUrl : "+dbClassName);
            System.out.println("url : "+dbUrl);
            System.out.println("dbUserName : "+dbUserName);
            System.out.println("dbPassword : "+dbPassword);
        
            con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
        
            System.out.println("connection : "+con);
        
//        PreparedStatement updateMailDetails = null;
//        String sql = "INSERT INTO \n" +
//        "ts_mail_master(MAIL_SUBJECT_TO, MAIL_CONTENT_TO, MAIL_ID_TO, ACTIVE_IND, CREATED_BY, CREATED_ON)  \n" +
//        "VALUES(?,?,?,?,?,now())";
//        updateMailDetails = con.prepareStatement(sql);
//        updateMailDetails.setString(1,subject);
//        updateMailDetails.setString(2,content);
//        updateMailDetails.setString(3,to);
//        updateMailDetails.setString(4,"Y");
//        updateMailDetails.setInt(5,1081);
//        updateStatus = updateMailDetails.executeUpdate();
//        //System.out.println("updateStatus = " + updateStatus);
//        con.close();
    } catch (Exception e) {
            e.printStackTrace();
            //System.out.println("Table doesn't exist.");
        }
        
//        new SendBrattleLogMail(smtp, emailPort, frommailid, password, subject, content, to, "").start();
        return;
    }

    
    /**
     * This method would log the statement if the log4jproperties
     * is set to Error mode.
     * @param loggableStatement - Object to be logged in
     * @param throwable Exception to be logged in
     */
    public static void fpErrorLog(Object loggableStatement, Throwable throwable) {
        if (LOG.isErrorEnabled()) {
            LOG.error(loggableStatement, throwable);
        }
    }

    /**
     * This method would log the statement if the log4jproperties
     * is set to Fatal mode.
     * @param loggableStatement - Object to be logged in
     */
    public static void fpFatalLog(Object loggableStatement) {
        if (LOG.isFatalEnabled()) {
            LOG.fatal(loggableStatement);
        }
    }

    /**
     * This method would log the statement if the log4jproperties
     * is set to FATAL mode.
     * @param loggableStatement - Object to be logged in
     * @param throwable Exception to be logged in
     */
    public static void fpFatalLog(Object loggableStatement, Throwable throwable) {
        if (LOG.isFatalEnabled()) {
            LOG.fatal(loggableStatement, throwable);
        }
    }

    /**
     * This method would log the statement if the log4jproperties
     * is set to INFO mode.
     * @param loggableStatement - Object to be logged in
     */
    public static void fpInfoLog(Object loggableStatement) {
        if (LOG.isInfoEnabled()) {
            LOG.info(loggableStatement);
        }
    }

    /**
     * This method would log the statement if the log4jproperties
     * is set to TRACE mode.
     * @param loggableStatement - Object to be logged in
     */
    public static void fpTraceLog(Object loggableStatement) {
        if (LOG.isTraceEnabled()) {
            LOG.trace(loggableStatement);
        }
    }
}
