<%-- 
    Document   : VehicleWisePandL
    Created on : Jan 18, 2013, 9:51:27 AM
    Author     : Entitle
--%>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->

        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <%@ page import="ets.domain.security.business.SecurityTO" %>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script src="/throttle/js/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script src="/throttle/js/TableSort.js" language="javascript" type="text/javascript"></script>
        <link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />

        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                //alert('hai');
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true
                });
            });
            $(function() {
                $( ".datepicker" ).datepicker({
                    changeMonth: true,changeYear: true
                });
            });
        </script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    </head>
    <script type="text/javascript">

        function submitPage(value)
        {
            if(document.search.fromDate.value == ""){
                    alert(" Select From Date ")
                    document.search.fromDate.focus()
                    return false;
                }
                if(document.search.toDate.value == ""){
                    alert(" Select To Date ")
                    document.search.toDate.focus()
                    return false;
                }
                var selectCompanyId = document.getElementById('companyId').options[document.getElementById('companyId').selectedIndex].text;
                var selectVehicle = document.getElementById('vehicleId').options[document.getElementById('vehicleId').selectedIndex].text;
                var selectVehicleType = document.getElementById('vehicleType').options[document.getElementById('vehicleType').selectedIndex].text;

                document.search.sVehicleNo.value = selectVehicle;
                document.search.sCompanyID.value = selectCompanyId;
                document.search.sVehicleType.value = selectVehicleType;
//sVehicleNo sCompanyID sVehicleType
                document.search.action="/throttle/displyVehicleWisePandL.do";
                document.search.submit();

            }






    </script>
    <body>
        <form name="search" action=""  method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <table cellpadding="0" cellspacing="2" align="center" border="0" width="700" id="report" bgcolor="#97caff" style="margin-top:0px;">
                <tr>
                    <td><b>Search </b></td>
                    <td align="right"><span id="openClose" onclick="displayCollapse();" style="cursor: pointer;">Close</span>&nbsp;</td>
                </tr>
                <tr>
                    <td style="padding:15px;" align="left">
                        <div class="tabs" align="center" style="width:900px">
                            <table width="750" cellpadding="0" cellspacing="1" border="0" align="left" class="table4">
                                <tr>
                                    <td  height="30">From Date</td>
                                    <td  height="30"><input type="text" name="fromDate" class="datepicker" ></td>
                                    <td height="30">To Date</td>
                                    <td height="30"><input type="text" name="toDate" class="datepicker" ></td>
                                </tr>
                                <tr>
                                    <td  height="25" >Vehicle Type</td>
                                    <td  height="25" >
                                        <select name="vehicleType" class="form-control" id="vehicleType" style="width:160px;" >
                                            <option value="0">---Select---</option>
                                            <option value="1">Own</option>
                                            <option value="2">Attach</option>
                                        </select>
                                    </td>
                                    <td height="30">Vehicle</td>
                                    <td height="30">
                                        <select name="vehicleId" onchange="fillData()" id="vehicleId" class="form-control" style="width:120px;" >
                                            <option value=""> -Select- </option>
                                            <c:if test = "${vehicleRegNos != null}" >
                                                <c:forEach items="${vehicleRegNos}" var="vd">
                                                    <option value='<c:out value="${vd.vehicleId}" />'><c:out value="${vd.regNo}" /></option>
                                                </c:forEach >
                                            </c:if>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td  height="25" >Location</td>
                                    <td  height="25" >
                                        <select name="companyId" class="form-control" id="companyId" multiple  style="width:160px;" >
                                            <option value="0">---Select---</option>
                                            <c:if test="${locationList != null}">
                                                <c:forEach items="${locationList}" var="ll">
                                                    <option value="<c:out value="${ll.compId}"/>"><c:out value="${ll.city}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </select>

                                        <input type="hidden" name="sVehicleNo" id="sVehicleNo" value="" >
                                        <input type="hidden" name="sCompanyID" id="sCompanyID" value="" >
                                        <input type="hidden" name="sVehicleType" id="sVehicleType" value="" >
                                    </td>
                                    <td><input align="middle" type="button" class="button"  onclick="submitPage(0);" value="Search"></td>
                                </tr>

                            </table>
                        </div>
                    </td>
                </tr>


            </table>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

    </body>

</html>