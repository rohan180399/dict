/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.programmingfree.excelexamples;

/**
 *
 * @author Arul
 */


import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.data.time.Day;
import org.jfree.data.time.Hour;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

public class XYJFreechart {

   public static void main(String args []) throws ParseException{
       Day today = new Day();
       //////System.out.println("today = " + today);
       SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
       SimpleDateFormat m = new SimpleDateFormat("MMMM");
       SimpleDateFormat d = new SimpleDateFormat("dd");
       SimpleDateFormat y = new SimpleDateFormat("yyyy");
        Date date = dt.parse("2014-01-12 01:00:00");
        String day=m.format(date);
        System.out.println(day);
        System.out.println(d.format(date));
        System.out.println(y.format(date));
        today = new Day(1,10,2014);
        //////System.out.println("today pppppp= " + today);
        TimeSeries series = new TimeSeries( "", Hour.class );
        //////System.out.println("new Hour(1,today) = " + new Hour(1,today));
        series.add(new Hour(1,today), 50);
        series.add(new Hour(2,today), 40);
        series.add(new Hour(3,today), 30);
        series.add(new Hour(4,today), 35);
        series.add(new Hour(5,today), 20);
        series.add(new Hour(6,today), 25);
        series.add(new Hour(7,today), 32);

        TimeSeriesCollection dataset=new TimeSeriesCollection();
        dataset.addSeries(series);

        JFreeChart chart = ChartFactory.createTimeSeriesChart("User Participation Chart",    
         "Time",                    
         "Temperature",            
         dataset,               
         true,                     
         true,              
         false              
        );

      try {
            ChartUtilities.saveChartAsJPEG(new File("d:/chart.jpg"), chart, 800, 600);
            //////System.out.println("Chart is created successfully.");
           } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Problem occurred creating chart.");
        }
    }
}

