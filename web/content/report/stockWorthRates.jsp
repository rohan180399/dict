
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
</head>
<body>
<form name="stockRateReport">
    <%@ include file="/content/common/path.jsp" %>
           
            <%@ include file="/content/common/message.jsp" %>
            <br>
<c:if test = "${rateList != null}" >
<table width="666" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
   <tr>
    <td class="contenthead" height="30" colspan="7">Stock Worth Rate Report </td>
   </tr>
  <tr>
    <td width="51" class="contentsub">Sno</td>
    <td width="102" height="30" class="contentsub">Manufacturer</td>
    <td width="82" class="contentsub">Model</td>    
    <td width="132" height="30" class="contentsub">Item Name</td>        
    <td width="86" height="30" class="contentsub">NewItem Price</td>
    <blockquote>&nbsp;</blockquote>         
    <td width="80" height="30" class="contentsub">RCItem Price</td>    
  </tr>
    <% int index = 0;%> 
                    <c:forEach items="${rateList}" var="rate"> 
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
  <tr>
    <td class="<%=classText %>"><%=index+1%></td>
    <td class="<%=classText %>" height="30"><c:out value="${rate.mfrName}" /></td>
    <td class="<%=classText %>"><c:out value="${rate.modelName}" /></td>
    <td class="<%=classText %>" height="30"><c:out value="${rate.itemName}" /></td>
    <td class="<%=classText %>"><c:out value="${rate.newPrice}" /></td>
    <td class="<%=classText %>" height="30"><c:out value="${rate.totalAmount}" /> </td>  
    
  </tr> 
 <% index++;%>
  </c:forEach> 
</table>
  </c:if>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
