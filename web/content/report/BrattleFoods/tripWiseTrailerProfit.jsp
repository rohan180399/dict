<%-- 
    Document   : Accounts Receivable
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <style type="text/css">
                    .container {width: 960px; margin: 0 auto; overflow: hidden;}
                    .content {width:800px; margin:0 auto; padding-top:50px;}
                    .contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

                    /* STOP ANIMATION */



                    /* Second Loadin Circle */

                    .circle1 {
                            background-color: rgba(0,0,0,0);
                            border:5px solid rgba(100,183,229,0.9);
                            opacity:.9;
                            border-left:5px solid rgba(0,0,0,0);
                    /*	border-right:5px solid rgba(0,0,0,0);*/
                            border-radius:50px;
                            /*box-shadow: 0 0 15px #2187e7; */
                    /*	box-shadow: 0 0 15px blue;*/
                            width:40px;
                            height:40px;
                            margin:0 auto;
                            position:relative;
                            top:-50px;
                            -moz-animation:spinoffPulse 1s infinite linear;
                            -webkit-animation:spinoffPulse 1s infinite linear;
                            -ms-animation:spinoffPulse 1s infinite linear;
                            -o-animation:spinoffPulse 1s infinite linear;
                    }

                    @-moz-keyframes spinoffPulse {
                            0% { -moz-transform:rotate(0deg); }
                            100% { -moz-transform:rotate(360deg);  }
                    }
                    @-webkit-keyframes spinoffPulse {
                            0% { -webkit-transform:rotate(0deg); }
                            100% { -webkit-transform:rotate(360deg);  }
                    }
                    @-ms-keyframes spinoffPulse {
                            0% { -ms-transform:rotate(0deg); }
                            100% { -ms-transform:rotate(360deg);  }
                    }
                    @-o-keyframes spinoffPulse {
                            0% { -o-transform:rotate(0deg); }
                            100% { -o-transform:rotate(360deg);  }
                    }
        </style>
        <script>
            $(document).ready(function() {
                $('.ball, .ball1').removeClass('stop');
                $('.trigger').click(function() {
                    $('.ball, .ball1').toggleClass('stop');
                });
            });

        </script>


        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>

        <script type="text/javascript">
            //auto com

            $(document).ready(function() {
                // Use the .autocomplete() method to compile the list based on input from user
                $('#trailerNo').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getTrailerNo.do",
                            dataType: "json",
                            data: {
                                trailerNo: request.term
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                response(items);
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var tmp = value.split('-');
                        $('#trailerId').val(tmp[0]);
                        $('#trailerNo').val(tmp[1]);
                        return false;
                    }
                }).data("autocomplete")._renderItem = function(ul, item) {
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };
            });


        </script>



        <script type="text/javascript">
            function submitPage(value) {
                if (document.getElementById('fromDate').value == '') {
                    alert("Please select from Date");
                    document.getElementById('fromDate').focus();
                } else if (document.getElementById('toDate').value == '') {
                    alert("Please select to Date");
                    document.getElementById('toDate').focus();
                } else {
                    if (value == 'ExportExcel') {
                        document.accountReceivable.action = '/throttle/handletripTrailerProfit.do?param=ExportExcel';
                        document.accountReceivable.submit();
                    } else {
                        document.accountReceivable.action = '/throttle/handletripTrailerProfit.do?param=Search';
                        document.accountReceivable.submit();
                    }
                }
            }
            function setValue() {
                if ('<%=request.getAttribute("page")%>' != 'null') {
                    var page = '<%=request.getAttribute("page")%>';
                    if (page == 1) {
                        submitPage('search');
                    }
                }
            }

            function viewVehicleDetails(trailerId) {
                window.open('/throttle/viewVehicleTrailer.do?trailerId=' + trailerId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
            }
            
            function viewCustomerProfitDetails(tripIds) {
//            alert(tripIds);
            window.open('/throttle/viewCustomerWiseProfitDetails.do?tripId='+tripIds+"&param=Search", 'PopupPage', 'height = 500, width = 1150, scrollbars = yes, resizable = yes');
            }
        </script>
    </head>
    <body onload="setValue();">
        <form name="accountReceivable" action=""  method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <br>
            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Trailer Wise Profitability</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    <tr>
                                        <td><font color="red"></font>Trailer No</td>
                                        <td height="30">
                                            <input type="hidden" name="trailerId" id="trailerId" value="<c:out value="${trailerId}"/>"/>
                                            <input type="text" name="trailerNo" id="trailerNo" value="<c:out value="${trailerNo}"/>" class="form-control"/>
                                        </td>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                                    </tr>
                                    <tr>
                                        <td><font color="red"></font>Operation Type</td>
                                        <td height="30">
                                            <select name="operationTypeId" id="operationTypeId" class="form-control">
                                                <option value="0">--Select--</option>
                                                <option value="Primary">Primary</option>
                                                <option value="Secondary">Secondary</option>
                                                <option value="All">All</option>
                                            </select>
                                            <script>
                                                document.getElementById("operationTypeId").value = '<c:out value="${operationTypeId}"/>'
                                            </script>
                                        </td>
                                        <!--<td><font color="red"></font>Fleet Center</td>
                                        <td height="30">
                                            <select name="fleetCenterId" id="fleetCenterId" class="form-control">
                                                <option value="0">--Select--</option>
                                                <c:if test="${companyList != null}">
                                                    <c:forEach items="${companyList}" var="cmpList">
                                                        <option value="<c:out value="${cmpList.cmpId}"/>"><c:out value="${cmpList.name}"/></option>
                                                    </c:forEach>
                                                </c:if>
                                            </select>
                                            <script>
                                                document.getElementById("fleetCenterId").value = '<c:out value="${fleetCenterId}"/>'
                                            </script>
                                        </td>-->
                                        <td><font color="red"></font>Profit/Non-Profit</td>
                                        <td height="30">
                                            <select name="profitType" id="profitType" class="form-control">
                                                <option value="All">All</option>
                                                <option value="Profit">Profitable</option>
                                                <option value="nonProfit">Non-Profitable</option>
                                            </select>
                                            <script>
                                                document.getElementById("profitType").value = '<c:out value="${profitType}"/>'
                                            </script>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="right"><input type="button" class="button" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);">&nbsp;&nbsp;</td>
                                        <td colspan="3"><input type="button" class="button" name="Search"  id="Search"  value="Search" onclick="submitPage(this.name);"></td>
                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>


            <br>
            <br>
            <br>
            <c:if test="${vehicleDetailsList == null && vehicleWiseProfitListSize == null}">
                <center>
                    <font color="blue">Please Wait Your Request is Processing</font>
                    <br>
                    <div class="container">
                        <div class="content">
                            <div class="circle"></div>
                            <div class="circle1"></div>
                        </div>
                    </div>
                </center>
            </c:if>
            
            <c:if test="${vehicleDetailsList != null && vehicleWiseProfitList != null}">
                <table border="1"  align="center" width="100%" cellpadding="0" cellspacing="1" >
                    <thead>
                        <tr>
                            <td rowspan="2" class="contenthead">S.No</td>
                            <td rowspan="2" class="contenthead">Trailer No</td>
                            <td rowspan="2" class="contenthead">Trailer Model</td>
<!--                            <td rowspan="2" class="contenthead">MFR Name</td>-->
                            <td rowspan="2" class="contenthead">Trips</td>
                            <td rowspan="2" class="contenthead">Earnings</td>
                            <td colspan="5" class="contenthead" style="text-align:center">Fixed Expenses </td>
                            <td colspan="5" class="contenthead" style="text-align:center">Operation Expenses </td>
                            <td rowspan="2" class="contenthead" style="text-align:center">Per Day<br> Fixed Expenses </td>
                            <td rowspan="2" class="contenthead" style="text-align:center">Fixed Expenses <br>for the Report Period</td>
                            <td rowspan="2" class="contenthead" style="text-align:center">Operation Expenses</td>
                            <td rowspan="2" class="contenthead">Maint <br>Expenses</td>
                            <td rowspan="2" class="contenthead">Nett <br>Expenses</td>
                            <td rowspan="2" class="contenthead">Profit</td>
                            <td rowspan="2" class="contenthead">Profit %</td>
                        </tr> 
                        <tr>
                            <td class="contenthead">Insurance</td>
                            <td class="contenthead">Road Tax </td>
                            <td class="contenthead">FC Amount </td>
                            <td class="contenthead">Permit </td>
                            <td class="contenthead">EMI </td>
                            <td class="contenthead">Trailer Expense</td>
                            <td class="contenthead">Trailer Revenue</td>
                            <td class="contenthead">Other Exp Amount </td>
                            <td class="contenthead">Driver / Cleaner Salary </td>
                            <td class="contenthead">Driver(Incentive,<br>Bata,Misc)</td>
                        </tr>
                    </thead>
                    <% int index = 0,sno = 1;%>
                    <c:set var="totalTrip" value="${0}"/>    
                    <c:set var="totalIncome" value="${0}"/>    
                    <c:set var="totalFixedExpense" value="${0}"/>    
                    <c:set var="totalOperationExpense" value="${0}"/>    
                    <c:set var="totalNetExpense" value="${0}"/>    
                    <c:set var="totalProfitGained" value="${0}"/>    
                    <c:forEach items="${vehicleDetailsList}" var="veh">
                        <c:if test="${vehicleWiseProfitList != null}">
                            <c:forEach items="${vehicleWiseProfitList}" var="profitList">
                                <c:if test="${veh.trailerId == profitList.trailerId}">
                                    
                                    <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                                    %>
                                    <tbody>
                                        <c:set var="profit" value="${0}"/>    
                                        <c:set var="profitPercent" value="${0}"/>
                                        <c:set var="countPercent" value="${0}"/>
                                        <c:if test="${profitType == 'All'}">
                                            <tr>
                                            <td class="<%=classText%>" rowspan="2" align="center"><%=sno++%></td>
                                            <td class="<%=classText%>" rowspan="2" ><a href="" onclick="viewVehicleDetails('<c:out value="${veh.trailerId}"/>')"><c:out value="${veh.trailerNo}"/></a></td>
                                            <td class="<%=classText%>"  rowspan="2" ><c:out value="${veh.modelName}"/></td>
<!--                                            <td class="<%=classText%>" rowspan="2" ><c:out value="${veh.mfrName}"/></td>-->
                                            <c:if test="${profitList.tripNos > 0}">
                                            <td class="<%=classText%>" rowspan="2" align="center"><a href="" onclick="viewCustomerProfitDetails('<c:out value="${profitList.tripId}"/>');"><c:out value="${profitList.tripNos}"/></a></td>
                                            </c:if>
                                            <c:if test="${profitList.tripNos == 0}">
                                            <td class="<%=classText%>" rowspan="2" align="center"><c:out value="${profitList.tripNos}"/></td>
                                            </c:if>
                                            <td class="<%=classText%>" rowspan="2" align="right"><c:out value="${profitList.freightAmount}"/></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.insuranceAmount}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.roadTaxAmount}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fcAmount}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.permitAmount}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.emiAmount}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.trailerExpence}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.trailerRevenue}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.tripOtherExpense}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.vehicleDriverSalary}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.driverExpense}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fixedExpensePerDay}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalFixedExpense}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalOperationExpense}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.maintainExpense}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netExpense}" /></td>
                                            <c:if test="${profitList.netProfit lt 0 || profitList.netProfit eq 0}">
                                                <td class="<%=classText%>" rowspan="2" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                                    </c:if>
                                                    <c:if test="${profitList.netProfit gt 0}">
                                                <td class="<%=classText%>" rowspan="2" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                                    </c:if>
                                            <c:if test="${profitList.freightAmount != '0.00'}">
                                            <c:set var="profit" value="${profitList.netProfit/profitList.freightAmount}"/>    
                                            <c:set var="profitPercent" value="${profit*100}"/>
                                            </c:if>    
                                            <c:if test="${profitPercent <= 0}">
                                            <td class="<%=classText%>" rowspan="2" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                            </c:if>  
                                            <c:if test="${profitPercent > 0}">
                                            <td class="<%=classText%>" rowspan="2" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                            </c:if>  
                                            <c:set var="totalTrip" value="${profitList.tripNos + totalTrip}"/>    
                                            <c:set var="totalIncome" value="${profitList.freightAmount + totalIncome}"/>    
                                            <c:set var="totalInsurance" value="${profitList.insuranceAmount + totalInsurance}"/>    
                                            <c:set var="totalRoadTax" value="${profitList.roadTaxAmount + totalRoadTax}"/>    
                                            <c:set var="totalFcAmount" value="${profitList.fcAmount + totalFcAmount}"/>    
                                            <c:set var="totalPermitAmount" value="${profitList.permitAmount + totalPermitAmount}"/>    
                                            <c:set var="totalEmiAmount" value="${profitList.emiAmount + totalEmiAmount}"/>    
                                            <c:set var="trailerExpence" value="${profitList.trailerExpence}"/>    
                                            <c:set var="trailerRevenue" value="${profitList.trailerRevenue}"/>    
                                            <c:set var="totalOtherExpenseAmount" value="${profitList.tripOtherExpense + totalOtherExpenseAmount}"/>    
                                            <c:set var="totalDriverSalaryAmount" value="${profitList.vehicleDriverSalary + totalDriverSalaryAmount}"/>    
                                            <c:set var="totalDriverExpense" value="${profitList.driverExpense + totalDriverExpense}"/>    
                                            <c:set var="totalFixedExpensePerDay" value="${profitList.fixedExpensePerDay + totalFixedExpensePerDay}"/>    
                                            <c:set var="totalFixedExpense" value="${profitList.totlalFixedExpense + totalFixedExpense}"/>    
                                            <c:set var="totalOperationExpense" value="${profitList.totlalOperationExpense + totalOperationExpense}"/>    
                                            <c:set var="totalMaintainExpense" value="${profitList.maintainExpense + totalMaintainExpense}"/>    
                                            <c:set var="totalNetExpense" value="${profitList.netExpense + totalNetExpense}"/>    
                                            <c:set var="totalProfitGained" value="${profitList.netProfit + totalProfitGained}"/>  
                                            <c:set var="totalProfitPercentValue" value="${profitPercent + totalProfitPercentValue}"/>  
                                        </tr>
                                        </c:if>    
                                        <c:if test="${profitType == 'Profit' && profitList.netProfit > 0}">
                                            <tr>
                                            <td class="<%=classText%>" rowspan="2" align="center"><%=sno++%></td>
                                            <td class="<%=classText%>" rowspan="2" ><a href="" onclick="viewVehicleDetails('<c:out value="${veh.trailerId}"/>')"><c:out value="${veh.trailerNo}"/></a></td>
                                            <td class="<%=classText%>"  rowspan="2" align="center"><c:out value="${veh.modelName}"/></td>
<!--                                            <td class="<%=classText%>" rowspan="2" align="center"><c:out value="${veh.mfrName}"/></td>-->
                                            <c:if test="${profitList.tripNos > 0}">
                                            <td class="<%=classText%>" rowspan="2" align="center"><a href="" onclick="viewCustomerProfitDetails('<c:out value="${profitList.tripId}"/>');"><c:out value="${profitList.tripNos}"/></a></td>
                                            </c:if>
                                            <c:if test="${profitList.tripNos == 0}">
                                            <td class="<%=classText%>" rowspan="2" align="center"><c:out value="${profitList.tripNos}"/></td>
                                            </c:if>
                                            <td class="<%=classText%>" rowspan="2" align="right"><c:out value="${profitList.freightAmount}"/></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.insuranceAmount}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.roadTaxAmount}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fcAmount}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.permitAmount}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.emiAmount}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.trailerExpence}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.trailerRevenue}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.tripOtherExpense}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.vehicleDriverSalary}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.driverExpense}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fixedExpensePerDay}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalFixedExpense}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalOperationExpense}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.maintainExpense}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netExpense}" /></td>
                                            <c:if test="${profitList.netProfit lt 0 || profitList.netProfit eq 0}">
                                                <td class="<%=classText%>" rowspan="2" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                                    </c:if>
                                                    <c:if test="${profitList.netProfit gt 0}">
                                                <td class="<%=classText%>" rowspan="2" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                                    </c:if>
                                             <c:if test="${profitList.freightAmount != '0.00'}">
                                            <c:set var="profit" value="${profitList.netProfit/profitList.freightAmount}"/>    
                                            <c:set var="profitPercent" value="${profit*100}"/>
                                            </c:if>
                                            <c:if test="${profitPercent <= 0}">
                                            <td class="<%=classText%>" rowspan="2" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                            </c:if>       
                                            <c:if test="${profitPercent > 0}">
                                            <td class="<%=classText%>" rowspan="2" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                            </c:if>  
                                            <c:set var="totalTrip" value="${profitList.tripNos + totalTrip}"/>    
                                            <c:set var="totalIncome" value="${profitList.freightAmount + totalIncome}"/>    
                                            <c:set var="totalInsurance" value="${profitList.insuranceAmount + totalInsurance}"/>    
                                            <c:set var="totalRoadTax" value="${profitList.roadTaxAmount + totalRoadTax}"/>    
                                            <c:set var="totalFcAmount" value="${profitList.fcAmount + totalFcAmount}"/>    
                                            <c:set var="totalPermitAmount" value="${profitList.permitAmount + totalPermitAmount}"/>    
                                            <c:set var="totalEmiAmount" value="${profitList.emiAmount + totalEmiAmount}"/>    
                                            <c:set var="trailerExpence" value="${profitList.trilerExpence}"/>    
                                            <c:set var="trailerRevenue" value="${profitList.trilerRevenue}"/>    
                                            <c:set var="totalOtherExpenseAmount" value="${profitList.tripOtherExpense + totalOtherExpenseAmount}"/>    
                                            <c:set var="totalDriverSalaryAmount" value="${profitList.vehicleDriverSalary + totalDriverSalaryAmount}"/>    
                                            <c:set var="totalDriverExpense" value="${profitList.driverExpense + totalDriverExpense}"/>    
                                            <c:set var="totalFixedExpensePerDay" value="${profitList.fixedExpensePerDay + totalFixedExpensePerDay}"/>    
                                            <c:set var="totalFixedExpense" value="${profitList.totlalFixedExpense + totalFixedExpense}"/>    
                                            <c:set var="totalOperationExpense" value="${profitList.totlalOperationExpense + totalOperationExpense}"/>    
                                            <c:set var="totalMaintainExpense" value="${profitList.maintainExpense + totalMaintainExpense}"/>  
                                            <c:set var="totalNetExpense" value="${profitList.netExpense + totalNetExpense}"/>    
                                            <c:set var="totalProfitGained" value="${profitList.netProfit + totalProfitGained}"/> 
                                            <c:set var="totalProfitPercentValue" value="${profitPercent + totalProfitPercentValue}"/>
                                        </tr>
                                        </c:if>
                                        <c:if test="${profitType == 'nonProfit' && profitList.netProfit <= 0}">
                                            <tr>
                                            <td class="<%=classText%>" rowspan="2" align="center"><%=sno++%></td>
                                            <td class="<%=classText%>" rowspan="2" ><a href="" onclick="viewVehicleDetails('<c:out value="${veh.trailerId}"/>')"><c:out value="${veh.trailerNo}"/></a></td>
                                            <td class="<%=classText%>"  rowspan="2" align="center"><c:out value="${veh.modelName}"/></td>
<!--                                            <td class="<%=classText%>" rowspan="2" align="center"><c:out value="${veh.mfrName}"/></td>-->
                                            <c:if test="${profitList.tripNos > 0}">
                                            <td class="<%=classText%>" rowspan="2" align="center"><a href="" onclick="viewCustomerProfitDetails('<c:out value="${profitList.tripId}"/>');"><c:out value="${profitList.tripNos}"/></a></td>
                                            </c:if>
                                            <c:if test="${profitList.tripNos == 0}">
                                            <td class="<%=classText%>" rowspan="2" align="center"><c:out value="${profitList.tripNos}"/></td>
                                            </c:if>
                                            <td class="<%=classText%>" rowspan="2" align="right"><c:out value="${profitList.freightAmount}"/></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.insuranceAmount}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.roadTaxAmount}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fcAmount}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.permitAmount}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.emiAmount}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.trailerExpence}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.treilerRevenue}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.tripOtherExpense}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.vehicleDriverSalary}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.driverExpense}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fixedExpensePerDay}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalFixedExpense}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalOperationExpense}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.maintainExpense}" /></td>
                                            <td class="<%=classText%>" rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netExpense}" /></td>
                                            <c:if test="${profitList.netProfit lt 0 || profitList.netProfit eq 0}">
                                                <td class="<%=classText%>" rowspan="2" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                                    </c:if>
                                                    <c:if test="${profitList.netProfit gt 0}">
                                                <td class="<%=classText%>" rowspan="2" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                                    </c:if>
                                             <c:if test="${profitList.freightAmount != '0.00'}">
                                            <c:set var="profit" value="${profitList.netProfit/profitList.freightAmount}"/>    
                                            <c:set var="profitPercent" value="${profit*100}"/>
                                            </c:if>
                                            <c:if test="${profitPercent <= 0}">
                                            <td class="<%=classText%>" rowspan="2" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                            </c:if>       
                                            <c:if test="${profitPercent > 0}">
                                            <td class="<%=classText%>" rowspan="2" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                            </c:if> 
                                            <c:set var="totalTrip" value="${profitList.tripNos + totalTrip}"/>    
                                            <c:set var="totalIncome" value="${profitList.freightAmount + totalIncome}"/>    
                                            <c:set var="totalInsurance" value="${profitList.insuranceAmount + totalInsurance}"/>    
                                            <c:set var="totalRoadTax" value="${profitList.roadTaxAmount + totalRoadTax}"/>    
                                            <c:set var="totalFcAmount" value="${profitList.fcAmount + totalFcAmount}"/>    
                                            <c:set var="totalPermitAmount" value="${profitList.permitAmount + totalPermitAmount}"/>    
                                            <c:set var="totalEmiAmount" value="${profitList.emiAmount + totalEmiAmount}"/>    
                                            <c:set var="trailerExpence" value="${profitList.trailerExpence}"/>    
                                            <c:set var="trailerRevenue" value="${profitList.trilerRevenue}"/>    
                                            <c:set var="totalOtherExpenseAmount" value="${profitList.tripOtherExpense + totalOtherExpenseAmount}"/>    
                                            <c:set var="totalDriverSalaryAmount" value="${profitList.vehicleDriverSalary + totalDriverSalaryAmount}"/>    
                                            <c:set var="totalDriverExpense" value="${profitList.driverExpense + totalDriverExpense}"/>    
                                            <c:set var="totalFixedExpensePerDay" value="${profitList.fixedExpensePerDay + totalFixedExpensePerDay}"/>    
                                            <c:set var="totalFixedExpense" value="${profitList.totlalFixedExpense + totalFixedExpense}"/>    
                                            <c:set var="totalOperationExpense" value="${profitList.totlalOperationExpense + totalOperationExpense}"/> 
                                            <c:set var="totalMaintainExpense" value="${profitList.maintainExpense + totalMaintainExpense}"/>  
                                            <c:set var="totalNetExpense" value="${profitList.netExpense + totalNetExpense}"/>    
                                            <c:set var="totalProfitGained" value="${profitList.netProfit + totalProfitGained}"/>  
                                        </tr>
                                        </c:if>
                                    </tbody>
                                </c:if>

                            </c:forEach>
                        </c:if>
                        <%
                   index++;
                        %>
                    </c:forEach>
                                    <tr>
                                        <td colspan="3" class="contenthead" style="text-align:center">Total</td>
                                        <td class="contenthead" style="text-align:center"><c:out value="${totalTrip}"/></td>
                                        <td class="contenthead" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalIncome}" /></td>
                                        <td class="contenthead" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalInsurance}" /></td>
                                        <td class="contenthead" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalRoadTax}" /></td>
                                        <td class="contenthead" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalFcAmount}" /></td>
                                        <td class="contenthead" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalPermitAmount}" /></td>
                                        <td class="contenthead" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalEmiAmount}" /></td>
                                        <td class="contenthead" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${trailerExpence}" /> </td>
                                        <td class="contenthead" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${trailerRevenue}" /> </td>
                                        <td class="contenthead" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalOtherExpenseAmount}" /> </td>
                                        <td class="contenthead" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalDriverSalaryAmount}" /> </td>
                                        <td class="contenthead" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalDriverExpense}" /> </td>
                                        <td class="contenthead" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalFixedExpensePerDay}" /> </td>
                                        <td class="contenthead" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalFixedExpense}" /> </td>
                                        <td class="contenthead" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalOperationExpense}" /> </td>
                                        <td class="contenthead" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalMaintainExpense}" /> </td>
                                        <td class="contenthead" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalNetExpense}" /> </td>
                                        <td class="contenthead" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProfitGained}" /> </td>
                                        <c:set var="totalProfitPercentValue1" value="${0}"/>
                                        <c:set var="totalProfitPercentValue" value="${0}"/>
                                        <c:if test="${totalIncome != 0}">
                                        <c:set var="totalProfitPercentValue1" value="${totalProfitGained / totalIncome}"/>
                                        <c:set var="totalProfitPercentValue" value="${totalProfitPercentValue1 * 100}"/>
                                        </c:if>
                                        <td class="contenthead" style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProfitPercentValue}" /> %</td>
                                    </tr>                   
                </table>
            </c:if>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <c:if test="${vehicleDetailsList != null && vehicleWiseProfitList != null}">
                
                <c:set var="totalNetExpense" value="${profitList.netExpense + totalNetExpense}"/>    
                <c:set var="totalProfitGained" value="${profitList.netProfit + totalProfitGained}"/> 
                
                <table border="2" style="border: 1px solid #666666;"  align="center"  cellpadding="0" cellspacing="1" >
                    <tr height="25">
                        <td style="background-color: #6374AB; color: #ffffff">Total Trips Carried Out</td>
                        <td width="150" align="right"><c:out value="${totalTrip}"/></td>
                    </tr>
                    <tr height="25">
                        <td style="background-color: #6374AB; color: #ffffff">Total Income</td>
                        <td width="150" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalIncome}" /></td>
                    </tr>
                    <tr height="25">
                        <td style="background-color: #6374AB; color: #ffffff">Total Fixed Expenses</td>
                        <td width="150" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalFixedExpense}" /> </td>
                    </tr>
                    <tr height="25">
                        <td style="background-color: #6374AB; color: #ffffff">Total Operation Expenses</td>
                        <td width="150" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${trailerExpence}" /> </td>
                    </tr>
                    <tr height="25">
                        <td style="background-color: #6374AB; color: #ffffff">Total Nett Expenses</td>
                        <td width="150" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalNetExpense}" /> </td>
                    </tr>
                    <tr height="25">
                        <td style="background-color: #6374AB; color: #ffffff">Total Profit gained</td>
                        <c:if test="${totalProfitGained lt 0 || totalProfitGained eq 0}">
                            <td width="150" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProfitGained}" /> </font></td>
                                </c:if>
                                <c:if test="${totalProfitGained gt 0 }">
                            <td width="150" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProfitGained}" /> </font></td>
                                </c:if>
                    </tr>
                    <tr height="25">
                        <c:set var="totalProfit" value="${0}"/>    
                        <c:set var="totalProfitPercent" value="${0}"/>
                        <c:if test="${totalIncome > 0}">
                        <c:set var="totalProfit" value="${totalProfitGained/totalIncome}"/>    
                        <c:set var="totalProfitPercent" value="${totalProfit*100}"/>
                        </c:if>
                        <td style="background-color: #6374AB; color: #ffffff">Total Profit gained &nbsp;%</td>
                        <c:if test="${totalProfitPercent lt 0 || totalProfitPercent eq 0}">
                            <td width="150" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProfitPercent}" /> &nbsp;%</font></td>
                                </c:if>
                                <c:if test="${totalProfitPercent gt 0 }">
                            <td width="150" align="right"><font color="green" ><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProfitPercent}" /> &nbsp;%</font></td>
                                </c:if>
                    </tr>
                </table>
            </c:if>
            
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>    
</html>