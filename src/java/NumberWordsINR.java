/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Arul
 */
import java.util.*;

public class NumberWordsINR {
    private String numberInWords = "";  
    private String roundedValue = "";
    String string;
    String st1[] = {"", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN",
        "EIGHT", "NINE",};
    String st2[] = {"HUNDRED", "THOUSAND", "LAKH", "CRORE"};
    String st3[] = {"TEN", "ELEVEN", "TWELVE", "THIRTEEN", "FOURTEEN",
        "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN", "NINTEEN",};
    String st4[] = {"TWENTY", "THIRTY", "FOURTY", "FIFTY", "SIXTY", "SEVENTY",
        "EIGHTY", "NINTY"};

    public String convert(int number) {
        int n = 1;
        int word;
        string = "";
        while (number != 0) {
            switch (n) {
                case 1:
                    word = number % 100;
                    pass(word);
                    if (number > 100 && number % 100 != 0) {
                        show("and ");
                    }
                    number /= 100;
                    break;

                case 2:
                    word = number % 10;
                    if (word != 0) {
                        show(" ");
                        show(st2[0]);
                        show(" ");
                        pass(word);
                    }
                    number /= 10;
                    break;

                case 3:
                    word = number % 100;
                    if (word != 0) {
                        show(" ");
                        show(st2[1]);
                        show(" ");
                        pass(word);
                    }
                    number /= 100;
                    break;

                case 4:
                    word = number % 100;
                    if (word != 0) {
                        show(" ");
                        show(st2[2]);
                        show(" ");
                        pass(word);
                    }
                    number /= 100;
                    break;

                case 5:
                    word = number % 100;
                    if (word != 0) {
                        show(" ");
                        show(st2[3]);
                        show(" ");
                        pass(word);
                    }
                    number /= 100;
                    break;

            }
            n++;
        }
        return string;
    }

    public void pass(int number) {
        int word, q;
        if (number < 10) {
            show(st1[number]);
        }
        if (number > 9 && number < 20) {
            show(st3[number - 10]);
        }
        if (number > 19) {
            word = number % 10;
            if (word == 0) {
                q = number / 10;
                show(st4[q - 2]);
            } else {
                q = number / 10;
                show(st1[word]);
                show(" ");
                show(st4[q - 2]);
            }
        }
    }

    public void show(String s) {
        String st;
        st = string;
        string = s;
        string += st;
    }

    public String getRoundedValue() {
        return roundedValue;
    }

    public void setRoundedValue(String roundedValue) {
        //////System.out.println("roundedValue first"+roundedValue);

        roundedValue = roundedValue.replace(".", "-");
        String fin = "";

        String[] test1 = roundedValue.split("-");

        if (test1.length > 1) {
            fin = test1[0] + "." + test1[1].substring(0, 1) + "0";

        } else {
            fin = test1[0] + ".00";
        }
        this.roundedValue = fin;
    }
    
    public String getNumberInWords() {
        return numberInWords;
    }

    public void setNumberInWords(String numberInWords) {
        String[] temp = new String[2];
        numberInWords = numberInWords.replace(".", "-");
        
        temp = numberInWords.split("-");        
        this.numberInWords = convert((int) Long.parseLong(temp[0]));
        if(Integer.parseInt(temp[1])!=0 ){
        this.numberInWords = this.numberInWords + " and "+ convert((int) Long.parseLong(temp[1]))+ " Paise";
        }
        
    }

    public static void main(String[] args) {
        NumberWordsINR w = new NumberWordsINR();
        Scanner input = new Scanner(System.in);
        System.out.print("Enter Number: ");
        int num = input.nextInt();
        String inwords = w.convert(num);
        System.out.println(inwords + " rupees");
    }
}
