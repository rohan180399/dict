
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
     <%@page language="java" contentType="text/html; charset=UTF-8"%>
 <%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import="ets.domain.company.business.CompanyTO" %>
<%@ page import="ets.domain.mrs.business.MrsTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
        <!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>-->
        <script type="text/javascript" src="/throttle/js/jquery-1.10.2.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>


        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            /*            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";*/
        </style>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

    
    <script>
    function submitpage()
    {
        if (textValidation(document.dept.typeName, "Vehicle Type Name")) {
            return;
        }
        if (textValidation(document.dept.description, "description")) {
            return;
        }
        document.dept.action = "/throttle/addVehicleType.do";
        document.dept.submit();
    }


    </script>

    <script>

        var axletypeId = $("#axletypeId:selected").text();
//        alert(axletypeId);
        document.getElementById("axletypeNmae").value = axletypeId;

    </script>
<div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="trucks.label.VehicleType"  text="VehicleType"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="stores.label.Vehicle"  text="Vehicle"/></a></li>
          <li class="active"><spring:message code="trucks.label.VehicleType"  text="VehicleType"/></li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">
    <body>
        <form name="dept"  method="post" >
            

            <%@ include file="/content/common/message.jsp" %>
            
<td colspan="4"  style="background-color:#5BC0DE;">
            <table class="table table-info mb30 table-hover" >

                <tr>
                    <td height="20"><font color=red>*</font><spring:message code="trucks.label.VehicleTypeName"  text="default text"/></td>
                    <td height="20"><input name="typeName" type="text" class="form-control" style="width:260px;height:40px;" value=""></td>
<!--                </tr>
                <tr>-->
                    <td height="20"><font color=red>*</font><spring:message code="trucks.label.AxleType"  text="default text"/></td>
                    <td  width="20%"><input type="hidden" name="axleTypeName" id="axleTypeName">
                        <select class="form-control" style="width:260px;height:40px;" name="axleTypeId" id="axleTypeId"   >

                            <option value="0">---<spring:message code="trucks.label.Select"  text="default text"/>---</option>
                            <c:if test = "${AxleList != null}" >
                                <c:forEach items="${AxleList}" var="Type">
                                    <option value='<c:out value="${Type.vehicleAxleId}" />'><c:out value="${Type.axleTypeName}" /></option>
                                </c:forEach>
                            </c:if>
                        </select></td>
                </tr>
                <tr>
                    <td height="20"><font color=red>*</font><spring:message code="trucks.label.VehicleTonnage"  text="default text"/></td>
                    <td height="20"><input name="tonnage" type="text" class="form-control" style="width:260px;height:40px;" value=""></td>
<!--                </tr>
                <tr>-->
                    <td height="20"><font color=red>*</font><spring:message code="trucks.label.VehicleCapacity(CBM)"  text="default text"/></td>
                    <td height="20"><input name="capacity" type="text" class="form-control" style="width:260px;height:40px;" value=""></td>
                </tr>
                <tr>
                    <td height="20"><font color=red>*</font> <spring:message code="trucks.label.Description"  text="default text"/></td>
                    <td height="20"><textarea class="form-control" style="width:260px;height:40px;" name="description"></textarea></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
</td>
            <br>
            <center>
                <input type="button" value="<spring:message code="trucks.label.Add"  text="default text"/>" class="btn btn-success" onclick="submitpage();">
                &emsp;<input type="reset" class="btn btn-success" value="<spring:message code="trucks.label.Clear"  text="default text"/>">
            </center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>


    </div>
    </div>





<%@ include file="/content/common/NewDesign/settings.jsp" %>