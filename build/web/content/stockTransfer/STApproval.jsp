<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        
        <%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>    
        <%@ page import="ets.domain.stockTransfer.business.StockTransferTO" %>  
    </head>
    <script language="javascript">
        function submitPage(value){          
            document.stockApproval.approvalStatus.value=value;
            selectedItemValidation();            
        }
        function selectedItemValidation(){
            var index = document.getElementsByName("selectedIndex");
            var approvedQty = document.getElementsByName("approvedQtys");
            var requestedQtys = document.getElementsByName("requestedQtys");
             var rcItem = document.getElementsByName("rcItem");
              var newItem = document.getElementsByName("newItem");
            var remarks = document.getElementsByName("remarks");  
            for(var i=0;(i<index.length && index.length!=0);i++){                       
                if(floatValidation(approvedQty[i],'Issue Quantity')){   
                    return;
                }   
                alert;
                 if(parseFloat(approvedQty[i].value) > parseFloat(requestedQtys[i].value)){
                alert("Approved  Quantity Should not be Greater Than Requested Quantity");
                return false;
                }
                 if(parseFloat(approvedQty[i].value) > (parseFloat(rcItem[i].value) + parseFloat(newItem[i].value))){
                alert("Approved  Quantity Should not be Greater Than Stock Quantity");
                return;
                }
            }
            if(textValidation(document.stockApproval.remarks,'Remarks')){       
               return;
           }  
          document.stockApproval.requestPtId.value='<%=request.getAttribute("requestPtId")%>';
            document.stockApproval.action ="/throttle/STApproval.do"
            document.stockApproval.submit(); 
        }
        function setValues(){                 
            document.stockApproval.requestId.value='<%=request.getAttribute("RequestId")%>';            
            document.stockApproval.approvedQtys0.focus();
            }
       
        function setSelectbox(i)
        {               
            var selected=document.getElementsByName("selectedIndex");
            selected[i];
        } 
    </script>
            
            
            <script>
   function changePageLanguage(langSelection){
   if(langSelection== 'ar'){
   document.getElementById("pAlign").style.direction="rtl";
   }else if(langSelection== 'en'){
   document.getElementById("pAlign").style.direction="ltr";
   }
   }
 </script>

  <c:if test="${jcList != null}">
  <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
  </c:if>
      
  <span style="float: right">
	<a href="?paramName=en">English</a>
	|
	<a href="?paramName=ar">Arabic</a>
  </span>
            
            
            
    <body onload="setValues();">
        <form name="stockApproval"  method="post" >
<%@ include file="/content/common/path.jsp" %>


<%@ include file="/content/common/message.jsp" %>
      

<% int index = 0;%> 
 <c:if test = "${itemList != null}" >
                <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
                    <tr>
                        <td class="contenthead" height="30" colspan="6"><div class="contenthead">Approve/Reject Request</div></td>
                    </tr>
                    <c:forEach items="${itemList}" var="company"> 
                        <% if (index == 0) {%>  
                        <tr>
                            <td height="30" class="text1"><spring:message code="stores.label.ServicePoint"  text="default text"/>
</td>
                            <td height="30" class="text1"><c:out value="${company.companyName}"/></td>                                        
                            <% index++;
            }%>                     
                  </c:forEach>
                            <td height="30" class="text1"><spring:message code="stores.label.RequestNo"  text="default text"/></td>
                            <td height="30" class="text1"><c:out value="${RequestId}"/></td>

                    </tr>
                </table>
                <br><br>
    <%  index = 0;%>             
                <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
                   
                    <tr>
                        <td width="29" rowspan="2" class="contentsub"><spring:message code="stores.label.Sno"  text="default text"/></td>
                        <td width="115" rowspan="2" class="contentsub">MFR Item Code</td>
                        <td width="91" rowspan="2" class="contentsub">PAPL Item Code</td>
                        <td width="64" rowspan="2" class="contentsub"><spring:message code="stores.label.ItemName"  text="default text"/></td>
                        <td width="77" rowspan="2" class="contentsub"><spring:message code="stores.label.RequestQuantity"  text="default text"/></td>
                        <td height="87" colspan="2" class="contentsub"><spring:message code="stores.label.RequestorStockAvailability"  text="default text"/> </td>
                        <td height="87" colspan="2" class="contentsub"><spring:message code="stores.label.ApproverStockAvailability"  text="default text"/> </td>
                        <td width="106"  class="contentsub"><span class="contentsub"><spring:message code="stores.label.OtherRequestQty"  text="default text"/></span> </td>
                        <td width="113"  colspan="2" rowspan="2" class="contentsub"><spring:message code="stores.label.IssueQuantity"  text="default text"/></td>
                    </tr>
                    <tr>
                        <td height="5" class="text2"><spring:message code="stores.label.New"  text="default text"/></td>
                        <td height="5" class="text2">RC</td>
                        <td height="5" class="text2"><spring:message code="stores.label.New"  text="default text"/></td>
                        <td height="5" class="text2">RC</td>
                        <td height="5" class="text2">&nbsp;</td>
                    </tr>
        <c:forEach items="${itemList}" var="item">               
                    
                    <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                    %>   
                  
                        <tr>
                            <td class="<%=classText %>" height="30"><%=index + 1%></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.mfrCode}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.paplCode}"/></td>
                            <td class="<%=classText %>" height="30"><input type="hidden" name="itemIds" value="<c:out value="${item.itemId}"/>"><c:out value="${item.itemName}"/></td>
                            <td class="<%=classText %>" height="30"><input type="hidden" name="requestedQtys" value="<c:out value="${item.requestedQty}"/>"><c:out value="${item.requestedQty}"/></td>
                            <td width="69" height="30" class="<%=classText %>"><div align="center"><c:out value="${item.requestPtNewItem}"/></div></td>
                            <td width="86" height="30" class="<%=classText %>"><div align="center"><c:out value="${item.requestPtRcItem}"/></div></td>
                            <td width="76" height="30" class="<%=classText %>"><div align="center"><input type="hidden" name="rcItem" value="<c:out value="${item.approvePtNewItem}"/>"><c:out value="${item.approvePtNewItem}"/></div></td>
                            <td width="76" height="30" class="<%=classText %>"><div align="center"><input type="hidden" name="newItem" value="<c:out value="${item.approvePtRcItem}"/>"><c:out value="${item.approvePtRcItem}"/></div></td>
                            <td class="<%=classText %>"><c:out value="${item.otherStockReq}"/></td>
							
                            <td class="<%=classText %>" height="30"><input type="text" maxlength="10" id="approvedQtys<%= index %>" name="approvedQtys" value="" size="5" class="form-control" onchange="setSelectbox('<%= index %>');" ></td>                              
                            <td width="77" height="30" class="<%=classText %>"><input type="hidden" name="selectedIndex" value='<%= index %>'></td>
                        </tr>
                        <%
            index++;
                        %>
                  </c:forEach>                   
                    <input type="hidden" name="requestId" value="">
                    <input type="hidden" name="requestPtId" value="">
  </table>
<%
ArrayList al = (ArrayList)request.getAttribute("itemList");
Iterator itr = al.iterator();
StockTransferTO listTO = null;
if (itr.hasNext()) {
    listTO = (StockTransferTO) itr.next();
    System.out.println(listTO.getCompanyName());
    System.out.println(listTO.getRequestedQty());
    System.out.println("remarks:"+listTO.getRemarks());
 }
%>

                <br>
                <div  align="center"> Request Remarks &nbsp;&nbsp;:&nbsp;&nbsp;<textarea class="form-control" rows="5" cols="30" readonly name="requestRemarks" ><%=listTO.getRemarks()%></textarea> </div>
                <br>
                <br>
                <div  align="center"> Approve Remarks &nbsp;&nbsp;:&nbsp;&nbsp;<textarea class="form-control" rows="5" cols="30" name="remarks" ></textarea> </div>
                <br>
                <center>
                    <input type="button" class="button"  value="Approve" name="APPROVED" onclick="submitPage(this.name);"> &nbsp;&nbsp;
                    <input type="button" class="button"  value="Reject" name="REJECTED" onclick="submitPage(this.name);">
                    <input type="hidden" value="" name="approvalStatus">
                </center>                
          </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    
    
</html>
