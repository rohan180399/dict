<%--
    Document   : invoiceReopen
    Created on : Mar 10, 2021, 4:20:13 PM
    Author     : hp
--%>




<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        $(".datepicker").datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true, changeYear: true
        });
    });
</script>


<script type="text/javascript">
    function submitPage(value) {

        if (document.getElementById('selectName').value == '') {
            alert("Please select Consignor/Consignee");
            document.getElementById('selectName').focus();
        }
        else if (document.getElementById('invoiceNo').value == '') {
            alert("Please select Consignor Name");
            document.getElementById('invoiceNo').focus();
        }

        else if (document.getElementById('consignmentOrder').value == '') {
            alert("Please select Consignment No");
            document.getElementById('consignmentOrder').focus();


        }
        else if (value == "Save") {
//            alert("huiii");
            document.BPCLTransaction.action = '/throttle/saveConsignorChanges.do';
            document.BPCLTransaction.submit();
        }
    }



    function submit2(value) {
        if (value == "creditUpdate") {
//             alert("hiiiiii");
            document.BPCLTransaction.action = '/throttle/searchBillingPartyChange.do?param=creditUpdate';
            document.BPCLTransaction.submit();
        }
    }
//    function submit1(value) {
//        alert(value);
//        if (document.getElementById('invoiceNo').value == '') {
//            alert("Please select Invoice No.");
//            document.getElementById('invoiceNo').focus();
//        }
//         else if (value == "search") {
//             alert("hiiiiii");
//            document.BPCLTransaction.action = '/throttle/handleUpdateInvoiceReopen.do?param=search';
//            document.BPCLTransaction.submit();
//        }
//
//
//    }
</script>

<script>
    $(document).ready(function() {
        $('#invoiceNo').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getConsignorChange.do",
                    dataType: "json",
                    data: {
                        invoiceNo: request.term, invoiceType: $("#invoiceType").val()
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            $('#invoiceNo').val('');
                            $('#customerId').val('');
                            $('#consignerMobile').val('');
                            $('#consignerAddress').val('');

                        }
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('~');
                $('#invoiceNo').val(tmp[0]);
                $('#customerId').val(tmp[1]);
                $('#consignerMobile').val(tmp[2]);
                $('#consignerAddress').val(tmp[3]);

                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('~');
            itemVal = '<font color="green">' + temp[0] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });


</script>

<script>
    $(document).ready(function() {
        $('#consignmentOrder').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getConsignmentOrderId.do",
                    dataType: "json",
                    data: {
                        consignmentOrder: request.term, invoiceType: $("#invoiceType").val()
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            $('#consignmentOrder').val('');
                            $('#consignmentId').val('');

                        }
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split(',');
                $('#consignmentOrder').val(tmp[0]);
                $('#consignmentId').val(tmp[1]);

                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split(',');
            itemVal = '<font color="green">' + temp[0] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });


</script>




<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Consignor/Consignee Change</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Billing</a></li>
            <li class="active">Consignor Changes</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body >
                <form name="BPCLTransaction" method="post">
                    <%@ include file="/content/common/message.jsp"%>

                    <table  class="table table-info mb30 table-hover" style="width: 100%">
                        <tr>
                            <td><font color="red">*</font>Consignor/Consignee</td>
                            <td >
                                <select class="form-control" style="width:180px;height:40px;" id="selectName" name="selectName" >
                                    <option value="1">Consignor</option>
                                    <option value="2">Consignee</option>
                                </select>
                            </td>
                            <td><font color="red">*</font>Consignor Name/Consignee Name</td>
                            <td height="30">
                                <input name="invoiceNo" id="invoiceNo" type="text" class=" form-control" style="width:240px;height:40px;" value="<c:out value="${invoiceNo}" />">
                            </td>
                            <td><font color="red">*</font>Consignment No</td>
                            <td height="30">
                                <input name="consignmentOrder" id="consignmentOrder" type="text" class=" form-control" style="width:240px;height:40px;" value="<c:out value="${consignmentOrder}" />">
                                <input name="customerId" id="customerId" type="hidden" class=" form-control" style="width:240px;height:40px;" value="<c:out value="${customerId}" />">
                                <input name="consignmentId" id="consignmentId" type="hidden" class=" form-control" style="width:240px;height:40px;" value="<c:out value="${consignmentId}" />">
                                <input name="consignerMobile" id="consignerMobile" type="hidden" class=" form-control" style="width:240px;height:40px;" value="<c:out value="${consignerMobile}" />">
                                <input name="consignerAddress" id="consignerAddress" type="hidden" class=" form-control" style="width:240px;height:40px;" value="<c:out value="${consignerAddress}" />">
                            </td>

                            <td style="display:none"><font color="red">*</font>Change In Billing Party</td>
                            </select>
                            </td>

                        </tr>
                        <script>
                            document.getElementById("invoiceType").value = '<c:out value="${invoiceType}" />';
                            document.getElementById("billingParty").value = '<c:out value="${customerId}" />';
                            document.getElementById("billingParty").value = '<c:out value="${consignmentId}" />';

                        </script>
                        <br>
                        <!--                            <script>
                                                        function setCustName()
                                                        {
                                                            var custName
                                                        }
                                                    </script>-->
                        <br>

                    </table>

                    <center>
                        <input type="button" class="btn btn-info" name="Save" onclick="submitPage(this.value);" value="Save">
                    </center>
                    <br>


                    <%--<table class="table table-info mb30 table-hover" id="table" style="width:100%">
                        <thead >
                            <tr>
                                <th>S.No</th>
                                <th>Trip Code</th>
                                <th>Gr. No.</th>
                                <th>Gr. Date</th>
                                <th>Customer Name</th>

                                <th height="30" ><div ><spring:message code="settings.label.Details" text="Select"/> </div></th>
                        </tr>
                        </thead>
                        <script>
                            function selectAllMenus() {
                                var selectAll = document.getElementById("selectAll").checked;
                                var functionStatus = document.getElementsByName("selectedIndex");
                                if (selectAll == true) {
                                    for (var i = 0; i < functionStatus.length; i++) {
                                        document.getElementById("selectedIndex" + i).checked = true;
                                    }
                                } else {
                                    for (var i = 0; i < functionStatus.length; i++) {
                                        document.getElementById("selectedIndex" + i).checked = false;
                                    }
                                }
                            }
                            function selectedCredit(sno)
                            {

                                var selectedIndex = document.getElementById("selectedIndex" + sno).checked;
                                if (selectedIndex == true)
                                {
                                    document.getElementById("checkedIndexs" + sno).value = 1;
                                } else
                                {
                                    document.getElementById("checkedIndexs" + sno).value = 0;
                                }
                            }
                        </script>
                        <tbody>
                            <%int sno=1;
                            int index =0;
                            String classText="";
                            %>
                            <c:forEach items="${billPartyChangeDetails}" var="closedBillList">
                                <%int oddEven = index % 2;
                              if (oddEven > 0) {
                                         classText = "text2";
                                     } else {
                                         classText = "text1";
                                     }
                                %>
                                <tr>
                                    <td  height="30"><%=sno++%></td>

                                    <td  height="30"><c:out value="${closedBillList.tripCode}"/></td>
                                    <td  style="display:none" height="30"><c:out value="${closedBillList.tripId}"/></td>
                                    <td  height="30"><c:out value="${closedBillList.grNo}"/></td>
                                    <td  height="30"><c:out value="${closedBillList.grDate}"/></td>
                                    <!--<td  style="display:none" height="30"><c:out value="${closedBillList.customerId}"/></td>-->
                                    <td style="display:none"> <input name="oldCustId" id="oldCustId" type="text" class=" form-control" style="width:240px;height:40px;" value="<c:out value="${closedBillList.customerId}" />"> </td>
                                    <td style="display:none"><input name="estimateRevenue" id="estimateRevenue" type="text" class=" form-control" style="width:240px;height:40px;" value="<c:out value="${closedBillList.estimatedRevenue}" />"></td>
                                    <td  height="30"><c:out value="${closedBillList.customerName}"/></td>
                                    <td  height="30"><input type="checkbox" name="selectedIndex" id="selectedIndex<%=index%>" onclick="selectedCredit(<%=index%>)" value="" ></td>

                                    <td style="display:none"><input name="checkedIndexs" id="checkedIndexs<%=index%>" type="text" class=" form-control" style="width:240px;height:40px;" value="0"></td>
                                    <td style="display:none"><input name="tripIds" id="tripIds<%=index%>" type="text" class="form-control" style="width:240px;height:40px;" value="<c:out value="${closedBillList.tripId}"/>"/></td>






                                </tr>
                                <%index++;%>
                            </c:forEach>
                        </tbody>
                    </table>
                    <center>    <input type="button" class="btn btn-info" name="creditUpdate" onclick="submit2(this.value);" value="creditUpdate"></center>--%>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

