<%@page import="java.text.DecimalFormat"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

        <!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">-->
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <%@ include file="/content/common/NewDesign/header.jsp" %>
        <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
        <%@page language="java" contentType="text/html; charset=UTF-8"%>
        <%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
        <%@page import="java.util.Locale"%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <%@page import="java.text.SimpleDateFormat" %>
        <!DOCTYPE html>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <script language="javascript" src="/throttle/js/validate.js"></script>

    <link rel="stylesheet" href="/throttle/css/jquery-ui.css">
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
    <script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
    <script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
    <script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });



        });

        $(function() {
            //	alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });

        });


        function replaceSpecialCharacters()
        {
            var content = document.getElementById("requestremarks").value;

            //alert(content.replace(/[^a-zA-Z0-9]/g,'_'));
            // content=content.replace(/[^a-zA-Z0-9]/g,'');
            content = content.replace(/[@&\/\\#,+()$~%'":*?<>{}]/g, ' ');
            document.getElementById("requestremarks").value = content;
            //alert("Repalced");

        }
    </script>

    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> Report</h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
                <li><a href="general-forms.html">Report</a></li>
                <li class="active">Blocked GR Summary</li>
            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">

                <script type="text/javascript">
                    function submitPage(value) {
                        if (document.getElementById('fromDate').value == '') {
                            alert("Please select from Date");
                            document.getElementById('fromDate').focus();
                        } else if (document.getElementById('toDate').value == '') {
                            alert("Please select to Date");
                            document.getElementById('toDate').focus();
                        } else {
                            if (value == 'ExportExcel') {
                                document.summary.action = '/throttle/handleBlockedGrSummaryReport.do?param=ExportExcel';
                                document.summary.submit();
                            } else {
                                document.summary.action = '/throttle/handleBlockedGrSummaryReport.do?param=Search';
                                document.summary.submit();
                            }
                        }
                    }
                </script>


                <body>
                    <form name="summary"  method="post" >
                        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
                        <%@ include file="/content/common/message.jsp" %>
<table class="table table-info mb30 table-hover" style="width:100%">
                        <tr height="30"   ><td colSpan="4" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">Blocked GR Summary</td></tr>
                        <tr>
                            <td><font color="red">*</font>From Date</td>
                            <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker , form-control" style="width:240px;height:40px;"  value="<c:out value="${fromDate}"/>" ></td>
                            <td><font color="red">*</font>To Date</td>
                            <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker , form-control" style="width:240px;height:40px;" value="<c:out value="${toDate}"/>"></td>
                        </tr>
                        <tr>
                            <td>Customer</td>
                            <td height="30" >
                                <select  name="customerId" id="customerId"  class="form-control" style="width:240px;height:40px;">
                                    <option value="">---Select---</option>
                                    <c:forEach items="${customerList}" var="customerList">
                                        <option value='<c:out value="${customerList.custId}"/>'><c:out value="${customerList.custName}"/></option>
                                    </c:forEach>
                                </select>
                                <script>
                                                document.getElementById('customerId').value = <c:out value="${customerId}"/>;
                                </script>
                            </td>
                            <td colspan="2" ><input type="button" class="btn btn-info" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);">&nbsp;&nbsp;<input type="button" class="btn btn-info" name="Search"  id="Search"  value="Search" onclick="submitPage(this.name);"></td>
                        </tr>
                        </table>
                        <c:if test="${blockedGrList != null}">


                           <table class="table table-info mb30 table-hover" id="table">
                                <thead height="30">
                                    <tr id="tableDesingTH" height="30">
                                        <th>S.No</th>
                                        <th>CustomerName</th>
                                        <th>GrDate</th>
                                        <th>BlockedGR</th>
                                        <th>UsedGR</th>
                                        <th>NotUsedGR</th>
                                        <th>CancelledGR</th>
                                        <th>Tot.BlockedGR</th>
                                        <th>Tot.UsedGR</th>
                                        <th>Tot.NotUsedGR</th>
                                        <th>Tot.CancelledGR</th>
                                        <th>CancelledBy </th>
                                        <th>Remarks</th>
                                    </tr>
                                </thead>
                                <%int index1 = 1;%>
                                <c:set var="totalBlocked" value="0"/>
                                <c:set var="totalNotUsed" value="0"/>
                                <c:set var="totalUsed" value="0"/>
                                <c:set var="totalCancelled" value="0"/>
                                <c:forEach items="${blockedGrList}" var="blockGr">
                                    <tr>
                                        <c:set var="totalBlocked" value="${totalBlocked + blockGr.totBlockedGr}"/>
                                        <c:set var="totalNotUsed" value="${totalNotUsed + blockGr.totNotUsedGr}"/>
                                        <c:set var="totalUsed" value="${totalUsed + blockGr.totUsedGr}"/>
                                        <c:set var="totalCancelled" value="${totalCancelled + blockGr.totCancelledGr}"/>
                                        <td ><%=index1++%></td>
                                        <td ><c:out value="${blockGr.custName}"/></td>
                                        <td > <c:out value="${blockGr.grDate}"/> </td>
                                        <td > <c:out value="${blockGr.blockedGr}"/></td>
                                        <td > <c:out value="${blockGr.usedGr}"/> </td>
                                        <td > <c:out value="${blockGr.notUsedGr}"/> </td>
                                        <td > <c:out value="${blockGr.cancelledGr}"/> </td>
                                        <td > <c:out value="${blockGr.totBlockedGr}"/> </td>
                                        <td > <c:out value="${blockGr.totUsedGr}"/> </td>
                                        <td > <c:out value="${blockGr.totNotUsedGr}"/> </td>
                                        <td > <c:out value="${blockGr.totCancelledGr}"/> </td>
                                        <td > <c:out value="${blockGr.createdBy}"/> </td>
                                        <td > <c:out value="${blockGr.remarks}"/> </td>
                                    </tr>
                                </c:forEach>
                                <tr>
                                    <td colspan="5">&nbsp;</td>
                                    <td colspan="2" align="center">Total</td>
                                    <td ><c:out value="${totalBlocked}"/></td>
                                    <td ><c:out value="${totalNotUsed}"/></td>
                                    <td ><c:out value="${totalUsed}"/></td>
                                    <td ><c:out value="${totalCancelled}"/></td>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                            </table>
                        </c:if>
                                
                                <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                        <br><br>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5" selected="selected">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span><spring:message code="hrms.label.EntriesPerPage" text="default text"/></span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text"><spring:message code="hrms.label.DisplayingPage" text="default text"/> <span id="currentpage"> <span id="currentpage"></span> <spring:message code="hrms.label.of" text="default text"/> <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 1);
                        </script>

                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>
