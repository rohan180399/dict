
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
    </head>
    <script language="javascript">
        function submitPage(){
            
            var chek=validation();
            if(chek=='true'){
                
                document.addVeh.action='/throttle/alterUsingCompany.do';
                
                document.addVeh.submit();
            }
        }
        
        function validation(){
            if(textValidation(document.addVeh.companyName,'Using Company')){
                return 'false';
            }
            
            
            return 'true';
        }
        
    </script>
    <body >
        <form name="addVeh" method="post">
            <%@ include file="/content/common/path.jsp" %>                 
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <c:if test = "${UsingCompList!= null}" >
                <table align="center" width="500" border="0" class="border" cellspacing="0" cellpadding="0">
                    <tr height="30">
                        <Td colspan="2" class="contenthead">Add Using Company</Td>
                    </tr>
                    <c:forEach items="${UsingCompList}" var="veh">
                        <tr>
                            <td class="text2"><font color="red">*</font>Using Company</td>
                            <td class="text2"><input type="text" class="form-control" name="companyName" value="<c:out value='${veh.companyName}'/>"></td>
                       <input type="hidden" name="usingCompId" value="<c:out value='${veh.usingCompId}'/>">
                       </tr>
                        <tr height="30">
                            <td class="text2"><font color="red">*</font>Status</td>
                            <td class="text2"> <select class="form-control" name="activeInd" style="width:125px">
                                <c:if test="${veh.activeInd=='Y'}">
                                    <option selected value="Y">Active</option>
                                     <option value="N">INActive</option>
                                </c:if>
                                <c:if test="${veh.activeInd=='N'}">
                                <option value="Y">Active</option>
                                    <option selected value="N">INActive</option>
                                </c:if>
                                
                            </select>
                        </tr>
                    </c:forEach>
                </table>
            </c:if>
            <br><br>
            <center><input type="button" class="button" value="Save" onclick="submitPage();" /></center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
