<%--
    Document   : viewCloseTripSheet
    Created on : Jan 12, 2013, 8:45:49 AM
    Author     : Entitle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" import="java.util.ArrayList,java.net.*,java.io.*,java.util.*,java.util.Iterator,ets.domain.operation.business.OperationTO,ets.domain.operation.business.TripAllowanceTO,ets.domain.operation.business.TripFuelDetailsTO"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8" import="java.util.ArrayList,java.util.Iterator,ets.domain.operation.business.OperationTO,ets.domain.operation.business.TripAllowanceTO,ets.domain.operation.business.TripFuelDetailsTO"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Trip Sheet</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        
        <script>



        function print()
        {
            var DocumentContainer = document.getElementById("printDiv");
            var WindowObject = window.open('', "TrackHistoryData",
            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }

        function back()
        {
            window.history.back()
        }

    </script>


    </head>
    <body >
        <form name="tripSheet"  >
            <%@ include file="/content/common/message.jsp" %>
            <div id="printDiv">
            <%
                        OperationTO operationTo = new OperationTO();
                        ArrayList tripDetailsAL = (ArrayList) request.getAttribute("tripDetails");
                        if (tripDetailsAL != null && tripDetailsAL.size() > 0) {
                            operationTo = (OperationTO) tripDetailsAL.get(0);
                            String vehileId = operationTo.getTripVehicleId();

            %>
            <center><h2> Trip Settlement Form</h2><input type="hidden" name="tripSheetIdParam" value="<%=request.getParameter("tripSheetIdParam")%>" /></center>
            <div style="padding-left: 60px;">
                <table cellpadding="0" cellspacing="4" border="0" width="80%">
                    <tr>
                        <td class="contenthead">Trip Sheet Details </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table name="mainTBL" class="TableMain" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td  class="texttitle1"> Route</td>
                                    <td  class="text1"><%=operationTo.getRouteName()%>
                                    </td>
                                    <td class="texttitle1">
                                        Vehicle
                                    </td>
                                    <td  class="text1"><%=operationTo.getRegno()%>
                                    </td>


                                    <td class="texttitle1">
                                        Trip Id
                                    </td>
                                    <td class="texttitle1"><%=operationTo.getTripSheetId()%>
                                    </td>
                                    <td class="texttitle1">Trip Date</td>
                                    <td class="texttitle1"><%=operationTo.getTripDate()%>
                                    </td>
                                    <td class="texttitle1">
                                    </td>
                                </tr>

                                <tr>
                                    <td class="texttitle1">
                                        Driver Name
                                    </td>
                                    <td class="texttitle1"><%=operationTo.getDriverName()%>
                                    </td>



                                    <td class="texttitle1">
                                        Onward Trip Revenue
                                    </td>
                                    <td class="texttitle1"><%=operationTo.getRevenue()%>
                                    </td>
                                    <td class="texttitle1">
                                        Loaded / Delivered /Shortage
                                    </td>
                                    <td class="texttitle1"><%=operationTo.getTotalTonnage()%> / <%=operationTo.getTotalTonnage()%>
                                    </td>




                                    <td class="texttitle1"> LPS  : </td>
                                    <td class="texttitle1">
                                        <%=operationTo.getOrderNo()%>
                                    </td>

                                </tr>
                                <tr>
                                    <td class="texttitle1">
                                        Vehicle Type
                                    </td>
                                    <td class="texttitle1">
                                        <%
                                            if("1".equals(operationTo.getOwnership())){
                                            %>Own<%    
                                            }else {
                                            %>Market Vehicle<%
                                            }
                                        %>
                                    </td>
                                    <td class="texttitle1">
                                        Vehicle Vendor
                                    </td>
                                    <td class="texttitle1"><%=operationTo.getVendorName()%>
                                    </td>
                                    <td class="texttitle1">
                                        Bill Type
                                    </td>
                                    <td class="texttitle1"><%=operationTo.getBillStatus()%>
                                    </td>
                                    <td class="texttitle1"> &nbsp; </td>
                                    <td class="texttitle1"> &nbsp; </td>

                                </tr>

                                <!--                                <div >-->

                            </table>
                                    <table name="mainTBL" class="TableMain" cellpadding="0" cellspacing="0" width="100%">
                                
                                <tr align="center">
                                    <td align="center"  colspan="5" rowspan="1">
                                        <h2>Advance Details</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="TableColumn" colspan="5" rowspan="1" style="height: 0%">
                                        <div>
                                            <table cellpadding="0" cellspacing="0" style="width: 100%; left: 30px;" class="border" cellpadding="0" cellspacing="0" rules="all" id="allowanceTBL" name="allowanceTBL">
                                                <tr style="width:50px;">
                                                    <th width="50" class="contenthead">S No&nbsp;</th>
                                                    <th class="contenthead">Location</th>
                                                    <th class="contenthead">Date</th>
                                                    <th class="contenthead">Amount</th>
                                                    <th class="contenthead">Paid Person</th>
                                                    <th class="contenthead">Remarks</th>
                                                </tr>
                                                <%
                                                                            int snoAllo = 0;
                                                                            float totalAllowanceGiven = 0.00F;

                                                                            TripAllowanceTO allowanceTo = null;
                                                                            ArrayList allowanceAL = (ArrayList) request.getAttribute("tripAllowanceDetails");
                                                                            if (allowanceAL != null) {
                                                                                Iterator it = allowanceAL.iterator();
                                                                                while (it.hasNext()) {
                                                                                    allowanceTo = new TripAllowanceTO();
                                                                                    allowanceTo = (TripAllowanceTO) it.next();
                                                                                    snoAllo++;
                                                                                    totalAllowanceGiven = totalAllowanceGiven + Float.parseFloat(allowanceTo.getTripAllowanceAmount());
                                                %>
                                                <tr>
                                                    <td><%=snoAllo%></td>
                                                    <td><%=allowanceTo.getLocationName()%>
                                                    </td>
                                                    <td><%=allowanceTo.getTripAllowanceDate()%>
                                                    </td>
                                                    <td><%=allowanceTo.getTripAllowanceAmount()%>
                                                    </td>
                                                    <td><%=allowanceTo.getTripAllowancePaidBy()%>
                                                    </td>
                                                    <td>
                                                        <%=allowanceTo.getTripAllowanceRemarks()%>
                                                        
                                                    </td>
                                                </tr>
                                                <%
                                                                                }

                                                                            }
                                                %>
                                                
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                
                                <tr align="center">
                                    <td align="center"  colspan="5" rowspan="1">
                                        <h2>Expense Details</h2>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td align="center" class="TableColumn" colspan="5" rowspan="1" style="height: 0%">
                                        <div>
                                            <table cellpadding="0" cellspacing="0" style="width: 100%; left: 30px;" class="border" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <th width="50" class="contenthead">S No&nbsp;</th>
                                                    <th class="contenthead">Expenses</th>
                                                    <th class="contenthead">Amount</th>
                                                    <th class="contenthead">Liters(if Fuel)</th>
                                                    <th class="contenthead">Date</th><th class="contenthead">Remarks</th><th class="contenthead">Mode</th>
                                                </tr>
                                                <%
                                                                            double expTotal = 0;
                                                                            int expSno = 0;
                                                                            TripFuelDetailsTO expTo = new TripFuelDetailsTO();
                                                                            ArrayList expAL = (ArrayList) request.getAttribute("etmExpDetails");
                                                                            if (expAL != null) {
                                                                                Iterator itExp = expAL.iterator();
                                                                                while (itExp.hasNext()) {
                                                                                    expSno++;
                                                                                    expTo = new TripFuelDetailsTO();
                                                                                    expTo = (TripFuelDetailsTO) itExp.next();
                                                                                    expTotal += Double.parseDouble(expTo.getAmount());
                                                %>
                                                <tr style="width:50px;">
                                                    <td width="50"><%=expSno%></td>
                                                    <td><%=expTo.getCategory()%></td>
                                                    <td><%=expTo.getAmount()%></td>
                                                    <td><%=expTo.getTripFuelLtrs()%></td><td><%=expTo.getTdate()%></td><td><%=expTo.getRemark()%></td><td><%=expTo.getMode()%></td>
                                                </tr>
                                                <%


                                                                                }
                                                                            }

                                                %>
                                                
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                
                            </table>
                        </td>
                    </tr>
                </table>

                    <%
                    if("1".equals(operationTo.getOwnership())){
                    %>
                    
                                <div id="ReturnDetail">
                                
                                                <table cellpadding="0" cellspacing="0" width="80%">
                                                    <tr align="center">
                                                        <td align="center"  colspan="9" rowspan="1">
                                                            <h2>Return Movements</h2>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th width="50" class="contenthead">S No&nbsp;</th>
                                                        <th class="contenthead">Return Trip Type</th>
                                                        <th class="contenthead">Customer</th>
                                                        <th class="contenthead">From Loc</th>
                                                        <th class="contenthead">To Loc</th>
                                                        <th class="contenthead">Product</th>
                                                        <th class="contenthead">Distance(KM)</th>
                                                        <th class="contenthead">Delivered / Shortage</th>
                                                        <th class="contenthead">Revenue</th>
                                                    </tr>
                                                    <% int cntr = 1;%>
                                                    <c:if test = "${returnTripList != null}" >

                                                        <c:forEach items="${returnTripList}" var="returnTrip">

                                                            <tr>
                                                                <td><%=cntr%></td>
                                                                <td >
                                                                    <c:out value="${returnTrip.returnTripType}" />
                                                                </td>
                                                                <td >
                                                                    <c:out value="${returnTrip.customerId}" />
                                                                </td>
                                                                <td >
                                                                    <c:out value="${returnTrip.returnFromLocation}" />
                                                                </td>
                                                                <td>
                                                                    <c:out value="${returnTrip.returnToLocation}" />
                                                                </td>

                                                                <td>
                                                                    <c:out value="${returnTrip.returnProductName}" />
                                                                </td>
                                                                <td>
                                                                    <c:out value="${returnTrip.returnKM}" />
                                                                </td>
                                                                <td>
                                                                    <c:out value="${returnTrip.deliveredTonnage}" /> / <c:out value="${returnTrip.shortageTonnage}" />
                                                                </td>
                                                                <td>
                                                                    <c:out value="${returnTrip.returnAmount}" />
                                                                </td>
                                                            </tr>
                                                            <% cntr++;%>
                                                        </c:forEach >
                                                    </c:if>

                                                    
                                                </table>
                                            </div>
                                        
                                     <br>
                                    <%
                                        }
                                    %>
                                     <table>
                                    

                                    <tr align="center">


                                        <td align="right" class="texttitle1">
                                            NETT Advances
                                        </td>
                                        <td align="left" class="texttitle1">&nbsp;</td>

                                        <td align="left" class="texttitle1">
                                            <%=operationTo.getTripTotalAllowances()%>
                                        </td>
                                        <td align="left" class="texttitle1">&nbsp;</td>
                                        <td align="right" class="texttitle1">
                                            NETT Expenses
                                        </td>
                                        <td align="left" class="texttitle1">&nbsp;</td>

                                        <td align="left" class="texttitle1"><%=operationTo.getTripTotalExpenses()%>
                                        </td>
                                        <td align="left" class="texttitle1">&nbsp;</td>
                                        

                                        <td align="right" class="texttitle1">
                                            NETT Kms
                                        </td>
                                        <td align="left" class="texttitle1">&nbsp;</td>
                                        <td align="left" class="texttitle1"><%=operationTo.getTripTotalKms()%>
                                        </td>
                                        <td align="left" class="texttitle1">&nbsp;</td>

                                        <td align="right" class="texttitle1">
                                            NETT Fuel Ltrs
                                        </td>
                                        <td align="left" class="texttitle1">&nbsp;</td>
                                        <td align="left" class="texttitle1"><%=operationTo.getTripTotalLitres()%>
                                        </td>
                                        <td align="left" class="texttitle1">&nbsp;</td>

                                       


                                        <td align="right" class="texttitle1">
                                            NETT Mileage
                                        </td>
                                        <td align="left" class="texttitle1">&nbsp;</td>
                                        <td align="left" class="texttitle1"><%=operationTo.getMileage()%>
                                            
                                        </td>
                                        <td align="left" class="texttitle1">&nbsp;</td>

                                        <td align="right" class="texttitle1">

                                            NETT Balance Amount

                                        </td>
                                        <td align="left" class="texttitle1">&nbsp;</td>

                                        <td align="left" class="texttitle1"> <%=operationTo.getTripBalanceAmount()%>
                                        </td>

                                        <td align="left" class="texttitle1">&nbsp;</td>
                                        <td align="left" class="texttitle1">&nbsp;</td>

                                    </tr>
                                </table>
                           
                            </div>
                
                </table>

            </div>
           
            <center><input align="center" type="button" onclick="print();" value = " Print "   /></center>

            <%
                        }
            %>
         </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

    </body>
</html>
