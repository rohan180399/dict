   /*-------------------------------------------------------------------------
 * __NAME__.java
 * __DATE__
 *
 * Copyright (c) Entitle.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Entitle ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Entitle.
-------------------------------------------------------------------------*/
/******************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver    Date                               Author                    Change
 * ----------------------------------------------------------------------------
 * 1.0   __DATE__                Your_Name ,Entitle      Created
 *
 ******************************************************************************/
package ets.domain.recondition.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import ets.domain.util.FPLogUtils;
import ets.arch.exception.FPRuntimeException;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import ets.domain.recondition.business.ReconditionTO;

public class ReconditionDAO extends SqlMapClientDaoSupport {

    private final int errorStatus = 4;
    private final static String CLASS = "ReconditionDAO";

    public ArrayList getRcQueue(ReconditionTO rec) {
        Map map = new HashMap();
        int status = 0;
        ArrayList rcQueue = new ArrayList();
        try {
            map.put("companyId", rec.getCompanyId());
            rcQueue = (ArrayList) getSqlMapClientTemplate().queryForList("recondition.getRcQueue", map);
            //////System.out.println("rcQueue=" + rcQueue.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRcQueue Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRcQueue", sqlException);
        }
        return rcQueue;
    }
    public ArrayList getRcQueueTyreNos(String rcQueueId) {
        Map map = new HashMap();
        int status = 0;
        ArrayList tyreNos = new ArrayList();
        try {
            map.put("rcQueueId", rcQueueId);
            tyreNos = (ArrayList) getSqlMapClientTemplate().queryForList("recondition.getRcQueueTyreNos", map);
            //////System.out.println("rcQueue=" + tyreNos.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRcQueueTyreNos Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRcQueueTyreNos", sqlException);
        }
        return tyreNos;
    }

    public int doInsertScrapRc(ArrayList scrapItems, int companyId, int userId) {
        Map map = new HashMap();
        int status = 0;
        Iterator itr;
        int Qty = 1;
        int update=0;
        String rcStatus = "N";
        ReconditionTO reconditionTO;
        try {
            map.put("rcStatus", rcStatus);
            map.put("companyId", companyId);
            map.put("userId", userId);
            itr = scrapItems.iterator();
            while (itr.hasNext()) {
                reconditionTO = new ReconditionTO();
                reconditionTO = (ReconditionTO) itr.next();
                map.put("rcQueueId", reconditionTO.getRcQueueId());
                String mrsId = (String) getSqlMapClientTemplate().queryForObject("recondition.getMrsId", map);
                map.put("mrsId", mrsId);
                map.put("itemId", reconditionTO.getItemId());
                map.put("rcItemId", reconditionTO.getRcNumber());
                map.put("quantity", Qty);
                status = (Integer) getSqlMapClientTemplate().update("recondition.insertScrap", map);
                //////System.out.println("Inserted Status=" + status);
                status = (Integer) getSqlMapClientTemplate().update("recondition.updateRcQueue", map);
                //////System.out.println("Updated Rc Queue Status=" + status);

                update=(Integer)getSqlMapClientTemplate().update("recondition.updateRcActiveStatus", map);
                //////System.out.println("Updated Rc Queue Status=" + update);

            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doInsertScrap Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-02", CLASS, "doInsertScrap", sqlException);
        }
        return status;
    }

    public int doGenerateWo(ArrayList vendorItems,String jobCardId,String customerType,String customerName,String customerAddress,String mobileNo, int companyId, int userId, ReconditionTO reconTO) {
        Map map = new HashMap();
        int RcwoId = 0, status = 0;
        map.put("gdType", reconTO.getGdType());
        map.put("remarks", reconTO.getRemarks());
        map.put("desc", reconTO.getRemarks());
        //Rc Zone
        map.put("jobCardId", jobCardId);
        map.put("customerType", customerType);
        map.put("customerName", customerName);
        map.put("customerAddress", customerAddress);
        map.put("mobileNo", mobileNo);
        //RC Zone End
        //////System.out.println("remarks" + reconTO.getRemarks());
        Iterator itr;
        itr = vendorItems.iterator();
        ReconditionTO vend = new ReconditionTO();
        try {
            map.put("userId", userId);
            map.put("companyId", companyId);

            if (itr.hasNext()) {
                //////System.out.println("itearte runned");
                vend = (ReconditionTO) itr.next();
                map.put("companyId", companyId);
                map.put("vendorId", vend.getVendorId());
                map.put("userId", userId);
                map.put("status","Y");
                RcwoId = (Integer) getSqlMapClientTemplate().insert("recondition.insertWo", map);
            }
            //////System.out.println("rc wo Id=" + RcwoId);
            map.put("RcwoId", RcwoId);
            status = (Integer) getSqlMapClientTemplate().update("recondition.insertGoodsDeliveryMaster", map);
            //////System.out.println("insertGoodsDeliveryMaster=" + status);
            itr = vendorItems.iterator();
            while (itr.hasNext()) {
                vend = new ReconditionTO();
                vend = (ReconditionTO) itr.next();
                map.put("rcQueueId", vend.getRcQueueId());
                map.put("rcItemId", vend.getRcNumber());
                map.put("rcStatus", "N");
                //////System.out.println("vend.getRcQueueId()==" + vend.getRcQueueId());
                status = (Integer) getSqlMapClientTemplate().update("recondition.insertWoItems", map);
                status = (Integer) getSqlMapClientTemplate().update("recondition.updateRcQueue", map);
                if (vend.getCategoryId().equalsIgnoreCase("1011")) {
                    status = (Integer) getSqlMapClientTemplate().update("recondition.updateTyreAvailability", map);
                } else {
                    status = (Integer) getSqlMapClientTemplate().update("recondition.updateRcItemMaster", map);
                }
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doGenerateWo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "doGenerateWo", sqlException);
        }
        return RcwoId;
    }

// Vinoth
    public ArrayList getWOList(ReconditionTO reconditionTO) {
        Map map = new HashMap();
        map.put("companyId", reconditionTO.getCompanyId());
        map.put("vendorId", reconditionTO.getVendorId());
        ArrayList workOrderList = new ArrayList();
        try {
            workOrderList = (ArrayList) getSqlMapClientTemplate().queryForList("recondition.getWOList", map);
            //////System.out.println("workOrderList size=" + workOrderList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-PR-01", CLASS, "workOrderList", sqlException);
        }
        return workOrderList;
    }

    public ArrayList getRcItemList(ReconditionTO stocktransferTO) {
        Map map = new HashMap();
        map.put("woId", stocktransferTO.getWoId());
        map.put("vendorId", stocktransferTO.getVendorId());
        //////System.out.println("woId" + stocktransferTO.getWoId());
        ArrayList itemList = new ArrayList();
        int prePrice = 0;
        try {
            System.out.println("map:"+map);
            itemList = (ArrayList) getSqlMapClientTemplate().queryForList("recondition.getRcItemList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-PR-01", CLASS, "itemList", sqlException);
        }
        return itemList;
    }

    public int insertRcItem(ArrayList List, int userId, ReconditionTO reconditionTO) {
        Map map = new HashMap();
        int status = 0;
        String activeInd = "";
        int billNo = 0;
        int qty = 1;
        int categoryId=0;
        //Hari
        int rcItemServicePt=0;

        ArrayList itemList = new ArrayList();
        String priceId = "";
        try {
            Iterator itr = List.iterator();
            ReconditionTO listTO = null;
            map.put("userId", userId);
            map.put("companyId", reconditionTO.getCompanyId());
            map.put("billAmount", reconditionTO.getBillAmount());
            map.put("date", reconditionTO.getReceivedDate());
            map.put("remarks", reconditionTO.getRemarks());
            map.put("Qty", qty);
            map.put("rcWoId", reconditionTO.getWoId());
            //////System.out.println("woid" + reconditionTO.getWoId());
            billNo = (Integer) getSqlMapClientTemplate().insert("recondition.insertRcBillMaster", map);
            //////System.out.println(" insertRcBillMaster" + billNo);
            //////System.out.println("Compnay Id-->"+reconditionTO.getCompanyId());
//            billNo = (Integer) getSqlMapClientTemplate().queryForObject("recondition.getRcBillId", map);
            map.put("billNo", billNo);
            while (itr.hasNext()) {
                listTO = (ReconditionTO) itr.next();
                map.put("itemId", listTO.getItemId());
                //////System.out.println("itemId" + listTO.getItemId());
//                map.put("userId", userId);
                map.put("itemAmount", listTO.getAmount());
                map.put("tax", listTO.getTax());
                map.put("sparesAmount", listTO.getSparesAmount());
                map.put("sparesAmountExternal", listTO.getSparesAmountExternal());
                map.put("taxAmount", (Float.parseFloat(listTO.getSparesWithTax()) - (Float.parseFloat(listTO.getSparesAmount()) + Float.parseFloat(listTO.getSparesAmountExternal()))));
                map.put("laborAmount", listTO.getLaborAmount());
                map.put("rcItemId", listTO.getRcItemId());
                //Hari
//                //////System.out.println("Spares With Tax"+Float.parseFloat(listTO.getSparesWithTax()));
//                //////System.out.println("Spares Amount"+Float.parseFloat(listTO.getSparesAmount()));
//                //////System.out.println("Spares Amount External"+Float.parseFloat(listTO.getSparesAmountExternal()));
//                //////System.out.println("Tax AMount"+(Float.parseFloat(listTO.getSparesWithTax()) - (Float.parseFloat(listTO.getSparesAmount()) + Float.parseFloat(listTO.getSparesAmountExternal()))));
                categoryId= (Integer) getSqlMapClientTemplate().queryForObject("recondition.getItemCategory", map);
                //////System.out.println("Item Id-->"+listTO.getItemId()+"Category-->"+categoryId);
                //Hari End
                //////System.out.println("Rc Item Id-->"+listTO.getRcItemId());
                //////System.out.println("listTO.getRcstatus()= in dao" + listTO.getRcstatus());
                if (listTO.getRcstatus().equalsIgnoreCase("Y")) {
                    map.put("rcStatus", listTO.getRcstatus());
                    map.put("activeInd", activeInd);
                    status = (Integer) getSqlMapClientTemplate().update("recondition.updateRcStockBalance", map);
                    //////System.out.println("Stock Balance status " + status);
                    if(status==0)
                    {
                    //////System.out.println("Previously No Entry For Item In Rc Stock Balance-->"+listTO.getItemId());
                    status = (Integer) getSqlMapClientTemplate().update("recondition.insertRcStockBalance", map);
                    //////System.out.println("Stock Added for ServicePoint -->"+reconditionTO.getCompanyId()+"Status-->"+status);
                    }
                    status = (Integer) getSqlMapClientTemplate().update("recondition.insertRcBillDetails", map);
                    //////System.out.println("insert insertRcBillDetailsstatus " + status);
                    //Tyre Item no need to update rcItemMaster
                    if(categoryId!=1011)
                    {
                    status = (Integer) getSqlMapClientTemplate().update("recondition.updateRcItemMaster", map);
                    //////System.out.println("Result updateRcItemMaster status"+status);
                    //////System.out.println("In Login Service Point");                    
                    }

                    //Hari
                    //If RcItemId is generated i Other Service pT for pdate its Available Status( not for Tyre).
                    if(status==0 && categoryId!=1011)
                    {
                    //////System.out.println("FFFOr Other Service Point Updates");
                    rcItemServicePt = (Integer) getSqlMapClientTemplate().queryForObject("recondition.getRcItemServicePoint", map);
                    map.put("companyId", rcItemServicePt);
                    status = (Integer) getSqlMapClientTemplate().update("recondition.updateRcItemMaster", map);
                    //////System.out.println("Sucessfull Update of Available Status for Service Point"+rcItemServicePt+"Status-->"+status);
                    }
                    //Hari End
                    
                } else {
                    String mrsId = (String) getSqlMapClientTemplate().queryForObject("recondition.getMrsIdFromRcId", map);
                    map.put("rcStatus", "N");
                    map.put("mrsId", mrsId);
                    map.put("quantity", qty);
                    status = (Integer) getSqlMapClientTemplate().update("recondition.insertRcBillDetails", map);
                    status = (Integer) getSqlMapClientTemplate().update("recondition.insertScrap", map);
                    //////System.out.println("Inserted Status=" + status);
                    status = (Integer) getSqlMapClientTemplate().update("recondition.updateRcQueue", map);
                    getSqlMapClientTemplate().update("recondition.updateRcActiveStatus", map);
                }
            }
            status = (Integer) getSqlMapClientTemplate().update("recondition.updateRcWo", map);
            //////System.out.println(" updateRcWo" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-PR-01", CLASS, "status", sqlException);
        }
        return billNo;
    }

    public ArrayList getAvailableRcItems(int companyId) {
        Map map = new HashMap();
        ArrayList rcItems = new ArrayList();
        try {
            map.put("companyId", companyId);
            rcItems = (ArrayList) getSqlMapClientTemplate().queryForList("recondition.availableRcItems", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAvailableRcItems Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-02", CLASS, "getAvailableRcItems", sqlException);
        }
        return rcItems;
    }

    public int getAvailableRcItems(int rcItemId, int queueId) {
        Map map = new HashMap();
        int stat = 0;
        try {
            map.put("rcItemId", rcItemId);
            map.put("queueId", queueId);
            stat = (Integer) getSqlMapClientTemplate().update("recondition.updateVehTyre", map);
            stat = (Integer) getSqlMapClientTemplate().update("recondition.updateRcQueueRcId", map);
            //////System.out.println("rcqueue update status=" + stat);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAvailableRcItems Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-02", CLASS, "getAvailableRcItems", sqlException);
        }
        return stat;
    }

    public int doGenerateRcItemId(ReconditionTO rec, int userId) {
        Map map = new HashMap();
        int stat = 0;
        int rcId = 0;
        try {
            map.put("itemId", rec.getItemId());
            map.put("companyId", rec.getCompanyId());
            map.put("availability", rec.getAvailability());
            map.put("status", rec.getStatus());
            map.put("userId", userId);
            rcId = (Integer) getSqlMapClientTemplate().insert("recondition.generateRcItemId", map);
//            rcId = (Integer) getSqlMapClientTemplate().queryForObject("recondition.getRcId", map);
            //////System.out.println("rcqueue update status=" + rcId);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doGenerateRcItemId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-02", CLASS, "doGenerateRcItemId", sqlException);
        }
        return rcId;
    }

    public ArrayList getVehicleRcRtItems(int companyId) {
        Map map = new HashMap();
        ArrayList rcList = new ArrayList();
        map.put("companyId", companyId);
        try {
            rcList = (ArrayList) getSqlMapClientTemplate().queryForList("recondition.getVehicleRtRcItems", map);
            //////System.out.println("getVehicleRcRtItems rcList size=" + rcList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleRcRtItems Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleRcRtItems", sqlException);
        }
        return rcList;
    }

    public ArrayList getWoRaisedItems() {
        Map map = new HashMap();
        ArrayList woList = new ArrayList();
        try {
            woList = (ArrayList) getSqlMapClientTemplate().queryForList("recondition.getRcWoRaisedItems", map);
            //////System.out.println("rcList size=" + woList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWoRaisedItems Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getWoRaisedItems", sqlException);
        }
        return woList;
    }
// Raja April04    
    public ArrayList getRcDuration(int itemId, int rcItemId) {
        Map map = new HashMap();
        ArrayList rcList = new ArrayList();
        map.put("itemId", itemId);
        map.put("rcItemId", rcItemId);
        try {
            rcList = (ArrayList) getSqlMapClientTemplate().queryForList("recondition.getRcDuration", map);
            //////System.out.println("rcDuration rcList size=" + rcList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRcDuration Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRcDuration", sqlException);
        }
        return rcList;
    }

    public int doUpdateIsRtTyre(ReconditionTO rec) {
        Map map = new HashMap();
        int stat = 0;
        //map.put("tyreId", rec.getRcItemId());
        map.put("tyreId", rec.getTyreId());
        map.put("status", rec.getStatus());
        map.put("stockStatus", rec.getStkStatus());
        map.put("companyId", rec.getCompanyId());
        System.out.println("map value is:"+map);
        try {
            stat = (Integer) getSqlMapClientTemplate().update("recondition.updateIsRtTyre", map);
            //////System.out.println("stat size=" + stat);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doupdateIsRtTyre Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "doupdateIsRtTyre", sqlException);
        }
        return stat;
    }

    public String getTyreNo(String tyreId, int companyId) {
        Map map = new HashMap();
        map.put("tyreId", tyreId);
        map.put("companyId", companyId);
        String tyreNo = "";
        //////System.out.println("tyreId=" + tyreId + "--companyId=" + companyId);
        try {
            tyreNo = (String) getSqlMapClientTemplate().queryForObject("recondition.getTyreNo", map);
            if (tyreNo == null) {
                tyreNo = "";
            }
            //////System.out.println("tyreNo=" + tyreNo);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "tyreNo", sqlException);
        }
        return tyreNo;
    }

    public int doUpdateRcPrice(ArrayList List) {
        Map map = new HashMap();
        int status = 0;
        String activeInd = "";
        int billNo = 0;
        int qty = 1;
        int check = 0;

        ArrayList itemList = new ArrayList();
        String priceId = "";
        try {
            Iterator itr = List.iterator();
            ReconditionTO listTO = null;
            while (itr.hasNext()) {
                listTO = new ReconditionTO();
                listTO = (ReconditionTO) itr.next();
                map.put("itemId", listTO.getItemId());
                map.put("itemAmount", listTO.getRcAmntWithOutTax());
                map.put("rcItemId", listTO.getRcItemId());
                if (listTO.getRcstatus().equalsIgnoreCase("Y")) {
                    check = (Integer) getSqlMapClientTemplate().queryForObject("recondition.checkRcItemPrice", map);
                    if (check == 0) {
                        status = (Integer) getSqlMapClientTemplate().update("recondition.insertRcPrice", map);
                    } else {
                        status = (Integer) getSqlMapClientTemplate().update("recondition.updateRcPrice", map);
                    }
                    //////System.out.println("doUpdateRcPrice " + status);
                }
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doUpdateRcPrice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-PR-01", CLASS, "doUpdateRcPrice", sqlException);
        }
        return status;
    }

    public int doInsertVehicleTyre(ReconditionTO tyreTO, int UserId, int companyId) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("vehicleId", tyreTO.getVehicleId());
        int status = 0;
        int insertStatus = 0;
        int tyreId = 0;
        Integer tyreNo;
        try {
            map.put("itemId", tyreTO.getItemId());
            map.put("companyId", companyId);
            map.put("userId", UserId);
            map.put("queueId", tyreTO.getRcQueueId());
            map.put("tyreNo", tyreTO.getTyreNo());

            tyreNo = (Integer) getSqlMapClientTemplate().queryForObject("recondition.checkTyreNo", map);
            //////System.out.println("doInsertVehicleTyre Tyre No-->"+tyreNo);
            if (tyreNo == null) {
                tyreId = (Integer) getSqlMapClientTemplate().insert("recondition.insertVehicleTyre", map);
                map.put("rcItemId", tyreId);
                //////System.out.println("u[pdate tyreId=" + tyreId + "queueId=" + tyreTO.getRcQueueId());
                insertStatus = (Integer) getSqlMapClientTemplate().update("recondition.updateRcQueueRcId", map);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doInsertVehicleTyre Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "doInsertVehicleTyre", sqlException);
        }
        return insertStatus;
    }

    public ArrayList getRcGoodsTransfer(int companyId) {
        Map map = new HashMap();
        int status = 0;
        ArrayList rcQueue = new ArrayList();
        try {
            map.put("companyId", companyId);
            rcQueue = (ArrayList) getSqlMapClientTemplate().queryForList("recondition.rcGoodsTransfer", map);
            //////System.out.println("getRcGoodsTransfer=" + rcQueue.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRcGoodsTransfer Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRcGoodsTransfer", sqlException);
        }
        return rcQueue;
    }

    public int doSenderStatus(int rcQueueId, String senderStatus) {
        Map map = new HashMap();
        int status = 0;
        ArrayList rcQueue = new ArrayList();
        try {
            map.put("rcQueueId", rcQueueId);
            map.put("senderStatus", senderStatus);
            status = getSqlMapClientTemplate().update("recondition.setSenderStatus", map);
            //////System.out.println("doSenderStatus=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("doSenderStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "doSenderStatus", sqlException);
        }
        return status;
    }

    public int doInsertFaultItems(int itemId, int qty,int woId,int companyId,int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("itemId", itemId);
        map.put("qty", qty);
        int rcId = 0;        
        int queueId = 0;
        int j = 1;
        try {
            //////System.out.println("dao recondition" + itemId);
            //////System.out.println("dao recondition" + qty);
            if (companyId == 1011) {
                map.put("senderStatus", "Y");
            }
            if (companyId != 1011) {
                map.put("senderStatus", "N");
            }
//            woId = (Integer) getSqlMapClientTemplate().insert("recondition.generateRcWo", map);
            map.put("woId", woId);
            map.put("companyId", companyId);
            map.put("userId", userId);


            for (int i = 0; i < qty; i++) {
                rcId = (Integer) getSqlMapClientTemplate().insert("recondition.insertRcMaster", map);
                map.put("rcId", rcId);
                //////System.out.println("rcid=" + rcId);
                //////System.out.println("woid=" + woId);
                queueId = (Integer) getSqlMapClientTemplate().insert("recondition.insertFaultItems", map);
                map.put("rcQueueId", queueId);
                status = (Integer) getSqlMapClientTemplate().update("recondition.insertwoDetails", map);
                //////System.out.println("status " + status);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertMrsItems Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-02", CLASS, "insertMrsItems", sqlException);
        }
        return status;

    }
    
    
public int doGenerateRcWO( int companyId, int userId, String vendorId,String jobCardId,String customerType,String customerName,String customerAddress,String mobileNo, String remarks) {
        Map map = new HashMap();
        int status = 0;    
        int woId = 0;
        map.put("companyId", companyId);
        map.put("userId", userId);
        map.put("vendorId", vendorId);
        map.put("jobCardId", jobCardId);
        map.put("customerType", customerType);
        map.put("customerName", customerName);
        map.put("customerAddress", customerAddress);
        map.put("mobileNo", mobileNo);
        map.put("remarks", remarks);
        try {
            if (companyId == 1011) {
                map.put("senderStatus", "Y");
            }
            if (companyId != 1011) {
                map.put("senderStatus", "N");
            }
            woId = (Integer) getSqlMapClientTemplate().insert("recondition.generateRcWo", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertMrsItems Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-02", CLASS, "insertMrsItems", sqlException);
        }
        return woId;        
}    

    public String getMfrItemId(String mfrCode) {
        Map map = new HashMap();


        map.put("mfrCode", mfrCode);
        String suggestions = "";
        try {
            suggestions = (String) getSqlMapClientTemplate().queryForObject("recondition.getMfrItemId", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMfrItemId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getMfrItemId", sqlException);
        }
        return suggestions;

    }

    public String getCodeItemId(String itemCode) {
        Map map = new HashMap();


        map.put("itemCode", itemCode);
        String suggestions = "";
        try {
            suggestions = (String) getSqlMapClientTemplate().queryForObject("recondition.getCodeItemId", map);
            //////System.out.println("suggestions" + suggestions);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("itemNameSuggestions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "itemNameSuggestions", sqlException);
        }
        return suggestions;

    }

    public String getNameItemId(String itemName) {
        Map map = new HashMap();


        map.put("itemName", itemName);
        String suggestions = "";
        try {
            suggestions = (String) getSqlMapClientTemplate().queryForObject("recondition.getNameItemId", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("itemNameSuggestions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "itemNameSuggestions", sqlException);
        }
        return suggestions;
    }

    public String getNameItemIdToScrap(String itemName,String paplCode) {
        Map map = new HashMap();
        map.put("itemCode", paplCode);
        map.put("itemName", itemName);
        String suggestions = "";
        try {
            //////System.out.println("In reconditionDAO");
            suggestions = (String) getSqlMapClientTemplate().queryForObject("recondition.getNameItemIdToScrap", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            sqlException.getMessage();
            FPLogUtils.fpDebugLog("itemNameSuggestions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "itemNameSuggestions", sqlException);
        }
        return suggestions;
    }

    public int addScrapItems(int itemId,int qty,int priceId, int userId) {
        Map map = new HashMap();
        map.put("itemId", itemId);
        map.put("reqQty", qty);
        map.put("priceId", priceId );
        map.put("userId", userId );
        map.put("status", "PENDING" );
        int status = 0;
        try {
            //////System.out.println("In reconditionDAO");
            getSqlMapClientTemplate().update("recondition.addScrapItems", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            sqlException.getMessage();
            FPLogUtils.fpDebugLog("itemNameSuggestions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "itemNameSuggestions", sqlException);
        }
        return status;
    }

    public ArrayList getRCWOItems(ReconditionTO repTO) {
        Map map = new HashMap();
        ArrayList woDetail = new ArrayList();
        try {
            map.put("woId", repTO.getWoId());
            //////System.out.println(" repTO.getWoId()   "+repTO.getWoId());
            woDetail = (ArrayList) getSqlMapClientTemplate().queryForList("recondition.woDetail", map);
            //////System.out.println("woDetail size=" + woDetail.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRCWOItems Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getRCWOItems", sqlException);
        }
        return woDetail;
    }

    public int updateRcWo(ReconditionTO recTO) {
        Map map = new HashMap();
        ArrayList woDetail = new ArrayList();
        int status = 0;
        try {
            map.put("woId", recTO.getWoId());
            map.put("vendorId", recTO.getVendorId());
            map.put("remarks", recTO.getRemarks());
            map.put("stat", recTO.getStatus());
            map.put("createdOn", recTO.getCreatedDate());      
            
            //////System.out.println("woId"+ recTO.getWoId());
            //////System.out.println("vendorId"+ recTO.getVendorId());
            //////System.out.println("remarks"+ recTO.getRemarks());
            //////System.out.println("stat"+ recTO.getStatus());
            //////System.out.println("createdOn"+ recTO.getCreatedDate());

            status = (Integer) getSqlMapClientTemplate().update("recondition.updateWO", map);
            //////System.out.println("update status=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRCWOItems Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getRCWOItems", sqlException);
        }
        return status;
    }


//Hari

 public ArrayList getRcWorkOrderList(int companyId) {
        Map map = new HashMap();
        int status = 0;
        ArrayList rcWorkOrder = new ArrayList();
        try {
            map.put("companyId", companyId);
            rcWorkOrder = (ArrayList) getSqlMapClientTemplate().queryForList("recondition.getRcWorkOrderList", map);
            //////System.out.println("getRcWorkOrderList=" + rcWorkOrder.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRcWorkOrder Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRcWorkOrder", sqlException);
        }
        return rcWorkOrder;
    }

 public ArrayList getRcWorkOrderDetails(int rcWorkId,int companyId) {
        Map map = new HashMap();
        int status = 0;
        ArrayList rcWorkOrderDetails = new ArrayList();
        try {
            map.put("companyId", companyId);
            map.put("rcWorkId", rcWorkId);
            rcWorkOrderDetails = (ArrayList) getSqlMapClientTemplate().queryForList("recondition.getRcWorkOrderDetails", map);
            //////System.out.println("getRcWorkOrderList=" + rcWorkOrderDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRcWorkOrderDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRcWorkOrderDetails", sqlException);
        }
        return rcWorkOrderDetails;
    }

 public ArrayList getRcMrsWorkOrderDetails(int rcWorkId,int companyId,int mrsId) {
        Map map = new HashMap();
        int status = 0;
        ArrayList rcMrsWorkOrderDetails = new ArrayList();
        try {
            map.put("companyId", companyId);
            map.put("rcWorkId", rcWorkId);
            map.put("mrsId", mrsId);
            rcMrsWorkOrderDetails = (ArrayList) getSqlMapClientTemplate().queryForList("recondition.getRcMrsWorkOrderDetails", map);
            //////System.out.println("rcMrsWorkOrderDetails=" + rcMrsWorkOrderDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRcMrsWorkOrderDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRcMrsWorkOrderDetails", sqlException);
        }
        return rcMrsWorkOrderDetails;
    }


 public ArrayList getCompanyEmployee(int companyId) {
        Map map = new HashMap();
        int status = 0;
        ArrayList compTechnician = new ArrayList();
        try {
            map.put("companyId", companyId);
            compTechnician = (ArrayList) getSqlMapClientTemplate().queryForList("recondition.getCompanyTechnician", map);
            //////System.out.println("getCompanyEmployee=" + compTechnician.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCompanyEmployee Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCompanyEmployee", sqlException);
        }
        return compTechnician;
    }



public ArrayList getRcMrsList(int rcWorkId)
    {
        Map map = new HashMap();
        ArrayList rcMrsList = new ArrayList();
        map.put("rcWorkId", Integer.valueOf(rcWorkId));
        try
        {
            rcMrsList = (ArrayList)getSqlMapClientTemplate().queryForList("recondition.getRcMrsList", map);
            System.out.println((new StringBuilder()).append("getRcMrsList size=").append(rcMrsList.size()).toString());
        }
        catch(Exception sqlException)
        {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getRcMrsList Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "getRcMrsList", sqlException);
        }
        return rcMrsList;
    }


public ArrayList getRcWOItems(int rcWorkId)
    {
        Map map = new HashMap();
        ArrayList rcWOItems = new ArrayList();
        map.put("rcWorkId", Integer.valueOf(rcWorkId));
        try
        {
            rcWOItems = (ArrayList)getSqlMapClientTemplate().queryForList("recondition.getRcWOItems", map);
            System.out.println((new StringBuilder()).append("getRcWOItems size=").append(rcWOItems.size()).toString());
        }
        catch(Exception sqlException)
        {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getRcWOItems Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "getRcWOItems", sqlException);
        }
        return rcWOItems;
    }

//vijay 22 mar 2010

public String getRCSpareAmount(String rcWorkId,int RcItemId)
    {
        Map map = new HashMap();
        String rcWOItems ="";
        String isExist="";
        float amount=0.0f;
        map.put("rcWorkId", Integer.valueOf(rcWorkId));
        map.put("RcItemId", RcItemId);
        //////System.out.println("rcWorkId"+rcWorkId);
        //////System.out.println("RcItemId"+RcItemId);
        try
        {
            
            
            isExist = (String)getSqlMapClientTemplate().queryForObject("recondition.getRCEntry", map);
            rcWOItems = (String)getSqlMapClientTemplate().queryForObject("recondition.getRCSpareAmount", map);

            if(rcWOItems!=null && !rcWOItems.equalsIgnoreCase("0")){
                amount=Float.parseFloat(rcWOItems)/Float.parseFloat(isExist);
            }


            //////System.out.println("rcWO spare amount"+rcWOItems);
        }
        catch(Exception sqlException)
        {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getRcWOItems Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "getRcWOItems", sqlException);
        }
        return String.valueOf(amount);
    }

public ArrayList getWorkorderItems(int wo_id) {
        Map map = new HashMap();
        ArrayList woItems = new ArrayList();
        try {
            map.put("woId", Integer.valueOf(wo_id));
            //////System.out.println("In getWorkorderItems-->"+wo_id);
            woItems = (ArrayList) getSqlMapClientTemplate().queryForList("recondition.getWoItems", map);
            //////System.out.println("Work Order Item Size-->"+woItems.size());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MFR-01", "PurchaseDAO", "MfrList", sqlException);
        }
        return woItems;
    }

public int modifyWOHeader(ReconditionTO recondition) {
        Map map = new HashMap();
        int status = 0;
        try {

            map.put("woId", Integer.valueOf(recondition.getRcWorkId()));
            map.put("woDate", recondition.getCreatedDate());
            map.put("vendorId", Integer.valueOf(recondition.getVendorId()));
            map.put("remarks", recondition.getRemarks());
            status = Integer.valueOf(getSqlMapClientTemplate().update("recondition.modifyWoHeader", map)).intValue();
            //////System.out.println("Work Order Header is Modified-->"+status);
            
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("modifyWOHeader Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MFR-01", "ReconditionDAO", "modifyWOHeader", sqlException);
        }
        return status;
    }


public int insertRCNewItem(ReconditionTO recondition) {
        Map map = new HashMap();
        int status = 0;
        int rcItemId = 0;
        int rcQueueId = 0;
        try {
            //////System.out.println("In Insert rc New Item");
            map.put("rcWorkId", recondition.getRcWorkId());
            map.put("itemId", recondition.getItemId());
            map.put("companyId", Integer.valueOf(recondition.getCompanyId()));
            map.put("userId", recondition.getUserId());
            map.put("createdBy", recondition.getUserId());
            rcItemId = (Integer)getSqlMapClientTemplate().insert("recondition.insertRCNewItem", map);
            //////System.out.println("Rc Item Id In DAO-->"+rcItemId);
            if(rcItemId!=0){
            map.put("rcItemId",rcItemId);
            rcQueueId = (Integer)getSqlMapClientTemplate().insert("recondition.insertRCQueueItem", map);
            map.put("rcQueueId",rcQueueId);
            //////System.out.println("Rc Queue id in DAO-->"+rcQueueId);
            }
            if(rcQueueId!=0)
            {
            status = Integer.valueOf(getSqlMapClientTemplate().update("recondition.insertRCItemDetails", map)).intValue();
            //////System.out.println("Insert in Rc WO Item Details-->"+status);
            }

            

        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("modifyPOAndMprHeader Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MFR-01", "PurchaseDAO", "modifyPOAndMprHeader", sqlException);
        }
        return status;
    }


public int deleteWoItems(ReconditionTO reconditionTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("rcWorkId", Integer.valueOf(reconditionTO.getRcWorkId()));
            map.put("itemId", Integer.valueOf(reconditionTO.getItemId()));
            map.put("rcItemId", Integer.valueOf(reconditionTO.getRcItemId()));
            map.put("rcQueueId", Integer.valueOf(reconditionTO.getRcQueueId()));
            //////System.out.println("In DAO rc Item Id-->"+reconditionTO.getRcItemId());
            //////System.out.println("In DAO rc Queue Id-->"+reconditionTO.getRcQueueId());
            status = Integer.valueOf(getSqlMapClientTemplate().update("recondition.deleteRCItemsMaster", map)).intValue();
            //////System.out.println("Delete From Rc Item Master Status-->"+status);
            status = Integer.valueOf(getSqlMapClientTemplate().update("recondition.deleteRCQueueItems", map)).intValue();
            //////System.out.println("Delete From Rc Queue Status-->"+status);
            status = Integer.valueOf(getSqlMapClientTemplate().update("recondition.deleteRCItemsDetails", map)).intValue();
            //////System.out.println("Delete From Rc Item Details-->"+status);
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("deleteWoItems Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "ReconditionDAO", "deleteWoItems", sqlException);
        }
        return status;
    }

//Rc ZOne

public ArrayList getJobCardList() {
        Map map = new HashMap();
        int status = 0;
        ArrayList jobCardList = new ArrayList();
        try {

            jobCardList = (ArrayList) getSqlMapClientTemplate().queryForList("recondition.getJobCardList", map);
            System.out.println("getJobCardList=" + jobCardList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJobCardList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getJobCardList", sqlException);
        }
        return jobCardList;
    }

public ArrayList getCustomerTypeList() {
        Map map = new HashMap();
        int status = 0;
        ArrayList customerTypeList = new ArrayList();
        try {

            customerTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("recondition.getCustomerTypeList", map);
            //////System.out.println("getCustomerTypeList=" + customerTypeList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerTypeList", sqlException);
        }
        return customerTypeList;
    }
}    
