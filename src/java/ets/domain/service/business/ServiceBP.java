/*---------------------------------------------------------------------------
 * ServiceBP.java
 * Mar 3, 2009
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
-------------------------------------------------------------------------*/
package ets.domain.service.business;

import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.contract.business.ContractTO;
import ets.domain.contract.data.ContractDAO;
import ets.domain.service.data.ServiceDAO;
import java.util.ArrayList;
import java.util.Iterator;

/****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver              Date                      Author                    Change
 * ---------------------------------------------------------------------------
 * 1.0           Mar 3, 2009              vijay			       Created
 *
 ******************************************************************************/
public class ServiceBP {

    private ServiceDAO serviceDAO;
    private ContractDAO contractDAO;

    public ContractDAO getContractDAO() {
        return contractDAO;
    }

    public void setContractDAO(ContractDAO contractDAO) {
        this.contractDAO = contractDAO;
    }

    public void setServiceDAO(ServiceDAO serviceDAO) {
        this.serviceDAO = serviceDAO;
    }

    public ServiceDAO getServiceDAO() {
        return serviceDAO;
    }

    public ArrayList getFreeServiceDetails(int modId, int mfrId) throws FPRuntimeException, FPBusinessException {
        ArrayList freeServiceDetails = new ArrayList();
        freeServiceDetails = serviceDAO.getFreeServiceDetails(modId, mfrId);
        if (freeServiceDetails.size() == 0) {
            // throw new FPBusinessException("EM-SER-01");
        }
        return freeServiceDetails;
    }

    public int saveFreeServiceDetails(ArrayList fServiceList, int userId, int modId, int mfrId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        ServiceTO serviceTO = null;
        Iterator itr = fServiceList.iterator();
        int fServiceId = 0;
        int month = 0;
        int km = 0;
        int hm = 0;
        String desc = "";
        String aStatus = "";
        while (itr.hasNext()) {
            serviceTO = new ServiceTO();
            serviceTO = (ServiceTO) itr.next();
            fServiceId = serviceTO.getFserviceId();
            aStatus = serviceTO.getStatus();
            desc = serviceTO.getDesc();
            km = serviceTO.getKm();
            hm = serviceTO.getHm();
            month = serviceTO.getMonth();
            if (fServiceId == 0) {
                System.out.println("int Insert");

                status = serviceDAO.insertFreeServiceDetails(month, km, hm, desc, modId, mfrId, aStatus);
                if (status == 0) {
                    // throw new FPBusinessException("EM-SER-02");
                }
            } else {
                System.out.println("int update");
                status = serviceDAO.updateFreeServiceDetails(fServiceId, month, km, hm, desc, modId, mfrId, aStatus);
                if (status == 0) {
                    // throw new FPBusinessException("EM-SER-03");
                }
            }

        }


        return status;
    }

    public ArrayList getMaintenanceDetails() throws FPRuntimeException, FPBusinessException {
        ArrayList serviceDetails = new ArrayList();
        serviceDetails = serviceDAO.getMaintenanceDetails();
        if (serviceDetails.size() == 0) {
            // throw new FPBusinessException("EM-SER-04");
        }
        return serviceDetails;
    }

    public int insertMaintance(ServiceTO serviceTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = serviceDAO.insertMaintance(serviceTO);
        if (status == 0) {
            // throw new FPBusinessException("EM-SER-05");
        }
        return status;

    }

    public int updateMaintance(String[] serviceName, String[] serviceId, String[] astatus, String[] desc, String[] index, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;

        ServiceTO serviceTO = null;

        int serId = 0;

        String serName = "";
        String descs = "";
        String aStatus = "";
        /*Iterator itr1 = details.iterator();
        while (itr1.hasNext()) {
        serviceTO = new ServiceTO();
        serviceTO = (ServiceTO) itr1.next();
        System.out.println("hello:" + serviceTO.getServiceId());
        System.out.println("hello:" + serviceTO.getServiceName());
        System.out.println("hello:" + serviceTO.getStatus());
        System.out.println("hello:" + serviceTO.getDesc());

        serviceId = serviceTO.getServiceId();
        aStatus = serviceTO.getStatus();
        desc = serviceTO.getDesc();
        serName = serviceTO.getServiceName();
        status = serviceDAO.updateMaintance(serviceId, serName, desc, aStatus, userId);

        }*/

        int temp = 0;
        ArrayList details = new ArrayList();
        for (int i = 0; i < index.length; i++) {
            System.out.println("hai:" + index[i]);
            temp = Integer.parseInt(index[i]);

            serId = Integer.parseInt(serviceId[temp]);
            serName = serviceName[temp];
            descs = desc[temp];
            aStatus = astatus[temp];

            System.out.println("serviceIds:" + serviceId[temp]);
            status = serviceDAO.updateMaintance(serId, serName, descs, aStatus, userId);
        }
        if (status == 0) {
            // throw new FPBusinessException("EM-SER-06");
        }

        return status;
    }
    public int updateJobCardServiceCheckList(int jobCardId, int serviceId, String[] checkListIds,String[] checkedStatus,
            String[] servicedStatus,String[] remarks, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;

        ServiceTO serviceTO = null;

        int temp = 0;
        int count = serviceDAO.getJobCardServiceCheckListCount(jobCardId, serviceId);
        for (int i = 0; i < checkListIds.length; i++) {
            System.out.println("hai:" + checkListIds[i]);
            status = serviceDAO.updateJobCardServiceCheckList(jobCardId, serviceId, checkListIds[i], checkedStatus[i],
                                            servicedStatus[i],remarks[i], count, userId);
        }

        return status;
    }

    public ArrayList getVehicleTypes() throws FPRuntimeException, FPBusinessException {
        ArrayList serviceDetails = new ArrayList();
        serviceDetails = serviceDAO.getVehicleTypes();
        if (serviceDetails.size() == 0) {
            // throw new FPBusinessException("EM-SER-07");
        }
        return serviceDetails;
    }

    public ArrayList getMfrList() throws FPRuntimeException, FPBusinessException {
        ArrayList serviceDetails = new ArrayList();
        serviceDetails = serviceDAO.getMfrList();
        if (serviceDetails.size() == 0) {
            // throw new FPBusinessException("EM-SER-08");
        }
        return serviceDetails;
    }

    public ArrayList configMaintenanceDetails(ServiceTO serviceTO) throws FPRuntimeException, FPBusinessException {
        ArrayList serviceDetails = new ArrayList();
        ArrayList serviceList = new ArrayList();
        ServiceTO configTO = null;
        ServiceTO newconfigTO = null;

        int status = 0;
        serviceDetails = serviceDAO.checkMaintenanceDetails(serviceTO);
        serviceList = getMaintenanceDetails();
        //if record not exist
        if (serviceDetails.size() == 0) {

            Iterator ser = serviceList.iterator();
            while (ser.hasNext()) {
                configTO = new ServiceTO();
                configTO = (ServiceTO) ser.next();
                if ((configTO.getStatus()).equals("Y")) {
                    System.out.println("inside if");
                    serviceTO.setServiceId(configTO.getServiceId());
                    status = serviceDAO.insertConfigMaintenanceDetails(serviceTO);
                }
            }



        } //if record  exist
        else {

            Iterator ser = serviceList.iterator();

            while (ser.hasNext()) {
                configTO = new ServiceTO();
                configTO = (ServiceTO) ser.next();
                Iterator newSer = serviceDetails.iterator();
                int count = 0;
                while (newSer.hasNext()) {
                    newconfigTO = new ServiceTO();
                    newconfigTO = (ServiceTO) newSer.next();
                    if (configTO.getServiceId() == newconfigTO.getServiceId()) {
                        count++;
                        System.out.println("configTO.getServiceId()" + configTO.getServiceId());
                        System.out.println("newconfigTO.getServiceId()" + newconfigTO.getServiceId());
                        System.out.println("count" + count);
                    }
                }
                //to add new service
                if (count == 0 && (configTO.getStatus()).equals("Y")) {
                    System.out.println("inside else");
                    serviceTO.setServiceId(configTO.getServiceId());
                    status = serviceDAO.insertConfigMaintenanceDetails(serviceTO);
                }

            }
        }


        serviceDetails = serviceDAO.checkMaintenanceDetails(serviceTO);
        return serviceDetails;
    }

    public int updateConfigMaintenance(ServiceTO serviceTO, String[] serviceIds, String[] km, String[] hourMeter, String[] month, String[] labour, String[] index) throws FPRuntimeException, FPBusinessException {
        int status = 0;



        int serId = 0;
        int kms = 0;
        String serName = "";
        String hourMeters = "";
        int months = 0;
        String labo = "";
        int temp = 0;

        ArrayList details = new ArrayList();
        for (int i = 0; i < index.length; i++) {
            System.out.println("hai:" + index[i]);
            temp = Integer.parseInt(index[i]);

            serId = Integer.parseInt(serviceIds[temp]);
            kms = Integer.parseInt(km[temp]);
            months = Integer.parseInt(month[temp]);
            hourMeters = hourMeter[temp];
            labo = labour[temp];
            status = serviceDAO.updateConfigMaintenance(serviceTO, serId, kms, hourMeters, months, labo);
        }
        if (status == 0) {
            // throw new FPBusinessException("EM-SER-09");
        }

        return status;

    }

    public ArrayList getSections() throws FPRuntimeException, FPBusinessException {
        ArrayList sections = new ArrayList();
        sections = serviceDAO.getSections();
        if (sections.size() == 0) {
            // throw new FPBusinessException("EM-SER-14");
        }
        return sections;

    }

    public ArrayList getModels(int mfrId) throws FPRuntimeException, FPBusinessException {
        ArrayList getModels = new ArrayList();
        getModels = serviceDAO.getModels(mfrId);
        if (getModels.size() == 0) {
            // throw new FPBusinessException("EM-SER-14");
        }
        return getModels;

    }

    public ArrayList getProblems(int secId) throws FPRuntimeException, FPBusinessException {
        ArrayList tempList = new ArrayList();
        tempList = serviceDAO.getProblems(secId);
        if (tempList.size() == 0) {
            // throw new FPBusinessException("EM-SER-10");
        }
        return tempList;
    }

    public ArrayList getProblemActivities(int secId, int groupId) throws FPRuntimeException, FPBusinessException {
        ArrayList tempList = new ArrayList();
        tempList = serviceDAO.getProblemActivities(secId, groupId);
        System.out.println("tempList.size()" + tempList.size());
        if (tempList.size() == 0) {
            // throw new FPBusinessException("EM-SER-11");
        }
        return tempList;

    }

    public int saveProblemActivity(ServiceTO serviceTO, String[] activityIds,
            String[] labour, String[] labourTime, String groupVal, String[] activityNames, String[] desc, String[] activeStatus, String[] index) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        int activityId = 0;

        int userId = serviceTO.getUserId();
        int groupId = serviceTO.getGroupId();
        int secId = serviceTO.getSecId();
        String desc1 = "";
        String aStatus = "";
        String activityName = "";
        String labor = "";
        String laborTime = "";
        String ratePerHour = "0.00";
        int temp = 0;
        for (int i = 0; i < index.length; i++) {
            System.out.println("hai:" + index[i]);
            temp = Integer.parseInt(index[i]);

            activityId = Integer.parseInt(activityIds[temp]);
            activityName = activityNames[temp];
            labor = labour[temp];
            laborTime = labourTime[temp];

            if ("2".equals(groupVal)) {
                ratePerHour = "" + (Float.parseFloat(labor) / Float.parseFloat(laborTime));
            }

            desc1 = desc[temp];
            aStatus = activeStatus[temp];
            System.out.println(activityName);

            System.out.println(labor);
            System.out.println(ratePerHour);
            System.out.println(laborTime);


            if (activityId == 0) {
                System.out.println("int Insert");


                status = serviceDAO.insertProblemActivityDetails(labor, laborTime,
                        ratePerHour, activityName, desc1, secId, groupId, aStatus, userId);
                if (status == 0) {
                    // throw new FPBusinessException("EM-SER-12");
                }
            } else {
                System.out.println("int update:labor:" + labor + " time:" + laborTime + " ratePerHour:" + ratePerHour + " activityId:" + activityId);
                status = serviceDAO.updateProblemActivityDetails(labor, laborTime, ratePerHour, activityId, activityName, desc1, groupId, aStatus);
                if (status == 0) {
                    // throw new FPBusinessException("EM-SER-13");
                }
            }
        }



        return status;
    }

    public ArrayList configLabourRate(ServiceTO serviceTO) throws FPRuntimeException, FPBusinessException {
        ArrayList serviceDetails = new ArrayList();
        ArrayList mfrList = new ArrayList();
        ArrayList modList = null;
        ServiceTO configTO = null;
        ServiceTO newconfigTO = null;
        ContractTO modTO = null;

        int status = 0;
        int mfrId = 0;
        int modId = 0;
        String[] temp = null;



        boolean ifExists = false;
        ArrayList mfrActivityList = serviceDAO.fetchMfrActivities(serviceTO);
        Iterator activityItr = mfrActivityList.iterator();
        while (activityItr.hasNext()) {
            configTO = new ServiceTO();
            configTO = (ServiceTO) activityItr.next();
            configTO.setModelId(serviceTO.getModelId());
            ifExists = serviceDAO.checkIfActivityExists(configTO);
            if (!ifExists) {
                //insert
                status = serviceDAO.insertConfigLabourRate(configTO.getModelId(), configTO.getMfrId(), configTO);
            }
        }
        serviceDetails = serviceDAO.checkLabourRates(serviceTO);

        /*
        mfrList=getMfrList();
        //if record not exist
        if(serviceDetails.size()==0){
        Iterator ser=mfrList.iterator();
        while(ser.hasNext()){
        configTO=new ServiceTO();
        configTO=(ServiceTO)ser.next();
        mfrId=configTO.getMfrId();
        System.out.println("mfrId"+mfrId);
        modList=new ArrayList();
        modList=contractDAO.getModList(mfrId);
        System.out.println("modList"+modList.size());
        Iterator mod=modList.iterator();
        while(mod.hasNext()){
        modTO=new ContractTO();
        modTO=(ContractTO)mod.next();
        temp=(modTO.getModelDetails()).split("-");
        modId=Integer.parseInt(temp[0]);
        status=serviceDAO.insertConfigLabourRate(modId,mfrId,serviceTO);
        }
        }

        

        }
        //if record  exist
        else
        {

        Iterator ser=mfrList.iterator();
        while(ser.hasNext()){
        configTO=new ServiceTO();
        configTO=(ServiceTO)ser.next();
        mfrId=configTO.getMfrId();
        System.out.println("mfrId"+mfrId);
        modList=new ArrayList();
        modList=contractDAO.getModList(mfrId);
        System.out.println("modList"+modList.size());
        Iterator mod=modList.iterator();
        while(mod.hasNext()){
        modTO=new ContractTO();
        modTO=(ContractTO)mod.next();
        temp=(modTO.getModelDetails()).split("-");
        System.out.println("tempsafasdf"+temp);
        System.out.println("temp1="+temp[0]);
        System.out.println("temp2="+temp[1]);
        modId=Integer.parseInt(temp[0]);


        Iterator newSer=serviceDetails.iterator();
        int count=0;
        while(newSer.hasNext()){
        newconfigTO=new ServiceTO();
        newconfigTO=(ServiceTO)newSer.next();
        if((mfrId==newconfigTO.getMfrId()) && (modId==newconfigTO.getModId())){
        count++;
        }
        }
        //to add new service
        if(count==0){

        status=serviceDAO.insertConfigLabourRate(modId,mfrId,serviceTO);
        }
        }
        }
        }
        serviceDetails=serviceDAO.checkLabourRates(serviceTO);
         */


        return serviceDetails;
    }

    public int updateLabourRates(ServiceTO serviceTO, String[] activityIds, String[] labourTime, String[] modIds, String[] mfrIds, String[] labour, String[] index) {
        int status = 0;



        int modId = 0;
        int mfrId = 0;
        int activityId = 0;
        String laborTime = "";
        String labo = "";
        int temp = 0;


        for (int i = 0; i < index.length; i++) {
            System.out.println("hai:" + index[i]);
            temp = Integer.parseInt(index[i]);
            modId = Integer.parseInt(modIds[temp]);
            mfrId = Integer.parseInt(mfrIds[temp]);
            activityId = Integer.parseInt(activityIds[temp]);
            laborTime = labourTime[temp];
            labo = labour[temp];
            status = serviceDAO.updateLabourRates(serviceTO, activityId, laborTime, mfrId, modId, labo);
        }

        return status;

    }

    public ArrayList getProblemCauses(int probId) {
        ArrayList serviceDetails = new ArrayList();
        serviceDetails = serviceDAO.getProblemCauses(probId);
        if (serviceDetails.size() == 0) {
            // throw new FPBusinessException("EM-SER-04");
        }
        return serviceDetails;
    }
    public ArrayList getJobCardCheckList(int jobCardId, int serviceId) {
        ArrayList checkList = new ArrayList();
        checkList = serviceDAO.getJobCardCheckList(jobCardId,serviceId);

        return checkList;
    }

    public int saveProblemCauses(ServiceTO serviceTO, String[] activityIds, String[] activityNames, String[] desc, String[] activeStatus, String[] index) {
        int status = 0;
        int activityId = 0;
        int userId = serviceTO.getUserId();
        int probId = serviceTO.getProbId();
        String desc1 = "";
        String aStatus = "";
        String activityName = "";
        int temp = 0;
        for (int i = 0; i < index.length; i++) {
            System.out.println("hai:" + index[i]);
            temp = Integer.parseInt(index[i]);

            activityId = Integer.parseInt(activityIds[temp]);
            activityName = activityNames[temp];
            desc1 = desc[temp];
            aStatus = activeStatus[temp];
            if (activityId == 0) {
                System.out.println("int Insert");

                status = serviceDAO.insertProblemCauseDetails(activityName, desc1, probId, aStatus, userId);
                if (status == 0) {
                    // throw new FPBusinessException("EM-SER-12");
                }
            } else {
                System.out.println("int update");
                status = serviceDAO.updateProblemCauseDetails(activityId, activityName, desc1, probId, aStatus);
                if (status == 0) {
                    // throw new FPBusinessException("EM-SER-13");
                }
            }
        }

        return status;
    }

    public int insertProblemActivityCauses(String activityName, String causeName, int probId, int userId) {
        int status = 0;
        status = serviceDAO.insertProblemActivityCauses(activityName, causeName, probId, userId);
        return status;
    }

    public int insertProblemActivityDetails(String activityName, String desc1, int secId, int probId, String aStatus, int userId) {
        int stat = 0;
        //stat = serviceDAO.insertProblemActivityDetails(activityName,desc1,secId,probId, aStatus,userId);
        return stat;
    }

    public int insertProblemCauseDetails(String activityName, String desc1, int probId, String aStatus, int userId) {
        int stat = 0;
        stat = serviceDAO.insertProblemCauseDetails(activityName, desc1, probId, aStatus, userId);
        return stat;
    }

    public ArrayList getServiceList() {
               ArrayList serviceDetails = new ArrayList();
            serviceDetails = serviceDAO.getServiceList();

            return serviceDetails;
        }
            public ArrayList getServiceCheckList(String serviceId) {
               ArrayList serviceDetails = new ArrayList();
            serviceDetails = serviceDAO.getServiceCheckList(serviceId);

            return serviceDetails;
        }
             public int insertServiceCheckListDetails(String serviceId, String checkList,int userId){
            int stat = 0;
            stat = serviceDAO.insertServiceCheckListDetails(serviceId,checkList,userId);
            return stat;
    }

}
