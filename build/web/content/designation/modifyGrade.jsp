

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
 
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bus</title>
<link href="/bus/css/admin1.css" rel="stylesheet" type="text/css">
<script language="javascript">

function submitpage(value)
        {              
document.modify.action='/bus/modifyGrade.do';
document.modify.submit();
}

function setSelectbox(i)
{
var selected=document.getElementsByName("selectedIndex") ;
selected[i].checked = 1;
}    

</script>

</head>

<!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/ 
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->

<body>
<form  method="post" name="modify"> 
<!-- copy there from end -->
<div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
<div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
<!-- pointer table -->
<table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
<tr>
<td >
<%@ include file="/content/common/path.jsp" %>
</td></tr></table>
<!-- pointer table -->
<!-- title table -->
<table width="875" cellpadding="0" cellspacing="0" align="center" border="0">
<tr>
<td height="20"><%@ include file="/content/common/pageTitle.jsp" %></td>
</tr>
</table>
<!-- title table -->
</div>
</div>
<br>
<br>
<!-- message table -->
<table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
<tr>
<td >
<%@ include file="/content/common/message.jsp" %>
</td></tr></table>
<!-- message table -->
<!-- copy there  end -->
 <%
    int index = 0;
ArrayList GradeList = (ArrayList) session.getAttribute("GradeList");
%>        
<br>
<table width="300" align="center" id="bg" cellpadding="0" cellspacing="0">

<tr>
<td class="contentsub" height="30">Grade Id</td> 
<td class="contentsub" height="30">Grade Name </td> 
<td class="contentsub" height="30">Description</td>
<td class="contentsub" height="30">Status</td>
<td class="contentsub" height="30">CheckBox</td>
</tr>
<c:if test = "${GradeList != null}" >
<c:forEach items="${GradeList}" var="grade"> 		
<tr>
<td class="text1" height="30"><input type="hidden" name="gradeIds" value='<c:out value="${grade.gradeId}"/>'> <div align="center"><c:out value="${Desig.deptId}"/></div> </td>                                           
<td class="text1" height="30"><input type="text" class="textbox" name="gradeNames" value="<c:out value="${grade.gradeName}"/>"></td>
<td class="text1" height="30"><input type="text" class="textbox" name="gradeDescs" value="<c:out value="${grade.description}"/>"></td>
<td height="30" class="text1"> <div align="center"><select name="gradeactiveInds" class="textbox" onchange="">
<c:choose>
<c:when test="${grade.status == 'Y'}">
<option value="Y" selected>Active</option>
<option value="N">InActive</option>
</c:when>
<c:otherwise>
<option value="Y">Active</option>
<option value="N" selected>InActive</option>
</c:otherwise>
</c:choose>
</select>
</div> 
</td>
<td width="77" height="30" class="text1"><input type="checkbox" name="selectedGrades" value='<%= index %>'></td>
</tr>
<%
index++;
%>
</c:forEach >
</c:if> 
</table>
<center>
<br>
<input type="button" name="save" value="Save" onClick="submitpage(this.name)" class="button" />
<input type="hidden" name="reqfor" value="" />
</center>
<br>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
