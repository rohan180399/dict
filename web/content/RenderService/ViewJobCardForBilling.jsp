<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <%@page language="java" contentType="text/html; charset=UTF-8"%>
        <%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
        <%@page import="java.util.Locale"%>

<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
            
    
    <script language="javascript">

function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}

        function submitPage(value)
        {
        if(value == 'search' || value == 'Prev' || value == 'Next' || value == 'GoTo' || value =='First' || value =='Last'){
                        if(value=='GoTo'){
                            var temp=document.jobCardCloseView.GoTo.value;       
                            document.jobCardCloseView.pageNo.value=temp;
                            document.jobCardCloseView.button.value=value;
                            document.jobCardCloseView.action = '/throttle/ViewJobCardsForBilling.do';   
                            document.jobCardCloseView.submit();
                        }else if(value == "First"){
                        temp ="1";
                        document.jobCardCloseView.pageNo.value = temp; 
                        value='GoTo';
                    }else if(value == "Last"){
                    temp =document.jobCardCloseView.last.value;
                    document.jobCardCloseView.pageNo.value = temp; 
                    value='GoTo';
                }
                /*
                 if(document.jobCardCloseView.jobcardId.value != ""){
                 **/
                document.jobCardCloseView.button.value=value;
                document.jobCardCloseView.action = '/throttle/ViewJobCardsForBilling.do';   
                document.jobCardCloseView.submit();
                /*
                 } else {
                 alert("Job Dard No is not Filled");
                }
                 **/
            }
        }
        
        function setValues(){
            if( '<%= request.getAttribute("regNo") %>' != 'null' ){
                document.jobCardCloseView.regNo.value = '<%= request.getAttribute("regNo") %>';
            }    
            if( '<%= request.getAttribute("jcId") %>' != 'null' ){
                document.jobCardCloseView.jobcardId.value = '<%= request.getAttribute("jcId") %>';
            }    
            if( '<%= request.getAttribute("compId") %>' != 'null' ){
                document.jobCardCloseView.serviceLocation.value = '<%= request.getAttribute("compId") %>';
            }    
            
         }          
function getVehicleNos(){
    //onkeypress='getList(sno,this.id)'         
    var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
}         
        
    </script>
<div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="service.label.SearchJobCardForBilling"  text="SearchJobCardForBilling"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="general.label.service"  text="service"/></a></li>
          <li class="active"><spring:message code="service.label.SearchJobCardForBilling"  text="SearchJobCardForBilling"/></li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">
        <c:if test="${jcList != null}">
        <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
        </c:if>
        <c:if test="${jcList == null}">
        <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
        </c:if>
        <form name="jobCardCloseView">
            
         <%@ include file="/content/common/message.jsp" %>
                   
<%          
            int index = 0;      
            int pag = (Integer)request.getAttribute("pageNo");
            index = ((pag -1) *10) +1 ;
%>  

    
   <table  class="table table-info mb30 table-hover">
       <thead> 
           <tr>
               
                <th colspan="7"><spring:message code="service.label.SearchJobCardForBilling" text="default text"/> </th>
           </tr>
            </thead>

    <tr>
         <td align="left" height="30"><font color="red">*</font><spring:message code="service.label.RegNo"  text="default text"/></td>
            <td height="30"><input type="text" style="width:260px;height:40px;"  class="form-control" id="regno" name="regNo" value="" /></td>
            <td align="left" height="30"><spring:message code="service.label.JobCardNo"  text="default text"/></td>
            <td><input type="text" style="width:260px;height:40px;"  class="form-control" name="jobcardId" value="" /> </td>
            <td><spring:message code="service.label.ServicePoint"  text="default text"/></td>
            <td> <select style="width:260px;height:40px;"  class="form-control" name="serviceLocation"  >
                <option selected   value=0>---<spring:message code="service.label.Select"  text="default text"/>---</option>
                <c:if test = "${servicePoints != null}" >
                <c:forEach items="${servicePoints}" var="mfr">
                <option  value='<c:out value="${mfr.compId}" />'>
                <c:out value="${mfr.compName}" />
                </c:forEach >
                </c:if>
                </select>
            </td>
        <td><input type="button" class="btn btn-success" name="search" value="<spring:message code="service.label.SEARCH"  text="default text"/>" onClick="submitPage(this.name);"  />  
        </td>
<!--        <td></td>
    <td></td>-->
    </tr>
    </table>

    
            <c:if test = "${jcList != null}" >
		<table class="table table-info mb30 table-hover" >
                <thead>
		
		<tr>
		<th><spring:message code="service.label.SNo"  text="default text"/></th>
		<th><spring:message code="service.label.JobCardNo"  text="default text"/></th>
		<th><spring:message code="service.label.VehicleNo"  text="default text"/></th>
		<th><spring:message code="service.label.ServicePoint"  text="default text"/></th>
		<th><spring:message code="service.label.DeliveryTime"  text="default text"/></th>
		<th><spring:message code="service.label.ClosedBy"  text="default text"/></th>
		<th><spring:message code="service.label.ClosedOn"  text="default text"/></th>
		<th><spring:message code="service.label.Status"  text="default text"/></th>
		<th>&nbsp;</th>
		</tr>
                </thead>
                    <%
 
                    %>
                    
                    <c:forEach items="${jcList}" var="list"> 	
                        <%

            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                %>
                <tr height="30">
                    <td><%=index %></td>
                    <td><c:out value="${list.jcMYFormatNo}"/></td>
                    <td><c:out value="${list.vehicleNo}"/></td>
                    <td><c:out value="${list.companyName}"/></td>
                    <td><c:out value="${list.jobCardPCD}"/></td>
                    <td><c:out value="${list.closedBy}"/></td>
                    <td><c:out value="${list.closedOn}"/></td>
                    <td><c:out value="${list.status}"/></td>
                    <td>
                    <c:if test = "${list.serviceVendor == 0}" >
                        <span class="label label-info"><a href='/throttle/closedJobCardBillDetails.do?jobCardId=<c:out value="${list.jobCardId}"/>&vehicleId=<c:out value="${list.vehicleId}"/>' > <font color="white"><spring:message code="service.label.Bill"  text="default text"/></font></a></span></td>
<!--                    <a href='/throttle/closedExtJobCardBillDetails.do?jobCardId=<c:out value="${list.jobCardId}"/>&vehicleId=<c:out value="${list.vehicleId}"/>' >Bill</a></td>-->
                    </c:if>
                    <c:if test = "${list.serviceVendor != 0}" >
                        <span class="label label-info"><a href='/throttle/closedExtJobCardBillDetails.do?jobCardId=<c:out value="${list.jobCardId}"/>&vehicleId=<c:out value="${list.vehicleId}"/>' > <font color="white"><spring:message code="service.label.Bill"  text="default text"/></font></a></span></td>
                    </c:if>
                </tr>
                <%
                index++;
                %>
                </c:forEach>
                </table>
            </c:if> 
            
            <br>
            
            <table align="center" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <%@ include file="/content/common/pagination.jsp"%>  
                    </td>                    
                </tr>
            </table>             
                <input type="hidden" name="reqfor" value="">
            <br>
            
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>


    </div>
    </div>





<%@ include file="/content/common/NewDesign/settings.jsp" %>