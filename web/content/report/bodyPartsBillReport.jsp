
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
    </head>
    <script language="javascript">
function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}
 function receiveContract(woId,jcId){
        window.open('/throttle/searchBWBill.do?mrsJobCardNumber='+woId+'&jcId='+jcId , 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
    } 
    function getWoDetail(woId){

window.open('/throttle/woDetail.do?woId='+woId, 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');


}
     function details(indx){             
          var jobCardIds=document.getElementsByName("jcNo");          
          var vendorId=document.getElementsByName("vendorIds");          
        var url = '/throttle/bodyBillDetails.do?jobCardId='+jobCardIds[indx].value+'&vendorId='+vendorId[indx].value;          
           window.open(url , 'PopupPage', 'height=450,width=650,scrollbars=yes,resizable=yes');    
         
         }
        
        
        function submitWindow(){
            
            var chek=validation();
            if(chek=='true'){                              
                document.bodyPartsReport.action = '/throttle/bodyBill.do';
                document.bodyPartsReport.submit();
            }
        }
        function validation(){
            if(document.bodyPartsReport.companyId.value==0){
                alert("Please Select Data for Company Name");
                document.bodyPartsReport.companyId.focus();
                return 'false';
            }           
            else  if(textValidation(document.bodyPartsReport.fromDate,'From Date')){
                return 'false';
            }
            else  if(textValidation(document.bodyPartsReport.toDate,'TO Date')){
                return'false';
            }
            return 'true';
        }
        
        function setValues(){           
            if('<%=request.getAttribute("companyId")%>'!='null'){
                document.bodyPartsReport.companyId.value='<%=request.getAttribute("companyId")%>';               
                document.bodyPartsReport.fromDate.value='<%=request.getAttribute("fromDate")%>';
                document.bodyPartsReport.toDate.value='<%=request.getAttribute("toDate")%>';
            }if('<%=request.getAttribute("vendorId")%>'!='null'){
             document.bodyPartsReport.vendorId.value='<%=request.getAttribute("vendorId")%>';
             }
            if('<%=request.getAttribute("woId")%>'!='null'){
                document.bodyPartsReport.woId.value='<%=request.getAttribute("woId")%>';
            }
            if('<%=request.getAttribute("regNo")%>'!='null'){
                document.bodyPartsReport.regNo.value='<%=request.getAttribute("regNo")%>';
            }
           
           
        }
        function getVehicleNos(){
            
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
        }   
    </script>
    <body onload="setValues();getVehicleNos();">
        <form name="bodyPartsReport" method="post">
            <%@ include file="/content/common/path.jsp" %>           
            <%@ include file="/content/common/message.jsp" %>

 <table width="900" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:900;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">Contractors Bill Report</li>
    </ul>
    <div id="first">
    <table width="900" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
    <tr>
            <td align="left" height="30">Vendor</td>
            <td><select name="vendorId" class="form-control" style="width:125px">
                            <option value="0">-select-</option>
                            <c:if test = "${VendorLists != null}" >
                                <c:forEach items="${VendorLists}" var="vendor">
                                    <option value="<c:out value="${vendor.vendorId}"/>"><c:out value="${vendor.vendorName}"/></option>
                                </c:forEach>
                            </c:if>
                    </select> </td>
            <td align="left" height="30">Work Order Number</td>
            <td height="30"><input type="text" name="woId" class="form-control" value=""></td>

            <td align="left" height="30"><font color="red">*</font>Location</td>
            <td height="30"><select class="form-control" name="companyId" style="width:125px">
                            <option value="0">-select-</option>
                            <c:if test = "${LocationLists != null}" >
                             <c:forEach items="${LocationLists}" var="company">
                             <c:choose>
                                <c:when test="${company.companyTypeId==1012}" >

                                    <option value="<c:out value="${company.cmpId}"/>"><c:out value="${company.name}"/></option>
                         </c:when>
                     </c:choose>
                     </c:forEach>
                            </c:if>    
                    </select></td>
                    <td>&nbsp;</td>
             </tr><tr>
            <td align="left" height="30">Vehicle Number</td>
            <td height="30"><input name="regNo" id="regno" type="text" class="form-control"  size="20"></td>
   
         <td width="80" height="30"><font color="red">*</font>From Date</td>
                    <td width="182"><input name="fromDate" type="text" class="form-control" value="" size="20">
                    <span><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.bodyPartsReport.fromDate,'dd-mm-yyyy',this)"/></span></td>
                    <td width="148"><font color="red">*</font>To Date</td>
                    <td width="162" height="30"><input name="toDate" type="text" class="form-control" value="" size="20">
                    <span><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.bodyPartsReport.toDate,'dd-mm-yyyy',this)"/></span></td>
        <td><input type="button"  value="Fetch Data" class="button" name="Fetch Data" onClick="submitWindow()">
                <input type="hidden" value="" name="reqfor"> </td>
    </tr>
    </table>
    </div></div>
    </td>
    </tr>
    </table>

             <% int index = 0;%> 
              <%int oddEven=0;%>
              <%  String classText = "";%>         
 <c:if test = "${BodyBillList != null}" > 
            <table width="650" border="0" align="center" cellpadding="0" cellsspacing="0" id="bg" class="border">
                    <tr class="contenthead">
                        <td  width="65" height="30" class="contentsub">WO Date</td>
                        <td  width="42" height="30" class="contentsub">WO No</td>
                        <td  width="42" height="30" class="contentsub">JC No</td>                                                                                                   
                          <td  width="58" height="30" class="contentsub">Vehicle No</td>   
                        <td  width="55" height="30" class="contentsub">Vendor</td>
                        <td  width="51" height="30" class="contentsub">Bill Date</td>                            
                        <td  width="65" height="30" class="contentsub">Bill Amount</td>
                        <td  width="80" height="30" class="contentsub">Jobcard Status</td>
                    </tr> 
                    <c:set var="totalAmount"  value="0"/>
        <c:forEach items="${BodyBillList}" var="bodyBill">            
                      <%                              
             oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                
                            <tr> <td class="<%=classText%>" width="51" height="30"><c:out value="${bodyBill.createdDate}" /></td>
                                <td class="<%=classText%>" width="31" height="30"><input type="hidden" name="woNo" value="<c:out value="${bodyBill.woId}" />">
                     <a href="" onclick="getWoDetail('<c:out value="${bodyBill.woId}"/>')">S<c:out value="${bodyBill.woId}" /></a></td>
                            <td class="<%=classText%>" width="41" height="30"><input type="hidden" name="jcNo" value="<c:out value="${bodyBill.jobCardId}" />"><c:out value="${bodyBill.jobCardId}" /></td>                            
                             <td class="<%=classText%>" width="69" height="30"><c:out value="${bodyBill.regNo}" /></td> 
                            <td class="<%=classText%>" width="91" height="30"><c:out value="${bodyBill.vendorName}" /></td>
                            <td class="<%=classText%>" width="51" height="30"><c:out value="${bodyBill.billDate}" /></td>
                            <c:if test = "${bodyBill.totalAmount !='Bill Not received'}" >
                            <td class="<%=classText%>" width="90" height="30"><a href="" onclick="details(<%=index%>);"><c:out value="${bodyBill.totalAmount}" /></a></td>
                             </c:if>
                            <c:if test = "${bodyBill.totalAmount =='Bill Not received'}" >

                            <td class="<%=classText%>" width="90" height="30"><a href="" onClick="receiveContract('<c:out value="${bodyBill.woId}"/>', '<c:out value="${bodyBill.jobCardId}"/>');"  ><c:out value="${bodyBill.totalAmount}" /></a></td>

                             </c:if>
                        <input type="hidden" name="vendorIds" value="<c:out value='${bodyBill.vendorId}' />">
                        <c:if test = "${bodyBill.totalAmount !='Bill Not received'}" >
                        <c:set var="totalAmount" value="${totalAmount + bodyBill.totalAmount}" />
                        </c:if>
                        <td class="<%=classText%>" width="51" height="30"><c:out value="${bodyBill.lastStatus}" /></td>
                       </tr>  
                    <% index++;%>
                  </c:forEach>      
                    <tr>
                     
                        <td width="51" class="text2" height="30">&nbsp;</td>
                        <td width="51" class="text2" height="30">&nbsp;</td>
                        <td width="61" class="text2" height="30"><b>TotalAmount</b></td>                       
                        <td width="69" class="text2" height="30">                            
                            <fmt:setLocale value="en_US" /><b>SAR: <fmt:formatNumber value="${totalAmount}" pattern="##.00"/></b></td>                          
                        <td width="51" class="text2" height="30">&nbsp;</td>
                        <td width="51" class="text2" height="30">&nbsp;</td>                          
                    </tr>  
                </table>
            </c:if>           
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    
</html>
