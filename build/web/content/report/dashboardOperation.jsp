<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>


<!--<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>-->

<!--<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>-->

<!-- Data from www.netmarketshare.com. Select Browsers => Desktop share by version. Download as tsv. -->
<pre id="tsv" style="display:none"></pre>




<div class="pageheader">
    <h2><i class="fa fa-home"></i>Dashboard
        <!--<span>Operations</span>-->
    </h2>
    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li>Dashboard</li>
            <!--<li class="active">Operations</li>-->
        </ol>
    </div>
</div>

<div class="contentpanel">

    <div class="row">

        <div class="col-sm-6 col-md-3">
            <div class="panel panel-success panel-stat">
                <div class="panel-heading">

                    <div class="stat">
                        <div class="row">
                            <div class="col-xs-4">

                                <i class="ion ion-android-bus"></i>
                            </div>
                            <div class="col-xs-8">
                                <!--                    <small class="stat-label">Truck Utilisation</small>-->
                                <h4> <spring:message code="dashboard.label.Trucks"  text="Default"/></h4>

                                <h1><span id="totalTruckNosSpan"></span></h1>
                            </div>
                        </div><!-- row -->

                        <div class="mb15"></div>

                    </div><!-- stat -->

                </div><!-- panel-heading -->
            </div><!-- panel -->
        </div><!-- col-sm-6 -->

<!--        <div class="col-sm-6 col-md-3">
            <div class="panel panel-primary panel-stat">
                <div class="panel-heading">

                    <div class="stat">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="ion ion-android-subway"></i>
                            </div>
                            <div class="col-xs-8">
                                <h4> <spring:message code="dashboard.label.Trailers"  text="Default"/></h4>
                                <h1><span id="totalTrailersNosSpan"></span></h1>
                            </div>
                        </div> row 

                        <div class="mb15"></div>



                    </div> stat 

                </div> panel-heading 
            </div> panel 
        </div> col-sm-6 -->

        <div class="col-sm-6 col-md-3">
            <div class="panel panel-danger panel-stat">
                <div class="panel-heading">

                    <div class="stat">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="ion ion-person-stalker"></i>
                            </div>
                            <div class="col-xs-8">
                                <h4> <spring:message code="dashboard.label.Drivers"  text="Default"/></h4>
                                <h1><span id="totalDriversNosSpan"></span></h1>
                            </div>
                        </div><!-- row -->

                        <div class="mb15"></div>

                    </div><!-- stat -->

                </div><!-- panel-heading -->
            </div><!-- panel -->
        </div><!-- col-sm-6 -->

        <div class="col-sm-6 col-md-3">
            <div class="panel panel-warning panel-stat">
                <div class="panel-heading">

                    <div class="stat">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="ion ion-happy"></i>
                            </div>
                            <div class="col-xs-8">
                                <h4> <spring:message code="dashboard.label.Customers"  text="Default"/></h4>
                                <h1><span id="totalMonthlyLeaseCustomerNosSpan"></span></h1>
                            </div>
                        </div><!-- row -->

                        <div class="mb15"></div>



                    </div><!-- stat -->

                </div><!-- panel-heading -->
            </div><!-- panel -->
        </div><!-- col-sm-6 -->

        <%--
        <div class="col-sm-6 col-md-3">
            <div class="panel panel-warning panel-stat">
                <div class="panel-heading">

                    <div class="stat">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="ion ion-alert-circled"></i>
                            </div>
                            <div class="col-xs-8">
                                <h4>DailyLeaseCustomer</h4>
                                <h1><span id="totalDailyLeaseCustomerNosSpan"></span></h1>
                            </div>
                        </div><!-- row -->

                        <div class="mb15"></div>



                    </div><!-- stat -->

                </div><!-- panel-heading -->
            </div><!-- panel -->
        </div><!-- col-sm-6 -->

        --%>
    </div><!-- row -->


    <table>
  
        <tr>
            
 <td >
                <div class="row" >
                    <div class="col-sm-8 col-md-9">
                        <div class="panel panel-default">
                            <div class="outer1">
                                <div class="panel-btns">
                                    <a href="" class="panel-close">&times;</a>
                                    <a href="" class="minimize">&minus;</a>
                                </div>
                                <h5 class="panel-title"><font color="white">Pending E-Invoice Counts</font></h5>
                                <div id="truckTypen" style="min-width: 550px; height: 400px; margin: 0 auto"></div>
                            </div>

<table align="center">
                                    <tr>
                                        <td>&nbsp;&nbsp;<center><span class="label label-success"><font size="2"><a href="#" onclick="viewStatusReport1()" style="text-decoration: none;color:white;"> E-Invoices</a></font></span></center></td>
                                    <td>&nbsp;&nbsp;</td>
                                    <td>&nbsp;&nbsp;<center><span class="label label-warning"><font size="2"><a href="#" onclick="viewStatusReport2()" style="text-decoration: none;color:white;"> Supp. E-Invoices</a></font></span></center></td>
                                    <td>&nbsp;&nbsp;</td>
                                    <td>&nbsp;&nbsp;<center><span class="label label-Primary"><font size="2"><a href="#" onclick="viewStatusReport3()" style="text-decoration: none;color:white;">Credit E-Invoices</a></font></span></center></td>
                                    <td>&nbsp;&nbsp;</td>
                                    <td>&nbsp;&nbsp;<center><span class="label label-danger"><font size="2"><a href="#" onclick="viewStatusReport4()" style="text-decoration: none;color:white;">Supp. Credit  E-Invoices</a></font></span></center></td>
                                    </tr>
                                </table>
                        </div>

                    </div>
                </div>
        </td>
        
 <td >
<!--//here-->
 <div class="row">
      <div class="col-sm-8 col-md-9">
                        <div class="panel panel-default">
                            <div class="outer1">
            <!--                    <div class="panel-btns">
                                    <a href="" class="panel-close">&times;</a>
                                    <a href="" class="minimize">&minus;</a>
                                </div> panel-btns -->
            <!--<h5 class="panel-title">Fleet Utilisation</h5>-->
             <div class="panel-btns">
                                    <a href="" class="panel-close">&times;</a>
                                    <a href="" class="minimize">&minus;</a>
                                </div>
                                <h5 class="panel-title"><font color="white">Pending E-Mail Count</font></h5>
                                <div id="barchartNxt" style="min-width: 550px; height: 400px; margin: 0 auto">
                                    
                                </div>
<table align="center">
                                    <tr>
                                    <td>&nbsp;&nbsp;<center><span class="label label-warning"><font size="2"><a href="#" onclick="viewInvocieReport2()" style="text-decoration: none;color:white;"> Invoices Report</a></font></span></center></td>
                                    <td>&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;<center><span class="label label-success"><font size="2"><a href="#" onclick="viewInvocieReport1()" style="text-decoration: none;color:white;"> supplement-Invoices Report</a></font></span></center></td>
                                    </tr>
                                </table>
        </div>
                      <table align="center">
                                    <tr>
                                        <td style="display:none">&nbsp;&nbsp;<center><span class="label label-success"><font size="2"><a href="#" onclick="viewStatusReport1()" style="text-decoration: none;color:white;"></a></font></span></center></td>
                                    <td>&nbsp;&nbsp;</td>
                                    <td style="display:none">&nbsp;&nbsp;<center><span class="label label-warning"><font size="2"><a href="#" onclick="viewStatusReport2()" style="text-decoration: none;color:white;"> </a></font></span></center></td>
                                    <td>&nbsp;&nbsp;</td>
                                    <td style="display:none">&nbsp;&nbsp;<center><span class="label label-Primary"><font size="2"><a href="#" onclick="viewStatusReport3()" style="text-decoration: none;color:white;"></a></font></span></center></td>
                                    <td>&nbsp;&nbsp;</td>
                                    <td style="display:none">&nbsp;&nbsp;<center><span class="label label-danger"><font size="2"><a href="#" onclick="viewStatusReport4()" style="text-decoration: none;color:white;"></a></font></span></center></td>
                                    </tr>
                                </table> 
                         
        </div>
        </div>
        </div>
        </td>
         

        </tr>

      <tr>
            <td>
                <div class="row" >
                    <div class="col-sm-8 col-md-9">
                        <div class="panel panel-default">

                            <div class="outer">
                                <div class="panel-btns">
                                    <a href="" class="panel-close">&times;</a>
                                    <a href="" class="minimize">&minus;</a>
                                </div><!-- panel-btns -->
                                <h5  class="panel-title"> <spring:message code="dashboard.label.TruckMake"  text="Default"/></h5>
                                <div  id="truckMake" style="min-width: 500px; height: 400px; margin: 0 auto"></div>
                            </div>
                            <!--            <div class="panel-body">


                                            <div class="col-sm-8">


                                                  <div id="containerFleetUtilisation" style="min-width: 600px; height: 400px; margin: 0 auto"></div>

                                            </div>



                                      </div>-->
                        </div>
                    </div>
            </td>
<!--            <td>
                <div class="col-sm-8 col-md-9">
                    <div class="panel panel-default">

                        <div class="outer1">
                            <div class="panel-btns">
                                <a href="" class="panel-close">&times;</a>
                                <a href="" class="minimize">&minus;</a>
                            </div> panel-btns 
                            <h5 class="panel-title"><spring:message code="dashboard.label.TrailerMake"  text="Trailer"/></h5>
                            <div id="trailerMake" style="min-width: 500px; height: 400px; margin: 0 auto"></div>
                        </div>
                                    <div class="panel-body">


                                        <div class="col-sm-8">

                                        </div>
                                  </div>
                    </div>
                </div>
            </td>-->
<!--        </tr>
        <tr>-->
            <td>
                <div class="row" >
                    <div class="col-sm-8 col-md-9">
                        <div class="panel panel-default">

                            <div class="outer2">
                                <div class="panel-btns">
                                    <a href="" class="panel-close">&times;</a>
                                    <a href="" class="minimize">&minus;</a>
                                </div><!-- panel-btns -->
                                <h5 class="panel-title"><spring:message code="dashboard.label.TruckType"  text="Default"/></h5>
                                <div id="truckType" style="min-width: 500px; height: 400px; margin: 0 auto"></div>
                            </div>
                            <!--            <div class="panel-body">


                                            <div class="col-sm-8">




                                            </div>



                                      </div>-->
                        </div>
                    </div>
            </td>
<!--            <td>
                <div class="col-sm-8 col-md-9">
                    <div class="panel panel-default">

                        <div class="outer3">
                            <div class="panel-btns">
                                <a href="" class="panel-close">&times;</a>
                                <a href="" class="minimize">&minus;</a>
                            </div> panel-btns 
                            <h5 class="panel-title"><spring:message code="dashboard.label.TrailerType"  text="Default"/></h5>
                            <div id="trailerType" style="min-width: 500px; height: 400px; margin: 0 auto"></div>
                        </div>
                                    <div class="panel-body">


                                        <div class="col-sm-8">

                                        </div>
                                  </div>
                    </div>
                </div>
            </td>-->
        </tr>

    </table>
    <table>

        <!--<div class="col-sm-8 col-md-9">-->
        <!--<div class="panel panel-default">-->

      
        <!--            <div class="panel-body">


                        <div class="col-sm-8">

                        </div>
                  </div>-->
        <!--</div>-->
        <!--</div>-->
    </table>
                            <br>
                            <br>
                            <br>


                            
                             <table>

        <!--<div class="col-sm-8 col-md-9">-->
        <!--<div class="panel panel-default">-->

       
        
<!--                        <div class="row" >
                    <div class="col-sm-8 col-md-9">
                        <div class="panel panel-default">
                            <div class="outer1">
                                <div class="panel-btns">
                                    <a href="" class="panel-close">&times;</a>
                                    <a href="" class="minimize">&minus;</a>
                                </div>
                                <div class="panel-btns">
                                    <a href="" class="panel-close">&times;</a>
                                    <a href="" class="minimize">&minus;</a>
                                </div> panel-btns 
            <h5 class="panel-title">Fleet Utilisation</h5>
            <div id="barchart" style="min-width: 550px; height: 400px; margin: 0 auto"></div>
<table align="center">
                                    <tr>
                                        <td></td>
                                    <td>&nbsp;&nbsp;</td>
                                    </tr>
                                </table>

                    </div>
                </div>-->
        <!--            <div class="panel-body">


                        <div class="col-sm-8">

                        </div>
                  </div>-->
        <!--</div>-->
        <!--</div>-->
    </table>




    <style>
        .outer {
            width: 600px;
            color: navy;
            background-color: #1caf9a;
            border: 2px solid darkblue;
            padding: 5px;
        }
        .outer1 {
            width: 600px;
            color: navy;
            background-color: #428bca;
            border: 2px solid darkblue;
            padding: 5px;
        }
        .outer2 {
            width: 600px;
            color: navy;
            background-color: #a94442;
            border: 2px solid darkblue;
            padding: 5px;
        }
        .outer3 {
            width: 600px;
            color: navy;
            background-color: #f0ad4e;
            border: 2px solid darkblue;
            padding: 5px;
        }
        .outer4 {
            width: 1210px;
            color: navy;
            background-color: pink;
            border: 2px solid darkblue;
            padding: 5px;
        }
        .ben{
            width: 600px;
        }
         .outer5{
            width: 600px;
            color: navy;
            background-color: pink;
            border: 2px solid darkblue;
            padding: 5px;
        }
    </style>
    <!-- /.box -->




</div><!-- contentpanel -->





</div><!-- mainpanel -->



<%@ include file="/content/common/NewDesign/settings.jsp" %>


<script>
    
      function viewInvocieReport1() {
        window.open('/throttle/getsupplementInvoiceReport.do?param=Search', 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
      function viewInvocieReport2() {
        window.open('/throttle/getMainInvoiceReport.do?param=Search', 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
      function viewStatusReport1() {
        window.open('/throttle/getFinanceEInvoiceValues.do?param=Search', 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
      function viewStatusReport2() {
        window.open('/throttle/getSuppFinanceEInvoiceValues.do?param=Search', 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
      function viewStatusReport3() {
        window.open('/throttle/getCreditFinanceEInvoiceValues.do?param=Search', 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
      function viewStatusReport4() {
        window.open('/throttle/getSuppCreditFinanceEInvoiceValues.do?param=Search', 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    loadMonthlyTripStatus();
    function loadMonthlyTripStatus() {
        var vehicleMakeArray = [];
        var vehicleCountArray = [];
        $.ajax({
            url: '/throttle/vehicleMake.do',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                        vehicleMakeArray.push(value.Make);
                        vehicleCountArray.push([parseInt(value.Count)]);
                });
                $('#barchart').highcharts({
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Truck Make'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        type: 'category',
                        categories: vehicleMakeArray,
                        labels: {
                            rotation: -45,
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'No Of Vehicles'
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: 'Truck : <b>{point.y:f} Nos</b>'
                    },
                    series: [{
                            name: 'Truck Make',
                            data: vehicleCountArray,
                            dataLabels: {
                                enabled: true,
                                rotation: -90,
                                color: '#FFFFFF',
                                align: 'right',
                                format: '{point.y:f}', // one decimal
                                y: 10, // 10 pixels down from the top
                                style: {
                                    fontSize: '13px',
                                    fontFamily: 'Verdana, sans-serif'
                                }
                            }
                        }]
                });
            }
        });
    }
</script>


<script>
    loadTrucknos();
    function loadTrucknos() {
        $.ajax({
            url: '/throttle/getDashBoardTruckNos.do',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                    if(value.Name == 'Make'){
                      $("#totalTruckNosSpan").text(value.Count);
                    }else if(value.Name == 'TrailerMaster'){
                      $("#totalTrailersNosSpan").text(value.Count);
                    }else if(value.Name == 'EmpMaster'){
                      $("#totalDriversNosSpan").text(value.Count);
                    }
                    else if(value.Name == 'CustomerMaster'){
                      $("#totalMonthlyLeaseCustomerNosSpan").text(value.Count);
                    }



                });
//                        vehicleMakeArray.push(value.Make);
//                        vehicleCountArray.push([parseInt(value.Count)]);
                }
            });
        }
        </script>


  <script>

    truckMake();
    function truckMake() {
        var x_values = [];
        var x_values_sub = {};
        var cntr = 0;
        $.ajax({
            url: '/throttle/vehicleMake.do',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                        x_values_sub['name'] = value.Make;
                        x_values_sub['y'] = parseInt(value.Count);
                        x_values.push(x_values_sub);
                        x_values_sub = {};
//                    }
                });
//                alert(vehicleMakeArray[1])
                $('#truckMake').highcharts({
                    chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },

                    title: {
                text: ''
            },

                    tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },

             plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y} ',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
                }
            },


                    series: [{
                    type: 'pie',
                    name: 'Nos',
                    data: x_values
                }]
                });
            }
        });
    }
</script>
  <script>

    trailerMake();
    function trailerMake() {
        var x_values = [];
        var x_values_sub = {};
        var cntr = 0;
        $.ajax({
            url: '/throttle/dbTrailerMake.do',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                        x_values_sub['name'] = value.Make;
                        x_values_sub['y'] = parseInt(value.Count);
                        x_values.push(x_values_sub);
                        x_values_sub = {};
//                    }
                });
//                alert(vehicleMakeArray[1])
                $('#trailerMake').highcharts({
                    chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },

                    title: {
                text: ''
            },

                    tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },

             plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y} ',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
                }
            },


                    series: [{
                    type: 'pie',
                    name: 'Nos',
                    data: x_values
                }]
                });
            }
        });
    }
</script>
  <script>

    truckType();
    function truckType() {
        var x_values = [];
        var x_values_sub = {};
        var cntr = 0;
        $.ajax({
            url: '/throttle/dbTruckType.do',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                        x_values_sub['name'] = value.Make;
                        x_values_sub['y'] = parseInt(value.Count);
                        x_values.push(x_values_sub);
                        x_values_sub = {};
//                    }
                });
//                alert(vehicleMakeArray[1])
                $('#truckType').highcharts({
                    chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },

                    title: {
                text: ''
            },

                    tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },

             plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y} ',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
                }
            },


                    series: [{
                    type: 'pie',
                    name: 'Nos',
                    data: x_values
                }]
                });
            }
        });
    }
    
      function11();
        function function11() {
            var vehicleMakeArray = [];
            var vehicleCountArray = [];
            $.ajax({
                url: '/throttle/getInvoiceCountValues.do',
                dataType: 'json',
                success: function(data) {
                    $.each(data, function(key, value) {
                        vehicleMakeArray.push(value.Name);
                        vehicleCountArray.push([parseInt(value.Count)]);
                    });
                    $('#truckTypen').highcharts({
                        chart: {type: 'column',
                            options2d: {
                                enabled: true,
                                alpha: 15,
                                beta: 15,
                                depth: 50,
                                viewDistance: 25
                            }
                        }, plotOptions: {
                            series: {
                                depth: 25,
                                colorByPoint: true
                            }
                        },
                        title: {
                            text: 'No of Invoices'
                        },
                        subtitle: {
                            text: ''
                        }, credits: {
                            enabled: false
                        },
                        xAxis: {
                            type: 'category',
                            categories: vehicleMakeArray,
                            labels: {
                                style: {
                                    fontSize: '13px',
                                }
                            }
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'No Of Invoice'
                            }
                        },
                        legend: {
                            enabled: false
                        },
                        tooltip: {
                            pointFormat: 'Invoice : <b>{point.y:f} Nos</b>'
                        },
                        series: [{
                                name: 'truck',
                                colorByPoint: true,
                                data: vehicleCountArray,
                                dataLabels: {
                                    enabled: true,
                                    rotation: 180,
                                    color: '#FFFFFF',
                                    align: 'right',
                                    format: '{point.y:f}',
                                    y: 10,
                                    style: {
                                        fontSize: '0px',
                                    }
                                }
                            }]
                    });
                }
            });
        }
</script>
  <script>

    trailerType();
    function trailerType() {
        var x_values = [];
        var x_values_sub = {};
        var cntr = 0;
        $.ajax({
            url: '/throttle/dbTrailerType.do',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                        x_values_sub['name'] = value.Make;
                        x_values_sub['y'] = parseInt(value.Count);
                        x_values.push(x_values_sub);
                        x_values_sub = {};
//                    }
                });
//                alert(vehicleMakeArray[1])
                $('#trailerType').highcharts({
                    chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },

                    title: {
                text: ''
            },

                    tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },

             plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y} ',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
                }
            },


                    series: [{
                    type: 'pie',
                    name: 'Nos',
                    data: x_values
                }]
                });
            }
        });
    }
</script>
<script>
    loadMonthlyTripStatusNxt();
    function loadMonthlyTripStatusNxt() {
        var customerOneArray = [];
        var customerTwoArray = [];
        
        $.ajax({
            url: '/throttle/emailListChart.do',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                        customerOneArray.push(value.invoice);
                        customerTwoArray.push([parseInt(value.customerId)]);
                });
                $('#barchartNxt').highcharts({
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Pending Email List'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        type: 'category',
                        categories: customerOneArray,
                        labels: {
                            rotation: -45,
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'No. Of. Emails'
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: ' <b>{point.y:f} Nos</b>'
                    },
                    series: [{
                            name: 'Email List',
                            data: customerTwoArray,
                            dataLabels: {
                                enabled: true,
                                rotation: -90,
                                color: '#FFFFFF',
                                align: 'right',
                                format: '{point.y:f}', // one decimal
                                y: 10, // 10 pixels down from the top
                                
                                style: {
                                    fontSize: '13px',
                                    fontFamily: 'Verdana, sans-serif'
                                }
                            }
                        }]
                });
            }
        });
    }
</script>





<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>




