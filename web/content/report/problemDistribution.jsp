
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="ets.domain.report.business.ReportTO" %>
        <%@ page import="java.util.*" %>
         <SCRIPT LANGUAGE="Javascript" SRC="/throttle/js/FusionCharts.js"></SCRIPT>
    </head>
    
    <script language="javascript">

function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}
       function submitPage(){
           var chek=validation();
           if(chek=='true'){
                document.salesBillTrend.action='/throttle/problemDistributionGraph.do';
                document.salesBillTrend.submit();
           }
       }
       function validation(){
               if(textValidation(document.salesBillTrend.fromDate,'From Date')){
                   document.salesBillTrend.fromDate.focus();
                   return 'false';
               }
               else if(textValidation(document.salesBillTrend.toDate,'To Date')){
                   document.salesBillTrend.toDate.focus();
                   return 'false';
               }
               return 'true';
          }
       function setValues(){
           var a='<%=request.getAttribute("fromDate")%>';
           var b='<%=request.getAttribute("mfrId")%>';
           var c='<%=request.getAttribute("usageId")%>';
           var d='<%=request.getAttribute("vehicleTypeId")%>';
           var age='<%=request.getAttribute("age")%>';
           var prob='<%=request.getAttribute("problem")%>';
           if(a!='null'){
               
                document.salesBillTrend.fromDate.value='<%=request.getAttribute("fromDate")%>';
                document.salesBillTrend.toDate.value='<%=request.getAttribute("toDate")%>';
           }
           if(b!='null'){
           
               
                document.salesBillTrend.mfrId.value=b;
           }
           if(c!='null'){
           
                document.salesBillTrend.usage.value=c;
           }
           if(d!='null'){
               
                document.salesBillTrend.vehicleType.value=d;
           }
           
           if(age!='null'){
               
                document.salesBillTrend.age.value=age;
           }
           if(prob!='null'){
               
                document.salesBillTrend.problem.value=prob;
           }
       }
       function getProblemName(){
           
        var oTextbox = new AutoSuggestControl(document.getElementById("problem"),new ListSuggestions("problem","/throttle/problemName.do?"));
        
        //getVehicleDetails(document.getElementById("regno"));
        
        
            }
    </script>
    <body onload="setValues();getProblemName();">
        <form  name="salesBillTrend" method="post">
            <%@ include file="/content/common/path.jsp" %>                 
            <%@ include file="/content/common/message.jsp" %>

<table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
<tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
</h2></td>
<td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
</tr>
<tr id="exp_table" >
<td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
    <div class="tabs" align="left" style="width:850;">
<ul class="tabNavigation">
        <li style="background:#76b3f1">Sales Bill Trend</li>
</ul>
<div id="first">
<table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
<tr>
<td><font color="red">*</font>From Date</td>
<td><input name="fromDate" class="form-control" type="text" value="" size="20">
<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.salesBillTrend.fromDate,'dd-mm-yyyy',this)"/></td>
<td height="25"><font color="red">*</font>To Date</td>
<td><input name="toDate" class="form-control" type="text" value="" size="20">
<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.salesBillTrend.toDate,'dd-mm-yyyy',this)"/></td>
<td>Problem</td>
<td><input id="problem" size="24" name="problem" type="text" class="form-control" value="" ></td>
</tr>

<tr>
    <td>MFR</td>
    <td>
    <select name="mfrId" style="width:125px">
        <option value="0">All</option>
        <c:if test = "${mfr != null}" >
            <c:forEach items="${mfr}" var="mfr">
                <option value="<c:out value="${mfr.mfrId}"/>"><c:out value="${mfr.mfrName}"/></option>
            </c:forEach>
        </c:if>
    </select></td>
    <td>Usage</td>
    <td><select name="usage" style="width:125px">
        <option value="0">All</option>
        <c:if test = "${usage != null}" >
            <c:forEach items="${usage}" var="usg">
                <option value="<c:out value="${usg.usageTypeIds}"/>"><c:out value="${usg.usageTypeName}"/></option>
            </c:forEach>
        </c:if>
    </select></td>
    <td>Age</td>
    <td><select style="width:150px;" name="age">
    <option value="0">All</option>
    <option value="1">< 1</option>
    <option value="2">< 2</option>
    <option value="3">< 3</option>
    <option value="4">< 4</option>
    <option value="5">> 5</option>
    </select></td>
    </tr>
<tr>
   <td>Vehicle Type</td>
   <td><select name="vehicleType" style="width:125px">
        <option value="0">All</option>
        <c:if test = "${vehicleType != null}" >
            <c:forEach items="${vehicleType}" var="vt">
                <option value="<c:out value="${vt.vehicleTypeId}"/>"><c:out value="${vt.vehicleTypeName}"/></option>
            </c:forEach>
        </c:if>
    </select></td>
    <td colspan="4"><input type="button" class="button" value="Search" onclick="submitPage();" /></td>
</tr>
</table>
</div></div>
</td>
</tr>
</table>
<br>
            
         <%   
if(request.getAttribute("problemDistributionGraph") != null){  
    
ArrayList problemDistributionGraph = (ArrayList) request.getAttribute("problemDistributionGraph");
ArrayList colorList = (ArrayList) request.getAttribute("ColorList");
System.out.println("problemDistributionGraph"+problemDistributionGraph.size());
System.out.println("colorList"+colorList.size());

int siz = problemDistributionGraph.size(); 
String[][] arrData = new String[siz][3];

int i1 = 0;
   
Iterator itr = problemDistributionGraph.iterator();
ReportTO reportTO = new ReportTO();
while(itr.hasNext()){
reportTO = (ReportTO) itr.next();
arrData[i1][0] = reportTO.getMfrName();
arrData[i1][1] = reportTO.getProblem();
i1++;
}

i1=0;

Iterator itr2 = colorList.iterator();
ReportTO reportTO1 = new ReportTO();
while(itr2.hasNext()){
reportTO1 = (ReportTO) itr2.next();
arrData[i1][2] = reportTO1.getColour();
i1++;
}

String strXML = "<graph showNames='1'  rotateNames='1' rotateValues='1' showValues='1' placeValuesInside='1'  decimalPrecision='0' caption='Problem distribution' YAxisName='Number of Vehicles' XAxisName='Manufacturer' >";

String strDataRev = "";

for(int i=0;i<arrData.length;i++){

strDataRev += "<set name='"+ arrData[i][0] +"' value='" + arrData[i][1] + "' color='" + arrData[i][2] + "' />";
		
}

strXML += strDataRev + "</graph>";

%>         
<br><br>

<table width='100%' bgcolor='white' border='0' cellpadding='0' cellspacing='0' class='repcontain'>

<tr>
    <td width="25%" valign="top" style="padding-top:40px;">
        <table width='90%' bgcolor='white' border='0' cellpadding='5' cellspacing='0' class='repcontain'>
            <c:if test = "${problemDistributionGraph != null}" >
                <tr>
                    <td class="contentsub">Mfr Name</td>
                    <td class="contentsub">Problem</td>
                </tr>
                <c:forEach items="${problemDistributionGraph}" var="pd">
                    <tr>    
                    <td class="text1"><c:out value="${pd.mfrName}"/></td>
                    <td class="text1"><c:out value="${pd.problem}"/></td>
                    </tr>
                </c:forEach> 
            </c:if>
        </table>
    </td>
    <td width="75%" valign="top"  >
        <table align="left" style="margin-left:0px;">
            <tr>
                <td>           
                    <jsp:include page="FusionChartsRenderer.jsp" flush="true"> 
                        <jsp:param name="chartSWF" value="/throttle/swf/FCF_Column3D.swf" /> 
                        <jsp:param name="strURL" value="" /> 
                        <jsp:param name="strXML" value="<%=strXML %>" /> 
                        <jsp:param name="chartId" value="productSales" /> 
                        <jsp:param name="chartWidth" value="800" /> 
                        <jsp:param name="chartHeight" value="700" /> 
                        <jsp:param name="debugMode" value="false" /> 	
                        <jsp:param name="registerWithJS" value="false" /> 
                    </jsp:include>
                </td>
            </tr>
        </table>
    </td>
</tr>
    

    

<%}%>


        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
