<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import="ets.domain.company.business.CompanyTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script language="javascript" src="/throttle/js/ajaxFunction.js"></script>

<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>



<script language="javascript">

    function computeMrp() {
        var spriceVal = document.addparts.sprice.value;
        var w = document.addparts.vatId.selectedIndex;
        var taxPercentage = document.addparts.vatId.options[w].text;
        //alert(taxPercentage);
        document.addparts.mrp.value = parseFloat(spriceVal) + (parseFloat(spriceVal) * parseFloat(taxPercentage) / 100);
    }
    function submitPage(value) {
        if (value == "save") {

            if (isSelect(document.addparts.mfrId, "manufactureName")) {
                return;
            } else if (isSelect(document.addparts.modelId, "modelName")) {
                return;
            } /*else if (isSelect(document.addparts.paplCode, "PaplCode")) {
             return;
             }*/ else if (isSelect(document.addparts.itemName, "ItemName")) {
                return;
            } else if (isSelect(document.addparts.uomId, "uomName")) {
                return;
            } else if (textValidation(document.addparts.specification, "specification")) {
                return;
            } else if (isSelect(document.addparts.reConditionable, "reConditionable")) {
                return;
            } else if (isSelect(document.addparts.scrapUomId, "Scrap Uom Name")) {
                return;
            } else if (textValidation(document.addparts.maxQuandity, "maxQuandity")) {
                return;
            } else if (textValidation(document.addparts.roLevel, "RoLevel")) {
                return;
            } else if (isSelect(document.addparts.rackId, "RackName")) {
                return;
            } else if (isSelect(document.addparts.subRackId, "subRackName")) {
                return;
            } /* else if (isSelect(document.addparts.sprice, "Selling Price")) {
             return;
             } else if (isSelect(document.addparts.vatId, "vatName")) {
             return;
             } else if (isSelect(document.addparts.groupId, "groupName")) {
             return;
             }
             */

            document.addparts.action = '/throttle/addParts.do';

//if(confirm("Please check Whether item already exists")){
            document.addparts.submit();
//}
        }
    }




    var httpReq;
    var temp = "";
    function ajaxData()
    {
        var url = "/throttle/getModels1.do?mfrId=" + document.addparts.mfrId.value;
        if (window.ActiveXObject)
        {
            httpReq = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else if (window.XMLHttpRequest)
        {
            httpReq = new XMLHttpRequest();
        }
        httpReq.open("GET", url, true);
        httpReq.onreadystatechange = function() {
            processAjax();
        };
        httpReq.send(null);
    }

    function processAjax()
    {
        if (httpReq.readyState == 4)
        {
            if (httpReq.status == 200)
            {
                temp = httpReq.responseText.valueOf();
                setOptions(temp, document.addparts.modelId);
            }
            else
            {
                alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
            }
        }
    }


    function ajaxRackData()
    {
        var url = "/throttle/getSubRack.do?rackId=" + document.addparts.rackId.value;
        if (window.ActiveXObject)
        {
            httpReq = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else if (window.XMLHttpRequest)
        {
            httpReq = new XMLHttpRequest();
        }
        httpReq.open("GET", url, true);
        httpReq.onreadystatechange = function() {
            processRackAjax();
        };
        httpReq.send(null);
    }


    function processRackAjax()
    {
        if (httpReq.readyState == 4)
        {
            if (httpReq.status == 200)
            {
                temp = httpReq.responseText.valueOf();
                setOptions(temp, document.addparts.subRackId);
            }
            else
            {
                alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
            }
        }
    }




    /*function setOptions(text){
     document.addparts.modelId.options.length=0;
     option0 = new Option("--select--",'0');
     document.addparts.modelId.options[0] = option0;
     
     if(text != ""){
     var splt = text.split('~');
     var temp1;
     document.addparts.modelId.options[0] = option0;
     for(var i=0;i<splt.length;i++){
     temp1 = splt[i].split('-');
     option0 = new Option(temp1[1],temp1[0])
     document.addparts.modelId.options[i+1] = option0;
     }
     }
     }*/


    function getItemNames() {
        // var oform-control = new AutoSuggestControl(document.getElementById("itemName"),new ListSuggestions("itemName","/throttle/handleItemSuggestions.do?"));
    }


    var checkItem;
    function checkItemExists(choice, param)
    {

        var url = '';
        if (choice == 'mfr' && param != '') {
            url = "/throttle/handleCheckItem.do?choice=mfr&param=" + document.addparts.itemCode.value;
        } else if (choice == 'papl' && param != '') {
            url = "/throttle/handleCheckItem.do?choice=papl&param=" + document.addparts.paplCode.value;
//                }else if(choice=='name' && param != '' ){
//                    url = "/throttle/handleCheckItem.do?choice=name&param="+document.addparts.itemName.value;
        }

        if (url != '') {
            if (window.ActiveXObject)
            {
                checkItem = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest)
            {
                checkItem = new XMLHttpRequest();
            }
            checkItem.open("GET", url, true);
            checkItem.onreadystatechange = function() {
                processCheckItem(choice);
            };
            checkItem.send(null);
        }
    }


    function processCheckItem(choice)
    {
        if (checkItem.readyState == 4)
        {
            if (checkItem.status == 200)
            {
                temp = checkItem.responseText.valueOf();
                document.addparts.checkName.value = temp;
                if (temp != '0') {
                    document.getElementById("userNameStatus").innerHTML = "Item Already Exists " + temp;
                    if (choice == 'mfr') {
                        setTimeout(function() {
                            document.addparts.itemCode.focus()
                        }, 50);
                        document.addparts.itemCode.select();
                    } else if (choice == 'papl') {
                        setTimeout(function() {
                            document.addparts.paplCode.focus()
                        }, 50);
                        document.addparts.paplCode.select();
                    }
//            else if(choice=='name'){
//                setTimeout(function () { document.addparts.itemName.focus() }, 50);
//                document.addparts.itemName.select();
//            }
$("#addButton").hide();
                } else {

                    document.getElementById("userNameStatus").innerHTML = "";
					$("#addButton").show();
                }
            }
            else
            {
                alert("Error loading page\n" + checkItem.status + ":" + checkItem.statusText);
            }
        }
    }





</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.PARTS"  text="Spare Parts"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="stores.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="stores.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.stores"  text="Stores"/></a></li>
            <li class="active"><spring:message code="stores.label.PARTS"  text="Spare Parts"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onLoad="">
                <form name="addparts"  method="post" >
                    <%@ include file="/content/common/message.jsp" %>

                    <font color="red" style="font-family:Arial, Helvetica, sans-serif; font-size:16px; font-weight:bold; ">
                   <div align="center" id="userNameStatus">&nbsp;&nbsp;</div></font>
                    <table class="table table-info mb30 table-hover">
                        <thead><tr><th colspan="4"><spring:message code="stores.label.PARTS"  text="Spare Parts"/></th></tr></thead>


                        <input type="hidden" name="sectionId" value='0'>

                        <tr>
                            <td ><font color="red">*</font><spring:message code="stores.label.Category"  text="default text"/></td>
                            <td  ><select class="form-control" style="width:260px;height:40px;" name="categoryId" form-control >
                                    <option value="0">---<spring:message code="stores.label.Select" text="default text"/>---</option>
                                    <c:if test = "${CategoryList != null}" >
                                        <c:forEach items="${CategoryList}" var="Dept">
                                            <option value='<c:out value="${Dept.categoryId}" />'><c:out value="${Dept.categoryName}" /></option>
                                        </c:forEach >
                                    </c:if>
                                </select></td>

                            <td ><font color="red">*</font><spring:message code="stores.label.Mfr"  text="default text"/>
                            </td>
                            <td  ><select class="form-control" style="width:260px;height:40px;" name="mfrId" form-control onChange="ajaxData();" >
                                    <option value="0">---<spring:message code="stores.label.Select" text="default text"/>---</option>
                                    <c:if test = "${MfrList != null}" >
                                        <c:forEach items="${MfrList}" var="Dept">
                                            <option value='<c:out value="${Dept.mfrId}" />'><c:out value="${Dept.mfrName}" /></option>
                                        </c:forEach >
                                    </c:if>
                                </select></td>
                        </tr>
                        <tr>
                            <td  ><font color="red">*</font><spring:message code="stores.label.Model"  text="default text"/>
                            </td>
                            <td   ><select class="form-control" style="width:260px;height:40px;" form-control name="modelId" >
                                    <option value="0">---<spring:message code="stores.label.Select" text="default text"/>---</option>
                                    <c:if test = "${ModelList != null}" >
                                        <c:forEach items="${ModelList}" var="Dept">
                                            <option value='<c:out value="${Dept.modelId}" />'><c:out value="${Dept.modelName}" /></option>
                                        </c:forEach >
                                    </c:if>
                                </select></td>

                            <td  ><font color="red">*</font><spring:message code="stores.label.ItemCode"  text="default text"/>
                            </td>
                            <td  ><input name="itemCode" type="text" class="form-control" style="width:260px;height:40px;" onchange="checkItemExists('mfr', this.value)" form-control value=""></td>
                            <!--<td  ><input name="itemCode" type="text" class="form-control" style="width:260px;height:40px;" onfocusout="checkItemExists('mfr',this.value)" form-control value=""></td>-->
                        </tr>
                        <tr>
                            <td ><font color="red">*</font><spring:message code="stores.label.TahoosCode"  text="default text"/></td>
                            <td ><input name="paplCode" type="text" form-control readonly class="form-control" style="width:260px;height:40px;"  onchange="checkItemExists('papl', this.value)" value=""></td>

                            <td ><font color="red">*</font><spring:message code="stores.label.ItemName"  text="default text"/>
                            </td>
                            <td >
                                <input name="itemName" form-control type="text" class="form-control" style="width:260px;height:40px;"  value="">  </td>
                        </tr>
                        <tr>
                            <td  ><font color="red">*</font>Uom</td>
                            <td   ><select class="form-control" style="width:260px;height:40px;" name="uomId" form-control >
                                    <option value="0">---<spring:message code="stores.label.Select" text="default text"/>---</option>
                                    <c:if test = "${UomList != null}" >
                                        <c:forEach items="${UomList}" var="Dept">
                                            <option value='<c:out value="${Dept.uomId}" />'><c:out value="${Dept.uomName}" /></option>
                                        </c:forEach >
                                    </c:if>
                                </select></td>

                            <!--                                            <td  ><font color="red">*</font><spring:message code="stores.label.GroupName"  text="default text"/>
                            </td>
                                                                        <td   ><select class="form-control" style="width:260px;height:40px;" name="groupId" form-control >
                                                                                <option value="0">---Select---</option>
                            <c:if test = "${groupList != null}" >
                                <c:forEach items="${groupList}" var="group">
                                    <option value='<c:out value="${group.groupId}" />'><c:out value="${group.groupName}" /></option>
                                </c:forEach >
                            </c:if>
                        </select></td>
                </tr>
                <tr>
                    <td ><font color="red">*</font><spring:message code="stores.label.Tax"  text="default text"/>
</td>
                    <td  ><select class="form-control" style="width:260px;height:40px;" name="vatId" form-control >
                            <option value="0">---Select---</option>
                            <c:if test = "${vatList != null}" >
                                <c:forEach items="${vatList}" var="vat">
                                    <option value='<c:out value="${vat.vatId}" />'><c:out value="${vat.vat}" /></option>
                                </c:forEach >
                            </c:if>
                        </select></td>-->

                        <input type="hidden" name="vatId" value="0"/>
                        <input type="hidden" name="groupId" value="0"/>

                        <td ><font color="red">*</font><spring:message code="stores.label.ItemSpecification"  text="default text"/>
                        </td>
                        <td ><textarea class="form-control" style="width:260px;height:40px;" name="specification" form-control></textarea></td>
                        </tr>
                        <tr>
                            <td  ><font color="red">*</font><spring:message code="stores.label.IsReconditionable"  text="default text"/>
                            </td>
                            <td  ><select name="reConditionable" form-control class="form-control" style="width:260px;height:40px;">
                                    <option value="0">---<spring:message code="stores.label.Select" text="default text"/>---</option>
                                    <option><spring:message code="stores.label.Y"  text="default text"/></option>
                                    <option><spring:message code="stores.label.N"  text="default text"/></option>
                                </select></td>

                            <td  ><font color="red">*</font><spring:message code="stores.label.ScrapUnit"  text="default text"/>
                            </td>
                            <td   ><select class="form-control" style="width:260px;height:40px;" form-control name="scrapUomId" >
                                    <option value="0">---<spring:message code="stores.label.Select" text="default text"/>---</option>
                                    <c:if test = "${UomList != null}" >
                                        <c:forEach items="${UomList}" var="Dept">
                                            <option value='<c:out value="${Dept.uomId}" />'><c:out value="${Dept.uomName}" /></option>
                                        </c:forEach >
                                    </c:if>
                                </select></td>
                        </tr>
                        <tr>
                            <td ><font color="red">*</font><spring:message code="stores.label.MaximumQty"  text="default text"/>
                            </td>
                            <td ><input name="maxQuandity" form-control type="text" class="form-control" style="width:260px;height:40px;" value=""></td>

                            <td ><font color="red">*</font><spring:message code="stores.label.RoLevel"  text="default text"/>
                            </td>
                            <td ><input name="roLevel" form-control type="text" class="form-control" style="width:260px;height:40px;" value=""></td>
                        </tr>
                        <tr>
                            <td  ><font color="red">*</font><spring:message code="stores.label.Rack"  text="default text"/>
                            </td>
                            <td   ><select class="form-control" style="width:260px;height:40px;" form-control name="rackId" onChange="ajaxRackData();" >
                                    <option value="0">---<spring:message code="stores.label.Select" text="default text"/>---</option>
                                    <c:if test = "${getRackList != null}" >
                                        <c:forEach items="${getRackList}" var="Dept">
                                            <option value='<c:out value="${Dept.rackId}" />'><c:out value="${Dept.rackName}" /></option>
                                        </c:forEach >
                                    </c:if>
                                </select></td>

                            <td  ><font color="red">*</font><spring:message code="stores.label.SubRack"  text="default text"/>
                            </td>
                            <td   ><select class="form-control" style="width:260px;height:40px;" form-control name="subRackId" >
                                    <option value="0">---<spring:message code="stores.label.Select" text="default text"/>---</option>
                                    <c:if test = "${getSubRackList != null}" >
                                        <c:forEach items="${getSubRackList}" var="Dept">
                                            <option value='<c:out value="${Dept.subRackId}" />'><c:out value="${Dept.subRackName}" /></option>
                                        </c:forEach >
                                    </c:if>
                                </select>
                                <input type="hidden" value="" name="checkName" >
                            </td>

                        </tr>

                        <!--                                        <tr>
                                                                    <td ><font color="red">*</font><spring:message code="stores.label.SellingPrice"  text="default text"/>
                                                                    </td>
                                                                    <td ><input name="sprice" form-control type="text" class="form-control" style="width:260px;height:40px;" value="" onchange="computeMrp();"></td>
                        
                                                                    <td ><font color="red">*</font><spring:message code="stores.label.MRP(with Tax)"  text="default text"/>
                                                                      </td>
                                                                    <td ><input name="mrp" form-control readonly type="text" class="form-control" style="width:260px;height:40px;" value=""></td>
                                                                </tr>-->
                        <input type="hidden" name="sprice" value="0"/>
                        <input type="hidden" name="mrp" value="0"/>




                    </table>
                    <br>
                    <center>
                        <input type="button" class="btn btn-success" id="addButton" value='<spring:message code="stores.label.Save"  text="default text"/>' name="save" onClick="submitPage(this.name)">
                        &emsp;<input type="reset" class="btn btn-success" value="<spring:message code="stores.label.Clear" text="default text"/>">
                        <input type="hidden" value="" name="temp" >

                    </center>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>


