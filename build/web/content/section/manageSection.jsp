<%@ include file="/content/common/NewDesign/header.jsp" %>
    <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BUS</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    </head>
    <script language="javascript">
        function submitPage(value)
        {
            if (value=='add')
                {
                    document.desigDetail.action ='/throttle/addSectionPage.do';
                }else if(value == 'modify'){
                
                document.desigDetail.action ='/throttle/alterSectionPage.do';
            }
            document.desigDetail.submit();
        }
    </script>
    <div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="serviceplan.label.ManageSection" text="default text"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="serviceplan.label.ServicePlan" text="default text"/></a></li>
          <li class="active"><spring:message code="serviceplan.label.ManageSection" text="default text"/></li>
        </ol>
      </div>
      </div>
 <div class="contentpanel">
<div class="panel panel-default">
 <div class="panel-body">
    <body>
        
        <form method="post" name="desigDetail">
           
      

<%@ include file="/content/common/message.jsp" %>

 <% int index = 0;  %>    
            <c:if test = "${SectionList != null}" >
               <table class="table table-info mb30 table-hover" id="table">
                    
                   <thead>
                    <tr align="center">
                        <th height="30" ><spring:message code="serviceplan.label.SNo" text="default text"/></th>
                        <th height="30" ><spring:message code="serviceplan.label.SectionCode" text="default text"/></th>
                        <th height="30" ><spring:message code="serviceplan.label.SectionName" text="default text"/></th>
                        <th height="30" ><spring:message code="serviceplan.label.Description" text="default text"/></th>
                        <th height="30" ><spring:message code="serviceplan.label.ServiceType" text="default text"/></th>
                        <th height="30" ><spring:message code="serviceplan.label.Status" text="default text"/></th>
                       
                    </tr>
                    </thead>
                    <%

                    %>
                    
                    <c:forEach items="${SectionList}" var="list"> 	
                        <%

            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr   > 
                            <td  height="30"><%=index + 1%></td>
                            <td  height="30"><c:out value="${list.sectionCode}"/></td>
                            <td  height="30"><input type="hidden" name="sectionId" value='<c:out value="${list.sectionId}"/>'> <c:out value="${list.sectionName}"/></td>
                            <td  height="30"><c:out value="${list.description}"/></td>
                            <td  height="30">
                            <c:if test="${serviceTypeList != null}">
                            <c:forEach items="${serviceTypeList}" var="service">
                                <c:if test="${service.serviceTypeId == list.serviceTypeId}">
                                    <c:out value="${service.serviceTypeName}"/>
                                </c:if>
                                </c:forEach>
                            </c:if>
                            </td>
                            <td  height="30">                                
                                <c:if test = "${list.activeInd == 'Y'}" >
                                    <spring:message code="serviceplan.label.Active" text="default text"/>
                                </c:if>
                                <c:if test = "${list.activeInd == 'N'}" >
                                    <spring:message code="serviceplan.label.InActive" text="default text"/>
                                </c:if>
                            </td>
                             
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach >
                    
                </table>
            </c:if> 
            <center>
                <input type="button" name="add" value=" <spring:message code="serviceplan.label.ADD" text="default text"/>" onClick="submitPage(this.name)" class="btn btn-success" >
                <c:if test = "${SectionList != null}" >
                    <input type="button" value=" <spring:message code="serviceplan.label.ALTER" text="default text"/>" name="modify" onClick="submitPage(this.name)" class="btn btn-success" >
                </c:if>
                <input type="hidden" name="reqfor" value="">
            </center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
