<%-- 
    Document   : billgeneration
    Created on : Nov 4, 2013, 8:15:50 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
        <link href="/throttle/css/tableFilter.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/filtergrid.css" rel="stylesheet" type="text/css"/>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>
        <script>
            function submitPage()
            {
                document.billGeneration.action = "/throttle/BrattleFoods/billGeneration.jsp?param";
                document.billGeneration.submit();
            }
            function generateBill()
            {
                document.billGeneration.action = "/throttle/BrattleFoods/printbillgeneration.jsp";
                document.billGeneration.submit();
            }

        </script>
    </head>
    <body>
        <% String menuPath = "Operations >>  Bill Generation";
            request.setAttribute("menuPath", menuPath);
        %>
        <form name="billGeneration" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Bill Generation</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    <tr>
                                        <td><font color="red">*</font>Customer</td>
                                        <td height="30">
                                            <select class="form-control" name="Customer" id="Customer"  style="width:125px;">
                                                <option value="0">---Select---</option>
                                                <option value='1' >M/S Doshi Foods</option>
                                                <option value='2' >M/S Bakers Circle</option>
                                                <option value='2' >M/S Balaji & Co</option>
                                            </select>
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker"  onclick="ressetDate(this);"></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" onclick="ressetDate(this);"></td>
                                        <td><input type="button" class="button"   value="FETCH DATA" onclick="submitPage();"></td>
                                    </tr>


                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <% if(request.getParameter("param") != null){ %>
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
                <thead>
                    <tr height="30">

                        <td class="contenthead">S.No</td>
                        <td class="contenthead">Customer</td>
                        <td class="contenthead">CNote No</td>
                        <td class="contenthead">Trip Id</td>
                        <td class="contenthead">Vehicle No</td>
                        <td class="contenthead">Vehicle Type</td>
                        <td class="contenthead">Route Name</td>
                        <td class="contenthead">Driver Name</td>
                        <td class="contenthead">Trip Start Date</td>
                        <td class="contenthead">Trip End Date</td>
                        <td class="contenthead">Out Km</td>
                        <td class="contenthead">In Km</td>
                        <td class="contenthead">Load Tonnage</td>
                        <td class="contenthead">Status</td>
                        <td class="contenthead">Action</td>
                        <td class="contenthead">select</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text1" >1</td>
                        <td class="text1" >M/s Doshi Foods</td>
                        <td class="text1" >CN/13-14/10023</td>
                        <td class="text1" ><a href="/throttle/BrattleFoods/tripSheet.jsp">TS/13-14/10045</a></td>
                        <td class="text1" >TN 02 AA 2025</td>
                        <td class="text1" >AL / 2516 / 12 PLT</td>
                        <td class="text1" >Delhi-Chennai</td>
                        <td class="text1" >Mr.Vinayagam</td>
                        <td class="text1" >07-11-2013</td>
                        <td class="text1" >10-11-2013</td>
                        <td class="text1" >125345</td>
                        <td class="text1" >&nbsp;</td>
                        <td class="text1" >25.00</td>
                        <td class="text1" ><img src="/throttle/images/icon_closed.png" alt="" /></td>
                        <td class="text1" ><a href="/throttle/BrattleFoods/tripSheetSettlement.jsp">Settlement</a></td>
                        <td class="text1"><input type="checkbox" name="select" value="Alter"></td>
                    </tr>
                    <tr>
                        <td class="text2" >2</td>
                        <td class="text2" >M/s Bakers Circle</td>
                        <td class="text2" >CN/13-14/10321</td>
                        <td class="text2" ><a href="/throttle/BrattleFoods/tripSheet.jsp">TS/13-14/10925</a></td>
                        <td class="text2" >DL 22 SR 4578</td>
                        <td class="text2" >AL / 2516 / 12 PLT</td>
                        <td class="text2" >Kashipur - Delhi</td>
                        <td class="text2" >Mr.Gurmeet</td>
                        <td class="text2" >06-11-2013</td>
                        <td class="text2" >10-11-2013</td>
                        <td class="text2" >278549</td>
                        <td class="text2" >&nbsp;</td>
                        <td class="text2" >35.00</td>
                        <td class="text2" ><img src="/throttle/images/icon_closed.png" alt="" /></td>
                        <td class="text2" ><a href="/throttle/BrattleFoods/tripSheetSettlement.jsp">Settlement</a></td>
                        <td class="text2"><input type="checkbox" name="select" value="Alter"></td>
                    </tr>
                    <tr>
                        <td class="text1" >3</td>
                        <td class="text1" >M/s Balaji & Co</td>
                        <td class="text1" >CN/13-14/10549</td>
                        <td class="text1" ><a href="/throttle/BrattleFoods/tripSheet.jsp">TS/13-14/10741</a></td>
                        <td class="text1" >DL 02 WE 7658</td>
                        <td class="text1" >TATA / 3118 / 16 PLT</td>
                        <td class="text1" >Delhi - Chandigarh</td>
                        <td class="text1" >Mr.Rajat</td>
                        <td class="text1" >03-11-2013</td>
                        <td class="text1" >04-11-2013</td>
                        <td class="text1" >225375</td>
                        <td class="text1" >225847</td>
                        <td class="text1" >29.00</td>
                        <td class="text1" ><img src="/throttle/images/icon_closed.png" alt="" /></td>
                        <td class="text1" ><a href="/throttle/BrattleFoods/tripSheetSettlement.jsp">Settlement</a></td>
                        <td class="text1"><input type="checkbox" name="select" value="Alter"></td>
                    </tr>
                </tbody>
            </table>
                
            <br>    
        <center>
            <input type="button" class="button" value="Generate Bill" name="Generate Bill" onClick="generateBill()">
        </center>
        <%} %>
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

</body>
</html>
