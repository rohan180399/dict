<%-- 
    Document   : GRProfitabilityExcel
    Created on : May 2, 2016, 4:22:35 PM
    Author     : gulshan kumar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
            <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "customerWiseProfitabilty-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
            <c:if test = "${tripDetails != null}" >
               
                <br>
                <table border="1"  align="center" width="800" cellpadding="0" cellspacing="1" >
                    <thead>
                        
                         <tr >
                           <th >S no</th>
                            <th >Customer Name</th>
                            <th >Gr No</th>
                            <th >movement Type</th>
                            <th >routeInfo</th>
                            <th >containerNo</th>
                            <th >Container Type</th>
                            <th >Revenue</th>
                            <th >Expense</th>
                            <th >profit</th>
                            <th >profit(%)</th>
                            
                            
                        </tr>
                    </thead>
                    <% int index = 0, sno = 1;%>
                     
                    <c:forEach items="${tripDetails}" var="csList">
                       


           <tr>
                                <td align="center"><%=sno%></td>
                             <td align="center" class="text1"><c:out value="${csList.customerName}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.grNo}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.movementType}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.routeInfo}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.containerNo}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.typeName}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.estimatedRevenue}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.estimatedExpenses}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.estimatedRevenue - csList.estimatedExpenses}"/></td>
                            <td align="center" class="text1"><c:out value="${(csList.estimatedRevenue -csList.estimatedExpenses/csList.estimatedRevenue)/100}"/></td>
                           
                           
                             



                        </tr>
                        <%
                            index++;
                            sno++;
                        %>
                    </c:forEach>
                </table>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>

