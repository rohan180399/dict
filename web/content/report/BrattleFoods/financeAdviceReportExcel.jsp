<%-- 
    Document   : driverSettlementReportExcel
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
      <%
                String menuPath = "Finance >> Daily Advance Advice";
                request.setAttribute("menuPath", menuPath);
                String dateval = request.getParameter("dateval");
                String active = request.getParameter("active");
                String type = request.getParameter("type");
    %>
     
    <body>

        <form name="financeReportDetails" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "FinanceAdviceReportDetails-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>
        
      

            <br>
            <br>
            <br>

<br><br>
            Advice Date: <%=dateval%>
            <br>
            <br>
               <c:if test = "${financeAdviceDetails != null}" >
               <table width="100%" align="center" border="0" id="table" class="sortable">
               <thead>                   
                    <tr height="40">
                        <th><h3>S.No</h3></th>
                        <th><h3>Type</h3></th>
                        <th><h3>Customer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h3></th>
                        <th><h3>CNoteNo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h3></th>
                        <th><h3>TripNo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h3></th>
                        <th><h3>VehicleNo</h3></th>
                        <th><h3>VehicleType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h3></th>
                        <th><h3>Route</h3></th>
                        <th><h3>Driver</h3></th>
                        <th><h3>TripStart</h3></th>
                        <th><h3>Freight Rate</h3></th>
                        <th><h3>Nett Expenses</h3></th>
                        <th><h3>Profit</h3></th>
                        <th><h3>Already PaidAmount</h3></th>
                        <th><h3>Transit Days</h3></th>
                        <th><h3>Journey Day</h3></th>
                        <th><h3>ToBePaid Today</h3></th>
                        <th><h3>Paid</h3></th>
                        <th><h3>action</h3></th>
                    </tr>
                    </thead>
                    <tbody>

                        <% int index = 0;%>
                        <c:forEach items="${financeAdviceDetails}" var="FD">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text1";
                                        } else {
                                            classText = "text2";
                                        }


                            %>
                            <tr height="30">
                                <td class="<%=classText%>" align="left"> <%= index + 1%> </td>
                                <td class="<%=classText%>" ><c:out value="${FD.batchType}"/></td>
                                <td class="<%=classText%>" ><c:out value="${FD.customerName}"/></td>
                                <td class="<%=classText%>">
                                    <c:out value="${FD.cnoteName}"/>
                                </td>
                                <td class="<%=classText%>" align="left"> <c:out value="${FD.tripcode}" />||<c:out value="${FD.tripid}" /></td>
                                <td class="<%=classText%>" align="left"><c:out value="${FD.regNo}" /></td>
                                <td class="<%=classText%>" ><c:out value="${FD.vehicleTypeName}"/></td>
                                <td class="<%=classText%>" ><c:out value="${FD.routeName}"/></td>
                                <td class="<%=classText%>" align="left"><c:out value="${FD.driverName}"/></td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${FD.planneddate}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${FD.freightcharges}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${FD.estimatedexpense}"/> </td>
                                <td class="<%=classText%>"  align="left"> 
                                    <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${FD.freightcharges-FD.estimatedexpense}" />
                                </td>
                                <td class="<%=classText%>" align="left"> <c:out value="${FD.actualadvancepaid}" /></td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${FD.estimatedtransitday}"/> </td>
                                <td class="<%=classText%>" align="left"> <c:out value="${FD.tripday}" /></td>


                                <td class="<%=classText%>" align="left">
                                    <c:if test="${FD.approvalstatus==0 || FD.approvalstatus==3}">
                                    <c:out value="${FD.estimatedadvance}" />
                                    </c:if>
                                    <c:if test="${FD.approvalstatus==1}">
                                    <c:out value="${FD.requestedadvance}" />
                                    </c:if>

                                </td>
                                
                                <%if(active.equals("1")){%>
                                <c:if test="${FD.approvalstatus==0}">
                                <td>-</td>
                                <td class="<%=classText%>" align="left">Waiting for Approval</td>
                                </c:if>



                                
                                <%--c:if test="${FD.approvalstatus==3 && FD.paidstatus=='N'}">
                                <td class="<%=classText%>" align="left"><a href="/throttle/tripSheetApprove.do?tripid=<c:out value="${FD.tripid}" />&tripday=<c:out value="${FD.tripday}" />
                                                                           &estimatedadvance=<c:out value="${FD.estimatedadvance}"/>&cnoteName=<c:out value="${FD.cnoteName}"/>&vehicleTypeName=<c:out value="${FD.vehicleTypeName}"/>&routeName=<c:out value="${FD.routeName}"/>
                                                                           &driverName=<c:out value="${FD.driverName}"/>&planneddate=<c:out value="${FD.planneddate}"/>&estimatedexpense=<c:out value="${FD.estimatedexpense}"/>
                                                                           &actualadvancepaid=<c:out value="${FD.actualadvancepaid}"/>&vegno=<c:out value="${FD.regNo}" />&advicedate=<%=dateval%>&billtype=<%=type%>">Request</a>&nbsp;&nbsp;</td>                                
                                </c:if--%>
                                
                                
                                


                                <c:if test="${(FD.approvalstatus==3) && FD.paidstatus=='N'}">
                                    <%if(!type.equalsIgnoreCase("M")){%>
                                    <td>-</td>
                                <td class="<%=classText%>" align="left"><a href="/throttle/tripSheetPay.do?customerName=<c:out value="${FD.customerName}"/>&tripCode=<c:out value="${FD.tripcode}" />&tripid=<c:out value="${FD.tripid}" />&tripday=<c:out value="${FD.tripday}" />
                                                                           &requestedadvance=<c:out value="${FD.requestedadvance}"/>&estimatedadvance=<c:out value="${FD.estimatedadvance}"/>&cnoteName=<c:out value="${FD.cnoteName}"/>&vehicleTypeName=<c:out value="${FD.vehicleTypeName}"/>&routeName=<c:out value="${FD.routeName}"/>
                                                                           &driverName=<c:out value="${FD.driverName}"/>&planneddate=<c:out value="${FD.planneddate}"/>&estimatedexpense=<c:out value="${FD.estimatedexpense}"/>&actualadvancepaid=<c:out value="${FD.actualadvancepaid}"/>&vegno=<c:out value="${FD.regNo}" />&tripAdvaceId=<c:out value="${FD.tripAdvaceId}" />&advicedate=<%=dateval%>&status=0">pay</a></td>
                                                                           <%}%>
                                 </c:if>
                                 <c:if test="${(FD.approvalstatus==1 && FD.paidstatus=='N')}">
                                     <%if(!type.equalsIgnoreCase("M")){%>
                                 <td>-</td>
                                 <td class="<%=classText%>" align="left"><a href="/throttle/tripSheetPay.do?customerName=<c:out value="${FD.customerName}"/>&tripCode=<c:out value="${FD.tripcode}" />&tripid=<c:out value="${FD.tripid}" />&tripday=<c:out value="${FD.tripday}" />
                                                                           &requestedadvance=<c:out value="${FD.requestedadvance}"/>&estimatedadvance=<c:out value="${FD.estimatedadvance}"/>&cnoteName=<c:out value="${FD.cnoteName}"/>&vehicleTypeName=<c:out value="${FD.vehicleTypeName}"/>&routeName=<c:out value="${FD.routeName}"/>
                                                                           &driverName=<c:out value="${FD.driverName}"/>&planneddate=<c:out value="${FD.planneddate}"/>&estimatedexpense=<c:out value="${FD.estimatedexpense}"/>&actualadvancepaid=<c:out value="${FD.actualadvancepaid}"/>&vegno=<c:out value="${FD.regNo}" />&tripAdvaceId=<c:out value="${FD.tripAdvaceId}" />&advicedate=<%=dateval%>&status=1">pay</a></td>
                                     <%}%>
                                 </c:if>

                                 <c:if test="${(FD.paidstatus!='Y')&& (FD.approvalstatus==1)}">
                                 <%if(type.equalsIgnoreCase("M")){%>
                                 <td>-</td>
                                 <td class="<%=classText%>" align="left"><a href="/throttle/tripSheetPay.do?customerName=<c:out value="${FD.customerName}"/>&tripCode=<c:out value="${FD.tripcode}" />&tripid=<c:out value="${FD.tripid}" />&tripday=<c:out value="${FD.tripday}" />
                                                                           &requestedadvance=<c:out value="${FD.requestedadvance}"/>&estimatedadvance=<c:out value="${FD.estimatedadvance}"/>&cnoteName=<c:out value="${FD.cnoteName}"/>&vehicleTypeName=<c:out value="${FD.vehicleTypeName}"/>&routeName=<c:out value="${FD.routeName}"/>
                                                                           &driverName=<c:out value="${FD.driverName}"/>&planneddate=<c:out value="${FD.planneddate}"/>&estimatedexpense=<c:out value="${FD.estimatedexpense}"/>&actualadvancepaid=<c:out value="${FD.actualadvancepaid}"/>&vegno=<c:out value="${FD.regNo}" />&tripAdvaceId=<c:out value="${FD.tripAdvaceId}" />&advicedate=<%=dateval%>&status=1">pay</a></td>
                                 <%}%>
                                 </c:if>

                                <c:if test="${(FD.approvalstatus ==2) && FD.paidstatus !='Y' }">
                                    <td>-</td>
                                <td class="<%=classText%>" align="left">
                                    <c:if test="${(FD.approvalstatus ==2)}">
                                    Rejected
                                    </c:if>
                                    <c:if test="${(FD.approvalstatus ==1)}">
                                    Approved
                                    </c:if>
                                </td>
                                </c:if>


                                                                           

                                <c:if test="${FD.paidstatus=='Y'}">                                
                                <td class="<%=classText%>" align="left"><c:out value="${FD.paidamt}"/></td>
                                <td class="<%=classText%>" align="left">Already Paid</td>
                                </c:if>


                                <%}else{%>

                                <c:if test="${FD.paidstatus=='Y'}">
                                <td class="<%=classText%>" align="left"><font color="green"><c:out value="${FD.paidamt}"/></font></td>
                                <td class="<%=classText%>" align="left"><font color="green">Paid</font></td>
                                </c:if>

                                <c:if test="${FD.paidstatus !='Y'}">
                                    <td class="<%=classText%>" align="center"><font color="red">-</font></td>
                                    <td class="<%=classText%>" align="center"><font color="red">Not Paid</font></td>
                                </c:if>

                                

                                <%}%>
                                <!--
                                <td class="<%=classText%>" align="left"><a href="/throttle/manualfinanceadvice.do?tripid=<c:out value="${FD.tripid}" />&tripday=<c:out value="${FD.tripday}" />
                                                                           &estimatedadvance=<c:out value="${FD.estimatedadvance}"/>&cnoteName=<c:out value="${FD.cnoteName}"/>&vehicleTypeName=<c:out value="${FD.vehicleTypeName}"/>&routeName=<c:out value="${FD.routeName}"/>
                                                                           &driverName=<c:out value="${FD.driverName}"/>&planneddate=<c:out value="${FD.planneddate}"/>&estimatedexpense=<c:out value="${FD.estimatedexpense}"/>
                                                                           &actualadvancepaid=<c:out value="${FD.actualadvancepaid}"/>&vegno=<c:out value="${FD.regNo}" />&advicedate=<%=dateval%>&billtype=<%=type%>">Manual Request</a>&nbsp;&nbsp;
                                
                                </td>
                                -->


                            </tr>
                            <% index++;%>
                        </c:forEach>
                    </c:if>


            </tbody>
            </table>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>