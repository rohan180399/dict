// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   PurchaseBP.java
package ets.domain.purchase.business;

import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.arch.util.FPUtil;
import ets.domain.mrs.business.MrsTO;
import ets.domain.mrs.data.MrsDAO;
import ets.domain.purchase.data.PurchaseDAO;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;

// Referenced classes of package ets.domain.purchase.business:
//            PurchaseTO
public class PurchaseBP {

    public PurchaseBP() {
    }

    public PurchaseDAO getPurchaseDAO() {
        return purchaseDAO;
    }

    public void setPurchaseDAO(PurchaseDAO purchaseDAO) {
        this.purchaseDAO = purchaseDAO;
    }

    public MrsDAO getMrsDAO() {
        return mrsDAO;
    }

    public void setMrsDAO(MrsDAO mrsDAO) {
        this.mrsDAO = mrsDAO;
    }

    public int processGenerateMpr(ArrayList mprMixedVendor, int purchaseType)
            throws FPBusinessException, FPRuntimeException {
        int status = 0;
        int mpr = 0;
        status = purchaseDAO.doGenerateMpr(mprMixedVendor, purchaseType);
        System.out.println((new StringBuilder()).append("status is=").append(status).toString());
        return status;
    }

    public ArrayList processVendorList(int vendorTypeId)
            throws FPBusinessException, FPRuntimeException {
        ArrayList vendorList = new ArrayList();
        vendorList = purchaseDAO.getVendorList(vendorTypeId);
        if (vendorList.size() == 0) {
            throw new FPBusinessException("EM-MPR-02");
        } else {
            return vendorList;
        }
    }
    public ArrayList purchaseReceipt()
            throws FPBusinessException, FPRuntimeException {
        ArrayList purchaseReceiptList = new ArrayList();
        purchaseReceiptList = purchaseDAO.purchaseReceipt();
            return purchaseReceiptList;

    }

    public ArrayList processVendorItemList(String vendorId)
            throws FPBusinessException, FPRuntimeException {
        ArrayList vendorItemList = new ArrayList();
        vendorItemList = purchaseDAO.getVendorItemList(vendorId);
        return vendorItemList;
    }

    public ArrayList processMprStatusList(PurchaseTO purchaseTO, String fromDate, String toDate)
            throws FPBusinessException, FPRuntimeException {
        ArrayList mprList = new ArrayList();
        mprList = purchaseDAO.getMprStatusList(purchaseTO, fromDate, toDate);
        if (mprList.size() == 0) {
            throw new FPBusinessException("EM-MPR-03");
        } else {
            return mprList;
        }
    }

    public ArrayList processMprPoList(PurchaseTO purchaseTO, String fromDate, String toDate)
            throws FPBusinessException, FPRuntimeException {
        ArrayList mprList = new ArrayList();
        mprList = purchaseDAO.getMprPoList(purchaseTO, fromDate, toDate);
        if (mprList.size() != 0);
        return mprList;
    }

    public ArrayList processMprDetail(int mprId)
            throws FPBusinessException, FPRuntimeException {
        ArrayList mprList = new ArrayList();
        mprList = purchaseDAO.getMprDetail(mprId);
        if (mprList.size() == 0) {
            //////System.out.println("test1");
            throw new FPBusinessException("EM-MPR-04");
        } else {
            return mprList;
        }
    }

    public void processMprApprove(PurchaseTO purchaseTO)
            throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = purchaseDAO.doMprApprove(purchaseTO);
        if (status == 0) {
            throw new FPBusinessException("EM-MPR-05");
        } else {
            return;
        }
    }

    public void processVendorItem(String vendorId, String itemId, String unitPrice, String vatId, String buyPrice, String discount, int userId)
            throws FPBusinessException, FPRuntimeException {
        System.out.println("Bp..");
        int status = purchaseDAO.processVendorItem(vendorId, itemId, unitPrice,vatId, buyPrice, discount, userId);
    }
    public int poIdValue = 0;
    public ArrayList processGeneratePO(int mprId, int userId, int companyId,String igst,String cgst,String sgst,
            String paymentDays,String shipVia,String 
            warranty,String freightTerms,String remarks)
            throws FPBusinessException, FPRuntimeException {
        int status = 0;
        int vendorId = 0;
        int poId = 0;
        String vehicleNo = null;
        String usageType = null;
        ArrayList mprList = new ArrayList();
        PurchaseTO purch = new PurchaseTO();
        ArrayList poDetail = new ArrayList();
        ArrayList poDetailList = new ArrayList();
        mprList = purchaseDAO.getMprApproveQty(mprId);
        String itemId[] = new String[mprList.size()];
        String reqQty[] = new String[mprList.size()];
        String price[] = new String[mprList.size()];
        String unitPrice[] = new String[mprList.size()];
        String discount[] = new String[mprList.size()];
        String vat[] = new String[mprList.size()];
        Iterator itr = mprList.iterator();
        for (int i = 0; itr.hasNext(); i++) {
            purch = (PurchaseTO) itr.next();
            itemId[i] = String.valueOf(purch.getItemId());
            reqQty[i] = String.valueOf(purch.getApprovedQty());
            price[i] = String.valueOf(purch.getBuyPrice());
            unitPrice[i] = String.valueOf(purch.getUnitPrice());
            discount[i] = String.valueOf(purch.getDiscount());
//            vat[i] = String.valueOf(purch.getVat());
            vat[i] = String.valueOf(Integer.parseInt(sgst)+Integer.parseInt(cgst)+Integer.parseInt(igst));
            vendorId = purch.getVendorId();
        }

        purch = new PurchaseTO();
        purch.setVendorId(vendorId);
        purch.setMprId(mprId);
        purch.setUserId(userId);
        purch.setItemIds(itemId);
        purch.setBuyPrices(price);
        purch.setUnitPrices(unitPrice);
        purch.setDiscounts(discount);
        purch.setTaxs(vat);

        purch.setApprovedQtys(reqQty);
        poId = purchaseDAO.doGeneratePo(purch,igst,cgst,sgst,paymentDays,shipVia,warranty,freightTerms,remarks);
        poIdValue = poId;
//        bala
        vehicleNo = purchaseDAO.getVehicleNo(poId);
        usageType = purchaseDAO.getUsageType(vehicleNo);
//        balaends
        purchaseDAO.doMprInactive(purch);
        if (poId == 0) {
            throw new FPBusinessException("EM-PO-01");
        } else {
            System.out.println((new StringBuilder()).append("search for Po=").append(poId).toString());
            poDetail = processPoDetail(poId, companyId);
            PurchaseTO vehicleDetails = null;
            for (Iterator itr1 = poDetail.iterator(); itr1.hasNext(); poDetailList.add(vehicleDetails)) {
                vehicleDetails = new PurchaseTO();
                vehicleDetails = (PurchaseTO) itr1.next();
                vehicleDetails.setVehicleNo(vehicleNo);
                vehicleDetails.setUsageType(usageType);
            }
            return poDetailList;
        }
    }

    public ArrayList processPoDetail(int poId, int companyId)
            throws FPBusinessException, FPRuntimeException {
        ArrayList poDetail = new ArrayList();
        ArrayList newPoDetail = new ArrayList();
        PurchaseTO purch = new PurchaseTO();
        poDetail = purchaseDAO.doGetPoDetail(poId, companyId);
        for (Iterator itr = poDetail.iterator(); itr.hasNext(); newPoDetail.add(purch)) {
            purch = new PurchaseTO();
            purch = (PurchaseTO) itr.next();
            purch.setAddress(addressSplit(purch.getVendorAddress()));
            purch.setSplitRemarks(remarksSplit(purch.getRemarks(), 50));
        }

        return newPoDetail;
    }
    public ArrayList processMprDetail(int mprId, int companyId)
            throws FPBusinessException, FPRuntimeException {
        ArrayList poDetail = new ArrayList();
        ArrayList newPoDetail = new ArrayList();
        PurchaseTO purch = new PurchaseTO();
        poDetail = purchaseDAO.processMprDetail(mprId, companyId);
        for (Iterator itr = poDetail.iterator(); itr.hasNext(); newPoDetail.add(purch)) {
            purch = new PurchaseTO();
            purch = (PurchaseTO) itr.next();
            purch.setAddress(addressSplit(purch.getVendorAddress()));
            purch.setSplitRemarks(remarksSplit(purch.getRemarks(), 50));
        }

        return newPoDetail;
    }

    public int processCancelPo(int poId)
            throws FPBusinessException, FPRuntimeException {
        int stat = 0;
        stat = purchaseDAO.doCancelPo(poId);
        return stat;
    }

    public ArrayList processGetRequiredItems(PurchaseTO compTO, int companyId, int startIndex, int endIndex)
            throws FPBusinessException, FPRuntimeException {
        ArrayList itemList = new ArrayList();
        ArrayList RequiredItems = new ArrayList();
        ArrayList poRaisedQty = new ArrayList();
        float vendorPoQty = 0.0F;
        float localPoQty = 0.0F;
        float totalQty = 0.0F;
        PurchaseTO purch = new PurchaseTO();
        MrsTO puchTO = new MrsTO();
        MrsTO puchTO1 = new MrsTO();
        itemList = purchaseDAO.getRequiredItems(compTO, startIndex, endIndex);
        if (itemList.size() != 0);
        for (Iterator itr = itemList.iterator(); itr.hasNext(); RequiredItems.add(purch)) {
            purch = (PurchaseTO) itr.next();
            puchTO = new MrsTO();
            puchTO.setMrsItemId(String.valueOf(purch.getItemId()));
            puchTO.setCompanyId(companyId);
            poRaisedQty = mrsDAO.getPoRaisedQty(puchTO);
            Iterator poItr = poRaisedQty.iterator();
            do {
                if (!poItr.hasNext()) {
                    break;
                }
                puchTO1 = new MrsTO();
                puchTO1 = (MrsTO) poItr.next();
                if (puchTO1.getPurchaseType().equals("1011")) {
                    localPoQty = Float.parseFloat(puchTO1.getQty());
                } else if (puchTO1.getPurchaseType().equals("1012")) {
                    vendorPoQty = Float.parseFloat(puchTO1.getQty());
                }
            } while (true);
            totalQty = localPoQty + vendorPoQty;
            purch.setPoRaisedQty(String.valueOf(totalQty));
            totalQty = 0.0F;
        }

        return RequiredItems;
    }

    public ArrayList processTotalGetRequiredItems(PurchaseTO compTO, int companyId, int startIndex, int endIndex)
            throws FPBusinessException, FPRuntimeException {
        ArrayList itemList = new ArrayList();
        itemList = purchaseDAO.getRequiredItems(compTO, startIndex, endIndex);
        return itemList;
    }

    public ArrayList processVendorItems(int vendorTypeId)
            throws FPBusinessException, FPRuntimeException {
        ArrayList vendorItemList = new ArrayList();
        vendorItemList = purchaseDAO.getVendorItemData(vendorTypeId);
        return vendorItemList;
    }

    public ArrayList processVendorMfrCat(int vendorTypeId)
            throws FPBusinessException, FPRuntimeException {
        ArrayList vendorItemList = new ArrayList();
        vendorItemList = purchaseDAO.getVendorMfrCatData(vendorTypeId);
        return vendorItemList;
    }

    public void processGenerateStRequest(PurchaseTO purch)
            throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = purchaseDAO.doGenerateStRequest(purch);
        if (status == 0) {
            throw new FPBusinessException("EM-PO-01");
        } else {
            return;
        }
    }

    public String processPoList(PurchaseTO purchaseTO)
            throws FPBusinessException, FPRuntimeException {
        ArrayList purchaseOrderList = new ArrayList();
        String poList = "";
        int counter = 0;
        purchaseOrderList = purchaseDAO.getPOList(purchaseTO);
        if (purchaseOrderList.size() == 0) {
            poList = "";
        } else {
            for (Iterator itr = purchaseOrderList.iterator(); itr.hasNext();) {
                purchaseTO = new PurchaseTO();
                purchaseTO = (PurchaseTO) itr.next();
                if (counter == 0) {
                    System.out.println((new StringBuilder()).append(" purchaseTO.getPoId()").append(purchaseTO.getPoId()).toString());
                    poList = String.valueOf(purchaseTO.getPoId());
                    counter++;
                } else {
                    poList = (new StringBuilder()).append(poList).append("~").append(String.valueOf(purchaseTO.getPoId())).toString();
                }
            }

        }
        return poList;
    }

    public ArrayList processItemList(PurchaseTO purchaseTO)
            throws FPBusinessException, FPRuntimeException {
        ArrayList itemList = new ArrayList();
        ArrayList supplyList = new ArrayList();
        ArrayList processedList = new ArrayList();
        int requiredQty = 0;
        itemList = purchaseDAO.getItemList(purchaseTO);
        supplyList = purchaseDAO.getAcceptedItemsQty(purchaseTO);
        Iterator orderItr = itemList.iterator();
        if (supplyList.size() != 0) {
            PurchaseTO purchTO;
            for (; orderItr.hasNext(); processedList.add(purchTO)) {
                purchTO = new PurchaseTO();
                purchTO = (PurchaseTO) orderItr.next();
                Iterator supplyItr = supplyList.iterator();
                do {
                    if (!supplyItr.hasNext()) {
                        break;
                    }
                    PurchaseTO supplyTO = new PurchaseTO();
                    supplyTO = (PurchaseTO) supplyItr.next();
                    System.out.println((new StringBuilder()).append("compare=").append(supplyTO.getItemId()).append(" and ").append(purchTO.getItemId()).toString());
                    System.out.print((new StringBuilder()).append("ordered=").append(purchTO.getApprovedQty()).append(" supplied ").append(supplyTO.getAcceptedQty()).toString());
                    if (supplyTO.getItemId() == purchTO.getItemId()) {
                        if (Float.parseFloat(purchTO.getApprovedQty()) > Float.parseFloat(supplyTO.getAcceptedQty())) {
                            System.out.println((new StringBuilder()).append(supplyTO.getItemId()).append("-> ordered ").append(purchTO.getApprovedQty()).append("- issues=").append(supplyTO.getAcceptedQty()).append(" bal=").append(requiredQty).toString());
                            purchTO.setAcceptedQty(supplyTO.getAcceptedQty());
                        } else if (Float.parseFloat(purchTO.getApprovedQty()) <= Float.parseFloat(supplyTO.getAcceptedQty())) {
                            purchTO.setAcceptedQty(supplyTO.getAcceptedQty());
                            //////System.out.println("setting pod item to inactive");
                            purchaseDAO.doPoItemInactive(supplyTO);
                        }
                    }
                } while (true);
            }

        } else {
            System.out.println((new StringBuilder()).append("processes list size-").append(processedList.size()).toString());
            processedList.addAll(itemList);
        }
        purchaseDAO.doPoInactive(purchaseTO);
        if (itemList.size() != 0);
        return processedList;
    }

    public int processInsertInVoice(PurchaseTO purchaseTO, int userId)
            throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = purchaseDAO.insertInVoice(purchaseTO, userId);
//        if (status == 0) {
         //   throw new FPBusinessException("EM-PR-02");
//        } else {
            return status;
//        }
    }
    public int processUpdateInVoiceDiscount(PurchaseTO purchaseTO, int userId)
            throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = purchaseDAO.processUpdateInVoiceDiscount(purchaseTO, userId);
//        if (status == 0) {
         //   throw new FPBusinessException("EM-PR-02");
//        } else {
            return status;
//        }
    }

    public int processInsertItem(ArrayList List, int userId, PurchaseTO purchaseTO)
            throws FPBusinessException, FPRuntimeException {
        int status = 0;
        System.out.println((new StringBuilder()).append("list size = ").append(List.size()).toString());
        status = purchaseDAO.insertItem(List, userId, purchaseTO);
        if (status == 0) {
            throw new FPBusinessException("EM-PR-02");
        } else {
            return status;
        }
    }

    public int processInsertDCItems(ArrayList List, int userId, PurchaseTO purchaseTO)
            throws FPBusinessException, FPRuntimeException {
        int status = 0;
        System.out.println((new StringBuilder()).append("list size = ").append(List.size()).toString());
        status = purchaseDAO.insertDCItems(List, userId, purchaseTO);
        if (status != 0);
        return status;
    }

    public int insertInVoiceReturnedQty(ArrayList List, int userId)
            throws FPBusinessException, FPRuntimeException {
        int status = 0;
        System.out.println((new StringBuilder()).append("list size = ").append(List.size()).toString());
        status = purchaseDAO.insertInVoiceReturnedQty(List, userId);
        if (status != 0);
        return status;
    }

    public ArrayList processGrnDetails(PurchaseTO purchaseTO)
            throws FPBusinessException, FPRuntimeException {
        ArrayList supply = new ArrayList();
        supply = purchaseDAO.getGrnDetails(purchaseTO);
        System.out.println((new StringBuilder()).append("supply list size = ").append(supply.size()).toString());
        if (supply.size() != 0);
        return supply;
    }
    public int doInvoiceDetails(PurchaseTO purchaseTO)
            throws FPBusinessException, FPRuntimeException {
        int status = purchaseDAO.doInvoiceDetails(purchaseTO);
        return status;
    }

    public void processUpdateGrn(PurchaseTO purchaseTO, int userId) throws FPBusinessException, FPRuntimeException {
        int itemId = 0;
        int itemLen = 0;
        int supplyId = 0;
        String dcPrice = "";
        String[] temp = null;
        int dcPriceId = 0;
        float dcStock = 0.0F;
        float accQty = 0.0F;
        float unusedQty = 0.0F;
        float reqQty = 0.0F;
        int compId = 0;
        String mrp = "";
        String tax = "";
        String discount = "";
        String amount = "";
        String unitPrice = "";
        itemLen = purchaseTO.getItemIds().length;
        supplyId = Integer.parseInt(purchaseTO.getSupplyId());
        compId = Integer.parseInt(purchaseTO.getCompanyId());
        int status = purchaseDAO.doInvoiceDetails(purchaseTO);
        for (int i = 0; i < itemLen; i++) {
            itemId = Integer.parseInt(purchaseTO.getItemIds()[i]);
            mrp = purchaseTO.getMrps()[i];
            tax = purchaseTO.getTaxs()[i];
            discount = purchaseTO.getDiscounts()[i];
            amount = purchaseTO.getItemAmounts()[i];
            unitPrice = purchaseTO.getUnitPrices()[i];
            //accQty = Float.parseFloat(purchaseTO.getAcceptedQtys()[i]);
//            bala
            accQty = Float.parseFloat(purchaseTO.getTotalQtys()[i]);
            unusedQty = Float.parseFloat(purchaseTO.getRetQtys()[i]);
            reqQty = Float.parseFloat(purchaseTO.getAcceptedQtys()[i]);
//            bala ends
//            dcPrice = purchaseDAO.getDcPriceValue(supplyId, itemId, compId, accQty);
            dcPrice = purchaseDAO.getDcPriceValue(supplyId, itemId, compId, accQty, reqQty);
            //////System.out.println("dcPrice" + dcPrice);
            temp = dcPrice.split("-");
            dcStock = Float.parseFloat(temp[1]);            
            status = purchaseDAO.doInvoiceItemDetails(purchaseTO, userId, dcStock, compId, mrp, tax, discount, amount, unitPrice, itemId, accQty, unusedQty, temp[0]);
        }
        //Hari
        ArrayList itemList = new ArrayList();
        ArrayList supplyList = new ArrayList();
        ArrayList processedList = new ArrayList();
        int requiredQty = 0;
        int flag = 0;
        itemList = purchaseDAO.getItemList(purchaseTO);
        supplyList = purchaseDAO.getAcceptedItemsQty(purchaseTO);
        Iterator orderItr = itemList.iterator();
        if (supplyList.size() != 0) {
            PurchaseTO purchTO;
            for (; orderItr.hasNext(); processedList.add(purchTO)) {
                purchTO = new PurchaseTO();
                purchTO = (PurchaseTO) orderItr.next();
                Iterator supplyItr = supplyList.iterator();
                do {
                    if (!supplyItr.hasNext()) {
                        break;
                    }
                    PurchaseTO supplyTO = new PurchaseTO();
                    supplyTO = (PurchaseTO) supplyItr.next();
                    System.out.println((new StringBuilder()).append("compare=").append(supplyTO.getItemId()).append(" and ").append(purchTO.getItemId()).toString());
                    System.out.print((new StringBuilder()).append("ordered=").append(purchTO.getApprovedQty()).append(" supplied ").append(supplyTO.getAcceptedQty()).toString());
                    if (supplyTO.getItemId() == purchTO.getItemId()) {
                        if (Float.parseFloat(purchTO.getApprovedQty()) == Float.parseFloat(supplyTO.getAcceptedQty())) {
                            //////System.out.println("All items Received");
                            flag = 1;
                        }
                    }
                } while (true);
            }

        } else {
            System.out.println((new StringBuilder()).append("processes list size-").append(processedList.size()).toString());
            processedList.addAll(itemList);
        }
        //Hari End
        //////System.out.println("Flag Value-->" + flag);
        if (flag == 1) {
            //////System.out.println("In Flag Block");
            status = purchaseDAO.doPurchaseDetails(purchaseTO.getPoId());
            //////System.out.println("Status-->" + status);
        }
        
    }

    public void processTyreSupply(ArrayList tyreList)
            throws FPBusinessException, FPRuntimeException {
        int status = purchaseDAO.doInsertTyreSupply(tyreList);
        if (status != 0);
    }

    public ArrayList getTyreSupplyDetails(PurchaseTO purchaseTO)
            throws FPBusinessException, FPRuntimeException {
        ArrayList supply = new ArrayList();
        supply = purchaseDAO.getTyreSupplyDetails(purchaseTO);
        System.out.println((new StringBuilder()).append("supply list size = ").append(supply.size()).toString());
        return supply;
    }

    public void processTyreDetail(ArrayList tyreList, int userId)
            throws FPBusinessException, FPRuntimeException {
        int status = purchaseDAO.doInsertTyreDetail(tyreList, userId);
        if (status != 0);
    }

    public String[] addressSplit(String longText) {
        String splitText[] = null;
        int lower = 0;
        int splitSize = poTextSplitSize;
        int size = longText.length();
        splitText = new String[size / splitSize + 1];
        for (int i = 0; i < size / splitSize + 1; i++) {
            int higher;
            if (size - lower < splitSize) {
                higher = size;
            } else {
                higher = lower + splitSize;
            }
            splitText[i] = longText.substring(lower, higher);
            System.out.println(splitText[i]);
            lower += splitSize;
        }

        return splitText;
    }

    public String[] remarksSplit(String longText, int textLength) {
        String splitText[] = null;
        int lower = 0;
        int splitSize = textLength;
        int size = longText.length();
        splitText = new String[size / splitSize + 1];
        for (int i = 0; i < size / splitSize + 1; i++) {
            int higher;
            if (size - lower < splitSize) {
                higher = size;
            } else {
                higher = lower + splitSize;
            }
            splitText[i] = longText.substring(lower, higher);
            System.out.println(splitText[i]);
            lower += splitSize;
        }

        return splitText;
    }

    public ArrayList processVatList()
            throws FPRuntimeException, FPBusinessException {
        ArrayList MfrList = new ArrayList();
        MfrList = purchaseDAO.getVatList();
        if (MfrList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        } else {
            return MfrList;
        }
    }

    public ArrayList processActiveVatList()
            throws FPRuntimeException, FPBusinessException {
        ArrayList ModelList = new ArrayList();
        ArrayList activeModelList = new ArrayList();
        PurchaseTO purchTO = null;
        ModelList = purchaseDAO.getVatList();
        Iterator itr = ModelList.iterator();
        do {
            if (!itr.hasNext()) {
                break;
            }
            purchTO = new PurchaseTO();
            purchTO = (PurchaseTO) itr.next();
            if (purchTO.getActiveInd().equalsIgnoreCase("y")) {
                activeModelList.add(purchTO);
            }
        } while (true);
        return activeModelList;
    }

    public int processModifyVatDetails(ArrayList List, int UserId)
            throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = purchaseDAO.doAlterVatDetails(List, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-MFR-02");
        } else {
            return insertStatus;
        }
    }

    public int processInsertVat(PurchaseTO purchTO, int UserId)
            throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = purchaseDAO.doInsertVatDetails(purchTO, UserId);
        return insertStatus;
    }

    public ArrayList getPurchaseorderItems(int poId)
            throws FPRuntimeException, FPBusinessException {
        ArrayList poItems = new ArrayList();
        poItems = purchaseDAO.getPurchaseorderItems(poId);
        return poItems;
    }

    public void doPoModify(PurchaseTO purch)
            throws FPRuntimeException, FPBusinessException {
        purchaseDAO.doPoModify(purch);
    }

    public void deletePoItems(PurchaseTO purch)
            throws FPRuntimeException, FPBusinessException {
        purchaseDAO.deletePoItems(purch);
    }

    public int modifyPOAndMprHeader(PurchaseTO purch)
            throws FPRuntimeException, FPBusinessException {
        int status = purchaseDAO.modifyPOAndMprHeader(purch);
        return status;
    }

    public int alterGrnHeaderInfo(PurchaseTO purch)
            throws FPRuntimeException, FPBusinessException {
        int status = purchaseDAO.alterGrnHeaderInfo(purch);
        return status;
    }

    public ArrayList getVendorPayments(PurchaseTO purch)
            throws FPRuntimeException, FPBusinessException {
        ArrayList vendorPayments = new ArrayList();
        vendorPayments = purchaseDAO.getVendorPayments(purch);
        return vendorPayments;
    }

    public int addPaymentDetails(PurchaseTO purch)
            throws FPRuntimeException, FPBusinessException {
        int stat = 0;
        stat = purchaseDAO.addPaymentDetails(purch);
        return stat;
    }

    public int updatePaymentDetails(PurchaseTO purch)
            throws FPRuntimeException, FPBusinessException {
        int stat = 0;
        stat = purchaseDAO.updatePaymentDetails(purch);
        return stat;
    }
    private PurchaseDAO purchaseDAO;
    private MrsDAO mrsDAO;
    static FPUtil fpUtil;
    static final int poTextSplitSize = Integer.parseInt(FPUtil.getInstance().getProperty("PO_ADDRESS_SPLIT"));

    static {
        fpUtil = FPUtil.getInstance();
        FPUtil _tmp = fpUtil;
    }

       public ArrayList getBarcodePrintItemList(String poId)
            throws FPBusinessException, FPRuntimeException {
        ArrayList itemList = new ArrayList();
        itemList = purchaseDAO.getBarcodePrintItemList(poId);
            return itemList;

    }
}
