<%-- 
    Document   : newAccountType
    Created on : 31 Oct, 2012, 5:15:03 PM
    Author     : ASHOK
--%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Add Account Type</title>
<link href="/throttle/css/parveen.css" rel="stylesheet"/>

<script language="javascript" src="/throttle/js/validate.js"></script>
</head>
<script>
  function submitPage()
    {
        if(textValidation(document.add.accountEntryTypeName,'accountEntryTypeName')){
            return;
        }
        if(textValidation(document.add.accountEntryTypeCode,'accountEntryTypeCode')){
            return;
        }
        if(textValidation(document.add.description,'description')){
            return;
        }
        document.add.action='/throttle/saveNewAccountType.do';
        document.add.submit();
}
function setFocus(){
    document.add.groupName.focus();
    }

</script>
<body onload="setFocus();">
<form name="add" method="post">
<%@ include file="/content/common/path.jsp" %>

<%@ include file="/content/common/message.jsp" %>

<table align="center" width="500" border="0" cellspacing="0" cellpadding="0" class="border">
 <tr height="30">
  <Td colspan="2" class="contenthead">Add Account Type</Td>
 </tr>
  <tr height="30">
      <td class="text2"><font color="red">*</font>Account Type Name</td>
    <td class="text2"><input name="accountEntryTypeName" type="text" class="textbox" value="" size="20"></td>
  </tr>
  <tr height="30">
    <td class="text1"><font color="red">*</font>Account Type Code</td>
    <td class="text2"><input name="accountEntryTypeCode" type="text" class="textbox" value="" size="20"></td>
  </tr>
  <tr height="30">
    <td class="text2"><font color="red">*</font>Description</td>
    <td class="text2"><input name="description" type="text" class="textbox" value="" size="20"></td>
  </tr>
</table>
<br>
<br>
<center>
    <input type="button" class="button" value="Save" onclick="submitPage();" />
    &emsp;<input type="reset" class="button" value="Clear">
</center>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>

