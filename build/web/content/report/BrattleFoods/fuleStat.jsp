
<%-- 
    Document   : commercialReport
    Created on : 23 Jun, 2015, 10:25:59 AM
    Author     : entitle
--%>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
       <meta http-equiv="content-type" content="text/html; charset=UTF-8">


        <script type='text/javascript' src='//code.jquery.com/jquery-1.9.1.js'></script>
        <link rel="stylesheet" type="text/css" href="/css/result-light.css">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet.http.HttpServletRequest" %>

        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <style type="text/css" title="currentStyle">
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

        <script type='text/javascript' src='//code.jquery.com/jquery-1.9.1.js'></script>
        <link rel="stylesheet" type="text/css" href="/css/result-light.css">


        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
<!--        <script src="/throttle/js/highcharts.js"></script>
        <script src="/throttle/js/exporting.js"></script>-->
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>


<script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>


        <script type="text/javascript">
            function submitPage(value) {
//                alert(value)
                document.commercialRep.action = '/throttle/getFuelStat.do?param=' + value;
                document.commercialRep.submit();
            }


        </script>

     <div class="pageheader">
    <h2><i class="fa fa-edit"></i> Report</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Report</a></li>
            <li class="active">Fuel statistic</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body onload="showGraph2();">
        <form name="commercialRep" method="post">
             <table class="table table-info mb30 table-hover" style="width:100%">
                <tr>
                    <td colspan="6" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">
                      
                            <b>Fuel statistic Report  </b>
                </tr>
                                

                                    <tr>


                                      <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" style="width:129px;" value="<c:out value="${fromDate}"/>" ></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" style="width:129px;" value="<c:out value="${toDate}"/>"></td>
                                    <td><input type="button" class="btn btn-info" name="search" onclick="submitPage(this.name);" value="search"></td>&nbsp;&nbsp;
                                    <td>
                                        <!--<input type="button" class="btn btn-info" name="ExportExcel" onclick="submitPage(this.name);" value="ExportExcel">-->
                                    </td>&nbsp;&nbsp;
                                    

                                    </tr>
                              
                            
                       
                  
             
            </table>
            <br>
            <br>
           
                <table  border="0" id="table" class="sortable" width="250px;" >
                    <tr></tr>
<tr>
    <td colspan="2">
        <table  border="0" id="table"  width="350px;" >
            

<tr height="40"> <th colspan="4" align="center"><h2>Fuel Statistics  :  </h2></th></tr>
<tr>
   
    <td colspan="4"><div id="container9" style="min-width: 1050px; height: 400px; margin: 0 auto"></div></td>
    
</tr>
</table></td>
</tr>

</table>

<br/>
<script>

    function showGraph2() {
       // alert("Hi I Am Here 1");
        
        var fromDate = document.getElementById("fromDate").value;
        var toDate = document.getElementById("toDate").value;
       var fuelPrice="",fuelDate="";
     
            $.ajax({
                url: '/throttle/getFuelDataAjax.do?',
                data: {fromDate: fromDate, toDate: toDate},
                dataType: 'json',
                success: function (data) {
                    $.each(data, function (j, data) {
                       // alert("data.ajaxData1 "+data.ajaxData);
                        if (data.ajaxData != 'undefined') {
                            if (data.ajaxData.length > 0) {
                                $.each(data.ajaxData, function (key, value) {
                                   
                                   
                                    fuelPrice = value.fuelPrice;
                                    fuelDate = value.fuelDate;
                                //    alert(fuelDate  + "~~~"+fuelPrice);
                                });

                                var fuelDateTmp = fuelDate.split(',');
                                var fuelPriceTmp = fuelPrice.split(',');
                                
                                var fuelDateSeries = [];
                                var fuelPriceSeries = [];
                               
                                for (var i = 0; i < fuelDateTmp.length; i++) {
                                    fuelPriceSeries.push([parseFloat(fuelPriceTmp[i])]);
                                    fuelDateSeries.push([fuelDateTmp[i]]);
                                }

                              //  alert("fuelDateSeries " + fuelDateSeries);
                              //  alert("fuelPriceSeries " + fuelPriceSeries);
                                
                                Highcharts.chart('container9', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Fuel statistics'
    },
    xAxis: {
        categories: fuelDateSeries,
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Fuel Price (INR)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f}INR</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Fuel Price Date',
        data: fuelPriceSeries

    }],
credits: {
                                        enabled: false
                                    },
                                    exporting: {
                                        enabled: false
                                    }
});
                                

//                                $('#container9').highcharts({
//                                    title: {
//                                        text: 'Fuel Rate'
//                                    },
//                                    xAxis: {
//                                        categories: fuelDateSeries
//                                    },
//                                    labels: {
//                                        items: [{
//                                                html: '',
//                                                style: {
//                                                    left: '50px',
//                                                    top: '18px',
//                                                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
//                                                }
//                                            }]
//                                    },
//                                    series: [{
//                                            type: 'column',
//                                            name: 'Fuel Price',
//                                            data: fuelPriceSeries
//
//                                        }],
//                                    credits: {
//                                        enabled: false
//                                    },
//                                    exporting: {
//                                        enabled: false
//                                    }
//                                });
                             
                               


                            }
                        }
                    });

                }
            });
       
    }
  





</script>


<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
</form>
</div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
