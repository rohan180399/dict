
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<!--<script language="javascript" src="/throttle/js/validate.js"></script>-->  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
        
          <script>
        function submitPage()
        {
            
        if(textValidation(document.addbank.branchName,'Branch Name')){
            return;
        }
        if(textValidation(document.addbank.bankCode,'Bank Code')){
            return;
        }
        if(textValidation(document.addbank.address,'Address')){
            return;
        }
        if(textValidation(document.addbank.phoneNo,'Phone No.')){
            return;
        }
        if(textValidation(document.addbank.accntCode,'Account No.')){
            return;
        }
        if(textValidation(document.addbank.IFSCCode,'IFSCCode')){
            return;
        }
        if(textValidation(document.addbank.BSRCode,'BSRCode')){
            return;
        }
        if(textValidation(document.addbank.description,'Description')){
            return;
        }

        document.addbank.action='/throttle/saveNewBank.do';
        document.addbank.submit();
}

        function setValues(sno, branchName, bankcode, address, phoneNo, accntCode, bankMappingCode, description, activeind, branchId, IFSCCode, BSRCode) {
                
                var count = parseInt(document.getElementById("count").value);
                
                for (i = 1; i <= count; i++) {
                    if (i != sno) {
                        document.getElementById("edit" + i).checked = false;
                    } else {
                        document.getElementById("edit" + i).checked = true;
                    }
                }
                
                document.getElementById("branchName").value = branchName;
                document.getElementById("bankCode").value = bankcode;
                document.getElementById("phoneNo").value = phoneNo;
                document.getElementById("address").value = address;
                document.getElementById("accntCode").value = accntCode;                
                document.getElementById("description").value = description;
                document.getElementById("branchId").value = branchId;
                document.getElementById("IFSCCode").value = IFSCCode;
                document.getElementById("BSRCode").value = BSRCode; 
            }

            var httpRequest;
            function checkBankAccountCode() {

                var bankCode = document.getElementById('bankCode').value;
                
                var url = '/throttle/checkBankAccountCode.do?bankCode=' + bankCode;
                if (window.ActiveXObject) {
                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                } else if (window.XMLHttpRequest) {
                    httpRequest = new XMLHttpRequest();
                }
                httpRequest.open("GET", url, true);
                httpRequest.onreadystatechange = function() {
                    processRequest();
                };
                httpRequest.send(null);

            }


            function processRequest() {
                if (httpRequest.readyState == 4) {
                    if (httpRequest.status == 200) {
                        var val = httpRequest.responseText.valueOf();
                        if (val != "" && val != 'null') {
                            $("#bankCodeStatus").show();
                            $("#bankCodeisValid").text('Please Check Bank Code  :' + val + ' is Already Exists');
                            document.getElementById("bankCode").value="";
                            document.getElementById("bankCode").focus();
                        } else {
                            $("#bankCodeStatus").hide();
                            $("#bankCodeisValid").text('');
                        }
                    } else {
                        alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
                    }
                }
            }
</script>
 <style>
    #index td {
   color:white;
}
</style>

<script>
	   function changePageLanguage(langSelection){
	   if(langSelection== 'ar'){
	   document.getElementById("pAlign").style.direction="rtl";
	   }else if(langSelection== 'en'){
	   document.getElementById("pAlign").style.direction="ltr";
	   }
	   }
	 </script>

	  <c:if test="${jcList != null}">
	  <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
	  </c:if>

<!--<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">-->
    <body>
        <table class="table table-info mb30 table-hover">
                    
                    <tr height="30" id="index">
                        <td colspan="4" style="background-color:#5BC0DE;"><b><spring:message code="finance.label.AddBankBranch"  text="default text"/></b></td>&nbsp;&nbsp;&nbsp;                    
                    </tr>
                    
                    <tr>
                        <td  colspan="4" align="center" style="display: none" id="bankCodeStatus"><label id="bankCodeisValid" style="color: red"></label></td>
                    </tr>
                    
                    <tr height="30">
                        <td ><font color="red">*</font><spring:message code="finance.label.BankName"  text="default text"/></td>
                        <td ><c:out value="${bankName}" />
                        <input name="bankName" id="bankName" type="hidden" class="form-control" value="<c:out value="${bankName}" />" style="width:260px;height:40px;" >
                        </td>
                        <td ><font color="red">*</font><spring:message code="finance.label.BranchName"  text="default text"/></td>
                        <td ><input name="branchName" id="branchName" type="text" class="form-control" value="" style="width:260px;height:40px;"></td>
                    </tr>
                
                    <tr height="30">
                        <td ><font color="red">*</font><spring:message code="finance.label.BranchCode"  text="default text"/></td>
                        <td ><input id="bankCode" name="bankCode" type="text" class="form-control" value="" style="width:260px;height:40px;" onchange="checkBankAccountCode();"  ></td>
                    
                        <td ><font color="red">*</font><spring:message code="finance.label.PhoneNo"  text="default text"/></td>
                        <td ><input id="phoneNo" name="phoneNo" type="text" class="form-control" value="" style="width:260px;height:40px;" maxlength="12"  onKeyPress='return onKeyPressBlockCharacters(event);' ></td>                    
                    </tr>
                    
                    <tr height="30">
                        <td ><font color="red">*</font><spring:message code="finance.label.BranchAddress"  text="default text"/></td>
                        <td >
                            <textarea class="form-control" id="address" name="address" style="width:260px;height:40px;"></textarea>
                        </td>
               
                        <td ><font color="red">*</font><spring:message code="finance.label.AccountNo"  text="default text"/></td>
                        <td ><input id="accntCode" name="accntCode" type="text" class="form-control" value=""style="width:260px;height:40px;"  onKeyPress='return onKeyPressBlockCharacters(event);' ></td>
                    </tr> 
                    <tr height="30">
                        <td ><font color="red">*</font><spring:message code="finance.label.IFSCCode"  text="default text"/></td>
                        <td ><input id="IFSCCode" name="IFSCCode" type="text" class="form-control" value="" style="width:260px;height:40px;"  ></td>
               
                        <td ><font color="red">*</font><spring:message code="finance.label.BSRCode"  text="default text"/></td>
                        <td ><input id="BSRCode" name="BSRCode" type="text" class="form-control" value="" style="width:260px;height:40px;"  ></td>
                    </tr>
                    <tr height="30">
                        <td ><font color="red">*</font><spring:message code="finance.label.Description"  text="default text"/></td>
                        <td ><textarea name="description" id="description" type="text" class="form-control" style="width:260px;height:40px;"  ></textarea></td>
                        <td >&nbsp;</td>
                        <td >&nbsp;</td>
                    </tr>
                    <input name="bankid" id="bankid" type="hidden" class="form-control" value="<c:out value="${bankId}" />" size="50"  >
                    <input type="hidden" id="branchId" name="branchId" value=""/>
                </table>

            <center>
                <input type="button" class="btn btn-success" style="width:100px;height:35px;" value="<spring:message code="finance.label.Save"  text="default text"/>" onclick="submitPage();" />
                &emsp;<input type="reset" class="btn btn-success" style="width:100px;height:35px;" value="<spring:message code="finance.label.Clear"  text="default text"/>">
            </center>
    <br>
                <c:if test = "${bankBranchLists != null}" >
                    <table class="table table-info mb30 table-hover" id="table">

                        <thead height="30"> 
                            <th><spring:message code="finance.label.Sno"  text="default text"/></th>
                            <th><spring:message code="finance.label.BranchName"  text="default text"/></th>
                            <th><spring:message code="finance.label.BranchCode"  text="default text"/></th>
                            <th><spring:message code="finance.label.BranchAddress"  text="default text"/></th>
                            <th><spring:message code="finance.label.BranchPhoneNo"  text="default text"/></th>
                            <th><spring:message code="finance.label.BranchAccountNo"  text="default text"/></th>
                            <th><spring:message code="finance.label.BranchLedgerCode"  text="default text"/></th>
                            <th><spring:message code="finance.label.IFSCCode"  text="default text"/></th>
                            <th><spring:message code="finance.label.BSRCode"  text="default text"/></th>
                            <th><spring:message code="finance.label.Description"  text="default text"/></th>
                            <th><spring:message code="finance.label.Status"  text="default text"/></th>
                            <th><spring:message code="finance.label.Edit"  text="default text"/></th>
                            
                        </thead>
                        <% int index = 0; int sno=0;%>
                        <c:forEach items="${bankBranchLists}" var="BL">
                            
                            <tr height="30">
                                <td   align="left"> <%= index + 1 %> </td>
                                <td  align="left">  <c:out value="${BL.branchName}" /></td>
                                <td   align="left"> <c:out value="${BL.bankcode}"/> </td>
                                <td   align="left"> <c:out value="${BL.address}"/> </td>
                                <td   align="left"> <c:out value="${BL.phoneNo}"/> </td>
                                <td   align="left"> <c:out value="${BL.accntCode}"/> </td>
                                <td   align="left"> <c:out value="${BL.bankMappingCode}"/> </td>
                                <td   align="left"> <c:out value="${BL.IFSCCode}"/> </td>
                                <td   align="left"> <c:out value="${BL.BSRCode}"/> </td>
                                <td   align="left"> <c:out value="${BL.description}"/> </td>
                                <c:if test="${BL.activeind == 'Y'}">
                                    <td   height="30"><c:out value="Active"/></td>
                                </c:if>
                                <c:if test="${BL.activeind == 'N'}">
                                    <td   height="30"><c:out value="In Active"/></td>
                                </c:if>
                                    
                                    <td  align="left"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%= index + 1%>, '<c:out value="${BL.branchName}" />', '<c:out value="${BL.bankcode}"/>', '<c:out value="${BL.address}"/>', '<c:out value="${BL.phoneNo}"/>', '<c:out value="${BL.accntCode}"/>', '<c:out value="${BL.bankMappingCode}"/>', '<c:out value="${BL.description}"/>', '<c:out value="${BL.activeind}"/>', '<c:out value="${BL.branchId}"/>','<c:out value="${BL.IFSCCode}"/>','<c:out value="${BL.BSRCode}"/>');" /> </td>
                            </tr>
                            <% index++;%>
                        </c:forEach>
                            <input type="hidden" name="count" id="count" value="<%=sno%>" />
                    

                </table>
                            </c:if>
                  
            
             <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)" style="width:60px;height:20px;">
                    <option value="5"  selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span><spring:message code="finance.label.EntriesPerPage"  text="default text"/></span>
            </div>
            <div id="navigation" >
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text"><spring:message code="finance.label.DisplayingPage"  text="default text"/> <span id="currentpage"></span> <spring:message code="finance.label.of"  text="default text"/> <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>
               
    </body>
<!-- </div>
    </div>
</div>-->
<%--<%@ include file="/content/common/NewDesign/settings.jsp" %>--%>
