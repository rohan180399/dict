<%--
    Document   : alterBank
    Created on : 21 Oct, 2012, 11:54:35 PM
    Author     : ASHOK
--%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Parveen Auto Care</title>
<link href="/throttle/css/parveen.css" rel="stylesheet"/>

<script language="javascript" src="/throttle/js/validate.js"></script>
</head>
<script>
  function submitPage()
    {
        if(textValidation(document.alter.bankName,'bankName')){
            return;
        }
        if(textValidation(document.alter.bankCode,'bankCode')){
            return;
        }
        if(textValidation(document.alter.description,'description')){
            return;
        }
        if(textValidation(document.alter.address,'Address')){
            return;
        }

        if(textValidation(document.alter.accntCode,'Accntcode')){
            return;
        }

        document.alter.action='/throttle/saveAlterBank.do';
        document.alter.submit();
}


</script>
<body>
<form name="alter" method="post">
<%@ include file="/content/common/path.jsp" %>

<%@ include file="/content/common/message.jsp" %>
<c:if test="${bankalterList != null}">
    <c:forEach items="${bankalterList}" var="BL">

<table align="center" width="500" border="0" cellspacing="0" cellpadding="0" class="border">
 <tr height="30">
  <Td colspan="2" class="contenthead">Edit   Bank</Td>
 </tr>
  <tr height="30">
      <td class="text2"><font color="red">*</font>Bank Name</td>
      <td class="text2"><input name="bankName" type="hidden" class="form-control" value="<c:out value="${BL.bankname}"/>" maxlength="15" size="20"><c:out value="${BL.bankname}"/>
          <input type="hidden" name="bankid" value="<c:out value="${BL.bankid}"/>"> </td>
  </tr>
  <tr height="30">
    <td class="text1"><font color="red">*</font>Bank Code</td>
    <td class="text1"><input name="bankCode" type="text" class="form-control" value="<c:out value="${BL.bankcode}"/>" maxlength="10"  size="20"></td>
  </tr>
  <tr height="30">
    <td class="text2"><font color="red">*</font>Address</td>
    <td class="text2"><textarea name="address"><c:out value="${BL.address}"/></textarea></td>
  </tr>
   <tr height="30">
    <td class="text1"><font color="red">*</font>Phone No</td>
    <td class="text1"><input name="phoneNo" type="text" class="form-control" value="<c:out value="${BL.phoneNo}"/>" maxlength="12" size="20" onKeyPress='return onKeyPressBlockCharacters(event);'></td>
  </tr>
    <tr height="30">
    <td class="text2"><font color="red">*</font>Account No</td>
    <td class="text2"><input name="accntCode" type="text" class="form-control" value="<c:out value="${BL.accntCode}"/>" maxlength="15" size="20"></td>
  </tr>
  <tr height="30">
    <td class="text1"><font color="red">*</font>Description</td>
    <td class="text1"><input name="description" type="text" class="form-control" value="<c:out value="${BL.description}"/>" maxlength="15" size="20"></td>
  </tr>
</table>
</c:forEach>
</c:if>

<br>
<br>
<center><input type="button" class="button" value="Save" onclick="submitPage();" /></center>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
