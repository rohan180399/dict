<%-- 
    Document   : viewRouteMaster
    Created on : Nov 1, 2013, 5:01:47 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <script type="text/javascript">

           $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#cityFrom').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCityFromName.do",
                    dataType: "json",
                    data: {
                        cityFrom: request.term,
                        cityToId: document.getElementById('cityToId').value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#cityFromId').val(tmp[0]);
                $('#cityFrom').val(tmp[1]);
                return false;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

        $('#cityTo').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCityToName.do",
                    dataType: "json",
                    data: {
                        cityTo: request.term,
                        cityFromId: document.getElementById('cityFromId').value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#cityToId').val(tmp[0]);
                $('#cityTo').val(tmp[1]);
                checkRouteCode();
                return false;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        }

    });


    var httpRequest;
    function checkRouteCode() {
        var cityFromId = document.getElementById('cityFromId').value;
        var cityToId = document.getElementById('cityToId').value;
        if (cityFromId != '' && cityToId != '') {
            var url = '/throttle/checkRoute.do?cityFromId=' + cityFromId + '&cityToId=' + cityToId;
            if (window.ActiveXObject) {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest) {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("GET", url, true);
            httpRequest.onreadystatechange = function() {
                processRequest();
            };
            httpRequest.send(null);
        }
    }


    function processRequest() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var val = httpRequest.responseText.valueOf();
                if (val != "" && val != 'null') {
                    $("#routeStatus").text('' + val);
                } else {
                    $("#routeStatus").text('');
                }
            } else {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }

//            $(document).ready(function() {
//                // Use the .autocomplete() method to compile the list based on input from user
//                $('#routeCode').autocomplete({
//                    source: function(request, response) {
//                        $.ajax({
//                            url: "/throttle/getRouteCode.do",
//                            dataType: "json",
//                            data: {
//                                term: request.term
//                            },
//                            success: function(data, textStatus, jqXHR) {
//                                var items = data;
//                                response(items);
//                            },
//                            error: function(data, type) {
//                                //console.log(type);
//                            }
//                        });
//                    },
//                    minLength: 1,
//                    select: function(event, ui) {
//                        //                        $( "#itemCode" ).val( ui.item.Name);
//                        var $itemrow = $(this).closest('tr');
//                        // Populate the input fields from the returned values
//                        var value = ui.item.Name;
////                        alert(value);
//                        var tmp = value.split('-');
//                        $itemrow.find('#routeCode').val(tmp[0]);
//                        $itemrow.find('#routeId').val(tmp[1]);
//                        $('#routeNameFrom').attr('readonly', true);
//                        $('#routeNameTo').attr('readonly', true);
//                        // Give focus to the next input field to recieve input from user
//
//                        //$itemrow.find('#itemQty').focus();
//                        return false;
//                    }
//                    // Format the list menu output of the autocomplete
//                }).data("autocomplete")._renderItem = function(ul, item) {
//                    //alert(item);
//                    var itemVal = item.Name;
//                    var temp = itemVal.split('-');
//                    itemVal = '<font color="green">' + temp[0] + '</font>';
//                    return $("<li></li>")
//                            .data("item.autocomplete", item)
//                            //.append( "<a>"+ item.Name + "</a>" )
//                            .append("<a>" + itemVal + "</a>")
//                            .appendTo(ul);
//                };
//
//            });
//            
//
//            function checkValue(value,id){
//                if(value == '' && id=='routeNameFrom'){
//                   $('#routeNameTo').attr('readonly', false);
//                   $('#routeCode').attr('readonly', false);
//                   document.getElementById('routeIdFrom').value = '';
//                }
//                if(value == '' && id=='routeNameTo'){
//                   $('#routeNameFrom').attr('readonly', false);
//                   $('#routeCode').attr('readonly', false);
//                   document.getElementById('routeIdTo').value = '';
//                }
//                if(value == '' && id=='routeCode'){
//                   $('#routeNameFrom').attr('readonly', false);
//                   $('#routeNameTo').attr('readonly', false);
//                   document.getElementById('routeId').value = '';
//                }
//            }

        function submitPage(val) {
                if (val == 'addRoute') {
                    document.routeMaster.action = '/throttle/addRouteMaster.do';
                    document.routeMaster.submit();
                } else if (val == 'Search') {
                    document.routeMaster.action = '/throttle/viewRouteDetails.do';
                    document.routeMaster.submit();
                }
            }

            function editPage(routeId){
                    document.routeMaster.action = '/throttle/editRouteDetails.do?editRouteId='+routeId;
                    document.routeMaster.submit();
            }

            function addSlabRate(vehicleId) {
                    $("#responseSpan").text("Map");
                    $('#responseDIV').html('');
                    var url = '/throttle/getMapLocation.do?vehicleId=' +vehicleId;
                    $.ajax({
                        url: url,
                        type: "get",
                        success: function (data)
                        {
                            $('#responseDIV').html(data);
                        }
                    });
                }
        </script>
        <form name="routeMaster"  method="post">
            <div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
                <div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
                    <!-- pointer table -->
                    <table width="700" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                        <tr>
                            <td >
                                <%@ include file="/content/common/path.jsp" %>
                                <%@ include file="/content/common/message.jsp" %>
                            </td></tr></table>
                    <!-- pointer table -->

                </div>
            </div>
            <br>
            <br>
            <br>
            <br>
            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                    <td align="right"><div style="height:17px;margin-top:0px;"><img src="../images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="../images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:900;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">View Route Details</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                                    <tr>
                                        <td height="30">&nbsp;&nbsp;<font color="red">*</font>Origin</td>
                                        <td><input type="hidden" name="cityFromId" id="cityFromId" value="<c:out value="${cityId}"/>" class="textbox"><input type="text" name="cityFrom" id="cityFrom" value="<c:out value="${cityName}"/>" class="textbox" ></td>
                                        <td height="30">&nbsp;&nbsp;<font color="red">*</font>Destination</td>
                                        <td><input type="hidden" name="cityToId" id="cityToId" value="<c:out value="${cityId}"/>" class="textbox"><input type="text" name="cityTo" id="cityTo" value="<c:out value="${cityName}"/>" class="textbox" ></td>
                                        
                                    </tr>
                                    <tr>
                                        <td  height="25" >Route Code</td>
                                        <td  height="25"><input type="text" name="routeStatus" id='routeStatus' class="textbox" onchange="checkValue(this.value,this.id)" value="<c:out value="${nextRouteCode}"/>" readonly><c:out value="${nextRouteCode}" /> </td>
<!--                                                                onchange="checkValue(this.value,this.id)"/> -->
<!--                                        <td class="text1" colspan="2"><label id="routeStatus" style="color: red"></label></td>-->
                                        <td>
                                            <input type="button"   value="Search" class="button" name="search" onClick="submitPage(this.value)" style="width: 120px;">
                                        </td>
                                        <td height="25"><input type="button"   value="Add Route" class="button" name="addRoute" onClick="submitPage(this.name)" style="width: 120px;"/>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <c:if test="${routeList != null}">
                <table width="100%" align="center" border="0" id="table" class="sortable">
                    <thead>
                        <tr height="40">
                            <th><h3>S.No</h3></th>
                            <th><h3>Route Code</h3></th>
                            <th><h3>From Location</h3></th>
                            <th><h3>To Location</h3></th>
                            <th><h3>Travel Time(Hrs)</h3></th>
                            <th><h3>Distance(Km)</h3></th>
                            <th><h3>Reefer Running Hours</h3></th>
                            <th><h3>Highway Type</h3></th>
                            <th><h3>view/edit</h3></th>
                            <th><h3>Primary Contract</h3></th>
<!--                            <th><h3>Link To Map</h3></th>-->
                        </tr>
                    </thead>
                    <% int index = 0;
                        int sno = 1;
                    %>
                    <tbody>
                        <c:forEach items="${routeList}" var="rl">
                            <%
                                String classText = "";
                                int oddEven = index % 2;
                                if (oddEven > 0) {
                                    classText = "text2";
                                } else {
                                    classText = "text1";
                                }
                            %>
                            <tr height="30">
                                <td align="left" class="<%=classText%>"><%=sno%></td>
                                <td align="left" class="<%=classText%>"><c:out value="${rl.routeCode}"/></td>
                                <td align="left" class="<%=classText%>"><c:out value="${rl.cityFromName}"/></td>
                                <td align="left" class="<%=classText%>"><c:out value="${rl.cityToName}"/></td>
                                <td align="center" class="<%=classText%>"><c:out value="${rl.travelHour}"/>:<c:out value="${rl.travelMinute}"/></td>
                                <td align="center" class="<%=classText%>"><c:out value="${rl.distance}"/></td>
                                <td align="center" class="<%=classText%>"><c:out value="${rl.reeferHour}"/>:<c:out value="${rl.reeferMinute}"/></td>
                                <td align="left" class="<%=classText%>"><c:out value="${rl.roadType}"/></td>
                                <td align="left" class="<%=classText%>"><a href="#" onclick="editPage('<c:out value="${rl.routeId}"/>')">view/edit</a></td>
                                <td align="left" class="<%=classText%>"><a href="#" onclick="editPage('<c:out value="${rl.routeId}"/>')">view  edit</a></td>
<!--                                <td  align="left">
                            <label class="btn" for="modal-1" id="cost" onclick="addSlabRate('<c:out value="${tripDetails.vehicleId}"/>');">MAP</label>
                                </td>-->
                            </tr>
                            <%sno++;%>
                        </c:forEach>
                    </tbody>
                </table>
                <script language="javascript" type="text/javascript">
                    setFilterGrid("table");
                </script>
                <div id="controls">
                    <div id="perpage">
                        <select onchange="sorter.size(this.value)">
                            <option value="5" selected="selected">5</option>
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        <span>Entries Per Page</span>
                    </div>
                    <div id="navigation">
                        <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                        <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                        <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                        <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                    </div>
                    <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                </div>
                <script type="text/javascript">
                    var sorter = new TINY.table.sorter("sorter");
                    sorter.head = "head";
                    sorter.asc = "asc";
                    sorter.desc = "desc";
                    sorter.even = "evenrow";
                    sorter.odd = "oddrow";
                    sorter.evensel = "evenselected";
                    sorter.oddsel = "oddselected";
                    sorter.paginate = true;
                    sorter.currentid = "currentpage";
                    sorter.limitid = "pagelimit";
                    sorter.init("table", 1);
                </script>
            </c:if>
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
