
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="ets.domain.department.business.DepartmentTO" %>
<%@ page import="java.util.*" %>

<script>
    function submitpage(indx)
    {
        selectedItemValidation();
    }
    function selectedItemValidation() {
        var index = document.getElementsByName("selectedIndex");
        var name = document.getElementsByName("names");
        var description = document.getElementsByName("descriptions");
        var toMail = document.getElementsByName("toMails");
        var ccMail = document.getElementsByName("ccMails");
        var status = document.getElementsByName("stats");
        var chec = 0;
        for (var i = 0; (i < index.length && index.length != 0); i++) {
            if (index[i].checked) {
                chec++;
                if (textValidation(name[i], 'Department Name')) {
                    return;
                }
                if (textValidation(description[i], 'Department Description')) {
                    return;
                }
                if (textValidation(toMail[i], 'Department ToMail')) {
                    return;
                }
                if (textValidation(ccMail[i], 'Department CcMail')) {
                    return;
                }
                if (textValidation(status[i], 'Department Status')) {
                    return;
                }
                document.manageDepartment.action = '/throttle/handleUpdateDepartment.do';
                document.manageDepartment.submit();
            }
        }
        if (chec == 0) {
            alert("Please Select Any One And Then Proceed");
            name[0].focus();
        }
    }

    function setSelectbox(i)
    {
        var selected = document.getElementsByName("selectedIndex");
        selected[i].checked = 1;
    }

</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="hrms.label.Department" text="default text"/></h2>
    <!--    <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="default text"/>:</span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.HRMS" text="default text"/></a></li>
                <li class="active"><spring:message code="hrms.label.Department" text="default text"/></li>
              <li class=""><spring:message code="hrms.label.AlterDepartment" text="default text"/></li>



            </ol>
          </div>-->
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="manageDepartment"  method="post" >
                    <c:if test = "${DepartmentLists != null}" >
                        <table class="table table-dark mb30 table-hover" >
                            <thead>
                                <tr height="30">
                                    <th width="222" align="left" class="contenthead" ><spring:message code="hrms.label.Name" text="default text"/></th>
                                    <th width="123" align="left" class="contenthead"><spring:message code="hrms.label.Description" text="default text"/></th>
                                    <th width="37" align="center" class="contenthead"><spring:message code="hrms.label.Status" text="default text"/></th>
                                    <th width="47" align="center" class="contenthead">To Mail ID</th>
                                    <th width="47" align="center" class="contenthead">CC Mail ID</th>
                                    <th width="47" align="center" class="contenthead"><spring:message code="hrms.label.Select" text="default text"/></th>
                                </tr>
                            </thead>
                            <% int index = 0;%>
                            <c:forEach items="${DepartmentLists}" var="department">
                                <%
                    String classText = "";
                    int oddEven = index % 2;
                    if (oddEven > 0) {
                        classText = "text2";
                    } else {
                        classText = "text1";
                    }
                                %>
                                <tr>
                                <input name="departmentIds" type="hidden" class="form-control" value="<c:out value='${department.departmentId}'/>" >
                                <td align="left" ><input name="names" style="width:200px;height:40px;" type="text" class="form-control" onchange="setSelectbox('<%= index %>');"  value=<c:out value="${department.name}"/>></td>
                                <td align="left" ><textarea style="width:200px;height:40px;" class="form-control" name="descriptions"  onchange="setSelectbox('<%= index %>');" ><c:out value="${department.description}"/></textarea></td>
                                <td align="center" > <select name="stats" style="width:200px;height:40px;" class="form-control" onchange="setSelectbox('<%= index %>');" >
                                        <c:if test="${(department.status=='y') || (department.status=='Y')}" >
                                            <option value="Y" selected><spring:message code="hrms.label.Active" text="default text"/></option><option value="N"><spring:message code="hrms.label.InActive" text="default text"/></option>
                                        </c:if>
                                        <c:if test="${(department.status=='n') || (department.status=='N')}" >
                                            <option value="Y" ><spring:message code="hrms.label.Active" text="default text"/></option><option value="N" selected><spring:message code="hrms.label.InActive" text="default text"/></option>
                                            </c:if>
                                    </select>
                                </td>
                                <td align="left" ><input name="toMails" id="toMails" style="width:250px;height:40px;" type="text" class="form-control" onchange="setSelectbox('<%= index %>');"  value=<c:out value="${department.toMail}"/>></td>
                                <td align="left" ><input name="ccMails" id="ccMails" style="width:250px;height:40px;" type="text" class="form-control" onchange="setSelectbox('<%= index %>');"  value=<c:out value="${department.ccMail}"/>></td>
                                <td width="87" height="30" ><input type="checkbox" name="selectedIndex" value='<%= index %>'></td>
                                </tr>
                                <%
                    index++;
                                %>
                            </c:forEach>
                        </table>
                        <br>
                        <center><input type="button" class="btn btn-primary" value="<spring:message code="hrms.label.SAVE" text="default text"/>" onclick="submitpage(this.value);"/></center>
                        <input type="hidden" name="reqfor" value="" />
                        <br>
                        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                    </c:if>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>