<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/content/common/NewDesign/header.jsp" %>
    <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
 <script language="javascript" src="/throttle/js/validate.js"></script>   


<script>
    function submitpage()
    {
       if(textValidation(document.dept.sectionName,"SectionName")){
           return;
       }
       if(textValidation(document.dept.sectionCode,"SectionCode")){
           return;
       }
       if(textValidation(document.dept.description,"Description")){
           return;
       }
       if(textValidation(document.dept.description,"Description")){
           return;
       }
       document.dept.action= "/throttle/addSection.do";
       document.dept.submit();
    }
</script>
 <div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="serviceplan.label.AddSection" text="default text"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="serviceplan.label.ServicePlan" text="default text"/></a></li>
          <li class=""><spring:message code="serviceplan.label.ManageSection" text="default text"/></li>
          <li class="active"><spring:message code="serviceplan.label.AddSection" text="default text"/></li>
        </ol>
      </div>
      </div>
 <div class="contentpanel">
<div class="panel panel-default">
 <div class="panel-body">
<body>
    
<form name="dept"  method="post">

       

<%@ include file="/content/common/message.jsp" %>
 
 <table class="table table-info mb30 table-hover" >

     <thead>
     <tr height="30">
  <th colspan="5" > <spring:message code="serviceplan.label.AddSection" text="default text"/></th>
  </tr>
  </thead>
<tr>
<td  height="30"><font color=red>*</font><spring:message code="serviceplan.label.SectionName" text="default text"/></td>
<td  height="30"><input name="sectionName" type="text" style="width:260px;height:40px;"  class="form-control" value=""></td>

<td  height="30"><font color=red>*</font><spring:message code="serviceplan.label.SectionCode" text="default text"/></td>
<td  height="30"><input name="sectionCode" type="text" maxlength="3" style="width:260px;height:40px;"  class="form-control" value=""></td>
</tr>
<tr>
<td  height="30"><font color=red>*</font> <spring:message code="serviceplan.label.Description" text="default text"/></td>
<td  height="30"><textarea style="width:260px;height:40px;"  class="form-control" name="description"></textarea></td>

<td  height="30"><font color=red>*</font><spring:message code="serviceplan.label.ServiceType" text="default text"/></td>
<td  height="30">
    <c:if test="${serviceTypeList != null}">
    <select name="serviceTypeId" id="serviceTypeId" style="width:260px;height:40px;"  class="form-control">
            <option value="0">--<spring:message code="serviceplan.label.Select" text="default text"/>--</option>
        <c:forEach items="${serviceTypeList}" var="service">
            <option value="<c:out value="${service.serviceTypeId}"/>"><c:out value="${service.serviceTypeName}"/></option>
        </c:forEach>
    </select>
    </c:if>
</td>
</tr>
</table>
<center>
<input type="button" value="<spring:message code="serviceplan.label.ADD" text="default text"/>" class="btn btn-success"  onclick="submitpage();">
&emsp;<input type="reset" class="btn btn-success"  value="<spring:message code="serviceplan.label.CLEAR" text="default text"/>">
</center>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

