
<html>
    <head>
     <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    </head>
    <script language="javascript">
        function submitPage(){
            var chek=validation();
            if(chek=='true'){
            document.report.action='/throttle/addVehicleMilleage.do';
            document.report.submit();
       }
        }
        
        function validation(){
            if(document.report.mfrId.value=="0"){
                alert("Please Select Mfr Name");
                return 'false';
                }
            if(document.report.modelId.value=="0"){
                alert("Please Select Model Name");
                return 'false';
                }
            if(textValidation(document.report.fromYear,'From Year')){
                return 'false';
                }
            if(textValidation(document.report.toYear,'To Year')){
                return 'false';
                }
                var data=ajaxData1();
                if(data==null){
                 return 'true';
                }else{
                alert("From Year and To Year Already Exists");
                return 'false';
                }
            }
         var httpReq;
var temp = "";
function ajaxData()
{
 
var url = "/throttle/getModels1.do?mfrId="+document.report.mfrId.value;    
if (window.ActiveXObject)
{
httpReq = new ActiveXObject("Microsoft.XMLHTTP");
}
else if (window.XMLHttpRequest)
{
httpReq = new XMLHttpRequest();
}
httpReq.open("GET", url, true);
httpReq.onreadystatechange = function() { processAjax(); } ;
httpReq.send(null);
}

function processAjax()
{
if (httpReq.readyState == 4)
{
	if(httpReq.status == 200)
	{
	temp = httpReq.responseText.valueOf();                 
        setOptions(temp,document.report.modelId);        
        if('<%= request.getAttribute("modelId")%>' != 'null' ){
            document.report.modelId.value = <%= request.getAttribute("modelId")%>;
        }
	}
	else
	{
	alert("Error loading page\n"+ httpReq.status +":"+ httpReq.statusText);
	}
}
}
         var httpReq1;
var temp1 = "";
function ajaxData1()
{
 
var url1 = "/throttle/chekExistence.do?mfrId="+document.report.mfrId.value+'&modelId='+document.report.modelId.value+'&fromYear='+document.report.fromYear.value+'&toYear='+document.report.toYear.value;    

if (window.ActiveXObject)
{
httpReq1 = new ActiveXObject("Microsoft.XMLHTTP");
}
else if (window.XMLHttpRequest)
{
httpReq1 = new XMLHttpRequest();
}
httpReq1.open("GET", url1, true);
httpReq1.onreadystatechange = function() { processAjax1(); } ;
httpReq1.send(null);
}

function processAjax1()
{
if (httpReq1.readyState == 4)
{
	if(httpReq1.status == 200)
	{
	temp1 = httpReq1.responseText.valueOf();   
       
        return temp1;
       
	}
	else
	{
	alert("Error loading page\n"+ httpReq.status +":"+ httpReq.statusText);
	}
}
}
        
    </script>
    <body >
        <form name="report" method="post">
            <%@ include file="/content/common/path.jsp" %>                 
            <%@ include file="/content/common/message.jsp" %>
            <br>
            
            
            
                
                <table align="center" width="400" class="border" border="0" cellspacing="0" cellpadding="0">
                    <tr height="30">
                        <td colspan="4" class="contenthead">Add Milleage</td>
                    </tr>
                    <tr>
                        <td height="30" class="text2"><font color="red">*</font>MFR</td>
                     <td height="30" class="text2"><select name="mfrId" onchange="ajaxData();" class="form-control" style="width:125px">
                            <option value="0">-select-</option>
                            <c:if test = "${MfrLists != null}" >
                                <c:forEach items="${MfrLists}" var="mfr"> 
                                    <option value='<c:out value="${mfr.mfrId}"/>'><c:out value="${mfr.mfrName}"/></option>
                                </c:forEach>
                            </c:if>        
                    </select></td>
                    </tr>
                    <tr>
                    <td height="30" class="text1"><font color="red">*</font>Model</td>
                    <td height="30" class="text1"><select name="modelId" class="form-control" style="width:125px">
                            <option value="0">-select-</option>
                            <c:if test = "${ModelLists != null}" >
                                <c:forEach items="${ModelLists}" var="model"> 
                                    <option value="<c:out value='${model.modelId}'/>"><c:out value="${model.modelName}"/></option>
                                </c:forEach>
                            </c:if>       
                    </select></td>                        
                </tr>
                     <tr>
                         <td class="text2"><font color="red">*</font>From Year</td>
                          <td class="text2"><input type="text" class="form-control" name="fromYear" ></td>
                      </tr>
                     <tr>
                         <td class="text1"><font color="red">*</font>To Year</td>
                          <td class="text1"><input type="text" class="form-control" name="toYear" ></td>
                      </tr>
                      <tr>
                        <td class="text2"><font color="red">*</font>Milleage</td>
                         <td class="text2"><input type="text" class="form-control" name="milleage" ></td>
                    </tr>
                   
                </table>
            
            <br>
            <center>
                <input type="button" class="button" value="Add" name="add" onclick="submitPage();" >
                &emsp;<input type="reset" class="button" value="Clear">
            </center>
                <br>
            
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
