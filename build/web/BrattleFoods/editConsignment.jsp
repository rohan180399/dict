<%-- 
    Document   : editConsignment
    Created on : Oct 31, 2013, 4:35:52 PM
    Author     : Arul
--%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>

        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $("#tabs").tabs();
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                $( ".datepicker" ).datepicker({
                    changeMonth: true,changeYear: true
                });
            });
        </script>
        <script type="text/javascript" language="javascript">
            var poItems = 0;
            var rowCount='';
            var sno='';
            var snumber = '';
            function addAllowanceRow()
            {
                if(sno < 19){
                    sno++;
                    var tab = document.getElementById("expenseTBL");
                    var rowCount = tab.rows.length;

                    snumber = parseInt(rowCount)-1;

                    var newrow = tab.insertRow( parseInt(rowCount)-1) ;
                    newrow.height="30px";
                    // var temp = sno1-1;
                    var cell = newrow.insertCell(0);
                    var cell0 = "<td><input type='hidden'  name='itemId' /> "+snumber+"</td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(1);
                    cell0 = "<td class='text1'><input name='driName' type='text' class='form-control' id='driName' onkeyup='getDriverName("+snumber+")' /></td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(2);
                    cell0 = "<td class='text1'><input name='cleanerName' type='text' class='form-control' id='cleanerName' /></td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(3);
                    cell0 = "<td class='text1'><input name='date' type='text' id='date' class='datepicker' /></td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    // rowCount++;

                    $( ".datepicker" ).datepicker({
                        /*altField: "#alternate",
                        altFormat: "DD, d MM, yy"*/
                        changeMonth: true,changeYear: true
                    });
                }
                document.settle.expenseId.value=sno;
            }

            function delAllowanceRow() {
                try {
                    var table = document.getElementById("expenseTBL");
                    rowCount = table.rows.length-1;
                    for(var i=2; i<rowCount; i++) {
                        var row = table.rows[i];
                        var checkbox = row.cells[6].childNodes[0];
                        if(null != checkbox && true == checkbox.checked) {
                            if(rowCount <= 1) {
                                alert("Cannot delete all the rows");
                                break;                            }
                            table.deleteRow(i);
                            rowCount--;
                            i--;
                            sno--;
                        }
                    }
                    document.settle.expenseId.value=sno;
                }catch(e) {
                    alert(e);
                }
            }
            function parseDouble(value){
                if(typeof value == "string") {
                    value = value.match(/^-?\d*/)[0];
                }
                return !isNaN(parseInt(value)) ? value * 1 : NaN;
            }
            function submitPage(obj){
                if(obj.name=="search"){
                    var fromDate=document.settle.fromDate.value;
                    var toDate=document.settle.toDate.value;
                    //var regno=document.settle.regno.value;
                    var driName=document.settle.driName.value;
                    if (driName=="") {
                        alert("please enter the Driver Name");
                        document.settle.driName.focus();
                    }else if (fromDate=="") {
                        alert("please enter the From Date");
                        document.settle.fromDate.focus();
                    }else if (toDate=="") {
                        alert("please enter the To Date");
                        document.settle.toDate.focus();
                    }else{
                        document.settle.action="/throttle/searchProDriverSettlement.do";
                        document.settle.submit();
                    }
                }
                /*alert("hi main: "+obj.name);
                if(obj.name=="save"){
                    alert("hi");
                    alert(document.settle.expenseId.value);
                    document.settle.buttonName.value = "save";
                    obj.name="none";
                    if(document.settle.expenseId.value != ""){
                        document.settle.action='/throttle/saveDriverExpenses.do';
                        document.settle.submit();
                    }
                }*/
                if(obj.name=="proceed"){
                    //alert("proceed");
                    document.settle.buttonName.value="proceed";
                    obj.name="none";
                    document.settle.action='/throttle/saveDriverExpenses.do';
                    document.settle.submit();
                }
            }
            function saveExp(obj){
                document.settle.buttonName.value = "save";
                obj.name="none";
                if(document.settle.expenseId.value != ""){
                    document.settle.action='/throttle/saveDriverExpenses.do';
                    document.settle.submit();
                }
            }

            function getDriverName(sno){
                var oTextbox = new AutoSuggestControl(document.getElementById("driName"),new ListSuggestions("driName","/throttle/handleDriverSettlement.do?"));

            }
        </script>
        <script language="">
            function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }
            function calcTotalPacks(val){
                if(document.getElementById('totalPackages').innerHTML == '0'){
                    document.getElementById('totalPackages').innerHTML  = parseInt(val);
                }else{
                    var totVal =   document.getElementById('totalPackages').innerHTML;
                    totVal = parseInt(val) + parseInt(totVal);
                    document.getElementById('totalPackages').innerHTML = totVal;
                }
            }
            
            function calcTotalWeights(val){
                if(document.getElementById('totalWeight').innerHTML == '0'){
                    document.getElementById('totalWeight').innerHTML  = parseInt(val);
                }else{
                    var totVal =   document.getElementById('totalWeight').innerHTML;
                    totVal = parseInt(val) + parseInt(totVal);
                    document.getElementById('totalWeight').innerHTML = totVal;
                }
            }
        </script>
    </head>

    <script type="text/javascript">
        var rowCount=1;
        var sno=0;
        var rowCount1=1;
        var sno1=0;
        var httpRequest;
        var httpReq;
        var styl = "";

        function addRow1(){
            if(parseInt(rowCount1) %2==0)
            {
                styl="text2";
            }else{
                styl="text1";
            }
            sno1++;
            var tab = document.getElementById("addTyres1");
            var newrow = tab.insertRow(rowCount1);

            var cell = newrow.insertCell(0);
            var cell0 = "<td class='text1' height='25' > "+sno1+"</td>";
            cell.setAttribute("className",styl);
            cell.innerHTML = cell0;

            // Positions
            cell = newrow.insertCell(1);
            var cell0 = "<td class='text1' height='30' ><font color='red'>*</font><input type='text' name='estDate' class='form-control' >";
            cell.setAttribute("className",styl);
            cell.innerHTML = cell0;
            // TyreIds
            var cell = newrow.insertCell(2);
            var cell0 = "<td class='text1' height='30' ><font color='red'>*</font><input type='text' name='estDate' class='form-control' >";
            cell.setAttribute("className",styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(3);
            var cell1 =  "<td class='text1' height='30' ><font color='red'>*</font><input type='text' name='packagesNos' id='packagesNos' class='form-control' value='' onkeyup='calcTotalPacks(this.value)'>";

            cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
            cell.setAttribute("className","text1");
            cell.innerHTML = cell1;

            cell = newrow.insertCell(4);
            var cell0 = "<td class='text1' height='30' ><font color='red'>*</font><input type='text' name='weights' id='weights' class='form-control' value='' onkeyup='calcTotalWeights(this.value)' >";
            cell.setAttribute("className",styl);
            cell.innerHTML = cell0;
            rowCount1++;
        }

        
        //        totalPackages totalWeights
        function addRow(){
            if(parseInt(rowCount) %2==0)
            {
                styl="text2";
            }else{
                styl="text1";
            }
            sno++;
            var tab = document.getElementById("addTyres");
            var newrow = tab.insertRow(rowCount);

            var cell = newrow.insertCell(0);
            var cell0 = "<td class='text1' height='25' > "+sno+"</td>";
            cell.setAttribute("className",styl);
            cell.innerHTML = cell0;

            // Positions
            cell = newrow.insertCell(1);
            var cell0 = "<td class='text1' height='25' >"+
                "<font color='red'>*</font><select name='positionIds'   class='form-control' > <option value='0'>-Select-</option>"+
                "<option value=Salem > Salem  </option>"+
                "<option value=Madurai >Madurai</option>"+
                "<option value=Madurai >Delhi</option>"+
                "<option value=Madurai >Chennai</option>"+
                "<option value=Madurai >Covai</option>"+
                +"</select> </td>";
            cell.setAttribute("className",styl);
            cell.innerHTML = cell0;
            // TyreIds
            var cell = newrow.insertCell(2);
            var cell0 = "<td class='text1' height='25' ><font color='red'>*</font>"+
                "<select name='itemIds' class='form-control'    > <option value='0'>-Select-</option>"+
                "<option value=pk > Pickup </option>"+
                "<option value=dp > Drop </option>"+
                "<option value=pkdp > Pickup &amp; Drop </option>"+

                +"</select> </td>";
            cell.setAttribute("className",styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(3);
            var cell1 =  "<td class='text1' height='30' ><font color='red'>*</font><input type='text' name='estDate' class='datepicker' >";
            cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
            cell.setAttribute("className","text1");
            cell.innerHTML = cell1;


            cell = newrow.insertCell(4);
            var cell1 =  "<td class='text1' height='30' ><font color='red'>*</font><input type='text' name='estDate' class='datepicker' >";
            cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
            cell.setAttribute("className","text1");
            cell.innerHTML = cell1;

            cell = newrow.insertCell(5);
            var cell0 = "<td class='text1' height='25' >"+
                "<font color='red'>*</font><select name='positionIds'   class='form-control' > <option value='0'>-Select-</option>"+
                "<option value=Salem > Salem  </option>"+
                "<option value=Madurai >Madurai</option>"+
                "<option value=Madurai >Delhi</option>"+
                "<option value=Madurai >Chennai</option>"+
                "<option value=Madurai >Covai</option>"+
                +"</select> </td>";
            cell.setAttribute("className",styl);
            cell.innerHTML = cell0;

            var cell = newrow.insertCell(6);
            var cell0 = "<td class='text1' height='25' ><font color='red'>*</font>"+
                "<select name='itemIds' class='form-control'    > <option value='0'>-Select-</option>"+
                "<option value=pk > Pickup </option>"+
                "<option value=dp > Drop </option>"+
                "<option value=pkdp > Pickup &amp; Drop </option>"+
                +"</select> </td>";
            cell.setAttribute("className",styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(7);
            var cell1 =  "<td class='text1' height='30' ><font color='red'>*</font><input type='text' name='estDate' class='form-control' >";
            cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
            cell.setAttribute("className","text1");
            cell.innerHTML = cell1;


            cell = newrow.insertCell(8);
            var cell2 =  "<td class='text1' height='30' ><font color='red'>*</font><input type='text' name='tyreDate' class='datepicker' >";
            cell.setAttribute("className","text1");
            cell.innerHTML = cell2;

            cell = newrow.insertCell(9);
            var cell2 =  "<td class='text1' height='30' ><font color='red'>*</font><input type='text' name='tyreDate' class='distance' value='100'>";
            cell.setAttribute("className","text1");
            cell.innerHTML = cell2;

            rowCount++;

            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                $( ".datepicker" ).datepicker({
                    changeMonth: true,changeYear: true
                });
            });

        }

        function goToImportPage(){
            document.settle.action='/throttle/BrattleFoods/importCnoteDetails.jsp';
            document.settle.submit();
        }

    </script>
    <body onload="addRow();addRow1();addAllowanceRow()">
        <% String menuPath = "Operation >>  Edit Consignment Sheet";
                    request.setAttribute("menuPath", menuPath);
        %>
        <form name="settle" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <table width="100%">
                <tr></tr>
            </table>
            <br>
            <br>
            <br>
            <table cellpadding="0" cellspacing="0" width="800" border="0px" style="border-color: #fff3ef" align="center">
                <tr>
                    <td class="contenthead" colspan="4">Consignment Note Type</td>
                </tr>
                <tr>
                    <td class="text2">Entry Option</td>
                    <td class="text2"><input type="radio" name="entry" value="1" >Manual</td>
                    <td class="text2"><input type="radio" name="entry" value="2" onclick="goToImportPage()">Import</td>
                    <td class="text2" >&nbsp;</td>
                </tr>
                <tr>
                    <td class="text1">Consignment Note </td>
                    <td class="text1"><input type="text" name="cNoteNo" id="cNoteNo" value="CN001" readonly /></td>
                    <td class="text1">Consignment Note Date</td>
                    <td class="text1"><input name="tripDate" type="text" class="datepicker"  readonly="readonly" id="tripDate"  /></td>
                </tr>

                <tr>
                    <td class="text2">Customer Type</td>
                    <td class="text2">
                        <select name="customerType" id="customerType" class="form-control" style="width:120px;" onchange="showContract(this.value)">
                            <option value="1"> Contract </option>
                            <option value="2"> Third Party </option>
                            <option value="2"> Walk In </option>
                        </select>
                    </td>
                    <td class="text2">Product Category</td>
                    <td class="text2">
                        <select name="vehicleId" onchange="fillData()" id="vehicleId" class="form-control" style="width:120px;">
                            <option value="0"> -Select- </option>
                            <option value="1"> Food Items </option>
                            <option value="2"> Beverages </option>
                            <option value="3"> Fragile Items </option>
                            <option value="4"> Alcohol Items </option>
                            <option value="5"> Perishable Items </option>
                            <option value="6"> Dangerous Items </option>
                        </select>
                    </td>
            </table>
            <br>
            <br>

            <div id="tabs">
                <ul>
                    <li><a href="#customerDetail"><span>Basic Details</span></a></li>
<!--                    <li><a href="#routeDetail"><span>Pickup &amp; Delivery Details</span></a></li>-->
                    <li><a href="#paymentDetails"><span>Payment Details</span></a></li>
                </ul>

                <div id="customerDetail">
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contenthead" colspan="4" >Customer Details</td>
                        </tr>
                        <tr>
                            <td class="text1">Name</td>
                            <td class="text1"><input name="customerName" type="text" class="form-control"  id="customerName" onchange="showContract(this.value)" /></td>
                            <td class="text1">Code</td>
                            <td class="text1"><input name="customerCode" type="text" class="form-control"  id="customerCode" /></td>
                        </tr>
                        <tr>
                            <td class="text2">Address</td>
                            <td class="text2"><textarea rows="3" cols="20"></textarea></td>
                            <td class="text2">Pincode</td>
                            <td class="text2"><input name="pincode" type="text"  class="form-control"  id="pincode" /></td>
                        </tr>
                        <tr>
                            <td class="text1">Mobile No</td>
                            <td class="text1"><input name="mobileNo" type="text"  class="form-control"  id="mobileNo" /></td>
                            <td class="text1">E-Mail ID</td>
                            <td class="text1"><input name="mailId" type="text" class="form-control"  id="mailId" /></td>
                        </tr>
                        <tr>
                            <td class="text2">Phone No</td>
                            <td class="text2"><input name="phoneNo" type="text"  class="form-control" maxlength="10"   id="phoneNo" /></td>
                            <td class="text2" colspan="2">&nbsp;</td>
                        </tr>

                    </table>
                    <script>
                        function showContract(val){
                            if(val != '' && document.getElementById('customerType').value == '1' ){
                                document.getElementById('contractDetails').style.display = 'block';
                            }else{
                                document.getElementById('contractDetails').style.display = 'none';
                            }
                        }
                    </script>
                    <div id="contractDetails" style="display: none">
                        <table>
                            <tr>
                                <td class="text1">Contract No :</td>
                                <td class="text1"><b>BF4567</b></td>
                                <td class="text1">Contract Expiry Date :</td>
                                <td class="text1"><b>10-10-2014</b></td>
                                <td class="text1">View Contract :</td>
                                <td class="text1"><a href=""><b>View Contract</b></a></td>
                            </tr>
                        </table>
                    </div>
                    <br>
                    <br>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">

                        <tr>
                            <td class="contenthead" colspan="6" >Consignment Details</td>
                        </tr>
                        <tr>
                            <td class="text1">Origin</td>
                            <td class="text1"><input type="text" class="form-control" id="origin" name="origin" ></td>
                            <td class="text1">Destination</td>
                            <td class="text1"><input type="text" class="form-control" id="destination" name="destination" ></td>
                            <td class="text1">Business Type</td>
                            <td class="text1">
                                <select name="businessType" class="form-control"  id="businessType" style="width:120px;">
                                    <option value="0"> --Select-- </option>
                                    <option value="1"> Primary  </option>
                                    <option value="2"> Secondary  </option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="text2">Multi Pickup</td>
                            <td class="text2"><input type="checkbox" class="form-control" name="multiPickup" id="multiPickup" onclick="multipickupShow()"></td>
                            <td class="text2">Multi Delivery</td>
                            <td class="text2"><input type="checkbox" class="form-control" name="multiDelivery" id="multiDelivery" ></td>
                            <td class="text2">Special Instruction</td>
                            <td class="text2"><textarea rows="3" cols="15"></textarea></td>
                        </tr>
                        <tr>
                            <td colspan="4" >

                                <table border="0" class="border" align="left" width="700" cellpadding="0" cellspacing="0" id="addTyres1">
                                    <tr>
                                        <td width="20" class="contenthead" align="center" height="30" ><div class="contenthead">Sno</div></td>
                                        <td class="contenthead" height="30" ><div class="contenthead">Product/Article Code</div></td>
                                        <td class="contenthead" height="30" ><div class="contenthead">Product/Article Name</div> </td>
                                        <td class="contenthead" height="30" ><div class="contenthead">No of Packages</div></td>
                                        <td class="contenthead" height="30" ><div class="contenthead">Total Weight (in Kg)</div></td>
                                    </tr>
                                    <br>

                                    <tr>
                                        <td colspan="5" align="center">
                                            &emsp;<input type="button" class="button" value="Add Row" name="save" onClick="addRow1()">
                                            &nbsp;&nbsp;&nbsp;<input type="reset" class="button" value="Clear">
                                            &emsp;<a  class="nexttab" href=""><input type="button" class="button" value="Save &amp; Next" name="Save" /></a>

                                        </td>
                                    </tr>
                                </table>
                                <br>
                                <br>
                            </td>
                            <td colspan="2">
                                <table border="0" class="border" align="right" width="300" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td colspan="2" height="100">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="contentsub">Total No Packages</label>
                                            <label id="totalPackages">0</label>
                                        </td>
                                        <td>
                                            <label class="contentsub">Total Weight (Kg)</label>
                                            <label id="totalWeight">0</label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">

                        <tr>
                            <td class="contenthead" colspan="6" >Vehicle (Required) Details</td>
                        </tr>

                        <tr>
                            <td class="text1">Service Type</td>
                            <td class="text1">
                                <select name="serviceType" class="form-control"  id="paymentType" style="width:120px;">
                                    <option value="0"> -Select- </option>
                                    <option value="1"> FTL </option>
                                    <option value="2"> LTL </option>
                                </select>
                            </td>
                            <td class="text1">Vehicle Type</td>
                            <td class="text1"> <select name="paymentType" class="form-control"  id="paymentType" style="width:120px;">
                                    <option value="0"> -Select- </option>
                                    <option value="1"> Trailer </option>
                                    <option value="2"> Semi Trailer </option>
                                </select></td>
                        </tr>
                        <tr>
                            <td class="text2">Reefer Required</td>
                            <td class="text2">
                                <select name="paymentType" class="form-control"  id="paymentType" style="width:120px;">
                                    <option value="0"> Yes </option>
                                    <option value="1"> No </option>
                                </select>
                            </td>
                            <td class="text2">Vehicle Required Date</td>
                            <td class="text2"><input type="textbox" class="datepicker" name="vehcleDate" id="vehcleDate" ></td>
                        </tr>
                        <tr>
                            <td class="text1">Vehicle Required Time</td>
                            <td class="text1"><input type="textbox" class="form-control" name="multiDelivery" id="multiDelivery" ></td>
                            <td class="text1">Special Instruction</td>
                            <td class="text1"><textarea rows="3" cols="10"></textarea></td>
                        </tr>
                    </table>
                    <br>
                    <br>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contentsub"  height="30" colspan="6">Consignor Details</td>
                        </tr>
                        <tr>
                            <td class="text2">Consignor Name</td>
                            <td class="text2"><input type="text" class="form-control" id="consignorName" name="consignorName" ></td>
                            <td class="text2">Mobile No</td>
                            <td class="text2"><input type="text" class="form-control" id="phoneNo" name="phoneNo" ></td>
                            <td class="text2">Address</td>
                            <td class="text2"><textarea rows="3" cols="20"></textarea> </td>
                        </tr>
                    </table>
                    <br>
                    <br>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contentsub"  height="30" colspan="6">Consignee Details</td>
                        </tr>
                        <tr>
                            <td class="text2">Consignee Name</td>
                            <td class="text2"><input type="text" class="form-control" id="consignorName" name="consignorName" ></td>
                            <td class="text2">Mobile No</td>
                            <td class="text2"><input type="text" class="form-control" id="phoneNo" name="phoneNo" ></td>
                            <td class="text2">Address</td>
                            <td class="text2"><textarea rows="3" cols="20"></textarea> </td>
                        </tr>
                    </table>
                    <br/>
                    <br/>
                    <center>
                        <a  class="nexttab" href=""><input type="button" class="button" value="Save &amp; Next" name="Save" ></a>
                    </center>
                </div>

<!--                <div id="routeDetail">
                    <table border="0" align="center" width="980" cellpadding="0" cellspacing="0" >
                        <tr>
                            <td align="left">
                                <label class="contentsub">Origin :</label>
                                <label>Chennai</label>
                            </td>
                            <td align="right">
                                <label class="contentsub">Destination:</label>
                                <label>Delhi</label>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <br>
                    <table border="0" class="border" align="center" width="980" cellpadding="0" cellspacing="0" id="addTyres">
                        <tr >
                            <td width="20" class="contenthead" align="center" height="30" ><div class="contenthead">Sno</div></td>
                            <td class="contenthead" height="30" ><div class="contenthead">Starting Point</div></td>
                            <td class="contenthead" height="30" ><div class="contenthead">Type</div> </td>
                            <td class="contenthead" height="30" ><div class="contenthead">St Address</div> </td>
                            <td class="contenthead" height="30" ><div class="contenthead">Est Date&amp;Time</div></td>
                            <td class="contenthead" height="30" ><div class="contenthead">Ending Point</div></td>
                            <td class="contenthead" height="30" ><div class="contenthead">Type</div></td>
                            <td class="contenthead" height="30" ><div class="contenthead">End Address</div></td>
                            <td class="contenthead" height="30" ><div class="contenthead">Est Date&amp;Time</div></td>
                            <td class="contenthead" height="30" ><div class="contenthead">Total Distance</div></td>
                        </tr>
                    </table>
                    <center>
                        &emsp;<input type="reset" class="button" value="Clear">
                        &emsp;<input type="button" class="button" value="Add Row" name="save" onClick="addRow()">
                        &emsp;<a  class="nexttab" href=""><input type="button" class="button" value="Save &amp; Next" name="Save" /></a>
                    </center>
                </div>-->
                <script>
                    $(".nexttab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected + 1);
                    });
                </script>
                <div id="paymentDetails">
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contentsub"  height="30" colspan="6">Payment Details</td>
                        </tr>
                        <tr>
                            <td class="text1">Billing Customer</td>
                            <td class="text1"><select name="billingCustomer" class="form-control"  id="billingCustomer" style="width:120px;">
                                    <option value="0"> -Select- </option>
                                    <option value="1"> Consignor </option>
                                    <option value="2"> Consignee </option>
                                    <option value="3"> Contract </option>
                                    <option value="4"> Third Party </option>
                                </select></td>
                            <td class="text1">Freight Charges</td>
                            <td class="text1"><input type="text" class="form-control" id="freightCharges" name="freightCharges" ></td>
                            <td class="text1">Service Tax Paid By</td>
                            <td class="text1"><select name="serviceTaxPaidBy" class="form-control"  id="serviceTaxPaidBy" style="width:120px;">
                                    <option value="0"> -Select- </option>
                                    <option value="1"> Transporter </option>
                                    <option value="2"> Billing Customer </option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="text2">FOV Rate(% Invoice)</td>
                            <td class="text2"><input type="text" class="form-control" id="fovRate" name="fovRate" ></td>
                            <td class="text2">FOV Charged</td>
                            <td class="text2"><input type="text" class="form-control" id="fovRate" name="fovRate" ></td>
                            <td class="text2" colspan="2">&nbsp;</td>
                        </tr>
                    </table>
                    <br/>
                    <br/>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contentsub"  height="30" colspan="6">Additional Charges</td>
                        </tr>
                        <tr>
                            <td class="text1">Document Charges</td>
                            <td class="text1"><input type="text" class="form-control" id="docCharges" name="docCharges" ></td>
                            <td class="text1">ODA Charges</td>
                            <td class="text1"><input type="text" class="form-control" id="odaCharges" name="odaCharges" ></td>
                            <td class="text1">Multi Pickup Charges</td>
                            <td class="text1"><input type="text" class="form-control" id="multiPickup" name="multiPickup" ></td>
                        </tr>
                        <tr>
                            <td class="text2">Multi Delivery Charges</td>
                            <td class="text2"><input type="text" class="form-control" id="multiDelivery" name="multiDelivery" ></td>
                            <td class="text2">Handling Charges</td>
                            <td class="text2"><input type="text" class="form-control" id="handleCharges" name="handleCharges" ></td>
                            <td class="text2">Other Charges</td>
                            <td class="text2"><input type="text" class="form-control" id="otherCharges" name="otherCharges" ></td>
                        </tr>
                        <tr>
                            <td class="text1">Fuel Surcharges</td>
                            <td class="text1"><input type="text" class="form-control" id="fuelCharges" name="fuelCharges" ></td>
                            <td class="text1">Unloading Charges</td>
                            <td class="text1"><input type="text" class="form-control" id="unloadingCharges" name="unloadingCharges" ></td>
                            <td class="text1">Loading Charges</td>
                            <td class="text1"><input type="text" class="form-control" id="loadingCharges" name="loadingCharges" ></td>
                        </tr>
                        <tr>
                            <td class="text2" colspan="4"></td>
                            <td class="text2">Sub Total</td>
                            <td class="text2"><input type="text" class="form-control" id="fuelCharges" name="fuelCharges" ></td>
                        </tr>
                    </table>
                    <br/>
                    <br/>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contentsub"  height="30" colspan="6">Applicable Charges</td>
                        </tr>
                        <tr>
                            <td class="text1">Service Tax Rate(%)</td>
                            <td class="text1"><input type="text" class="form-control" id="docCharges" name="docCharges" ></td>
                            <td class="text1">Educational Cess Rate(%)</td>
                            <td class="text1"><input type="text" class="form-control" id="freightCharges" name="freightCharges" ></td>
                            <td class="text1">Higher Educational Cess Rate(%)</td>
                            <td class="text1"><input type="text" class="form-control" id="odaCharges" name="odaCharges" ></td>
                        </tr>
                        <tr>
                            <td class="text2">Service Tax on Sub-Total</td>
                            <td class="text2"><input type="text" class="form-control" id="multiPickup" name="multiPickup" ></td>
                            <td class="text2">Educational Cess on Service Tax</td>
                            <td class="text2"><input type="text" class="form-control" id="multiDelivery" name="multiDelivery" ></td>
                            <td class="text2">Higher Educational Cess on Service Tax</td>
                            <td class="text2"><input type="text" class="form-control" id="handleCharges" name="handleCharges" ></td>
                        </tr>
                        <tr>
                            <td class="text1">Service Tax Collected</td>
                            <td class="text1"><input type="text" class="form-control" id="otherCharges" name="otherCharges" ></td>
                            <td class="text1">Educational Cess Collected</td>
                            <td class="text1"><input type="text" class="form-control" id="holidayCharges" name="holidayCharges" ></td>
                            <td class="text1">Higher Educational Cess Collected</td>
                            <td class="text1"><input type="text" class="form-control" id="fuelCharges" name="fuelCharges" ></td>
                        </tr>
                    </table>
                    <br/>
                    <br/>
                    <center>
                        <input type="button" class="button" name="Save" value="Save" >
                    </center>
                </div>

            </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>