<%--
    Document   : searchCustomerWiseProfitability
    Created on : Oct 30, 2013, 5:06:46 PM
    Author     : srinivasan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>


        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>
    </head>
    <%
        String menuPath = "Report >> Customer Wise Profitability";
        request.setAttribute("menuPath",menuPath);
    %>
    <body>
        <form name="customerWise" method="post">
            <%@ include file="/content/common/path.jsp" %>

            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:900;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Customer Wise Trip Report</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">

                                    <tr>
                                        <td  height="25" >Customer Name</td>
                                        <td  height="25" width="182"><input name="fromDate" type="text" class="form-control" value="" size="20">
                                        </td>

                                    </tr>
                                    <tr>


                                        <td width="80" height="30"><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker"  onclick="ressetDate(this);"></td>
                                        <td width="80" height="30"><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" onClick="ressetDate(this);"></td>

                                        <td><input type="button"   value="Search" class="button" name="search" onClick="submitWindow()">
                                            <input type="hidden" value="" name="reqfor"> </td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <center><h3>Customer Wise Profitability Report</h3></center>
            <br>
            <table width="800" align="center" border="0" id="table" class="sortable">
                <thead>
                    <tr height="30">

                        <td rowspan="2" class="contenthead">S.No</td>
                        <td rowspan="2" class="contenthead">Customer Name</td>
                        <td rowspan="2" class="contenthead">MFR Name</td>
                        <td rowspan="2" class="contenthead">Vehicle Model</td>
                        <td rowspan="2" class="contenthead">Cons No</td>
                        <td rowspan="2" class="contenthead">Trip Id</td>
                        <td rowspan="2" class="contenthead">Route</td>
                        <td rowspan="2" class="contenthead">Billing Type</td>
                        <td rowspan="2" class="contenthead">Earnings</td>
                        <td rowspan="2" class="contenthead">Nett <br>Expenses</td>
                        <td rowspan="2" class="contenthead">Profit</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td rowspan="2" class="text1">1</td>
                        <td rowspan="2" class="text1" >M/S Bakers Circle</td>
                        <td rowspan="2" class="text1" >Ashok Leyland</td>
                        <td rowspan="2" class="text1" >2216</td>
                        <td rowspan="2"  class="text1">CN/13-14/100254</td>
                        <td rowspan="2"  class="text1">TS/13-14/100457</td>
                        <td rowspan="2"  class="text1">Kashipur - Delhi</td>
                        <td rowspan="2"  class="text1">Actual KMs</td>
                        <td rowspan="2"  class="text1">53,000.00</td>
                        <td rowspan="2"  class="text1">26,000.00</td>
                        <td rowspan="2"  class="text1">27,000.00</td>


                    </tr>
                </tbody>

                <tbody>
                <tr>
                <td rowspan="2" class="text2">2</td>
                <td rowspan="2" class="text2" >M/S Bakers Circle</td>
                <td rowspan="2" class="text2" >Ashok Leyland</td>
                <td rowspan="2" class="text2" >2216</td>
                <td rowspan="2"  class="text2">CN/13-14/100257</td>
                <td rowspan="2"  class="text2">TS/13-14/100459</td>
                <td rowspan="2"  class="text2">Kashipur - Chandigarh</td>
                <td rowspan="2"  class="text2">Actual KMs</td>
                <td rowspan="2"  class="text2">93,000.00</td>
                <td rowspan="2"  class="text2">46,000.00</td>
                <td rowspan="2"  class="text2">47,000.00</td>


                </tr>
                </tbody>
            </table>

            <br/>
            <br/>
            <table border="2" style="border: 1px solid #666666;"  align="center"  cellpadding="0" cellspacing="1" >
                <tr height="25">
                    <td style="background-color: #6374AB; color: #ffffff">Total Trips Carried Out</td>
                    <td width="150">2</td>
                </tr>
                <tr height="25">
                    <td style="background-color: #6374AB; color: #ffffff">Total Income</td>
                    <td width="150">35,000.00</td>
                </tr>
                <tr height="25">
                    <td style="background-color: #6374AB; color: #ffffff">Total Fixed Expenses</td>
                    <td width="150">50,000.00</td>
                </tr>
                <tr height="25">
                    <td style="background-color: #6374AB; color: #ffffff">Total Operation Expenses</td>
                    <td width="150">15,000.00</td>
                </tr>
                <tr height="25">
                    <td style="background-color: #6374AB; color: #ffffff">Total Nett Expenses</td>
                    <td width="150">35,000.00</td>
                </tr>
                <tr height="25">
                    <td style="background-color: #6374AB; color: #ffffff">Total Profit </td>
                    <td width="150"><font color="green">35,000.00</font></td>
                </tr>
            </table>
            <br/>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
