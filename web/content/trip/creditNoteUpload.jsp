
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">
    function submitPage(value) {
        if (value == 'Proceed') {
             $('#saveShipping').show();
            
            document.upload.action = '/throttle/saveCusotmerCreditUpload.do';
            document.upload.submit();
        } else {
             $('#uploadShipping').show();
            document.upload.action = '/throttle/creditAndPDAUpload.do';
            document.upload.submit();
        }
    }
</script>

<html>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body>
     <c:if test="${customerDetailsSize <= 0}">
      <form name="upload" method="post" enctype="multipart/form-data">
      </c:if>
  <c:if test="${customerDetailsSize > 0}">
        <form name="upload" method="post">
      </c:if>
                    <%@ include file="/content/common/message.jsp" %>
                    <br>
                    <br>
                    <br>

                    <%
                    if(request.getAttribute("errorMessage")!=null){
                    String errorMessage=(String)request.getAttribute("errorMessage");                
                    %>
                <center><b><font color="red" size="1"><%=errorMessage%></font></b></center>
                        <%}%>
                <table class="table table-info mb30 table-hover" style="width:100%">
                    <tr>
                        <td style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;" colspan="4">Credit Amount Upload</td>
                    </tr>
                    <tr>
                        <td class="text2">Select file</td>
                        <td class="text2"><input type="file" name="importBpclTransaction" id="importBpclTransaction" class="importBpclTransaction"></td>                             
                    </tr>
                    <tr>
                        <td colspan="4" class="text1" align="center" ><input type="button" class="btn btn-info" value="Submit" name="Submit" onclick="submitPage(this.value)" >
                    </tr>
                </table>
                <br>
                <br>
                <br>
                <% int i = 1 ;%>
                <% int index = 0;%> 
                <%int oddEven = 0;%>
                <%  String classText = "";%>    
                <c:if test="${customerDetailsSize > 0}">
                    <table class="table table-info mb30 table-hover" style="width:100%">
                        <th  style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;" colspan="10">File Uploaded Details&nbsp;:</th>
                        <tr>
                            <td >S No</td>
                            <td >Customer Code</td>
                            <td >Customer name</td>
                            <td >Available Amount</td>
                            <td >Credit Days</td>
                            <td >PDA Amount</td>
                            <td >Credit Amount</td>
                            
                        </tr>
                        <c:forEach items="${customerDetails}" var="bpcl">
                            <%
                          oddEven = index % 2;
                          if (oddEven > 0) {
                              classText = "text2";
                          } else {
                              classText = "text1";
                            }
                            %>
                            <tr>
                                <td class="<%=classText%>" ><%=i++%></font></td>
                                <td class="<%=classText%>" ><input type="hidden" name="customerCode" id="customerCode" value="<c:out value="${bpcl.customerCode}"/>" /><c:out value="${bpcl.customerCode}"/></font></td>
                                <td class="<%=classText%>" ><input type="hidden" name="customerName" id="customerName" value="<c:out value="${bpcl.customerName}"/>" /><c:out value="${bpcl.customerName}"/></font></td>
                                <td class="<%=classText%>" ><input type="hidden" name="creditAmount" id="creditAmount" value="<c:out value="${bpcl.creditLimit}"/>" /><c:out value="${bpcl.creditLimit}"/></font></td>
                                <td class="<%=classText%>" ><input type="hidden" name="creditDays" id="creditDays" value="<c:out value="${bpcl.creditDays}"/>" /><c:out value="${bpcl.creditDays}"/></font></td>
                                <td class="<%=classText%>" ><input type="hidden" name="pdaAmount" id="pdaAmount" value="<c:out value="${bpcl.pdaAmount}"/>" /><c:out value="${bpcl.pdaAmount}"/></font></td>
                                <td class="<%=classText%>" ><input type="hidden" name="fixedCreditAmount" id="fixedCreditAmount" value="<c:out value="${bpcl.fixedCreditAmount}"/>" /><c:out value="${bpcl.fixedCreditAmount}"/></font></td>
                                

                            </tr>
                            <%index++;%>
                        </c:forEach>
                    </table>
                    <br>
                    <br>
                    <br>
                    <center>
                        <input type="button"  class="btn btn-info" value="Proceed" onclick="submitPage(this.value)"/>
                    </center>

                </c:if>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>