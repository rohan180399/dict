/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.mis.web;

import ets.arch.web.BaseController;
import ets.domain.users.business.LoginBP;
import ets.domain.util.FPLogUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.ModelAndView;
import java.util.Iterator;
import java.util.ArrayList;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.util.ParveenErrorConstants;
import ets.arch.business.PaginationHelper;
import java.io.*;
import java.text.SimpleDateFormat;
import ets.domain.mis.web.MisCommand;
import ets.domain.mis.business.MisBP;
import ets.domain.mis.business.MisTO;

/**
 *
 * @author Rajasekhar
 */
public class MisController extends BaseController {

    LoginBP loginBP;
    MisCommand misCommand;
    MisBP misBP;

    public LoginBP getLoginBP() {
        return this.loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    public MisCommand getMisCommand() {
        return misCommand;
    }

    public void setMisCommand(MisCommand misCommand) {
        this.misCommand = misCommand;
    }

    public MisBP getMisBP() {
        return misBP;
    }

    public void setMisBP(MisBP misBP) {
        this.misBP = misBP;
    }

    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        binder.closeNoCatch();  initialize(request);
    }

    public ModelAndView handlecustomerrevenue(HttpServletRequest request, HttpServletResponse response, MisCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        String menuPath = "Mis >>  Report >> Revenue";
        path = "content/MIS/customerwiserevenue.jsp";
        ArrayList customerList = new ArrayList();
        misCommand = command;
        MisTO misTO = new MisTO();

        try {

            String pageTitle = "View Customer Revenue";
            request.setAttribute("pageTitle", pageTitle);

            if (misCommand.getCustomerId() != null && !"".equals(misCommand.getCustomerId())) {
                misTO.setCustomerId(misCommand.getCustomerId());
            }
            if (misCommand.getFromDate() != null && !"".equals(misCommand.getFromDate())) {
                misTO.setFromDate(misCommand.getFromDate());
            }
            if (misCommand.getToDate() != null && !"".equals(misCommand.getToDate())) {
                misTO.setToDate(misCommand.getToDate());
            }

            customerList = misBP.getcustomerList();

            request.setAttribute("customerList", customerList);
            request.setAttribute("customerId", misTO.getCustomerId());
            request.setAttribute("fromDate", misTO.getFromDate());
            request.setAttribute("toDate", misTO.getToDate());
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView customerrevenuebillwise(HttpServletRequest request, HttpServletResponse response, MisCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        String menuPath = "Mis >>  Report >> Revenue Per Bill";
        path = "content/MIS/billwiserevenue.jsp";
        ArrayList customerList = new ArrayList();
        misCommand = command;
        MisTO misTO = new MisTO();

        try {

            String pageTitle = "View Customer Revenue";
            request.setAttribute("pageTitle", pageTitle);

            if (misCommand.getCustomerId() != null && !"".equals(misCommand.getCustomerId())) {
                misTO.setCustomerId(misCommand.getCustomerId());
            }
            if (misCommand.getFromDate() != null && !"".equals(misCommand.getFromDate())) {
                misTO.setFromDate(misCommand.getFromDate());
            }
            if (misCommand.getToDate() != null && !"".equals(misCommand.getToDate())) {
                misTO.setToDate(misCommand.getToDate());
            }

            customerList = misBP.getcustomerList();

            request.setAttribute("customerList", customerList);
            request.setAttribute("customerId", misTO.getCustomerId());
            request.setAttribute("fromDate", misTO.getFromDate());
            request.setAttribute("toDate", misTO.getToDate());
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView revenuefcwise(HttpServletRequest request, HttpServletResponse response, MisCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        String menuPath = "Mis >>  Report >> Revenue Per FC";
        path = "content/MIS/fcwiserevenue.jsp";
        ArrayList FCList = new ArrayList();
        misCommand = command;
        MisTO misTO = new MisTO();

        try {

            String pageTitle = "FC Wise Revenue";
            request.setAttribute("pageTitle", pageTitle);

            if (misCommand.getFcId() != null && !"".equals(misCommand.getFcId())) {
                misTO.setFcId(misCommand.getFcId());
            }
            if (misCommand.getFromDate() != null && !"".equals(misCommand.getFromDate())) {
                misTO.setFromDate(misCommand.getFromDate());
            }
            if (misCommand.getToDate() != null && !"".equals(misCommand.getToDate())) {
                misTO.setToDate(misCommand.getToDate());
            }

            FCList = misBP.getFCList();

            request.setAttribute("FCList", FCList);
            request.setAttribute("fcId", misTO.getFcId());
            request.setAttribute("fromDate", misTO.getFromDate());
            request.setAttribute("toDate", misTO.getToDate());
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView vehiclerevenue(HttpServletRequest request, HttpServletResponse response, MisCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        String menuPath = "Mis >>  Report >> Vehicle Revenue";
        path = "content/MIS/revenuebyvehicle.jsp";
        ArrayList VehicleList = new ArrayList();
        misCommand = command;
        MisTO misTO = new MisTO();

        try {

            String pageTitle = "Vehicle Revenue";
            request.setAttribute("pageTitle", pageTitle);

            if (misCommand.getVehicleId() != null && !"".equals(misCommand.getVehicleId())) {
                misTO.setVehicleId(misCommand.getVehicleId());
            }
            if (misCommand.getFromDate() != null && !"".equals(misCommand.getFromDate())) {
                misTO.setFromDate(misCommand.getFromDate());
            }
            if (misCommand.getToDate() != null && !"".equals(misCommand.getToDate())) {
                misTO.setToDate(misCommand.getToDate());
            }

            VehicleList = misBP.getVehicleList();

            request.setAttribute("VehicleList", VehicleList);
            request.setAttribute("vehicleId", misTO.getVehicleId());
            request.setAttribute("fromDate", misTO.getFromDate());
            request.setAttribute("toDate", misTO.getToDate());
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView vehicletyperevenue(HttpServletRequest request, HttpServletResponse response, MisCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        String menuPath = "Mis >>  Report >> Vehicle Type Revenue";
        path = "content/MIS/revenuebyvehicletype.jsp";
        ArrayList VehicleTypeList = new ArrayList();
        misCommand = command;
        MisTO misTO = new MisTO();

        try {

            String pageTitle = "Vehicle Type Revenue";
            request.setAttribute("pageTitle", pageTitle);

            if (misCommand.getVehicleTypeId() != null && !"".equals(misCommand.getVehicleTypeId())) {
                misTO.setVehicleTypeId(misCommand.getVehicleTypeId());
            }
            if (misCommand.getFromDate() != null && !"".equals(misCommand.getFromDate())) {
                misTO.setFromDate(misCommand.getFromDate());
            }
            if (misCommand.getToDate() != null && !"".equals(misCommand.getToDate())) {
                misTO.setToDate(misCommand.getToDate());
            }

            VehicleTypeList = misBP.getVehicleTypeList();

            request.setAttribute("VehicleTypeList", VehicleTypeList);
            request.setAttribute("vehicleTypeId", misTO.getVehicleTypeId());
            request.setAttribute("fromDate", misTO.getFromDate());
            request.setAttribute("toDate", misTO.getToDate());
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView accountreceivablerevenue(HttpServletRequest request, HttpServletResponse response, MisCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        String menuPath = "Mis >>  Report >> AR Revenue";
        path = "content/MIS/customerwiseaccountreceivable.jsp";
        ArrayList customerList = new ArrayList();
        misCommand = command;
        MisTO misTO = new MisTO();

        try {

            String pageTitle = "View AR Revenue";
            request.setAttribute("pageTitle", pageTitle);

            if (misCommand.getCustomerId() != null && !"".equals(misCommand.getCustomerId())) {
                misTO.setCustomerId(misCommand.getCustomerId());
            }
            if (misCommand.getFromDate() != null && !"".equals(misCommand.getFromDate())) {
                misTO.setFromDate(misCommand.getFromDate());
            }
            if (misCommand.getToDate() != null && !"".equals(misCommand.getToDate())) {
                misTO.setToDate(misCommand.getToDate());
            }

            customerList = misBP.getcustomerList();

            request.setAttribute("customerList", customerList);
            request.setAttribute("customerId", misTO.getCustomerId());
            request.setAttribute("fromDate", misTO.getFromDate());
            request.setAttribute("toDate", misTO.getToDate());
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView accountreceivablebillwiserevenue(HttpServletRequest request, HttpServletResponse response, MisCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        String menuPath = "Mis >>  Report >> AR Revenue Per Bill";
        path = "content/MIS/billwiseaccountreceivable.jsp";
        ArrayList customerList = new ArrayList();
        misCommand = command;
        MisTO misTO = new MisTO();

        try {

            String pageTitle = "View AR Revenue";
            request.setAttribute("pageTitle", pageTitle);

            if (misCommand.getCustomerId() != null && !"".equals(misCommand.getCustomerId())) {
                misTO.setCustomerId(misCommand.getCustomerId());
            }
            if (misCommand.getFromDate() != null && !"".equals(misCommand.getFromDate())) {
                misTO.setFromDate(misCommand.getFromDate());
            }
            if (misCommand.getToDate() != null && !"".equals(misCommand.getToDate())) {
                misTO.setToDate(misCommand.getToDate());
            }

            customerList = misBP.getcustomerList();

            request.setAttribute("customerList", customerList);
            request.setAttribute("customerId", misTO.getCustomerId());
            request.setAttribute("fromDate", misTO.getFromDate());
            request.setAttribute("toDate", misTO.getToDate());
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }


    public ModelAndView customerwiseprofit(HttpServletRequest request, HttpServletResponse response, MisCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        String menuPath = "Mis >>  Report >> Customer Profit";
        path = "content/MIS/customerwiseprofitability.jsp";
        ArrayList customerList = new ArrayList();
        misCommand = command;
        MisTO misTO = new MisTO();

        try {

            String pageTitle = "View Customer Profit";
            request.setAttribute("pageTitle", pageTitle);

            if (misCommand.getCustomerId() != null && !"".equals(misCommand.getCustomerId())) {
                misTO.setCustomerId(misCommand.getCustomerId());
            }
            if (misCommand.getFromDate() != null && !"".equals(misCommand.getFromDate())) {
                misTO.setFromDate(misCommand.getFromDate());
            }
            if (misCommand.getToDate() != null && !"".equals(misCommand.getToDate())) {
                misTO.setToDate(misCommand.getToDate());
            }

            customerList = misBP.getcustomerList();

            request.setAttribute("customerList", customerList);
            request.setAttribute("customerId", misTO.getCustomerId());
            request.setAttribute("fromDate", misTO.getFromDate());
            request.setAttribute("toDate", misTO.getToDate());
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView vehiclewiseprofit(HttpServletRequest request, HttpServletResponse response, MisCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        String menuPath = "Mis >>  Report >> Vehicle Wise Profit";
        path = "content/MIS/vehiclewiseprofitability.jsp";
        ArrayList VehicleList = new ArrayList();
        misCommand = command;
        MisTO misTO = new MisTO();

        try {

            String pageTitle = "Vehicle Wise Profit";
            request.setAttribute("pageTitle", pageTitle);

            if (misCommand.getVehicleId() != null && !"".equals(misCommand.getVehicleId())) {
                misTO.setVehicleId(misCommand.getVehicleId());
            }
            if (misCommand.getFromDate() != null && !"".equals(misCommand.getFromDate())) {
                misTO.setFromDate(misCommand.getFromDate());
            }
            if (misCommand.getToDate() != null && !"".equals(misCommand.getToDate())) {
                misTO.setToDate(misCommand.getToDate());
            }

            VehicleList = misBP.getVehicleList();

            request.setAttribute("VehicleList", VehicleList);
            request.setAttribute("vehicleId", misTO.getVehicleId());
            request.setAttribute("fromDate", misTO.getFromDate());
            request.setAttribute("toDate", misTO.getToDate());
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView fcwiseprofit(HttpServletRequest request, HttpServletResponse response, MisCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        String menuPath = "Mis >>  Report >> FC Wise Profit";
        path = "content/MIS/fcwiseprofitability.jsp";
        ArrayList FCList = new ArrayList();
        misCommand = command;
        MisTO misTO = new MisTO();

        try {

            String pageTitle = "FC Wise Profit";
            request.setAttribute("pageTitle", pageTitle);

            if (misCommand.getFcId() != null && !"".equals(misCommand.getFcId())) {
                misTO.setFcId(misCommand.getFcId());
            }
            if (misCommand.getFromDate() != null && !"".equals(misCommand.getFromDate())) {
                misTO.setFromDate(misCommand.getFromDate());
            }
            if (misCommand.getToDate() != null && !"".equals(misCommand.getToDate())) {
                misTO.setToDate(misCommand.getToDate());
            }

            FCList = misBP.getFCList();

            request.setAttribute("FCList", FCList);
            request.setAttribute("fcId", misTO.getFcId());
            request.setAttribute("fromDate", misTO.getFromDate());
            request.setAttribute("toDate", misTO.getToDate());
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }


     public ModelAndView totaloperationcost(HttpServletRequest request, HttpServletResponse response, MisCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        String menuPath = "Mis >>  Report >> Total Operation Cost";
        path = "content/MIS/totaloperationcost.jsp";
        
        misCommand = command;
        MisTO misTO = new MisTO();

        try {

            String pageTitle = "Total Operation Cost";
            request.setAttribute("pageTitle", pageTitle);

            if (misCommand.getFromDate() != null && !"".equals(misCommand.getFromDate())) {
                misTO.setFromDate(misCommand.getFromDate());
            }
            if (misCommand.getToDate() != null && !"".equals(misCommand.getToDate())) {
                misTO.setToDate(misCommand.getToDate());
            }            
                        
            request.setAttribute("fromDate", misTO.getFromDate());
            request.setAttribute("toDate", misTO.getToDate());
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");        
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
     public ModelAndView totaloperationcostperbill(HttpServletRequest request, HttpServletResponse response, MisCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        String menuPath = "Mis >>  Report >> Total Operation Cost Bill Wise";
        path = "content/MIS/totaloperationcostperbill.jsp";

        misCommand = command;
        MisTO misTO = new MisTO();

        try {

            String pageTitle = "Total Operation Cost Bill Wise";
            request.setAttribute("pageTitle", pageTitle);

            if (misCommand.getFromDate() != null && !"".equals(misCommand.getFromDate())) {
                misTO.setFromDate(misCommand.getFromDate());
            }
            if (misCommand.getToDate() != null && !"".equals(misCommand.getToDate())) {
                misTO.setToDate(misCommand.getToDate());
            }

            request.setAttribute("fromDate", misTO.getFromDate());
            request.setAttribute("toDate", misTO.getToDate());
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
     
     public ModelAndView fuelcostperbill(HttpServletRequest request, HttpServletResponse response, MisCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        String menuPath = "Mis >>  Report >> Fuel Cost Bill Wise";
        path = "content/MIS/fuelcost.jsp";

        misCommand = command;
        MisTO misTO = new MisTO();

        try {

            String pageTitle = "Fuel Cost Bill Wise";
            request.setAttribute("pageTitle", pageTitle);

            if (misCommand.getFromDate() != null && !"".equals(misCommand.getFromDate())) {
                misTO.setFromDate(misCommand.getFromDate());
            }
            if (misCommand.getToDate() != null && !"".equals(misCommand.getToDate())) {
                misTO.setToDate(misCommand.getToDate());
            }

            request.setAttribute("fromDate", misTO.getFromDate());
            request.setAttribute("toDate", misTO.getToDate());
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
     public ModelAndView consolidatedreportpermonth(HttpServletRequest request, HttpServletResponse response, MisCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        String menuPath = "Mis >>  Report >> Consolidated Report";
        path = "content/MIS/consolidatedreportpermonth.jsp";

        misCommand = command;
        MisTO misTO = new MisTO();

        try {

            String pageTitle = "Consolidated Report";
            request.setAttribute("pageTitle", pageTitle);

            if (misCommand.getFromDate() != null && !"".equals(misCommand.getFromDate())) {
                misTO.setFromDate(misCommand.getFromDate());
            }
            if (misCommand.getToDate() != null && !"".equals(misCommand.getToDate())) {
                misTO.setToDate(misCommand.getToDate());
            }

            request.setAttribute("fromDate", misTO.getFromDate());
            request.setAttribute("toDate", misTO.getToDate());
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
     public ModelAndView consolidatedreportperbill(HttpServletRequest request, HttpServletResponse response, MisCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        String menuPath = "Mis >>  Report >> Consolidated Report Bill Wise";
        path = "content/MIS/consolidatedreportperbill.jsp";

        misCommand = command;
        MisTO misTO = new MisTO();

        try {

            String pageTitle = "Consolidated Report Bill Wise";
            request.setAttribute("pageTitle", pageTitle);

            if (misCommand.getFromDate() != null && !"".equals(misCommand.getFromDate())) {
                misTO.setFromDate(misCommand.getFromDate());
            }
            if (misCommand.getToDate() != null && !"".equals(misCommand.getToDate())) {
                misTO.setToDate(misCommand.getToDate());
            }

            request.setAttribute("fromDate", misTO.getFromDate());
            request.setAttribute("toDate", misTO.getToDate());
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
}
