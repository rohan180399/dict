<%-- 
    Document   : manageCustomerExport
    Created on : Jan 12, 2021, 5:37:43 PM
    Author     : hp
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>
        <form name="manufacturer" method="post">
            <%
                            Date dNow = new Date();
                            SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                            //System.out.println("Current Date: " + ft.format(dNow));
                            String curDate = ft.format(dNow);
                            String expFile = "CustomerLists" + curDate + ".xls";

                            String fileName = "attachment;filename=" + expFile;
                            response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                            response.setHeader("Content-disposition", fileName);
            %>

            <br>

            <c:if test="${CustomerLists !=nul}">
                <table style="width:auto" align="center" border="1" id="table" class="sortable">
                    <thead>
                        <tr height="80">
                            <td><h3>Sno</h3></td>
                            <td><h3>Customer code</h3></td>
                            <td><h3>Customer Name</h3></td>
                            <td><h3>Customer Mail</h3></td>
                            <td><h3>Customer Type</h3></td>
                            <td><h3>Primary Billing Type</h3></td>
                            <td><h3>Location</h3></td>
                            <td><h3>Enrolled Date</h3></td>
                            <td><h3>Created By</h3></td>                            
                            <td><h3>Customer Address</h3></td>
                            <td><h3>Pincode</h3></td>
                            <td><h3>PAN no.</h3></td>
                            <td><h3>GST no.</h3></td>
                            <td><h3>ERP Id</h3></td>
                            <td><h3>Active</h3></td>
                            <td><h3>Contract Type</h3></td>
                        </tr>
                    </thead>
                    <tbody>
                        <%int sno=1;
                        int index =0;
                        String classText="";
                        %>
                        <c:forEach items="${CustomerLists}" var="CustomerList">
                            <%int oddEven = index % 2;
                          if (oddEven > 0) {
                                     classText = "text2";
                                 } else {
                                     classText = "text1";
                                 }
                            %>
                            <tr>
                                <td  height="30"><%=sno++%></td>
                                <td  height="30"><c:out value="${CustomerList.customerCode}"/></td>
                                <td  height="30" align="left"><c:out value="${CustomerList.custName}"/></td>
                                <td  height="30" align="left"><c:out value="${CustomerList.custEmail}"/></td>
                                <td  height="30"><c:out value="${CustomerList.customerTypeName}"/></td>
                                <td  height="30"><c:out value="${CustomerList.billingTypeName}"/></td>
                                <td  height="30" width="auto" align="left"><c:out value="${CustomerList.custCity}"/></td>
                                <td  height="30" width="auto" align="left"><c:out value="${CustomerList.enrollDate}"/></td>
                                <td  height="30" align="left"><c:out value="${CustomerList.createdBy}"/></td>  

                                <td  height="30" align="left"><c:out value="${CustomerList.custAddress}"/></td>  
                                <td  height="30" align="left"><c:out value="${CustomerList.pinCode}"/></td>  
                                <td  height="30" align="left"><c:out value="${CustomerList.panNo}"/></td>  
                                <td  height="30" align="left"><c:out value="${CustomerList.gstNo}"/></td>  
                                <td  height="30" align="left"><c:out value="${CustomerList.erpId}"/></td>  
                                <td  height="30" align="left"><c:out value="${CustomerList.custStatus}"/></td>  
                                <td  height="30">
                                    
                                    <c:if test="${(customerList.custTypeId == 1)}" >
                                        Credit
                                    </c:if>
                                    <c:if test="${(customerList.custTypeId == 2)}" >
                                        PDA
                                    </c:if>
                                    <c:if test="${(customerList.custTypeId == null)}" >
                                        -
                                    </c:if>
                                        
                                </td>
                            </tr>
                            <%index++;%>
                        </c:forEach>
                    </tbody>
                </table>

            </c:if>

            <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

    </body>
</html>
