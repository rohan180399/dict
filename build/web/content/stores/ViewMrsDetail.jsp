<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<!--<title>PAPL</title>-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->

<script>

function addRow(val){
    if(document.getElementById(val).innerHTML == ""){
    document.getElementById(val).innerHTML = "<input type='text' size='7' class='form-control' >";
    }else{
    document.getElementById(val).innerHTML = "";
    }
}
function newWO(val){
        window.open('/throttle/OtherServiceStockAvailability.do?itemId='+val, 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
    }
    function back(){
        document.spareIssue.action='/throttle/MRSList.do';
        document.spareIssue.submit();
        }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.ViewMRS"  text="MRS"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.stores"  text="Stores"/></a></li>
            <li class="active"><spring:message code="stores.label.ViewMRS"  text="MRS"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
<body>
<form name="spareIssue"  method="post" >
 <%--<%@ include file="/content/common/path.jsp" %>--%>


<%@ include file="/content/common/message.jsp" %>
    <c:if test = "${vehicleDetails != null}" >
<table class="table table-info mb30 table-hover" id="bg" >
    <thead>
<tr>
<th  colspan="4" align="center" height="30"><div ><spring:message code="stores.label.VehicleDetails"  text="default text"/></div></th>
</tr>
</thead>

      <c:forEach items="${vehicleDetails}" var="vehicle">

<tr>
<td  height="30"><spring:message code="stores.label.VehicleNumber"  text="default text"/></td>
<td  height="30"><c:out value="${vehicle.mrsVehicleNumber}"/></td>
<td  height="30"><spring:message code="stores.label.KM"  text="default text"/></td>
<td  height="30"><c:out value="${vehicle.mrsVehicleKm}"/></td>
</tr>
<tr>
<td  height="30"><spring:message code="stores.label.VehicleType"  text="default text"/></td>
<td  height="30"><c:out value="${vehicle.mrsVehicleType}"/></td>
<td  height="30"><spring:message code="stores.label.MFR"  text="default text"/></td>
<td  height="30"><c:out value="${vehicle.mrsVehicleMfr}"/></td>
</tr>

<tr>
<td  height="30"><spring:message code="stores.label.UseType"  text="default text"/></td>
<td  height="30"><c:out value="${vehicle.mrsVehicleUsageType}"/></td>
<td  height="30"><spring:message code="stores.label.Model"  text="default text"/></td>
<td  height="30"><c:out value="${vehicle.mrsVehicleModel}"/></td>
</tr>

<tr>
<td  height="30"><spring:message code="stores.label.EngineNo"  text="default text"/></td>
<td  height="30"><c:out value="${vehicle.mrsVehicleNumber}"/></td>
<td  height="30"><spring:message code="stores.label.ChasisNo"  text="default text"/></td>
<td  height="30"><c:out value="${vehicle.mrsVehicleChassisNumber}"/></td>
</tr>

<tr>
<td  height="30"><spring:message code="stores.label.Technician"  text="default text"/></td>
<td  height="30"><c:out value="${vehicle.mrsTechnician}"/></td>
<td  height="30"><spring:message code="stores.label.JobCardNo"  text="default text"/>.</td>
<td  height="30"><c:out value="${vehicle.mrsJobCardNumber}"/></td>
</tr>

 </c:forEach>
 </c:if>
</table>
<br>



    <c:if test = "${rcMrsDetails != null}" >
<table class="table table-info mb30 table-hover" id="bg" >
    <thead>
<tr>
<td  colspan="4" align="center" height="30"><spring:message code="stores.label.RCDetails"  text="default text"/></td>
</tr>
</thead>

      <c:forEach items="${rcMrsDetails}" var="rc">

<tr>
<td  height="30"><spring:message code="stores.label.WorkOrderNo"  text="default text"/></td>
<td  height="30"><c:out value="${rc.rcWorkId}"/></td>
<td  height="30"><spring:message code="stores.label.RequestedDate"  text="default text"/></td>
<td  height="30"><c:out value="${rc.issuedOn}"/></td>
</tr>
<tr>
<td  height="30"><spring:message code="stores.label.VendorName"  text="default text"/></td>
<td  height="30"><c:out value="${rc.vendorName}"/></td>
<td  height="30"><spring:message code="stores.label.Technician"  text="default text"/></td>
<td  height="30"><c:out value="${rc.mrsTechnician}"/></td>
</tr>

 </c:forEach>
 </c:if>
</table>
    <c:if test = "${counterMrsDetails != null}" >
<table align="center" border="0" cellpadding="0" cellspacing="0" width="400" id="bg" class="border">
<tr>
<td  colspan="4" align="center" height="30"><div ><spring:message code="stores.label.CounterSaleDetails"  text="default text"/></div></td>
</tr>

      <c:forEach items="${counterMrsDetails}" var="counter">

<tr>
<td  height="30"><spring:message code="stores.label.CounterSaleNo"  text="default text"/></td>
<td  height="30"><c:out value="${counter.counterId}"/></td>
<td  height="30"><spring:message code="stores.label.CustomerName"  text="default text"/></td>
<td  height="30"><c:out value="${counter.name}"/></td>
</tr>
<tr>
<td  height="30"><spring:message code="stores.label.CustomerAddress"  text="default text"/></td>
<td  height="30"><c:out value="${counter.address}"/></td>
<td  height="30"><spring:message code="stores.label.MobileNumber"  text="default text"/></td>
<td  height="30"><c:out value="${counter.mobileNo}"/></td>
</tr>
<tr>
<td  height="30"><spring:message code="stores.label.RequestedDate"  text="default text"/></td>
<td  height="30"><c:out value="${counter.issuedOn}"/></td>
<td  height="30">&nbsp;</td>
<td  height="30">&nbsp;</td>
</tr>

 </c:forEach>
 </c:if>
</table>


<br>
 <c:if test = "${mrsDetails != null}" >
<table class="table table-info mb30 table-hover">
    <thead>
  <tr >
    <th colspan="9" ><center><spring:message code="stores.label.MRSDetails"  text="default text"/></center> </th>
  </tr>
  <tr>
    <th  ><div ><spring:message code="stores.label.Sno"  text="default text"/></div></th>
    <th  ><div ><spring:message code="stores.label.ItemCode"  text="default text"/></div> </th>
    <th  height="15" ><div  align="left"><spring:message code="stores.label.PAPLCode"  text="default text"/></div></th>
    <th  height="15" ><div ><spring:message code="stores.label.ItemName"  text="default text"/></div></th>
    <th height="15" colspan="2" ><div ><spring:message code="stores.label.AvailableStock"  text="default text"/></div></th>
    <th  height="15" ><div ><spring:message code="stores.label.StockAtotherServicePoint"  text="default text"/></div></th>
    <th  height="15" ><div ><spring:message code="stores.label.RequiredQty"  text="default text"/></div></th>
    <th  height="15" ><div ><spring:message code="stores.label.POQty"  text="default text"/></div></th>
 <!--<th></th>-->
  </tr>
  <tr>
    <th >&nbsp;</th>
    <th >&nbsp;</th>
    <th height="30" ><div align="left"></div></th>
    <th height="30" >&nbsp;</th>
    <th width="52" height="30" ><spring:message code="stores.label.New"  text="default text"/></th>
    <th width="57" ><spring:message code="stores.label.RC"  text="default text"/></th>
    <th height="30" >&nbsp;</th>
    <th height="30" >&nbsp;</th>
    <th height="30" >&nbsp;</th>
 </thead>
  </tr>
   <% int index = 0; %>
      <c:forEach items="${mrsDetails}" var="mrs">
<%
   String classText = "";
    int oddEven = index % 2;
    if (oddEven > 0) {
    classText = "text2";
    } else {
    classText = "text1";
    }
    %>
  <tr>
    <td class="<%=classText %>"><%=index+1%></td>
    <td class="<%=classText %>"><c:out value="${mrs.mrsItemMfrCode}"/></td>
    <td class="<%=classText %>" height="30"><div align="left"><c:out value="${mrs.mrsPaplCode}"/></div></td>
    <td class="<%=classText %>" height="30"><c:out value="${mrs.mrsItemName}"/></td>
    <td height="30" class="<%=classText %>"><div align="center"><c:out value="${mrs.mrsLocalServiceItemSum}"/></div></td>
    <td height="30" class="<%=classText %>"><div align="center"><c:out value="${mrs.qty}"/></div></td>
    <td class="<%=classText %>" height="30"><div align="center"><a href="" onClick="newWO(<c:out value="${mrs.mrsItemId}"/>)"><c:out value="${mrs.mrsOtherServiceItemSum}"/></a></div></td>
    <td class="<%=classText %>" height="30"><div align="center"><c:out value="${mrs.mrsRequestedItemNumber}"/></div></td>
    <td class="<%=classText %>" height="30"><div align="center"><c:out value="${mrs.poQuantity}"/></div></td>
    <!--<td></td>-->
  </tr>
  <% index++; %>
  </c:forEach>
   </c:if>
</table>
<center>
<input type="button" value="<spring:message code="stores.label.Back"  text="default text"/>" class="btn btn-success" onclick="back();">
</center>

<br>

<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
    </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
