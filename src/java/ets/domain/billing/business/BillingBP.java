/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.billing.business;

import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.billing.data.BillingDAO;
import ets.domain.operation.data.OperationDAO;
import java.io.IOException;
import java.util.ArrayList;
import com.ibatis.sqlmap.client.SqlMapClient;
import java.util.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author vinoth
 */
public class BillingBP {

    private BillingDAO billingDAO;
    private OperationDAO operationDAO;

    public BillingDAO getBillingDAO() {
        return billingDAO;
    }

    public void setBillingDAO(BillingDAO billingDAO) {
        this.billingDAO = billingDAO;
    }

    public OperationDAO getOperationDAO() {
        return operationDAO;
    }

    public void setOperationDAO(OperationDAO operationDAO) {
        this.operationDAO = operationDAO;
    }

    public ArrayList getOrdersForBilling(BillingTO billingTO) throws FPRuntimeException, FPBusinessException {
        ArrayList clisedTrips = null;
        clisedTrips = billingDAO.getOrdersForBilling(billingTO);
        return clisedTrips;
    }

    public ArrayList getOrdersForSupplementBilling(BillingTO billingTO) throws FPRuntimeException, FPBusinessException {
        ArrayList clisedTrips = null;
        clisedTrips = billingDAO.getOrdersForSupplementBilling(billingTO);
        return clisedTrips;
    }

    public ArrayList getRepoOrdersForBilling(BillingTO billingTO) throws FPRuntimeException, FPBusinessException {
        ArrayList clisedTrips = null;
        clisedTrips = billingDAO.getRepoOrdersForBilling(billingTO);
        return clisedTrips;
    }

    public ArrayList getOrdersToBeBillingDetails(BillingTO billingTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = billingDAO.getOrdersToBeBillingDetails(billingTO);
        return tripDetails;
    }

    public ArrayList getOrdersToBeSupplementBillingDetails(BillingTO billingTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = billingDAO.getOrdersToBeSupplementBillingDetails(billingTO);
        return tripDetails;
    }

    public ArrayList getOrdersOtherExpenseDetails(BillingTO billingTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = billingDAO.getOrderOtherExpenseDetails(billingTO);
        return tripDetails;
    }

    public ArrayList getOrdersSupplementOtherExpenseDetails(BillingTO billingTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = billingDAO.getOrdersSupplementOtherExpenseDetails(billingTO);
        return tripDetails;
    }

    public ArrayList getTripDetails(BillingTO billingTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = billingDAO.getTripDetails(billingTO);
        return tripDetails;
    }

    public String getCustGtaType(BillingTO billingTO) throws FPBusinessException, FPRuntimeException {
        String getCustGtaType = "";
        
               getCustGtaType = billingDAO.getCustGtaType(billingTO);
        return getCustGtaType;
    }

    public int saveOrderBill(BillingTO billingTO, int userId, String finYear) throws FPRuntimeException, FPBusinessException, IOException, Exception {
        int status = 0;
        int invoiceId = 0;
        SqlMapClient session = billingDAO.getSqlMapClient();
        boolean exceptionStatus = false;
        Exception excp = null;
        try {
            synchronized(this){
            session.startTransaction();

                  
                  
                  
            ArrayList ordersToBeBilledDetails = billingDAO.getOrdersToBeBillingDetails1(billingTO, session);
            ArrayList ordersOtherExpenseDetails = new ArrayList();

            String grDate = "";
            Iterator itr7 = ordersToBeBilledDetails.iterator();
            BillingTO billingTONew12 = null;
            while (itr7.hasNext()) {
                billingTONew12 = new BillingTO();
                billingTONew12 = (BillingTO) itr7.next();
                //grDate = billingTONew12.getToDate();
                grDate = billingTONew12.getGrDate();
            }
            System.out.println(" selected  finYear : " + finYear);
//            String compareDate = "31-03-2018";
//           // String june = "30-06-2017";
//            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
//            Date d1 = sdf.parse(compareDate);
//            Date d2 = sdf.parse(grDate);
//            System.out.println("compareDate " + compareDate);
//            System.out.println("grDate " + grDate);
//            System.out.println("d2... " + d2);
//            String finacialYear = "";
//            String currentYear = "";
            String invoiceCode = "";
            String fromDate = "";
            String invoiceCodeSequence = "";
            int gstCheck = 1;
            
            System.out.println("billingTO.getGstType() invoice BP----"+billingTO.getGstType());
            //if (d2.after(d1)) { // After marc 31st 2019
            
            /////////////////// gst type needs to check here //////////////////////
            
            if("N".equals(billingTO.getGstType()) || billingTO.getGstType() == null) { 
                  if (finYear.equalsIgnoreCase("2024")) { // After marc 31st 2022

                    Calendar now = Calendar.getInstance();
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = new Date();
                    System.out.println("1111111111111 ");
                    invoiceCode = "2324TBS";
                      System.out.println("invoiceCodeMINE---"+invoiceCode);
                    fromDate = dateFormat.format(date);
                    invoiceCodeSequence = billingDAO.getInvoiceCodeSequenceBos2324(session, gstCheck);
                    
                    System.out.println(" invoiceCodeSequence " + invoiceCodeSequence);
                    System.out.println(" invoiceCode" + invoiceCode);
                    System.out.println(" fromDate" + fromDate);

                } else { //Befoe
                    System.out.println("1111111111 before ");
                    fromDate = "2023-03-31 22:00:00";
                    invoiceCode = "2223TBS";
                    System.out.println("invoiceCodeMINE---"+invoiceCode);
                    invoiceCodeSequence = billingDAO.getInvoiceCodeSequenceBos2223(session, gstCheck);
                    System.out.println(" invoiceCodeSequence " + invoiceCodeSequence);
                    System.out.println(" invoiceCode" + invoiceCode);
                    System.out.println(" fromDate" + fromDate);
                }
            } else {
                if (finYear.equalsIgnoreCase("2024")) { // After marc 31st 2022

                    Calendar now = Calendar.getInstance();
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = new Date();
                    System.out.println("2222222222222 ");
                    invoiceCode = "2324TPG";
                    fromDate = dateFormat.format(date);
                    invoiceCodeSequence = billingDAO.getInvoiceCodeSequence2324(session, gstCheck);
                    System.out.println(" invoiceCodeSequence " + invoiceCodeSequence);
                    System.out.println(" invoiceCode" + invoiceCode);
                    System.out.println(" fromDate" + fromDate);

                } else { //Befoe
                    System.out.println("22222222 before ");
                    fromDate = "2023-03-31 22:00:00";
                    invoiceCode = "2223TPG";
                    invoiceCodeSequence = billingDAO.getInvoiceCodeSequence2223(session, gstCheck);
                    System.out.println(" invoiceCodeSequence " + invoiceCodeSequence);
                    System.out.println(" invoiceCode" + invoiceCode);
                    System.out.println(" fromDate" + fromDate);
                }
            }

            billingTO.setFromDate(fromDate);
            invoiceCode = invoiceCode + invoiceCodeSequence;
            billingTO.setInvoiceCode(invoiceCode);
            billingTO.setInvoiceNo(billingTO.getInvoiceNo());
            String totalRevenue = billingDAO.getBillingTotalRevenue(billingTO, session);
            String totalExpense = billingDAO.getBillingTotalExpense(billingTO, session);
            System.out.println("billingTO.getTotalTax()" + billingTO.getTotalTax());
            String grandTotal = Float.parseFloat(totalRevenue) + Float.parseFloat(totalExpense)
                    + Float.parseFloat(billingTO.getTotalTax()) + "";
            billingTO.setTotalRevenue(totalRevenue);
            billingTO.setOtherExpense(totalExpense);
            billingTO.setGrandTotal(grandTotal);
            

            //savebillheader
            invoiceId = billingDAO.saveBillHeader(billingTO, session);
            billingTO.setInvoiceId("" + invoiceId);
            int invoiceDetailId = 0;
            if (invoiceId > 0) {
                int tax = billingDAO.saveBillTax(billingTO, session);
                System.out.println("tax----"+tax);
                Iterator itr = ordersToBeBilledDetails.iterator();
                BillingTO billingTONew = null;
                while (itr.hasNext()) {
                    billingTONew = new BillingTO();
                    billingTONew = (BillingTO) itr.next();
                    billingTONew.setInvoiceId("" + invoiceId);
                    billingTONew.setInvoiceCode(invoiceCode);
                    billingTONew.setTotalRevenue(billingTONew.getEstimatedRevenue());
                    // billingTONew.setOtherExpense(otherExpense);
                    billingTONew.setGrandTotal(grandTotal);
                    billingTONew.setIgstAmount(billingTO.getIgstAmount());
                    invoiceDetailId = billingDAO.saveBillDetails(billingTONew,billingTO, session);

                    ordersOtherExpenseDetails = billingDAO.getOrderOtherExpenseDetails1(billingTONew, session);
                    Iterator itr1 = ordersOtherExpenseDetails.iterator();
                    BillingTO billingTONew1 = null;
                    while (itr1.hasNext()) {
                        billingTONew1 = new BillingTO();
                        billingTONew1 = (BillingTO) itr1.next();
                        billingTONew1.setTripId(billingTONew.getTripId());
                        billingTONew1.setInvoiceId("" + invoiceId);
                        billingTONew1.setInvoiceDetailId("" + invoiceDetailId);
                        billingTONew1.setInvoiceCode(invoiceCode);
                        status = billingDAO.saveBillDetailExpense(billingTONew1, session);
                    }

                    billingTONew.setTripSheetId(billingTONew.getTripId());
                    billingTONew.setStatusId("16");
                    status = billingDAO.updateStatus(billingTONew, userId, session);

                    System.out.println("status---" + status);
                    //update trip status to billed**********************************************************************
                }
            }
            
//            Iterator itr = ordersToBeBilledDetails.iterator();
//                BillingTO billingTONew = null;
//                while (itr.hasNext()) {
//                    billingTONew = new BillingTO();
//                    billingTONew = (BillingTO) itr.next();

            status = operationDAO.updateInvoiceOustanding(billingTO, userId, session);
            System.out.println("status---$$" + status);

//    }
            session.commitTransaction();
             }
        } catch (Exception e) {
            exceptionStatus = true;
            excp = e;
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
            if (exceptionStatus) {
                throw excp;
            }
        }
        return invoiceId;
    }

    public int saveOrderSupplementBill(BillingTO billingTO, String[] supplementsCharges, String[] detaintionCharge, String[] tollCharge, String[] greenTax, String[] otherExpense, String[] weightmentCharge, String[] tripIds, int userId, String finYear) throws FPRuntimeException, FPBusinessException, IOException, Exception {
        int status = 0;
        int invoiceId = 0;
        SqlMapClient session = billingDAO.getSqlMapClient();
        boolean exceptionStatus = false;
        Exception excp = null;
        try {
            synchronized(this){
            session.startTransaction();

            ArrayList ordersToBeBilledDetails = billingDAO.getOrdersToBeSuppBillingDetails1(billingTO, session);
            ArrayList ordersOtherExpenseDetails = new ArrayList();

            String grDate = "";
            Iterator itr7 = ordersToBeBilledDetails.iterator();
            BillingTO billingTONew12 = null;
            while (itr7.hasNext()) {
                billingTONew12 = new BillingTO();
                billingTONew12 = (BillingTO) itr7.next();
                //grDate = billingTONew12.getToDate();
                grDate = billingTONew12.getGrDate();
                System.out.println("$$$$$$$" + grDate);
            }
            System.out.println(" grdate" + grDate);
            String compareDate = "31-03-2018";
            String june = "30-06-2017";
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date d1 = sdf.parse(compareDate);
            Date d2 = sdf.parse(grDate);
            System.out.println("compareDate " + compareDate);
            System.out.println("grDate " + grDate);
            System.out.println("d2... " + d2);
            String finacialYear = "";
            String invoiceCode = "";
            String currentYear = "";
            String fromDate = "";
            String invoiceCodeSequence = "";
            int gstCheck = 1;
            System.out.println("finYear0-----" + finYear);
            //if (d2.after(d1)) { // After marc 31st 2020
            
            System.out.println("billingTO.getGstType()-----"+billingTO.getGstType());
            
            if("N".equals(billingTO.getGstType()) || billingTO.getGstType() == null) {
                if (finYear.equalsIgnoreCase("2024")) {

                    Calendar now = Calendar.getInstance();
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = new Date();
                    System.out.println("1111111111111 ");
                    invoiceCode = "2324TBSSUPP";
                    fromDate = dateFormat.format(date);
                    invoiceCodeSequence = billingDAO.getSuppInvoiceCodeSequenceBos2324(session, gstCheck);
                    System.out.println(" invoiceCodeSequenceinvoiceCodeSequenceinvoiceCodeSequence" + invoiceCodeSequence);
                    System.out.println(" invoiceCode" + invoiceCode);
                    System.out.println(" fromDate" + fromDate);

                } else { //Befoe
                    System.out.println("22222222 ");
                    fromDate = "2023-03-31 22:00:00";
                    invoiceCode = "2223TBSSUPP";
                    invoiceCodeSequence = billingDAO.getSuppInvoiceCodeSequenceBos2223(session, gstCheck);
                    System.out.println(" invoiceCodeSequenceinvoiceCodeSequenceinvoiceCodeSequence" + invoiceCodeSequence);
                    System.out.println(" invoiceCode" + invoiceCode);
                    System.out.println(" fromDate" + fromDate);
                }
            }else{
                if (finYear.equalsIgnoreCase("2024")) {

                    
                    
                    Calendar now = Calendar.getInstance();
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = new Date();
                    System.out.println("1111111111111 ");
                    invoiceCode = "2324TPGSUPP";
                    fromDate = dateFormat.format(date);
                    invoiceCodeSequence = billingDAO.getSuppInvoiceCodeSequence2324(session, gstCheck);
                    System.out.println(" invoiceCodeSequenceinvoiceCodeSequenceinvoiceCodeSequence" + invoiceCodeSequence);
                    System.out.println(" invoiceCode" + invoiceCode);
                    System.out.println(" fromDate" + fromDate);

                } else { //Befoe
                    System.out.println("22222222 ");
                    fromDate = "2023-03-31 22:00:00";
                    invoiceCode = "2223TPGSUPP";
                    invoiceCodeSequence = billingDAO.getSuppInvoiceCodeSequence2223(session, gstCheck);
                    System.out.println(" invoiceCodeSequenceinvoiceCodeSequenceinvoiceCodeSequence" + invoiceCodeSequence);
                    System.out.println(" invoiceCode" + invoiceCode);
                    System.out.println(" fromDate" + fromDate);
                }
            }

            billingTO.setFromDate(fromDate);
            invoiceCode = invoiceCode + invoiceCodeSequence;
            billingTO.setSuppInvoiceNo(invoiceCode);
            billingTO.setInvoiceNo(billingTO.getInvoiceNo());
//            String totalRevenue = billingDAO.getBillingTotalRevenue(billingTO, session);
            String totalExpense = billingDAO.getSuppBillingTotalExpense(billingTO, session);
            System.out.println("billingTO.getTotalTax()" + billingTO.getTotalTax());
            //  String grandTotal = Float.parseFloat(totalRevenue) + Float.parseFloat(totalExpense)
            //         + Float.parseFloat(billingTO.getTotalTax()) + "";
            //  billingTO.setTotalRevenue(totalRevenue);
           
            billingTO.setOtherExpense(totalExpense);
            // billingTO.setGrandTotal(grandTotal);

            //savebillheader
            invoiceId = billingDAO.saveSupplementBillHeader(billingTO, session);
            billingTO.setInvoiceId("" + invoiceId);
            int invoiceDetailId = 0;
            if (invoiceId > 0) {
                int tax = billingDAO.saveSupplementBillTax(billingTO, session);
                Iterator itr = ordersToBeBilledDetails.iterator();
                BillingTO billingTONew = null;

                int i = 0;
                while (itr.hasNext()) {
                    billingTONew = new BillingTO();
                    billingTONew = (BillingTO) itr.next();
                    billingTONew.setInvoiceId("" + invoiceId);
                    billingTONew.setInvoiceCode(invoiceCode);

                    billingTONew.setTotalRevenue(supplementsCharges[i]);
                    billingTONew.setGrandTotal(supplementsCharges[i]);
                    billingTONew.setIgstAmount(billingTO.getIgstAmount());
                    invoiceDetailId = billingDAO.saveSupplementBillDetails(billingTONew,billingTO, session);

                    ordersOtherExpenseDetails = billingDAO.getOrderOtherExpenseSupplementaryDetails(billingTONew, session);
                    Iterator itr1 = ordersOtherExpenseDetails.iterator();
                    BillingTO billingTONew1 = null;
                    while (itr1.hasNext()) {
                        billingTONew1 = new BillingTO();
                        billingTONew1 = (BillingTO) itr1.next();
                        billingTONew1.setTripId(billingTONew.getTripId());
                        billingTONew1.setInvoiceId("" + invoiceId);
                        billingTONew1.setInvoiceDetailId("" + invoiceDetailId);
                        billingTONew1.setInvoiceCode(invoiceCode);
                        status = billingDAO.saveSuppBillDetailExpense(billingTONew1, session);
                    }

                    billingTONew.setTripSheetId(billingTONew.getTripId());
                    billingTONew.setStatusId("32");
                    status = billingDAO.updateStatus(billingTONew, userId, session);

                    i++;
                    //update trip status to billed**********************************************************************
                }

                //update trip status to billed**********************************************************************
            }
            billingTO.setSuppInvoiceId("" + invoiceId);
            status = operationDAO.updateSuppInvoiceOustanding(billingTO, userId, session);
            System.out.println("status--supplementry-$$" + status);

            session.commitTransaction();
        }
        } catch (Exception e) {
            exceptionStatus = true;
            excp = e;
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
            if (exceptionStatus) {
                throw excp;
            }
        }
        return invoiceId;
    }
//    public int saveBillHeader(BillingTO billingTO) throws FPRuntimeException, FPBusinessException {
//        int status = 0;
//        status = billingDAO.saveBillHeader(billingTO);
//        return status;
//    }
//    
//     public int saveBillDetails(BillingTO billingTO) throws FPRuntimeException, FPBusinessException {
//        int status = 0;
//        status = billingDAO.saveBillDetails(billingTO);
//        return status;
//    }
//    public int saveBillDetailExpense(BillingTO billingTO) throws FPRuntimeException, FPBusinessException {
//        int status = 0;
//        status = billingDAO.saveBillDetailExpense(billingTO);
//        return status;
//    }
//    
//    public int updateStatus(BillingTO billingTO, int userId) throws FPRuntimeException, FPBusinessException {
//        int status = 0;
//        status = billingDAO.updateStatus(billingTO, userId);
//        return status;
//    }

    public ArrayList viewBillForSubmission(String customerId, String fromDate, String toDate, String tripType, String submitStatus, String billNo, String grNo, String containerNo) throws FPRuntimeException, FPBusinessException {
        ArrayList closedBill = null;
        closedBill = billingDAO.viewBillForSubmission(customerId, fromDate, toDate, tripType, submitStatus, billNo, grNo, containerNo);
        return closedBill;
    }

    public ArrayList viewSuppBillForSubmission(String customerId, String fromDate, String toDate, String tripType, String submitStatus, String billNo, String grNo) throws FPRuntimeException, FPBusinessException {
        ArrayList closedBill = null;
        closedBill = billingDAO.viewSuppBillForSubmission(customerId, fromDate, toDate, tripType, submitStatus, billNo, grNo);
        return closedBill;
    }

    public int submitBill(String invoiceId, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = billingDAO.submitBill(invoiceId, userId);
        return status;
    }

    public int updateCancel(String billingNo, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = billingDAO.updateCancel(billingNo, userId);
        return status;
    }

    public ArrayList getPreviewHeader(BillingTO billingTO) throws FPRuntimeException, FPBusinessException {
        ArrayList closedBill = null;
        closedBill = billingDAO.getPreviewHeader(billingTO);
        return closedBill;
    }

    public ArrayList getPreviewSupplementHeader(BillingTO billingTO) throws FPRuntimeException, FPBusinessException {
        ArrayList closedBill = null;
        closedBill = billingDAO.getPreviewSupplementHeader(billingTO);
        return closedBill;
    }

    public ArrayList getBillingDetailsForPreview(BillingTO billingTO) throws FPRuntimeException, FPBusinessException {
        ArrayList closedBill = null;
        closedBill = billingDAO.getBillingDetailsForPreview(billingTO);
        return closedBill;
    }

    public ArrayList getSupplementBillingDetailsForPreview(BillingTO billingTO) throws FPRuntimeException, FPBusinessException {
        ArrayList closedBill = null;
        closedBill = billingDAO.getSupplementBillingDetailsForPreview(billingTO);
        return closedBill;
    }

    public ArrayList getGSTTaxDetails(BillingTO billingTO) throws FPBusinessException, FPRuntimeException {
        ArrayList gstDetails = new ArrayList();
        gstDetails = billingDAO.getGSTTaxDetails(billingTO);
        return gstDetails;
    }

    public ArrayList getInvoiceGSTTaxDetails(BillingTO billingTO) throws FPBusinessException, FPRuntimeException {
        ArrayList gstDetails = new ArrayList();
        gstDetails = billingDAO.getInvoiceGSTTaxDetails(billingTO);
        return gstDetails;
    }

    public ArrayList getInvoiceSupplementGSTTaxDetails(BillingTO billingTO) throws FPBusinessException, FPRuntimeException {
        ArrayList gstDetails = new ArrayList();
        gstDetails = billingDAO.getInvoiceSupplementGSTTaxDetails(billingTO);
        return gstDetails;
    }

    public int updateTripGst(String invoiceId, String[] tripIds, String[] tripIdIGSTAmount, String[] tripIdCGSTAmount, String[] tripIdSGSTAmount) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = billingDAO.updateTripGst(invoiceId, tripIds, tripIdIGSTAmount, tripIdCGSTAmount, tripIdSGSTAmount);
        return status;
    }

    public ArrayList getInvoiceCreditNoteDetails(String creditNoteId) throws FPBusinessException, FPRuntimeException {
        ArrayList creditDetails = new ArrayList();
        creditDetails = billingDAO.getInvoiceCreditNoteDetails(creditNoteId);
        return creditDetails;
    }

    public ArrayList getCustomerInvoiceList(BillingTO billingTO) throws FPBusinessException, FPRuntimeException {
        ArrayList gstDetails = new ArrayList();
        gstDetails = billingDAO.getCustomerInvoiceList(billingTO);
        return gstDetails;
    }

    public ArrayList getCustomerPaymentHistory(BillingTO billingTO) throws FPBusinessException, FPRuntimeException {
        ArrayList gstDetails = new ArrayList();
        gstDetails = billingDAO.getCustomerPaymentHistory(billingTO);
        return gstDetails;
    }

    public int saveInvoicePayment(String[] invoiceIds, String[] InvoiceCodes, String[] invoiceDates, String[] invoiceAmounts, String[] modes, String[] modeNos, String[] bankNames, String[] paidAmounts, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = billingDAO.saveInvoicePayment(invoiceIds, InvoiceCodes, invoiceDates, invoiceAmounts, modes, modeNos, bankNames, paidAmounts, userId);
        return status;
    }

    public int saveCustomerPayment(String customerId, String modes, String modeNos, String bankNames, String payDate, String paidAmounts, String custType, int userId) throws FPRuntimeException, FPBusinessException {
        SqlMapClient session = billingDAO.getSqlMapClient();
        boolean exceptionStatus = false;
        int status = 0;
        String receiptNo = "";
        Exception excp = null;
        try {
            session.startTransaction();
            String receiptCode = "";

            if (custType.equals("1")) {
                receiptCode = "2122REC";
                receiptNo = billingDAO.getReceiptSequence(session);
            } else {
                receiptCode = "2122PDA";
                receiptNo = billingDAO.getPdaReceiptSequence(session);
            }

            System.out.println("receiptNo--" + receiptNo);
            System.out.println("receiptCode--" + receiptCode);
            receiptCode = receiptCode + receiptNo;
            status = billingDAO.saveCustomerPayment(customerId, receiptCode, modes, modeNos, bankNames, payDate, paidAmounts, custType, userId, session);
            System.out.println("status---" + status);
            session.commitTransaction();
        } catch (Exception e) {
            exceptionStatus = true;
            excp = e;
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
//            if (exceptionStatus) {
//                throw excp;
//            }
        }
        return status;
    }

    public ArrayList getCustomerStatementHistory(BillingTO billingTO) throws FPBusinessException, FPRuntimeException {
        ArrayList gstDetails = new ArrayList();
        gstDetails = billingDAO.getCustomerStatementHistory(billingTO);
        return gstDetails;
    }

    public int saveTempCreditLimit(BillingTO billingTO, int userId) throws FPBusinessException, FPRuntimeException {

        SqlMapClient session = billingDAO.getSqlMapClient();
        boolean exceptionStatus = false;
        int status = 0;
        String receiptCode = "2122TEM";
        String receiptNo = "";
        Exception excp = null;
        try {
            session.startTransaction();
            receiptNo = billingDAO.getTempReceiptSequence(session);
            System.out.println("receiptNo--" + receiptNo);
            System.out.println("receiptCode--" + receiptCode);
            receiptCode = receiptCode + receiptNo;
            status = billingDAO.saveTempCreditlimit(receiptCode, billingTO, userId, session);
            System.out.println("status---" + status);
            session.commitTransaction();
        } catch (Exception e) {
            exceptionStatus = true;
            excp = e;
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
//            if (exceptionStatus) {
//                throw excp;
//            }
        }
        return status;

    }

    public ArrayList getTempCreditDetails(BillingTO billingTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tempCreditDetails = new ArrayList();
        tempCreditDetails = billingDAO.getTempCreditDetails(billingTO);
        return tempCreditDetails;
    }

    public ArrayList getInvoiceSuppCreditNoteDetails(String creditNoteId) throws FPBusinessException, FPRuntimeException {
        ArrayList creditDetails = new ArrayList();
        creditDetails = billingDAO.getInvoiceSuppCreditNoteDetails(creditNoteId);
        return creditDetails;
    }

    public ArrayList getSuppInvoiceGSTTaxDetails(BillingTO billingTO) throws FPBusinessException, FPRuntimeException {
        ArrayList gstDetails = new ArrayList();
        gstDetails = billingDAO.getSuppInvoiceGSTTaxDetails(billingTO);
        return gstDetails;
    }

    public int saveAdjustPayment(String customerId, String transactionType, String balAmt, String availAmount, String adjustRemark, String adjustAmount, String custType, int userId) throws FPRuntimeException, FPBusinessException {
        SqlMapClient session = billingDAO.getSqlMapClient();
        boolean exceptionStatus = false;
        int status = 0;
        String receiptCode = "2122PAJ";
        String receiptNo = "";
        Exception excp = null;
        try {
            session.startTransaction();
            receiptNo = billingDAO.getAdjReceiptSequence(session);
            System.out.println("receiptNo--" + receiptNo);
            System.out.println("receiptCode--" + receiptCode);
            receiptCode = receiptCode + receiptNo;
            status = billingDAO.saveAdjustPayment(customerId, receiptCode, transactionType, balAmt, availAmount, adjustRemark, adjustAmount, custType, userId, session);
            System.out.println("status---" + status);
            session.commitTransaction();
        } catch (Exception e) {
            exceptionStatus = true;
            excp = e;
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
            //            if (exceptionStatus) {
            //                throw excp;
            //            }
        }
        return status;
    }

    public int saveCreditAdjustPayment(String customerId, String transactionType, String balAmt, String availAmount, String adjustRemark, String adjustAmount, String custType, int userId) throws FPRuntimeException, FPBusinessException {
        SqlMapClient session = billingDAO.getSqlMapClient();
        boolean exceptionStatus = false;
        int status = 0;
        String receiptCode = "2122CAJ";
        String receiptNo = "";
        Exception excp = null;
        try {
            session.startTransaction();
            receiptNo = billingDAO.getCreditAdjReceiptSequence(session);
            System.out.println("receiptNo--" + receiptNo);
            System.out.println("receiptCode--" + receiptCode);
            receiptCode = receiptCode + receiptNo;
            status = billingDAO.saveCreditAdjustPayment(customerId, receiptCode, transactionType, balAmt, availAmount, adjustRemark, adjustAmount, custType, userId, session);
            System.out.println("status---" + status);
            session.commitTransaction();
        } catch (Exception e) {
            exceptionStatus = true;
            excp = e;
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
            //            if (exceptionStatus) {
            //                throw excp;
            //            }
        }
        return status;
    }

    public ArrayList getCustomerAdjustmentHistory(BillingTO billingTO) throws FPBusinessException, FPRuntimeException {
        ArrayList gstDetails = new ArrayList();
        gstDetails = billingDAO.getCustomerAdjustmentHistory(billingTO);
        return gstDetails;
    }

    public ArrayList getCreditAdjustmentHistory(BillingTO billingTO) throws FPBusinessException, FPRuntimeException {
        ArrayList gstDetails = new ArrayList();
        gstDetails = billingDAO.getCreditAdjustmentHistory(billingTO);
        return gstDetails;
    }

    public int approveInvoice(BillingTO billingTO) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = billingDAO.approveInvoice(billingTO);
        return status;
    }

    public ArrayList getOrdersForAutoBilling() {
        ArrayList clisedTrips = null;
        System.out.println("haiiiiii");
        clisedTrips = billingDAO.getOrdersForAutoBilling();
        return clisedTrips;
    }

//     <!--NEW E Invoice Starts-->
    public ArrayList viewInvoiceList(String customerId, String fromDate, String toDate, String tripType, String submitStatus, String billNo, String grNo, String containerNo) throws FPRuntimeException, FPBusinessException {
        ArrayList closedBill = null;
        closedBill = billingDAO.viewInvoiceList(customerId, fromDate, toDate, tripType, submitStatus, billNo, grNo, containerNo);
        return closedBill;
    }

    public ArrayList viewSupplyInvoiceList(BillingTO billingTO) throws FPRuntimeException, FPBusinessException {
        ArrayList viewSupplyInvoiceList = null;
        viewSupplyInvoiceList = billingDAO.viewSupplyInvoiceList(billingTO);
        return viewSupplyInvoiceList;
    }

//    public int eInvoiceGenerate(BillingTO billingTO) throws FPBusinessException, FPRuntimeException {
//        int status = 0;
//        SqlMapClient session = billingDAO.getSqlMapClient();
//        session.startTransaction();
//        try{
//        status = billingDAO.eInvoiceGenerate(billingTO);
//        session.commitTransaction();
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            //System.out.println("am here 7");
//            try {
//                session.endTransaction();
//            } catch (Exception ex) {
//                ex.printStackTrace();
//                //System.out.println("am here 8" + ex.getMessage());
//            } finally {
//                session.getSession().close();
//            }
//        }
//        return status;
//    }
    public int eInvoiceGenerate(BillingTO billingTO, int userId) throws FPBusinessException, FPRuntimeException {

        SqlMapClient session = billingDAO.getSqlMapClient();
        boolean exceptionStatus = false;
        int status = 0;
        String receiptCode = "2122TEM";
        String receiptNo = "";
        Exception excp = null;
        try {
            session.startTransaction();
            status = billingDAO.eInvoiceGenerate(billingTO, userId, session);
            System.out.println("status---" + status);
            session.commitTransaction();
        } catch (Exception e) {
            exceptionStatus = true;
            excp = e;
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
//            if (exceptionStatus) {
//                throw excp;
//            }
        }
        return status;

    }

    public ArrayList viewEInvoiceList(String customerId, String fromDate, String toDate, String tripType, String submitStatus, String billNo, String grNo, String containerNo) throws FPRuntimeException, FPBusinessException {
        ArrayList closedBill = null;
        closedBill = billingDAO.viewEInvoiceList(customerId, fromDate, toDate, tripType, submitStatus, billNo, grNo, containerNo);
        return closedBill;
    }

    public ArrayList viewClosedEInvoiceList(String custId, String invoiceId) throws FPRuntimeException, FPBusinessException {
        ArrayList closedBill = null;
        closedBill = billingDAO.viewClosedEInvoiceList(custId, invoiceId);
        return closedBill;
    }

    public ArrayList insertEInvoiceUploadTemp(ArrayList shippingLineNoList, String invoiceId) throws FPRuntimeException, FPBusinessException {
        ArrayList getSbillNoListTemp = new ArrayList();
        getSbillNoListTemp = billingDAO.insertEInvoiceUploadTemp(shippingLineNoList, invoiceId);
        return getSbillNoListTemp;
    }

    public ArrayList getEInvoiveUploadData(String invoiceId) throws FPRuntimeException, FPBusinessException {
        ArrayList getSbillNoListTemp = new ArrayList();
        getSbillNoListTemp = billingDAO.getEInvoiveUploadData(invoiceId);
        return getSbillNoListTemp;
    }

    public int insertEInvoiceUpload(BillingTO billTO) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        SqlMapClient session = billingDAO.getSqlMapClient();
        insertStatus = billingDAO.insertEInvoiceUpload(billTO, session);
        return insertStatus;
    }

//    supp invoice
    public ArrayList viewESuppInvoiceList(String customerId, String fromDate, String toDate, String tripType, String submitStatus, String billNo, String grNo, String containerNo) throws FPRuntimeException, FPBusinessException {
        ArrayList closedBill = null;
        closedBill = billingDAO.viewESuppInvoiceList(customerId, fromDate, toDate, tripType, submitStatus, billNo, grNo, containerNo);
        return closedBill;
    }

    public ArrayList viewClosedESuppInvoiceList(String custId, String invoiceId) throws FPRuntimeException, FPBusinessException {
        ArrayList closedBill = null;
        closedBill = billingDAO.viewClosedESuppInvoiceList(custId, invoiceId);
        return closedBill;
    }

    public int eSuppInvoiceGenerate(BillingTO billingTO, int userId) throws FPBusinessException, FPRuntimeException {

        SqlMapClient session = billingDAO.getSqlMapClient();
        boolean exceptionStatus = false;
        int status = 0;
        String receiptCode = "2122TEM";
        String receiptNo = "";
        Exception excp = null;
        try {
            session.startTransaction();
            status = billingDAO.eSuppInvoiceGenerate(billingTO, userId, session);
            System.out.println("status---" + status);
            session.commitTransaction();
        } catch (Exception e) {
            exceptionStatus = true;
            excp = e;
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
//            if (exceptionStatus) {
//                throw excp;
//            }
        }
        return status;

    }

    public ArrayList insertESuppInvoiceUploadTemp(ArrayList shippingLineNoList, String invoiceId) throws FPRuntimeException, FPBusinessException {
        ArrayList getSbillNoListTemp = new ArrayList();
        getSbillNoListTemp = billingDAO.insertEInvoiceUploadTemp(shippingLineNoList, invoiceId);
        return getSbillNoListTemp;
    }

    public ArrayList getESuppInvoiveUploadData(String invoiceId) throws FPRuntimeException, FPBusinessException {
        ArrayList getSbillNoListTemp = new ArrayList();
        getSbillNoListTemp = billingDAO.getEInvoiveUploadData(invoiceId);
        return getSbillNoListTemp;
    }

    public int insertESuppInvoiceUpload(BillingTO billTO) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        SqlMapClient session = billingDAO.getSqlMapClient();
        insertStatus = billingDAO.insertEInvoiceUpload(billTO, session);
        return insertStatus;
    }

    //     E Credit Invoice        
    public ArrayList getEcreditNoteList(BillingTO billingTO) throws FPBusinessException, FPRuntimeException {
        ArrayList creditNoteSearchList = new ArrayList();
        creditNoteSearchList = billingDAO.getEcreditNoteList(billingTO);
        return creditNoteSearchList;
    }

    public ArrayList getEsupplyCreditNoteList(BillingTO billingTO) throws FPBusinessException, FPRuntimeException {
        ArrayList creditNoteSearchList = new ArrayList();
        creditNoteSearchList = billingDAO.getEsupplyCreditNoteList(billingTO);
        return creditNoteSearchList;
    }

//    e credit invoice
    public int eCreditInvoiceGenerate(BillingTO billingTO, int userId) throws FPBusinessException, FPRuntimeException {

        SqlMapClient session = billingDAO.getSqlMapClient();
        boolean exceptionStatus = false;
        int status = 0;
        String receiptCode = "2122TEM";
        String receiptNo = "";
        Exception excp = null;
        try {
            session.startTransaction();
            status = billingDAO.eCreditInvoiceGenerate(billingTO, userId, session);
            System.out.println("status---" + status);
            session.commitTransaction();
        } catch (Exception e) {
            exceptionStatus = true;
            excp = e;
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
//            if (exceptionStatus) {
//                throw excp;
//            }
        }
        return status;

    }

    public int eCreditSuppInvoiceGenerate(BillingTO billingTO, int userId) throws FPBusinessException, FPRuntimeException {

        SqlMapClient session = billingDAO.getSqlMapClient();
        boolean exceptionStatus = false;
        int status = 0;
        String receiptCode = "2122TEM";
        String receiptNo = "";
        Exception excp = null;
        try {
            session.startTransaction();
            status = billingDAO.eCreditSuppInvoiceGenerate(billingTO, userId, session);
            System.out.println("status---" + status);
            session.commitTransaction();
        } catch (Exception e) {
            exceptionStatus = true;
            excp = e;
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
//            if (exceptionStatus) {
//                throw excp;
//            }
        }
        return status;

    }


    public ArrayList viewECreditInvoiceList(String customerId, String fromDate, String toDate, String tripType, String submitStatus, String billNo, String grNo, String containerNo) throws FPRuntimeException, FPBusinessException {
        ArrayList closedBill = null;
        closedBill = billingDAO.viewECreditInvoiceList(customerId, fromDate, toDate, tripType, submitStatus, billNo, grNo, containerNo);
        return closedBill;
    }

    public ArrayList viewECreditSuppInvoiceList(String customerId, String fromDate, String toDate, String tripType, String submitStatus, String billNo, String grNo, String containerNo) throws FPRuntimeException, FPBusinessException {
        ArrayList closedBill = null;
        closedBill = billingDAO.viewECreditSuppInvoiceList(customerId, fromDate, toDate, tripType, submitStatus, billNo, grNo, containerNo);
        return closedBill;
    }

    public ArrayList viewClosedECreditInvoiceList(String custId, String invoiceId) throws FPRuntimeException, FPBusinessException {
        ArrayList closedBill = null;
        closedBill = billingDAO.viewClosedECreditInvoiceList(custId, invoiceId);
        return closedBill;
    }

    public ArrayList viewClosedECreditSuppInvoiceList(String custId, String invoiceId) throws FPRuntimeException, FPBusinessException {
        ArrayList closedBill = null;
        closedBill = billingDAO.viewClosedECreditSuppInvoiceList(custId, invoiceId);
        return closedBill;
    }

    public int approveSuppInvoice(BillingTO billingTO) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = billingDAO.approveSuppInvoice(billingTO);
        return status;
    }
    
    
    public int saveOrderBillForGta(BillingTO billingTO, int userId, String finYear) throws FPRuntimeException, FPBusinessException, IOException, Exception {
        int status = 0;
        int invoiceId = 0;
        SqlMapClient session = billingDAO.getSqlMapClient();
        boolean exceptionStatus = false;
        Exception excp = null;
        try {
            session.startTransaction();

                  
                  
                  
            ArrayList ordersToBeBilledDetails = billingDAO.getOrdersToBeBillingDetails1(billingTO, session);
            ArrayList ordersOtherExpenseDetails = new ArrayList();

            String grDate = "";
            Iterator itr7 = ordersToBeBilledDetails.iterator();
            BillingTO billingTONew12 = null;
            while (itr7.hasNext()) {
                billingTONew12 = new BillingTO();
                billingTONew12 = (BillingTO) itr7.next();
                //grDate = billingTONew12.getToDate();
                grDate = billingTONew12.getGrDate();
            }
            System.out.println(" selected  finYear : " + finYear);
            String invoiceCode = "";
            String fromDate = "";
            String invoiceCodeSequence = "";
            int gstCheck = 1;
            System.out.println("billingTO.getGstType() invoice BP----"+billingTO.getGstType());
                 if (finYear.equalsIgnoreCase("2024")) { // After marc 31st 2022

                    Calendar now = Calendar.getInstance();
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = new Date();
                    System.out.println("1111111111111 ");
                    invoiceCode = "2324TBS";
                      System.out.println("invoiceCodeMINE---"+invoiceCode);
                    fromDate = dateFormat.format(date);
                    invoiceCodeSequence = billingDAO.getInvoiceCodeSequenceBos2324(session, gstCheck);
                    
                    System.out.println(" invoiceCodeSequence " + invoiceCodeSequence);
                    System.out.println(" invoiceCode" + invoiceCode);
                    System.out.println(" fromDate" + fromDate);

                } else { //Befoe
                    System.out.println("1111111111 before ");
                    fromDate = "2023-03-31 22:00:00";
                    invoiceCode = "2223TBS";
                    System.out.println("invoiceCodeMINE---"+invoiceCode);
                    invoiceCodeSequence = billingDAO.getInvoiceCodeSequenceBos2223(session, gstCheck);
                    System.out.println(" invoiceCodeSequence " + invoiceCodeSequence);
                    System.out.println(" invoiceCode" + invoiceCode);
                    System.out.println(" fromDate" + fromDate);
                }

            billingTO.setFromDate(fromDate);
            invoiceCode = invoiceCode + invoiceCodeSequence;
            billingTO.setInvoiceCode(invoiceCode);
            billingTO.setInvoiceNo(billingTO.getInvoiceNo());
            String totalRevenue = billingDAO.getBillingTotalRevenue(billingTO, session);
            String totalExpense = billingDAO.getBillingTotalExpense(billingTO, session);
            System.out.println("billingTO.getTotalTax()" + billingTO.getTotalTax());
            String grandTotal = Float.parseFloat(totalRevenue) + Float.parseFloat(totalExpense)
                    + Float.parseFloat(billingTO.getTotalTax()) + "";
            billingTO.setTotalRevenue(totalRevenue);
            billingTO.setOtherExpense(totalExpense);
            billingTO.setGrandTotal(grandTotal);

            //savebillheader
            invoiceId = billingDAO.saveBillHeader(billingTO, session);
            billingTO.setInvoiceId("" + invoiceId);
            int invoiceDetailId = 0;
            if (invoiceId > 0) {
                int tax = billingDAO.saveBillTax(billingTO, session);
                System.out.println("tax----"+tax);
                Iterator itr = ordersToBeBilledDetails.iterator();
                BillingTO billingTONew = null;
                while (itr.hasNext()) {
                    billingTONew = new BillingTO();
                    billingTONew = (BillingTO) itr.next();
                    billingTONew.setInvoiceId("" + invoiceId);
                    billingTONew.setInvoiceCode(invoiceCode);
                    billingTONew.setTotalRevenue(billingTONew.getEstimatedRevenue());
                    // billingTONew.setOtherExpense(otherExpense);
                    billingTONew.setGrandTotal(grandTotal);
                    billingTONew.setIgstAmount(billingTO.getIgstAmount());
                    invoiceDetailId = billingDAO.saveBillDetails(billingTONew,billingTO, session);

                    ordersOtherExpenseDetails = billingDAO.getOrderOtherExpenseDetails1(billingTONew, session);
                    Iterator itr1 = ordersOtherExpenseDetails.iterator();
                    BillingTO billingTONew1 = null;
                    while (itr1.hasNext()) {
                        billingTONew1 = new BillingTO();
                        billingTONew1 = (BillingTO) itr1.next();
                        billingTONew1.setTripId(billingTONew.getTripId());
                        billingTONew1.setInvoiceId("" + invoiceId);
                        billingTONew1.setInvoiceDetailId("" + invoiceDetailId);
                        billingTONew1.setInvoiceCode(invoiceCode);
                        status = billingDAO.saveBillDetailExpense(billingTONew1, session);
                    }

                    billingTONew.setTripSheetId(billingTONew.getTripId());
                    billingTONew.setStatusId("16");
                    status = billingDAO.updateStatus(billingTONew, userId, session);

                    System.out.println("status---" + status);
                }
            }
            
//            Iterator itr = ordersToBeBilledDetails.iterator();
//                BillingTO billingTONew = null;
//                while (itr.hasNext()) {
//                    billingTONew = new BillingTO();
//                    billingTONew = (BillingTO) itr.next();

            status = operationDAO.updateInvoiceOustanding(billingTO, userId, session);
            System.out.println("status---$$" + status);

//    }
            session.commitTransaction();
        } catch (Exception e) {
            exceptionStatus = true;
            excp = e;
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
            if (exceptionStatus) {
                throw excp;
            }
        }
        return invoiceId;
    }
    
    public int saveOrderSupplementBillForGta(BillingTO billingTO, String[] supplementsCharges, String[] detaintionCharge, String[] tollCharge, String[] greenTax, String[] otherExpense, String[] weightmentCharge, String[] tripIds, int userId, String finYear) throws FPRuntimeException, FPBusinessException, IOException, Exception {
        int status = 0;
        int invoiceId = 0;
        SqlMapClient session = billingDAO.getSqlMapClient();
        boolean exceptionStatus = false;
        Exception excp = null;
        try {
            session.startTransaction();

            ArrayList ordersToBeBilledDetails = billingDAO.getOrdersToBeSuppBillingDetails1(billingTO, session);
            ArrayList ordersOtherExpenseDetails = new ArrayList();

            String grDate = "";
            Iterator itr7 = ordersToBeBilledDetails.iterator();
            BillingTO billingTONew12 = null;
            while (itr7.hasNext()) {
                billingTONew12 = new BillingTO();
                billingTONew12 = (BillingTO) itr7.next();
                //grDate = billingTONew12.getToDate();
                grDate = billingTONew12.getGrDate();
                System.out.println("$$$$$$$" + grDate);
            }
            System.out.println(" grdate" + grDate);
            String compareDate = "31-03-2018";
            String june = "30-06-2017";
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date d1 = sdf.parse(compareDate);
            Date d2 = sdf.parse(grDate);
            System.out.println("compareDate " + compareDate);
            System.out.println("grDate " + grDate);
            System.out.println("d2... " + d2);
            String finacialYear = "";
            String invoiceCode = "";
            String currentYear = "";
            String fromDate = "";
            String invoiceCodeSequence = "";
            int gstCheck = 1;
            System.out.println("finYear0-----" + finYear);
            //if (d2.after(d1)) { // After marc 31st 2020
            
//            System.out.println("billingTO.getGstType()-----"+billingTO.getGstType());
            
                 if (finYear.equalsIgnoreCase("2024")) {

                    Calendar now = Calendar.getInstance();
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = new Date();
                    System.out.println("1111111111111 ");
                    invoiceCode = "2324TBSSUPP";
                    fromDate = dateFormat.format(date);
                    invoiceCodeSequence = billingDAO.getSuppInvoiceCodeSequenceBos2324(session, gstCheck);
                    System.out.println(" invoiceCodeSequenceinvoiceCodeSequenceinvoiceCodeSequence" + invoiceCodeSequence);
                    System.out.println(" invoiceCode" + invoiceCode);
                    System.out.println(" fromDate" + fromDate);

                } else { //Befoe
                    System.out.println("22222222 ");
                    fromDate = "2023-03-31 22:00:00";
                    invoiceCode = "2223TBSSUPP";
                    invoiceCodeSequence = billingDAO.getSuppInvoiceCodeSequenceBos2223(session, gstCheck);
                    System.out.println(" invoiceCodeSequenceinvoiceCodeSequenceinvoiceCodeSequence" + invoiceCodeSequence);
                    System.out.println(" invoiceCode" + invoiceCode);
                    System.out.println(" fromDate" + fromDate);
                }
            

            billingTO.setFromDate(fromDate);
            invoiceCode = invoiceCode + invoiceCodeSequence;
            billingTO.setSuppInvoiceNo(invoiceCode);
            billingTO.setInvoiceNo(billingTO.getInvoiceNo());
//            String totalRevenue = billingDAO.getBillingTotalRevenue(billingTO, session);
            String totalExpense = billingDAO.getSuppBillingTotalExpense(billingTO, session);
            System.out.println("billingTO.getTotalTax()" + billingTO.getTotalTax());
            //  String grandTotal = Float.parseFloat(totalRevenue) + Float.parseFloat(totalExpense)
            //         + Float.parseFloat(billingTO.getTotalTax()) + "";
            //  billingTO.setTotalRevenue(totalRevenue);
           
            billingTO.setOtherExpense(totalExpense);
            // billingTO.setGrandTotal(grandTotal);

            //savebillheader
            invoiceId = billingDAO.saveSupplementBillHeader(billingTO, session);
            billingTO.setInvoiceId("" + invoiceId);
            int invoiceDetailId = 0;
            if (invoiceId > 0) {
                int tax = billingDAO.saveSupplementBillTax(billingTO, session);
                Iterator itr = ordersToBeBilledDetails.iterator();
                BillingTO billingTONew = null;

                int i = 0;
                while (itr.hasNext()) {
                    billingTONew = new BillingTO();
                    billingTONew = (BillingTO) itr.next();
                    billingTONew.setInvoiceId("" + invoiceId);
                    billingTONew.setInvoiceCode(invoiceCode);

                    billingTONew.setTotalRevenue(supplementsCharges[i]);
                    billingTONew.setGrandTotal(supplementsCharges[i]);
                    billingTONew.setIgstAmount(billingTO.getIgstAmount());
                    invoiceDetailId = billingDAO.saveSupplementBillDetails(billingTONew,billingTO, session);

                    ordersOtherExpenseDetails = billingDAO.getOrderOtherExpenseSupplementaryDetails(billingTONew, session);
                    Iterator itr1 = ordersOtherExpenseDetails.iterator();
                    BillingTO billingTONew1 = null;
                    while (itr1.hasNext()) {
                        billingTONew1 = new BillingTO();
                        billingTONew1 = (BillingTO) itr1.next();
                        billingTONew1.setTripId(billingTONew.getTripId());
                        billingTONew1.setInvoiceId("" + invoiceId);
                        billingTONew1.setInvoiceDetailId("" + invoiceDetailId);
                        billingTONew1.setInvoiceCode(invoiceCode);
                        status = billingDAO.saveSuppBillDetailExpense(billingTONew1, session);
                    }

                    billingTONew.setTripSheetId(billingTONew.getTripId());
                    billingTONew.setStatusId("32");
                    status = billingDAO.updateStatus(billingTONew, userId, session);

                    i++;
                    //update trip status to billed**********************************************************************
                }

                //update trip status to billed**********************************************************************
            }
            billingTO.setSuppInvoiceId("" + invoiceId);
            status = operationDAO.updateSuppInvoiceOustanding(billingTO, userId, session);
            System.out.println("status--supplementry-$$" + status);

            session.commitTransaction();
        } catch (Exception e) {
            exceptionStatus = true;
            excp = e;
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
            if (exceptionStatus) {
                throw excp;
            }
        }
        return invoiceId;
    }

    public int resendInvoice(BillingTO billingTO) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = billingDAO.resendInvoice(billingTO);
        return status;
    }
    
     public int eInvoiceGenerateAuto(BillingTO billingTO, int userId) throws FPBusinessException, FPRuntimeException {

        SqlMapClient session = billingDAO.getSqlMapClient();
        boolean exceptionStatus = false;
        int status = 0;
        String receiptCode = "2122TEM";
        String receiptNo = "";
        Exception excp = null;
        try {
            session.startTransaction();
            status = billingDAO.eInvoiceGenerateAuto(billingTO, userId, session);
            System.out.println("status---" + status);
            session.commitTransaction();
        } catch (Exception e) {
            exceptionStatus = true;
            excp = e;
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
//            if (exceptionStatus) {
//                throw excp;
//            }
        }
        return status;

    }
     
         public int eSuppInvoiceGenerateAuto(BillingTO billingTO, int userId) throws FPBusinessException, FPRuntimeException {

        SqlMapClient session = billingDAO.getSqlMapClient();
        boolean exceptionStatus = false;
        int status = 0;
        String receiptCode = "2122TEM";
        String receiptNo = "";
        Exception excp = null;
        try {
            session.startTransaction();
            status = billingDAO.eSuppInvoiceGenerateAuto(billingTO, userId, session);
            System.out.println("status---" + status);
            session.commitTransaction();
        } catch (Exception e) {
            exceptionStatus = true;
            excp = e;
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
//            if (exceptionStatus) {
//                throw excp;
//            }
        }
        return status;

    }
     public int eCreditInvoiceGenerateAuto(BillingTO billingTO, int userId) throws FPBusinessException, FPRuntimeException {

        SqlMapClient session = billingDAO.getSqlMapClient();
        boolean exceptionStatus = false;
        int status = 0;
        String receiptCode = "2122TEM";
        String receiptNo = "";
        Exception excp = null;
        try {
            session.startTransaction();
            status = billingDAO.eCreditInvoiceGenerateAuto(billingTO, userId, session);
            System.out.println("status---" + status);
            session.commitTransaction();
        } catch (Exception e) {
            exceptionStatus = true;
            excp = e;
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
//            if (exceptionStatus) {
//                throw excp;
//            }
        }
        return status;

    }

    public int eCreditSuppInvoiceGenerateAuto(BillingTO billingTO, int userId) throws FPBusinessException, FPRuntimeException {

        SqlMapClient session = billingDAO.getSqlMapClient();
        boolean exceptionStatus = false;
        int status = 0;
        String receiptCode = "2122TEM";
        String receiptNo = "";
        Exception excp = null;
        try {
            session.startTransaction();
            status = billingDAO.eCreditSuppInvoiceGenerateAuto(billingTO, userId, session);
            System.out.println("status---" + status);
            session.commitTransaction();
        } catch (Exception e) {
            exceptionStatus = true;
            excp = e;
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
//            if (exceptionStatus) {
//                throw excp;
//            }
        }
        return status;

    }
     public ArrayList viewcombinedBillList(String customerId, String fromDate, String toDate, String tripType, String submitStatus, String billNo, String grNo, String containerNo) throws FPRuntimeException, FPBusinessException {
        ArrayList viewcombinedBillList = null;
        viewcombinedBillList = billingDAO.viewcombinedBillList(customerId, fromDate, toDate, tripType, submitStatus, billNo, grNo, containerNo);
        return viewcombinedBillList;
    }
     
      public ArrayList viewWorkOrderSubmission(String invoiceId) throws FPRuntimeException, FPBusinessException {
        ArrayList viewWorkOrderSubmission = null;
        viewWorkOrderSubmission = billingDAO.viewWorkOrderSubmission(invoiceId);
        return viewWorkOrderSubmission;
    }
      
      
       public int insertWorkOrderDetails(String invoiceId,String workOrderNumber) throws FPBusinessException, FPRuntimeException {
        int insertWorkOrderDetails = 0;
        insertWorkOrderDetails = billingDAO.insertWorkOrderDetails(invoiceId,workOrderNumber);
        return insertWorkOrderDetails;
    }

        public ArrayList viewWorkOrderTwoSubmission(String invoiceId) throws FPRuntimeException, FPBusinessException {
        ArrayList viewWorkOrderTwoSubmission = null;
        viewWorkOrderTwoSubmission = billingDAO.viewWorkOrderTwoSubmission(invoiceId);
        return viewWorkOrderTwoSubmission;
    }
        
        
         public int insertWorkOrderTwoDetails(String invoiceId,String workOrderNumber) throws FPBusinessException, FPRuntimeException {
        int insertWorkOrderTwoDetails = 0;
        insertWorkOrderTwoDetails = billingDAO.insertWorkOrderTwoDetails(invoiceId,workOrderNumber);
        return insertWorkOrderTwoDetails;
    }

    public String getPriviewPrintBilling(String billingPartyId) throws FPBusinessException, FPRuntimeException {
        String status = "";
        status = billingDAO.getPriviewPrintBilling(billingPartyId);
        return status;
    }

}
