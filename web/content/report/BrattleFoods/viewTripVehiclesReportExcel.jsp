<%-- 
    Document   : GRSummaryReportExcel
    Created on : Feb 15, 2016, 2:09:44 PM
    Author     : hp
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<html>
    <head>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
            <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "VehicleTripReport-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
           
                <table border="1"  align="center" width="100%" cellpadding="0" cellspacing="1" >
                    <thead>
                           <tr id="tableDesingTH" height="30">
                                    <th rowspan="2" >S.No</th>
                                     <th rowspan="2" >Vehicle Reg. No.</th>
                                     <th rowspan="2" >No. Of. Trips</th>
                                     <th rowspan="2" >OwnerShip</th>
                                   
                                    
                                </tr>       
                    </thead>
                    <% int index = 0,sno = 1;%>
                   
                            <c:forEach items="${getVehicleTripDetails}" var="csList">
                                <tr>

                                   <td align="center"><%=sno%></td>
                            <td ><c:out value="${csList.vehicleNo}"/></td>
                            <td ><c:out value="${csList.noOfTrips}"/></td>
                                 <c:if test = "${csList.ownType == 1}" >
                                  <td>Own</td>
                              </c:if>
                           
                              <c:if test = "${csList.ownType == 2}" >
                                  <td>Hire</td>
                              </c:if>
                                </tr>
                        <%
                   index++;
                     sno++;
                        %>

                    </c:forEach>

                </table>
          
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
