/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.finance.data;

import com.ibatis.sqlmap.client.SqlMapClient;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.finance.business.FinanceTO;
import ets.domain.util.FPLogUtils;
import ets.domain.util.ThrottleConstants;
import java.io.File;
import java.text.SimpleDateFormat;
import java.io.FileInputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import java.util.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author ASHOK
 */
public class FinanceDAO extends SqlMapClientDaoSupport {

    private final int errorStatus = 4;
    private final static String CLASS = "FinanceDAO";
    /**
     * userId -The Identifier of the User
     */
    int userId = 0;

    /**
     * constructor
     */
    public FinanceDAO() {
        /*
         *
         */
    }

    /**
     * This method used to Get Finance Bank .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    ///////////////arun edited on  12/01/16///////////////////////////
    public int updateStartTripSheetfinance(String ownerShip, String EstimatedExpense, String TripSheetId, String DieselCost, String TollCost, String TripId, String DriverBatta, String DriverIncentive, String MiscValue, int userId) {

        int insertStatus = 0;

        try {

            //Account Entry Start
            //account entry details start    
            String code2 = "";
            String[] temp = null;

            //fuel cost
            Map map = new HashMap();

            map.put("userId", userId);
            map.put("DetailCode", "1");
            map.put("voucherType", "%PAYMENT%");
            code2 = (String) getSqlMapClientTemplate().queryForObject("finance.getTripVoucherCode", map);
            temp = code2.split("-");
            int codeval2 = Integer.parseInt(temp[1]);
            int codev2 = codeval2 + 1;
            String voucherCode = "PAYMENT-" + codev2;
            System.out.println("voucherCode = " + voucherCode);
            System.out.println("OwnerShip:" + ownerShip);

            if ("3".equals(ownerShip) || "2".equals(ownerShip)) {
                map.put("voucherCode", voucherCode);
                map.put("mainEntryType", "VOUCHER");
                map.put("entryType", "PAYMENT");
                map.put("ledgerId", 1582);
                map.put("particularsId", "LEDGER-1562");

                map.put("amount", EstimatedExpense);
                map.put("Accounts_Type", "DEBIT");
                map.put("Remark", "Hire Charge");
                map.put("Reference", "Trip");

                System.out.println("tripId = " + TripSheetId);
                map.put("SearchCode", TripSheetId);
                System.out.println("map1 =---------------------> " + map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("finance.insertTripAccountEntry", map);
                System.out.println("status1 = " + insertStatus);
                //--------------------------------- acc 2nd row start --------------------------
                if (insertStatus > 0) {
                    map.put("DetailCode", "2");
                    map.put("ledgerId", "52");
                    map.put("particularsId", "LEDGER-40");
                    map.put("Accounts_Type", "CREDIT");
                    System.out.println("map2 =---------------------> " + map);
                    insertStatus = (Integer) getSqlMapClientTemplate().update("finance.insertTripAccountEntry", map);
                    System.out.println("status2 = " + insertStatus);
                }

            } else {
                map.put("voucherCode", voucherCode);
                map.put("mainEntryType", "VOUCHER");
                map.put("entryType", "PAYMENT");
                map.put("ledgerId", 37);
                map.put("particularsId", "LEDGER-29");

                map.put("amount", DieselCost);
                map.put("Accounts_Type", "DEBIT");
                map.put("Remark", "Fuel Charges");
                map.put("Reference", "Trip");

                System.out.println("tripId = " + TripSheetId);
                map.put("SearchCode", TripSheetId);
                System.out.println("map1 =---------------------> " + map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("finance.insertTripAccountEntry", map);
                System.out.println("status1 = " + insertStatus);
                //--------------------------------- acc 2nd row start --------------------------
                if (insertStatus > 0) {
                    map.put("DetailCode", "2");
                    map.put("ledgerId", "52");
                    map.put("particularsId", "LEDGER-40");
                    map.put("Accounts_Type", "CREDIT");
                    System.out.println("map2 =---------------------> " + map);
                    insertStatus = (Integer) getSqlMapClientTemplate().update("finance.insertTripAccountEntry", map);
                    System.out.println("status2 = " + insertStatus);
                }

                //toll cost
                codev2++;
                voucherCode = "PAYMENT-" + codev2;
                System.out.println("voucherCode = " + voucherCode);
                map.put("voucherCode", voucherCode);
                map.put("mainEntryType", "VOUCHER");
                map.put("entryType", "PAYMENT");
                map.put("ledgerId", 41);
                map.put("particularsId", "LEDGER-33");

                map.put("amount", TollCost);
                map.put("Accounts_Type", "DEBIT");
                map.put("Remark", "Toll Charges");
                map.put("Reference", "Trip");

                System.out.println("tripId = " + TripId);
                map.put("SearchCode", TripId);
                System.out.println("map1 =---------------------> " + map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("finance.insertTripAccountEntry", map);
                System.out.println("status1 = " + insertStatus);
                //--------------------------------- acc 2nd row start --------------------------
                if (insertStatus > 0) {
                    map.put("DetailCode", "2");
                    map.put("ledgerId", "52");
                    map.put("particularsId", "LEDGER-40");
                    map.put("Accounts_Type", "CREDIT");
                    System.out.println("map2 =---------------------> " + map);
                    insertStatus = (Integer) getSqlMapClientTemplate().update("finance.insertTripAccountEntry", map);
                    System.out.println("status2 = " + insertStatus);
                }

                //driver Bata cost
                codev2++;
                voucherCode = "PAYMENT-" + codev2;
                System.out.println("voucherCode = " + voucherCode);
                map.put("voucherCode", voucherCode);
                map.put("mainEntryType", "VOUCHER");
                map.put("entryType", "PAYMENT");
                map.put("ledgerId", 34);
                map.put("particularsId", "LEDGER-27");

                map.put("amount", DriverBatta);
                map.put("Accounts_Type", "DEBIT");
                map.put("Remark", "Driver Bhatta Charges");
                map.put("Reference", "Trip");

                System.out.println("tripId = " + TripId);
                map.put("SearchCode", TripId);
                System.out.println("map1 =---------------------> " + map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("finance.insertTripAccountEntry", map);
                System.out.println("status1 = " + insertStatus);
                //--------------------------------- acc 2nd row start --------------------------
                if (insertStatus > 0) {
                    map.put("DetailCode", "2");
                    map.put("ledgerId", "52");
                    map.put("particularsId", "LEDGER-40");
                    map.put("Accounts_Type", "CREDIT");
                    System.out.println("map2 =---------------------> " + map);
                    insertStatus = (Integer) getSqlMapClientTemplate().update("finance.insertTripAccountEntry", map);
                    System.out.println("status2 = " + insertStatus);
                }

                //driver Incentive cost
                codev2++;
                voucherCode = "PAYMENT-" + codev2;
                System.out.println("voucherCode = " + voucherCode);
                map.put("voucherCode", voucherCode);
                map.put("mainEntryType", "VOUCHER");
                map.put("entryType", "PAYMENT");
                map.put("ledgerId", 34);
                map.put("particularsId", "LEDGER-27");

                map.put("amount", DriverIncentive);
                map.put("Accounts_Type", "DEBIT");
                map.put("Remark", "Driver Incentive Charges");
                map.put("Reference", "Trip");

                System.out.println("tripId = " + TripId);
                map.put("SearchCode", TripId);
                System.out.println("map1 =---------------------> " + map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("finance.insertTripAccountEntry", map);
                System.out.println("status1 = " + insertStatus);
                //--------------------------------- acc 2nd row start --------------------------
                if (insertStatus > 0) {
                    map.put("DetailCode", "2");
                    map.put("ledgerId", "52");
                    map.put("particularsId", "LEDGER-40");
                    map.put("Accounts_Type", "CREDIT");
                    System.out.println("map2 =---------------------> " + map);
                    insertStatus = (Integer) getSqlMapClientTemplate().update("finance.insertTripAccountEntry", map);
                    System.out.println("status2 = " + insertStatus);
                }
                //Misc cost

                codev2++;
                voucherCode = "PAYMENT-" + codev2;
                System.out.println("voucherCode = " + voucherCode);
                map.put("voucherCode", voucherCode);
                map.put("mainEntryType", "VOUCHER");
                map.put("entryType", "PAYMENT");
                map.put("ledgerId", 40);
                map.put("particularsId", "LEDGER-32");

                map.put("amount", MiscValue);
                map.put("Accounts_Type", "DEBIT");
                map.put("Remark", "Misc Charges");
                map.put("Reference", "Trip");

                System.out.println("tripId = " + TripId);
                map.put("SearchCode", TripId);
                System.out.println("map1 =---------------------> " + map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("finance.insertTripAccountEntry", map);
                System.out.println("status1 = " + insertStatus);
                //--------------------------------- acc 2nd row start --------------------------
                if (insertStatus > 0) {
                    map.put("DetailCode", "2");
                    map.put("ledgerId", "52");
                    map.put("particularsId", "LEDGER-40");
                    map.put("Accounts_Type", "CREDIT");
                    System.out.println("map2 =---------------------> " + map);
                    insertStatus = (Integer) getSqlMapClientTemplate().update("finance.insertTripAccountEntry", map);
                    System.out.println("status2 = " + insertStatus);
                }

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetfinance Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetfinance List", sqlException);
        }

        return insertStatus;
    }

    public ArrayList financeBank(String bank) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
//        //////System.out.println("letters " + letter);
//        map.put("letters", letter);
        ArrayList financeBank = new ArrayList();
        try {
//            //////System.out.println("letters " + loginTO.getLetters());
            financeBank = (ArrayList) getSqlMapClientTemplate().queryForList("finance.financeBank", map);
//            //////System.out.println("status is in insertdao " + userList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("financeBank Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "financeBank", sqlException);
        }
        return financeBank;
    }

    public ArrayList getBankList() {
        Map map = new HashMap();
        ArrayList bankList = new ArrayList();

        try {
            bankList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getBankList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBankList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getBankList", sqlException);
        }
        return bankList;
    }

    public ArrayList getBankBranchList(FinanceTO financeTO) {
        Map map = new HashMap();
        ArrayList bankList = new ArrayList();
        map.put("bankid", financeTO.getBankId());
        try {
            bankList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getBankBranchList", map);
            System.out.println("bankList" + bankList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBankBranchList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getBankBranchList", sqlException);
        }
        return bankList;
    }

    public ArrayList getBankBranchLists() {
        Map map = new HashMap();
        ArrayList bankList = new ArrayList();
        try {
            bankList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getBankBranchLists", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBankBranchList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getBankBranchList", sqlException);
        }
        return bankList;
    }

    public ArrayList getPrimaryBankList() {
        Map map = new HashMap();
        ArrayList primarybankList = new ArrayList();

        try {
            primarybankList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getPrimaryBankList", map);
            System.out.println("primarybankList" + primarybankList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPrimaryBankList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getPrimaryBankList", sqlException);
        }
        return primarybankList;
    }

    public int saveNewBank(FinanceTO financeTO, int userId) {
        Map map = new HashMap();
        String code = "";
        String delimiter = "-";
        String[] temp;
        int status = 0;
//        CryptoLibrary cl = new CryptoLibrary();
        map.put("userId", userId);
        map.put("bankid", financeTO.getBankid());
        map.put("branchName", financeTO.getBranchName());
        map.put("bankcode", financeTO.getBankcode());
        map.put("address", financeTO.getAddress());
        map.put("phoneNo", financeTO.getPhoneNo());
        map.put("accntCode", financeTO.getAccntCode());
        map.put("IFSCCode", financeTO.getIFSCCode());
        map.put("BSRCode", financeTO.getBSRCode());

        code = (String) getSqlMapClientTemplate().queryForObject("finance.getLedgerCode", map);
        temp = code.split(delimiter);
        //////System.out.println("temp[1] = " + temp[1]);
        int codeval = Integer.parseInt(temp[1]);
        int codev = codeval + 1;
        //////System.out.println("codev = " + codev);
        String ledgercode = "LEDGER-" + codev;
        //////System.out.println("ledgercode = " + ledgercode);
        map.put("ledgercode", ledgercode);
        map.put("description", financeTO.getDescription());
        map.put("groupCode", ThrottleConstants.bankAccountsGroupCode);
        map.put("levelId", ThrottleConstants.bankAccountsLevelId);
        map.put("financialYear", ThrottleConstants.financialYear);
        map.put("bankBranchname", financeTO.getBankname() + " - " + financeTO.getBranchName());
        status = (Integer) getSqlMapClientTemplate().insert("finance.insertBankLedger", map);
        map.put("ledgerId", status);
        try {
            //////System.out.println("insertBank map:" + map);
            status = (Integer) getSqlMapClientTemplate().update("finance.insertBank", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertBank Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertBank", sqlException);
        }
        return status;
    }

    public int savePrimaryBank(FinanceTO financeTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", userId);
        map.put("bankname", financeTO.getBankname());
        map.put("bankcode", financeTO.getBankcode());
        map.put("Activeind", financeTO.getActive_ind());

        try {
            System.out.println("insert Primary Bank map:" + map);
            status = (Integer) getSqlMapClientTemplate().update("finance.insertPrimaryBank", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insert Primary Bank Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertPrimaryBank", sqlException);
        }
        return status;
    }

    public String getClosingBalance(String ledgerId, String sDate) {
        Map map = new HashMap();
        map.put("ledgerId", ledgerId);
        map.put("sDate", sDate);
        String closingBalance = "";
        System.out.println("map new = " + map);
        try {
            closingBalance = (String) getSqlMapClientTemplate().queryForObject("finance.getClosingBalance", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOpeningBalance Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getOpeningBalance", sqlException);
        }
        return closingBalance;
    }

    public String getOpeningBalance(String ledgerId, String sDate) {
        Map map = new HashMap();
        map.put("ledgerId", ledgerId);
        map.put("sDate", sDate);
        String openingBalance = "";
        System.out.println("map new = " + map);
        try {
            openingBalance = (String) getSqlMapClientTemplate().queryForObject("finance.getOpeningBalance", map);
            /* ArrayList opList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getOpeningBalance", map);

            Iterator itr = opList.iterator();
            FinanceTO fto = null;
            Float creditValue = 0.00F;
            Float debitValue = 0.00F;
            String accountType = "N";
            while (itr.hasNext()) {
                fto = new FinanceTO();
                fto = (FinanceTO) itr.next();
                System.out.println("fto.getAccountType():" + fto.getAccountType());
                System.out.println("fto.getAccountAmount():" + fto.getAccountAmount());
                if ("credit".equalsIgnoreCase(fto.getAccountType())) {
                    creditValue = Float.parseFloat(fto.getAccountAmount());
                }
                if ("debit".equalsIgnoreCase(fto.getAccountType())) {
                    debitValue = Float.parseFloat(fto.getAccountAmount());
                }
            }
            if (creditValue == debitValue) {
                openingBalance = "0~N";
            }
            if (creditValue > debitValue) {
                openingBalance = (creditValue - debitValue) + "~CREDIT";
            }
            if (debitValue > creditValue) {
                openingBalance = (debitValue - creditValue) + "~DEBIT";
            }
             */
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOpeningBalance Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getOpeningBalance", sqlException);
        }
        return openingBalance;
    }

    public ArrayList processBankalterList(String bankid) {
        Map map = new HashMap();
        ArrayList bankList = new ArrayList();
        try {
            map.put("bankid", bankid);
            //////System.out.println("map = " + map);
            bankList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getalterBank", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "bankList", sqlException);
        }
        return bankList;
    }

    public int saveAlterBank(FinanceTO financeTO, int userid) {

        Map map = new HashMap();
        int status = 0;

        map.put("branchId", financeTO.getBranchId());
        map.put("userid", userid);
        map.put("branchName", financeTO.getBranchName());
        map.put("bankcode", financeTO.getBankcode());
        map.put("address", financeTO.getAddress());
        map.put("phoneNo", financeTO.getPhoneNo());
        map.put("accntCode", financeTO.getAccntCode());
        map.put("description", financeTO.getDescription());
        map.put("IFSCCode", financeTO.getIFSCCode());
        map.put("BSRCode", financeTO.getBSRCode());

        System.out.println("map = " + map);

        try {
            status = (Integer) getSqlMapClientTemplate().update("finance.updateBank", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertBank", sqlException);
        }
        return status;
    }

    public ArrayList getGroupList() {
        Map map = new HashMap();
        ArrayList groupList = new ArrayList();

        try {
            groupList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getGroupList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getGroupList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getGroupList", sqlException);
        }
        return groupList;
    }

    public int saveNewGroup(FinanceTO financeTO, int userId) {
        Map map = new HashMap();
        int status = 0;
//        CryptoLibrary cl = new CryptoLibrary();
        map.put("userId", userId);
        map.put("groupname", financeTO.getGroupname());
        map.put("groupcode", financeTO.getGroupcode());
        map.put("description", financeTO.getGroupdesc());

        try {
            status = (Integer) getSqlMapClientTemplate().update("finance.insertGroup", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertBank", sqlException);
        }
        return status;
    }

    public ArrayList processGroupalterList(String groupid) {
        Map map = new HashMap();
        ArrayList groupList = new ArrayList();
        try {
            map.put("groupid", groupid);
            //////System.out.println("map = " + map);
            groupList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getalterGroup", map);
            //////System.out.println("groupList size=" + groupList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "bankList", sqlException);
        }
        return groupList;
    }

    public int saveAlterGroup(FinanceTO financeTO, String groupid, int userId) {
        //////System.out.println("groupid DAO= " + groupid);
        //////System.out.println("userId DAO= " + userId);
        Map map = new HashMap();
        int status = 0;
        map.put("userId", userId);
        map.put("groupid", groupid);
        map.put("groupname", financeTO.getGroupname());
        map.put("groupcode", financeTO.getGroupcode());
        map.put("description", financeTO.getGroupdesc());
        try {
            status = (Integer) getSqlMapClientTemplate().update("finance.updateGroup", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertBank", sqlException);
        }
        return status;
    }

    /**
     * This method used to get primary list
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList handlePrimaryList() {
        //////System.out.println("in DAO::::::");
        Map map = new HashMap();
        ArrayList primaryLest = new ArrayList();
        try {
            primaryLest = (ArrayList) getSqlMapClientTemplate().queryForList("finance.handlePrimaryList", map);
            //////System.out.println("primaryLest size is::" + primaryLest.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("handlePrimaryList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "handlePrimaryList", sqlException);
        }
        return primaryLest;
    }

    public ArrayList getPendingInvoiceDetails(FinanceTO financeTO) {
        Map map = new HashMap();
        ArrayList invoiceDetails = new ArrayList();
        try {
            map.put("customerId", financeTO.getCustomerId());
            map.put("fromDate", financeTO.getFromDate());
            map.put("toDate", financeTO.getToDate());

            //////System.out.println("invoiceDetails map= " + map);
            invoiceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getPendingInvoiceDetails", map);
            //////System.out.println("invoiceDetails DAO= " + invoiceDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("invoiceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "invoiceDetails", sqlException);
        }
        return invoiceDetails;
    }

    public ArrayList getLevelList(FinanceTO lData1) {
        Map map = new HashMap();
        ArrayList levelList = new ArrayList();
        map.put("groupID", lData1.getGroupID());
        map.put("refId", lData1.getLevelgroupId());
        map.put("fromDate", lData1.getFromDate());
        map.put("toDate", lData1.getToDate());
        //////System.out.println("getLevelList...map: " + map);
        try {
            //levelList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getLevelList", map);
            levelList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getLevelListDateBased", map);

            //////System.out.println("levelList size is::" + levelList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getLevelGroupCode Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getLevelGroupCode", sqlException);
        }
        return levelList;
    }

    /**
     * This method used to Get Finance Ledger .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getLedgerList() {
        //////System.out.println("DAO");
        Map map = new HashMap();
        ArrayList ledgerList = new ArrayList();
        try {
            ledgerList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getLedgerList", map);
            //////System.out.println("ledgerList DAO = " + ledgerList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getLedgerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getLedgerList", sqlException);
        }
        return ledgerList;
    }

    public int saveNewLedger(FinanceTO financeTO, int userId, SqlMapClient session) throws SQLException {
        Map map = new HashMap();
        String code = "";
        String delimiter = "-";
        String[] temp;
        int status = 0;
        map.put("userId", userId);
        map.put("ledgername", financeTO.getLedgerName());
        map.put("primaryID", financeTO.getPrimaryID());
        map.put("levelgroupID", financeTO.getLevelgroupId());
        map.put("description", financeTO.getDescription());
        map.put("fullName", financeTO.getFullName());
        map.put("openingBalance", financeTO.getOpeningBalance());
        map.put("accountType", financeTO.getAcctType());
        if ("DEBIT".equals(financeTO.getAcctType())) {
            map.put("debit", financeTO.getOpeningBalance());
            map.put("credit", "0");
        } else if ("CREDIT".equals(financeTO.getAcctType())) {
            map.put("credit", financeTO.getOpeningBalance());
            map.put("debit", "0");
        }
        map.put("openingBalanceDate", financeTO.getOpeningBalanceDate());
        map.put("remarks", "OpeningBalanceAdd");

        code = (String) session.queryForObject("finance.getLedgerCode", map);
        int codeval = 0;
        int codev = 0;
        if (code == null) {
            codev = 0;
        } else {
            temp = code.split(delimiter);
            //////System.out.println("temp[1] = " + temp[1]);
            codeval = Integer.parseInt(temp[1]);
            codev = codeval + 1;

        }
        System.out.println("codev = " + codev);
        String ledgercode = "LEDGER-" + codev;
        System.out.println("ledgercode = " + ledgercode);

        //current year and month start
        String accYear = (String) session.queryForObject("finance.accYearVal", map);
        System.out.println("accYear:" + accYear);
        map.put("accYear", accYear);
        //current year and month end

        map.put("ledgercode", ledgercode);
//        map.put("groupode", financeTO.getGroupCode());
//        map.put("amounttype", financeTO.getAmountType());
        System.out.println("map = " + map);
        try {
            status = (Integer) session.insert("finance.insertLedger", map);
            System.out.println("status = " + status);
            int financeStatus = 0;
            if (status > 0) {
                map.put("ledgerID", status);
                map.put("accountType", financeTO.getAccountType());
                map.put("voucherType", "%PAYMENT%");
                map.put("particularsId", ledgercode);
                map.put("reference", "OB");
                map.put("formId", ThrottleConstants.cashPaymentFormId);
                String code2 = (String) session.queryForObject("finance.getVoucerCode", map);
                String[] temp1 = code2.split("-");
                int codeval2 = Integer.parseInt(temp1[1]);
                int codev2 = codeval2 + 1;
                String voucherCode = "PAYMENT-" + codev2;
                System.out.println("voucherCode = " + voucherCode);
                map.put("voucherCode", voucherCode);
                System.out.println("financeEnterty" + map);
                financeStatus = (Integer) session.update("finance.insertLedgerFinanceEntry", map);

            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertLedger", sqlException);
        }
        return status;
    }

    /*
    public int saveNewLedger(FinanceTO financeTO, int userId) {
        Map map = new HashMap();
        String code = "";
        String delimiter = "-";
        String[] temp;
        int status = 0;
        map.put("userId", userId);
        map.put("ledgerID", financeTO.getLedgerID());
        map.put("ledgername", financeTO.getLedgerName());
        map.put("primaryID", financeTO.getPrimaryID());
        map.put("levelgroupID", financeTO.getLevelgroupId());
        map.put("description", financeTO.getDescription());
        map.put("openingBalance", financeTO.getOpeningBalance());
        map.put("accountType", financeTO.getAcctType());
        map.put("openingBalanceDate", financeTO.getDate());
//        map.put("fullName", financeTO.getFullName());
//        map.put("amounttype", financeTO.getAmountType());
//        map.put("groupcode", financeTO.getGroupCode());

        code = (String) getSqlMapClientTemplate().queryForObject("finance.getLedgerCode", map);
        int codeval = 0;
        int codev = 0;
        if (code == null) {
            codev = 0;
        } else {
            temp = code.split(delimiter);
            //////System.out.println("temp[1] = " + temp[1]);
            codeval = Integer.parseInt(temp[1]);
            codev = codeval + 1;

        }
        //////System.out.println("codev = " + codev);
        String ledgercode = "LEDGER-" + codev;
        //////System.out.println("ledgercode = " + ledgercode);

        //current year and month start
        String accYear = (String) getSqlMapClientTemplate().queryForObject("finance.accYearVal", map);
        System.out.println("accYear:" + accYear);
        //current year and month end

        map.put("accYear", accYear);
        map.put("ledgercode", ledgercode);
        System.out.println("map = " + map);

        try {
            if ("CREDIT".equalsIgnoreCase(financeTO.getAcctType())) {
                map.put("credit", financeTO.getOpeningBalance());
                map.put("debit", 0.00);
            } else {
                map.put("debit", financeTO.getOpeningBalance());
                map.put("credit", 0.00);
            }
            if (financeTO.getLedgerID() != null && financeTO.getLedgerID() == "") {
                status = (Integer) getSqlMapClientTemplate().update("finance.insertLedger", map);

            } else {
                status = (Integer) getSqlMapClientTemplate().update("finance.updateLedger", map);

            }
//            }
//            if("CREDIT".equalsIgnoreCase(financeTO.getAcctType())){
//            if (financeTO.getLedgerID() != null && financeTO.getLedgerID() == "") {
//                status = (Integer) getSqlMapClientTemplate().update("finance.insertLedgerCredit", map);
//
//            } else {
//                status = (Integer) getSqlMapClientTemplate().update("finance.updateLedgerCredit", map);
//
//            }
//            }

//        try {
//            status = (Integer) getSqlMapClientTemplate().update("finance.insertLedger", map);
//            //////System.out.println("status = " + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
           
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertLedger", sqlException);
        }
        return status;
    }
     */
    public int saveNewLevel(FinanceTO financeTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("primaryID", financeTO.getPrimaryID());
        map.put("levelgroupId", financeTO.getLevelgroupId());
        map.put("fullName", financeTO.getFullName());
        map.put("levelName", financeTO.getLevelName());
        map.put("description", financeTO.getDescription());
        map.put("openingBalance", financeTO.getOpeningBalance());
        //////System.out.println("map = " + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("finance.insertLevelGroup", map);
            //////System.out.println("status = " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveNewLevel", sqlException);
        }
        return status;
    }

    public ArrayList processledgeralerList(String ledgerID) {
        Map map = new HashMap();
        ArrayList ledgerList = new ArrayList();
        try {
            map.put("ledgerID", ledgerID);
            //////System.out.println("map = " + map);
            ledgerList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getalterLedger", map);
            //////System.out.println("ledgerList size=" + ledgerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "ledgerList", sqlException);
        }
        return ledgerList;
    }

    /*   public int saveAlterLedger(FinanceTO financeTO, String ledgerID, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", userId);
        map.put("ledgerID", ledgerID);
        map.put("amountType", financeTO.getAmountType());
        map.put("openingBalance", financeTO.getOpeningBalance());
        map.put("openingBalanceDate", financeTO.getDate());
        try {
            status = (Integer) getSqlMapClientTemplate().update("finance.updateLedger", map);

        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "Update Ledger", sqlException);
        }
        return status;
    }
     */
    public int saveAlterLedger(FinanceTO financeTO, String ledgerID, int userId, SqlMapClient session) throws SQLException {
        Map map = new HashMap();
        int status = 0;
        int financeStatus = 0;
        String code = "";
        String delimiter = "-";
        String[] temp;
        map.put("userId", userId);
        map.put("ledgerID", ledgerID);
//        map.put("amountType", financeTO.getAmountType());
        map.put("openingBalance", financeTO.getOpeningBalance());
        map.put("openingBalanceDate", financeTO.getDate());
        map.put("accountType", financeTO.getAmountType());
        map.put("remarks", "OpeningBalanceEdit");
        code = (String) session.queryForObject("finance.getLedgerCode", map);
        temp = code.split(delimiter);
        System.out.println("temp[1] = " + temp[1]);
        int codeval = Integer.parseInt(temp[1]);
        int codev = codeval + 1;
        System.out.println("codev = " + codev);
        String ledgercode = "LEDGER-" + codev;
        System.out.println("ledgercode = " + ledgercode);

        //current year and month start
        String accYear = (String) session.queryForObject("finance.accYearVal", map);
        System.out.println("accYear:" + accYear);
        map.put("accYear", accYear);
        //current year and month end

        map.put("ledgercode", ledgercode);
        map.put("groupode", financeTO.getGroupCode());
        map.put("amounttype", financeTO.getAmountType());
        System.out.println("map = " + map);
        try {
            status = (Integer) session.update("finance.updateLedger", map);

            map.put("accountType", financeTO.getAmountType());
            map.put("voucherType", "%PAYMENT%");
            map.put("particularsId", ledgercode);
            map.put("reference", "OB");
            map.put("formId", ThrottleConstants.cashPaymentFormId);
            String code2 = (String) session.queryForObject("finance.getVoucerCode", map);
            String[] temp1 = code2.split("-");
            int codeval2 = Integer.parseInt(temp1[1]);
            int codev2 = codeval2 + 1;
            String voucherCode = "PAYMENT-" + codev2;
            System.out.println("voucherCode = " + voucherCode);
            map.put("voucherCode", voucherCode);
            System.out.println("financeEnterty" + map);
            /*
            financeStatus = (Integer) session.update("finance.insertLedgerFinanceEntry", map);
             */
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "Update Ledger", sqlException);
        }
        return status;
    }

    /**
     * This Ajax method used to Get level1 List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception Model
     */
    public ArrayList getLevel1Data(String groupCode) {
        //////System.out.println("in daoooooooooooo");
        Map map = new HashMap();
        ArrayList level1List = new ArrayList();
        map.put("groupCode", groupCode);
        try {

            level1List = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getLevel1Data", map);
            //////System.out.println("level1List.size() = " + level1List.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "AjaxModelList", sqlException);
        }
        return level1List;

    }

    /**
     * This Ajax method used to Get level2 List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception Model
     */
    public ArrayList getLeve21Data(String level1Code) {
        //////System.out.println("in daoooooooooooo");
        Map map = new HashMap();
        ArrayList level2List = new ArrayList();
        map.put("level1Code", level1Code);
        try {

            level2List = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getLevel2Data", map);
            //////System.out.println("level1List.size() = " + level2List.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "AjaxModelList", sqlException);
        }
        return level2List;

    }

    /**
     * This Ajax method used to Get level 3 List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception Model
     */
    public ArrayList getLeve31Data(String level2Code) {
        //////System.out.println("in daoooooooooooo");
        Map map = new HashMap();
        ArrayList level3List = new ArrayList();
        map.put("level2Code", level2Code);
        try {

            level3List = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getLevel3Data", map);
            //////System.out.println("level3List.size() = " + level3List.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "level3List", sqlException);
        }
        return level3List;

    }

    /**
     * This method used to Get Finance Voucher .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getVoucherList() {
        Map map = new HashMap();
        ArrayList voucherList = new ArrayList();

        try {
            voucherList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getVoucherList", map);
            //////System.out.println("voucherList = " + voucherList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("voucherList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "voucherList", sqlException);
        }
        return voucherList;
    }

    public int saveNewVoucher(FinanceTO financeTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", userId);
        map.put("VoucherTypeName", financeTO.getVoucherTypeName());
        map.put("VoucherTypeCode", financeTO.getVoucherTypeCode());
        map.put("Description", financeTO.getDescription());
        //////System.out.println("map =----------------> " + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("finance.insertVoucher", map);
            //////System.out.println("status = " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertLedger", sqlException);
        }
        return status;
    }

    public ArrayList processVoucheralterList(String voucherTypeID) {
        Map map = new HashMap();
        ArrayList voucherList = new ArrayList();
        try {
            map.put("voucherTypeID", voucherTypeID);
            //////System.out.println("map = " + map);
            voucherList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getalterVoucher", map);
            //////System.out.println("voucherTypeID size=" + voucherList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "VoucherList", sqlException);
        }
        return voucherList;
    }

    public int saveAlterVoucher(FinanceTO financeTO, String voucherTypeID, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", userId);
        map.put("voucherTypeID", voucherTypeID);
        map.put("voucherTypeName", financeTO.getVoucherTypeName());
        map.put("voucherTypeCode", financeTO.getVoucherTypeCode());
        map.put("description", financeTO.getDescription());
        try {
            status = (Integer) getSqlMapClientTemplate().update("finance.updateVoucher", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "Update Ledger", sqlException);
        }
        return status;
    }

    /**
     * This method used to Get Finance Account Type .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getAccountTypeList() {
        Map map = new HashMap();
        ArrayList accountTypeList = new ArrayList();

        try {
            accountTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getAccountTypeList", map);
            //////System.out.println("accountTypeList = " + accountTypeList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("voucherList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "voucherList", sqlException);
        }
        return accountTypeList;
    }

    public int saveNewAccountType(FinanceTO financeTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", userId);
        map.put("accountEntryTypeName", financeTO.getAccountEntryTypeName());
        map.put("accountEntryTypeCode", financeTO.getAccountEntryTypeCode());
        map.put("Description", financeTO.getDescription());
        //////System.out.println("map =----------------> " + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("finance.insertAccountType", map);
            //////System.out.println("status = " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertLedger", sqlException);
        }
        return status;
    }

    public ArrayList processAccountTypealterList(String accountEntryTypeID) {
        Map map = new HashMap();
        ArrayList accountTypeList = new ArrayList();
        try {
            map.put("accountEntryTypeID", accountEntryTypeID);
            //////System.out.println("map = " + map);
            accountTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getalterAccountType", map);
            //////System.out.println("voucherTypeID size=" + accountTypeList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "accountEntryType List", sqlException);
        }
        return accountTypeList;
    }

    public int saveAlterAccountType(FinanceTO financeTO, String accountEntryTypeID, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", userId);
        map.put("accountEntryTypeID", accountEntryTypeID);
        map.put("AccountEntryTypeName", financeTO.getAccountEntryTypeName());
        map.put("AccountEntryTypeCode", financeTO.getAccountEntryTypeCode());
        map.put("description", financeTO.getDescription());
        //////System.out.println("map 1= " + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("finance.updateAccountType", map);
            //////System.out.println("status 2= " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "Update Ledger", sqlException);
        }
        return status;
    }

    /**
     * This method used to Get Tax list.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getTaxList() {
        Map map = new HashMap();
        ArrayList taxList = new ArrayList();

        try {
            taxList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getTaxList", map);
            //////System.out.println("taxList = " + taxList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("voucherList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "taxList", sqlException);
        }
        return taxList;
    }

    public int saveNewTax(FinanceTO financeTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", userId);
        map.put("taxName", financeTO.getTaxName());
        map.put("taxCode", financeTO.getTaxCode());
        map.put("Description", financeTO.getDescription());
        map.put("taxPercent", financeTO.getTaxPercent());
        map.put("effectiveDate", financeTO.getEffectiveDate());

        System.out.println("map =----------------> " + map);

        try {
            status = (Integer) getSqlMapClientTemplate().update("finance.insertTax", map);
            //////System.out.println("status = " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertLedger", sqlException);
        }
        return status;
    }

    public ArrayList processTaxAlterList(String taxId) {
        Map map = new HashMap();
        ArrayList taxList = new ArrayList();
        try {
            map.put("taxId", taxId);
            //////System.out.println("map = " + map);
            taxList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getalterTax", map);
            //////System.out.println("voucherTypeID size=" + taxList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "accountEntryType List", sqlException);
        }
        return taxList;
    }

    public int saveAlterTax(FinanceTO financeTO, String taxId, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", userId);
        map.put("taxId", taxId);
        map.put("taxName", financeTO.getTaxName());
        map.put("taxCode", financeTO.getTaxCode());
        map.put("Description", financeTO.getDescription());
        map.put("taxPercent", financeTO.getTaxPercent());
        map.put("effectiveDate", financeTO.getEffectiveDate());

        System.out.println("map 1= " + map);

        try {

            int checkDate = (Integer) getSqlMapClientTemplate().queryForObject("finance.checkEffectiveDate", map);

            System.out.println("checkDate :" + checkDate);

            if (checkDate == 0) {
                status = (Integer) getSqlMapClientTemplate().update("finance.insertTax", map);
                status = (Integer) getSqlMapClientTemplate().update("finance.updateTaxInactive", map);
                System.out.println("status 1= " + status);
            } else {
                status = (Integer) getSqlMapClientTemplate().update("finance.updateTax", map);
                System.out.println("status 2= " + status);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "Update Ledger", sqlException);
        }
        return status;
    }

    /**
     * This method used to Get country list.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getCountryList() {
        Map map = new HashMap();
        ArrayList countryList = new ArrayList();

        try {
            countryList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getCountryList", map);
            //////System.out.println("taxList = " + countryList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("voucherList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "taxList", sqlException);
        }
        return countryList;
    }

    public int saveNewCountry(FinanceTO financeTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", userId);
        map.put("countryName", financeTO.getCountryName());
        map.put("countryCode", financeTO.getCountryCode());
        //////System.out.println("map =----------------> " + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("finance.insertCountry", map);
            //////System.out.println("status = " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertLedger", sqlException);
        }
        return status;
    }

    public ArrayList processCountryAlterList(String countryID) {
        Map map = new HashMap();
        ArrayList countryList = new ArrayList();
        try {
            map.put("countryID", countryID);
            //////System.out.println("map = " + map);
            countryList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getalterCountry", map);
            //////System.out.println("voucherTypeID size=" + countryList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "accountEntryType List", sqlException);
        }
        return countryList;
    }

    public int saveAlterCountry(FinanceTO financeTO, String countryID, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", userId);
        map.put("countryID", countryID);
        map.put("countryName", financeTO.getCountryName());
        map.put("CountryCode", financeTO.getCountryCode());
        //////System.out.println("map 1= " + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("finance.updateCountry", map);
            //////System.out.println("status 2= " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "Update Ledger", sqlException);
        }
        return status;
    }

    /**
     * This method used to Get State list.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getStateList() {
        Map map = new HashMap();
        ArrayList StateList = new ArrayList();

        try {
            StateList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getStateList", map);
            //////System.out.println("StateList = " + StateList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("StateList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "StateList", sqlException);
        }
        return StateList;
    }

    public int saveNewState(FinanceTO financeTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", userId);
        map.put("stateName", financeTO.getStateName());
        map.put("stateCode", financeTO.getStateCode());
        map.put("countryID", financeTO.getCountryID());
        map.put("description", financeTO.getDescription());
        //////System.out.println("map = " + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("finance.insertState", map);
            //////System.out.println("status = " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertLedger", sqlException);
        }
        return status;
    }

    public ArrayList processStatealterList(String stateID) {
        Map map = new HashMap();
        ArrayList StateList = new ArrayList();
        try {
            map.put("stateID", stateID);
            //////System.out.println("map = " + map);
            StateList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getalterState", map);
            //////System.out.println("StateList size=" + StateList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "StateList", sqlException);
        }
        return StateList;
    }

    public int saveAlterState(FinanceTO financeTO, String stateID, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", userId);
        map.put("stateID", stateID);
        map.put("stateName", financeTO.getStateName());
        map.put("stateCode", financeTO.getStateCode());
        map.put("countryID", financeTO.getCountryID());
        map.put("description", financeTO.getDescription());
        //////System.out.println("map 1= " + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("finance.updateState", map);
            //////System.out.println("status 2= " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "Update State", sqlException);
        }
        return status;
    }

    /**
     * This method used to Get District list.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getDistrictList() {
        Map map = new HashMap();
        ArrayList DistrictList = new ArrayList();

        try {
            DistrictList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getDistrictList", map);
            //////System.out.println("DistrictList = " + DistrictList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("DistrictList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "District List", sqlException);
        }
        return DistrictList;
    }

    public int saveNewDistrict(FinanceTO financeTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", userId);
        map.put("districtName", financeTO.getDistrictName());
        map.put("districtCode", financeTO.getDistrictCode());
        map.put("countryID", financeTO.getCountryID());
        map.put("stateID", financeTO.getStateID());
        map.put("description", financeTO.getDescription());
        //////System.out.println("map = " + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("finance.insertDistrict", map);
            //////System.out.println("status = " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insert State", sqlException);
        }
        return status;
    }

    public ArrayList processDistrictalterList(String districtID) {
        Map map = new HashMap();
        ArrayList DistrictList = new ArrayList();
        try {
            map.put("districtID", districtID);
            //////System.out.println("map = " + map);
            DistrictList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getalterDistrict", map);
            //////System.out.println("DistrictList size=" + DistrictList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "DistrictList", sqlException);
        }
        return DistrictList;
    }

    public int saveAlterDistrict(FinanceTO financeTO, String districtID, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", userId);
        map.put("districtName", financeTO.getDistrictName());
        map.put("districtCode", financeTO.getDistrictCode());
        map.put("countryID", financeTO.getCountryID());
        map.put("stateID", financeTO.getStateID());
//        map.put("countryID", financeTO.getCountryName());
        map.put("description", financeTO.getDescription());
        map.put("DistrictID", financeTO.getDistrictID());
        //////System.out.println("map 1= " + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("finance.updateDistrict", map);
            //////System.out.println("status 2= " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "Update District", sqlException);
        }
        return status;
    }

    public ArrayList getcontraEntryList() {
        //////System.out.println("DAO");
        Map map = new HashMap();
        ArrayList contraEntryList = new ArrayList();
        try {
            contraEntryList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getContraEntryList", map);
            //////System.out.println("ContraEntryList DAO = " + contraEntryList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContraEntryList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getContraEntryList", sqlException);
        }
        return contraEntryList;
    }

    /**
     * This method used to Get Contra Entry.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getBankLedgerList() {
        //////System.out.println("in DAO");
        Map map = new HashMap();
        ArrayList bankLedgerList = new ArrayList();
        try {
            //////System.out.println("tttttt");
            bankLedgerList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getBankLList", map);
            //////System.out.println("bankLedgerList DAO= " + bankLedgerList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("voucherList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "bankLedgerList", sqlException);
        }
        return bankLedgerList;
    }

    public int insertContraEntry(int detailCode, int userId, String date, String bankid1Values, String amountValues, String accTypeValues, String narrationValues, String voucherCode) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        String code = "";
        String delimiter = "-";
        String[] temp;

        int status = 0;
        map.put("entryStatus", "CE");
        map.put("detailCode", detailCode);
        map.put("userId", userId);
        map.put("date", date);
        map.put("bankid1Values", bankid1Values);

        String[] val = bankid1Values.split("~");
        map.put("ledgerID", val[0]);
        map.put("groupCode", val[1]);
        map.put("levelID", val[2]);
        map.put("ledgerCode", val[3]);

        map.put("amountValues", amountValues);
        map.put("accTypeValues", accTypeValues);
        map.put("narrationValues", narrationValues);
        //////System.out.println("map = " + map);
        //////System.out.println("---------------");
        map.put("voucherType", "%CONTRA%");
        map.put("reference", "CONTRA");
        map.put("searchCode", "CONTRA");

        /*  Commented By Arularasan as on 24-10-2103
         *  for here code value must be increamented by twice
         *
         code = (String) getSqlMapClientTemplate().queryForObject("finance.getVoucerCode", map);
         temp = code.split(delimiter);
         //////System.out.println("temp[1] = " + temp[1]);
         int codeval = Integer.parseInt(temp[1]);
         int codev = codeval + 1;
         //////System.out.println("codev = " + codev);
         String voucherCode = "CONTRA-" + codev;
         //////System.out.println("voucherCode = " + voucherCode);
         *
         */
        map.put("voucherCode", voucherCode);

        //////System.out.println("insert contra in DAO...");
        //////System.out.println("userId" + userId);
        //////System.out.println("date" + date);
        //////System.out.println("bankid1Values" + bankid1Values);
        //////System.out.println("amountValues" + amountValues);
        //////System.out.println("accTypeValues" + accTypeValues);
        //////System.out.println("narrationValues" + narrationValues);
        //////System.out.println("map = " + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("finance.insertContraEntry", map);
            //////System.out.println("status in DAO= " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContraEntry Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertContraEntry", sqlException);
        }
        return status;
    }

    /**
     * This method used to Get journal Entry.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getInvoiceList() {
        //////System.out.println("in DAO");
        Map map = new HashMap();
        ArrayList invoiceList = new ArrayList();
        try {
            invoiceList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getInvoiceList", map);
            //////System.out.println("invoiceList DAO= " + invoiceList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("invoiceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "invoiceList", sqlException);
        }
        return invoiceList;
    }

    /**
     * This method used to Get Credit Note.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertCreditNote(int detailCode, int userId, String date, String bankid1Values, String amountValues, String accTypeValues, String narrationValues) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        String code = "";
        String delimiter = "-";
        String[] temp;
        int status = 0;
        map.put("detailCode", detailCode);
        map.put("userId", userId);
        map.put("date", date);
        map.put("bankid1Values", bankid1Values);
        map.put("amountValues", amountValues);
//      map.put("accTypeValues", accTypeValues);
        map.put("narrationValues", narrationValues);
        map.put("voucherType", "%CreditNote%");
        code = (String) getSqlMapClientTemplate().queryForObject("finance.getVoucerCode", map);
        temp = code.split(delimiter);
        //////System.out.println("temp[1] = " + temp[1]);
        int codeval = Integer.parseInt(temp[1]);
        int codev = codeval + 1;
        //////System.out.println("codev = " + codev);
        String voucherCode = "CreditNote-" + codev;
        //////System.out.println("voucherCode = " + voucherCode);

        map.put("voucherCode", voucherCode);

        //////System.out.println("insert CreditNote in DAO...");
        //////System.out.println("userId" + userId);
        //////System.out.println("date" + date);
        //////System.out.println("bankid1Values" + bankid1Values);
        //////System.out.println("amountValues" + amountValues);
//      //////System.out.println("accTypeValues"+accTypeValues);
        //////System.out.println("narrationValues" + narrationValues);
        //////System.out.println("map = " + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("finance.insertCreditNote", map);
            //////System.out.println("status in DAO= " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertCreditNote Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertCreditNote", sqlException);
        }
        return status;
    }

    /**
     * This method used to Get Debit Note.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertDebitNote(int detailCode, int userId, String date, String bankid1Values, String amountValues, String accTypeValues, String narrationValues) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        String code = "";
        String delimiter = "-";
        String[] temp;
        int status = 0;
        map.put("detailCode", detailCode);
        map.put("userId", userId);
        map.put("date", date);
        map.put("bankid1Values", bankid1Values);
        map.put("amountValues", amountValues);
//      map.put("accTypeValues", accTypeValues);
        map.put("narrationValues", narrationValues);
        map.put("voucherType", "%DebitNote%");

        code = (String) getSqlMapClientTemplate().queryForObject("finance.getVoucerCode", map);
        temp = code.split(delimiter);
        //////System.out.println("temp[1] = " + temp[1]);
        int codeval = Integer.parseInt(temp[1]);
        int codev = codeval + 1;
        //////System.out.println("codev = " + codev);
        String voucherCode = "DebitNote-" + codev;
        //////System.out.println("voucherCode = " + voucherCode);

        map.put("voucherCode", voucherCode);

        //////System.out.println("insert DebitNote in DAO...");
        //////System.out.println("userId" + userId);
        //////System.out.println("date" + date);
        //////System.out.println("bankid1Values" + bankid1Values);
        //////System.out.println("amountValues" + amountValues);
//        //////System.out.println("accTypeValues"+accTypeValues);
        //////System.out.println("narrationValues" + narrationValues);
        //////System.out.println("map = " + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("finance.insertDebitNote", map);
            //////System.out.println("status in DAO= " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertDebitNote Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertDebitNote", sqlException);
        }
        return status;
    }

    /**
     * This method used to Get Contra Entry.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getjournalEntryList() {
        //////System.out.println("DAO");
        Map map = new HashMap();
        ArrayList contraEntryList = new ArrayList();
        try {
            contraEntryList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getJournalEntryList", map);
            //////System.out.println("JournalEntryList DAO = " + contraEntryList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJournalEntryList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getjournalEntryList", sqlException);
        }
        return contraEntryList;
    }

    /**
     * This method used to Get Journal Code.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String getVoucherCode() {
        Map map = new HashMap();
        String journalCode = "";
        String voucherCode = "";
        map.put("voucherType", "%JOURNAL%");
        String temp[] = null;
        try {
            if ((String) getSqlMapClientTemplate().queryForObject("finance.getVoucerCode", map) != null) {
                journalCode = (String) getSqlMapClientTemplate().queryForObject("finance.getVoucerCode", map);
                temp = journalCode.split("-");
                int codeVal = Integer.parseInt(temp[1]);
                int codev = codeVal + 1;
                voucherCode = "JOURNAL-" + codev;
            } else {
                voucherCode = "JOURNAL-" + 1;
            }
            //////System.out.println("voucherCode = " + voucherCode);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJournalEntryList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getjournalEntryList", sqlException);
        }
        return voucherCode;
    }

    public int insertJournalEntry(int detailCode, int userId, String date, String bankid1Values, String amountValues, String accTypeValues, String narrationValues, String voucherCode) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        String code = "";
        String[] temp;
        int status = 0;

        map.put("entryStatus", "JE");
        map.put("detailCode", detailCode);
        map.put("userId", userId);
        map.put("date", date);
        map.put("bankid1Values", bankid1Values);

        map.put("reference", "JOURNAL");
        map.put("searchCode", "JOURNAL");

        String[] val = bankid1Values.split("~");
        map.put("ledgerID", val[0]);
        map.put("groupCode", val[1]);
        map.put("levelID", val[2]);
        map.put("ledgerCode", val[3]);
        map.put("amountValues", amountValues);
        map.put("accTypeValues", accTypeValues);
        map.put("narrationValues", narrationValues);
        map.put("voucherCode", voucherCode);
        //////System.out.println("insert journal in DAO...");
        //////System.out.println("userId" + userId);
        //////System.out.println("date" + date);
        //////System.out.println("bankid1Values" + bankid1Values);
        //////System.out.println("amountValues" + amountValues);
        //////System.out.println("accTypeValues" + accTypeValues);
        //////System.out.println("narrationValues" + narrationValues);
        //////System.out.println("map = " + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("finance.insertJournalEntry", map);

            //////System.out.println("status in DAO= " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJournalEntry Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJournalEntry", sqlException);
        }
        return status;
    }

    /**
     * This method used to Get Journal Code List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getJournalCodeList() {
        Map map = new HashMap();
        ArrayList journalCodeList = new ArrayList();
        try {

            map.put("voucherNo", ThrottleConstants.JournalVoucherCode);
            journalCodeList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getJournalCodeList", map);
            //////System.out.println("journalCodeList = " + journalCodeList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJournalCodeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getJournalCodeList", sqlException);
        }
        return journalCodeList;
    }

    /**
     * This method used to Get Journal Entry Credit List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getJournalEntryCreditList() {
        Map map = new HashMap();
        ArrayList journalEntryCreditList = new ArrayList();
        try {
            journalEntryCreditList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getJournalEntryCreditList", map);
            //////System.out.println("JournalEntryCreditList = " + journalEntryCreditList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJournalEntryCreditList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getJournalEntryCreditList", sqlException);
        }
        return journalEntryCreditList;
    }

    /**
     * This method used to Get Journal Entry Debit List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getJournalEntryDebitList() {
        Map map = new HashMap();
        ArrayList journalEntryDebitList = new ArrayList();
        try {
            journalEntryDebitList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getJournalEntryDebitList", map);
            //////System.out.println("JournalEntryDebitList = " + journalEntryDebitList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJournalEntryDebitList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getJournalEntryDebitList", sqlException);
        }
        return journalEntryDebitList;
    }

    /**
     * This method used to Get Contra Code List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getContraCodeList() {
        Map map = new HashMap();
        ArrayList contraCodeList = new ArrayList();
        try {
            map.put("voucherNo", ThrottleConstants.ContraVoucherCode);
            contraCodeList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getContraCodeList", map);
            //////System.out.println("ContraCodeList = " + contraCodeList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContraCodeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getContraCodeList", sqlException);
        }
        return contraCodeList;
    }

    /**
     * This method used to Get Contra Entry Credit List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getContraEntryCreditList() {
        Map map = new HashMap();
        ArrayList contraEntryCreditList = new ArrayList();
        try {
            contraEntryCreditList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getContraEntryCreditList", map);
            //////System.out.println("ContraEntryCreditList = " + contraEntryCreditList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContraEntryCreditList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getContraEntryCreditList", sqlException);
        }
        return contraEntryCreditList;
    }

    /**
     * This method used to Get Contra Entry Debit List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getContraEntryDebitList() {
        Map map = new HashMap();
        ArrayList contraEntryDebitList = new ArrayList();
        try {
            contraEntryDebitList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getContraEntryDebitList", map);
            //////System.out.println("ContraEntryDebitList = " + contraEntryDebitList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContraEntryDebitList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getContraEntryDebitList", sqlException);
        }
        return contraEntryDebitList;
    }

    /**
     * This method used to Get Cash Payment List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getCashPaymentList() {
        Map map = new HashMap();
        ArrayList cashPaymentList = new ArrayList();
        try {
            cashPaymentList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getCashPaymentList", map);
            //////System.out.println("getCashPaymentList = " + cashPaymentList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCashPaymentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getCashPaymentList", sqlException);
        }
        return cashPaymentList;
    }

    /**
     * This method used to Get Bank Payment List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getBankPaymentList() {
        Map map = new HashMap();
        ArrayList bankPaymentList = new ArrayList();
        try {
            bankPaymentList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getBankPaymentList", map);
            //////System.out.println("getBankPaymentList = " + bankPaymentList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBankPaymentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getBankPaymentList", sqlException);
        }
        return bankPaymentList;
    }

    /**
     * This method used to Get Cash Receipt List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getCashReceiptList() {
        Map map = new HashMap();
        ArrayList cashReceiptList = new ArrayList();
        try {
            cashReceiptList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getCashReceiptList", map);
            //////System.out.println("getCashReceiptList = " + cashReceiptList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCashReceiptList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getCashReceiptList", sqlException);
        }
        return cashReceiptList;
    }

    /**
     * This method used to Get Bank Receipt List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getBankReceiptList() {
        Map map = new HashMap();
        ArrayList bankReceiptList = new ArrayList();
        try {
            bankReceiptList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getBankReceiptList", map);
            //////System.out.println("getBankReceiptList = " + bankReceiptList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBankReceiptList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getBankReceiptList", sqlException);
        }
        return bankReceiptList;
    }

    /**
     * This method used to Get Journal Entry.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getJournalEntry(String voucherCode) {
        Map map = new HashMap();
        ArrayList journalEntry = new ArrayList();
        map.put("voucherCode", voucherCode);
        //////System.out.println("map = " + map);
        try {
            journalEntry = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getJournalEntry", map);
            //////System.out.println("journalEntryList = " + journalEntry.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJournalEntryList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getJournalEntryList", sqlException);
        }
        return journalEntry;
    }

    /**
     * This method used to Get Cash Payment Entry.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getCashPaymentEntry(String voucherCode) {
        Map map = new HashMap();
        ArrayList cashPaymentEntry = new ArrayList();
        map.put("voucherCode", voucherCode);
        try {
            cashPaymentEntry = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getCashPaymentEntry", map);
            //////System.out.println("getCashPaymentEntry = " + cashPaymentEntry.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCashPaymentEntry Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getCashPaymentEntry", sqlException);
        }
        return cashPaymentEntry;
    }

    /**
     * This method used to Get Bank Payment Entry.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getBankPaymentEntryPrint(String voucherCode) {
        Map map = new HashMap();
        ArrayList bankPaymentEntry = new ArrayList();
        map.put("voucherCode", voucherCode);
        try {
            bankPaymentEntry = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getBankPaymentEntryPrint", map);
            //////System.out.println("getBankPaymentEntryPrint = " + bankPaymentEntry.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBankPaymentEntryPrint Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getBankPaymentEntryPrint", sqlException);
        }
        return bankPaymentEntry;
    }

    /**
     * This method used to Get Cash Receipt Entry.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getCashReceiptEntry(String voucherCode) {
        Map map = new HashMap();
        ArrayList cashReceiptEntry = new ArrayList();
        map.put("voucherCode", voucherCode);
        try {
            cashReceiptEntry = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getCashReceiptEntry", map);
            //////System.out.println("getCashReceiptEntry = " + cashReceiptEntry.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCashReceiptEntry Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getCashReceiptEntry", sqlException);
        }
        return cashReceiptEntry;
    }

    /**
     * This method used to Get Bank Receipt Entry.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getBankReceiptEntry(String voucherCode) {
        Map map = new HashMap();
        ArrayList bankReceiptEntry = new ArrayList();
        map.put("voucherCode", voucherCode);
        try {
            bankReceiptEntry = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getBankReceiptEntry", map);
            //////System.out.println("getBankReceiptEntry = " + bankReceiptEntry.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBankReceiptEntry Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getBankReceiptEntry", sqlException);
        }
        return bankReceiptEntry;
    }

    /**
     * This method used to Get Contra Entry.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getContraEntry(String voucherCode) {
        Map map = new HashMap();
        ArrayList contraEntry = new ArrayList();
        map.put("voucherCode", voucherCode);
        try {
            contraEntry = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getContraEntry", map);
            //////System.out.println("journalEntry = " + contraEntry.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContraEntry Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getContraEntry", sqlException);
        }
        return contraEntry;
    }

    /**
     * This method used to Get VAT list.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getVatList() {
        Map map = new HashMap();
        ArrayList vatList = new ArrayList();

        try {
            vatList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getCountryList", map);
            //////System.out.println("vatList = " + vatList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vatList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "vatList", sqlException);
        }
        return vatList;
    }

    //contra end
    // journal start
    public ArrayList getJournalLedgerList() {
        //////System.out.println("in DAO");
        Map map = new HashMap();
        ArrayList journalLedgerList = new ArrayList();
        try {
            //////System.out.println("tttttt");
            journalLedgerList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getJouranlList", map);
            //////System.out.println("bankLedgerList DAO= " + journalLedgerList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJournalLedgerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getJournalLedgerList", sqlException);
        }
        return journalLedgerList;
    }

    public ArrayList getLedgerTransactionList(String ledgerId, String sDate, String eDate) {
        Map map = new HashMap();
        map.put("sDate", sDate);
        map.put("eDate", eDate);
        map.put("ledgerId", ledgerId);
        ArrayList ledgerTransactionList = new ArrayList();
        try {
            //////System.out.println("ledgerTransactionList");
            ledgerTransactionList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getLedgerTransactionList", map);
            //////System.out.println("ledgerTransactionList DAO= " + ledgerTransactionList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getLedgerTransactionList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getLedgerTransactionList", sqlException);
        }
        return ledgerTransactionList;
    }

    public ArrayList getDayBook(String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList ledgerTransactionList = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        try {
            //////System.out.println("ledgerTransactionList");
            ledgerTransactionList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getDayBook", map);
            //////System.out.println("ledgerTransactionList DAO= " + ledgerTransactionList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getLedgerTransactionList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getLedgerTransactionList", sqlException);
        }
        return ledgerTransactionList;
    }

    public ArrayList getTBTransactionList(String levelId, String sDate, String eDate) {
        Map map = new HashMap();
        map.put("sDate", sDate);
        map.put("eDate", eDate);
        map.put("levelId", levelId);
        ArrayList ledgerTransactionList = new ArrayList();
        try {
            //////System.out.println("ledgerTransactionList:" + map);
            ledgerTransactionList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getTBTransactionList", map);
            //////System.out.println("ledgerTransactionList DAO= " + ledgerTransactionList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getLedgerTransactionList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getLedgerTransactionList", sqlException);
        }
        return ledgerTransactionList;
    }

    public ArrayList getLedgerTransactionListOld(String ledgerId) {
        //////System.out.println("in DAO");
        Map map = new HashMap();
        map.put("ledgerId", ledgerId);
        ArrayList ledgerTransactionList = new ArrayList();
        try {
            //////System.out.println("ledgerTransactionList");
            ledgerTransactionList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getLedgerTransactionListOLD", map);
            //////System.out.println("ledgerTransactionList DAO= " + ledgerTransactionList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getLedgerTransactionList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getLedgerTransactionList", sqlException);
        }
        return ledgerTransactionList;
    }

    public ArrayList getPaymentLedgerList() {
        //////System.out.println("in DAO");
        Map map = new HashMap();
        ArrayList paymentLedgerList = new ArrayList();
        try {
            //////System.out.println("tttttt");
            paymentLedgerList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getPaymentLedgerList", map);
//            paymentLedgerList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.financeGroupMaster", map);
            // System.out.println("dharani paymentLedgerList= " + paymentLedgerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPaymentLedgerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getPaymentLedgerList", sqlException);
        }
        return paymentLedgerList;
    }

    public ArrayList getContraPaymentLedgerList() {
        //////System.out.println("in DAO");
        Map map = new HashMap();
        ArrayList paymentLedgerList = new ArrayList();
        try {
            map.put("cashOnHandLedgerId", ThrottleConstants.CashOnHandLedgerId);
            //////System.out.println("tttttt");
            paymentLedgerList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getContraPaymentLedgerList", map);
//            paymentLedgerList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.financeGroupMaster", map);
            // System.out.println("dharani paymentLedgerList= " + paymentLedgerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPaymentLedgerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getPaymentLedgerList", sqlException);
        }
        return paymentLedgerList;
    }

    public ArrayList getBankPaymentLedgerList() {
        //////System.out.println("in DAO");
        Map map = new HashMap();
        ArrayList paymentLedgerList = new ArrayList();
        try {
            //////System.out.println("tttttt");
            paymentLedgerList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getBankPaymentLedgerList", map);
            //////System.out.println("getBankPaymentLedgerList DAO= " + paymentLedgerList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBankPaymentLedgerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getBankPaymentLedgerList", sqlException);
        }
        return paymentLedgerList;
    }

    /*   public int insertPaymentEntry(int cashDetailCode, String totalCreditAmt, int detailCode, int userId, String date, String bankid1Values, String amountValues, String accTypeValues, String narrationValues, String cashLedgerval, String searchCode, String accountType, SqlMapClient session) {
     Map map = new HashMap();
        
     String code = "";
     String delimiter = "-";
     String[] temp;
     int status = 0;
     map.put("entryStatus", "CP");
     map.put("detailCode", detailCode);
     map.put("userId", userId);
     map.put("formId", ThrottleConstants.cashPaymentFormId);
     map.put("date", date);
     map.put("bankid1Values", bankid1Values);
     map.put("totalCreditAmt", totalCreditAmt);
     String[] val = bankid1Values.split("~");
     map.put("ledgerID", val[0]);
     map.put("groupCode", val[1]);
     map.put("levelID", val[2]);
     map.put("ledgerCode", val[3]);
     map.put("amountValues", amountValues);
     map.put("accTypeValues", accTypeValues);
     map.put("narrationValues", narrationValues);

     map.put("reference", accountType);
     map.put("searchCode", searchCode);

     try {
     map.put("voucherType", "%PAYMENT%");
     code = (String) session.queryForObject("finance.getVoucerCode", map);
     temp = code.split(delimiter);

     int codeval = Integer.parseInt(temp[1]);
     int codev = codeval + 1;

     String voucherCode = "PAYMENT-" + codev;

     if (detailCode == 1) {
     map.put("voucherCode", voucherCode);
     } else {
     map.put("voucherCode", code);
     }

     int tempDetailCode = detailCode + 1;

     if (tempDetailCode == cashDetailCode) {
     //  System.out.println("map = 1 line " + map);
     status = (Integer) session.update("finance.insertPaymentEntry", map);
     map.put("detailCode", cashDetailCode);

     // System.out.println("cashLedgerval " + cashLedgerval);
     String[] val1 = cashLedgerval.split("~");
     map.put("ledgerID", val1[0]);
     map.put("groupCode", val1[1]);
     map.put("levelID", val1[2]);
     map.put("ledgerCode", val1[3]);

     map.put("amountValues", totalCreditAmt);
     map.put("accTypeValues", "CREDIT");
     //   System.out.println("map = 2 line " + map);
     status = (Integer) session.update("finance.insertPaymentEntry", map);
     } else {
     //  status = (Integer) getSqlMapClientTemplate().update("finance.insertPaymentEntry", map);
     }
     //System.out.println("status in DAO= " + status);
     } catch (Exception sqlException) {
           
     FPLogUtils.fpDebugLog("insertPaymentEntry Error" + sqlException.toString());
     FPLogUtils.fpErrorLog("sqlException" + sqlException);
     throw new FPRuntimeException("EM-SYS-01", CLASS, "insertPaymentEntry", sqlException);
     }
     return status;
     }

     public int insertBankPaymentEntry(String chequeDate, String chequeNo, String BankHead, int cashDetailCode, String totalCreditAmt, int detailCode, int userId, String date, String bankid1Values, String amountValues, String accTypeValues, String narrationValues, SqlMapClient session) {
     Map map = new HashMap();
     String code = "";
     String empid = "";
     String delimiter = "-";
     String[] temp;
     int status = 0;
     map.put("entryStatus", "BP");
     map.put("chequeDate", chequeDate);
     map.put("chequeNo", chequeNo);
     map.put("BankHead", BankHead);
     map.put("detailCode", detailCode);
     map.put("userId", userId);
     map.put("date", date);
     map.put("bankid1Values", bankid1Values);
     map.put("totalCreditAmt", totalCreditAmt);
     String[] val = bankid1Values.split("~");
     map.put("ledgerID", val[0]);
     map.put("groupCode", val[1]);
     map.put("levelID", val[2]);
     map.put("ledgerCode", val[3]);
     map.put("amountValues", amountValues);
     map.put("accTypeValues", accTypeValues);
     map.put("narrationValues", narrationValues);

     try {
     empid = (String) session.queryForObject("finance.getEmployeeId", map);

     map.put("reference", "Employee");
     map.put("searchCode", empid);
     map.put("voucherType", "%PAYMENT%");
     code = (String) session.queryForObject("finance.getVoucerCode", map);
     temp = code.split(delimiter);
     //////System.out.println("temp[1] = " + temp[1]);
     int codeval = Integer.parseInt(temp[1]);
     int codev = codeval + 1;
     //////System.out.println("codev = " + codev);
     String voucherCode = "PAYMENT-" + codev;
     //////System.out.println("voucherCode = " + voucherCode);

     if (detailCode == 1) {
     map.put("voucherCode", voucherCode);
     } else {
     map.put("voucherCode", code);
     }
     int tempDetailCode = detailCode + 1;
     if (tempDetailCode == cashDetailCode) {
     status = (Integer) session.update("finance.insertPaymentEntry", map);
     String[] LedgerVal = BankHead.split("~");
     map.put("ledgerID", LedgerVal[0]);
     map.put("groupCode", LedgerVal[1]);
     map.put("levelID", LedgerVal[2]);
     map.put("ledgerCode", LedgerVal[3]);
     map.put("detailCode", cashDetailCode);
     map.put("amountValues", totalCreditAmt);
     map.put("accTypeValues", "CREDIT");
     //////System.out.println("map = 1 line " + map);
     status = (Integer) session.update("finance.insertPaymentEntry", map);
     } else {
     //////System.out.println("map = 2 line " + map);
     status = (Integer) session.update("finance.insertPaymentEntry", map);
     }
     //////System.out.println("status in DAO= " + status);
     } catch (Exception sqlException) {
            
     FPLogUtils.fpDebugLog("insertPaymentEntry Error" + sqlException.toString());
     FPLogUtils.fpErrorLog("sqlException" + sqlException);
     throw new FPRuntimeException("EM-SYS-01", CLASS, "insertPaymentEntry", sqlException);
     }
     return status;
     }

    

     public int insertReceiptEntry(String totalDebitAmt, int cashDetailCode, int detailCode, int userId, String date, String bankid1Values, String amountValues, String accTypeValues, String narrationValues, String cashLedgerval) {
     Map map = new HashMap();
      
     String code = "";
     String delimiter = "-";
     String[] temp;
     int status = 0;
     map.put("entryStatus", "CR");
     map.put("detailCode", detailCode);
     map.put("userId", userId);
     map.put("date", date);
     map.put("bankid1Values", bankid1Values);
     map.put("totalDebitAmt", totalDebitAmt);
     //////System.out.println("bankid1Values = " + bankid1Values);
     String[] val = bankid1Values.split("~");
     map.put("ledgerID", val[0]);
     map.put("groupCode", val[1]);
     map.put("levelID", val[2]);
     map.put("ledgerCode", val[3]);

     map.put("amountValues", amountValues);
     map.put("accTypeValues", accTypeValues);
     map.put("narrationValues", narrationValues);
     map.put("voucherType", "%RECEIPT%");

     code = (String) getSqlMapClientTemplate().queryForObject("finance.getVoucerCode", map);
     temp = code.split(delimiter);
     //////System.out.println("temp[1] = " + temp[1]);
     int codeval = Integer.parseInt(temp[1]);
     int codev = codeval + 1;
     //////System.out.println("codev = " + codev);
     String voucherCode = "RECEIPT-" + codev;
     //////System.out.println("voucherCode = " + voucherCode);

     if (detailCode == 1) {
     map.put("voucherCode", voucherCode);
     } else {
     map.put("voucherCode", code);
     }

     //////System.out.println("insert journal in DAO...");
     //////System.out.println("userId" + userId);
     //////System.out.println("date" + date);
     //////System.out.println("bankid1Values" + bankid1Values);
     //////System.out.println("amountValues" + amountValues);
     //////System.out.println("accTypeValues" + accTypeValues);
     //////System.out.println("narrationValues" + narrationValues);
     //////System.out.println("map = " + map);
     try {

     int tempDetailCode = detailCode + 1;
     //////System.out.println("tempDetailCode = " + tempDetailCode);
     if (tempDetailCode == cashDetailCode) {
     status = (Integer) getSqlMapClientTemplate().update("finance.insertReceiptEntry", map);

     map.put("detailCode", cashDetailCode);

     String[] val1 = cashLedgerval.split("~");
     map.put("ledgerID", val1[0]);
     map.put("groupCode", val1[1]);
     map.put("levelID", val1[2]);
     map.put("ledgerCode", val1[3]);

     map.put("amountValues", totalDebitAmt);
     map.put("accTypeValues", "DEBIT");

     //////System.out.println("map = 1 line " + map);
     status = (Integer) getSqlMapClientTemplate().update("finance.insertReceiptEntry", map);
     } else {
     //////System.out.println("map = 2 line " + map);
     status = (Integer) getSqlMapClientTemplate().update("finance.insertReceiptEntry", map);
     }
     //////System.out.println("status in DAO= " + status);

     // status = (Integer) getSqlMapClientTemplate().update("finance.insertReceiptEntry", map);
     // //////System.out.println("status in DAO= " + status);
     } catch (Exception sqlException) {
     FPLogUtils.fpDebugLog("insertReceiptEntry Error" + sqlException.toString());
     FPLogUtils.fpErrorLog("sqlException" + sqlException);
     throw new FPRuntimeException("EM-SYS-01", CLASS, "insertReceiptEntry", sqlException);
     }
     return status;
     }

     public int insertBankReceiptEntry(String chequeDate, String chequeNo, String BankHead, int cashDetailCode, String totalDebitAmt, int detailCode, int userId, String date, String bankid1Values, String amountValues, String accTypeValues, String narrationValues) {
     Map map = new HashMap();
     String code = "";
     String delimiter = "-";
     String[] temp;
     int status = 0;
     map.put("entryStatus", "BR");
     map.put("chequeDate", chequeDate);
     map.put("chequeNo", chequeNo);
     map.put("BankHead", BankHead);
     map.put("detailCode", detailCode);
     map.put("userId", userId);
     map.put("date", date);
     map.put("bankid1Values", bankid1Values);
     String[] val = bankid1Values.split("~");
     map.put("ledgerID", val[0]);
     map.put("groupCode", val[1]);
     map.put("levelID", val[2]);
     map.put("ledgerCode", val[3]);
     map.put("amountValues", amountValues);
     map.put("accTypeValues", accTypeValues);
     map.put("narrationValues", narrationValues);
     map.put("voucherType", "%RECEIPT%");
     code = (String) getSqlMapClientTemplate().queryForObject("finance.getVoucerCode", map);
     temp = code.split(delimiter);
     //////System.out.println("temp[1] = " + temp[1]);
     int codeval = Integer.parseInt(temp[1]);
     int codev = codeval + 1;
     //////System.out.println("codev = " + codev);
     String voucherCode = "RECEIPT-" + codev;
     //////System.out.println("voucherCode = " + voucherCode);
     if (detailCode == 1) {
     map.put("voucherCode", voucherCode);
     } else {
     map.put("voucherCode", code);
     }
     //////System.out.println("insert RECEIPT in DAO...");
     //////System.out.println("userId" + userId);
     //////System.out.println("date" + date);
     //////System.out.println("bankid1Values" + bankid1Values);
     //////System.out.println("amountValues" + amountValues);
     //////System.out.println("accTypeValues" + accTypeValues);
     //////System.out.println("narrationValues" + narrationValues);
     //////System.out.println("map = " + map);
     try {
     int tempDetailCode = detailCode + 1;
     //////System.out.println("tempDetailCode = " + tempDetailCode);
     if (tempDetailCode == cashDetailCode) {
     //                status = (Integer) getSqlMapClientTemplate().update("finance.insertPaymentEntry", map);
     status = (Integer) getSqlMapClientTemplate().update("finance.insertReceiptEntry", map);
     String[] LedgerVal = BankHead.split("~");
     map.put("ledgerID", LedgerVal[0]);
     map.put("groupCode", LedgerVal[1]);
     map.put("levelID", LedgerVal[2]);
     map.put("ledgerCode", LedgerVal[3]);
     map.put("detailCode", cashDetailCode);
     map.put("amountValues", totalDebitAmt);
     map.put("accTypeValues", "DEBIT");
     //////System.out.println("map = 1 line " + map);
     //                status = (Integer) getSqlMapClientTemplate().update("finance.insertPaymentEntry", map);
     status = (Integer) getSqlMapClientTemplate().update("finance.insertReceiptEntry", map);
     } else {
     //////System.out.println("map = 2 line " + map);
     //                status = (Integer) getSqlMapClientTemplate().update("finance.insertPaymentEntry", map);
     status = (Integer) getSqlMapClientTemplate().update("finance.insertReceiptEntry", map);
     }
     //////System.out.println("status in DAO= " + status);
     } catch (Exception sqlException) {
     FPLogUtils.fpDebugLog("insertReceiptEntry Error" + sqlException.toString());
     FPLogUtils.fpErrorLog("sqlException" + sqlException);
     throw new FPRuntimeException("EM-SYS-01", CLASS, "insertReceiptEntry", sqlException);
     }
     return status;
     }
     */
    public ArrayList receiptEntry() {
        //////System.out.println("in DAO");
        Map map = new HashMap();
        ArrayList receiptLedgerList = new ArrayList();
        try {
            //////System.out.println("tttttt");
            receiptLedgerList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getReceiptEntry", map);
            //////System.out.println("getReceiptEntry DAO= " + receiptLedgerList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getReceiptEntry Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getReceiptEntry", sqlException);
        }
        return receiptLedgerList;
    }

    public ArrayList getInvoiceList(FinanceTO financeTO) {
        Map map = new HashMap();
        ArrayList invoiceList = new ArrayList();
        try {
            map.put("customerId", financeTO.getCustomerId());
            //////System.out.println("getInvoiceList map= " + map);
            invoiceList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getCustomerInvoiceList", map);
            //////System.out.println("getInvoiceList DAO= " + invoiceList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getInvoiceList", sqlException);
        }
        return invoiceList;
    }

    public ArrayList getPurchaseList(FinanceTO financeTO) {
        Map map = new HashMap();
        ArrayList invoiceList = new ArrayList();
        try {
            map.put("vendorId", financeTO.getVendorId());
//            map.put("vendorType", financeTO.getVendorTypeId());
            System.out.println("getPurchaseList map= " + map);
//            if ("1001".equals(financeTO.getVendorTypeId())) {//leasing vendor
//                System.out.println("if ..............leasing vendor");
//                invoiceList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getVendorLPList", map);
//            } else {
//                System.out.println("else.......................");
//                invoiceList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getVendorPOList", map);
//            }
            invoiceList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getVendorPaymentList", map);
            System.out.println("getPurchaseList DAO= " + invoiceList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getInvoiceList", sqlException);
        }
        return invoiceList;
    }

    public ArrayList getPendingPurchaseDetails(FinanceTO financeTO) {
        Map map = new HashMap();
        ArrayList invoiceList = new ArrayList();
        try {
            map.put("vendorId", financeTO.getVendorId());
            map.put("fromDate", financeTO.getFromDate());
            map.put("toDate", financeTO.getToDate());
            System.out.println("getPendingPurchaseDetails map= " + map);
            invoiceList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getPendingPurchaseDetails", map);
            System.out.println("getPendingPurchaseDetails DAO= " + invoiceList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPendingPurchaseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getPendingPurchaseDetails", sqlException);
        }
        return invoiceList;
    }

    public ArrayList getInvoiceDetails(FinanceTO financeTO) {
        Map map = new HashMap();
        ArrayList invoiceDetails = new ArrayList();
        try {
            map.put("invoiceId", financeTO.getInvoiceId());
            //////System.out.println("invoiceDetails map= " + map);
            invoiceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getCustomerInvoiceDetails", map);
            //////System.out.println("invoiceDetails DAO= " + invoiceDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("invoiceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "invoiceDetails", sqlException);
        }
        return invoiceDetails;
    }

    public ArrayList getInvoiceGRList(FinanceTO financeTO) {
        Map map = new HashMap();
        ArrayList invoiceDetails = new ArrayList();
        try {
            map.put("invoiceId", financeTO.getInvoiceId());
            System.out.println("invoiceDetails map= " + map);
            invoiceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getInvoiceGRList", map);
            System.out.println("invoiceDetails DAO= " + invoiceDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("invoiceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "invoiceDetails", sqlException);
        }
        return invoiceDetails;
    }

    public ArrayList getInvoiceDetailsList(FinanceTO financeTO) {
        Map map = new HashMap();
        ArrayList invoiceDetails = new ArrayList();
        try {
            map.put("invoiceId", financeTO.getInvoiceId());
            System.out.println("invoiceDetails map= " + map);
            invoiceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getInvoiceDetailsList", map);
            System.out.println("invoiceDetails DAO= " + invoiceDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("invoiceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "invoiceDetails", sqlException);
        }
        return invoiceDetails;
    }

    public int saveInvoicePaymentDetails(FinanceTO financeTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        int invoiceReceipt = 0;
        int invoiceDetail = 0;
        try {

            map.put("customerId", Integer.parseInt(financeTO.getCustomerId()));
            map.put("receiptAmount", financeTO.getReceiptAmount());
            map.put("invoiceId", financeTO.getInvoiceId());
            map.put("receiptDate", financeTO.getReceiptDate());
            map.put("referenceNo", financeTO.getReferenceNo());
            map.put("payAmount", financeTO.getPayAmount());
            map.put("paymentMode", Integer.parseInt(financeTO.getPaymentMode()));
            map.put("bankId", financeTO.getBankId());
            map.put("bankBranchId", financeTO.getBranchId());
            map.put("userId", userId);
            System.out.println("map saveInvoicePaymentDetails " + map);
            invoiceReceipt = (Integer) session.insert("finance.saveInvoiceReceiptPayment", map);
            System.out.println("map saveInvoicePaymentDetails invoiceReceipt " + invoiceReceipt);
            int bankPaymentId = 0;
            map.put("formReferenceId", ThrottleConstants.invoiceReceiptFormId);
            map.put("creditLedgerId", financeTO.getCustomerLedgerId());
            map.put("debitLedgerId", financeTO.getBankLedgerId());
            map.put("reference", "invoiceReceipt");
            map.put("referenceId", invoiceReceipt);
            map.put("bankEntryType", "RECEIPT");
            map.put("formRefernceCode", ThrottleConstants.invoiceReceiptformCode + Integer.toString(invoiceReceipt));
            bankPaymentId = (Integer) session.insert("finance.insertBankClearanceDetails", map);

            String[] invoiceIds = financeTO.getInvoiceIds();
            String[] paidAmounts = financeTO.getPaidAmounts();
            String[] invoiceCodes = financeTO.getInvoiceCodes();
            String narration[] = financeTO.getNarration();
            for (int i = 0; i < invoiceIds.length; i++) {
                map.put("invoiceReceipt", invoiceReceipt);
                map.put("payAmount", paidAmounts[i]);
                map.put("invoiceId", invoiceIds[i]);
                map.put("invoiceCode", invoiceCodes[i]);
                System.out.println("map savePurchasePaymentDetail " + map);
                invoiceDetail = (Integer) session.update("finance.saveInvoicePaymentDetail", map);
//                invoiceDetail = (Integer) session.update("finance.savePurchasePaymentDetail", map);
            }
            /*pavi
             //account entry start
             //  --------------------------------- acc 1st row start --------------------------
             map.put("userId", userId);
             map.put("DetailCode", "1");
             map.put("voucherType", "%RECEIPT%");
             String code2 = (String) session.queryForObject("finance.getVoucerCode", map);
             String[] temp = code2.split("-");
             int codeval2 = Integer.parseInt(temp[1]);
             int codev2 = codeval2 + 1;
             String voucherCode = "RECEIPT-" + codev2;
             //////System.out.println("voucherCode = " + voucherCode);
             map.put("voucherCode", voucherCode);
             map.put("mainEntryType", "VOUCHER");
             map.put("entryType", "RECEIPT");

             map.put("ledgerId", financeTO.getCustomerLedgerId());
             map.put("particularsId", financeTO.getCustomerLedgerId());

             map.put("amount", financeTO.getReceiptAmount());
             map.put("Accounts_Type", "CREDIT");
             map.put("Remark", "Invoice Receipts");
             map.put("Reference", financeTO.getInvoiceId());
             map.put("SearchCode", financeTO.getInvoiceId());
             System.out.println("map1 =---------------------> " + map);
             int status1 = (Integer) session.update("finance.insertFinanceAccountEntry", map);
             //////System.out.println("status1 = " + status1);
             //account entry end
             //--------------------------------- acc 2nd row start --------------------------
             if (status1 > 0) {
             map.put("DetailCode", "2");
             map.put("ledgerId", financeTO.getBankLedgerId());
             map.put("particularsId", financeTO.getBankLedgerId());
             map.put("Accounts_Type", "DEBIT");
             System.out.println("map2 =---------------------> " + map);
             int status2 = (Integer) session.update("finance.insertFinanceAccountEntry", map);
             //////System.out.println("status2 = " + status2);
             }
             //--------------------------------- acc 2nd row end --------------------------
             */
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertReceiptEntry Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertReceiptEntry", sqlException);
        }
        return invoiceReceipt;
    }

    public int savePurchasePaymentDetails(FinanceTO financeTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        int invoicePayment = 0;
        int invoiceDetail = 0;
        try {

            map.put("vendorId", financeTO.getVendorId());
            map.put("receiptAmount", financeTO.getReceiptAmount());
            map.put("receiptDate", financeTO.getReceiptDate());
            map.put("referenceNo", financeTO.getReferenceNo());
            map.put("paymentMode", Integer.parseInt(financeTO.getPaymentMode()));
            map.put("bankId", financeTO.getBankId());
            map.put("bankBranchId", financeTO.getBranchId());
            map.put("userId", userId);
            System.out.println("map savePurchasePaymentMaster " + map);
            invoicePayment = (Integer) session.insert("finance.savePurchasePaymentMaster", map);

            int bankPaymentId = 0;
            map.put("formReferenceId", ThrottleConstants.invoicePaymentFormId);
            map.put("creditLedgerId", financeTO.getBankLedgerId());
            map.put("debitLedgerId", financeTO.getVendorLedgerId());
            map.put("reference", "invoicePayment");
            map.put("referenceId", invoicePayment);
            map.put("bankEntryType", "PAYMENT");
            map.put("formRefernceCode", ThrottleConstants.invoicePaymentFormCode + Integer.toString(invoicePayment));
            System.out.println("insertBankClearanceDetails" + map);
            bankPaymentId = (Integer) session.insert("finance.insertBankClearanceDetails", map);

            String[] invoiceIds = financeTO.getInvoiceIds();
            String[] paidAmounts = financeTO.getPaidAmounts();
            String[] invoiceCodes = financeTO.getInvoiceCodes();
            String narration[] = financeTO.getNarration();
            for (int i = 0; i < invoiceIds.length; i++) {
                map.put("invoiceReceipt", invoicePayment);
                map.put("payAmount", paidAmounts[i]);
                map.put("invoiceId", invoiceIds[i]);
                map.put("invoiceCode", invoiceCodes[i]);
                System.out.println("map savePurchasePaymentDetail " + map);
                invoiceDetail = (Integer) session.update("finance.savePurchasePaymentDetail", map);
            }

            //////

            /*pavi //account entry start
             //  --------------------------------- acc 1st row start --------------------------
             map.put("userId", userId);
             map.put("DetailCode", "1");
             map.put("voucherType", "%PAYMENT%");
             String code2 = (String) getSqlMapClientTemplate().queryForObject("finance.getVoucerCode", map);
             String[] temp = code2.split("-");
             int codeval2 = Integer.parseInt(temp[1]);
             int codev2 = codeval2 + 1;
             String voucherCode = "PAYMENT-" + codev2;
             //////System.out.println("voucherCode = " + voucherCode);
             map.put("voucherCode", voucherCode);
             map.put("mainEntryType", "VOUCHER");
             map.put("entryType", "PAYMENT");

             map.put("ledgerId", financeTO.getVendorLedgerId());
             map.put("particularsId", financeTO.getVendorLedgerId());

             map.put("amount", financeTO.getReceiptAmount());
             map.put("Accounts_Type", "DEBIT");
             if ("1001".equals(financeTO.getVendorTypeId())) {
             map.put("Remark", "Leasing Payments");
             } else {
             map.put("Remark", "Purchase Payments");
             }
             map.put("Reference", financeTO.getInvoiceId());
             map.put("SearchCode", financeTO.getInvoiceId());
             //////System.out.println("map1 =---------------------> " + map);
             int status1 = (Integer) getSqlMapClientTemplate().update("finance.insertFinanceAccountEntry", map);
             //////System.out.println("status1 = " + status1);
             //account entry end
             //--------------------------------- acc 2nd row start --------------------------
             if (status1 > 0) {
             map.put("DetailCode", "2");
             map.put("ledgerId", financeTO.getBankLedgerId());
             map.put("particularsId", financeTO.getBankLedgerId());
             map.put("Accounts_Type", "CREDIT");
             //////System.out.println("map2 =---------------------> " + map);
             int status2 = (Integer) getSqlMapClientTemplate().update("finance.insertFinanceAccountEntry", map);
             //////System.out.println("status2 = " + status2);
             }
             //--------------------------------- acc 2nd row end --------------------------
             */
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertReceiptEntry Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertReceiptEntry", sqlException);
        }
        return invoicePayment;
    }

    /*
    public int saveInvoicePayment(FinanceTO financeTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int invoiceReceipt = 0;
        int invoiceDetail = 0;
        try {

            map.put("invoiceReceipt", financeTO.getInvoiceDetailId());
            map.put("invoiceId", financeTO.getInvoiceId());
            map.put("payAmount", financeTO.getPayAmount());
            map.put("userId", userId);
            //////System.out.println("map saveInvoicePaymentDetails " + map);
            invoiceReceipt += (Integer) session.update("finance.saveInvoicePaymentDetail", map);

        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog("saveInvoicePaymentDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveInvoicePaymentDetail", sqlException);
        }
        return invoiceReceipt;
    }

    public int savePurchasePayment(FinanceTO financeTO, int userId) {
        Map map = new HashMap();
        int invoiceReceipt = 0;
        int invoiceDetail = 0;
        try {

            map.put("invoiceReceipt", financeTO.getInvoiceDetailId());
            map.put("payAmount", financeTO.getPayAmount());
            map.put("userId", userId);
            map.put("userId", financeTO.getVendorTypeId());
            if ("1001".equals(financeTO.getVendorTypeId())) {
                map.put("vpId", financeTO.getInvoiceId());
                map.put("poId", 0);
            } else {
                map.put("poId", financeTO.getInvoiceId());
                map.put("vpId", 0);
            }
            //////System.out.println("map savePurchasePaymentDetail " + map);
            invoiceReceipt = (Integer) getSqlMapClientTemplate().update("finance.savePurchasePaymentDetail", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("insertReceiptEntry Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertReceiptEntry", sqlException);
        }
        return invoiceReceipt;
    }
     */
    public int saveDebitNote(FinanceTO financeTO, int userId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        int invoiceReceipt = 0;
        int update = 0;
        int status = 0;
        int formId = 0;
        int voucherNo = 0;
        try {

            map.put("invoiceId", financeTO.getInvoiceId());
            map.put("userId", userId);
            map.put("vendorId", financeTO.getVendorId());
            map.put("remarks", financeTO.getRemarks());
            map.put("receiptDate", financeTO.getReceiptDate());
            map.put("amount", financeTO.getPayAmount());
//            map.put("referenceId", financeTO.getr());

            //  System.out.println("map saveDebitNote " + map);
            String vendorLedgerId = (String) getSqlMapClientTemplate().queryForObject("finance.getVendorLedgerId", map);
            String vendorLedgerCode = (String) getSqlMapClientTemplate().queryForObject("finance.getVendorLedgerCode", map);

//            invoiceReceipt = (Integer) getSqlMapClientTemplate().update("finance.updateSupplyMaster", map);
            //   System.out.println("status2 = " + status2);
            update = (Integer) getSqlMapClientTemplate().update("finance.updatePaymentDetails", map);

            //     System.out.println("status final = " + status);
            formId = Integer.parseInt(ThrottleConstants.cashPaymentFormId);
            map.put("formId", formId);
            voucherNo = (Integer) getSqlMapClientTemplate().insert("finance.getFormVoucherNo", map);
            map.put("voucherNo", voucherNo);
            map.put("voucherCodeNo", ThrottleConstants.CashPaymentVoucherCode + voucherNo);
            map.put("voucherType", "%PAYMENT%");
            String code = "";
            String[] temp;
            code = (String) getSqlMapClientTemplate().queryForObject("finance.getVoucerCode", map);
            temp = code.split("-");
            int codeval2 = Integer.parseInt(temp[1]);
            int codev2 = codeval2 + 1;
            String voucherCode = "PAYMENT-" + codev2;
            System.out.println("voucherCode = " + voucherCode);

            map.put("detailCode", "1");
            map.put("voucherCode", voucherCode);
            map.put("accountEntryDate", financeTO.getReceiptDate());
            map.put("mainEntryType", "VOUCHER");
            map.put("entryType", "PAYMENT");
            map.put("ledgerId", vendorLedgerId);
            map.put("particularsId", vendorLedgerCode);
            map.put("amount", financeTO.getPayAmount());
            map.put("accountsType", "DEBIT");
            map.put("narration", financeTO.getRemarks());
            map.put("searchCode", financeTO.getInvoiceCode());
            map.put("reference", "DEBITNOTE");

            System.out.println("map1 updateClearnceDate=---------------------> " + map);
            status = (Integer) getSqlMapClientTemplate().update("finance.insertAccountEntry", map);
            System.out.println("status1 = " + status);
            //--------------------------------- acc 2nd row start --------------------------
            if (status > 0) {
                map.put("detailCode", "2");
                map.put("ledgerId", ThrottleConstants.CashOnHandLedgerId);
                map.put("particularsId", ThrottleConstants.CashOnHandLedgerCode);
                map.put("accountsType", "CREDIT");
                System.out.println("map2 updateClearnceDate=---------------------> " + map);
                status = (Integer) getSqlMapClientTemplate().update("finance.insertAccountEntry", map);
                System.out.println("status2 = " + status);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveDebitNote Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveDebitNote", sqlException);
        }
        return update;
    }

    public String saveCreditNote(FinanceTO financeTO, String[] tripIds, String[] freightAmount, String[] grandTotal, String[] payAmount, String reason, String[] tollCredit, String[] greenTaxCredit, String[] weightmentCredit, String[] detaintionCredit, String[] otherCredit, int userId, SqlMapClient session, String finYear) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int invoiceReceipt = 0;
        int status = 0;
        int formId = 0;
        int voucherNo = 0;
        String creditNoteCode = "";
        String fromDate = "";
        String invoiceCode = "";
        String currentYear = "";
        String InvoiceDate = "";
        String compareDate = "31-03-2018";
        int creditNoteNumber = 0;
        try {
            synchronized(this){
            InvoiceDate = financeTO.getInvoiceDate();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date d1 = sdf.parse(compareDate);
            Date d2 = sdf.parse(InvoiceDate);

//            creditNoteNumber = (Integer) getSqlMapClientTemplate().insert("finance.getCreditNoteCode1819", map);
//            creditNoteNumber = (Integer) session.insert("finance.getCreditNoteCode1819", map);
//            creditNoteCode = "CR1819TPG";
            //if (d2.after(d1)) { // After marc 31st 2020
            System.out.println("finYear------" + finYear);
            if (!"Y".equals(financeTO.getGstType())) {

                if (finYear.equalsIgnoreCase("2324")) { // After marc 31st 2020
                    Calendar now = Calendar.getInstance();
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = new Date();
                    System.out.println("1111111111111 ");
                    fromDate = dateFormat.format(date);
                    
                    creditNoteNumber = (Integer) session.insert("finance.getCreditNoteCodeBos2324", map);
                    
                    creditNoteCode = "CR2324TBS";
                } else { //Before
                    System.out.println("22222222 ");
                    fromDate = "2023-03-31 22:00:00";
                    creditNoteNumber = (Integer) session.insert("finance.getCreditNoteCodeBos2223", map);
                    creditNoteCode = "CR2223TBS";
                }

            } else {

                if (finYear.equalsIgnoreCase("2324")) { // After marc 31st 2020
                    Calendar now = Calendar.getInstance();
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = new Date();
                    System.out.println("1111111111111 ");
                    fromDate = dateFormat.format(date);
                    creditNoteNumber = (Integer) session.insert("finance.getCreditNoteCode2324", map);
                    creditNoteCode = "CR2324TPG";
                } else { //Before
                    System.out.println("22222222 ");
                    fromDate = "2023-03-31 22:00:00";
                    creditNoteNumber = (Integer) session.insert("finance.getCreditNoteCode2223", map);
                    creditNoteCode = "CR2223TPG";
                }

            }

            map.put("amount", financeTO.getPayAmount());
            map.put("customer", financeTO.getCustomerId());
            map.put("invoiceId", financeTO.getInvoiceId());
            map.put("totalInvoiceAmount", financeTO.getInvoiceAmount());

            map.put("remarks", financeTO.getRemarks());
            map.put("gstType", financeTO.getGstType());
            map.put("reason", reason);
            map.put("userId", userId);

            //getCredit NOte Code
            invoiceCode = creditNoteNumber + "";
            if (invoiceCode.length() == 1) {
                invoiceCode = "000000" + invoiceCode;
                System.out.println("invoice no 1.." + invoiceCode);
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "00000" + invoiceCode;
                System.out.println("invoice lenght 2.." + invoiceCode);
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 4) {
                invoiceCode = "000" + invoiceCode;
            } else if (invoiceCode.length() == 5) {
                invoiceCode = "00" + invoiceCode;
            } else if (invoiceCode.length() == 6) {
                invoiceCode = "0" + invoiceCode;
            }
            creditNoteCode = creditNoteCode + invoiceCode;
            System.out.println("getInvoiceCodeSequence=" + creditNoteCode);

            System.out.println("creditNoteCode:" + creditNoteCode);
            map.put("creditNoteCode", creditNoteCode);
            map.put("fromDate", fromDate);
            System.out.println("creditNote Map:" + map);
            int insertCreditNote = (Integer) session.insert("finance.insertInvoiceCreditNote", map);
//            int insertCreditNote = (Integer) getSqlMapClientTemplate().insert("finance.insertInvoiceCreditNote", map);
//            invoiceReceipt = (Integer) getSqlMapClientTemplate().update("finance.updateInvoiceHeader", map);
            invoiceReceipt = (Integer) session.update("finance.updateInvoiceHeader", map);
            if (insertCreditNote > 0) {
                for (int i = 0; i < tripIds.length; i++) {
                    map.put("freightAmount", freightAmount[i]);
                    map.put("tripId", tripIds[i]);
                    map.put("toll", tollCredit[i]);
                    map.put("greenTax", "0");
//                   map.put("greenTax",greenTaxCredit[i]);
                    map.put("detaintion", detaintionCredit[i]);
                    map.put("weightment", weightmentCredit[i]);
                    map.put("other", otherCredit[i]);
                    if ("".equalsIgnoreCase(payAmount[i]) || payAmount[i] == null) {
                        payAmount[i] = "0.00";
                    }
                    map.put("creditAmount", payAmount[i]);
                    map.put("creditNoteId", insertCreditNote);
                    System.out.println("insertCreditNoteDetails map:" + map);
//                    int insertCreditNoteDetails = (Integer) getSqlMapClientTemplate().update("finance.insertInvoiceCreditNoteDetails", map);
                    int insertCreditNoteDetails = (Integer) session.update("finance.insertInvoiceCreditNoteDetails", map);
                    System.out.println("insertCreditNoteDetails---" + insertCreditNoteDetails);
                }
            }

            // CREDIT NOTE OUTSTANDING CREDIT LIMIT
            map.put("customerId", financeTO.getCustomerId());
            map.put("invoiceId", financeTO.getInvoiceId());
            map.put("invoiceAmount", financeTO.getPayAmount());

            map.put("consignmentOrderId", "0");
            map.put("custType", "1");
            map.put("outstandingAmount", financeTO.getPayAmount());
            map.put("totalFreightAmount", financeTO.getPayAmount());
            map.put("creditNoteInvoiceId", insertCreditNote);
            map.put("supplymentryInvoiceId", "0");
            map.put("paymentId", "0");

            map.put("userId", userId);
            System.out.println("updateInvoiceOustanding----" + map);

            int updateCnoteCount, updateOutstanding, updateCreditLimit, updateUsedLimit, updateOutstandingLog = 0;
            String getCustomerType = (String) session.queryForObject("operation.getCustomerType", map);
            String availBlockedAmount = "";
            String availBlockedAmountTemp[] = null;
            map.put("custType", getCustomerType);
            map.put("transactionName", "CreditNote");

            if ("1".equalsIgnoreCase(getCustomerType)) {   //for credit Customers Only
                availBlockedAmount = (String) session.queryForObject("operation.getAvailandBlockedAmount", map);
                System.out.println("availBlockedAmount------" + availBlockedAmount);
                availBlockedAmountTemp = availBlockedAmount.split("~");
                map.put("fixedLimit", availBlockedAmountTemp[0]);
                map.put("blockedAmount", availBlockedAmountTemp[2]);

                double usedLimit = Double.parseDouble(availBlockedAmountTemp[3]) - Double.parseDouble(financeTO.getPayAmount());
                double availLimit = Double.parseDouble(availBlockedAmountTemp[1]) + Double.parseDouble(financeTO.getPayAmount());

                map.put("availAmount", availLimit);

                if (usedLimit < 0) {
                    map.put("usedLimit", 0);
                } else {
                    map.put("usedLimit", availBlockedAmountTemp[3]);
                }

                updateCreditLimit = (Integer) session.update("operation.updateCreditNoteInvoiceLimit", map);

                map.put("transactionType", "Credit");   // credit
                map.put("debitAmount", "0.00");
                map.put("creditAmount", financeTO.getPayAmount());
                updateOutstandingLog = (Integer) session.update("operation.updateOutstandingLog", map);

            } else if ("2".equalsIgnoreCase(getCustomerType)) {
                availBlockedAmount = (String) session.queryForObject("operation.getAvailandBlockedAmount", map);
                System.out.println("availBlockedAmount------" + availBlockedAmount);
                availBlockedAmountTemp = availBlockedAmount.split("~");
                map.put("fixedLimit", availBlockedAmountTemp[0]);
                map.put("blockedAmount", availBlockedAmountTemp[2]);

                double usedLimit = Double.parseDouble(availBlockedAmountTemp[3]) - Double.parseDouble(financeTO.getPayAmount());
                double availLimit = Double.parseDouble(availBlockedAmountTemp[1]) + Double.parseDouble(financeTO.getPayAmount());

                map.put("availAmount", availLimit);

                if (usedLimit < 0) {
                    map.put("usedLimit", 0);
                } else {
                    map.put("usedLimit", availBlockedAmountTemp[3]);
                }

                updateCreditLimit = (Integer) session.update("operation.updateCreditNoteInvoiceLimit", map);

                map.put("transactionType", "Credit");   // credit
                map.put("debitAmount", "0.00");
                map.put("creditAmount", financeTO.getPayAmount());
                updateOutstandingLog = (Integer) session.update("operation.updateOutstandingLog", map);
            }

            // CREDIT NOTE OUTSTANDING CREDIT LIMIT      
//            String customerLedgerId = (String) getSqlMapClientTemplate().queryForObject("finance.getCustomerLedgerId", map);
//            String customerLedgerCode = (String) getSqlMapClientTemplate().queryForObject("finance.getCustomerLedgerCode", map);
            String customerLedgerId = (String) session.queryForObject("finance.getCustomerLedgerId", map);
            String customerLedgerCode = (String) session.queryForObject("finance.getCustomerLedgerCode", map);

//            invoiceReceipt = (Integer) getSqlMapClientTemplate().update("finance.updateSupplyMaster", map);
            //   System.out.println("status2 = " + status2);
            System.out.println("status final = " + status);
            System.out.println("ThrottleConstants.cashPaymentFormId = " + ThrottleConstants.cashPaymentFormId);
            formId = Integer.parseInt(ThrottleConstants.cashPaymentFormId);
            map.put("formId", formId);
//            voucherNo = (Integer) getSqlMapClientTemplate().insert("finance.getFormVoucherNo", map);
            voucherNo = (Integer) session.insert("finance.getFormVoucherNo", map);
            map.put("voucherNo", voucherNo);
            map.put("voucherCodeNo", ThrottleConstants.CashPaymentVoucherCode + voucherNo);
            map.put("voucherType", "%PAYMENT%");
            String code = "";
            String[] temp;
//            code = (String) getSqlMapClientTemplate().queryForObject("finance.getVoucerCode", map);
            code = (String) session.queryForObject("finance.getVoucerCode", map);
            System.out.println("code----" + code);
            int codeval2 = 0;
            if (code == null) {
                codeval2 = 0;
            } else {
                temp = code.split("-");
                codeval2 = Integer.parseInt(temp[1]);
            }
            int codev2 = codeval2 + 1;
            String voucherCode = "PAYMENT-" + codev2;
            System.out.println("voucherCode = " + voucherCode);

            map.put("detailCode", "1");
            map.put("voucherCode", voucherCode);
            map.put("accountEntryDate", financeTO.getReceiptDate());
            map.put("mainEntryType", "VOUCHER");
            map.put("entryType", "PAYMENT");
            map.put("ledgerId", ThrottleConstants.CashOnHandLedgerId);
            map.put("particularsId", ThrottleConstants.CashOnHandLedgerCode);
            map.put("amount", financeTO.getPayAmount());
            map.put("accountsType", "DEBIT");
            map.put("narration", financeTO.getRemarks());
            map.put("searchCode", financeTO.getInvoiceCode());
            map.put("reference", "CREDITNOTE");

            System.out.println("map1 updateClearnceDate=---------------------> " + map);
//            status = (Integer) getSqlMapClientTemplate().update("finance.insertAccountEntry", map);
            status = (Integer) session.update("finance.insertAccountEntry", map);
            System.out.println("status1 = " + status);
            //--------------------------------- acc 2nd row start --------------------------
            if (status > 0) {
                map.put("detailCode", "2");
                map.put("ledgerId", customerLedgerId);
                map.put("particularsId", customerLedgerCode);
                map.put("accountsType", "CREDIT");
                System.out.println("map2 updateClearnceDate=---------------------> " + map);
//                status = (Integer) getSqlMapClientTemplate().update("finance.insertAccountEntry", map);
                status = (Integer) session.update("finance.insertAccountEntry", map);
                System.out.println("status2 = " + status);
            }

            /*
            map.put("remarks", financeTO.getRemarks());
            map.put("receiptDate", financeTO.getReceiptDate());
            map.put("invoiceId", financeTO.getInvoiceId());
            map.put("userId", userId);
            //////System.out.println("map saveCreditNote " + map);

            map.put("DetailCode", "1");
            map.put("voucherType", "%RECEIPT%");
            String code2 = (String) getSqlMapClientTemplate().queryForObject("finance.getVoucerCode", map);
            String[] temp = code2.split("-");
            int codeval2 = Integer.parseInt(temp[1]);
            int codev2 = codeval2 + 1;
            String voucherCode = "RECEIPT-" + codev2;
            //////System.out.println("voucherCode = " + voucherCode);
            map.put("voucherCode", voucherCode);
            map.put("mainEntryType", "VOUCHER");
            map.put("entryType", "RECEIPT");

            map.put("ledgerId", 51);
            map.put("particularsId", "LEDGER-39");

            map.put("amount", financeTO.getPayAmount());
            map.put("Accounts_Type", "DEBIT");
            map.put("AmountType", "DEBIT");
            map.put("SearchCode", financeTO.getInvoiceId());
            //////System.out.println("map1 =---------------------> " + map);
            int status1 = (Integer) getSqlMapClientTemplate().update("finance.insertCreditFinanceAccountEntry", map);
            //////System.out.println("status1 = " + status1);
            //account entry end
            //--------------------------------- acc 2nd row start --------------------------
            if (status1 > 0) {
                map.put("DetailCode", "2");
                map.put("amount", financeTO.getPayAmount());
                map.put("particularsId", financeTO.getCustomerId());
                map.put("Accounts_Type", "CREDIT");
                map.put("AmountType", "CREDIT");
                map.put("reference", "Credit-Note");

                //getcustomer ledgerid, code
                //get ledger info
                map.put("customer", financeTO.getCustomerId());
                String ledgerInfo = (String) getSqlMapClientTemplate().queryForObject("finance.getCustomerLedgerInfo", map);
                //////System.out.println("ledgerInfo:" + ledgerInfo);
                temp = ledgerInfo.split("~");
                String ledgerId = temp[0];
                String particularsId = temp[1];
                map.put("ledgerId", ledgerId);
                map.put("particularsId", particularsId);

                //////System.out.println("map2 =---------------------> " + map);
                int status2 = (Integer) getSqlMapClientTemplate().update("finance.insertDebitFinanceAccountEntry", map);
                /
                //////System.out.println("status2 = " + status2);
                invoiceReceipt = (Integer) getSqlMapClientTemplate().update("finance.updateInvoiceHeader", map);
            }*/
        }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveCreditNote Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveCreditNote", sqlException);
        }
        return creditNoteCode;
    }
    
    
    public String saveCreditNoteGta(FinanceTO financeTO, String[] tripIds, String[] freightAmount, String[] grandTotal, String[] payAmount, String reason, String[] tollCredit, String[] greenTaxCredit, String[] weightmentCredit, String[] detaintionCredit, String[] otherCredit, int userId, SqlMapClient session, String finYear) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int invoiceReceipt = 0;
        int status = 0;
        int formId = 0;
        int voucherNo = 0;
        String creditNoteCode = "";
        String fromDate = "";
        String invoiceCode = "";
        String currentYear = "";
        String InvoiceDate = "";
        String compareDate = "31-03-2018";
        int creditNoteNumber = 0;
        try {
            InvoiceDate = financeTO.getInvoiceDate();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date d1 = sdf.parse(compareDate);
            Date d2 = sdf.parse(InvoiceDate);

//            creditNoteNumber = (Integer) getSqlMapClientTemplate().insert("finance.getCreditNoteCode1819", map);
//            creditNoteNumber = (Integer) session.insert("finance.getCreditNoteCode1819", map);
//            creditNoteCode = "CR1819TPG";
            //if (d2.after(d1)) { // After marc 31st 2020
            System.out.println("finYear------" + finYear);

              if (finYear.equalsIgnoreCase("2324")) { // After marc 31st 2020
                    Calendar now = Calendar.getInstance();
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = new Date();
                    System.out.println("1111111111111 ");
                    fromDate = dateFormat.format(date);
                    
                    creditNoteNumber = (Integer) session.insert("finance.getCreditNoteCodeBos2324", map);
                    
                    creditNoteCode = "CR2324TBS";
                } else { //Before
                    System.out.println("22222222 ");
                    fromDate = "2023-03-31 22:00:00";
                    creditNoteNumber = (Integer) session.insert("finance.getCreditNoteCodeBos2223", map);
                    creditNoteCode = "CR2223TBS";
                }

           

            map.put("amount", financeTO.getPayAmount());
            map.put("customer", financeTO.getCustomerId());
            map.put("invoiceId", financeTO.getInvoiceId());
            map.put("totalInvoiceAmount", financeTO.getInvoiceAmount());

            map.put("remarks", financeTO.getRemarks());
            map.put("gstType", financeTO.getGstType());
            map.put("reason", reason);
            map.put("userId", userId);

            //getCredit NOte Code
            invoiceCode = creditNoteNumber + "";
            if (invoiceCode.length() == 1) {
                invoiceCode = "000000" + invoiceCode;
                System.out.println("invoice no 1.." + invoiceCode);
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "00000" + invoiceCode;
                System.out.println("invoice lenght 2.." + invoiceCode);
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 4) {
                invoiceCode = "000" + invoiceCode;
            } else if (invoiceCode.length() == 5) {
                invoiceCode = "00" + invoiceCode;
            } else if (invoiceCode.length() == 6) {
                invoiceCode = "0" + invoiceCode;
            }
            creditNoteCode = creditNoteCode + invoiceCode;
            System.out.println("getInvoiceCodeSequence=" + creditNoteCode);

            System.out.println("creditNoteCode:" + creditNoteCode);
            map.put("creditNoteCode", creditNoteCode);
            map.put("fromDate", fromDate);
            System.out.println("creditNote Map:" + map);
            int insertCreditNote = (Integer) session.insert("finance.insertInvoiceCreditNote", map);
//            int insertCreditNote = (Integer) getSqlMapClientTemplate().insert("finance.insertInvoiceCreditNote", map);
//            invoiceReceipt = (Integer) getSqlMapClientTemplate().update("finance.updateInvoiceHeader", map);
            invoiceReceipt = (Integer) session.update("finance.updateInvoiceHeader", map);
            if (insertCreditNote > 0) {
                for (int i = 0; i < tripIds.length; i++) {
                    map.put("freightAmount", freightAmount[i]);
                    map.put("tripId", tripIds[i]);
                    map.put("toll", tollCredit[i]);
                    map.put("greenTax", "0");
//                   map.put("greenTax",greenTaxCredit[i]);
                    map.put("detaintion", detaintionCredit[i]);
                    map.put("weightment", weightmentCredit[i]);
                    map.put("other", otherCredit[i]);
                    if ("".equalsIgnoreCase(payAmount[i]) || payAmount[i] == null) {
                        payAmount[i] = "0.00";
                    }
                    map.put("creditAmount", payAmount[i]);
                    map.put("creditNoteId", insertCreditNote);
                    System.out.println("insertCreditNoteDetails map:" + map);
//                    int insertCreditNoteDetails = (Integer) getSqlMapClientTemplate().update("finance.insertInvoiceCreditNoteDetails", map);
                    int insertCreditNoteDetails = (Integer) session.update("finance.insertInvoiceCreditNoteDetails", map);
                    System.out.println("insertCreditNoteDetails---" + insertCreditNoteDetails);
                }
            }

            // CREDIT NOTE OUTSTANDING CREDIT LIMIT
            map.put("customerId", financeTO.getCustomerId());
            map.put("invoiceId", financeTO.getInvoiceId());
            map.put("invoiceAmount", financeTO.getPayAmount());

            map.put("consignmentOrderId", "0");
            map.put("custType", "1");
            map.put("outstandingAmount", financeTO.getPayAmount());
            map.put("totalFreightAmount", financeTO.getPayAmount());
            map.put("creditNoteInvoiceId", insertCreditNote);
            map.put("supplymentryInvoiceId", "0");
            map.put("paymentId", "0");

            map.put("userId", userId);
            System.out.println("updateInvoiceOustanding----" + map);

            int updateCnoteCount, updateOutstanding, updateCreditLimit, updateUsedLimit, updateOutstandingLog = 0;
            String getCustomerType = (String) session.queryForObject("operation.getCustomerType", map);
            String availBlockedAmount = "";
            String availBlockedAmountTemp[] = null;
            map.put("custType", getCustomerType);
            map.put("transactionName", "CreditNote");

            if ("1".equalsIgnoreCase(getCustomerType)) {   //for credit Customers Only
                availBlockedAmount = (String) session.queryForObject("operation.getAvailandBlockedAmount", map);
                System.out.println("availBlockedAmount------" + availBlockedAmount);
                availBlockedAmountTemp = availBlockedAmount.split("~");
                map.put("fixedLimit", availBlockedAmountTemp[0]);
                map.put("blockedAmount", availBlockedAmountTemp[2]);

                double usedLimit = Double.parseDouble(availBlockedAmountTemp[3]) - Double.parseDouble(financeTO.getPayAmount());
                double availLimit = Double.parseDouble(availBlockedAmountTemp[1]) + Double.parseDouble(financeTO.getPayAmount());

                map.put("availAmount", availLimit);

                if (usedLimit < 0) {
                    map.put("usedLimit", 0);
                } else {
                    map.put("usedLimit", availBlockedAmountTemp[3]);
                }

                updateCreditLimit = (Integer) session.update("operation.updateCreditNoteInvoiceLimit", map);

                map.put("transactionType", "Credit");   // credit
                map.put("debitAmount", "0.00");
                map.put("creditAmount", financeTO.getPayAmount());
                updateOutstandingLog = (Integer) session.update("operation.updateOutstandingLog", map);

            } else if ("2".equalsIgnoreCase(getCustomerType)) {
                availBlockedAmount = (String) session.queryForObject("operation.getAvailandBlockedAmount", map);
                System.out.println("availBlockedAmount------" + availBlockedAmount);
                availBlockedAmountTemp = availBlockedAmount.split("~");
                map.put("fixedLimit", availBlockedAmountTemp[0]);
                map.put("blockedAmount", availBlockedAmountTemp[2]);

                double usedLimit = Double.parseDouble(availBlockedAmountTemp[3]) - Double.parseDouble(financeTO.getPayAmount());
                double availLimit = Double.parseDouble(availBlockedAmountTemp[1]) + Double.parseDouble(financeTO.getPayAmount());

                map.put("availAmount", availLimit);

                if (usedLimit < 0) {
                    map.put("usedLimit", 0);
                } else {
                    map.put("usedLimit", availBlockedAmountTemp[3]);
                }

                updateCreditLimit = (Integer) session.update("operation.updateCreditNoteInvoiceLimit", map);

                map.put("transactionType", "Credit");   // credit
                map.put("debitAmount", "0.00");
                map.put("creditAmount", financeTO.getPayAmount());
                updateOutstandingLog = (Integer) session.update("operation.updateOutstandingLog", map);
            }

            // CREDIT NOTE OUTSTANDING CREDIT LIMIT      
//            String customerLedgerId = (String) getSqlMapClientTemplate().queryForObject("finance.getCustomerLedgerId", map);
//            String customerLedgerCode = (String) getSqlMapClientTemplate().queryForObject("finance.getCustomerLedgerCode", map);
            String customerLedgerId = (String) session.queryForObject("finance.getCustomerLedgerId", map);
            String customerLedgerCode = (String) session.queryForObject("finance.getCustomerLedgerCode", map);

//            invoiceReceipt = (Integer) getSqlMapClientTemplate().update("finance.updateSupplyMaster", map);
            //   System.out.println("status2 = " + status2);
            System.out.println("status final = " + status);
            System.out.println("ThrottleConstants.cashPaymentFormId = " + ThrottleConstants.cashPaymentFormId);
            formId = Integer.parseInt(ThrottleConstants.cashPaymentFormId);
            map.put("formId", formId);
//            voucherNo = (Integer) getSqlMapClientTemplate().insert("finance.getFormVoucherNo", map);
            voucherNo = (Integer) session.insert("finance.getFormVoucherNo", map);
            map.put("voucherNo", voucherNo);
            map.put("voucherCodeNo", ThrottleConstants.CashPaymentVoucherCode + voucherNo);
            map.put("voucherType", "%PAYMENT%");
            String code = "";
            String[] temp;
//            code = (String) getSqlMapClientTemplate().queryForObject("finance.getVoucerCode", map);
            code = (String) session.queryForObject("finance.getVoucerCode", map);
            System.out.println("code----" + code);
            int codeval2 = 0;
            if (code == null) {
                codeval2 = 0;
            } else {
                temp = code.split("-");
                codeval2 = Integer.parseInt(temp[1]);
            }
            int codev2 = codeval2 + 1;
            String voucherCode = "PAYMENT-" + codev2;
            System.out.println("voucherCode = " + voucherCode);

            map.put("detailCode", "1");
            map.put("voucherCode", voucherCode);
            map.put("accountEntryDate", financeTO.getReceiptDate());
            map.put("mainEntryType", "VOUCHER");
            map.put("entryType", "PAYMENT");
            map.put("ledgerId", ThrottleConstants.CashOnHandLedgerId);
            map.put("particularsId", ThrottleConstants.CashOnHandLedgerCode);
            map.put("amount", financeTO.getPayAmount());
            map.put("accountsType", "DEBIT");
            map.put("narration", financeTO.getRemarks());
            map.put("searchCode", financeTO.getInvoiceCode());
            map.put("reference", "CREDITNOTE");

            System.out.println("map1 updateClearnceDate=---------------------> " + map);
//            status = (Integer) getSqlMapClientTemplate().update("finance.insertAccountEntry", map);
            status = (Integer) session.update("finance.insertAccountEntry", map);
            System.out.println("status1 = " + status);
            //--------------------------------- acc 2nd row start --------------------------
            if (status > 0) {
                map.put("detailCode", "2");
                map.put("ledgerId", customerLedgerId);
                map.put("particularsId", customerLedgerCode);
                map.put("accountsType", "CREDIT");
                System.out.println("map2 updateClearnceDate=---------------------> " + map);
//                status = (Integer) getSqlMapClientTemplate().update("finance.insertAccountEntry", map);
                status = (Integer) session.update("finance.insertAccountEntry", map);
                System.out.println("status2 = " + status);
            }

            /*
            map.put("remarks", financeTO.getRemarks());
            map.put("receiptDate", financeTO.getReceiptDate());
            map.put("invoiceId", financeTO.getInvoiceId());
            map.put("userId", userId);
            //////System.out.println("map saveCreditNote " + map);

            map.put("DetailCode", "1");
            map.put("voucherType", "%RECEIPT%");
            String code2 = (String) getSqlMapClientTemplate().queryForObject("finance.getVoucerCode", map);
            String[] temp = code2.split("-");
            int codeval2 = Integer.parseInt(temp[1]);
            int codev2 = codeval2 + 1;
            String voucherCode = "RECEIPT-" + codev2;
            //////System.out.println("voucherCode = " + voucherCode);
            map.put("voucherCode", voucherCode);
            map.put("mainEntryType", "VOUCHER");
            map.put("entryType", "RECEIPT");

            map.put("ledgerId", 51);
            map.put("particularsId", "LEDGER-39");

            map.put("amount", financeTO.getPayAmount());
            map.put("Accounts_Type", "DEBIT");
            map.put("AmountType", "DEBIT");
            map.put("SearchCode", financeTO.getInvoiceId());
            //////System.out.println("map1 =---------------------> " + map);
            int status1 = (Integer) getSqlMapClientTemplate().update("finance.insertCreditFinanceAccountEntry", map);
            //////System.out.println("status1 = " + status1);
            //account entry end
            //--------------------------------- acc 2nd row start --------------------------
            if (status1 > 0) {
                map.put("DetailCode", "2");
                map.put("amount", financeTO.getPayAmount());
                map.put("particularsId", financeTO.getCustomerId());
                map.put("Accounts_Type", "CREDIT");
                map.put("AmountType", "CREDIT");
                map.put("reference", "Credit-Note");

                //getcustomer ledgerid, code
                //get ledger info
                map.put("customer", financeTO.getCustomerId());
                String ledgerInfo = (String) getSqlMapClientTemplate().queryForObject("finance.getCustomerLedgerInfo", map);
                //////System.out.println("ledgerInfo:" + ledgerInfo);
                temp = ledgerInfo.split("~");
                String ledgerId = temp[0];
                String particularsId = temp[1];
                map.put("ledgerId", ledgerId);
                map.put("particularsId", particularsId);

                //////System.out.println("map2 =---------------------> " + map);
                int status2 = (Integer) getSqlMapClientTemplate().update("finance.insertDebitFinanceAccountEntry", map);
                /
                //////System.out.println("status2 = " + status2);
                invoiceReceipt = (Integer) getSqlMapClientTemplate().update("finance.updateInvoiceHeader", map);
            }*/
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveCreditNote Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveCreditNote", sqlException);
        }
        return creditNoteCode;
    }

    public ArrayList getFinanceYearList() {
        Map map = new HashMap();
        ArrayList financeYearList = new ArrayList();
        try {
            //////System.out.println("invoiceDetails map= " + map);
            financeYearList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getFinanceYearList", map);
            //////System.out.println("financeYearList DAO= " + financeYearList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("financeYearList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "financeYearList", sqlException);
        }
        return financeYearList;
    }

    public ArrayList getFinanceYear(FinanceTO financeTO) {
        Map map = new HashMap();
        ArrayList getFinanceYear = new ArrayList();
        try {
            map.put("accountYearid", financeTO.getAccountYearId());
            System.out.println("accountYearid map= " + map);
            getFinanceYear = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getFinanceYear", map);
            //////System.out.println("financeYearList DAO= " + financeYearList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("financeYearList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "financeYearList", sqlException);
        }
        return getFinanceYear;
    }

    public String getChargeCodeList(FinanceTO financeTO) {
        Map map = new HashMap();
        map.put("ChargeCode", "%" + financeTO.getChargeCode() + "%");
        String ChargeCode = "";
        System.out.println("map new = " + map);
        try {
            ChargeCode = (String) getSqlMapClientTemplate().queryForObject("finance.getChargeCodeList", map);
            System.out.println("ChargeCode in DAO = " + ChargeCode);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getChargeCodeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getChargeCodeList", sqlException);
        }
        return ChargeCode;
    }

    public String checkBankAccountName(FinanceTO financeTO) {
        Map map = new HashMap();
        map.put("bankName", financeTO.getBankName());
        String bankName = "";
        System.out.println("map new = " + map);
        try {
            bankName = (String) getSqlMapClientTemplate().queryForObject("finance.checkBankAccountName", map);
            System.out.println("bankName in DAO = " + bankName);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("checkBankAccountName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "checkBankAccountName", sqlException);
        }
        return bankName;
    }

    public String checktaxName(FinanceTO financeTO) {
        Map map = new HashMap();
        map.put("taxName", financeTO.getTaxName());
        String taxName = "";
        System.out.println("map new = " + map);
        try {
            taxName = (String) getSqlMapClientTemplate().queryForObject("finance.checktaxName", map);
            System.out.println("taxName in DAO = " + taxName);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("checktaxName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "checktaxName", sqlException);
        }
        return taxName;
    }

    public ArrayList getCreditNoteSearchList(FinanceTO financeTO) {
        Map map = new HashMap();
        ArrayList creditNoteSearchList = new ArrayList();
        map.put("customerId", financeTO.getCustomerId());
        map.put("fromDate", financeTO.getFromDate());
        map.put("toDate", financeTO.getToDate());
        try {
            System.out.println("creditNoteSearchList map= " + map);
            creditNoteSearchList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getCreditNoteSearchList", map);
            System.out.println("getCreditNoteSearchList DAO= " + creditNoteSearchList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCreditNoteSearchList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getCreditNoteSearchList", sqlException);
        }
        return creditNoteSearchList;
    }

    public int saveFinYear(FinanceTO financeTO, int userId) {
        Map map = new HashMap();
        int status = 0;

        map.put("userId", userId);
        map.put("accountYear", financeTO.getFinYear());
        map.put("from_date", financeTO.getFromDate());
        map.put("to_date", financeTO.getToDate());
        map.put("Active_ind", financeTO.getActive_ind());
        map.put("description", financeTO.getDescription());

        try {
            System.out.println("insert Fin Year map:" + map);
            status = (Integer) getSqlMapClientTemplate().update("finance.InactiveOldAccYear", map);

            status = (Integer) getSqlMapClientTemplate().update("finance.insertAccYear", map);
            System.out.println("insert status" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertFinYear Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertFinYear", sqlException);
        }
        return status;
    }

    public int updateFinYear(FinanceTO financeTO, int userId) {
        Map map = new HashMap();
        int status = 0;

        map.put("userId", userId);
        map.put("accountYear", financeTO.getFinYear());
        map.put("from_date", financeTO.getFromDate());
        map.put("to_date", financeTO.getToDate());
        map.put("Active_ind", financeTO.getActive_ind());
        map.put("description", financeTO.getDescription());
        map.put("accountYearId", financeTO.getAccountYearId());

        try {
            System.out.println("update Fin Year map:" + map);
            status = (Integer) getSqlMapClientTemplate().update("finance.updateAccYear", map);
            System.out.println("insert status" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateAccYear Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "updateAccYear", sqlException);
        }
        return status;
    }

    public String checkBankAccountCode(FinanceTO financeTO) {
        Map map = new HashMap();
        map.put("bankCode", financeTO.getBankCode());
        String checkBankAccountCode = "";
        System.out.println("map new = " + map);
        try {
            checkBankAccountCode = (String) getSqlMapClientTemplate().queryForObject("finance.checkBankAccountCode", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("checkBankAccountCode Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "checkBankAccountCode", sqlException);
        }
        return checkBankAccountCode;
    }

    public String checkFinYear(FinanceTO financeTO) {
        Map map = new HashMap();
        map.put("finYear", financeTO.getFinYear());
        String checkFinYear = "";
        //////System.out.println("map new = " + map);getFinanceYear
        try {
            checkFinYear = (String) getSqlMapClientTemplate().queryForObject("finance.checkFinYear", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("checkFinYear Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "checkFinYear", sqlException);
        }
        return checkFinYear;
    }

    public ArrayList getLedgerDetailsGrp(String ledgerName, String entryType) {
        Map map = new HashMap();
        ArrayList ledgerList = new ArrayList();
        map.put("ledgerName", ledgerName);

        try {
            //////System.out.println("creditNoteSearchList map= " + map);
            ledgerList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.paymentLedgerListForFinGrp", map);
            System.out.println("getCreditNoteSearchList DAO= " + ledgerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getLedgerDetailsGrp Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getLedgerDetailsGrp", sqlException);
        }
        return ledgerList;

    }

    public ArrayList getGroupLevelList(FinanceTO financeTO) {
        Map map = new HashMap();
        ArrayList groupLevelList = new ArrayList();
        map.put("groupId", financeTO.getGroupID());
        map.put("referenceId", financeTO.getReference());
        try {
            groupLevelList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getGroupLevelList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getGroupLevelList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getGroupLevelList", sqlException);
        }
        return groupLevelList;
    }

    public ArrayList getCashLedgerDetails(String cashHeadName, String entryType) {
        Map map = new HashMap();
        ArrayList ledgerList = new ArrayList();
        map.put("ledgerName", cashHeadName);

        try {
            //////System.out.println("creditNoteSearchList map= " + map);
            ledgerList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getCashLedgerList", map);
            System.out.println("getCashLedgerDetails DAO= " + ledgerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCashLedgerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getCashLedgerDetails", sqlException);
        }
        return ledgerList;

    }

    public ArrayList getVendorList(String vendorType) {
        Map map = new HashMap();
        ArrayList vendorList = new ArrayList();
        map.put("vendorType", vendorType);
        System.out.println("map" + map);
        try {
            vendorList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getVendorList", map);
            System.out.println("vendorList = " + vendorList.size());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "vendorList", sqlException);
        }
        return vendorList;
    }

    public ArrayList getAccountTypeDetails(String accountType, String searchCode) {
        Map map = new HashMap();
        ArrayList getAccountTypeDetails = new ArrayList();
        map.put("accountType", accountType);
        map.put("searchCode", searchCode);

        try {
            if (accountType.equalsIgnoreCase("Trip")) {
                getAccountTypeDetails = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getAccountTripDetail", map);
            } else if (accountType.equalsIgnoreCase("Order")) {
                getAccountTypeDetails = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getAccountOrderDetail", map);
            }

            System.out.println("getAccountTypeDetails = " + getAccountTypeDetails.size());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog("getAccountTypeDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getAccountTypeDetails", sqlException);
        }
        return getAccountTypeDetails;
    }

    public ArrayList getPrimaryLevelMasterList() {
        Map map = new HashMap();
        ArrayList levelMasterList = new ArrayList();
        System.out.println("map" + map);
        try {
            levelMasterList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getPrimaryLevelMasterList", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "vendorList", sqlException);
        }
        return levelMasterList;
    }

    public ArrayList getNextLevelList(String levelId) {
        Map map = new HashMap();
        ArrayList levelMasterList = new ArrayList();
        map.put("levelId", levelId);
        System.out.println("map" + map);
        try {
            levelMasterList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getNextLevelList", map);
            if (levelMasterList.size() == 0) {
                levelMasterList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getLedgersForTheLevel", map);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "vendorList", sqlException);
        }
        return levelMasterList;
    }

    public ArrayList getBankLedgerDetails(String cashHeadName, String entryType) {
        Map map = new HashMap();
        ArrayList ledgerList = new ArrayList();
        map.put("ledgerName", cashHeadName);

        try {
            //////System.out.println("creditNoteSearchList map= " + map);
            ledgerList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getBankLedgerList", map);
            System.out.println("getCashLedgerDetails DAO= " + ledgerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCashLedgerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getCashLedgerDetails", sqlException);
        }
        return ledgerList;

    }

    //pavi
    public int insertCRJ(FinanceTO financeTO, int userId, String vendorTypeId, String vendorNameId, String crjDate, String invoiceNo, String invoiceDate,
            String invoiceAmount, String remarks, String vendorLedgerId, String actualFilePath, String fileSaved, String debitLedgerId, SqlMapClient session) {
        Map map = new HashMap();
        System.out.println("insertCRJ dao");
        /*
         * set the parameters in the map for sending to ORM
         */
        int crjId = 0;
        int status = 0;
        int formId = 0;
        int voucherNo = 0;
        FileInputStream fis = null;
        try {
            String temp1[] = debitLedgerId.split("~");
            String debitLedgerNameId = temp1[0];
            String debitLedgerCode = temp1[1];
            map.put("vendorTypeId", vendorTypeId);
            map.put("vendorNameId", vendorNameId);
            map.put("crjDate", crjDate);
            map.put("invoiceNo", invoiceNo);
            map.put("invoiceDate", invoiceDate);
            map.put("invoiceAmount", invoiceAmount);
            map.put("remarks", remarks);
            map.put("vendorLedgerId", vendorLedgerId);
            map.put("vendorId", vendorNameId);
            map.put("userId", userId);
            map.put("debitLedgerNameId", debitLedgerNameId);
            //file
            map.put("fileName", fileSaved);
            System.out.println("map1111 = " + map);
            System.out.println("actualFilePath = " + actualFilePath);
            File file = new File(actualFilePath);
            System.out.println("file = " + file);
            fis = new FileInputStream(file);
            System.out.println("fis = " + fis);
            byte[] uploadfile = new byte[(int) file.length()];
//           System.out.println("uploadfile = " + uploadfile);
            fis.read(uploadfile);
            fis.close();
            map.put("uploadfile", uploadfile);
            //file end
            System.out.println("the saveFileUploadDetails123455" + map);
            String vendorLedgerCode = (String) session.queryForObject("finance.getVendorLedgerCode", map);
            System.out.println("map insertCRJ " + map);
            crjId = (Integer) session.insert("finance.insertCRJ", map);
            map.put("paymentReferenceCode", ThrottleConstants.CRJformCode + String.valueOf(crjId));
            map.put("paymentReferenceId", crjId);
            map.put("paymentReferenceName", "CRJ");
            map.put("paymentAmount", invoiceAmount);
//            map.put("vendorNameId", vendorNameId);
            status = (Integer) session.update("finance.insertPaymentDetails", map);
            if (status > 0) {
                formId = Integer.parseInt(ThrottleConstants.cashPaymentFormId);
                map.put("formId", formId);
                voucherNo = (Integer) session.insert("finance.getFormVoucherNo", map);
                map.put("voucherNo", voucherNo);
                map.put("voucherCodeNo", ThrottleConstants.CashPaymentVoucherCode + voucherNo);
                map.put("voucherType", "%PAYMENT%");
                String code = "";
                String[] temp;
                code = (String) session.queryForObject("finance.getVoucerCode", map);
                temp = code.split("-");
                int codeval2 = Integer.parseInt(temp[1]);
                int codev2 = codeval2 + 1;
                String voucherCode = "PAYMENT-" + codev2;
                System.out.println("voucherCode = " + voucherCode);

                map.put("detailCode", "1");
                map.put("voucherCode", voucherCode);
                map.put("accountEntryDate", invoiceDate);
                map.put("mainEntryType", "VOUCHER");
                map.put("entryType", "PAYMENT");
                map.put("ledgerId", debitLedgerNameId);
                map.put("particularsId", debitLedgerCode);
                map.put("amount", invoiceAmount);
                map.put("accountsType", "DEBIT");
                map.put("narration", "CRJ");
                map.put("searchCode", ThrottleConstants.CRJformCode + String.valueOf(crjId));
                map.put("reference", "CRJ");

                System.out.println("map1 updateClearnceDate=---------------------> " + map);
                status = (Integer) session.update("finance.insertAccountEntry", map);
                System.out.println("status1 = " + status);
                //--------------------------------- acc 2nd row start --------------------------
                if (status > 0) {
                    map.put("detailCode", "2");
                    map.put("ledgerId", vendorLedgerId);
                    map.put("particularsId", vendorLedgerCode);
                    map.put("accountsType", "CREDIT");
                    System.out.println("map2 updateClearnceDate=---------------------> " + map);
                    status = (Integer) session.update("finance.insertAccountEntry", map);
                    System.out.println("status2 = " + status);
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveInvoicePaymentDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveInvoicePaymentDetail", sqlException);
        }
        return crjId;
    }

    public ArrayList getExpenseGroupList() {
        Map map = new HashMap();
        ArrayList expenseGroupList = new ArrayList();
        map.put("expenseGroupCode", ThrottleConstants.expenseGroupCode);
        try {
            System.out.println(" getLedger map =" + map);
            expenseGroupList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getExpenseGroupList", map);
            System.out.println(" expenseGroupList =" + expenseGroupList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("fromLocationList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "driverList", sqlException);
        }
        return expenseGroupList;
    }

    public ArrayList getExpenseLedgerList(FinanceTO financeTO) {
        Map map = new HashMap();
        ArrayList expenseGroupList = new ArrayList();
        map.put("levelId", financeTO.getLevelID());
        try {
            System.out.println(" getLedger map =" + map);
            expenseGroupList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getExpenseLedgerList", map);
            System.out.println(" getExpenseLedgerList =" + expenseGroupList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("fromLocationList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "driverList", sqlException);
        }
        return expenseGroupList;
    }

    public ArrayList getBPClearanceDetails(FinanceTO financeTO) {
        Map map = new HashMap();
        ArrayList BPClearanceDetails = new ArrayList();
        if (financeTO.getFromDate() == null) {
            map.put("fromDate", "");
        } else {
            map.put("fromDate", financeTO.getFromDate());
        }
        if (financeTO.getToDate() == null) {
            map.put("toDate", "");
        } else {
            map.put("toDate", financeTO.getToDate());
        }
        if ("0".equals(financeTO.getBankId()) || financeTO.getBankId() == null) {
            map.put("bankId", "");
        } else {
            map.put("bankId", financeTO.getBankId());
        }
        if ("0".equals(financeTO.getBranchId()) || financeTO.getBranchId() == null) {
            map.put("bankBranchId", "");
        } else {
            map.put("bankBranchId", financeTO.getBranchId());
        }
        if (financeTO.getReferenceNo() == null) {
            map.put("referenceCode", "");
        } else {
            map.put("referenceCode", financeTO.getReferenceNo());
        }

        try {
            System.out.println("getBPClearanceDetails map= " + map);
            BPClearanceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("finance.BPClearanceDetails", map);
            System.out.println("getBPClearanceDetails DAO= " + BPClearanceDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCashLedgerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getCashLedgerDetails", sqlException);
        }
        return BPClearanceDetails;

    }

    public int updateClearnceDate(FinanceTO financeTO, int userId, String clearanceDate, String clearanceStatus, String clearanceremark, String bankPaymentId, String debitLedgerId,
            String creditLedgerId, String chequeAmount, String chequeNo, String chequeDate, String entryType, String formReferenceCode, SqlMapClient session) {
        Map map = new HashMap();
        System.out.println("insertCRJ dao");
        /*
         * set the parameters in the map for sending to ORM
         */
        int status = 0;
        int formId = 0;
        int voucherNo = 0;
        map.put("clearanceDate", clearanceDate);
        map.put("clearanceStatus", clearanceStatus);
        map.put("clearanceremark", clearanceremark);
        try {

            map.put("bankPaymentId", bankPaymentId);
            map.put("debitLedgerId", debitLedgerId);
            map.put("creditLedgerId", creditLedgerId);
            map.put("chequeAmount", chequeAmount);
            map.put("chequeNo", chequeNo);
            map.put("chequeDate", chequeDate);
            map.put("formReferenceCode", formReferenceCode);
            status = (Integer) session.update("finance.updateClearnceDate", map);

            if (status > 0 && "1".equals(clearanceStatus)) {
                String creditLedgerCode = (String) session.queryForObject("finance.getcreditLedgerCode", map);
                String debitLedgerCode = (String) session.queryForObject("finance.getdebitLedgerCode", map);
                if ("PAYMENT".equalsIgnoreCase(entryType)) {
                    formId = Integer.parseInt(ThrottleConstants.BankPaymentFormId);
                    map.put("formId", formId);
                    voucherNo = (Integer) session.insert("finance.getFormVoucherNo", map);
                    map.put("voucherNo", voucherNo);
                    map.put("voucherCodeNo", ThrottleConstants.BankPaymentVoucherCode + voucherNo);
//                    map.put("voucherType", "%PAYMENT%");
//                    String code = "";
//                    String[] temp;
//                    code = (String) session.queryForObject("finance.getVoucerCode", map);
//                    temp = code.split("-");
//                    int codeval2 = Integer.parseInt(temp[1]);
//                    int codev2 = codeval2 + 1;
                    String voucherCode = "PAYMENT-" + voucherNo;
                    System.out.println("voucherCode = " + voucherCode);

                    map.put("detailCode", "1");
                    map.put("voucherCode", voucherCode);
                    map.put("accountEntryDate", clearanceDate);
                    map.put("mainEntryType", "VOUCHER");
                    map.put("entryType", "PAYMENT");
                    map.put("ledgerId", debitLedgerId);
                    map.put("particularsId", debitLedgerCode);
                    map.put("amount", chequeAmount);
                    map.put("accountsType", "DEBIT");
                    map.put("narration", "CLEARANCE");
                    map.put("reference", "CLEARANCE");
                    map.put("searchCode", formReferenceCode);

                    System.out.println("map1 updateClearnceDate=---------------------> " + map);
                    status = (Integer) session.update("finance.insertAccountEntry", map);
                    System.out.println("status1 = " + status);
                    //--------------------------------- acc 2nd row start --------------------------
                    if (status > 0) {
                        map.put("detailCode", "2");
                        map.put("ledgerId", creditLedgerId);
                        map.put("particularsId", creditLedgerCode);
                        map.put("accountsType", "CREDIT");
                        System.out.println("map2 updateClearnceDate=---------------------> " + map);
                        status = (Integer) session.update("finance.insertAccountEntry", map);
                        System.out.println("status2 = " + status);
                    }
                } else if ("RECEIPT".equalsIgnoreCase(entryType)) {
                    formId = Integer.parseInt(ThrottleConstants.BankReceiptFormId);
                    map.put("formId", formId);
                    voucherNo = (Integer) session.insert("finance.getFormVoucherNo", map);
                    map.put("voucherNo", voucherNo);
                    map.put("voucherCodeNo", ThrottleConstants.BankReceiptVoucherCode + voucherNo);

                    map.put("voucherType", "%RECEIPT%");
                    String code = "";
                    String[] temp;
                    code = (String) session.queryForObject("finance.getVoucerCode", map);
                    temp = code.split("-");
                    int codeval2 = Integer.parseInt(temp[1]);
                    int codev2 = codeval2 + 1;
                    String voucherCode = "RECEIPT-" + codev2;
                    System.out.println("voucherCode = " + voucherCode);

                    map.put("detailCode", "1");
                    map.put("voucherCode", voucherCode);
                    map.put("accountEntryDate", clearanceDate);
                    map.put("mainEntryType", "VOUCHER");
                    map.put("entryType", "RECEIPT");
                    map.put("ledgerId", debitLedgerId);
                    map.put("particularsId", debitLedgerCode);
                    map.put("amount", chequeAmount);
                    map.put("accountsType", "DEBIT");
                    map.put("narration", "clearance");
                    map.put("reference", "clearance");

                    System.out.println("map1 updateClearnceDate=---------------------> " + map);
                    status = (Integer) session.update("finance.insertAccountEntry", map);
                    System.out.println("status1 = " + status);
                    //--------------------------------- acc 2nd row start --------------------------
                    if (status > 0) {
                        map.put("detailCode", "2");
                        map.put("ledgerId", creditLedgerId);
                        map.put("particularsId", creditLedgerCode);
                        map.put("accountsType", "CREDIT");
                        System.out.println("map2 updateClearnceDate=---------------------> " + map);
                        status = (Integer) session.update("finance.insertAccountEntry", map);
                        System.out.println("status2 = " + status);
                    }
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveInvoicePaymentDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveInvoicePaymentDetail", sqlException);
        }
        return status;
    }

    public ArrayList getBRSChequeList(FinanceTO financeTO, String val) {
        Map map = new HashMap();
        ArrayList BRSChequeList = new ArrayList();
        map.put("fromDate", financeTO.getFromDate());
        map.put("toDate", financeTO.getToDate());
        map.put("branchId", financeTO.getBranchId());
        map.put("bankId", financeTO.getBankid());
        map.put("val", val);
        try {
            System.out.println("creditNoteSearchList map= " + map);
            if ("PAYMENT".equalsIgnoreCase(val)) {
                BRSChequeList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.BRSChequeIssuedList", map);
                System.out.println("getBRSChequeList DAO= " + val + "------>" + BRSChequeList.size());
            } else if ("RECEIPT".equalsIgnoreCase(val)) {
                BRSChequeList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.BRSChequeReceivedList", map);
                System.out.println("getBRSChequeList DAO= " + val + "------>" + BRSChequeList.size());
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCashLedgerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getCashLedgerDetails", sqlException);
        }
        return BRSChequeList;

    }

    public int getFormVoucherNo(int formId, SqlMapClient session) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        int voucherNo = 0;
        try {

            map.put("formId", formId);
            System.out.println("map getFormVoucherNo " + map);
            voucherNo = (Integer) session.insert("finance.getFormVoucherNo", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertReceiptEntry Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertReceiptEntry", sqlException);
        }
        return voucherNo;
    }

    public int reverseAccountEntry(String voucherCodee, String fromDate, int formId, int userId, int voucherNo, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        int accStatus = 0;
        int accStatus1 = 0;
        ArrayList voucherList = new ArrayList();
        FinanceTO financeTO = new FinanceTO();
        try {
            map.put("voucherCodee", voucherCodee);
            System.out.println("map in reverseCostCenterEntry " + map);
            //status = (Integer) session.update("finance.updateCostCenterEntry", map);
            map.put("date", fromDate);
            map.put("userId", userId);
            map.put("formId", formId);
            map.put("voucherNo", voucherNo);
            String codee = "";
            String[] temp2;
            voucherList = (ArrayList) session.queryForList("finance.getAccountEntryList", map);
            Iterator itr = voucherList.iterator();
            int cntr = 0;
            String voucherCode = "";
            while (itr.hasNext()) {
                financeTO = (FinanceTO) itr.next();
                if (cntr == 0) {
                    String[] temp1 = financeTO.getVoucherCode().split("-");
                    map.put("voucherType", "%" + temp1[0] + "%");
                    codee = (String) session.queryForObject("finance.getVoucerCode", map);
                    temp2 = codee.split("-");
                    int codeval2 = Integer.parseInt(temp2[1]);
                    int codev2 = codeval2 + 1;
                    voucherCode = temp1[0] + "-" + codev2;
                    System.out.println("voucherCode = " + voucherCode);
                }
                map.put("voucherCode", voucherCode);
                map.put("detailCode", financeTO.getDetailCode());
                map.put("mainEntryType", financeTO.getMainEntryType());
                map.put("entryType", financeTO.getEntryType());
                map.put("ledgerCode", financeTO.getParticularsId());
                map.put("amountValues", financeTO.getAccountAmount());
                map.put("narrationValues", financeTO.getHeaderNarration());
                map.put("ledgerID", financeTO.getLedgerID());
                map.put("entryStatus", financeTO.getEntryStatus());
                map.put("levelID", financeTO.getLevelID());
                map.put("chequeNo", "");
                map.put("chequeDate", "0000-00-00 00:00:00");
                map.put("SearchCode", "0");
                map.put("Reference", "EntriesAmountReversal");
                map.put("updateStatus", "1");
                System.out.println("reversal map in update" + map);
                if ("DEBIT".equalsIgnoreCase(financeTO.getAccountsType())) {
                    map.put("accTypeValues", "CREDIT");
                    accStatus = (Integer) session.update("finance.insertEntries", map);
                } else if ("CREDIT".equalsIgnoreCase(financeTO.getAccountsType())) {
                    map.put("accTypeValues", "DEBIT");
                    accStatus = (Integer) session.update("finance.insertEntries", map);
                }
                cntr++;
            }
            accStatus1 = (Integer) session.update("finance.updateEntryReversalAccEntry", map);
            System.out.println("map in reset Trip Expense " + map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return status;
    }

    public int insertCostCenter(FinanceTO financeTO, int userId, int accountEntryId, String fromDate, String amountValues, String vehicleId, String costCenterNarration, String accType, SqlMapClient session) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        int costCenter = 0;
        try {
            map.put("accountEntryId", accountEntryId);
            String accountEntryIds = (String) session.queryForObject("finance.getAccountEntryIds", map);
            String voucherCode = (String) session.queryForObject("finance.getLastInsertedVoucher", map);
            map.put("accountEntryIds", accountEntryIds);
            map.put("voucherCode", voucherCode);
            map.put("vehicleId", vehicleId);
            map.put("amount", amountValues);
            map.put("accType", accType);
            map.put("narration", costCenterNarration);
            map.put("userId", userId);
            map.put("entryDate", fromDate);
            System.out.println("map insertCostCenterCashPayment " + map);
            costCenter = (Integer) session.update("finance.insertCostCenterCashPayment", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertCostCenterCashPayment Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertReceiptEntry", sqlException);
        }
        return costCenter;
    }

    public int insertPaymentEntry(int cashDetailCode, String totalCreditAmt, int detailCode, int userId, String fromDate, String bankid1Values, String amountValues, String accTypeValues, String narrationValues, int voucherNo, int formId, String voucherCodeNo, SqlMapClient session) throws SQLException {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        String code = "";
        String delimiter = "-";
        String[] temp;
        int status = 0;
        map.put("voucherCodeNo", voucherCodeNo);
        map.put("formId", formId);
        map.put("voucherNo", voucherNo);
        map.put("updateStatus", "0");
        map.put("entryStatus", "CP");
        map.put("detailCode", detailCode);
        map.put("userId", userId);
        map.put("date", fromDate);
        map.put("bankid1Values", bankid1Values);
        map.put("totalCreditAmt", totalCreditAmt);
        String[] val = bankid1Values.split("~");
        map.put("ledgerID", val[0]);
        map.put("groupCode", val[1]);
        map.put("levelID", val[2]);
        map.put("ledgerCode", val[3]);
        map.put("amountValues", amountValues);
        map.put("accTypeValues", accTypeValues);
        map.put("narrationValues", narrationValues);
        map.put("voucherType", "%PAYMENT%");
        code = (String) session.queryForObject("finance.getVoucerCode", map);
        temp = code.split(delimiter);
        System.out.println("temp[1] = " + temp[1]);
        int codeval = Integer.parseInt(temp[1]);
        int codev = codeval + 1;
        System.out.println("codev = " + codev);
        String voucherCode = "PAYMENT-" + codev;
        System.out.println("voucherCode = " + voucherCode);

        if (detailCode == 1) {
            map.put("voucherCode", voucherCode);
        } else {
            map.put("voucherCode", code);
        }

        System.out.println("insert journal in DAO...");
        System.out.println("userId" + userId);
        System.out.println("date" + fromDate);
        System.out.println("bankid1Values" + bankid1Values);
        System.out.println("amountValues" + amountValues);
        System.out.println("accTypeValues" + accTypeValues);
        System.out.println("narrationValues" + narrationValues);

        System.out.println("map = " + map);
        try {
            int tempDetailCode = detailCode + 1;
            System.out.println("tempDetailCode = " + tempDetailCode);
            if (tempDetailCode == cashDetailCode) {
                status = (Integer) session.insert("finance.insertPaymentEntry", map);
                map.put("detailCode", cashDetailCode);
                map.put("ledgerCode", ThrottleConstants.CashOnHandLedgerCode);
                map.put("ledgerID", ThrottleConstants.CashOnHandLedgerId);
                map.put("amountValues", totalCreditAmt);
                map.put("accTypeValues", "CREDIT");
                map.put("levelID", ThrottleConstants.CashOnHandLevelId);
                System.out.println("map = 1 line " + map);
                status = (Integer) session.insert("finance.insertPaymentEntry", map);
            } else {
                System.out.println("map = 2 line " + map);
                status = (Integer) session.insert("finance.insertPaymentEntry", map);
            }
            System.out.println("status in DAO= " + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertPaymentEntry Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertPaymentEntry", sqlException);
        }
        return status;
    }

    public int insertReceiptEntry(String totalDebitAmt, int cashDetailCode, int detailCode, int userId, String fromDate, String bankid1Values, String amountValues, String accTypeValues, String narrationValues, int voucherNo, int formId, String voucherCodeNo, SqlMapClient session) throws SQLException {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        String code = "";
        String delimiter = "-";
        String[] temp;
        int status = 0;

        /**
         * ***************getFormVoucherNo******************
         */
        map.put("voucherCodeNo", voucherCodeNo);
        map.put("formId", formId);
        map.put("voucherNo", voucherNo);
        map.put("updateStatus", "0");
        /**
         * *******************
         */
        map.put("entryStatus", "CR");
        map.put("detailCode", detailCode);
        map.put("userId", userId);
        map.put("date", fromDate);
        map.put("bankid1Values", bankid1Values);
        map.put("totalDebitAmt", totalDebitAmt);
        System.out.println("bankid1Values = " + bankid1Values);
        String[] val = bankid1Values.split("~");
        map.put("ledgerID", val[0]);
        map.put("groupCode", val[1]);
        map.put("levelID", val[2]);
        map.put("ledgerCode", val[3]);

        map.put("amountValues", amountValues);
        map.put("accTypeValues", accTypeValues);
        map.put("narrationValues", narrationValues);
        map.put("voucherType", "%RECEIPT%");

        code = (String) session.queryForObject("finance.getVoucerCode", map);
        temp = code.split(delimiter);
        System.out.println("temp[1] = " + temp[1]);
        int codeval = Integer.parseInt(temp[1]);
        int codev = codeval + 1;
        System.out.println("codev = " + codev);
        String voucherCode = "RECEIPT-" + codev;
        System.out.println("voucherCode = " + voucherCode);

        if (detailCode == 1) {
            map.put("voucherCode", voucherCode);
        } else {
            map.put("voucherCode", code);
        }

        System.out.println("insert journal in DAO...");
        System.out.println("userId" + userId);
        System.out.println("date" + fromDate);
        System.out.println("bankid1Values" + bankid1Values);
        System.out.println("amountValues" + amountValues);
        System.out.println("accTypeValues" + accTypeValues);
        System.out.println("narrationValues" + narrationValues);

        System.out.println("map = " + map);
        try {

            int tempDetailCode = detailCode + 1;
            System.out.println("tempDetailCode = " + tempDetailCode);
            if (tempDetailCode == cashDetailCode) {
                status = (Integer) session.update("finance.insertReceiptEntry", map);
                map.put("detailCode", cashDetailCode);
                map.put("ledgerCode", ThrottleConstants.CashOnHandLedgerCode);
                map.put("ledgerID", ThrottleConstants.CashOnHandLedgerId);
                map.put("amountValues", totalDebitAmt);
                map.put("accTypeValues", "DEBIT");
                map.put("levelID", ThrottleConstants.CashOnHandLevelId);
                System.out.println("map = 1 line " + map);
                status = (Integer) session.update("finance.insertReceiptEntry", map);
            } else {
                System.out.println("map = 2 line " + map);
                status = (Integer) session.update("finance.insertReceiptEntry", map);
            }
            System.out.println("status in DAO= " + status);

            // status = (Integer) getSqlMapClientTemplate().update("finance.insertReceiptEntry", map);
            // System.out.println("status in DAO= " + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertReceiptEntry Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertReceiptEntry", sqlException);
        }
        return status;
    }

    public int insertBankPaymentEntry(String chequeDate, String chequeNo, String BankHead, String totalCreditAmt, int cashDetailCode, int detailCode, int userId, String fromDate, String bankid1Values, String amountValues, String accTypeValues, String narrationValues, int voucherNo, int formId, String voucherCodeNo, SqlMapClient session) throws SQLException {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        String code = "";
        String delimiter = "-";
        String[] temp;
        int status = 0;

        /**
         * ***************getFormVoucherNo******************
         */
        map.put("voucherCodeNo", voucherCodeNo);
        map.put("formId", formId);
        map.put("voucherNo", voucherNo);
        map.put("updateStatus", "0");
        /**
         * *******************
         */

        map.put("entryStatus", "BP");
        map.put("chequeDate", chequeDate);
        map.put("chequeNo", chequeNo);
        map.put("BankHead", BankHead);
        map.put("detailCode", detailCode);
        map.put("userId", userId);
        map.put("date", fromDate);
        map.put("bankid1Values", bankid1Values);
        map.put("totalCreditAmt", totalCreditAmt);
        String[] val = bankid1Values.split("~");
        map.put("ledgerID", val[0]);
        map.put("groupCode", val[1]);
        map.put("levelID", val[2]);
        map.put("ledgerCode", val[3]);
        map.put("amountValues", amountValues);
        map.put("accTypeValues", accTypeValues);
        map.put("narrationValues", narrationValues);
        map.put("voucherType", "%PAYMENT%");
        code = (String) session.queryForObject("finance.getVoucerCode", map);
        temp = code.split(delimiter);
        System.out.println("temp[1] = " + temp[1]);
        int codeval = Integer.parseInt(temp[1]);
        int codev = codeval + 1;
        System.out.println("codev = " + codev);
        String voucherCode = "PAYMENT-" + codev;
        System.out.println("voucherCode = " + voucherCode);

        if (detailCode == 1) {
            map.put("voucherCode", voucherCode);
        } else {
            map.put("voucherCode", code);
        }

        System.out.println("insert journal in DAO...");
        System.out.println("userId" + userId);
        System.out.println("date" + fromDate);
        System.out.println("bankid1Values" + bankid1Values);
        System.out.println("amountValues" + amountValues);
        System.out.println("accTypeValues" + accTypeValues);
        System.out.println("narrationValues" + narrationValues);

        System.out.println("map = " + map);
        try {
            int tempDetailCode = detailCode + 1;
            System.out.println("tempDetailCode = " + tempDetailCode);
            if (tempDetailCode == cashDetailCode) {
                status = (Integer) session.insert("finance.insertPaymentEntry", map);
                String[] LedgerVal = BankHead.split("~");
                map.put("ledgerID", LedgerVal[0]);
                map.put("groupCode", LedgerVal[1]);
                map.put("levelID", LedgerVal[2]);
                map.put("ledgerCode", LedgerVal[3]);
                map.put("detailCode", cashDetailCode);
                map.put("amountValues", totalCreditAmt);
                map.put("accTypeValues", "CREDIT");
                System.out.println("map = 1 line " + map);
                status = (Integer) session.insert("finance.insertPaymentEntry", map);
            } else {
                System.out.println("map = 2 line " + map);
                status = (Integer) session.insert("finance.insertPaymentEntry", map);
            }
            System.out.println("status in DAO= " + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertPaymentEntry Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertPaymentEntry", sqlException);
        }
        return status;
    }

    public int insertBankReceiptEntry(String chequeDate, String chequeNo, String BankHead, String totalDebitAmt, int cashDetailCode, int detailCode, int userId, String fromDate, String bankid1Values, String amountValues, String accTypeValues, String narrationValues, int voucherNo, int formId, String voucherCodeNo, SqlMapClient session) throws SQLException {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        String code = "";
        String delimiter = "-";
        String[] temp;
        int status = 0;
        /**
         * ***************getFormVoucherNo******************
         */
        map.put("voucherCodeNo", voucherCodeNo);
        map.put("formId", formId);
        map.put("voucherNo", voucherNo);
        map.put("updateStatus", "0");
        /**
         * *******************
         */
        map.put("entryStatus", "BR");
        map.put("chequeDate", chequeDate);
        map.put("chequeNo", chequeNo);
        map.put("BankHead", BankHead);
        map.put("detailCode", detailCode);
        map.put("userId", userId);
        map.put("date", fromDate);
        map.put("bankid1Values", bankid1Values);
        String[] val = bankid1Values.split("~");
        map.put("ledgerID", val[0]);
        map.put("groupCode", val[1]);
        map.put("levelID", val[2]);
        map.put("ledgerCode", val[3]);
        map.put("amountValues", amountValues);
        map.put("accTypeValues", accTypeValues);
        map.put("narrationValues", narrationValues);
        map.put("voucherType", "%RECEIPT%");
        code = (String) session.queryForObject("finance.getVoucerCode", map);
        temp = code.split(delimiter);
        System.out.println("temp[1] = " + temp[1]);
        int codeval = Integer.parseInt(temp[1]);
        int codev = codeval + 1;
        System.out.println("codev = " + codev);
        String voucherCode = "RECEIPT-" + codev;
        System.out.println("voucherCode = " + voucherCode);
        if (detailCode == 1) {
            map.put("voucherCode", voucherCode);
        } else {
            map.put("voucherCode", code);
        }
        System.out.println("insert RECEIPT in DAO...");
        System.out.println("userId" + userId);
        System.out.println("date" + fromDate);
        System.out.println("bankid1Values" + bankid1Values);
        System.out.println("amountValues" + amountValues);
        System.out.println("accTypeValues" + accTypeValues);
        System.out.println("narrationValues" + narrationValues);
        System.out.println("map = " + map);
        try {
            int tempDetailCode = detailCode + 1;
            System.out.println("tempDetailCode = " + tempDetailCode);
            if (tempDetailCode == cashDetailCode) {
//                status = (Integer) getSqlMapClientTemplate().update("finance.insertPaymentEntry", map);
                status = (Integer) session.update("finance.insertReceiptEntry", map);
                String[] LedgerVal = BankHead.split("~");
                map.put("ledgerID", LedgerVal[0]);
                map.put("groupCode", LedgerVal[1]);
                map.put("levelID", LedgerVal[2]);
                map.put("ledgerCode", LedgerVal[3]);
                map.put("detailCode", cashDetailCode);
                map.put("amountValues", totalDebitAmt);
                map.put("accTypeValues", "DEBIT");
                System.out.println("map = 1 line " + map);
//                status = (Integer) getSqlMapClientTemplate().update("finance.insertPaymentEntry", map);
                status = (Integer) session.update("finance.insertReceiptEntry", map);
            } else {
                System.out.println("map = 2 line " + map);
//                status = (Integer) getSqlMapClientTemplate().update("finance.insertPaymentEntry", map);
                status = (Integer) session.update("finance.insertReceiptEntry", map);
            }
            System.out.println("status in DAO= " + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertReceiptEntry Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertReceiptEntry", sqlException);
        }
        return status;
    }

    public int insertContraEntry(int cashDetailCode, int detailCode, int userId, String fromDate, String bankid1Values, String amountValues, String accTypeValues, String narrationValues, int voucherNo, int formId, String voucherCode, String voucherCodeNo, SqlMapClient session) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        String code = "";
        String delimiter = "-";
        String[] temp;
        int status = 0;

        /**
         * ***************getFormVoucherNo******************
         */
        map.put("voucherCodeNo", voucherCodeNo);
        map.put("formId", formId);
        map.put("voucherNo", voucherNo);
        map.put("updateStatus", "0");
        /**
         * *******************
         */

        map.put("entryStatus", "CE");
        map.put("detailCode", detailCode);
        map.put("userId", userId);
        map.put("date", fromDate);
        map.put("bankid1Values", bankid1Values);

        String[] val = bankid1Values.split("~");
        map.put("ledgerID", val[0]);
        map.put("groupCode", val[1]);
        map.put("levelID", val[2]);
        map.put("ledgerCode", val[3]);

        map.put("amountValues", amountValues);
        map.put("accTypeValues", accTypeValues);
        map.put("narrationValues", narrationValues);
        System.out.println("map = " + map);
        System.out.println("---------------");
        map.put("voucherType", "%CONTRA%");

        /*  Commented By Arularasan as on 24-10-2103
         *  for here code value must be increamented by twice
         *
         code = (String) getSqlMapClientTemplate().queryForObject("finance.getVoucerCode", map);
         temp = code.split(delimiter);
         System.out.println("temp[1] = " + temp[1]);
         int codeval = Integer.parseInt(temp[1]);
         int codev = codeval + 1;
         System.out.println("codev = " + codev);
         String voucherCode = "CONTRA-" + codev;
         System.out.println("voucherCode = " + voucherCode);
         *
         */
        map.put("voucherCode", voucherCode);

        System.out.println("insert contra in DAO...");
        System.out.println("userId" + userId);
        System.out.println("date" + fromDate);
        System.out.println("bankid1Values" + bankid1Values);
        System.out.println("amountValues" + amountValues);
        System.out.println("accTypeValues" + accTypeValues);
        System.out.println("narrationValues" + narrationValues);

        System.out.println("map = " + map);
        try {
            status = (Integer) session.update("finance.insertContraEntry", map);
            System.out.println("status in DAO= " + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContraEntry Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertContraEntry", sqlException);
        }
        return status;
    }

    public int insertJournalEntry(int cashDetailCode, int detailCode, int userId, String fromDate, String bankid1Values, String amountValues, String accTypeValues, String narrationValues, int voucherNo, int formId, String voucherCode, String voucherCodeNo, SqlMapClient session) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        String code = "";
        String[] temp;
        int status = 0;

        /**
         * ***************getFormVoucherNo******************
         */
        map.put("voucherCodeNo", voucherCodeNo);
        map.put("formId", formId);
        map.put("voucherNo", voucherNo);
        map.put("updateStatus", "0");
        /**
         * *******************
         */

        map.put("entryStatus", "JE");
        map.put("detailCode", detailCode);
        map.put("userId", userId);
        map.put("date", fromDate);
        map.put("bankid1Values", bankid1Values);

        String[] val = bankid1Values.split("~");
        map.put("ledgerID", val[0]);
        map.put("groupCode", val[1]);
        map.put("levelID", val[2]);
        map.put("ledgerCode", val[3]);
        map.put("amountValues", amountValues);
        map.put("accTypeValues", accTypeValues);
        map.put("narrationValues", narrationValues);
        map.put("voucherCode", voucherCode);
        System.out.println("insert journal in DAO...");
        System.out.println("userId" + userId);
        System.out.println("date" + fromDate);
        System.out.println("bankid1Values" + bankid1Values);
        System.out.println("amountValues" + amountValues);
        System.out.println("accTypeValues" + accTypeValues);
        System.out.println("narrationValues" + narrationValues);
        System.out.println("map = " + map);
        try {
            status = (Integer) session.update("finance.insertJournalEntry", map);

            System.out.println("status in DAO= " + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJournalEntry Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJournalEntry", sqlException);
        }
        return status;
    }

    public String getVoucherCode(String val, SqlMapClient session) {
        Map map = new HashMap();
        String journalCode = "";
        String voucherCode = "";
        String contraCode = "";

        String temp[] = null;
        try {
            if (val.equalsIgnoreCase("contra")) {
                map.put("voucherType", "%CONTRA%");
                if ((String) session.queryForObject("finance.getVoucerCode", map) != null) {
                    contraCode = (String) session.queryForObject("finance.getVoucerCode", map);
                    temp = contraCode.split("-");
                    int codeVal = Integer.parseInt(temp[1]);
                    int codev = codeVal + 1;
                    voucherCode = "CONTRA-" + codev;
                } else {
                    voucherCode = "CONTRA-" + 1;
                }
            } else {
                map.put("voucherType", "%JOURNAL%");
                if ((String) session.queryForObject("finance.getVoucerCode", map) != null) {
                    journalCode = (String) session.queryForObject("finance.getVoucerCode", map);
                    temp = journalCode.split("-");
                    int codeVal = Integer.parseInt(temp[1]);
                    int codev = codeVal + 1;
                    voucherCode = "JOURNAL-" + codev;
                } else {
                    voucherCode = "JOURNAL-" + 1;
                }
            }
            System.out.println("voucherCode = " + voucherCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJournalEntryList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getjournalEntryList", sqlException);
        }
        return voucherCode;
    }

    public String getBankBalance(FinanceTO financeTO) {
        Map map = new HashMap();
        map.put("ledgerId", financeTO.getBankLedgerId());
        String bankBalance = "";
        System.out.println("map new = " + map);
        try {
            bankBalance = (String) getSqlMapClientTemplate().queryForObject("finance.getBankBalance", map);
            System.out.println("bankBalance" + bankBalance);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("checkFinYear Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "checkFinYear", sqlException);
        }
        return bankBalance;
    }

    public int insertBRSDetails(FinanceTO financeTO, int userId, String fromDate, String branchId, String bankledgerId, String toDate, String perStatement,
            String bankBalance, String diffrenceAmt, String grandTotal, String totalReceivedAmount, String totalIssuedAmount, String bankId, String issuedVoucherCode[], String issuedLedgerNameId[], String issuedChequeNo[], String issuedChequeDate[], String issuedClearanceDate[], String issuedChequeAmount[],
            String receivedVoucherCode[], String receivedLedgerNameId[], String receivedChequeNo[], String receivedChequeDate[], String receivedClearanceDate[], String receivedChequeAmount[],
            String issuedClearanceId[], String receivedClearanceId[], String creditPaidTo[], String creditDate[], String creditChequeNo[], String creditChequeDate[],
            String creditReference[], String creditAmount[], String debitReceivedFrom[], String debitDate[], String debitChequeNo[], String debitChequeDate[], String debitReference[], String debitAmount[], SqlMapClient session) {
        Map map = new HashMap();
        System.out.println("insertBRSDetails dao");
        /*
         * set the parameters in the map for sending to ORM
         */
        int brsId = 0;
        int status = 0;
        try {

            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("branchId", branchId);
            map.put("bankId", bankId);
            map.put("bankledgerId", bankledgerId);
            map.put("perStatement", perStatement);
            map.put("bankBalance", bankBalance);
            map.put("diffrenceAmt", diffrenceAmt);
            map.put("grandTotal", grandTotal);
            map.put("totalReceivedAmount", totalReceivedAmount);
            map.put("totalIssuedAmount", totalIssuedAmount);

            System.out.println("map insertBRSDetails " + map);
            // try {
            brsId = (Integer) session.insert("finance.insertBRSDetails", map);
            // } catch (SQLException ex) {
            //     Logger.getLogger(FinanceDAO.class.getName()).log(Level.SEVERE, null, ex);
            // }
            if (brsId > 0) {
                for (int i = 0; i < issuedLedgerNameId.length; i++) {
                    map.put("brsId", brsId);
                    map.put("VoucherCode", issuedVoucherCode[i]);
                    map.put("ledgerNameId", issuedLedgerNameId[i]);
                    map.put("chequeNo", issuedChequeNo[i]);
                    map.put("chequeAmount", issuedChequeAmount[i]);
                    map.put("chequeDate", issuedChequeDate[i]);
                    map.put("clearanceDate", issuedClearanceDate[i]);
                    map.put("clearanceId", issuedClearanceId[i]);
                    status = (Integer) session.update("finance.insertBRSIssuedDetails", map);
                }
                for (int i = 0; i < receivedLedgerNameId.length; i++) {
                    map.put("brsId", brsId);
                    map.put("VoucherCode", receivedVoucherCode[i]);
                    map.put("ledgerNameId", receivedLedgerNameId[i]);
                    map.put("chequeNo", receivedChequeNo[i]);
                    map.put("chequeAmount", receivedChequeAmount[i]);
                    map.put("chequeDate", receivedChequeDate[i]);
                    map.put("clearanceDate", receivedClearanceDate[i]);
                    map.put("clearanceId", receivedClearanceId[i]);
                    status = (Integer) session.update("finance.insertBRSReceivedDetails", map);
                }

                for (int i = 0; i < creditAmount.length; i++) {
                    map.put("brsId", brsId);
                    map.put("creditPaidTo", creditPaidTo[i]);
                    map.put("creditDate", creditDate[i]);
                    map.put("creditChequeNo", creditChequeNo[i]);
                    map.put("creditChequeDate", creditChequeDate[i]);
                    map.put("creditReference", creditReference[i]);
                    map.put("creditAmount", creditAmount[i]);
                    status = (Integer) session.update("finance.insertBRSCreditDetails", map);
                }

                for (int i = 0; i < debitAmount.length; i++) {
                    map.put("brsId", brsId);
                    map.put("debitReceivedFrom", debitReceivedFrom[i]);
                    map.put("debitDate", debitDate[i]);
                    map.put("debitChequeNo", debitChequeNo[i]);
                    map.put("debitChequeDate", debitChequeDate[i]);
                    map.put("debitReference", debitReference[i]);
                    map.put("debitAmount", debitAmount[i]);
                    status = (Integer) session.update("finance.insertBRSDebitDetails", map);
                }

            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveInvoicePaymentDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveInvoicePaymentDetail", sqlException);
        }
        return brsId;
    }

    public ArrayList getDayBookAmountList(String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList dayBookAmountList = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        try {
            System.out.println("dayBookAmountList" + map);
            dayBookAmountList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getDayBookAmountList", map);
            System.out.println("dayBookAmountList DAO= " + dayBookAmountList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getLedgerTransactionList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getLedgerTransactionList", sqlException);
        }
        return dayBookAmountList;
    }

    public ArrayList getDayBook(String fromDate, String toDate, int startIndex, int endIndex, FinanceTO financeTO) {
        Map map = new HashMap();
        ArrayList ledgerTransactionList = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("startIndex", startIndex);
        map.put("endIndex", endIndex);

        try {
            System.out.println("ledgerTransactionList" + map);
            ledgerTransactionList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getDayBook", map);
            System.out.println("ledgerTransactionList DAO= " + ledgerTransactionList);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getLedgerTransactionList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getLedgerTransactionList", sqlException);
        }
        return ledgerTransactionList;
    }

    public ArrayList getVoucherMasterList() {
        Map map = new HashMap();
        ArrayList voucherMasterList = new ArrayList();
        try {
            System.out.println("getVoucherMasterList:" + map);
            voucherMasterList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getVoucherMasterList", map);
            System.out.println("getVoucherMasterList DAO= " + voucherMasterList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getLedgerTransactionList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getLedgerTransactionList", sqlException);
        }
        return voucherMasterList;
    }

    public ArrayList getTBTransactionNewList(String levelId, String sDate, String eDate, String val, FinanceTO financeTO) {
        Map map = new HashMap();
        if (sDate == null) {
            sDate = "";
        }
        if (eDate == null) {
            eDate = "";
        }
        map.put("fromDate", sDate);
        map.put("toDate", eDate);
        map.put("levelId", levelId);
        map.put("val", val);
        ArrayList ledgerTransactionList = new ArrayList();
        try {
            System.out.println("getTBTransactionNewList:" + map);
            if ("popup".equals(val)) {
                map.put("startIndex", financeTO.getStartIndex());
                map.put("endIndex", financeTO.getEndIndex());
                ledgerTransactionList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getTBTransactionnew", map);
            } else if ("summary".equals(val)) {
                ledgerTransactionList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getTBTransactionNewList", map);
                //          ledgerTransactionList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getTBTransactionList", map);
            } else if ("dateWise".equals(val)) {
                ledgerTransactionList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getTBTransactiondateWiseList", map);
            }
            System.out.println("getTBTransactionNewList DAO= " + ledgerTransactionList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getLedgerTransactionList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getLedgerTransactionList", sqlException);
        }
        return ledgerTransactionList;
    }

    public ArrayList getPrimaryHistoryList(String toDate) {
        Map map = new HashMap();
        ArrayList levelList = new ArrayList();
//        map.put("levelId",levelId);
        if (toDate != null) {
            map.put("toDate", toDate);
        } else {
            map.put("toDate", "");
        }
        System.out.println("getLevelListNew...map: " + map);
        try {
            levelList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getPrimaryHistoryList", map);
            System.out.println("getList size is::" + levelList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getLevelGroupCode Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getLevelGroupCode", sqlException);
        }
        return levelList;
    }

    public ArrayList getGroupHistoryList(String toDate) {
        Map map = new HashMap();
        ArrayList levelList = new ArrayList();
//        map.put("levelId",levelId);
        if (toDate != null) {
            map.put("toDate", toDate);
        } else {
            map.put("toDate", "");
        }
        System.out.println("getLevelListNew...map: " + map);
        try {
            levelList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getGroupHistoryList", map);
            System.out.println("getList size is::" + levelList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getLevelGroupCode Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getLevelGroupCode", sqlException);
        }
        return levelList;
    }

    public ArrayList getEntriesReport(FinanceTO financeTO, String distcheck) {
        Map map = new HashMap();
        ArrayList entriesList = new ArrayList();
        map.put("fromDate", financeTO.getFromDate());
        map.put("toDate", financeTO.getToDate());
        map.put("entryType", financeTO.getEntryType());
        map.put("ledgerId", financeTO.getLedgerId());
        map.put("distcheck", distcheck);
        try {
            System.out.println(" getVoucherNo map =" + map);
            entriesList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getEntriesReport", map);
            System.out.println(" getEntriesReport =" + entriesList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("fromLocationList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "driverList", sqlException);
        }

        return entriesList;
    }

    public ArrayList getEntriesReportCreditList(FinanceTO financeTO, String distcheck) {
        Map map = new HashMap();
        ArrayList entriesList = new ArrayList();
        map.put("fromDate", financeTO.getFromDate());
        map.put("toDate", financeTO.getToDate());
        map.put("entryType", financeTO.getEntryType());
        map.put("ledgerId", financeTO.getLedgerId());
        try {

            System.out.println(" getVoucherNo map =" + map);
            entriesList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getEntriesReportCreditList", map);
            System.out.println(" getEntriesReport =" + entriesList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("fromLocationList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "driverList", sqlException);
        }

        return entriesList;
    }

    public ArrayList getEntriesReportDebitList(FinanceTO financeTO, String distcheck) {
        Map map = new HashMap();
        ArrayList entriesList = new ArrayList();
        map.put("fromDate", financeTO.getFromDate());
        map.put("toDate", financeTO.getToDate());
        map.put("entryType", financeTO.getEntryType());
        map.put("ledgerId", financeTO.getLedgerId());
        try {
            System.out.println(" getVoucherNo map =" + map);
            entriesList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getEntriesReportDebitList", map);
            System.out.println(" getEntriesReport =" + entriesList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("fromLocationList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "driverList", sqlException);
        }

        return entriesList;
    }

    public ArrayList getLevelListNew(FinanceTO lData1) {
        Map map = new HashMap();
        ArrayList levelList = new ArrayList();
        map.put("groupID", lData1.getGroupID());
        map.put("refId", lData1.getLevelgroupId());
        if (lData1.getFromDate() != null) {
            map.put("fromDate", lData1.getFromDate());
        } else {
            map.put("fromDate", "");
        }
        if (lData1.getToDate() != null) {
            map.put("toDate", lData1.getToDate());
        } else {
            map.put("toDate", "");
        }
//        if(Integer.parseInt(lData1.getGroupID())== Integer.parseInt(lData1.getLevelgroupId())){
//            map.put("test", "1"); 
//        }else{
//           map.put("test", "2"); 
//        }
        System.out.println("getLevelListNew...map: " + map);

        try {
            //levelList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getLevelList", map);
            levelList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getLevelListNewList", map);

            System.out.println("getLevelListNew size is::" + levelList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getLevelGroupCode Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getLevelGroupCode", sqlException);
        }
        return levelList;
    }

    public ArrayList getTBTransactionDayBkSummary(String sDate, String eDate, FinanceTO financeTO) {
        Map map = new HashMap();
        if (sDate == null) {
            sDate = "";
        }
        if (eDate == null) {
            eDate = "";
        }
        map.put("fromDate", sDate);
        map.put("toDate", eDate);
        ArrayList ledgerTransactionList = new ArrayList();
        try {
            System.out.println("getTBTransactionDayBkSummary:" + map);
            ledgerTransactionList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getTBTransactionDayBkSummary", map);
            System.out.println("getTBTransactionNewList DAO= " + ledgerTransactionList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getLedgerTransactionList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getLedgerTransactionList", sqlException);
        }
        return ledgerTransactionList;
    }

    public ArrayList handlePrimaryList(String fromDate, String toDate) {
        System.out.println("in DAO::::::");
        Map map = new HashMap();
        ArrayList primaryLest = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        try {
            primaryLest = (ArrayList) getSqlMapClientTemplate().queryForList("finance.handlePrimaryListNew", map);//pavi
//            primaryLest = (ArrayList) getSqlMapClientTemplate().queryForList("finance.handlePrimaryList", map);
            System.out.println("primaryLest size is::" + primaryLest.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("handlePrimaryList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "handlePrimaryList", sqlException);
        }
        return primaryLest;
    }

    public ArrayList getCostCenterReport(FinanceTO financeTO, String distcheck) {
        Map map = new HashMap();
        ArrayList costCenterReportList = new ArrayList();
//        map.put("voucherCode", financeTO.getVoucherCode());
        map.put("fromDate", financeTO.getFromDate());
        map.put("toDate", financeTO.getToDate());
        map.put("vehicleId", financeTO.getVehicleId());
        map.put("ledgerId", financeTO.getLedgerId());
        map.put("accountType", financeTO.getAccountType());
        try {
            System.out.println(" getVoucherNo map =" + map);
            if ("distDetail".equals(distcheck)) {
                costCenterReportList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getCostCenterReportForDateWise", map);
                System.out.println(" costCenterReportList =" + costCenterReportList.size());
            } else {
                costCenterReportList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getCostCenterReportForSummary", map);
                System.out.println(" costCenterReportList =" + costCenterReportList.size());
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("fromLocationList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "driverList", sqlException);
        }

        return costCenterReportList;
    }

    public ArrayList getBRSHeaderList(FinanceTO financeTO, String val) {
        Map map = new HashMap();
        ArrayList brsHeaderList = new ArrayList();
        map.put("fromDate", financeTO.getFromDate());
        map.put("toDate", financeTO.getToDate());
        map.put("branchId", financeTO.getBranchId());
        map.put("bankId", financeTO.getBankid());
        map.put("brsId", financeTO.getBrsId());
        map.put("val", val);
        try {
            System.out.println("creditNoteSearchList map= " + map);
            if ("HEADER".equalsIgnoreCase(val)) {
                brsHeaderList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.brsHeaderList", map);
                System.out.println("getBRSChequeList111111111 DAO= " + val + "------>" + brsHeaderList.size());
            } else if ("PAYMENT".equalsIgnoreCase(val)) {
                brsHeaderList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.brsIssuedList", map);
                System.out.println("brsIssuedList DAO= " + val + "------>" + brsHeaderList.size());
            } else if ("RECEIPT".equalsIgnoreCase(val)) {

                brsHeaderList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.brsReceivedList", map);
                System.out.println("brsReceivedList DAO= " + val + "------>" + brsHeaderList.size());
            } else if ("CREDIT".equalsIgnoreCase(val)) {

                brsHeaderList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.brsCreditList", map);
                System.out.println("brsReceivedList DAO= " + val + "------>" + brsHeaderList.size());
            } else if ("DEBIT".equalsIgnoreCase(val)) {

                brsHeaderList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.brsDebitList", map);
                System.out.println("brsReceivedList DAO= " + val + "------>" + brsHeaderList.size());
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCashLedgerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getCashLedgerDetails", sqlException);
        }
        return brsHeaderList;

    }

    public ArrayList getCRJDetailList(FinanceTO financeTO) {
        Map map = new HashMap();
        ArrayList CRJDetailList = new ArrayList();

        map.put("fromDate", financeTO.getFromDate());
        map.put("toDate", financeTO.getToDate());
        map.put("vendorId", financeTO.getVendorName());
        map.put("vendorTypeId", financeTO.getVendorTypeId());
        map.put("crjCode", financeTO.getCrjCode());
        System.out.println("getCRJDetailList map= " + map);

        try {
            CRJDetailList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.CRJDetailList", map);
            System.out.println("getCRJDetailList DAO= " + CRJDetailList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCashLedgerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getCashLedgerDetails", sqlException);
        }
        return CRJDetailList;

    }

    public ArrayList getLedger(FinanceTO financeTO) {
        Map map = new HashMap();
        ArrayList ledgerList = new ArrayList();
        map.put("ledgerName", "%" + financeTO.getLedgerName() + "%");
        try {
            System.out.println(" getLedger map =" + map);
            ledgerList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getLedger", map);
            System.out.println(" ledgerList =" + ledgerList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("fromLocationList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "driverList", sqlException);
        }
        return ledgerList;
    }

    public ArrayList getContractRateApproveList(String companyId) {
        Map map = new HashMap();
        ArrayList contractRateApproveList = new ArrayList();
        map.put("companyId", companyId);
        try {
            System.out.println(" getLedger map =" + map);
            contractRateApproveList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getContractRateApproveList", map);
            System.out.println(" contractRateApproveList =" + contractRateApproveList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("contractRateApproveList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "contractRateApproveList", sqlException);
        }
        return contractRateApproveList;
    }

    public int saveApproveRejectContract(String contractRateId, String approvalStatus, String remarks, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", userId);
        map.put("approvalStatus", approvalStatus);
        map.put("contractRateId", contractRateId);
        map.put("remarks", remarks);

        try {
            System.out.println("insert saveApproveRejectContract map:" + map);
            status = (Integer) getSqlMapClientTemplate().update("finance.saveApproveRejectContract", map);
        } catch (Exception e) {
            /*
             * Log the exception and propagate to the calling class
             */
            e.printStackTrace();
            FPLogUtils.fpDebugLog("insert saveApproveRejectContract Error" + e.toString());
            FPLogUtils.fpErrorLog("sqlException" + e);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveApproveRejectContract", e);
        }
        return status;
    }

    public ArrayList getVendorRateApproveList(String companyId) {
        Map map = new HashMap();
        ArrayList contractRateApproveList = new ArrayList();
        map.put("companyId", companyId);
        try {
            System.out.println("map### =" + map);
            contractRateApproveList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getVendorRateApproveList", map);
            System.out.println(" contractRateApproveList =" + contractRateApproveList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("contractRateApproveList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "contractRateApproveList", sqlException);
        }
        return contractRateApproveList;
    }

    public int saveApproveRejectVendor(String contractRateId, String approvalStatus, String remarks, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", userId);
        map.put("approvalStatus", approvalStatus);
        map.put("contractRateId", contractRateId);
        map.put("remarks", remarks);

        try {
            System.out.println("insert saveApproveRejectVendor map:" + map);
            status = (Integer) getSqlMapClientTemplate().update("finance.saveApproveRejectVendor", map);
            System.out.println("status" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insert saveApproveRejectVendor Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveApproveRejectVendor", sqlException);
        }
        return status;
    }

    public ArrayList getContractVendorPaymentDetails(String vendorId, String startDate, String endDate, String contractTypeId) {
        Map map = new HashMap();
        ArrayList vendorPaymentList = new ArrayList();
        map.put("vendorId", vendorId);
        map.put("endDate", endDate);
        map.put("contractTypeId", contractTypeId);
        map.put("invoiceMonth", startDate);
        System.out.println("getContractVendorPaymentDetails map" + map);
        try {
            if ("1".equals(contractTypeId)) {

                vendorPaymentList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getContractVendorPaymentDedicate", map);
                System.out.println("vendorPaymentList = " + vendorPaymentList.size());
            } else if ("2".equals(contractTypeId)) {
                vendorPaymentList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getContractVendorPaymentHire", map);
                System.out.println("vendorPaymentList = " + vendorPaymentList.size());
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "vendorList", sqlException);
        }
        return vendorPaymentList;
    }

    public ArrayList getVendorInvoiceTripDetailsList(FinanceTO financeTO) {
        Map map = new HashMap();
        ArrayList vendorPaymentList = new ArrayList();
        map.put("virCode", financeTO.getVirCode());
        System.out.println("getVendorInvoiceTripDetailsList map" + map);
        try {
            if ("2".equals(financeTO.getContractTypeId())) {
                vendorPaymentList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getVendorInvoiceHireTrips", map);

            } else if ("1".equals(financeTO.getContractTypeId())) {
                vendorPaymentList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getVendorInvoiceDedicateTrips", map);
            }
            System.out.println("vendorInvoiceTripDetailsList = " + vendorPaymentList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "PurchaseDAO", "vendorList", sqlException);
        }
        return vendorPaymentList;
    }

    public int insertVendorInvoiceReceipt(FinanceTO financeTO, int userId, String vendorTypeId, String vendorNameId, String invoiceNo, String invoiceDate,
            String invoiceAmount, String remarks, String vendorLedgerId, String vendorInvoiceFile, String fileName,
            String debitLedgerId, String vendorInvoiceId, String contractTypeId, SqlMapClient session) {
        Map map = new HashMap();
        System.out.println("insertVendorInvoiceReceipt dao");
        /*
         * set the parameters in the map for sending to ORM
         */
        int virId = 0;
        int status = 0;
        int formId = 0;
        int voucherNo = 0;
        FileInputStream fis = null;
        try {
            String temp1[] = debitLedgerId.split("~");
            String debitLedgerNameId = temp1[0];
            String debitLedgerCode = temp1[1];
            map.put("vendorTypeId", vendorTypeId);
            map.put("vendorNameId", vendorNameId);
            map.put("invoiceNo", invoiceNo);
            map.put("invoiceDate", invoiceDate);
            map.put("invoiceAmount", invoiceAmount);
            map.put("remarks", remarks);
            map.put("vendorLedgerId", vendorLedgerId);
            map.put("vendorId", vendorNameId);
            map.put("userId", userId);
            map.put("debitLedgerNameId", debitLedgerNameId);
            map.put("contractTypeId", contractTypeId);
            //file
            map.put("fileName", fileName);
            System.out.println("map1111 = " + map);
            System.out.println("actualFilePath = " + vendorInvoiceFile);
            if (vendorInvoiceFile != null & !"".equals(vendorInvoiceFile)) {
                File file = new File(vendorInvoiceFile);
                System.out.println("file = " + file);
                fis = new FileInputStream(file);
                System.out.println("fis = " + fis);
                byte[] uploadfile = new byte[(int) file.length()];
//           System.out.println("uploadfile = " + uploadfile);
                fis.read(uploadfile);
                fis.close();
                map.put("uploadfile", uploadfile);
                //file end  
            } else {
                map.put("uploadfile", "");
            }
            if ("1".equals(contractTypeId)) {
                map.put("invoiceMonth", financeTO.getInvoiceMonth());
                map.put("invoiceYear", financeTO.getInvoiceYear());
            } else {
                map.put("invoiceMonth", "00-00");
                map.put("invoiceYear", "");
            }
            System.out.println("vendorInvoiceId" + vendorInvoiceId);
            System.out.println("the saveFileUploadDetails123455" + map);
            System.out.println("map insertVendorInvoiceReceipt " + map);
            if ("0".equals(vendorInvoiceId)) {
                virId = (Integer) session.insert("finance.insertVendorInvoiceReceipt", map);
                if (virId > 0) {
                    map.put("virId", virId);
                    map.put("code", ThrottleConstants.vendorInvoiceReceipt + ThrottleConstants.financialYear + "-" + virId);
                    status = (Integer) session.update("finance.updateVendorInvoiceCode", map);
                    if (status > 0) {
                        virId = virId;
                    } else {
                        virId = 0;
                    }
                }
            } else {
                map.put("vendorInvoiceId", vendorInvoiceId);
                status = (Integer) session.update("finance.editVendorInvoiceReceipt", map);
                if (status > 0) {
                    virId = Integer.parseInt(vendorInvoiceId);
                } else {
                    virId = 0;
                }
            }

            System.out.println("insertVendorInvoiceReceipt virId" + virId);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveInvoicePaymentDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveInvoicePaymentDetail", sqlException);
        }
        return virId;
    }

    public ArrayList getVirdetailList(FinanceTO financeTO) {
        Map map = new HashMap();
        ArrayList VirdetailList = new ArrayList();

        map.put("fromDate", financeTO.getFromDate());
        map.put("toDate", financeTO.getToDate());
        map.put("vendorId", financeTO.getVendorName());
        map.put("vendorTypeId", financeTO.getVendorTypeId());
        map.put("virCode", financeTO.getVirCode());
        map.put("contractTypeId", financeTO.getContractTypeId());
        System.out.println("getVirdetailList map= " + map);

        try {
            VirdetailList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getVirdetailList", map);
            System.out.println("getVirdetailList DAO= " + VirdetailList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCashLedgerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getCashLedgerDetails", sqlException);
        }
        return VirdetailList;

    }

    public int saveVendorInvoiceTripDetails(FinanceTO financeTO, int userId,
            int virId, String contractTypeId, String invoiceDate, String totalAmount, String vendorLedgerId,
            String debitLedgerId, String remarks, String vendorInvoiceId, SqlMapClient session) {
        Map map = new HashMap();
        System.out.println("saveVendorPayment");
        /*
         * set the parameters in the map for sending to ORM
         */
        int status = 0;
        int vid = 0;
        int formId = 0;
        int voucherNo = 0;
        map.put("userId", userId);
        map.put("contractTypeId", contractTypeId);
        System.out.println("contractTypeId" + contractTypeId);
        System.out.println("vendorInvoiceId" + vendorInvoiceId);
        System.out.println("financeTO.getActiveInds().length" + financeTO.getActiveInds().length);
        try {
            if ("2".equals(contractTypeId)) {
                if ("0".equals(vendorInvoiceId)) {
                    map.put("virCode", ThrottleConstants.vendorInvoiceReceipt + ThrottleConstants.financialYear + "-" + virId);
                } else {
                    map.put("virCode", ThrottleConstants.vendorInvoiceReceipt + ThrottleConstants.financialYear + "-" + vendorInvoiceId);
                    status = (Integer) session.update("finance.deleteVendorInvoiceHireDetails", map);
                    System.out.println("deleteVendorInvoiceHireDetails" + status);
                }
                for (int i = 0; i < financeTO.getActiveInds().length; i++) {
                    if ("Y".equals(financeTO.getActiveInds()[i])) {
                        map.put("vehicleId", financeTO.getVehicleIds()[i]);
                        map.put("vehicleTypeId", financeTO.getVehicleTypeIds()[i]);
                        map.put("containerHire", financeTO.getContainerTypes()[i]);
                        map.put("containerQtyHire", financeTO.getContainerQtys()[i]);
                        map.put("amount", financeTO.getAmounts()[i]);

                        map.put("routeHire", financeTO.getRoutes()[i]);
                        map.put("tripIdHire", financeTO.getTripIds()[i]);
                        System.out.println("saveVendorInvoiceHire map--->[" + i + "]--->" + map);
                        status = (Integer) session.update("finance.saveVendorInvoiceHire", map);
                    }
                }
            } else if ("1".equals(contractTypeId)) {
                if ("0".equals(vendorInvoiceId)) {
                    map.put("virCode", ThrottleConstants.vendorInvoiceReceipt + ThrottleConstants.financialYear + "-" + virId);
                } else {
                    map.put("virCode", ThrottleConstants.vendorInvoiceReceipt + ThrottleConstants.financialYear + "-" + vendorInvoiceId);
                    System.out.println("deleteVendorInvoiceDedicateDetails map" + map);
                    status = (Integer) session.delete("finance.deleteVendorInvoiceDedicateDetails", map);
                    status = (Integer) session.delete("finance.deleteVendorInvoiceDedicateTrips", map);
                    System.out.println("deleteVendorInvoiceDedicateDetails" + status);
                }
                for (int i = 0; i < financeTO.getActiveInds().length; i++) {
                    if ("Y".equals(financeTO.getActiveInds()[i])) {
                        map.put("vehicleId", financeTO.getVehicleIds()[i]);
                        map.put("vehicleTypeId", financeTO.getVehicleTypeIds()[i]);
                        map.put("totRunKM", financeTO.getTotRunKMs()[i]);
                        map.put("allowedKM", financeTO.getAllowedKMs()[i]);
                        map.put("totExtraRunKm", financeTO.getTotExtraRunKms()[i]);
                        map.put("totExtraKMRate", financeTO.getTotExtraKMRates()[i]);
                        map.put("actualRatePerKM", financeTO.getActualRatePerKMs()[i]);
                        map.put("ratePerExtraKm", financeTO.getRatePerExtraKms()[i]);
                        map.put("fixedCost", financeTO.getFixedCosts()[i]);
                        map.put("amount", financeTO.getAmounts()[i]);

                        map.put("noOfTrips", financeTO.getNoOfTrips()[i]);

                        map.put("contractCategory", financeTO.getContractCategorys()[i]);
                        System.out.println("saveVendorInvoiceDedicate map--->[" + i + "]--->" + map);
                        vid = (Integer) session.insert("finance.saveVendorInvoiceDedicate", map);
                        map.put("vid", vid);
                        if (financeTO.getTripIds()[i].contains(",")) {
                            String tripids[] = financeTO.getTripIds()[i].split(",");
                            for (int j = 0; j < tripids.length; j++) {
                                System.out.println("tripids[j]" + tripids[j]);
                                map.put("tripid", tripids[j]);
                                status = (Integer) session.update("finance.saveVendorInvoiceDedicateTrips", map);
                            }
                        } else {
                            System.out.println("tripids[]" + financeTO.getTripIds()[i]);
                            map.put("tripid", financeTO.getTripIds()[i]);
                            status = (Integer) session.update("finance.saveVendorInvoiceDedicateTrips", map);
                        }
                    }
                }
            }

            System.out.println("saveVendorInvoiceTripDetails" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveInvoicePaymentDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveInvoicePaymentDetail", sqlException);
        }
        return status;
    }

    public int saveVendorPayment(FinanceTO financeTO, int userId,
            String virCode, String contractTypeId, String startDate, String endDate, String description,
            String totalAmount, String serviceTax, String rtgs, String actualInvoiceAmount,
            String vendorLedgerId, String debitLedgerId, String accountEntryDate,
            String vehicleIds[], String vehicleTypeIds[],
            String activeInds[], String maxAllowableKMs[], String amounts[], String fixedKmDedicates[], String extraKmDedicates[],
            String ratePerExtraKmDedicates[], String totalFixedCostDedicates[], String additionalCostDedicates[], String noOfTripss[],
            String routeIdFullTruckHires[], String tripIdHires[], SqlMapClient session) {
        Map map = new HashMap();
        System.out.println("saveVendorPayment");
        /*
         * set the parameters in the map for sending to ORM
         */
        int status = 0;
        int formId = 0;
        int voucherNo = 0;
        map.put("userId", userId);
        System.out.println("contractTypeId" + contractTypeId);
        try {

            map.put("virCode", virCode);
            map.put("contractTypeId", contractTypeId);
            map.put("startDate", startDate);
            map.put("endDate", endDate);
            map.put("description", description);
            map.put("totalAmount", totalAmount);
            map.put("serviceTax", serviceTax);
            map.put("actualInvoiceAmount", actualInvoiceAmount);
            map.put("rtgs", rtgs);
            status = (Integer) session.update("finance.updateVendorInvoiceReceipt", map);
            if ("1".equals(contractTypeId)) {
                for (int i = 0; i < activeInds.length; i++) {
                    if ("Y".equals(activeInds[i])) {
                        map.put("vehicleId", vehicleIds[i]);
                        map.put("vehicleTypeId", vehicleTypeIds[i]);
                        map.put("maxAllowableKM", maxAllowableKMs[i]);
                        map.put("amount", amounts[i]);

                        map.put("fixedKmDedicate", fixedKmDedicates[i]);
                        map.put("extraKmDedicate", extraKmDedicates[i]);
                        map.put("ratePerExtraKmDedicate", ratePerExtraKmDedicates[i]);
                        map.put("totalFixedCostDedicate", totalFixedCostDedicates[i]);
                        map.put("additionalCostDedicate", additionalCostDedicates[i]);
                        map.put("noOfTrips", noOfTripss[i]);
                        System.out.println("saveVendorInvoiceDedicate map--->[" + i + "]--->" + map);
                        status = (Integer) session.update("finance.saveVendorInvoiceDedicate", map);
                    }
                }

            } else if ("2".equals(contractTypeId)) {
                System.out.println("activeInds.length" + activeInds.length);
                System.out.println("activeInds val 0" + activeInds[0]);
                for (int i = 0; i < activeInds.length; i++) {
                    if ("Y".equals(activeInds[i])) {
                        map.put("vehicleId", vehicleIds[i]);
                        map.put("vehicleTypeId", vehicleTypeIds[i]);
                        map.put("maxAllowableKM", maxAllowableKMs[i]);
                        map.put("amount", amounts[i]);

                        map.put("routeIdFullTruckHire", routeIdFullTruckHires[i]);
                        map.put("tripIdHire", tripIdHires[i]);
                        System.out.println("saveVendorInvoiceHire map--->[" + i + "]--->" + map);
                        status = (Integer) session.update("finance.saveVendorInvoiceHire", map);
                    }
                }
            }

            System.out.println("status" + status);
            map.put("paymentReferenceCode", virCode);
            map.put("paymentReferenceId", 0);
            map.put("paymentReferenceName", "VIR");
            map.put("paymentAmount", actualInvoiceAmount);
            status = (Integer) session.update("finance.insertPaymentDetails", map);
            /*
             if (status > 0) {
             formId = Integer.parseInt(ThrottleConstants.vendorInvoiceReceiptFormId);
             map.put("formId", formId);
             voucherNo = (Integer) session.insert("finance.getFormVoucherNo", map);
             map.put("voucherNo", voucherNo);
             map.put("voucherCodeNo", ThrottleConstants.vendorInvoiceReceiptVoucherCode + voucherNo);
             map.put("voucherType", "%PAYMENT%");
             String code = "";
             String[] temp;
             code = (String) session.queryForObject("finance.getVoucerCode", map);
             temp = code.split("-");
             int codeval2 = Integer.parseInt(temp[1]);
             int codev2 = codeval2 + 1;
             String voucherCode = "PAYMENT-" + codev2;
             System.out.println("voucherCode = " + voucherCode);

             map.put("detailCode", "1");
             map.put("voucherCode", voucherCode);
             map.put("accountEntryDate", accountEntryDate);
             map.put("mainEntryType", "VOUCHER");
             map.put("entryType", "PAYMENT");
             map.put("ledgerId", debitLedgerId);
             map.put("debitLedgerId", debitLedgerId);
             String getdebitLedgerCode = (String) session.queryForObject("finance.getdebitLedgerCode", map);
             map.put("particularsId", getdebitLedgerCode);
             map.put("amount", actualInvoiceAmount);
             map.put("accountsType", "DEBIT");
             map.put("narration", "VIR");
             map.put("searchCode", virCode);
             map.put("reference", "VIR");

             System.out.println("map1 updateClearnceDate=---------------------> " + map);
             status = (Integer) session.update("finance.insertAccountEntry", map);
             System.out.println("status1 = " + status);
             //--------------------------------- acc 2nd row start --------------------------
             if (status > 0) {
             map.put("detailCode", "2");
             map.put("ledgerId", vendorLedgerId);
             map.put("creditLedgerId", vendorLedgerId);
             String getcreditLedgerCode = (String) session.queryForObject("finance.getcreditLedgerCode", map);
             map.put("particularsId", getcreditLedgerCode);
             map.put("accountsType", "CREDIT");
             System.out.println("map2 updateClearnceDate=---------------------> " + map);
             status = (Integer) session.update("finance.insertAccountEntry", map);
             System.out.println("status2 = " + status);
             }
             }
             */
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveInvoicePaymentDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveInvoicePaymentDetail", sqlException);
        }
        return status;
    }

    public int updateVendorInvoiceTrips(FinanceTO financeTO, int userId,
            String serviceTax, String tds, String actualInvoiceAmount, String description, String virCode,
            String contractTypeId, String virIds[],
            String activeInds[], SqlMapClient session) {
        Map map = new HashMap();
        System.out.println("saveVendorPayment");
        /*
         * set the parameters in the map for sending to ORM
         */
        int status = 0;
        int formId = 0;
        int voucherNo = 0;
        map.put("userId", userId);
        try {

            map.put("virCode", virCode);
            map.put("description", description);

            map.put("tds", tds);
            map.put("actualInvoiceAmount", actualInvoiceAmount);
            if ("9".equalsIgnoreCase(financeTO.getBillingState())) {

                if ("1".equalsIgnoreCase(contractTypeId)) {
                    map.put("serviceTax", String.valueOf(Double.parseDouble(financeTO.getSgstDedicated()) + Double.parseDouble(financeTO.getCgstDedicated())));
                    map.put("taxAmount", String.valueOf(Double.parseDouble(financeTO.getSgstDedicated()) + Double.parseDouble(financeTO.getCgstDedicated())));
                } else {
                    map.put("serviceTax", String.valueOf(Double.parseDouble(financeTO.getSgstHire()) + Double.parseDouble(financeTO.getCgstHire())));
                    map.put("taxAmount", String.valueOf(Double.parseDouble(financeTO.getSgstHire()) + Double.parseDouble(financeTO.getCgstHire())));
                }
                map.put("gstType", "CGST");
                map.put("taxPercentage", financeTO.getCgstTax());

                status = (Integer) session.update("finance.saveInvoiceTax", map);
                map.put("gstType", "SGST");
                map.put("taxPercentage", financeTO.getSgstTax());
                status = (Integer) session.update("finance.saveInvoiceTax", map);

            } else {
                if ("1".equalsIgnoreCase(contractTypeId)) {
                    map.put("serviceTax", financeTO.getIgstDedicated());
                    map.put("taxAmount", financeTO.getIgstDedicated());
                } else {
                    map.put("serviceTax", financeTO.getIgstHire());
                    map.put("taxAmount", financeTO.getIgstHire());
                }
                map.put("gstType", "IGST");
                map.put("taxPercentage", financeTO.getIgstTax());
                status = (Integer) session.update("finance.saveInvoiceTax", map);
            }
            System.out.println("updateVendorInvoiceReceipt map#######" + map);
            status = (Integer) session.update("finance.updateVendorInvoiceReceipt", map);
            for (int i = 0; i < virIds.length; i++) {

                if ("Y".equals(activeInds[i])) {
                    map.put("virId", virIds[i]);
                    System.out.println("updateVendorInvoiceReceipt map--->[" + i + "]--->" + map);
                    if ("2".equals(contractTypeId)) {
                        status = (Integer) session.update("finance.updateVendorInvoiceTrips", map);
                    } else if ("1".equals(contractTypeId)) {
                        status = (Integer) session.update("finance.updateVIRdedicateTrips", map);
                    }
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveInvoicePaymentDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveInvoicePaymentDetail", sqlException);
        }
        return status;
    }

    public ArrayList getTripDetails(FinanceTO financeTO) {
        Map map = new HashMap();
        ArrayList TripDetails = new ArrayList();
        if (financeTO.getTripId().contains(",")) {
            String[] tripSheetIds = financeTO.getTripId().split(",");
            List tripSheetId = new ArrayList(tripSheetIds.length);
            for (int i = 0; i < tripSheetIds.length; i++) {
                System.out.println("value:" + tripSheetIds[i]);
                tripSheetId.add(tripSheetIds[i]);
            }
            map.put("tripids", tripSheetId);
            map.put("test", "1");
        } else {
            map.put("test", "2");
            map.put("tripids", financeTO.getTripId());
        }

        System.out.println("getTripDetails map= " + map);

        try {
            TripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getTripDetails", map);
            System.out.println("getTripDetails DAO= " + TripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCashLedgerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getCashLedgerDetails", sqlException);
        }
        return TripDetails;

    }

    public ArrayList getGSTTaxDetails(FinanceTO financeTO) {
        Map map = new HashMap();
        map.put("vendorId", financeTO.getVendorId());
        System.out.println("getTripDetails map= " + map);
        ArrayList GSTDetails = new ArrayList();
        try {
            GSTDetails = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getGSTTaxDetails", map);
            System.out.println("GSTDetails DAO= " + GSTDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCashLedgerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getCashLedgerDetails", sqlException);
        }
        return GSTDetails;

    }

    public ArrayList getInvoiceTax(FinanceTO financeTO) {
        Map map = new HashMap();
        map.put("virCode", financeTO.getVirCode());
        System.out.println("gettax map map= " + map);
        ArrayList GSTDetails = new ArrayList();
        try {
            GSTDetails = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getInvoiceTax", map);
            System.out.println("tax details DAO= " + GSTDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCashLedgerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getCashLedgerDetails", sqlException);
        }
        return GSTDetails;

    }

    public ArrayList getCreidtNoteDetails(String invoiceId) {
        Map map = new HashMap();
        map.put("invoiceId", invoiceId);
        System.out.println("getCreidtNoteDetails map map= " + map);
        ArrayList creidtNoteDetails = new ArrayList();
        try {
            creidtNoteDetails = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getCreidtNoteDetails", map);
            System.out.println("getCreidtNoteDetails DAO= " + creidtNoteDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCashLedgerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getCashLedgerDetails", sqlException);
        }
        return creidtNoteDetails;

    }

    public String getCreidtNoteNo(String invoiceId) {
        Map map = new HashMap();
        map.put("invoiceId", invoiceId);
        System.out.println("creditNoteNo map map= " + map);
        String creditNoteNo = "";
        try {
            creditNoteNo = (String) getSqlMapClientTemplate().queryForObject("finance.getCreditNoteNo", map);
            System.out.println("creditNoteNo= " + creditNoteNo);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCashLedgerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getCashLedgerDetails", sqlException);
        }
        return creditNoteNo;

    }

    public ArrayList getSupplyInvoiceList(FinanceTO financeTO) {
        Map map = new HashMap();
        ArrayList invoiceList = new ArrayList();
        try {
            map.put("customerId", financeTO.getCustomerId());
            //////System.out.println("getInvoiceList map= " + map);
            invoiceList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getCustomerSupplyInvoiceList", map);
            //////System.out.println("getInvoiceList DAO= " + invoiceList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getInvoiceList", sqlException);
        }
        return invoiceList;
    }

    public ArrayList getSuppInvoiceGRList(FinanceTO financeTO) {
        Map map = new HashMap();
        ArrayList invoiceDetails = new ArrayList();
        try {
            map.put("invoiceId", financeTO.getInvoiceId());
            System.out.println("invoiceDetails map= " + map);
            invoiceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getSuppInvoiceGRList", map);
            System.out.println("invoiceDetails DAO= " + invoiceDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("invoiceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "invoiceDetails", sqlException);
        }
        return invoiceDetails;
    }

    public ArrayList getSuppInvoiceDetailsList(FinanceTO financeTO) {
        Map map = new HashMap();
        ArrayList invoiceDetails = new ArrayList();
        try {
            map.put("invoiceId", financeTO.getInvoiceId());
            System.out.println("invoiceDetails map= " + map);
            invoiceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getSuppInvoiceDetailsList", map);
            System.out.println("invoiceDetails DAO= " + invoiceDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("invoiceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "invoiceDetails", sqlException);
        }
        return invoiceDetails;
    }

    public ArrayList getSuppCreditNoteDetails(String invoiceId) {
        Map map = new HashMap();
        map.put("invoiceId", invoiceId);
        System.out.println("getCreidtNoteDetails map map= " + map);
        ArrayList creidtNoteDetails = new ArrayList();
        try {
            creidtNoteDetails = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getSuppCreidtNoteDetails", map);
            System.out.println("getCreidtNoteDetails DAO= " + creidtNoteDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCashLedgerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getCashLedgerDetails", sqlException);
        }
        return creidtNoteDetails;

    }

    public String getSuppCreditNoteNo(String invoiceId) {
        Map map = new HashMap();
        map.put("invoiceId", invoiceId);
        System.out.println("getSuppCreditNoteNo map map= " + map);
        String creditNoteNo = "";
        try {
            creditNoteNo = (String) getSqlMapClientTemplate().queryForObject("finance.getSuppCreditNoteNo", map);
            System.out.println("creditNoteNo= " + creditNoteNo);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCashLedgerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getCashLedgerDetails", sqlException);
        }
        return creditNoteNo;

    }

    public String saveSupplyCreditNote(FinanceTO financeTO, String[] tripIds, String[] freightAmount, String[] grandTotal, String[] payAmount, String reason, String[] tollCredit, String[] greenTaxCredit, String[] weightmentCredit, String[] detaintionCredit, String[] otherCredit, int userId, SqlMapClient session, String finYear) {
        Map map = new HashMap();
        /*
             * set the parameters in the map for sending to ORM
         */
        int invoiceReceipt = 0;
        int status = 0;
        int formId = 0;
        int voucherNo = 0;
        String creditNoteCode = "";
        String fromDate = "";
        String invoiceCode = "";
        String currentYear = "";
        String InvoiceDate = "";
        String compareDate = "31-03-2018";
        int creditNoteNumber = 0;
        try {
            InvoiceDate = financeTO.getInvoiceDate();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            //            Date d1 = sdf.parse(compareDate);
            //            Date d2 = sdf.parse(InvoiceDate);

            //            creditNoteNumber = (Integer) getSqlMapClientTemplate().insert("finance.getCreditNoteCode1819", map);
            //            creditNoteNumber = (Integer) session.insert("finance.getCreditNoteCode1819", map);
            //            creditNoteCode = "CR1819TPG";
            //if (d2.after(d1)) { // After marc 31st 2020
            if (!"Y".equals(financeTO.getGstType())) {
                if (finYear.equalsIgnoreCase("2324")) { // After marc 31st 2020

                    Calendar now = Calendar.getInstance();
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = new Date();
                    System.out.println("1111111111111 ");
                    fromDate = dateFormat.format(date);
                    creditNoteNumber = (Integer) session.insert("finance.getSuppCreditNoteCodeBos2324", map);
                    
//                  int  lastInvoiceCodeTmp = (Integer) session.queryForObject("finance.getLastSuppCreditNoteCodeBos2223", map);
//                  
//                    System.out.println("lastInvoiceCodeTmp-----"+lastInvoiceCodeTmp);
//                  
//                  int codeSequence = lastInvoiceCodeTmp + 1;
//                  
//                    System.out.println("lastInvoiceCodeTmp-----"+lastInvoiceCodeTmp);
//                    
//                  map.put("invoiceCode", codeSequence);
//                  
//                    codeSequence = (Integer) session.insert("finance.insertSuppCreditNoteCodeSequenceBos2223", map);
//                  
//                  
//                        creditNoteNumber = (Integer) session.queryForObject("finance.getLastSuppCreditNoteCodeBos2223", map);
                  
                  
                    
                    creditNoteCode = "CR2324TBSSU";

                } else { //Befoe
                    System.out.println("22222222 ");
                    fromDate = "2023-03-31 22:00:00";
                    creditNoteNumber = (Integer) session.insert("finance.getSuppCreditNoteCodeBos2223", map);
                    creditNoteCode = "CR2223TBSSU";
                }
                
            } else {
                if (finYear.equalsIgnoreCase("2324")) { // After marc 31st 2020

                    Calendar now = Calendar.getInstance();
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = new Date();
                    System.out.println("1111111111111 ");
                    fromDate = dateFormat.format(date);
                    creditNoteNumber = (Integer) session.insert("finance.getSuppCreditNoteCode2324", map);
                    
                              
//                  int  lastInvoiceCodeTmp = (Integer) session.queryForObject("finance.getLastSuppCreditNoteCode2223", map);
//                  
//                    System.out.println("lastInvoiceCodeTmp-----"+lastInvoiceCodeTmp);
//                  
//                  int codeSequence = lastInvoiceCodeTmp + 1;
//                  
//                    System.out.println("lastInvoiceCodeTmp-----"+lastInvoiceCodeTmp);
//                    
//                  map.put("invoiceCode", codeSequence);
//                  
//                    codeSequence = (Integer) session.insert("finance.insertSuppCreditNoteCodeSequence2223", map);
//                  
//                  
//                        creditNoteNumber = (Integer) session.queryForObject("finance.getLastSuppCreditNoteCode2223", map);
//                  
//                  
                    
                    
                    
                    
                    creditNoteCode = "CR2324TPGSU";

                } else { //Befoe
                    System.out.println("22222222 ");
                    fromDate = "2023-03-31 22:00:00";
                    creditNoteNumber = (Integer) session.insert("finance.getSuppCreditNoteCode2223", map);
                    creditNoteCode = "CR2223TPGSU";
                }
            }

            map.put("amount", financeTO.getPayAmount());
            map.put("customer", financeTO.getCustomerId());
            map.put("invoiceId", financeTO.getInvoiceId());
            map.put("totalInvoiceAmount", financeTO.getInvoiceAmount());

            map.put("remarks", financeTO.getRemarks());
            map.put("gstType", financeTO.getGstType());
            map.put("reason", reason);
            map.put("userId", userId);

            //getCredit NOte Code
            invoiceCode = creditNoteNumber + "";
//                if (invoiceCode.length() == 1) {
//                    invoiceCode = "000000" + invoiceCode;
//                    System.out.println("invoice no 1.." + invoiceCode);
//                } else if (invoiceCode.length() == 2) {
//                    invoiceCode = "00000" + invoiceCode;
//                    System.out.println("invoice lenght 2.." + invoiceCode);
//                } else 
            if (invoiceCode.length() == 1) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "000" + invoiceCode;
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "00" + invoiceCode;
            } else if (invoiceCode.length() == 4) {
                invoiceCode = "0" + invoiceCode;
            }
            creditNoteCode = creditNoteCode + invoiceCode;
            System.out.println("getInvoiceCodeSequence=" + creditNoteCode);

            System.out.println("creditNoteCode:" + creditNoteCode);
            map.put("creditNoteCode", creditNoteCode);
            map.put("fromDate", fromDate);
            System.out.println("creditNote Map:" + map);
            int insertCreditNote = (Integer) session.insert("finance.insertSuppInvoiceCreditNote", map);
            //            int insertCreditNote = (Integer) getSqlMapClientTemplate().insert("finance.insertInvoiceCreditNote", map);
            //            invoiceReceipt = (Integer) getSqlMapClientTemplate().update("finance.updateInvoiceHeader", map);
            invoiceReceipt = (Integer) session.update("finance.updateSuppInvoiceHeader", map);
            if (insertCreditNote > 0) {
                for (int i = 0; i < tripIds.length; i++) {
                    map.put("freightAmount", freightAmount[i]);
                    map.put("tripId", tripIds[i]);
                    map.put("toll", tollCredit[i]);
                    map.put("greenTax", "0");
                    //                   map.put("greenTax",greenTaxCredit[i]);
                    map.put("detaintion", detaintionCredit[i]);
                    map.put("weightment", weightmentCredit[i]);
                    map.put("other", otherCredit[i]);
                    if ("".equalsIgnoreCase(payAmount[i]) || payAmount[i] == null) {
                        payAmount[i] = "0.00";
                    }
                    map.put("creditAmount", payAmount[i]);
                    map.put("creditNoteId", insertCreditNote);
                    System.out.println("insertCreditNoteDetails map:" + map);
                    //                    int insertCreditNoteDetails = (Integer) getSqlMapClientTemplate().update("finance.insertInvoiceCreditNoteDetails", map);
                    int insertCreditNoteDetails = (Integer) session.update("finance.insertSuppInvoiceCreditNoteDetails", map);
                    System.out.println("insertCreditNoteDetails---" + insertCreditNoteDetails);
                }
            }

            // CREDIT NOTE OUTSTANDING CREDIT LIMIT
            map.put("customerId", financeTO.getCustomerId());
            map.put("invoiceId", "0");
            map.put("invoiceAmount", financeTO.getPayAmount());

            map.put("consignmentOrderId", "0");
            map.put("custType", "1");
            map.put("outstandingAmount", financeTO.getPayAmount());
            map.put("totalFreightAmount", financeTO.getPayAmount());
            map.put("creditNoteInvoiceId", "0");
            map.put("supplementrycreditId", insertCreditNote);
            map.put("supplymentryInvoiceId", financeTO.getInvoiceId());
            map.put("paymentId", "0");

            map.put("userId", userId);
            System.out.println("updateInvoiceOustanding----" + map);

            int updateCnoteCount, updateOutstanding, updateCreditLimit, updateUsedLimit, updateOutstandingLog = 0;
            String getCustomerType = (String) session.queryForObject("operation.getCustomerType", map);
            String availBlockedAmount = "";
            String availBlockedAmountTemp[] = null;
            map.put("custType", getCustomerType);
            map.put("transactionName", "SuppCreditNote");

            if ("1".equalsIgnoreCase(getCustomerType)) {   //for credit Customers Only
                availBlockedAmount = (String) session.queryForObject("operation.getAvailandBlockedAmount", map);
                System.out.println("availBlockedAmount------" + availBlockedAmount);
                availBlockedAmountTemp = availBlockedAmount.split("~");
                map.put("fixedLimit", availBlockedAmountTemp[0]);
                map.put("blockedAmount", availBlockedAmountTemp[2]);

                double usedLimit = Double.parseDouble(availBlockedAmountTemp[3]) - Double.parseDouble(financeTO.getPayAmount());
                double availLimit = Double.parseDouble(availBlockedAmountTemp[1]) + Double.parseDouble(financeTO.getPayAmount());

                map.put("availAmount", availLimit);

                if (usedLimit < 0) {
                    map.put("usedLimit", 0);
                } else {
                    map.put("usedLimit", availBlockedAmountTemp[3]);
                }

                updateCreditLimit = (Integer) session.update("operation.updateCreditNoteInvoiceLimit", map);

                map.put("transactionType", "Credit");   // credit
                map.put("debitAmount", "0.00");
                map.put("creditAmount", financeTO.getPayAmount());
                updateOutstandingLog = (Integer) session.update("operation.updateOutstandingLog", map);

            } else if ("2".equalsIgnoreCase(getCustomerType)) {

                availBlockedAmount = (String) session.queryForObject("operation.getAvailandBlockedAmount", map);
                System.out.println("availBlockedAmount------" + availBlockedAmount);
                availBlockedAmountTemp = availBlockedAmount.split("~");
                map.put("fixedLimit", availBlockedAmountTemp[0]);
                map.put("blockedAmount", availBlockedAmountTemp[2]);

                double usedLimit = Double.parseDouble(availBlockedAmountTemp[3]) - Double.parseDouble(financeTO.getPayAmount());
                double availLimit = Double.parseDouble(availBlockedAmountTemp[1]) + Double.parseDouble(financeTO.getPayAmount());

                map.put("availAmount", availLimit);

                if (usedLimit < 0) {
                    map.put("usedLimit", 0);
                } else {
                    map.put("usedLimit", availBlockedAmountTemp[3]);
                }

                updateCreditLimit = (Integer) session.update("operation.updateCreditNoteInvoiceLimit", map);

                map.put("transactionType", "Credit");   // credit
                map.put("debitAmount", "0.00");
                map.put("creditAmount", financeTO.getPayAmount());
                updateOutstandingLog = (Integer) session.update("operation.updateOutstandingLog", map);
            }

            // CREDIT NOTE OUTSTANDING CREDIT LIMIT      
            //            String customerLedgerId = (String) getSqlMapClientTemplate().queryForObject("finance.getCustomerLedgerId", map);
            //            String customerLedgerCode = (String) getSqlMapClientTemplate().queryForObject("finance.getCustomerLedgerCode", map);
            String customerLedgerId = (String) session.queryForObject("finance.getCustomerLedgerId", map);
            String customerLedgerCode = (String) session.queryForObject("finance.getCustomerLedgerCode", map);

            //            invoiceReceipt = (Integer) getSqlMapClientTemplate().update("finance.updateSupplyMaster", map);
            //   System.out.println("status2 = " + status2);
            System.out.println("status final = " + status);
            System.out.println("ThrottleConstants.cashPaymentFormId = " + ThrottleConstants.cashPaymentFormId);
            formId = Integer.parseInt(ThrottleConstants.cashPaymentFormId);
            map.put("formId", formId);
            //            voucherNo = (Integer) getSqlMapClientTemplate().insert("finance.getFormVoucherNo", map);
            voucherNo = (Integer) session.insert("finance.getFormVoucherNo", map);
            map.put("voucherNo", voucherNo);
            map.put("voucherCodeNo", ThrottleConstants.CashPaymentVoucherCode + voucherNo);
            map.put("voucherType", "%PAYMENT%");
            String code = "";
            String[] temp;
            //            code = (String) getSqlMapClientTemplate().queryForObject("finance.getVoucerCode", map);
            code = (String) session.queryForObject("finance.getVoucerCode", map);
            System.out.println("code----" + code);
            int codeval2 = 0;
            if (code == null) {
                codeval2 = 0;
            } else {
                temp = code.split("-");
                codeval2 = Integer.parseInt(temp[1]);
            }
            int codev2 = codeval2 + 1;
            String voucherCode = "PAYMENT-" + codev2;
            System.out.println("voucherCode = " + voucherCode);

            map.put("detailCode", "1");
            map.put("voucherCode", voucherCode);
            map.put("accountEntryDate", financeTO.getReceiptDate());
            map.put("mainEntryType", "VOUCHER");
            map.put("entryType", "PAYMENT");
            map.put("ledgerId", ThrottleConstants.CashOnHandLedgerId);
            map.put("particularsId", ThrottleConstants.CashOnHandLedgerCode);
            map.put("amount", financeTO.getPayAmount());
            map.put("accountsType", "DEBIT");
            map.put("narration", financeTO.getRemarks());
            map.put("searchCode", financeTO.getInvoiceCode());
            map.put("reference", "CREDITNOTE");

            System.out.println("map1 updateClearnceDate=---------------------> " + map);
            //            status = (Integer) getSqlMapClientTemplate().update("finance.insertAccountEntry", map);
            status = (Integer) session.update("finance.insertAccountEntry", map);
            System.out.println("status1 = " + status);
            //--------------------------------- acc 2nd row start --------------------------
            if (status > 0) {
                map.put("detailCode", "2");
                map.put("ledgerId", customerLedgerId);
                map.put("particularsId", customerLedgerCode);
                map.put("accountsType", "CREDIT");
                System.out.println("map2 updateClearnceDate=---------------------> " + map);
                //                status = (Integer) getSqlMapClientTemplate().update("finance.insertAccountEntry", map);
                status = (Integer) session.update("finance.insertAccountEntry", map);
                System.out.println("status2 = " + status);
            }

//                billingTONew.setStatusId("33");
//                map.put("statusId", "33"); // suppcredit NOte
//                status = (Integer) session.update("billing.insertTripStatus", map);
//                sout        
//                status = (Integer) session.update("billing.updateTripStatus", map);
        } catch (Exception sqlException) {
            
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveCreditNote Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveCreditNote", sqlException);
        }
        return creditNoteCode;
    }
    
    
    public String saveSupplyCreditNoteGta(FinanceTO financeTO, String[] tripIds, String[] freightAmount, String[] grandTotal, String[] payAmount, String reason, String[] tollCredit, String[] greenTaxCredit, String[] weightmentCredit, String[] detaintionCredit, String[] otherCredit, int userId, SqlMapClient session, String finYear) {
        Map map = new HashMap();
        /*
             * set the parameters in the map for sending to ORM
         */
        int invoiceReceipt = 0;
        int status = 0;
        int formId = 0;
        int voucherNo = 0;
        String creditNoteCode = "";
        String fromDate = "";
        String invoiceCode = "";
        String currentYear = "";
        String InvoiceDate = "";
        String compareDate = "31-03-2018";
        int creditNoteNumber = 0;
        try {
            InvoiceDate = financeTO.getInvoiceDate();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
          
          if (finYear.equalsIgnoreCase("2324")) { // After marc 31st 2020

                    Calendar now = Calendar.getInstance();
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = new Date();
                    System.out.println("1111111111111 ");
                    fromDate = dateFormat.format(date);
                    creditNoteNumber = (Integer) session.insert("finance.getSuppCreditNoteCodeBos2324", map);
                    creditNoteCode = "CR2324TBSSU";

                } else { //Befoe
                    System.out.println("22222222 ");
                    fromDate = "2023-03-31 22:00:00";
                    creditNoteNumber = (Integer) session.insert("finance.getSuppCreditNoteCodeBos2223", map);
                    creditNoteCode = "CR2223TBSSU";
                }

            map.put("amount", financeTO.getPayAmount());
            map.put("customer", financeTO.getCustomerId());
            map.put("invoiceId", financeTO.getInvoiceId());
            map.put("totalInvoiceAmount", financeTO.getInvoiceAmount());

            map.put("remarks", financeTO.getRemarks());
            map.put("gstType", financeTO.getGstType());
            map.put("reason", reason);
            map.put("userId", userId);

            //getCredit NOte Code
            invoiceCode = creditNoteNumber + "";
//                if (invoiceCode.length() == 1) {
//                    invoiceCode = "000000" + invoiceCode;
//                    System.out.println("invoice no 1.." + invoiceCode);
//                } else if (invoiceCode.length() == 2) {
//                    invoiceCode = "00000" + invoiceCode;
//                    System.out.println("invoice lenght 2.." + invoiceCode);
//                } else 
            if (invoiceCode.length() == 1) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "000" + invoiceCode;
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "00" + invoiceCode;
            } else if (invoiceCode.length() == 4) {
                invoiceCode = "0" + invoiceCode;
            }
            creditNoteCode = creditNoteCode + invoiceCode;
            System.out.println("getInvoiceCodeSequence=" + creditNoteCode);

            System.out.println("creditNoteCode:" + creditNoteCode);
            map.put("creditNoteCode", creditNoteCode);
            map.put("fromDate", fromDate);
            System.out.println("creditNote Map:" + map);
            int insertCreditNote = (Integer) session.insert("finance.insertSuppInvoiceCreditNote", map);
            //            int insertCreditNote = (Integer) getSqlMapClientTemplate().insert("finance.insertInvoiceCreditNote", map);
            //            invoiceReceipt = (Integer) getSqlMapClientTemplate().update("finance.updateInvoiceHeader", map);
            invoiceReceipt = (Integer) session.update("finance.updateSuppInvoiceHeader", map);
            if (insertCreditNote > 0) {
                for (int i = 0; i < tripIds.length; i++) {
                    map.put("freightAmount", freightAmount[i]);
                    map.put("tripId", tripIds[i]);
                    map.put("toll", tollCredit[i]);
                    map.put("greenTax", "0");
                    //                   map.put("greenTax",greenTaxCredit[i]);
                    map.put("detaintion", detaintionCredit[i]);
                    map.put("weightment", weightmentCredit[i]);
                    map.put("other", otherCredit[i]);
                    if ("".equalsIgnoreCase(payAmount[i]) || payAmount[i] == null) {
                        payAmount[i] = "0.00";
                    }
                    map.put("creditAmount", payAmount[i]);
                    map.put("creditNoteId", insertCreditNote);
                    System.out.println("insertCreditNoteDetails map:" + map);
                    //                    int insertCreditNoteDetails = (Integer) getSqlMapClientTemplate().update("finance.insertInvoiceCreditNoteDetails", map);
                    int insertCreditNoteDetails = (Integer) session.update("finance.insertSuppInvoiceCreditNoteDetails", map);
                    System.out.println("insertCreditNoteDetails---" + insertCreditNoteDetails);
                }
            }

            // CREDIT NOTE OUTSTANDING CREDIT LIMIT
            map.put("customerId", financeTO.getCustomerId());
            map.put("invoiceId", "0");
            map.put("invoiceAmount", financeTO.getPayAmount());

            map.put("consignmentOrderId", "0");
            map.put("custType", "1");
            map.put("outstandingAmount", financeTO.getPayAmount());
            map.put("totalFreightAmount", financeTO.getPayAmount());
            map.put("creditNoteInvoiceId", "0");
            map.put("supplementrycreditId", insertCreditNote);
            map.put("supplymentryInvoiceId", financeTO.getInvoiceId());
            map.put("paymentId", "0");

            map.put("userId", userId);
            System.out.println("updateInvoiceOustanding----" + map);

            int updateCnoteCount, updateOutstanding, updateCreditLimit, updateUsedLimit, updateOutstandingLog = 0;
            String getCustomerType = (String) session.queryForObject("operation.getCustomerType", map);
            String availBlockedAmount = "";
            String availBlockedAmountTemp[] = null;
            map.put("custType", getCustomerType);
            map.put("transactionName", "SuppCreditNote");

            if ("1".equalsIgnoreCase(getCustomerType)) {   //for credit Customers Only
                availBlockedAmount = (String) session.queryForObject("operation.getAvailandBlockedAmount", map);
                System.out.println("availBlockedAmount------" + availBlockedAmount);
                availBlockedAmountTemp = availBlockedAmount.split("~");
                map.put("fixedLimit", availBlockedAmountTemp[0]);
                map.put("blockedAmount", availBlockedAmountTemp[2]);

                double usedLimit = Double.parseDouble(availBlockedAmountTemp[3]) - Double.parseDouble(financeTO.getPayAmount());
                double availLimit = Double.parseDouble(availBlockedAmountTemp[1]) + Double.parseDouble(financeTO.getPayAmount());

                map.put("availAmount", availLimit);

                if (usedLimit < 0) {
                    map.put("usedLimit", 0);
                } else {
                    map.put("usedLimit", availBlockedAmountTemp[3]);
                }

                updateCreditLimit = (Integer) session.update("operation.updateCreditNoteInvoiceLimit", map);

                map.put("transactionType", "Credit");   // credit
                map.put("debitAmount", "0.00");
                map.put("creditAmount", financeTO.getPayAmount());
                updateOutstandingLog = (Integer) session.update("operation.updateOutstandingLog", map);

            } else if ("2".equalsIgnoreCase(getCustomerType)) {

                availBlockedAmount = (String) session.queryForObject("operation.getAvailandBlockedAmount", map);
                System.out.println("availBlockedAmount------" + availBlockedAmount);
                availBlockedAmountTemp = availBlockedAmount.split("~");
                map.put("fixedLimit", availBlockedAmountTemp[0]);
                map.put("blockedAmount", availBlockedAmountTemp[2]);

                double usedLimit = Double.parseDouble(availBlockedAmountTemp[3]) - Double.parseDouble(financeTO.getPayAmount());
                double availLimit = Double.parseDouble(availBlockedAmountTemp[1]) + Double.parseDouble(financeTO.getPayAmount());

                map.put("availAmount", availLimit);

                if (usedLimit < 0) {
                    map.put("usedLimit", 0);
                } else {
                    map.put("usedLimit", availBlockedAmountTemp[3]);
                }

                updateCreditLimit = (Integer) session.update("operation.updateCreditNoteInvoiceLimit", map);

                map.put("transactionType", "Credit");   // credit
                map.put("debitAmount", "0.00");
                map.put("creditAmount", financeTO.getPayAmount());
                updateOutstandingLog = (Integer) session.update("operation.updateOutstandingLog", map);
            }

            // CREDIT NOTE OUTSTANDING CREDIT LIMIT      
            //            String customerLedgerId = (String) getSqlMapClientTemplate().queryForObject("finance.getCustomerLedgerId", map);
            //            String customerLedgerCode = (String) getSqlMapClientTemplate().queryForObject("finance.getCustomerLedgerCode", map);
            String customerLedgerId = (String) session.queryForObject("finance.getCustomerLedgerId", map);
            String customerLedgerCode = (String) session.queryForObject("finance.getCustomerLedgerCode", map);

            //            invoiceReceipt = (Integer) getSqlMapClientTemplate().update("finance.updateSupplyMaster", map);
            //   System.out.println("status2 = " + status2);
            System.out.println("status final = " + status);
            System.out.println("ThrottleConstants.cashPaymentFormId = " + ThrottleConstants.cashPaymentFormId);
            formId = Integer.parseInt(ThrottleConstants.cashPaymentFormId);
            map.put("formId", formId);
            //            voucherNo = (Integer) getSqlMapClientTemplate().insert("finance.getFormVoucherNo", map);
            voucherNo = (Integer) session.insert("finance.getFormVoucherNo", map);
            map.put("voucherNo", voucherNo);
            map.put("voucherCodeNo", ThrottleConstants.CashPaymentVoucherCode + voucherNo);
            map.put("voucherType", "%PAYMENT%");
            String code = "";
            String[] temp;
            //            code = (String) getSqlMapClientTemplate().queryForObject("finance.getVoucerCode", map);
            code = (String) session.queryForObject("finance.getVoucerCode", map);
            System.out.println("code----" + code);
            int codeval2 = 0;
            if (code == null) {
                codeval2 = 0;
            } else {
                temp = code.split("-");
                codeval2 = Integer.parseInt(temp[1]);
            }
            int codev2 = codeval2 + 1;
            String voucherCode = "PAYMENT-" + codev2;
            System.out.println("voucherCode = " + voucherCode);

            map.put("detailCode", "1");
            map.put("voucherCode", voucherCode);
            map.put("accountEntryDate", financeTO.getReceiptDate());
            map.put("mainEntryType", "VOUCHER");
            map.put("entryType", "PAYMENT");
            map.put("ledgerId", ThrottleConstants.CashOnHandLedgerId);
            map.put("particularsId", ThrottleConstants.CashOnHandLedgerCode);
            map.put("amount", financeTO.getPayAmount());
            map.put("accountsType", "DEBIT");
            map.put("narration", financeTO.getRemarks());
            map.put("searchCode", financeTO.getInvoiceCode());
            map.put("reference", "CREDITNOTE");

            System.out.println("map1 updateClearnceDate=---------------------> " + map);
            //            status = (Integer) getSqlMapClientTemplate().update("finance.insertAccountEntry", map);
            status = (Integer) session.update("finance.insertAccountEntry", map);
            System.out.println("status1 = " + status);
            //--------------------------------- acc 2nd row start --------------------------
            if (status > 0) {
                map.put("detailCode", "2");
                map.put("ledgerId", customerLedgerId);
                map.put("particularsId", customerLedgerCode);
                map.put("accountsType", "CREDIT");
                System.out.println("map2 updateClearnceDate=---------------------> " + map);
                //                status = (Integer) getSqlMapClientTemplate().update("finance.insertAccountEntry", map);
                status = (Integer) session.update("finance.insertAccountEntry", map);
                System.out.println("status2 = " + status);
            }

//                billingTONew.setStatusId("33");
//                map.put("statusId", "33"); // suppcredit NOte
//                status = (Integer) session.update("billing.insertTripStatus", map);
//                sout        
//                status = (Integer) session.update("billing.updateTripStatus", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveCreditNote Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "saveCreditNote", sqlException);
        }
        return creditNoteCode;
    }

    public ArrayList getSuppCreditNoteSearchList(FinanceTO financeTO) {
        Map map = new HashMap();
        ArrayList creditNoteSearchList = new ArrayList();
        map.put("customerId", financeTO.getCustomerId());
        map.put("fromDate", financeTO.getFromDate());
        map.put("toDate", financeTO.getToDate());
        try {
            System.out.println("creditNoteSearchList map= " + map);
            creditNoteSearchList = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getSuppCreditNoteSearchList", map);
            System.out.println("getCreditNoteSearchList DAO= " + creditNoteSearchList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCreditNoteSearchList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getCreditNoteSearchList", sqlException);
        }
        return creditNoteSearchList;
    }
    
    
 public String getCustGtaType(FinanceTO financeTO) {
        Map map = new HashMap();
String getCustGtaType=" ";
     
        try {
            map.put("custId",financeTO.getCustomerId());
            System.out.println("map--------------------------"+map);
             getCustGtaType = (String) getSqlMapClientTemplate().queryForObject("billing.getCustGtaType", map);
            System.out.println("getCustGtaType size=----------------------------------------" + getCustGtaType);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return getCustGtaType;
    }


public ArrayList getContractRateApproveListForMail() {
        Map map = new HashMap();
        ArrayList getContractRateApproveListForMail = new ArrayList();
        try {
            System.out.println(" getLedger map =" + map);
            getContractRateApproveListForMail = (ArrayList) getSqlMapClientTemplate().queryForList("finance.getContractRateApproveListForMail", map);
            System.out.println(" getContractRateApproveListForMail =" + getContractRateApproveListForMail.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractRateApproveListForMail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getContractRateApproveListForMail", sqlException);
        }
        return getContractRateApproveListForMail;
    }


public int updateStatus(String status,int userId) {
        int updateStatus = 0;
            Map map = new HashMap();
        try {
            map.put("status",status);
            map.put("userId",userId);
            System.out.println("status-------"+status);
           updateStatus = (Integer) getSqlMapClientTemplate().update("finance.updateStatus", map);
           updateStatus = (Integer) getSqlMapClientTemplate().update("finance.updateStatusLog", map);
            System.out.println(" updateStatus =" + updateStatus);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractRateApproveListForMail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getContractRateApproveListForMail", sqlException);
        }
        return updateStatus;
    }
public String getStatus() {
        String getStatus = "";
            Map map = new HashMap();
        try {
            getStatus = (String) getSqlMapClientTemplate().queryForObject("finance.getStatus", map);
            System.out.println(" getStatus =" + getStatus);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractRateApproveListForMail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getContractRateApproveListForMail", sqlException);
        }
        return getStatus;
    }

}
