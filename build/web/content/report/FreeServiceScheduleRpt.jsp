<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script>       
        <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
        
<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>        
        <title></title>
    </head>
    
    
    
    <script>

function searchSubmit(){
    
   
document.serviceDue.action = '/throttle/handleFreeserviceDueList.do'
document.serviceDue.submit();    

}    

function setDate(companyId){
    
    if(companyId != 'null'){
        document.serviceDue.companyId.value=companyId;
    }    
    if('<%= request.getAttribute("regNo") %>' != 'null'){
        document.serviceDue.regNo.value='<%= request.getAttribute("regNo") %>';
    }    
    if('<%= request.getAttribute("status") %>' != 'null'){
        document.serviceDue.status.value='<%= request.getAttribute("status") %>';
    }    
}


function getVehicleNos(){  
    var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
} 


    </script>
    
   <body onLoad="getVehicleNos();setDate('<%= request.getAttribute("companyId") %>' )">             
        
        <form name="serviceDue"  method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
<table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:850;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">Work Order Report</li>
    </ul>
    <div id="first">
    <table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
  <tr>
        <td height="30">Reg No</td>
        <td  height="30" class="text1"><input type="text" class="form-control" id="regno" name="regNo" value="" ></td>
        <td height="30">Operation Point</td>
        <td height="30" class="text1">
            <select class="form-control" name="companyId"  style="width:125px;">
                <option value="0">---Select---</option>
                <c:if test = "${operationPointList != null}" >
                    <c:forEach items="${operationPointList}" var="Dept">
                        <option value='<c:out value="${Dept.opId}" />'> <c:out value="${Dept.opName}" /></option>
                    </c:forEach >
                </c:if>
            </select>
        </td>
        <td height="30">Status</td>
        <td height="30"><select class="form-control" name="status"  style="width:130px;">
                            <option value="0">---Select---</option>
                            <option value='Y'>Free Service Done</option>
                            <option value='N'>Free Service NotDone</option>
                    </select></td>
        <td height="30"><input type="button" class="button" readonly name="search" value="search" onClick="searchSubmit();" ></td>
        </tr>
    </table>
    </div></div>
    </td>
    </tr>
    </table>
            
    <br>
	 <c:if test = "${serviceList != null}" >
<table  border="0" class="border" align="center" width="650" cellpadding="0" cellspacing="0" id="bg">
<tr>
<td class="contentsub" width="20" height="30">S.No</td>
<td class="contentsub" width="70" height="30">Vehicle No </td>
<td class="contentsub" width="95" height="30">Operation Point</td>
<td class="contentsub" width="60" height="30">KM</td>
<td class="contentsub" width="85" height="30">Month</td>
<td class="contentsub" width="100" height="30" width="200">Description</td>
</tr>

 <% int index = 0;%> 
                    <c:forEach items="${serviceList}" var="service"> 
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
<tr>

<td class="<%=classText %>" width="20" height="30"><%=index+1%></td>
<td class="<%=classText %>" width="70" height="30"><c:out value="${service.regNo}" /></td>
<td class="<%=classText %>"  width="95" height="30"><c:out value="${service.companyName}" /></td>
<td class="<%=classText %>" width="55" height="30"><c:out value="${service.km}" /></td>
<td class="<%=classText %>" width="45" height="30"><c:out value="${service.month}" /></td>
<td class="<%=classText %>" width="125" height="30"><c:out value="${service.description}" /></td>                   
</tr>
 <% index++;%>
 </c:forEach>
</table>
</c:if>
<br>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</html>

