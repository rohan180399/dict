<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/content/common/NewDesign/header.jsp" %>
    <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<script language="javascript" src="/throttle/js/validate.js"></script>  
<%@ page import="ets.domain.contract.business.ContractTO" %> 
<%@ page import="ets.domain.service.business.ServiceTO" %>   
</head>
<script>
       var count=0;
       function submitPage(value){
var checValidate = selectedItemValidation();        
                if(checValidate == 'SubmitForm'){
document.alterMaintenance.action = '/throttle/updateMaintenance.do';
document.alterMaintenance.submit();
}
} 


 function setSelectbox(i)
    {       
        
        var selected=document.getElementsByName("selectedindex") ;
        selected[i].checked = 1;    
        count++;
    }
    
    function selectedItemValidation(){

    var index = document.getElementsByName("selectedindex");
    var serviceNames = document.getElementsByName("serviceNames");    
    var desc = document.getElementsByName("desc");
    var chec=0;
    var mess = "SubmitForm";
    for(var i=0;(i<index.length && index.length!=0);i++){
    if(index[i].checked){
    chec++;
        if(textValidation(serviceNames[i],'Maintenance')){
        return 'notSubmit';        
        }else if(textValidation(desc[i],"Description")){
        return 'notSubmit';
        }
    }
    }
    if(chec == 0){
    alert("Please Select Any One And Then Proceed");
    serviceNames[0].focus();
    return 'notSubmit';
    }
return mess;
}
    
    
    
    
    
  </script>
<div class="pageheader">
      <h2><i class="fa fa-edit"></i><spring:message code="serviceplan.label.AlterPeriodicService" text="default text"/>  </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="serviceplan.label.ServicePlan" text="default text"/></a></li>
          <li class=""><spring:message code="serviceplan.label.PeriodicService" text="default text"/></li>
          <li class="active"><spring:message code="serviceplan.label.AlterPeriodicService" text="default text"/></li>
        </ol>
      </div>
      </div>
 <div class="contentpanel">
<div class="panel panel-default">
 <div class="panel-body">
<body onload="document.alterMaintenance.serviceNames[0].focus();">
  
<form name="alterMaintenance" method="post">
  


<!-- pointer table -->

<!-- message table -->

<%@ include file="/content/common/message.jsp" %>

<table class="table table-info mb30 table-hover" id="table">
    <thead>
<tr>
<th width="45" height="30" align="left" ><div ><spring:message code="serviceplan.label.SNo" text="default text"/></div></th>
<th width="172" height="30" align="left" ><div ><spring:message code="serviceplan.label.Maintenance" text="default text"/></div></th>
<th width="158" height="30" align="left" ><div ><spring:message code="serviceplan.label.Description" text="default text"/></div></th>
<th width="76" height="30" align="left" ><div ><spring:message code="serviceplan.label.Status" text="default text"/></div></th>
<th width="76" height="30" align="left" ><div ><spring:message code="serviceplan.label.Select" text="default text"/></div></th>
</tr>
</thead>
<% 
int index1=0; 
%>
<c:if test = "${maintenanceDetails != null}" >
      <c:forEach items="${maintenanceDetails}" var="service"> 
<%
    String classText = "";
    int oddEven = index1 % 2;
    if (oddEven > 0) {
    classText = "text2";
    } else {
    classText = "text1";
    }
    %>
<tr>
<td  height="30" ><%=index1+1%></td>
<input type="hidden"  name="serviceIds"  value='<c:out value="${service.serviceId}"/>'>
<td  height="30"><input name="serviceNames"  maxlength="100" style="width:260px;height:40px;"  class="form-control" type="text" onchange="setSelectbox(<%=index1%>)"  value='<c:out value="${service.serviceName}"/>'></td>
<td  height="30" ><textarea rows="2" name="desc" style="width:260px;height:40px;"  class="form-control" onkeyup="maxlength(this.form.desc[<%= index1 %>],200)"   onchange="setSelectbox(<%= index1 %>)" cols="30"><c:out value="${service.desc}"/></textarea></td>
<td height="30" > <div align="center"><select name="activeInds" onchange="setSelectbox(<%= index1 %>)" style="width:260px;height:40px;"  class="form-control">
        <c:choose>
        <c:when test="${service.status == 'Y'}">
        <option value="Y" selected><spring:message code="serviceplan.label.Active" text="default text"/></option>
        <option value="N"><spring:message code="serviceplan.label.InActive" text="default text"/></option>
        </c:when>
        <c:otherwise>
        <option value="Y"><spring:message code="serviceplan.label.Active" text="default text"/></option>
        <option value="N" selected><spring:message code="serviceplan.label.InActive" text="default text"/></option>
        </c:otherwise>
        </c:choose>
        </select>
        </div> 
        </td>
        <td  height="30"> <input type="checkbox" name="selectedindex" value='<%= index1 %>' </td>
</tr>
    <%
    index1++;
    %>
    </c:forEach>
</c:if>
</table>
<center>
 <input type="button" class="btn btn-success"  value="<spring:message code="serviceplan.label.SAVE" text="default text"/>" name="Save" onClick="submitPage(this.name)">   
</center>


<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>

</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
