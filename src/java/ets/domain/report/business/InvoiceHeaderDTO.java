/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ets.domain.report.business;

import java.util.ArrayList;

/**
 *
 * @author Sony
 */
public class InvoiceHeaderDTO {

    private String billingState = "";
    private String createdBy = "";
    private String creditNoteNo = "";
    private String creditNoteDate = "";
    private String taxAmount = "";
    private String panNo = "";
    private String gstNo = "";
    private String rateWithReeferPerKg = "";
    private String totalWeight = "";
    private String custName = "";
    private String billingParty = "";
    private String invoiceCode = "";
    private String invoiceId = "";
    private String custAddress = "";
    private String tripCode = "";
    private String movementType = "";
    private String routeInfo = "";
    private String vehicleNo = "";
    private String billDate = "";
    private String customerBillingNameAddress = "";
    private String containerType = "";
    private String billingType = "";
    private String remarks = "";
    private String userName = "";
    private ArrayList invoiceDetailsDTO ;
    private String mailId= "";

    public String getBillingState() {
        return billingState;
    }

    public void setBillingState(String billingState) {
        this.billingState = billingState;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreditNoteNo() {
        return creditNoteNo;
    }

    public void setCreditNoteNo(String creditNoteNo) {
        this.creditNoteNo = creditNoteNo;
    }

    public String getCreditNoteDate() {
        return creditNoteDate;
    }

    public void setCreditNoteDate(String creditNoteDate) {
        this.creditNoteDate = creditNoteDate;
    }

    public String getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(String taxAmount) {
        this.taxAmount = taxAmount;
    }

    public String getPanNo() {
        return panNo;
    }

    public void setPanNo(String panNo) {
        this.panNo = panNo;
    }

    public String getGstNo() {
        return gstNo;
    }

    public void setGstNo(String gstNo) {
        this.gstNo = gstNo;
    }

    public String getRateWithReeferPerKg() {
        return rateWithReeferPerKg;
    }

    public void setRateWithReeferPerKg(String rateWithReeferPerKg) {
        this.rateWithReeferPerKg = rateWithReeferPerKg;
    }

    public String getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(String totalWeight) {
        this.totalWeight = totalWeight;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getBillingParty() {
        return billingParty;
    }

    public void setBillingParty(String billingParty) {
        this.billingParty = billingParty;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }


    public String getCustAddress() {
        return custAddress;
    }

    public void setCustAddress(String custAddress) {
        this.custAddress = custAddress;
    }

    public String getTripCode() {
        return tripCode;
    }

    public void setTripCode(String tripCode) {
        this.tripCode = tripCode;
    }

    public String getMovementType() {
        return movementType;
    }

    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    public String getRouteInfo() {
        return routeInfo;
    }

    public void setRouteInfo(String routeInfo) {
        this.routeInfo = routeInfo;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getCustomerBillingNameAddress() {
        return customerBillingNameAddress;
    }

    public void setCustomerBillingNameAddress(String customerBillingNameAddress) {
        this.customerBillingNameAddress = customerBillingNameAddress;
    }

    public String getContainerType() {
        return containerType;
    }

    public void setContainerType(String containerType) {
        this.containerType = containerType;
    }

    public String getBillingType() {
        return billingType;
    }

    public void setBillingType(String billingType) {
        this.billingType = billingType;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public ArrayList getInvoiceDetailsDTO() {
        return invoiceDetailsDTO;
    }

    public void setInvoiceDetailsDTO(ArrayList invoiceDetailsDTO) {
        this.invoiceDetailsDTO = invoiceDetailsDTO;
    }

    public String getMailId() {
        return mailId;
    }

    public void setMailId(String mailId) {
        this.mailId = mailId;
    }





}

