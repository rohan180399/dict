<%-- 
    Document   : Accounts Receivable
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
         <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>
       
        <form name="accountReceivable" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "TripWiseProfitReport-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>

          <c:if test="${tripDetails != null}">
            <table border="1"  align="center" width="100%" cellpadding="0" cellspacing="1" >
                    <tr >
                        <th ><h3>S.No</h3></th>
                         <th ><h3>Vehicle No </h3></th>
                         <th ><h3>Trip Code</h3></th>
                         <th ><h3>Consignment No</h3></th>
                         <th ><h3>Customer Name </h3></th>
                         <th ><h3>Customer Type </h3></th>
                         <th ><h3>Billing Type </h3></th>
                         <th ><h3>Route </h3></th>
                         <th ><h3>Vehicle Type </h3></th>
                         <th ><h3>Reefer </h3></th>
                         <th ><h3>Driver Name</h3></th>
                         <th ><h3>Earnings </h3></th>
                         <th ><h3>Total Expense </h3></th>
                         <th ><h3>Profit </h3></th>
                         <th ><h3>Profit %</h3></th>
                    </tr>
                    <% int index = 1;%>
                 <c:set var="totalTrips" value="0" />
                    <c:set var="totalEarnings" value="0" />
                    <c:set var="totalExpense" value="0" />
                    <c:set var="earnings" value="0" />
                    <c:set var="expense" value="0" />
                    <c:forEach items="${tripDetails}" var="tripDetails">
                         <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                                    %>

                                      <c:set var="totalTrips" value="${totalTrips + 1}"/>
                         <c:set var="totalEarnings" value="${totalEarnings + tripDetails.revenue}"/>
                         <c:set var="totalExpense" value="${totalExpense + tripDetails.expenses}"/>
                        <tr >
                            <td class="<%=classText%>"  align="center"><%=index++%></td>
                              <td width="150" class="<%=classText%>" align="center" ><a href="" onclick="viewVehicleDetails('<c:out value="${tripDetails.vehicleId}"/>')"><c:out value="${tripDetails.vehicleNo}"/></a></td>
                            <td class="<%=classText%>"  align="center">
                                <a href="" onclick="viewTripDetails('<c:out value="${tripDetails.tripId}"/>');"><c:out value="${tripDetails.tripCode}"/></a>
                            </td>
<!--                            <td class="<%=classText%>"  align="center" ><c:out value="${tripDetails.companyName}"/></td>-->
                            <td class="<%=classText%>"  align="center"><c:out value="${tripDetails.consignmentName}"/></td>
                            <td class="<%=classText%>"  align="center"><c:out value="${tripDetails.customerName}"/></td>
                            <td class="<%=classText%>"  align="center"><c:out value="${tripDetails.customerTypeId}"/></td>
                            <td class="<%=classText%>"  align="center"><c:out value="${tripDetails.billingType}"/></td>
                            <td class="<%=classText%>"  align="center"><c:out value="${tripDetails.routeName}"/></td>
                            <td class="<%=classText%>"  align="center"><c:out value="${tripDetails.vehicleTypeName}"/></td>
                            <td class="<%=classText%>"  align="center"><c:out value="${tripDetails.reeferRequired}"/></td>
                            <td class="<%=classText%>"  align="center"><c:out value="${tripDetails.driverName}"/></td>
                            <td class="<%=classText%>"  align="center"><fmt:formatNumber pattern="##0.00" value="${tripDetails.revenue}"/></td>
                            <td class="<%=classText%>"  align="center"><fmt:formatNumber pattern="##0.00" value="${tripDetails.expenses}"/></td>
<!--                            <td class="<%=classText%>"  align="center"><fmt:formatNumber pattern="##0.00" value="${tripDetails.fuelAmount}"/></td>
                            <td class="<%=classText%>"  align="center"><fmt:formatNumber pattern="##0.00" value="${tripDetails.vehicleDriverSalary}"/></td>
                            <td class="<%=classText%>"  align="center"><fmt:formatNumber pattern="##0.00" value="${tripDetails.driverExpense}"/></td>
                            <td class="<%=classText%>"  align="center"><fmt:formatNumber pattern="##0.00" value="${tripDetails.totlalOperationExpense}"/></td>-->
                            <c:set var="earnings" value="${tripDetails.revenue}" />
                            <c:set var="expense" value="${tripDetails.expenses}" />
                            <c:set var="profit" value="${earnings - expense}" />
                            <td class="<%=classText%>"  align="right">
                            <c:if test="${profit <= 0 && tripDetails.customerName != 'Empty Trip'}">
                            <font color="red"><fmt:formatNumber pattern="##0.00"  value="${profit}" /></font>
                                </c:if>
                                <c:if test="${profit gt 0}">
                            <font color="green"><fmt:formatNumber pattern="##0.00"  value="${profit}" /></font>
                                </c:if></td>

                             <c:set var="profitValue" value="0" />
                            <c:set var="percentageprofit" value="0" />
                            <c:if test="${earnings != '0.00' }">
                            <c:set var="profitValue" value="${profit/earnings}" />
                            <c:set var="percentageprofit" value="${profitValue*100}" />
                            </c:if>

                            <td class="<%=classText%>"  align="right">
                            <c:if test="${percentageprofit <= 0  && tripDetails.customerName != 'Empty Trip'}">
                            <font color="red"><fmt:formatNumber pattern="##0.00"  value="${percentageprofit}" />&nbsp;%</font>
                            </c:if>
                            <c:if test="${percentageprofit > 0}">
                            <font color="green"><fmt:formatNumber pattern="##0.00"  value="${percentageprofit}" />&nbsp;%</font>
                            </c:if>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </c:if>
            <br/>
            <br/>
            <br/>
              <c:if test="${tripDetailsSize != '0'}">
                <table border="2" style="border: 1px solid #666666;"  align="center"  cellpadding="0" cellspacing="1" >
                     <c:set var="profitper" value="0" />
                     <c:set var="profitper" value="${totalEarnings - totalExpense}"/>
                    <tr height="25">
                        <td style="background-color: #6374AB; color: #ffffff">Total Trips Carried Out</td>
                        <td width="150"><c:out value="${totalTrips}"/></td>
                    </tr>
                    <tr height="25">
                        <td style="background-color: #6374AB; color: #ffffff">Total Income</td>
                        <td width="150"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalEarnings}" /></td>
                    </tr>
                    <tr height="25">
                        <td style="background-color: #6374AB; color: #ffffff">Total Operation Expenses</td>
                        <td width="150"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalExpense}" /> </td>
                    </tr>
                    <tr height="25">
                        <td style="background-color: #6374AB; color: #ffffff">Profit </td>
                         <td width="150">
                          <c:if test="${profitper gt 1}"><font color="green">
                                 <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalEarnings - totalExpense}" /></font>
                          </c:if>
                          <c:if test="${profitper lt 1}"><font color="red">
                                 <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalEarnings - totalExpense}" /></font>
                          </c:if>
                         </td>
                    </tr>

                    <tr height="25">
                        <td style="background-color: #6374AB; color: #ffffff"> Profit Percentage <c:out value="${profitper}"/></td>
                    <c:if test="${profitper gt 1}">
                            <td width="150"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${((totalEarnings - totalExpense) * 100 )/ totalEarnings }" />% </font></td>
                     </c:if>
                    <c:if test="${profitper lt 1}">
                         <td width="150"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${((totalEarnings - totalExpense) * 100 )/ totalEarnings }" />% </font></td>
                    </c:if>
                    </tr>
                </table>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>    
</html>