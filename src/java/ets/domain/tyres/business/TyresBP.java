/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ets.domain.tyres.business;

/**
 *
 * @author vinoth
 */
import ets.domain.tyres.data.TyresDAO;
import ets.domain.tyres.business.TyresTO;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import java.util.ArrayList;
import java.util.Iterator;

public class TyresBP {

    private TyresDAO tyresDAO;

    public TyresDAO getTyresDAO() {
        return tyresDAO;
    }

    public void setTyresDAO(TyresDAO tyresDAO) {
        this.tyresDAO = tyresDAO;
    }


    /*
      function used to get Vehicles List
    */
    public ArrayList getVehiclesList() throws FPRuntimeException, FPBusinessException {
        ArrayList vehiclesList = new ArrayList();
        vehiclesList = tyresDAO.getVehiclesList();
        if (vehiclesList == null) {
            throw new FPBusinessException("EM-CM-02");
        }
        return vehiclesList;
    }
    /*
      function used to get the MFR List
    */
    public ArrayList getMfrList() throws FPRuntimeException, FPBusinessException {
        ArrayList mfrList = new ArrayList();
        mfrList = tyresDAO.getMfrList();
        if (mfrList == null) {
            throw new FPBusinessException("EM-CM-02");
        }
        return mfrList;
    }


  /*
      function used to get the Vehicle Details
    */
    public ArrayList getVehicleDetails(TyresTO tyresTo) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleList = new ArrayList();
        vehicleList = tyresDAO.getVehicleDetails(tyresTo);
        if (vehicleList == null) {
            throw new FPBusinessException("EM-CM-02");
        }
        return vehicleList;
    }

    public ArrayList getTyresReportByTyre(TyresTO tyresTo) throws FPRuntimeException, FPBusinessException {
        ArrayList tyreDetailsTemp = new ArrayList();
        ArrayList tyreDetails = new ArrayList();
        tyreDetails = tyresDAO.getTyresReportByTyre(tyresTo);
        /*
        int count = 0;
        TyresTO tyresTO;
        if(tyreDetailsTemp != null){
            Iterator it = tyreDetailsTemp.iterator();
            while(it.hasNext()){
                tyresTO = new TyresTO();
                tyresTO = (TyresTO) it.next();
                if(count == 0){
                    tyresTO.setRcType("New");
                } else {
                    tyresTO.setRcType("RC-"+count);
                }
                tyreDetails.add(tyresTO);
                count++;
            }
        }
        */

        if (tyreDetails == null) {
            throw new FPBusinessException("EM-CM-02");
        }
        return tyreDetails;
    }

    public String processTyreNoSuggests(String tyreNo ) {
        String tyreNos = "";
        tyreNos = tyreNo + "%";
        tyreNos = tyresDAO.getTyreSuggests(tyreNos);
        return tyreNos;
    }
    public ArrayList getTyresReportByVehicle(TyresTO tyresTo) throws FPRuntimeException, FPBusinessException {
        ArrayList tyreDetailsTemp = new ArrayList();
        ArrayList tyreDetails = new ArrayList();
        tyreDetails = tyresDAO.getTyresReportByVehicle(tyresTo);
        if (tyreDetails == null) {
            throw new FPBusinessException("EM-CM-02");
        }
        return tyreDetails;
    }

    public ArrayList getVehicleDetailsList(TyresTO tyresTo) throws FPRuntimeException, FPBusinessException {
        ArrayList tyreRotationList = new ArrayList();
        ArrayList tyre = new ArrayList();
        tyreRotationList = tyresDAO.getVehicleDetailsList(tyresTo);
        if (tyre == null) {
            throw new FPBusinessException("EM-CM-02");
        }
        return tyreRotationList;
    }

    public ArrayList getTyresList(TyresTO tyresTo) throws FPRuntimeException, FPBusinessException {
        ArrayList tyreList = new ArrayList();        
        tyreList = tyresDAO.getTyresList(tyresTo);
        if (tyreList == null) {
            throw new FPBusinessException("EM-CM-02");
        }
        return tyreList;
    }
    public ArrayList getTyreRotation(TyresTO tyresTo) throws FPRuntimeException, FPBusinessException {
        ArrayList tyreList = new ArrayList();
        tyreList = tyresDAO.getTyreRotation(tyresTo);
        if (tyreList == null) {
            throw new FPBusinessException("EM-CM-02");
        }
        return tyreList;
    }

    public ArrayList getCurrentTyrePosition(TyresTO tyresTo) throws FPRuntimeException, FPBusinessException {
        ArrayList currentTyrePosition = new ArrayList();
        currentTyrePosition = tyresDAO.getCurrentTyrePosition(tyresTo);
        if (currentTyrePosition == null) {
            throw new FPBusinessException("EM-CM-02");
        }
        return currentTyrePosition;
    }
    

    public int saveTyreRotation(TyresTO tyresTo) throws FPRuntimeException, FPBusinessException {
        int rotationId = 0;
        rotationId = tyresDAO.saveTyreRotation(tyresTo);
        return rotationId;
    }

    public int saveTyreRotationPosition(int rotationId,String vehicleId,String tyreId,String positionId,int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = tyresDAO.saveTyreRotationPosition(rotationId,vehicleId,tyreId,positionId,userId);
        return status;
    }

    public ArrayList getVehicleTyresNoList(TyresTO tyresTo) throws FPRuntimeException, FPBusinessException {
        ArrayList currentTyrePosition = new ArrayList();
        currentTyrePosition = tyresDAO.getVehicleTyresNoList(tyresTo);
        return currentTyrePosition;
    }
    
    public int getVehicleTyresRotationCount(TyresTO tyresTo) {
        int tyreRotationCount = 0;
        tyreRotationCount = tyresDAO.getVehicleTyresRotationCount(tyresTo);
        return tyreRotationCount;
    }
    public int updateTyreRotation(TyresTO tyresTO, int userId) {
        int tyreRotationCount = 0;
        tyreRotationCount = tyresDAO.updateTyreRotation(tyresTO,userId);
        return tyreRotationCount;
    }
}
