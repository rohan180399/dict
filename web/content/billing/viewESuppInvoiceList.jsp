<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>
<script>
    function submitPage(param) {
        document.billDetails.action = "/throttle/handleESuppInvoiceListForDownloadUpload.do?param=" + param;
        document.billDetails.submit();
    }
    function downloadEInvoiceList(custId,gstNo,invId) {
//        alert(invId)
        document.billDetails.action = "/throttle/handleESuppInvoiceListForDownloadUpload.do?param=download&custId="+custId+"&gstNo="+gstNo+"&invoiceId="+invId;
        document.billDetails.submit();
    }
    function uploadEInvoiceList(custId,gstNo,invId) {
//        alert(invId)
        document.billDetails.action = "/throttle/eSuppInvoiceUpload.do?custId="+custId+"&gstNo="+gstNo+"&invoiceId="+invId;
        document.billDetails.submit();
    }
    function showBillDeatil(invoiceId) {
        document.billDetails.action = "/throttle/showinvoicedetail.do?invoiceId=" + invoiceId;
        document.billDetails.submit();

    }
    function submitBillCourierDetails(invoiceId) {
//        document.billDetails.action = "" +  + "";
//        document.billDetails.submit();
        window.open('/throttle/viewOrderSuppBillDetailsForSubmit.do?invoiceId=' + invoiceId+'&submitStatus=<c:out value="${submitStatus}"/>', 'PopupPage', 'height = 600, width = 900, scrollbars = yes, resizable = yes');

    }
 function eInvoiceGenerate() {

        var checkIn = document.getElementsByName("selectedIndex");
        var chec = 0;
        for (var i = 0; i < checkIn.length; i++) {
            if (checkIn[i].checked) {
                chec = chec + 1;
            }
        }
        if (chec > 0) {
            document.billDetails.action = '/throttle/eSuppInvoiceGenerate.do';
            document.billDetails.submit();
        }
        if (chec == 0) {
            alert("Please Select Any One And Then Proceed");
            checkIn[0].focus();
        }

    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> E-SuppInvoice List</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Primary Billing</a></li>
            <li class="active">View E-SuppInvoice</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%
                String menuPath = "E-SuppInvoice >> View Invoice";
                request.setAttribute("menuPath", menuPath);
            %>
            <body>
                <form name="billDetails" method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp"%>
                    <table class="table table-info mb30 table-hover" style="width:80%">
                        <tr height="30"   ><td colSpan="5" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">View Invoice List</td></tr>

                        <tr>
                            <td><font color="red">*</font>From Date</td>
                            <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker"   style="width:240px;height:40px;" onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>"></td>
                            <td><font color="red">*</font>To Date</td>
                            <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker"   style="width:240px;height:40px;"onclick="ressetDate(this);" value="<c:out value="${toDate}"/>"></td>

                            <td> <input type="hidden" name="days" id="days" value="" /><input type="hidden" name="tripType" id="tripType" value="<c:out value="${tripType}"/>" /></td>
                        </tr>
                        <tr>
                            <td><font color="red">*</font>Customer Name</td>
                            <td height="30">
                                <select  name="customerId" id="customerId"  class="form-control" style="width:240px;height:40px;"><option value="">---Select---</option>
                                    <c:forEach items="${customerList}" var="customerList">
                                        <option value='<c:out value="${customerList.custId}"/>'><c:out value="${customerList.custName}"/></option>
                                    </c:forEach>
                                </select>
                                <script>
                                    document.getElementById('customerId').value = '<c:out value="${customerId}"/>';
                                </script>
                            </td>
                            <td><font color="red">*</font>Bill No</td>
                            <td height="30">
                                <input name="billNo" id="billNo" type="text" class="form-control"   style="width:240px;height:40px;" value="<c:out value="${billNo}"/>">
                            </td>
                        </tr>
<!--                        <tr>
                            <td><font color="red">*</font>Gr No</td>
                            <td height="30">
                                <input name="grNo" id="grNo" type="text" class="form-control"   style="width:240px;height:40px;" value="<c:out value="${grNo}"/>">
                            </td>
                            <td><font color="red">*</font>Container No</td>
                            <td height="30">
                                <input name="containerNo" id="containerNo" type="text" class="form-control"   style="width:240px;height:40px;" value="<c:out value="${containerNo}"/>">
                            </td>
                        </tr>-->
                        <tr>
                            <td colspan="4" align="center">
                                <input type="button" class="btn btn-info"   value="FETCHDATA" onclick="submitPage(this.value);">&nbsp;&nbsp;
                                <!--<input type="button" class="btn btn-info"   value="ExcelExport" onclick="submitPage(this.value);"></td>-->
                        </tr>
                    </table>
                    <script>
                            (function () {
                            var billNo = document.getElementById('billNo');
                            billNo.addEventListener('keypress', function (event) {
                                if (event.keyCode == 13 || event.keyCode == 9) {
                                    event.preventDefault();
                                    if (billNo != '') {
                                        submitPage('FETCH DATA');
                                    }
                                }
                            });
                        }());
                            (function () {
                            var grNo = document.getElementById('grNo');
                            grNo.addEventListener('keypress', function (event) {
                                if (event.keyCode == 13 || event.keyCode == 9) {
                                    event.preventDefault();
                                    if (grNo != '') {
                                        submitPage('FETCH DATA');
                                    }
                                }
                            });
                        }());
                    </script>        
                    <c:if test="${closedBillList !=nul}">

                             <table class="table table-info mb30 table-hover" id="table" style="width:100%">
                            <thead >
                                <tr>
                                    <th>S.No</th>
                            <th>Bill.No</th>
                            <th>Bill Date</th>
                            <th>Billing Party</th>
                            <th>Customer</th>
                            <th>GR Nos</th>
                            <th>Container Nos</th>
                            <th>Amount</th>
                            <th>View</th>
                            <th>Action</th>                            
                            <th>Status</th>                            
                            <!--<th height="30" ><div ><spring:message code="settings.label.Details" text="Select All"/>&nbsp;<input type="checkbox" id="selectAll" onclick="selectAllMenus()" /> </div></th>-->
                            </tr>
                            </thead>
                            <script>
                                function selectAllMenus() {
                                    var selectAll = document.getElementById("selectAll").checked;
                                    var functionStatus = document.getElementsByName("selectedIndex");
                                    if (selectAll == true) {
                                        for (var i = 0; i < functionStatus.length; i++) {
                                            document.getElementById("selectedIndex" + i).checked = true;
                                        }
                                    } else {
                                        for (var i = 0; i < functionStatus.length; i++) {
                                            document.getElementById("selectedIndex" + i).checked = false;
                                        }
                                    }
                                }
                            </script>
                            
                            <tbody>
                              
                                <%int sno=1;
                                int index =0;
                                String classText="";
                                %>
                                
                                 <c:forEach items="${closedBillList}" var="closedBillList">
                                    <%int oddEven = index % 2;
                                  if (oddEven > 0) {
                                             classText = "text2";
                                         } else {
                                             classText = "text1";
                                         }
                                    %>
                                    <tr>
                                        <td  height="30"><%=sno++%></td>
                                        <td  height="30"><input type="hidden" id="invoiceId" name="invoiceId" value='<c:out value="${closedBillList.invoiceId}"/>' /><input type="hidden" id="custId" name="custid" value='<c:out value="${closedBillList.customerId}"/>' /><c:out value="${closedBillList.invoiceCode}"/></td>
                                        <td  height="30"><c:out value="${closedBillList.createddate}"/></td>
                                        <td  height="30"><c:out value="${closedBillList.billingParty}"/></td>
                                        <td  height="30"><c:out value="${closedBillList.invoicecustomer}"/></td>
                                        <td  height="30"><c:out value="${closedBillList.grNo}"/></td>
                                        <td  height="30"><c:out value="${closedBillList.containerNo}"/></td>
                                        <td  height="30"><c:out value="${closedBillList.grandTotal}"/></td>
                                        <td  height="30"><a href="#" onclick="submitBillCourierDetails('<c:out value="${closedBillList.invoiceId}"/>')">view</a></td>
                                        <td  height="30"><input type="button" class="btn btn-success" value="Download" name="download" onClick="downloadEInvoiceList('<c:out value="${closedBillList.customerId}"/>','<c:out value="${closedBillList.billingGstin}"/>','<c:out value="${closedBillList.invoiceId}"/>');">
                                            <c:if test="${closedBillList.upload_status == 0 && closedBillList.upload_status == 2}">
                                            <input type="button" class="btn btn-success" value="Upload" name="upload" onClick="uploadEInvoiceList('<c:out value="${closedBillList.customerId}"/>','<c:out value="${closedBillList.billingGstin}"/>','<c:out value="${closedBillList.invoiceId}"/>');">
                                            </c:if>
                                            <c:if test="${closedBillList.upload_status == 1}">
                                            <font color="GREEN">UPLOADED</font>
                                            </c:if>
                                        </td>
                                        
                                         <td  height="30">
                                            <c:if test="${closedBillList.upload_status == '1'}">
                                               SUCCESS
                                            </c:if>
                                            <c:if test="${closedBillList.upload_status == '2'}">
                                                <input type="hidden" id="errorStatus" name="errorStatus" value='<c:out value="${closedBillList.apiErrorMsg}"/>' />
                                                <a href="#" onclick="viewErrorStatus()">ErrorMsg</a>
                                            </c:if>
                                                
                                            <c:if test="${closedBillList.upload_status == '0'}">
                                                In-Progress
                                            </c:if>
                                        </td>
                                        
                                        <%--<td  height="30"><input type="checkbox" name="selectedIndex" id="selectedIndex<%=index%>" value="<c:out value="${closedBillList.invoiceId}"/>" ></td>--%>
                                        
                                      
                                    </tr>
                                    <%index++;%>
                                </c:forEach>
                            </tbody>
                        </table>
                        
                        <center>
                            <!--<input type="button" class="btn btn-success" value="E-Invoice Generate" name="einvoices" onClick="eInvoiceGenerate();">-->
                        </center>
                                
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                        <script>
                            function viewErrorStatus(){
                                   var errorMsg = $("#errorStatus").val();
                                   alert(errorMsg);
                                }
                        </script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5" selected="selected">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span>Entries Per Page</span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 0);
                        </script>
                    </c:if>

                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>