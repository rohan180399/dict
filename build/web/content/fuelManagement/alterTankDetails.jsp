<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
       
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
</head>
<script language="javascript">
    function submitPage(){
      var chek=validation();
      if(chek=='true'){
        //document.tank.action='/throttle/alterTankDetail.do';
      
       // document.tank.submit();
        }
        }
        function validation(){
         var index = document.getElementsByName("selectedIndex");
var fuel = document.getElementsByName("fuels");
var present = document.getElementsByName("presentReadings");
var companyId= document.getElementsByName("companyIds");
var rate = document.getElementsByName("rates");
var chec=0;
for(var i=0;(i<index.length && index.length!=0);i++){
if(index[i].checked){
chec++;
if(textValidation(fuel[i],'Fuel Filled')){       
        return 'false';
   }     
 if(textValidation(present[i],'Current Reading')){ 
        return 'false';
   }     
 if(textValidation(companyId[i],'Filling Station')){ 
        return 'false';
   }  
 if(textValidation(rate[i],'Fuel Filled Amount')){ 
        return 'false';
   }  
  
   
}
 
}
if(chec == 0){
alert("Please Select Any One And Then Proceed");
companyId[0].focus();
}
return 'true';
}    
            
            
            
      function setSelectbox(i)
        {
            var selected=document.getElementsByName("selectedIndex") ;
            selected[i].checked = 1;
        }    
</script>
<body>
<form name="tank" method="post">
     <%@ include file="/content/common/path.jsp" %>                 
        <%@ include file="/content/common/message.jsp" %>
        <br>
     <c:if test = "${TankDetails != null}" >
<table align="center" width="650" border="0" class="border" cellspacing="0" cellpadding="0">
 <tr height="30">
  <Td colspan="5" class="contenthead">Tank Details</Td>
 </tr> 
  <tr >
    <td class="contentsub">Date</td>  
     <td class="contentsub">Filling Station</td>
      <td class="contentsub">Filled Fuel</td>       
        <td class="contentsub">Current_Reading(Ltrs)</td>
         <td class="contentsub">Filled Fuel Amount</td>
         
    
    
  </tr>
   <% int index=0;%>
   <c:forEach items="${TankDetails}" var="tank"> 
      <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                    %>	
  <tr>   
    <td class="<%=classText%>"><input type="hidden" name="dats" value='<c:out value="${tank.date}"/>'><c:out value="${tank.date}"/></td>    
    <td class="<%=classText%>"><select class="form-control" name="companyIds" style="width:125px" onchange="setSelectbox('<%= index %>');">
                        <option value="0">---Select---</option>
                        <c:if test = "${CompanyList != null}" >
                            <c:forEach items="${CompanyList}" var="comp"> 
                                 <c:choose>                                
                                <c:when test="${comp.name==tank.companyName}">
                                <option selected value='<c:out value="${comp.cmpId}" />'><c:out value="${comp.name}" /></option>
                            </c:when>
                            <c:otherwise>
                                <option value='<c:out value="${comp.cmpId}" />'><c:out value="${comp.name}"/></option>
                            </c:otherwise>
                        </c:choose>
                            </c:forEach>
                        </c:if>  		
                </select></td>       
                <input type="hidden" name="servicePtIds" value='<c:out value="${tank.companyId}"/>'>
    <td class="<%=classText%>"><input name="fuels" class="form-control"  type="textbox" value='<c:out value="${tank.fuelFilled}"/>' size="20" onchange="setSelectbox('<%= index %>');"></td>   
    <td class="<%=classText%>"><input name="presentReadings" class="form-control"  type="textbox" value='<c:out value="${tank.present}"/>' size="20" onchange="setSelectbox('<%= index %>');"></td>           
    <td class="<%=classText%>"><input name="rates" class="form-control" type="textbox" value='<c:out value="${tank.rate}"/>' size="20" onchange="setSelectbox('<%= index %>');"></td>              
    <td  height="30" class="<%=classText %>"><input type="checkbox" style="visibility:hidden"  name="selectedIndex" value='<%= index %>'></td>   
 </tr>
  <% index++;%>
</c:forEach>
</table>

<br><br>
<center><input type="button" class="button" value="Save" onclick="submitPage();" />
</center>
</c:if>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
