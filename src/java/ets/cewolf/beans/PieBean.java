package ets.cewolf.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.data.general.DefaultPieDataset;
import de.laures.cewolf.DatasetProduceException;
import de.laures.cewolf.DatasetProducer;
import de.laures.cewolf.links.PieSectionLinkGenerator;
import de.laures.cewolf.tooltips.PieToolTipGenerator;

public class PieBean implements DatasetProducer, PieToolTipGenerator, PieSectionLinkGenerator, Serializable {

     private static final long serialVersionUID = 1L;
     private static final Log log = LogFactory.getLog(PieBean.class);
     private String[] seriesNames = {"Madhavaram", "CTS", "Nokia", "Hyundai"};
     private String[] seriesToolTip = {"Madhavaram", "CTS", "Nokia", "Hyundai"};
     private int pieCount = 0;

     /**
      *  Produces some random data.
      */
     public Object produceDataset(Map params) throws DatasetProduceException {
          log.debug("producing data.");


          // Dataset can either be populated by hard-coded or else from a datasource
          DefaultPieDataset datasetPie = new DefaultPieDataset() {

               /**
                *
                */
               private static final long serialVersionUID = 1L;

               protected void finalize() throws Throwable {
                    super.finalize();
                    log.debug(this + " finalized.");
               }
          };

          datasetPie.setValue("Madhavaram", 15);
          datasetPie.setValue("CTS", 10);
          datasetPie.setValue("Nokia", 25);
          datasetPie.setValue("Hyundai", 5);

          return datasetPie;




     }

     /**
      * This producer's data is invalidated after 5 seconds. By this method the
      * producer can influence Cewolf's caching behaviour the way it wants to.
      */
     public boolean hasExpired(Map params, Date since) {
          log.debug(getClass().getName() + "hasExpired()");
          return (System.currentTimeMillis() - since.getTime()) > 5000;
     }

     /**
      * Returns a unique ID for this DatasetProducer
      */
     public String getProducerId() {
          return "PageViewCountData DatasetProducer";
     }

     /**
      * Returns a link target for a special data item.
      */
     public String generateLink(Object dataset, Object category) {
          return seriesNames[pieCount++];

     }

     /**
      * @see java.lang.Object#finalize()
      */
     protected void finalize() throws Throwable {
          super.finalize();
          log.debug(this + " finalized.");
     }

     /**
      * @see org.jfree.chart.tooltips.CategoryToolTipGenerator#generateToolTip(CategoryDataset, int, int)
      */
     public String generateToolTip(PieDataset data, Comparable key, int pieIndex) {
          return seriesToolTip[pieIndex];
     }
}





