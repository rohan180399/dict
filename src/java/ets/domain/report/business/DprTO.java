/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.report.business;

/**
 *
 * @author Arul
 */
public class DprTO {

    private String trailerRevenue;
    private String trailerExpence;
    private String trailerId;
    private String wfuDateTime;
    private String startDate;
    private String wflHours;
    private String wfuHours;
    private String transitHours;
    private String loadingTransitHours;
    private String startDateTime;
    private String endDateTime;
    private String originReportingDateTime;
    private String destinationReportingDateTime;
    private String totalRunKm;
    private String totalRunHm;
    private String rcm;
    private String actualExpense;
    private String estimatedRevenue;
    private String actualAdvancePaid;
    private String tripCode;
    private String routeInfo;
    private String vehicleTypeName;
    private String regno;
    private String fleetCenterName;
    private String statusName;
    private String orderSequence = "";
    private String emptyExpense = "";
    private String earnings = "";
    private String customerName = "";
    private String loadedExpense = "";
    private String customerId = "";
    private String tripMergingId = "";
    private String billingType = "";
    private String totalExpense = "";
    private String tripCount = "";
    private String tripId = "";
    private double tripOtherExpense = 0;
    private double vehicleDriverSalary = 0;
    private double miscAmount = 0;
    private String vehicleId = "";
    private String tripNos = "";
    private String freightAmount = "";
    private String otherExpenseAmount = "";
    private double insuranceAmount = 0;
    private double fcAmount = 0;
    private double roadTaxAmount = 0;
    private double permitAmount = 0;
    private double emiAmount = 0;
    private double fixedExpensePerDay = 0;
    private double totlalFixedExpense = 0;
    private int timePeriod = 0;
    private double tollAmount = 0;
    private double fuelAmount = 0;
    private double driverIncentive = 0;
    private double routeExpenses = 0;
    private double maintainExpense = 0;
    private double driverBata = 0;

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getTripNos() {
        return tripNos;
    }

    public void setTripNos(String tripNos) {
        this.tripNos = tripNos;
    }

    public String getFreightAmount() {
        return freightAmount;
    }

    public void setFreightAmount(String freightAmount) {
        this.freightAmount = freightAmount;
    }

    public double getInsuranceAmount() {
        return insuranceAmount;
    }

    public void setInsuranceAmount(double insuranceAmount) {
        this.insuranceAmount = insuranceAmount;
    }

    public double getFcAmount() {
        return fcAmount;
    }

    public void setFcAmount(double fcAmount) {
        this.fcAmount = fcAmount;
    }

    public double getRoadTaxAmount() {
        return roadTaxAmount;
    }

    public void setRoadTaxAmount(double roadTaxAmount) {
        this.roadTaxAmount = roadTaxAmount;
    }

    public double getPermitAmount() {
        return permitAmount;
    }

    public void setPermitAmount(double permitAmount) {
        this.permitAmount = permitAmount;
    }

    public double getEmiAmount() {
        return emiAmount;
    }

    public void setEmiAmount(double emiAmount) {
        this.emiAmount = emiAmount;
    }

    public String getOtherExpenseAmount() {
        return otherExpenseAmount;
    }

    public void setOtherExpenseAmount(String otherExpenseAmount) {
        this.otherExpenseAmount = otherExpenseAmount;
    }

    public double getTollAmount() {
        return tollAmount;
    }

    public void setTollAmount(double tollAmount) {
        this.tollAmount = tollAmount;
    }

    public double getFuelAmount() {
        return fuelAmount;
    }

    public void setFuelAmount(double fuelAmount) {
        this.fuelAmount = fuelAmount;
    }

    public double getDriverIncentive() {
        return driverIncentive;
    }

    public void setDriverIncentive(double driverIncentive) {
        this.driverIncentive = driverIncentive;
    }

    public double getRouteExpenses() {
        return routeExpenses;
    }

    public void setRouteExpenses(double routeExpenses) {
        this.routeExpenses = routeExpenses;
    }

    public double getMaintainExpense() {
        return maintainExpense;
    }

    public void setMaintainExpense(double maintainExpense) {
        this.maintainExpense = maintainExpense;
    }

    public double getDriverBata() {
        return driverBata;
    }

    public void setDriverBata(double driverBata) {
        this.driverBata = driverBata;
    }

    public int getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(int timePeriod) {
        this.timePeriod = timePeriod;
    }

    public double getFixedExpensePerDay() {
        return fixedExpensePerDay;
    }

    public void setFixedExpensePerDay(double fixedExpensePerDay) {
        this.fixedExpensePerDay = fixedExpensePerDay;
    }

    public double getTotlalFixedExpense() {
        return totlalFixedExpense;
    }

    public void setTotlalFixedExpense(double totlalFixedExpense) {
        this.totlalFixedExpense = totlalFixedExpense;
    }

    public double getMiscAmount() {
        return miscAmount;
    }

    public void setMiscAmount(double miscAmount) {
        this.miscAmount = miscAmount;
    }

    public double getTripOtherExpense() {
        return tripOtherExpense;
    }

    public void setTripOtherExpense(double tripOtherExpense) {
        this.tripOtherExpense = tripOtherExpense;
    }

    public double getVehicleDriverSalary() {
        return vehicleDriverSalary;
    }

    public void setVehicleDriverSalary(double vehicleDriverSalary) {
        this.vehicleDriverSalary = vehicleDriverSalary;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getBillingType() {
        return billingType;
    }

    public void setBillingType(String billingType) {
        this.billingType = billingType;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getEarnings() {
        return earnings;
    }

    public void setEarnings(String earnings) {
        this.earnings = earnings;
    }

    public String getEmptyExpense() {
        return emptyExpense;
    }

    public void setEmptyExpense(String emptyExpense) {
        this.emptyExpense = emptyExpense;
    }

    public String getLoadedExpense() {
        return loadedExpense;
    }

    public void setLoadedExpense(String loadedExpense) {
        this.loadedExpense = loadedExpense;
    }

    public String getTotalExpense() {
        return totalExpense;
    }

    public void setTotalExpense(String totalExpense) {
        this.totalExpense = totalExpense;
    }

    public String getTripCount() {
        return tripCount;
    }

    public void setTripCount(String tripCount) {
        this.tripCount = tripCount;
    }

    public String getTripMergingId() {
        return tripMergingId;
    }

    public void setTripMergingId(String tripMergingId) {
        this.tripMergingId = tripMergingId;
    }

    public String getOrderSequence() {
        return orderSequence;
    }

    public void setOrderSequence(String orderSequence) {
        this.orderSequence = orderSequence;
    }

    public String getActualAdvancePaid() {
        return actualAdvancePaid;
    }

    public void setActualAdvancePaid(String actualAdvancePaid) {
        this.actualAdvancePaid = actualAdvancePaid;
    }

    public String getActualExpense() {
        return actualExpense;
    }

    public void setActualExpense(String actualExpense) {
        this.actualExpense = actualExpense;
    }

    public String getDestinationReportingDateTime() {
        return destinationReportingDateTime;
    }

    public void setDestinationReportingDateTime(String destinationReportingDateTime) {
        this.destinationReportingDateTime = destinationReportingDateTime;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }

    public String getEstimatedRevenue() {
        return estimatedRevenue;
    }

    public void setEstimatedRevenue(String estimatedRevenue) {
        this.estimatedRevenue = estimatedRevenue;
    }

    public String getFleetCenterName() {
        return fleetCenterName;
    }

    public void setFleetCenterName(String fleetCenterName) {
        this.fleetCenterName = fleetCenterName;
    }

    public String getLoadingTransitHours() {
        return loadingTransitHours;
    }

    public void setLoadingTransitHours(String loadingTransitHours) {
        this.loadingTransitHours = loadingTransitHours;
    }

    public String getOriginReportingDateTime() {
        return originReportingDateTime;
    }

    public void setOriginReportingDateTime(String originReportingDateTime) {
        this.originReportingDateTime = originReportingDateTime;
    }

    public String getRcm() {
        return rcm;
    }

    public void setRcm(String rcm) {
        this.rcm = rcm;
    }

    public String getRegno() {
        return regno;
    }

    public void setRegno(String regno) {
        this.regno = regno;
    }

    public String getRouteInfo() {
        return routeInfo;
    }

    public void setRouteInfo(String routeInfo) {
        this.routeInfo = routeInfo;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getTotalRunHm() {
        return totalRunHm;
    }

    public void setTotalRunHm(String totalRunHm) {
        this.totalRunHm = totalRunHm;
    }

    public String getTotalRunKm() {
        return totalRunKm;
    }

    public void setTotalRunKm(String totalRunKm) {
        this.totalRunKm = totalRunKm;
    }

    public String getTransitHours() {
        return transitHours;
    }

    public void setTransitHours(String transitHours) {
        this.transitHours = transitHours;
    }

    public String getTripCode() {
        return tripCode;
    }

    public void setTripCode(String tripCode) {
        this.tripCode = tripCode;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    public String getWflHours() {
        return wflHours;
    }

    public void setWflHours(String wflHours) {
        this.wflHours = wflHours;
    }

    public String getWfuDateTime() {
        return wfuDateTime;
    }

    public void setWfuDateTime(String wfuDateTime) {
        this.wfuDateTime = wfuDateTime;
    }

    public String getWfuHours() {
        return wfuHours;
    }

    public void setWfuHours(String wfuHours) {
        this.wfuHours = wfuHours;
    }

    public String getTrailerId() {
        return trailerId;
    }

    public void setTrailerId(String trailerId) {
        this.trailerId = trailerId;
    }

    public String getTrailerRevenue() {
        return trailerRevenue;
    }

    public void setTrailerRevenue(String trailerRevenue) {
        this.trailerRevenue = trailerRevenue;
    }

    public String getTrailerExpence() {
        return trailerExpence;
    }

    public void setTrailerExpence(String trailerExpence) {
        this.trailerExpence = trailerExpence;
    }
    
}
