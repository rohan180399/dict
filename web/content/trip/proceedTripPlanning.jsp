<%-- 
    Document   : proceedTripPlanning
    Created on : Oct 29, 2013, 1:18:37 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">


    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

</head>




<script>


    


    function submitPage(val) {
        document.proceed.action = "/throttle/saveTripPlanning.do";
        document.proceed.submit();
    }
    
</script>

<body>
    <form name="proceed" method="post">
        <%@ include file="/content/common/path.jsp" %>
        <%@ include file="/content/common/message.jsp" %>
        <br>
        <br>
        
        <br>
        <table class="table" width="100%" align="center" style="width:1150px;">
            <tr>
                <td class="text1">File Name</td>
                <td class="text1"><c:out value="${filePath}"/></td>
                <td class="text1">Plan Date</td>
                <td class="text1"><input type="hidden" name="planDate" id="planDate" value="<c:out value="${planDate}"/>"/><c:out value="${planDate}"/></td>
            </tr>
            <tr>
                <td class="text2">Remarks</td>
                <td class="text2" colspan="4"><b><font color="blue">This is Excel Sheet Information please click proceed to continue the trip planning</font></b></td>
            </tr>
        </table>
        <c:if test = "${tripPlanList != null}" >
            <table width="100%" align="center" border="0" id="table" class="sortable"  style="width:1150px;" >
                <thead>
                    <tr height="40">
                        <th><h3>Sno</h3></th>
                        <th><h3>Consignment No</h3></th>
                        <th><h3>Consignment Status</h3></th>
                        <th><h3>Vehicle No</h3></th>
                        <th><h3>Vehicle Status</h3></th>
                    </tr>
                </thead>
                <% int index = 0;
                            int sno = 1;
                %>
                <tbody>
                    <c:forEach items="${tripPlanList}" var="tpl">
                        <tr height="30">
                            <td align="left" ><%=sno%></td>
                            <td align="left" >
                                <input type="hidden" name="consignmentOrderId" id="consignmentOrderId<%=index%>" value="<c:out value="${tpl.consignmentOrderId}"/>"/>
                                <c:if test="${tpl.consignmentStatus == 'Invalid Consignment No' || tpl.consignmentStatus == 'Trip Already Planned' || tpl.consignmentStatus == 'Trip Generated'}">
                                    <font color="red"><c:out value="${tpl.consignmentOrderNo}"/></font>
                                </c:if>
                                <c:if test="${tpl.consignmentStatus == 'OK'}">
                                    <font color="green"><c:out value="${tpl.consignmentOrderNo}"/></font>
                                </c:if>
                            </td>
                            <td align="left" >
                                <c:if test="${tpl.consignmentStatus == 'Invalid Consignment No' || tpl.consignmentStatus == 'Trip Already Planned' || tpl.consignmentStatus == 'Trip Generated'}">
                                    <font color="red"><c:out value="${tpl.consignmentStatus}"/></font>
                                </c:if>
                                <c:if test="${tpl.consignmentStatus == 'OK'}">
                                    <font color="green"><c:out value="${tpl.consignmentStatus}"/></font>
                                </c:if>
                                </td>
                            <td align="left" >
                                <input type="hidden" name="vehicleId" id="vehicleId<%=index%>" value="<c:out value="${tpl.consignmentVehicleId}"/>"/>
                                <c:if test="${tpl.consignmentVehicleStatus == 'Invalid Vehicle No' || tpl.consignmentVehicleStatus == 'Trip Already Planned'}">
                                    <font color="red">
                                    <c:out value="${tpl.vehicleNo}"/>
                                    </font>
                                </c:if>
                                <c:if test="${tpl.consignmentVehicleStatus == 'OK'}">
                                    <font color="green">
                                    <c:out value="${tpl.vehicleNo}"/>
                                    </font>
                                </c:if>
                            </td>
                            <td align="left" >
                                <c:if test="${tpl.consignmentVehicleStatus == 'Invalid Vehicle No' || tpl.consignmentVehicleStatus == 'Trip Already Planned' }">
                                    <font color="red"><c:out value="${tpl.consignmentVehicleStatus}"/></font>
                                </c:if>
                                <c:if test="${tpl.consignmentVehicleStatus == 'OK'}">
                                <font color="green"><c:out value="${tpl.consignmentVehicleStatus}"/></font>
                                </c:if>
                            </td>
                        </tr>
                        <%index++;%>
                        <%sno++;%>
                    </c:forEach>
                        <tr><td colspan="5" align="center"><input class="button" type="button" name="Proceed" id="Proceed" value="Proceed" onclick="submitPage(this.name)"/></td></tr>
                </tbody>
            </table>
        </c:if>
        
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
