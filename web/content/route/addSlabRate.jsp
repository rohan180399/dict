<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ include file="/content/common/message.jsp" %>
<!--<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>-->
<input type="hidden" name="slabVehicleId" id='slabVehicleId' class="form-control" value='<c:out value="${slabVehicleId}" />'>
<input type="hidden" name="slabRouteCostId" id='slabRouteCostId' class="form-control" value='<c:out value="${slabRouteCostId}" />'>
<input type="hidden" name="slabRouteId" id='slabRouteId' class="form-control" value='<c:out value="${slabRouteId}" />'>
<input type="hidden" name="slabRateId" id='slabRateId' class="form-control" value=''>
<script>
     function setValues(sno,slabRateId,slabId,customerId,slabRate,status){
        var count = parseInt(document.getElementById("count").value);
        document.getElementById('inActive').style.display = 'block';
        for (var i = 1; i <= count; i++) {
//        alert("sno"+sno);
//        alert("i"+i);
            if(i != sno) {
                document.getElementById("edit"+i).checked = false;
            } else {
                document.getElementById("edit"+i).checked = true;
            }
        }
        document.getElementById("slabRateId").value = slabRateId;
        document.getElementById("slabId").value = slabId;
        document.getElementById("customerId").value = customerId;
        document.getElementById("slabRate").value = slabRate;
        document.getElementById("status").value = status;
        
    }
    function slabsubmit()
    {
        var errStr = "";
        var vehicleTypeId = document.getElementById('slabVehicleId').value;
        var routeCostId = document.getElementById('slabRouteCostId').value;
        var routeId = document.getElementById('slabRouteId').value;
        var slabId = document.getElementById('slabId').value;
        var customerId = document.getElementById('customerId').value;
        var slabRate = document.getElementById('slabRate').value;
        var status = document.getElementById('status').value;
        var slabRateId = document.getElementById('slabRateId').value;
        if (document.getElementById("slabId").value == "") {
            errStr = "Please select valid slabId.\n";
            alert(errStr);
            document.getElementById("slabId").focus();
        } else if (document.getElementById("customerId").value == "") {
            errStr = "Please select valid customer.\n";
            alert(errStr);
            document.getElementById("customerId").focus();
        } else if (document.getElementById("slabRate").value == "") {
            errStr = "Please enter slabRate.\n";
            alert(errStr);
            document.getElementById("slabRate").focus();
        }
        if (errStr == "") {
            var url = '/throttle/addSlabRate.do?vehicleTypeId=' + vehicleTypeId + '&routeCostId=' + routeCostId + '&routeId=' + routeId +'&slabId=' + slabId + '&customerId=' + customerId + '&slabRate=' + slabRate + '&status=' + status + '&slabRateId=' + slabRateId;
            jQuery.ajax({
                url: url,
                type: "get",
                success: function (data)
                {
//                    alert("data out "+ data);
                    $('#slabRateListSet').html(data);
                }
            });
        }
    }
</script>
<br>
<table width="100%" align="center" class="table2" cellpadding="0" cellspacing="0" style="border: 2px #000000">
    <tr><th style="text-align: center;font-size: 25px" colspan="4">Slab Rate - <c:out value="${slabVehicleTypeName}" /></th></tr>
    <tr>
        <td class="text1">Slab (Ton)</td>
        <td class="text1">
            <select class="form-control" name="slabId" id="slabId" style="width:125px;">
                <option value="" checked>--Select--</option>
                <c:forEach items="${slabTypeList}" var="slabList">
                    <option value='<c:out value="${slabList.slabId}" />'><c:out value="${slabList.slabName}" /></option>
                </c:forEach >
            </select>
        </td>
        <td class="text1">Customer</td>
        <td class="text1">
            <select class="form-control" name="customerId" id="customerId" style="width:125px;">
                <option value="" checked>--Select--</option>
                <option value="0" checked>Own</option>
                <c:forEach items="${customerList}" var="CList">
                    <option value='<c:out value="${CList.customerId}" />'><c:out value="${CList.customerName}" /></option>
                </c:forEach >
            </select>
        </td>
    </tr>
    <tr>
        <td class="text2">Slab Rate</td>
        <td class="text2"><input type="text" name="slabRate" id='slabRate' class="form-control" value=''></td>
        <td class="text2">&nbsp;&nbsp;&nbsp;&nbsp;Status</td>
        <td class="text2">
            <select  align="center" class="form-control" name="status" id="status" >
                <option value='Y'>Active</option>
                <option value='N' id="inActive" style="display: none">In-Active</option>
            </select>
        </td>
    </tr>
    <tr>
        <td class="text2" colspan="4">
    <center>
        <input type="button" class="button" value="Save" name="Submit" onClick="slabsubmit()">
    </center>
</td>
</tr>
</table>
<br>
<br>
<% int count = 0;%>
<table width="100%" align="center" border="0" id='table1' class="sortable" style="width: 600px;">
    <thead>
        <tr height="70">
            <th style="width: 90px;text-align: center;">S.No</th>
            <th style="width: 90px;text-align: center;">Slab (Ton)</th>
            <th style="width: 90px;text-align: center;">Customer</th>
            <th style="width: 90px;text-align: center;">Slab Rate</th>
            <th style="width: 90px;text-align: center;">Status</th>
            <th style="width: 90px;text-align: center;">Select</th>
        </tr>
    </thead>
    <% int index = 0;
        int sno = 0;
    %>
    <c:if test="${vehicleSlabrateList != null}">
        <tbody>
            <c:forEach items="${vehicleSlabrateList}" var="vsl">
                <%
                    sno++;
                    String classText = "";
                    int oddEven = index % 2;
                    if (oddEven > 0) {
                        classText = "text2";
                    } else {
                        classText = "text1";
                    }
                %>
                <tr height="30">
                    <td align="left" class="<%=classText%>"><%=sno%></td>
                    <td align="left" class="<%=classText%>"><c:out value="${vsl.slabName}" /></td>
                    <td align="left" class="<%=classText%>"><c:out value="${vsl.customerName}" /></td>
                    <td align="left" class="<%=classText%>"><c:out value="${vsl.slabRate}" /></td>
                    <td align="left" class="<%=classText%>"><c:out value="${vsl.status}" /></td>
                    <td align="left" class="<%=classText%>"><input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%=sno%>,'<c:out value="${vsl.slabRateId}" />','<c:out value="${vsl.slabId}" />','<c:out value="${vsl.customerId}" />','<c:out value="${vsl.slabRate}" />','<c:out value="${vsl.status}" />');" /></td>
                </tr>
                <%index++;%>
            </c:forEach>
        </tbody>
        <input type="hidden" name="count" id="count" value="<%=sno%>" />
    </table>
    <script language="javascript" type="text/javascript">
        setFilterGrid("table1");</script>
    <div id="controls">
        <div id="perpage">
            <select onchange="sorter.size(this.value)">
                <option value="5" selected="selected">5</option>
                <option value="10">10</option>
                <option value="20">20</option>
                <option value="50">50</option>
                <option value="200">200</option>
            </select>
            <span>Entries Per Page</span>
        </div>
        <div id="navigation">
            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
        </div>
        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
    </div>
    <script type="text/javascript">
        var sorter = new TINY.table.sorter("sorter");
        sorter.head = "head";
        sorter.asc = "asc";
        sorter.desc = "desc";
        sorter.even = "evenrow";
        sorter.odd = "oddrow";
        sorter.evensel = "evenselected";
        sorter.oddsel = "oddselected";
        sorter.paginate = true;
        sorter.currentid = "currentpage";
        sorter.limitid = "pagelimit";
        sorter.init("table1", 1);
    </script>
</c:if>
<br>

