<%-- 
Document   : rolefunctions
Created on : Jul 23, 2008, 11:04:14 AM
Author     : vidya
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
   <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import = "ets.domain.users.business.LoginTO" %>
<%@ page import="java.util.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript">
function getfunction() {
document.rolefunctions.action = '/throttle/getfunc.do';
document.rolefunctions.reqfor.value = 'getFunctions';
document.rolefunctions.submit();
}

function copyAddress(availableFunc)
{
var avilableFunction = document.getElementById("availableFunc");
var index = 0;
var selectedLength = 0;
if(avilableFunction.length != 0){
for (var i=0; i<avilableFunction.options.length ; i++) {    
    if(avilableFunction.options[i].selected == true){
            selectedLength++;
    }    
  }
for (var j=0; j<selectedLength ; j++) {
        for (var i=0; i<avilableFunction.options.length ; i++) {
            if(avilableFunction.options[i].selected == true){
                
                    var optionCounter = document.rolefunctions.assignedfunc.length;
                    document.rolefunctions.assignedfunc.length = document.rolefunctions.assignedfunc.length +1;
                    document.rolefunctions.assignedfunc.options[optionCounter].text = avilableFunction.options[i].text ;
                    document.rolefunctions.assignedfunc.options[optionCounter].value =  avilableFunction.options[i].value;
                    avilableFunction.options[i] = null;
                   break;
            }
          }
}  
/*if(availableFunc != ""){
var optionCounter = document.rolefunctions.assignedfunc.length;
document.rolefunctions.assignedfunc.length = document.rolefunctions.assignedfunc.length +1;
document.rolefunctions.assignedfunc.options[optionCounter].text = document.rolefunctions.availableFunc.options[document.rolefunctions.availableFunc.selectedIndex].text ;
document.rolefunctions.assignedfunc.options[optionCounter].value = document.rolefunctions.availableFunc.options[document.rolefunctions.availableFunc.selectedIndex].value ;
document.rolefunctions.availableFunc.options[document.rolefunctions.availableFunc.selectedIndex]=null;

}*/
}
else {
alert("Please Select any Value");
}
}


function copyAddress1(assRole)
{
//var value = 
var selectedValue = document.getElementById('assignedfunc');
var index = 0;
var selectedLength = 0;

if(selectedValue.length != 0){
for (var i=0; i<selectedValue.options.length ; i++) {    
    if(selectedValue.options[i].selected == true){
            selectedLength++;
    }    
  }  
//alert(selectedLength);
//alert(selectedValue.options.length);
for (var j=0; j<selectedLength ; j++) {
        for (var i=0; i<selectedValue.options.length ; i++) {
            if(selectedValue.options[i].selected == true){
                
                    var optionCounter = document.rolefunctions.availableFunc.length;
                    document.rolefunctions.availableFunc.length = document.rolefunctions.availableFunc.length +1;
                    document.rolefunctions.availableFunc.options[optionCounter].text = selectedValue.options[i].text ;
                    document.rolefunctions.availableFunc.options[optionCounter].value =  selectedValue.options[i].value;
                    selectedValue.options[i] = null;
                   break;
            }
          }
}  




//document.rolefunctions.availableFunc.length = document.rolefunctions.availableFunc.length +1;
//document.rolefunctions.availableFunc.options[optionCounter].text = document.rolefunctions.assignedfunc.options[document.rolefunctions.assignedfunc.selectedIndex].text ;
//document.rolefunctions.availableFunc.options[optionCounter].value = document.rolefunctions.assignedfunc.options[document.rolefunctions.assignedfunc.selectedIndex].value ;
//document.rolefunctions.assignedfunc.options[document.rolefunctions.assignedfunc.selectedIndex] = null;

}
else {
alert("Please Select any Value");
}
}

function setRole() {
var roleId = '<%= request.getAttribute("rolelist") %>';

if(roleId != 'null') {
document.rolefunctions.rolesId.value=roleId; 

} else {
//
}
}
function submitPage() {

var length = document.rolefunctions.availableFunc.length;
var counter = document.rolefunctions.assignedfunc.length;


for(var j = 0; j < counter; j++){			
document.rolefunctions.assignedfunc.options[j].selected = true;
}
document.rolefunctions.action='/throttle/assignfunction.do';
document.rolefunctions.reqfor.value='assignFunctions';
document.rolefunctions.submit();
}

window.onload = setRole;
</script>
 

<!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/ 
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->

<div class="pageheader">
      <h2><i class="fa fa-edit"></i><spring:message code="settings.label.ManageRoleFunction" text="default text"/>  </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="header.label.YouAreHere" text="default text"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="header.label.Home" text="default text"/></a></li>
          <li><a href="general-forms.html"><spring:message code="header.label.Setting" text="default text"/></a></li>
          <li class=""><spring:message code="settings.label.ManageRoleFunction" text="default text"/>  </li>


                  </ol>
      </div>
      </div>
<div class="contentpanel">
            <div class="panel panel-default">
             <div class="panel-body">

<body>

<form name="rolefunctions" method="post">
<%--<%@ include file="/content/common/path.jsp" %>--%>

<!-- pointer table -->


<%@ include file="/content/common/message.jsp" %>
<!-- message table -->
<!-- copy there  end -->
<c:if test = "${roleList != null}">
<table class="table table-bordered">

  <table  class="table table-info mb30 table-hover">
<!--
      <thead>
          
    <th  colspan="6" height="30"><spring:message code="settings.label.AssignRoleFunctions" text="default text"/></th>
      </thead>-->

<tr>
<td  height="45" style="border-top-color:#5BC0DE;"> <spring:message code="settings.label.RoleTypes" text="default text"/></td>
    <td align="center" height="30" style="border-top-color:#5BC0DE;">
<select name="rolesId" style="width:260px;height:40px;"  class="form-control">
<option value="0">-<spring:message code="settings.label.Select" text="default text"/>-</option>

<c:forEach items="${roleList}" var="role"> 
<option value='<c:out value="${role.roleId}" />'><c:out value="${role.roleName}" /></option>
</c:forEach >

</select>  </td>
<th height="45"  style="border-top-color:#5BC0DE;"><input type="button" class="btn btn-success" value="<spring:message code="settings.label.GO" text="default text"/>" class="button" onClick="getfunction()"></th>
</tr>  
</table>
</td>

</table>




<table class="table table-info mb30 table-hover" >
    <thead>
<tr>
<th  valign="center" height="35" ><spring:message code="settings.label.AvailableFunctions" text="default text"/> </th>
<th  height="35"></th>
<th  valign="center" height="35"><spring:message code="settings.label.AssignedFunctions" text="default text"/> </th>
</tr>
</thead>
<tr>
<td >
<table width="150" height="150" cellpadding="0" cellspacing="0" align="center" id="bg">
<tr>  
<td width="150"><select style="width:200px;" multiple size="10" id="availableFunc" name="availableFunc" class="textbox">
<c:if test = "${AvailableFunction != null}" >
<c:forEach items="${AvailableFunction}" var="roles"> 
<option value='<c:out value="${roles.functionId}" />'><c:out value="${roles.functionName}" /></option>
</c:forEach >
</c:if>
</select></td>
</tr>
</table>                       

</td>   

<td height="100" valign="center">
<table width="20" height="70" cellpadding="2" cellspacing="2" align="center" id="bg">
<tr>
<td height="15" align="center" bgcolor="#F4F4F4" style=" border:1px; border-bottom-style:solid; "><input type="button" class="textbox" value=">" onClick="copyAddress(availableFunc.value)"></td></tr>
<tr >
<td height="15" align="center" bgcolor="#F4F4F4" style=" border:1px; "><input type="button" class="textbox" value="<" onClick="copyAddress1(assignedfunc.value)"></td></tr>


</table></td>
<td valign="top">

<table width="150" height="150" cellpadding="0" cellspacing="0" align="center" id="bg">
<tr>
<td width="150"><select style="width:200px;" multiple size="10" id="assignedfunc" name="assignedfunc"  class="textbox">
<c:if test = "${AssignedFucntions != null}">
<c:forEach items="${AssignedFucntions}" var="role"> 
<option value='<c:out value="${role.functionId}" />'><c:out value="${role.functionName}" /></option>
</c:forEach >
</c:if>
</select></td>
</tr>
</table></td>


</tr>




</table>
</c:if>
<center>

<input type="button" name="save" class="btn btn-success" value="<spring:message code="settings.label.SAVE" text="default text"/>" onClick="submitPage();" class="button">


<input type="hidden" value="" name="reqfor">
</center>


<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>         
</body>  

</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
