
<html>
    <head>
     <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    </head>
    <script language="javascript">

function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}

        function submitPage(){
            var chek=validation();
            if(chek=='true'){
            document.report.action='/throttle/milleageReport.do';
            document.report.submit();
        }
        }
        function submitExcel(){
            document.report.action='/throttle/milleageReportExcel.do';
            document.report.submit();
            }
        function validation(){
            if(textValidation(document.report.fromDate,'From Date')){
                return 'false';                
                }
            if(textValidation(document.report.toDate,'TO Date')){
                return 'false';
                
                }
                if(document.report.companyId.value=='0'){
                    alert("Please select Filling Station");
                    document.report.companyId.focus();
                    return 'false';
                    }
            return 'true';
            }
        function getVehicleNos(){
            
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
        }       
        function setValues(){
            if('<%=request.getAttribute("companyId")%>' !='null'){
            document.report.fromDate.value='<%=request.getAttribute("fromDate")%>';
            document.report.toDate.value='<%=request.getAttribute("toDate")%>';
            document.report.companyId.value='<%=request.getAttribute("companyId")%>';
            }if('<%=request.getAttribute("regNo")%>'!='null'){
            document.report.regNo.value='<%=request.getAttribute("regNo")%>';
            }
            }
    
    </script>
    <body onload="getVehicleNos();setValues();">
        <form name="report" method="post">
            <%@ include file="/content/common/path.jsp" %>                 
            <%@ include file="/content/common/message.jsp" %>



<table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
<tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
</h2></td>
<td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
</tr>
<tr id="exp_table" >
<td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
    <div class="tabs" align="left" style="width:850;">
<ul class="tabNavigation">
        <li style="background:#76b3f1">Milleage Report</li>
</ul>
<div id="first">
<table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
<tr height="30">
    <td>VehicleNo</td>
    <td><input name="regNo" id="regno" class="form-control" type="text" value="" size="20"></td>
      <td>Filling Station</td>
    <td><select class="form-control" name="companyId" style="width:125px">
        <option value="0">---Select---</option>
        <c:if test = "${CompanyList != null}" >
            <c:forEach items="${CompanyList}" var="comp">
            <c:choose>
            <c:when test="${comp.companyTypeId==1012}" >
                <option value='<c:out value="${comp.cmpId}" />'><c:out value="${comp.name}" /></option>
            </c:when>
            </c:choose>
           </c:forEach>
        </c:if>
</select></td>
<td rowspan="2" valign="middle"><input type="button" class="button" name="Search" value="Search" onclick="submitPage();"></td>

</tr>

<tr height="30">
    <td><font color="red">*</font>From Date</td>
    <td><input name="fromDate" class="form-control" type="text"  size="20"> <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.report.fromDate,'dd-mm-yyyy',this)"/></td>
    <td ><font color="red">*</font>TO Date</td>
    <td ><input name="toDate" class="form-control" type="text"  size="20"> <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.report.toDate,'dd-mm-yyyy',this)"/></td>
</tr>
</table>
</div></div>
</td>
</tr>
</table>
<br>
            <c:if test = "${milleageReport!= null}" >
                
                <table align="center" width="700" class="border" border="0" cellspacing="0" cellpadding="0">
                    <tr height="30">
                        <td colspan="6" class="contenthead">Fuel Report</td>
                    </tr>
                    
                    <tr height="30">                                                                                                    
                        <td class="contentsub">Date</td> 
                        <td class="contentsub">Vehicle Number</td> 
                        <td class="contentsub">Owning Company</td>
                        <td class="contentsub">Using Company</td>
                        <td class="contentsub">Mileage Configured</td>
                        <td class="contentsub">Actual Mileage</td>
                        
                        
                    </tr>
                    <% int index = 0;%>
                    <c:forEach items="${milleageReport}" var="fuel"> 
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>	
                        <tr height="30">
                            
                            <td class="<%=classText %>"><input type="hidden" name="dats" value='<c:out value="${fuel.date}"/>'><c:out value="${fuel.date}"/></td>                             
                            <td class="<%=classText %>"><input type="hidden" name="regNos" value='<c:out value="${fuel.regNo}"/>'><c:out value="${fuel.regNo}"/></td>                             
                            <td class="<%=classText %>"><input type="hidden" name="companyNames" value='<c:out value="${fuel.companyName}"/>'><c:out value="${fuel.companyName}"/></td> 
                             <td class="<%=classText %>"><input type="hidden" name="servicePtNames" value='<c:out value="${fuel.servicePtName}"/>'><c:out value="${fuel.servicePtName}"/></td>
                            <td class="<%=classText %>"><input type="hidden" name="milleages" value='<c:out value="${fuel.milleage}"/>'><c:out value="${fuel.milleage}" /></td>
                            <td class="<%=classText %>"><input type="hidden" name="avgKms" value='<c:out value="${fuel.avgKm}"/>'><c:out value="${fuel.avgKm}"/></td>                                                       
                        </tr>
                        <%index++;%>
                    </c:forEach>
                </table>
                <br>
                <center><input type="button" class="button" name="Excel" value="To Excel" onclick="submitExcel();"></center>
            </c:if>
            <br><br>
            
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
