package ets.domain.problemActivities.web;

import ets.arch.business.PaginationHelper;
import ets.arch.web.BaseController;
import ets.domain.racks.business.RackBP;
import ets.domain.racks.business.RackTO;
import ets.domain.users.business.LoginBP;
import ets.domain.util.FPLogUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.ModelAndView;
import java.util.Iterator;
import java.util.ArrayList;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.problemActivities.business.ProblemActivitiesBP;
import ets.domain.problemActivities.business.ProblemActivitiesTO;
import ets.domain.util.ParveenErrorConstants;
import ets.domain.users.web.CryptoLibrary;
import ets.domain.vehicle.business.VehicleBP;
import java.io.*;
import ets.domain.racks.business.RackTO;
import ets.domain.section.business.SectionBP;
import ets.domain.section.business.SectionTO;
import ets.domain.vehicle.business.VehicleTO;
import ets.domain.vehicle.web.VehicleCommand;

public class ProblemActivitiesController extends BaseController {

    ProblemActivitiesCommand problemActivitiesCommand;
    LoginBP loginBP;
    ProblemActivitiesBP problemActivitiesBP;
    SectionBP sectionBP;

    public SectionBP getSectionBP() {
        return sectionBP;
    }

    public void setSectionBP(SectionBP sectionBP) {
        this.sectionBP = sectionBP;
    }

    public ProblemActivitiesCommand getProblemActivitiesCommand() {
        return problemActivitiesCommand;
    }

    public void setProblemActivitiesCommand(ProblemActivitiesCommand problemActivitiesCommand) {
        this.problemActivitiesCommand = problemActivitiesCommand;
    }

    public ProblemActivitiesBP getProblemActivitiesBP() {
        return problemActivitiesBP;
    }

    public void setProblemActivitiesBP(ProblemActivitiesBP problemActivitiesBP) {
        this.problemActivitiesBP = problemActivitiesBP;
    }

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        binder.closeNoCatch();  initialize(request);


    }

    //handlemanageSectionPage
    /**
     * This method used to View MFR Page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView manageProblemAct(HttpServletRequest request, HttpServletResponse response, ProblemActivitiesCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String menuPath = "Problem  >>     Manage Problem";
        ArrayList sectionList = new ArrayList();
        path = "content/problemActivities/manageProblem.jsp";
        try {
//            if (!loginBP.checkAuthorisation(userFunctions, "Problem-View")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                String pageTitle = "View Problem";
                request.setAttribute("pageTitle", pageTitle);

                int totalRecords = 0;
                int pageNo = 0;
                int totalPages = 0;
                sectionList = sectionBP.processGetSectionList();
                request.setAttribute("SectionList", sectionList);

                ProblemActivitiesTO problemActivitiesTO = new ProblemActivitiesTO();
                PaginationHelper pagenation = new PaginationHelper();
                int startIndex = 0;
                int endIndex = 0;
                problemActivitiesTO.setStartIndex(startIndex);
                problemActivitiesTO.setEndIndex(endIndex);


                ArrayList paList = new ArrayList();
                paList = problemActivitiesBP.processGetpaList(problemActivitiesTO);


                totalRecords = paList.size();
                pagenation.setTotalRecords(totalRecords);

                String buttonClicked = "";
                if (request.getParameter("button") != null && request.getParameter("button") != "") {
                    buttonClicked = request.getParameter("button");
                }
                if (request.getParameter("pageNo") != null && request.getParameter("pageNo") != "") {
                    pageNo = Integer.parseInt(request.getParameter("pageNo"));
                }
                pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
                totalPages = pagenation.getTotalNoOfPages();
                request.setAttribute("pageNo", pageNo);
                //////System.out.println("pageNo" + pageNo);
                request.setAttribute("totalPages", totalPages);
                startIndex = pagenation.getStartIndex();
                endIndex = pagenation.getEndIndex();

                problemActivitiesTO.setStartIndex(startIndex);
                problemActivitiesTO.setEndIndex(endIndex);

                paList = problemActivitiesBP.processGetpaList(problemActivitiesTO);

                request.setAttribute("ProblemActivities", paList);



                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //alterProblemAct
    /**
     * This method used to view Alter Page.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView alterProblemAct(HttpServletRequest request, HttpServletResponse response, ProblemActivitiesCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        problemActivitiesCommand = command;
        String pageTitle = "Alter Problem";
        request.setAttribute("pageTitle", pageTitle);
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Problem  >>     Alter Problem";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        path = "content/problemActivities/alterProblem.jsp";
        try {
//            if (!loginBP.checkAuthorisation(userFunctions, "Problem-Alter")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                // this for getting reselt set for single vendorId
                int problemId = Integer.parseInt(request.getParameter("problemId"));


                request.getAttribute("problemId");
                ArrayList problemDetailUnique = new ArrayList();
                problemDetailUnique = problemActivitiesBP.processProblemGetsDetail(problemId);
                request.setAttribute("GetProblemDetailUnique", problemDetailUnique);
                request.setAttribute("problemId", problemId);

                ArrayList sectionList = new ArrayList();
                sectionList = sectionBP.processGetSectionList();
                request.setAttribute("SectionList", sectionList);
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//            }



        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Vendor data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to handle Alter Vendor Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView alterProblem(HttpServletRequest request, HttpServletResponse response, ProblemActivitiesCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        problemActivitiesCommand = command;
        String pageTitle = "Alter Problem";
        String path = "";
        String menuPath = "Service Plan >> Manage Problem";
        ModelAndView mv = new ModelAndView();

        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        path = "content/problemActivities/manageProblem.jsp";
        HttpSession session = request.getSession();

        int userId = (Integer) session.getAttribute("userId");

        try {
            ArrayList sectionList = new ArrayList();
            sectionList = sectionBP.processGetSectionList();
            request.setAttribute("SectionList", sectionList);

            ProblemActivitiesTO problemActivitiesTO = new ProblemActivitiesTO();
            int problemId = Integer.parseInt(problemActivitiesCommand.getProblemId());

            if (problemActivitiesCommand.getProblemId() != null && problemActivitiesCommand.getProblemId() != "") {
                problemActivitiesTO.setProblemId(problemActivitiesCommand.getProblemId());

            }
            if (problemActivitiesCommand.getProblemName() != null && problemActivitiesCommand.getProblemName() != "") {
                problemActivitiesTO.setProblemName(problemActivitiesCommand.getProblemName());

            }
            if (problemActivitiesCommand.getDiscription() != null && problemActivitiesCommand.getDiscription() != "") {
                problemActivitiesTO.setDiscription(problemActivitiesCommand.getDiscription());

            }
            if (problemActivitiesCommand.getSectionId() != null && problemActivitiesCommand.getSectionId() != "") {
                problemActivitiesTO.setSectionId(problemActivitiesCommand.getSectionId());

            }
            if (problemActivitiesCommand.getActiveInd() != null && problemActivitiesCommand.getActiveInd() != "") {
                problemActivitiesTO.setActiveInd(problemActivitiesCommand.getActiveInd());

            }

            int status = 0;
            status = problemActivitiesBP.processUpdateProblemDetails(problemActivitiesTO, problemId);
            ArrayList paList = new ArrayList();
            mv = manageProblemAct(request, response, command);
//            request.setAttribute("ProblemActivities", paList);
//            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Problem Altered Successfully");
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve vendor data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    /**
     * This method used to handle Alter Vendor Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView addProblemPage(HttpServletRequest request, HttpServletResponse response, ProblemActivitiesCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        problemActivitiesCommand = command;
        String pageTitle = "Add Problem";

        request.setAttribute("pageTitle", pageTitle);
        String path = "";
        String menuPath = "Problem >> Add Problem";
        path = "content/problemActivities/addProblem.jsp";
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        int userId = (Integer) session.getAttribute("userId");

        try {
//            if (!loginBP.checkAuthorisation(userFunctions, "Problem-Add")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                ProblemActivitiesTO problemActivitiesTO = new ProblemActivitiesTO();


                ArrayList sectionList = new ArrayList();
                sectionList = sectionBP.processGetSectionList();
                request.setAttribute("SectionList", sectionList);


                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve vendor data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //addProblem
    /**
     * This method used to handle Alter Vendor Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView addProblem(HttpServletRequest request, HttpServletResponse response, ProblemActivitiesCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        problemActivitiesCommand = command;
        String pageTitle = "Add Problem";
        request.setAttribute("pageTitle", pageTitle);
        String path = "";
        path = "content/problemActivities/manageProblem.jsp";
        HttpSession session = request.getSession();
        ModelAndView mv = new ModelAndView();

        int userId = (Integer) session.getAttribute("userId");

        try {

            ProblemActivitiesTO problemActivitiesTO = new ProblemActivitiesTO();


            ArrayList sectionList = new ArrayList();
            sectionList = sectionBP.processGetSectionList();
            request.setAttribute("SectionList", sectionList);


            if (problemActivitiesCommand.getProblemName() != null && problemActivitiesCommand.getProblemName() != "") {
                problemActivitiesTO.setProblemName(problemActivitiesCommand.getProblemName());

            }
            if (problemActivitiesCommand.getDiscription() != null && problemActivitiesCommand.getDiscription() != "") {
                problemActivitiesTO.setDiscription(problemActivitiesCommand.getDiscription());

            }
            if (problemActivitiesCommand.getSectionId() != null && problemActivitiesCommand.getSectionId() != "") {
                problemActivitiesTO.setSectionId(problemActivitiesCommand.getSectionId());

            }


            int status = 0;
            status = problemActivitiesBP.processAddProblemDetails(problemActivitiesTO, userId);

            ArrayList paList = new ArrayList();
            mv = manageProblemAct(request,response,command);
//            request.setAttribute("ProblemActivities", paList);
//            String menuPath = "Problem >> Add Problem";
//            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "problem Added Successfully");
//            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve vendor data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }
}






