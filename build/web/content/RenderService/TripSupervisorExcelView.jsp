<%@page import="java.text.SimpleDateFormat"%>
<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
        <%@ page import="ets.domain.operation.business.OperationTO" %>

        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }

        </style>
    </head>    
    <body>
        <form name="viewTrips" action=""  method="post">
            <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "Trip_Sheet_Report-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
            %>
            <br/>            
            <c:if test = "${tripDetails != null}" >
                <%
                             DecimalFormat df = new DecimalFormat("0.00##");
                            int index = 1;
                            int total = 0;
                            String classText2 = "";
                            String imageName="";
                            String regno = "",tripDate = "",routeName = "",empName = "",outKM = "",freight = "";
                            String inKM = "",totalTonnage = "",deliveredTonnage = "",status = "",tripId = "";
                            double grandTotalTonnage = 0,grandTotalFreight = 0,grandDeliveredTonnage = 0;
                            ArrayList tripDetails = (ArrayList) request.getAttribute("tripDetails");
                            Iterator tripDet = tripDetails.iterator();
                            OperationTO optTO = null;
                            int flag  = 0;
                            if (tripDetails.size() != 0) {
                %>
                <table  border="1" class="border" align="center" width="95%" cellpadding="0" cellspacing="0" id="bg">
                    <tr>
                                    <td class="contentsub" height="30">S.No</td>
                                    <td class="contentsub" height="30">Trip Id</td>
                                    <td class="contentsub" height="30">Vehicle No</td>
                                    <td class="contentsub" height="30">Trip Date</td>
                                    <td class="contentsub" height="30">Route Name</td>
                                    <td class="contentsub" height="30">Driver Name</td>
                                    <td class="contentsub" height="30">OUT KM</td>
                                    <td class="contentsub" height="30">IN KM</td>
                                    <td class="contentsub" height="30">Freight</td>
                                    <td class="contentsub" height="30">Total Tonnage</td>
                                    <td class="contentsub" height="30">Delivered Tonnage</td>
                                    <td class="contentsub" height="30">Status</td>
                                   
                                    <!--<td class="contentsub" height="30">Map View</td>-->
                                </tr>
                                <%
                                while (tripDet.hasNext()) {
                                    index++;
                                    total++;
                                    int oddEven2 = index % 2;
                                    if (oddEven2 > 0) {
                                        classText2 = "text2";
                                        imageName="flag2";
                                    } else {
                                        classText2 = "text1";
                                        imageName="flag2";
                                    }


                                     optTO = new OperationTO();
                                    optTO = (OperationTO) tripDet.next();

                                    flag = optTO.getFlag();

                                    regno = optTO.getRegno();
                                    if(regno == null){
                                        regno = "";
                                        }
                                    tripId = optTO.getTripId();
                                    if(tripId == null){
                                        tripId = "";
                                        }
                                    tripDate = optTO.getTripDate();
                                    if(tripDate == null){
                                        tripDate = "";
                                        }
                                    routeName = optTO.getRouteName();
                                    if(routeName == null){
                                        routeName = "";
                                        }
                                    freight = optTO.getRevenue();
                                    if(freight == null){
                                        freight = "";
                                        }
                                    grandTotalFreight += Double.parseDouble(freight);
                                    empName = optTO.getEmpName();
                                    if(empName == null){
                                        empName = "";
                                        }
                                    outKM = optTO.getOutKM();
                                    if(outKM == null){
                                        outKM = "0";
                                        }
                                    inKM = optTO.getInKM();
                                    if(inKM == null){
                                        inKM = "0";
                                        }
                                    totalTonnage = optTO.getTotalTonnage();
                                    if(totalTonnage == null){
                                        totalTonnage = "";
                                        }
                                    grandTotalTonnage += Double.parseDouble(totalTonnage);
                                    deliveredTonnage = optTO.getDeliveredTonnage();
                                    if(deliveredTonnage == null){
                                        deliveredTonnage = "0";
                                        }
                                    grandDeliveredTonnage += Double.parseDouble(deliveredTonnage);
                                    status = optTO.getStatus();
                                    if(status == null){
                                        status = "";
                                        }



                                        %>
                                        <tr>
                                           
                                        <td class="<%=classText2%>"  height="30" align="left"><%=index%></td>
                                        <td class="<%=classText2%>"  height="30"><%=tripId%></td>
                                        <td class="<%=classText2%>"  height="30"><%=regno%></td>
                                        <td class="<%=classText2%>"  height="30"><%=tripDate%></td>
                                        <td class="<%=classText2%>"  height="30"><%=routeName%></td>
                                        <td class="<%=classText2%>"  height="30"><%=empName%>&nbsp;&nbsp;</td>
                                        <td class="<%=classText2%>"  height="30"><%=outKM%>&nbsp;&nbsp;</td>
                                        <td class="<%=classText2%>"  height="30"><%=inKM%>&nbsp;&nbsp;</td>
                                        <td class="<%=classText2%>"  height="30"><%=freight%>&nbsp;&nbsp;</td>
                                        <td class="<%=classText2%>"  height="30"><%=totalTonnage%>&nbsp;&nbsp;</td>
                                        <td class="<%=classText2%>"  height="30"><%=deliveredTonnage%>&nbsp;&nbsp;</td>
                                        <td class="<%=classText2%>"  height="30"><%=status%></td>
                                       
                                        </tr>

                                <%



                                    }

%>
                        <tr>
                            <td class="<%=classText2%>"  height="30" colspan="6" >&nbsp;</td>
                            <td class="<%=classText2%>"  height="30" colspan="2"><b>Grand Total</b></td>
                            <td class="<%=classText2%>" align="right"  height="30"><b><%=df.format(grandTotalFreight)%></b></td>
                            <td class="<%=classText2%>" align="right"  height="30"><b><%=df.format(grandTotalTonnage)%></b></td>
                            <td class="<%=classText2%>" align="right"  height="30"><b><%=df.format(grandDeliveredTonnage)%></b></td>
                            <td class="<%=classText2%>"  height="30" >&nbsp;</td>
                        </tr>
                </table>
                <%
                            }
                %>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>