/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.vehicle.web;

/**
 *
 * @author karudaiyar Subramaniam
 */
public class VehicleCommand {

    public VehicleCommand() {
    }
    
    //raj
     private String fleetTypeId = "";
     private String vehicleTypeId = "";
     
    private String paymentDatePermit = "";
    private String clearanceIdPermit = "";
    private String permitid = "";
    private String paymentTypePermit = "";
    private String bankIdPermit = "";
    private String bankBranchIdPermit = "";
    private String chequeDatePermit = "";
    private String chequeNoPermit = "";
    private String chequeAmountPermit = "";
    private String clearanceDatePermit = "";
    
    private String paymentDateFC = "";
    private String clearanceIdFC = "";
    private String fcid = "";
    private String paymentTypeFC = "";
    private String bankIdFC = "";
    private String bankBranchIdFC = "";
    private String creditLedgerIdFC = "";
    private String chequeDateFC = "";
    private String chequeNoFC = "";
    private String chequeAmountFC = "";
    private String clearanceDateFC = "";
    
    
    private String paymentDateRT = "";
    private String clearanceIdRT = "";
    private String roadTaxId = "";
    private String paymentTypeRT = "";
    private String bankIdRT = "";
    private String bankBranchIdRT = "";
    private String chequeDateRT = "";
    private String chequeNoRT = "";
    private String chequeAmountRT = "";
    private String clearanceDateRT = "";
    
    private String vendorId1 = "";
    private String cashOnHandLedgerId = "";
    private String cashOnHandLedgerCode = "";
    private String ledgerCode = "";
    private String creditLedgerId = "";
    private String clearanceId = "";
    private String insuranceId = "";
    private String paymentType = "";
    private String bankId = "";
    private String bankBranchId = "";
    private String chequeDate = "";
    private String chequeNo = "";
    private String chequeAmount = "";
    private String clearanceDate = "";
    
    private String ledgerId = "";
    private String branchId = "";
    private String branchName = "";
    //Natraj
    
    private String plannedDischargePort = "";
    private String status = "";
    private String voyageNo = "";
    private String linersName = "";
    private String agentName = ""; private String issuingOffice= "";
    private String ownerName= "";
     private String regId= "";
      private String dateOfReg= "";
      private String truckNo= "";
      private String transportionType= "";
      private String ownerId= "";
      private String color= "";
      private String noOfPersons= "";
       private String odometer= "";
      private String reportNo= "";
    private String plannedArrivalDate = "";
    private String plannedArrivalPort = "";
    private String plannedDischargeDate = "";
    private String dischargeVesselName = "";
    private String vesselId = "";
    private String vesselName = "";
    private String vesselCode = "";
    private String vesselType = "";
    private String linerCode = "";
    private String[] vehicleMfrs = null;
    private String[] trailerMfrs = null;
    private String[] batteryMfrs = null;
    private String[] tyreMfrs = null;
    private String vehicleMfr = "";
    private String trailerMfr = "";
    private String batteryMfr = "";
    private String tyreMfr = "";
     private String tyreNo = "";
     private String depthVal = "";
     private String vehicleYear = "";
     private String depreciation = "";
     private String yearCost = "";
     private String perMonth = "";
     private String vehicleAmt = "";
    private String monthlyAmount = "";
    private String balanceAmount = "";
    private String emiPayer = "";
    private String amountPaid = "";
    private String paymentMadeOn = "";
    private String paymentDate = "";
    private String paymentId = "";
    private String emiAmtPaid = "";
    private String balanceEmi = "";
    private String lastEmiBalance = "";
    private String totalBalance = "";
    private String insPolicy = "";
    private String prevPolicy = "";
    private String packageType = "";
    private String insName = "";
    private String insAddress = "";
    private String insMobileNo = "";
    private String insPolicyNo = "";
    private String insIdvValue = "";
    private String fromDate1 = "";
    private String toDate1 = "";
    private String premiunAmount = "";
    private String axleDetailId = "";
    private String positionName = "";
    private String positionNo = "";
    private String updateValue = "";
    private String updateType = "";
    private String axleTypeId = "";
    private String vName = "";
    private String cType = "";
    private String eRegNo = "";
    private String rRegNo = "";
    private String rDate = "";
    private String ownerShips = "";

    private String trailerId = "";
    private String trailerNo = "";
    private String ownership = "";
    private String mfrId = "";
    private String groupId = "";
    private String bodyGroupId = "";
    private String groupIds[] = null;
    private String bodyGroupIds[] = null;
    private String mfrName = "";
    private String[] mfrNames = null;
    private String[] mfrIds = null;
    private String[] activeInds = null;
    private String[] modelNames = null;
    private String[] modelIds = null;
    private String[] tyreType = null;
    private String[] serialNo = null;
    private String[] positionId = null;
    private String[] itemIds = null;

    private String[] tyreDate = null;

    private String activeInd = null;
    private String description = "";
    private String[] selectedIndex = null;
    private String[] descriptions = null;
    private String typeId = "";
    private String[] typeIds = null;
    private String typeName = "";
    private String[] typeNames = null;
    private String fuelId = "";
    private String[] fuelIds = null;
    private String fuelName = "";
    private String[] fuelNames = null;
    private String modelId = "";
    private String usageId = "";
    private String usageName = "";
    private String classId = "";
    private String className = "";
    private String war_period = "";
    private String dateOfSale = "";
    private String registrationDate = "";
    private String engineNo = "";
    private String chassisNo = "";
    private String nextFCDate = "";
    private String seatCapacity = "";
    private String toCity = "";
    private String state = "";
    private String marketVehiclePercentage = "";
    private String opId = "";
    private String spId = "";
    private String regNo = "";
    private String regno = "";
    private String kmReading = "";
    private String actualKm = "";
    private String totalKm = "";
    private String reason = "";
    private String hmReading = "";
    private String custId = "";
    private String dailyKm = "";
    private String dailyHm = "";
    private String vehicleId = "";
    private String[] percentage = null;
    private String[] tyreIds = null;
    private String[] posIds = null;
    private String[] positionIds = null;
    private String fservice = "";
    private String perc = "";
    private String[] tyreNos = null;

    private String vehicleCost = "0";
    private String vehicleDepreciation = "0";
    private String vehicleColor = "";
    private String vendorId = "";

    //----------Insurance------------------
    private String insuranceCompanyName = "";
    private String premiumNo = "";
    private String premiumPaidDate = "";
    private double premiumPaidAmount = 0;
    private double vehicleValue = 0;
    private String premiumExpiryDate = "";
    private String insuarnceNCB = "";
    private String insuarnceRemarks = "";

    //---------Accident---------------
    private String accidentSpot = "";
    private String accidentDate = "";
    private String driverName = "";
    private String policeStation = "";
    private String firNo = "";
    private String firDate = "";
    private String firPreparedBy = "";
    private String accidentRemarks = "";

    //---------Road Tax--------------------
    private String roadTaxReceiptNo = "";
    private String roadTaxReceiptDate = "";
    private String roadTaxPaidLocation = "";
    private String roadTaxPaidOffice = "";
    private String roadTaxPeriod = "";
    private double roadTaxPaidAmount = 0;
    private String roadTaxRemarks = "";

    //---------FC Details--------------------
    private String rtoDetail = "";
    private String fcDate = "";
    private String fcReceiptNo = "";
    private double fcAmount = 0;
    private String fcExpiryDate = "";
    private String fcRemarks = "";

    //Add Route Start
    private String routeId = "";
    private String routeCode = "";
    private String fromLocation = "";
    private String toLocation = "";
    private String viaRoute = "";
    private String km = "";
    private String tollAmount = "";
    private String tonnageRate = "";
    private String sla = "";
    private String eligibleTrip = "";
    private String driverBata = "";

    private String companyname = "";
    private String premiumno = "";
    private String premiumpaiddate = "";
    private String premiumamount = "";
    private String vehiclevalue = "";
    private String expirydate = "";
    private String ncb = "";
    private String remarks = "";
    private String insuranceid = "";
    private String id = "";
    private String roadtaxreceiptno = "";
    private String roadtaxreceiptdate = "";
    private String roadtaxpaidlocation = "";
    private String roadtaxamount = "";
    private String roadtaxperiod = "";
    private String rtodetail = "";
    private String receiptno = "";
    private String fcamount = "";
    private String fcexpirydate = "";
    private String fcdate = "";

    private String[] routeIds = null;
    private String[] routeCodes = null;
    private String[] fromLocations = null;
    private String[] toLocations = null;
    private String[] viaRoutes = null;
    private String[] kms = null;
    private String[] tollAmounts = null;
    private String[] tonnageRates = null;
    private String[] driverBatas = null;

    private String permitType = "";
    private String permitNo = "";
    private double permitAmount = 0;
    private String permitPaidDate = "";
    private String permitExpiryDate = "";
    private String permitRemarks = "";

    private String custName = "";
    private String agreementYear = "";
    private String fromDate = "";
    private String toDate = "";
    private String street = "";
    private String city = "";
    private String phoneNo = "";
    private String mobileNo = "";
    private String emailId = "";
    private String asset = "";
    private String leasingCustId = "";
    private String bankerName = "";
    private String bankerAddress = "";
    private String financeAmount = "";
    private String roi = "";
    private String interestType = "";
    private String emiMonths = "";
    private String emiAmount = "";
    private String emiStartDate = "";
    private String emiEndDate = "";
    private String emiPayDay = "";
    private String payMode = "";

    private String vehicleMake = "";
    private String vehicleModel = "";
    private String vehicleUsage = "";
    private String amcCompanyName = "";
    private String amcAmount = "";
    private String amcDuration = "";
    private String amcChequeDate = "";
    private String amcChequeNo = "";
    private String amcFromDate = "";
    private String amcToDate = "";
    private String amcRemarks = "";
    private String roadTaxFromDate = "";
    private String nextRoadTaxDate = "";
    private String amcId = "";
    //Add Route End

    //clpl manage vehicle
    private String gpsSystem = "";
    private String warrantyDate = "";
// vehicle profile
    private String receiptNo = "";
    private String roadTaxAmount = "";
    private String roadTaxToDate = "";

    private String fromLocationId = "";
    private String toLocationId = "";

    private String ToLocationid = "";
    private String tonnageRateMarket = "";

//Brattle Foods
    private String axles = "";
    private String dailyRunKm = "";
    private String dailyRunHm = "";
    private String gpsSystemId = "";
    private String tonnage = "";
    private String capacity = "";
    private String[] tonnages = null;
    private String[] capacities = null;

//Brattle Foods
//    pavi
//     private String issuingOffice= "";
//    private String ownerName= "";
//     private String regId= "";
//      private String dateOfReg= "";
//      private String truckNo= "";
//      private String transportionType= "";
//      private String ownerId= "";
//      private String color= "";
//      private String noOfPersons= "";
//       private String odometer= "";
//      private String reportNo= "";
 private String depreciationType= "";
      private String vehicleDepreciationCost= "";

    public String getDepreciationType() {
        return depreciationType;
    }

    public void setDepreciationType(String depreciationType) {
        this.depreciationType = depreciationType;
    }

    public String getVehicleDepreciationCost() {
        return vehicleDepreciationCost;
    }

    public void setVehicleDepreciationCost(String vehicleDepreciationCost) {
        this.vehicleDepreciationCost = vehicleDepreciationCost;
    }
      
    public String getIssuingOffice() {
        return issuingOffice;
    }

    public void setIssuingOffice(String issuingOffice) {
        this.issuingOffice = issuingOffice;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getRegId() {
        return regId;
    }

    public void setRegId(String regId) {
        this.regId = regId;
    }

    public String getDateOfReg() {
        return dateOfReg;
    }

    public void setDateOfReg(String dateOfReg) {
        this.dateOfReg = dateOfReg;
    }

    public String getTruckNo() {
        return truckNo;
    }

    public void setTruckNo(String truckNo) {
        this.truckNo = truckNo;
    }

    public String getTransportionType() {
        return transportionType;
    }

    public void setTransportionType(String transportionType) {
        this.transportionType = transportionType;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getNoOfPersons() {
        return noOfPersons;
    }

    public void setNoOfPersons(String noOfPersons) {
        this.noOfPersons = noOfPersons;
    }

    public String getOdometer() {
        return odometer;
    }

    public void setOdometer(String odometer) {
        this.odometer = odometer;
    }

    public String getReportNo() {
        return reportNo;
    }

    public void setReportNo(String reportNo) {
        this.reportNo = reportNo;
    }
      
    public String getActualKm() {
        return actualKm;
    }

    public void setActualKm(String actualKm) {
        this.actualKm = actualKm;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getRegno() {
        return regno;
    }

    public void setRegno(String regno) {
        this.regno = regno;
    }

    public String getTotalKm() {
        return totalKm;
    }

    public void setTotalKm(String totalKm) {
        this.totalKm = totalKm;
    }

    public String getChassisNo() {
        return chassisNo;
    }

    public void setChassisNo(String chassisNo) {
        this.chassisNo = chassisNo;
    }

    public String getDateOfSale() {
        return dateOfSale;
    }

    public void setDateOfSale(String dateOfSale) {
        this.dateOfSale = dateOfSale;
    }

    public String getEngineNo() {
        return engineNo;
    }

    public void setEngineNo(String engineNo) {
        this.engineNo = engineNo;
    }

    public String getNextFCDate() {
        return nextFCDate;
    }

    public void setNextFCDate(String nextFCDate) {
        this.nextFCDate = nextFCDate;
    }

    public String getOpId() {
        return opId;
    }

    public void setOpId(String opId) {
        this.opId = opId;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getSeatCapacity() {
        return seatCapacity;
    }

    public void setSeatCapacity(String seatCapacity) {
        this.seatCapacity = seatCapacity;
    }

    public String getSpId() {
        return spId;
    }

    public void setSpId(String spId) {
        this.spId = spId;
    }

    public String getWar_period() {
        return war_period;
    }

    public void setWar_period(String war_period) {
        this.war_period = war_period;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getUsageId() {
        return usageId;
    }

    public void setUsageId(String usageId) {
        this.usageId = usageId;
    }

    public String getUsageName() {
        return usageName;
    }

    public void setUsageName(String usageName) {
        this.usageName = usageName;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getMfrId() {
        return mfrId;
    }

    public void setMfrId(String mfrId) {
        this.mfrId = mfrId;
    }
    private String modelName = "";

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getFuelId() {
        return fuelId;
    }

    public void setFuelId(String fuelId) {
        this.fuelId = fuelId;
    }

    public String[] getFuelIds() {
        return fuelIds;
    }

    public void setFuelIds(String[] fuelIds) {
        this.fuelIds = fuelIds;
    }

    public String getFuelName() {
        return fuelName;
    }

    public void setFuelName(String fuelName) {
        this.fuelName = fuelName;
    }

    public String[] getFuelNames() {
        return fuelNames;
    }

    public void setFuelNames(String[] fuelNames) {
        this.fuelNames = fuelNames;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String[] getTypeIds() {
        return typeIds;
    }

    public void setTypeIds(String[] typeIds) {
        this.typeIds = typeIds;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String[] getTypeNames() {
        return typeNames;
    }

    public void setTypeNames(String[] typeNames) {
        this.typeNames = typeNames;
    }

    public String[] getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String[] descriptions) {
        this.descriptions = descriptions;
    }

    public String[] getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(String[] selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String[] getMfrIds() {
        return mfrIds;
    }

    public void setMfrIds(String[] mfrIds) {
        this.mfrIds = mfrIds;
    }

    public String getMfrName() {
        return mfrName;
    }

    public void setMfrName(String mfrName) {
        this.mfrName = mfrName;
    }

    public String[] getMfrNames() {
        return mfrNames;
    }

    public void setMfrNames(String[] mfrNames) {
        this.mfrNames = mfrNames;
    }

    public String[] getActiveInds() {
        return activeInds;
    }

    public void setActiveInds(String[] activeInds) {
        this.activeInds = activeInds;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String[] getModelIds() {
        return modelIds;
    }

    public void setModelIds(String[] modelIds) {
        this.modelIds = modelIds;
    }

    public String[] getTyreType() {
        return tyreType;
    }

    public void setTyreType(String[] tyreType) {
        this.tyreType = tyreType;
    }

    public String[] getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String[] serialNo) {
        this.serialNo = serialNo;
    }

    public String[] getPositionId() {
        return positionId;
    }

    public void setPositionId(String[] positionId) {
        this.positionId = positionId;
    }

    public String[] getModelNames() {
        return modelNames;
    }

    public void setModelNames(String[] modelNames) {
        this.modelNames = modelNames;
    }

    public String[] getItemIds() {
        return itemIds;
    }

    public void setItemIds(String[] itemIds) {
        this.itemIds = itemIds;
    }

    public String getHmReading() {
        return hmReading;
    }

    public void setHmReading(String hmReading) {
        this.hmReading = hmReading;
    }

    public String getKmReading() {
        return kmReading;
    }

    public void setKmReading(String kmReading) {
        this.kmReading = kmReading;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getDailyHm() {
        return dailyHm;
    }

    public void setDailyHm(String dailyHm) {
        this.dailyHm = dailyHm;
    }

    public String getDailyKm() {
        return dailyKm;
    }

    public void setDailyKm(String dailyKm) {
        this.dailyKm = dailyKm;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String[] getTyreIds() {
        return tyreIds;
    }

    public void setTyreIds(String[] tyreIds) {
        this.tyreIds = tyreIds;
    }

    public String[] getPositionIds() {
        return positionIds;
    }

    public void setPositionIds(String[] positionIds) {
        this.positionIds = positionIds;
    }

    public String getFservice() {
        return fservice;
    }

    public void setFservice(String fservice) {
        this.fservice = fservice;
    }

    public String[] getPosIds() {
        return posIds;
    }

    public void setPosIds(String[] posIds) {
        this.posIds = posIds;
    }

    public String[] getTyreNos() {
        return tyreNos;
    }

    public void setTyreNos(String[] tyreNos) {
        this.tyreNos = tyreNos;
    }

    public String[] getPercentage() {
        return percentage;
    }

    public void setPercentage(String[] percentage) {
        this.percentage = percentage;
    }

    public String getPerc() {
        return perc;
    }

    public void setPerc(String perc) {
        this.perc = perc;
    }

    public String getBodyGroupId() {
        return bodyGroupId;
    }

    public void setBodyGroupId(String bodyGroupId) {
        this.bodyGroupId = bodyGroupId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String[] getBodyGroupIds() {
        return bodyGroupIds;
    }

    public void setBodyGroupIds(String[] bodyGroupIds) {
        this.bodyGroupIds = bodyGroupIds;
    }

    public String[] getGroupIds() {
        return groupIds;
    }

    public void setGroupIds(String[] groupIds) {
        this.groupIds = groupIds;
    }

    public String getVehicleColor() {
        return vehicleColor;
    }

    public void setVehicleColor(String vehicleColor) {
        this.vehicleColor = vehicleColor;
    }

    public String getVehicleCost() {
        return vehicleCost;
    }

    public void setVehicleCost(String vehicleCost) {
        this.vehicleCost = vehicleCost;
    }

    public String getVehicleDepreciation() {
        return vehicleDepreciation;
    }

    public void setVehicleDepreciation(String vehicleDepreciation) {
        this.vehicleDepreciation = vehicleDepreciation;
    }

    public String getInsuarnceNCB() {
        return insuarnceNCB;
    }

    public void setInsuarnceNCB(String insuarnceNCB) {
        this.insuarnceNCB = insuarnceNCB;
    }

    public String getInsuarnceRemarks() {
        return insuarnceRemarks;
    }

    public void setInsuarnceRemarks(String insuarnceRemarks) {
        this.insuarnceRemarks = insuarnceRemarks;
    }

    public String getInsuranceCompanyName() {
        return insuranceCompanyName;
    }

    public void setInsuranceCompanyName(String insuranceCompanyName) {
        this.insuranceCompanyName = insuranceCompanyName;
    }

    public String getPremiumExpiryDate() {
        return premiumExpiryDate;
    }

    public void setPremiumExpiryDate(String premiumExpiryDate) {
        this.premiumExpiryDate = premiumExpiryDate;
    }

    public String getPremiumNo() {
        return premiumNo;
    }

    public void setPremiumNo(String premiumNo) {
        this.premiumNo = premiumNo;
    }

    public double getPremiumPaidAmount() {
        return premiumPaidAmount;
    }

    public void setPremiumPaidAmount(double premiumPaidAmount) {
        this.premiumPaidAmount = premiumPaidAmount;
    }

    public String getPremiumPaidDate() {
        return premiumPaidDate;
    }

    public void setPremiumPaidDate(String premiumPaidDate) {
        this.premiumPaidDate = premiumPaidDate;
    }

    public double getVehicleValue() {
        return vehicleValue;
    }

    public void setVehicleValue(double vehicleValue) {
        this.vehicleValue = vehicleValue;
    }

    public String getAccidentDate() {
        return accidentDate;
    }

    public void setAccidentDate(String accidentDate) {
        this.accidentDate = accidentDate;
    }

    public String getAccidentRemarks() {
        return accidentRemarks;
    }

    public void setAccidentRemarks(String accidentRemarks) {
        this.accidentRemarks = accidentRemarks;
    }

    public String getAccidentSpot() {
        return accidentSpot;
    }

    public void setAccidentSpot(String accidentSpot) {
        this.accidentSpot = accidentSpot;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getFirDate() {
        return firDate;
    }

    public void setFirDate(String firDate) {
        this.firDate = firDate;
    }

    public String getFirNo() {
        return firNo;
    }

    public void setFirNo(String firNo) {
        this.firNo = firNo;
    }

    public String getFirPreparedBy() {
        return firPreparedBy;
    }

    public void setFirPreparedBy(String firPreparedBy) {
        this.firPreparedBy = firPreparedBy;
    }

    public String getPoliceStation() {
        return policeStation;
    }

    public void setPoliceStation(String policeStation) {
        this.policeStation = policeStation;
    }

    public double getRoadTaxPaidAmount() {
        return roadTaxPaidAmount;
    }

    public void setRoadTaxPaidAmount(double roadTaxPaidAmount) {
        this.roadTaxPaidAmount = roadTaxPaidAmount;
    }

    public String getRoadTaxPaidLocation() {
        return roadTaxPaidLocation;
    }

    public void setRoadTaxPaidLocation(String roadTaxPaidLocation) {
        this.roadTaxPaidLocation = roadTaxPaidLocation;
    }

    public String getRoadTaxPaidOffice() {
        return roadTaxPaidOffice;
    }

    public void setRoadTaxPaidOffice(String roadTaxPaidOffice) {
        this.roadTaxPaidOffice = roadTaxPaidOffice;
    }

    public String getRoadTaxPeriod() {
        return roadTaxPeriod;
    }

    public void setRoadTaxPeriod(String roadTaxPeriod) {
        this.roadTaxPeriod = roadTaxPeriod;
    }

    public String getRoadTaxReceiptDate() {
        return roadTaxReceiptDate;
    }

    public void setRoadTaxReceiptDate(String roadTaxReceiptDate) {
        this.roadTaxReceiptDate = roadTaxReceiptDate;
    }

    public String getRoadTaxReceiptNo() {
        return roadTaxReceiptNo;
    }

    public void setRoadTaxReceiptNo(String roadTaxReceiptNo) {
        this.roadTaxReceiptNo = roadTaxReceiptNo;
    }

    public String getRoadTaxRemarks() {
        return roadTaxRemarks;
    }

    public void setRoadTaxRemarks(String roadTaxRemarks) {
        this.roadTaxRemarks = roadTaxRemarks;
    }

    public double getFcAmount() {
        return fcAmount;
    }

    public void setFcAmount(double fcAmount) {
        this.fcAmount = fcAmount;
    }

    public String getFcDate() {
        return fcDate;
    }

    public void setFcDate(String fcDate) {
        this.fcDate = fcDate;
    }

    public String getFcExpiryDate() {
        return fcExpiryDate;
    }

    public void setFcExpiryDate(String fcExpiryDate) {
        this.fcExpiryDate = fcExpiryDate;
    }

    public String getFcReceiptNo() {
        return fcReceiptNo;
    }

    public void setFcReceiptNo(String fcReceiptNo) {
        this.fcReceiptNo = fcReceiptNo;
    }

    public String getFcRemarks() {
        return fcRemarks;
    }

    public void setFcRemarks(String fcRemarks) {
        this.fcRemarks = fcRemarks;
    }

    public String getRtoDetail() {
        return rtoDetail;
    }

    public void setRtoDetail(String rtoDetail) {
        this.rtoDetail = rtoDetail;
    }

    //Add Route Start
    public String getDriverBata() {
        return driverBata;
    }

    public void setDriverBata(String driverBata) {
        this.driverBata = driverBata;
    }

    public String getEligibleTrip() {
        return eligibleTrip;
    }

    public void setEligibleTrip(String eligibleTrip) {
        this.eligibleTrip = eligibleTrip;
    }

    public String getKm() {
        return km;
    }

    public void setKm(String km) {
        this.km = km;
    }

    public String getRouteCode() {
        return routeCode;
    }

    public void setRouteCode(String routeCode) {
        this.routeCode = routeCode;
    }

    public String getTollAmount() {
        return tollAmount;
    }

    public void setTollAmount(String tollAmount) {
        this.tollAmount = tollAmount;
    }

    public String getTonnageRate() {
        return tonnageRate;
    }

    public void setTonnageRate(String tonnageRate) {
        this.tonnageRate = tonnageRate;
    }

    public String getViaRoute() {
        return viaRoute;
    }

    public void setViaRoute(String viaRoute) {
        this.viaRoute = viaRoute;
    }

    public String getFromLocation() {
        return fromLocation;
    }

    public void setFromLocation(String fromLocation) {
        this.fromLocation = fromLocation;
    }

    public String getToLocation() {
        return toLocation;
    }

    public void setToLocation(String toLocation) {
        this.toLocation = toLocation;
    }

    public String[] getDriverBatas() {
        return driverBatas;
    }

    public void setDriverBatas(String[] driverBatas) {
        this.driverBatas = driverBatas;
    }

    public String[] getFromLocations() {
        return fromLocations;
    }

    public void setFromLocations(String[] fromLocations) {
        this.fromLocations = fromLocations;
    }

    public String[] getKms() {
        return kms;
    }

    public void setKms(String[] kms) {
        this.kms = kms;
    }

    public String[] getRouteCodes() {
        return routeCodes;
    }

    public void setRouteCodes(String[] routeCodes) {
        this.routeCodes = routeCodes;
    }

    public String[] getToLocations() {
        return toLocations;
    }

    public void setToLocations(String[] toLocations) {
        this.toLocations = toLocations;
    }

    public String[] getTollAmounts() {
        return tollAmounts;
    }

    public void setTollAmounts(String[] tollAmounts) {
        this.tollAmounts = tollAmounts;
    }

    public String[] getTonnageRates() {
        return tonnageRates;
    }

    public void setTonnageRates(String[] tonnageRates) {
        this.tonnageRates = tonnageRates;
    }

    public String[] getViaRoutes() {
        return viaRoutes;
    }

    public void setViaRoutes(String[] viaRoutes) {
        this.viaRoutes = viaRoutes;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String[] getRouteIds() {
        return routeIds;
    }

    public void setRouteIds(String[] routeIds) {
        this.routeIds = routeIds;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getExpirydate() {
        return expirydate;
    }

    public void setExpirydate(String expirydate) {
        this.expirydate = expirydate;
    }

    public String getFcamount() {
        return fcamount;
    }

    public void setFcamount(String fcamount) {
        this.fcamount = fcamount;
    }

    public String getFcdate() {
        return fcdate;
    }

    public void setFcdate(String fcdate) {
        this.fcdate = fcdate;
    }

    public String getFcexpirydate() {
        return fcexpirydate;
    }

    public void setFcexpirydate(String fcexpirydate) {
        this.fcexpirydate = fcexpirydate;
    }

    public String getFcid() {
        return fcid;
    }

    public void setFcid(String fcid) {
        this.fcid = fcid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInsuranceid() {
        return insuranceid;
    }

    public void setInsuranceid(String insuranceid) {
        this.insuranceid = insuranceid;
    }

    public String getNcb() {
        return ncb;
    }

    public void setNcb(String ncb) {
        this.ncb = ncb;
    }

    public String getPremiumamount() {
        return premiumamount;
    }

    public void setPremiumamount(String premiumamount) {
        this.premiumamount = premiumamount;
    }

    public String getPremiumno() {
        return premiumno;
    }

    public void setPremiumno(String premiumno) {
        this.premiumno = premiumno;
    }

    public String getPremiumpaiddate() {
        return premiumpaiddate;
    }

    public void setPremiumpaiddate(String premiumpaiddate) {
        this.premiumpaiddate = premiumpaiddate;
    }

    public String getReceiptno() {
        return receiptno;
    }

    public void setReceiptno(String receiptno) {
        this.receiptno = receiptno;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRoadtaxamount() {
        return roadtaxamount;
    }

    public void setRoadtaxamount(String roadtaxamount) {
        this.roadtaxamount = roadtaxamount;
    }

    public String getRoadtaxpaidlocation() {
        return roadtaxpaidlocation;
    }

    public void setRoadtaxpaidlocation(String roadtaxpaidlocation) {
        this.roadtaxpaidlocation = roadtaxpaidlocation;
    }

    public String getRoadtaxperiod() {
        return roadtaxperiod;
    }

    public void setRoadtaxperiod(String roadtaxperiod) {
        this.roadtaxperiod = roadtaxperiod;
    }

    public String getRoadtaxreceiptdate() {
        return roadtaxreceiptdate;
    }

    public void setRoadtaxreceiptdate(String roadtaxreceiptdate) {
        this.roadtaxreceiptdate = roadtaxreceiptdate;
    }

    public String getRoadtaxreceiptno() {
        return roadtaxreceiptno;
    }

    public void setRoadtaxreceiptno(String roadtaxreceiptno) {
        this.roadtaxreceiptno = roadtaxreceiptno;
    }

    public String getRtodetail() {
        return rtodetail;
    }

    public void setRtodetail(String rtodetail) {
        this.rtodetail = rtodetail;
    }

    public String getVehiclevalue() {
        return vehiclevalue;
    }

    public void setVehiclevalue(String vehiclevalue) {
        this.vehiclevalue = vehiclevalue;
    }

    public double getPermitAmount() {
        return permitAmount;
    }

    public void setPermitAmount(double permitAmount) {
        this.permitAmount = permitAmount;
    }

    public String getPermitExpiryDate() {
        return permitExpiryDate;
    }

    public void setPermitExpiryDate(String permitExpiryDate) {
        this.permitExpiryDate = permitExpiryDate;
    }

    public String getPermitNo() {
        return permitNo;
    }

    public void setPermitNo(String permitNo) {
        this.permitNo = permitNo;
    }

    public String getPermitPaidDate() {
        return permitPaidDate;
    }

    public void setPermitPaidDate(String permitPaidDate) {
        this.permitPaidDate = permitPaidDate;
    }

    public String getPermitRemarks() {
        return permitRemarks;
    }

    public void setPermitRemarks(String permitRemarks) {
        this.permitRemarks = permitRemarks;
    }

    public String getPermitType() {
        return permitType;
    }

    public void setPermitType(String permitType) {
        this.permitType = permitType;
    }

    public String getPermitid() {
        return permitid;
    }

    public void setPermitid(String permitid) {
        this.permitid = permitid;
    }

    public String getAgreementYear() {
        return agreementYear;
    }

    public void setAgreementYear(String agreementYear) {
        this.agreementYear = agreementYear;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getAsset() {
        return asset;
    }

    public void setAsset(String asset) {
        this.asset = asset;
    }

    public String getFromDate() {
        return fromDate;
    }

    public String getLeasingCustId() {
        return leasingCustId;
    }

    public void setLeasingCustId(String leasingCustId) {
        this.leasingCustId = leasingCustId;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getBankerAddress() {
        return bankerAddress;
    }

    public void setBankerAddress(String bankerAddress) {
        this.bankerAddress = bankerAddress;
    }

    public String getBankerName() {
        return bankerName;
    }

    public void setBankerName(String bankerName) {
        this.bankerName = bankerName;
    }

    public String getEmiAmount() {
        return emiAmount;
    }

    public void setEmiAmount(String emiAmount) {
        this.emiAmount = emiAmount;
    }

    public String getEmiEndDate() {
        return emiEndDate;
    }

    public void setEmiEndDate(String emiEndDate) {
        this.emiEndDate = emiEndDate;
    }

    public String getEmiMonths() {
        return emiMonths;
    }

    public void setEmiMonths(String emiMonths) {
        this.emiMonths = emiMonths;
    }

    public String getEmiPayDay() {
        return emiPayDay;
    }

    public void setEmiPayDay(String emiPayDay) {
        this.emiPayDay = emiPayDay;
    }

    public String getEmiStartDate() {
        return emiStartDate;
    }

    public void setEmiStartDate(String emiStartDate) {
        this.emiStartDate = emiStartDate;
    }

    public String getFinanceAmount() {
        return financeAmount;
    }

    public void setFinanceAmount(String financeAmount) {
        this.financeAmount = financeAmount;
    }

    public String getInterestType() {
        return interestType;
    }

    public void setInterestType(String interestType) {
        this.interestType = interestType;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getRoi() {
        return roi;
    }

    public void setRoi(String roi) {
        this.roi = roi;
    }

    public String getAmcAmount() {
        return amcAmount;
    }

    public void setAmcAmount(String amcAmount) {
        this.amcAmount = amcAmount;
    }

    public String getAmcChequeDate() {
        return amcChequeDate;
    }

    public void setAmcChequeDate(String amcChequeDate) {
        this.amcChequeDate = amcChequeDate;
    }

    public String getAmcChequeNo() {
        return amcChequeNo;
    }

    public void setAmcChequeNo(String amcChequeNo) {
        this.amcChequeNo = amcChequeNo;
    }

    public String getAmcCompanyName() {
        return amcCompanyName;
    }

    public void setAmcCompanyName(String amcCompanyName) {
        this.amcCompanyName = amcCompanyName;
    }

    public String getAmcDuration() {
        return amcDuration;
    }

    public void setAmcDuration(String amcDuration) {
        this.amcDuration = amcDuration;
    }

    public String getAmcFromDate() {
        return amcFromDate;
    }

    public void setAmcFromDate(String amcFromDate) {
        this.amcFromDate = amcFromDate;
    }

    public String getAmcRemarks() {
        return amcRemarks;
    }

    public void setAmcRemarks(String amcRemarks) {
        this.amcRemarks = amcRemarks;
    }

    public String getAmcToDate() {
        return amcToDate;
    }

    public void setAmcToDate(String amcToDate) {
        this.amcToDate = amcToDate;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getVehicleUsage() {
        return vehicleUsage;
    }

    public void setVehicleUsage(String vehicleUsage) {
        this.vehicleUsage = vehicleUsage;
    }

    public String getNextRoadTaxDate() {
        return nextRoadTaxDate;
    }

    public void setNextRoadTaxDate(String nextRoadTaxDate) {
        this.nextRoadTaxDate = nextRoadTaxDate;
    }

    public String getRoadTaxFromDate() {
        return roadTaxFromDate;
    }

    public void setRoadTaxFromDate(String roadTaxFromDate) {
        this.roadTaxFromDate = roadTaxFromDate;
    }

    public String getAmcId() {
        return amcId;
    }

    public void setAmcId(String amcId) {
        this.amcId = amcId;
    }

    public String getGpsSystem() {
        return gpsSystem;
    }

    public void setGpsSystem(String gpsSystem) {
        this.gpsSystem = gpsSystem;
    }

    public String getWarrantyDate() {
        return warrantyDate;
    }

    public void setWarrantyDate(String warrantyDate) {
        this.warrantyDate = warrantyDate;
    }

    public String getReceiptNo() {
        return receiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }

    public String getRoadTaxAmount() {
        return roadTaxAmount;
    }

    public void setRoadTaxAmount(String roadTaxAmount) {
        this.roadTaxAmount = roadTaxAmount;
    }

    public String getRoadTaxToDate() {
        return roadTaxToDate;
    }

    public void setRoadTaxToDate(String roadTaxToDate) {
        this.roadTaxToDate = roadTaxToDate;
    }

    public String[] getTyreDate() {
        return tyreDate;
    }

    public void setTyreDate(String[] tyreDate) {
        this.tyreDate = tyreDate;
    }

    public String getMarketVehiclePercentage() {
        return marketVehiclePercentage;
    }

    public void setMarketVehiclePercentage(String marketVehiclePercentage) {
        this.marketVehiclePercentage = marketVehiclePercentage;
    }

    public String getToCity() {
        return toCity;
    }

    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    public String getSla() {
        return sla;
    }

    public void setSla(String sla) {
        this.sla = sla;
    }

    public String getFromLocationId() {
        return fromLocationId;
    }

    public void setFromLocationId(String fromLocationId) {
        this.fromLocationId = fromLocationId;
    }

    public String getToLocationId() {
        return toLocationId;
    }

    public void setToLocationId(String toLocationId) {
        this.toLocationId = toLocationId;
    }

    public String getToLocationid() {
        return ToLocationid;
    }

    public void setToLocationid(String ToLocationid) {
        this.ToLocationid = ToLocationid;
    }

    public String getTonnageRateMarket() {
        return tonnageRateMarket;
    }

    public void setTonnageRateMarket(String tonnageRateMarket) {
        this.tonnageRateMarket = tonnageRateMarket;
    }

    //Add Route End
    public String getAxles() {
        return axles;
    }

    public void setAxles(String axles) {
        this.axles = axles;
    }

    public String getDailyRunKm() {
        return dailyRunKm;
    }

    public void setDailyRunKm(String dailyRunKm) {
        this.dailyRunKm = dailyRunKm;
    }

    public String getDailyRunHm() {
        return dailyRunHm;
    }

    public void setDailyRunHm(String dailyRunHm) {
        this.dailyRunHm = dailyRunHm;
    }

    String getGpsSystemId() {
        return gpsSystemId;
    }

    public void setGpsSystemId(String gpsSystemId) {
        this.gpsSystemId = gpsSystemId;
    }

    public String getOwnership() {
        return ownership;
    }

    public void setOwnership(String ownership) {
        this.ownership = ownership;
    }

    public String getTrailerId() {
        return trailerId;
    }

    public void setTrailerId(String trailerId) {
        this.trailerId = trailerId;
    }

    public String getTrailerNo() {
        return trailerNo;
    }

    public void setTrailerNo(String trailerNo) {
        this.trailerNo = trailerNo;
    }

    public String getcType() {
        return cType;
    }

    public void setcType(String cType) {
        this.cType = cType;
    }

    public String geteRegNo() {
        return eRegNo;
    }

    public void seteRegNo(String eRegNo) {
        this.eRegNo = eRegNo;
    }

    public String getrDate() {
        return rDate;
    }

    public void setrDate(String rDate) {
        this.rDate = rDate;
    }

    public String getrRegNo() {
        return rRegNo;
    }

    public void setrRegNo(String rRegNo) {
        this.rRegNo = rRegNo;
    }

    public String getvName() {
        return vName;
    }

    public void setvName(String vName) {
        this.vName = vName;
    }

    public String getOwnerShips() {
        return ownerShips;
    }

    public void setOwnerShips(String ownerShips) {
        this.ownerShips = ownerShips;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getTonnage() {
        return tonnage;
    }

    public void setTonnage(String tonnage) {
        this.tonnage = tonnage;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String[] getTonnages() {
        return tonnages;
    }

    public void setTonnages(String[] tonnages) {
        this.tonnages = tonnages;
    }

    public String[] getCapacities() {
        return capacities;
    }

    public void setCapacities(String[] capacities) {
        this.capacities = capacities;
    }

    public String getAxleDetailId() {
        return axleDetailId;
    }

    public void setAxleDetailId(String axleDetailId) {
        this.axleDetailId = axleDetailId;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getPositionNo() {
        return positionNo;
    }

    public void setPositionNo(String positionNo) {
        this.positionNo = positionNo;
    }

    public String getUpdateValue() {
        return updateValue;
    }

    public void setUpdateValue(String updateValue) {
        this.updateValue = updateValue;
    }

    public String getUpdateType() {
        return updateType;
    }

    public void setUpdateType(String updateType) {
        this.updateType = updateType;
    }

    public String getAxleTypeId() {
        return axleTypeId;
    }

    public void setAxleTypeId(String axleTypeId) {
        this.axleTypeId = axleTypeId;
    }

    public String getVehicleAmt() {
        return vehicleAmt;
    }

    public void setVehicleAmt(String vehicleAmt) {
        this.vehicleAmt = vehicleAmt;
    }

    public String getMonthlyAmount() {
        return monthlyAmount;
    }

    public void setMonthlyAmount(String monthlyAmount) {
        this.monthlyAmount = monthlyAmount;
    }

    public String getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(String balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getEmiPayer() {
        return emiPayer;
    }

    public void setEmiPayer(String emiPayer) {
        this.emiPayer = emiPayer;
    }

    public String getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(String amountPaid) {
        this.amountPaid = amountPaid;
    }

    public String getPaymentMadeOn() {
        return paymentMadeOn;
    }

    public void setPaymentMadeOn(String paymentMadeOn) {
        this.paymentMadeOn = paymentMadeOn;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getEmiAmtPaid() {
        return emiAmtPaid;
    }

    public void setEmiAmtPaid(String emiAmtPaid) {
        this.emiAmtPaid = emiAmtPaid;
    }

    public String getBalanceEmi() {
        return balanceEmi;
    }

    public void setBalanceEmi(String balanceEmi) {
        this.balanceEmi = balanceEmi;
    }

    public String getLastEmiBalance() {
        return lastEmiBalance;
    }

    public void setLastEmiBalance(String lastEmiBalance) {
        this.lastEmiBalance = lastEmiBalance;
    }

    public String getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(String totalBalance) {
        this.totalBalance = totalBalance;
    }

    public String getRoadTaxId() {
        return roadTaxId;
    }

    public void setRoadTaxId(String roadTaxId) {
        this.roadTaxId = roadTaxId;
    }

    public String getInsPolicy() {
        return insPolicy;
    }

    public void setInsPolicy(String insPolicy) {
        this.insPolicy = insPolicy;
    }

    public String getPrevPolicy() {
        return prevPolicy;
    }

    public void setPrevPolicy(String prevPolicy) {
        this.prevPolicy = prevPolicy;
    }

    public String getPackageType() {
        return packageType;
    }

    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }

    public String getInsName() {
        return insName;
    }

    public void setInsName(String insName) {
        this.insName = insName;
    }

    public String getInsAddress() {
        return insAddress;
    }

    public void setInsAddress(String insAddress) {
        this.insAddress = insAddress;
    }

    public String getInsMobileNo() {
        return insMobileNo;
    }

    public void setInsMobileNo(String insMobileNo) {
        this.insMobileNo = insMobileNo;
    }

    public String getInsPolicyNo() {
        return insPolicyNo;
    }

    public void setInsPolicyNo(String insPolicyNo) {
        this.insPolicyNo = insPolicyNo;
    }

    public String getInsIdvValue() {
        return insIdvValue;
    }

    public void setInsIdvValue(String insIdvValue) {
        this.insIdvValue = insIdvValue;
    }

    public String getFromDate1() {
        return fromDate1;
    }

    public void setFromDate1(String fromDate1) {
        this.fromDate1 = fromDate1;
    }

    public String getToDate1() {
        return toDate1;
    }

    public void setToDate1(String toDate1) {
        this.toDate1 = toDate1;
    }

    public String getPremiunAmount() {
        return premiunAmount;
    }

    public void setPremiunAmount(String premiunAmount) {
        this.premiunAmount = premiunAmount;
    }

    public String getVehicleYear() {
        return vehicleYear;
    }

    public void setVehicleYear(String vehicleYear) {
        this.vehicleYear = vehicleYear;
    }

    public String getDepreciation() {
        return depreciation;
    }

    public void setDepreciation(String depreciation) {
        this.depreciation = depreciation;
    }

    public String getYearCost() {
        return yearCost;
    }

    public void setYearCost(String yearCost) {
        this.yearCost = yearCost;
    }

    public String getPerMonth() {
        return perMonth;
    }

    public void setPerMonth(String perMonth) {
        this.perMonth = perMonth;
    }

    public String getDepthVal() {
        return depthVal;
    }

    public void setDepthVal(String depthVal) {
        this.depthVal = depthVal;
    }

    public String getTyreNo() {
        return tyreNo;
    }

    public void setTyreNo(String tyreNo) {
        this.tyreNo = tyreNo;
    }

    public String getVehicleMfr() {
        return vehicleMfr;
    }

    public void setVehicleMfr(String vehicleMfr) {
        this.vehicleMfr = vehicleMfr;
    }

    public String getTrailerMfr() {
        return trailerMfr;
    }

    public void setTrailerMfr(String trailerMfr) {
        this.trailerMfr = trailerMfr;
    }

    public String getBatteryMfr() {
        return batteryMfr;
    }

    public void setBatteryMfr(String batteryMfr) {
        this.batteryMfr = batteryMfr;
    }

    public String getTyreMfr() {
        return tyreMfr;
    }

    public void setTyreMfr(String tyreMfr) {
        this.tyreMfr = tyreMfr;
    }

    public String[] getVehicleMfrs() {
        return vehicleMfrs;
    }

    public void setVehicleMfrs(String[] vehicleMfrs) {
        this.vehicleMfrs = vehicleMfrs;
    }

    public String[] getTrailerMfrs() {
        return trailerMfrs;
    }

    public void setTrailerMfrs(String[] trailerMfrs) {
        this.trailerMfrs = trailerMfrs;
    }

    public String[] getBatteryMfrs() {
        return batteryMfrs;
    }

    public void setBatteryMfrs(String[] batteryMfrs) {
        this.batteryMfrs = batteryMfrs;
    }

    public String[] getTyreMfrs() {
        return tyreMfrs;
    }

    public void setTyreMfrs(String[] tyreMfrs) {
        this.tyreMfrs = tyreMfrs;
    }

    public String getVesselName() {
        return vesselName;
    }

    public void setVesselName(String vesselName) {
        this.vesselName = vesselName;
    }

    public String getVesselCode() {
        return vesselCode;
    }

    public void setVesselCode(String vesselCode) {
        this.vesselCode = vesselCode;
    }

    public String getVesselType() {
        return vesselType;
    }

    public void setVesselType(String vesselType) {
        this.vesselType = vesselType;
    }

    public String getLinerCode() {
        return linerCode;
    }

    public void setLinerCode(String linerCode) {
        this.linerCode = linerCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVoyageNo() {
        return voyageNo;
    }

    public void setVoyageNo(String voyageNo) {
        this.voyageNo = voyageNo;
    }

    public String getLinersName() {
        return linersName;
    }

    public void setLinersName(String linersName) {
        this.linersName = linersName;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getPlannedArrivalDate() {
        return plannedArrivalDate;
    }

    public void setPlannedArrivalDate(String plannedArrivalDate) {
        this.plannedArrivalDate = plannedArrivalDate;
    }

    public String getPlannedArrivalPort() {
        return plannedArrivalPort;
    }

    public void setPlannedArrivalPort(String plannedArrivalPort) {
        this.plannedArrivalPort = plannedArrivalPort;
    }

    public String getPlannedDischargeDate() {
        return plannedDischargeDate;
    }

    public void setPlannedDischargeDate(String plannedDischargeDate) {
        this.plannedDischargeDate = plannedDischargeDate;
    }

    public String getDischargeVesselName() {
        return dischargeVesselName;
    }

    public void setDischargeVesselName(String dischargeVesselName) {
        this.dischargeVesselName = dischargeVesselName;
    }

    public String getVesselId() {
        return vesselId;
    }

    public void setVesselId(String vesselId) {
        this.vesselId = vesselId;
    }

    public String getPlannedDischargePort() {
        return plannedDischargePort;
    }

    public void setPlannedDischargePort(String plannedDischargePort) {
        this.plannedDischargePort = plannedDischargePort;
    }

    public String getClearanceIdPermit() {
        return clearanceIdPermit;
    }

    public void setClearanceIdPermit(String clearanceIdPermit) {
        this.clearanceIdPermit = clearanceIdPermit;
    }

    public String getPaymentTypePermit() {
        return paymentTypePermit;
    }

    public void setPaymentTypePermit(String paymentTypePermit) {
        this.paymentTypePermit = paymentTypePermit;
    }

    public String getBankIdPermit() {
        return bankIdPermit;
    }

    public void setBankIdPermit(String bankIdPermit) {
        this.bankIdPermit = bankIdPermit;
    }

    public String getBankBranchIdPermit() {
        return bankBranchIdPermit;
    }

    public void setBankBranchIdPermit(String bankBranchIdPermit) {
        this.bankBranchIdPermit = bankBranchIdPermit;
    }

    public String getChequeDatePermit() {
        return chequeDatePermit;
    }

    public void setChequeDatePermit(String chequeDatePermit) {
        this.chequeDatePermit = chequeDatePermit;
    }

    public String getChequeNoPermit() {
        return chequeNoPermit;
    }

    public void setChequeNoPermit(String chequeNoPermit) {
        this.chequeNoPermit = chequeNoPermit;
    }

    public String getChequeAmountPermit() {
        return chequeAmountPermit;
    }

    public void setChequeAmountPermit(String chequeAmountPermit) {
        this.chequeAmountPermit = chequeAmountPermit;
    }

    public String getClearanceDatePermit() {
        return clearanceDatePermit;
    }

    public void setClearanceDatePermit(String clearanceDatePermit) {
        this.clearanceDatePermit = clearanceDatePermit;
    }

    public String getClearanceIdFC() {
        return clearanceIdFC;
    }

    public void setClearanceIdFC(String clearanceIdFC) {
        this.clearanceIdFC = clearanceIdFC;
    }

    public String getPaymentTypeFC() {
        return paymentTypeFC;
    }

    public void setPaymentTypeFC(String paymentTypeFC) {
        this.paymentTypeFC = paymentTypeFC;
    }

    public String getBankIdFC() {
        return bankIdFC;
    }

    public void setBankIdFC(String bankIdFC) {
        this.bankIdFC = bankIdFC;
    }

    public String getBankBranchIdFC() {
        return bankBranchIdFC;
    }

    public void setBankBranchIdFC(String bankBranchIdFC) {
        this.bankBranchIdFC = bankBranchIdFC;
    }

    public String getCreditLedgerIdFC() {
        return creditLedgerIdFC;
    }

    public void setCreditLedgerIdFC(String creditLedgerIdFC) {
        this.creditLedgerIdFC = creditLedgerIdFC;
    }

    public String getChequeDateFC() {
        return chequeDateFC;
    }

    public void setChequeDateFC(String chequeDateFC) {
        this.chequeDateFC = chequeDateFC;
    }

    public String getChequeNoFC() {
        return chequeNoFC;
    }

    public void setChequeNoFC(String chequeNoFC) {
        this.chequeNoFC = chequeNoFC;
    }

    public String getChequeAmountFC() {
        return chequeAmountFC;
    }

    public void setChequeAmountFC(String chequeAmountFC) {
        this.chequeAmountFC = chequeAmountFC;
    }

    public String getClearanceDateFC() {
        return clearanceDateFC;
    }

    public void setClearanceDateFC(String clearanceDateFC) {
        this.clearanceDateFC = clearanceDateFC;
    }

    public String getPaymentDateRT() {
        return paymentDateRT;
    }

    public void setPaymentDateRT(String paymentDateRT) {
        this.paymentDateRT = paymentDateRT;
    }

    public String getClearanceIdRT() {
        return clearanceIdRT;
    }

    public void setClearanceIdRT(String clearanceIdRT) {
        this.clearanceIdRT = clearanceIdRT;
    }

    public String getPaymentTypeRT() {
        return paymentTypeRT;
    }

    public void setPaymentTypeRT(String paymentTypeRT) {
        this.paymentTypeRT = paymentTypeRT;
    }

    public String getBankIdRT() {
        return bankIdRT;
    }

    public void setBankIdRT(String bankIdRT) {
        this.bankIdRT = bankIdRT;
    }

    public String getBankBranchIdRT() {
        return bankBranchIdRT;
    }

    public void setBankBranchIdRT(String bankBranchIdRT) {
        this.bankBranchIdRT = bankBranchIdRT;
    }

    public String getChequeDateRT() {
        return chequeDateRT;
    }

    public void setChequeDateRT(String chequeDateRT) {
        this.chequeDateRT = chequeDateRT;
    }

    public String getChequeNoRT() {
        return chequeNoRT;
    }

    public void setChequeNoRT(String chequeNoRT) {
        this.chequeNoRT = chequeNoRT;
    }

    public String getChequeAmountRT() {
        return chequeAmountRT;
    }

    public void setChequeAmountRT(String chequeAmountRT) {
        this.chequeAmountRT = chequeAmountRT;
    }

    public String getClearanceDateRT() {
        return clearanceDateRT;
    }

    public void setClearanceDateRT(String clearanceDateRT) {
        this.clearanceDateRT = clearanceDateRT;
    }

    public String getVendorId1() {
        return vendorId1;
    }

    public void setVendorId1(String vendorId1) {
        this.vendorId1 = vendorId1;
    }

    public String getCashOnHandLedgerId() {
        return cashOnHandLedgerId;
    }

    public void setCashOnHandLedgerId(String cashOnHandLedgerId) {
        this.cashOnHandLedgerId = cashOnHandLedgerId;
    }

    public String getCashOnHandLedgerCode() {
        return cashOnHandLedgerCode;
    }

    public void setCashOnHandLedgerCode(String cashOnHandLedgerCode) {
        this.cashOnHandLedgerCode = cashOnHandLedgerCode;
    }

    public String getLedgerCode() {
        return ledgerCode;
    }

    public void setLedgerCode(String ledgerCode) {
        this.ledgerCode = ledgerCode;
    }

    public String getCreditLedgerId() {
        return creditLedgerId;
    }

    public void setCreditLedgerId(String creditLedgerId) {
        this.creditLedgerId = creditLedgerId;
    }

    public String getClearanceId() {
        return clearanceId;
    }

    public void setClearanceId(String clearanceId) {
        this.clearanceId = clearanceId;
    }

    public String getInsuranceId() {
        return insuranceId;
    }

    public void setInsuranceId(String insuranceId) {
        this.insuranceId = insuranceId;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getBankBranchId() {
        return bankBranchId;
    }

    public void setBankBranchId(String bankBranchId) {
        this.bankBranchId = bankBranchId;
    }

    public String getChequeDate() {
        return chequeDate;
    }

    public void setChequeDate(String chequeDate) {
        this.chequeDate = chequeDate;
    }

    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    public String getChequeAmount() {
        return chequeAmount;
    }

    public void setChequeAmount(String chequeAmount) {
        this.chequeAmount = chequeAmount;
    }

    public String getClearanceDate() {
        return clearanceDate;
    }

    public void setClearanceDate(String clearanceDate) {
        this.clearanceDate = clearanceDate;
    }

    public String getLedgerId() {
        return ledgerId;
    }

    public void setLedgerId(String ledgerId) {
        this.ledgerId = ledgerId;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getPaymentDatePermit() {
        return paymentDatePermit;
    }

    public void setPaymentDatePermit(String paymentDatePermit) {
        this.paymentDatePermit = paymentDatePermit;
    }

    public String getPaymentDateFC() {
        return paymentDateFC;
    }

    public void setPaymentDateFC(String paymentDateFC) {
        this.paymentDateFC = paymentDateFC;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getFleetTypeId() {
        return fleetTypeId;
    }

    public void setFleetTypeId(String fleetTypeId) {
        this.fleetTypeId = fleetTypeId;
    }

    

}
