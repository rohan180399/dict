<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<script language="javascript" src="/throttle/js/validate.js"></script>  
<%@ page import="ets.domain.contract.business.ContractTO" %> 
<%@ page import="ets.domain.service.business.ServiceTO" %>   
</head>
<script>
       var count=0;
       function submitPage(value){
            selectedItemValidation();
            document.alterMaintenance.action = '/throttle/updateJobCardServiceCheckList.do';
            document.alterMaintenance.submit();

        }

    function selectedItemValidation(){

    var checked = document.getElementsByName("checked");
    var checkedStatus = document.getElementsByName("checkedStatus");
    var serviced = document.getElementsByName("serviced");
    var servicedStatus = document.getElementsByName("servicedStatus");

    var serviceNames = document.getElementsByName("serviceNames");    
    var desc = document.getElementsByName("desc");
    var chec=0;
    var mess = "SubmitForm";
    //alert(checked.length);
    for(var i=0;(i<checked.length && checked.length!=0);i++){
        //alert(i);
        if(checked[i].checked){
            checkedStatus[i].value = 'Y';
        }else {
            checkedStatus[i].value = 'N';
        }
        //alert("am here checkedstatus set");
        if(serviced[i].checked){
            servicedStatus[i].value = 'Y';
        }else {
            servicedStatus[i].value = 'N';
        }
        //alert("am here servicedStatus set");
    }
    
}
    
  </script>
<div class="pageheader">
      <h2><i class="fa fa-edit"></i> JobCard Service Checklist</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="index.html">Home</a></li>
          <li><a href="general-forms.html">Service</a></li>
          <li class="">JobCard</li>
          <li class="active">Service Checklist</li>
        </ol>
      </div>
      </div>
 <div class="contentpanel">
<div class="panel panel-default">
 <div class="panel-body">
<body >
  
<form name="alterMaintenance" method="post">
  


<!-- pointer table -->

<!-- message table -->

<%@ include file="/content/common/message.jsp" %>

<table class="table table-info mb30 table-hover" id="table">
    <thead>
<tr>
<th width="45" height="30" align="left" ><div >SNo</div></th>
<th width="172" height="30" align="left" ><div >CheckListDescription</div></th>
<th width="158" height="30" align="left" ><div >checked</div></th>
<th width="76" height="30" align="left" ><div >serviced</div></th>
<th width="76" height="30" align="left" ><div >remarks</div></th>
</tr>
</thead>
<% 
int index1=0; 
%>
<input type="hidden"  name="jobCardId"  value='<c:out value="${jobCardId}"/>'>
<input type="hidden"  name="serviceId"  value='<c:out value="${serviceId}"/>'>
<c:if test = "${checkList != null}" >
      <c:forEach items="${checkList}" var="service">
<%
    String classText = "";
    int oddEven = index1 % 2;
    if (oddEven > 0) {
    classText = "text2";
    } else {
    classText = "text1";
    }
    %>
<tr>
<td  height="30" ><%=index1+1%></td>
<input type="hidden"  name="checkListIds"  value='<c:out value="${service.checkListId}"/>'>
<td  height="30"><c:out value="${service.checkListPoint}"/></td>
<td  height="30">
    <c:if test = "${service.checked == 'Y'}" >
        <input type="checkbox" name="checked" checked  value='<%= index1 %>'>
    </c:if>
    <c:if test = "${service.checked == 'N'}" >
        <input type="checkbox" name="checked" value='<%= index1 %>'>
    </c:if>

    <input type="hidden" name="checkedStatus"  value='<c:out value="${service.checked}"/>' >
    <input type="hidden" name="servicedStatus"  value='<c:out value="${service.serviced}"/>' >

</td>
<td  height="30">
    <c:if test = "${service.serviced == 'Y'}" >
        <input type="checkbox" name="serviced" checked  value='<%= index1 %>'>
    </c:if>
    <c:if test = "${service.serviced == 'N'}" >
        <input type="checkbox" name="serviced" value='<%= index1 %>'
    </c:if>
    
</td>
<td  height="30" ><textarea rows="2" name="remarks" cols="30"><c:out value="${service.remarks}"/></textarea></td>

        
</tr>
    <%
    index1++;
    %>
    </c:forEach>
</c:if>
</table>
<br>
<center>
 <input type="button" class="button" value="Save" name="Save" onClick="submitPage(this.name)">   
</center>
<br>
    <br>
        <br>

<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>

</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
