// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3)
// Source File Name:   MrsDAO.java
package ets.domain.mrs.data;

import ets.arch.exception.FPRuntimeException;
import ets.domain.mrs.business.MrsTO;
import ets.domain.util.FPLogUtils;
import java.io.PrintStream;
import java.util.*;
import org.springframework.orm.ibatis.SqlMapClientTemplate;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class MrsDAO extends SqlMapClientDaoSupport {

    public MrsDAO() {
    }

    public ArrayList getApprovalList(MrsTO mrsTO, String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList mrsList = new ArrayList();
        try {
            map.put("companyId", Integer.valueOf(mrsTO.getCompanyId()));
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            System.out.println("map = " + map);
            mrsList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getUnApproval", map);
            System.out.println((new StringBuilder()).append("mrsUnApprovedList size=").append(mrsList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "UnApproved List", sqlException);
        }
        return mrsList;
    }

    public ArrayList getVehicleDetails(int mrsId) {
        Map map = new HashMap();
        ArrayList vehicleDetails = new ArrayList();
        try {
            //map.put("mrsId", Integer.valueOf(mrsId));
            map.put("mrsId", mrsId);
            vehicleDetails = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getVehicleDetails", map);
            System.out.println((new StringBuilder()).append("vehicleDetails size=").append(vehicleDetails.size()).toString());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-VEH-01", "MrsDAO", "vehicleDetails", sqlException);
        }
        return vehicleDetails;
    }

    public ArrayList getMrsDetails(int mrsId) {
        Map map = new HashMap();
        String positionId = "";
        ArrayList mrsDetails = new ArrayList();
        try {
            map.put("mrsId", Integer.valueOf(mrsId));
            System.out.println("map" + map);
            positionId = (String) getSqlMapClientTemplate().queryForObject("mrs.checkPositionId", map);
            System.out.println("positionId" + positionId);
            if ("0".equals(positionId)) {
                System.out.println("iff");
                map.put("mrsId", Integer.valueOf(mrsId));
                mrsDetails = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getMrsDetail", map);
            } else {
                System.out.println("else");
                map.put("mrsId", Integer.valueOf(mrsId));
                mrsDetails = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getMrsDetails", map);
                System.out.println((new StringBuilder()).append("mrsDetails size=").append(mrsDetails.size()).toString());
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "mrsDetails", sqlException);
        }
        return mrsDetails;
    }

    public int doAddMrsApprovedQuantity(int userId, MrsTO mrsTO, ArrayList List) {
        Map map = new HashMap();
        int status = 0;
        int counter = 0;
        try {
            //////System.out.println("in doAddMrsApprovedQuantity");
            map.put("userId", Integer.valueOf(userId));
            Iterator itr = List.iterator();
            MrsTO listTO = null;
            for (; itr.hasNext(); System.out.println((new StringBuilder()).append("status=").append(status).toString())) {
                listTO = new MrsTO();
                listTO = (MrsTO) itr.next();
                map.put("Issue", Float.valueOf(Float.parseFloat(listTO.getMrsIssueQuantity())));
                map.put("MrsId", Integer.valueOf(Integer.parseInt(listTO.getMrsNumber())));
                map.put("RequestedQty", Float.valueOf(Float.parseFloat(listTO.getMrsRequestedItemNumber())));
                map.put("ItemId", Integer.valueOf(Integer.parseInt(listTO.getMrsItemId())));
                map.put("Remarks", listTO.getMrsApprovalRemarks());
                map.put("Status", listTO.getMrsApprovalStatus());
                map.put("positionId", listTO.getPositionId());
                if (counter == 0) {
                    status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.InsertMrsStatus", map)).intValue();
                    counter++;
                }
                status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.insertMrsApprovedQuantity", map)).intValue();
            }

        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "Add Mrs", sqlException);
        }
        return status;
    }

    public String getLocalCenterRcItemsStock(int Item_id, int companyId) {
        Map map = new HashMap();
        int qty = 0;
        int status = 0;
        String quantity;
        try {
            map.put("itemId", Integer.valueOf(Item_id));
            map.put("companyId", Integer.valueOf(companyId));
            System.out.println((new StringBuilder()).append("item_id=").append(Item_id).toString());
            System.out.println((new StringBuilder()).append("companyId=").append(companyId).toString());
            quantity = (String) getSqlMapClientTemplate().queryForObject("mrs.getRcItems", map);
            if (quantity == null) {
                quantity = "";
            }
            System.out.println((new StringBuilder()).append("qty=").append(qty).toString());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "Rc items Mrs", sqlException);
        }
        return quantity;
    }

    public String getBestBuy(int Item_id) {
        Map map = new HashMap();
        int qty = 0;
        int status = 0;
        String bestBuy = "";
        try {
            map.put("itemId", Item_id);
            System.out.println((new StringBuilder()).append("item_id=").append(Item_id).toString());
            bestBuy = (String) getSqlMapClientTemplate().queryForObject("mrs.getBestBuy", map);
            if (bestBuy == null) {
                bestBuy = "NA";
            }
            //////System.out.println("bestBuy:"+bestBuy);
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("bestBuy Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "bestBuy", sqlException);
        }
        return bestBuy;
    }

    public String getOtherMrsRequest(int mrsId, int Item_id) {
        Map map = new HashMap();
        String qty = "";
        try {
            map.put("mrsId", Integer.valueOf(mrsId));
            map.put("itemId", Integer.valueOf(Item_id));
            System.out.println((new StringBuilder()).append("mrsId=").append(mrsId).toString());
            System.out.println((new StringBuilder()).append("Item_id=").append(Item_id).toString());
            qty = (String) getSqlMapClientTemplate().queryForObject("mrs.getRequests", map);
            System.out.println((new StringBuilder()).append("qty=").append(qty).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "Other Mrs Request", sqlException);
        }
        return qty;
    }

    public String getOtherServicePointQuantity(int item_id, int companyId) {
        Map map = new HashMap();
        String mrsOtherServicePointQuantity = "";
        try {
            map.put("itemId", Integer.valueOf(item_id));
            map.put("companyId", Integer.valueOf(companyId));
            mrsOtherServicePointQuantity = (String) getSqlMapClientTemplate().queryForObject("mrs.OtherServicePointQuantitys", map);
            if (mrsOtherServicePointQuantity == null) {
                mrsOtherServicePointQuantity = "";
            }
            System.out.println((new StringBuilder()).append("mrsOtherServicePointQuantity =").append(mrsOtherServicePointQuantity).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "OtherService Point Item Sum", sqlException);
        }
        return mrsOtherServicePointQuantity;
    }

    public String getLocalServicePointQuantity(int item_id, int companyId) {
        Map map = new HashMap();
        String mrsLocalServicePointQuantity = "";
        try {
            map.put("itemId", Integer.valueOf(item_id));
            map.put("companyId", Integer.valueOf(companyId));
            mrsLocalServicePointQuantity = (String) getSqlMapClientTemplate().queryForObject("mrs.localServicePointQuantitys", map);
            System.out.println((new StringBuilder()).append("mrsLocalServicePointQuantity =").append(mrsLocalServicePointQuantity).toString());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getLocalServicePointQuantity Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "getLocalServicePointQuantity", sqlException);
        }
        return mrsLocalServicePointQuantity;
    }

    public ArrayList getNewStockDetails(int ItemId, int companyId) {
        Map map = new HashMap();
        ArrayList newStockDetails = new ArrayList();
        try {
            map.put("itemId", Integer.valueOf(ItemId));
            map.put("companyId", Integer.valueOf(companyId));
            newStockDetails = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.newStockDetails", map);
            System.out.println((new StringBuilder()).append("newStockDetails size=").append(newStockDetails.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "v", sqlException);
        }
        return newStockDetails;
    }

    public ArrayList getRcStockDetails(int ItemId, int companyId) {
        Map map = new HashMap();
        ArrayList rcStockDetails = new ArrayList();
        try {
            map.put("itemId", Integer.valueOf(ItemId));
            map.put("companyId", Integer.valueOf(companyId));
            rcStockDetails = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.rcStockDetails", map);
            System.out.println((new StringBuilder()).append("RcStockDetails size=").append(rcStockDetails.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "RcStockDetails", sqlException);
        }
        return rcStockDetails;
    }

    public ArrayList getMrsList(int companyId, String fromDate, String toDate) {
        Map map = new HashMap();
        map.put("companyId", Integer.valueOf(companyId));
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        ArrayList mrsList = new ArrayList();
        System.out.println((new StringBuilder()).append("companyId=").append(companyId).toString());
        try {
            mrsList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getMrsList", map);
            System.out.println((new StringBuilder()).append("mrsList size=").append(mrsList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "MRSList", sqlException);
        }
        return mrsList;
    }

    public ArrayList processServicePtList() {
        Map map = new HashMap();
        map.put("companyType", 1012);

        ArrayList servicePtList = new ArrayList();

        try {
            servicePtList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getServicePtList", map);
            System.out.println((new StringBuilder()).append("servicePtList size=").append(servicePtList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "servicePtList", sqlException);
        }
        return servicePtList;
    }

    public ArrayList getTotalIssuedQty(int mrsId, int itemId) {
        Map map = new HashMap();
        ArrayList totalIssued = new ArrayList();
        int status = 0;
        map.put("mrsId", Integer.valueOf(mrsId));
        map.put("itemId", Integer.valueOf(itemId));
        try {
            map.put("itemId", Integer.valueOf(itemId));
            totalIssued = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getTotalIssuedQty", map);
            System.out.println((new StringBuilder()).append("totalIssued=").append(totalIssued.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getTotalIssuedQty Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "getTotalIssuedQty", sqlException);
        }
        return totalIssued;
    }

    public ArrayList getNewPriceList(MrsTO mrsTO) {
        Map map = new HashMap();
        ArrayList newPriceList = new ArrayList();
        map.put("itemId", mrsTO.getMrsItemId());
        System.out.println((new StringBuilder()).append("mrsTO.getMrsItemId()").append(mrsTO.getMrsItemId()).toString());
        System.out.println((new StringBuilder()).append("mrsTO.getCompanyId()").append(mrsTO.getCompanyId()).toString());
        map.put("companyId", Integer.valueOf(mrsTO.getCompanyId()));
        try {
            newPriceList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getNewPriceList", map);
            System.out.println((new StringBuilder()).append("newPriceList size=").append(newPriceList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "newPriceList", sqlException);
        }
        return newPriceList;
    }

    public ArrayList getRcPriceList(MrsTO mrsTO) {
        Map map = new HashMap();
        ArrayList rcPriceList = new ArrayList();
        map.put("itemId", mrsTO.getMrsItemId());
        map.put("companyId", Integer.valueOf(mrsTO.getCompanyId()));
        try {
            rcPriceList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getRcPriceList", map);
            System.out.println((new StringBuilder()).append("rcPriceList size=").append(rcPriceList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "rcPriceList", sqlException);
        }
        return rcPriceList;
    }

    public ArrayList getRtTyrePriceList(MrsTO mrsTO) {
        Map map = new HashMap();
        ArrayList rcPriceList = new ArrayList();
        map.put("itemId", mrsTO.getMrsItemId());
        map.put("companyId", Integer.valueOf(mrsTO.getCompanyId()));
        try {
            //////System.out.println("In Dao");
            rcPriceList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getRtTyrePrice", map);
            //////System.out.println("A4 Query");
            System.out.println((new StringBuilder()).append("getRtTyrePriceList size=").append(rcPriceList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getRtTyrePriceList Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "getRtTyrePriceList", sqlException);
        }
        return rcPriceList;
    }

    public ArrayList getVehicleRcItems(MrsTO mrsTO) {
        Map map = new HashMap();
        ArrayList rcPriceList = new ArrayList();
        map.put("itemId", mrsTO.getMrsItemId());
        map.put("vehicleId", Integer.valueOf(mrsTO.getVehicleId()));
        try {
            System.out.println((new StringBuilder()).append("itemId in dao=").append(mrsTO.getMrsItemId()).toString());
            System.out.println((new StringBuilder()).append("vehicleId in dao=").append(mrsTO.getVehicleId()).toString());
            rcPriceList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getVehicleRcItems", map);
            System.out.println((new StringBuilder()).append("rcPriceList size=").append(rcPriceList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getVehicleRcItems Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "getVehicleRcItems", sqlException);
        }
        return rcPriceList;
    }

    public int insertNewItemHistory(int userId, MrsTO mrsTO) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", Integer.valueOf(userId));
        map.put("itemId", mrsTO.getMrsItemId());
        map.put("companyId", Integer.valueOf(mrsTO.getCompanyId()));
        map.put("mrsId", mrsTO.getMrsNumber());
        map.put("actionType", mrsTO.getActionType());
        map.put("priceId", mrsTO.getPriceIds());
        map.put("quantity", Float.valueOf(mrsTO.getQuantityRI()));
        map.put("faultStatus", mrsTO.getFaultStatus());
        //////System.out.println("In DAO userID:"+Integer.valueOf(userId)+"itemId:"+mrsTO.getMrsItemId()+"companyId"+Integer.valueOf(mrsTO.getCompanyId())+"mrsId"+mrsTO.getMrsNumber()+
        //"actionType"+mrsTO.getActionType()+"priceId"+mrsTO.getPriceIds()+"quantity"+Float.valueOf(mrsTO.getQuantityRI())+"faultStatus"+mrsTO.getFaultStatus());
        try {
//            status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.insertNewItemHistory", map)).intValue();
            //////System.out.println("before insert");
            status = ((Integer) getSqlMapClientTemplate().update("mrs.insertNewItemHistory", map));
            //////System.out.println("after insert");
//            System.out.println((new StringBuilder()).append("in insertNewItemHistory history added status").append(status).toString());
            //////System.out.println("status = " + status);
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-04", "MrsDAO", "status", sqlException);
        }
        return status;
    }

    public int insertNewItemMaster(int userId, MrsTO mrsTO) {
        //////System.out.println("/////////////////insertNewItemMaster");
        Map map = new HashMap();
        int status = 0;
        int status1 = 0;
        float totalIssued = 0.0F;
        float priceWithTax = 10.0F;
        String priceWithTx = "";
        ArrayList priceWthTax = new ArrayList();
        float amount = 0.0F;
        float quantity = 0.0F;
        map.put("userId", Integer.valueOf(userId));
        map.put("itemId", mrsTO.getMrsItemId());
        map.put("companyId", Integer.valueOf(mrsTO.getCompanyId()));
        map.put("mrsId", mrsTO.getMrsNumber());
        map.put("priceId", mrsTO.getPriceIds());
        System.out.println((new StringBuilder()).append("mrsTO.getQuantityRI()==").append(mrsTO.getQuantityRI()).toString());
        map.put("quantity", Float.valueOf(mrsTO.getQuantityRI()));
        //////System.out.println("in insertNewItemMaster");
        try {
            status = ((Integer) getSqlMapClientTemplate().queryForObject("mrs.checkItem", map)).intValue();
////hari start
//            //////System.out.println("Begining");
//            //////System.out.println("mrsTO.getPriceId()-->"+mrsTO.getPriceIds());
//            priceWthTax = (ArrayList)getSqlMapClientTemplate().queryForList("mrs.getPriceWithOutTax", map);
//            //////System.out.println("priceWthTax.size()-->"+priceWthTax.size());
//
//            Iterator itrP = priceWthTax.iterator();
//            while(itrP.hasNext())
//            {
//                MrsTO mrs = (MrsTO)itrP.next();
//                //////System.out.println("mrs.getPriceWithTax()"+mrs.getPriceWithTax());
//                priceWithTax = Float.valueOf(mrs.getPriceWithTax());
//                //////System.out.println("Float.valueOf-->"+priceWithTax);
//            }
//            quantity=Float.valueOf(mrsTO.getQuantityRI());
//            amount = Float.valueOf(quantity * priceWithTax).floatValue();
//            //////System.out.println("After Calculate Price for quantity1234-->"+amount);
//
//            map.put("priceWithTax",priceWithTax);
//            map.put("amount",amount);
//            map.put("rcWorkId",Integer.valueOf(mrsTO.getRcWorkId()));
//            map.put("rcItemsId",Integer.valueOf(mrsTO.getRcItemsId()));
//            map.put("mrsItemId",Integer.valueOf(mrsTO.getIssuedItemsId()));
//
//            //////System.out.println("mrsTO.getRcWorkId()"+mrsTO.getRcWorkId());
//            //////System.out.println("mrsTO.getIssuedItemsId()"+mrsTO.getIssuedItemsId());
//            //////System.out.println("mrsTO.getRcItemsId()"+mrsTO.getRcItemsId());
//
//            status1 = ((Integer)getSqlMapClientTemplate().update("mrs.updatePriceForRcItems", map)).intValue();
//            //////System.out.println("Update Price For Rc Items-->"+status1);
//
////hari end

            if (status == 0) {
                //////System.out.println("new price inserted");
                status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.insertNewItemMaster", map)).intValue();
            } else {
                //////System.out.println("updating existing price");
                map.put("totalIssued", mrsTO.getTotalIssued());
                System.out.println((new StringBuilder()).append("quantity R/I").append(mrsTO.getQuantityRI()).toString());
                System.out.println((new StringBuilder()).append("total Issued ").append(mrsTO.getTotalIssued()).toString());
                totalIssued = mrsTO.getQuantityRI();
                System.out.println((new StringBuilder()).append("total issued qty=").append(totalIssued).toString());
                map.put("totalIssued", Float.valueOf(totalIssued));
                //////System.out.println("test");
                //////System.out.println("-------------");
                System.out.println((new StringBuilder()).append("mrsId = ").append(mrsTO.getMrsNumber()).toString());
                System.out.println((new StringBuilder()).append("itemId = ").append(mrsTO.getMrsItemId()).toString());
                System.out.println((new StringBuilder()).append("priceId = ").append(mrsTO.getPriceIds()).toString());
                status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.updateNewItemMaster", map)).intValue();
                status = ((Integer) getSqlMapClientTemplate().queryForObject("mrs.getMrsIssueQty", map)).intValue();
                System.out.println((new StringBuilder()).append("test issued Qty =").append(status).toString());

//                //Hari
//                //////System.out.println("amount>>>"+amount);
//                //////System.out.println("quantity>>>"+Float.valueOf(mrsTO.getQuantityRI()));
//                status1 = Integer.valueOf(getSqlMapClientTemplate().update("mrs.updateRcReturnSpares", map)).intValue();
//                //////System.out.println(">>>>>>>>>>>>>>>>>updateRcReturnSpares"+status1);
//                //Hari End
            }
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-04", "MrsDAO", "status", sqlException);
        }
        return status;
    }

    public int insertRcItemHistory(int userId, MrsTO mrsTO) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", Integer.valueOf(userId));
        map.put("itemId", mrsTO.getMrsItemId());
        map.put("companyId", Integer.valueOf(mrsTO.getCompanyId()));
        map.put("mrsId", mrsTO.getMrsNumber());
        map.put("actionType", mrsTO.getActionType());
        map.put("priceId", mrsTO.getPriceIds());
        map.put("rcItemId", mrsTO.getRcItemIds());
        map.put("quantity", Float.valueOf(mrsTO.getQuantityRI()));
        map.put("faultStatus", mrsTO.getFaultStatus());
        map.put("rcPriceWoId", mrsTO.getRcPriceWoId());
        try {
            status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.insertRcItemHistory", map)).intValue();
            System.out.println((new StringBuilder()).append("insertRcItemHistory ").append(status).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-04", "MrsDAO", "status", sqlException);
        }
        return status;
    }

    public int insertRcItemIssueMaster(int userId, MrsTO mrsTO) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", Integer.valueOf(userId));
        map.put("itemId", mrsTO.getMrsItemId());
        map.put("companyId", Integer.valueOf(mrsTO.getCompanyId()));
        map.put("mrsId", mrsTO.getMrsNumber());
        map.put("priceId", mrsTO.getPriceIds());
        map.put("rcItemId", mrsTO.getRcItemIds());
        map.put("quantity", Float.valueOf(mrsTO.getQuantityRI()));
        try {
            status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.insertRcItemIssueMaster", map)).intValue();
            System.out.println((new StringBuilder()).append("insertRcItemIssueMaster ").append(status).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-04", "MrsDAO", "status", sqlException);
        }
        return status;
    }

    public int insertFaultItem(int userId, MrsTO mrsTO) {
        Map map = new HashMap();
        int status = 0;
        String activeInd = "Y";
        String IsRcReq = "Y";
        map.put("userId", Integer.valueOf(userId));
        map.put("itemId", mrsTO.getMrsItemId());
        map.put("companyId", Integer.valueOf(mrsTO.getCompanyId()));
        map.put("isRcReq", IsRcReq);
        map.put("mrsId", mrsTO.getMrsNumber());
        map.put("priceId", mrsTO.getPriceIds());
        map.put("quantity", Float.valueOf(mrsTO.getQuantityRI()));
        map.put("faultStatus", mrsTO.getFaultStatus());
        map.put("status", activeInd);
        try {
            status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.insertFaultItem", map)).intValue();
            System.out.println((new StringBuilder()).append("insertFaultItem ").append(status).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-04", "MrsDAO", "status", sqlException);
        }
        return status;
    }

    public int updateNewItemMaster(int userId, MrsTO mrsTO) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", Integer.valueOf(userId));
        map.put("itemId", mrsTO.getMrsItemId());
        map.put("companyId", Integer.valueOf(mrsTO.getCompanyId()));
        map.put("mrsId", mrsTO.getMrsNumber());
        map.put("priceId", mrsTO.getPriceIds());
        map.put("quantity", Float.valueOf(mrsTO.getQuantityRI()));
        try {
            status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.updateNewItemMaster", map)).intValue();
            System.out.println((new StringBuilder()).append("updateNewItemMaster ").append(status).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-04", "MrsDAO", "status", sqlException);
        }
        return status;
    }

    public int updateNewItemStock(int userId, MrsTO mrsTO) {
        //////System.out.println("//////////updateNewItemStock");
        Map map = new HashMap();
        int status = 0;
        map.put("userId", Integer.valueOf(userId));
        map.put("itemId", mrsTO.getMrsItemId());
        map.put("companyId", Integer.valueOf(mrsTO.getCompanyId()));
        map.put("priceId", mrsTO.getPriceIds());
        map.put("quantity", Float.valueOf(mrsTO.getQuantityRI()));
        System.out.println((new StringBuilder()).append("quantity  = ").append(mrsTO.getQuantityRI()).toString());
        System.out.println((new StringBuilder()).append("price  = ").append(mrsTO.getPriceIds()).toString());
        System.out.println((new StringBuilder()).append("company  = ").append(mrsTO.getCompanyId()).toString());
        try {
            //////System.out.println("In updateNewItemStock");
            status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.updateNewItemStock", map)).intValue();
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-04", "MrsDAO", "status", sqlException);
        }
        return status;
    }

    public int updateRcItemIssueMaster(int userId, MrsTO mrsTO) {
        Map map = new HashMap();
        int status = 0;
        String activeInd = "";
        if (Integer.parseInt(mrsTO.getActionType()) == 1011) {
            activeInd = "Y";
        } else {
            activeInd = "N";
        }
        map.put("userId", Integer.valueOf(userId));
        map.put("itemId", mrsTO.getMrsItemId());
        map.put("companyId", Integer.valueOf(mrsTO.getCompanyId()));
        map.put("mrsId", mrsTO.getMrsNumber());
        map.put("priceId", mrsTO.getPriceIds());
        map.put("rcItemId", mrsTO.getRcItemIds());
        map.put("status", activeInd);
        System.out.println((new StringBuilder()).append("status").append(activeInd).toString());
        System.out.println((new StringBuilder()).append(" mrsTO.getMrsNumber()=").append(mrsTO.getMrsNumber()).toString());
        System.out.println((new StringBuilder()).append(" mrsTO.getMrsItemId()=").append(mrsTO.getMrsItemId()).toString());
        System.out.println((new StringBuilder()).append(" mrsTO.getRcItemIds()=").append(mrsTO.getRcItemIds()).toString());
        try {
            status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.updateRcItemIssueMaster", map)).intValue();
            System.out.println((new StringBuilder()).append("updateRcItemIssueMaster ").append(status).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-04", "MrsDAO", "status", sqlException);
        }
        return status;
    }

    public int updateRcItemMaster(int userId, MrsTO mrsTO) {
        Map map = new HashMap();
        int status = 0;
        String activeInd = "";
        if (Integer.parseInt(mrsTO.getActionType()) == 1011) {
            activeInd = "N";
        } else {
            activeInd = "Y";
        }
        System.out.println((new StringBuilder()).append("set rc item status = ").append(activeInd).toString());
        map.put("userId", Integer.valueOf(userId));
        map.put("itemId", mrsTO.getMrsItemId());
        map.put("companyId", Integer.valueOf(mrsTO.getCompanyId()));
        map.put("mrsId", mrsTO.getMrsNumber());
        map.put("priceId", mrsTO.getPriceIds());
        map.put("rcItemId", mrsTO.getRcItemIds());
        map.put("quantity", Float.valueOf(mrsTO.getQuantityRI()));
        map.put("status", activeInd);
        try {
            status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.updateRcItemMaster", map)).intValue();
            System.out.println((new StringBuilder()).append("updateRcItemMaster ").append(status).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-04", "MrsDAO", "status", sqlException);
        }
        return status;
    }

    public int updateRcItemStock(int userId, MrsTO mrsTO) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", Integer.valueOf(userId));
        map.put("itemId", mrsTO.getMrsItemId());
        map.put("companyId", Integer.valueOf(mrsTO.getCompanyId()));
        map.put("mrsId", mrsTO.getMrsNumber());
        map.put("priceId", mrsTO.getPriceIds());
        map.put("quantity", Float.valueOf(mrsTO.getQuantityRI()));
        map.put("rcItemId", mrsTO.getRcItemIds());
        try {
            status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.updateRcItemStock", map)).intValue();
            System.out.println((new StringBuilder()).append("updateRcItemStock ").append(status).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-04", "MrsDAO", "status", sqlException);
        }
        return status;
    }

    public ArrayList getMultipleMrsItems(MrsTO mrsTO) {
        Map map = new HashMap();
        ArrayList mrsItems = new ArrayList();
        MrsTO testTO = new MrsTO();
        Integer test[] = new Integer[mrsTO.getMrsId().length];
        for (int i = 0; i < test.length; i++) {
            test[i] = Integer.valueOf(Integer.parseInt(mrsTO.getMrsId()[i]));
            System.out.println((new StringBuilder()).append("mrsids[]=").append(test[i]).toString());
        }

        testTO.setMrsidsArray(test);
        map.put("itemId", mrsTO.getMrsItemId());
        map.put("mrsIds", mrsTO.getMrsId());
        try {
            mrsItems = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getMultipleMrsItems", map);
            //////System.out.println("in getMultipleMrsItems");
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getMultipleMrsItems Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "getMultipleMrsItems", sqlException);
        }
        return mrsItems;
    }

    public String getSelectedMrsReqQuantity(MrsTO mrsTO) {
        Map map = new HashMap();
        String ReqQty = "";
        String mrs = "";
        String mrsId[] = mrsTO.getMrsId();
        System.out.println((new StringBuilder()).append("mrsIds=").append(mrsTO.getMrsId().length).toString());
        for (int i = 0; i < mrsId.length; i++) {
            System.out.println((new StringBuilder()).append("mrid:").append(mrsId[i]).toString());
        }

        System.out.println((new StringBuilder()).append("itemId=").append(mrsTO.getMrsItemId()).toString());
        System.out.println((new StringBuilder()).append("comp=").append(mrsTO.getCompanyId()).toString());
        map.put("itemId", mrsTO.getMrsItemId());
        map.put("companyId", Integer.valueOf(mrsTO.getCompanyId()));
        map.put("mrsIds", mrsTO.getMrsId());
        try {
            ReqQty = (String) getSqlMapClientTemplate().queryForObject("mrs.getReqForMultiMrs", map);
            if (ReqQty == null) {
                ReqQty = "";
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getSelectedMrsReqQuantity Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "getSelectedMrsReqQuantity", sqlException);
        }
        return ReqQty;
    }

    public ArrayList getIssuedQty4SelecMrs(MrsTO mrsTO) {
        Map map = new HashMap();
        ArrayList issueQty = new ArrayList();
        String mrs = "";
        String mrsId[] = mrsTO.getMrsId();
        for (int i = 0; i < mrsId.length; i++) {
            if (i == 0) {
                mrs = (new StringBuilder()).append("(").append(mrsId[i]).toString();
            } else {
                mrs = (new StringBuilder()).append(mrs).append(",").append(mrsId[i]).toString();
            }
        }

        mrs = (new StringBuilder()).append(mrs).append(")").toString();
        map.put("itemId", mrsTO.getMrsItemId());
        map.put("CompanyId", Integer.valueOf(mrsTO.getCompanyId()));
        map.put("mrsIds", mrsTO.getMrsId());
        try {
            issueQty = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getIssued4SelecMrs", map);
            //////System.out.println("getIssuedQty4SelecMrs");
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getIssuedQty4SelecMrs Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "getIssuedQty4SelecMrs", sqlException);
        }
        return issueQty;
    }

    public ArrayList getPoRaisedQty(MrsTO mrsTO) {
        Map map = new HashMap();
        ArrayList poQty = new ArrayList();
        map.put("itemId", mrsTO.getMrsItemId());
        try {
            poQty = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getPoRaisedQty", map);
            //////System.out.println("in getPoRaisedQty");
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getPoRaisedQty Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "getPoRaisedQty", sqlException);
        }
        return poQty;
    }

    public ArrayList getVendorsForItems(MrsTO mrsTO) {
        Map map = new HashMap();
        ArrayList mrsVendors = new ArrayList();
        map.put("itemId", mrsTO.getMrsItemId());
        map.put("mrsIds", mrsTO.getMrsId());
        try {
            mrsVendors = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getVendorsForItem", map);
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getVendorsForItem Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "getVendorsForItem", sqlException);
        }
        return mrsVendors;
    }

    public ArrayList getJobCardList(int companyId) {
        Map map = new HashMap();
        ArrayList tempList = new ArrayList();
        map.put("companyId", Integer.valueOf(companyId));
        try {
            tempList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getJobCardList", map);
            System.out.println((new StringBuilder()).append("getJobCardList size=").append(tempList.size()).toString());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getJobCardList Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "getJobCardList", sqlException);
        }
        return tempList;
    }

    public ArrayList getJobCardNoVehNo(int companyId) {
        Map map = new HashMap();
        ArrayList tempList = new ArrayList();
        map.put("companyId", Integer.valueOf(companyId));
        try {
            tempList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getJobCardNoVehNo", map);
            System.out.println((new StringBuilder()).append("getJobCardList size=").append(tempList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getJobCardNoVehNo Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "getJobCardNoVehNo", sqlException);
        }
        return tempList;
    }

    public ArrayList vehicleDetails(int jobCardId) {
        Map map = new HashMap();
        ArrayList vehicleDetails = new ArrayList();
        try {
            map.put("jobCardId", Integer.valueOf(jobCardId));
            vehicleDetails = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.vehicleDetails", map);
            System.out.println((new StringBuilder()).append("vehicleDetails size=").append(vehicleDetails.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("vehicleDetails Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-VEH-01", "MrsDAO", "vehicleDetails", sqlException);
        }
        return vehicleDetails;
    }
    public ArrayList getVehicleTyres(int jobCardId) {
        Map map = new HashMap();
        ArrayList vehicleTyres = new ArrayList();
        try {
            map.put("jobCardId", Integer.valueOf(jobCardId));
            vehicleTyres = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getVehicleTyres", map);
            System.out.println((new StringBuilder()).append("vehicleTyres size=").append(vehicleTyres.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("vehicleTyres Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-VEH-01", "MrsDAO", "vehicleTyres", sqlException);
        }
        return vehicleTyres;
    }

    public ArrayList techniciansList(int compId) {
        Map map = new HashMap();
        ArrayList techniciansList = new ArrayList();
        map.put("compId", Integer.valueOf(compId));
        try {
            if (compId == 1026) {
//                hard coded due to volvo service point technician names added
                map.put("compId", 1022);
            } else {
                map.put("compId", compId);
            }
            System.out.println("techniciansList **************************** = " + map);
            techniciansList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.techniciansList", map);
            System.out.println((new StringBuilder()).append("techniciansList size=").append(techniciansList.size()).toString());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            FPLogUtils.fpDebugLog((new StringBuilder()).append("techniciansList Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-VEH-01", "MrsDAO", "techniciansList", sqlException);
        }
        return techniciansList;
    }

    public ArrayList jobCardtechList(int companyId, int jobCardId) {
        Map map = new HashMap();
        ArrayList techniciansList = new ArrayList();
        try {
            map.put("compId", Integer.valueOf(companyId));
            map.put("jobCardId", Integer.valueOf(jobCardId));
            techniciansList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.jobCardtechList", map);
            System.out.println((new StringBuilder()).append("jobCardtechList size=").append(techniciansList.size()).toString());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("jobCardtechList Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-VEH-01", "MrsDAO", "jobCardtechList", sqlException);
        }
        return techniciansList;
    }

    public String mfrCodeSuggestions(String mfrCode) {
        Map map = new HashMap();
        MrsTO mrsTO = null;
        map.put("mfrCode", mfrCode);
        String suggestions = "";
        try {
            ArrayList getItemList = new ArrayList();
            getItemList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.mfrCodeSuggestions", map);
            for (Iterator itr = getItemList.iterator(); itr.hasNext();) {
                mrsTO = (MrsTO) itr.next();
                suggestions = (new StringBuilder()).append(mrsTO.getMfrCode()).append("~").append(suggestions).toString();
            }

        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("mfrCodeSuggestions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-SYS-01", "MrsDAO", "mfrCodeSuggestions", sqlException);
        }
        return suggestions;
    }

    public String itemCodeSuggestions(String itemCode) {
        Map map = new HashMap();
        MrsTO mrsTO = null;
        map.put("itemCode", itemCode);
        String suggestions = "";
        try {
            ArrayList getItemList = new ArrayList();
            getItemList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.itemCodeSuggestions", map);
            for (Iterator itr = getItemList.iterator(); itr.hasNext();) {
                mrsTO = (MrsTO) itr.next();
                suggestions = (new StringBuilder()).append(mrsTO.getItemCode()).append("~").append(suggestions).toString();
            }

        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("itemCodeSuggestions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-SYS-01", "MrsDAO", "itemCodeSuggestions", sqlException);
        }
        return suggestions;
    }

    public String itemNameSuggestions(String itemName) {
        Map map = new HashMap();
        MrsTO mrsTO = null;
        map.put("itemName", itemName);
        String suggestions = "";
        try {
            ArrayList getItemList = new ArrayList();
            getItemList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.itemNameSuggestions", map);
            for (Iterator itr = getItemList.iterator(); itr.hasNext();) {
                mrsTO = (MrsTO) itr.next();
                suggestions = (new StringBuilder()).append(mrsTO.getItemName()).append("~").append(suggestions).toString();
            }

        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("itemNameSuggestions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-SYS-01", "MrsDAO", "itemNameSuggestions", sqlException);
        }
        return suggestions;
    }

    public String getMfrItemId(String mfrCode, String vendorId, String reqType) {
        Map map = new HashMap();
        MrsTO mrsTO = null;
        map.put("mfrCode", mfrCode);
        map.put("vendorId", vendorId);
        map.put("reqType", reqType);
        System.out.println("map1" + map);
        String suggestions = "";
        try {
            if (vendorId != null && !"0".equals(vendorId)) {
                suggestions = (String) getSqlMapClientTemplate().queryForObject("mrs.getMfrItemId", map);
            } else {
                suggestions = (String) getSqlMapClientTemplate().queryForObject("mrs.getMfrItemId1", map);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getMfrItemId Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-SYS-01", "MrsDAO", "getMfrItemId", sqlException);
        }
        return suggestions;
    }

    public String getCodeItemId(String itemCode, String vendorId, String reqType) {
        Map map = new HashMap();
        MrsTO mrsTO = null;
        map.put("itemCode", itemCode);
        map.put("vendorId", vendorId);
        map.put("reqType", reqType);
        System.out.println("map2" + map);
        String suggestions = "";
        try {
            if (vendorId != null && !"0".equals(vendorId)) {
                suggestions = (String) getSqlMapClientTemplate().queryForObject("mrs.getCodeItemId", map);
            } else {
                suggestions = (String) getSqlMapClientTemplate().queryForObject("mrs.getCodeItemId1", map);
            }
            System.out.println((new StringBuilder()).append("suggestions").append(suggestions).toString());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("itemNameSuggestions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-SYS-01", "MrsDAO", "itemNameSuggestions", sqlException);
        }
        return suggestions;
    }

    public String getNameItemId(String itemName, String vendorId, String reqType) {
        Map map = new HashMap();
        MrsTO mrsTO = null;
        map.put("itemName", itemName);
        map.put("vendorId", vendorId);
        map.put("reqType", reqType);
        System.out.println("map3" + map);
        String suggestions = "";
        try {
            if (vendorId != null && !"0".equals(vendorId)) {
                suggestions = (String) getSqlMapClientTemplate().queryForObject("mrs.getNameItemId", map);
            } else {
                suggestions = (String) getSqlMapClientTemplate().queryForObject("mrs.getNameItemId1", map);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("itemNameSuggestions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-SYS-01", "MrsDAO", "itemNameSuggestions", sqlException);
        }
        return suggestions;
    }

    public int insertMRS(int technicianId, int jobCardId, int rcWorkId, int counterId, String remarks, int companyId, String manualMrsDate, int userId) {
        Map map = new HashMap();
        int status = 0;
        int mrsId = 0;
        String temp = "";
        //////System.out.println("technicianId" + Integer.valueOf(technicianId) + "jobCardId" + Integer.valueOf(jobCardId)
//                + "rcWorkId" + Integer.valueOf(rcWorkId) + "remarks" + String.valueOf(remarks)
//                + "manualMrsDate" + String.valueOf(manualMrsDate) + "userId" + Integer.valueOf(userId));
        map.put("technicianId", Integer.valueOf(technicianId));
        map.put("jobCardId", Integer.valueOf(jobCardId));
        map.put("rcWorkId", Integer.valueOf(rcWorkId));
        map.put("counterId", Integer.valueOf(counterId));
        map.put("remarks", String.valueOf(remarks));
        map.put("mrsCreatedCompany", Integer.valueOf(companyId));
        map.put("manualMrsDate", String.valueOf(manualMrsDate));
        map.put("userId", Integer.valueOf(userId));
        try {
            //check mrsId exist for this jobcardId
            //temp =(String)getSqlMapClientTemplate().queryForObject("mrs.getMrsId", map);

            //if(temp==null){
            mrsId = ((Integer) getSqlMapClientTemplate().insert("mrs.insertMRS", map)).intValue();
            //////System.out.println("Mrs Generate" + mrsId);
//            if(mrsId != 0)
//            {
//                System.out.println((new StringBuilder()).append("mrsId").append(mrsId).toString());
            map.put("MrsId", Integer.valueOf(mrsId));
            map.put("Status", "Approved");
            map.put("Remarks", "Approved");
            status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.InsertMrsStatus", map)).intValue();
//            }
//            }else{
//               mrsId=Integer.parseInt(temp);
//            }

            System.out.println((new StringBuilder()).append("status ").append(status).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("insertMRS Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "insertMRS", sqlException);
        }
        return mrsId;
    }

    public int insertMrsItems(int itemId, float qty, int mrsId, int rcItemsId, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("itemId", Integer.valueOf(itemId));
        map.put("qty", Float.valueOf(qty));
        map.put("mrsId", Integer.valueOf(mrsId));
        String temp = "";
        try {
            //check this item exist

            map.put("Issue", Float.valueOf(qty));
            map.put("RequestedQty", Float.valueOf(qty));
            map.put("positionId", Integer.valueOf(0));
            map.put("userId", Integer.valueOf(userId));
            map.put("ItemId", Integer.valueOf(itemId));
            map.put("MrsId", Integer.valueOf(mrsId));

            //Hari
            map.put("rcItemsId", Integer.valueOf(rcItemsId));

//            temp=(String)getSqlMapClientTemplate().queryForObject("mrs.isthisItemExist",map);
//            if(temp==null){
            status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.insertMrsItems", map)).intValue();
            status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.insertMrsApprovedQuantity", map)).intValue();
//            }else{
//              status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.updateMrsItems", map)).intValue();
//              status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.updateMrsApprovedQuantity", map)).intValue();
//            }



            System.out.println((new StringBuilder()).append("status ").append(status).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("insertMrsItems Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "insertMrsItems", sqlException);
        }
        return status;
    }

    public ArrayList getMrsList1(int jobcardId) {
        Map map = new HashMap();
        ArrayList mrsList = new ArrayList();
        map.put("jobcardId", Integer.valueOf(jobcardId));
        try {
            mrsList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getJobCardMrsList", map);
            System.out.println((new StringBuilder()).append("getMrsList size=").append(mrsList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getMrsList Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "getMrsList", sqlException);
        }
        return mrsList;
    }

    public ArrayList getMrsItems(int mrsId) {
        Map map = new HashMap();
        ArrayList mrsList = new ArrayList();
        map.put("mrsId", Integer.valueOf(mrsId));
        try {
            mrsList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getMrsItems", map);
            System.out.println((new StringBuilder()).append("getMrsList size=").append(mrsList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getMrsItems Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "getMrsItems", sqlException);
        }
        return mrsList;
    }

    public ArrayList getApprovedMrsItems(int mrsId) {
        Map map = new HashMap();
        ArrayList mrsList = new ArrayList();
        ArrayList returnMrsList = new ArrayList();
        map.put("mrsId", Integer.valueOf(mrsId));
        System.out.println("mrsId" + map);
        String quantity = "";
        try {
            mrsList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getApprovedMrsItems", map);

            Iterator itrP = mrsList.iterator();
            while (itrP.hasNext()) {
                MrsTO mrs = (MrsTO) itrP.next();
                map.put("itemId", mrs.getMrsItemId());
                quantity = (String) getSqlMapClientTemplate().queryForObject("mrs.totalIssued", map);
                mrs.setMrsIssueQuantity(quantity);
                returnMrsList.add(mrs);
            }

            System.out.println((new StringBuilder()).append("getMrsList size=").append(mrsList.size()).toString());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getApprovedMrsItems Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "getApprovedMrsItems", sqlException);
        }
        return returnMrsList;
    }

    public String getTotalIssuedQuantity(int mrsId, int itemId) {
        Map map = new HashMap();
        String quantity = "";
        map.put("mrsId", Integer.valueOf(mrsId));
        map.put("itemId", Integer.valueOf(itemId));
        try {
            quantity = (String) getSqlMapClientTemplate().queryForObject("mrs.totalIssued", map);
            System.out.println((new StringBuilder()).append("Total issued Qty for mrs and itemId=").append(mrsId).append("-").append(itemId).append("qty").append(quantity).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getTotalIssuedQuantity Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "getTotalIssuedQuantity", sqlException);
        }
        return quantity;
    }

    public String getTotalIssuedQuantityPrice(int mrsId, int itemId) {
        Map map = new HashMap();
        String quantity = "";
        map.put("mrsId", Integer.valueOf(mrsId));
        map.put("itemId", Integer.valueOf(itemId));
        try {
            quantity = (String) getSqlMapClientTemplate().queryForObject("mrs.totalIssuedPrice", map);
            System.out.println((new StringBuilder()).append("Total issued Qty for mrs and itemId=").append(mrsId).append("-").append(itemId).append("qty").append(quantity).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getTotalIssuedQuantity Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "getTotalIssuedQuantity", sqlException);
        }
        return quantity;
    }

    public String getTotalIssuedQuantityTax(int mrsId, int itemId) {
        Map map = new HashMap();
        String tax = "";
        map.put("mrsId", Integer.valueOf(mrsId));
        map.put("itemId", Integer.valueOf(itemId));
        try {
            tax = (String) getSqlMapClientTemplate().queryForObject("mrs.totalIssuedTax", map);
            System.out.println((new StringBuilder()).append("Total issued Tax for mrs and itemId=").append(mrsId).append("-").append(itemId).append("Tax").append(tax).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getTotalIssuedQuantityTax Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "getTotalIssuedQuantityTax", sqlException);
        }
        return tax;
    }

    public String getMrsRcItems(int Item_id, int companyId) {
        Map map = new HashMap();
        String qty = "";
        int status = 0;
        //////System.out.println("hello");
        try {
            System.out.println((new StringBuilder()).append("itemId=").append(Item_id).toString());
            System.out.println((new StringBuilder()).append("companyId=").append(companyId).toString());
            map.put("itemId", Integer.valueOf(Item_id));
            map.put("companyId", Integer.valueOf(companyId));
            qty = (String) getSqlMapClientTemplate().queryForObject("mrs.getRcItems", map);
            if (qty == null) {
                qty = "0";
            }
            System.out.println((new StringBuilder()).append("qty=").append(qty).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "Rc items Mrs", sqlException);
        }
        return qty;
    }

    public ArrayList getMrsIssueHistory(int mrsId) {
        Map map = new HashMap();
        int qty = 0;
        ArrayList itemList = new ArrayList();
        ArrayList tyreList = new ArrayList();
        ArrayList newItemList = new ArrayList();
        String temp[] = null;
        MrsTO mrs = new MrsTO();
        try {
            map.put("mrsId", Integer.valueOf(mrsId));
            itemList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getMrsIssueHistory", map);
            tyreList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getTyreMrsIssueHistory", map);
            for (Iterator itr = itemList.iterator(); itr.hasNext(); newItemList.add(mrs)) {
                mrs = (MrsTO) itr.next();
                temp = mrs.getMrsNumber().split("~");
                mrs.setMrsNumber(temp[0]);
                mrs.setItemType(temp[1]);
                mrs.setQuantityRI(Float.parseFloat(temp[2]));
            }

            newItemList.addAll(tyreList);
            System.out.println((new StringBuilder()).append("newItemList size=").append(newItemList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "Rc items Mrs", sqlException);
        }
        return newItemList;
    }

    public ArrayList getJobcardIssueHistory(int jcId) {
        Map map = new HashMap();
        int qty = 0;
        ArrayList itemList = new ArrayList();
        ArrayList tyreList = new ArrayList();
        ArrayList newItemList = new ArrayList();
        String temp[] = null;
        MrsTO mrs = new MrsTO();
        try {
            map.put("jobCardId", Integer.valueOf(jcId));
            itemList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getJobcardItemIssueHistory", map);
            tyreList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getJobcardTyreIssueHistory", map);
            for (Iterator itr = itemList.iterator(); itr.hasNext(); newItemList.add(mrs)) {
                mrs = (MrsTO) itr.next();
                temp = mrs.getMrsNumber().split("~");
                mrs.setMrsNumber(temp[0]);
                mrs.setItemType(temp[1]);
                mrs.setQuantityRI(Float.parseFloat(temp[2]));
            }

            newItemList.addAll(tyreList);
            System.out.println((new StringBuilder()).append("newItemList size=").append(newItemList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getJobcardIssueHistory Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "getJobcardIssueHistory", sqlException);
        }
        return newItemList;
    }

    public ArrayList processMrsItemStatus(int jcId) {
        Map map = new HashMap();
        int qty = 0;
        ArrayList itemList = new ArrayList();
        try {
            map.put("jobCardId", Integer.valueOf(jcId));
            itemList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.processMrsItemStatus", map);

        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getJobcardIssueHistory Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "getJobcardIssueHistory", sqlException);
        }
        return itemList;
    }

    public int doInsertScrapItems(MrsTO mrs, int companyId, int userId) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("companyId", Integer.valueOf(companyId));
            map.put("userId", Integer.valueOf(userId));
            map.put("mrsId", mrs.getMrsNumber());
            map.put("itemId", mrs.getMrsItemId());
            if (!mrs.getRcItemIds().equals("")) {
                map.put("rcItemId", mrs.getRcItemIds());
            } else {
                map.put("rcItemId", "0");
            }
            //////System.out.println("rcItemId = ::");
            System.out.println((new StringBuilder()).append("rcItemId = :").append(mrs.getRcItemIds()).append(":").toString());
            map.put("quantity", mrs.getFaultQty());
            status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.insertScrap", map)).intValue();
            System.out.println((new StringBuilder()).append("Inserted Status=").append(status).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doInsertScrap Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "doInsertScrap", sqlException);
        }
        return status;
    }

    public String getFaultItemsReceived(MrsTO mrsTO) {
        Map map = new HashMap();
        String qty = "";
        MrsTO mrs = null;
        try {
            map.put("itemId", mrsTO.getMrsItemId());
            map.put("mrsId", mrsTO.getMrsNumber());
            qty = (String) getSqlMapClientTemplate().queryForObject("mrs.faultItemsReceived", map);
            if (qty == null) {
                qty = "0";
            }
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getFaultItemsReceived Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "getFaultItemsReceived", sqlException);
        }
        return qty;
    }

    public int doGenerateMrsGdn(MrsTO mrsTO, int userId) {
        Map map = new HashMap();
        int stat = 0;
        MrsTO mrs = null;
        try {
            map.put("mrsId", mrsTO.getMrsNumber());
            map.put("remarks", mrsTO.getMrsApprovalRemarks());
            map.put("userId", Integer.valueOf(userId));
            stat = Integer.valueOf(getSqlMapClientTemplate().update("mrs.generateMrsGdn", map)).intValue();
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doGenerateMrsGdn Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "doGenerateMrsGdn", sqlException);
        }
        return stat;
    }

    public ArrayList getMrsApprovedTyres(int mrsId) {
        Map map = new HashMap();
        int stat = 0;
        ArrayList tyresList = new ArrayList();
        MrsTO mrs = null;
        try {
            map.put("mrsId", Integer.valueOf(mrsId));
            System.out.println((new StringBuilder()).append("for mrsId ").append(mrsId).toString());
            tyresList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getApprovedTyres", map);
            System.out.println((new StringBuilder()).append("tyres ist=").append(tyresList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doGenerateMrsGdn Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "doGenerateMrsGdn", sqlException);
        }
        return tyresList;
    }

    public ArrayList getNewTyresList(int itemId, int companyId) {
        Map map = new HashMap();
        int stat = 0;
        ArrayList tyresList = new ArrayList();
        MrsTO mrs = null;
        try {
            map.put("itemId", Integer.valueOf(itemId));
            map.put("companyId", Integer.valueOf(companyId));
            System.out.println((new StringBuilder()).append("for itemId ").append(itemId).toString());
            System.out.println((new StringBuilder()).append("for companyId ").append(companyId).toString());
            tyresList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getNewTyresList", map);
            System.out.println((new StringBuilder()).append("tyres ist=").append(tyresList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getNewTyresList Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "getNewTyresList", sqlException);
        }
        return tyresList;
    }

    public int doTyresIssue(MrsTO mrsTO, int userId) {
        Map map = new HashMap();
        int stat = 0;
        String isRt = "";
        ArrayList tyresList = new ArrayList();
        int historyId = 0;
        MrsTO mrs = null;
        try {
            map.put("itemId", mrsTO.getMrsItemId());
            map.put("mrsId", mrsTO.getMrsNumber());
            map.put("tyreId", mrsTO.getTyreId());
            map.put("actionType", mrsTO.getActionType());
            map.put("positionId", mrsTO.getPositionId());
            map.put("price", mrsTO.getPriceIds());
            map.put("userId", Integer.valueOf(userId));
            map.put("status", "N");
            map.put("rcPriceWoId", mrsTO.getRcPriceWoId());
            //////System.out.println("in doTyresIssue" + mrsTO.getRcPriceWoId());
            isRt = (String) getSqlMapClientTemplate().queryForObject("mrs.getTyreRtStat", map);
            map.put("rtStatus", isRt);
            historyId = ((Integer) getSqlMapClientTemplate().insert("mrs.issueTyre", map)).intValue();
            map.put("historyId", Integer.valueOf(historyId));
            stat = Integer.valueOf(getSqlMapClientTemplate().update("mrs.updateStockStatus", map)).intValue();
            if (!mrsTO.getItemType().equalsIgnoreCase("NEW")) {
                stat = Integer.valueOf(getSqlMapClientTemplate().update("mrs.insertRtTyrePrice", map)).intValue();
            }
            System.out.println((new StringBuilder()).append("tyres ist=").append(tyresList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doTyresIssue Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "doTyresIssue", sqlException);
        }
        return stat;
    }

    public int getFaultItemsReceived(int mrsId, int itemId) {
        Map map = new HashMap();
        int stat = 0;
        MrsTO mrs = null;
        try {
            map.put("itemId", Integer.valueOf(itemId));
            map.put("mrsId", Integer.valueOf(mrsId));
            stat = ((Integer) getSqlMapClientTemplate().queryForObject("mrs.getFaultTyresReceived", map)).intValue();
            System.out.println((new StringBuilder()).append("fault ItemsReceive=").append(stat).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getFaultItemsReceived Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "getFaultItemsReceived", sqlException);
        }
        return stat;
    }

    public int getTotalTyresIssued(int mrsId, int itemId, int positionId) {
        Map map = new HashMap();
        int stat = 0;
        MrsTO mrs = null;
        try {
            map.put("mrsId", Integer.valueOf(mrsId));
            map.put("itemId", Integer.valueOf(itemId));
            map.put("positionId", Integer.valueOf(positionId));
            stat = ((Integer) getSqlMapClientTemplate().queryForObject("mrs.getTyresIssuedQty", map)).intValue();
            System.out.println((new StringBuilder()).append("getRtTyresStock=").append(stat).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getRtTyresStock Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "getRtTyresStock", sqlException);
        }
        return stat;
    }

    public int doRtStockUpdate(int tyreId, String status) {
        Map map = new HashMap();
        int stat = 0;
        MrsTO mrs = null;
        try {
            map.put("tyreId", Integer.valueOf(tyreId));
            map.put("status", status);
            stat = Integer.valueOf(getSqlMapClientTemplate().update("mrs.updateStockStatus", map)).intValue();
            System.out.println((new StringBuilder()).append("doRtStockUpdate=").append(stat).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doRtStockUpdate Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "doRtStockUpdate", sqlException);
        }
        return stat;
    }

    public int doInsertRcQueue(MrsTO mrsTo, int userId) {
        Map map = new HashMap();
        int stat = 0;
        int faultQt = 0;
        faultQt = Integer.parseInt(mrsTo.getFaultQty());
        try {
            System.out.println((new StringBuilder()).append("companyId Stat = ").append(mrsTo.getCompanyId()).toString());
            if (mrsTo.getCompanyId() == 1022) {
                map.put("senderStatus", "Y");
            }
            if (mrsTo.getCompanyId() != 1022) {
                map.put("senderStatus", "N");
            }
            map.put("mrsId", mrsTo.getMrsNumber());
            map.put("itemId", mrsTo.getMrsItemId());
            map.put("companyId", Integer.valueOf(mrsTo.getCompanyId()));
            map.put("status", "Y");
            int categoryId = Integer.valueOf(mrsTo.getCategoryId()).intValue();
            map.put("userId", Integer.valueOf(userId));
            if (!mrsTo.getRcItemId().equalsIgnoreCase("0") && mrsTo.getRcItemId() != "" && mrsTo.getRcItemId() != null) {
                map.put("rcItemId", mrsTo.getRcItemId());
            } else {

                if (categoryId == 1011) {
                    //generate rc item id
                    map.put("refNo", mrsTo.getOldTyreId());
                    System.out.println("rcItemId map:"+map);
                    int rcItemId = (Integer) getSqlMapClientTemplate().insert("mrs.insertFaultItem", map);
                    System.out.println("rcItemId value="+rcItemId);
                    map.put("rcItemId", rcItemId);
                }else{
                    map.put("rcItemId", "0");
                }
            }


            for (int i = 0; i < faultQt; i++) {
                stat = (Integer) getSqlMapClientTemplate().insert("mrs.insertRcQueue", map);
            }

            System.out.println((new StringBuilder()).append("doInsertRcQueue=").append(stat).toString());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doInsertRcQueue Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "doInsertRcQueue", sqlException);
        }
        return stat;
    }

    public ArrayList getItemsIsuedDetails(int mrsId, int itemId) {
        Map map = new HashMap();
        int qty = 0;
        ArrayList itemList = new ArrayList();
        ArrayList newItemList = new ArrayList();
        String temp[] = null;
        MrsTO mrs = new MrsTO();
        try {
            map.put("mrsId", Integer.valueOf(mrsId));
            map.put("itemId", Integer.valueOf(itemId));
            itemList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.itemsIssuedDetails", map);
            for (Iterator itr = itemList.iterator(); itr.hasNext(); newItemList.add(mrs)) {
                mrs = (MrsTO) itr.next();
                temp = mrs.getIssueId().split("~");
                mrs.setIssueId(temp[0]);
                mrs.setItemType(temp[1]);
                temp = mrs.getMrsItemId().split("~");
                mrs.setRcItemId(temp[0]);
                mrs.setPriceId(temp[1]);
                temp = mrs.getTotalIssued().split("~");
                mrs.setPrice(temp[0]);
                mrs.setTotalIssued(temp[1]);
            }

            System.out.println((new StringBuilder()).append("newItemList size=").append(newItemList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "Rc items Mrs", sqlException);
        }
        return newItemList;
    }

    public ArrayList getTyresIsuedDetails(int mrsId, int itemId, int positionId) {
        Map map = new HashMap();
        int stat = 0;
        ArrayList itemList = new ArrayList();
        ArrayList newItemList = new ArrayList();
        MrsTO mrs = new MrsTO();
        String temp1[] = null;
        String temp2[] = null;
        try {
            map.put("mrsId", Integer.valueOf(mrsId));
            map.put("itemId", Integer.valueOf(itemId));
            map.put("positionId", Integer.valueOf(positionId));
            itemList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.tyresIssuedDetails", map);
            System.out.println((new StringBuilder()).append("itemList size=").append(itemList.size()).toString());
            for (Iterator itr = itemList.iterator(); itr.hasNext(); newItemList.add(mrs)) {
                //////System.out.println("in loop");
                mrs = new MrsTO();
                mrs = (MrsTO) itr.next();
                temp1 = mrs.getTyreId().split("~");
                mrs.setTyreId(temp1[0]);
                mrs.setItemType(temp1[1]);
                temp2 = mrs.getPrice().split("~");
                mrs.setPriceId(temp2[0]);
                mrs.setPrice(temp2[1]);
                mrs.setTotalIssued("1");
            }

            System.out.println((new StringBuilder()).append("tyres ist=").append(itemList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getTyresIsuedDetails Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "getTyresIsuedDetails", sqlException);
        }
        return itemList;
    }

    public int doReturnIssuedTyre(MrsTO mrsTo) {
        Map map = new HashMap();
        //////System.out.println("////////////////////doReturnIssuedTyre");
        int stat = 0;
        int stat1 = 0;
        int stat2 = 0;
        int faultQt = 0;
        try {
            map.put("historyId", mrsTo.getIssueId());
            map.put("status", "Y");
            map.put("tyreIssueStatus", "N");
            map.put("tyreId", mrsTo.getTyreId());
            map.put("companyId", Integer.valueOf(mrsTo.getCompanyId()));
            map.put("itemId", mrsTo.getMrsItemId());
            map.put("priceId", mrsTo.getPriceId());
            map.put("quantity", "1");
            stat = Integer.valueOf(getSqlMapClientTemplate().update("mrs.returnIssuedTyre", map)).intValue();
            stat1 = Integer.valueOf(getSqlMapClientTemplate().update("mrs.updateStockStatus", map)).intValue();
            stat2 = Integer.valueOf(getSqlMapClientTemplate().update("mrs.updateNewItemStock", map)).intValue();
            System.out.println((new StringBuilder()).append("stat=").append(stat).toString());
            System.out.println((new StringBuilder()).append("stat1=").append(stat1).toString());
            System.out.println((new StringBuilder()).append("stat2=").append(stat2).toString());
            System.out.println((new StringBuilder()).append("doReturnIssuedTyre=").append(stat).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doReturnIssuedTyre Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "doReturnIssuedTyre", sqlException);
        }
        return stat;
    }

    public int doReturnRtTyre(MrsTO mrsTo) {
        Map map = new HashMap();
        int stat = 0;
        int stat1 = 0;
        int stat2 = 0;
        int faultQt = 0;
        int qty = 1;
        //////System.out.println("In doReturnRtTyre");
        try {
            map.put("historyId", mrsTo.getIssueId());
            map.put("status", "Y");
            map.put("tyreIssueStatus", "N");
            map.put("tyreId", mrsTo.getTyreId());
            map.put("companyId", Integer.valueOf(mrsTo.getCompanyId()));
            map.put("itemId", mrsTo.getMrsItemId());
            map.put("priceId", mrsTo.getPriceId());
            map.put("quantity", Integer.valueOf(qty));
            System.out.println((new StringBuilder()).append("companyId = ").append(mrsTo.getCompanyId()).toString());
            System.out.println((new StringBuilder()).append("ItemId = ").append(mrsTo.getMrsItemId()).toString());
            stat = Integer.valueOf(getSqlMapClientTemplate().update("mrs.returnIssuedTyre", map)).intValue();
            stat1 = Integer.valueOf(getSqlMapClientTemplate().update("mrs.updateStockStatus", map)).intValue();
            stat2 = Integer.valueOf(getSqlMapClientTemplate().update("mrs.updateRcItemStock", map)).intValue();
            System.out.println((new StringBuilder()).append("stat=").append(stat).toString());
            System.out.println((new StringBuilder()).append("stat1=").append(stat1).toString());
            System.out.println((new StringBuilder()).append("stat2=").append(stat2).toString());
            System.out.println((new StringBuilder()).append("doReturnIssuedTyre=").append(stat).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doReturnIssuedTyre Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "doReturnIssuedTyre", sqlException);
        }
        return stat;
    }

    public ArrayList getTyrePostions() {
        Map map = new HashMap();
        int qty = 0;
        ArrayList itemList = new ArrayList();
        try {
            itemList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getTyrePostions", map);
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getTyrePostions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "getTyrePostions", sqlException);
        }
        return itemList;
    }

    public int insertMrsTyreItems(int itemId, int mrsId, int posId, int userId, int rcItemsId) {
        Map map = new HashMap();
        int status = 0;
        map.put("itemId", Integer.valueOf(itemId));
        map.put("posId", Integer.valueOf(posId));
        map.put("mrsId", Integer.valueOf(mrsId));
        //Hari
        map.put("rcItemsId", Integer.valueOf(rcItemsId));
        try {
            status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.insertMrsTyreItems", map)).intValue();
            map.put("Issue", Integer.valueOf(1));
            map.put("RequestedQty", Integer.valueOf(1));
            map.put("positionId", Integer.valueOf(posId));
            map.put("userId", Integer.valueOf(userId));
            map.put("ItemId", Integer.valueOf(itemId));
            map.put("MrsId", Integer.valueOf(mrsId));
            status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.insertMrsApprovedQuantity", map)).intValue();
            System.out.println((new StringBuilder()).append("status ").append(status).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("insertMrsTyreItems Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "insertMrsTyreItems", sqlException);
        }
        return status;
    }

    public int updateItem(int mrsId, int item, int qty, int catId) {
        Map map = new HashMap();
        int status = 0;
        map.put("mrsId", Integer.valueOf(mrsId));
        map.put("itemId", Integer.valueOf(item));
        map.put("qty", Integer.valueOf(qty));
        try {
            if (catId != 1011) {
                status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.updateItem", map)).intValue();
            }
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("updateItem Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "updateItem", sqlException);
        }
        return status;
    }

    public int deleteItem(int mrsId, int item, int cat) {
        Map map = new HashMap();
        int status = 0;
        map.put("mrsId", Integer.valueOf(mrsId));
        map.put("itemId", Integer.valueOf(item));
        try {
            if (cat != 1011) {
                status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.deleteItem", map)).intValue();
            } else {
                status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.deleteTyreItem", map)).intValue();
            }
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("updateItem Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "updateItem", sqlException);
        }
        return status;
    }

    public int doUpdateRcPrice(int mrsId, int item, int cat) {
        Map map = new HashMap();
        int status = 0;
        map.put("mrsId", Integer.valueOf(mrsId));
        map.put("itemId", Integer.valueOf(item));
        try {
            if (cat != 1011) {
                status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.deleteItem", map)).intValue();
            } else {
                status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.deleteTyreItem", map)).intValue();
            }
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("updateItem Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "updateItem", sqlException);
        }
        return status;
    }

    public int getMrsVehicleId(int mrsId) {
        Map map = new HashMap();
        int vehicleId = 0;
        Integer test = Integer.valueOf(0);
        map.put("mrsId", Integer.valueOf(mrsId));
        try {
            test = (Integer) getSqlMapClientTemplate().queryForObject("mrs.getMrsVehicleId", map);
            if (test != null) {
                vehicleId = test.intValue();
            }
            System.out.println((new StringBuilder()).append("vehicleId=").append(vehicleId).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getMrsVehicleId Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "getMrsVehicleId", sqlException);
        }
        return vehicleId;
    }

    public int doInsertVehicleTyre(MrsTO tyreTO, int UserId, int companyId) {
        Map map = new HashMap();
        map.put("vehicleId", Integer.valueOf(tyreTO.getVehicleId()));
        int status = 0;
        String temp = "";
        int insertStatus = 0;
        int tyreId = 0;
        try {
            map.put("tyreId", tyreTO.getOldTyreId());
            map.put("mrsId", tyreTO.getMrsNumber());
            map.put("positionId", tyreTO.getPositionId());
            map.put("itemId", tyreTO.getMrsItemId());
            map.put("companyId", Integer.valueOf(companyId));
            map.put("userId", Integer.valueOf(UserId));
            System.out.println((new StringBuilder()).append("tyre Id =").append(tyreTO.getTyreId()).toString());
            System.out.println((new StringBuilder()).append("positionId =").append(tyreTO.getPositionId()).toString());
            System.out.println((new StringBuilder()).append("itemId =").append(tyreTO.getMrsItemId()).toString());
            temp = (String)getSqlMapClientTemplate().queryForObject("mrs.checkVehPositionTyre", map);
            System.out.println((new StringBuilder()).append("temp=").append(temp).toString());
            if(tyreTO.getOldTyreId().equals("0")){//old tyre no is not given
                System.out.println("am hreeee33333");
                map.put("tyreId", tyreTO.getTyreId());
                insertStatus = Integer.valueOf(getSqlMapClientTemplate().update("mrs.updateNewVehTyre", map)).intValue();
            }else{//old tyre no is given
                System.out.println("am hreeee44444");
                map.put("tyreId", tyreTO.getOldTyreId());
                System.out.println("map updateVehTyre="+map);
                insertStatus = Integer.valueOf(getSqlMapClientTemplate().update("mrs.updateVehTyre", map)).intValue();
                System.out.println("insertStatus value="+insertStatus);
                String[] temp1 = temp.split("~");
                map.put("service_point_id", temp1[0]);
                map.put("axle_detail_id", temp1[1]);
                map.put("axle_id", temp1[2]);
                map.put("axle_type_name", temp1[3]);
                map.put("position_name", temp1[4]);
                map.put("position_no", temp1[5]);
                map.put("tyreId", tyreTO.getTyreId());

                System.out.println("map insertVehTyre="+map);
                insertStatus = Integer.valueOf(getSqlMapClientTemplate().update("mrs.insertVehTyre", map)).intValue();
            }
            if (temp == null) { //tyre no is not mapped to this vehicle
                /* commented on 12 Dec 16
                insertStatus = Integer.valueOf(getSqlMapClientTemplate().update("mrs.updateVehPosition", map)).intValue();
                insertStatus = Integer.valueOf(getSqlMapClientTemplate().update("mrs.updateVehTyre", map)).intValue();
                insertStatus = Integer.valueOf(getSqlMapClientTemplate().update("mrs.insertVehTyre", map)).intValue();
                */
                /*
                        1.
                */
            }else{//tyre no exists
                
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doInsertVehicleTyre Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MFR-02", "MrsDAO", "doInsertVehicleTyre", sqlException);
        }
        return insertStatus;
    }

    public int doUpdateReturnVehicleTyre(MrsTO tyreTO) {
        Map map = new HashMap();
        map.put("vehicleId", Integer.valueOf(tyreTO.getVehicleId()));
        int status = 0;
        int test = 0;
        int insertStatus = 0;
        int tyreId = 0;
        try {
            map.put("tyreId", tyreTO.getTyreId());
            map.put("positionId", tyreTO.getPositionId());
            map.put("itemId", tyreTO.getMrsItemId());
            System.out.println((new StringBuilder()).append("tyre Id =").append(tyreTO.getTyreId()).toString());
            System.out.println((new StringBuilder()).append("positionId =").append(tyreTO.getPositionId()).toString());
            System.out.println((new StringBuilder()).append("itemId =").append(tyreTO.getMrsItemId()).toString());
            insertStatus = Integer.valueOf(getSqlMapClientTemplate().update("mrs.updateVehPosition", map)).intValue();
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("doInsertVehicleTyre Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MFR-02", "MrsDAO", "doInsertVehicleTyre", sqlException);
        }
        return insertStatus;
    }

    //Hari
    public ArrayList getRcMrsDetails(int mrsId) {
        Map map = new HashMap();
        ArrayList rcMrsDetails = new ArrayList();
        try {
            map.put("mrsId", Integer.valueOf(mrsId));
            rcMrsDetails = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getRcMrsDetails", map);
            System.out.println((new StringBuilder()).append("RcmrsDetails size=").append(rcMrsDetails.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getUserAuthorisedFunctions Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "getRcMrsDetails", sqlException);
        }
        return rcMrsDetails;
    }

    public int insertRcSpareDetails(int rcWorkId, int rcItem, int itemId, int userId, float qty) {
        Map map = new HashMap();
        int status = 0;
        ArrayList checkList = new ArrayList();
        String temp = "";
        try {
            //check this item exist

            map.put("rcWorkId", Integer.valueOf(rcWorkId));
            map.put("rcItem", Integer.valueOf(rcItem));
            map.put("itemId", Integer.valueOf(itemId));
            map.put("qty", Float.valueOf(qty));

            map.put("userId", Integer.valueOf(userId));

            checkList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getRcSpareDetails", map);
            if (checkList.size() != 0) {
                //////System.out.println("Already It Hold it");
                status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.updateRcSpareDetails", map)).intValue();
                //////System.out.println("Updated Spare RC Details -->" + status);
            } else {
                status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.insertRcSpareDetails", map)).intValue();
                System.out.println((new StringBuilder()).append("insertRcSpareDetails ").append(status).toString());

            }


        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("insertMrsItems Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "insertMrsItems", sqlException);
        }
        return status;
    }

    public int processReturnSpareItems(MrsTO mrsTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int retItemId = 0;
        ArrayList checkList = new ArrayList();
        String temp = "";
        Float amount = 0.0F;
        Float price = 0.0F;
        Float quantity = 0.0F;
        try {
            //check this item exist

            //////System.out.println("mrsTO.getPrice()-->" + mrsTO.getPrice());
            map.put("price", Float.valueOf(mrsTO.getPrice()));

            //////System.out.println("mrsTO.getQuantityRI()-->" + mrsTO.getQuantityRI());
            map.put("quantity", Float.valueOf(mrsTO.getQuantityRI()));

            //////System.out.println("mrsTO.getPrice()-->" + mrsTO.getPrice());
            price = Float.valueOf(mrsTO.getPrice());
            quantity = Float.valueOf(mrsTO.getQuantityRI());
            amount = quantity * price;

            //////System.out.println("AMount-->" + amount);
            map.put("amount", amount);
            //////System.out.println("mrsTO.getType() in Return New Or Rcs" + mrsTO.getType());
            if (mrsTO.getType().equalsIgnoreCase("NEW")) {
                //////System.out.println("Return For New Items");
                //////System.out.println("mrsTO.getRcItemIds()-->" + mrsTO.getRcItemIds());
                retItemId = Integer.valueOf(mrsTO.getRcItemIds());
                //////System.out.println("mrsTO.getRcItemIds()" + mrsTO.getRcItemIds());
            } else {
                //////System.out.println("Return For Rc Items");
                map.put("retItemId", Integer.valueOf(mrsTO.getRcItemIds()));
                retItemId = Integer.valueOf(mrsTO.getRcItemIds());
                //////System.out.println("From Jsp-->" + retItemId);

                retItemId = (Integer) getSqlMapClientTemplate().queryForObject("mrs.getItemIdForRcItem", map);

                //////System.out.println("Return Item Id Now()-->" + retItemId);

            }

            map.put("retItemId", retItemId);

            //////System.out.println("mrsTO.getRcWorkId()-->" + mrsTO.getRcWorkId());
            map.put("rcWorkId", Integer.valueOf(mrsTO.getRcWorkId()));

            //////System.out.println("mrsTO.getRcItemsId()-->" + mrsTO.getRcItemsId());
            map.put("rcItemsId", Integer.valueOf(mrsTO.getRcItemsId()));

            //////System.out.println("userId-->" + userId);
            map.put("userId", Integer.valueOf(userId));

            status = Integer.valueOf(getSqlMapClientTemplate().update("mrs.updateReturnSpareItems", map)).intValue();
            System.out.println((new StringBuilder()).append("updateReturnSpareItems ").append(status).toString());


        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("updateReturnSpareItems Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "updateReturnSpareItems", sqlException);
        }
        return status;
    }

    public int updatePriceForSpareItems(MrsTO mrsTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        ArrayList checkList = new ArrayList();
        String temp = "";
        try {
            //check this item exist
            //////System.out.println(">>>>>>>>>>> In updatePriceForSpareItems ");
            Float price = 0.0F;
            int priceId = 0;
            Float quantity = 0.0F;
            Float amount = 0.0F;


            ArrayList priceList = new ArrayList();
            //////System.out.println("mrsTO.getType() in update" + mrsTO.getType());
            if (mrsTO.getType().equals("1011")) {
                priceId = Integer.valueOf(mrsTO.getPriceIds()).intValue();
                //////System.out.println("priceId" + priceId);
                map.put("priceId", priceId);
                //////System.out.println("It's Update New Items");
                priceList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getPriceWithTax", map);
                //////System.out.println("priceWthTax.size()-->" + priceList.size());

                Iterator itrP = priceList.iterator();
                while (itrP.hasNext()) {
                    MrsTO mrs = (MrsTO) itrP.next();
                    //////System.out.println("mrs.getPriceWithTax()" + mrs.getPriceWithTax());
                    price = Float.valueOf(mrs.getPriceWithTax());
                    //////System.out.println("New Item Price"+price);
                }
            } else {
                //////System.out.println("RC Items Updated Here");
                //////System.out.println("Rc Price-->" + mrsTO.getPriceIds());
                price = Float.valueOf(mrsTO.getPriceIds()).floatValue();
            }
            //////System.out.println("Float.valueOf-->" + price);
            //////System.out.println("mrsTO.getQty()-->" + mrsTO.getQty());

            int categoryId = Integer.valueOf(mrsTO.getCategoryId()).intValue();
            if (categoryId == 1011) {
                quantity = 1.0F;
            } else {
                quantity = Float.valueOf(mrsTO.getQty());
            }

            //////System.out.println("+ Quantity category-->" + quantity + "-->" + categoryId);
            amount = Float.valueOf(quantity * price).floatValue();
            //////System.out.println("  quantity-->" + quantity);
            //////System.out.println("After Calculate Price for quantity-->" + amount);

            map.put("quantity", quantity);
            map.put("priceId", priceId);
            map.put("amount", amount);
            map.put("rcWorkId", Integer.valueOf(mrsTO.getRcWorkId()));
            map.put("rcItemsId", Integer.valueOf(mrsTO.getRcItemsId()));
            map.put("mrsItemId", Integer.valueOf(mrsTO.getMrsItemId()));

            //////System.out.println("mrsTO.getRcWorkId()" + mrsTO.getRcWorkId());
            //////System.out.println("mrsTO.mrsTO.getMrsItemId()" + mrsTO.getMrsItemId());
            //////System.out.println("mrsTO.getRcItemsId()" + mrsTO.getRcItemsId());

            status = ((Integer) getSqlMapClientTemplate().update("mrs.updatePriceForRcItems", map)).intValue();
            //////System.out.println("Update Price For Rc Items updateTyreItemSpareDetails-->" + status);

        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("updatePriceForRcItems Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-02", "MrsDAO", "updatePriceForRcItems", sqlException);
        }
        return status;
    }

    public ArrayList getRcApprovedMrsItems(int mrsId) {
        Map map = new HashMap();
        ArrayList rcMrsList = new ArrayList();
        map.put("mrsId", Integer.valueOf(mrsId));
        try {
            rcMrsList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getRcApprovedMrsItems", map);
            System.out.println((new StringBuilder()).append("rcMrsList size=").append(rcMrsList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getRcApprovedMrsItems Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "getRcApprovedMrsItems", sqlException);
        }
        return rcMrsList;
    }
//For Counter Bill Items

    public int insertCounterMrs(MrsTO mrsTO) {
        Map map = new HashMap();
        int status = 0;
        String temp = "";
        try {
            //check this item exist

            map.put("customerName", mrsTO.getName());
            map.put("customerAddress", mrsTO.getAddress());
            map.put("mobileNo", mrsTO.getMobileNo());
            map.put("remarks", mrsTO.getRemarks());
            map.put("userId", mrsTO.getUserId());
            map.put("companyId", mrsTO.getCompanyId());
            status = (Integer) getSqlMapClientTemplate().insert("mrs.insertCounterMrs", map);
            System.out.println((new StringBuilder()).append("insertCounterMrs ").append(status).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("insertCounterMrs Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-CNTR-01", "MrsDAO", "insertCounterMrs", sqlException);
        }
        return status;
    }

    public int insertCounterBillMaster(MrsTO mrsTO) {
        Map map = new HashMap();
        int status = 0;
        int billNo = 0;
        ArrayList checkList = new ArrayList();
        String temp = "";
        String invoiceNo = "";
        try {
            //check this item exist
            //////System.out.println("In Mrs insertCounterMrsBillMaster DAO ");
            map.put("mrsId", mrsTO.getMrsId1());
            map.put("spareAmount", mrsTO.getSpareAmount());
            map.put("sparePercentage", mrsTO.getHike());
            map.put("tax", mrsTO.getTax());
            map.put("spareTaxAmount", mrsTO.getSpareTaxAmount());
            map.put("discount", mrsTO.getDiscount());
            map.put("discountAmount", mrsTO.getDiscountAmount());
            map.put("spareRetAmount", mrsTO.getSpareRetAmount());
            map.put("nettAmount", mrsTO.getNettAmount());
            map.put("remarks", mrsTO.getRemarks());
            map.put("invoiceType", "CASH");
            map.put("userId", mrsTO.getUserId());

            //////System.out.println("B4 Invoice");
            invoiceNo = (String) getSqlMapClientTemplate().queryForObject("mrs.getInvoiceNo", map);
            //////System.out.println("invoiceNo serial" + invoiceNo);
            //////System.out.println("A4 Invoice");
            if (invoiceNo == null) {
                invoiceNo = (String) getSqlMapClientTemplate().queryForObject("mrs.getFirstInvoiceNo", map);
                //////System.out.println("invoiceNo first" + invoiceNo);
            }

            map.put("invoiceNo", invoiceNo);
            if (invoiceNo != null) {
                billNo = (Integer) getSqlMapClientTemplate().insert("mrs.insertCounterMrsBillMaster", map);
            }
            System.out.println((new StringBuilder()).append("insertCounterMrsBillMaster BillNo").append(billNo).toString());





        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("insertCounterMrsBillMaster Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-CNTR-01", "MrsDAO", "insertCounterMrsBillMaster", sqlException);
        }
        return billNo;
    }

    public int insertCounterBillItems(ArrayList counterItems, int billNo, float counterBillAmount) {
        Map map = new HashMap();
        int status = 0;
        ArrayList checkList = new ArrayList();


        try {
            //check this item exist
            //////System.out.println("In Mrs insertCounterBillItems DAO ");
            map.put("billNo", billNo);
            Iterator itr = counterItems.iterator();
            MrsTO mrsTO = null;
            while (itr.hasNext()) {
                mrsTO = (MrsTO) itr.next();
                //////System.out.println("mrsTO.getIssuedItemsId()"+mrsTO.getIssuedItemsId());
                map.put("itemId", mrsTO.getIssuedItemsId());
                //////System.out.println("mrsTO.getQuantityRI()"+mrsTO.getQuantityRI());
                map.put("quantity", mrsTO.getQuantityRI());
                //////System.out.println("mrsTO.getTax()"+mrsTO.getTax());
                map.put("tax", mrsTO.getTax());
                //////System.out.println("mrsTO.getPrice()"+mrsTO.getPrice());
                map.put("price", Float.parseFloat(mrsTO.getPrice()));
                //////System.out.println("mrsTO.getNettAmount()"+mrsTO.getNettAmount());
                map.put("nettAmount", mrsTO.getNettAmount());
                //counterId = mrsTO.getCounterId();
                //////System.out.println("CounterId"+mrsTO.getCounterId());
                map.put("counterId", mrsTO.getCounterId());
                status += (Integer) getSqlMapClientTemplate().update("mrs.insertCounterBillItems", map);
            }
            System.out.println((new StringBuilder()).append("insertCounterBillItems ").append(status).append("Nett AMount is").append(counterBillAmount).toString());
            map.put("counterBillAmount", counterBillAmount);

            status = (Integer) getSqlMapClientTemplate().update("mrs.updateCounterMaster", map);
            //////System.out.println("Update Counter Master status"+status);
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("insertCounterBillItems Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-CNTR-01", "MrsDAO", "insertCounterBillItems", sqlException);
        }
        return billNo;
    }

    public ArrayList getCounterBillHeaderInfo(int counterId) {
        Map map = new HashMap();
        ArrayList billHeader = new ArrayList();
        try {
            map.put("counterId", Integer.valueOf(counterId));
            //////System.out.println("getCounterBillHeaderInfo-->"+counterId);
            billHeader = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getCounterBillHeaderInfo", map);
            System.out.println((new StringBuilder()).append("getCounterBillHeaderInfo size=").append(billHeader.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getCounterBillHeaderInfo Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "getCounterBillHeaderInfo", sqlException);
        }
        return billHeader;
    }

    public ArrayList getItemDetails(int compId, String mfrCode) {
        Map map = new HashMap();
        ArrayList tempList = new ArrayList();
        map.put("compId", compId);
        map.put("mfrCode", mfrCode);
        try {
            tempList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getItemDetails", map);
            System.out.println((new StringBuilder()).append("getItemDetails size=").append(tempList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getItemDetails Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "getItemDetails", sqlException);
        }
        return tempList;
    }

    public ArrayList getIssuedItemDetails(int compId, String jcno, String mfrCode) {
        Map map = new HashMap();
        ArrayList tempList = new ArrayList();
        map.put("mfrCode", mfrCode);
        map.put("jcno", jcno);
        map.put("compId", compId);
        try {
            tempList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getIssuedItemDetails", map);
            System.out.println((new StringBuilder()).append("getItemDetails size=").append(tempList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getItemDetails Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "getItemDetails", sqlException);
        }
        return tempList;
    }

    public ArrayList getCounterBillItems(int counterId) {
        Map map = new HashMap();
        ArrayList billItems = new ArrayList();
        try {
            //////System.out.println("getCounterBillItems-->"+counterId);
            map.put("counterId", Integer.valueOf(counterId));
            billItems = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getCounterBillItems", map);
            System.out.println((new StringBuilder()).append("getCounterBillItems size=").append(billItems.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getCounterBillItems Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "getCounterBillItems", sqlException);
        }
        return billItems;
    }

    public ArrayList getCounterMrsDetails(int mrsId) {
        Map map = new HashMap();
        ArrayList counterMrsDetails = new ArrayList();
        try {
            map.put("mrsId", Integer.valueOf(mrsId));
            counterMrsDetails = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getCounterMrsDetails", map);
            System.out.println((new StringBuilder()).append("counterMrsDetails size=").append(counterMrsDetails.size()).toString());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getCounterMrsDetails Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "getCounterMrsDetails", sqlException);
        }
        return counterMrsDetails;
    }
//
    //For Retrieve Non Contract Customer Details

    public ArrayList getCounterCustomer() {
        Map map = new HashMap();
        ArrayList counterCustomer = new ArrayList();
        try {
            counterCustomer = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getCounterCustomer", map);
            System.out.println((new StringBuilder()).append("counterCustomer size=").append(counterCustomer.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("counterCustomer Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-MRS-01", "MrsDAO", "counterCustomer", sqlException);
        }
        return counterCustomer;
    }

    public ArrayList getMFRcode(MrsTO mrsTO) {
        Map map = new HashMap();
        ArrayList mfrCodeList = new ArrayList();
        map.put("mfrCode", "%" + mrsTO.getMfrCode() + "%");
        System.out.println("getMFRcode" + map);
        try {
            if (getSqlMapClientTemplate().queryForList("mrs.mfrCodeList", map) != null) {
                mfrCodeList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.mfrCodeList", map);
                System.out.println("mfrCodeList" + mfrCodeList.size());
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleNoTripSheet Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleNoTripSheet", ne);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleNoTripSheet Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleNoTripSheet", sqlException);
        }
        return mfrCodeList;
    }

    public ArrayList getActiveCode(String acode, MrsTO mrsTO) {
        Map map = new HashMap();
        ArrayList activityCodeList = new ArrayList();
        map.put("aCode", "%" + acode + "%");
        System.out.println("getActiveCode" + map);
        try {
            if (getSqlMapClientTemplate().queryForList("mrs.getActiveCodeList", map) != null) {
                activityCodeList = (ArrayList) getSqlMapClientTemplate().queryForList("mrs.getActiveCodeList", map);
                System.out.println("activityCodeList" + activityCodeList.size());
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleNoTripSheet Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleNoTripSheet", ne);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleNoTripSheet Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleNoTripSheet", sqlException);
        }
        return activityCodeList;
    }
    private final int errorStatus = 4;
    private static final String CLASS = "MrsDAO";
}
