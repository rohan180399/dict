<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="ets.domain.operation.business.OperationTO" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->
<script language="JavaScript" src="FusionCharts.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/prettify.js"></script>
<script type="text/javascript" src="js/json2.js"></script>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
</script>

<script language="javascript">

    function show_src() {
        document.getElementById('exp_table').style.display = 'none';
    }
    function show_exp() {
        document.getElementById('exp_table').style.display = 'block';
    }
    function show_close() {
        document.getElementById('exp_table').style.display = 'none';
    }


    function toexcel() {

        document.stockWorthReport.action = '/throttle/handleStockWorthExcelRep.do';
        document.stockWorthReport.submit();
    }

    var httpReq;
    var temp = "";
    function ajaxData()
    {
        if (document.stockWorthReport.mfrId.value != '0') {
            var url = "/throttle/getModels1.do?mfrId=" + document.stockWorthReport.mfrId.value;
            if (window.ActiveXObject)
            {
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest)
            {
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function() {
                processAjax();
            };
            httpReq.send(null);
        }
    }
    function processAjax()
    {
        if (httpReq.readyState == 4)
        {
            if (httpReq.status == 200)
            {
                temp = httpReq.responseText.valueOf();
                setOptions(temp, document.stockWorthReport.modelId);

                if (('<%= request.getAttribute("modelId")%>') != 'null') {
                    document.stockWorthReport.modelId.value = <%=request.getAttribute("modelId")%>;
                }
            }
            else
            {
                alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
            }
        }
    }
    function submitPage() {
        var chek = validation();
        if (chek == 'true') {

            document.stockWorthReport.action = '/throttle/stockWorthReport.do';
            document.stockWorthReport.submit();
        }
    }
    function validation() {
        if (document.stockWorthReport.companyId.value == 0) {
            alert("Please Select Service Point");
            document.stockWorthReport.companyId.focus();
            return 'false';
        }
        else if (textValidation(document.stockWorthReport.fromDate, 'From Date')) {
            return 'false';
        }
        return 'true';
    }
    function setValues() {

        if ('<%=request.getAttribute("companyId")%>' != 'null') {
            document.stockWorthReport.companyId.value = '<%=request.getAttribute("companyId")%>';
            document.stockWorthReport.fromDate.value = '<%=request.getAttribute("fromDate")%>';
        }
        if ('<%=request.getAttribute("sectionId")%>' != 'null') {
            document.stockWorthReport.sectionId.value = '<%=request.getAttribute("sectionId")%>';
        }
        if ('<%=request.getAttribute("mfrId")%>' != 'null') {
            document.stockWorthReport.mfrId.value = '<%=request.getAttribute("mfrId")%>';
        }
        if ('<%=request.getAttribute("modelId")%>' != 'null') {
            document.stockWorthReport.modelId.value = '<%=request.getAttribute("modelId")%>';
            ajaxData();
        }
        if ('<%=request.getAttribute("mfrCode")%>' != 'null') {
            document.stockWorthReport.mfrCode.value = '<%=request.getAttribute("mfrCode")%>';
        }
        if ('<%=request.getAttribute("paplCode")%>' != 'null') {
            document.stockWorthReport.paplCode.value = '<%=request.getAttribute("paplCode")%>';
        }
        if ('<%=request.getAttribute("itemName")%>' != 'null') {
            document.stockWorthReport.itemName.value = '<%=request.getAttribute("itemName")%>';
        }

    }
    function getItemNames() {
        var oTextbox = new AutoSuggestControl(document.getElementById("itemName"), new ListSuggestions("itemName", "/throttle/handleItemSuggestions.do?"));

    }

    function rateDetails(indx) {
        var itemName = document.getElementsByName("itemNam");
        var processId = document.getElementsByName("processId");
        var url = '/throttle/rateDetails.do?processId=' + processId[indx].value + '&itemName=' + itemName[indx].value;
        window.open(url, 'PopupPage', 'height=450,width=650,scrollbars=yes,resizable=yes');
    }


    function print()
    {
        var DocumentContainer = document.getElementById('print');
        var WindowObject = window.open('', "TrackHistoryData",
                "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        //WindowObject.close();
    }



</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="fmsstores.label.StockValueReport" text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="fmsstores.label.FMSStores" text="default text"/></a></li>
            <li class="active"><spring:message code="fmsstores.label.StockValueReport" text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="setValues();
                    getItemNames();" >
                <form name="stockWorthReport" method="post" >
                    <%--<%@ include file="/content/common/path.jsp" %>--%>                        
                    <%@ include file="/content/common/message.jsp" %>



                    <!--<table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                        <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                        <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                        </tr>
                        <tr id="exp_table" >
                        <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                            <div class="tabs" align="left" style="width:850;">
                        <ul class="tabNavigation">
                                <li style="background:#76b3f1">Stock Value Report</li>
                        </ul>
                        <div id="first">-->
                    <table class="table table-info mb30 table-hover">
                        <thead><tr><th colspan="4"><spring:message code="fmsstores.label.StockValueReport"  text="default text"/></th></tr></thead>
                        <tr>
                            <td height="30"><spring:message code="fmsstores.label.Category"  text="default text"/></td>
                            <td height="30">
                                <select name="sectionId" class="form-control" style="width:260px;height:40px;">
                                    <option value="0">-<spring:message code="fmsstores.label.Select"  text="default text"/>-</option>
                                    <c:if test = "${SectionList != null}" >
                                        <c:forEach items="${SectionList}" var="section">
                                            <option value='<c:out value="${section.categoryId}"/>'> <c:out value="${section.categoryName}"/> </option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                            <td height="30"><spring:message code="fmsstores.label.MFR"  text="default text"/></td>
                            <td height="30">
                                <select name="mfrId" onchange="ajaxData();" class="form-control" style="width:260px;height:40px;">
                                    <option value="0">-<spring:message code="fmsstores.label.Select"  text="default text"/>-</option>
                                    <c:if test = "${MfrLists != null}" >
                                        <c:forEach items="${MfrLists}" var="mfr">
                                            <option value="<c:out value="${mfr.mfrId}"/>"><c:out value="${mfr.mfrName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td height="30"><spring:message code="fmsstores.label.Model"  text="default text"/></td>
                            <td height="30"><select name="modelId" class="form-control" style="width:260px;height:40px;">
                                    <option value="0">-<spring:message code="fmsstores.label.Select"  text="default text"/>-</option>
                                    <c:if test = "${ModelLists != null}" >
                                        <c:forEach items="${ModelLists}" var="test">
                                            <option value="<c:out value="${test.modelId}" />"><c:out value="${test.modelName}" /></option>
                                        </c:forEach>
                                    </c:if>

                                </select>
                            </td>

                            <td height="30"><spring:message code="fmsstores.label.Location"  text="default text"/></td>
                            <td height="30">
                                <select  class="form-control" style="width:260px;height:40px;" name="companyId" >
                                    <c:if test = "${companyId == 1011}" >
                                        <option value="0">---<spring:message code="fmsstores.label.Select"  text="default text"/>---</option>
                                    </c:if>
                                    <c:if test = "${LocationLists != null}" >
                                        <c:forEach items="${LocationLists}" var="company">
                                            <c:if test = "${company.companyTypeId == 1012}" >
                                                <c:if test = "${company.cmpId== companyId && companyId != 1011}" >
                                                    <option selected value="<c:out value="${company.cmpId}"/>"><c:out value="${company.name}"/></option>
                                                </c:if>
                                                <c:if test = "${companyId==1011}" >
                                                    <option value="<c:out value="${company.cmpId}"/>"><c:out value="${company.name}"/></option>
                                                </c:if>
                                            </c:if>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td height="30"><spring:message code="fmsstores.label.ItemName"  text="default text"/></td>
                            <td height="30"><input name="itemName" id="itemName" type="text"  class="form-control" style="width:260px;height:40px;" value=""> </td>
                            <td height="30"><spring:message code="fmsstores.label.MfrItemCode"  text="default text"/></td>
                            <td height="30"><input name="mfrCode" type="text" class="form-control" style="width:260px;height:40px;" value="" size="20"> </td>        
                        </tr>
                        <tr>
                            <td height="30"><spring:message code="fmsstores.label.PAPLCode"  text="default text"/></td>
                            <td height="30"><input name="paplCode" type="text" class="form-control" style="width:260px;height:40px;" value="" size="20"></td>
                            <td height="30"><font color="red">*</font><spring:message code="fmsstores.label.Date"  text="default text"/></td>
                            <td height="30"><input name="fromDate" type="text"  autocomplete="off" class="datepicker form-control" style="width:260px;height:40px;" value="" size="20">
                                <!--<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.stockWorthReport.fromDate,'dd-mm-yyyy',this)"/>-->
                            </td>
                        </tr>
                        <tr>
                            <td height="30"><spring:message code="fmsstores.label.ItemType"  text="default text"/></td>
                            <td height="30"><select class="form-control" style="width:260px;height:40px;" name="itemType">
                                    <option value="NEW"><spring:message code="fmsstores.label.New"  text="default text"/></option>
                                    <option value="RC"><spring:message code="fmsstores.label.RC"  text="default text"/></option>
                                </select>
                            </td>
                            <td></td>
                            <td ><input type="button"  value="<spring:message code="fmsstores.label.Search"  text="default text"/>" class="btn btn-success" name="Search" onClick="submitPage()"><input type="hidden"  name="reqfor"></td>

                        </tr>
                    </table>
                    <!--    </div></div>
                        </td>
                        </tr>
                        </table>-->

                    <c:set var="totalValue" value="0"/>
                    <c:set var="rcValue" value="0"/>
                    <c:set var="newValue" value="0"/>
                    <c:if test = "${stockWorthList != null}" >
                        <div id="print">
                            <table class="table table-info mb30 table-hover" id="bg" >
                                <thead>
                                <tr>
                                    <th  height="30" colspan="12"><spring:message code="fmsstores.label.StockWorthReport"  text="default text"/> </th>        
                                </tr>
                                <tr>
                                    <th width="30" ><spring:message code="fmsstores.label.Sno"  text="default text"/></th>
                                    <!-- <td width="102" height="30" >Manufacturer</td> -->
                                    <!-- <td width="82" >Model</td> -->
                                    <th width="80" height="30" ><spring:message code="fmsstores.label.MfrItemCode"  text="default text"/> </th>
                                    <th width="82" ><spring:message code="fmsstores.label.PAPLCode"  text="default text"/> </th>
                                    <th width="142" height="30" ><spring:message code="fmsstores.label.ItemName"  text="default text"/></th>
                                    <th width="80" height="30" ><spring:message code="fmsstores.label.Category"  text="default text"/></th>
                                    <th width="80" height="30" ><spring:message code="fmsstores.label.Group"  text="default text"/></th>
                                    <th width="71" height="30" ><spring:message code="fmsstores.label.ItemType"  text="default text"/></th>
                                    <th width="70" height="30" ><spring:message code="fmsstores.label.PRICE"  text="default text"/></th>
                                    <th width="70" height="30" ><spring:message code="fmsstores.label.Qty"  text="default text"/></th>    
                                    <th width="70" height="30" ><spring:message code="fmsstores.label.StockValue(SAR)"  text="default text"/> </th>
                                </tr>
                                </thead>
                                <% int index = 0;%> 
                                <c:forEach items="${stockWorthList}" var="stockWorth"> 
                                    <%
                        String classText = "";
                        int oddEven = index % 2;
                        if (oddEven > 0) {
                            classText = "text2";
                        } else {
                            classText = "text1";
                        }
                                    %>	
                                    <tr>
                                        <td ><%=index+1%></td>
                                        <!--<td  height="30"><c:out value="${stockWorth.mfrName}" /></td> -->
                                        <!-- <td ><c:out value="${stockWorth.modelName}" /></td> -->
                                    <input type="hidden" name="sno" value=<%=index+1%>>
                                    <input type="hidden" name="paplcodes" value='<c:out value="${stockWorth.paplCode}" />'>
                                    <input type="hidden" name="itemNames" value='<c:out value="${stockWorth.itemName}" />'>    
                                    <input type="hidden" name="categoryNames" value='<c:out value="${stockWorth.categoryName}" />'>
                                    <input type="hidden" name="groupNames" value='<c:out value="${stockWorth.groupName}" />'>
                                    <input type="hidden" name="itemTypes" value='<c:out value="${stockWorth.itemTypes}" />'>
                                    <input type="hidden" name="itemPrice" value='<c:out value="${stockWorth.itemPrice}" />'>
                                    <input type="hidden" name="totalQty" value='<c:out value="${stockWorth.totalQty}" />'>
                                    <input type="hidden" name="stworth" value='<c:out value="${stockWorth.stworth}" />'>

                                    <td  height="30"><c:out value="${stockWorth.mfrCode}" /></td> 
                                    <td ><c:out value="${stockWorth.paplCode}" /></td>
                                    <td  height="30"><c:out value="${stockWorth.itemName}" /> </td>
                                    <td  height="30"><c:out value="${stockWorth.categoryName}" /></td>
                                    <td  height="30"><c:out value="${stockWorth.itemTypes}" /></td>
                                    <td  height="30"><c:out value="${stockWorth.itemPrice}" /></td>
                                    <td  height="30"><c:out value="${stockWorth.totalQty}" /></td>

                                    <td  align="right" height="30"><c:out value="${stockWorth.stworth}" /><td>
                                        <c:set var="totalValue" value="${stockWorth.stworth + totalValue}" />
                                        <c:if test = "${stockWorth.itemTypes =='New'}" >
                                            <c:set var="newValue" value="${stockWorth.stworth + newValue}" />
                                        </c:if>
                                        <c:if test = "${stockWorth.itemTypes =='RC'}" >
                                            <c:set var="rcValue" value="${stockWorth.stworth + rcValue}" />
                                        </c:if>
                                        <input type="hidden" name="processId" value='<c:out value="${stockWorth.processId}" />'>

                                        </tr>
                                        <% index++;%>
                                    </c:forEach> 


                                <tr>
                                    <!--<td >&nbsp;</td>
                                    <td  height="30">&nbsp;</td> -->
                                    <td >&nbsp;</td>
                                    <td  height="30">&nbsp;</td>
                                    <td >&nbsp;</td>
                                    <td  height="30"><b><spring:message code="fmsstores.label.NewStockWorth"  text="default text"/></b> </td>
                                    <td  height="30">
                                        <fmt:setLocale value="en_US" /><b><spring:message code="fmsstores.label.SAR"  text="default text"/>:<fmt:formatNumber value="${newValue}" pattern="##.00"/></b></td>
                                    <td  height="30">&nbsp;</td>
                                    <td  height="30">&nbsp;</td>
                                    <td  height="30">&nbsp;</td>

                                </tr>
                                <tr>
                                    <!--<td >&nbsp;</td>
                                    <td  height="30">&nbsp;</td> -->
                                    <td >&nbsp;</td>
                                    <td  height="30">&nbsp;</td>
                                    <td >&nbsp;</td>
                                    <td  height="30"><b><spring:message code="fmsstores.label.RCStockWorth"  text="default text"/></b> </td>
                                    <td  height="30">
                                        <fmt:setLocale value="en_US" /><b><spring:message code="fmsstores.label.SAR"  text="default text"/>:<fmt:formatNumber value="${rcValue}" pattern="##.00"/></b></td>
                                    <td  height="30">&nbsp;</td>
                                    <td  height="30">&nbsp;</td>
                                    <td  height="30">&nbsp;</td>

                                </tr>
                                <tr>
                                    <!--<td >&nbsp;</td>
                                    <td  height="30">&nbsp;</td> -->
                                    <td >&nbsp;</td>
                                    <td  height="30">&nbsp;</td>
                                    <td >&nbsp;</td>
                                    <td  height="30"><b><spring:message code="fmsstores.label.TotalStockWorth"  text="default text"/></b> </td>
                                    <td  height="30">
                                        <fmt:setLocale value="en_US" /><b><spring:message code="fmsstores.label.SAR"  text="default text"/>:<fmt:formatNumber value="${totalValue}" pattern="##.00"/></b></td> 
                                    <td  height="30">&nbsp;</td>
                                    <td  height="30">&nbsp;</td>
                                    <td  height="30">&nbsp;</td>

                                </tr>
                            </table>
                            <style>
                                .text1 {
                                    font-family:Tahoma;
                                    font-size:12px;
                                    color:#333333;
                                    background-color:#E9FBFF;
                                    padding-left:10px;
                                }
                                .text2 {
                                    font-family:Tahoma;
                                    font-size:12px;
                                    color:#333333;
                                    background-color:#FFFFFF;
                                    padding-left:10px;
                                }        
                                .contentsub {
                                    background-image:url(/throttle/images/button.gif);
                                    background-repeat:repeat-x;
                                    height:15px;
                                    font-family:Tahoma;
                                    padding-left:5px;
                                    font-size:11px;
                                    font-weight:bold;
                                    color:#0070D4;
                                    text-align:left;
                                    padding-bottom:8px;
                                }

                                .contenthead {
                                    background-image:url(/throttle/images/button.gif);
                                    background-repeat:repeat-x;
                                    height:15px;
                                    font-family:Tahoma;
                                    padding-left:5px;
                                    font-size:11px;
                                    font-weight:bold;
                                    color:#0070D4;
                                    text-align:center;
                                    padding-bottom:8px;
                                }

                            </style>    
                        </div> 
                        <br>
                        <center> <input class="btn btn-success" type="button" value="<spring:message code="fmsstores.label.Print"  text="default text"/>" onClick="print();">    
                            <input class="btn btn-success" type="button" value="<spring:message code="fmsstores.label.ExportToExcel"  text="default text"/>" onClick="toexcel();">  
                        </center>

                    </c:if>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
