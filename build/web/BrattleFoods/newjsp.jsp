<%--
    Document   : Cnoteentry
    Created on : Oct 29, 2013, 10:15:41 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
      <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Trip Sheet</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true
                });
            });

            $(function() {
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });
        </script>
    <body>
        <form name="tripSheet" action="saveTripSheet.do" method="post" onsubmit="return validateSubmit();" >

            <div style="padding-left: 60px;">
                <center><h2>CONSIGNMENT NOTE</h2></center>
                <table cellpadding="0" cellspacing="0" style="width: 90%;">
                    <tr>
                        <td colspan="5" class="contenthead">Consignment Note Entry</td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <table name="mainTBL" class="TableMain" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                  <td class="texttitle2">
                                    Entry Option

                                      <input type="checkbox" name="vehicle" value="Bike">Manual<br>
                                    </td>
                                     <td class="texttitle2">

                                         <input type="checkbox" name="vehicle" value="Bike">Import<br>
                                     </td>

                                    <td class="texttitle2">&nbsp;</td>
                                    <td class="texttitle2">

                                    </td>
                                   <td class="text2">

                                    </td>
                                </tr>
                                <tr>
                                    <td class="texttitle1">
                                       C Note
                                    </td>
                                    <td class="text1">
                                        <c:set var="routeData" value="" />
                                        <select name="routeId" id="routeId" class="form-control" style="width:120px;">
                                            <option value=""> -Select- </option>

                                        </select>
                                        <input type="hidden" name="routeDetails" id="routeDetails" value='<c:out value="${routeData}" />' />
                                    </td>
                                    <td class="texttitle1">  </td>
                                    <td class="texttitle1">
                                     C Date

                                    </td>
                                    <td class="text2">
                                        <input name="tripDate" type="text" class="datepicker"  readonly="readonly" id="tripDate"  />
                                    </td>

                                    <td class="text1">
                                    </td>
                                </tr>

                                <tr>
                                    <td class="texttitle2">
                                      Customer Type
                                    </td>
                                      <td class="text1">
                                        <select name="vehicleId" onchange="fillData()" id="vehicleId" class="form-control" style="width:120px;">
                                            <option value=""> -Select- </option>

                                        </select>
                                    </td>
                                    <td class="text2">&nbsp;</td>
                                    <td class="texttitle2">Product</td>
                                   <td class="text1">
                                        <select name="vehicleId" onchange="fillData()" id="vehicleId" class="form-control" style="width:120px;">
                                            <option value=""> -Select- </option>

                                        </select>
                                    </td>
                                    <td class="texttitle2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="texttitle1">

                                    </td>
                                    <td class="text1">

                                    </td>
                                    <td class="texttitle1">&nbsp;</td>
                                    <td class="texttitle1">
                                        Type of Movement
                                    </td>
                                    <td class="text1">
                                                 <select name="vehicleId" onchange="fillData()" id="vehicleId" class="form-control" style="width:120px;">
                                            <option value=""> -Select- </option>

                                        </select>
                                    </td>
                                    <td class="text1">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="texttitle2">
                                     New Customer
                                    </td>
                                    <td class="text2">

                                    </td>
                                    <td class="texttitle2">&nbsp;</td>
                                    <td class="texttitle2">

                                    </td>
                                    <td class="text2">

                                    </td>
                                    <td class="texttitle1">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="texttitle1">
                                      Name
                                    </td>
                                    <td class="texttitle1">
                                           <input name="routeKm" type="text" readonly class="form-control"  id="routeKm" />
                                    </td>
                                    <td class="texttitle1">&nbsp;</td>
                                    <td class="texttitle1">

                                    </td>
                                    <td class="texttitle1">

                                    </td>
                                </tr>
                                <tr>
                                    <td class="texttitle2">
                                        Address
                                    </td>
                                    <td class="texttitle2">
                                        <input name="driverBata" type="text" readonly class="form-control"  id="driverBata" />
                                    </td>
                                    <td class="texttitle2">&nbsp;</td>
                                    <td class="texttitle2">

                                    </td>
                                    <td class="texttitle2">

                                    </td>
                                </tr>
                                <tr>
                                    <td class="texttitle1">
                                      City - Pincode
                                    </td>
                                    <td class="texttitle1">
                                        <input name="tonnage" type="text" onChange="callAjax();"  class="form-control"  id="tonnage" />
                                    </td>
                                    <td class="texttitle1">&nbsp;</td>
                                    <td class="texttitle1">

                                    </td>
                                    <td class="texttitle1">

                                    </td>
                                </tr>
                                <tr>
                                    <td class="texttitle2">
                                        Mobile No
                                    </td>
                                   <td class="texttitle1">
                                        <input name="tonnage" type="text" onChange="callAjax();"  class="form-control"  id="tonnage" />
                                    </td>
                                    <td class="texttitle2">&nbsp;</td>
                                    <td class="texttitle2">

                                    </td>
                                    <td class="texttitle2">

                                    </td>
                                </tr>
                                <tr>
                                    <td class="texttitle1">
                                       E-Mail ID
                                    </td>
                                    <td class="texttitle1">
                                        <input name="tonnage" type="text" onChange="callAjax();"  class="form-control"  id="tonnage" />
                                    </td>
                                    <td class="texttitle1">&nbsp;</td>
                                    <td class="texttitle1">
                                        Product
                                    </td>
                                    <td class="texttitle2">
                                        <select name="vehicleOutLoc" id="vehicleOutLoc" class="form-control" style="width:120px;">
                                            <option value="">--Select--</option>

                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="texttitle2">
                                        Phone No
                                    </td>
                                    <td class="texttitle2">
                                        <input name="vehicleOutDate" type="text"  class="form-control" maxlength="10"   id="vehicleOutDate" />
                                    </td>
                                    <td class="texttitle2">&nbsp;</td>
                                    <td class="texttitle2">
                                      Destination
                                    </td>
                                    <td class="texttitle2">
                                        <input type="text" class="form-control" id="" name="" >
                                    </td>

                                </tr>
                                <tr>
                                    <td class="texttitle1">
                                      Payment Type
                                    </td>
                                    <td class="texttitle1">
                                        <select name="tripPurpose" class="form-control"  id="tripPurpose" style="width:120px;">
                                           <option value=""> -Select- </option>
                                        </select>
                                    </td>
                                    <td class="texttitle1">&nbsp;</td>
                                    <td class="texttitle1">
                                       Service Type
                                    </td>
                                    <td class="texttitle1">
                                       <select name="tripPurpose" class="form-control"  id="tripPurpose" style="width:120px;">
                                           <option value=""> -Select- </option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="texttitle1">

                                    </td>
                                    <td class="texttitle1">

                                    </td>
                                    <td class="texttitle1">&nbsp;</td>
                                    <td class="texttitle1">

                                    </td>
                                    <td class="texttitle1">

                                    </td>
                                </tr>
                                <tr>
                                    <td class="texttitle1">
                                    Pickup/Delivery
                                    </td>
                                    <td class="texttitle1">
                                        <select name="tripPurpose" class="form-control"  id="tripPurpose" style="width:120px;">
                                           <option value=""> -Select- </option>
                                        </select>
                                    </td>
                                    <td class="texttitle1">&nbsp;</td>
                                    <td class="texttitle1">
                                      Packaging Type
                                    </td>
                                    <td class="texttitle1">
                                       <select name="tripPurpose" class="form-control"  id="tripPurpose" style="width:120px;">
                                           <option value=""> -Select- </option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="texttitle1">
                                      From City
                                    </td>
                                    <td class="texttitle1">
                                        <input type="text" class="form-control" id=" " name="">
                                    </td>
                                    <td class="texttitle1">&nbsp;</td>
                                    <td class="texttitle1">
                                    To City
                                    </td>
                                    <td class="texttitle1">
                                        <input type="text" class="form-control" name="" id="">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="texttitle1">
                                     Contents
                                    </td>
                                    <td class="texttitle1">
                                        <select name="tripPurpose" class="form-control"  id="tripPurpose" style="width:120px;">
                                           <option value=""> -Select- </option>
                                        </select>
                                    </td>
                                    <td class="texttitle1">&nbsp;</td>
                                    <td class="texttitle1">
                                    Special Instruction
                                    </td>
                                    <td class="texttitle1">
                                        <input type="text" class="form-control" name="" id="">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="texttitle1">
                                      Volumetric
                                    </td>
                                    <td class="texttitle1">
                                        <input type="checkbox" class="form-control" id=" " name="">
                                    </td>
                                    <td class="texttitle1">&nbsp;</td>
                                    <td class="texttitle1">
                                   ODA
                                    </td>
                                    <td class="texttitle1">
                                        <input type="checkbox" class="form-control" name="" id="">
                                    </td>
                                </tr>
                                <tr>
                                  <td class="texttitle1">
                                    Local Cnote
                                    </td>
                                    <td class="texttitle1">
                                        <input type="checkbox" class="form-control" name="" id="">
                                    </td>
                                       <td class="texttitle1">&nbsp;</td>
                                  <td class="texttitle1">
                                   Source Note
                                    </td>
                                    <td class="texttitle1">
                                        <input type="checkbox" class="form-control" name="" id="">
                                    </td>
                                </tr>
                                <tr>
                                  <td class="texttitle1">
                                   Multi Pickup
                                    </td>
                                    <td class="texttitle1">
                                        <input type="checkbox" class="form-control" name="" id="">
                                    </td>
                                       <td class="texttitle1">&nbsp;</td>
                                  <td class="texttitle1">
                                   Multi Delivery
                                    </td>
                                    <td class="texttitle1">
                                        <input type="checkbox" class="form-control" name="" id="">
                                    </td>
                                </tr>
                                <tr>
                                  <td class="texttitle1">
                                  Business Type
                                    </td>
                                    <td class="texttitle1">
                                            <select name="tripPurpose" class="form-control"  id="tripPurpose" style="width:120px;">
                                           <option value=""> -Select- </option>
                                        </select>
                                    </td>
                                       <td class="texttitle1">&nbsp;</td>
                                  <td class="texttitle1">

                                    </td>
                                    <td class="texttitle1">

                                    </td>
                                </tr>
                                <tr>
                                  <td></td>
                                    <td></td>
                                   <td >&nbsp;</td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td></td>
                                    <td></td>
                                   <td >&nbsp;</td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                        <td colspan="2" class="contenthead">
                         Consignor Details
                        </td>
                        <td colspan="3" class="contenthead">
                            Consignee Details
                        </td>

                    </tr>
                    <tr>
                                    <td class="texttitle2">
                                       Consignor Name & Code
                                    </td>
                                    <td class="texttitle2">
                                            <select name="tripPurpose" class="form-control"  id="tripPurpose" style="width:120px;">
                                           <option value=""> -Select- </option>
                                        </select>
                                        <input type="text" class="form-control" id="" name="" >
                                    </td>
                                    <td class="texttitle2">&nbsp;</td>
                                    <td class="texttitle2">
                                    Packaging Type
                                    </td>
                                    <td class="texttitle2">
                                           <select name="tripPurpose" class="form-control"  id="tripPurpose" style="width:120px;">
                                           <option value=""> -Select- </option>
                                        </select>
                                        <input type="text" class="form-control" id="" name="" >
                                    </td>

                                </tr><tr>
                                    <td class="texttitle2">
                                      Address
                                    </td>
                                    <td class="texttitle2">
                                        <input name="vehicleOutDate" type="text"  class="form-control" maxlength="10"   id="vehicleOutDate" />
                                    </td>
                                    <td class="texttitle2">&nbsp;</td>
                                    <td class="texttitle2">
                                   Address
                                    </td>
                                    <td class="texttitle2">
                                        <input type="text" class="form-control" id="" name="" >
                                    </td>

                                </tr>
                                <tr>
                                    <td class="texttitle3" style="border-bottom:1px  #000   solid">
                                      New/Walkin
                                    </td>
                                    <td class="texttitle3"  style="border-bottom:1px #000 solid">
                                    </td>
                                    <td class="texttitle3"  style="border-bottom:1px #000 solid">&nbsp;</td>
                                    <td class="texttitle3"  style="border-bottom:1px #000 solid">
                                      New/Walkin
                                    </td>
                                    <td class="texttitle3"  style="border-bottom:1px #000 solid">

                                    </td>

                                </tr>
                                <tr>
                                    <td class="texttitle2">
                                       Name
                                    </td>
                                    <td class="texttitle2">
                                        <input name="vehicleOutDate" type="text"  class="form-control" maxlength="10"   id="vehicleOutDate" />
                                    </td>
                                    <td class="texttitle2">&nbsp;</td>
                                    <td class="texttitle2">
                                      Name
                                    </td>
                                    <td class="texttitle2">
                                        <input type="text" class="form-control" id="" name="" >
                                    </td>

                                </tr>
                                </tr><tr>
                                    <td class="texttitle2">
                                       Address
                                    </td>
                                    <td class="texttitle2">
                                        <input name="vehicleOutDate" type="text"  class="form-control" maxlength="10"   id="vehicleOutDate" />
                                    </td>
                                    <td class="texttitle2">&nbsp;</td>
                                    <td class="texttitle2">
                                     Address
                                    </td>
                                    <td class="texttitle2">
                                        <input type="text" class="form-control" id="" name="" >
                                    </td>

                                </tr>

                                <tr>
                                    <td class="texttitle2">
                                    City - Pincode
                                    </td>
                                    <td class="texttitle2">
                                        <input name="vehicleOutDate" type="text"  class="form-control" maxlength="10"   id="vehicleOutDate" />
                                    </td>
                                    <td class="texttitle2">&nbsp;</td>
                                    <td class="texttitle2">
                                    City - Pincode
                                    </td>
                                    <td class="texttitle2">
                                        <input type="text" class="form-control" id="" name="" >
                                    </td>

                                </tr>
                                <tr>
                                    <td class="texttitle2">
                                       Mobile No
                                    </td>
                                    <td class="texttitle2">
                                        <input name="vehicleOutDate" type="text"  class="form-control" maxlength="10"   id="vehicleOutDate" />
                                    </td>
                                    <td class="texttitle2">&nbsp;</td>
                                    <td class="texttitle2">
                                   Mobile No
                                    </td>
                                    <td class="texttitle2">
                                        <input type="text" class="form-control" id="" name="" >
                                    </td>

                                </tr>
                                <tr>
                                    <td class="texttitle2">
                                    E-Mail ID
                                    </td>
                                    <td class="texttitle2">
                                        <input name="vehicleOutDate" type="text"  class="form-control" maxlength="10"   id="vehicleOutDate" />
                                    </td>
                                    <td class="texttitle2">&nbsp;</td>
                                    <td class="texttitle2">
                                 E-Mail ID
                                    </td>
                                    <td class="texttitle2">
                                        <input type="text" class="form-control" id="" name="" >
                                    </td>

                                </tr>
                                <tr>
                                    <td class="texttitle2">
                                     Phone No
                                    </td>
                                    <td class="texttitle2">
                                        <input name="vehicleOutDate" type="text"  class="form-control" maxlength="10"   id="vehicleOutDate" />
                                    </td>
                                    <td class="texttitle2">&nbsp;</td>
                                    <td class="texttitle2">
                                Phone No
                                    </td>
                                    <td class="texttitle2">
                                        <input type="text" class="form-control" id="" name="" >
                                    </td>

                                </tr>
                                <tr>
                                    <td class="texttitle2">
                                     Customer Ref. No.
                                    </td>
                                    <td class="texttitle2">
                                        <input name="vehicleOutDate" type="text"  class="form-control" maxlength="10"   id="vehicleOutDate" />
                                    </td>
                                    <td class="texttitle2">&nbsp;</td>
                                    <td class="texttitle2">
                                 TP No.
                                    </td>
                                    <td class="texttitle2">
                                        <input type="text" class="form-control" id="" name="" >
                                    </td>

                                </tr>
                                <tr>
                                    <td class="texttitle2">
                                     Risk Type
                                     &nbsp;&nbsp;&nbsp;&nbsp;
                                      <input type="checkbox" name="vehicle" value="Bike">Carrier's Risk<br>
                                    </td>
                                    <td class="texttitle2">
                                        <input type="checkbox" name="vehicle" value="Bike">Owner's Risk<br>
                                    </td>

                                    <td class="texttitle2">&nbsp;</td>
                                    <td class="texttitle2">
                              Policy No & Date
                                    </td>
                                    <td class="texttitle2">
                                        <input type="text" class="form-control" id="" name="" >
                                        <input name="tripDate" type="text" class="datepicker"  readonly="readonly" id="tripDate"  />
                                    </td>

                                </tr>







                                <tr style="height: 10px;">
                                    <td align="right" class="TableColumn" colspan="6">&nbsp;</td>
                                </tr>
                                <tr id="ctl00_ContentPlaceHolder1_TripSheetRow" align="center">
                                    <td  colspan="6" rowspan="1">
                                        <h2>Invoice Details</h2>
                                    </td>
                                </tr>
                                <tr id="ctl00_ContentPlaceHolder1_Tr2">
                                    <td align="center" class="TableColumn" colspan="6" rowspan="1" style="height: 0%">
                                        <div>
                                            <table class="border" style="width: 100%; left: 30px;" cellpadding="0" cellspacing="0" rules="all" id="allowanceTBL" name="allowanceTBL" style="width:850px;border-collapse:collapse;">
                                                <tr style="width:50px;">

                                                    <th width="50" class="contenthead">S.No.&nbsp;</th>
                                                    <th class="contenthead">Invoice No.</th>
                                                    <th class="contenthead">Invoice Date</th>
                                                    <th class="contenthead">Batch No.</th>
                                                    <th class="contenthead">Declared Value</th>
                                                    <th class="contenthead">No.of Pkgs</th>
                                                    <th class="contenthead">Actual Weight</th>
                                                </tr>
                                                <tr>
                                                    <td>1</td>
                                                    <td>
                                                        <input type="text" id="" class="form-control" name="" >


                                                    </td>
                                                    <td>
                                                        <input name="" type="text" class="datepicker"  id="tripAllowanceDate"  />
                                                    </td>
                                                    <td>
                                                        <input name="" type="text" class="form-control"   id=""   />

                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" id="" name=""/>
                                                    </td>
                                                    <td>
                                                        <input  type="text" class="form-control" id="" name="" />
                                                    </td>
                                                    <td>
                                                        <input  type="text" class="form-control" id="" name="" />
                                                    </td>

                                                </tr>
                                            </table>
                                            <br><br>
                                           <table  style="width: 100%; left: 30px;" cellpadding="0" cellspacing="0"  style="width:850px;border-collapse:collapse;">
                                <tr>
                                    <td class="TableColumn">&nbsp;</td>
                                    <td class="TableColumn">&nbsp;</td>
                                    <td class="TableRowNew">&nbsp;</td>
                                    <td colspan="5" align="center" class="TableColumn">
                                     <input type="button" name="add row" value="Add Row"  id="add row" class="button" />
                                    </td>

                                    <td  class="TableColumn">
                                             Charged Weight
                                    </td>

                                    <td class="TableRowNew"><input type="text" class="form-control"></td>
                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
<tr id="ctl00_ContentPlaceHolder1_Tr2">
                                    <td align="center" class="TableMain" colspan="6" rowspan="1" style="height: 0%">
                                        <div>
                                            <br><br>
                                            <tr>
                        <td colspan="5" class="contenthead">Payment Details</td>
<!--                        <td class="contenthead">Consignee Details</td>-->
                    </tr>
                                            <table cellpadding="0" cellspacing="0" class="border" id="fuelTBL" name="fuelTBL" width="100%">
 <tr>
                                  <td class="texttitle2">
                                   Billing Party


                                    </td>
                                     <td class="texttitle2">

                                        <input type="checkbox" name="vehicle" value="Bike">Consignor<br>
                                     </td>
                                     <td class="texttitle2">

                                         <input type="checkbox" name="vehicle" value="Bike">Consignee<br>
                                     </td>

                                    <td class="texttitle2">&nbsp;</td>
                                    <td class="texttitle2">

                                         <input type="checkbox" name="vehicle" value="Bike">Contract<br>
                                     </td>

                                   <td class="texttitle2">

                                         <input type="checkbox" name="vehicle" value="Bike">Third Party<br>
                                     </td>

                                </tr>


                                <tr>
                                    <td class="texttitle1">

                                    </td>
                                    <td class="text1">

                                    </td>
                                    <td class="texttitle1">&nbsp;</td>
                                    <td class="texttitle1">

                                    </td>
                                    <td class="text1">

                                    </td>
                                    <td class="text1">&nbsp;</td>
                                </tr>
                                    </table>
                                            <table cellpadding="0" cellspacing="0" class="border" id="fuelTBL" name="fuelTBL" width="100%">
 <tr>
                                    <td class="texttitle1">
                                      Freight Charges
                                    </td>
                                    <td class="text1">

                                        <input type="text" name="routeDetails"  class="form-control"id="routeDetails" />
                                        &nbsp;Flat (In SAR)<input type="text" value="Arrived Value" class="form-control" />
                                    </td>
                                    <td class="texttitle1">  </td>
                                    <td class="texttitle1">
                                      EDD
                                    </td>
                                    <td class="text1">
                                      <input name="tripDate" type="text" class="datepicker"  readonly="readonly" id="tripDate"  />
                                    </td>
                                    <td class="text1">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="texttitle1">
                                Service Tax Paid By
                                    </td>
                                    <td class="text1">

                                          <select name="tripPurpose" class="form-control"  id="tripPurpose" style="width:120px;">
                                           <option value=""> -Select- </option>
                                        </select>
                                    </td>
                                    <td class="texttitle1">  </td>
                                    <td class="texttitle1">
                                    Billed At
                                    </td>
                                    <td class="text1">
                                      <input name="" type="text" class="form-control"  readonly="readonly" id="tripDate"  />
                                    </td>
                                    <td class="text1">
                                    </td>
                                </tr>
 <tr>
                                    <td class="texttitle1">
                                      Freight Charges
                                    </td>
                                    <td class="text1">

                                        <input type="text" name="routeDetails"  class="form-control"id="routeDetails" />
                                        &nbsp;FOV Calculated&nbsp;&nbsp; <input type="text" value="Auto Value" class="form-control" />
                                    </td>
                                    <td class="texttitle1">  </td>
                                    <td class="texttitle1">
                                      FOV Charged
                                    </td>
                                    <td class="text1">
                                      <input name="" type="text" class="form-control" value="Arrived Value" />
                                    </td>
                                    <td class="text1">
                                    </td>
                                </tr>


                                <tr>
                                    <td class="texttitle1">

                                    </td>
                                    <td class="text1">

                                    </td>
                                    <td class="texttitle1">&nbsp;</td>
                                    <td class="texttitle1">

                                    </td>
                                    <td class="text1">

                                    </td>
                                    <td class="text1">&nbsp;</td>
                                </tr>
                                    </table>
                                        </div>

                                    </td>
                                </tr>

                                <br><br>
                                 <tr id="ctl00_ContentPlaceHolder1_Tr1" align="center">
                                    <td align="center" class="TableColumn" colspan="6" rowspan="1">
                                        <h2></h2>
                                    </td>
                                </tr>
         <tr id="ctl00_ContentPlaceHolder1_Tr2">
     <td align="center" class="TableColumn" colspan="6" rowspan="1" style="height: 0%">
           <div>
<br><br>
                <tr>
                        <td colspan="5" class="contenthead">Additional Charges</td>
<!--                        <td class="contenthead">Consignee Details</td>-->
                 </tr>
                    <tr>
                                    <td class="texttitle2">
                                        Document Charges
                                    </td>
                                    <td class="texttitle2">

                                        <input type="" class="form-control" value="" id="" name="" >
                                    </td>
                                    <td class="texttitle2">&nbsp;</td>
                                    <td class="texttitle2">

                                    </td>
                                    <td class="texttitle2">

                                    </td>

                   </tr>
                                <tr>
                                    <td class="texttitle2">
                                     Special Charges
                                    </td>
                                    <td class="texttitle2">
                                        <input type="" class="form-control" value="" id="" name="" >
                                    </td>
                                    <td class="texttitle2">&nbsp;</td>
                                    <td class="texttitle2">

                                    </td>
                                    <td class="texttitle2">

                                    </td>

                                </tr>
                                <tr>
                                    <td class="texttitle2">
                                       ODA Charges
                                    </td>
                                    <td class="texttitle2">
                                         <input type="" class="form-control" value="" id="" name="" >
                                    </td>
                                    <td class="texttitle2">&nbsp;</td>
                                    <td class="texttitle2">

                                    </td>
                                    <td class="texttitle2">

                                    </td>

                                </tr>
                                <tr>
                                    <td class="texttitle2">
                                      MultiPickup Charges
                                    </td>
                                    <td class="texttitle2">
                                         <input type="" class="form-control" value="" id="" name="" >
                                    </td>
                                    <td class="texttitle2">&nbsp;</td>
                                    <td class="texttitle2">

                                    </td>
                                    <td class="texttitle2">

                                    </td>

                                </tr>
                                <tr>
                                    <td class="texttitle2">
                                      MultiDelivery Charges
                                    </td>
                                    <td class="texttitle2">
                                         <input type="" class="form-control" value="" id="" name="" >
                                    </td>
                                    <td class="texttitle2">&nbsp;</td>
                                    <td class="texttitle2">

                                    </td>
                                    <td class="texttitle2">

                                    </td>

                                </tr>
                                <tr>
                                    <td class="texttitle2">
                                       Handling Charges
                                    </td>
                                    <td class="texttitle2">
                                         <input type="" class="form-control" value="" id="" name="" >
                                    </td>
                                    <td class="texttitle2">&nbsp;</td>
                                    <td class="texttitle2">

                                    </td>
                                    <td class="texttitle2">

                                    </td>

                                </tr>
                       </div>

              </td>
         </tr>

       <tr id="ctl00_ContentPlaceHolder1_Tr1" align="center">
                                    <td align="center" class="TableColumn" colspan="6" rowspan="1">
                                        <h2></h2>
                                    </td>
                                </tr>
         <tr id="ctl00_ContentPlaceHolder1_Tr2">

     <td align="center" class="TableColumn" colspan="6" rowspan="1" style="height: 0%">
           <div>
<br><br>
                <tr>
                        <td colspan="5" class="contenthead">Applicable Taxes Charges</td>
<!--                        <td class="contenthead">Consignee Details</td>-->
                 </tr>
                    <tr>
                                    <td class="texttitle2">
                                        Service Tax Rate(%)
                                    </td>
                                    <td class="texttitle2">

                                        <input type="" class="form-control" value="3" id="" name="" >
                                    </td>
                                    <td class="texttitle2">&nbsp;</td>
                                    <td class="texttitle2">
                                      Service Tax on Sub-Total
                                    </td>
                                    <td class="texttitle2">

                                        <input type="" class="form-control" value="sub total*3%" id="" name="" >
                                    </td>

                   </tr>
                                <tr>
                                    <td class="texttitle2">
                                      Educational Cess Rate(%)
                                    </td>
                                    <td class="texttitle2">
                                        <input type="" class="form-control" value="2" id="" name="" >
                                    </td>
                                    <td class="texttitle2">&nbsp;</td>
                                    <td class="texttitle2">
                                   Educational Cess on Service Tax
                                    </td>
                                    <td class="texttitle2">
                                        <input type="" class="form-control" value="servicetax*2%" id="" name="" >
                                    </td>

                                </tr>
                                <tr>
                                    <td class="texttitle2">
                                       Higher Educational Cess Rate(%)
                                    </td>
                                    <td class="texttitle2">
                                         <input type="" class="form-control" value="1" id="" name="" >
                                    </td>
                                    <td class="texttitle2">&nbsp;</td>
                                    <td class="texttitle2">
                                       Higher Educational Cess on Service Tax
                                    </td>
                                    <td class="texttitle2">
                                          <input type="" class="form-control" value="servicetax*1%" id="" name="" >
                                    </td>

                                </tr>
                       </div>

              </td>
         </tr>


                            </table>
                    <tr>
                        <td style="width: 189px; height: 19px">&nbsp;</td>
                        <td align="center" colspan="2">&nbsp;</td>
                        <td align="center" style="height: 19px; width: 76px;">&nbsp;</td>
                        <td align="center" style="width: 76px; height: 19px">&nbsp;</td>
                    </tr>




                <center>
                    <input type="submit" name="Submit" value="Submit" id="save" class="button" style="width:120px;" />
                </center>

            </div>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
