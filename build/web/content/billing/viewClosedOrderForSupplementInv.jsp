<%@page import="ets.domain.billing.business.BillingTO"%>
<%@page import="java.util.ArrayList"%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css" />

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        $(".datepicker").datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true, changeYear: true
        });
    });
</script>
<script type="text/javascript">

    function submitPage() {
//                var customerId = document.getElementById("customerId").value;
//                if (customerId == "") {
//                    alert("Select Billing party");
//                                   }
        document.enter.action = '/throttle/handleSupplementInvoice.do';
        document.enter.submit();

    }
    function generateBill() {
        document.enter.action = '/throttle/generateOrderSupplementBill.do';
        document.enter.submit();
    }
    function selectCheckCondition() {
        if (document.getElementById("select").checked = true) {
            document.getElementById("select").value = "selected";
        } else {
            document.getElementById("select").value = "";
        }



    }


    $(function() {
        // alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

    function printTripGrn(tripSheetId) {
        window.open('/throttle/handleTripSheetPrint.do?tripSheetId=' + tripSheetId, 'PopupPage', 'height = 800, width = 1200, scrollbars = yes, resizable = yes');
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Supplement Billing</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"> Billing(Order)</a></li>
            <li class="active">Supplement Invoice</li>
        </ol>
    </div>
</div>
<!--<div id ="theID">  <p style='color:red'>Supplement Invoice already Generated! </p></div>-->
<div id="dialog-message" title="ALERT" style="display:none">
    <p style='color:red'> Supplement Invoice already Generated! </p>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%
                String menuPath = "Finance >> View Bills";
                request.setAttribute("menuPath", menuPath);
            %>
            <body>
                <form name="enter" method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp"%>
                    <table class="table table-info mb30 table-hover" style="width:80%">
                        <tr height="30"   ><td colSpan="5" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">View Bill Details</td></tr>

                        <tr>
                            <td><font color="red">*</font>From Date</td>
                            <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker"   style="width:240px;height:40px;" onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>"></td>
                            <td><font color="red">*</font>To Date</td>
                            <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker"   style="width:240px;height:40px;"onclick="ressetDate(this);" value="<c:out value="${toDate}"/>"></td>

                            <td> <input type="hidden" name="days" id="days" value="" /><input type="hidden" name="tripType" id="tripType" value="<c:out value="${tripType}"/>" /></td>
                        </tr>
                        <tr>
                            <td><font color="red">*</font>Customer Name</td>
                            <td height="30">
                                <select  name="customerId" id="customerId"  class="form-control" style="width:240px;height:40px;"><option value="">---Select---</option>
                                    <c:forEach items="${customerList}" var="customerList">
                                        <option value='<c:out value="${customerList.custId}"/>'><c:out value="${customerList.custName}"/></option>
                                    </c:forEach>
                                </select>
                                <script>
                                    document.getElementById('customerId').value = '<c:out value="${customerId}"/>';
                                </script>
                            </td>
                            <td><font color="red">*</font>Bill No</td>
                            <td height="30">
                                <input name="billNo" id="billNo" type="text" class="form-control"   style="width:240px;height:40px;" value="<c:out value="${billNo}"/>">
                            </td>
                        </tr>
                        <tr>
                            <td><font color="red">*</font>Gr No</td>
                            <td height="30">
                                <input name="grNo" id="grNo" type="text" class="form-control"   style="width:240px;height:40px;" value="<c:out value="${grNo}"/>">
                            </td>
                            <td colspan="2">
                        <center><input type="button" class="btn btn-info"   value="fetch data" onclick="submitPage();">
                        </center>
                        </tr>
                    </table>
                    <script>
                        (function() {
                            var billNo = document.getElementById('billNo');
                            billNo.addEventListener('keypress', function(event) {
                                if (event.keyCode == 13 || event.keyCode == 9) {
                                    event.preventDefault();
                                    if (billNo != '') {
                                        submitPage();
                                    }
                                }
                            });
                        }());
                        (function() {
                            var grNo = document.getElementById('grNo');
                            grNo.addEventListener('keypress', function(event) {
                                if (event.keyCode == 13 || event.keyCode == 9) {
                                    event.preventDefault();
                                    if (grNo != '') {
                                        submitPage();
                                    }
                                }
                            });
                        }());
                    </script>    


                    <br>
                    <br>
                    <%
                    if((ArrayList)request.getAttribute("closedTrips")!=null){
                        String fromDate=(String)request.getAttribute("fromDate");
                        String toDate=(String)request.getAttribute("toDate");
                    ArrayList list=(ArrayList)request.getAttribute("closedTrips");
                    %>
                    <%if(list.size()>0){%>

                    <table class="table table-info mb30 table-hover"  id="table" style="width:100%">
                        <thead>
                            <tr >
                                <th>S No</th>
                                <th>Select</th>
                                <th>Invoice Code</th>                         
                                <th>Bill of Entry</th>
                                <th>Shipping Bill No</th>
                                <th>Order Type</th>
                                <th>Billing Party</th>
                                <th>Customer Name</th>
                                <th>Route Name</th>
                                <th>GR NO</th>
                                <th>C-Notes</th>
                                <th>ContainerNo</th>
                                <th>Trip Code</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%int index=0;
                            int sno=1;
                            String classText ="";
                            %>
                            <%
                                 String statusId="";
                                 String customerName="";
                                 String billingParty="";
                                 String container="";
                                 String cnoteNo="";
                                 String consignmentOrderId="";
                                 String tripId="";
                                 String regNo="";
                                 String vehicleType="";
                                 String routeName="";
                                 String vehicleTypeName="";
                                 String tripStartDate="";
                                 String tripEndDate="";
                                 String tripstartkm="";
                                 String tripendkm="";
                                 String totalWeight="";
                                 String destinationName="";
                                 String freightAmt="";
                                 String billingType="";
                                 String customerId="";
                                 String cnoteId="";
                                 String checkTripId="";
                                 String totalkmrun="";
                                 String podStatus="";
                                 String reeferRequired="";
                                 String timeElapsedValue="";
                                 String billOfEntry="";
                                 String shippingBillNo="";
                                 String movementType="";
                                 String panNo="";
                                 String invoiceCode="";
                                 String invoiceId="";
                                 String organizationId="";
                                 String customerType="";
                                 String gstNo="";
                                 String containerNo="";
                                 for(int i=0;i<list.size();i++){
                                 String tripCodes[] = null;
                                 String tripIds[] = null;
                                 String tripCode="";
                                 String grNo="";
                                 int oddEven = index % 2;

                                  if (oddEven > 0) {
                                             classText = "text2";
                                         } else {
                                             classText = "text1";
                                         }

                                     BillingTO opto =(BillingTO)list.get(i);
                                     customerName = opto.getCustomerName();
                                     cnoteNo = opto.getConsignmentNoteNo();
                                     containerNo = opto.getContainerNo();
                                     tripId=opto.getTripId();

                                     if(opto.getTripId().contains(",")){
                                     tripCodes = opto.getTripCode().split(",");
                                     tripIds=opto.getTripId().split(",");
                                        for(int k=0; k<tripCodes.length; k++){
                                         if(tripCode == ""){
                                         tripCode="<a href='#' onclick='viewTripDetails("+tripIds[k]+")'>"+tripCodes[k]+"</a>,<br>";
                                         }else{
                                         if(k == tripCodes.length-1){
                                         tripCode=tripCode+"<a href='#' onclick='viewTripDetails("+tripIds[k]+")'>"+tripCodes[k]+"</a>";
                                         }else{
                                         tripCode=tripCode+"<a href='#' onclick='viewTripDetails("+tripIds[k]+")'>"+tripCodes[k]+"</a>,<br>";
                                         }
                                         }

                                        }
                                     }else{
                                     tripCode="<a href='#' onclick='viewTripDetails("+opto.getTripId()+")'>"+opto.getTripCode()+"</a>";
                                     }
                                     statusId=opto.getStatusId();
                                     invoiceCode=opto.getInvoiceCode();
                                     invoiceId=opto.getInvoiceId();
                                     checkTripId=opto.getTripId();
                                     destinationName=opto.getConsigmentDestination();
                                     freightAmt=opto.getFreightCharges();
                                     billingType=opto.getBillingTypeId();
                                     customerId=opto.getCustomerId();
                                     totalkmrun=opto.getTotalKmRun();
                                     totalWeight=opto.getTotalWeightage();
                                     podStatus=opto.getStatus();
                                     routeName=opto.getRouteInfo();
                                     reeferRequired=opto.getTempReeferRequired();
                                     consignmentOrderId=opto.getConsignmentOrderId();
                                     billingParty=opto.getBillingParty();
                                     container=opto.getContainerTypeName();
                                     timeElapsedValue=opto.getTimeElapsedValue();
                                     billOfEntry=opto.getBillOfEntryNo();
                                     shippingBillNo=opto.getShipingBillNo();
                                     movementType=opto.getMovementType();
                                     panNo=opto.getPanNo();
                                     gstNo=opto.getGstNo();
                                     organizationId=opto.getOrganizationId();
                                     customerType=opto.getCompanyType();
                                     String[] grNos=null;
                                   //  grNo=opto.getGrNo();
                                     if(opto.getGrNo().contains(",")){
                                     grNos = opto.getGrNo().split(",");
                                     tripIds=opto.getTripId().split(",");
                                        for(int k=0; k<grNos.length; k++){
                                         if(grNo == ""){
                                         grNo="<a href='#' onclick='printTripGrn("+tripIds[k]+")'>"+grNos[k]+"</a>,<br>";
                                         }else{
                                         if(k == grNos.length-1){
                                         grNo=grNo+"<a href='#' onclick='printTripGrn("+tripIds[k]+")'>"+grNos[k]+"</a>";
                                         }else{
                                         grNo=grNo+"<a href='#' onclick='printTripGrn("+tripIds[k]+")'>"+grNos[k]+"</a>,<br>";
                                         }
                                         }

                                        }
                                     }else{
                                     grNo="<a href='#' onclick='printTripGrn("+opto.getTripId()+")'>"+opto.getGrNo()+"</a>";
                                     }
                            %>

                            <tr>
                                <td  >
                                    <% if(Integer.parseInt(timeElapsedValue )> -2) {%>
                                    <font color="green">   <%=sno%></font>
                                    <% }else if (Integer.parseInt(timeElapsedValue )> -5){ %>
                                    <font color="orange">    <%=sno%></font>
                                    <%}else { %>
                                    <font color="red">  <%=sno%></font>
                                    <% }%>
                                </td>

                                <td  >
                                    <input type="hidden" name="invoiceIds" id="invoiceIds<%=sno%>" value="<%=invoiceId%>"/>
                                    <input type="hidden" name="tripId" id="tripId<%=sno%>" value="<%=tripId%>"/>
                                    <input type="hidden" name="customerId<%=sno%>" id="customerId<%=sno%>"value="<%=customerId%>"/>
                                    <input type="hidden" name="panNo<%=sno%>" id="panNo<%=sno%>"value="<%=panNo%>"/>
                                    <input type="hidden" name="gstNo<%=sno%>" id="gstNo<%=sno%>"value="<%=gstNo%>"/>
                                    <input type="hidden" name="statusId<%=sno%>" id="statusId<%=sno%>"value="<%=statusId%>"/>
                                    <input type="hidden" name="organizationId<%=sno%>" id="organizationId<%=sno%>"value="<%=organizationId%>"/>
                                    <input type="hidden" name="customerType<%=sno%>" id="customerType<%=sno%>"value="<%=customerType%>"/>
                                    <input type="radio" name="consignmentId" id="consignmentId<%=sno%>" value="<%=tripId%>" onclick="setBillingOrder('<%=sno%>', this, this.value)"/>
                                    <input type="hidden" name="consignmentIds" id="consignmentIds<%=sno%>" value="<%=consignmentOrderId%>" />
                                    <input type="hidden" name="tripIdStatus" id="tripIdStatus<%=sno%>" value="0" />
                                    <input type="hidden" name="consingmentStatus" id="consingmentStatus<%=sno%>" value="0" />
                                </td>
                                <td   ><%=invoiceCode%></td>
                                <td   ><%=billOfEntry%></td>
                                <td   ><%=shippingBillNo%></td>
                                <td   ><%=movementType%></td>
                                <td   ><%=billingParty%></td>
                                <td   ><%=customerName%></td>
                                <%--       <td   style="width:120px;text-align: right"><%=freightAmt%></td> --%>
                                <td   style="width:120px"><%=routeName%></td>
                                <td   style="width:120px"><%=grNo%></td>
                                <td   style="width:120px">
                                    <% if(Integer.parseInt(timeElapsedValue )> -2) {%>

                                    <font color="green"><%=cnoteNo%></font>
                                    <% }else if (Integer.parseInt(timeElapsedValue )> -5){ %>
                                    <font color="orange"><%=cnoteNo%></font>
                                    <%}else { %>
                                    <font color="red"><%=cnoteNo%></font>
                                    <% }%>
                                </td>
                                <td style="width:120px"><%=containerNo%></td>
                                <td style="width:120px"><%=tripCode%></td>
                                <td>Invoice Generated</td>
                            </tr>
                            <%
                             cnoteId="500000";
                            sno++;
                            index++;
                            %>
                            <%
                        }
                            %>
                        </tbody>
                    </table>
                    <center>
                        <input type="hidden" name="invoiceId" id="invoiceId" value=""/>
                        <input type="hidden" name="consignmentOrderId" id="consignmentOrderId" value=""/>
                        <input type="hidden" name="custId" id="custId" value="<%=customerId%>" />
                        <input type="hidden" name="tripSheetId" id="tripSheetId" value="" />
                        <input type="button" class="btn btn-info"   value="GENERATE BILL" onclick="generateBill();" accesskey="" >
                    </center>
                    <br>
                    <br>
                    <input type="hidden" name="billingOrderId" id="billingOrderId" value=""/>
                    <input type="hidden" name="invoicefromDate" value="<%=fromDate%>"/>
                    <input type="hidden" name="invoicetoDate" value="<%=toDate%>"/>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                    </script>
                    <%}}%>
                    <script type="text/javascript">
                        var customerName = '<c:out value="${customerName}"/>';
                        var customerId = '<c:out value="${customerId}"/>';

                        if (customerId != null & customerId != "") {
                            document.getElementById("customerId").value = customerId;
                        }
                        if (customerName != null & customerName != "") {
                            document.getElementById("Customer").value = customerName;
                        }

                        function viewTripDetails(tripId) {
                            window.open('/throttle/viewTripSheetDetails.do?tripId=' + tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
                        }

                        function setBillingOrder(sno, obj, value) {
                           
                           document.getElementById("dialog-message").style.display = "block";
                            var cnoteId = document.getElementById("consignmentIds" + sno).value;
                            var statusId = document.getElementById("statusId" + sno).value;
                            var panNo = document.getElementById("panNo" + sno).value;
                            var organizationId = document.getElementById("organizationId" + sno).value;
                            var gstNo = document.getElementById("gstNo" + sno).value;
                            var invoiceId = document.getElementById("invoiceIds" + sno).value;
                            var customerType = document.getElementById("customerType" + sno).value; //alert("sno:"+sno+" customerType:"+customerType);
                            if (customerType == '') {
                                obj.checked = false;
                                alert("Invoice cannot process since companyType for customer is Empty");
                                return;
                            }
                            if (statusId == '32') {
//                                alert("Supplement Invoice already Generated!");
//                                alertify.alert(document.getElementById("mydiv").innerHTML);

//                                var divText = document.getElementById('theID').innerHTML;
//                                window.alert(divText);


                             
                                    $("#dialog-message").dialog({
                                        modal: true,
                                        buttons: {
                                            Ok: function() {
                                                  if (organizationId == '') {
                                obj.checked = false;
                                alert("Invoice cannot process since Organization for customer is Empty");
                                return;
                            }
                            if (organizationId == '1' && panNo == '') {
                                obj.checked = false;
                                alert("Invoice cannot process since Organization is individual,Pan No for customer is Empty");
                                return;
                            }
                            if (organizationId == '2' && gstNo == '' && panNo == '') {
                                obj.checked = false;
                                alert("Invoice cannot process since Organization is company,GST No,Pan No for customer is Empty");
                                return;
                            }
                            if (panNo == '') {
                                obj.checked = false;
                                alert("Invoice cannot process since PAN no for customer is Empty");
                                return;
                            }
                            if (obj.checked == true) {

                                document.getElementById("tripIdStatus" + sno).value = value;
                                document.getElementById("tripSheetId").value = value;
                                document.getElementById("consingmentStatus" + sno).value = cnoteId;
                                document.getElementById("consignmentOrderId").value = cnoteId;
                                document.getElementById("invoiceId").value = invoiceId;
                                //alert("test:"+document.getElementById("tripSheetId").value);
                                var txt;
                                var organizationName = "";
                                if (organizationId == '1') {
                                    organizationName = "Individual";
                                } else {
                                    organizationName = "Company";
                                }
                                alert("Org Name:" + organizationName + "  Pan No:" + panNo + "  GST NO:" + gstNo);
                                var r = confirm("Press ok to generate bill !");
                                if (r == true) {
                                    document.enter.action = '/throttle/generateOrderSupplementBill.do';
                                    document.enter.submit();
                                }
                            } else {
                                document.getElementById("tripIdStatus" + sno).value = 0;
                                document.getElementById("tripSheetId").value = 0;
                                document.getElementById("consingmentStatus" + sno).value = 0;
                                document.getElementById("consignmentOrderId").value = 0;
                            }
                                                $(this).dialog("close");
                                            }
                                        }});
                                
                            }
                            else{
                            if (organizationId == '') {
                                obj.checked = false;
                                alert("Invoice cannot process since Organization for customer is Empty");
                                return;
                            }
                            if (organizationId == '1' && panNo == '') {
                                obj.checked = false;
                                alert("Invoice cannot process since Organization is individual,Pan No for customer is Empty");
                                return;
                            }
                            if (organizationId == '2' && gstNo == '' && panNo == '') {
                                obj.checked = false;
                                alert("Invoice cannot process since Organization is company,GST No,Pan No for customer is Empty");
                                return;
                            }
                            if (panNo == '') {
                                obj.checked = false;
                                alert("Invoice cannot process since PAN no for customer is Empty");
                                return;
                            }
                            if (obj.checked == true) {

                                document.getElementById("tripIdStatus" + sno).value = value;
                                document.getElementById("tripSheetId").value = value;
                                document.getElementById("consingmentStatus" + sno).value = cnoteId;
                                document.getElementById("consignmentOrderId").value = cnoteId;
                                document.getElementById("invoiceId").value = invoiceId;
                                //alert("test:"+document.getElementById("tripSheetId").value);
                                var txt;
                                var organizationName = "";
                                if (organizationId == '1') {
                                    organizationName = "Individual";
                                } else {
                                    organizationName = "Company";
                                }
                                alert("Org Name:" + organizationName + "  Pan No:" + panNo + "  GST NO:" + gstNo);
                                var r = confirm("Press ok to generate bill !");
                                if (r == true) {
                                    document.enter.action = '/throttle/generateOrderSupplementBill.do';
                                    document.enter.submit();
                                }
                            } else {
                                document.getElementById("tripIdStatus" + sno).value = 0;
                                document.getElementById("tripSheetId").value = 0;
                                document.getElementById("consingmentStatus" + sno).value = 0;
                                document.getElementById("consignmentOrderId").value = 0;
                            }
                        }
                        }
                    </script>

                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
