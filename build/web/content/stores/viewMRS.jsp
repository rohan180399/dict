<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/parveencss/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
<%@ page import="ets.domain.mrs.business.MrsTO" %>
<%@ page import="java.util.*" %>
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script type="text/javascript" language="javascript" src="/throttle/js/ajaxFunction.js"></script>
<script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>

<script>


    function newWO(val) {
        var itemId = document.getElementsByName("mrsItemId");
        window.open('/throttle/OtherServiceStockAvailability.do?itemId=' + itemId[val].value, 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
    }

    function newWO1() {
        window.open('/throttle/getIssueHistory.do?mrsId=' + document.spareIssue.mrsId.value, 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
    }



    function submitWindow()
    {
        document.spareIssue.action = '/throttle/handleViewMrsIssue.do?mrsId=' + document.spareIssue.mrsId.value;
        document.spareIssue.submit();
    }

    function issuePopup(val) {

        var itemId = document.getElementsByName("mrsItemId");
        var itemName = document.getElementsByName("itemName");
        var categoryId = document.getElementsByName("categoryId");
        var rcStatus = document.getElementsByName("rcStatus");
        var mrsId = document.spareIssue.mrsId.value;
        var jobCardId = document.spareIssue.jobCardId.value;
        var positionId = document.getElementsByName("positionId");
        var approvedQty = document.getElementsByName("approvedQty");
        var rcWorkId = document.getElementsByName("rcWorkId");
        var rcItemsId = document.getElementsByName("rcItemsId");
        var mrsItemId = document.getElementsByName("mrsItemId");
    <%--var counterFlag =document.spareIssue.counterFlag.value;
    alert(counterFlag);
    var counterId=0;
    if(counterFlag=='Y')--%>
        var counterId = 0;

        counterId = document.spareIssue.counterId.value;
        var url = '/throttle/handleItemIssuePopup.do?categoryId=' + categoryId[val].value + '&itemName=' + itemName[val].value + '&itemId=' + itemId[val].value + '&mrsId=' + mrsId+ '&jobCardId=' + jobCardId

        url = url + '&rcStatus=' + rcStatus[val].value + '&approvedQty=' + approvedQty[val].value;
        if (rcWorkId.length > 0) {
            url = url + '&positionId=' + positionId[val].value + '&rcWorkId=' + document.spareIssue.rcWorkId.value + '&rcItemsId=' + rcItemsId[val].value + '&mrsItemId=' + mrsItemId[val].value + '&counterId=' + counterId;
        } else {
            url = url + '&positionId=' + positionId[val].value + '&rcWorkId=0' + '&rcItemsId=0' + '&mrsItemId=0&counterId=' + counterId;
        }
        window.open(url, 'PopupPage', 'height=700,width=700,scrollbars=yes,resizable=yes');
    }





    function submitPag1(val) {
        if (val == 'scrap') {
            if (scrapValidate() != 'fail') {
                document.spareIssue.action = "/throttle/addScrapItems.do";
                document.spareIssue.submit();
            }
        } else if (val == 'gdn') {
            document.spareIssue.action = "/throttle/generateMrsGdn.do";
            document.spareIssue.submit();
        }
    }

    function submitPag2() {
        document.spareIssue.action = "/throttle/MRSList.do";
        document.spareIssue.submit();
    }



    function ajaxSave(indx) {
        var itemType = document.getElementsByName("itemType");
        var rcItemIds = document.getElementsByName("rcItemIds");
        var priceIds = document.getElementsByName("priceIds");
        var actionType = document.getElementsByName("actionType");
        var quantityRI = document.getElementsByName("quantityRI");
        var faultStatus = document.getElementsByName("faultStatus");
        var totalIssued = document.getElementsByName("totalIssued");
        var mrsNumber = document.getElementsByName("mrsNumber");
        var mrsItemId = document.getElementsByName("mrsItemId");
        var faultStatus = document.getElementsByName("faultStatus");
        var rcStatus = document.getElementsByName("rcStatus");
        save = 'itemType=' + itemType[indx].value + "&rcItemIds=" + rcItemIds[indx].value + "&priceIds=" + priceIds[indx].value + "&actionType=" + actionType[indx].value + "&quantityRI=" + quantityRI[indx].value + "&faultStatus=" + faultStatus[indx].value + "&totalIssued=" + totalIssued[indx].value + "&mrsNumber=" + mrsNumber[indx].value + "&mrsItemId=" + mrsItemId[indx].value;
        save = save + "&rcStatus=" + rcStatus[indx].value;
        var url1 = '/throttle/save.do?' + save;

        if (faultStatus[indx].value == '0' && actionType[indx].value == '1012') {
            alert('Please Select Whether Receiving item is Fault');
            faultStatus[indx].focus();
        } else {
            if (window.ActiveXObject) {
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest) {
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url1, true);
            httpReq.onreadystatechange = function() {
                processSaveAjax(indx);
            };
            httpReq.send(null);
        }
    }

// ItemType
// 1011 - New Item
// 1012 - RC Item

// Action Type
// 1011 - Issue
// 1012 - Receive




    function getPrice(indx) {
        var itemType = document.getElementsByName("itemType");
        var mrsItemId = document.getElementsByName("mrsItemId");
        var actionType = document.getElementsByName("actionType");
        var item;
        var url;
        var counter = 0;
        if (itemType[indx].value == 1012) {
            document.spareIssue.rcItemIds.disabled = false;
        } else {
            document.spareIssue.rcItemIds.disabled = true;
        }
        alert('itemtype=' + itemType[indx].value + '--' + actionType[indx].value);
        if (itemType[indx].value == '1011' && actionType[indx].value == '1011') {
            alert('itemtype=' + itemType[indx].value + '--' + actionType[indx].value);
            item = 'itemType=' + itemType[indx].value + "&mrsItemId=" + mrsItemId[indx].value;
            url = '/throttle/itemPrice.do?' + item;
            counter++;
        } else if (itemType[indx].value == '1011' && actionType[indx].value == '1012') {
            alert('itemtype=' + itemType[indx].value + '--' + actionType[indx].value);
            item = 'itemType=' + itemType[indx].value + "&mrsItemId=" + mrsItemId[indx].value;
            url = '/throttle/itemPrice.do?' + item;
            counter++;
        } else if (itemType[indx].value == '1012' && actionType[indx].value == '1011') {
            alert('itemtype=' + itemType[indx].value + '--' + actionType[indx].value);
            item = 'itemType=' + itemType[indx].value + "&mrsItemId=" + mrsItemId[indx].value;
            url = '/throttle/getRcPriceList.do?' + item;
            counter++;
        } else if (itemType[indx].value == '1012' && actionType[indx].value == '1012') {
            alert('itemtype=' + itemType[indx].value + '--' + actionType[indx].value);
            item = 'itemType=' + itemType[indx].value + "&mrsItemId=" + mrsItemId[indx].value + "&vehicleId=" + document.spareIssue.vehicleId.value;
            url = '/throttle/getVehicleRcItems.do?' + item;
            counter++;
        }
        if (counter > 0) {
            if (window.ActiveXObject) {
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest) {
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function() {
                processAjax(itemType[indx].value, actionType[indx].value, indx);
            };
            httpReq.send(null);
        }
    }


    function processAjax(itemType, actionType, indx)
    {
        if (httpReq.readyState == 4)
        {
            if (httpReq.status == 200)
            {
                var pricIds = document.getElementsByName("priceIds");
                temp = httpReq.responseText.valueOf();
                if (itemType == '1011' && actionType == '1011') {
                    document.spareIssue.priceIds[indx].length = 0;
                    setOptions1(temp, pricIds[indx], '0', '1');
                }
                if (itemType == '1011' && actionType == '1012') {
                    setOptions1(temp, pricIds[indx], '0', '1');
                } else if (itemType == '1012' && actionType == '1011') {
                    setOptions1(temp, pricIds[indx], '0', '0');
                    setOptions1(temp, document.spareIssue.rcItemIds[indx], '1', '1');
                } else if (itemType == '1012' && actionType == '1012') {
                    setOptions1(temp, document.spareIssue.rcItemIds[indx], '0', '0');
                }
                //document.spareIssue.qty.value = splt[0];
                // document.spareIssue.mrsLocalServiceItemSum.value = splt[1];
                // document.spareIssue.count.value = splt[2];
                // document.spareIssue.totalIssued.value = splt[3];
            }
            else
            {
                alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
            }
        }
    }
    //qty=rcItemQuantity
    //count=otherMrsRequest
    //mrsLocalServiceItemSum=NewItemQuantity
    function processSaveAjax(indx)
    {
        var qty = document.getElementsByName("qty");
        var localItems = document.getElementsByName("mrsLocalServiceItemSum");
        var otherReq = document.getElementsByName("count");
        var totIsssued = document.getElementsByName("totalIssued");
        if (httpReq.readyState == 4)
        {
            if (httpReq.status == 200)
            {
                temp = httpReq.responseText.valueOf();
                var splt = temp.split('-');
                qty[indx].value = splt[0];
                localItems[indx].value = splt[1];
                otherReq[indx].value = splt[2];
                totIsssued[indx].value = splt[3];
            }
            else
            {
                alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
            }
        }
    }

    function disableFaultItem(indx)
    {
        var faultItem = document.getElementsByName('faultStatus');
        var actionType = document.getElementsByName('actionType');
        if (actionType[indx].value == 1011) {
            faultItem[indx].value = 'N';
            faultItem[indx].disabled = true;
        } else {
            faultItem[indx].value = '0';
            faultItem[indx].disabled = false;
        }
    }

    function scrapValidate() {
        var cntr = 0;
        var mess = 'pass';
        for (var i = 0; i < document.spareIssue.selectedIndex1.length; i++) {
            if (document.spareIssue.selectedIndex1[i].checked == 1) {
                if (numberValidation(document.spareIssue.faultQty[i], 'Fault Items')) {
                    mess = 'fail';
                    return mess;
                } else {
                    cntr++;
                }
            }
        }
        if (cntr > 0) {
            mess = 'pass'
            return mess;
        }
        return "fail"
    }


    function printMrs() {
        //alert(document.spareIssue.counterFlag.value);
        window.open('/throttle/printMrs.do?mrsNumber=' + document.spareIssue.mrsId.value, 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
    }

    function correctRetAmount() {
        var result = 0;
        //correctOthers();
        //correctNett();
        if (isFloat(document.spareIssue.spareRetAmount.value)) {
            alert("Please Enter spareRetAmount");
            document.spareIssue.spareRetAmount.focus();
            document.spareIssue.spareRetAmount.select();
            return;
        }
        if (parseFloat(document.spareIssue.spareRetAmount.value) > parseFloat(document.spareIssue.nettAmount.value)) {
            alert("Spare Return value cannot be greater than bill value");
            document.spareIssue.spareRetAmount.value = "0";
            document.spareIssue.spareRetAmount.focus();
            return
        }
        else {
            result = parseFloat(document.spareIssue.nett.value) - parseFloat(document.spareIssue.spareRetAmount.value);
            document.spareIssue.nettAmount.value = result.toFixed(2);
        }
    }




    function correctNett() {
        var result = 0;
        if (isFloat(document.spareIssue.discount.value)) {
            alert("Please Enter Discount");
            document.spareIssue.discount.focus();
            document.spareIssue.discount.select();
            return;
        }
        if (parseFloat(document.spareIssue.discount.value) > parseFloat(document.spareIssue.nettAmount.value)) {
            alert("Discount value cannot be greater than bill value");
            document.spareIssue.discount.value = "0";
            document.spareIssue.discount.focus();
            exit;
            return;
        }
        else {

            //result = parseFloat(document.spareIssue.nettAmount.value) - (parseFloat(document.spareIssue.nettAmount.value) * (parseFloat(document.spareIssue.discount.value)/100));
            //result = parseFloat(document.spareIssue.spareAmount.value) - (parseFloat(document.spareIssue.spareAmount.value) * (parseFloat(document.spareIssue.discount.value)/100));            
            result = parseFloat(document.spareIssue.sparesOriginalAmount.value) - (parseFloat(document.spareIssue.sparesOriginalAmount.value) * (parseFloat(document.spareIssue.discount.value) / 100));
            document.spareIssue.nettAmount.value = result.toFixed(2);
            document.spareIssue.nett.value = result.toFixed(2);
            correctRetAmount();
        }
    }
    function correctOthers() {
        var sparePrices = document.getElementsByName("price");
        var tax = document.getElementsByName("tax");
        var hike = parseFloat(document.spareIssue.hike.value);

        if (isFloat(hike)) {
            alert("Please Enter Hike Percentage");
            document.spareIssue.discount.focus();
            document.spareIssue.hike.focus();
            document.spareIssue.hike.select();
            return;
        }
        // Items
        var sparePrices = document.getElementsByName("price");
        //var taxPrices = document.getElementsByName("taxPrice");
        var originalPrice = document.getElementsByName("originalPrice");
        var totalIssued = document.getElementsByName("totalIssued");
        var spareAmounts = document.getElementsByName("spareAmount");
        var spareTotal = 0;




        for (var i = 0; i < sparePrices.length; i++) {
            //sparePrices[i].value  =parseFloat( sparePrices[i].value ) + parseFloat( (sparePrices[i].value * hike)/100 );
            //sparePrices[i].value  =parseFloat( taxPrices[i].value ) + parseFloat( (taxPrices[i].value * hike)/100 );
            sparePrices[i].value = parseFloat(originalPrice[i].value) + parseFloat((originalPrice[i].value * hike) / 100);
            sparePrices[i].value = parseFloat(sparePrices[i].value).toFixed(2);
            spareTotal = parseFloat(spareTotal) + parseFloat(sparePrices[i].value) * parseFloat(totalIssued[i].value);

        }
        document.spareIssue.spareAmount.value = spareTotal.toFixed(2);
        document.spareIssue.sparesOriginalAmount.value = spareTotal.toFixed(2);
        document.spareIssue.nettAmount.value = spareTotal.toFixed(2);
        changeTax();
        correctNett();
        //correctRetAmount();

    }
    function changeTax()
    {

        var tax = document.getElementsByName("tax");
        var spareAmounts = document.getElementsByName("spareAmount");
        var sparesOriginalAmount = document.getElementsByName("sparesOriginalAmount");
        var totalAmount = document.getElementsByName("totalAmount");

        var spareTaxAmount = document.getElementsByName("spareTaxAmount");
        var total = 0;

        for (var i = 0; i < tax.length; i++)
        {
            if (tax[i].selectedIndex == 0)
            {
                alert("Please Select Tax");
                tax[i].selectedIndex = 4;
                tax[i].focus();
                return;
            }
        }

        total = parseFloat((spareAmounts[0].value * tax[0].value) / 100);
        spareTaxAmount[0].value = total.toFixed(2);
        sparesOriginalAmount[0].value = parseFloat(spareAmounts[0].value) + total;
        totalAmount[0].value = parseFloat(spareAmounts[0].value) + total;
        totalAmount[0].value = parseFloat(totalAmount[0].value).toFixed(2);
        correctNett();
    }


    function setTax()
    {

        if (document.spareIssue.counterId.value != 0)
        {
            var tax = document.getElementsByName("tax");
            for (var i = 0; i < tax.length; i++)
            {
                tax[i].selectedIndex = 4;
            }
        }
    }

</script>


<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.IssueMRS"  text="MRS"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.stores"  text="Stores"/></a></li>
            <li class="active"><spring:message code="stores.label.IssueMRS"  text="MRS"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="setTax();">
                <form name="spareIssue"  method="post" >
                    <%@ include file="/content/common/message.jsp" %>
                    <% String classText = ""; %>
                    <%--<input type="hidden" name="counterFlag" value='<%= request.getAttribute("counter")%>'>--%>
                    <input type="hidden" name="counterId" value="<c:out value="${counterId}"/>"/>
                    <c:if test = "${vehicleDetails != null}" >
                        <table class="table table-info mb30 table-hover" >
                            <thead>
                                <tr>
                                    <th colspan="4" ><spring:message code="stores.label.IssueMRS"  text="default text"/></th>
                                </tr>
                            </thead>
                            <c:forEach items="${vehicleDetails}" var="vehicle">
                                <%
                                classText = "text2";
                                %>
                                <tr>
                                    <td><spring:message code="stores.label.VehicleNumber"  text="default text"/></td>
                                    <td><c:out value="${vehicle.mrsVehicleNumber}"/></td>
                                    <td><spring:message code="stores.label.KM"  text="default text"/></td>
                                    <td><c:out value="${vehicle.mrsVehicleKm}"/></td>
                                <input type="hidden" name="vehicleId" value=<c:out value="${vehicle.vehicleId}"/>  >
                                </tr>

                                <tr>
                                    <td><spring:message code="stores.label.VehicleType"  text="default text"/></td>
                                    <td><c:out value="${vehicle.mrsVehicleType}"/></td>
                                    <td><spring:message code="stores.label.MFR"  text="default text"/></td>
                                    <td><c:out value="${vehicle.mrsVehicleMfr}"/></td>
                                </tr>

                                <tr>
                                    <td><spring:message code="stores.label.UseType"  text="default text"/></td>
                                    <td><c:out value="${vehicle.mrsVehicleUsageType}"/></td>
                                    <td><spring:message code="stores.label.Model"  text="default text"/></td>
                                    <td><c:out value="${vehicle.mrsVehicleModel}"/></td>
                                </tr>

                                <tr>
                                    <td><spring:message code="stores.label.EngineNo"  text="default text"/></td>
                                    <td><c:out value="${vehicle.mrsVehicleEngineNumber}"/></td>
                                    <td><spring:message code="stores.label.ChassisNo"  text="default text"/></td>
                                    <td><c:out value="${vehicle.mrsVehicleChassisNumber}"/></td>
                                </tr>

                                <tr>
                                    <td><spring:message code="stores.label.Technician"  text="default text"/></td>
                                    <td> <c:out value="${vehicle.mrsTechnician}"/> </td>
                                    <td><spring:message code="stores.label.JobCardNo"  text="default text"/>.</td>
                                    <td><c:out value="${vehicle.mrsJobCardNumber}"/>
                                        <input type="hidden" name="jobCardId" value='<c:out value="${vehicle.mrsJobCardNumber}"/>' />
                                    </td>
                                </tr>
                            </c:forEach>
                        </c:if>

                    </table>
                    <c:if test = "${rcMrsDetails != null}" >
                        <table class="table table-info mb30 table-hover" >
                            <thead>
                                <tr>
                                    <th colspan="4" align="center" height="30"><div><spring:message code="stores.label.RCDetails"  text="default text"/></div></th>
                            </tr>
                            </thead>
                            <c:forEach items="${rcMrsDetails}" var="rc">
                                <tr>
                                    <td><spring:message code="stores.label.WorkOrderNo"  text="default text"/></td>
                                    <td><input type="hidden" name="rcWorkId" value='<c:out value="${rc.rcWorkId}"/>'>
                                        <c:out value="${rc.rcWorkId}"/></td>
                                    <td><spring:message code="stores.label.RequestedDate"  text="default text"/></td>
                                    <td><c:out value="${rc.issuedOn}"/></td>
                                </tr>
                                <tr>
                                    <td><spring:message code="stores.label.VendorName"  text="default text"/></td>
                                    <td><c:out value="${rc.vendorName}"/></td>
                                    <td><spring:message code="stores.label.Technician"  text="default text"/></td>
                                    <td><c:out value="${rc.mrsTechnician}"/></td>
                                </tr>

                            </c:forEach>
                        </c:if>
                    </table>
                    <c:if test = "${counterMrsDetails != null}" >
                        <table class="table table-info mb30 table-hover" >
                            <thead>
                                <tr>
                                    <th colspan="4" align="center" height="30"><div><spring:message code="stores.label.CounterSaleDetails"  text="default text"/></div></th>
                            </tr>
                            </thead>
                            <c:forEach items="${counterMrsDetails}" var="counter">

                                <tr>
                                    <td><spring:message code="stores.label.CounterSaleNo"  text="default text"/></td>
                                    <td><c:out value="${counter.counterId}"/></td>
                                    <td><spring:message code="stores.label.CustomerName"  text="default text"/></td>
                                    <td><c:out value="${counter.name}"/></td>
                                </tr>
                                <tr>
                                    <td><spring:message code="stores.label.CustomerAddress"  text="default text"/></td>
                                    <td><c:out value="${counter.address}"/></td>
                                    <td><spring:message code="stores.label.MobileNumber"  text="default text"/></td>
                                    <td><c:out value="${counter.mobileNo}"/></td>
                                </tr>
                                <tr>
                                    <td><spring:message code="stores.label.RequestedDate"  text="default text"/></td>
                                    <td><c:out value="${counter.issuedOn}"/></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>

                            </c:forEach>
                        </c:if>
                    </table>
                    <br>
                    <br>
                    <table class="table table-info mb30 table-hover" >
                        <thead>
                            <tr>
                                <% int counter = 0;%>
                                <c:if test = "${mrsDetails != null}" >
                                    <c:forEach items="${mrsDetails}" var="mrs">
                                        <% if (counter == 0) {%>
                                        <th colspan="13" align="center"><strong><spring:message code="stores.label.MRSNo"  text="default text"/></strong>&nbsp;&nbsp;:&nbsp;&nbsp;<c:out value="${mrs.mrsNumber}"/> </th>
                                <input type="hidden" name="mrsId" value=<c:out value="${mrs.mrsNumber}"/> >
                                <% counter++;
                        }%>
                                </tr>

                            </c:forEach>
                        </c:if>



                        <c:set var="spareAmount" value=""/>
                        <c:if test = "${mrsDetails != null}" >
                            <tr >
                                <th width="70" height="30"  ><spring:message code="stores.label.MFR"  text="default text"/><br><spring:message code="stores.label.Code"  text="default text"/></th>
                                <th width="70" height="30"  ><spring:message code="stores.label.PAPL"  text="default text"/> <br><spring:message code="stores.label.Code"  text="default text"/></th>
                                <th width="98" height="30"  ><spring:message code="stores.label.Item"  text="default text"/><br> <spring:message code="stores.label.Name"  text="default text"/></th>
                                <th width="98" height="30"  ><spring:message code="stores.label.Position"  text="default text"/></th>

                                <th width="61" height="30"  ><spring:message code="stores.label.RC"  text="default text"/><br> <spring:message code="stores.label.Stock"  text="default text"/></th>
                                <th width="61" height="30"  ><spring:message code="stores.label.New"  text="default text"/> <br><spring:message code="stores.label.Stock"  text="default text"/></th>
                                <th width="70" height="30" width="60"  ><spring:message code="stores.label.OtherS.P"  text="default text"/><br><spring:message code="stores.label.Stock"  text="default text"/></th>
                                <th width="56" height="30" width="60" ><spring:message code="stores.label.Other"  text="default text"/> <br><spring:message code="stores.label.MRSReq"  text="default text"/></th>
                                <th width="61" height="30"  ><spring:message code="stores.label.Approved"  text="default text"/><br> <spring:message code="stores.label.Qty"  text="default text"/></th>
                                <th width="68" height="30"  ><spring:message code="stores.label.Total"  text="default text"/> <br><spring:message code="stores.label.Issued"  text="default text"/></th>
                                    <c:if test="${counterId != 0}" >
                                    <th width="68" height="30"  ><spring:message code="stores.label.Price"  text="default text"/></th>
                                    </c:if>
                                <th width="68" height="30"  ><spring:message code="stores.label.Uom"  text="default text"/> </th>
                                <th width="68" height="30" colspan="2"  >&nbsp; </th>
                            </tr>
                            </thead>

                            <% int index = 0;%>
                            <c:forEach items="${mrsDetails}" var="mrs">
                                <% if(index % 2 ==0){
                                    classText = "text1";
                                } else{
                                    classText = "text2";
                                }    %>

                                <tr>
                                    <td class="<%= classText %>"  height="30"><input type="hidden" name="mrsItemMfrCode" value="<c:out value="${mrs.mrsItemMfrCode}"/>"><c:out value="${mrs.mrsItemMfrCode}"/></td>
                                    <td class="<%= classText %>"  height="30"><input type="hidden" name="mrsPaplCode" value="<c:out value="${mrs.mrsPaplCode}"/>"><c:out value="${mrs.mrsPaplCode}"/></td>
                                    <td class="<%= classText %>"  height="30"><input type="hidden" name="mrsItemId" value="<c:out value="${mrs.mrsItemId}"/>"><c:out value="${mrs.mrsItemName}"/></td>
                                <input type="hidden" name="itemName" value="<c:out value="${mrs.mrsItemName}"/>" >
                                <input type="hidden" name="rcItemsId" value="<c:out value="${mrs.rcItemsId}"/>" >
                                <input type="hidden" name="categoryId" value="<c:out value="${mrs.categoryId}"/>"  >
                                <td class="<%= classText %>"  height="30"><input type="hidden" name="positionId" value="<c:out value="${mrs.positionId}"/>"><c:out value="${mrs.positionName}"/></td>
                                <input type="hidden"  name="rcStatus" value="<c:out value="${mrs.rcStatus}"/>">

                                <td width="41" height="30"  class="<%= classText %>"><input type="text" size="5" class="form-control"  readonly name="qty" value="<c:out value="${mrs.qty}"/>"> </td>
                                <td width="51" height="30"  class="<%= classText %>"><input type="text" size="5" class="form-control"  readonly name="mrsLocalServiceItemSum" value="<c:out value="${mrs.mrsLocalServiceItemSum}"/>"> </td>
                                <td class="<%= classText %>"  height="30"><input type="hidden" name="mrsOtherServiceItemSum"><a href="" onClick="newWO(<%= index %>);
                                        return false;"><c:out value="${mrs.mrsOtherServiceItemSum}"/></a></td>
                                <td class="<%= classText %>"  height="30"><input type="text" class="form-control"  size="5" readonly name="count" value="<c:out value="${mrs.count}"/>"> </td>
                                <td class="<%= classText %>"  height="30"><input type="hidden" name="approvedQty" value="<c:out value="${mrs.approvedQty}"/>"><c:out value="${mrs.approvedQty}"/></td>
                                <td class="<%= classText %>"  height="30"><input type="text" class="form-control" size="5" readonly name="totalIssued" value="<c:out value="${mrs.totalIssued}"/>"> </td>

                                <c:if test="${counterId != 0}" >
                                    <td class="<%= classText %>"  height="30"><input type="text" class="form-control" size="5" readonly name="price" value="<c:out value="${mrs.price}"/>"><input type="hidden" name="originalPrice" value="<c:out value="${mrs.price}"/>"/><input type="hidden" name="itemTax" value="<c:out value="${mrs.tax}"/>"/><input type="hidden" name="taxPrice" value="<c:out value="${mrs.price}"/>"/><c:set var="spareAmount" value="${spareAmount+ (mrs.totalIssued * mrs.price)}"/></td>
                                    </c:if>
                                <td class="<%= classText %>"  height="30"><c:out value="${mrs.uomName}"/></td>
                                <td class="<%= classText %>"   height="30"><a href="" onClick="issuePopup('<%= index %>');
                                        return false;" ><spring:message code="stores.label.Issue"  text="default text"/></a> </td>
                                <td class="<%= classText %>"   height="30"></td>

                                </tr>
                                <input type="hidden" name="mrsNumber" value="<c:out value="${mrs.mrsNumber}"/>">
                                <input type="hidden" name="selectedIndex" value='<%= index %>'  >
                                <% index++; %>
                            </c:forEach>
                        </table>
                        <c:if test="${counterId != 0}" >
                            <table id="bg" align="center" border="0" cellpadding="0" cellspacing="0" width="90%" class="border">


                                <tr>
                                    <td><spring:message code="stores.label.HikePercentage"  text="default text"/></td>
                                    <td><input type="text" class="form-control" name="hike" value="0" onchange="correctOthers();"></td>
                                </tr>
                                <tr>
                                    <td><spring:message code="stores.label.Discount"  text="default text"/></td>
                                    <td><input type="text" class="form-control" name="discount" value="0" onchange="correctNett();"></td>

                                </tr>
                                <tr>
                                    <td><spring:message code="stores.label.SpareAmount"  text="default text"/></td>
                                    <td><input type="text" class="form-control" readonly name="spareAmount" value="<fmt:formatNumber pattern="##.00" value="${spareAmount}"/>"> <input type="hidden" name="sparesOriginalAmount" value="<fmt:formatNumber pattern="##.00" value="${spareAmount}"/>"/></td>

                                </tr>
                                <tr>
                                    <td><spring:message code="stores.label.Tax"  text="default text"/></td>
                                    <td>
                                        <select id="tax" name="tax" style="width:130px;" class="form-control" onchange="changeTax();">
                                            <option value="1">----------<spring:message code="stores.label.Select"  text="default text"/>---------</option>
                                            <c:forEach items="${vatList}" var="tax">
                                                <option value="<c:out value="${tax.vat}"/>"><c:out value="${tax.vat}"/></option>
                                            </c:forEach>
                                        </select>
                                    </td>

                                </tr>
                                <tr>
                                    <td><spring:message code="stores.label.TaxAmount"  text="default text"/></td>
                                    <td><input type="text" class="form-control" name="spareTaxAmount" value="0" ></td>

                                </tr>
                                <tr>
                                    <td><spring:message code="stores.label.TotalAmount"  text="default text"/></td>
                                    <td><input type="text" class="form-control" name="totalAmount" value="0" ></td>

                                </tr>
                                <tr>
                                    <td><spring:message code="stores.label.SpareReturnAmount"  text="default text"/></td>
                                    <td><input type="text" class="form-control" name="spareRetAmount" value="0" onchange="correctRetAmount();"></td>

                                </tr>
                                <tr>
                                    <td><spring:message code="stores.label.NettAmount"  text="default text"/></td>
                                    <td><input type="text" class="form-control" readonly name="nettAmount" value="<fmt:formatNumber pattern="##.00" value="${spareAmount}"/>"><input type="hidden" name="nett" value="<fmt:formatNumber pattern="##.00" value="${spareAmount}"/>"/></td>
                                </tr>

                            </table>
                        </c:if>
                        <center> <textarea class="form-control" style="width:380px;height:60px;" name="remarks" rows="2" cols="15" ></textarea> </center>
                        <div align="center" ><a href="" onClick="newWO1();
                                return false;"><spring:message code="stores.label.History"  text="default text"/></a></div>
                        <br>
                        <center>
                            <!-- <input type="button" value="Scrap" name="scrap" class="button" onClick="submitPag1(this.name);">&nbsp; -->
                            <input type="button" name="gdn" value="<spring:message code="stores.label.GENERATEGDN"  text="default text"/>" class="btn btn-success" onClick="submitPag1(this.name);">&nbsp;
                            <input type="button" name="back" value="<spring:message code="stores.label.Back"  text="default text"/>" class="btn btn-success" onClick="submitPag2(this.name);">&nbsp;

                        </center>
                        <br>
                        <center><a href="" class="text2" onClick="printMrs();"> <spring:message code="stores.label.Print"  text="default text"/> </a> &nbsp;
                            <input type="hidden" value="" name="temp" ></center>
                        </c:if>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
