<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ page import="ets.domain.util.ThrottleConstants" %>


<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">
    function setValues(val) {

        // alert("vsl....."+val);
        var temp = val.split("~");

        document.getElementById("stateId").value = temp[0];
        document.getElementById("stateCode").value = temp[1];
    }
</script>
<script type="text/javascript">
    function validatePanNo()
    {
        txt = document.getElementById("panNo").value.toUpperCase();
        var regex = /[a-zA-Z]{3}[PCHFATBLJG]{1}[a-zA-Z]{1}[0-9]{4}[a-zA-Z]{1}$/;
        var pan = {P: "Personal" ,C: "Company", A: "Association of Persons (AOP)",B: "Body of Individuals (BOI)", F: "Firm",G: "Govt", H: "Hindu Undivided Family (HUF)", L: "Local Authority",T: "AOP (Trust)", J: "Artificial Juridical Person" };
        pan = pan[txt[3]];
        if (regex.test(txt))
        {
            if (pan != "undefined"){
                document.getElementById("panNo").value = txt.toUpperCase();
                if(pan == "Artificial Juridical Person"){
                    document.getElementById("organizationId").value = 10;
                }else if(pan == "AOP (Trust)"){
                    document.getElementById("organizationId").value = 9;
                }else if(pan == "Local Authority"){
                    document.getElementById("organizationId").value = 8;
                }else if(pan == "Hindu Undivided Family (HUF)"){
                    document.getElementById("organizationId").value = 7;
                }else if(pan == "Govt"){
                    document.getElementById("organizationId").value = 6;
                }else if(pan == "Firm"){
                    document.getElementById("organizationId").value = 5;
                }else if(pan == "Body of Individuals (BOI)"){
                    document.getElementById("organizationId").value = 4;
                }else if(pan == "Association of Persons (AOP)"){
                    document.getElementById("organizationId").value = 3;
                }else if(pan == "Company"){
                    document.getElementById("organizationId").value = 2;
                }else if(pan == "Personal"){
                    document.getElementById("organizationId").value = 1;
                }
//                alert(pan + " card detected");
                return true;
            }else{
                alert("PAN Card No Invalid ");
            return false;
            }
        } else{
            alert("Please enter pan no");
        return false;
        }
    }
    function validatePanNo() {
        var nPANNo = document.getElementById("panNo").value;
        if (nPANNo != "") {
            document.getElementById("panNo").value = nPANNo.toUpperCase();
            var ObjVal = nPANNo;
            var pancardPattern = /^([ABCEFGHLJPTD]{1})([A-Z]{2})([PCFHABTGL]{1})([A-Z]{1})(\d{4})([A-Z]{1})$/;
            var patternArray = ObjVal.match(pancardPattern);
            if (patternArray == null) {
                alert("PAN Card No Invalid ");
                return false;
            } else {
                return true;
            }
        } else {
            alert("Please enter pan no");
            return false;
        }
    }



    function validateGSTNo() {
        var nPANNo = document.getElementById("panNo").value;
        var gstNo = document.getElementById("gstNo").value;
        var stateCode = document.getElementById("stateCode").value;
        var companyType = document.getElementById("companyType").value;
        var tempSateCode = gstNo.substring(0, 2);
        var tempPanNo = gstNo.substring(2, 12).toUpperCase();
        
      

        if ((stateCode != tempSateCode || nPANNo != tempPanNo) && gstNo != '') {
            alert("Invalid GST No.Please check pan No and state code.");
            return false;
        } else {
            return true;
        }
    }
</script>
<script type = "text/javascript">
        function Disable_Space() 
        {
            if(event.keyCode == 32)
            {
                event.returnValue = false;
                return false;
            }
        }
    </script>
<script language="javascript">
    function submitPage() {
        var companyType = document.getElementById("companyType").value;
        if(companyType  == '1'){
         var gstNo = document.getElementById("gstNo").value;
         var length = gstNo.length;
            if(gstNo.length  < 15){
                alert("Length of the gstno. is :"+length+". Please enter the valid GST No.");
                 document.manufacturer.gstNo.focus();
            return;
            }
        var check = validatePanNo();
        var gstValidate = validateGSTNo();
        }
        else{
            var check = true;
        var gstValidate = true;
        }
        var pincode = document.getElementById("pinCode").value;
        if (pincode == '' || pincode == '0') {
            alert("Please Enter pincode");
            document.manufacturer.pincode.focus();
            return;
        }
        var custCity = document.getElementById("custCity").value;
        if (custCity == '' || pincode == '0') {
            alert("Please Enter City");
            document.manufacturer.custCity.focus();
            return;
        }
        //       var check = true;
        //       var gstValidate=true;
        //var gstExist = checkGstNoExists();
        //  alert("check:"+check);
        // alert("gstValidate"+gstValidate);
        // alert("gstExist"+gstExist);
        if (check == true && gstValidate == true) {
            var nPANNo = document.getElementById("panNo").value;
            var orgId = document.getElementById("organizationId").value;
            var companyType = document.getElementById("companyType").value;
            var gstNo = document.getElementById("gstNo").value;
          
            var stateId = document.getElementById("stateId").value;
//            alert(orgId);
//            alert(nPANNo.charAt(3).toUpperCase());
            if (companyType == '0') {
                alert("Company Type cannot be empty ");
                document.manufacturer.companyType.focus();
                return;
            }
            if (stateId == '' || stateId == '0') {
                alert("Billing State  cannot be empty ");
                document.manufacturer.stateIdTemp.focus();
                return;
            }
            
            if (orgId == '' || orgId == '0') {
                alert("organisation cannot be empty ");
                document.manufacturer.orgId.focus();
                return;
            }

            if (nPANNo.charAt(3).toUpperCase() == 'P' && orgId != '1') {
                alert("Please choose correct organisation name as individual.since 4th letter of Pan no is p ");
                document.manufacturer.organizationId.focus();
                return;
            } else if (nPANNo.charAt(3).toUpperCase() == 'C' && orgId != '2') {
                alert("Please choose correct organisation name as Company .since 4th letter of Pan no is C ");
                document.manufacturer.organizationId.focus();
                return;
            } else if (nPANNo.charAt(3).toUpperCase() == 'A' && orgId != '3') {
                alert("Please choose correct organisation name as Association of Persons (AOP).since 4th letter of Pan no is  " + nPANNo.charAt(3));
                document.manufacturer.organizationId.focus();
                return;
            } else if (nPANNo.charAt(3).toUpperCase() == 'B' && orgId != '4') {
                alert("Please choose correct organisation name as Body of Individuals (BOI).since 4th letter of Pan no is  " + nPANNo.charAt(3));
                document.manufacturer.organizationId.focus();
                return;
            } else if (nPANNo.charAt(3).toUpperCase() == 'F' && orgId != '5') {
                alert("Please choose correct organisation name as Firm.since 4th letter of Pan no is  " + nPANNo.charAt(3));
                document.manufacturer.organizationId.focus();
                return;
            } else if (nPANNo.charAt(3).toUpperCase() == 'G' && orgId != '6') {
                alert("Please choose correct organisation name as Government.since 4th letter of Pan no is  " + nPANNo.charAt(3));
                document.manufacturer.organizationId.focus();
                return;
            } else if (nPANNo.charAt(3).toUpperCase() == 'H' && orgId != '7') {
                alert("Please choose correct organisation name as HUF (Hindu Undivided Family).since 4th letter of Pan no is  " + nPANNo.charAt(3));
                document.manufacturer.organizationId.focus();
                return;
            } else if (nPANNo.charAt(3).toUpperCase() == 'L' && orgId != '8') {
                alert("Please choose correct organisation name as Local Authority.since 4th letter of Pan no is  " + nPANNo.charAt(3));
                document.manufacturer.organizationId.focus();
                return;
            } else if (nPANNo.charAt(3).toUpperCase() == 'T' && orgId != '9') {
                alert("Please choose correct organisation name as Trust(AOP).since 4th letter of Pan no is  " + nPANNo.charAt(3));
                document.manufacturer.organizationId.focus();
                return;
            } else {

            }
            if (orgId == '0') {
                alert("organization  cannot be empty ");
                document.manufacturer.organizationId.focus();
                return;
            }
            if (gstNo == '' && companyType == '1') {
                alert("GST No cannot be empty ");
                document.manufacturer.gstNo.focus();
                return;
            }

            document.manufacturer.action = '/throttle/addCustomer.do';
            document.manufacturer.submit();
        }
    }
</script>

<script language="javascript">

    $(document).ready(function () {
        $('#custName').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getCustomerNameDetails.do",
                    dataType: "json",
                    data: {
                        custName: request.term
                    },
                    success: function (data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function (data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });


    function checkCustomerNameExists(custName) {
//        alert("test");
        $.ajax({
            url: "/throttle/checkCustomerNameExists.do",
            dataType: "json",
            data: {
                custName: custName
            },
            success: function (data, textStatus, jqXHR) {
                var count = data.CustomerCount;
                if (count == 0) {
                    $("#customerStatus").text("");
                    $("#saveButton").show();
                } else {
                    $("#customerStatus").text("Customer Already Exists");
                    $("#saveButton").hide();
                }
            },
            error: function (data, type) {
                console.log(type);
            }

        });
    }

    function checkGstNoExists(gstNo) {

        // var gstNo = document.getElementById("gstNo").value;
//      alert("test"+gstNo);
        $.ajax({
            url: "/throttle/checkGstNoExists.do",
            dataType: "json",
            data: {
                gstNo: gstNo
            },
            success: function (data, textStatus, jqXHR) {
                var count = data.gstEixsts;
                if (count == 0) {
                    $("#SAVE").removeClass('hidden');

                } else {
                    alert("gst No already exists..");
                    $("gstNo").val("");
                    $("#SAVE").addClass('hidden');

                }
            },
            error: function (data, type) {
                console.log(type);
            }

        });
    }
</script>

<script>
    $(document).ready(function () {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#accountManager').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getAccountManager.do",
                    dataType: "json",
                    data: {
                        accountManagerName: request.term
                    },
                    success: function (data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            $('#accountManagerId').val('');
                            $('#accountManager').val('');
                        } else {
                            response(items);
                        }
                    },
                    error: function (data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                $("#accountManager").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('-');
                $itemrow.find('#accountManagerId').val(tmp[0]);
                $itemrow.find('#accountManager').val(tmp[1]);
                return false;
            }
        }).data("autocomplete")._renderItem = function (ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    });
</script>

<style>
    #index div {
        color:white;
    }
</style>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.Sales"  text="Sales"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.Sales"  text="Sales"/></a></li>
            <li class="active"><spring:message code="sales.label.AddCustomer"  text="AddCustomer"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">

            <body onload="setFocus();">
                <form name="manufacturer"  method="post"  >
                    <%@ include file="/content/common/message.jsp" %>
                    <br>
                    <input type="hidden" name="pageId" id="pageId" value="<c:out value="${uniquePageId}"/>"/>
                    <!--<table align="center" border="0" cellpadding="0" cellspacing="0" width="700" id="bg" class="border">-->
                    <table class="table table-bordered  mb30 table-hover" >
                        <center><span id='customerStatus' style="color: red;"></span></center>
                        <thead>
                        <th colspan="4" id="index" style="background-color:#5BC0DE;">
                            <center>ADD CUSTOMER</center>
                        </th>
                        </thead>

                        <tr>
                            <td><font color="red">*</font>Customer Name</td>
                            <td>
                                <input name="custName" id="custName" type="text" class="textbox"  style="width:200px;height:30px;" value="" onchange="checkCustomerNameExists(this.value)" maxlength="500" placeholder="Type customer name..." onpaste="return false;" required>
                                <input name="custCode" id="custCode" type="hidden" class="textbox" value="" maxlength="8" readonly >
                            </td>
                            <td><font color="red">*</font>GST Type</td>
                            <td>
                                <select class="textbox"  style="width:200px;height:30px;" name="companyType" id="companyType" required >

                                    <option value="0">--Select--</option>
                                    <option value="1">Registered</option>
                                    <option value="2">Unregistered</option>


                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><font color="red">*</font>PrimaryContractType1</td>
                            <td>
                                <select class="textbox"  style="width:200px;height:30px;" name="billingTypeId" id="billingTypeId" required>
                                    <c:if test="${billingTypeList != null}">
                                        <option value="">Choose One</option>
                                        <c:forEach items="${billingTypeList}" var="btl">
                                            <option value="<c:out value="${btl.billingTypeId}"/>" ><c:out value="${btl.billingTypeName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                            <td ></td>
                            <!--                            <td >PrimaryContractType2</td>-->
                            <td >
                                <input type="hidden" name="secondaryBillingTypeId" id="secondaryBillingTypeId" value="0"/>
                                <!--                                <select disabled class="textbox"  style="width:200px;height:30px;" name="secondaryBillingTypeId" id="secondaryBillingTypeId" >
                                                                     <option value="0">--select--</option>
                                <%--<c:if test="${billingTypeList != null}">--%>
                                   
                                <%--<c:forEach items="${billingTypeList}" var="btl">--%>
                                    <option value="<c:out value="${btl.billingTypeId}"/>" ><c:out value="${btl.billingTypeName}"/></option>
                                <%--</c:forEach>--%>
                                <%--</c:if>--%>
                            </select>-->
                            </td> 
                        </tr>
                        <tr>
                            <td><font color="red">*</font>Account Manager</td>
                            <td>
                                <select class="textbox"  style="width:200px;height:30px;" name="accountManagerId" id="accountManagerId" required>
                                    <c:if test="${accountManagerList != null}">
                                        <option value="">Choose One</option>
                                        <c:forEach items="${accountManagerList}" var="btl">
                                            <option value="<c:out value="${btl.empId}"/>" ><c:out value="${btl.empName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                            <td >Organization Name</td>
                            <td>
                                <select class="textbox"  style="width:200px;height:30px;" name="organizationId" id="organizationId" >
                                    <c:if test="${organizationList != null}">
                                        <option value="0">--Select--</option>
                                        <c:forEach items="${organizationList}" var="org">
                                            <option value="<c:out value="${org.orgId}"/>" ><c:out value="${org.orgName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                        </tr>
                        <tr>
                        <input name="paymentType" id="paymentType"hidden type="text" class="textbox"  value="1" >
                        <td><font color="red">*</font>Contactperson</td>
                        <td><input name="custContactPerson" id="custContactPerson" type="text" class="textbox"  style="width:200px;height:30px;" value="" onKeyPress="return onKeyPressBlockNumbers(event);" placeholder="Type contact person..."  required required></td>
                        <td><font color="red">*</font>Address</td>
                        <td><textarea class="textbox"  style="width:200px;height:30px;" name="custAddress" id="custAddress" placeholder="Type customer address..."  required onpaste="return false;"></textarea></td>
                        </tr>

                        <tr>
                            <td><font color="red">*</font>City</td>
                            <td><input name="custCity" id="custCity" type="text" class="textbox"  style="width:200px;height:30px;" value="" onKeyPress="return onKeyPressBlockNumbers(event);" placeholder="Type city name..."  required></td>
                            <td>Delivery  Address </td>
                            <td><textarea class="textbox"  style="width:200px;height:30px;" name="custAddresstwo" id="custAddresstwo" placeholder="Type delivery address..."  ></textarea></td>
                        </tr>
                        <tr>

                            <td>Additional  Address </td>
                            <td><textarea class="textbox"  style="width:200px;height:30px;" name="custAddressthree" id="custAddressthree" placeholder="Type additional address if any..."  ></textarea></td>
                            <td><font color="red">*</font>Phone No</td>
                            <td><input name="custPhone" id="custPhone" type="text" class="textbox"  style="width:200px;height:30px;" value="" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10" placeholder="Type landline no..."  required></td>
                        </tr>
                        <tr>

                        <input name="custState" id="custState" type="hidden" class="textbox"  style="width:200px;height:30px;" value="" onKeyPress="return onKeyPressBlockNumbers(event);" placeholder="Type state name..."  required>
                        <td><font color="red">*</font>Mobile No</td>
                        <td><input name="custMobile" id="custMobile" type="text" class="textbox"  style="width:200px;height:30px;" value="" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="12" placeholder="Type mobile no..."  required></td>
                        <td><font color="red">*</font>Email</td>
                        <td><textarea class="textbox"  style="width:200px;height:30px;" name="custEmail" id="custEmail" placeholder="Type Email Id with comma seperated"  ></textarea></td>
                        
                        <!--<td><input name="custEmail" id="custEmail" type="text" class="textbox"  style="width:200px;height:30px;" value="" maxlength="100" placeholder="Type email address..." required  ></td>-->
                        </tr>
                        <tr>
                            <td><font color="red">*</font>Customer Type</td>    
                            <td>  <select name="custTypeId" id="custTypeId" style="width:200px;height:30px;" onchange="showCredit(this.value);">
                                    <option value="0">---select---</option>
                                    <option value="1">Credit</option>
                                    <option value="2">PDA</option>

                                </select>       
                            </td>
                            <td><font color="red">*</font>ERP ID</td>
                            <td><input name="erpId" id="erpId" type="text" class="textbox"  style="width:200px;height:30px;" value="<c:out value="${cudl.erpId}"/>" maxlength="10" placeholder="Type erp id..."  required></td>

                        </tr>
                        <script>
                            function validateManager(value) {
                                if (value == "") {
                                    $("#accountManagerId").val('');
                                }
                            }
                            function showCredit(custType) {
                                if (custType == '1') {
                                    $("#creditLimit").show();
                                    $("#advanceAmount").hide();
                                    $("#advanceAmount").val(0);
                                    $("#creditDays").val(0);
                                    $("#availSpan").text('Fixed Credit Limit');
                                    $("#availSpan1").text('Avail. Credit Limit');

                                } else if (custType == '2') {
                                    $("#creditLimit").hide();
                                    $("#advanceAmount").show();
                                    $("#creditLimit").val(0);
                                    $("#creditDays").val(0);
                                    $("#fixedCreditLimit").val(0);
                                    $('#fixedCreditLimit').attr('readonly', 'true');
                                    $("#availSpan").text('Fixed PDA Amount');
                                    $("#availSpan1").text('PDA Amount');
                                }
                            }


                            function setAvailAMount(val) {
                                var custType = $("#creditLimit").val();
                                if (custType == '1') {
                                    $("#creditLimit").val(val);
                                } else {
                                    $("#creditLimit").val('0');
                                }
                            }
                        </script>
                        <tr height="35" >
                            <td ><span id="availSpan"><font color="red">*</font>Fixed Credit Limit</span></td>
                            <td ><input name="fixedCreditLimit" id="fixedCreditLimit" type="text" class="textbox"  style="width:200px;height:30px;" value="<c:out value="${cudl.fixedCreditLimit}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" onkeyup="setAvailAMount(this.val);" maxlength="10" ></td>  
                            <td><font color="red">*</font>Credit Days</td> 
                            <td> <input name="creditDays" id="creditDays" type="text" class="textbox"  style="width:200px;height:30px;" value="<c:out value="${cudl.creditDays}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10" ></td>

                        </tr>

                        <tr>
                            <td ><span id="availSpan1"><font color="red">*</font>Available Limit</span></td>
                            <td>  <input name="creditLimit" id="creditLimit" type="text" class="textbox"  style="width:200px;height:30px;" value="<c:out value="${cudl.creditLimit}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10" readonly>
                                <input type="hidden" name="advanceAmount" id="advanceAmount" type="form-control" class="textbox"  style="width:200px;height:30px;" value="<c:out value="${cudl.advanceAmount}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10" readonly>
                            </td>
                            <!--                             <td id="pdaLable" style="visibility:hidden;"><font color="red">*</font>PDA Amount</td>
                                                         <input type="text" name="advanceAmount" id="advanceAmount" type="form-control" class="textbox"  style="width:200px;height:30px;" value="<c:out value="${cudl.advanceAmount}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10" >
                            -->
                            <td><font color="red">*</font>Used Limit</td>
                            <td>
                                <input name="usedLimit" id="usedLimit"   type="text" class="textbox" value="0" style="width:200px;height:30px;" readonly>

                            </td>


                        </tr>
                        <!--                        <tr>
                                                <td id="pdaLable" style="visibility:hidden;"><font color="red">*</font>PDA Amount</td>
                                                 <input hidden name="advanceAmount" id="advanceAmount" type="form-control" class="textbox"  style="width:200px;height:30px;" value="<c:out value="${cudl.advanceAmount}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10" >
                                                  <td></td>
                                                     <td></td>
                                                 </tr> -->
                        <!--                        <tr> 
                                                    <td><font color="red">*</font>ERP ID</td>
                                                    <td><input name="erpId" id="erpId" type="text" class="textbox"  style="width:200px;height:30px;" value="<c:out value="${cudl.erpId}"/>" maxlength="10" placeholder="Type erp id..."  required></td>
                                                     <td></td>
                                                     <td></td>
                        
                                                </tr>-->
                        <tr>
                            <td><font color="red">*</font>PAN No</td>
                            <td><input name="panNo" id="panNo" type="text" class="textbox"  style="width:200px;height:30px;text-transform:uppercase;" value="" maxlength="10" onBlur="validatePanNo()" placeholder="Type pan no..."  required></td>
                            <!--<td><input name="panNo" id="panNo" type="text" class="textbox"  style="width:200px;height:30px;" value="" maxlength="10" placeholder="Type pan no..."  required></td>-->
                            <td><font color="red">*</font>PinCode</td>
                            <td><input name="pinCode" id="pinCode" type="text" class="textbox"  style="width:200px;height:30px;" value="" maxlength="10" onKeyPress="return onKeyPressBlockCharacters(event);" placeholder="Type pincode..."  required></td>

                        </tr>
                        <tr>
                            <td><font color="red">*</font>Billing State</td>
                            <td>
                                <input name="stateId" id="stateId"  type="hidden" class="textbox"  style="width:200px;height:30px;" >
                                <input name="stateCode" id="stateCode"  type="hidden" class="textbox"  style="width:200px;height:30px;" >
                                <select name="stateIdTemp" id="stateIdTemp" class="textbox"  style="width:200px;height:30px;" onchange="setValues(this.value);" required data-placeholder="Choose One" >
                                    <option value="0">Choose One</option>
                                    <c:if test = "${stateList != null}" >
                                        <c:forEach items="${stateList}" var="Type">
                                            <option value='<c:out value="${Type.stateId}" />~<c:out value="${Type.stateCode}" />'><c:out value="${Type.stateName}" /></option>
                                        </c:forEach >
                                    </c:if>
                                </select>
                            </td>
                            <td><font color="red">*</font>GST No</td>
                            <td>
                                <input name="gstNo" id="gstNo" required  type="text" class="textbox" maxlength="15" style="width:200px;height:30px;text-transform:uppercase;" onpaste="return false;" onChange="validateGSTNo();" autocomplete="off" onchange="checkGstNoExists(this.value);">
                                <!--<input name="gstNo" id="gstNo" required  type="text" class="textbox"  style="width:200px;height:30px;" >-->

                            </td>

                        <select name="customerTypeId" id="customerTypeId" style="visibility:hidden;">
                            <option value="1">Customer</option>
                            <option value="2">Consignee</option>
                            <option value="3">Consignor</option>
                        </select>

                        <select class="textbox"   name="customerGroup" id="customerGroup"  style="visibility:hidden;">
                            <c:if test="${customerGroup != null}">
                                <option value="0">NA</option>
                                <c:forEach items="${customerGroup}" var="cgl">
                                    <option value="<c:out value="${cgl.groupId}"/>" ><c:out value="${cgl.groupName}"/></option>
                                </c:forEach>
                            </c:if>
                        </select>

                        </tr>

                        <tr>
                            <td><font color="red">*</font>Billing Name and Address</td>
                            <td ><textarea name="billingNameAddress" id="billingNameAddress" class="textbox"  style="width:200px;height:30px;" placeholder="Type customer billing name and address..."  required  maxlength="100" onkeypress = "return Disable_Space();"></textarea></td>
                        </tr>
                        <tr>
                         <td><font color="red">*</font>GTA Type</td>    
                            <td>  <select name="gtaType" id="gtaType" style="width:200px;height:30px;">
                                    <option value="Y">Yes</option>
                                    <option selected value="N">No</option>

                                </select>       
                            </td>
                        </tr>
                    </table>
                    <br>
                    <center>
                        <input type="button" name="SAVE" value="SAVE" id="SAVE" class="btn btn-info" onClick="submitPage();" style="width:100px;height:35px;">

                        <input type="reset" class="btn btn-info" value="<spring:message code="sales.label.Clear"  text="default text"/>">

                    </center>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>
