<%-- 
    Document   : addGroup
    Created on : Jun 22, 2010, 12:37:53 PM
    Author     : Hari
--%>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
</head>
<script>
    function submitPage()
    {

        if(textValidation(document.addGroup.groupName,'Group Name')){

        return;
        }
        if(textValidation(document.addGroup.description,'Group Description')){

        return;
         }
        document.addGroup.action='/throttle/handleAddGroup.do';
        document.addGroup.submit();
    }
    function setFocus(){
        document.addGroup.groupName.focus();
        }

</script>


<script>
   function changePageLanguage(langSelection){
   if(langSelection== 'ar'){
   document.getElementById("pAlign").style.direction="rtl";
   }else if(langSelection== 'en'){
   document.getElementById("pAlign").style.direction="ltr";
   }
   }
 </script>

  <c:if test="${jcList != null}">
  <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
  </c:if>
      
  <span style="float: right">
	<a href="?paramName=en">English</a>
	|
	<a href="?paramName=ar">Arabic</a>
  </span>



<body onload="setFocus();">
<form name="addGroup" method="post" >
<%@ include file="/content/common/path.jsp" %>


<%@ include file="/content/common/message.jsp" %>

<table align="center" border="0" cellpadding="0" cellspacing="0" width="300" id="bg" class="border">
<tr>
<td colspan="2" class="contenthead" height="30"><div class="contenthead"><spring:message code="stores.label.AddPartsGroup"  text="default text"/>
</div></td>
</tr>
<%
    String classText = "";
    classText = "text2";

    %>
<tr>
<td class="<%=classText %>" height="30"><font color="red">*</font><spring:message code="stores.label.GroupName"  text="default text"/></td>
<td class="<%=classText %>" height="30"><input name="groupName" type="text" class="textbox" value=""></td>
</tr>
<%
   classText = "text1";
 %>
<tr>
<td class="<%=classText %>" height="30"><font color="red">*</font><spring:message code="stores.label.GroupDescription"  text="default text"/></td>
<td class="<%=classText %>" height="30"><textarea class="textbox"  name="description"></textarea></td>
</tr>
</table>
<br>
<center>
<input type="button" value="<spring:message code="stores.label.ADD"  text="default text"/>" class="button" onclick="submitPage();">
&emsp;<input type="reset" class="button" value="<spring:message code="stores.label.Clear"  text="default text"/>">
</center>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
