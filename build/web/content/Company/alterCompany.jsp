<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>

<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script>
    function submitPage()
    {
        if (textValidation(document.update.name, 'Company Name')) {
            return;
        } else if (textValidation(document.update.address, 'Address')) {
            return;
        } else if (document.update.companyTypeId.value == 0) {
            alert("Please enter the location");
            document.update.companyTypeId.focus();
            return;
        } else if (textValidation(document.update.city, 'City')) {
            return;
        } else if (textValidation(document.update.district, 'District')) {
            return;
        } else if (textValidation(document.update.stateId, 'State')) {
            return;
        } else if (numberValidation(document.update.pinCode, 'pinCode')) {
            return;
        } else if (numberValidation(document.update.phone1, 'phone1')) {
            return;
        } else if (numberValidation(document.update.organizationId, 'Organization Id')) {
            return;
        } else if (numberValidation(document.update.gstNo, 'Gst No')) {
            return;
        }
        if (document.update.phone2.value != "") {
            if (numberValidation(document.update.phone2, 'Phone2')) {
                return;
            }
        } else {
            document.update.phone2.value = parseInt(0);
        }
        if (document.update.fax.value != "") {
            if (isSpecialCharacter(document.update.fax.value)) {
                alert('Special Characters are Not allowed for Fax');
                document.update.fax.focus();
                return;
            }
        } else {
            document.update.fax.value = parseInt(0);
        }

        if (document.update.email.value != "") {
            if (isEmail(document.update.email.value)) {
                document.update.email.focus();
                return;
            }
            if (isSpecialCharacter(document.update.email.value)) {
                alert('Special Characters are Not allowed for Email');
                document.update.email.focus();
                return;
            }
        } else {
            document.update.email.value = "-";
        }
        if (document.update.web.value != "") {

            if (isSpecialCharacter(document.update.web.value)) {
                alert('Special Characters are Not allowed for Web');
                document.update.web.focus();
                return;
            }
        } else {
            document.update.web.value = "-";
        }
        if (document.update.number.value == 0) {
            document.update.action = '/throttle/alterCompany.do';
            document.update.submit();
        } else {
            alert("please Select Any One and then Proceed");
        }
    }


    var whitespace = " \t\n\r";

    function isEmpty(s) {
        var i = 0;
        if ((s == null) || (s.length == 0)) {
            return true;
        }
        for (i = 0; i < s.length; i++) {
            var c = s.charAt(i);
            if (whitespace.indexOf(c) == -1) {
                return false;
            }
        }
        return true;
    }
    function isChar(s) {
        if (!(/^-?\d+$/.test(s))) {
            return true;
        }
        return false;
    }
    function isEmail(s)
    {

        if (/[^@]+@[^@]+\.(com)|(co.in)$/.test(s))
            return false;
        alert("Email not in valid form!");
        return true;
    }

    function index() {

        document.update.number.value = 1;
    }

    function setFocus() {
        document.update.companyTypeId.focus();
    }

</script>    
<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
            setValues();
            getVehicleNos();">
    </c:if>
    <style>
        #index td {
            color:white;
        }
    </style>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.AlterCompany" text="default text"/></h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="default text"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.HRMS" text="default text"/></a></li>
                <li class=""><spring:message code="hrms.label.AlterCompany" text="default text"/></li>

            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">

                <body onload="setFocus();">
                    <form  name="update"  method="post">
                        <c:if test="${companyDetail!=null}">
                            <c:forEach items="${companyDetail}" var="comp" >    
                                <table class="table table-info mb30 table-hover">
                                    <!--                        <tr>
                                                                <Td height="30" width="500" align="center" colspan="2" class="contenthead"> <spring:message code="hrms.label.AlterCompany" text="default text"/></Td>
                                                            </tr> -->
                                    <tr height="30">
                                        <td ><font color="red">*</font><spring:message code="hrms.label.Company-Type" text="default text"/></td>
                                        <td > <select name="companyTypeId" class="form-control" style="width:260px;height:40px;">
                                                <c:if test="${companyTypes!=null}">                                        
                                                    <c:forEach items="${companyTypes}" var="compType" >  
                                                        <c:choose>
                                                            <c:when test="${compType.companyTypeId==comp.companyTypeId}" >
                                                                <option value="<c:out value="${compType.companyTypeId}"/>" selected > <c:out value="${compType.companyType}"/> </option>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <option value="<c:out value="${compType.companyTypeId}"/>" > <c:out value="${compType.companyType}"/> </option>                                            
                                                            </c:otherwise> 
                                                        </c:choose>
                                                    </c:forEach>                                           
                                                </c:if>            
                                            </select> 
                                        </td>
                                        <!--                        </tr>
                                                                <tr height="30">-->
                                        <td ><font color="red">*</font><spring:message code="hrms.label.Name" text="default text"/></td>
                                        <td ><input name="name" type="text" style="width:260px;height:40px;"  class="form-control" value="<c:out value="${comp.name}"/>" size="20" onchange="index();"></td>
                                    <input type="hidden" name="cmpId" value="<c:out value="${comp.cmpId}"/>"  >
                                    </tr>
                                    <tr height="30">
                                        <td ><font color="red">*</font><spring:message code="hrms.label.Address" text="default text"/></td>
                                        <td ><textarea class="form-control" cols="20" rows="2" size="20"  name="address" onchange="index();" style="width:260px;height:40px;"><c:out value="${comp.address}"/></textarea></td>
                                        <!--                        </tr>                       
                                                                <tr height="30">-->
                                        <td ><font color="red">*</font><spring:message code="hrms.label.City" text="default text"/></td>
                                        <td ><input name="city" style="width:260px;height:40px;" class="form-control" type="text" value="<c:out value="${comp.city}"/>" onchange="index();" size="20"></td>
                                    </tr>
                                    <tr height="30">
                                        <td ><font color="red">*</font><spring:message code="hrms.label.District" text="default text"/></td>
                                        <td ><input name="district" style="width:260px;height:40px;" class="form-control" type="text" value="<c:out value="${comp.district}"/>" onchange="index();" size="20"></td>
                                        <!--                        </tr>
                                                                <tr height="30">-->
<!--                                        <td ><font color="red">*</font><spring:message code="hrms.label.State" text="default text"/></td>
                                        <td ><input name="state" style="width:260px;height:40px;" class="form-control" type="text" value="<c:out value="${comp.state}"/>" onchange="index();" size="20"></td>-->
                                        <td><font color="red">*</font>Billing State</td>
                            <td>
                                <input name="stateIdTemp" id="stateIdTemp"  type="hidden" class="form-control" style="width:260px;height:40px;">
                                <select name="stateId" id="stateId" class="form-control" required data-placeholder="Choose One" style="width:260px;height:40px;">
                                    <option value="">Choose One</option>
                                    <c:if test = "${stateList != null}" >
                                        <c:forEach items="${stateList}" var="Type">
                                            <option value='<c:out value="${Type.stateId}" />'><c:out value="${Type.stateName}" /></option>
                                        </c:forEach >
                                    </c:if>
                                </select>
                            </td>
                            <script>
//                                document.getElementById("categoryName").value = '<c:out value="${getGstCategoryCode}"/>';
                                document.getElementById("stateId").value = '<c:out value="${comp.stateId}"/>';
                               
                            </script>
                                    </tr>
                                    <tr height="30">
                                        <td ><font color="red">*</font><spring:message code="hrms.label.Phone" text="default text"/>1</td>
                                        <td ><input name="phone1" class="form-control" style="width:260px;height:40px;" type="text" maxlength="20" onchange="index();" value="<c:out value="${comp.phone1}"/>" size="20"  ></td>
                                        <!--                        </tr>
                                                                <tr height="30">-->
                                        <td >&nbsp;&nbsp;<spring:message code="hrms.label.Pincode" text="default text"/></td>
                                        <td ><input name="pinCode" style="width:260px;height:40px;" class="form-control" type="text" value="<c:out value="${comp.pinCode}"/>" onchange="index();" size="20" maxlength="6"></td>

                                    </tr>
                                    <tr height="30">
                                        <td >&nbsp;&nbsp;<spring:message code="hrms.label.Phone" text="default text"/>2</td>
                                        <td ><input name="phone2" style="width:260px;height:40px;" class="form-control" type="text" maxlength="20" onchange="index();" value="<c:out value="${comp.phone2}"/>" size="20"></td>
                                        <!--                        </tr>
                                                                <tr height="30">-->
                                        <td >&nbsp;&nbsp;<spring:message code="hrms.label.Fax" text="default text"/></td>
                                        <td ><input name="fax" style="width:260px;height:40px;" class="form-control" type="text" maxlength="20" onchange="index();" value="<c:out value="${comp.fax}"/>" size="20"></td>
                                    </tr>
                                    <tr height="30">
                                        <td >&nbsp;&nbsp;<spring:message code="hrms.label.Email" text="default text"/></td>
                                        <td ><input name="email" class="form-control" style="width:260px;height:40px;" type="text" onchange="index();" value="<c:out value="${comp.email}"/>" size="20"></td>
                                        <!--                        </tr>
                                                                <tr height="30">-->
                                        <td >&nbsp;&nbsp;<spring:message code="hrms.label.Web" text="default text"/></td>
                                        <td ><input name="web" class="form-control" style="width:260px;height:40px;" type="text" onchange="index();" value="<c:out value="${comp.web}"/>" size="20"></td>
                                    </tr>
                                    <tr height="30">
                                        <td >&nbsp;&nbsp;<spring:message code="hrms.label.Status" text="default text"/></td>
                                        <td ><select name="status" class="form-control" onchange="index();" style="width:260px;height:40px;" >
                                                <c:choose>
                                                    <c:when test="${comp.status=='y' || comp.status=='Y'}" >
                                                        <option value="Y" selected><spring:message code="hrms.label.Active" text="default text"/></option>
                                                        <option value="N"><spring:message code="hrms.label.InActive" text="default text"/></option>                                            
                                                    </c:when>
                                                    <c:otherwise>
                                                        <option value="Y" ><spring:message code="hrms.label.Active" text="default text"/></option>
                                                        <option value="N" selected><spring:message code="hrms.label.InActive" text="default text"/></option>                                                                                        
                                                    </c:otherwise>    
                                                </c:choose>                                    
                                            </select></td>
                                    </tr>
                                    <tr>
                            <td >Organization Name</td>
                            <td>
                                <select class="form-control" name="organizationId" id="organizationId" style="width:260px;height:40px;">
                                    <c:if test="${organizationList != null}">
                                        <option value="">--Select--</option>
                                        <c:forEach items="${organizationList}" var="org">
                                            <option value="<c:out value="${org.orgId}"/>" ><c:out value="${org.orgName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                            <script>
                                document.getElementById("organizationId").value = '<c:out value="${comp.organizationId}"/>';
                               
                            </script>
                            <td><font color="red">*</font>GST No</td>
                            <td>
                                <input name="gstNo" id="gstNo"  type="text" class="form-control" style="width:260px;height:40px;" value="<c:out value="${comp.gstNo}"/>">
                            </td>
                            </tr>
                                    <input type="hidden" name="number" value="">
                                </table>
                            </c:forEach>    
                        </c:if>    
                        <br>
                        <br>
                        <center><input type="button" class="btn btn-success" value="<spring:message code="hrms.label.SAVE" text="default text"/>" onClick="submitPage();" /></center>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>