/*---------------------------------------------------------------------------
 * ServiceController.java
 * Mar 3, 2009
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
-------------------------------------------------------------------------*/
package ets.domain.service.web;

import ets.arch.exception.FPRuntimeException;
import ets.arch.web.BaseController;
import ets.domain.contract.business.ContractBP;
import ets.domain.service.business.ServiceBP;
import ets.domain.service.business.ServiceTO;
import ets.domain.users.business.LoginBP;
import ets.domain.util.FPLogUtils;
import ets.domain.util.ParveenErrorConstants;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;

/****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver              Date                      Author                    Change
 * ---------------------------------------------------------------------------
 * 1.0           Mar 3, 2009              vijay			       Created
 *
 ******************************************************************************/
public class ServiceController extends BaseController {

    ServiceCommand serviceCommand;
    ServiceBP serviceBP;
    LoginBP loginBP;
    ContractBP contractBP;

    public ServiceCommand getServiceCommand() {
        return serviceCommand;
    }

    public ContractBP getContractBP() {
        return contractBP;
    }

    public void setContractBP(ContractBP contractBP) {
        this.contractBP = contractBP;
    }

    public ServiceBP getServiceBP() {
        return serviceBP;
    }

    public void setServiceBP(ServiceBP serviceBP) {
        this.serviceBP = serviceBP;
    }

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    /**
     * This method used to bind the request values to the command object.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        binder.closeNoCatch();  initialize(request);
    }

    /**
     * This method caters to get free service
     *
     * @param 	request - Http request object
     *
     * @param 	response - Http response object
     *
     * @param	command - JobCommand object contains the request values.
     *
     * @return ModelAndView
     */
    public ModelAndView searchFreeService(HttpServletRequest request, HttpServletResponse reponse, ServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Service Plan >>Configure Free Service ";
        String pageTitle = "Free Service Plan";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Service-ConfigFreeService")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                path = "content/Service/ConfigFreeService.jsp";
                request.removeAttribute("freeServiceDetails");
                ArrayList mfrList = new ArrayList();
                mfrList = contractBP.getMfrList();
                request.setAttribute("mfrList", mfrList);
//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view free service   Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView freeServiceDetails(HttpServletRequest request, HttpServletResponse reponse, ServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        System.out.println("vijay start");
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Service Plan >>Configure Free Service";
        String pageTitle = "Free Service Plan";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/Service/ConfigFreeService.jsp";
            ArrayList mfrList = new ArrayList();
            ArrayList freeServiceDetails = new ArrayList();
            int modId = Integer.parseInt(request.getParameter("modId"));
            int mfrId = Integer.parseInt(request.getParameter("mfrId"));

            freeServiceDetails = serviceBP.getFreeServiceDetails(modId, mfrId);
            request.setAttribute("freeServiceDetails", freeServiceDetails);
            mfrList = contractBP.getMfrList();
            request.setAttribute("mfrList", mfrList);
            request.setAttribute("mfrId", request.getParameter("mfrId"));
            request.setAttribute("modId", request.getParameter("modId"));


        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view free service   Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView savefreeServiceDetails(HttpServletRequest request, HttpServletResponse reponse, ServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        serviceCommand = command;
        String path = "";
        String menuPath = "";
        menuPath = "Service Plan >>Configure Free Service >> Free Service Plan";
        String pageTitle = "Free Service Plan";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/Service/ConfigFreeService.jsp";
            ServiceTO serviceTO = null;
            ArrayList mfrList = new ArrayList();
            ArrayList freeServiceDetails = new ArrayList();
            ArrayList fServiceList = new ArrayList();
            int modId = Integer.parseInt(request.getParameter("modId"));
            int mfrId = Integer.parseInt(request.getParameter("mfrId"));
            int userId = (Integer) session.getAttribute("userId");
            System.out.println("vijaykanth start");
            System.out.println(request.getParameter("modId"));
            System.out.println(request.getParameter("mfrId"));
            String[] km = null;
            km = request.getParameterValues("km");
            String[] hm = null;
            hm = request.getParameterValues("hm");
            String[] month = null;
            month = request.getParameterValues("month");
            String[] desc = null;
            desc = request.getParameterValues("desc");
            String[] fServiceIds = request.getParameterValues("fserviceIds");
            String[] status = request.getParameterValues("activeInds");
            String[] index = request.getParameterValues("selectedindex");
            int temp = 0;
            if (km != null) {
                System.out.println("km length" + km.length);
                System.out.println("km length" + index.length);
                for (int i = 0; i < index.length; i++) {
                    serviceTO = new ServiceTO();
                    System.out.println("index1s=" + index[i]);
                    temp = Integer.parseInt(index[i]);
                    System.out.println("value1s=" + km[temp]);
                    System.out.println("value2s=" + month[temp]);
                    System.out.println("value3s=" + desc[temp]);
                    System.out.println("status=" + status[temp]);
                    System.out.println("value4s=" + fServiceIds[temp]);
                    serviceTO.setKm(Integer.parseInt(km[temp]));
                    serviceTO.setHm(Integer.parseInt(hm[temp]));
                    System.out.println("hm=" + Integer.parseInt(hm[temp]));
                    serviceTO.setFserviceId(Integer.parseInt(fServiceIds[temp]));
                    serviceTO.setMonth(Integer.parseInt(month[temp]));
                    serviceTO.setStatus(status[temp]);
                    serviceTO.setDesc(desc[temp]);
                    fServiceList.add(serviceTO);
                }
                int status1 = 0;
                status1 = serviceBP.saveFreeServiceDetails(fServiceList, userId, modId, mfrId);
            }

            request.setAttribute("mfrId", request.getParameter("mfrId"));
            request.setAttribute("modId", request.getParameter("modId"));
            freeServiceDetails = serviceBP.getFreeServiceDetails(modId, mfrId);
            request.setAttribute("freeServiceDetails", freeServiceDetails);
            mfrList = contractBP.getMfrList();
            request.setAttribute("mfrList", mfrList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view free service   Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method caters to view maintance details
     *
     * @param 	request - Http request object
     *
     * @param 	response - Http response object
     *
     * @param	command - JobCommand object contains the request values.
     *
     * @return ModelAndView
     */
    public ModelAndView manageMaintenance(HttpServletRequest request, HttpServletResponse reponse, ServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Service Plan >>Periodic Service >>Manage Maintance";
        String pageTitle = "Manage Maintance";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Service-PeriodicService")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                path = "content/Maintenance/manageMaintenance.jsp";
                ArrayList maintenanceDetails = new ArrayList();
                maintenanceDetails = serviceBP.getMaintenanceDetails();
                request.setAttribute("maintenanceDetails", maintenanceDetails);
//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view maintance   Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView addMaintenancePage(HttpServletRequest request, HttpServletResponse reponse, ServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Service Plan >>Periodic Service >>Add Maintance";
        String pageTitle = "Add Maintance";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            path = "content/Maintenance/addMaintenance.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view maintance add page   Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView insertMaintenance(HttpServletRequest request, HttpServletResponse reponse, ServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        serviceCommand = command;
        ServiceTO serviceTO = new ServiceTO();
        menuPath = "Service Plan >>Periodic Service >>Manage Maintance";
        String pageTitle = "Manage Maintance";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            System.out.println(serviceCommand.getServiceName());
            System.out.println(serviceCommand.getDescription());
            int userId = (Integer) session.getAttribute("userId");
            path = "content/Maintenance/manageMaintenance.jsp";
            if (serviceCommand.getServiceName() != null && serviceCommand.getServiceName() != "") {
                serviceTO.setServiceName(serviceCommand.getServiceName());
                System.out.println("hai");
            }
            if (serviceCommand.getDescription() != null && serviceCommand.getDescription() != "") {
                serviceTO.setDesc(serviceCommand.getDescription());
            }
            serviceTO.setUserId(userId);
            int status = 0;
            status = serviceBP.insertMaintance(serviceTO);
            ArrayList maintenanceDetails = new ArrayList();
            maintenanceDetails = serviceBP.getMaintenanceDetails();
            request.setAttribute("maintenanceDetails", maintenanceDetails);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to add maintance   Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView alterMaintenancePage(HttpServletRequest request, HttpServletResponse reponse, ServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Service Plan >>Periodic Service >>Alter Maintance";
        String pageTitle = "Alter Maintance";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/Maintenance/alterMaintenance.jsp";
            ArrayList maintenanceDetails = new ArrayList();
            maintenanceDetails = serviceBP.getMaintenanceDetails();
            request.setAttribute("maintenanceDetails", maintenanceDetails);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view maintance alter page   Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView updateMaintenanceDetails(HttpServletRequest request, HttpServletResponse reponse, ServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        serviceCommand = command;
        ServiceTO serviceTO = new ServiceTO();
        menuPath = "Service Plan >>Periodic Service >>Manage Maintance";
        String pageTitle = "Manage Maintance";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/Maintenance/manageMaintenance.jsp";
            int userId = (Integer) session.getAttribute("userId");
            String[] serviceName = serviceCommand.getServiceNames();
            String[] serviceId = serviceCommand.getServiceIds();
            String[] astatus = serviceCommand.getActiveInds();
            String[] desc = serviceCommand.getDesc();
            String[] index = serviceCommand.getSelectedindex();


            int status = 0;

            status = serviceBP.updateMaintance(serviceName, serviceId, astatus, desc, index, userId);

            ArrayList maintenanceDetails = new ArrayList();
            maintenanceDetails = serviceBP.getMaintenanceDetails();
            request.setAttribute("maintenanceDetails", maintenanceDetails);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to update maintance   Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView updateJobCardServiceCheckList(HttpServletRequest request, HttpServletResponse reponse, ServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        serviceCommand = command;
        ServiceTO serviceTO = new ServiceTO();
        menuPath = "Service Plan >>Periodic Service >>Manage Maintance";
        String pageTitle = "Manage Maintance";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            int userId = (Integer) session.getAttribute("userId");
            int jobCardId = Integer.parseInt(request.getParameter("jobCardId"));
            int serviceId = Integer.parseInt(request.getParameter("serviceId"));
            String[] checkListIds = request.getParameterValues("checkListIds");
            String[] checkedStatus = request.getParameterValues("checkedStatus");
            String[] servicedStatus = request.getParameterValues("servicedStatus");
            String[] remarks = request.getParameterValues("remarks");
            


            int status = 0;

            status = serviceBP.updateJobCardServiceCheckList(jobCardId, serviceId, checkListIds, checkedStatus, servicedStatus,remarks, userId);
            path = "content/Maintenance/jobCardServiceCheckList.jsp";

            ArrayList checkList = new ArrayList();
            checkList = serviceBP.getJobCardCheckList(jobCardId,serviceId);

            System.out.println("checkList size" + checkList.size());

            request.setAttribute("checkList", checkList);
            request.setAttribute("jobCardId", jobCardId);
            request.setAttribute("serviceId", serviceId);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to update maintance   Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView configMaintenancePage(HttpServletRequest request, HttpServletResponse reponse, ServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Service Plan >>Periodic Service >>Config Maintenance";
        String pageTitle = "Config Maintenance";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/Maintenance/configMaintenance.jsp";
            ArrayList mfrList = new ArrayList();
            ArrayList vtypeList = new ArrayList();
            mfrList = serviceBP.getMfrList();
            System.out.println("mfrList" + mfrList.size());
            vtypeList = serviceBP.getVehicleTypes();
            System.out.println("vList" + vtypeList.size());

            request.setAttribute("vtypeList", vtypeList);
            request.setAttribute("mfrList", mfrList);
            request.removeAttribute("maintenanceDetails");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view maintance config page   Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView searchConfigMaintenance(HttpServletRequest request, HttpServletResponse reponse, ServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        serviceCommand = command;
        ServiceTO serviceTO = new ServiceTO();
        menuPath = "Service Plan >>Periodic Service >>Config Maintenance";
        String pageTitle = "Manage Maintance";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/Maintenance/configMaintenance.jsp";
            int userId = (Integer) session.getAttribute("userId");
            if (serviceCommand.getMfrId() != null && serviceCommand.getMfrId() != "") {
                serviceTO.setMfrId(Integer.parseInt(serviceCommand.getMfrId()));
                System.out.println("hai");
            }
            if (serviceCommand.getModId() != null && serviceCommand.getModId() != "") {
                serviceTO.setModId(Integer.parseInt(serviceCommand.getModId()));
                System.out.println("hai");
            }
            if (serviceCommand.getVtypeId() != null && serviceCommand.getVtypeId() != "") {
                serviceTO.setVtypeId(Integer.parseInt(serviceCommand.getVtypeId()));
            }
            serviceTO.setUserId(userId);
            int status = 0;
            ArrayList maintenanceDetails = new ArrayList();
            maintenanceDetails = serviceBP.configMaintenanceDetails(serviceTO);

            ArrayList mfrList = new ArrayList();
            ArrayList vtypeList = new ArrayList();
            mfrList = serviceBP.getMfrList();
            System.out.println("mfrList" + mfrList.size());
            vtypeList = serviceBP.getVehicleTypes();
            System.out.println("vList" + vtypeList.size());

            request.setAttribute("vtypeList", vtypeList);
            request.setAttribute("mfrList", mfrList);
            request.setAttribute("maintenanceDetails", maintenanceDetails);
            request.setAttribute("mfrId", serviceCommand.getMfrId());
            request.setAttribute("modId", serviceCommand.getModId());
            request.setAttribute("vtypeId", serviceCommand.getVtypeId());


        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to add maintance   Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method caters to update confi maintance details
     *
     * @param 	request - Http request object
     *
     * @param 	response - Http response object
     *
     * @param	command - JobCommand object contains the request values.
     *
     * @return ModelAndView
     */
    public ModelAndView updateConfigMaintenance(HttpServletRequest request, HttpServletResponse reponse, ServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        serviceCommand = command;
        ServiceTO serviceTO = new ServiceTO();
        menuPath = "Service Plan >>Periodic Service >>Config Maintance";
        String pageTitle = "Manage Maintance";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {


            path = "content/Maintenance/configMaintenance.jsp";

            int userId = (Integer) session.getAttribute("userId");


            if (serviceCommand.getMfrId() != null && serviceCommand.getMfrId() != "") {
                serviceTO.setMfrId(Integer.parseInt(serviceCommand.getMfrId()));
                System.out.println(" in update hai");
            }
            if (serviceCommand.getModId() != null && serviceCommand.getModId() != "") {
                serviceTO.setModId(Integer.parseInt(serviceCommand.getModId()));
                System.out.println("hai");
            }
            if (serviceCommand.getVtypeId() != null && serviceCommand.getVtypeId() != "") {
                serviceTO.setVtypeId(Integer.parseInt(serviceCommand.getVtypeId()));
            }
            serviceTO.setUserId(userId);
            String[] serviceIds = request.getParameterValues("serviceIds");
            String[] km = request.getParameterValues("km");
            String[] hourMeter = request.getParameterValues("hourMeter");
            String[] month = request.getParameterValues("month");
            String[] labour = request.getParameterValues("labour");
            String[] selectedindex = request.getParameterValues("selectedIndex");

            int status = 0;
            status = serviceBP.updateConfigMaintenance(serviceTO, serviceIds, km, hourMeter, month, labour, selectedindex);
            ArrayList mfrList = new ArrayList();
            ArrayList vtypeList = new ArrayList();
            mfrList = serviceBP.getMfrList();
            System.out.println("mfrList" + mfrList.size());
            vtypeList = serviceBP.getVehicleTypes();
            System.out.println("vList" + vtypeList.size());
            request.setAttribute("vtypeList", vtypeList);
            request.setAttribute("mfrList", mfrList);
            request.setAttribute("mfrId", serviceCommand.getMfrId());
            request.setAttribute("modId", serviceCommand.getModId());
            request.setAttribute("vtypeId", serviceCommand.getVtypeId());
            request.removeAttribute("maintenanceDetails");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to update config maintance   Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView manageProblemActivity(HttpServletRequest request, HttpServletResponse reponse, ServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Service Plan >> Activities ";
        String pageTitle = "Manage Problem Activity";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Service-ProblemActivity")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                path = "content/Maintenance/manageProblemActivity.jsp";
                ArrayList secList = new ArrayList();
                secList = serviceBP.getSections();
                System.out.println("secList" + secList.size());
                ArrayList mfrList = new ArrayList();
                mfrList = contractBP.getMfrList();
                request.setAttribute("mfrList", mfrList);

                request.setAttribute("sections", secList);
                request.removeAttribute("activityDetails");
//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view manage activity page   Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView manageProblemActivityCause(HttpServletRequest request, HttpServletResponse reponse, ServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Service Plan >>Problem Activities >>Manage Problem Activity Cause";
        String pageTitle = "Manage Problem Activity";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Service-ProblemActivity")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                path = "content/Maintenance/manageProblemActivityCause.jsp";
                ArrayList secList = new ArrayList();
                secList = serviceBP.getSections();
                System.out.println("secList" + secList.size());

                request.setAttribute("sections", secList);
                request.removeAttribute("activityDetails");
//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view manage activity page   Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method caters to get section problems
     *
     * @param 	request - Http request object
     *
     * @param 	response - Http response object
     *
     * @param	command - JobCommand object contains the request values.
     *
     * @return ModelAndView
     */
    public void getSecProblems(HttpServletRequest request, HttpServletResponse response, ServiceCommand command) throws IOException {
        String path = "";

        HttpSession session = request.getSession();
        PrintWriter pw = response.getWriter();

        ServiceTO serviceTO = null;
        ArrayList probDetails = new ArrayList();
        try {
            int secId = 0;
            System.out.println("sectionId" + request.getParameter("secId"));
            secId = Integer.parseInt(request.getParameter("secId"));
            System.out.println("secId" + secId);
            String returnValue = "";
            probDetails = serviceBP.getProblems(secId);
            Iterator itr = probDetails.iterator();
            while (itr.hasNext()) {
                serviceTO = (ServiceTO) itr.next();
                returnValue = returnValue + "," + serviceTO.getProbDetails();
                System.out.println("returnValue" + returnValue);
            }
            pw.print(returnValue);

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to get problems-> " + exception);
            exception.printStackTrace();
        }

    }

    public ModelAndView getProblemActivity(HttpServletRequest request, HttpServletResponse reponse, ServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        serviceCommand = command;
        String menuPath = "";
        menuPath = "Service Plan >>Problem Activities >>Manage Problem Activity";
        String pageTitle = "Manage Problem Activity";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ServiceTO serviceTO = null;
            ArrayList secList = new ArrayList();
            ArrayList activityDetails = new ArrayList();
            path = "content/Maintenance/manageProblemActivity.jsp";

            int secId = Integer.parseInt(request.getParameter("secId"));
            System.out.println("secId1:" + secId);
            System.out.println("secId2:" + serviceCommand.getSecId());
            int groupId = Integer.parseInt(request.getParameter("groupId"));
            System.out.println("probId1:" + groupId);
            System.out.println("probId2:" + serviceCommand.getProbId());

            activityDetails = serviceBP.getProblemActivities(secId, groupId);
            secList = serviceBP.getSections();
            System.out.println("secList" + secList.size());
            request.setAttribute("sections", secList);
            request.setAttribute("activityDetails", activityDetails);
            request.setAttribute("secId", request.getParameter("secId"));
            request.setAttribute("groupId", request.getParameter("groupId"));
            ArrayList mfrList = new ArrayList();
            mfrList = contractBP.getMfrList();
            request.setAttribute("mfrList", mfrList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view manage activity page   Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveProblemActivity(HttpServletRequest request, HttpServletResponse reponse, ServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        serviceCommand = command;
        String path = "";
        String menuPath = "";
        menuPath = "Service Plan >>Configure Free Service >> Free Service Plan";
        String pageTitle = "Free Service Plan";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ServiceTO serviceTO = new ServiceTO();
            path = "content/Maintenance/manageProblemActivity.jsp";
            int userId = (Integer) session.getAttribute("userId");
            int secId = Integer.parseInt(request.getParameter("secId"));
            int groupId = Integer.parseInt(request.getParameter("groupId"));

            serviceTO.setSecId(secId);
            serviceTO.setGroupId(groupId);
            serviceTO.setUserId(userId);

            String[] activityIds = serviceCommand.getActivityId();
            String[] activityNames = serviceCommand.getActivityName();
            String[] desc = serviceCommand.getDesc();
            String[] activeStatus = serviceCommand.getActiveInds();
            String[] selectedindex = serviceCommand.getSelectedIndex();
            String[] labour = request.getParameterValues("labour");
            String[] labourTime = request.getParameterValues("labourTime");
            String groupVal = request.getParameter("groupVal");
            System.out.println("groupVal:"+groupVal);

            int status = 0;

            status = serviceBP.saveProblemActivity(serviceTO, activityIds, labour, labourTime, groupVal,activityNames, desc, activeStatus, selectedindex);
            ArrayList secList = new ArrayList();
            secList = serviceBP.getSections();
            System.out.println("secList" + secList.size());
            request.setAttribute("sections", secList);
            request.setAttribute("secId", request.getParameter("secId"));
            request.setAttribute("groupId", request.getParameter("groupId"));
            ArrayList mfrList = new ArrayList();
            mfrList = contractBP.getMfrList();
            request.setAttribute("mfrList", mfrList);
            request.setAttribute("successMessage", "saved successfully");

            request.removeAttribute("activityDetails");
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to save problem activity service   Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveProblemActivityCause(HttpServletRequest request, HttpServletResponse reponse, ServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        serviceCommand = command;
        ModelAndView mv = null;
        String path = "";
        String menuPath = "";
        menuPath = "Service Plan >>Configure Free Service >> Free Service Plan";
        String pageTitle = "Free Service Plan";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ServiceTO serviceTO = new ServiceTO();
            path = "content/Maintenance/manageProblemActivityCause.jsp";
            int userId = (Integer) session.getAttribute("userId");
            int secId = Integer.parseInt(request.getParameter("secId"));
            int probId = Integer.parseInt(request.getParameter("probId"));
            int index = 0;

            String[] activityNames = serviceCommand.getActivityName();
            String[] causeNames = serviceCommand.getCauseName();
            String[] selectedindex = serviceCommand.getSelectedindex();

            int status = 0;
            for (int i = 0; i < selectedindex.length; i++) {
                index = Integer.parseInt(selectedindex[i]);
                status = serviceBP.insertProblemActivityCauses(activityNames[index], causeNames[index], probId, userId);
            }
            ArrayList secList = new ArrayList();
            secList = serviceBP.getSections();
            System.out.println("secList" + secList.size());
            request.setAttribute("sections", secList);
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Problem and cause added successfully");
            request.setAttribute("secId", request.getParameter("secId"));
            request.setAttribute("probId", request.getParameter("probId"));

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to save problem activity service   Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method caters to get activities
     *
     * @param 	request - Http request object
     *
     * @param 	response - Http response object
     *
     * @param	command - JobCommand object contains the request values.
     *
     * @return ModelAndView
     */
    public void getActivitis(HttpServletRequest request, HttpServletResponse response, ServiceCommand command) throws IOException {
        String path = "";

        HttpSession session = request.getSession();
        PrintWriter pw = response.getWriter();

        ServiceTO serviceTO = null;
        ArrayList activityDetails = new ArrayList();
        try {
            int probId = 0;
            probId = Integer.parseInt(request.getParameter("probId"));
            int secId = Integer.parseInt(request.getParameter("secId"));
            System.out.println("probId" + probId);
            String returnValue = "";
            activityDetails = serviceBP.getProblemActivities(secId, probId);
            String details = "";
            Iterator itr = activityDetails.iterator();
            while (itr.hasNext()) {
                serviceTO = new ServiceTO();
                serviceTO = (ServiceTO) itr.next();
                details = serviceTO.getActivityId() + "-" + serviceTO.getActivityName();
                returnValue = returnValue + "," + details;
                System.out.println("returnValue" + returnValue);
            }
            pw.print(returnValue);

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to get problem activities-> " + exception);
            exception.printStackTrace();
        }

    }

    public ModelAndView configLabourRatePage(HttpServletRequest request, HttpServletResponse reponse, ServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Service Plan >>Configure Labour >>Config Labour Charge";
        String pageTitle = "Config Labour Charge";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Service-ConfigRate")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                path = "content/Maintenance/configLabourRate.jsp";
                ArrayList secList = new ArrayList();
                secList = serviceBP.getSections();
                System.out.println("secList" + secList.size());
                request.setAttribute("sections", secList);
                ArrayList mfrList = new ArrayList();
                mfrList = contractBP.getMfrList();
                request.setAttribute("mfrList", mfrList);

                request.removeAttribute("labourRateDetails");
//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view  config labour Rate Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView searchLabourRates(HttpServletRequest request, HttpServletResponse reponse, ServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        serviceCommand = command;
        ServiceTO serviceTO = new ServiceTO();
        menuPath = "Service Plan >>Periodic Service >>Config Maintenance";
        String pageTitle = "Manage Maintance";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/Maintenance/configMaintenance.jsp";
            int userId = (Integer) session.getAttribute("userId");
            if (serviceCommand.getSecId() != null && serviceCommand.getSecId() != "") {
                serviceTO.setSecId(Integer.parseInt(serviceCommand.getSecId()));
                System.out.println("serviceTO.getSecId():"+serviceTO.getSecId());
            }
            if (serviceCommand.getMfrId() != null && serviceCommand.getMfrId() != "") {
                serviceTO.setMfrId(Integer.parseInt(serviceCommand.getMfrId()));
            }

            serviceTO.setModelId(Integer.parseInt(request.getParameter("modelId")));

            serviceTO.setUserId(userId);
            int status = 0;
            ArrayList labourRates = new ArrayList();
            labourRates = serviceBP.configLabourRate(serviceTO);

            path = "content/Maintenance/configLabourRate.jsp";
            ArrayList secList = new ArrayList();
            secList = serviceBP.getSections();

            ArrayList mfrList = new ArrayList();
            mfrList = contractBP.getMfrList();
            request.setAttribute("mfrList", mfrList);

            System.out.println("secList" + secList.size());
            request.setAttribute("sections", secList);
            request.setAttribute("labourRateDetails", labourRates);
            request.setAttribute("secId", serviceCommand.getSecId());
            request.setAttribute("mfrId", serviceCommand.getMfrId());
            request.setAttribute("modelId", serviceTO.getModelId());
            ArrayList modelList = new ArrayList();
            int mfrId = Integer.parseInt(request.getParameter("mfrId"));
            modelList = serviceBP.getModels(mfrId);
            request.setAttribute("modelList", modelList);

            //request.setAttribute("activityId",request.getParameter("activityId"));


        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to add maintance   Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView searchLabourRatesInJcBill(HttpServletRequest request, HttpServletResponse reponse, ServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        serviceCommand = command;
        ServiceTO serviceTO = new ServiceTO();
        menuPath = "Service Plan >>Periodic Service >>Config Maintenance";
        String pageTitle = "Manage Maintance";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/Maintenance/configMaintenance.jsp";
            int userId = (Integer) session.getAttribute("userId");
            if (serviceCommand.getSecId() != null && serviceCommand.getSecId() != "") {
                serviceTO.setSecId(Integer.parseInt(serviceCommand.getSecId()));
                System.out.println("hai");
            }
            if (serviceCommand.getProbId() != null && serviceCommand.getProbId() != "") {
                serviceTO.setProbId(Integer.parseInt(serviceCommand.getProbId()));
            }

            serviceTO.setActivityId(Integer.parseInt(request.getParameter("actvtyId")));
            serviceTO.setUserId(userId);
            int status = 0;
            ArrayList labourRates = new ArrayList();
            labourRates = serviceBP.configLabourRate(serviceTO);

            path = "content/Maintenance/configLabourRate.jsp";
            ArrayList secList = new ArrayList();
            secList = serviceBP.getSections();
            System.out.println("secList" + secList.size());
            request.setAttribute("sections", secList);
            request.setAttribute("labourRateDetails", labourRates);
            request.setAttribute("secId", serviceCommand.getSecId());
            request.setAttribute("probId", serviceCommand.getProbId());
            request.setAttribute("activityId", request.getParameter("actvtyId"));


        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to add maintance   Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method caters to update confi maintance details
     *
     * @param 	request - Http request object
     *
     * @param 	response - Http response object
     *
     * @param	command - JobCommand object contains the request values.
     *
     * @return ModelAndView
     */
    public ModelAndView updateLabourRates(HttpServletRequest request, HttpServletResponse reponse, ServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        serviceCommand = command;
        ServiceTO serviceTO = new ServiceTO();
        menuPath = "Service Plan >>Activity Rates";
        String pageTitle = "Manage Maintenance";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {


            path = "content/Maintenance/configMaintenance.jsp";

            int userId = (Integer) session.getAttribute("userId");
            if (serviceCommand.getSecId() != null && serviceCommand.getSecId() != "") {
                serviceTO.setSecId(Integer.parseInt(serviceCommand.getSecId()));
                System.out.println("hai");
            }
            if (serviceCommand.getProbId() != null && serviceCommand.getProbId() != "") {
                serviceTO.setProbId(Integer.parseInt(serviceCommand.getProbId()));
            }

            serviceTO.setActivityId(Integer.parseInt(request.getParameter("activityId")));
            serviceTO.setRatePerHour(request.getParameter("basicCharge"));
            serviceTO.setUserId(userId);

            String[] modIds = request.getParameterValues("modId");
            String[] activityIds = request.getParameterValues("activityId");
            String[] mfrIds = request.getParameterValues("manfrId");
            String[] labour = request.getParameterValues("labour");
            String[] labourTime = request.getParameterValues("labourTime");

            String[] selectedindex = request.getParameterValues("selectedindex");
            int status = 0;
            status = serviceBP.updateLabourRates(serviceTO,activityIds, labourTime,modIds, mfrIds, labour, selectedindex);
            path = "content/Maintenance/configLabourRate.jsp";
            ArrayList secList = new ArrayList();
            secList = serviceBP.getSections();
            System.out.println("secList" + secList.size());
            request.setAttribute("sections", secList);
            request.removeAttribute("labourRateDetails");
            request.setAttribute("secId", serviceCommand.getSecId());
            request.setAttribute("probId", serviceCommand.getProbId());
            request.setAttribute("activityId", request.getParameter("activityId"));
            ArrayList mfrList = new ArrayList();
            mfrList = contractBP.getMfrList();
            request.setAttribute("mfrList", mfrList);
            ArrayList modelList = new ArrayList();
            int mfrId = Integer.parseInt(request.getParameter("mfrId"));
            modelList = serviceBP.getModels(mfrId);
            request.setAttribute("modelList", modelList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to update config maintance   Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView manageProblemCause(HttpServletRequest request, HttpServletResponse reponse, ServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Service Plan >>Problem Causes >>Manage Problem Causes";
        String pageTitle = "Manage Problem Causes";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Service-ProblemCause")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                path = "content/Maintenance/manageProblemCause.jsp";
                ArrayList secList = new ArrayList();
                secList = serviceBP.getSections();
                System.out.println("secList" + secList.size());

                request.setAttribute("sections", secList);
                request.removeAttribute("activityDetails");
//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view manage cause page   Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView getServiceCheckList(HttpServletRequest request, HttpServletResponse reponse, ServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Service Plan >>Problem Causes >>Manage Problem Causes";
        String pageTitle = "Manage Problem Causes";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Service-ProblemCause")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                path = "content/Maintenance/jobCardServiceCheckList.jsp";

                int jobCardId = Integer.parseInt(request.getParameter("jobCardId"));
                int serviceId = Integer.parseInt(request.getParameter("serviceId"));

                ArrayList checkList = new ArrayList();
                checkList = serviceBP.getJobCardCheckList(jobCardId,serviceId);
                
                System.out.println("checkList size" + checkList.size());

                request.setAttribute("checkList", checkList);
                request.setAttribute("jobCardId", jobCardId);
                request.setAttribute("serviceId", serviceId);

//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view manage cause page   Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView getProblemCause(HttpServletRequest request, HttpServletResponse reponse, ServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        serviceCommand = command;
        String menuPath = "";
        menuPath = "Service Plan >>Problem Cause >>Manage Problem Cause";
        String pageTitle = "Manage Problem Cause";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ServiceTO serviceTO = null;
            ArrayList secList = new ArrayList();
            ArrayList activityDetails = new ArrayList();
            path = "content/Maintenance/manageProblemCause.jsp";

            int secId = Integer.parseInt(request.getParameter("secId"));
            System.out.println("secId1:" + secId);
            System.out.println("secId2:" + serviceCommand.getSecId());
            int probId = Integer.parseInt(request.getParameter("probId"));
            System.out.println("probId1:" + probId);
            System.out.println("probId2:" + serviceCommand.getProbId());

            activityDetails = serviceBP.getProblemCauses(probId);
            secList = serviceBP.getSections();
            System.out.println("secList" + secList.size());
            request.setAttribute("sections", secList);
            request.setAttribute("activityDetails", activityDetails);
            request.setAttribute("secId", request.getParameter("secId"));
            request.setAttribute("probId", request.getParameter("probId"));
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view manage cause page   Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveProblemCause(HttpServletRequest request, HttpServletResponse reponse, ServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        serviceCommand = command;
        String path = "";
        String menuPath = "";
        menuPath = "Service Plan >>Problem Cause >>Manage Problem Cause";
        String pageTitle = "Manage Problem Cause";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ServiceTO serviceTO = new ServiceTO();
            path = "content/Maintenance/manageProblemCause.jsp";
            int userId = (Integer) session.getAttribute("userId");
            int secId = Integer.parseInt(request.getParameter("secId"));
            int probId = Integer.parseInt(request.getParameter("probId"));

            serviceTO.setSecId(secId);
            serviceTO.setProbId(probId);
            serviceTO.setUserId(userId);

            String[] activityIds = serviceCommand.getActivityId();
            String[] activityNames = serviceCommand.getActivityName();
            String[] desc = serviceCommand.getDesc();
            String[] activeStatus = serviceCommand.getActiveInds();
            String[] selectedindex = serviceCommand.getSelectedindex();

            int status = 0;

            status = serviceBP.saveProblemCauses(serviceTO, activityIds, activityNames, desc, activeStatus, selectedindex);
            ArrayList secList = new ArrayList();
            secList = serviceBP.getSections();
            System.out.println("secList" + secList.size());
            request.setAttribute("sections", secList);
            request.setAttribute("secId", request.getParameter("secId"));
            request.setAttribute("probId", request.getParameter("probId"));
            request.removeAttribute("activityDetails");
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to save problem activity service   Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView addProblemActivity(HttpServletRequest request, HttpServletResponse reponse, ServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        System.out.println("1");
        HttpSession session = request.getSession();
        System.out.println("2");
        serviceCommand = command;
        System.out.println("3");
        String path = "";
        String menuPath = "";
        menuPath = "Service Plan >>Problem Activity >> Add Problem";
        String pageTitle = "Add Problem Activity";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        System.out.println("4");
        int status = 0;
        String activityName = request.getParameter("activityName");
        int mfrId = Integer.parseInt(request.getParameter("mfrId"));
        int secId = Integer.parseInt(request.getParameter("secId"));
        try {
            ServiceTO serviceTO = new ServiceTO();
            path = "content/Maintenance/addProblem.jsp";
            int userId = (Integer) session.getAttribute("userId");
            System.out.println("5");
            status = serviceBP.insertProblemActivityDetails(activityName, activityName, secId, mfrId, "Y", userId);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to save problem activity service   Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView addProblemCause(HttpServletRequest request, HttpServletResponse reponse, ServiceCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        serviceCommand = command;
        String path = "";
        String menuPath = "";
        menuPath = "Service Plan >>Problem Activity >> Add Problem";
        String pageTitle = "Add Problem Activity";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int status = 0;
        String activityName = request.getParameter("causeName");
        int probId = Integer.parseInt(request.getParameter("problemId"));
        try {
            ServiceTO serviceTO = new ServiceTO();
            path = "content/Maintenance/addProblem.jsp";
            int userId = (Integer) session.getAttribute("userId");
            status = serviceBP.insertProblemCauseDetails(activityName, activityName, probId, "Y", userId);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to save problem activity service   Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleCheckListConfigPage(HttpServletRequest request, HttpServletResponse response, ServiceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        serviceCommand = command;
        String menuPath = "Vendor >> Item Configure";
        String pageTitle = "Generate Vendor Item";
        request.setAttribute("pageTitle", pageTitle);
        String path = "/content/Service/checkListConfigure.jsp";
        HttpSession session = request.getSession();
        int servicePtId = Integer.parseInt((String) session.getAttribute("companyId"));
        String companyId = (String) session.getAttribute("companyId");
        int compId = Integer.parseInt(companyId);
        System.out.println((new StringBuilder()).append("companyID=").append(companyId).toString());
        request.setAttribute("menuPath", menuPath);
        ArrayList serviceList = new ArrayList();
        ArrayList serviceCheckList = new ArrayList();
        try {
            ArrayList userFunctions = new ArrayList();
            userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Purchase-ShowRequiredItems")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                path = "content/Service/checkListConfigure.jsp";

                String serviceId = request.getParameter("serviceId");
                if (serviceId != null && !"0".equals(serviceId)) {
                    serviceCheckList = serviceBP.getServiceCheckList(serviceId);
                    request.setAttribute("serviceCheckList", serviceCheckList);
                    request.setAttribute("serviceId", serviceId);


                }
                serviceList = serviceBP.getServiceList();
                request.setAttribute("serviceList", serviceList);

//            }
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        
        } catch (Exception exception2) {
            exception2.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleGenerateMpr --> ").append(exception2).toString());
        }
        return new ModelAndView(path);
    }

     public ModelAndView saveServiceCheckList(HttpServletRequest request, HttpServletResponse response, ServiceCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ServiceTO serviceTO = new ServiceTO();
        String menuPath = "Vendor >> Item Configure";
        String pageTitle = "MPR List";
        request.setAttribute("pageTitle", pageTitle);
        int selectedInd = 0;
          ArrayList serviceList = new ArrayList();
        ArrayList serviceCheckList = new ArrayList();
        String companyId = (String) session.getAttribute("companyId");
        String path = "";
        request.setAttribute("menuPath", menuPath);
        int userId = ((Integer) session.getAttribute("userId")).intValue();
        String checkList[] = request.getParameterValues("checkList");
        String serviceId = request.getParameter("serviceId");

        System.out.println("i m here");
        try {
            ArrayList userFunctions = new ArrayList();
            userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Mpr-GenerateMPR")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
                for (int i = 0; i < checkList.length; i++) {
                    if (!"".equals(checkList[i]) && checkList[i] != null) {
                        serviceBP.insertServiceCheckListDetails(serviceId, checkList[i], userId);
                    }
                }

                path = "content/Service/checkListConfigure.jsp";
                 serviceId = request.getParameter("serviceId");
                if (serviceId != null && !"0".equals(serviceId)) {
                    serviceCheckList = serviceBP.getServiceCheckList(serviceId);
                    System.out.println("serviceCheckList size"+serviceCheckList.size());
                    request.setAttribute("serviceCheckList", serviceCheckList);
                    request.setAttribute("serviceId", serviceId);


                }
                serviceList = serviceBP.getServiceList();
                request.setAttribute("serviceList", serviceList);



//            }
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
        
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to handleGenerateMpr --> ").append(exception).toString());
        }
        return new ModelAndView(path);
    }

}
