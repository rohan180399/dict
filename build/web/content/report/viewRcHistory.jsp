
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css"/>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>  
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
    </head>
<body>
<form name="rcHistory" method="post">
    <%@ include file="/content/common/path.jsp" %>           
            <%@ include file="/content/common/message.jsp" %>
            <br>
      <c:if test = "${RcHistoryList != null}" >
 <table align="center" width="631" border="0" cellspacing="0" cellpadding="0" class="border">
  
     <tr>
         <td colspan="6" align="center" class="contenthead" height="30">RC ITEM HISTORY</td>
     </tr>
     <tr>
        <td  height="30"  class="contentsub"><div align="center">Date</div></td>
         <td  height="30"  class="contentsub"><div align="center">JobCard No</div></td>
          <td   class="contentsub">Vehicle No</td>            
          <td   class="contentsub">RC Vendor </td>
          <td   class="contentsub">RC Cost (INRS)</td>
          <td   class="contentsub">Vehicle Km</td>
          

     </tr>
      <% int index = 0;%>
                  <c:forEach items="${RcHistoryList}" var="rc"> 
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
     <tr>
             <td  class="<%=classText %>"  height="30"><c:out value="${rc.billDate}" /></td>
             <td  class="<%=classText %>"  height="30"><c:out value="${rc.jobCardId}" /></td>
             <td  class="<%=classText %>" ><c:out value="${rc.regNo}" /></td>
             <td  class="<%=classText %>" ><c:out value="${rc.vendorName}" /></td>
             <td  class="<%=classText %>" ><c:out value="${rc.totalAmount}" /></td>
             <td  class="<%=classText %>" ><c:out value="${rc.km}" /></td>
         </tr>
 <%index++;%>
            </c:forEach>
            </table>
        </c:if>
 <br>

<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>


</html>
