<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.customer.business.CustomerTO" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--                            <th><h3>Vehicle No</h3></th>-->
        <!--                                <td align="left" class="text2"><c:out value="${vendorVehicle.vehicleNo}"/> </td>-->

        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
    </head>
    <body>
        <form name="vendorVehicleWiseContract" method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <input type="hidden" name="vendorId" id="vendorId" value="<c:out value="${vendorId}"/>" />
            <c:if test = "${vendorVehicleContractLists != null}" >
                 <ul class="tabNavigation">
                                <li style="background:#76b3f1">vendor Vehicle Wise Contract</li>
                 </ul>
                <table width="100%" align="center" border="0" id="table" class="sortable">
                    <thead>
                        <tr height="40">
                            <th><h3>S.No</h3></th>

                            <th><h3>Vehicle Type</h3></th>
                            <th><h3>Contract Type</h3></th>
                            <th><h3>Operation Type</h3></th>
                            <th><h3>Status</h3></th>
                            <th><h3>View / Edit</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 0, sno = 1;%>
                        <c:forEach items="${vendorVehicleContractLists}" var="vendorVehicle">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <tr height="30">
                                <td align="left" class="text2"><%=sno%></td>

                                <td align="left" class="text2"><c:out value="${vendorVehicle.vehicleTypeName}"/> </td>
                                <td align="left" class="text2">
                                    <c:if test="${(vendorVehicle.contractType=='1')}" >
                                        Fixed KM Based
                                    </c:if>

                                    <c:if test="${(vendorVehicle.contractType=='2')}" >
                                        Actual KM Based
                                    </c:if>
                                    <c:if test="${(vendorVehicle.contractType=='0')}" >
                                        NA
                                    </c:if>

                                </td>
                                <td align="left" class="text2">
                                    <c:if test="${(vendorVehicle.operationType=='1')}" >
                                       Primary
                                    </c:if>

                                    <c:if test="${(vendorVehicle.operationType=='2')}" >
                                        Secondary
                                    </c:if>
                                    <c:if test="${(vendorVehicle.operationType=='0')}" >
                                        NA
                                    </c:if>

                                </td>
                                <td align="left" class="text2">
                                    <c:if test="${(vendorVehicle.activeInd=='n') || (vendorVehicle.activeInd=='N')}" >
                                        InActive
                                    </c:if>
                                    <c:if test="${(vendorVehicle.activeInd=='y') || (vendorVehicle.activeInd=='Y')}" >
                                        Active
                                    </c:if>
                                </td>
                                <td align="left" class="text2">
                                        <a href="/throttle/viewVendorVehicleContract.do?vendorId=<c:out value="${vendorVehicle.vendorId}"/>&contractId=<c:out value="${vendorVehicle.contractId}"/>&vehicleTypeId=<c:out value="${vendorVehicle.vehicleTypeId}"/>">View/Edit</a>
                                </td>

                            </tr>
                            <%
                                        index++;
                                        sno++;
                            %>
                        </c:forEach>

                    </tbody>
                </table>
            </c:if>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
