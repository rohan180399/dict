<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.fuelManagement.business.FuelManagementTO" %>
        <%@ page import="java.util.*" %>
        <SCRIPT LANGUAGE="Javascript" SRC="/throttle/js/FusionCharts.js"></SCRIPT>
    </head>
    
    <script language="javascript">
        var httpRequest;
        var temp = "";
        function setVisibility(){
            if(document.fuelFilling.outFill.checked==true){
                document.getElementById("place").style.visibility="visible";
                document.getElementById("fuel").style.visibility="visible";
                
            }else{    
            document.getElementById("place").style.visibility="hidden";
            document.getElementById("fuel").style.visibility="hidden";       
        }         
    }
    
    function calculate(){
        var price=document.fuelFilling.fuelPrice.value;
        document.fuelFilling.amount.value=(parseFloat(price)*parseFloat(document.fuelFilling.fuelFilled.value))
        if(document.fuelFilling.outFill.checked==false){
            var total=parseInt(document.fuelFilling.present.value) - parseInt(document.fuelFilling.previous.value)
        }else{
        var total=parseInt(document.fuelFilling.outFillKm.value) - parseInt(document.fuelFilling.previous.value)
    }
    var avg=total/parseFloat(document.fuelFilling.lastFilled.value);
    document.fuelFilling.totalKm.value=total;
    if(document.fuelFilling.lastFilled.value=="0"){
        document.fuelFilling.avgKm.value="0";    
        document.fuelFilling.totalKm.value="0";
    }else{
    document.fuelFilling.avgKm.value=parseFloat(avg);
}
document.fuelFilling.tankReading.value=parseFloat(document.fuelFilling.tankReading.value)-parseFloat(document.fuelFilling.fuelFilled.value);
}
function submitPage(indx){
    var chek=validation(indx);
    if(chek=='true'){
       
       document.fuelFilling.action='/throttle/addOutFillFuel.do';
        document.fuelFilling.submit();
    }
    
}
function validation(indx){
    
    
    var date=document.getElementsByName("dats");
    var fuel=document.getElementsByName("fuels");
    var regNo=document.getElementsByName("regNos");
    var place=document.getElementsByName("places");
    var outFillKm=document.getElementsByName("outFillKms");
    var rate=document.getElementsByName("rates");
    var index=indx;
   
    for(var i=0;i<(index);i++){        
        if(textValidation(regNo[i],'Vehicle No')){
            return 'false'
            }
        if(textValidation(date[i],'Date')){
            return 'false'
            }
        if(textValidation(place[i],'Place')){
            return 'false'
            }
        if(textValidation(fuel[i],'Fuel Filled')){
            return 'false'
            }
        if(textValidation(outFillKm[i],'Meter Reading')){
            return 'false'
            }
        if(textValidation(rate[i],'Amount')){
            return 'false'
            }
        
        }
    return 'true';
    
}
function reset(){
    
       
    document.fuelFilling.fuels.value='';              
    document.fuelFilling.outFillKms.value='';       
    document.fuelFilling.dats.value='';
    document.fuelFilling.places.value='';
    document.fuelFilling.rates.value='';
            
}
function getVehicleNos(){
    
    var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
}    


function ajax(regNo) {
    var url='/throttle/getVehDetails.do?regNo='+regNo;
    
    if (window.ActiveXObject)
        {
            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else if (window.XMLHttpRequest)
            {
                httpRequest = new XMLHttpRequest();
            }
            
            httpRequest.open("POST", url, true);
            httpRequest.onreadystatechange = function() {processAjax(); } ;
            httpRequest.send(null);
        }
        
        function processAjax()
        {
            if (httpRequest.readyState == 4)
                {
                    if(httpRequest.status == 200)
                        {
                            temp = httpRequest.responseText.valueOf();
                            if(temp !=""){
                                var result=temp.split('-');                                
                                document.fuelFilling.typeId.value=result[1];
                                document.fuelFilling.companyId.value=result[2];
                                document.fuelFilling.servicePtId.value=result[3];
                                document.fuelFilling.previous.value=result[4];
                                document.fuelFilling.lastFilled.value=result[5];
                                
                            }else{
                            
                            alert("Please Add Vehicle In Manage Vehicle Company");
                        }
                    }
                    else
                        {
                            alert("Error loading page\n"+ httpReq.status +":"+ httpReq.statusText);
                        }
                    }
                }
               
        
              function setValues(){                   
                
                  if('<%=request.getAttribute("regNo")%>'!='null'){                     
                    document.fuelFilling.regNos.value='<%=request.getAttribute("regNo")%>';                                            
                    }                  
                  }
                    var rowCount=4;
          var sno=3;  
          var index=0;
          function addRow()
          {
               index+=1;
               sno++;
             
               
                var tab = document.getElementById("items");  
                var newrow = tab.insertRow(rowCount);
                                             
               var cell = newrow.insertCell(0); 
               var cell0 = "<td  colspan='3' height='25' >&nbsp;</td>";
               cell.setAttribute("className","text2");
               cell.innerHTML = cell0;
            
                       

               rowCount++;  
                                          
               var newrow = tab.insertRow(rowCount);
                                             
               var cell = newrow.insertCell(0); 
               var cell0 = "<td class='text1' height='25'>Date</td>";
               cell.setAttribute("className","text2");
               cell.innerHTML = cell0;
               
               var cell = newrow.insertCell(1); 
               
               var cell1 = "<td> <input name='dats' id='date"+index+"' class='form-control' type='text' value=' ' size='20'><img src='/throttle/images/cal.gif' name='Calendar'  onclick=\"displayCalendar(document.fuelFilling.date"+index+",'dd-mm-yyyy',this)\"/></td>";                             
               cell.setAttribute("className","text2");
               cell.innerHTML = cell1;           
               
               var cell = newrow.insertCell(2); 
               var cell2 = "<td class='text1' height='25' >Vehicle Number</td>";
               cell.setAttribute("className","text2");
               cell.innerHTML = cell2;               
               
               var cell = newrow.insertCell(3); 
               var cell2 = "<td><input name='regNos' class='form-control' value='<c:out value="${regNo}"/>' type='text'></td>";
               cell.setAttribute("className","text2");
               cell.innerHTML = cell2;               
                rowCount++;   
                var newrow = tab.insertRow(rowCount);
                                             
               var cell = newrow.insertCell(0); 
               var cell0 = "<td class='text1' height='25'  >Place</td>";
               cell.setAttribute("className","text1");
               cell.innerHTML = cell0;
               
               var cell = newrow.insertCell(1); 
               var cella = "<td><input name='places' class='form-control' value='' type='text'></td>";
               cell.setAttribute("className","text1");
               cell.innerHTML = cella;
               
               var cell = newrow.insertCell(2); 
               var cellb = "<td class='text1' height='25'  >Fuel Filled</td>";
               cell.setAttribute("className","text1");
               cell.innerHTML = cellb;
               
               var cell = newrow.insertCell(3); 
               var cellc = "<td><input name='fuels' class='form-control' value='' type='text'></td>";
               cell.setAttribute("className","text1");
               cell.innerHTML = cellc;
                       

               rowCount++;               
              var newrow = tab.insertRow(rowCount);
                                             
               var cell = newrow.insertCell(0); 
               var cell0 = "<td class='text1' height='25' width='25'>Amount</td>";
               cell.setAttribute("className","text2");
               cell.innerHTML = cell0;
               
               var cell = newrow.insertCell(1); 
               var celld = "<td><input name='rates' class='form-control' value='' type='text'></td>";
               cell.setAttribute("className","text2");
               cell.innerHTML = celld;
               var cell = newrow.insertCell(2); 
               var celle = "<td class='text1' height='25' width='25'  >Meter Reading</td>";
               cell.setAttribute("className","text2");
               cell.innerHTML = celle;
               var cell = newrow.insertCell(3); 
               var cellf = "<td><input name='outFillKms' class='form-control' value='' type='text'></td>";
               cell.setAttribute("className","text2");
               cell.innerHTML = cellf;
                       
               rowCount++;               
          }     
    </script>
    <body onload="setValues();">
        <form  name="fuelFilling" method="post">
            <%@ include file="/content/common/path.jsp" %>                 
            <%@ include file="/content/common/message.jsp" %>
            <br>            
            
            <br>
           
           
            <table align="center" width="600" id="items" class="border" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <Td colspan="2" class="contenthead">Fuel Filling Out</Td>
                </tr>
               <%int index=0;%>
                <tr>
                    <td class="text2">Date</td>
                    <td class="text2"><input name="dats" id="date<%=index%>" class="form-control" type="text" value="" size="20">
                    <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.fuelFilling.date<%=index%>,'dd-mm-yyyy',this)"/></td>
                    
                    <td class="text2">Vehicle Number</td>
                    <td class="text2"><input name="regNos" id="regno"  class="form-control" readonly  type="text"  size="20" ></td>                                    
               
               </tr>                
                <tr>
                    <td class="text1">Place</td>
                    <td class="text1"><input name="places" class="form-control" type="text" value="" size="20"></td>                   
                    
                    <td class="text1">Fuel Filled</td>
                    <td class="text1"><input name="fuels" class="form-control" type="text"  value="" size="20"></td>                                                                 
                </tr>
                <tr>
                    <td class="text2">Amount</td>
                    <td class="text2"><input name="rates" class="form-control" type="text"  value="" size="20"></td>                                                 
                    
                    <td class="text2">Meter Reading(Km)</td>
                    <td class="text2"><input name="outFillKms"  class="form-control"  type="text" value="" size="20"></td>   
                </tr>
           
            </table>            
            <br>
              
   <br>
            <center>
                 <input type="button" class="button" value="addRow" onclick="addRow();" />   
                <input type="button" class="button" value="Save" onclick="submitPage(<%=index%>);" />
                <input type="button" class="button" value="Reset" onclick="reset();" />    
                                
            </center>            
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
