<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="ets.domain.util.ThrottleConstants" %>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>-->
<script type="text/javascript">
    function setValues(val) {
        // alert("vsl....."+val);
        var temp = val.split("~");

        document.getElementById("stateId").value = temp[0];
        document.getElementById("stateCode").value = temp[1];
    }
</script>
<script type="text/javascript">
    function validatePanNo() {
        var nPANNo = document.getElementById("panNo").value;
        if (nPANNo != "") {
            document.getElementById("panNo").value = nPANNo.toUpperCase();
            var ObjVal = nPANNo;
//            var pancardPattern = /^([ABCFGHLJPT]{1})([A-Z]{2})([PCFHABTGL]{1})([A-Z]{1})(\d{4})([A-Z]{1})$/;
            var pancardPattern = /^([ABCEFGHLJPTD]{1})([A-Z]{2})([PCFHABTGL]{1})([A-Z]{1})(\d{4})([A-Z]{1})$/;
            var patternArray = ObjVal.match(pancardPattern);
            if (patternArray == null) {
                alert("PAN Card No Invalid ");
                return false;
            } else {
                return true;
            }
        } else {
            alert("Please enter pan no");
            return false;
        }
    }

    function validateGSTNo() {
        var nPANNo = document.getElementById("panNo").value;
        var gstNo = document.getElementById("gstNo").value;
        var stateCode = document.getElementById("stateCode").value;
        var tempSateCode = gstNo.substring(0, 2);
        var tempPanNo = gstNo.substring(2, 12).toUpperCase();

        if ((stateCode != tempSateCode || nPANNo != tempPanNo) && gstNo != '') {
            alert("Invalid GST No.Please check pan No and state code.");
            return false;
        } else {
            return true;
        }
    }

    function checkGstNoExists(gstNo) {

        // var gstNo = document.getElementById("gstNo").value;
        //  alert("test"+gstNo);
        $.ajax({
            url: "/throttle/checkGstNoExists.do",
            dataType: "json",
            data: {
                gstNo: gstNo
            },
            success: function(data, textStatus, jqXHR) {
                var count = data.gstEixsts;
                if (count == 0) {
                    $("#SAVE").removeClass('hidden');

                } else {
                    alert("gst No already exists..");
                    $("#SAVE").addClass('hidden');

                }
            },
            error: function(data, type) {
                console.log(type);
            }

        });
    }
</script>

<script language="javascript">
    function submitPage() {
        var check = true;
        var gstValidate = true;
        //  var gstExist =checkGstNoExists();alert("after:"+gstExist);
        var companyType = document.getElementById("companyType").value;
        if (companyType == '1') {
            check = validatePanNo();
            gstValidate = validateGSTNo();
            var gstNo = document.getElementById("gstNo").value;
            var length = gstNo.length;
            if (gstNo.length < 15) {
                alert("Length of the gstno. is :" + length + ". Please enter the valid GST No.");
                document.editcustomer.gstNo.focus();
                return;
            }
        } else {
            check = true;
            gstValidate = true;
        }
        var pincode = document.getElementById("pinCode").value;
//        alert(pinCode);
        if (pincode == '' || pincode == '0') {
            alert("Please Enter pincode");
            document.editcustomer.pincode.focus();
            return;
        }
        if (check == true && gstValidate == true) {
            var nPANNo = document.getElementById("panNo").value;
            var orgId = document.getElementById("organizationId").value;
            var companyType = document.getElementById("companyType").value;
            var gstNo = document.getElementById("gstNo").value;
            var stateId = document.getElementById("stateId").value;
//            alert(orgId);
//            alert(nPANNo.charAt(3).toUpperCase());
            if (companyType == '0') {
                alert("Company Type cannot be empty ");
                document.editcustomer.companyType.focus();
                return;
            }

            if (stateId == '0' || stateId == '') {
                alert("Billing State  cannot be empty ");
                document.editcustomer.stateIdTemp.focus();
                return;
            }
            if (nPANNo.charAt(3).toUpperCase() == 'P' && orgId != '1') {
                alert("Please choose correct organisation name as individual.since 4th letter of Pan no is p ");
                document.editcustomer.organizationId.focus();
                return;
            } else if (nPANNo.charAt(3).toUpperCase() == 'C' && orgId != '2') {
                alert("Please choose correct organisation name as Company .since 4th letter of Pan no is C ");
                document.editcustomer.organizationId.focus();
                return;
            } else if (nPANNo.charAt(3).toUpperCase() == 'A' && orgId != '3') {
                alert("Please choose correct organisation name as Association of Persons (AOP).since 4th letter of Pan no is  " + nPANNo.charAt(3));
                document.editcustomer.organizationId.focus();
                return;
            } else if (nPANNo.charAt(3).toUpperCase() == 'B' && orgId != '4') {
                alert("Please choose correct organisation name as Body of Individuals (BOI).since 4th letter of Pan no is  " + nPANNo.charAt(3));
                document.editcustomer.organizationId.focus();
                return;
            } else if (nPANNo.charAt(3).toUpperCase() == 'F' && orgId != '5') {
                alert("Please choose correct organisation name as Firm.since 4th letter of Pan no is  " + nPANNo.charAt(3));
                document.editcustomer.organizationId.focus();
                return;
            } else if (nPANNo.charAt(3).toUpperCase() == 'G' && orgId != '6') {
                alert("Please choose correct organisation name as Government.since 4th letter of Pan no is  " + nPANNo.charAt(3));
                document.editcustomer.organizationId.focus();
                return;
            } else if (nPANNo.charAt(3).toUpperCase() == 'H' && orgId != '7') {
                alert("Please choose correct organisation name as HUF (Hindu Undivided Family).since 4th letter of Pan no is  " + nPANNo.charAt(3));
                document.editcustomer.organizationId.focus();
                return;
            } else if (nPANNo.charAt(3).toUpperCase() == 'L' && orgId != '8') {
                alert("Please choose correct organisation name as Local Authority.since 4th letter of Pan no is  " + nPANNo.charAt(3));
                document.editcustomer.organizationId.focus();
                return;
            } else if (nPANNo.charAt(3).toUpperCase() == 'T' && orgId != '9') {
                alert("Please choose correct organisation name as Trust(AOP).since 4th letter of Pan no is  " + nPANNo.charAt(3));
                document.editcustomer.organizationId.focus();
                return;
            } else {

            }
            if (orgId == '0') {
                alert("organization  cannot be empty ");
                document.editcustomer.gstNo.focus();
                return;
            }
            if (gstNo == '' && companyType == '1') {
                alert("GST No cannot be empty ");
                document.editcustomer.gstNo.focus();
                return;
            }
            document.editcustomer.action = '/throttle/addCustomer.do';
            document.editcustomer.submit();
        }
    }
    function setFocus() {
        // document.editcustomer.custCode.focus();
        var roleId = document.getElementById("roleId").value;
        var userId = document.getElementById("userId").value;
        // alert("roleId:"+roleId);
        if (roleId != '1023' && userId != '1514') {

            document.getElementById("panNo").readOnly = true;
            document.getElementById("stateIdTemp").disabled = true;
            document.getElementById("gstNo").readOnly = true;
        }
    }
</script>

<script>
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#accountManager').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getAccountManager.do",
                    dataType: "json",
                    data: {
                        accountManagerName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            $('#accountManagerId').val('');
                            $('#accountManager').val('');
                        } else {
                            response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#accountManager").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('-');
                $itemrow.find('#accountManagerId').val(tmp[0]);
                $itemrow.find('#accountManager').val(tmp[1]);
                return false;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    });
</script>
<style>
    #index div {
        color:white;

    }
</style>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i>Sales</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.Sales"  text="Sales"/></a></li>
            <li class="active">View/Edit Customer</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">

            <body onload="setFocus();">
                <form name="editcustomer"  method="post" >
                    <%@ include file="/content/common/message.jsp" %>
                    <br>  

                    <input type="hidden"  name="roleId" id="roleId" value='<c:out value="${roleId}"/>'/>
                    <input type="hidden"  name="userId" id="userId" value='<c:out value="${userId}"/>'/>
                    <c:if test="${getCustomerDetails != null}">
                        <c:forEach items="${getCustomerDetails}" var="cudl">
                            <table class="table table-bordered  mb30 table-hover" >
                                <thead>
                                <th colspan="4"  height="30" id="index" style="background-color:#5BC0DE;">
                                    <div  align="center">View/Edit Customer</div>
                                </th>
                                </thead>
                                <tr height="35">
                                    <td width="25%"><font color="red">*</font>Customer Name</td>
                                    <td width="25%"><input name="customerId" id="customerId" type="hidden" class="textbox" value="<c:out value="${cudl.customerId}"/>" onClick="ressetDate(this);"><input name="customerName" id="customerName" type="text" class="textbox" value="<c:out value="${cudl.custName}"/>" style="width:200px;height:30px;" onpaste="return false;"></td>
                                    <td><font color="red">*</font>GST Type</td>
                                    <td>
                                        <select class="textbox" name="companyType" id="companyType" style="width:200px;height:30px;">>
                                            <option value="0">--Select--</option>
                                            <option value="1">Registered</option>
                                            <option value="2">Unregistered</option>
                                        </select>
                                        <script>
                                            document.getElementById("companyType").value = '<c:out value="${cudl.companyType}"/>';

                                        </script>
                                    </td>
                                </tr>
                                <tr height="35">
                                    <td><font color="red">*</font>PrimaryContractType 1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td>
                                        <select class="textbox" name="billingTypeId" id="billingTypeId" style="width:200px;height:30px;">
                                            <c:if test="${billingTypeList != null}">
                                                <c:forEach items="${billingTypeList}" var="btl">
                                                    <c:choose>
                                                        <c:when test="${btl.billingTypeId==cudl.billingTypeId}">
                                                            <option selected value='<c:out value="${btl.billingTypeId}"/>'><c:out value="${btl.billingTypeName}"/>
                                                            </c:when>
                                                            <c:otherwise>
                                                            <option value="<c:out value="${btl.billingTypeId}"/>" ><c:out value="${btl.billingTypeName}"/></option>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:forEach >
                                            </c:if>
                                        </select>
                                    </td>
                                    <td >&nbsp;&nbsp;&nbsp;<font color="red">*</font>PrimaryContractType 2</td>
                                    <td>
                                        <select class="textbox" name="secondaryBillingTypeId" id="secondaryBillingTypeId" style="width:200px;height:30px;">
                                            <option value="" selected>--Select--</option>
                                            <c:if test="${billingTypeList != null}">
                                                <c:forEach items="${billingTypeList}" var="btl">
                                                    <c:choose>
                                                        <c:when test="${btl.billingTypeId==cudl.secondaryBillingTypeId}">
                                                            <option selected value='<c:out value="${btl.billingTypeId}"/>'><c:out value="${btl.billingTypeName}"/>
                                                            </c:when>
                                                            <c:otherwise>
                                                            <option value="<c:out value="${btl.billingTypeId}"/>" ><c:out value="${btl.billingTypeName}"/></option>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:forEach >
                                            </c:if>
                                        </select>
                                    </td>
                                </tr>
                                <tr height="35">
                                    <td><font color="red">*</font>Account Manager</td>
                                    <td>
                                        <select class="textbox" name="accountManagerId" id="accountManagerId" style="width:200px;height:30px;">
                                            <option value="<c:out value="${cudl.accountManagerId}"/>" selected ><c:out value="${cudl.empName}"/></option>
                                            <c:if test="${accountManagerList != null}">
                                                <c:forEach items="${accountManagerList}" var="btl">
                                                    <c:if test="${cudl.accountManagerId != btl.empId}">
                                                        <option value="<c:out value="${btl.empId}"/>" ><c:out value="${btl.empName}"/></option>
                                                    </c:if>
                                                </c:forEach>
                                            </c:if>
                                        </select>
                                    </td>
                                    <td >Organization Name</td>
                                    <td>
                                        <input name="organizationIdTemp" id="organizationIdTemp"  type="hidden" class="form-control" >
                                        <select class="form-control" name="organizationId" id="organizationId" style="width:200px;height:40px;"required data-placeholder="Choose One">
                                            <option value="0">--Select--</option>
                                            <!--<option value="<c:out value="${cudl.orgId}"/>" selected ><c:out value="${cudl.orgId}"/></option>-->
                                            <c:if test="${organizationList != null}">
                                                <c:forEach items="${organizationList}" var="org">
                                                    <option value="<c:out value="${org.orgId}"/>" ><c:out value="${org.orgName}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </select>
                                    </td>
                                </tr>
                                <script>
                                    document.getElementById("organizationId").value = '<c:out value="${cudl.orgId}"/>';
                                </script>
                                <tr height="35">
                                <input name="paymentType" id="paymentType"hidden type="text" class="textbox"  value="1" >

                                <td>&nbsp;&nbsp;&nbsp;<font color="red">*</font>Contactperson</td>
                                <td><input name="custContactPerson" id="custContactPerson" type="text" class="textbox" value="<c:out value="${cudl.custContactPerson}"/>" onKeyPress="return onKeyPressBlockNumbers(event);" style="width:200px;height:30px;"></td>
                                </tr>


                                <tr height="35">
                                    <td><font color="red">*</font>Customer Billing  Address</td>
                                    <td><textarea name="custAddress" id="custAddress"  style="width:200px;" onpaste="return false;" ><c:out value="${cudl.custAddress}"  /></textarea></td>
                                    <td>&nbsp;&nbsp;&nbsp;<font color="red">*</font> City</td>
                                    <td><input name="custCity" id="custCity" type="text" class="textbox"  value="<c:out value="${cudl.custCity}"/>" onKeyPress="return onKeyPressBlockNumbers(event);" style="width:200px;height:30px;"></td>
                                </tr>
                                <tr height="35">
                                    <td>&nbsp;Customer Delivery Address</td>
                                    <td ><textarea name="custAddresstwo" id="custAddresstwo" style="width:200px;"><c:out value="${cudl.custAddresstwo}"/></textarea></td>

                                    <td>&nbsp;&nbsp;&nbsp;Customer  Additional Address</td>
                                    <td><textarea name="custAddressthree" id="custAddressthree" style="width:200px;"><c:out value="${cudl.custAddressthree}"/></textarea></td>

                                </tr>
                                <tr height="35">
                                <input name="custState" id="custState" type="hidden" class="textbox" value="<c:out value="${cudl.custState}"/>" onKeyPress="return onKeyPressBlockNumbers(event);" style="width:200px;height:30px;">
                                <td>&nbsp;&nbsp;&nbsp;<font color="red">*</font>Phone No</td>
                                <td><input name="custPhone" id="custPhone" type="text" class="textbox" value="<c:out value="${cudl.custPhone}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10" style="width:200px;height:30px;"></td>
                                <td><font color="red">*</font>Mobile No</td>
                                <td><input name="custMobile" id="custMobile" type="text" class="textbox" value="<c:out value="${cudl.custMobile}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="12" style="width:200px;height:30px;"></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;&nbsp;&nbsp;Email</td>
                                    <td><textarea name="custEmail" id="custEmail" style="width:200px;"><c:out value="${cudl.custEmail}"/></textarea></td>
                                    <!--<td><input name="custEmail" id="custEmail" type="text" class="textbox" value="<c:out value="${cudl.custEmail}"/>" maxlength="20" style="width:200px;height:30px;"></td>--> 
                                    <td><font color="red">*</font>Customer Type</td>    
                                    <td> <c:if test="${cudl.custTypeId == '1'}">
                                            Credit
                                        </c:if>
                                        <c:if test="${cudl.custTypeId == '2'}">
                                            PDA
                                        </c:if>
                                        <c:if test="${cudl.custTypeId != '1' && cudl.custTypeId != '2'}">
                                            <select name="custTypeId" id="custTypeId" style="width:200px;height:30px;" onchange="showCredit(this.value);" >
                                                <option value="0">---select---</option>
                                                <option value="1">Credit</option>
                                                <option value="2">PDA</option>

                                            </select>  
                                        </c:if>
                                    </td>
                                <script>  document.getElementById("custTypeId").value = '<c:out value="${cudl.custTypeId}"/>';</script>
                                </tr>
                                <tr >
                                    <td><font color="red">*</font>Fixed Credit Limit</td>
                                    <td>   <input name="fixedCreditLimit" id="fixedCreditLimit" type="text" class="textbox"  style="width:200px;height:30px;" value="<c:out value="${cudl.fixedCreditLimit}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10"  onchange="setAvailLimit();"  ></td>  
                                    <td><font color="red">*</font>Used Limit</td>
                                    <td><span id="cUsed"><c:out value="${cudl.usedLimit}"/></span> 
                                        <input name="usedLimit" id="usedLimit"  type="hidden" class="textbox"  value="<c:out value="${cudl.usedLimit}"/>" style="width:200px;height:30px;" >

                                    </td>
                                </tr>
                                <tr height="35" >

                                    <td id="availRow"><font color="red">*</font>Available Limit</td>
                                    <td id="availRow1"> 
                                        <input name="creditLimit" id="creditLimit" type="text" class="textbox" value="<c:out value="${cudl.availLimit}"/>"  maxlength="10" style="width:200px;height:30px;" ></td>

                                    <td id="pdaRow"><font color="red">*</font>PDA Amount</td>
                                    <td id="pdaRow1"><span id="pdaAmt"><c:out value="${cudl.advanceAmount}"/></span>  <input name="advanceAmount" type="hidden" id="advanceAmount" class="textbox" value="<c:out value="${cudl.advanceAmount}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10" style="width:200px;height:30px;">

                                    <td><font color="red">*</font>Credit Days</td> 
                                    <td><span id="cDays"><c:out value="${cudl.creditDays}"/></span> <input name="creditDays" id="creditDays" type="hidden" class="textbox" value="<c:out value="${cudl.creditDays}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10" style="width:200px;height:30px;" ></td>
                                </tr>
                                <!--                                 <tr height="35" id="pdaRow">
                                                                  
                                                                     <td><font color="red">*</font>PDA Amount</td>
                                                                     <td><span id="pda"><c:out value="${cudl.advanceAmount}"/></span> <input name="advanceAmount" hidden id="advanceAmount" type="text" class="textbox" value="<c:out value="${cudl.advanceAmount}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10" style="width:200px;height:30px;">
                                                                </tr>-->
                                <tr height="35">
                                    <td><font color="red">*</font>ERP ID</td>
                                    <td><input name="erpId" id="erpId" type="text" class="textbox" value="<c:out value="${cudl.erpId}"/>" maxlength="10" style="width:200px;height:30px;"></td>
                                    <td><font color="red">*</font>PAN No</td>
                                    <td><input name="panNo" id="panNo" type="text" onBlur="validatePanNo()" class="textbox" value="<c:out value="${cudl.panNo}"/>" maxlength="10" style="width:200px;height:30px;text-transform:uppercase;" ></td>

                                </tr>
                                <script>
                                    showCredit('<c:out value="${cudl.custTypeId}"/>');

                                    function showCredit(custType) {
                                        if (custType == '1') {
                                            $("#availRow").show();
                                            $("#availRow1").show();
                                            $("#pdaRow").hide();
                                            $("#pdaRow1").hide();
                                        } else if (custType == '2') {
                                            $("#availRow").hide();
                                            $("#availRow1").hide();
                                            $("#pdaRow").show();
                                            $("#pdaRow1").show();
                                        } else {
                                            $("#availRow").show();
                                            $("#availRow1").show();
                                            $("#pdaRow").hide();
                                            $("#pdaRow1").hide();
                                        }
                                    }
//                            function setAvailLimit(){
//                                var fixedCreditLimit = $("#fixedCreditLimit").val();
//                                var creditLimit = $("#creditLimit").val();
//                                var custTypeId = $("#custTypeId").val();
//                                alert("creditLimit"+creditLimit);
//                                if(custTypeId == "1"){
//                                 $("#creditLimit").val(creditLimit);
//                                }else{
//                                 $("#creditLimit").val(0);
//                                  alert($("#creditLimit").val());
//                                }
//                            }
                                </script>
                                <tr>
                                    <td><font color="red">*</font>Billing State</td>
                                    <td>
                                        <input name="stateId" id="stateId"  type="hidden" class="form-control" value="<c:out value="${cudl.stateId}"/>">
                                        <input name="stateCode" id="stateCode"  type="hidden" class="form-control"value="<c:out value="${cudl.stateCode}"/>" >
                                        <select name="stateIdTemp" id="stateIdTemp" class="textbox" required data-placeholder="Choose One" onchange="setValues(this.value);" style="width:200px;height:30px;">
                                            <option value="">Choose One</option>
                                            <c:if test = "${stateList != null}" >
                                                <c:forEach items="${stateList}" var="Type">
                                                    <option value='<c:out value="${Type.stateId}" />~<c:out value="${Type.stateCode}" />'><c:out value="${Type.stateName}" /></option>
                                                </c:forEach >
                                            </c:if>
                                        </select>
                                    </td>
                                    <td><font color="red">*</font>GST No</td>
                                    <td>
                                        <input name="gstNo" id="gstNo"  type="text" class="textbox" value="<c:out value="${cudl.gstNo}"/>" style="width:200px;height:30px;text-transform:uppercase;" onchange="checkGstNoExists(this.value);" onpaste="return false;" >

                                    </td>
                                </tr>
                                <tr height="35" style="display:none">
                                    <td><font color="red">*</font>Status</td>
                                    <td >
                                        <select readonly name="custStatus" id="custStatus" class="textbox" style="width:200px;height:30px;">
                                            <c:if test="${cudl.custStatus == 'Y'}">
                                                <option value="Y" selected >Active</option>
                                                <option value="N"  >In Active</option>
                                            </c:if>
                                            <c:if test="${cudl.custStatus == 'N'}">
                                                <option value="Y" >Active</option>
                                                <option value="N" selected>In Active</option>
                                            </c:if>
                                        </select >
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;&nbsp;&nbsp;<font color="red">*</font> PinCode</td>
                                    <td><input type="text" class="textbox"   name="pinCode" id="pinCode" value="<c:out value="${cudl.pinCode}"/>"  style="width:200px;height:30px;"/></td>


                                    <td><font color="red">*</font>Billing Name and Address</td>
                                    <td colspan="3"><textarea name="billingNameAddress" id="billingNameAddress" cols="100" rows="4" style="width:200px;" ><c:out value="${cudl.billingNameAddress}"/></textarea></td>

                                </tr>
                                <tr>
                                    <td><font color="red">*</font>GTA Type</td>    
                                    <td>  <select name="gtaType" id="gtaType" style="width:200px;height:30px;">
                                            <option value="Y">Yes</option>
                                            <option seleted value="N">No</option>

                                        </select>       
                                    </td>
                                </tr>
                                <script>
                                    //                         
                                    document.getElementById("stateIdTemp").value = '<c:out value="${cudl.stateId}"/>~<c:out value="${cudl.stateCode}"/>';
                                    document.getElementById("gtaType").value = '<c:out value="${cudl.gtaType}"/>';


                                </script>
                            </table>
                        </c:forEach >
                    </c:if>
                    <br>
                    <center>
                        <input type="button" name="SAVE" id="SAVE" value="SAVE" class="btn btn-info" onClick="submitPage();" style="width:100px;height:35px;">
                        &emsp;<input type="reset" class="btn btn-info" value="Clear" style="width:100px;height:35px;">
                    </center>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>
