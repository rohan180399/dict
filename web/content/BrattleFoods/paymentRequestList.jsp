<%@page import="java.text.DecimalFormat"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {

                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function() {
                //	alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });

            });
        </script>

    </head>
    <script language="javascript">
        function submitPage() {

            document.approve.action = '/throttle/savePaymentRequest.do';
            document.approve.submit();
        }

    </script>


    <body onload="setFocus();">
        <form name="approve"  method="post" >

            <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>

            <br>



            <c:if test = "${paymentDetails != null}" >
            <table width="100%" align="center" border="0" id="table" class="sortable">
                <thead>
                    <tr height="40">
                        <th><h3>S.No</h3></th>
                        <th><h3>Payment Date</h3></th>
                        <th><h3>Consignment No</h3></th>
                        <th><h3>Payment Type</h3></th>
                        <th><h3>Payment Mode</h3></th>
                        <th><h3>PayMode Reference No</h3></th>
                        <th><h3>PayMode Reference Remarks</h3></th>
                        <th><h3>Paid Amount</h3></th>
                        <th><h3>Payment Remarkst</h3></th>
                        <th><h3>Estimated Freight Amount</h3></th>
                        <th><h3>Action</h3></th>
                    </tr>
                </thead>
                <tbody>
                    <% int index = 0,sno = 1;%>
                    <c:forEach items="${paymentDetails}" var="payment">
                        <%
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text2";
                                    } else {
                                        classText = "text1";
                                    }
                        %>
                        <tr height="30">
                            <td align="left" class="text2"><%=sno%></td>
                            <td align="left" class="text2"><c:out value="${payment.createdDate}"/> </td>
                            <td align="left" class="text2">
                                <input name="consignmentOrderId" id="consignmentOrderId" type="hidden" class="textbox" value="<c:out value="${payment.consignmentOrderId}"/>"><c:out value="${payment.consignmentNoteNo}"/> </td>
                            <td align="left" class="text2">
                                <c:if test="${payment.paymentType=='1'}" >
                                    Credit
                                </c:if>
                                <c:if test="${payment.paymentType=='2'}" >
                                    Advance
                                </c:if>
                                <c:if test="${payment.paymentType=='3'}" >
                                    To Pay and Advance
                                </c:if>
                                <c:if test="${payment.paymentType=='4'}" >
                                    To Pay
                                </c:if>
                            </td>
                            <td align="left" class="text2"><c:out value="${payment.paymentModeName}"/> </td>
                            <td align="left" class="text2">
                                <c:if test="${payment.paymentModeId=='1' }" >
                                    -
                                </c:if>

                                <c:if test="${payment.paymentModeId=='6' }" >
                                    <c:out value="${payment.rtgsNo}"/>
                                </c:if>

                                <c:if test="${payment.paymentModeId=='2' }" >
                                    <c:out value="${payment.chequeNo}"/>
                                </c:if>

                                <c:if test="${payment.paymentModeId=='7' }" >
                                    <c:out value="${payment.draftNo}"/>
                                </c:if>
                            </td>
                            <td align="left" class="text2">
                                <c:if test="${payment.paymentModeId=='1' }" >
                                    -
                                </c:if>

                                <c:if test="${payment.paymentModeId=='6' }" >
                                    <c:out value="${payment.rtgsRemarks}"/>
                                </c:if>

                                <c:if test="${payment.paymentModeId=='2' }" >
                                    <c:out value="${payment.chequeRemarks}"/>
                                </c:if>

                                <c:if test="${payment.paymentModeId=='7' }" >
                                    <c:out value="${payment.draftRemarks}"/>
                                </c:if>
                            </td>

                            <td align="left" class="text2"><c:out value="${payment.paidAmount}"/></td>
                            <td align="left" class="text2"><c:out value="${payment.paymentRemarks}"/></td>
                            <td align="left" class="text2"><c:out value="${payment.freightAmount}"/></td>

                            <td align="left" class="text2">
                                    <c:if test="${payment.approvalstatus=='1' }" >
                                        Requested &nbsp;/&nbsp;
                                        <a href="/throttle/viewChequeRequest.do?consignmentOrderId=<c:out value="${payment.consignmentOrderId}"/>&paymentTypeId=<c:out value="${payment.paymentTypeId}"/>&nextPage=<c:out value="${nextPage}"/>&mode=1&customerId=<c:out value="${customerId}"/>"/>Approve</a>
                                    </c:if>
                                    <c:if test="${payment.approvalstatus=='2' }" >
                                        Approved &nbsp;/&nbsp;
                                        <a href="/throttle/viewChequeRequest.do?consignmentOrderId=<c:out value="${payment.consignmentOrderId}"/>&paymentTypeId=<c:out value="${payment.paymentTypeId}"/>&nextPage=<c:out value="${nextPage}"/>&mode=2&customerId=<c:out value="${customerId}"/>"/>view</a>
                                    </c:if>
                                    <c:if test="${payment.approvalstatus=='3' }" >
                                        Rejected &nbsp;/&nbsp;
                                        <a href="/throttle/viewChequeRequest.do?consignmentOrderId=<c:out value="${payment.consignmentOrderId}"/>&paymentTypeId=<c:out value="${payment.paymentTypeId}"/>&nextPage=<c:out value="${nextPage}"/>&mode=2&customerId=<c:out value="${customerId}"/>"/>view</a>
                                    </c:if>
                            </td>
                        </tr>
                        <%
                                   index++;
                                   sno++;
                        %>
                    </c:forEach>

                </tbody>
            </table>

            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        </c:if>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
