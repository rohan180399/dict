<%--
    Document   : newState
    Created on : 8 Nov, 2012, 10:14:38 AM
    Author     : ASHOK
--%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Add Ledger</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>

        <script language="javascript" src="/throttle/js/validate.js"></script>
    </head>
    <script>
        function submitPage()
        {
            

            if(textValidation(document.add.districtName,'DistrictName')){
                return;
            }
            if(textValidation(document.add.districtCode,'DistrictCode')){
                return;
            }
            if(document.add.countryID.value=='0'){
                alert("Please Select Country");
                return 'false';
            }
            if(document.add.stateID.value=='0'){
                alert("Please Select State");
                return 'false';
            }
            if(textValidation(document.add.description,'description')){
                return;
            }
            

            document.add.action='/throttle/saveNewDistrict.do';
            document.add.submit();
        }
        function setFocus(){
            document.add.DistrictName.focus();
        }

    </script>
    <body onload="setFocus();">
        <form name="add" method="post">
            <%@ include file="/content/common/path.jsp" %>

            <%@ include file="/content/common/message.jsp" %>

            <table align="center" width="500" border="0" cellspacing="0" cellpadding="0" class="border">
                <tr height="30">
                    <Td colspan="2" class="contenthead">Add District</Td>
                </tr>
                <tr height="30">
                    <td class="text1"><font color="red">*</font>Name</td>
                    <td class="text1"><input name="districtName" type="text" class="form-control" value="" maxlength="15" size="20"></td>
                </tr>
                <tr height="30">
                    <td class="text2"><font color="red">*</font>Code</td>
                    <td class="text2"><input name="districtCode" type="text" class="form-control" value="" maxlength="10" size="20"></td>
                </tr>
                <tr height="30">
                    <td class="text1"><font color="red">*</font> Country Name</td>
                    <td class="text1">
                        <select class="form-control" name="countryID"  style="width:125px">
                            <option value="0">---Select---</option>
                            <c:if test = "${CountryLists != null}" >
                                <c:forEach items="${CountryLists}" var="CL">
                                    <option value='<c:out value="${CL.countryID}" />'><c:out value="${CL.countryName}" /></option>
                                </c:forEach>
                            </c:if>
                        </select>
                    </td>
                </tr>
                <tr height="30">
                    <td class="text2"><font color="red">*</font> State Name</td>
                    <td class="text2">
                        <select class="form-control" name="stateID"  style="width:125px">
                            <option value="0">---Select---</option>
                            <c:if test = "${StateLists != null}" >
                                <c:forEach items="${StateLists}" var="SL">
                                    <option value='<c:out value="${SL.stateID}" />'><c:out value="${SL.stateName}" /></option>
                                </c:forEach>
                            </c:if>
                        </select>
                    </td>
                </tr>               

                <tr height="30">
                    <td class="text1"><font color="red">*</font>Description</td>
                    <td class="text1"><input name="description" type="text" class="form-control" value="" maxlength="15" size="20"></td>
                </tr>
            </table>
            <br>
            <br>
            <center>
                <input type="button" class="button" value="Save" onclick="submitPage();" />
                &emsp;<input type="reset" class="button" value="Clear">
            </center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>

</html>

