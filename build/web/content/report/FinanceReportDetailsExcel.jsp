<%-- 
    Document   : driverSettlementReportExcel
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>

        <form name="financeReportDetails" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "FinanceReportDetails-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>

<c:if test = "${financeAdvice != null}" >
                <table width="100%" align="center" border="1" id="table" class="sortable">
                    <thead>
                        <tr height="40">
                            <th><h3>S.No</h3></th>
                            <th><h3>Advice Date</h3></th>                            
                            <th><h3>Advice Type</h3></th>                            
                            <th><h3>No. of Trips</h3></th>
                            <th><h3>Estimated Amt.</h3></th>
                            <th><h3>Requested Amt.</h3></th>
                            <th><h3>Paid Amt.</h3></th>                            
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 0,sno = 1;%>
                        <c:forEach items="${financeAdvice}" var="fd">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <tr height="30">
                                <td align="left" class="text2"><%=sno%></td>
                                <td align="left" class="text2"><c:out value="${fd.advicedate}"/> </td>                                

                                <td align="left" class="text2">
                                <c:if test="${(fd.batchType=='b') || (fd.batchType=='B')}" >
                                    Batch
                                    </c:if>
                                    <c:if test="${(fd.batchType=='a') || (fd.batchType=='A')}" >
                                    Adhoc
                                    </c:if>
                                    <c:if test="${(fd.batchType=='m') || (fd.batchType=='M')}" >
                                    Manual
                                    </c:if>
                                </td>

                                <td align="left" class="text2"><c:out value="${fd.tripday}"/></td>                                
                                <td align="left" class="text2">
                                    <c:if test="${(fd.estimatedadvance=='')||(fd.estimatedadvance==null)}" >
                                    0
                                    </c:if>
                                    <c:if test="${(fd.estimatedadvance!='')||(fd.estimatedadvance!=null)}" >
                                    <c:out value="${fd.estimatedadvance}"/>
                                    </c:if>
                                </td>
                                <td align="left" class="text2"><c:out value="${fd.requestedadvance}"/></td>
                                <td align="left" class="text2"><c:out value="${fd.actualadvancepaid}"/></td>
                            </tr>
                        <%
                                   index++;
                                   sno++;
                        %>
                    </c:forEach>
                       </c:if>

                    </tbody>
                </table>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>