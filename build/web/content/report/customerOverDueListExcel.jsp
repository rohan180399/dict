<%--
    Document   : Accounts Receivable
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">




        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>

        <form name="AccountReceivableReport" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "AccountReceivableReport-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
            <br>
            <br>
            <c:if test = "${customerOverDueList != null}" >
                <table style="width: 1100px" align="center" border="1">
                    <thead>
                        <tr height="40">
                            <th class="contentsub"><h3>S.No</h3></th>
                            <th class="contentsub"><h3>Customer Name</h3></th>
                            <th class="contentsub"><h3>Total Amount Receivable</h3></th>
                            <th class="contentsub"><h3>Receivable OverDue</h3></th>
                            <th class="contentsub"><h3>View Details</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 0,sno = 1;%>
                        <c:forEach items="${customerOverDueList}" var="odList">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                                   <% if (oddEven > 0) { %>
                              <tr height="30">
                                    <td align="left" class="text2"><%=sno%></td>
                                    <td align="left" class="text2"><c:out value="${odList.customerName}"/> </td>
                                    <td align="right" class="text2"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${odList.overDueGrandTotal}" /></td>
                                    <c:set var="overDue" value="0.00"/>
                                    <c:if test="${odList.overDueGrandTotal > odList.creditLimit && odList.creditLimit >  0.00}">
                                    <c:set var="overDue" value="${odList.overDueGrandTotal - odList.creditLimit}"/>
                                    </c:if>
                                    <c:if test="${odList.overDueGrandTotal == odList.creditLimit && odList.overDueGrandTotal < odList.creditLimit}">
                                    <c:set var="overDue" value="${0.00}"/>
                                    </c:if>
                                    <c:if test="${odList.overDueGrandTotal > odList.creditLimit && odList.creditLimit ==  0.00}">
                                    <c:set var="overDue" value="${odList.overDueGrandTotal}"/>
                                    </c:if>
                                    <td align="right" class="text2"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${overDue}" /></td>
                                    </td>
                                    <td align="left" class="text2"><a href="" onclick="viewInvoiceDetails('<c:out value="${odList.customerId}"/>')"><c:out value="${odList.customerCode}"/></a></td>
                               </tr>
                               <%}else{%>
                                <tr height="30">
                                    <td align="left" class="text1"><%=sno%></td>
                                    <td align="left" class="text1"><c:out value="${odList.customerName}"/> </td>
                                    <td align="right" class="text1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${odList.overDueGrandTotal}" /></td>
                                    <c:set var="overDue" value="0.00"/>
                                    <c:if test="${odList.overDueGrandTotal > odList.creditLimit && odList.creditLimit >  0.00}">
                                    <c:set var="overDue" value="${odList.overDueGrandTotal - odList.creditLimit}"/>
                                    </c:if>
                                    <c:if test="${odList.overDueGrandTotal == odList.creditLimit && odList.overDueGrandTotal < odList.creditLimit}">
                                    <c:set var="overDue" value="${0.00}"/>
                                    </c:if>
                                    <c:if test="${odList.overDueGrandTotal > odList.creditLimit && odList.creditLimit ==  0.00}">
                                    <c:set var="overDue" value="${odList.overDueGrandTotal}"/>
                                    </c:if>
                                    <td align="right" class="text1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${overDue}" /></td>
                                    </td>
                                    <td align="left" class="text1"><a href="" onclick="viewInvoiceDetails('<c:out value="${odList.customerId}"/>')"><c:out value="${odList.customerCode}"/></a></td>
                               </tr>
                                
                                 <%}%>
                                     <%
                                       index++;
                                       sno++;

                                   %>

                        </c:forEach>
                    </tbody>
                </table>
            </c:if>



        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>