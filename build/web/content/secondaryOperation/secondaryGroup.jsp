<%--
    Document   : SecondaryGroupMaster
    Created on : Oct 29, 2013, 11:32:08 AM
    Author     : srinivasan
--%>

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
    function submitPage()
    {
//        var errStr = "";
////        var nameCheckStatus = $("#StandardChargeNameStatus").text();
//        if(document.getElementById("groupName").value == "") {
//            errStr = "Please enter  Group Name.\n";
//            alert(errStr);
//            document.getElementById("groupName").focus();
//        }
//        else if(nameCheckStatus != "") {
//            errStr ="StandardCharge Name Already Exists.\n";
//            alert(errStr);
//            document.getElementById("stChargeName").focus();
//        }
//        else if(document.getElementById("stChargeDesc").value == "") {
//            errStr ="Please enter standard  Description.\n";
//            alert(errStr);
//            document.getElementById("stChargeDesc").focus();
//        }
//        else if(document.getElementById("stChargeUnit").value == "") {
//            errStr ="Please enter standard Charge Unit.\n";
//            alert(errStr);
//            document.getElementById("stChargeUnit").focus();
//        }
//        if(errStr == "") {
            document.SecondaryGroup.action=" /throttle/saveSecondaryGroup.do";
            document.SecondaryGroup.method="post";
            document.SecondaryGroup.submit();
//        }



    }
    function setValues(sno,groupCode,groupName,remarks,groupId,status){
        var count = parseInt(document.getElementById("count").value);
        document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if(i != sno) {
                document.getElementById("edit"+i).checked = false;
            } else {
                document.getElementById("edit"+i).checked = true;
            }
        }
        document.getElementById("groupId").value = groupId;
        document.getElementById("groupCode").value = groupCode;
        document.getElementById("groupName").value = groupName;
        document.getElementById("remarks").value = remarks;
        document.getElementById("status").value = status;
    }






    function onKeyPressBlockNumbers(e)
	{

		var key = window.event ? e.keyCode : e.which;
		var keychar = String.fromCharCode(key);
		reg = /\d/;
		return !reg.test(keychar);

	}




//var httpRequest;
//    function checkstChargeName() {
//
//        var stChargeName = document.getElementById('stChargeName').value;
//        var nameCheckStatus = $("#productCategoryNameStatus").text();
//            var url = '/throttle/checkStandardCharge.do?stChargeName=' + stChargeName ;
//            if (window.ActiveXObject) {
//                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
//            } else if (window.XMLHttpRequest) {
//                httpRequest = new XMLHttpRequest();
//            }
//            httpRequest.open("GET", url, true);
//            httpRequest.onreadystatechange = function() {
//                processRequest();
//            };
//            httpRequest.send(null);
//       
//    }
//
//
//    function processRequest() {
//        if (httpRequest.readyState == 4) {
//            if (httpRequest.status == 200) {
//                var val = httpRequest.responseText.valueOf();
//                if (val != "" && val != 'null') {
//                     $("#nameStatus").show();
//                    $("#StandardChargeNameStatus").text('Please Check Standard Charge Name  :' + val+' is Already Exists');
//                } else {
//                    $("#nameStatus").hide();
//                    $("#StandardChargeNameStatus").text('');
//                }
//            } else {
//                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
//            }
//        }
//    }

</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body onload="document.SecondaryGroup.stChargeName.focus();">


        <form name="SecondaryGroup"  method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <table  border="1" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                <input type="hidden" name="groupId" id="groupId" value=""  />
                <tr>
                <table  border="0" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">

                <tr>
                    <td class="contenthead" colspan="4" >Standard Charges Master</td>
                </tr>
                <tr>
                    <td class="text1" colspan="4" align="center" style="display: none" id="nameStatus"><label id="StandardChargeNameStatus" style="color: red"></label></td>
                </tr>
                <tr>
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Group Code</td>
                    <td class="text1"><input type="text" name="groupCode" id="groupCode" class="textbox" maxlength="15" value=""  /></td>
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Group Name</td>
                    <td class="text1"><input type="text" name="groupName" id="groupName" class="textbox" maxlength="15"></td>
                </tr>
                <tr>
                    <td class="text2">&nbsp;&nbsp;&nbsp;&nbsp;Description</td>
                    <td class="text2"><input type="text" name="remarks" id="remarks" class="textbox" >
                        
                    </td>
                    <td class="text2">&nbsp;&nbsp;&nbsp;&nbsp;Status</td>
                    <td class="text2">
                        <select  align="center" class="textbox" name="status" id="status" >
                            <option value='Y'>Active</option>
                            <option value='N' id="inActive" style="display: none">In-Active</option>
                        </select>
                    </td>
                </tr>
                </table>
                </tr>
                <tr>
                    <td>
                        <br>
                        <center>
                            <input type="button" class="button" value="Save" name="Submit" onClick="submitPage()">
                        </center>
                    </td>
                </tr>
          </table>
            <br>


            <h2 align="center">List The standard Charge's</h2>


            <table width="815" align="center" border="0" id="table" class="sortable">

                <thead>

                    <tr height="30">
                        <th><h3>S.No</h3></th>
                        <th><h3>Group Code</h3></th>
                        <th><h3>Group Name</h3></th>
                        <th><h3>Description</h3></th>
                        <th><h3>status</h3></th>
                        <th><h3>Select</h3></th>
                    </tr>
                </thead>
                <tbody>


                    <% int sno = 0;%>
                    <c:if test = "${secondaryGroupMaster != null}">
                        <c:forEach items="${secondaryGroupMaster}" var="stCharge">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td class="<%=className%>"  align="left"> <%= sno + 1%> </td>
                                <td class="<%=className%>"  align="left"> <c:out value="${stCharge.groupCode}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${stCharge.groupName}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${stCharge.remarks}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${stCharge.status}" /></td>
                                <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues( <%= sno%>,'<c:out value="${stCharge.groupCode}" />','<c:out value="${stCharge.groupName}" />','<c:out value="${stCharge.remarks}" />','<c:out value="${stCharge.groupId}" />','<c:out value="${stCharge.status}" />');" /></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </c:if>
            </table>


            <input type="hidden" name="count" id="count" value="<%=sno%>" />

            <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>