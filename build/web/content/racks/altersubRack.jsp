<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<!--<title>PAPL</title>-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script> 
<script>
    function submitPage()
    {
        var checValidate = selectedItemValidation();           
    }
    function setSelectbox(i){
    var selected=document.getElementsByName("selectedIndex") ;
    selected[i].checked = 1;
    } 
    function selectedItemValidation(){
var index = document.getElementsByName("selectedIndex");
var subRackName = document.getElementsByName("subRackNameList");
var rackName = document.getElementsByName("rackIdList");
var subRackDescription = document.getElementsByName("subRackDescriptionList");
var subRackStatus = document.getElementsByName("subRackStatusList");
var chec=0;

for(var i=0;(i<index.length && index.length!=0);i++){
if(index[i].checked){
chec++;
if(textValidation(subRackName[i],'SubRack Name ')){       
        return;
   }     
 if(textValidation(rackName[i],'Rack Name')){ 
        return;
   }     
 if(textValidation(subRackDescription[i],'SubRack Description')){ 
        return;
   }     
 if(textValidation(subRackStatus[i],'SubRack Status')){ 
        return;
   }  
        document.alterSubRack.action='/throttle/handleUpdateSubRack.do';
        document.alterSubRack.submit();      
}
}
if(chec == 0){
alert("Please Select Any One And Then Proceed");
custName[0].focus();
}
}
</script>

  <script>
   function changePageLanguage(langSelection){

            if(langSelection== 'ar'){
            document.getElementById("pAlign").style.direction="rtl";
            }else if(langSelection== 'en'){
            document.getElementById("pAlign").style.direction="ltr";
            }
        }

    </script>

<div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.SubRack"  text="SubRacks"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="general.label.stores"  text="Stores"/></a></li>
          <li class="active"><spring:message code="stores.label.SubRack"  text="SubRacks"/></li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">
<body>
<form name="alterSubRack" method="post" >
<table class="table table-info mb30 table-hover" >
<!--<td colspan="8" style="background-color:#5BC0DE;">-->
<thead>
<tr>
<th><spring:message code="stores.label.SubRackId"  text="default text"/></th>
<th ><spring:message code="stores.label.RackName"  text="default text"/></th>
<th ><spring:message code="stores.label.SubRackName"  text="default text"/></th>
<th ><spring:message code="stores.label.RackDescription"  text="default text"/></th>
<th ><spring:message code="stores.label.Status"  text="default text"/></th>
<th ><spring:message code="stores.label.Select"  text="default text"/></th>
</tr>
</thead>
<% int index=0; %>
  <c:if test = "${subRackLists != null}" >
      <c:forEach items="${subRackLists}" var="subRack"> 
<%
String classText = "";
int oddEven = index % 2;
if (oddEven > 0) {
classText = "text2";
} else {
classText = "text1";
}
%>
<tr>
<td ><input type="hidden" name="subRackIdList" onchange="setSelectbox('<%= index %>');" value="<c:out value="${subRack.subRackId}"/>"><c:out value="${subRack.subRackId}"/></td>
<td ><select class="form-control" style="width:200px;height:40px;"  name="rackIdList" onchange="setSelectbox('<%= index %>');">
<c:forEach items="${rackLists}" var="subRack1">  
<c:choose>
   <c:when test="${subRack.rackId==subRack1.rackId}" > 
<option value="<c:out value="${subRack1.rackId}"/>" onchange="setSelectbox('<%= index %>');" selected ><c:out value="${subRack1.rackName}"/></option> 
   </c:when>
    <c:otherwise>
<option value="<c:out value="${subRack1.rackId}"/>" onchange="setSelectbox('<%= index %>');" ><c:out value="${subRack1.rackName}"/></option>         
    </c:otherwise> 
</c:choose>
</c:forEach>    
</select></td>
<td><input type="text" class="form-control" style="width:200px;height:40px;" name="subRackNameList" value="<c:out value="${subRack.subRackName}"/>" onchange="setSelectbox('<%= index %>');" ></td>
<td><textarea class="form-control" style="width:200px;height:40px;" name="subRackDescriptionList" onchange="setSelectbox('<%= index %>');"><c:out value="${subRack.subRackDescription}"/></textarea></td>
<td><select class="form-control" style="width:200px;height:40px;" name="subRackStatusList" onchange="setSelectbox('<%= index %>');">
 <c:if test="${(subRack.subRackStatus=='n') || (subRack.subRackStatus=='N')}" >
 <option value="Y" ><spring:message code="stores.label.Active"  text="default text"/></option><option value="N" selected><spring:message code="stores.label.InActive"  text="default text"/></option>                           
 </c:if>   
<c:if test="${(subRack.subRackStatus=='y') || (subRack.subRackStatus=='Y')}" >
 <option value="Y" selected><spring:message code="stores.label.Active"  text="default text"/></option><option value="N"><spring:message code="stores.label.InActive"  text="default text"/></option>
</c:if>
</select></td>
<td width="40" height="30" ><input type="checkbox" name="selectedIndex" value='<%= index %>'></td>

</tr>
 <%
  index++;
  %>
</c:forEach>
</c:if>   
</table>
</td>
<center>
<input type="button" value="<spring:message code="stores.label.Save"  text="default text"/>" class="btn btn-success" onclick="submitPage();">
</center>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>