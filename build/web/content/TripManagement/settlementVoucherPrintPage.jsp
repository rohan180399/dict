<%-- 
    Document   : settlementVoucherPrintPage
    Created on : Mar 29, 2013, 8:15:15 PM
    Author     : Entitle
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <link rel="stylesheet" href="/throttle/css/rupees.css"  type="text/css" />

        <%@ page import="ets.domain.renderservice.business.RenderServiceTO" %>
        <%@ page import="ets.domain.renderservice.business.JobCardItemTO" %>
        <%@ page import="ets.domain.problemActivities.business.ProblemActivitiesTO" %>


        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <%@ page import="java.util.*" %>

        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <title>Cash Payment Voucher</title>
    </head>

    <script>



        function print()
        {
            var DocumentContainer = document.getElementById("printDiv");
            var WindowObject = window.open('', "TrackHistoryData",
            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }

        function back()
        {
            window.history.back()
        }

    </script>


    <body>
        <form name="suggestedPS" method="post">
            <div id="printDiv">
                <br>
                <br>
                <br>


                <table align="center" width="725" border="1" cellspacing="0" cellpadding="0"  style="border:1px; border-color:#E8E8E8; border-style:solid;" >
                    <tr>
                        <td>
                            <table align="center" width="725" border="1" cellspacing="0" cellpadding="0"  style="border:0px; border-color:#E8E8E8; border-style:solid;" >
                                <tr>
                                    <td height="27" colspan="6" align="left"  scope="row" style="border:3px;">
                                        <font size="3"> CHETTINAD LOGISTICS PRIVATE LIMITED,KARIKKALI  &emsp;&emsp;&emsp;&emsp; Hire Payment Voucher-SETTLEMENT </font>
                                    </td>

                                    <c:if test="${tripDetails != null}">
                                    <c:forEach items="${tripDetails}" var="td">
                                        <c:set var="routeName" value="${td.routeName}"/>                                        
                                    </c:forEach>
                                </c:if>

                                </tr>
                                <tr><td style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">&nbsp;</td></tr>
                                <tr>
                                    <td align="left" style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        Voucher No :</td>
                                    <td style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        <c:out value="${tripDate}"/></td>

                                    <td align="right" style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        Voucher Date :</td>
                                    <td style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        <c:out value="${tripDate}"/></td>

                                </tr>
                                <tr><td style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">&nbsp;</td></tr>
                                <tr>
                                    <td align="left" style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        Credit A/C :</td>
                                    <td colspan="3" style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        <c:out value="${tripDate}"/></td>
                                </tr>
                                <tr>
                                    <td align="left" style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        Remarks    :</td>
                                    <td colspan="3" style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        <c:out value="${tripDate}"/></td>
                                </tr>
                                <tr><td style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">&nbsp;</td></tr>
                                <tr>
                                    <td align="left" style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        Debit A/C </td>
                                    <td style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        <c:out value="${tripDate}"/></td>

                                    <td align="right" style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        On Account of </td>
                                    <td align="right" style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        Amount(SAR.)</td>
                                </tr>
                                <tr><td colspan="4" style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        -------------------------------------------------------------------------------------------------------------------------------</td></tr>

                                <tr>
                                    <td align="left" style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        <c:out value="${tripDate}"/> </td>
                                    <td style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        &nbsp;</td>

                                    <td align="right" style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        &nbsp;<c:out value="${tripDate}"/></td>
                                    <td align="right" style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        &nbsp;<c:out value="${tripDate}"/></td>
                                </tr>

                                <tr>
                                    <td colspan="3" style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        &nbsp;</td>
                                    <td align="right" style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        ------------------------------</td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        &nbsp;</td>
                                    <td align="right" style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        Voucher Total : <c:out value="${tripDate}"/></td>
                                    <td align="right" style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        &nbsp;<c:out value="${tripDate}"/></td>
                                </tr>



                                <tr><td colspan="4" style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        -------------------------------------------------------------------------------------------------------------------------------</td></tr>

                                <tr><td style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">&nbsp;</td></tr>

                                <tr>
                                    <td align="left" style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        Vehicle NO :
                                    </td>
                                    <td style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        <c:out value="${tripDate}"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        Movement Details :
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <table align="center" width="725" border="0" cellspacing="0" cellpadding="0"  style="border:0px; border-color:#E8E8E8; border-style:solid;">
                                            <tr>

                                                <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; font-size: 12px; width: 5px;"><b>Trip No</b></td>
                                                <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; font-size: 12px; width: 10px;"><b>Date</b></td>
                                                <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; font-size: 12px; width: 10px;"><b>From</b></td>
                                                <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; font-size: 12px; width: 10px;"><b>To</b></td>
                                                <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; font-size: 12px; width: 10px;"><b>Dist</b></td>
                                                <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; font-size: 12px; width: 10px;"><b>Qty</b></td>
                                                <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; font-size: 12px; width: 10px;"><b>D.Qty</b></td>
                                                <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; font-size: 12px; width: 10px;"><b>A.frieght</b></td>
                                                <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; font-size: 12px; width: 10px;"><b>M.frieght</b></td>
                                                <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; font-size: 12px; width: 10px;"><b>Crossing</b></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <c:if test="${tripAllowanceDetails != null}">
                                        <c:forEach items="${tripAllowanceDetails}" var="tad">
                                            <c:if test="${opLocation != null}">
                                                <c:forEach items="${opLocation}" var="fd">
                                                    <c:if test="${fd.locationId == tad.tripStageId}">
                                                        <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;"><c:out value="${fd.locationName}"/></td>
                                                    </c:if>
                                                </c:forEach>
                                            </c:if>
                                            <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;"><c:out value="${tad.tripAllowanceDate}"/></td>
                                            <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;"><c:out value="${tad.tripAllowanceAmount}"/></td>
                                            <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;"><c:out value="${tad.tripAllowancePaidBy}"/></td>
                                            <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;"><c:out value="${tad.tripAllowanceRemarks}"/></td>
                                        </c:forEach>
                                    </c:if>
                                </tr>


                                <tr><td style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">&nbsp;</td></tr>
                                <tr><td style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">&nbsp;</td></tr>
                                <tr><td style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">&nbsp;</td></tr>

                                <tr>
                                    <td  align="left" style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        Prepared by</td>
                                    <td  align="left" style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        Checked by</td>
                                    <td   align="right" style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        Passed by</td>
                                    <td  align="center" style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        Receiver</td>
                                    <td  align="right" style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        Cashier</td>
                                </tr>

                                
                            </table>
                            <table align="center" width="725" border="0" cellspacing="0" cellpadding="0"  style="border:1px; border-color:#E8E8E8; border-style:solid;" >
                                <br>
                                <br>
                                <br>
                                <br>
                                <tr align="center">
                                    <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">

                                        <input align="center" type="button" onclick="print();" value = " Print "   />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>