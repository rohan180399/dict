<%
    Date today = new Date();
    SimpleDateFormat sdftSql = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    Connection conn = null;

    String date = "";
    String time = "";
    String NewTripId = "";
    int status = 0;
    //TripId,IdentityNo,DeviceId,InOutIndication,LocationCode,InOutDateTime
    String userId = "1";
    String tripId = request.getParameter("TripId");
    String identityNo = request.getParameter("IdentityNo");
    String deviceId = request.getParameter("DeviceId");
    String inOutIndication = request.getParameter("InOutIndication");
    String locationId = request.getParameter("LocationCode");
    String inOutDateTime = request.getParameter("InOutDateTime");
    try{
        if(inOutDateTime != null && inOutDateTime.contains("$")) {
            date = inOutDateTime.split("\\$")[0];
            date = date.split("-")[2]+"-"+date.split("-")[1]+"-"+date.split("-")[0];
            time = inOutDateTime.split("\\$")[1];
            inOutDateTime = date+" "+time;
        } else if(inOutDateTime != null && inOutDateTime.contains(" ")) {
            date = inOutDateTime.split(" ")[0];
            date = date.split("-")[2]+"-"+date.split("-")[1]+"-"+date.split("-")[0];
            time = inOutDateTime.split(" ")[1];
            inOutDateTime = date+" "+time;
        } else {
            inOutDateTime = sdftSql.format(today);
        }

        String applicationPath = application.getRealPath("WEB-INF/classes");
        applicationPath = applicationPath.replace("\\", "/");
        java.io.FileInputStream fis = new java.io.FileInputStream(applicationPath+"/jdbc_url.properties");
        java.util.Properties props = new java.util.Properties();
        props.load(fis);

        String driverClass = props.getProperty("jdbc.driverClassName");
        String jdbcUrl = props.getProperty("jdbc.url");
        String userName = props.getProperty("jdbc.username");
        String password = props.getProperty("jdbc.password");
        Class.forName(driverClass);
        conn = DriverManager.getConnection(jdbcUrl, userName, password);
        /*Statement stmt = conn.createStatement();
        ResultSet SAR = stmt.executeQuery("SELECT Location_Id FROM ra_operation_location_master_epos where Location_Code='"+locationCode+"'");
        String locationId="";
        while(SAR.next()){
            locationId = SAR.getString("Location_Id");
        }*/        
        
        Statement stmt = conn.createStatement();
	ResultSet SAR = stmt.executeQuery("SELECT status FROM ra_trip_open_close_epos where TripId="+tripId+" and IdentityNo="+identityNo);
	String tripStatus="";
	while(SAR.next()){
		tripStatus = SAR.getString("status");
	}
	if(tripStatus.equals("Open") || tripStatus.equals("Close")){
		PreparedStatement pstmt = conn.prepareStatement("INSERT INTO ra_trip_vehicle_in_out_epos (TripId, IdentityNo, DeviceId, InOutIndication, Location_Id, InOutDateTime, Act_Ind, CreatedBy,CreatedDate) VALUES (?,?,?,?,?,?,?,?,now())");
		pstmt.setString(1, tripId);
		pstmt.setString(2, identityNo);
		pstmt.setString(3, deviceId);
		pstmt.setString(4, inOutIndication);
		pstmt.setString(5, locationId);
		pstmt.setString(6, inOutDateTime);
		pstmt.setString(7, "Y");
		pstmt.setString(8, userId);
		status = pstmt.executeUpdate();

		/*ResultSet res = stmt.executeQuery("SELECT last_insert_id() as tripId ");
		while(res.next()) {
		    NewTripId = res.getString("tripId");
		    out.print("Trip Sheet opned - ["+NewTripId+"]");
		}
		*/
		out.println((status+"$").trim());
	}
	else{out.println("0$");}
        if(conn != null) {
            conn.close();
        }
    }catch(Exception e) {
        out.println("0$");
        FPLogUtils.fpErrorLog((new StringBuilder()).append("Exception in EPOS Vehicle IN OUT --> ").append(e.getMessage()).toString());
    }
    finally{
        if(conn != null) {
            conn.close();

        }
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*,java.util.Date,java.text.SimpleDateFormat,ets.domain.util.FPLogUtils" %>