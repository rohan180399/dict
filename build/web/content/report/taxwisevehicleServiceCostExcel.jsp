

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <%@ page import="java.util.*" %>
    </head>



    <script>
        function setValues(){
            document.serviceCost.regNo.focus();
            if('<%=request.getAttribute("fromDate")%>'!='null'){
                document.serviceCost.fromDate.value ='<%=request.getAttribute("fromDate")%>';
                document.serviceCost.toDate.value ='<%=request.getAttribute("toDate")%>';
            }if('<%=request.getAttribute("regNo")%>' !='null'){
                document.serviceCost.regNo.value='<%=request.getAttribute("regNo")%>';
            }if('<%=request.getAttribute("custId")%>' != 'null'){
                document.serviceCost.custId.value='<%=request.getAttribute("custId")%>';
            }
            if('<%= request.getAttribute("reportType")%>' != 'null'){
                document.serviceCost.reportType.value='<%= request.getAttribute("reportType")%>';
            }

            
        }
  


    </script>

    <body onload="setValues();">

        <form name="serviceCost"  method="post" >

<%

String fileName = "attachment;filename=VehicleServiceTaxReport.xls";
response.setContentType("application/vnd.ms-excel;charset=UTF-8");
response.setHeader("Content-disposition",fileName);
%>
<b>
<%
            if(request.getAttribute("fromDate")!= null && !"".equals(request.getAttribute("fromDate"))){
                %>
                From Date: <%=request.getAttribute("fromDate")%>;

                To Date: <%=request.getAttribute("toDate")%>;
                <%
            }if(request.getAttribute("regNo") != null && !"".equals(request.getAttribute("regNo"))){
                %>
                Vehicle No: <%=request.getAttribute("regNo")%>;
                <%
            }if(request.getAttribute("custId") != null  && !"".equals(request.getAttribute("custId"))){
                %>
                Customer:<%=request.getAttribute("custId")%>;
                <%
            }
            if(request.getAttribute("reportType") != null && !"".equals(request.getAttribute("reportType"))){
                if(request.getAttribute("reportType").equals("2" )){
                %>
                Based On: Bill Date
                <%
                }else{
                    %>
                Based On: Job Card Date
                <%
                }
            }

%>
        </b>

            <c:set var="total" value="0"/>
            <c:set var="spareAmount" value="0" />
            <c:set var="laborAmount" value="0" />

            <c:set var="totalAmount"  value="0"/>
            <% int index = 0;
                        String classText = "";
                        int oddEven = 0;
            %>    
            <c:if test = "${serviceCostList != null}" >

                <center> 
                    <input type="button" class="button" name="print" value="print" onClick="printPage();" > &nbsp;&nbsp;&nbsp;
                    <input type="button" class="button" name="print" value="Export To Excel" onClick="submitForExcel();" > 
                </center>
                <br>
                <div id="printPage" >
                    <table align="center" border="1" cellpadding="0" cellspacing="0" width="850" id="bg" class="border">

                        <tr class="contenthead">
                            <td  class="contentsub">Sno</td>
                            <td  height="30" class="contentsub">Custmer Name</td>
                            <td  height="30" class="contentsub">Veh No</td>
                            <td  class="contentsub">JobCard Id</td>


                            <c:forEach items="${VatValues}" var="ser">


                                <td  height="30" class="contentsub">Vat@<c:out value="${ser.acd}"/></td>
                                <td  height="30" class="contentsub">Nett </td>
                            </c:forEach>
                            <td  height="30" class="contentsub">Contr.Tax</td>
                            <td  height="30" class="contentsub"> Contr. Amnt </td>
                            <td  height="30" class="contentsub">Spare Amnt </td>
                            <td  height="30" class="contentsub"> Labour Amnt </td>
                            <td  height="30" class="contentsub"> Service Tax (10%) </td>
                            <td  height="30" class="contentsub"> Edu Cess (2%) </td>
                            <td  height="30" class="contentsub"> Hr. Edu Cess (1%) </td>

                            <td  height="30" class="contentsub"> Total</td>

                        </tr>

                        <c:set var="totJcd" value="0" />

                        <c:forEach items="${serviceCostList}" var="service">
                            <%

                                        oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>


                            <tr>
                                <td class="<%=classText%>" height="30"> <%= index + 1%> </td>
                                <td class="<%=classText%>" height="30"><c:out value="${service.custName}"/></td>
                                <td class="<%=classText%>" height="30"><input type="hidden" name="vehicleNo" value="<c:out value="${service.regNo}"/>"><c:out value="${service.regNo}"/></td>
                                <td class="<%=classText%>" height="30"><c:out value="${service.totJc}"/>
                                <c:set var="totJcd" value="${totJcd + service.totJc}" />
                                </td>
                                <c:forEach items="${service.vatValues}"  var="vat">
                                    <td class="<%=classText%>" height="30" ><fmt:formatNumber value="${vat.taxAmount}" pattern="##.00"/></td>
                                    <td class="<%=classText%>" height="30"  ><c:out value="${vat.amount}"/></td>

                                </c:forEach>


                                <td class="<%=classText%>" height="30"  ><c:out value="${service.spareAmount}"/></td>
                                <td class="<%=classText%>" height="30"  ><c:out value="${service.laborAmount}"/></td>


                                    <c:if test = "${service.tax != '0.00'}" >
                                    <td class="<%=classText%>" height="30"  >
                                        <fmt:formatNumber value="${service.laborAmount * 10/100}" pattern="##.00"/>
                                    </td>
                                    <td class="<%=classText%>" height="30"  >
                                        <fmt:formatNumber value="${service.laborAmount * 0.2/100}" pattern="##.00"/>

                                    </td>
                                    <td class="<%=classText%>" height="30"  >
                                        <fmt:formatNumber value="${service.laborAmount * 0.1/100}" pattern="##.00"/>

                                    </td>
                                    </c:if>
                                    <c:if test = "${service.tax == '0.00'}" >
                                    <td class="<%=classText%>" height="30"  ><c:out value="${service.tax}"/></td>
                                    <td class="<%=classText%>" height="30"  ><c:out value="${service.tax}"/></td>
                                    <td class="<%=classText%>" height="30"  ><c:out value="${service.tax}"/></td>
                                    </c:if>
                                <td class="<%=classText%>" height="30" >
                                    <c:set var="total" value="${service.laborAmount+service.spareAmount + service.contractAmnt}"/>
                                    <fmt:formatNumber value="${total}" pattern="##.00"/>
                                <c:set var="totalAmount" value="${totalAmount + total}" />
                                    <c:set var="total" value="0"/>
                                </td>
                                <c:set var="spareAmount" value="${spareAmount + service.spareAmount}" />
                                <c:set var="laborAmount" value="${laborAmount + service.laborAmount}" />
                            </tr>


                            <%
                                        index++;
                            %>
                        </c:forEach>

                    </table>

                    <style>
                        .text1 {
                            font-family:Tahoma;
                            font-size:12px;
                            color:#333333;
                            background-color:#E9FBFF;
                            padding-left:10px;
                        }
                        .text2 {
                            font-family:Tahoma;
                            font-size:12px;
                            color:#333333;
                            background-color:#FFFFFF;
                            padding-left:10px;
                        }
                        .contentsub {
                            background-image:url(/throttle/images/button.gif);
                            background-repeat:repeat-x;
                            height:15px;
                            font-family:Tahoma;
                            padding-left:5px;
                            font-size:11px;
                            font-weight:bold;
                            color:#0070D4;
                            text-align:left;
                            padding-bottom:8px;
                        }

                        .contenthead {
                            background-image:url(/throttle/images/button.gif);
                            background-repeat:repeat-x;
                            height:15px;
                            font-family:Tahoma;
                            padding-left:5px;
                            font-size:11px;
                            font-weight:bold;
                            color:#0070D4;
                            text-align:center;
                            padding-bottom:8px;
                        }

                    </style>
                </div>

                 <br>
                  <table  align="center" border="0" cellpadding="0" cellspacing="0" width="500" id="bg" class="border">
                      <tr>
                      <td class="text1" height="30"> Total Jobcards </td>
                      <td class="text1" height="30"> <b><c:out value="${totJcd}"/> </b> </td>
                      </tr>
                      <tr>
                      <td class="text1" height="30"> Spare Amount </td>
                      <td class="text1" height="30"> <fmt:setLocale value="en_US" /><b>SAR: <fmt:formatNumber value="${spareAmount}" pattern="##.00"/></b> </td>
                      </tr>
                      <tr>
                      <td class="text1" height="30"> Labour Amount </td>
                      <td class="text1" height="30"> <fmt:setLocale value="en_US" /><b>SAR: <fmt:formatNumber value="${laborAmount}" pattern="##.00"/></b></td>
                      </tr>
                      <tr>
                      <td class="text1" height="30"> Total Amount </td>
                      <td class="text1" height="30"> <fmt:setLocale value="en_US" /><b>SAR: <fmt:formatNumber value="${totalAmount}" pattern="##.00"/></b></td>
                      </tr>

                  </table>
            </c:if>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>


    </body>
</html>
