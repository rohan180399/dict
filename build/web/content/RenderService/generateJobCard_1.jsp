<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

        <%@page language="java" contentType="text/html; charset=UTF-8"%>
        <%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
        <%@page import="java.util.Locale"%>

        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <%@ page import="ets.domain.service.business.ServiceTO" %>
        <%@ page import="java.util.*" %>
        <%@ page import="java.http.*" %>
        <%@ page import="ets.domain.mrs.business.MrsTO" %>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
		<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javasckript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

    



             <link rel="stylesheet" href="/throttle/css/jquery-ui.css">
        <script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
        <script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>





<script type="text/javascript">


            $(document).ready(function() {

                $("#datepicker").datepicker({
                    showOn: "button",
                    format: "dd-mm-yyyy",
                    autoclose: true,
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function() {
                $(".datepicker").datepicker({
                    format: "dd-mm-yyyy",
                    autoclose: true
                });

            });


        </script>



    <SCRIPT>

        function openPopup(jobCardId, probId){
            var url = '/throttle/handleTechnicianPopup.do?jobCardId='+jobCardId+'&probId='+probId;
            window.open( url, 'PopupPage', 'height=500,width=600,scrollbars=yes,resizable=yes');
        }
        function openCheckList(jobCardId, serviceId){
            var url = '/throttle/getServiceCheckList.do?jobCardId='+jobCardId+'&serviceId='+serviceId;
            window.open( url, 'PopupPage', 'height=1000,width=1000,scrollbars=yes,resizable=yes');
        }
        var httpRequest1;
        function getProblems(secId,sno) {

            if(secId!='null' ){

                var list1=document.getElementsByName("probId");
                while (list1[sno].childNodes[0]) {
                    list1[sno].removeChild(list1[sno].childNodes[0]);
                }

                var url='/throttle/getProblems.do?secId='+secId;

                if (window.ActiveXObject)


                    {
                        httpRequest1 = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)
                        {
                            httpRequest1 = new XMLHttpRequest();
                        }
                        httpRequest1.open("POST", url, true);
                        httpRequest1.onreadystatechange = function() {go(secId,sno); } ;
                        httpRequest1.send(null);
                    }
                }

                function go(secId,sno) {



                    if (httpRequest1.readyState == 4) {
                        if (httpRequest1.status == 200) {
                            var response = httpRequest1.responseText;
                            //alert(response);
                            //  response=0+'-'+'select'+response;
                            //alert(response);
                            var list=document.getElementsByName("probId");
                            var details=response.split(',');
                            for (i=1; i <details.length; i++) {
                                var temp = details[i].split('-');
                                var probId= temp[0];
                                var  probName = temp[1];
                                var  x=document.createElement('option');
                                var  name=document.createTextNode(probName);
                                x.appendChild(name);
                                x.setAttribute('value',probId+'-'+probName)
                                list[sno].appendChild(x);

                            }

                            if(secId==1016 || secId==1040) {

                                document.getElementById("technicianId"+sno).style.visibility = 'hidden';
                            }else{

                            document.getElementById("technicianId"+sno).style.visibility = 'visible';
                        }




                    }
                }


            }


            var rowCount=2;
            var sno=0;
            var index=0;
            var style="text1";
            var cntr = 0;
          
            function addRow(value,ind)
            {
                
                if(rowCount%2==0){
                    style="text2";
                }else{
                style="text1";
                }




            
            var tab = document.getElementById("addRows");            
            var lastElement = tab.rows.length;           
            var newrow = tab.insertRow(lastElement);
            
            if(index==0){
            index=ind;
            //sno=parseInt(lastElement)-2;
            if(value==0){
            sno++;
            }else{
            sno=parseInt(value)+1;
            }
            }else{
            index++;
            sno++;
            }            
            
            
            var cell = newrow.insertCell(0);
            var cell0 = "<td height='25' ><input type='hidden' name='validate' value='0'> "+sno+"</td>";
            cell.setAttribute("className",style);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(1);
            var cell1 = "<td class='text1' height='25'><select class='form-control' id='secId"+index+"'  name='secId' style='width:100px;' onchange='getProblems(this.value,"+index+")'><option  selected value='0'>---<spring:message code="service.label.Select"  text="default text"/>---</option><c:if test = "${sections != null}" ><c:forEach items="${sections}" var="sec"><option  value='<c:out value="${sec.secId}" />'><c:out value="${sec.secName}" /></c:forEach ></c:if> </select></td>";
            cell.setAttribute("className",style);
            cell.innerHTML = cell1;

            cell = newrow.insertCell(2);
            var cell2 = "<td class='text1' height='25'><select class='form-control' id='probId"+index+"' style='width:200px;'  name='probId'><option  selected value='0'>---<spring:message code="service.label.Select"  text="default text"/>---</option></select></td>";
            cell.setAttribute("className",style);
            cell.innerHTML = cell2;

            cell = newrow.insertCell(3);
            var cell3 = "<td class='text1' height='25'><textarea  name='symptoms' style='width:120px;' onchange='setSelectbox("+index+")' ></textarea></td>";
            cell.setAttribute("className",style);
            cell.innerHTML = cell3;

            cell = newrow.insertCell(4);
            var cell4 = " <td class='text1'  height='25'><select name='severity' onchange='setSelectbox("+index+")'  class='text2'><option  selected value='0'>-<spring:message code="service.label.Select"  text="default text"/>-</option><option value='1'><spring:message code="service.label.Low"  text="default text"/></option><option value='2' ><spring:message code="service.label.Medium"  text="default text"/></option><option value='3'><spring:message code="service.label.High"  text="default text"/></option></select></td>";
            cell.setAttribute("className",style);
            cell.innerHTML = cell4;

            cell = newrow.insertCell(5);
            var cell5 = "<td class='text1' height='25'><select name='technicianId' id='technicianId"+index+"'   class='text2' style='width:80px;' onChange='setSelectbox("+index+");newWO1(this.value,"+index+",1);'><option value='0'>--select--</option><c:if test = "${technicians != null}" ><c:forEach items="${technicians}" var="mfr"><option  value='<c:out value="${mfr.empId}" />'><c:out value="${mfr.empName}" /></c:forEach ></c:if> </select></td>";
            //var cell5 = "<td class='text1' height='25'><input type='hidden' name='technicianId' value='1005'>&nbsp;</td>";
            cell.setAttribute("className",style);
            cell.innerHTML = cell5;



            cell = newrow.insertCell(6);
            var cell5 = "<td class='text1' height='25' width='30'><input type='text'  size='9'  onchange='setSelectbox("+index+")'   readonly name='scheduledDate' id='scheduledDate"+index+"'   class='form-control pull-right datepicker'> </td>";

            cell.setAttribute("className",style);
            cell.innerHTML = cell5;


            cell = newrow.insertCell(7);
            var cell7 = "<td class='text1' height='25'><select class='text2' onchange='setSelectbox("+index+")'  name='status' style='width:80px;'><option value='U'><spring:message code="service.label.Notplanned"  text="Notplanned"/></option><option value='P'><spring:message code="service.label.Planned"  text="default text"/></option><option value='C' > <spring:message code="service.label.Completed"  text="default text"/></option></select></td>";
            cell.setAttribute("className",style);
            cell.innerHTML = cell7;

            cell = newrow.insertCell(8);
            cell7 = "<td class='text1' height='25'><textarea name='cremarks' style='width:127px;'  onkeyup='maxlength(this.value,300)' class='form-control'>ok</textarea></td>";
            cell.setAttribute("className",style);
            cell.innerHTML = cell7;
            //var temp=rowCount-1;

            cell = newrow.insertCell(9);
            var cell7 = "<td class='text1' height='30'> <input type='checkbox'  name='selectedindex' value='"+index+"'>  </td>";
            cell.setAttribute("className",style);
            cell.innerHTML = cell7;



            //index++;
            rowCount++;


              $(document).ready(function() {

                $("#datepicker").datepicker({
                    showOn: "button",
                    format: "dd-mm-yyyy",
                    autoclose: true,
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function() {
                $(".datepicker").datepicker({
                    format: "dd-mm-yyyy",
                    autoclose: true
                });

            });

        }




/////////////////////////
            var rowCount1=2;
            var sno1=0;
            var index1=0;
            var styl="text1";
            var rowIndex = 0;
            function addNonGracePeriod()
         {
                    if(parseInt(rowCount1) %2==0)
                        {
                            styl="text2";
                        }else{
                        styl="text1";
                    }
                    sno1++;

                    //alert('test');
                    //if( parseInt(rowIndex) > 0 && document.getElementsByName("mfrCode")[rowIndex].value!= '')
                    var tab = document.getElementById("nonGracePeriodServices");
                    var newrow = tab.insertRow(rowCount1);

                    var cell = newrow.insertCell(0);
                    var cell0 = "<td class='text1' height='25' >  "+sno1+"</td>";
                    cell.setAttribute("className",styl);
                    cell.innerHTML = cell0;

                    var cell = newrow.insertCell(1);
                    var cell0 = "<td class='text1' height='25'><select class='form-control'   name='NonGraceServiceId' onchange='getDueKmHm("+rowIndex+");setSelectbox1("+rowIndex+");'><option  selected value='0'>---Select---</option><c:if test = "${nonGracePeriodList != null}" ><c:forEach items="${nonGracePeriodList}" var="sec"><option  value='<c:out value="${sec.serviceId}" />'><c:out value="${sec.serviceId}" />-<c:out value="${sec.serviceName}" /></c:forEach ></c:if> </select></td>";
                    cell.setAttribute("className",styl);
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(2);
                    var cell1 = "<td class='text1' height='25'><input name='NonGraceDueKm' readonly class='form-control' type='text'></td>";
                    cell.setAttribute("className",styl);
                    cell.innerHTML = cell1;

                    cell = newrow.insertCell(3);
                    var cell2 = "<td class='text1' height='25'><input name='NonGraceBalKm' readonly class='form-control'    type='text'></td>";
                    cell.setAttribute("className",styl);
                    cell.innerHTML = cell2;

                    cell = newrow.insertCell(4);
                    var cell3 = " <td class='text1' height='25'><input name='NonGraceDueHm' size='5' readonly class='form-control'  type='text'></td>";
                    cell.setAttribute("className",styl);
                    cell.innerHTML = cell3;

                    cell = newrow.insertCell(5);
                    var cell4 = " <td class='text1' height='25'><input name='NonGraceBalHm' readonly  size='5'  class='form-control'  type='text'></td>";
                    cell.setAttribute("className",styl);
                    cell.innerHTML = cell4;

                    cell = newrow.insertCell(6);
                    var cell5 = " <td class='text1' height='25'><input name='NonGraceDate'  size='10'  class='form-control pull-right datepicker' id='NonGraceDate"+rowCount1+"' value='' type='text'>  </td>";
                    cell.setAttribute("className",styl);
                    cell.innerHTML = cell5;

                    cell = newrow.insertCell(7);
                    var cell6 = " <td class='text1' height='25'><select name='NonGraceTech' class='text2' onChange='setSelectbox1("+rowIndex+");' style='width:80px;' ><option value='0'>--select--</option><c:if test = "${technicians != null}" ><c:forEach items="${technicians}" var="mfr"><option  value='<c:out value="${mfr.empId}" />'><c:out value="${mfr.empName}" /></c:forEach ></c:if> </select></td>";
                    cell.setAttribute("className",styl);
                    cell.innerHTML = cell6;

                    cell = newrow.insertCell(8);
                    var cell1 = "<td class='text1' height='25'><select class='text2' name='nonGracePeriodStatus' onChange='setSelectbox1("+rowIndex+");' style='width:80px;'><option value='U'>UnPlanned</option><option value='P'>Planned</option><option value='C' > Completed</option></select><input type='checkbox' value='"+(rowCount1-1)+"' name='nonGraceSelectInd' > </td>";
                    cell.setAttribute("className","text2");
                    cell.innerHTML = cell1;

                    rowIndex++;
                    rowCount1++;
        }


var httpRequestHmKm;
function getDueKmHm(ind)
{
    var serviceId = document.getElementsByName('NonGraceServiceId');
    var vehicleId = document.workOrder.vehicleId.value;
    var km = document.workOrder.km.value;
    var hm = document.workOrder.hm.value;
    var url = '/throttle/getServiceHmKm.do?serviceId='+serviceId[ind].value+'&vehicleId='+vehicleId;
    url = url + '&km='+km+'&hm='+hm;

        if (window.ActiveXObject)
        {
        httpRequestHmKm = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else if (window.XMLHttpRequest)
        {
        httpRequestHmKm = new XMLHttpRequest();
        }
    httpRequestHmKm.open("GET", url, true);
    httpRequestHmKm.onreadystatechange = function() { processKmHm(ind); } ;
    httpRequestHmKm.send(null);
}


function processKmHm(ind)
{
    var dueKm = document.getElementsByName("NonGraceDueKm");
    var dueHm = document.getElementsByName("NonGraceDueHm");
    var balKm = document.getElementsByName("NonGraceBalKm");
    var balHm = document.getElementsByName("NonGraceBalHm");
    if (httpRequestHmKm.readyState == 4)
    {
    if(httpRequestHmKm.status == 200)
    {
        if(httpRequestHmKm.responseText.valueOf()!=""){
             var splt = httpRequestHmKm.responseText.valueOf().split('~');
             dueKm[ind].value = splt[0];
             dueHm[ind].value = splt[1];
             balKm[ind].value = splt[2];
             balHm[ind].value = splt[3];
        }else{
             dueKm[ind].value = '';
             dueHm[ind].value = '';
             balKm[ind].value = '';
             balHm[ind].value = '';
        }
    }
    else
    {
    alert("Error loading page\n"+ httpRequestHmKm.status +":"+ httpRequestHmKm.statusText);
    }
    }
}


function getWoDetail(woId){

window.open('/throttle/woDetail.do?woId='+woId, 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');


}




    </SCRIPT>


    <script type="text/javascript">


    function setFocus()
    {
        var date='<%=request.getAttribute("compDate")%>'
        if(date!='null'){
            document.workOrder.compDate.value=date;
        }
        var jRemarks='<%=request.getAttribute("jRemarks")%>';
        var newjRemarks = "<table border='0'>";
        if(jRemarks !='null'){
            //document.workOrder.jremarks.value=jRemarks;
            var jr = jRemarks.split("@");
            for(var j=0;j<jr.length; j++){
                newjRemarks = newjRemarks + "<tr><td align='left'>"+jr[j]+"</td></tr>";
            }
            newjRemarks=newjRemarks+"</table>";
            document.getElementById("jobCardRemarks").innerHTML=newjRemarks;
        }

    }

    </script>
   <div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="service.label.ViewJobCard"  text="ViewJobCard"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="general.label.service"  text="Service"/></a></li>
          <li class="active"><spring:message code="service.label.ViewJobCard"  text="ViewJobCard"/></li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">

    <body onload="setFocus();addNonGracePeriod();">
        <form name="workOrder" method="post">


            <!-- message table -->

<%@ include file="/content/common/message.jsp"%>

<%String jobCardRemarks = "";%>

            <table class="table table-info mb30 table-hover">
                    <thead>

			<tr>
                             <th colspan="6" ><b><spring:message code="service.label.JobCard"  text="default text"/>&nbsp;&nbsp; :&nbsp;&nbsp; <%=request.getAttribute("jcMYFormatNo")%> </b></th>


			</tr>
                    </thead>
                <tr>
                    <input type="hidden"  name="jobcardId"  value='<%=request.getAttribute("jobcardId")%>'>
                    <td><b><spring:message code="service.label.WorkOrderNo"  text="default text"/></b></td>
                    <input type="hidden" name="workOrderId" value='<%=request.getAttribute("workOrderId")%>'>
                    <input type="hidden" name="km" value='<%=request.getAttribute("km")%>'>
                    <input type="hidden" name="hm" value='<%=request.getAttribute("hm")%>'>
                    <input type="hidden" name="jcMYFormatNo" value='<%=request.getAttribute("jcMYFormatNo")%>'>

                    <td><%=request.getAttribute("workOrderId")%></td>
                    <td><b><spring:message code="service.label.Km"  text="default text"/></b></td>
                    <td><%=request.getAttribute("km")%></td>
                    <td><b><spring:message code="service.label.HourMeter"  text="default text"/></b></td>
                    <td><%=request.getAttribute("hm")%></td>
                </tr>

                <input type="hidden" name="vehicleId" value='<%=request.getAttribute("vehicleId")%>'>
                <input type="hidden" name="reqDate" value='<%=request.getAttribute("reqDate")%>'>
                <c:if test = "${vehicleDetails != null}" >
                    <c:forEach items="${vehicleDetails}" var="fservice">
                        <tr>
                            <td><b><spring:message code="service.label.VehicleNo"  text="default text"/></b></td>
                            <td><c:out value="${fservice.regno}"/></td>
                            <td><b><spring:message code="service.label.CommittedDelivery"  text="default text"/></b></td>
                            <td><%=request.getAttribute("reqDate")%></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td><b><spring:message code="service.label.MFR"  text="default text"/></b></td>
                            <td><c:out value="${fservice.mfrName}"/></td>
                            <td><b><spring:message code="service.label.Model"  text="default text"/></b></td>
                            <td><c:out value="${fservice.modelName}"/></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>




                    </c:forEach>
                </c:if>
                <tr>
                    <tr>
                        <td><b><spring:message code="service.label.CompletionSchedule"  text="default text"/></b></td>
                        <td> <input name="compDate" type="text" class="form-control pull-right datepicker" value="">
<!--                        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.workOrder.compDate,'dd-mm-yyyy',this)"/>-->

                        </td>
                        <td colspan="4" align="left">&nbsp;</td>
                    </tr>
                </tr>
            </table>

            <td>
            <br>

            <table align="center" >
                <%

            String classText = "text2";
            int index = 0;
            ArrayList jobCardList = (ArrayList) request.getAttribute("jobCardList");

            Iterator itr = jobCardList.iterator();
            OperationTO operationTO = null;
            while (itr.hasNext()) {
                operationTO = new OperationTO();
                operationTO = (OperationTO) itr.next();
                jobCardRemarks = operationTO.getRemarks();
             }
            if (jobCardList.size() != 0) {
                %>
                <c:if test="${jobCardList!=null}">
                    <table align="right" width="90%" cellpadding="0" cellspacing="0" border="0" class="border" >

                        <td colspan="5" align="center"><div align="center"><strong><spring:message code="service.label.CompletionSchedule"  text="default text"/></strong></div></td>


                        <tr>
                            <td ><div ><spring:message code="service.label.SNo"  text="default text"/></div></td>
                            <td ><div ><spring:message code="service.label.JobCardNo"  text="default text"/></td>
                            <td ><div  align="center"><spring:message code="service.label.Status"  text="default text"/></div></td>
                            <td ><div ><spring:message code="service.label.Date"  text="default text"/></td>
                            <td ><div ><spring:message code="service.label.Expense"  text="default text"/></td>
                        </tr>
                        <c:forEach items="${jobCardList}" var="fservice">
<%
                     classText = "";
                     int oddEven = index % 2;
                     if (oddEven > 0) {
                         classText = "text1";
                     } else {
                         classText = "text2";
                     }
%>

                            <tr>
                                <td id="blk"><%=index + 1%></td>
                                <td ><a href=# onclick="newWindow('<c:out value="${fservice.jobCardId}"/>','<c:out value="${fservice.workOrderId}"/>')"><c:out value="${fservice.jobCardId}"/></a></td>
                                <td ><c:out value="${fservice.status}"/></td>
                                <td ><c:out value="${fservice.scheduledDate}"/></td>
                                <td >
				<fmt:setLocale value="en_US" /><spring:message code="service.label.SAR."  text="default text"/> <fmt:formatNumber value="${fservice.totalAmount}" pattern="##.00"/>  </td>
                            </tr>
                            <%
                         index++;
                            %>
                        </c:forEach>
                    </table>

                </c:if>
                <%
            }
                %>






                <%

            index = 0;
            ArrayList problemList = (ArrayList) request.getAttribute("problemList");
            if (problemList.size() != 0) {
                %>
                <c:if test="${problemList!=null}">
             
                    <table class="table table-info mb30 table-hover" >
                    <thead>
                        <tr>
                            <th>Last 5 Complaints</th>
                        </tr>
                        <tr>
                            <th  ><spring:message code="service.label.SNo"  text="default text"/></th>
                            <th  ><spring:message code="service.label.ProblemName"  text="Complaint"/></th>
                            <th  ><spring:message code="service.label.Description"  text="Description"/></th>
                             <th >Symptoms</th>
                        </tr>
                    </thead>
                        <c:forEach items="${problemList}" var="service">
                            
                            <tr>
                                <td ><%=index + 1%></td>
                                <td><c:out value="${service.probName}"/></td>
                                <td><c:out value="${service.desc}"/></td>
                                   <td align="center"><c:out value="${service.symptoms}"/></td>
                            </tr>
                            <%
                     index++;
                            %>
                        </c:forEach>
                    </table>
            
                </c:if>
                <%
            }
                %>

            </table>
            
            <%

            int count = 0;

            int service = 0;
            int c = 0;

            ArrayList routineService = (ArrayList) request.getAttribute("routineServiceList");
            if (routineService.size() != 0) {

            %>
            <c:if test = "${routineServiceList != null}" >

                <br>
<!--                <table id="bg"  border="0" cellpadding="0" align="center" cellspacing="0" width="90%" class="border">-->
                    <table class="table table-info mb30 table-hover" id="bg">
                        <thead>
                            <tr>
                                <th colspan="10" >
                            <div align="center" ><strong><spring:message code="service.label.RoutineServiceActivities"  text="default text"/></strong><br></div>
                                </th>
                            </tr>
                        
                    <tr>

                            <th  ><spring:message code="service.label.Done"  text="Done"/></th>
                            <th  ><spring:message code="service.label.Schedule"  text="Schedule"/></th>
                            <th  >Service Name</th>
                            <th > <div >Due Km</div></th>
                            <th ><div > Balance Km</div></th>
                            <th  ><div >Due HourMeter</div></th>
                            <th > <div >Balance HourMeter</div></th>
                            <th  ><div >Serviced Date / Scheduled Date</div></th>
                            <th  >TechnicianName</th>
                            <th  >Status</th>
                        </tr>
                        </thead>
                        <c:forEach items="${routineServiceList}" var="service">
                            <%

                     classText = "";
                     int oddEven = service % 2;
                     if (oddEven > 0) {
                         classText = "text1";
                     } else {
                         classText = "text2";
                     }
                     c = 0;
                            %>
                            <tr>
                            <%
                                 int test = 0;
                                 ArrayList scheduledServices = (ArrayList) request.getAttribute("scheduledServices");
                                 if (scheduledServices.size() != 0) {
                                 int cntrVal = 0;
                                %>
                                <c:forEach items="${scheduledServices}" var="temp">

                                    <c:if test = "${service.serviceId == temp.serviceId}" >
                                        <c:if test = "${temp.empId == ''}" >
                                            <td><input type="checkbox" onclick="getDetails('<%=service%>',1)"  id="servicesDone<%=service%>" name="servicesDone" value='<%=service%>'></td>
                                            <td><input type="checkbox" onclick="getDetails('<%=service%>',0)" id="selectedServices<%=service%>"  name="selectedServices" value='<%=service%>'></td>
                                        </c:if>

                                           <% cntrVal++;
                                           %>
                                        <c:if test = "${temp.empId != ''}" >

                                            <td><a href="#" onclick="openCheckList(<%=request.getAttribute("jobcardId")%>,<c:out value="${service.serviceId}"/>);return false;"> checklist</a>  </td>
                                            <td>Yes</td>
                                        </c:if>
                                    </c:if>
                                </c:forEach>
                                <% if (cntrVal == 0){
                                    %>
                                    <td><input type="checkbox" onclick="getDetails('<%=service%>',1)"  id="servicesDone<%=service%>" name="servicesDone" value='<%=service%>'></td>
                                     <td><input type="checkbox" onclick="getDetails('<%=service%>',0)" id="selectedServices<%=service%>"  name="selectedServices" value='<%=service%>'></td>
                                    <%
                                    }
                                           %>
                                <%}else{%>

                                <td><input type="checkbox" onclick="getDetails('<%=service%>',1)"  id="servicesDone<%=service%>" name="servicesDone" value='<%=service%>'></td>
                                <td><input type="checkbox" onclick="getDetails('<%=service%>',0)" id="selectedServices<%=service%>"  name="selectedServices" value='<%=service%>'></td>
                                <%}%>
                                <input type="hidden" id="serviceId<%=service%>" name="serviceId" value='<c:out value="${service.serviceId}"/>'>
                                <input type="hidden" id="serviceName<%=service%>" name="serviceName" value='<c:out value="${service.serviceName}"/>'>


                                <td > <c:out value="${service.serviceName}"/></td>
                                <td ><c:out value="${service.kmReading}"/></td>

                                <td >
                                    <c:if test = "${service.balanceKm < 2000}" >
                                        <font color="red"><b><c:out value="${service.balanceKm}"/></b></font>
                                    </c:if>
                                    <c:if test = "${service.balanceKm > 2000}" >
                                        <font color="green"><b><c:out value="${service.balanceKm}"/></b></font>
                                    </c:if>
                                </td>
                                <td ><c:out value="${service.hourMeter}"/></td>
                                <td ><c:out value="${service.balanceHm}"/></td>
                                <%
                      test = 0;
                     scheduledServices = (ArrayList) request.getAttribute("scheduledServices");
                     if (scheduledServices.size() != 0) {

                                %>

                                <c:if test = "${scheduledServices != null}" >
                                    <c:forEach items="${scheduledServices}" var="temp">

                                        <c:if test = "${service.serviceId == temp.serviceId}" >
                                            <%
                             test++;
                                            %>

                                            <td><input type="text" name="servicedDate" size="10" onchange="setSchedule('<%=service%>')" id="servicedDate<%=service%>" value='<c:out value="${temp.scheduledDate}"/>' class="form-control pull-right datepicker"></td>

                                            <td>
                                                <select name="tech" id="tech<%=service%>" onchange="setSchedule('<%=service%>')" style="width:200px;" >
                                                    <c:if test = "${technicians != null}" >
                                                        <option   value="0">--select--</option>
                                                        <c:forEach items="${technicians}" var="mfr">
                                                            <c:choose>
                                                                <c:when test="${temp.empId==mfr.empId}">
                                                                    <option selected  value='<c:out value="${mfr.empId}" />'><c:out value="${mfr.empName}" />
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <option  value='<c:out value="${mfr.empId}" />'><c:out value="${mfr.empName}" />
                                                                </c:otherwise>
                                                            </c:choose>

                                                        </c:forEach >
                                                    </c:if>

                                            </select></td>

                                            <td ><select name="serviceStatus" onchange="setSchedule('<%=service%>')" id="serviceStatus<%=service%>">
                                                    <c:if test = "${temp.status =='N'}" >
                                                        <option selected value='U'>NotPlanned</option>
                                                        <option  value='P'>Planned</option>
                                                        <option  value='C' > Completed</option>
                                                    </c:if>
                                                    <c:if test = "${temp.status =='P'}" >
                                                        <option selected value='P'>Planned</option>
                                                        <option value='N'>NotPlanned</option>
                                                        <option  value='C' > Completed</option>
                                                    </c:if>
                                                    <c:if test = "${temp.status =='C'}" >
                                                        <option selected value='C' > Completed</option>
                                                        <option value='N'>NotPlanned</option>
                                                        <option  value='P'>Planned</option>
                                                    </c:if>
                                            </select></td>

                                        </c:if>

                                    </c:forEach>
                                    <%if (test == 0) {
                                    %>

                                    <td><input type="text" size="10"  name="servicedDate" onchange="setSchedule('<%=service%>')" id="servicedDate<%=service%>" value='' class="form-control pull-right datepicker"></td>


                                    <td>
                                        <select name="tech" id="tech<%=service%>" onchange="setSchedule('<%=service%>')" style="width:80px;" >
                                            <c:if test = "${technicians != null}" >
                                                <option   value="0">--select--</option>
                                                <c:forEach items="${technicians}" var="mfr">
                                                    <option  value='<c:out value="${mfr.empId}" />'><c:out value="${mfr.empName}" />
                                                </c:forEach >
                                            </c:if>

                                    </select></td>

                                    <td ><select name="serviceStatus" onchange="setSchedule('<%=service%>')" id="serviceStatus<%=service%>">
                                            <option selected value='U'>NotPlanned</option>
                                            <option  value='P'>Planned</option>
                                            <option  value='C' > Completed</option>
                                    </select></td>



                                    <%
                             }
                                    %>

                                </c:if>


                                <%
                         } else {
                                %>
                                <td><input type="text" name="servicedDate"  size="10"  onchange="setSchedule('<%=service%>')" id="servicedDate<%=service%>" value='' class="form-control pull-right datepicker"></td>

                                <td>
                                    <select name="tech" id="tech<%=service%>" onchange="setSchedule('<%=service%>')" style="width:80px;" >
                                        <c:if test = "${technicians != null}" >
                                            <option   value="0">--select--</option>
                                            <c:forEach items="${technicians}" var="mfr">
                                                <option  value='<c:out value="${mfr.empId}" />'><c:out value="${mfr.empName}" />
                                            </c:forEach >
                                        </c:if>

                                </select></td>

                                <td ><select name="serviceStatus" onchange="setSchedule('<%=service%>')" id="serviceStatus<%=service%>">
                                        <option selected value='U'>NotPlanned</option>
                                        <option  value='P'>Planned</option>
                                        <option  value='C' > Completed</option>
                                </select></td>


                                <%
                     }
                                %>

                            </tr>
                            <%

                     service++;
                            %>
                        </c:forEach>

                </tbody></table>
                <br>
            </c:if>

<!--srini commented this for Tahoos on 15 June 2016            -->
<div style="display:none;">
<!--<table id="nonGracePeriodServices"  border="0" cellpadding="0" align="center" cellspacing="0" width="90%" class="border">-->
    <table class="table table-info mb30 table-hover" id="nonGracePeriodServices">

                    <thead>
                        <tr>
                            <th colspan="9" >
                                <div align="center" ><strong>Non Due Routine Service Activities</strong><br></div>
                            </th>
                        </tr>
                    
			<tr>
                            <th  >Done</th>
                            <th  >Service Name</th>
                            <th > Due Km</th>
                            <th > Balance Km</th>
                            <th  >Due HourMeter</th>
                            <th > Balance HourMeter</th>
                            <th  >Serviced Date/Scheduled Date</th>
                            <th  >Technician</th>
                            <th  >Status</th>
                        </tr>
                        </thead>
</table>
<br>
<center> <input type="button" class="button" value="Add Row" onClick="addNonGracePeriod()"> </center>

</div>
<!--srini commented this for Tahoos on 15 June 2016            -->









            <%

            }
            int pIndex = 0;
            int woPId = 0;
            %>
            <br>

            <c:if test = "${workOrderProblemDetails != null}" >                
                
                    <table class="table table-info mb30 table-hover">
                    <thead>

                    

                        <tr>
                        <th colspan="10" align="center"><strong><spring:message code="service.label.JobCardComplaints"  text="default text"/></strong></th>
                        </tr>

                        <tr>
                            <th ><div ><spring:message code="service.label.SNo"  text="default text"/></div></th>
                            <th ><div ><spring:message code="service.label.Section"  text="default text"/></div></th>
                            <th ><div ><spring:message code="service.label.Complaints"  text="default text"/></div></th>
                            <th ><div ><spring:message code="service.label.Symptoms"  text="default text"/></div></th>
                            <th ><div ><spring:message code="service.label.Severity"  text="default text"/></div></th>
                            <th ><div ><spring:message code="service.label.Technician"  text="default text"/></div></th>
                            <th ><div ><spring:message code="service.label.scheduledDate"  text="default text"/></div> </th>
                            <th ><div ><spring:message code="service.label.Status"  text="default text"/></div></th>
                            <th ><div ><spring:message code="service.label.Remarks"  text="default text"/></div></th>
                            <th colspan="6" ><div ><spring:message code="service.label.Select"  text="default text"/></div></th>
                         </tr>
                    </thead>
                        <c:forEach items="${workOrderProblemDetails}" var="prob">
                            <%
                                classText = "";
                                int oddEven = pIndex % 2;
                                if (oddEven > 0) {
                                    classText = "text1";
                                } else {
                                    classText = "text2";
                                }
                            %>
                            <c:if test = "${prob.identifiedby !='Technician'}" >
                            <tr>
                                <input type='hidden' name='validate' value='1'>
                                <input name="probId" id="probId<%=pIndex%>" type="hidden" value='<c:out value="${prob.probId}"/>'>
                                <input name="secId" id="secId<%=pIndex%>" type="hidden" value='<c:out value="${prob.secId}"/>'>
                                <input name="secName" type="hidden" value='<c:out value="${prob.secName}"/>'>
                                <input name="probName" id="probName<%=pIndex%>" type="hidden" value='<c:out value="${prob.probName}"/>'>
                                <input name="symptoms" type="hidden" value='<c:out value="${prob.symptoms}"/>'>
                                <input name="severity" type="hidden" value='<c:out value="${prob.severity}"/>'>
                                <td><%=pIndex + 1%></td>
                                <td><c:out value="${prob.secName}"/></td>

                                <td><c:out value="${prob.probName}"/></td>
                                <td><c:out value="${prob.symptoms}"/></td>
                                <c:if test = "${prob.severity ==1}" >
                                    <td>Low</td>
                                </c:if>
                                <c:if test = "${prob.severity ==2}" >
                                    <td>Medium</td>
                                </c:if>
                                <c:if test = "${prob.severity ==3}" >
                                    <td>High</td>
                                </c:if>

                                <c:if test = "${prob.secId!=1016  || prob.secId==1040 }" >
                                    <td>
                                            <input type="hidden" name="technicianId" value="1005" >
<%--                                        <select name="technicianId"  style="width:80px;" onchange="newWO1(this.value,<%=pIndex%>,0);">
                                            <c:if test = "${prob.empId ==0}" >
                                                <option  selected value="0">--select--</option>
                                                <c:if test = "${technicians != null}" >
                                                    <c:forEach items="${technicians}" var="mfr">
                                                        <option  value='<c:out value="${mfr.empId}" />'><c:out value="${mfr.empName}" />

                                                    </c:forEach >
                                                </c:if>
                                            </c:if>


                                            <c:if test = "${ prob.empId !=0}" >
                                                <c:if test = "${technicians != null}" >
                                                    <option   value="0">--select--</option>
                                                    <c:forEach items="${technicians}" var="mfr">
                                                        <c:choose>
                                                            <c:when test="${prob.empId==mfr.empId}">
                                                                <option selected  value='<c:out value="${mfr.empId}" />'><c:out value="${mfr.empName}" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <option  value='<c:out value="${mfr.empId}" />'><c:out value="${mfr.empName}" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:forEach >

                                                </c:if>
                                            </c:if>
                                                                    
                                        </select>--%>
    <a href="" onclick="openPopup(<%=request.getAttribute("jobcardId")%>,<c:out value="${prob.probId}"/>);return false;" ><spring:message code="service.label.Technicians"  text="default text"/></a>
                                    </td>
                                </c:if>
                                <c:if test = "${prob.secId==1016 }" >
                                    <input type="hidden" name="technicianId" value="1" >
                                    <%
            int test = 0;
            ArrayList bodyWorksList = (ArrayList) request.getAttribute("bodyWorksList");
            if (bodyWorksList.size() == 0) {
                                    %>
                                    <td >Generate WO</td>
                                    <%
                                         count++;
                                     } else {
                                    %>
                                    <c:if test = "${bodyWorksList != null}" >
                                        <c:forEach items="${bodyWorksList}" var="body">


                                            <c:if test = "${body.probId==prob.probId}" >
                                                <%
                                         test++;
                                         if(test <=1)
                                                %>
                                                <td >WO No:<%=test %><a href="" onclick="getWoDetail('<c:out value="${body.workOrderId}"/>');"><c:out value="${body.workOrderId}"/></a></td>
                                            </c:if>




                                        </c:forEach >
                                    </c:if>
                                    <%
                                         if (test == 0) {
                                    %>
                                    <c:if test = "${body.probId!=prob.probId}" >
                                        <td >Generate WO</td>
                                        <%
                                        count++;
                                        %>
                                    </c:if>
                                    <%
                }
            }
                                    %>

                                </c:if>
                                <td><input type="text"  size="10" id="scheduledDate<%=pIndex%>" name="scheduledDate" value='<c:out value="${prob.scheduledDate}"/>' onchange="setSelectbox(<%= pIndex %>)" class="form-control pull-right datepicker"></td>
                                <td ><select name="status" >
                                        <c:if test = "${prob.status =='N'}" >
                                            <option selected value='U'><spring:message code="service.label.Notplanned"  text="default text"/></option>
                                            <option  value='P'><spring:message code="service.label.Planned"  text="default text"/></option>
                                            <option  value='C' ><spring:message code="service.label.Completed"  text="default text"/> </option>
                                        </c:if>
                                        <c:if test = "${prob.status =='P'}" >
                                            <option selected value='P'><spring:message code="service.label.Planned"  text="default text"/></option>
                                            <option value='N'><spring:message code="service.label.Notplanned"  text="default text"/></option>
                                            <option  value='C' ><spring:message code="service.label.Completed"  text="default text"/></option>
                                        </c:if>
                                        <c:if test = "${prob.status =='C'}" >
                                            <option selected value='C' ><spring:message code="service.label.Completed"  text="default text"/></option>
                                            <option value='N'><spring:message code="service.label.Notplanned"  text="default text"/></option>
                                            <option  value='P'><spring:message code="service.label.Planned"  text="default text"/></option>
                                        </c:if>
                                </select></td>
                                <td >
                                <textarea name="cremarks"  style="width:127px;"  onkeyup="maxlength(this.form.remark,300)" ><c:out value="${prob.cRemarks}"/>ok</textarea>
                                </td>
                                <td ><input type="checkbox" name="selectedindex" value='<%= pIndex %>'></td>
                                <input type="hidden"  name="cause"  value="">
                                <input name="remark" type="hidden"  value="">


                            </tr>
                            <%
                        pIndex++;
                        woPId++;
                            %>
                            </c:if>
                        </c:forEach>

                </table>
            </c:if>
            <br>





                <table class="table table-info mb30 table-hover" id="addRows" >
                    <thead>
                <tr>

                        <th colspan="10" align="center"><strong><spring:message code="service.label.IdentifiedComplaints"  text="default text"/></strong></th>
                        
                    </tr>

                    <%
            int sno = 0;
                    %>
                    <c:if test = "${jobCardProblemDetails != null}" >

                        <tr>
                            <td   ><spring:message code="service.label.SNo"  text="default text"/></td>
                            <td  ><spring:message code="service.label.Section"  text="default text"/></td>
                            <td   ><spring:message code="service.label.Problem"  text="default text"/></td>
                            <td   ><spring:message code="service.label.Symptoms"  text="default text"/></td>
                            <td   ><spring:message code="service.label.Severity"  text="default text"/></td>
                            <td   ><spring:message code="service.label.Technician"  text="default text"/></td>
                            <td   ><spring:message code="service.label.scheduledDate"  text="default text"/></td>
                            <td   ><spring:message code="service.label.Status"  text="default text"/></td>
                            <td   ><spring:message code="service.label.Remarks"  text="default text"/></td>
                            <td   ><spring:message code="service.label.Select"  text="default text"/></td>


                        </tr>
                        <c:forEach items="${jobCardProblemDetails}" var="prob">
                        <%
            classText = "";
            int odd = pIndex % 2;
            if (odd > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }

                        %>
                        <tr>
                            <c:if test = "${prob.identifiedby=='Technician'}" >
                            <input type='hidden' name='validate' value='1'>
                            <td><%=sno + 1%></td>
                            <input name="probId" id="probId<%=pIndex%>" type="hidden" value='<c:out value="${prob.probId}"/>'>
                            <input name="secId" id="secId<%=pIndex%>" type="hidden" value='<c:out value="${prob.secId}"/>'>
                            <input name="secName" type="hidden" value='<c:out value="${prob.secName}"/>'>
                            <input name="probName" id="probName<%=pIndex%>" type="hidden" value='<c:out value="${prob.probName}"/>'>
                            <input name="symptoms" type="hidden" value='<c:out value="${prob.symptoms}"/>'>
                            <input name="severity" type="hidden" value='<c:out value="${prob.severity}"/>'>
                            <td><c:out value="${prob.secName}"/></td>
                            <td><c:out value="${prob.probName}"/></td>
                            <td><c:out value="${prob.symptoms}"/></td>
                            <c:if test = "${prob.severity ==1}" >
                                <td>Low</td>
                            </c:if>
                            <c:if test = "${prob.severity ==2}" >
                                <td>Medium</td>
                            </c:if>
                            <c:if test = "${prob.severity ==3}" >
                                <td>High</td>
                            </c:if>

                            <c:if test = "${prob.secId!=1016 || prob.secId==1040 }" >



                                <td>
                                    <input type="hidden" name="technicianId" value="1005">
                                    <%--
                                    <select name="technicianId"  style="width:80px;" onchange="newWO1(this.value,<%=pIndex%>,0);">
                                        <c:if test = "${prob.empId ==0}" >
                                            <option  selected value="0">--select--</option>
                                            <c:if test = "${technicians != null}" >
                                                <c:forEach items="${technicians}" var="mfr">
                                                    <option  value='<c:out value="${mfr.empId}" />'><c:out value="${mfr.empName}" />

                                                </c:forEach >
                                            </c:if>
                                        </c:if>


                                        <c:if test = "${prob.empId !=0}" >
                                            <c:if test = "${technicians != null}" >
                                                <option   value="0">--select--</option>
                                                <c:forEach items="${technicians}" var="mfr">
                                                    <c:choose>
                                                        <c:when test="${prob.empId==mfr.empId}">
                                                            <option selected  value='<c:out value="${mfr.empId}" />'><c:out value="${mfr.empName}" />
                                                        </c:when>
                                                        <c:otherwise>
                                                            <option  value='<c:out value="${mfr.empId}" />'><c:out value="${mfr.empName}" />
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:forEach >

                                            </c:if>
                                        </c:if>
                                </select>--%>
<a href="#" onclick="openPopup(<%=request.getAttribute("jobcardId")%>,<c:out value="${prob.probId}"/>);return false;" >technicians</a>
                                </td>
                            </c:if>
                            <c:if test = "${prob.secId==1016 || prob.secId==1040 }" >
                                <input type="hidden" name="technicianId" value="1" >
                                <%
            int test1 = 0;
            ArrayList bodyWorksList1 = (ArrayList) request.getAttribute("bodyWorksList");
            if (bodyWorksList1.size() == 0) {
                                %>
                                <td >Generate WO</td>
                                <%
                                   count++;
                               } else {
                                %>
                                <c:if test = "${bodyWorksList != null}" >
                                    <c:forEach items="${bodyWorksList}" var="body">

                                        <c:if test = "${body.probId==prob.probId }" >
                                            <%
                                   test1++;
                                            %>
                                            <td >WO No:
                                                <a href="" onclick="getWoDetail('<c:out value="${body.workOrderId}"/>');"><c:out value="${body.workOrderId}"/></a>
                                            </td>
                                        </c:if>


                                    </c:forEach >
                                </c:if>
                                <%
                                   if (test1 == 0) {
                                %>
                                <c:if test = "${body.probId!=prob.probId}" >
                                    <td >Generate WO</td>
                                    <%
                                    count++;
                                    %>
                                </c:if>
                                <%
                }
            }
                                %>
                            </c:if>

                            <td><input type="text"  size="10" name="scheduledDate" id="scheduledDate<%=pIndex%>"  value='<c:out value="${prob.scheduledDate}"/>' onchange="setSelectbox(<%= pIndex %>)" class="form-control pull-right datepicker"></td>
                            <td ><select name="status" >
                                    <c:if test = "${prob.status =='N'}" >
                                        <option selected value='U'>NotPlanned</option>
                                        <option  value='P'>Planned</option>
                                        <option  value='C' > Completed</option>
                                    </c:if>
                                    <c:if test = "${prob.status =='P'}" >
                                        <option selected value='P'>Planned</option>
                                        <option value='N'>NotPlanned</option>
                                        <option  value='C' > Completed</option>
                                    </c:if>
                                    <c:if test = "${prob.status =='C'}" >
                                        <option selected value='C' > Completed</option>
                                        <option value='N'>NotPlanned</option>
                                        <option  value='P'>Planned</option>
                                    </c:if>
                            </select></td>
                            <td  >
                            <textarea name="cremarks"   style="width:127px;"  onkeyup="maxlength(this.form.remark,300)"><c:out value="${prob.cRemarks}"/></textarea>
                            </td>
                            <td ><input type="checkbox" name="selectedindex" value='<%= pIndex %>'></td>
							<input type="hidden"  name="cause"  value="">
							<input name="remark" type="hidden"  value="">


                        </tr>
                        <%
            pIndex++;
            sno++;
                        %>
                    </c:if>
                    </c:forEach>
                    </c:if>




            </table>

            <br>

            <center>
                <input type="button" class="button" value="<spring:message code="service.label.ADDROW"  text="default text"/>" onClick="addRow('<%=sno%>',<%=pIndex%>)">

                <br>
                <br>
                <div class="text"><b><spring:message code="service.label.JobCardRemarks"  text="default text"/> </b></div>
                <div id="jobCardRemarks" align="center" class="text"></div>
                <textarea name="jremarks"  style="width:250px;height:60px;"  onkeyup="maxlength(this.form.remark,300)"></textarea>
                <br>
                <br>
                <input type="button" class="button" value="<spring:message code="service.label.Save"  text="default text"/>" onclick="submitPage()">
                <input type="button" class="button" value="<spring:message code="service.label.Print"  text="default text"/>" onclick="printPage()">
                <%
            if (count != 0) {
                %>
                <input type="button" class="button" value="GenerateW.O" onclick="generateWO()">
                <%
            }
                %>
                <input type="hidden" name="buttonClick" value="">
            </center>


        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    <script type="text/javascript">
        var woCount=0
        function newWindow(jobcardId,workorderId){

            window.open('/throttle/previousJobCard.do?jobcardId='+jobcardId+"&workOrderId="+workorderId, 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
        }
        function newWO(){
            window.open('/throttle/content/RenderService/bodyParts.html', 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
            window.close();
        }
        function newWO1(tech,index,check){

            var jobcardId=document.workOrder.jobcardId.value;
            if(check==1){
                var temp=document.getElementById("probId"+index).value;
                var p=temp.split('-');
                var probId=p[0];
                var probName=p[1];
            }else{


            var probId=document.getElementById("probId"+index).value;
            var probName=document.getElementById("probName"+index).value;
        }

        if(tech==1 ){
            window.open('/throttle/externalTech.do?probId='+probId+"&jobcardId="+jobcardId+"&probName="+probName, 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
        }

        setSelectbox(index);
    }


    function nonGraceServiceValidate()
    {
        var serviceId = document.getElementsByName("NonGraceServiceId");
        var selectedInd = document.getElementsByName("nonGraceSelectInd");
        var serviceDate = document.getElementsByName("NonGraceDate");
        var tech = document.getElementsByName("NonGraceTech");

        for(var i=0;i<selectedInd.length;i++){
            if(selectedInd[i].checked==true ){
                if(serviceId[i].value=='0'){
                    alert("Please Select Service");
                    serviceId[i].focus();
                    return 'fail';
                }
                else if(serviceDate[i].value==''){
                    alert("Please Enter Schedule Date");
                    return 'fail';
                }
                else if(tech[i].value=='0'){
                    alert("Please Select Technician");
                    tech[i].focus();
                    return 'fail';
                }
            }
        }
        return 'pass';
    }


    function submitPage()
    {
        var x=0;
        var index = document.getElementsByName("selectedindex");
        var serIndex=document.getElementsByName("selectedServices");
        var nonGraceIndex = document.getElementsByName("nonGraceSelectInd");

        if(nonGraceServiceValidate() == 'fail'){
            return;
        }

        for(var i=0;(i<index.length && index.length!=0);i++){
            if(index[i].checked){
                x++;
            }
        }

        for(var i=0;(i<nonGraceIndex.length && nonGraceIndex.length!=0);i++){
            if(nonGraceIndex[i].checked){
                x++;
            }
        }

        for(var i=0;(i<serIndex.length && serIndex.length!=0);i++){
            if(serIndex[i].checked){
                x++;
            }
        }
        //var checValidate = selectedItemValidation();
        //    if(checValidate == 'SubmitForm'){
        if(x!=0){
            if(confirm("Have you entered Technician Effort Details")){
                document.workOrder.buttonClick.value="save";
                document.workOrder.action='/throttle/scheduleJobCard.do';
                document.workOrder.submit();
            }
        }else{
        alert("Please Select any One And then Proceed");
    }
    //}
}

function printPage(){
    document.workOrder.action='/throttle/printJobCard.do';
            document.workOrder.submit();
    }
function generateWO()
{
    var wo=0;
    var index = document.getElementsByName("selectedindex");

    var j=1;
    for(var i=0;(i<index.length && index.length!=0);i++){
        if(index[i].checked==true){
            var secId =document.getElementsByName("secId");
            if(secId[i].value=='1016' || secId[i].value == '1040'){
                wo++;
            }
            j++;
        }
    }
    if(wo!=0){
        document.workOrder.buttonClick.value="generateWO";
        document.workOrder.action='/throttle/scheduleJobCard.do';
        document.workOrder.submit();
    }else{
    alert("select Any One Body related problem");
}
}


function selectedItemValidation(){

    var index = document.getElementsByName("selectedindex");

    var chec=0;
    var mess = "SubmitForm";
    for(var i=0;(i<index.length && index.length!=0);i++){
        var secId =document.workOrder.secId[i];
        var probId =document.workOrder.probId[i];
        var symptoms =document.workOrder.symptoms[i];
        var severity =document.workOrder.severity[i];
        if(index[i].checked){
            chec++;

            if(isSelect(secId,'section')){
                return 'notSubmit';
            }else if(isSelect(probId,"Fault")){
            return 'notSubmit';
        }else if(textValidation(symptoms,"symptoms")){
        return 'notSubmit';
    }else if(isSelect(severity,"severity")){
    return 'notSubmit';
}
}
j++;
}
if(chec == 0){
    //alert("Please Select Any One And Then Proceed");
    //km[0].focus();
    return 'notSubmit';
}
return 'SubmitForm';
}

function getDetails(val,con)
{
    var x=0;

    if(con==1){



        document.getElementById("tech"+val).disabled=true;
        document.getElementById("serviceStatus"+val).disabled=true;
        document.getElementById("selectedServices"+val).checked=0;

        var serviceId=document.getElementById("serviceId"+val).value;
        var serviceName=document.getElementById("serviceName"+val).value;
        var vehicleId=document.workOrder.vehicleId.value;
        window.open('/throttle/serviceDone.do?serviceId='+serviceId+"&serviceName="+serviceName+"&vehicleId="+vehicleId, 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');


    }else
    {



        document.getElementById("servicesDone"+val).checked=0;
        document.getElementById("tech"+val).disabled=false;
        document.getElementById("serviceStatus"+val).disabled=false;
        document.getElementById("selectedServices"+val).checked=true;


    }

}

function show(status,ind)
{
    if(status==2){
        document.getElementById("shows1").style.visibility="visible";
        document.getElementById("shows2").style.visibility="visible";
        document.getElementById("shows3").style.visibility="visible";
        document.getElementById("shows4").style.visibility="visible";
        document.getElementById("show"+ind).style.visibility="visible";
        document.getElementById("showRemark"+ind).style.visibility="visible";

    }else
    {
        document.getElementById("shows1").style.visibility="hidden";
        document.getElementById("shows2").style.visibility="hidden";
        document.getElementById("show"+ind).style.visibility="hidden";
        document.getElementById("showRemark"+ind).style.visibility="hidden";

    }
    setSelectbox(ind);
}


function setSchedule(i)
{
    var selected=document.getElementsByName("selectedServices") ;
    selected[i].checked = 1;
}
function setSelectbox(i)
{


    var selected=document.getElementsByName("selectedindex") ;
    var selectedDate=document.getElementsByName("scheduledDate") ;
    var selectedStatus=document.getElementsByName("status") ;

    selected[i].checked = true;
    if(selectedDate[i].value != ''){
        selectedStatus[i].value='P';
    }
}

function setSelectbox1(i)
{
    var selected=document.getElementsByName("nonGraceSelectInd") ;
    selected[i].checked = 1;
}
    </script>

        </div>


        </div>
        </div>


<%@ include file="/content/common/NewDesign/settings.jsp" %>
