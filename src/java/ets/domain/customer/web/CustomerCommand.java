package ets.domain.customer.web;

/**
 *
 * @author vidya
 */
public class CustomerCommand {

    private String erpId = "";
    private String customerTypeId = "";
    private String secondaryBillingTypeId;
    private String secondaryBillingTypeName;
    private String customerGroup;
    private String customerGroupId;
    private String groupName;
    private String creditLimit;
    private String name;
    private String empName;
    private String creditDays;
    private String customerName;
    private String customerId;

    private String paymentType;
    private int custId;
    private String custName;
    private String custContactPerson;
    private String custAddress;
    private String custCity;
    private String custState;
    private String custPhone;
    private String custMobile;
    private String custEmail;
    private String custStatus;
    private String[] custIdList;
    private String[] custNameList;
    private String[] custTypeList;
    private String[] custContactPersonList;
    private String[] custAddressList;
    private String[] custCityList;
    private String[] custStateList;
    private String[] custPhoneList;
    private String[] custMobileList;
    private String[] custEmailList;
    private String[] custStatusList;
    private String[] selectedIndex = null;
    private String contManufacturerName;
    private String contModelName;
    private String contPercentAgeUpSpares;
    private String contPercentAgeUpLabour;
    private String contStatus;
    private String contFromDate;
    private String contToDate;
    private String customerType;

    private String BunkName;
    private String fuelType;
    private String currRate;
    private String location;
    private String state;
    private String activeStatus;
    private String remarks;
    private String tancem;
    private double currentrate;
    private String custCode = "";
    private String accountManagerId = "";
    private String billingTypeId = "";
    private String organizationId = "";

//    12/05/16
    private String custAddresstwo = "";
    private String custAddressthree = "";
    
    //gst
    private String gstNo = "";
    private String stateId = "";

    public String getCustAddresstwo() {
        return custAddresstwo;
    }

    public void setCustAddresstwo(String custAddresstwo) {
        this.custAddresstwo = custAddresstwo;
    }

    public String getCustAddressthree() {
        return custAddressthree;
    }

    public void setCustAddressthree(String custAddressthree) {
        this.custAddressthree = custAddressthree;
    }

    public String getBunkName() {
        return BunkName;
    }

    public void setBunkName(String BunkName) {
        this.BunkName = BunkName;
    }

    public String getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(String activeStatus) {
        this.activeStatus = activeStatus;
    }

    public String getCurrRate() {
        return currRate;
    }

    public void setCurrRate(String currRate) {
        this.currRate = currRate;
    }

    public double getCurrentrate() {
        return currentrate;
    }

    public void setCurrentrate(double currentrate) {
        this.currentrate = currentrate;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCustAddress() {
        return custAddress;
    }

    public void setCustAddress(String custAddress) {
        this.custAddress = custAddress;
    }

    public String getCustCity() {
        return custCity;
    }

    public void setCustCity(String custCity) {
        this.custCity = custCity;
    }

    public String getCustContactPerson() {
        return custContactPerson;
    }

    public void setCustContactPerson(String custContactPerson) {
        this.custContactPerson = custContactPerson;
    }

    public String getCustEmail() {
        return custEmail;
    }

    public void setCustEmail(String custEmail) {
        this.custEmail = custEmail;
    }

    public int getCustId() {
        return custId;
    }

    public void setCustId(int custId) {
        this.custId = custId;
    }

    public String getCustMobile() {
        return custMobile;
    }

    public void setCustMobile(String custMobile) {
        this.custMobile = custMobile;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCustPhone() {
        return custPhone;
    }

    public void setCustPhone(String custPhone) {
        this.custPhone = custPhone;
    }

    public String getCustState() {
        return custState;
    }

    public void setCustState(String custState) {
        this.custState = custState;
    }

    public String getCustStatus() {
        return custStatus;
    }

    public void setCustStatus(String custStatus) {
        this.custStatus = custStatus;
    }

    public String[] getCustAddressList() {
        return custAddressList;
    }

    public void setCustAddressList(String[] custAddressList) {
        this.custAddressList = custAddressList;
    }

    public String[] getCustContactPersonList() {
        return custContactPersonList;
    }

    public void setCustContactPersonList(String[] custContactPersonList) {
        this.custContactPersonList = custContactPersonList;
    }

    public String[] getCustEmailList() {
        return custEmailList;
    }

    public void setCustEmailList(String[] custEmailList) {
        this.custEmailList = custEmailList;
    }

    public String[] getCustIdList() {
        return custIdList;
    }

    public void setCustIdList(String[] custIdList) {
        this.custIdList = custIdList;
    }

    public String[] getCustCityList() {
        return custCityList;
    }

    public void setCustCityList(String[] custCityList) {
        this.custCityList = custCityList;
    }

    public String[] getCustMobileList() {
        return custMobileList;
    }

    public void setCustMobileList(String[] custMobileList) {
        this.custMobileList = custMobileList;
    }

    public String[] getCustNameList() {
        return custNameList;
    }

    public void setCustNameList(String[] custNameList) {
        this.custNameList = custNameList;
    }

    public String[] getCustPhoneList() {
        return custPhoneList;
    }

    public void setCustPhoneList(String[] custPhoneList) {
        this.custPhoneList = custPhoneList;
    }

    public String[] getCustStateList() {
        return custStateList;
    }

    public void setCustStateList(String[] custStateList) {
        this.custStateList = custStateList;
    }

    public String[] getCustStatusList() {
        return custStatusList;
    }

    public void setCustStatusList(String[] custStatusList) {
        this.custStatusList = custStatusList;
    }

    public String[] getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(String[] selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public String getContFromDate() {
        return contFromDate;
    }

    public void setContFromDate(String contFromDate) {
        this.contFromDate = contFromDate;
    }

    public String getContManufacturerName() {
        return contManufacturerName;
    }

    public void setContManufacturerName(String contManufacturerName) {
        this.contManufacturerName = contManufacturerName;
    }

    public String getContModelName() {
        return contModelName;
    }

    public void setContModelName(String contModelName) {
        this.contModelName = contModelName;
    }

    public String getContPercentAgeUpLabour() {
        return contPercentAgeUpLabour;
    }

    public void setContPercentAgeUpLabour(String contPercentAgeUpLabour) {
        this.contPercentAgeUpLabour = contPercentAgeUpLabour;
    }

    public String getContPercentAgeUpSpares() {
        return contPercentAgeUpSpares;
    }

    public void setContPercentAgeUpSpares(String contPercentAgeUpSpares) {
        this.contPercentAgeUpSpares = contPercentAgeUpSpares;
    }

    public String getContStatus() {
        return contStatus;
    }

    public void setContStatus(String contStatus) {
        this.contStatus = contStatus;
    }

    public String getContToDate() {
        return contToDate;
    }

    public void setContToDate(String contToDate) {
        this.contToDate = contToDate;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String[] getCustTypeList() {
        return custTypeList;
    }

    public void setCustTypeList(String[] custTypeList) {
        this.custTypeList = custTypeList;
    }

    public String getTancem() {
        return tancem;
    }

    public void setTancem(String tancem) {
        this.tancem = tancem;
    }

    public String getCustCode() {
        return custCode;
    }

    public void setCustCode(String custCode) {
        this.custCode = custCode;
    }

    public String getAccountManagerId() {
        return accountManagerId;
    }

    public void setAccountManagerId(String accountManagerId) {
        this.accountManagerId = accountManagerId;
    }

    public String getBillingTypeId() {
        return billingTypeId;
    }

    public void setBillingTypeId(String billingTypeId) {
        this.billingTypeId = billingTypeId;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getSecondaryBillingTypeId() {
        return secondaryBillingTypeId;
    }

    public void setSecondaryBillingTypeId(String secondaryBillingTypeId) {
        this.secondaryBillingTypeId = secondaryBillingTypeId;
    }

    public String getSecondaryBillingTypeName() {
        return secondaryBillingTypeName;
    }

    public void setSecondaryBillingTypeName(String secondaryBillingTypeName) {
        this.secondaryBillingTypeName = secondaryBillingTypeName;
    }

    public String getCreditDays() {
        return creditDays;
    }

    public void setCreditDays(String creditDays) {
        this.creditDays = creditDays;
    }

    public String getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(String creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getCustomerGroup() {
        return customerGroup;
    }

    public void setCustomerGroup(String customerGroup) {
        this.customerGroup = customerGroup;
    }

    public String getCustomerGroupId() {
        return customerGroupId;
    }

    public void setCustomerGroupId(String customerGroupId) {
        this.customerGroupId = customerGroupId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(String customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    public String getErpId() {
        return erpId;
    }

    public void setErpId(String erpId) {
        this.erpId = erpId;
    }

    public String getGstNo() {
        return gstNo;
    }

    public void setGstNo(String gstNo) {
        this.gstNo = gstNo;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }
    

}
