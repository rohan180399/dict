


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>          
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script>
        <title>Modify GRN</title>
    </head>
    <body>
        
        
        <script>
            function submitPage(val){ 

                    document.modifyGrn.action="/throttle/receiveInvoice.do";
                    document.modifyGrn.submit();                
                }

                  
        </script>
        
        <form name="modifyGrn" method="post" >                    
            <%@ include file="/content/common/path.jsp" %>            
            
            <br>
            <!-- message table -->           
            <%@ include file="/content/common/message.jsp" %>    
            <br>
<% int index = 0; %>                
                <c:if test = "${supplyList != null}" >   
                
          <c:forEach items="${supplyList}" var="supply">  
          <% if(index==0){ %> 
         <table align="center" width="700" border="0" cellspacing="0" cellpadding="0">
            <tr>
            <td>                
                
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="400" id="bg">
                <tr>
                    <td colspan="9" height="80" align="center" class="contenthead" >GRN Info</td>
                </tr>

         
                
                
                <tr>
                    <td class="text1" height="30">GRN No</td>
                    <input type="hidden" name="supplyId" value=<c:out value="${supply.supplyId}"/> >    
                    <td class="text1" height="30"> <c:out value="${supply.supplyId}"/> </td>
                </tr>  
                <tr>
                    <td class="text2" height="30">Vendor Name</td>
                    <td class="text2" height="30"> <c:out value="${supply.vendorName}"/> </td>
                </tr>           
                <tr>
                    <td class="text1" height="30">DC No</td>
                    <td class="text1" height="30">  <input type="text" readonly="" name="dcNumber" class="form-control"  value=<c:out value="${supply.dcNumber}"/> >     </td>
                </tr>           
                <tr>
                    <td class="text2" height="30">Invoice No</td>
                    <td class="text2" height="30"> <input type="text" readonly="" name="inVoiceNumber" class="form-control"  value=<c:out value="${supply.inVoiceNumber}"/> >        </td>
                </tr>           
                <tr>
                    <td class="text1" height="30">Invoice Date</td>
                    <td class="text1" height="30">
                    <input type="text" name="inVoiceDate" class="form-control" value=<c:out value="${supply.inVoiceDate}"/> >                    
                    </td>
                </tr>
                <tr>
                    <td class="text2" height="30">Bill Amount</td>
                    <td class="text2" height="30"> <input type="text" readonly name="inVoiceAmount" class="form-control" value=<c:out value="${supply.inVoiceAmount}"/>  ></td>
                </tr> 
                <tr>
                    <td class="text1" height="30">Remarks</td>
                    <td class="text1" height="30"><c:out value="${supply.remarks}"/></td>
                </tr>
                <c:set var="transactionType" value="${supply.inVoiceNumber}" />
            </table>
        </td>    
	<td>

		<table width="200" align="right" border="0" cellpadding="0" cellspacing="0" class="border">
		<tr> 
		<td colspan="2" height="30" class="blue" align="center" style="border:1px;border-color:#FFFFFF;border-bottom-style:dashed;">Total</td>
		</tr>

		<tr> 
		<td width="121" height="30"  class="bluetext">SAR.</td>
                 <td width="86" height="30" class="bluetext"><div id="total" ><c:out value="${supply.inVoiceAmount}"/></div>  </td>
                </tr>
             
                
                </table>
	</td>
 </tr>
  
</table>       
<% index++; } %>                
                    </c:forEach> 
            <br>
            <br>    
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="800" id="bg">
                <%index = 0;
            String classText = "";
            int oddEven = 0;
                %>
                <c:if test = "${supplyList != null}" >
                <tr>
                    <td colspan="8" align="center" class="contenthead" height="30">Modify GRN </td>
                </tr>  
                        <tr>
                            <td class="text2" height="30"><b>Mfr Code</b></td>
                            <td class="text2" height="30"><b>Papl Code</b></td>                       
                            <td class="text2" height="30"><b>Item Name</b></td>
                            <td class="text2" height="30"><b>Uom</b></td>
                            <td class="text2" height="30"><b>Discount(%)</b></td>
                            <td class="text2" height="30"><b>Tax(%)</b></td>
                            <td class="text2" height="30"><b>Unit Price</b></td>
                            <td class="text2" height="30"><b>MRP</b></td>
                            <td class="text2" height="30"><b>Accepted Quantity</b> </td>                                                                                                    
                            <td class="text2" height="30"><b>Amount</b> </td>                                                                                                  
                        </tr>                
                    <c:forEach items="${supplyList}" var="supply"> 
                        <%

            oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr>
                            <input type="hidden" name="itemIds" value=<c:out value="${supply.itemId}"/> > 
                            <td class="<%=classText %>" height="30"> <c:out value="${supply.mfrCode}"/></td>
                            <td class="<%=classText %>" height="30"> <c:out value="${supply.paplCode}"/></td>                       
                            <td class="<%=classText %>" height="30"> <c:out value="${supply.itemName}"/></td>
                            <td class="<%=classText %>" height="30"> <c:out value="${supply.uomName}"/></td>
                            <td class="<%=classText %>" height="30"> <input type="text" readonly  name="discount" size="5" class="form-control" value=<c:out value="${supply.discount}"/> > </td>
                            <td class="<%=classText %>" height="30"> <input type="text" readonly name="tax" size="5" class="form-control" value=<c:out value="${supply.tax}"/> > </td>
                            <td class="<%=classText %>" height="30"> <input type="text"  readonly name="unitPrice" onFocusOut="calculateMRP('<%= index %>');"  size="5" class="form-control" value=<c:out value="${supply.unitPrice}"/> > </td>
                            <td class="<%=classText %>" height="30"> <input type="text"  readonly  name="mrps" size="5" class="form-control" value=<c:out value="${supply.mrp}"/> > </td>                                                                                                                              
                            <td class="<%=classText %>" height="30"> <input type="text" readonly  name="acceptedQtys" size="5" onChange="setAmount('<%= index %>')" class="form-control" value=<c:out value="${supply.acceptedQty}"/> > </td>                                                                                                                              
                            <td class="<%=classText %>" height="30"> <input type="text"  readonly  name="itemAmounts" class="form-control" value=<c:out value="${supply.itemAmount}"/> > </td>
                            <c:set var="inventoryStat" value="${supply.inventoryStatus}" />                            
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach>
                </c:if>                  
            </table> 
<br>           
<br>           
                <input type="hidden" name="status" value="" >             

             <center>   

          <input type="button" class="button" name="Back" value="Back" onClick="submitPage(this.name);" > &nbsp;           

            </center>
<br>            
<br>            
                </c:if>                  
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
