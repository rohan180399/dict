<%-- 
    Document   : uploadPodResponsive
    Created on : Aug 6, 2015, 5:47:25 PM
    Author     : entitle
--%>



<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>
     <script type="text/javascript" src="/throttle/js/jspdf.debug.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>

    <script type="text/javascript">
        function mailTriggerPage(tripId){
            window.open('/throttle/viewTripDetailsMail.do?tripId='+tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
        function viewCurrentLocation(vehicleRegNo) {
            var url = ' http://203.115.101.126/api/vehicleonmap/map.php?uname=interem&group_id=5087&height=600&width=700&vehicle='+vehicleRegNo;
            //alert(url);
            window.open(url, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }

        function viewTripDetails(tripId,vehicleId, tripType) {
            window.open('/throttle/viewTripSheetDetails.do?tripId='+tripId+'&tripType='+tripType, '&vehicleId='+vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
        function viewVehicleDetails(vehicleId) {
            window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });

    </script>
    <script type="text/javascript">
        function setValues(){
            if('<%=request.getAttribute("vehicleTypeId")%>' != 'null' ){
                document.getElementById('vehicleTypeId').value= '<%=request.getAttribute("vehicleTypeId")%>';
            }
            if('<%=request.getAttribute("fromDate")%>' != 'null' ){
                document.getElementById('fromDate').value= '<%=request.getAttribute("fromDate")%>';
            }
            if('<%=request.getAttribute("toDate")%>' != 'null' ){
                document.getElementById('toDate').value= '<%=request.getAttribute("toDate")%>';
            }
            if('<%=request.getAttribute("fleetCenterId")%>' != 'null'){
                document.getElementById('fleetCenterId').value='<%=request.getAttribute("fleetCenterId")%>';
            }
            if('<%=request.getAttribute("cityFromId")%>' != 'null'){
                document.getElementById('cityFromId').value='<%=request.getAttribute("cityFromId")%>';
            }
            if('<%=request.getAttribute("cityFrom")%>' != 'null' ){
                document.getElementById('cityFrom').value='<%=request.getAttribute("cityFrom")%>';
            }
            if('<%=request.getAttribute("vehicleId")%>' != 'null'){
                document.getElementById('vehicleId').value='<%=request.getAttribute("vehicleId")%>';
            }
            if('<%=request.getAttribute("zoneId")%>' != 'null' ){
                document.getElementById('zoneId').value= '<%=request.getAttribute("zoneId")%>';
            }

            if('<%=request.getAttribute("tripSheetId")%>' != 'null' ){
                document.getElementById('tripSheetId').value= '<%=request.getAttribute("tripSheetId")%>';
            }
            if('<%=request.getAttribute("customerId")%>' != 'null' ){
                document.getElementById('customerId').value= '<%=request.getAttribute("customerId")%>';
            }
            if('<%=request.getAttribute("tripStatusId")%>' != 'null' ){
                document.getElementById('tripStatusId').value= '<%=request.getAttribute("tripStatusId")%>';
            }
            if('<%=request.getAttribute("podStatus")%>' != 'null' ){
                document.getElementById('podStatus').value= '<%=request.getAttribute("podStatus")%>';
            }
            if('<%=request.getAttribute("statusId")%>' != 'null' ){
                document.getElementById('stsId').value= '<%=request.getAttribute("statusId")%>';
            }
        }

        function submitPage() {
            document.tripSheet.action = '/throttle/viewTripSheets.do?statusId='+document.getElementById('stsId').value;
            document.tripSheet.submit();
        }

        function submitPage1() {
            document.tripSheet.action = '/throttle/viewTripSheets.do?param=exportExcel';
            document.tripSheet.submit();
        }

        

    </script>

    <script type="text/javascript">
        var httpRequest;
        function getLocation() {
            var zoneid = document.getElementById("zoneId").value;
            if (zoneid != '') {

                // Use the .autocomplete() method to compile the list based on input from user
                $('#cityFrom').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getLocationName.do",
                            dataType: "json",
                            data: {
                                cityId: request.term,
                                zoneId: document.getElementById('zoneId').value
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                response(items);
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var tmp = value.split('-');
                        $('#cityFromId').val(tmp[0]);
                        $('#cityFrom').val(tmp[1]);
                        return false;
                    }
                }).data("autocomplete")._renderItem = function(ul, item) {
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
                };

            }
        }
   
    </script>

</head>
<body onload="sorter.size(20);setValues();">
    <form name="tripSheet" method="post">
       <br>
        <a href="content/common/menuResponsive.jsp"><img src="/throttle/images/menu.jpg"></a>
        <br>
        <br>
        <br>
        <c:if test="${tripDetails != null}">
            <table align="center" border="0" id="table" class="sortable" style="width:-1700px;" >
                 <!--<table align="center" border="0" id="table" class="sortable" style="width:300;" >-->
                <thead>
                    <tr height="50">
                        <th><h3>Sno</h3></th>
                        <th><h3>Vehicle No </h3></th>
                        <th><h3>Trip Code</h3></th>
                        <th><h3>Customer Name </h3></th>
                        <th><h3>Route </h3></th>
                        <th><h3>Driver Name with Contact </h3></th>
                        <c:if test="${tripType == 1}">
                            <th><h3>POD</h3></th>
                      </c:if>
                    </tr>
                </thead>
                <tbody>
                    <% int index = 1;%>
                    <c:forEach items="${tripDetails}" var="tripDetails">
                        <%
                                    String className = "text1";
                                    if ((index % 2) == 0) {
                                        className = "text1";
                                    } else {
                                        className = "text2";
                                    }
                        %>
                        
                        <tr height="30">
                            <td class="<%=className%>" width="10" align="left">
                                <%=index%>
                            </td>
                            <td class="<%=className%>" width="120">
                                <a href="" onclick="viewVehicleDetails('<c:out value="${tripDetails.vehicleId}"/>')"><c:out value="${tripDetails.vehicleNo}"/></a><br><font color="red"><c:out value="${tripDetails.oldVehicleNo}"/></font></td>
                             
                            <td class="<%=className%>" width="120" >
                            <c:if test="${tripDetails.oldVehicleNo != null}">
                                <a href="" onclick="viewTripDetails('<c:out value="${tripDetails.tripSheetId}"/>','<c:out value="${tripDetails.vehicleId}"/>','<c:out value="${tripType}"/>',1);"><c:out value="${tripDetails.tripCode}"/></a>
                            </c:if>
                            <c:if test="${tripDetails.oldVehicleNo == null}">
                                <a href="" onclick="viewTripDetails('<c:out value="${tripDetails.tripSheetId}"/>','<c:out value="${tripDetails.vehicleId}"/>','<c:out value="${tripType}"/>',0);"><c:out value="${tripDetails.tripCode}"/></a>
                            </c:if>
                            </td>
                            <td  align="center" class="<%=className%>" width="150">
                                <c:if test="${tripDetails.customerName != 'Empty Trip'}">
                                    <c:out value="${tripDetails.customerName}"/>
                                </c:if>
                                <c:if test="${tripDetails.customerName == 'Empty Trip'}">
                                    <img src="images/emptytrip.png" alt="EmptyTrip"   title="Empty Trip"/>
                                </c:if></td>
                            <td class="<%=className%>" >                                
                                <c:if test="${tripDetails.routeNameStatus > '2'}">
                                    <img src="images/Black_star1.png" alt="MultiPoint"  title=" MultiPoint"/><c:out value="${tripDetails.routeInfo}"/>
                                </c:if>
                                <c:if test="${tripDetails.routeNameStatus <= '2'}">
                                    <c:out value="${tripDetails.routeInfo}"/>
                                </c:if>
                            </td>
                            <td class="<%=className%>" ><c:out value="${tripDetails.driverName}"/><br/><c:out value="${tripDetails.mobileNo}"/><br><font color="red"><c:out value="${tripDetails.oldDriverName}"/></font></td>
                             <c:if test="${tripType == 1}">
                                <td class="<%=className%>" align="center">
                                    <c:if test="${tripDetails.statusId == 10 || tripDetails.statusId == 12 || tripDetails.statusId == 13  || tripDetails.statusId == 14  || tripDetails.statusId == 16}">
                                        <c:if test="${tripDetails.customerName != 'Empty Trip'}">
                                           <c:if test="${tripDetails.podCount == '0'}">
                                            <a href="viewTripPod.do?respStatus=Y&tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">  <img src="images/Podinactive.png" alt="Y"   title="click to upload pod"/></a>
                                            </c:if>
                                            <c:if test="${tripDetails.podCount != '0'}">
                                            <a href="viewTripPod.do?respStatus=Y&tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">  <img src="images/Podactive.png" alt="Y"   title="click to upload pod"/></a>
                                            </c:if>
                                            </c:if>
                                        </c:if>
                                </td>
                          </c:if>
                        </tr>

                        <%index++;%>
                    </c:forEach>
                </tbody>
            </table>
        </c:if>
          <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5" >5</option>
                    <option value="10">10</option>
                    <option value="20" selected="selected">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>

