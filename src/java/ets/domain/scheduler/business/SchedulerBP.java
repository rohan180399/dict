/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.scheduler.business;
//import com.Service;
//import com.ServiceSoap;
import com.lowagie.text.Document;

import com.lowagie.text.Chunk;

import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;


import com.lowagie.text.pdf.*;
   
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import com.ibatis.sqlmap.client.SqlMapClient;
//import com.itextpdf.text.pdf.security.BouncyCastleDigest;
//import com.itextpdf.text.pdf.security.DigestAlgorithms;
//import com.itextpdf.text.pdf.security.ExternalDigest;
//import com.itextpdf.text.pdf.security.PrivateKeySignature;
//import com.itextpdf.text.pdf.security.MakeSignature;
//import com.itextpdf.text.pdf.security.MakeSignature.CryptoStandard;
import com.itextpdf.text.pdf.PdfStamper;
import com.lowagie.text.Paragraph;
import com.itextpdf.text.pdf.PdfReader;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.billing.business.BillingTO;
import ets.domain.report.business.NumberWords;
//import ets.arch.util.CreatePieChartExamplesXLSX;
import ets.domain.report.business.ReportTO;
import ets.domain.scheduler.data.SchedulerDAO;
import ets.domain.billing.data.BillingDAO;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import ets.domain.thread.web.SendEmail;
import ets.domain.trip.business.TripBP;
import ets.domain.trip.business.TripTO;
import ets.domain.trip.data.TripDAO;
import ets.domain.util.ThrottleConstants;
import static ets.domain.util.ThrottleConstants.tripPlannedBccMailId;
import static ets.domain.util.ThrottleConstants.tripPlannedCCMailId;
import static ets.domain.util.ThrottleConstants.vehicleutilisedBccMailId;
import static ets.domain.util.ThrottleConstants.vehicleutilisedCCMailId;
import static ets.domain.util.ThrottleConstants.vehicleutilisedToMailId;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import net.sf.json.JSONException;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.Security;
import java.util.Enumeration;

import org.bouncycastle.jce.provider.BouncyCastleProvider;


//import com.ets.to.batch.alerts.main.CreatePieChartExampleXLSX;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;

import java.util.Date;
import java.text.SimpleDateFormat;
import javax.mail.Multipart;
import javax.mail.internet.MimeMultipart;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 *
 * @author sabreesh
 */
public class SchedulerBP {

    private SchedulerDAO schedulerDAO;
    private CellStyle style1;
    private ArrayList dvmDetailsDays;
    TripDAO tripDAO;
    BillingDAO billingDAO;
   

    
    
    public BillingDAO getBillingDAO() {
        return billingDAO;
    }

    public void setBillingDAO(BillingDAO billingDAO) {
        this.billingDAO = billingDAO;
    }

    public TripDAO getTripDAO() {
        return tripDAO;
    }

    public void setTripDAO(TripDAO tripDAO) {
        this.tripDAO = tripDAO;
    }
//    private int insertstatus;

    public SchedulerDAO getSchedulerDAO() {
        return schedulerDAO;
    }

    public void setSchedulerDAO(SchedulerDAO schedulerDAO) {
        this.schedulerDAO = schedulerDAO;
    }

   
    
    

    public int processWayBillToCSV(SchedulerTO schedulerTO, String fromDate, String toDate) throws JSONException, FPRuntimeException, FPBusinessException {
        int status = 0;
        ArrayList wayBillsList = new ArrayList();
        List wayBillBarCode = new ArrayList();
        String COMMA_DELIMITER = ",";
        String NEW_LINE_SEPARATOR = "\n";
        //CSV file header
//	String FILE_HEADER = "BARCODE,BOOKING,DATE,DELIVERY,PACK,WayBill_No,WayBill_Id,Article_Serial_No";
        String FILE_HEADER = "BARCODE,BOOKING,DATE,DELIVERY,PACK,Private_Marking,WayBill_No";
        SqlMapClient session = schedulerDAO.getSqlMapClient();
        FileWriter fileWriter = null;
        try {
            session.startTransaction();
            wayBillsList = schedulerDAO.processWayBillList(schedulerTO, fromDate, toDate, session);
            SchedulerTO schTO = new SchedulerTO();
            Iterator itr1 = wayBillsList.iterator();
            String fileName = "";
            while (itr1.hasNext()) {
                schTO = (SchedulerTO) itr1.next();
                fileName = schTO.getWayBillNo().replaceAll("/", "");
                fileName = ThrottleConstants.csvFileDirectory + "PRINT\\" + schTO.getOrigin() + "\\" + fileName + ".csv";
                fileWriter = new FileWriter(fileName);
                fileWriter.append(FILE_HEADER.toString());
                //Add a new line separator after the header
                fileWriter.append(NEW_LINE_SEPARATOR);
                wayBillBarCode = schedulerDAO.processWayBillToCSV(schTO, fromDate, toDate, session);
                SchedulerTO sTO = new SchedulerTO();
                Iterator itr = wayBillBarCode.iterator();
                int count = 0;
                while (itr.hasNext()) {
                    sTO = (SchedulerTO) itr.next();
                    //Write the CSV file header
                    //Write a new student object list to the CSV file
                    String wayBillNo = sTO.getWayBillNo();
                    String wayBillNoTemp[] = wayBillNo.split("/");
                    String wayBillRunningNo = wayBillNoTemp[1].substring(wayBillNoTemp[1].length() - 5);
                    String barCode = sTO.getBarCodeNo();
                    String branchCode = sTO.getBranchCode();
                    if (branchCode.length() > 2) {
                        branchCode = sTO.getBranchCode().substring(sTO.getBranchCode().length() - 3);
                    }
                    sTO.setBarCodeNo(wayBillNoTemp[0] + "/" + branchCode + "/" + wayBillRunningNo + "-" + sTO.getBarCodeNo());
                    fileWriter.append(sTO.getBarCodeNo());
                    fileWriter.append(COMMA_DELIMITER);
                    fileWriter.append(sTO.getOrigin());
                    fileWriter.append(COMMA_DELIMITER);
                    fileWriter.append(sTO.getWayBillDate());
                    fileWriter.append(COMMA_DELIMITER);
                    fileWriter.append(sTO.getDestination());
                    fileWriter.append(COMMA_DELIMITER);
                    fileWriter.append(sTO.getNoOfArticles());
                    fileWriter.append(COMMA_DELIMITER);
                    fileWriter.append(sTO.getPrivateMarking());
                    fileWriter.append(COMMA_DELIMITER);
                    fileWriter.append(sTO.getWayBillNo());
//                fileWriter.append(COMMA_DELIMITER);
//                fileWriter.append(barCode);
                    fileWriter.append(NEW_LINE_SEPARATOR);
                }
                System.out.println("CSV file was created successfully !!!");
                fileWriter.flush();
                fileWriter.close();
                status = schedulerDAO.updateWayBillCSVStatus(schTO, session);
            }

            session.commitTransaction();
        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                System.out.println("Error while flushing/closing fileWriter !!!");
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return status;
    }

    public int processOgplBillToCSV(SchedulerTO schedulerTO, String fromDate, String toDate) throws JSONException, FPRuntimeException, FPBusinessException {
        int status = 0;
        ArrayList wayBillsList = new ArrayList();
        List ogplBarCode = new ArrayList();
        String COMMA_DELIMITER = ",";
        String NEW_LINE_SEPARATOR = "\n";
        //CSV file header
//	String FILE_HEADER = "TRIP_NO,MANIFEST_NO,SERIAL_NO,DESTINATION,WayBill_No,WayBill_Id,Article_Serial_No";
        String FILE_HEADER = "TRIP_NO,MANIFEST_NO,SERIAL_NO,DESTINATION,WayBill_No";
        SqlMapClient session = schedulerDAO.getSqlMapClient();
        FileWriter fileWriter = null;
        try {
            session.startTransaction();
            wayBillsList = schedulerDAO.processOgplList(schedulerTO, fromDate, toDate, session);
            SchedulerTO schTO = new SchedulerTO();
            Iterator itr1 = wayBillsList.iterator();
            ArrayList wayBillBarCode = new ArrayList();
            SchedulerTO csvTO = new SchedulerTO();

            String fileName = "";
            while (itr1.hasNext()) {
                schTO = (SchedulerTO) itr1.next();
                fileName = schTO.getTripCode().replaceAll("-", "_");
                fileName = fileName.replaceAll("/", "") + "-" + schTO.getOgplNo();
                fileName = ThrottleConstants.csvFileDirectory + "MANIFEST\\" + schTO.getOrigin() + "\\" + fileName + ".csv";
                fileWriter = new FileWriter(fileName);
                fileWriter.append(FILE_HEADER.toString());
                //Add a new line separator after the header
                fileWriter.append(NEW_LINE_SEPARATOR);
                ogplBarCode = schedulerDAO.processOGPLToCSV(schTO, fromDate, toDate, session);
                SchedulerTO sTO = new SchedulerTO();
                Iterator itr = ogplBarCode.iterator();
                int count = 0;
                while (itr.hasNext()) {
                    csvTO = new SchedulerTO();
                    sTO = (SchedulerTO) itr.next();
                    //Write the CSV file header
                    //Write a new student object list to the CSV file
//                schTO.setTripCode(schTO.getTripCode().replaceAll("-","_"));
                    fileWriter.append(schTO.getTripCode());
                    fileWriter.append(COMMA_DELIMITER);
                    fileWriter.append(schTO.getOgplNo());
                    fileWriter.append(COMMA_DELIMITER);
                    String wayBillNo = sTO.getWayBillNo();
                    String wayBillNoTemp[] = wayBillNo.split("/");
                    String wayBillRunningNo = wayBillNoTemp[1].substring(wayBillNoTemp[1].length() - 5);
                    String branchCode = sTO.getBranchCode();
                    String barCode = sTO.getBarCodeNo();
                    if (branchCode.length() > 2) {
                        branchCode = sTO.getBranchCode().substring(sTO.getBranchCode().length() - 3);
                    }
                    sTO.setBarCodeNo(wayBillNoTemp[0] + "/" + branchCode + "/" + wayBillRunningNo + "-" + sTO.getBarCodeNo());
                    fileWriter.append(sTO.getBarCodeNo());
                    csvTO.setBarCodeNo(sTO.getBarCodeNo());
                    csvTO.setWayBillId(sTO.getWayBillId());
                    csvTO.setWayBillNo(sTO.getWayBillNo());
                    fileWriter.append(COMMA_DELIMITER);
                    fileWriter.append(schTO.getDestination());
                    fileWriter.append(COMMA_DELIMITER);
                    fileWriter.append(sTO.getWayBillNo());
//                fileWriter.append(COMMA_DELIMITER);
//                fileWriter.append(sTO.getWayBillId());
//                fileWriter.append(COMMA_DELIMITER);
//                fileWriter.append(barCode);
                    fileWriter.append(NEW_LINE_SEPARATOR);
                    wayBillBarCode.add(csvTO);
                }
                schTO.setBarCodeList(wayBillBarCode);
                status = schedulerDAO.insertOgplBarCodeDetails(schTO, session);
                System.out.println("CSV file was created successfully !!!");
                fileWriter.flush();
                fileWriter.close();
                status = schedulerDAO.updateOgplCSVStatus(schTO, session);
            }

            session.commitTransaction();
        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                System.out.println("Error while flushing/closing fileWriter !!!");
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return status;
    }

    public int processOgplLoad(SchedulerTO schedulerTO, String fromDate, String toDate) throws JSONException, FPRuntimeException, FPBusinessException {
        int status = 0;
        ArrayList wayBillsList = new ArrayList();
        SqlMapClient session = schedulerDAO.getSqlMapClient();
        try {
            session.startTransaction();
            wayBillsList = schedulerDAO.processOgplLoadList(schedulerTO, fromDate, toDate, session);
            SchedulerTO schTO = new SchedulerTO();
            Iterator itr1 = wayBillsList.iterator();
            ArrayList ogplBarCodeLoaded = new ArrayList();
            SchedulerTO csvTO = new SchedulerTO();

            String fileName = "";
            while (itr1.hasNext()) {
                schTO = (SchedulerTO) itr1.next();
                fileName = schTO.getTripCode().replaceAll("-", "_");
                fileName = fileName.replaceAll("/", "") + "-" + schTO.getOgplNo();
                fileName = ThrottleConstants.csvFileDirectory + "LOADING\\" + schTO.getDestination() + "\\" + fileName + ".csv";
                System.out.println("fileName = " + fileName);
                File file = new File(fileName);

                if (file.exists()) {
                    BufferedReader br = null;
                    String line = "";
                    String csvSplitBy = ",";
//            CSVReader csvReader = new CSVReader(new FileReader(fileName));
                    br = new BufferedReader(new FileReader(fileName));
                    int count = 0;
                    while ((line = br.readLine()) != null) {
                        // use comma as separator
                        csvTO = new SchedulerTO();
                        String[] row = line.split(csvSplitBy);

                        System.out.println(row[0] + " # " + row[1] + " #  " + row[2]);
                        if (row[4].equals("1") && (count != 0)) {
                            csvTO.setTripCode(row[0]);
                            csvTO.setOgplNo(row[1]);
                            csvTO.setBarCodeNo(row[2]);
                            csvTO.setDestination(row[3]);
                            csvTO.setLoadedStatus(row[4]);
                            csvTO.setLoadedDateTime(row[5]);
                            csvTO.setLoadedUserId(row[6]);
                            ogplBarCodeLoaded.add(csvTO);
                        }

                        count++;
                    }

//            String[] row = null;
//            int count = 0;
//            while((row = csvReader.readNext()) != null) {
//                csvTO = new SchedulerTO();
//                if(count == 0){
//                    csvReader.getSkipLines();
//                }else{
//                System.out.println(row[0]+ " # " + row[1]+ " #  " + row[2]);
//                if(row[4].equals("1")){
//                csvTO.setTripCode(row[0]);
//                csvTO.setOgplNo(row[1]);
//                csvTO.setBarCodeNo(row[2]);
//                csvTO.setDestination(row[3]);
//                csvTO.setLoadedStatus(row[4]);
//                csvTO.setLoadedDateTime(row[5]);
//                csvTO.setLoadedUserId(row[6]);
//                ogplBarCodeLoaded.add(csvTO);
//                }
//                }
//                count++;
//            }
                    //...
                    br.close();
                    if (count > 0) {
                        schTO.setBarCodeList(ogplBarCodeLoaded);
                        status = schedulerDAO.updateBarCodeLoad(schTO, session);
                    }

                } else {
                    System.out.println("file not exists in to the system");
                }
            }

            session.commitTransaction();
        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                System.out.println("Error while flushing/closing fileWriter !!!");
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return status;
    }

    public int processOgplUnLoad(SchedulerTO schedulerTO, String fromDate, String toDate) throws JSONException, FPRuntimeException, FPBusinessException {
        int status = 0;
        ArrayList wayBillsList = new ArrayList();
        SqlMapClient session = schedulerDAO.getSqlMapClient();
        try {
            session.startTransaction();
            wayBillsList = schedulerDAO.processOgplUnLoadList(schedulerTO, fromDate, toDate, session);
            SchedulerTO schTO = new SchedulerTO();
            Iterator itr1 = wayBillsList.iterator();
            ArrayList ogplBarCodeLoaded = new ArrayList();
            SchedulerTO csvTO = new SchedulerTO();

            String fileName = "";
            while (itr1.hasNext()) {
                schTO = (SchedulerTO) itr1.next();
                fileName = schTO.getTripCode().replaceAll("-", "_");
                fileName = fileName.replaceAll("/", "") + "-" + schTO.getOgplNo();
                fileName = ThrottleConstants.csvFileDirectory + "UNLOADING\\" + schTO.getDestination() + "\\" + fileName + ".csv";
                File file = new File(fileName);
                System.out.println("file = " + file);
                if (file.exists()) {

                    BufferedReader br = null;
                    String line = "";
                    String csvSplitBy = ",";
//            CSVReader csvReader = new CSVReader(new FileReader(fileName));
                    br = new BufferedReader(new FileReader(fileName));
                    int count = 0;
                    while ((line = br.readLine()) != null) {
                        // use comma as separator
                        csvTO = new SchedulerTO();
                        String[] row = line.split(csvSplitBy);
                        System.out.println(row[0] + " # " + row[1] + " #  " + row[2]);
                        if (row[7].equals("1") && (count != 0)) {
                            csvTO.setTripCode(row[0]);
                            csvTO.setOgplNo(row[1]);
                            csvTO.setBarCodeNo(row[2]);
                            csvTO.setDestination(row[3]);
                            csvTO.setUnLoadedStatus(row[7]);
                            csvTO.setUnLoadedDateTime(row[8]);
                            csvTO.setUnLoadedUserId(row[9]);
                            ogplBarCodeLoaded.add(csvTO);
                        }
                        count++;
                    }
//            String[] row = null;
//            int count = 0;
//            while((row = csvReader.readNext()) != null) {
//                csvTO = new SchedulerTO();
//                if(count == 0){
//                    csvReader.getSkipLines();
//                }else{
//                System.out.println(row[0]+ " # " + row[1]+ " #  " + row[2]);
//                if(row[7].equals("1")){
//                csvTO.setTripCode(row[0]);
//                csvTO.setOgplNo(row[1]);
//                csvTO.setBarCodeNo(row[2]);
//                csvTO.setDestination(row[3]);
//                csvTO.setUnLoadedStatus(row[7]);
//                csvTO.setUnLoadedDateTime(row[8]);
//                csvTO.setUnLoadedUserId(row[9]);
//                ogplBarCodeLoaded.add(csvTO);
//                }
//                }
//                count++;
//            }
                    //...
                    br.close();
                    if (count > 0) {
                        schTO.setBarCodeList(ogplBarCodeLoaded);
                        status = schedulerDAO.updateBarCodeUnLoad(schTO, session);
                    }
                }
            }
            session.commitTransaction();
        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                System.out.println("Error while flushing/closing fileWriter !!!");
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return status;
    }

//    public int processGPSDataXMLParser(SchedulerTO schedulerTO, String fromDate, String toDate) throws JSONException, FPRuntimeException, FPBusinessException {
//        int insertStatus = 0;
//        try {
//        Service srv = new Service();
//        ServiceSoap srvs = srv.getServiceSoap();
//        ArrayList vehicleInfo = new ArrayList();
//        VehicleInfo veh = null;
////            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
////            DocumentBuilder db = dbf.newDocumentBuilder();
////            SAXParserFactory dbf = SAXParserFactory.newInstance();
////            SAXParser db = dbf.newSAXParser();
//            System.out.println("xml data:" + srvs.getInformationAll("ponpure"));
////            File inputFile = new File(srvs.getInformationAll("ponpure"));
////            UserHandler userhandler = new UserHandler();
////            db.parse(inputFile,userhandler);
////            schedulerTO.setVehicleInfo(vehicleInfo);
////            insertStatus = schedulerDAO.processGPSData(schedulerTO, fromDate, toDate);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return insertStatus;
//    }
//
//
//    public int processGPSData(SchedulerTO schedulerTO, String fromDate, String toDate) throws JSONException, FPRuntimeException, FPBusinessException {
//        int insertStatus = 0;
//        try {
//        Service srv = new Service();
//        ServiceSoap srvs = srv.getServiceSoap();
//        ArrayList vehicleInfo = new ArrayList();
//        VehicleInfo veh = null;
//        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
//            DocumentBuilder db = dbf.newDocumentBuilder();
//            Document dom = null;
//            System.out.println("xml data:" + srvs.getInformationAll("ponpure"));
//            dom = db.parse(new InputSource(new ByteArrayInputStream(srvs.getInformationAll("ponpure").getBytes())));
//            dom.getDocumentElement().normalize();
////            System.out.println("Root element :" + dom.getDocumentElement().getNodeName());
//            NodeList nList = dom.getElementsByTagName("VEHICLE");
//            for (int temp = 0; temp < nList.getLength(); temp++) {
//
//                Node nNode = nList.item(temp);
//		       //   System.out.println("\nCurrent Element :" + nNode.getNodeName());
//                //  System.out.println("nNode.getNodeType():"+nNode.getNodeType());
//                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
//
//                    Element eElement = (Element) nNode;
//                    System.out.println("eElement = " + eElement);
//			//System.out.println("vehicleId : " + eElement.getElementsByTagName("VEHICLE_ID").item(0).getTextContent());
//                    veh = new VehicleInfo();
//                    System.out.println("First === "+eElement.getElementsByTagName("VEHICLE_ID").item(0).toString());
//                    Node nNodeNew = eElement.getElementsByTagName("VEHICLE_ID").item(0);
//                    System.out.println("nNodeNew one = " + nNodeNew);
//                    System.out.println("nNodeNew two = " + nNodeNew.getTextContent().toString());
////                    System.out.println("Second === "+eElement.getElementsByTagName("VEHICLE_ID").item(0).getTextContent());
////                    veh.setVehicleId(eElement.getElementsByTagName("VEHICLE_ID").item(0).getTextContent().trim());
////                    veh.setVehicleAlias(eElement.getElementsByTagName("VEHICLE_ALIAS").item(0).getTextContent().trim());
////                    veh.setVehicleRtoNo(eElement.getElementsByTagName("VEHICLE_RTO_NO").item(0).getTextContent().trim());
////                    veh.setCurrentLocation(eElement.getElementsByTagName("VEHICLE_LOCATION").item(0).getTextContent().trim());
////                    veh.setGpsDateTime(eElement.getElementsByTagName("VEHICLE_GPS_DATETIME").item(0).getTextContent().trim());
////                    veh.setLatitude(eElement.getElementsByTagName("VEHICLE_LAT").item(0).getTextContent().trim());
////                    veh.setLongitude(eElement.getElementsByTagName("VEHICLE_LONG").item(0).getTextContent().trim());
////                    veh.setVehicleSpeed(eElement.getElementsByTagName("VEHICLE_SPEED").item(0).getTextContent().trim());
////                    veh.setVehicleDayKm(eElement.getElementsByTagName("VEHICLE_DAYKM").item(0).getTextContent().trim());
////                    veh.setVehicleOdometerReading(eElement.getElementsByTagName("VEHICLE_ODOMETER").item(0).getTextContent().trim());
////                    veh.setGeoFeneceLoc(eElement.getElementsByTagName("GEOFENCE_LOC").item(0).getTextContent().trim());
////                    veh.setGeoFeneceInd(eElement.getElementsByTagName("GEOFENCE_IND").item(0).getTextContent().trim());
////                    veh.setGeoFeneceDateTime(eElement.getElementsByTagName("GEOFENCE_DATETIME").item(0).getTextContent().trim());
////                    veh.setVendor(eElement.getElementsByTagName("VENDOR").item(0).getTextContent().trim());
//                    vehicleInfo.add(veh);
//                }
//            }
//            schedulerTO.setVehicleInfo(vehicleInfo);
////            insertStatus = schedulerDAO.processGPSData(schedulerTO, fromDate, toDate);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return insertStatus;
//    }
//
//
//
//    public int processGPSDataOLD(SchedulerTO schedulerTO, String fromDate, String toDate) throws JSONException, FPRuntimeException, FPBusinessException {
//        int insertStatus = 0;
//        try {
//            Service srv = new Service();
//            ServiceSoap srvs = srv.getServiceSoap();
//            ArrayList vehicleInfo = new ArrayList();
//            VehicleInfo veh = null;
//            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
//            DocumentBuilder db = dbf.newDocumentBuilder();
//            Document dom = null;
//            String inputLine = null;
//            inputLine = (String) srvs.getInformationAll("ponpure");
//            JSONObject jObject  = new JSONObject(inputLine);
//            JSONObject data = jObject.getJSONObject("DocumentElement");
//            System.out.println("jObject = " + data);
//            while (inputLine != null) {
//                JSONArray objArray = new JSONArray(inputLine);
//                for (int i = 0; i < objArray.length(); i++) {
//                    JSONObject innerObj = (JSONObject) objArray.get(i);
//                    veh = new VehicleInfo();
//                    veh.setVehicleId(innerObj.getString("VEHICLE_ID"));
//                    veh.setVehicleAlias(innerObj.getString("VEHICLE_ALIAS"));
//                    veh.setVehicleRtoNo(innerObj.getString("VEHICLE_RTO_NO"));
//                    veh.setCurrentLocation(innerObj.getString("VEHICLE_LOCATION"));
//                    veh.setGpsDateTime(innerObj.getString("VEHICLE_GPS_DATETIME"));
//                    veh.setLatitude(innerObj.getString("VEHICLE_LAT"));
//                    veh.setLongitude(innerObj.getString("VEHICLE_LONG"));
//                    veh.setVehicleSpeed(innerObj.getString("VEHICLE_SPEED"));
//                    veh.setVehicleDayKm(innerObj.getString("VEHICLE_DAYKM"));
//                    veh.setVehicleOdometerReading(innerObj.getString("VEHICLE_ODOMETER"));
//                    veh.setGeoFeneceLoc(innerObj.getString("GEOFENCE_LOC"));
//                    veh.setGeoFeneceInd(innerObj.getString("GEOFENCE_IND"));
//                    veh.setGeoFeneceDateTime(innerObj.getString("GEOFENCE_DATETIME"));
//                    veh.setVendor(innerObj.getString("VENDOR"));
//                    vehicleInfo.add(veh);
//
//                }
//                System.out.println("obj = " + objArray);
//                schedulerTO.setVehicleInfo(vehicleInfo);
//                insertStatus = schedulerDAO.processGPSData(schedulerTO, fromDate, toDate);
//
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return insertStatus;
//    }
    public int processLoadCSV(SchedulerTO schedulerTO, String fromDate, String toDate) throws JSONException, FPRuntimeException, FPBusinessException {
        int status = 0;
        ArrayList wayBillsList = new ArrayList();
        SqlMapClient session = schedulerDAO.getSqlMapClient();
        try {
            session.startTransaction();
            String fileName = "";
            fileName = ThrottleConstants.csvFileDirectory + "NEW_CLPL.csv";
            File file = new File(fileName);
            System.out.println("file = " + file);
            ArrayList ogplBarCodeLoaded = new ArrayList();
            if (file.exists()) {

                BufferedReader br = null;
                String line = "";
                String csvSplitBy = ",";
//            CSVReader csvReader = new CSVReader(new FileReader(fileName));
                br = new BufferedReader(new FileReader(fileName));
                int count = 0;
                LoadCSVTO loadCSVTO = new LoadCSVTO();
                LoadCSVTO csvTO = new LoadCSVTO();
                while ((line = br.readLine()) != null) {
                    // use comma as separator
                    // Katchathu No, OWNERSHIP, TRIP DATE, PARTY_S NAME, ORDER NO, ORDER TYPE, PRODUCT_PACKING, TONNAGE, INVOICE NO, GP NO, VEHICLE NO, DESTINATION, THROTTLE DESTINATION, DRIVER NAME, DRIVER MOB#NO#, FREIGHT AMOUNT, MARKET FREIGHT, ADVANCE AMOUNT, BUNK NAME, FUEL LITERS, FUEL AMOUNT

                    loadCSVTO = new LoadCSVTO();
                    String[] row = line.split(csvSplitBy);
//                    if (count == 0) {
//                        br.readLine();
//                    } else {
                    System.out.println(row[0] + " # " + row[1] + " #  " + row[2]);
                    loadCSVTO.setParam1(row[0]);
                    loadCSVTO.setParam2(row[1]);
                    loadCSVTO.setParam3(row[2]);
                    loadCSVTO.setParam4(row[3]);
                    loadCSVTO.setParam5(row[4]);
                    loadCSVTO.setParam6(row[5]);
                    loadCSVTO.setParam7(row[6]);
                    loadCSVTO.setParam8(row[7]);
                    loadCSVTO.setParam9(row[8]);
                    loadCSVTO.setParam10(row[9]);
                    loadCSVTO.setParam11(row[10]);
                    loadCSVTO.setParam12(row[11]);
                    loadCSVTO.setParam13(row[12]);
                    loadCSVTO.setParam14(row[13]);
                    loadCSVTO.setParam15(row[14]);
                    loadCSVTO.setParam16(row[15]);
                    loadCSVTO.setParam17(row[16]);
                    loadCSVTO.setParam18(row[17]);
                    loadCSVTO.setParam19(row[18]);
                    loadCSVTO.setParam20(row[19]);
                    loadCSVTO.setParam21(row[20]);
                    loadCSVTO.setParam22(row[21]);
                    loadCSVTO.setParam23(row[22]);
                    loadCSVTO.setParam24(row[23]);
                    loadCSVTO.setParam25(row[24]);
                    loadCSVTO.setParam26(row[25]);
                    loadCSVTO.setParam27(row[26]);
                    ogplBarCodeLoaded.add(loadCSVTO);
//                    }
                    count++;

                }
                csvTO.setBarCodeList(ogplBarCodeLoaded);
                status = schedulerDAO.insertCSVData(csvTO, session);
            }
            session.commitTransaction();
        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                System.out.println("Error while flushing/closing fileWriter !!!");
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return status;
    }

    public ArrayList getDailyVehicleMovement() throws FPRuntimeException, FPBusinessException, IOException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        String systemTime = sdf.format(c.getTime()).toString();
        System.out.println("systemTime = " + systemTime);
        String[] dateTemp = systemTime.split(" ");
        String[] timeTemp = dateTemp[1].split(":");

//             -------DVM scheduler----------
        String rangeStartDate = "";
        String rangeStartDate1 = "";
        String Filename = "";
        String File_Name = "";
        String contentdata = "";
        StringBuffer contentSB = new StringBuffer();
        SendEmail em = new SendEmail();
        ArrayList alertsDetails = new ArrayList();
        ArrayList alertsEmail = new ArrayList();

        File theDir = new File("c:\\DVMAlerts");

        if (!theDir.exists()) {
            boolean result = theDir.mkdir();
            if (result) {
                System.out.println("c:\\DVMAlerts\\DIR created");
            }

        }
//        }

        try {
            int i = 0;

            System.out.println("inside processAlerts function");
            final Properties properties = new Properties();

            String recTo = ThrottleConstants.dvmToMailId;
            String recCc = ThrottleConstants.dvmCcMailId;
            String recBcc = ThrottleConstants.dvmBccMailId;
            System.out.println("recipients recTo:  " + recTo);
            System.out.println("recipients recCc: " + recCc);
            System.out.println("recipients recBcc: " + recBcc);

            Date dNow1 = new Date();
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            SimpleDateFormat processDateFormat11 = new SimpleDateFormat("yyyy-MM-dd");
            String dt = dateFormat1.format(dNow1).toString();
            String substr = dt.substring(11, 13);

            int MILLIS_IN_DAY = 1000 * 60 * 60 * 24 * 7;
//                Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
            SimpleDateFormat processDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat processDateFormat1 = new SimpleDateFormat("dd-MM-yyyy");
            rangeStartDate1 = processDateFormat1.format(date.getTime());
            System.out.println("rangeStartDate1@@@" + rangeStartDate1);
            rangeStartDate = processDateFormat.format(date.getTime());
            System.out.println("rangeStartDate@@@" + rangeStartDate);
            System.out.println("recipients: " + rangeStartDate);

            int uStatus = 0;
            ReportTO reportTO = null;

            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("hh:mm:a");
            SimpleDateFormat fHour = new SimpleDateFormat("hh");
            System.out.println("Current Date: " + ft.format(dNow));
            System.out.println("Current Hour: " + fHour.format(dNow));
            String curTime = ft.format(dNow);
            String[] temp = null;
            temp = curTime.split(":");
            int curHour = Integer.parseInt(fHour.format(dNow));
            System.out.println("curTime = " + curTime);
            System.out.println("curHour = " + curHour);
            String curMeridian = temp[2];
            System.out.println("curMeridian = " + curMeridian);

            int time = 0;
            int everyHourStatus = 0;
            int oddEven = curHour % 2;
            if (oddEven > 0) {
                everyHourStatus = 0;
            } else {
                everyHourStatus = 1;
            }
            String emailFormat4 = "";
            String emailFormat3 = "";
            String emailFormat2 = "";
            String emailFormat1 = "<html>"
                    + "<body>"
                    + "<p style='font-style: italic;'><b>Dear Sir/Madam, <br><br>Please find below Attatchement of Daily vehicle Moving  alert<br><br/>";

            String timeNow = temp[0] + "_" + temp[1] + "_" + temp[2];
            String filename = "c:\\DVMAlerts\\" + "DVM_Summary_" + rangeStartDate1 + ".xls";
//                String filename = "c:/DVMAlerts/" + "DVM_Summary_" + rangeStartDate1 + ".xls";
            System.out.println("filename = " + filename);
            ArrayList dvmDetailsDays = new ArrayList();
            dvmDetailsDays = schedulerDAO.getDailyVehicleMovement();
            System.out.println("dvmDetailsDaysSize = " + dvmDetailsDays.size());

//                 Excel start
            String sheetName = "DVM_" + rangeStartDate1;
          //  Workbook wr = WorkbookFactory.create(new file(sheetName));
           
            HSSFWorkbook my_workbook = new HSSFWorkbook();
            System.out.println("sheetName@@@" + sheetName);
            HSSFSheet sheet1 = (HSSFSheet) my_workbook.createSheet(sheetName); 
            Sheet my_sheet = (Sheet) my_workbook.createSheet(sheetName);
            System.out.println("my_sheet@@@" + my_sheet);
            
            Row s1Row1 = my_sheet.createRow((short) 0);
            s1Row1.setHeightInPoints(50); // row hight
            
            HSSFCellStyle style = my_workbook.createCellStyle();
            style.setFillForegroundColor(HSSFColor.CORNFLOWER_BLUE.index);
            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            style.setRightBorderColor(IndexedColors.BLACK.getIndex());
            style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            style.setTopBorderColor(IndexedColors.BLACK.getIndex());
            style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            HSSFFont font = my_workbook.createFont();
            font.setFontName(HSSFFont.FONT_ARIAL);
            font.setFontHeightInPoints((short) 10);
            font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            font.setColor(HSSFColor.BLACK.index);
            style.setFont(font);
            
            
            Cell cellc1 = s1Row1.createCell((short) 0);
            cellc1.setCellValue("S.No");
            cellc1.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 0, (short) 4000); // cell width
            
            Cell cellc2 = s1Row1.createCell((short) 1);
            cellc2.setCellValue("Vehicle No");
            cellc2.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 1, (short) 4000); // cell width
            
            Cell cellc3 = s1Row1.createCell((short) 2);
            cellc3.setCellValue("Vehicle Type");
            cellc3.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 2, (short) 4000); // cell width
            
            Cell cellc4 = s1Row1.createCell((short) 3);
            cellc4.setCellValue("Driver Name");
            cellc4.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 3, (short) 4000); // cell width
            
            Cell cellc5 = s1Row1.createCell((short) 4);
            cellc5.setCellValue("Mob. Number");
            cellc5.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 4, (short) 4000); // cell width
            
            Cell cellc6 = s1Row1.createCell((short) 5);
            cellc6.setCellValue("TPT");
            cellc6.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 5, (short) 4000); // cell width
            
            Cell cellc7 = s1Row1.createCell((short) 6);
            cellc7.setCellValue("GR Number");
            cellc7.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 6, (short) 4000); // cell width
            
            Cell cellc8 = s1Row1.createCell((short) 7);
            cellc8.setCellValue("GR Date");
            cellc8.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 7, (short) 4000); // cell width
            
            Cell cellc9 = s1Row1.createCell((short) 8);
            cellc9.setCellValue("Activity");
            cellc9.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 8, (short) 4000); // cell width
            
            Cell cellc10 = s1Row1.createCell((short) 9);
            cellc10.setCellValue("Customer Name");
            cellc10.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 9, (short) 4000); // cell width
            
            Cell cellc11 = s1Row1.createCell((short) 10);
            cellc11.setCellValue("Route");
            cellc11.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 10, (short) 4000); // cell width
            
            Cell cellc12 = s1Row1.createCell((short) 11);
            cellc12.setCellValue("Container 1");
            cellc12.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 11, (short) 4000); // cell width
            
            Cell cellc13 = s1Row1.createCell((short) 12);
            cellc13.setCellValue("Container 2");
            cellc13.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 12, (short) 4000); // cell width
            
            Cell cellc14 = s1Row1.createCell((short) 13);
            cellc14.setCellValue("20/40");
            cellc14.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 13, (short) 4000); // cell width
            
            Cell cellc15 = s1Row1.createCell((short) 14);
            cellc15.setCellValue("Current Status");
            cellc15.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 14, (short) 4000); // cell width
            
            Cell cellc16 = s1Row1.createCell((short) 15);
            cellc16.setCellValue("Date");
            cellc16.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 15, (short) 4000); // cell width
            
            Cell cellc17 = s1Row1.createCell((short) 16);
            cellc17.setCellValue("Remarks");
            cellc17.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 16, (short) 4000); // cell width
            

            Iterator itr = dvmDetailsDays.iterator();
            reportTO = null;
            int cntr = 1;
            while (itr.hasNext()) {
                reportTO = (ReportTO) itr.next();

                s1Row1 = my_sheet.createRow((short) cntr);
                s1Row1.createCell((short) 0).setCellValue(cntr);
                s1Row1.createCell((short) 1).setCellValue(reportTO.getRegNo());
                s1Row1.createCell((short) 2).setCellValue(reportTO.getVehicleType());
                s1Row1.createCell((short) 3).setCellValue(reportTO.getDriverName());
                s1Row1.createCell((short) 4).setCellValue(reportTO.getMobile());
                s1Row1.createCell((short) 5).setCellValue(reportTO.getTransporter());
                s1Row1.createCell((short) 6).setCellValue(reportTO.getGrNumber());
                s1Row1.createCell((short) 7).setCellValue(reportTO.getGrDate());
                s1Row1.createCell((short) 8).setCellValue(reportTO.getActivity());
                s1Row1.createCell((short) 9).setCellValue(reportTO.getCustomerName());
                s1Row1.createCell((short) 10).setCellValue(reportTO.getRouteName());
                s1Row1.createCell((short) 11).setCellValue(reportTO.getContainer1());
                s1Row1.createCell((short) 12).setCellValue(reportTO.getContainer2());
                s1Row1.createCell((short) 13).setCellValue(reportTO.getContainerType());
                s1Row1.createCell((short) 14).setCellValue(reportTO.getCurrentStatus());
                s1Row1.createCell((short) 15).setCellValue(reportTO.getDate());
                s1Row1.createCell((short) 16).setCellValue(reportTO.getRemarks());
                cntr++;

            }
            System.out.println("Your excel 1 Sheet  created");
                // excel sheet end

            FileOutputStream fileOut = new FileOutputStream(filename);
            my_workbook.write(fileOut);
            fileOut.close();

            File_Name = "c:\\DVMAlerts\\" + "DVM_Summary_" + rangeStartDate1 + ".xls";
            System.out.println("File_Name" + File_Name);
            Filename = "DVM_summary_" + rangeStartDate1 + ".xls";
            System.out.println("Filename@@@!!!" + Filename);
           // String bcc = "";
            String mailContent = "DICT_Daily Vehicle Movement Summary Alerts" + rangeStartDate1 + " " + ft.format(dNow);
            System.out.println("Filename@@@" + Filename);
            System.out.println("mailContent@@@" + mailContent);
            System.out.println("recTo@@@" + recTo);
            System.out.println("recCc@@@" + recCc);
            System.out.println("recBcc@@@" + recBcc);
            System.out.println("my_workbook@@@" + my_workbook);
//            em.sendExcelMail(Filename, recTo, recCc, recBcc, mailContent, my_workbook);
            contentdata = "";

            Date dNow11 = new Date();
            SimpleDateFormat dateFormat11 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            SimpleDateFormat processDateFormat111 = new SimpleDateFormat("yyyy-MM-dd");
            String dt1 = dateFormat11.format(dNow1).toString();
            String filenameContent = "";
           // recBcc = "";

//                String atlMsg = "TMS ALERTS - Daily vehicle Moving  alert as on - " + rangeStartDate1;
            String dueAlt = "DVM alert as on " + rangeStartDate1;
//                String headContent = atlMsg + dueAlt;
//                contentdata = "Hi Greetings From Brattle, <br><br>";
            emailFormat2 = emailFormat1 + "<table border='1' cellspacing='4' cellpadding='1' width='600px'>" + contentSB.toString() + "</table>";
            emailFormat3 = emailFormat2 + "<br><br>Hi Greetings From TMS"
                    + "</tr>"
                    + "</table>"
                    + "</body></html>";
            filenameContent = filename;
            emailFormat4 = emailFormat3;
            SchedulerTO schTO = new SchedulerTO();
            schTO.setMailStatus("1");
            schTO.setMailTypeId("2");
            schTO.setMailIdTo(recTo);
            schTO.setMailIdCc(recCc);
            schTO.setMailIdBcc(recBcc);
            schTO.setMailSubjectTo("Daily vehicle movement Report");
            schTO.setMailSubjectCc("Daily vehicle movement Report");
            schTO.setMailSubjectBcc("Daily vehicle movement Report");
            schTO.setFilenameContent(filenameContent);
            schTO.setEmailFormat(emailFormat4);
            System.out.println("filenameContent" + filenameContent);
            System.out.println("emailFormat4" + emailFormat4);

            Integer mails =schedulerDAO.insertMailDetails(schTO,1);
            System.out.println("insertMailDetails@@@"+mails);
//                }
            em.sendExcelMail(Filename, recTo, recCc, recBcc, mailContent,emailFormat4, my_workbook);
            
        } catch (Exception excp) {
            System.out.println("ERROR STATUS   " + excp);

            excp.printStackTrace();
        }
        return dvmDetailsDays;
    }
//          -------DVM scheduler---end-----

// starts user Activity report
    public String getUserActivitySummary() throws FPRuntimeException, FPBusinessException {
        ArrayList userActivitySummary = new ArrayList();
        userActivitySummary = schedulerDAO.getUserActivitySummary();
        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("E dd/MM/yyyy 'at' hh:mma");
        String curDate = ft.format(dNow);
        String atlMsg = "TMS ALERTS - DICT_User Activity Summary Report as on - " + curDate;
        String emailFormat3 = "";
        String emailFormat2 = "";
        String emailFormat1 = "<html>"
                + "<body>"
                + "<p style='font-style: italic;'><b>Dear Sir/Madam, <br><br>Please find below User Activity Summary<br><br/>"
                + "<body><table border='1' style='font-style: italic;' align='center' width='80%' cellpadding='0' cellspacing='0'>"
                + "<tr><th colspan='6'> User Activity </th></tr>"
                + "<tr><th>S.No</th><th>User Name</th><th>No.of Login</th><th>login Duration</th>"
                + "</tr>";
        Iterator itr1 = userActivitySummary.iterator();
        ReportTO repTO = null;
        int i = 1;
        while (itr1.hasNext()) {
            repTO = new ReportTO();
            repTO = (ReportTO) itr1.next();
            emailFormat2 = emailFormat2
                    + "<tr style= align='center'><td>" + i + "</td><td>" + repTO.getUserName() + "</td><td>" + repTO.getNoOfLogins() + "</td><td>" + repTO.getLoginDuration() + "</td>"
                    + "</tr>";
            i = i + 1;
        }

        emailFormat2 = emailFormat2 + "</body></html>";
        emailFormat3 = "<br><br><br>"
                + "<html>"
                + "<body>"
                + "<table>"
                + "<tr>"
                + "<br><br/>Hi Greetings From TMS,"
                + "<br><br> Team Throttle."
                + " .</p>"
                //                                  + "User Activity alert as on "+ curDate;
                + "</tr>"
                + "</table>"
                + "</body></html>";

        String emailFormat = emailFormat1 + "" + emailFormat2 + "" + emailFormat3 + "";
        System.out.println("emailFormat" + emailFormat);

        return emailFormat;
    }

     // ends user Activity report
//     GR Daily Created Report
    public String getGRCreatedSummary() throws FPRuntimeException, FPBusinessException {
        ArrayList getGRCreatedSummary9AM = new ArrayList();
        ArrayList getOrderSummary9AM = new ArrayList();
        ArrayList getTripCLosedSummary9AM = new ArrayList();

        getGRCreatedSummary9AM = schedulerDAO.getGRCreatedSummary9AM();
        getOrderSummary9AM = schedulerDAO.getOrderSummary9AM();
        getTripCLosedSummary9AM = schedulerDAO.getTripCLosedSummary9AM();

        ArrayList getGRCreatedSummary9PM = new ArrayList();
        ArrayList getOrderSummary9PM = new ArrayList();
        ArrayList getTripCLosedSummary9PM = new ArrayList();

        getGRCreatedSummary9PM = schedulerDAO.getGRCreatedSummary9PM();
        getOrderSummary9PM = schedulerDAO.getOrderSummary9PM();
        getTripCLosedSummary9PM = schedulerDAO.getTripCLosedSummary9PM();

        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("E dd/MM/yyyy 'at' hh:mma");
        String curDate = ft.format(dNow);
        String atlMsg = "TMS ALERTS - DICT_User Activity Summary Report as on - " + curDate;
        String emailFormat4 = "";
        String emailFormat3 = "";
        String emailFormat2 = "";
        String emailFormat1 = "<html>"
                + "<body>"
                + "<p style='font-style: italic;'><b>Dear Sir/Madam, <br><br>Please find below GR Created Summary<br><br/>"
                + "<body><table border='1' style='font-style: italic;' align='center' width='80%' cellpadding='0' cellspacing='0'>"
                + "<tr><th colspan='3'> Summary Yesterday 9AM to 9PM </th></tr>"
                + "<tr><td align='center'>GR Created </td>"
                + "<td align='center'>No of Order </td>"
                + "<td align='center'>Trip closed</td>"
                + "</tr>";
        Iterator itr1 = getGRCreatedSummary9AM.iterator();
        Iterator itr2 = getOrderSummary9AM.iterator();
        Iterator itr3 = getTripCLosedSummary9AM.iterator();

        Iterator itr4 = getGRCreatedSummary9PM.iterator();
        Iterator itr5 = getOrderSummary9PM.iterator();
        Iterator itr6 = getTripCLosedSummary9PM.iterator();

        SchedulerTO schTO1 = null;
        SchedulerTO schTO2 = null;
        SchedulerTO schTO3 = null;

        SchedulerTO schTO4 = null;
        SchedulerTO schTO5 = null;
        SchedulerTO schTO6 = null;
//            ReportTO repTO1 = null;
//            ReportTO repTO2 = null;
//            ReportTO repTO3 = null;
        int i = 1;
        while (itr1.hasNext()) {
            schTO1 = new SchedulerTO();
            schTO2 = new SchedulerTO();
            schTO3 = new SchedulerTO();

            schTO1 = (SchedulerTO) itr1.next();
            schTO2 = (SchedulerTO) itr2.next();
            schTO3 = (SchedulerTO) itr3.next();
            schTO4 = (SchedulerTO) itr4.next();
            schTO5 = (SchedulerTO) itr5.next();
            schTO6 = (SchedulerTO) itr6.next();
            emailFormat2 = emailFormat2 + "<tr>"
                    + "<td>" + schTO1.getNoOfGR9AM() + "</td><td>" + schTO2.getNoOfOrder9AM() + "</td><td>" + schTO3.getNoOfTripClosed9AM() + "</td>"
                    + "</tr>"
                    + "<tr><th colspan='3'> Summary Yesterday 9PM to 9AM </th></tr>"
                    + "<tr><td align='center'>GR Created </td>"
                    + "<td align='center'>No of Order </td>"
                    + "<td align='center'>Trip closed</td>"
                    + "</tr>";
            emailFormat3 = emailFormat3 + "<tr>"
                    + "</td><td>" + schTO4.getNoOfGR9PM() + "</td><td>" + schTO5.getNoOfOrder9PM() + "</td><td>" + schTO6.getNoOfTripClosed9PM() + "</td>"
                    + "</tr>";
            i = i + 1;
        }

        emailFormat3 = emailFormat3 + "</body></html>";
        emailFormat4 = "<br><br><br>"
                + "<html>"
                + "<body>"
                + "<table>"
                + "<tr>"
                + "<br><br/>Hi Greetings From TMS,"
                + "<br><br> Team Throttle."
                + " .</p>"
                //                                  + "User Activity alert as on "+ curDate;
                + "</tr>"
                + "</table>"
                + "</body></html>";

        String emailFormat = emailFormat1 + "" + emailFormat2 + "" + emailFormat3 + "" + emailFormat4 + "";
        System.out.println("emailFormat" + emailFormat);

        return emailFormat;
    }
   //     GR Daily Created Report

     // monthly order status
    public ArrayList getMonthlyOrderStatus() throws FPRuntimeException, FPBusinessException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        String systemTime = sdf.format(c.getTime()).toString();
        System.out.println("systemTime = " + systemTime);
        String[] dateTemp = systemTime.split(" ");
        String[] timeTemp = dateTemp[1].split(":");
        ArrayList getTotalOrderList = new ArrayList();

//             -------DVM scheduler----------
//       try{
        String rangeStartDate = "";
        String rangeStartDate1 = "";
        String Filename = "";
        String File_Name = "";
        String contentdata = "";
        String graphSheet = "";
        StringBuffer contentSB = new StringBuffer();
        SendEmail em = new SendEmail();
        ArrayList alertsDetails = new ArrayList();
        ArrayList alertsEmail = new ArrayList();

        File theDir = new File("c:\\MonthlyOrderAlerts");

        if (!theDir.exists()) {
            boolean result = theDir.mkdir();
            if (result) {
                System.out.println("c:\\MonthlyOrderAlerts\\DIR created");
            }

        }

        try {

            int i = 0;

            System.out.println("inside processAlerts function");
            final Properties properties = new Properties();
            String recTo = ThrottleConstants.monthlyOrderToMailId;
            String recCc = ThrottleConstants.monthlyOrderCcMailId;
            String recBcc = ThrottleConstants.monthlyOrderBccMailId;
            System.out.println("recipients recTo:  " + recTo);
            System.out.println("recipients recCc: " + recCc);
            System.out.println("recipients recBcc: " + recBcc);

            Date dNow1 = new Date();
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            SimpleDateFormat processDateFormat11 = new SimpleDateFormat("yyyy-MM-dd");
            String dt = dateFormat1.format(dNow1).toString();
            String substr = dt.substring(11, 13);

            int MILLIS_IN_DAY = 1000 * 60 * 60 * 24 * 7;
//                Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
            SimpleDateFormat processDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat processDateFormat1 = new SimpleDateFormat("dd-MM-yyyy");
            rangeStartDate1 = processDateFormat1.format(date.getTime());
            System.out.println("rangeStartDate1@@@" + rangeStartDate1);
            rangeStartDate = processDateFormat.format(date.getTime());
            System.out.println("rangeStartDate@@@" + rangeStartDate);
            System.out.println("recipients: " + rangeStartDate);

            int uStatus = 0;
            SchedulerTO schTO = null;

            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("hh:mm:a");
            SimpleDateFormat fHour = new SimpleDateFormat("hh");
            System.out.println("Current Date: " + ft.format(dNow));
            System.out.println("Current Hour: " + fHour.format(dNow));
            String curTime = ft.format(dNow);
            String[] temp = null;
            temp = curTime.split(":");
            int curHour = Integer.parseInt(fHour.format(dNow));
            System.out.println("curTime = " + curTime);
            System.out.println("curHour = " + curHour);
            String curMeridian = temp[2];
            System.out.println("curMeridian = " + curMeridian);

            int time = 0;
            int everyHourStatus = 0;
            int oddEven = curHour % 2;
            if (oddEven > 0) {
                everyHourStatus = 0;
            } else {
                everyHourStatus = 1;
            }

            String timeNow = temp[0] + "_" + temp[1] + "_" + temp[2];
            String filename = "c:\\MonthlyOrderAlerts\\" + "Order_Summary_" + rangeStartDate1 + ".xls";
            System.out.println("filename = " + filename);
//                ArrayList getTotalOrderList = new ArrayList();
            ArrayList result = new ArrayList();
            ArrayList result1 = new ArrayList();
            ArrayList result2 = new ArrayList();
            ArrayList result3 = new ArrayList();
            ArrayList result4 = new ArrayList();
            ArrayList result5 = new ArrayList();
            getTotalOrderList = schedulerDAO.getTotalOrderList();
            System.out.println("getTotalOrderList.size() = " + getTotalOrderList.size());
            Iterator itrVehicle = getTotalOrderList.iterator();
            schTO = null;
            while (itrVehicle.hasNext()) {
                schTO = (SchedulerTO) itrVehicle.next();
                String consignmentOrderId = schTO.getConsignmentOrderId();
                System.out.println("itrVehicle = " + schTO.getConsignmentOrderId());
                System.out.println("itrVehicle1@@@ = " + consignmentOrderId);
                String createdOrderStatus = "created";
                result = schedulerDAO.getCreatedOrderList(consignmentOrderId, createdOrderStatus);
                if (result.size() > 0) {
                    result1.addAll(result);
                    System.out.println("Result Waiting = " + result1.size());
                } else if (result.size() == 0) {
                    String freezedOrderStatus = "freezed";
                    result = schedulerDAO.getCreatedOrderList(consignmentOrderId, freezedOrderStatus);
                    if (result.size() > 0) {
                        result2.addAll(result);
                        System.out.println("result2.size() = " + result2.size());
                    } else if (result.size() == 0) {
                        String inProgressStatus = "inProgress";
                        result = schedulerDAO.getCreatedOrderList(consignmentOrderId, inProgressStatus);
                        if (result.size() > 0) {
                            result3.addAll(result);
                            System.out.println("result3.size() = " + result3.size());
                        } else if (result.size() == 0) {
                            String endOrderStatus = "endOrder";
                            result = schedulerDAO.getCreatedOrderList(consignmentOrderId, endOrderStatus);
                            if (result.size() > 0) {
                                result4.addAll(result);
                                System.out.println("result4.size() = " + result4.size());
                            } else if (result.size() == 0) {
                                String closedOrderStatus = "closedOrder";
                                result = schedulerDAO.getCreatedOrderList(consignmentOrderId, closedOrderStatus);
                                if (result.size() > 0) {
                                    result5.addAll(result);
                                    System.out.println("result5.size() = " + result5.size());
                                } else if (result.size() == 0) {
                                    System.out.println("result Not Found = " + schTO.getConsignmentOrderId());
                                }
                            }
                        }
                    }

                }
            }

            System.out.println("result1.size() = " + result1.size());
            System.out.println("result2.size() = " + result2.size());
            System.out.println("result3.size() = " + result3.size());
            System.out.println("result4.size() = " + result4.size());
            System.out.println("result5.size() = " + result5.size());
            System.out.println("result.size() = " + result.size());

            //first my_sheet start
            HSSFWorkbook my_workbook = new HSSFWorkbook();
            String sheetName = "Order_Created" + rangeStartDate1;
            Sheet my_sheet = (Sheet) my_workbook.createSheet(sheetName);
            Row s1Row1 = my_sheet.createRow((short) 0);
            s1Row1.setHeightInPoints(20); // row hight
            
            
            // Border and Cell Color
            HSSFCellStyle style = my_workbook.createCellStyle();
            style.setFillForegroundColor(HSSFColor.CORNFLOWER_BLUE.index);
            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            style.setRightBorderColor(IndexedColors.BLACK.getIndex());
            style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            style.setTopBorderColor(IndexedColors.BLACK.getIndex());
            style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            HSSFFont font = my_workbook.createFont();
            font.setFontName(HSSFFont.FONT_ARIAL);
            font.setFontHeightInPoints((short) 10);
            font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            font.setColor(HSSFColor.BLACK.index);
            style.setFont(font);
        

            Cell cellc1 = s1Row1.createCell((short) 0);
            cellc1.setCellValue("S.No");
            cellc1.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 0, (short) 4000); // cell width
            Cell cellc2 = s1Row1.createCell((short) 1);
            cellc2.setCellValue("Consignment No");
            cellc2.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 1, (short) 4000); // cell width
            Cell cellc3 = s1Row1.createCell((short) 2);
            cellc3.setCellValue("Billing Party");
            cellc3.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 2, (short) 4000); // cell width
            Cell cellc4 = s1Row1.createCell((short) 3);
            cellc4.setCellValue("Customer");
            cellc4.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 3, (short) 4000); // cell width
            Cell cellc5 = s1Row1.createCell((short) 4);
            cellc5.setCellValue("Order Type");
            cellc5.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 4, (short) 4000); // cell width
            Cell cellc6 = s1Row1.createCell((short) 5);
            cellc6.setCellValue("Pick up Date");
            cellc6.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 5, (short) 4000); // cell width
            Cell cellc7 = s1Row1.createCell((short) 6);
            cellc7.setCellValue("Status");
            cellc7.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 6, (short) 4000); // cell width
            Cell cellc8 = s1Row1.createCell((short) 7);
            cellc8.setCellValue("Route");
            cellc8.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 7, (short) 4000); // cell width
            Cell cellc9 = s1Row1.createCell((short) 8);
            cellc9.setCellValue("Total Km");
            cellc9.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 8, (short) 4000); // cell width
            Cell cellc10 = s1Row1.createCell((short) 9);
            cellc10.setCellValue("Total Volume");
            cellc10.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 9, (short) 4000); // cell width

            Iterator itr = result1.iterator();
            schTO = null;
            int cntr = 1;
            int order_closed = 0;
            int job_Dedicate = 0;
            int job_General = 0;
            int order_end = 0;
            int order_progress = 0;
            int order_freezed = 0;
            int order_created = 0;

            while (itr.hasNext()) {
                schTO = (SchedulerTO) itr.next();

                s1Row1 = my_sheet.createRow((short) cntr);
                s1Row1.createCell((short) 0).setCellValue(cntr);
                s1Row1.createCell((short) 1).setCellValue(schTO.getConsignmentOrderNo());
                s1Row1.createCell((short) 2).setCellValue(schTO.getBillingParty());
                if ("Order Created".equals(schTO.getStatusname())) {
                    order_created++;
                } else if ("Trip Freezed".equals(schTO.getStatusname())) {
                    order_freezed++;
                } else if ("Trip Started / Trip In Progress".equals(schTO.getStatusname())) {
                    order_progress++;
                } else if ("Trip End".equals(schTO.getStatusname())) {
                    order_end++;
                } else if ("Trip Closure".equals(schTO.getStatusname())) {
                    order_closed++;
                }
                s1Row1.createCell((short) 3).setCellValue(schTO.getCustomerName());
                s1Row1.createCell((short) 4).setCellValue(schTO.getMovementType());
                s1Row1.createCell((short) 5).setCellValue(schTO.getVehicleRequiredDateTime());
                s1Row1.createCell((short) 6).setCellValue(schTO.getStatusname());
                s1Row1.createCell((short) 7).setCellValue(schTO.getRouteInfo());
                s1Row1.createCell((short) 8).setCellValue(schTO.getTotalWeight());
                s1Row1.createCell((short) 9).setCellValue(schTO.getTotalVolume());


                cntr++;
//                        }
//                    }

            }
            System.out.println("Your excel 1 Sheet  created");
                //1 my_sheet end


            //2 my_sheet start
            sheetName = "Order_Freezed" + rangeStartDate1;
            Sheet my_sheet1 = (Sheet) my_workbook.createSheet(sheetName);
            
            Row s1Row2 = my_sheet1.createRow((short) 0);
            s1Row2.setHeightInPoints(20); // row hight
            
            s1Row2 = my_sheet1.createRow((short) 0);
            s1Row2.setHeightInPoints(20); // row hight
            Cell cellf1 = s1Row2.createCell((short) 0);
            cellf1.setCellValue("S.No");
            cellf1.setCellStyle((CellStyle) style);
            my_sheet1.setColumnWidth((short) 0, (short) 4000); // cell width
            Cell cellf2 = s1Row2.createCell((short) 1);
            cellf2.setCellValue("Consignment No");
            cellf2.setCellStyle((CellStyle) style);
            my_sheet1.setColumnWidth((short) 1, (short) 4000); // cell width
            Cell cellf3 = s1Row2.createCell((short) 2);
            cellf3.setCellValue("Billing Party");
            cellf3.setCellStyle((CellStyle) style);
            my_sheet1.setColumnWidth((short) 2, (short) 4000); // cell width
            Cell cellf4 = s1Row2.createCell((short) 3);
            cellf4.setCellValue("Customer");
            cellf4.setCellStyle((CellStyle) style);
            my_sheet1.setColumnWidth((short) 3, (short) 4000); // cell width
            Cell cellf5 = s1Row2.createCell((short) 4);
            cellf5.setCellValue("Order Type");
            cellf5.setCellStyle((CellStyle) style);
            my_sheet1.setColumnWidth((short) 4, (short) 4000); // cell width
            Cell cellf6 = s1Row2.createCell((short) 5);
            cellf6.setCellValue("Pick up Date");
            cellf6.setCellStyle((CellStyle) style);
            my_sheet1.setColumnWidth((short) 5, (short) 4000); // cell width
            Cell cellf7 = s1Row2.createCell((short) 6);
            cellf7.setCellValue("Status");
            cellf7.setCellStyle((CellStyle) style);
            my_sheet1.setColumnWidth((short) 6, (short) 4000); // cell width
            Cell cellf8 = s1Row2.createCell((short) 7);
            cellf8.setCellValue("Route");
            cellf8.setCellStyle((CellStyle) style);
            my_sheet1.setColumnWidth((short) 7, (short) 4000); // cell width
            Cell cellf9 = s1Row2.createCell((short) 8);
            cellf9.setCellValue("Total Km");
            cellf9.setCellStyle((CellStyle) style);
            my_sheet1.setColumnWidth((short) 8, (short) 4000); // cell width
            Cell cellf10 = s1Row2.createCell((short) 9);
            cellf10.setCellValue("Total Volume");
            cellf10.setCellStyle((CellStyle) style);
            my_sheet1.setColumnWidth((short) 9, (short) 4000); // cell width
            //result = paDBHelper.getWatingForUnLoading();

            itr = result2.iterator();
            schTO = null;
            cntr = 1;
            while (itr.hasNext()) {
                schTO = (SchedulerTO) itr.next();
                s1Row2 = my_sheet1.createRow((short) cntr);
                s1Row2.createCell((short) 0).setCellValue(cntr);
                s1Row2.createCell((short) 1).setCellValue(schTO.getConsignmentOrderNo());
                s1Row2.createCell((short) 2).setCellValue(schTO.getBillingParty());
                if ("Order Created".equals(schTO.getStatusname())) {
                    order_created++;
                } else if ("Trip Freezed".equals(schTO.getStatusname())) {
                    order_freezed++;
                } else if ("Trip Started / Trip In Progress".equals(schTO.getStatusname())) {
                    order_progress++;
                } else if ("Trip End".equals(schTO.getStatusname())) {
                    order_end++;
                } else if ("Trip Closure".equals(schTO.getStatusname())) {
                    order_closed++;
                }
                s1Row2.createCell((short) 3).setCellValue(schTO.getCustomerName());
                s1Row2.createCell((short) 4).setCellValue(schTO.getMovementType());
                s1Row2.createCell((short) 5).setCellValue(schTO.getVehicleRequiredDateTime());
                s1Row2.createCell((short) 6).setCellValue(schTO.getStatusname());
                s1Row2.createCell((short) 7).setCellValue(schTO.getRouteInfo());
                s1Row2.createCell((short) 8).setCellValue(schTO.getTotalWeight());
                s1Row2.createCell((short) 9).setCellValue(schTO.getTotalVolume());

                cntr++;

            }
            System.out.println("Your excel 2 Sheet  created");
                //2 my_sheet end
            //3 my_sheet start

            sheetName = "Order_In_Progress" + rangeStartDate1;
            Sheet my_sheet2 = (Sheet) my_workbook.createSheet(sheetName);
            
            Row s1Row3 = my_sheet2.createRow((short) 0);
            s1Row3.setHeightInPoints(20); // row hight
            
            s1Row3 = my_sheet2.createRow((short) 0);
            s1Row3.setHeightInPoints(20); // row hight
            Cell cellp1 = s1Row3.createCell((short) 0);
            cellp1.setCellValue("S.No");
            cellp1.setCellStyle((CellStyle) style);
            my_sheet2.setColumnWidth((short) 0, (short) 4000); // cell width
            Cell cellp2 = s1Row3.createCell((short) 1);
            cellp2.setCellValue("Consignment No");
            cellp2.setCellStyle((CellStyle) style);
            my_sheet2.setColumnWidth((short) 1, (short) 4000); // cell width
            Cell cellp3 = s1Row3.createCell((short) 2);
            cellp3.setCellValue("Billing Party");
            cellp3.setCellStyle((CellStyle) style);
            my_sheet2.setColumnWidth((short) 2, (short) 4000); // cell width
            Cell cellp4 = s1Row3.createCell((short) 3);
            cellp4.setCellValue("Customer");
            cellp4.setCellStyle((CellStyle) style);
            my_sheet2.setColumnWidth((short) 3, (short) 4000); // cell width
            Cell cellp5 = s1Row3.createCell((short) 4);
            cellp5.setCellValue("Order Type");
            cellp5.setCellStyle((CellStyle) style);
            my_sheet2.setColumnWidth((short) 4, (short) 4000); // cell width
            Cell cellp6 = s1Row3.createCell((short) 5);
            cellp6.setCellValue("Pick up Date");
            cellp6.setCellStyle((CellStyle) style);
            my_sheet2.setColumnWidth((short) 5, (short) 4000); // cell width
            Cell cellp7 = s1Row3.createCell((short) 6);
            cellp7.setCellValue("Status");
            cellp7.setCellStyle((CellStyle) style);
            my_sheet2.setColumnWidth((short) 6, (short) 4000); // cell width
            Cell cellp8 = s1Row3.createCell((short) 7);
            cellp8.setCellValue("Route");
            cellp8.setCellStyle((CellStyle) style);
            my_sheet2.setColumnWidth((short) 7, (short) 4000); // cell width
            Cell cellp9 = s1Row3.createCell((short) 8);
            cellp9.setCellValue("Total Km");
            cellp9.setCellStyle((CellStyle) style);
            my_sheet2.setColumnWidth((short) 8, (short) 4000); // cell width
            Cell cellp10 = s1Row3.createCell((short) 9);
            cellp10.setCellValue("Total Volume");
            cellp10.setCellStyle((CellStyle) style);
            my_sheet2.setColumnWidth((short) 9, (short) 4000); // cell width
//                result = paDBHelper.getTripInProgress();

            itr = result3.iterator();
            schTO = null;
            cntr = 1;
            while (itr.hasNext()) {
                schTO = (SchedulerTO) itr.next();

                s1Row3 = my_sheet2.createRow((short) cntr);
                s1Row3.createCell((short) 0).setCellValue(cntr);
                s1Row3.createCell((short) 1).setCellValue(schTO.getConsignmentOrderNo());
                s1Row3.createCell((short) 2).setCellValue(schTO.getBillingParty());
                if ("Order Created".equals(schTO.getStatusname())) {
                    order_created++;
                } else if ("Trip Freezed".equals(schTO.getStatusname())) {
                    order_freezed++;
                } else if ("Trip Started / Trip In Progress".equals(schTO.getStatusname())) {
                    order_progress++;
                } else if ("Trip End".equals(schTO.getStatusname())) {
                    order_end++;
                } else if ("Trip Closure".equals(schTO.getStatusname())) {
                    order_closed++;
                }
                s1Row3.createCell((short) 3).setCellValue(schTO.getCustomerName());
                s1Row3.createCell((short) 4).setCellValue(schTO.getMovementType());
                s1Row3.createCell((short) 5).setCellValue(schTO.getVehicleRequiredDateTime());
                s1Row3.createCell((short) 6).setCellValue(schTO.getStatusname());
                s1Row3.createCell((short) 7).setCellValue(schTO.getRouteInfo());
                s1Row3.createCell((short) 8).setCellValue(schTO.getTotalWeight());
                s1Row3.createCell((short) 9).setCellValue(schTO.getTotalVolume());

                cntr++;

            }
            System.out.println("Your excel 3 Sheet  created");
            System.out.println("Trip Not Start");
                //3 my_sheet end


            //4 my_sheet start
            sheetName = "Order_End" + rangeStartDate1;
            Sheet my_sheet3 = (Sheet) my_workbook.createSheet(sheetName);
            Row s1Row4 = my_sheet3.createRow((short) 0);
            s1Row4.setHeightInPoints(20); // row hight
            Cell celle1 = s1Row4.createCell((short) 0);
            celle1.setCellValue("S.No");
            celle1.setCellStyle((CellStyle) style);
            my_sheet3.setColumnWidth((short) 0, (short) 4000); // cell width
            Cell celle2 = s1Row4.createCell((short) 1);
            celle2.setCellValue("Consignment No");
            celle2.setCellStyle((CellStyle) style);
            my_sheet3.setColumnWidth((short) 1, (short) 4000); // cell width
            Cell celle3 = s1Row4.createCell((short) 2);
            celle3.setCellValue("Billing Party");
            celle3.setCellStyle((CellStyle) style);
            my_sheet3.setColumnWidth((short) 2, (short) 4000); // cell width
            Cell celle4 = s1Row4.createCell((short) 3);
            celle4.setCellValue("Customer");
            celle4.setCellStyle((CellStyle) style);
            my_sheet3.setColumnWidth((short) 3, (short) 4000); // cell width
            Cell celle5 = s1Row4.createCell((short) 4);
            celle5.setCellValue("Movement Type");
            celle5.setCellStyle((CellStyle) style);
            my_sheet3.setColumnWidth((short) 4, (short) 4000); // cell width
            Cell celle6 = s1Row4.createCell((short) 5);
            celle6.setCellValue("Pick up Date");
            celle6.setCellStyle((CellStyle) style);
            my_sheet3.setColumnWidth((short) 5, (short) 4000); // cell width
            Cell celle7 = s1Row4.createCell((short) 6);
            celle7.setCellValue("Status");
            celle7.setCellStyle((CellStyle) style);
            my_sheet3.setColumnWidth((short) 6, (short) 4000); // cell width
            Cell celle8 = s1Row4.createCell((short) 7);
            celle8.setCellValue("Route");
            celle8.setCellStyle((CellStyle) style);
            my_sheet3.setColumnWidth((short) 7, (short) 4000); // cell width
            Cell celle9 = s1Row4.createCell((short) 8);
            celle9.setCellValue("Total Km");
            celle9.setCellStyle((CellStyle) style);
            my_sheet3.setColumnWidth((short) 8, (short) 4000); // cell width
            Cell celle10 = s1Row4.createCell((short) 9);
            celle10.setCellValue("Total Volume");
            celle10.setCellStyle((CellStyle) style);
            my_sheet3.setColumnWidth((short) 9, (short) 4000); // cell width
            //result = paDBHelper.getTripNotStart();

            itr = result4.iterator();
            schTO = null;
            cntr = 1;
            while (itr.hasNext()) {
                schTO = (SchedulerTO) itr.next();

                s1Row4 = my_sheet3.createRow((short) cntr);
                s1Row4.createCell((short) 0).setCellValue(cntr);
                s1Row4.createCell((short) 0).setCellValue(cntr);
                s1Row4.createCell((short) 1).setCellValue(schTO.getConsignmentOrderNo());
                s1Row4.createCell((short) 2).setCellValue(schTO.getBillingParty());
                if ("Order Created".equals(schTO.getStatusname())) {
                    order_created++;
                } else if ("Trip Freezed".equals(schTO.getStatusname())) {
                    order_freezed++;
                } else if ("Trip Started / Trip In Progress".equals(schTO.getStatusname())) {
                    order_progress++;
                } else if ("Trip End".equals(schTO.getStatusname())) {
                    order_end++;
                } else if ("Trip Closure".equals(schTO.getStatusname())) {
                    order_closed++;
                }
                s1Row4.createCell((short) 3).setCellValue(schTO.getCustomerName());
                s1Row4.createCell((short) 4).setCellValue(schTO.getMovementType());
                s1Row4.createCell((short) 5).setCellValue(schTO.getVehicleRequiredDateTime());
                s1Row4.createCell((short) 6).setCellValue(schTO.getStatusname());
                s1Row4.createCell((short) 7).setCellValue(schTO.getRouteInfo());
                s1Row4.createCell((short) 8).setCellValue(schTO.getTotalWeight());
                s1Row4.createCell((short) 9).setCellValue(schTO.getTotalVolume());


                cntr++;

            }
            System.out.println("Your excel 4 Sheet  created");
            System.out.println("Job 4");
                //4 my_sheet end
            //5 my_sheet start
            //my_sheet1 = (Sheet) my_workbook.createSheet("Trip On Hold" + rangeStartDate1);
            sheetName = "Order_Closed" + rangeStartDate1;
            Sheet my_sheet4 = (Sheet) my_workbook.createSheet(sheetName);
            Row s1Row5 = my_sheet4.createRow((short) 0);
            s1Row5.setHeightInPoints(20); // row hight
            Cell cellcl1 = s1Row5.createCell((short) 0);
            cellcl1.setCellValue("S.No");
            cellcl1.setCellStyle((CellStyle) style);
            my_sheet4.setColumnWidth((short) 0, (short) 4000); // cell width
            Cell cellcl2 = s1Row5.createCell((short) 1);
            cellcl2.setCellValue("Consignment No");
            cellcl2.setCellStyle((CellStyle) style);
            my_sheet4.setColumnWidth((short) 1, (short) 4000); // cell width
            Cell cellcl3 = s1Row5.createCell((short) 2);
            cellcl3.setCellValue("Billing Party");
            cellcl3.setCellStyle((CellStyle) style);
            my_sheet4.setColumnWidth((short) 2, (short) 4000); // cell width
            Cell cellcl4 = s1Row5.createCell((short) 3);
            cellcl4.setCellValue("Customer");
            cellcl4.setCellStyle((CellStyle) style);
            my_sheet4.setColumnWidth((short) 3, (short) 4000); // cell width
            Cell cellcl5 = s1Row5.createCell((short) 4);
            cellcl5.setCellValue("Segment");
            cellcl5.setCellStyle((CellStyle) style);
            my_sheet4.setColumnWidth((short) 4, (short) 4000); // cell width
            Cell cellcl6 = s1Row5.createCell((short) 5);
            cellcl6.setCellValue("Pick up Date");
            cellcl6.setCellStyle((CellStyle) style);
            my_sheet4.setColumnWidth((short) 5, (short) 4000); // cell width
            Cell cellcl7 = s1Row5.createCell((short) 6);
            cellcl7.setCellValue("Status");
            cellcl7.setCellStyle((CellStyle) style);
            my_sheet4.setColumnWidth((short) 6, (short) 4000); // cell width
            Cell cellcl8 = s1Row5.createCell((short) 7);
            cellcl8.setCellValue("Route");
            cellcl8.setCellStyle((CellStyle) style);
            my_sheet4.setColumnWidth((short) 7, (short) 4000); // cell width
            Cell cellcl9 = s1Row5.createCell((short) 8);
            cellcl9.setCellValue("Total Km");
            cellcl9.setCellStyle((CellStyle) style);
            my_sheet4.setColumnWidth((short) 8, (short) 4000); // cell width
            Cell cellcl10 = s1Row5.createCell((short) 9);
            cellcl10.setCellValue("Total Volume");
            cellcl10.setCellStyle((CellStyle) style);
            my_sheet4.setColumnWidth((short) 9, (short) 4000); // cell width

            //result = paDBHelper.getJobCardDetails();
            itr = result5.iterator();
            schTO = null;
            cntr = 1;
            while (itr.hasNext()) {
                schTO = (SchedulerTO) itr.next();

                s1Row5 = my_sheet4.createRow((short) cntr);
                s1Row5.createCell((short) 0).setCellValue(cntr);
                s1Row5.createCell((short) 0).setCellValue(cntr);
                s1Row5.createCell((short) 1).setCellValue(schTO.getConsignmentOrderNo());
                s1Row5.createCell((short) 2).setCellValue(schTO.getBillingParty());
                if ("Order Created".equals(schTO.getStatusname())) {
                    order_created++;
                } else if ("Trip Freezed".equals(schTO.getStatusname())) {
                    order_freezed++;
                } else if ("Trip Started / Trip In Progress".equals(schTO.getStatusname())) {
                    order_progress++;
                } else if ("Trip End".equals(schTO.getStatusname())) {
                    order_end++;
                } else if ("Trip Closure".equals(schTO.getStatusname())) {
                    order_closed++;
                }
                s1Row5.createCell((short) 3).setCellValue(schTO.getCustomerName());
                s1Row5.createCell((short) 4).setCellValue(schTO.getMovementType());
                s1Row5.createCell((short) 5).setCellValue(schTO.getVehicleRequiredDateTime());
                s1Row5.createCell((short) 6).setCellValue(schTO.getStatusname());
                s1Row5.createCell((short) 7).setCellValue(schTO.getRouteInfo());
                s1Row5.createCell((short) 8).setCellValue(schTO.getTotalWeight());
                s1Row5.createCell((short) 9).setCellValue(schTO.getTotalVolume());

                cntr++;

            }
            System.out.println("Your excel 5 Sheet  created");

                //5 my_sheet end
                //6 my_sheet start
            sheetName = "Summary " + rangeStartDate1;
            graphSheet = sheetName;
            my_sheet1 = (Sheet) my_workbook.createSheet(sheetName);
            s1Row1 = my_sheet1.createRow((short) 0);
            s1Row1.setHeightInPoints(20); // row hight
            // border and Cell Color
            //   HSSFCellStyle style = my_workbook.createCellStyle();
            style.setFillForegroundColor(HSSFColor.CORNFLOWER_BLUE.index);
            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            style.setRightBorderColor(IndexedColors.BLACK.getIndex());
            style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            style.setTopBorderColor(IndexedColors.BLACK.getIndex());
            style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            //  HSSFFont font = my_workbook.createFont();
            font.setFontName(HSSFFont.FONT_ARIAL);
            font.setFontHeightInPoints((short) 10);
            font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            font.setColor(HSSFColor.BLACK.index);
            style.setFont(font);

            HSSFCellStyle style1 = my_workbook.createCellStyle();
            //style1.setBorderLeft(HSSFCellStyle.BORDER_THICK);
            style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
            style1.setRightBorderColor(IndexedColors.BLACK.getIndex());

            Cell cell = s1Row1.createCell((short) 0);
            cell.setCellValue("Order Status");
            cell.setCellStyle((CellStyle) style);
            //                s1Row1.createCell((short) 0).setCellValue("Order Status");
            my_sheet1.setColumnWidth((short) 0, (short) 5000); // cell width
            //                s1Row1.createCell((short) 1).setCellValue("No of Orders");
            Cell cell1 = s1Row1.createCell((short) 1);
            cell1.setCellValue("No of Orders");
            cell1.setCellStyle((CellStyle) style);
            //cell1.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 1, (short) 5000); // cell width
            s1Row1 = my_sheet1.createRow((short) 1);
            Cell cell2 = s1Row1.createCell((short) 0);
            cell2.setCellValue("Order Created");
            cell2.setCellStyle((CellStyle) style1);
            //                s1Row1.createCell((short) 0).setCellValue("Order Created");
            Cell cell3 = s1Row1.createCell((short) 1);
            cell3.setCellValue(order_created);
            cell3.setCellStyle((CellStyle) style1);
            //                s1Row1.createCell((short) 1).setCellValue(order_created);
            s1Row1 = my_sheet1.createRow((short) 2);
            Cell cell4 = s1Row1.createCell((short) 0);
            cell4.setCellValue("Order Freezed");
            cell4.setCellStyle((CellStyle) style1);
            //                s1Row1.createCell((short) 0).setCellValue("Order Freezed");
            Cell cell5 = s1Row1.createCell((short) 1);
            cell5.setCellValue(order_freezed);
            cell5.setCellStyle((CellStyle) style1);
            //                s1Row1.createCell((short) 1).setCellValue(order_freezed);
            s1Row1 = my_sheet1.createRow((short) 3);
            Cell cell6 = s1Row1.createCell((short) 0);
            cell6.setCellValue("Order IN PROGRESS");
            cell6.setCellStyle((CellStyle) style1);
            //                s1Row1.createCell((short) 0).setCellValue("Order IN PROGRESS");
            Cell cell7 = s1Row1.createCell((short) 1);
            cell7.setCellValue(order_progress);
            cell7.setCellStyle((CellStyle) style1);
            //                s1Row1.createCell((short) 1).setCellValue(order_progress);
            s1Row1 = my_sheet1.createRow((short) 4);
            Cell cell8 = s1Row1.createCell((short) 0);
            cell8.setCellValue("Order End");
            cell8.setCellStyle((CellStyle) style1);
            //                s1Row1.createCell((short) 0).setCellValue("Order End");
            Cell cell9 = s1Row1.createCell((short) 1);
            cell9.setCellValue(order_end);
            cell9.setCellStyle((CellStyle) style1);
            //                s1Row1.createCell((short) 1).setCellValue(order_end);
            s1Row1 = my_sheet1.createRow((short) 5);
            Cell cell10 = s1Row1.createCell((short) 0);
            cell10.setCellValue("Order Closed");
            cell10.setCellStyle((CellStyle) style1);
            //                s1Row1.createCell((short) 0).setCellValue("Order Closed");
            Cell cell11 = s1Row1.createCell((short) 1);
            cell11.setCellValue(order_closed);
            cell11.setCellStyle((CellStyle) style1);
            //                s1Row1.createCell((short) 1).setCellValue(order_closed);
            s1Row1 = my_sheet1.createRow((short) 6);
            Cell cell12 = s1Row1.createCell((short) 0);
            cell12.setCellValue("");
            cell12.setCellStyle((CellStyle) style1);
            //                s1Row1.createCell((short) 0).setCellValue("");
            Cell cell13 = s1Row1.createCell((short) 1);
            cell13.setCellValue("");
            cell13.setCellStyle((CellStyle) style1);
                //                s1Row1.createCell((short) 1).setCellValue("");
            //  s1Row1.createCell((short) 7).setCellValue("");
            s1Row1 = my_sheet1.createRow((short) 7);
            Cell cell14 = s1Row1.createCell((short) 0);
            cell14.setCellValue("Total");
            cell14.setCellStyle((CellStyle) style);
            Cell cell15 = s1Row1.createCell((short) 1);
            cell15.setCellValue(order_created + order_freezed + order_progress + order_end + order_closed);
            cell15.setCellStyle((CellStyle) style);

            System.out.println("Your excel 6 Sheet  created");
            System.out.println("Summary");

            //6 my_sheet end
            my_workbook.setSheetOrder("Summary " + rangeStartDate1, 0);
            my_workbook.setSheetOrder("Order_Created" + rangeStartDate1, 1);
            my_workbook.setSheetOrder("Order_Freezed" + rangeStartDate1, 2);
            my_workbook.setSheetOrder("Order_In_Progress" + rangeStartDate1, 3);
            my_workbook.setSheetOrder("Order_End" + rangeStartDate1, 4);
            my_workbook.setSheetOrder("Order_Closed" + rangeStartDate1, 5);
            System.out.println("testing in the excel sheet creation");
            FileOutputStream fileOut = new FileOutputStream(filename);
            my_workbook.write(fileOut);
            fileOut.close();

            System.out.println("Your excel file has been generated!");
            File_Name = "c:\\MonthlyOrderAlerts\\" + "Order_Summary_" + rangeStartDate1 + ".xls";
            System.out.println(File_Name);
            //calling Pie Chart Function

//            CreatePieChartExamplesXLSX piechart = new CreatePieChartExamplesXLSX();
//            new CreatePieChartExamplesXLSX();
//            System.out.println("Your excel new file has been generated!");
//            FileInputStream chart_file_input = new FileInputStream(new File("/home/hp/Desktop/Rajlog/MonthlyOrderAlerts/" + "Order_Summary_" + rangeStartDate1 + ".xls"));
//            my_workbook = new HSSFWorkbook(chart_file_input);

               // FileOutputStream out = new FileOutputStream(new File("c:/DailyOrderAlerts/" + "Order_Summary_" + rangeStartDate1 + ".xls"));
            // my_workbook.write(out);
            //  out.close();
            //end 
            Filename = "Order_Summary_" + rangeStartDate1 + " " + ft.format(dNow) + ".xls";
            System.out.println(Filename);
            String bcc = "";
            String mailContent = "DICT_Monthly_Order_Summary Alerts" + rangeStartDate1 + " " + ft.format(dNow);
            System.out.println("entered");
            recBcc = "";
            String atlMsg = "TMS ALERTS - Order Summary alert as on - " + rangeStartDate1;
            String dueAlt = "Order Summary alert as on " + rangeStartDate1;
//            contentdata = "Hi Greetings From TMS, <br><br>";
//            contentdata = contentdata + "<table border='1' cellspacing='4' cellpadding='1' width='600px'>" + contentSB.toString() + "</table>";
//            contentdata = contentdata + "<br><br> Team Throttle";
            //  em.send(recTo, recCc, atlMsg, dueAlt, contentdata, "TMS support", "");
            String emailFormat1= "";
            String emailFormat2= "";
            emailFormat1 = "<html>"
                    + "<body>"
                    + "<p style='font-style: italic;'><b>Dear Sir/Madam, <br><br>Please find below Attatchement of Monthly Order Summary alert<br><br/>";
            emailFormat2 = emailFormat1 + "<br><br>Hi Greetings From TMS"
                    + "</body></html>";
            
            SchedulerTO schTO1 = new SchedulerTO();
            schTO1.setMailStatus("1");
            schTO1.setMailTypeId("2");
            schTO1.setMailIdTo(recTo);
            schTO1.setMailIdCc(recCc);
            schTO1.setMailIdBcc(recBcc);
            schTO1.setMailSubjectTo("Monthly Order Status Report");
            schTO1.setMailSubjectCc("Monthly Order Status Report");
            schTO1.setMailSubjectBcc("Monthly Order Status Report");
            schTO1.setFilenameContent(File_Name);
            schTO1.setEmailFormat(emailFormat2);
            System.out.println("filenameContent" + Filename);
            System.out.println("emailFormat4" + contentdata);
            
            
            Integer mails =schedulerDAO.insertMailDetails(schTO1,1);
            System.out.println("insertMailDetails@@@"+mails);
            
            em.sendExcelMail(Filename, recTo, recCc, recBcc, mailContent,emailFormat2, my_workbook);
            System.out.println("closed");
//                recTo = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts.to");
//                recCc = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts.cc");
            Date dNow11 = new Date();
            SimpleDateFormat dateFormat11 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            SimpleDateFormat processDateFormat111 = new SimpleDateFormat("yyyy-MM-dd");
            String dt1 = dateFormat11.format(dNow1).toString();
            String substr1 = dt.substring(11, 13);
               //  String compare=substr+" 10:30:00";
            // substr = substr + " 10:00:00";
//           if (substr1.trim().equals("17")) {
//                    recTo = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts.to");
//                    recCc = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts.cc");
//                } else {
//                    recTo = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts1.to");
//                    recCc = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts1.cc");
//                }

        } catch (Exception excp) {
            System.out.println("ERROR STATUS   " + excp);

            excp.printStackTrace();
        }
        return getTotalOrderList;
    }

    public ArrayList getDailyOrderGenerated() throws FPRuntimeException, FPBusinessException {
        System.out.println("getDailyOrderGenerated@@@@@@");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        String systemTime = sdf.format(c.getTime()).toString();
        System.out.println("systemTime = " + systemTime);
        String[] dateTemp = systemTime.split(" ");
        String[] timeTemp = dateTemp[1].split(":");
        ArrayList dvmDetailsDays = new ArrayList();

//             -------DVM scheduler----------
        String rangeStartDate = "";
        String rangeStartDate1 = "";
        String Filename = "";
        String File_Name = "";
        String contentdata = "";
        StringBuffer contentSB = new StringBuffer();
        SendEmail em = new SendEmail();
        ArrayList alertsDetails = new ArrayList();
        ArrayList alertsEmail = new ArrayList();

        File theDir = new File("c:\\OrderAlerts");

        if (!theDir.exists()) {
            boolean result = theDir.mkdir();
            if (result) {
                System.out.println("c:\\OrderAlerts\\DIR created");
            }

        }
//        }

        try {
            int i = 0;

            System.out.println("inside processAlerts function");
            final Properties properties = new Properties();
            String recTo =  ThrottleConstants.dailyOrderGeneratedToMailId;
            String recCc =  ThrottleConstants.dailyOrderGeneratedCcMailId;
            String recBcc = ThrottleConstants.dailyOrderGeneratedBccMailId;
            System.out.println("recipients recTo:  " + recTo);
            System.out.println("recipients recCc: " + recCc);
            System.out.println("recipients recBcc: " + recBcc);

            Date dNow1 = new Date();
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            SimpleDateFormat processDateFormat11 = new SimpleDateFormat("yyyy-MM-dd");
            String dt = dateFormat1.format(dNow1).toString();
            String substr = dt.substring(11, 13);

            int MILLIS_IN_DAY = 1000 * 60 * 60 * 24 * 7;
//                Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
            SimpleDateFormat processDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat processDateFormat1 = new SimpleDateFormat("dd-MM-yyyy");
            rangeStartDate1 = processDateFormat1.format(date.getTime());
            System.out.println("rangeStartDate1@@@" + rangeStartDate1);
            rangeStartDate = processDateFormat.format(date.getTime());
            System.out.println("rangeStartDate@@@" + rangeStartDate);
            System.out.println("recipients: " + rangeStartDate);

            int uStatus = 0;
            ReportTO reportTO = null;
            SchedulerTO schTO = null;

            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("hh:mm:a");
            SimpleDateFormat fHour = new SimpleDateFormat("hh");
            System.out.println("Current Date: " + ft.format(dNow));
            System.out.println("Current Hour: " + fHour.format(dNow));
            String curTime = ft.format(dNow);
            String[] temp = null;
            temp = curTime.split(":");
            int curHour = Integer.parseInt(fHour.format(dNow));
            System.out.println("curTime = " + curTime);
            System.out.println("curHour = " + curHour);
            String curMeridian = temp[2];
            System.out.println("curMeridian = " + curMeridian);

            int time = 0;
            int everyHourStatus = 0;
            int oddEven = curHour % 2;
            if (oddEven > 0) {
                everyHourStatus = 0;
            } else {
                everyHourStatus = 1;
            }

            String timeNow = temp[0] + "_" + temp[1] + "_" + temp[2];
            String filename = "c:\\OrderAlerts\\" + "Order_Summary_" + rangeStartDate1 + ".xls";
            System.out.println("filename = " + filename);
            ArrayList getTotalOrderList = new ArrayList();
            ArrayList result1 = new ArrayList();
            getTotalOrderList = schedulerDAO.getHourlyTotalOrderList();
            System.out.println("getTotalOrderList.size() = " + getTotalOrderList.size());
            if (getTotalOrderList.size() > 0) {
                result1.addAll(getTotalOrderList);
                System.out.println("Result Waiting = " + result1.size());
            }
            //Iterator itrVehicle = getTotalOrderList.iterator();
            schTO = null;

            System.out.println("result1.size() = " + result1.size());

            //first my_sheet start
           
            
            
            HSSFWorkbook my_workbook = new HSSFWorkbook();
            String sheetName = "Order_Created" + rangeStartDate1;
            Sheet my_sheet = (Sheet) my_workbook.createSheet(sheetName);
            Row s1Row1 = my_sheet.createRow((short) 0);
            s1Row1.setHeightInPoints(50); // row hight
            
            HSSFCellStyle style = my_workbook.createCellStyle();
            style.setFillForegroundColor(HSSFColor.CORNFLOWER_BLUE.index);
            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            style.setRightBorderColor(IndexedColors.BLACK.getIndex());
            style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            style.setTopBorderColor(IndexedColors.BLACK.getIndex());
            style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            HSSFFont font = my_workbook.createFont();
            font.setFontName(HSSFFont.FONT_ARIAL);
            font.setFontHeightInPoints((short) 10);
            font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            font.setColor(HSSFColor.BLACK.index);
            style.setFont(font);
            
            Cell cellc1 = s1Row1.createCell((short) 0);
            cellc1.setCellValue("S.No");
            cellc1.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 0, (short) 4000); // cell width
            
            Cell cellc2 = s1Row1.createCell((short) 1);
            cellc2.setCellValue("Consignment No");
            cellc2.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 1, (short) 4000); // cell width
            
            Cell cellc3 = s1Row1.createCell((short) 2);
            cellc3.setCellValue("Billing Party");
            cellc3.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 2, (short) 4000); // cell width
            
            Cell cellc4 = s1Row1.createCell((short) 3);
            cellc4.setCellValue("Customer");
            cellc4.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 3, (short) 4000); // cell width
            
            Cell cellc5 = s1Row1.createCell((short) 4);
            cellc5.setCellValue("Order Type");
            cellc5.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 4, (short) 4000); // cell width
            
            Cell cellc6 = s1Row1.createCell((short) 5);
            cellc6.setCellValue("Pick up Date");
            cellc6.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 5, (short) 4000); // cell width
            
            Cell cellc7 = s1Row1.createCell((short) 6);
            cellc7.setCellValue("Status");
            cellc7.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 6, (short) 4000); // cell width
            
            Cell cellc8 = s1Row1.createCell((short) 7);
            cellc8.setCellValue("Route");
            cellc8.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 7, (short) 4000); // cell width
            
            Cell cellc9 = s1Row1.createCell((short) 8);
            cellc9.setCellValue("Total Container");
            cellc9.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 8, (short) 4000); // cell width
           
            Cell cellc10 = s1Row1.createCell((short) 9);
            cellc10.setCellValue("unplanned 20'");
            cellc10.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 9, (short) 4000); // cell width
            
            Cell cellc11 = s1Row1.createCell((short) 10);
            cellc11.setCellValue("unplanned 40'");
            cellc11.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 10, (short) 4000); // cell width
            
            Cell cellc12 = s1Row1.createCell((short) 11);
            cellc12.setCellValue("planned 20'");
            cellc12.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 11, (short) 4000); // cell width
            
            Cell cellc13 = s1Row1.createCell((short) 12);
            cellc13.setCellValue("planned 40'");
            cellc13.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 12, (short) 4000); // cell width
            
            Iterator itr = result1.iterator();
            schTO = null;
            int cntr = 1;
            int order_closed = 0;
            int order_end = 0;
            int order_progress = 0;
            int order_freezed = 0;
            int order_created = 0;

            while (itr.hasNext()) {
                schTO = (SchedulerTO) itr.next();

                s1Row1 = my_sheet.createRow((short) cntr);
                s1Row1.createCell((short) 0).setCellValue(cntr);
                s1Row1.createCell((short) 1).setCellValue(schTO.getConsignmentOrderNo());
                s1Row1.createCell((short) 2).setCellValue(schTO.getBillingParty());
                if ("Order Created".equals(schTO.getStatusname())) {
                    order_created++;
                } else if ("Trip Freezed".equals(schTO.getStatusname())) {
                    order_freezed++;
                } else if ("Trip Started / Trip In Progress".equals(schTO.getStatusname())) {
                    order_progress++;
                } else if ("Trip End".equals(schTO.getStatusname())) {
                    order_end++;
                } else if ("Trip Closure".equals(schTO.getStatusname())) {
                    order_closed++;
                }
                s1Row1.createCell((short) 3).setCellValue(schTO.getCustomerName());
                s1Row1.createCell((short) 4).setCellValue(schTO.getMovementType());
                s1Row1.createCell((short) 5).setCellValue(schTO.getVehicleRequiredDateTime());
                s1Row1.createCell((short) 6).setCellValue(schTO.getStatusname());
                s1Row1.createCell((short) 7).setCellValue(schTO.getRouteInfo());
                s1Row1.createCell((short) 8).setCellValue(schTO.getContainerTypeName());
                s1Row1.createCell((short) 9).setCellValue(schTO.getTwentyftcontainerType());
                s1Row1.createCell((short) 10).setCellValue(schTO.getFourtyftcontainerType());
                s1Row1.createCell((short) 11).setCellValue(schTO.getPlannedTwentyftcontainerType());
                s1Row1.createCell((short) 12).setCellValue(schTO.getPlannedFourtyftcontainerType());


                cntr++;


            }
            System.out.println("Your excel 1 Sheet  created");
            
            my_workbook.setSheetOrder("Order_Created" + rangeStartDate1, 0);
            System.out.println("rangeStartDate111112:" + rangeStartDate1);

            // my_workbook.setSheetOrder("Future Trip" + rangeStartDate1, 6);
            System.out.println("testing in the excel sheet creation");
            FileOutputStream fileOut = new FileOutputStream(filename);
            my_workbook.write(fileOut);
            fileOut.close();

            System.out.println("Your excel file has been generated!");

            File_Name = "c:\\OrderAlerts\\" + "Order_Summary_" + rangeStartDate1 + ".xls";
            Filename = "Order_Summary_" + rangeStartDate1 + " " + ft.format(dNow) + ".xls";
            System.out.println(File_Name);
            System.out.println(Filename);
            String bcc = "";
            String mailContent = "DICT_Daily_Order_Summary Alerts" + rangeStartDate1 + " " + ft.format(dNow);
           
//                recTo = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts.to");
//                recCc = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts.cc");
            Date dNow11 = new Date();
            SimpleDateFormat dateFormat11 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            SimpleDateFormat processDateFormat111 = new SimpleDateFormat("yyyy-MM-dd");
            String dt1 = dateFormat11.format(dNow1).toString();
            String substr1 = dt.substring(11, 13);
               //  String compare=substr+" 10:30:00";
            // substr = substr + " 10:00:00";
//           if (substr1.trim().equals("17")) {
//                    recTo = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts.to");
//                    recCc = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts.cc");
//                } else {
//                    recTo = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts1.to");
//                    recCc = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts1.cc");
//                }

            recBcc = "";
            String emailFormat1="";
            String emailFormat2="";
            
            String atlMsg = "TMS ALERTS - Order Summary alert as on - " + rangeStartDate1;
            String dueAlt = "Order Summary alert as on " + rangeStartDate1;
            contentdata = "Hi Greetings From TMS, <br><br>";
            contentdata = contentdata + "<table border='1' cellspacing='4' cellpadding='1' width='600px'>" + contentSB.toString() + "</table>";
            contentdata = contentdata + "<br><br> Team Throttle";
            emailFormat1 = "<html>"
                    + "<body>"
                    + "<p style='font-style: italic;'><b>Dear Sir/Madam, <br><br>Please find below Attatchement of DailyOrder Generated alert<br><br/>";
            
            emailFormat2 = emailFormat1 + "<br><br>Hi Greetings From TMS"
                    + "</body></html>";
            
            
            SchedulerTO schTO1 = new SchedulerTO();
            schTO.setMailStatus("1");
            schTO1.setMailTypeId("2");
            schTO1.setMailIdTo(recTo);
            schTO1.setMailIdCc(recCc);
            schTO1.setMailIdBcc(recBcc);
            schTO1.setMailSubjectTo("DailyOrder Generated Report");
            schTO1.setMailSubjectCc("DailyOrder Generated Report");
            schTO1.setMailSubjectBcc("DailyOrder Generated Report");
            schTO1.setFilenameContent(File_Name);
            schTO1.setEmailFormat(emailFormat2);
            System.out.println("filenameContent" + Filename);
            System.out.println("emailFormat4" + emailFormat2);
            
            
            Integer mails =schedulerDAO.insertMailDetails(schTO1,1);
            System.out.println("insertMailDetails@@@"+mails);
            
//            if (result1.size() > 0) {
            em.sendExcelMail(Filename, recTo, recCc, recBcc, mailContent,emailFormat2, my_workbook);
            System.out.println("closed");
//            }
            

        } catch (Exception excp) {
            System.out.println("ERROR STATUS   " + excp);

            excp.printStackTrace();
        }
        return dvmDetailsDays;
    }

    public ArrayList getDailyProcessMain() throws FPRuntimeException, FPBusinessException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        String systemTime = sdf.format(c.getTime()).toString();
        System.out.println("systemTime = " + systemTime);
        String[] dateTemp = systemTime.split(" ");
        String[] timeTemp = dateTemp[1].split(":");
        ArrayList dailyProcessMain = new ArrayList();

//             -------DailyProcessMain scheduler----------
        boolean errorOccured = false;
        int errorSeverity = 0;
        int batchRunNum = 0;

        boolean returnValue = false;
        String rangeStartDate = "";
        String lastRunTime = "";

        SendEmail em = new SendEmail();
        ArrayList alertsDetails = new ArrayList();
        ArrayList alertsEmail = new ArrayList();
        try {
            errorOccured = false;
            errorSeverity = 0;

            try {
                System.out.println("inside DailyProcessMain processAlerts function");
                final Properties properties = new Properties();
                String recTo = "";
                String recCc = "";
              //  String recTo = batchUtil.getProperty("mail.recipients.advance.to");
                // String recCc = batchUtil.getProperty("mail.recipients.cc");
                //  System.out.println("recipients: " + recTo);

                int MILLIS_IN_DAY = 1000 * 60 * 60 * 24 * 7;
//                Date date = new Date();
                SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
                SimpleDateFormat processDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                rangeStartDate = processDateFormat.format(date.getTime());

                int uStatus = 0;
                SchedulerTO schTO = null;

                //lastRunTime = paDBHelper.getLastRunTime(batchRunNum);
                //uStatus = paDBHelper.getTripDetails();
                Date dNow = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("E dd/MM/yyyy 'at' hh:mma");
                //System.out.println("Current Date: " + ft.format(dNow));
                String curDate = ft.format(dNow);
                String atlMsg = "THROTTLE ALERTS - Batch Advance Advice - " + curDate;

                // Email Alert for Batch Advance
                //   alertsEmail = paDBHelper.getBatchAdvanceDetails();
                System.out.println("alertsEmail batch advance:" + alertsEmail.size());
                if (alertsDetails != null) {
                    String sMsg = "";
                    try {
                        int i = 0;
                        String contentData = "";
                        StringBuffer contentSB = new StringBuffer();
                        contentSB.append("<tr>");
                        contentSB.append("<th colspan='14'>Batch Advance Advice For " + curDate + "</th>");
                        contentSB.append("</tr>");
                        contentSB.append("<tr>");
                        contentSB.append("<th>S.No</th>");
                        contentSB.append("<th>Customer</th>");
                        contentSB.append("<th>C Note</th>");
                        contentSB.append("<th>Trip Code</th>");
                        contentSB.append("<th>Vehicle</th>");
                        contentSB.append("<th>Vehicle Type</th>");
                        contentSB.append("<th>Route</th>");
                        contentSB.append("<th>Driver</th>");
                        contentSB.append("<th>Trip Started On</th>");
                        contentSB.append("<th>Nett Expenses</th>");
                        contentSB.append("<th>Already Paid Amount</th>");
                        contentSB.append("<th>TransitDays</th>");
                        contentSB.append("<th>JourneyDay</th>");
                        contentSB.append("<th>To Be Paid</th>");
                        contentSB.append("</tr>");
                        Iterator itr = alertsEmail.iterator();

                        while (itr.hasNext()) {
                            i++;
                            schTO = (SchedulerTO) itr.next();
                            contentSB.append("<tr>");
                            contentSB.append("<td>" + i + "</td>");
                            contentSB.append("<td>" + schTO.getCustomerName() + "</td>");
                            contentSB.append("<td>" + schTO.getcNote() + "</td>");
                            contentSB.append("<td>" + schTO.getTripCode() + "</td>");
                            contentSB.append("<td>" + schTO.getVehicleNo() + "</td>");
                            contentSB.append("<td>" + schTO.getVehicleType() + "</td>");
                            contentSB.append("<td>" + schTO.getRoute() + "</td>");
                            contentSB.append("<td>" + schTO.getDriver() + "</td>");
                            contentSB.append("<td>" + schTO.getTripStartDate() + "</td>");
                            contentSB.append("<td>" + schTO.getNettExpense() + "</td>");
                            contentSB.append("<td>" + schTO.getPaidAmount() + "</td>");
                            contentSB.append("<td>" + schTO.getTransitDays() + "</td>");
                            contentSB.append("<td>" + schTO.getJourneyDays() + "</td>");
                            contentSB.append("<td>" + schTO.getToBePaid() + "</td>");
                            contentSB.append("</tr>");
                        }
                        contentData = "Hi, <br><br>";
                        contentData = contentData + "<table border='1' cellspacing='4' cellpadding='1' width='600px'>" + contentSB.toString() + "</table>";
                        contentData = contentData + "<br><br> Team Brattlefoods";

                        if (i > 0) {
                            String dueAlt = "Permit Due - as on " + curDate;
                            //  em.send(recTo, recCc, atlMsg, dueAlt, contentData, "Throttle support", "");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("Exception in email Send Call: " + e.getMessage());
                    }
                }
                // Email Alert To trip closed and not billed beyond 24 hours
                //  recTo = batchUtil.getProperty("mail.recipients.notBilled.to");
                //   recCc = batchUtil.getProperty("mail.recipients.notBilled.cc");
                atlMsg = "THROTTLE ALERTS - Trip Closed and Not Billed beyond 24 hours as on " + curDate;
                //   alertsEmail = paDBHelper.tripClosedNotBilledBeyond24();
                System.out.println("alertsEmail trip not billed:" + alertsEmail.size());
                if (alertsDetails != null) {
                    String sMsg = "";
                    try {
                        int i = 0;

                        String contentData = "";
                        StringBuffer contentSB = new StringBuffer();
                        contentSB.append("<tr>");
                        contentSB.append("<th colspan='10'>Trip Closed and Not Billed beyond 24 hours as on " + curDate + "</th>");
                        contentSB.append("</tr>");
                        contentSB.append("<tr>");
                        contentSB.append("<th>S.No</th>");
                        contentSB.append("<th>Vehicle</th>");
                        contentSB.append("<th>FleetCenter</th>");
                        contentSB.append("<th>Vehicle Type</th>");
                        contentSB.append("<th>Customer</th>");
                        contentSB.append("<th>C Note</th>");
                        contentSB.append("<th>Trip Code</th>");
                        contentSB.append("<th>Route</th>");
                        contentSB.append("<th>Product Category</th>");
                        contentSB.append("<th>Reefer Requirement</th>");
                        contentSB.append("<th>Trip Started On</th>");
                        contentSB.append("<th>Trip Closed On</th>");
                        contentSB.append("</tr>");
                        Iterator itr = alertsEmail.iterator();

                        while (itr.hasNext()) {
                            i++;
                            schTO = (SchedulerTO) itr.next();
                            contentSB.append("<tr>");
                            contentSB.append("<td>" + i + "</td>");
                            contentSB.append("<td>" + schTO.getVehicleNo() + "</td>");
                            contentSB.append("<td>" + schTO.getFleetCenter() + "</td>");
                            contentSB.append("<td>" + schTO.getVehicleType() + "</td>");
                            contentSB.append("<td>" + schTO.getCustomerName() + "</td>");
                            contentSB.append("<td>" + schTO.getcNote() + "</td>");
                            contentSB.append("<td>" + schTO.getTripCode() + "</td>");
                            contentSB.append("<td>" + schTO.getRoute() + "</td>");
                            contentSB.append("<td>" + schTO.getProductCategory() + "</td>");
                            contentSB.append("<td>" + schTO.getReeferRequirement() + "</td>");
                            contentSB.append("<td>" + schTO.getTripStartDate() + "</td>");
                            contentSB.append("<td>" + schTO.getTripEndDate() + "</td>");
                            contentSB.append("</tr>");
                        }
                        contentData = "Hi, <br><br> List of trips that are not billed beyond 24 hours from closure";
                        contentData = contentData + "<table border='1' cellspacing='4' cellpadding='1' width='600px'>" + contentSB.toString() + "</table>";
                        contentData = contentData + "<br><br> Team Brattlefoods";

                        if (i > 0) {
                            String dueAlt = "Permit Due - as on " + curDate;
                            //  em.send(recTo, recCc, atlMsg, dueAlt, contentData, "Throttle support", "");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("Exception in email Send Call: " + e.getMessage());
                    }
                }
                // Email Alert To trip closed and not Settled beyond 24 hours
                // recTo = batchUtil.getProperty("mail.recipients.notClosed.to");
                //  recCc = batchUtil.getProperty("mail.recipients.notClosed.cc");
                atlMsg = "THROTTLE ALERTS - Trip Closed and Not Settled beyond 24 hours as on " + curDate;
                // alertsEmail = paDBHelper.tripClosedNotSettledBeyond24();
                System.out.println("alertsEmail trip not settled:" + alertsEmail.size());
                if (alertsDetails != null) {
                    String sMsg = "";
                    try {
                        int i = 0;

                        String contentData = "";
                        StringBuffer contentSB = new StringBuffer();
                        contentSB.append("<tr>");
                        contentSB.append("<th colspan='10'>Trip Closed and Not Settled beyond 24 hours as on " + curDate + "</th>");
                        contentSB.append("</tr>");
                        contentSB.append("<tr>");
                        contentSB.append("<th>S.No</th>");
                        contentSB.append("<th>GR No</th>");
                        contentSB.append("<th>Trip Code</th>");
                        contentSB.append("<th>C Note</th>");
                        contentSB.append("<th>Billing party</th>");
                        contentSB.append("<th>Vehicle Type</th>");
                        contentSB.append("<th>Route</th>");
                        contentSB.append("<th>Trip Started On</th>");
                        contentSB.append("<th>Trip Closed On</th>");
                        contentSB.append("</tr>");
                        Iterator itr = alertsEmail.iterator();

                        while (itr.hasNext()) {
                            i++;
                            schTO = (SchedulerTO) itr.next();
                            contentSB.append("<tr>");
                            contentSB.append("<td>" + i + "</td>");
                            contentSB.append("<td>" + schTO.getGrNumber() + "</td>");
                            contentSB.append("<td>" + schTO.getTripCode() + "</td>");
                            contentSB.append("<td>" + schTO.getcNote() + "</td>");
                            contentSB.append("<td>" + schTO.getBillingParty() + "</td>");
                            contentSB.append("<td>" + schTO.getVehicleType() + "</td>");
                            contentSB.append("<td>" + schTO.getRoute() + "</td>");
                            contentSB.append("<td>" + schTO.getTripStartDate() + "</td>");
                            contentSB.append("<td>" + schTO.getTripEndDate() + "</td>");
                            contentSB.append("</tr>");
                        }
                        contentData = "Hi, <br><br> List of trips that are not Settled beyond 24 hours from closure";
                        contentData = contentData + "<table border='1' cellspacing='4' cellpadding='1' width='600px'>" + contentSB.toString() + "</table>";
                        contentData = contentData + "<br><br> Team Brattlefoods";

                        if (i > 0) {
                            String dueAlt = "" + curDate;
                            //  em.send(recTo, recCc, atlMsg, dueAlt, contentData, "Throttle support", "");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("Exception in email Send Call: " + e.getMessage());
                    }
                }
                // Email Alert To trip ended and not closed beyond 24 hours
//                recTo = batchUtil.getProperty("mail.recipients.notClosed.to");
//                recCc = batchUtil.getProperty("mail.recipients.notClosed.cc");
                recTo = ThrottleConstants.tripEndToMailId;
                recCc = ThrottleConstants.tripEndCcMailId;
                String recBcc = ThrottleConstants.tripEndBccMailId;
                System.out.println("recipients recTo:  " + recTo);
                System.out.println("recipients recCc: " + recCc);
                System.out.println("recipients recBcc: " + recBcc);

                System.out.println("recTo::" + recTo);
                atlMsg = "DICT TMS ALERT - Trip Ended and Not Closed within 48 hours as on " + curDate;
                alertsEmail = schedulerDAO.tripEndedNotClosedBeyond24();
                System.out.println("alertsEmail trip not closed:" + alertsEmail.size());
                if (alertsDetails != null) {
                    String sMsg = "";
                    try {
                        int i = 0;

                        String contentData = "";
                        StringBuffer contentSB = new StringBuffer();
                        contentSB.append("<tr>");
                        contentSB.append("<th colspan='10'>Total " + alertsEmail.size()+ "Trips Ended and Not Closed  as on " + curDate + "</th>");
                        contentSB.append("</tr>");
                        contentSB.append("<tr>");
                        contentSB.append("<th>S.No</th>");
                        contentSB.append("<th>GR No</th>");
                        contentSB.append("<th>Gr Date</th>");
                        contentSB.append("<th>Container No</th>");
                        contentSB.append("<th>Container Type</th>");
                        contentSB.append("<th>vehicle No</th>");
                        contentSB.append("<th>Billing Party</th>");
                        contentSB.append("<th>Route</th>");
                        contentSB.append("<th>Trip Started On</th>");
                        contentSB.append("<th>Trip Ended On</th>");
                        contentSB.append("</tr>");
//                        String contentTripEnd = "<table> "
//                                           + "<tr><th colspan='10'>Trip Ended and Not Closed  as on " + curDate + "</th></tr>"
//                                             + "<tr><th>S.No</th><th>GR No</th><th>Gr Date</th><th>Container No</th><th>Container Type</th>"
//                                             + "<th>vehicle No</th><th>Billing Party</th><th>Route</th><th>Trip Started On</th><th>Trip Ended On</th></tr>";

                        Iterator itr = alertsEmail.iterator();

                        while (itr.hasNext()) {
                            i++;
                            schTO = (SchedulerTO) itr.next();
                            contentSB.append("<tr>");
                            contentSB.append("<td>" + i + "</td>");
                            contentSB.append("<td>" + schTO.getGrNumber() + "</td>");
                            contentSB.append("<td>" + schTO.getGrDate() + "</td>");
                            contentSB.append("<td>" + schTO.getContainerNo() + "</td>");
                            contentSB.append("<td>" + schTO.getContainerTypeName() + "</td>");
                            contentSB.append("<td>" + schTO.getVehicleNo() + "</td>");
                            contentSB.append("<td>" + schTO.getBillingParty() + "</td>");
                            contentSB.append("<td>" + schTO.getRoute() + "</td>");
                            contentSB.append("<td>" + schTO.getTripStartDate() + "</td>");
                            contentSB.append("<td>" + schTO.getTripEndDate() + "</td>");
                            contentSB.append("</tr>");
//                           String contentTripEndList = "<tr><td>" + i + "</td><td>" + schTO.getGrNumber() + "</td><td>" + schTO.getGrDate() + "</td>"
//                                                        +"<td>" + schTO.getContainerNo() + "</td><td>" + schTO.getContainerTypeName() + "</td>"
//                                                        + "<td>" + schTO.getVehicleNo() + "</td><td>" + schTO.getBillingParty() + "</td>"
//                                                        + "<td>" + schTO.getRoute() + "</td><td>" + schTO.getTripStartDate() + "</td>"
//                                                        + "<td>" + schTO.getTripEndDate() + "</td></tr>"
//                                                        + "<br><br><br><br>" ;

                        }
                        contentData = "Hi, <br><br> List of trips that are not closed ";
                        contentData = contentData + "<table border='1' cellspacing='4' cellpadding='1' width='600px'>" + contentSB.toString() + "</table>";
                        contentData = contentData + "<br><br> TMS Team ";

                        schTO.setMailStatus("0");
                        schTO.setMailTypeId("2");
                        schTO.setMailSubjectTo("TripEnd and Not closed Report");
                        schTO.setMailSubjectCc("TripEnd and Not closed Report");
                        schTO.setMailSubjectBcc("TripEnd and Not closed Report");
                        schTO.setEmailFormat(contentData);
                      //  System.out.println("contentData@@@@@@@@@@@@@@@@@@@@@@@" + contentData);
                        schTO.setMailIdTo(recTo);
                        schTO.setMailIdCc(recCc);
                        schTO.setMailIdBcc(recBcc);

                        Integer mails = schedulerDAO.insertMailDetails(schTO, 1);
                        System.out.println("insertMailDetails@@@" + mails);
                        if (i > 0) {
                            String dueAlt = "Permit Due - as on " + curDate;
//                            ----raj here
//                            em.send(recTo, recCc, atlMsg, dueAlt, contentData, "Throttle support", "");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("Exception in email Send Call: " + e.getMessage());
                    }
                }

                // Email Alert Trips in progress
                atlMsg = "Trip In Progress - Primary as on " + curDate;
             //   alertsEmail = paDBHelper.tripInProgress();
                //  recTo = batchUtil.getProperty("mail.recipients.tripInProgress.to");
                //  recCc = batchUtil.getProperty("mail.recipients.tripInProgress.cc");
                System.out.println("alertsEmail trip in progress:" + alertsEmail.size());
                if (alertsDetails != null) {
                    String sMsg = "";
                    try {
                        int i = 0;

                        String contentData = "";
                        StringBuffer contentSB = new StringBuffer();
                        contentSB.append("<tr>");
                        contentSB.append("<th colspan='10'>Trip In Progress as on " + curDate + "</th>");
                        contentSB.append("</tr>");
                        contentSB.append("<tr>");
                        contentSB.append("<th>S.No</th>");
                        contentSB.append("<th>Vehicle</th>");
                        contentSB.append("<th>FleetCenter</th>");
                        contentSB.append("<th>Customer</th>");
                        contentSB.append("<th>C Note</th>");
                        contentSB.append("<th>Trip Code</th>");
                        contentSB.append("<th>Vehicle Type</th>");
                        contentSB.append("<th>Route</th>");
                        contentSB.append("<th>Driver Name</th>");
                        contentSB.append("<th>Estimated Transit Days</th>");
                        contentSB.append("<th>Actual Transit Days</th>");
                        contentSB.append("<th>Trip Started On</th>");
                        contentSB.append("<th>Trip Planned End</th>");
                        contentSB.append("</tr>");
                        Iterator itr = alertsEmail.iterator();

                        while (itr.hasNext()) {
                            i++;
                            schTO = (SchedulerTO) itr.next();
                            contentSB.append("<tr>");
                            contentSB.append("<td>" + i + "</td>");
                            contentSB.append("<td>" + schTO.getVehicleNo() + "</td>");
                            contentSB.append("<td>" + schTO.getFleetCenter() + "</td>");
                            contentSB.append("<td>" + schTO.getCustomerName() + "</td>");
                            contentSB.append("<td>" + schTO.getcNote() + "</td>");
                            contentSB.append("<td>" + schTO.getTripCode() + "</td>");
                            contentSB.append("<td>" + schTO.getVehicleType() + "</td>");
                            contentSB.append("<td>" + schTO.getRoute() + "</td>");
                            contentSB.append("<td>" + schTO.getEmpName() + "</td>");
                            contentSB.append("<td>" + schTO.getEstimatedTransitDays() + "</td>");
                            contentSB.append("<td>" + schTO.getActualTransitDays() + "</td>");
                            contentSB.append("<td>" + schTO.getTripStartDate() + "</td>");
                            contentSB.append("<td>" + schTO.getTripEndDate() + "</td>");
                            contentSB.append("</tr>");
                        }
                        contentData = "Hi, <br><br> List of trips that are in progress";
                        contentData = contentData + "<table border='1' cellspacing='4' cellpadding='1' width='600px'>" + contentSB.toString() + "</table>";
                        contentData = contentData + "<br><br> Team Brattlefoods";

                        if (i > 0) {
                            String dueAlt = "Permit Due - as on " + curDate;
                            //   em.send(recTo, recCc, atlMsg, dueAlt, contentData, "Throttle support", "");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("Exception in email Send Call: " + e.getMessage());
                    }
                }

                System.out.println("Now Going TO POD Not Updated For The Closed Trip");
                alertsDetails = new ArrayList();
                alertsEmail = new ArrayList();
                ArrayList hourList = new ArrayList();

                dNow = new Date();
                ft = new SimpleDateFormat("hh:mm:a");
                SimpleDateFormat fHour = new SimpleDateFormat("hh");
                System.out.println("Current Date: " + ft.format(dNow));
                System.out.println("Current Hour: " + fHour.format(dNow));
                String curTime = ft.format(dNow);
                String[] temp = null;
                temp = curTime.split(":");
                int curHour = Integer.parseInt(fHour.format(dNow));
                System.out.println("curTime = " + curTime);
                System.out.println("curHour = " + curHour);
                String curMeridian = temp[2];
                System.out.println("curMeridian = " + curMeridian);
                // Alert is for Trip Not Freeze
                //int[] podHours = {24, 48, 72, 73};
                atlMsg = "THROTTLE ALERTS - POD Not Updated For The Closed Trips as on  - " + curTime;

                //} else if (podHours[k] > 72 && everyHourStatus == 1) {
                //    alertsEmail = paDBHelper.getTripPODEvery24HourAlert(0);
                System.out.println("alertsEmail.size() = " + alertsEmail.size());
                System.out.println("alertsEmail trip no pod:" + alertsEmail.size());
                if (alertsEmail.size() > 0) {
                    alertsDetails.addAll(alertsEmail);
                    hourList.add(73);
                    alertsEmail = new ArrayList();
                }

                System.out.println("alertsDetails.size() = " + alertsDetails.size());
                if (alertsDetails != null) {
                    String sMsg = "";
                    try {
                        if (hourList.size() > 0) {
                            int i = 0;
                            String contentData = "";
                            StringBuffer contentSB = new StringBuffer();
                            Iterator itr1 = hourList.iterator();

                            int hrs = (Integer) itr1.next();

                            String aDesc = "Hi,";
                            contentSB.append("<tr>");

                            contentSB.append("<th colspan='20'>POD Not Updated for the below mentioned Trips </th>");

                            contentSB.append("</tr>");
                            contentSB.append("<tr>");
                            contentSB.append("<th>S.No</th>");
                            contentSB.append("<th>VehicleNo</th>");
                            contentSB.append("<th>FleetCenter</th>");
                            contentSB.append("<th>Vehicle Type Name</th>");
                            contentSB.append("<th>Trip Sheet No</th>");
                            contentSB.append("<th>Customer Name</th>");
                            contentSB.append("<th>Customer Code</th>");
                            contentSB.append("<th>Consignment Order No</th>");
                            contentSB.append("<th>Hours passed from trip end</th>");
                            contentSB.append("<th>Origin Report Date&Time</th>");
                            contentSB.append("<th>Destination Report Date&Time</th>");
                            contentSB.append("<th>Vehicle Start Date&Time</th>");
                            contentSB.append("<th>Vehicle End Date&Time</th>");
                            contentSB.append("<th>Billing Type</th>");
                            contentSB.append("<th>Route Information</th>");
                            contentSB.append("<th>Product Information</th>");
                            contentSB.append("<th>Reefer Required</th>");
                            contentSB.append("<th>Total Weitage</th>");
                            contentSB.append("<th>Travel Days</th>");
                            contentSB.append("<th>Revenue</th>");
                            contentSB.append("<th>Expense</th>");
                            contentSB.append("</tr>");
                            Iterator itr = alertsDetails.iterator();

                            while (itr.hasNext()) {
                                schTO = (SchedulerTO) itr.next();
                                i++;
                                contentSB.append("<tr>");
                                contentSB.append("<td>" + i + "</td>");
                                contentSB.append("<td>" + schTO.getVehicleNo() + "</td>");
                                contentSB.append("<td>" + schTO.getFleetCenter() + "</td>");
                                contentSB.append("<td>" + schTO.getVehicleTypeName() + "</td>");
                                contentSB.append("<td>" + schTO.getTripCode() + "</td>");
                                contentSB.append("<td>" + schTO.getCustomerName() + "</td>");
                                contentSB.append("<td>" + schTO.getCustomerCode() + "</td>");
                                contentSB.append("<td>" + schTO.getConsignmentOrderNo() + "</td>");
                                contentSB.append("<td>" + schTO.getDifferenceHour() + "</td>");
                                contentSB.append("<td>" + schTO.getOriginReportingDateTime() + "</td>");
                                contentSB.append("<td>" + schTO.getDestinationReportingDateTime() + "</td>");
                                contentSB.append("<td>" + schTO.getVehicleStartDateTime() + "</td>");
                                contentSB.append("<td>" + schTO.getVehicleEndDateTime() + "</td>");
                                contentSB.append("<td>" + schTO.getBillingType() + "</td>");
                                contentSB.append("<td>" + schTO.getRouteInfo() + "</td>");
                                contentSB.append("<td>" + schTO.getProductInfo() + "</td>");
                                contentSB.append("<td>" + schTO.getReeferRequired() + "</td>");
                                contentSB.append("<td>" + schTO.getTotalWeight() + "</td>");
                                contentSB.append("<td>" + schTO.getTravelDays() + "</td>");
                                contentSB.append("<td>" + schTO.getRevenue() + "</td>");
                                contentSB.append("<td>" + schTO.getExpense() + "</td>");
                                contentSB.append("</tr>");

                            }

                            contentData = "Hi Team, <br><br><table border='1' cellspacing='4' cellpadding='1' width='600px'>" + contentSB.toString() + "</table>";
                            contentData = contentData + "<br><br> TMS Team ";
                            System.out.println(contentData);
//                            out.println(contentData);

//                            recTo = batchUtil.getProperty("mail.recipients.pod.to");
//                            recCc = batchUtil.getProperty("mail.recipients.pod.cc");
                            recTo = "natarajans@entitlesolutions.com";
                            recCc = "natarajans@entitlesolutions.com";
//                           String recBcc = "natarajans@entitlesolutions.com";
                            System.out.println("recipients recTo:  " + recTo);
                            System.out.println("recipients recCc: " + recCc);
                            System.out.println("recipients recBcc: " + recBcc);

                            if (i > 0) {
                                String dueAlt = "Permit Due - as on " + curTime;
                                //    em.send(recTo, recCc, atlMsg, dueAlt, contentData, "Throttle support", "");
                                //String[] temp = recTo.split(",");
                                //for (int z = 0; z < temp.length; z++) {
                                //em.sendTextMail(temp[z], "THROTTLE ALERTS", "Permit Due", contentData,"Throttle support", "");
                                //em.send(temp[z], atlMsg, dueAlt, contentData, "Throttle support", "");
                                //}
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("Exception in email Send Call: " + e.getMessage());
                    }
                }

                System.out.println("Now Going TO Send Trip Planned And Not Trip Freezeed Consignment List");
                alertsDetails = new ArrayList();
                alertsEmail = new ArrayList();
                hourList = new ArrayList();
                // Alert is for Trip Not Freeze
                //int[] freezeHours = {48, 24, 12, 0};
                atlMsg = "THROTTLE ALERTS - Trip Not Freezed For Planned Vehicles as on - " + curTime;

                alertsEmail = schedulerDAO.getTripFreezingEveryTwoHourAlert(0);
                System.out.println("alertsEmail.size() = " + alertsEmail.size());
                System.out.println("alertsEmail trip not freezed:" + alertsEmail.size());
                if (alertsEmail.size() > 0) {
                    alertsDetails.addAll(alertsEmail);
                    hourList.add(0);
                    alertsEmail = new ArrayList();
                }

                System.out.println("alertsDetails.size() = " + alertsDetails.size());
                if (alertsDetails != null) {
                    String sMsg = "";
                    try {
                        if (hourList.size() > 0) {
                            int i = 0;
                            String contentData = "";
                            String recTo1=ThrottleConstants.freezeToMailId;                            
                            String recCc1=ThrottleConstants.freezeCcMailId;                               
                            String recBcc1=ThrottleConstants.freezeBccMailId;                               
                            
                            StringBuffer contentSB = new StringBuffer();
                            Iterator itr1 = hourList.iterator();
                         
                            int hrs = (Integer) itr1.next();
                            String aDesc = "Hi,";
                            contentSB.append("<tr>");
                            //if (hrs == 0) {
                            contentSB.append("<th colspan='13'>Trip is not freezed for these below mentioned consignment orders</th>");
//                                } else {
//                                    contentSB.append("<th colspan='13'>Trip is not freezed for these below mentioned consignment orders about " + hrs + " hours left..</th>");
//                                }
                            contentSB.append("</tr>");
                            contentSB.append("<tr>");
                            contentSB.append("<th>S.No</th>");
                            contentSB.append("<th>Customer Name</th>");
                            contentSB.append("<th>Customer Code</th>");
                            contentSB.append("<th>Consignment Order No</th>");
                            contentSB.append("<th>Customer Order Reference No</th>");
                            contentSB.append("<th>Consignment Order Date</th>");
                            contentSB.append("<th>Trip Sheet No</th>");
                            contentSB.append("<th>Route Name</th>");
                            contentSB.append("<th>Vehicle Type Name</th>");
                            contentSB.append("<th>Vehicle Required Date Time</th>");
                            contentSB.append("<th>Consignment Origin</th>");
                            contentSB.append("<th>Consignment Destination</th>");
                            contentSB.append("<th>Product Category Name</th>");
                            contentSB.append("<th>Reefer Required</th>");
                            contentSB.append("</tr>");
                            Iterator itr = alertsDetails.iterator();
                            while (itr.hasNext()) {
                                schTO = (SchedulerTO) itr.next();
                                String differenceHour = schTO.getDifferenceHour();
                                if (hrs == Integer.parseInt(differenceHour)) {
                                    i++;
                                    contentSB.append("<tr>");
                                    contentSB.append("<td>" + i + "</td>");
                                    contentSB.append("<td>" + schTO.getTripCode() + "</td>");
                                    contentSB.append("<td>" + schTO.getConsignmentOrderNo() + "</td>");
                                    contentSB.append("<td>" + schTO.getCustomerOrderReferenceNo() + "</td>");
                                    contentSB.append("<td>" + schTO.getConsignmentOrderDate() + "</td>");
                                    contentSB.append("<td>" + schTO.getCustomerCode() + "</td>");
                                    contentSB.append("<td>" + schTO.getCustomerName() + "</td>");
                                    contentSB.append("<td>" + schTO.getVehicleTypeName() + "</td>");
                                    contentSB.append("<td>" + schTO.getVehicleRequiredDateTime() + "</td>");
                                    contentSB.append("<td>" + schTO.getOrigin() + "</td>");
                                    contentSB.append("<td>" + schTO.getDestination() + "</td>");
                                    contentSB.append("<td>" + schTO.getProductCategoryName() + "</td>");
                                    contentSB.append("<td>" + schTO.getReeferRequired() + "</td>");
                                    contentSB.append("</tr>");
                                }
                            }

                            contentData = "Hi Team,<table border='1' cellspacing='4' cellpadding='1' width='600px'>" + contentSB.toString() + "</table>";
                            contentData = contentData + "<br><br> TMS Team ";
                            System.out.println(contentData);
                            
                            schTO.setMailStatus("0");
                            schTO.setMailTypeId("2");
                            schTO.setMailSubjectTo("Trip Not Freezed For Planned Vehicles Report");
                            schTO.setMailSubjectCc("Trip Not Freezed For Planned Vehicles Report");
                            schTO.setMailSubjectBcc("Trip Not Freezed For Planned Vehicles Report");
                            schTO.setEmailFormat(contentData);
                            System.out.println("contentData@@@@@@@@@@@@@@@@@@@@@@@" + contentData);
                            schTO.setMailIdTo(recTo1);
                            schTO.setMailIdCc(recCc1);
                            schTO.setMailIdBcc(recBcc1);

                            Integer mails = schedulerDAO.insertMailDetails(schTO, 1);
                            System.out.println("insertMailDetails@@@" + mails);

//                            recTo = batchUtil.getProperty("mail.recipients.notFreezed.to");
//                            recCc = batchUtil.getProperty("mail.recipients.notFreezed.cc");
                            recTo = "natarajans@entitlesolutions.com";
                            recCc = "natarajans@entitlesolutions.com";
//                               String recBcc = "natarajans@entitlesolutions.com";
                            System.out.println("recipients recTo:  " + recTo);
                            System.out.println("recipients recCc: " + recCc);
                            System.out.println("recipients recBcc: " + recBcc);
                            if (i > 0) {
                                String dueAlt = "Permit Due - as on " + curTime;
                                //     em.send(recTo, recCc, atlMsg, dueAlt, contentData, "Throttle support", "");

                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("Exception in email Send Call: " + e.getMessage());
                    }
                }

            } catch (Exception excp) {
                System.out.println("ERROR STATUS   " + excp);
                errorOccured = true;

            }

        } catch (Exception excp) {
            System.out.println("ERROR STATUS   " + excp);

            excp.printStackTrace();
        }
        return dailyProcessMain;
    }

    public ArrayList getProcessMain() throws FPRuntimeException, FPBusinessException {

        ArrayList processMain = new ArrayList();
        boolean returnValue = false;
        String rangeStartDate = "";
        String lastRunTime = "";

        SendEmail em = new SendEmail();
        ArrayList alertsDetails = new ArrayList();
        ArrayList alertsEmail = new ArrayList();
        try {

            try {

                System.out.println("inside DailyProcessMain processAlerts function");
                final Properties properties = new Properties();
                String recTo = "";
                String recCc = "";
              //  String recTo = batchUtil.getProperty("mail.recipients.advance.to");
                // String recCc = batchUtil.getProperty("mail.recipients.cc");
                //  System.out.println("recipients: " + recTo);

                int MILLIS_IN_DAY = 1000 * 60 * 60 * 24 * 7;
                Date date = new Date();
                SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
                SimpleDateFormat processDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                rangeStartDate = processDateFormat.format(date.getTime());

                int uStatus = 0;
                SchedulerTO schTO = new SchedulerTO();

                Date dNow = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("E dd/MM/yyyy 'at' hh:mma");
                //System.out.println("Current Date: " + ft.format(dNow));
                String curDate = ft.format(dNow);
                String atlMsg = "THROTTLE ALERTS - Batch Advance Advice - " + curDate;

                // Email Alert To Permit Dues
//                recTo = batchUtil.getProperty("mail.recipients.permit.to");
//                recCc = batchUtil.getProperty("mail.recipients.permit.cc");
                alertsEmail = schedulerDAO.getPermitDueFullDetails();
                if (alertsDetails != null) {
                    String sMsg = "";
                    try {
                        int i = 0;
                        String recTo1 = ThrottleConstants.permitToMailId;
                        String recCc1 = ThrottleConstants.permitCcMailId;
                        String recBcc1 = ThrottleConstants.permitBccMailId;
                        String aDesc = "Permit Due";
                        String contentData = "";
                        StringBuffer contentSB = new StringBuffer();
                        contentSB.append("<tr>");
                        contentSB.append("<th colspan='6'>PERMIT DUE - as on " + curDate + "</th>");
                        contentSB.append("</tr>");
                        contentSB.append("<tr>");
                        contentSB.append("<th>S.No</th>");
                        contentSB.append("<th>Fleet Center</th>");
                        contentSB.append("<th>Reg No</th>");
                        contentSB.append("<th>Permit Due Date</th>");
                        contentSB.append("<th>Days</th>");

                        contentSB.append("<th>Permit Type</th>");
                        contentSB.append("</tr>");
                        Iterator itr = alertsEmail.iterator();
                        while (itr.hasNext()) {
                            i++;
                            schTO = (SchedulerTO) itr.next();
                            contentSB.append("<tr>");
                            contentSB.append("<td>" + i + "</td>");
                            contentSB.append("<td>" + schTO.getOperationPoint() + "</td>");
                            contentSB.append("<td>" + schTO.getRegNo() + "</td>");
                            contentSB.append("<td>" + schTO.getDueDate() + "</td>");
                            contentSB.append("<td>" + schTO.getDueDays() + "</td>");
                            contentSB.append("<td>" + schTO.getPermitType() + "</td>");
                            contentSB.append("</tr>");
                        }
                        contentData = "Hi Team,<table border='1' cellspacing='4' cellpadding='1' width='600px'>" + contentSB.toString() + "</table>";
                        contentData = contentData + "<br><br> TMS Team ";
                        System.out.println(contentData);

                        schTO.setMailStatus("0");
                        schTO.setMailTypeId("2");
                        schTO.setMailSubjectTo("Permit Due Full Details Report");
                        schTO.setMailSubjectCc("Permit Due Full Details Report");
                        schTO.setMailSubjectBcc("Permit Due Full Details Report");
                        schTO.setEmailFormat(contentData);
                        System.out.println("contentData@@@@@@@@@@@@@@@@@@@@@@@" + contentData);
                        schTO.setMailIdTo(recTo1);
                        schTO.setMailIdCc(recCc1);
                        schTO.setMailIdBcc(recBcc1);

                        Integer mails = schedulerDAO.insertMailDetails(schTO, 1);
                        System.out.println("insertMailDetails@@@" + mails);
                        if (i > 0) {
                            String dueAlt = "Permit Due - as on " + curDate;
//                            em.send(recTo, recCc, atlMsg, dueAlt, contentData, "Throttle support", "");
                            //String[] temp = recTo.split(",");
                            //for (int z = 0; z < temp.length; z++) {
                            //em.sendTextMail(temp[z], "THROTTLE ALERTS", "Permit Due", contentData,"Throttle support", "");
                            //em.send(temp[z], atlMsg, dueAlt, contentData, "Throttle support", "");
                            //}
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("Exception in email Send Call: " + e.getMessage());
                    }
                }

                // Email Alert For Insurance Dues
//                recTo = batchUtil.getProperty("mail.recipients.insurance.to");
//                recCc = batchUtil.getProperty("mail.recipients.insurance.cc");
                atlMsg = "TMS ALERTS - Insurance Dues -" + curDate;
                alertsEmail = schedulerDAO.getInsuranceDueFullDetails();
                if (alertsDetails != null) {
                    String sMsg = "";
                    try {
                        int i = 0;
                        String recTo2 = ThrottleConstants.insuranceToMailId;
                        String recCc2 = ThrottleConstants.insuranceCcMailId;
                        String recBcc2 = ThrottleConstants.insuranceBccMailId;
                        String aDesc = "Insurance Due";
                        String contentData = "";
                        StringBuffer contentSB = new StringBuffer();
                        contentSB.append("<tr>");
                        contentSB.append("<th colspan='5'>INSURANCE DUE - as on " + curDate + "</th>");
                        contentSB.append("</tr>");
                        contentSB.append("<tr>");
                        contentSB.append("<th>S.No</th>");
                        contentSB.append("<th>Fleet Center</th>");
                        contentSB.append("<th>Reg No</th>");
                        contentSB.append("<th>Insurance Due Date</th>");
                        contentSB.append("<th>Days</th>");
                        contentSB.append("</tr>");
                        Iterator itr = alertsEmail.iterator();
                        while (itr.hasNext()) {
                            i++;
                            schTO = (SchedulerTO) itr.next();
                            contentSB.append("<tr>");
                            contentSB.append("<td>" + i + "</td>");
                            contentSB.append("<td>" + schTO.getOperationPoint() + "</td>");
                            contentSB.append("<td>" + schTO.getRegNo() + "</td>");
                            contentSB.append("<td>" + schTO.getDueDate() + "</td>");
                            contentSB.append("<td>" + schTO.getDueDays() + "</td>");
                            contentSB.append("</tr>");
                        }
                        contentData = "Hi Team,<table border='1' cellspacing='4' cellpadding='1' width='600px'>" + contentSB.toString() + "</table>";
                        contentData = contentData + "<br><br> TMS Team ";
                        System.out.println(contentData);
                        
                        schTO.setMailStatus("0");
                        schTO.setMailTypeId("2");
                        schTO.setMailSubjectTo("Insurance Due Full Details Report");
                        schTO.setMailSubjectCc("Insurance Due Full Details Report");
                        schTO.setMailSubjectBcc("Insurance Due Full Details Report");
                        schTO.setEmailFormat(contentData);
                        System.out.println("contentData@@@@@@@@@@@@@@@@@@@@@@@" + contentData);
                        schTO.setMailIdTo(recTo2);
                        schTO.setMailIdCc(recCc2);
                        schTO.setMailIdBcc(recBcc2);

                        Integer mails = schedulerDAO.insertMailDetails(schTO, 1);
                        System.out.println("insertMailDetails@@@" + mails);
                        if (i > 0) {
                            String dueAlt = "Insurance Due - as on " + curDate;
                            //String[] temp = recTo.split(",");
//                            em.send(recTo, recCc, atlMsg, dueAlt, contentData, "Throttle support", "");
                            //for (int z = 0; z < temp.length; z++) {
                            //em.sendTextMail(temp[z], "THROTTLE ALERTS", "Insurance Due" ,contentData,"Throttle support", "");
                            //em.send(temp[z], atlMsg, dueAlt, contentData, "Throttle support", "");
                            //}
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("Exception in email Send Call: " + e.getMessage());
                    }
                }

                // Email Alert For FC Dues
//                recTo = batchUtil.getProperty("mail.recipients.fc.to");
//                recCc = batchUtil.getProperty("mail.recipients.fc.cc");
                atlMsg = "TMS ALERTS - FC Dues -" + curDate;
                alertsEmail = schedulerDAO.getFcDueFullDetails();
                if (alertsDetails != null) {
                    String sMsg = "";
                    try {
                        int i = 0;
                        String recTo3 = ThrottleConstants.fcToMailId;
                        String recCc3 = ThrottleConstants.fcCcMailId;
                        String recBcc3 = ThrottleConstants.fcBccMailId;
                        String aDesc = "FC Due";
                        String contentData = "";
                        StringBuffer contentSB = new StringBuffer();
                        contentSB.append("<tr>");
                        contentSB.append("<th colspan='5'>FC DUE - as on " + curDate + "</th>");
                        contentSB.append("</tr>");
                        contentSB.append("<tr>");
                        contentSB.append("<th>S.No</th>");
                        contentSB.append("<th>Fleet Center</th>");
                        contentSB.append("<th>Reg No</th>");
                        contentSB.append("<th>FC Due Date</th>");
                        contentSB.append("<th>Days</th>");
                        contentSB.append("</tr>");
                        Iterator itr = alertsEmail.iterator();
                        while (itr.hasNext()) {
                            i++;
                            schTO = (SchedulerTO) itr.next();
                            contentSB.append("<tr>");
                            contentSB.append("<td>" + i + "</td>");
                            contentSB.append("<td>" + schTO.getOperationPoint() + "</td>");
                            contentSB.append("<td>" + schTO.getRegNo() + "</td>");
                            contentSB.append("<td>" + schTO.getDueDate() + "</td>");
                            contentSB.append("<td>" + schTO.getDueDays() + "</td>");
                            contentSB.append("</tr>");
                        }
                        contentData = "Hi Team,<table border='1' cellspacing='4' cellpadding='1' width='600px'>" + contentSB.toString() + "</table>";
                        contentData = contentData + "<br><br> TMS Team ";
                        System.out.println(contentData);
                       
                        schTO.setMailStatus("0");
                        schTO.setMailTypeId("2");
                        schTO.setMailSubjectTo("Fc Due Full Details Report");
                        schTO.setMailSubjectCc("Fc Due Full Details Report");
                        schTO.setMailSubjectBcc("Fc Due Full Details Report");
                        schTO.setEmailFormat(contentData);
                        System.out.println("contentData@@@@@@@@@@@@@@@@@@@@@@@" + contentData);
                        schTO.setMailIdTo(recTo3);
                        schTO.setMailIdCc(recCc3);
                        schTO.setMailIdBcc(recBcc3);

                        Integer mails = schedulerDAO.insertMailDetails(schTO, 1);
                        System.out.println("insertMailDetails@@@" + mails);
                        if (i > 0) {
                            String dueAlt = "FC Due - as on " + curDate;
//                            em.send(recTo, recCc, atlMsg, dueAlt, contentData, "Throttle support", "");
                            //String[] temp = recTo.split(",");
                            //for (int z = 0; z < temp.length; z++) {
                            //em.sendTextMail(temp[z], "THROTTLE ALERTS", "FC Due" ,contentData,"Throttle support", "");
                            //em.send(temp[z], atlMsg, dueAlt, contentData, "Throttle support", "");
                            //}
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("Exception in email Send Call: " + e.getMessage());
                    }
                }

                // Email Alert For Road Tax Dues
//                recTo = batchUtil.getProperty("mail.recipients.roadTax.to");
//                recCc = batchUtil.getProperty("mail.recipients.roadTax.cc");
                atlMsg = "TMS ALERTS - Road Tax Dues -" + curDate;
                alertsEmail = schedulerDAO.getRoadTaxDueFullDetails();
                if (alertsDetails != null) {
                    String sMsg = "";
                    try {
                        int i = 0;
                        String recTo4 = ThrottleConstants.roadTaxToMailId;
                        String recCc4 = ThrottleConstants.roadTaxCcMailId;
                        String recBcc4 = ThrottleConstants.roadTaxBccMailId;
                        String aDesc = "Road Tax Due";
                        String contentData = "";
                        StringBuffer contentSB = new StringBuffer();
                        contentSB.append("<tr>");
                        contentSB.append("<th colspan='5'>ROAD TAX DUE - as on " + curDate + "</th>");
                        contentSB.append("</tr>");
                        contentSB.append("<tr>");
                        contentSB.append("<th>S.No</th>");
                        contentSB.append("<th>Fleet Center</th>");
                        contentSB.append("<th>Reg No</th>");
                        contentSB.append("<th>Road Tax Due Date</th>");
                        contentSB.append("<th>Days</th>");
                        contentSB.append("</tr>");
                        Iterator itr = alertsEmail.iterator();
                        while (itr.hasNext()) {
                            i++;
                            schTO = (SchedulerTO) itr.next();
                            contentSB.append("<tr>");
                            contentSB.append("<td>" + i + "</td>");
                            contentSB.append("<td>" + schTO.getOperationPoint() + "</td>");
                            contentSB.append("<td>" + schTO.getRegNo() + "</td>");
                            contentSB.append("<td>" + schTO.getDueDate() + "</td>");
                            contentSB.append("<td>" + schTO.getDueDays() + "</td>");
                            contentSB.append("</tr>");
                        }
                        contentData = "Hi Team,<table border='1' cellspacing='4' cellpadding='1' width='600px'>" + contentSB.toString() + "</table>";
                        contentData = contentData + "<br><br> TMS Team ";
                        System.out.println(contentData);

                        schTO.setMailStatus("0");
                        schTO.setMailTypeId("2");
                        schTO.setMailSubjectTo("RoadTax Due Full Details Report");
                        schTO.setMailSubjectCc("RoadTax Due Full Details Report");
                        schTO.setMailSubjectBcc("RoadTax Due Full Details Report");
                        schTO.setEmailFormat(contentData);
                        System.out.println("contentData@@@@@@@@@@@@@@@@@@@@@@@" + contentData);
                        schTO.setMailIdTo(recTo4);
                        schTO.setMailIdCc(recCc4);
                        schTO.setMailIdBcc(recBcc4);

                        Integer mails = schedulerDAO.insertMailDetails(schTO, 1);
                        System.out.println("insertMailDetails@@@" + mails);

                        if (i > 0) {
                            String dueAlt = "Road Tax Due - as on " + curDate;
//                            em.send(recTo, recCc, atlMsg, dueAlt, contentData, "Throttle support", "");
                            //String[] temp = recTo.split(",");
                            //for (int z = 0; z < temp.length; z++) {
                            //em.sendTextMail(temp[z], "THROTTLE ALERTS", "Road Tax Due" ,contentData,"Throttle support", "");
                            //em.send(temp[z], atlMsg, dueAlt, contentData, "Throttle support", "");
                            //}
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("Exception in email Send Call: " + e.getMessage());
                    }
                }

                // Email Alert For Service Dues
//                recTo = batchUtil.getProperty("mail.recipients.service.to");
//                recCc = batchUtil.getProperty("mail.recipients.service.cc");
                atlMsg = "THROTTLE ALERTS - Service Dues -" + curDate;
                alertsEmail = schedulerDAO.getServiceDueFullDetails();
                if (alertsDetails != null) {
                    String sMsg = "";
                    try {
                        int i = 0;
                        String recTo5 = ThrottleConstants.serviceToMailId;
                        String recCc5 = ThrottleConstants.serviceCcMailId;
                        String recBcc5 = ThrottleConstants.serviceBccMailId;
                        String aDesc = "Service Due";
                        String contentData = "";
                        StringBuffer contentSB = new StringBuffer();
                        contentSB.append("<tr>");
                        contentSB.append("<th colspan='5'>SERVICE DUE - as on " + curDate + "</th>");
                        contentSB.append("</tr>");
                        contentSB.append("<tr>");
                        contentSB.append("<th>S.No</th>");
                        contentSB.append("<th>FleetCenter</th>");
                        contentSB.append("<th>RegNo</th>");
                        contentSB.append("<th>ServiceDues</th>");
                        contentSB.append("</tr>");
                        Iterator itr = alertsEmail.iterator();
                        String[] temp = null;
                        String serviceDues = "";
                        String serviceDuesStr = "";
                        while (itr.hasNext()) {
                            i++;
                            schTO = (SchedulerTO) itr.next();
                            contentSB.append("<tr>");
                            contentSB.append("<td>" + i + "</td>");
                            contentSB.append("<td>" + schTO.getOperationPoint() + "</td>");
                            contentSB.append("<td>" + schTO.getRegNo() + "</td>");
                            serviceDues = schTO.getServiceDues();
                            temp = serviceDues.split("~");
                            serviceDuesStr = "";
                            for (int k = 0; k < temp.length; k++) {
                                serviceDuesStr = serviceDuesStr + temp[k] + "<br>";
                            }

                            contentSB.append("<td>" + serviceDuesStr + "</td>");
                            contentSB.append("</tr>");
                        }
                        contentData = "Hi Team,<table border='1' cellspacing='4' cellpadding='1' width='600px'>" + contentSB.toString() + "</table>";
                        contentData = contentData + "<br><br> TMS Team ";
                        System.out.println(contentData);
                        
                        schTO.setMailStatus("0");
                        schTO.setMailTypeId("2");
                        schTO.setMailSubjectTo("Service Due Full Details Report");
                        schTO.setMailSubjectCc("Service Due Full Details Report");
                        schTO.setMailSubjectBcc("Service Due Full Details Report");
                        schTO.setEmailFormat(contentData);
                        System.out.println("contentData@@@@@@@@@@@@@@@@@@@@@@@" + contentData);
                        schTO.setMailIdTo(recTo5);
                        schTO.setMailIdCc(recCc5);
                        schTO.setMailIdBcc(recBcc5);

                        Integer mails = schedulerDAO.insertMailDetails(schTO, 1);
                        System.out.println("insertMailDetails@@@" + mails);

                        if (i > 0) {
                            String dueAlt = "Service Due - as on " + curDate;
//                            em.send(recTo, recCc, atlMsg, dueAlt, contentData, "Throttle support", "");
                            //String[] temp = recTo.split(",");
                            //for (int z = 0; z < temp.length; z++) {
                            //em.sendTextMail(temp[z], "THROTTLE ALERTS", "Service Due" ,contentData,"Throttle support", "");
                            //em.send(temp[z], atlMsg, dueAlt, contentData, "Throttle support", "");
                            //}
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("Exception in email Send Call: " + e.getMessage());
                    }
                }

            } catch (Exception excp) {
                System.out.println("ERROR STATUS   " + excp);

            }
        } catch (Exception excp) {
            System.out.println("ERROR STATUS   " + excp);

            excp.printStackTrace();
        }
        return processMain;
    }

    
    
    
    public int getDailyVehicleTripAlerts() throws IOException {
        int insertstatus = 0;
        insertstatus = schedulerDAO.getDailyVehicleTripDetails();
        System.out.println("status......:" + insertstatus);
        return insertstatus;

    }

    public int insertMailDetails(SchedulerTO schedulerTO, int userId) throws IOException {
        int insertMailDetails = 0;
        insertMailDetails = schedulerDAO.insertMailDetails(schedulerTO, userId);
        return insertMailDetails;
    }

    // GPS ACTIVITY
   // monthly billed scheduler
   public ArrayList getMonthlyBillingDetails() throws FPRuntimeException, FPBusinessException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        String systemTime = sdf.format(c.getTime()).toString();
        System.out.println("systemTime = " + systemTime);
        String[] dateTemp = systemTime.split(" ");
        String[] timeTemp = dateTemp[1].split(":");
        ArrayList getTotalBillingList = new ArrayList();

//             -------DVM scheduler----------
//       try{
        String rangeStartDate = "";
        String rangeStartDate1 = "";
        String Filename = "";
        String File_Name = "";
        String contentdata = "";
        String graphSheet = "";
        StringBuffer contentSB = new StringBuffer();
        SendEmail em = new SendEmail();
        ArrayList alertsDetails = new ArrayList();
        ArrayList alertsEmail = new ArrayList();

        File theDir = new File("c:\\MonthlyBillingDetails");

        if (!theDir.exists()) {
            boolean result = theDir.mkdir();
            if (result) {
                System.out.println("c:\\MonthlyBillingDetails\\DIR created");
            }

        }

        try {

            int i = 0;

            System.out.println("inside processAlerts function");
            final Properties properties = new Properties();
            String recTo = ThrottleConstants.mothlyBillingToMailId;
            String recCc = ThrottleConstants.mothlyBillingCcMailId;
            String recBcc = ThrottleConstants.mothlyBillingBccMailId;
            System.out.println("recipients recTo:  " + recTo);
            System.out.println("recipients recCc: " + recCc);
            System.out.println("recipients recBcc: " + recBcc);

            Date dNow1 = new Date();
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            SimpleDateFormat processDateFormat11 = new SimpleDateFormat("yyyy-MM-dd");
            String dt = dateFormat1.format(dNow1).toString();
            String substr = dt.substring(11, 13);

            int MILLIS_IN_DAY = 1000 * 60 * 60 * 24 * 7;
//                Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
            SimpleDateFormat processDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat processDateFormat1 = new SimpleDateFormat("dd-MM-yyyy");
            rangeStartDate1 = processDateFormat1.format(date.getTime());
            System.out.println("rangeStartDate1@@@" + rangeStartDate1);
            rangeStartDate = processDateFormat.format(date.getTime());
            System.out.println("rangeStartDate@@@" + rangeStartDate);
            System.out.println("recipients: " + rangeStartDate);

            int uStatus = 0;
            SchedulerTO schTO = null;

            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("hh:mm:a");
            SimpleDateFormat fHour = new SimpleDateFormat("hh");
            System.out.println("Current Date: " + ft.format(dNow));
            System.out.println("Current Hour: " + fHour.format(dNow));
            String curTime = ft.format(dNow);
            String[] temp = null;
            temp = curTime.split(":");
            int curHour = Integer.parseInt(fHour.format(dNow));
            System.out.println("curTime = " + curTime);
            System.out.println("curHour = " + curHour);
            String curMeridian = temp[2];
            System.out.println("curMeridian = " + curMeridian);

            int time = 0;
            int everyHourStatus = 0;
            int oddEven = curHour % 2;
            if (oddEven > 0) {
                everyHourStatus = 0;
            } else {
                everyHourStatus = 1;
            }

            String timeNow = temp[0] + "_" + temp[1] + "_" + temp[2];
            String filename = "c:\\MonthlyBillingDetails\\" + "Monthly_Bill_Summary_" + rangeStartDate1 + ".xls";
            System.out.println("filename = " + filename);
//                ArrayList getTotalOrderList = new ArrayList();
            ArrayList resultVehicle1 = new ArrayList();
            ArrayList resultVehicle2 = new ArrayList();
            ArrayList resultVehicle3 = new ArrayList();
            ArrayList result = new ArrayList();
            ArrayList result1 = new ArrayList();
            ArrayList result1V = new ArrayList();
            ArrayList result2 = new ArrayList();
            ArrayList result2V = new ArrayList();
            ArrayList result3 = new ArrayList();
            ArrayList result3V = new ArrayList();
            ArrayList vehicleTripOwnCount = new ArrayList();
            ArrayList vehicleTripLeasedCount = new ArrayList();

            getTotalBillingList = schedulerDAO.getTotalBillingList();
            System.out.println("getTotalBillingList.size() = " + getTotalBillingList.size());
//                Iterator itrVehicle = getTotalBillingList.iterator();
            schTO = null;
//                while (itrVehicle.hasNext()) {
//                    schTO = (SchedulerTO) itrVehicle.next();
//                    String consignmentOrderId = schTO.getConsignmentOrderId();
//                    System.out.println("itrVehicle = " + schTO.getConsignmentOrderId());
//                    System.out.println("itrVehicle1@@@ = " + consignmentOrderId);
            String ownBillingList = "1";
            resultVehicle1 = schedulerDAO.getVehicleNos(ownBillingList);
            result1 = schedulerDAO.getBillingList(ownBillingList);
            System.out.println("result1 = " + result1.size());
            System.out.println("resultVehicle1 = " + resultVehicle1.size());

            result1.addAll(result1);
            result1V.addAll(resultVehicle1);
            System.out.println("ownBillingList = " + result1.size());

            String leasedBillingList = "2";
            resultVehicle2 = schedulerDAO.getVehicleNos(leasedBillingList);
            result2 = schedulerDAO.getBillingList(leasedBillingList);
            System.out.println("result2 = " + result2.size());

            result2.addAll(result2);
            result2V.addAll(resultVehicle2);
            System.out.println("result2.size() = " + result2.size());

            String marketBillingList = "3";
            resultVehicle3 = schedulerDAO.getVehicleNos(marketBillingList);
            result3 = schedulerDAO.getBillingList(marketBillingList);

            result3.addAll(result3);
            result3V.addAll(resultVehicle3);
            System.out.println("result3.size() = " + result3V.size());



            //first my_sheet start
            HSSFWorkbook my_workbook = new HSSFWorkbook();
            String sheetName = "Own" + rangeStartDate1;
            Sheet my_sheet = (Sheet) my_workbook.createSheet(sheetName);
//            Row s1Row1s = my_sheet.createRow((short) 0);
//            s1Row1s.setHeightInPoints(40); // row hight

            Row s1Row1 = my_sheet.createRow((short) 0);
            s1Row1.setHeightInPoints(20); // row hight



            // Border and Cell Color
            HSSFCellStyle style = my_workbook.createCellStyle();
            style.setFillForegroundColor(HSSFColor.CORNFLOWER_BLUE.index);
            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            style.setRightBorderColor(IndexedColors.BLACK.getIndex());
            style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            style.setTopBorderColor(IndexedColors.BLACK.getIndex());
            style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            HSSFFont font = my_workbook.createFont();
            font.setFontName(HSSFFont.FONT_ARIAL);
            font.setFontHeightInPoints((short) 10);
            font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            font.setColor(HSSFColor.BLACK.index);
            style.setFont(font);

            HSSFCellStyle style1 = my_workbook.createCellStyle();
            style1.setFillForegroundColor(HSSFColor.WHITE.index);
            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
            style1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            style1.setBorderTop(HSSFCellStyle.BORDER_THIN);
            style1.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            style1.setRightBorderColor(IndexedColors.BLACK.getIndex());
            style1.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            style1.setTopBorderColor(IndexedColors.BLACK.getIndex());
            style1.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            HSSFFont font1= my_workbook.createFont();
            font1.setFontName(HSSFFont.FONT_ARIAL);
            font1.setFontHeightInPoints((short) 10);
            font1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            font1.setColor(HSSFColor.BLACK.index);
            style1.setFont(font1);

            Cell cellc1 = s1Row1.createCell((short) 0);
            cellc1.setCellValue("S.No");
            cellc1.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 0, (short) 2000); // cell width
            Cell cellc2 = s1Row1.createCell((short) 1);
            cellc2.setCellValue("Vehicle No");
            cellc2.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 1, (short) 4000); // cell width

            Cell cellc3 = s1Row1.createCell((short) 2);
            cellc3.setCellValue("");
//                cellc3.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 2, (short) 4000); // cell width

//

            Cell cellc4 = s1Row1.createCell((short) 3);
            cellc4.setCellValue("S.No");
            cellc4.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 3, (short) 2000); // cell width

            Cell cellc5 = s1Row1.createCell((short) 4);
            cellc5.setCellValue("Trip Code");
            cellc5.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 4, (short) 4000); // cell width

            Cell cellc6 = s1Row1.createCell((short) 5);
            cellc6.setCellValue("vehicle No");
            cellc6.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 5, (short) 4000); // cell width

            Cell cellc7 = s1Row1.createCell((short) 6);
            cellc7.setCellValue("Start Date");
            cellc7.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 6, (short) 4000); // cell width

            Cell cellc8 = s1Row1.createCell((short) 7);
            cellc8.setCellValue("End Date");
            cellc8.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 7, (short) 4000); // cell width

//                result = paDBHelper.getWatingForLoading();
            Iterator itrV = result1V.iterator();
            Iterator itr = result1.iterator();

            schTO = null;
            int cntrV = 1;
            int cntr = 1;
            int order_closed = 0;
            int job_Dedicate = 0;
            int job_General = 0;
            int order_end = 0;
            int order_progress = 0;
            int order_freezed = 0;
            int order_created = 0;
//                contentSB.append("<tr>");
//                contentSB.append("<th colspan='6'>WFL Ageing Status</th>");
//                contentSB.append("</tr>");
//                contentSB.append("<tr>");
//                contentSB.append("<td>Sno</td>");
//                contentSB.append("<td>Vehicle No</td>");
//                contentSB.append("<td>Fleet Center</td>");
//                contentSB.append("<td>Location</td>");
//                contentSB.append("<td>No Of Hours</td>");
//                contentSB.append("<td>No Of Days</td>");
//                contentSB.append("</tr>");
            Integer resultsV = result1V.size();
            Integer results = result1.size();
            Integer resultAdd = resultsV + results;


            //
            int vehicleCount1 = 0,tripCount1 = 0;

            for(int x=0,y=0;x<result1V.size() || y<result1.size();x++,y++){
                s1Row1 = my_sheet.createRow((short) cntrV);
                 while (itrV.hasNext()) {
                schTO = (SchedulerTO) itrV.next();
                s1Row1.createCell((short) 0).setCellValue(cntrV);
                s1Row1.createCell((short) 1).setCellValue(schTO.getVehicleNo());
                s1Row1.createCell((short) 2).setCellValue("");
                vehicleCount1++;
               break;
            }


            while (itr.hasNext()) {
                schTO = (SchedulerTO) itr.next();
                s1Row1.createCell((short) 3).setCellValue(cntrV);
                s1Row1.createCell((short) 4).setCellValue(schTO.getTripCode());
                s1Row1.createCell((short) 5).setCellValue(schTO.getVehicleNo());
                s1Row1.createCell((short) 6).setCellValue(schTO.getStartDate());
                s1Row1.createCell((short) 7).setCellValue(schTO.getEndDate());
                tripCount1++;
                break;
            }


                cntrV++;
            }
            //

            System.out.println("vehicle count:"+vehicleCount1);
            System.out.println("trip  count:"+tripCount1);

////                    }
//
//                }
            System.out.println("Your excel 1 Sheet  created");
                //1 my_sheet end


           //2 my_sheet start
           sheetName = "Leased" + rangeStartDate1;
            Sheet my_sheet1 = (Sheet) my_workbook.createSheet(sheetName);

            Row s1Row2 = my_sheet1.createRow((short) 0);
            s1Row2.setHeightInPoints(20); // row hight

            Cell cellfs = s1Row2.createCell((short) 0);
            cellfs.setCellValue("S.No");
            cellfs.setCellStyle((CellStyle) style);
            my_sheet1.setColumnWidth((short) 0, (short) 2000); // cell width

            Cell cellf1s = s1Row2.createCell((short) 1);
            cellf1s.setCellValue("Vehicle No");
            cellf1s.setCellStyle((CellStyle) style);
            my_sheet1.setColumnWidth((short) 1, (short) 4000); // cell width

            Cell cellf2s = s1Row2.createCell((short) 2);
            cellf2s.setCellValue("");
//            cellf1s.setCellStyle((CellStyle) style);
            my_sheet1.setColumnWidth((short) 2, (short) 4000); // cell width

            Cell cellf1 = s1Row2.createCell((short) 3);
            cellf1.setCellValue("S.No");
            cellf1.setCellStyle((CellStyle) style);
            my_sheet1.setColumnWidth((short) 3, (short) 2000); // cell width
            Cell cellf2 = s1Row2.createCell((short) 4);
            cellf2.setCellValue("Trip Code");
            cellf2.setCellStyle((CellStyle) style);
            my_sheet1.setColumnWidth((short) 4, (short) 4000); // cell width
            Cell cellf3 = s1Row2.createCell((short) 5);
            cellf3.setCellValue("Vehicle No");
            cellf3.setCellStyle((CellStyle) style);
            my_sheet1.setColumnWidth((short) 5, (short) 4000); // cell width
            Cell cellf4 = s1Row2.createCell((short) 6);
            cellf4.setCellValue("Start Date");
            cellf4.setCellStyle((CellStyle) style);
            my_sheet1.setColumnWidth((short) 6, (short) 4000); // cell width
            Cell cellf5 = s1Row2.createCell((short) 7);
            cellf5.setCellValue("End Date");
            cellf5.setCellStyle((CellStyle) style);
            my_sheet1.setColumnWidth((short) 7, (short) 4000); // cell width


            //result = paDBHelper.getWatingForUnLoading();
            Iterator itr2V = result2V.iterator();
            Iterator itr2 = result2.iterator();
            schTO = null;
            cntr = 1;
            int vehicleCount2=0, tripCount2=0;
            for(int x=0,y=0;x<result2V.size() || y<result2.size();x++,y++){
                s1Row2 = my_sheet1.createRow((short) cntr);
                 while (itr2V.hasNext()) {
                schTO = (SchedulerTO) itr2V.next();
                s1Row2.createCell((short) 0).setCellValue(cntr);
                s1Row2.createCell((short) 1).setCellValue(schTO.getVehicleNo());
                s1Row2.createCell((short) 2).setCellValue("");
                vehicleCount2++;
               break;
            }


            while (itr2.hasNext()) {
                schTO = (SchedulerTO) itr2.next();
                s1Row2.createCell((short) 3).setCellValue(cntr);
                s1Row2.createCell((short) 4).setCellValue(schTO.getTripCode());
                s1Row2.createCell((short) 5).setCellValue(schTO.getVehicleNo());
                s1Row2.createCell((short) 6).setCellValue(schTO.getStartDate());
                s1Row2.createCell((short) 7).setCellValue(schTO.getEndDate());
                tripCount2++;
                break;
            }


                cntr++;
            }
            System.out.println("Your excel 2 Sheet  created");
            System.out.println("vehicle count:"+vehicleCount2);
            System.out.println("trip  count:"+tripCount2);
//                //2 my_sheet end

             //3 my_sheet start
           sheetName = "Market" + rangeStartDate1;
            Sheet my_sheet2 = (Sheet) my_workbook.createSheet(sheetName);
//            my_sheet2.getNumberOfCells();

            Row s1Row3 = my_sheet2.createRow((short) 0);
            s1Row3.setHeightInPoints(20); // row hight

            Cell cellds = s1Row3.createCell((short) 0);
            cellds.setCellValue("S.No");
            cellds.setCellStyle((CellStyle) style);
            my_sheet2.setColumnWidth((short) 0, (short) 2000); // cell width

            Cell celld1s = s1Row3.createCell((short) 1);
            celld1s.setCellValue("Vehicle No");
            celld1s.setCellStyle((CellStyle) style);
            my_sheet2.setColumnWidth((short) 1, (short) 4000); // cell width

            Cell celld2s = s1Row3.createCell((short) 2);
            celld2s.setCellValue("");
//            cellf1s.setCellStyle((CellStyle) style);
            my_sheet2.setColumnWidth((short) 2, (short) 4000); // cell width

            Cell celld1 = s1Row3.createCell((short) 3);
            celld1.setCellValue("S.No");
            celld1.setCellStyle((CellStyle) style);
            my_sheet2.setColumnWidth((short) 3, (short) 2000); // cell width
            Cell celld2 = s1Row3.createCell((short) 4);
            celld2.setCellValue("Trip Code");
            celld2.setCellStyle((CellStyle) style);
            my_sheet2.setColumnWidth((short) 4, (short) 4000); // cell width
            Cell celld3 = s1Row3.createCell((short) 5);
            celld3.setCellValue("Vehicle No");
            celld3.setCellStyle((CellStyle) style);
            my_sheet2.setColumnWidth((short) 5, (short) 4000); // cell width
            Cell celld4 = s1Row3.createCell((short) 6);
            celld4.setCellValue("Start Date");
            celld4.setCellStyle((CellStyle) style);
            my_sheet2.setColumnWidth((short) 6, (short) 4000); // cell width
            Cell celld5 = s1Row3.createCell((short) 7);
            celld5.setCellValue("End Date");
            celld5.setCellStyle((CellStyle) style);
            my_sheet2.setColumnWidth((short) 7, (short) 4000); // cell width
            Cell celld6 = s1Row3.createCell((short) 8);
            celld6.setCellValue("Trip Days");
            celld6.setCellStyle((CellStyle) style);
            my_sheet2.setColumnWidth((short) 8, (short) 4000); // cell width


            //result = paDBHelper.getWatingForUnLoading();
            Iterator itr3V = result3V.iterator();
            Iterator itr3 = result3.iterator();
            schTO = null;
            int cntr3 = 1;
            int vehicleCount3= 0,tripCount3 = 0;
            int tripCounts = 0;
            int tripDays = 0;
            int totTripDays =0;
            for(int x=0,y=0;x<result3V.size() || y<result3.size();x++,y++){
                s1Row3 = my_sheet2.createRow((short) cntr3);
                 while (itr3V.hasNext()) {
                schTO = (SchedulerTO) itr3V.next();
                s1Row3.createCell((short) 0).setCellValue(cntr3);
                s1Row3.createCell((short) 1).setCellValue(schTO.getVehicleNo());
                s1Row3.createCell((short) 2).setCellValue("");
                vehicleCount3++;
               break;
            }


            while (itr3.hasNext()) {
                schTO = (SchedulerTO) itr3.next();
                s1Row3.createCell((short) 3).setCellValue(cntr3);
                s1Row3.createCell((short) 4).setCellValue(schTO.getTripCode());
                s1Row3.createCell((short) 5).setCellValue(schTO.getVehicleNo());
                s1Row3.createCell((short) 6).setCellValue(schTO.getStartDate());
                s1Row3.createCell((short) 7).setCellValue(schTO.getEndDate());
                totTripDays =Integer.parseInt(schTO.getTripDays());
                s1Row3.createCell((short) 8).setCellValue(totTripDays);
//                tripDays= 0;
//                tripDays= schTO.getTripDays();
                tripDays = tripDays + Integer.parseInt(schTO.getTripDays());
                tripCount3++;
                tripCounts = tripCount3 + 1;
                break;
            }

                cntr3++;
            }
//            if(tripCounts < 250){
//                Row s1Rows3 = my_sheet2.createRow((short) tripCounts);
//                s1Rows3.setHeightInPoints(20); // row hight
//
//                System.out.println("tripCounts:"+tripCounts);
//                s1Rows3.createCell((short) tripCounts);
//                Cell celld4f4 = s1Rows3.createCell((short) 7);
//                celld4f4.setCellValue("TOTAL");
//                celld4f4.setCellStyle((CellStyle) style);
//
//                s1Rows3.createCell((short) tripCounts);
//                Cell celld5f4 = s1Rows3.createCell((short) 8);
//                celld5f4.setCellValue(tripDays);
//                celld5f4.setCellStyle((CellStyle) style);
//
//                System.out.println("tripDays:"+tripDays);
//                System.out.println("tripCount3:"+tripCount3);
//                System.out.println("tripCounts:"+tripCounts);
////                s1Row3.createCell((short) 10).setCellValue(totTrip);
//             System.out.println("vehicle count3:"+vehicleCount3);
//            System.out.println("trip  count3:"+tripCount3);
//            System.out.println("Your excel 3 Sheet  created");
////                //3 my_sheet end
//            }else if(tripCounts > 250){
//
//                Cell celld7 = s1Row3.createCell((short) 11);
//                celld7.setCellValue("Total Trip Days");
//                celld7.setCellStyle((CellStyle) style);
//                my_sheet2.setColumnWidth((short) 11, (short) 6000); // cell width
//
//                Row s1Rowss3 = my_sheet2.createRow((short) 2);
//                Cell celld5f5 = s1Rowss3.createCell((short) 11);
//                celld5f5.setCellValue(tripDays);
//
//
//            }

//             //4 my_sheet start
            sheetName = "Summary" + rangeStartDate1;
            Sheet my_sheet3 = (Sheet) my_workbook.createSheet(sheetName);


            Row s1Row4 = my_sheet3.createRow((short) 6);
            s1Row4.setHeightInPoints(40); // row hight

            Row s1Row5 = my_sheet3.createRow((short) 7);
            s1Row5.setHeightInPoints(20); // row hight

            Row s1Row6 = my_sheet3.createRow((short) 8);
            s1Row6.setHeightInPoints(20); // row hight

            Row s1Row7 = my_sheet3.createRow((short) 9);
            s1Row7.setHeightInPoints(20); // row hight

            Row s1Row8 = my_sheet3.createRow((short) 10);
            s1Row8.setHeightInPoints(20); // row hight

            Cell celles = s1Row4.createCell((short) 1);
            celles.setCellValue("OwnerShip");
            celles.setCellStyle((CellStyle) style);
            my_sheet3.setColumnWidth((short) 1, (short) 6000); // cell width

            Cell celleo1s = s1Row4.createCell((short) 2);
            celleo1s.setCellValue("No of Vehicles");
            celleo1s.setCellStyle((CellStyle) style);
            my_sheet3.setColumnWidth((short) 2, (short) 4000); // cell width

            Cell celleo11s = s1Row4.createCell((short) 3);
            celleo11s.setCellValue("No of Trips");
            celleo11s.setCellStyle((CellStyle) style);
            my_sheet3.setColumnWidth((short) 3, (short) 4000); // cell width

            Cell celleo111s = s1Row4.createCell((short) 4);
            celleo111s.setCellValue("Trip Days");
            celleo111s.setCellStyle((CellStyle) style);
            my_sheet3.setColumnWidth((short) 4, (short) 4000); // cell width

            Cell celleo1111s = s1Row4.createCell((short) 5);
            celleo1111s.setCellValue("No of Vehicles for Billing");
            celleo1111s.setCellStyle((CellStyle) style);
            my_sheet3.setColumnWidth((short) 5, (short) 6000); // cell width

            Cell celle5s = s1Row5.createCell((short) 1);
            celle5s.setCellValue("OWN VEHICLE");
            celle5s.setCellStyle((CellStyle) style1);
            my_sheet3.setColumnWidth((short) 1, (short) 6000); // cell width

            Cell celleo5s = s1Row5.createCell((short) 2);
            celleo5s.setCellValue(vehicleCount1);
            celleo5s.setCellStyle((CellStyle) style1);
            my_sheet3.setColumnWidth((short) 1, (short) 4000); // cell width

            Cell celleo55s = s1Row5.createCell((short) 3);
            celleo55s.setCellValue(tripCount1);
            celleo55s.setCellStyle((CellStyle) style1);
            my_sheet3.setColumnWidth((short) 3, (short) 4000); // cell width

            Cell celleo555s = s1Row5.createCell((short) 4);
            celleo555s.setCellValue("");
            celleo555s.setCellStyle((CellStyle) style1);
            my_sheet3.setColumnWidth((short) 4, (short) 4000); // cell width

            Cell celleo5555s = s1Row5.createCell((short) 5);
            celleo5555s.setCellValue(vehicleCount1);
            celleo5555s.setCellStyle((CellStyle) style1);
            my_sheet3.setColumnWidth((short) 5, (short) 6000); // cell width


            Cell celle6s = s1Row6.createCell((short) 1);
            celle6s.setCellValue("LEASING VEHICLE");
            celle6s.setCellStyle((CellStyle) style1);
            my_sheet3.setColumnWidth((short) 1, (short) 6000); // cell width

            Cell celleo6s = s1Row6.createCell((short) 2);
            celleo6s.setCellValue(vehicleCount2);
            celleo6s.setCellStyle((CellStyle) style1);
            my_sheet3.setColumnWidth((short) 2, (short) 4000); // cell width

            Cell celleo66s = s1Row6.createCell((short) 3);
            celleo66s.setCellValue(tripCount2);
            celleo66s.setCellStyle((CellStyle) style1);
            my_sheet3.setColumnWidth((short) 3, (short) 4000); // cell width

            Cell celleo666s = s1Row6.createCell((short) 4);
            celleo666s.setCellValue("");
            celleo666s.setCellStyle((CellStyle) style1);
            my_sheet3.setColumnWidth((short) 4, (short) 4000); // cell width

            Cell celleo6666s = s1Row6.createCell((short) 5);
            celleo6666s.setCellValue(vehicleCount2);
            celleo6666s.setCellStyle((CellStyle) style1);
            my_sheet3.setColumnWidth((short) 5, (short) 6000); // cell width

            Cell celle7s = s1Row7.createCell((short) 1);
            celle7s.setCellValue("MARKET VEHICLE");
            celle7s.setCellStyle((CellStyle) style1);
            my_sheet3.setColumnWidth((short) 1, (short) 6000); // cell width

            Cell celleo7s = s1Row7.createCell((short) 2);
            celleo7s.setCellValue(vehicleCount3);
            celleo7s.setCellStyle((CellStyle) style1);
            my_sheet3.setColumnWidth((short) 2, (short) 4000); // cell width

            Cell celleo77s = s1Row7.createCell((short) 3);
            celleo77s.setCellValue(tripCount3);
            celleo77s.setCellStyle((CellStyle) style1);
            my_sheet3.setColumnWidth((short) 3, (short) 4000); // cell width

            Cell celleo777s = s1Row7.createCell((short) 4);
            celleo777s.setCellValue(tripDays);
            celleo777s.setCellStyle((CellStyle) style1);
            my_sheet3.setColumnWidth((short) 4, (short) 4000);// cell width
            int marketVehicleCount=0;
            marketVehicleCount= tripDays/24;
            System.out.println("marketVehicleCount"+marketVehicleCount);
            Cell celleo7777s = s1Row7.createCell((short) 5);
            celleo7777s.setCellValue(marketVehicleCount);
            celleo7777s.setCellStyle((CellStyle) style1);
            my_sheet3.setColumnWidth((short) 5, (short) 6000); // cell width


            Cell celleo8s = s1Row8.createCell((short) 4);
            celleo8s.setCellValue("Total");
            celleo8s.setCellStyle((CellStyle) style);
            my_sheet3.setColumnWidth((short) 4, (short) 6000); // cell width

            Cell celleo88s = s1Row8.createCell((short) 5);
            celleo88s.setCellValue(vehicleCount1+vehicleCount2+marketVehicleCount);
            celleo88s.setCellStyle((CellStyle) style1);
            my_sheet3.setColumnWidth((short) 5, (short) 6000); // cell width


            System.out.println("Your excel 4 Sheet  created");
//                //4 my_sheet end
//

                 my_workbook.setSheetOrder("Summary" + rangeStartDate1, 0);
                 my_workbook.setSheetOrder("Own" + rangeStartDate1, 1);
                 my_workbook.setSheetOrder("Leased" + rangeStartDate1, 2);
                 my_workbook.setSheetOrder("Market" + rangeStartDate1, 3);
//                 my_workbook.setSheetOrder("Order_End" + rangeStartDate1, 4);
//                 my_workbook.setSheetOrder("Order_Closed" + rangeStartDate1, 5);
            System.out.println("testing in the excel sheet creation");
            FileOutputStream fileOut = new FileOutputStream(filename);
            my_workbook.write(fileOut);
            fileOut.close();



//            recBcc = "";
//            String atlMsg = "TMS ALERTS - Monthly_Bill_Summary as on - " + rangeStartDate1;
//            String dueAlt = "Monthly_Bill_Summary alert as on " + rangeStartDate1;
//            contentdata = "Hi Greetings From TMS, <br><br>";
//            contentdata = contentdata + "<table border='1' cellspacing='4' cellpadding='1' width='600px'>" + contentSB.toString() + "</table>";
//            contentdata = contentdata + "<br><br> Team Throttle";
            //  em.send(recTo, recCc, atlMsg, dueAlt, contentdata, "TMS support", "");



//            Filename = "Monthly_Bill_Summary_" + rangeStartDate1 + " " + ft.format(dNow) + ".xls";
//            System.out.println(Filename);
//            String bcc = "";
//            String mailContent = "Monthly_Bill_Summary_" + rangeStartDate1 + " " + ft.format(dNow);
//            System.out.println("entered");
////            em.sendExcelMail(Filename, recTo, recCc, recBcc, mailContent, my_workbook);
//            System.out.println("closed");
//            contentdata = "";

            System.out.println("Your excel file has been generated!");
            File_Name = "c:\\MonthlyBillingDetails\\" + "Monthly_Bill_Summary_" + rangeStartDate1 + ".xls";
            System.out.println(File_Name);

            Filename = "Monthly_Bill_Summary_" + rangeStartDate1 + " " + ft.format(dNow) + ".xls";
            System.out.println(Filename);
            String bcc = "";
            String mailContent = "DICT_Monthly_Bill_Summary Alerts" + rangeStartDate1 + " " + ft.format(dNow);
            System.out.println("entered");
//            String atlMsg = "DICT_Monthly_Bill_Summary alert as on - " + rangeStartDate1;
//            String dueAlt = "Order Summary alert as on " + rangeStartDate1;
//            contentdata = "Hi Greetings From TMS, <br><br>";
//            contentdata = contentdata + "<table border='1' cellspacing='4' cellpadding='1' width='600px'>" + contentSB.toString() + "</table>";
//            contentdata = contentdata + "<br><br> Team Throttle";
            //  em.send(recTo, recCc, atlMsg, dueAlt, contentdata, "TMS support", "");
            String emailFormat1= "";
            String emailFormat2= "";
            emailFormat1 = "<html>"
                    + "<body>"
                    + "<p style='font-style: italic;'><b>Dear Sir/Madam, <br><br>Please find below Attatchement of Monthly Bill Summary alert<br><br/>";
            emailFormat2 = emailFormat1 + "<br><br>Hi Greetings From TMS"
                    + "</body></html>";

            SchedulerTO schTO1 = new SchedulerTO();
            schTO1.setMailStatus("1");
            schTO1.setMailTypeId("2");
            schTO1.setMailIdTo(recTo);
            schTO1.setMailIdCc(recCc);
            schTO1.setMailIdBcc(recBcc);
            schTO1.setMailSubjectTo("Monthly Billing Summary Report");
            schTO1.setMailSubjectCc("Monthly Billing Summary Report");
            schTO1.setMailSubjectBcc("Monthly Billing Summary Report");
            schTO1.setFilenameContent(File_Name);
            schTO1.setEmailFormat(emailFormat2);
            System.out.println("filenameContent" + Filename);
            System.out.println("emailFormat4" + contentdata);


            Integer mails =schedulerDAO.insertMailDetails(schTO1,1);
            System.out.println("insertMailDetails@@@"+mails);

            em.sendExcelMail(Filename, recTo, recCc, recBcc, mailContent,emailFormat2, my_workbook);

        } catch (Exception excp) {
            System.out.println("ERROR STATUS   " + excp);

            excp.printStackTrace();
        }
        return getTotalBillingList;
    }

    public ArrayList getEligibleTripForStartEmail() throws FPBusinessException, FPRuntimeException {
        ArrayList eligibleTripForStartEmail = new ArrayList();
        eligibleTripForStartEmail = schedulerDAO.getEligibleTripForStartEmail();
        return eligibleTripForStartEmail;
    }
    public ArrayList getEligibleTripForEndEmail() throws FPBusinessException, FPRuntimeException {
        ArrayList eligibleTripForEndEmail = new ArrayList();
        eligibleTripForEndEmail = schedulerDAO.getEligibleTripForEndEmail();
        return eligibleTripForEndEmail;
    }
    public ArrayList getLatLong(String vehicleNo) throws FPBusinessException, FPRuntimeException {
        ArrayList latlong = new ArrayList();
        latlong = schedulerDAO.getLatLong(vehicleNo);
        return latlong;
    }
    public ArrayList getCityLatLong(String pointId) throws FPBusinessException, FPRuntimeException {
        ArrayList latlong = new ArrayList();
        latlong = schedulerDAO.getCityLatLong(pointId);
         return latlong;
    }
   
    public ArrayList sendStartEmail(String tripId,String duration,String flag) throws FPRuntimeException, FPBusinessException {

        ArrayList processMain = new ArrayList();
        boolean returnValue = false;
        String rangeStartDate = "";
        String lastRunTime = "";

        SendEmail em = new SendEmail();
        ArrayList alertsDetails = new ArrayList();
        ArrayList alertsEmail = new ArrayList();
        try {

            try {

                System.out.println("inside sendStartEmail function");
                final Properties properties = new Properties();
                String recTo = "";
                String recCc = "";
              //  String recTo = batchUtil.getProperty("mail.recipients.advance.to");
                // String recCc = batchUtil.getProperty("mail.recipients.cc");
                //  System.out.println("recipients: " + recTo);

                int MILLIS_IN_DAY = 1000 * 60 * 60 * 24 * 7;
                Date date = new Date();
                SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
                SimpleDateFormat processDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                rangeStartDate = processDateFormat.format(date.getTime());
             //   long hourDuration=(Long.parseLong(duration)/3600);
                int uStatus = 0;
                SchedulerTO schTO = new SchedulerTO();

                Date dNow = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("E dd/MM/yyyy 'at' hh:mma");
                //System.out.println("Current Date: " + ft.format(dNow));
                String curDate = ft.format(dNow);
                String atlMsg = "THROTTLE ALERTS - Batch Advance Advice - " + curDate;
                  String containerNo="";
                // Email Alert To Permit Dues
//                recTo = batchUtil.getProperty("mail.recipients.permit.to");
//                recCc = batchUtil.getProperty("mail.recipients.permit.cc");
                  String to=schedulerDAO.getCustomerEmailId(tripId);
                alertsEmail = schedulerDAO.getStartEmailDetails(tripId);
                if (alertsDetails != null) {
                    String sMsg = "";
                    try {
                        int i = 0;
                        String recTo1 = to;
                        String recCc1 = ThrottleConstants.tripStatNotificationCCMailId;
                        String recBcc1 = "";
                        
                        String contentData = "";
                        StringBuffer contentSB = new StringBuffer();
                        contentSB.append("<tr>");
                        contentSB.append("<th colspan='2'>Trip Details</th>");
                        contentSB.append("</tr>");
                        Iterator itr = alertsEmail.iterator();
                        while (itr.hasNext()) {
                            i++;
                            schTO = (SchedulerTO) itr.next();
                            containerNo=schTO.getContainerNo();
                            String operationType="",ETADate="",ETATime="",travelHours="",travelMin="";
                            String containerType="";
                            String containerType1="";
                            Date startDate1;
                            String[] tempStartDate=schTO.getStartDate().split(" ");
                            String startDate=tempStartDate[0];
                            String[] startTime=tempStartDate[1].split(":");
                            int startHour=Integer.parseInt(startTime[0]);
                            int starMmin=Integer.parseInt(startTime[1]);
                            int hourDuration= Integer.parseInt(schTO.getTravelHours()) + (Integer.parseInt(schTO.getTravelMinutes()) / 60);
                            System.out.println("hourDuration:"+hourDuration);
                              DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                                startDate1 = formatter.parse(startDate);
                                 Calendar cal = Calendar.getInstance(); // creates calendar
                                cal.setTime(startDate1); // sets calendar time/date
                                int tripStartHours =startHour + (starMmin / 60);
                                cal.add(Calendar.HOUR_OF_DAY, tripStartHours); // adds trip start hour
                                cal.add(Calendar.HOUR_OF_DAY, (int) hourDuration); // adds transit hours
                                Date tripETADateDt = cal.getTime(); // returns new date object, one hour in the future
                                ETADate = formatter.format(tripETADateDt);
                                SimpleDateFormat sdf = new SimpleDateFormat("kk:mm:ss");
                                ETATime = sdf.format(tripETADateDt);
                                if ("24:00:00".equals(ETATime)) {
                                    ETATime = "00:00:00";
                                }
                            if("2".equalsIgnoreCase(schTO.getMovementType()) || "5".equalsIgnoreCase(schTO.getMovementType()) ){
                            containerType="Import";
                            containerType1="Destuffed";
                            operationType="destuffed";
                            }else{
                            containerType="Empty";
                            containerType1="Loaded";
                            operationType="stuff";
                            }
                        contentSB.append("<tr>");
                        contentSB.append("<th>Container Numbers:</th>");
                        contentSB.append("<th>"+schTO.getContainerNo()+"</th>");
                        contentSB.append("</tr>");
                        contentSB.append("<tr>");
                        contentSB.append("<th>Trailer Number:</th>");
                        contentSB.append("<th>"+schTO.getVehicleNo()+"</th>");
                        contentSB.append("</tr>");
                        contentSB.append("<tr>");
                        contentSB.append("<th>Trailer Contact Number:</th>");
                        contentSB.append("<th>"+schTO.getMobileNo()+"</th>");
                        contentSB.append("</tr>");
                        contentSB.append("<tr>");
                        contentSB.append("<th>"+containerType+" Container DICT Gate Out Time:</th>");
                        contentSB.append("<th>"+schTO.getStartDate()+"</th>");
                        contentSB.append("</tr>");
                        contentSB.append("<tr>");
                        contentSB.append("<th>ETA Factory:</th>");
                         contentSB.append("<th>"+ETADate+" "+ ETATime+"</th>");
                        contentSB.append("</tr>");
                        contentSB.append("<tr>");
                        contentSB.append("<th>"+containerType+" Container Factory Arrival Time:</th>");
                         contentSB.append("<th></th>");
                        contentSB.append("</tr>");
                        contentSB.append("<tr>");
                        contentSB.append("<th>"+containerType1+" Container Factory Despatch Time:</th>");
                         contentSB.append("<th></th>");
                        contentSB.append("</tr>");
                        contentSB.append("<tr>");
                        if(!"2".equalsIgnoreCase(schTO.getMovementType()) && !"5".equalsIgnoreCase(schTO.getMovementType()) ){
                        contentSB.append("<th>Loaded Container DICT Gate In Time:</th>");
                        contentSB.append("<th></th>");
                        contentSB.append("</tr>");
                        }
                            
                        }
                        contentData = "Hi ,<br>Below is the Trip Details: <table border='1' cellspacing='4' cellpadding='1' width='600px'>" + contentSB.toString() + "</table>";
                         if("start".equalsIgnoreCase(flag)){
                        contentData = contentData + "<br><br> Please arrange to Stuff the above mentioned container and release our trailer as soon as possible to avoid applicable Trailer detention charges as per our agreement. ";
                         }else{
                         contentData = contentData;
                         }
                         String disclamier="<b>Disclaimer:</b> <font size='2'>While we have used our reasonable efforts to ensure the accuracy of the data above, it should be read as indicative rather than exact figures. We are sharing the above data for overall convenience of customers and should not be treated as obligation of Delhi International Cargo Terminals, International Cargo Terminal & Rail Infrastructure Private Limited. The information about container tracking is sourced from various external links. Delhi International Cargo Terminals, International Cargo Terminal & Rail Infrastructure Private Limited and its group companies have no means to cross verify this information. We accept no liability for the content of this information and the consequences, losses and damages that may arises to any customer by using this information. If you are not the intended user you are notified that any dissemination, use, review, distribution, printing or copying of this data in whole or in part is strictly prohibited. Prior using the above data customer are agreeing to have read, understood and agreed to the above terms. Upon declining to accept these terms, customer should not access and use the above data.</font>";
                        String footer="Team DICT(Sonepat)";
                        contentData=contentData+"<br><br>Thanks,<br>"+footer +"<br><br>"+disclamier;
                        System.out.println(contentData);

                        schTO.setMailStatus("0");
                        schTO.setMailTypeId("2");
                        if("start".equalsIgnoreCase(flag)){
                        schTO.setMailSubjectTo("Trip Start Notification for Container No: "+containerNo);
                        schTO.setMailSubjectCc("Trip Start Notification for Container No: "+containerNo);
                        schTO.setMailSubjectBcc("Trip Start Notification for Container No: "+containerNo);
                        }else{
                        schTO.setMailSubjectTo("Vehicle reached Notification for Container No: "+containerNo);
                        schTO.setMailSubjectCc("Vehicle reached Notification for Container No: "+containerNo);
                        schTO.setMailSubjectBcc("Vehicle reached Notification for Container No: "+containerNo);
                        }
                        schTO.setEmailFormat(contentData);
                        System.out.println("contentData@@@@@@@@@@@@@@@@@@@@@@@" + contentData);
                        schTO.setMailIdTo(recCc1);
                        schTO.setMailIdCc(recCc1);
                        schTO.setMailIdBcc(recBcc1);

                        Integer mails = schedulerDAO.insertMailDetails(schTO, 1);
                        System.out.println("insertMailDetails@@@" + mails);
                        if (mails > 0) {
                            if("start".equalsIgnoreCase(flag)){
                           schedulerDAO.updateStartEmailNotification(tripId);
                            }else{
                            schedulerDAO.updateEndEmailNotification(tripId);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("Exception in email Send Call: " + e.getMessage());
                    }
                }

               

            } catch (Exception excp) {
                System.out.println("ERROR STATUS   " + excp);

            }
        } catch (Exception excp) {
            System.out.println("ERROR STATUS   " + excp);

            excp.printStackTrace();
        }
        return processMain;
    }
    
     public ArrayList getOwnLeasedTripCountEmail() throws FPBusinessException, FPRuntimeException {
        ArrayList ownLeasedTripCount = new ArrayList();
        try{
        String recTo1 = ThrottleConstants.dailyTripReportTo;
        String recCc1 = ThrottleConstants.dailyTripReportCc;
        String recBcc1 = "";
        ownLeasedTripCount = schedulerDAO.getOwnLeasedTripCount();
        String contentData = "";
        StringBuffer contentSB = new StringBuffer();
        contentSB.append("<tr>");
        contentSB.append("<th >Date</th>");
        contentSB.append("<th >Name of Transporter</th>");
        contentSB.append("<th >No. of vehicle Used</th>");
        contentSB.append("<th >No. of Trips</th>");
        contentSB.append("<th >cumulative Trips</th>");
        contentSB.append("<th >Avg. Trips</th>");
        contentSB.append("</tr>");
        Iterator itr= ownLeasedTripCount.iterator();
          SchedulerTO schTO=new SchedulerTO();
          while(itr.hasNext()){
          schTO=(SchedulerTO)itr.next();
          contentSB.append("<tr>");
        contentSB.append("<td >"+schTO.getStartDate()+"</td>");
        contentSB.append("<td>DICT + Leased</td>");
        contentSB.append("<td>"+schTO.getVehicleCount()+"</td>");
        contentSB.append("<td>"+schTO.getTripCount()+"</td>");
        contentSB.append("<td></td>");
        contentSB.append("<td></td>");
        contentSB.append("</tr>");
       }
    contentData = "Hi ,<br>Below is the Trip Details: <table border='1' cellspacing='4' cellpadding='1' width='600px'>" + contentSB.toString() + "</table>";
    System.out.println(contentData);
        schTO.setMailStatus("0");
        schTO.setMailTypeId("2");
        schTO.setMailSubjectTo("Daywise Trip Report(GR basis): ");
        schTO.setMailSubjectCc("Daywise Trip Report(GR basis): ");
        schTO.setMailSubjectBcc("Daywise Trip Report(GR basis): ");
        schTO.setEmailFormat(contentData);
        System.out.println("contentData@@@@@@@@@@@@@@@@@@@@@@@" + contentData);
        schTO.setMailIdTo(recTo1);
        schTO.setMailIdCc(recCc1);
        schTO.setMailIdBcc(recBcc1);
        Integer mails = schedulerDAO.insertMailDetails(schTO, 1);
       
    
     } catch (Exception e) {
        e.printStackTrace();
        System.out.println("Exception in email Send Call: " + e.getMessage());
    }
    return ownLeasedTripCount;
}
    public void getPDFEmail(String tripSheetId1) throws FPBusinessException, FPRuntimeException {
        ArrayList containerNoList = new ArrayList();
        try{
            
             ArrayList tripDetailsList = tripDAO.getPrintTripSheetDetails(tripSheetId1);
             String consignmentNoteOredrId ="";
           // if (tripDetailsList.size() > 0) {
                TripTO tripTo = (TripTO) tripDetailsList.get(0);
                String fromCityName= tripTo.getFromCityName();
                String toCityName= tripTo.getToCityName();
                String interamPoint = tripTo.getInteramPoint();
                String consigneeName= tripTo.getConsigneeName();
                String consigneeMobileNo= tripTo.getConsigneeMobileNo();
                String consigneeAddress= tripTo.getConsigneeAddress();
                String consignorName= tripTo.getConsignorName();
                String consignorMobileNo= tripTo.getConsignorMobileNo();
                String consignorAddress= tripTo.getConsignorAddress();
                String totalWeight=tripTo.getTotalWeight();
                String freightAmount=tripTo.getFreightAmount();
                String consignmentNoteId= tripTo.getConsignmentNoteId();
                String vehicleNo= tripTo.getVehicleNo();
                String grNumber= tripTo.getGrNumber();
                String grDate= tripTo.getTripDate();
                String orderType= tripTo.getOrderType();
                //    String"sealNo", tripTo.getSealNo());
                String articleName= tripTo.getArticleName();
                String grStatus= tripTo.getGrStatus();
                String billingParty= tripTo.getBillingParty();
                String tripStatusId= tripTo.getTripStatusId();
               System.out.println("consignorAddress:"+consignorAddress);
               System.out.println("consigneeAddress:"+consigneeAddress);
               System.out.println("orderType:"+orderType);
                System.out.println("gr no:" + tripTo.getGrNumber() + "date:" + tripTo.getTripDate() + "order type" + tripTo.getOrderType() + "interam Point:" + tripTo.getInteramPoint());
                consignmentNoteOredrId = tripTo.getConsignmentNoteId();

           // }
                String containerNo= "";
                String linerName= "";
                String containerTypeId= "";
                String sealNo= "";
                String containerNo1= "";
                    String linerName1= "";
                    String containerTypeId1= "";
                    String sealNo1= "";
            containerNoList = tripDAO.getConsignmentNoteContainer(consignmentNoteOredrId, tripSheetId1);
            if (containerNoList.size() > 0) {
                TripTO tripTo1 = (TripTO) containerNoList.get(0);
                 containerNo= tripTo1.getContainerNo();
                 linerName= tripTo1.getLinerName();
                 containerTypeId= tripTo1.getContainerTypeId();
                 sealNo= tripTo1.getSealNo();
                if (containerNoList.size() > 1) {
                    TripTO tripTo2 = (TripTO) containerNoList.get(1);
                     containerNo1= tripTo2.getContainerNo();
                     linerName1= tripTo2.getLinerName();
                     containerTypeId1= tripTo2.getContainerTypeId();
                     sealNo1= tripTo2.getSealNo();
                    System.out.println("container 2 details:" + tripTo2.getContainerNo() + tripTo2.getLinerName() + tripTo2.getContainerTypeId());
                }
            }
            
            
            ArrayList consignmentArticleList = tripDAO.getConsignmentArticleList(tripSheetId1);
//            if (consignmentArticleList.size() > 0) {
//                String consignmentArticleList= consignmentArticleList;
//            }
       SendEmail em = new SendEmail();
       ByteArrayOutputStream outputStream=new ByteArrayOutputStream();
       Document document = new Document();
        PdfWriter.getInstance(document, outputStream);
            System.out.println("HI.pdf is called");
        document.open();
        //content of pdf start
        
        PdfPTable table = new PdfPTable(3);
        PdfPTable innertable1 = new PdfPTable(10);

        PdfPTable innertable2 = new PdfPTable(3);
        PdfPTable innertable3 = new PdfPTable(2);
        PdfPTable innertable4 = new PdfPTable(1);
        PdfPTable innertable5 = new PdfPTable(1);
        PdfPTable innerHeadertable = new PdfPTable(3);
        
        table.setTotalWidth(new float[]{100,320,100});
        //table.setTotalWidth(new float[]{191,191,90,92});
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.setLockedWidth(true);
        innerHeadertable.setTotalWidth(new float[]{300,120,100});
        //table.setTotalWidth(new float[]{191,191,90,92});
        innerHeadertable.setHorizontalAlignment(Element.ALIGN_LEFT);
        innerHeadertable.setLockedWidth(true);
        
        innertable1.setTotalWidth(new float[]{47, 87,57,47,47,47,47,47,47,47});
        innertable1.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable1.setLockedWidth(true);
        
        innertable2.setTotalWidth(new float[]{320, 100,100});
        innertable2.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable2.setLockedWidth(true);
        innertable3.setTotalWidth(new float[]{420,100});
        innertable3.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable3.setLockedWidth(true);
        innertable4.setTotalWidth(new float[]{520});
        innertable4.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable4.setLockedWidth(true);
        innertable5.setTotalWidth(new float[]{520});
        innertable5.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable5.setLockedWidth(true);
//        document.addTitle("Test PDF NITHYA");
//        document.addSubject("Testing email PDF NITHYA");
//        document.addKeywords("iText, email");
//        document.addAuthor("Nithya");
//        document.addCreator("Nithya");
          Image img = Image.getInstance("images/dict-logo11.png");
          img.scalePercent(30);
         PdfPCell headCell1 = new PdfPCell(new Phrase("Header", FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD, GrayColor.BLACK)));
          headCell1.setFixedHeight(40);
         headCell1.disableBorderSide(4);
        //headCell.setBackgroundColor(Color.lightGray);
        headCell1.addElement(new Chunk(img, 10, -10));
        headCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(headCell1);
        
        headCell1 = new PdfPCell(new Phrase("SUBJECT TO DELHI JURISDICTION \n International Cargo Terminals And Rail Infrastructure Pvt.Ltd. \n (Formely Known As BoxTrans Logistics (India) Services Pvt Ltd.) \n Panchi Gujran,Tehsil-Ganaur, Dist-Sonepat-131101 \n E-Mail:-transportdict@ict.in, &nbsp;&nbsp;Website:www.ict.in,Tel:0130-3053400", FontFactory.getFont(FontFactory.HELVETICA, 8, Font.BOLD, GrayColor.BLACK)));
        headCell1.setFixedHeight(40);
        
         headCell1.disableBorderSide(4);
        //headCell.setBackgroundColor(Color.lightGray);
       
        headCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(headCell1);
        
         headCell1 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD, GrayColor.BLACK)));
        headCell1.setFixedHeight(40);
         headCell1.disableBorderSide(4);
        //headCell.setBackgroundColor(Color.lightGray);
        headCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(headCell1);
        
        
        PdfPCell headCell2 = new PdfPCell(new Phrase("Booking Office: DICT SONEPAT                                                     CONSIGNOR COPY", FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
          headCell2.setFixedHeight(10);
          headCell2.setColspan(3);
         headCell2.disableBorderSide(3);
         headCell2.disableBorderSide(4);
        headCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(headCell2);
        
        PdfPCell headCell3 = new PdfPCell(new Phrase("Consignor's Name & Address \n\n" +consignorName+"\n\n"+consignorAddress, FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
          headCell3.setFixedHeight(40);
         
         headCell3.disableBorderSide(4);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        innerHeadertable.addCell(headCell3);
        
        headCell3 = new PdfPCell(new Phrase("AT OWNER'S RISK\n\n" +"INSURANCE \n\n"+"The Customer has Stated That:-", FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
          headCell3.setFixedHeight(40);
         
         headCell3.disableBorderSide(4);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        innerHeadertable.addCell(headCell3);
        
        
        
        headCell3 = new PdfPCell(new Phrase("G.R.NO: "+grNumber+"\n \n Date:"+grDate, FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
          headCell3.setFixedHeight(40);
        
         headCell3.disableBorderSide(4);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        innerHeadertable.addCell(headCell3);
        
        headCell3 = new PdfPCell(new Phrase("Consignee's Name & Address: \n\n"+consigneeName+"\n\n"+consigneeAddress, FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
          headCell3.setFixedHeight(40);
         headCell3.disableBorderSide(3);
         headCell3.disableBorderSide(4);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        innerHeadertable.addCell(headCell3);
        
        headCell3 = new PdfPCell(new Phrase("He has not insured the consignment. [] \n\n"+"He has insured the consignment.  []", FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
          headCell3.setFixedHeight(40);
         headCell3.disableBorderSide(3);
         headCell3.disableBorderSide(4);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        innerHeadertable.addCell(headCell3);
        
         headCell3 = new PdfPCell(new Phrase("From:"+toCityName +"\n\n To:"+interamPoint +"\n\nTo:"+fromCityName, FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
          headCell3.setFixedHeight(40);
         headCell3.disableBorderSide(3);
         headCell3.disableBorderSide(4);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        innerHeadertable.addCell(headCell3);
        
        
        headCell3 = new PdfPCell(new Phrase("Billing Party: \n\n"+billingParty+ "\n\n Movement Type:"+orderType, FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
          headCell3.setFixedHeight(40);
        
         headCell3.disableBorderSide(4);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        innerHeadertable.addCell(headCell3);
        
        headCell3 = new PdfPCell(new Phrase("Company.........................................\n\n" +"Policy No.........................................", FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
          headCell3.setFixedHeight(40);
         
         headCell3.disableBorderSide(4);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        innerHeadertable.addCell(headCell3);
        
         headCell3 = new PdfPCell(new Phrase("Vehicle No:"+vehicleNo, FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
          headCell3.setFixedHeight(40);
        
         headCell3.disableBorderSide(4);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        innerHeadertable.addCell(headCell3);
        
        
        PdfPCell headCell4 = new PdfPCell(new Phrase("Some text here"));
        

        headCell4 = new PdfPCell(new Phrase("No of PKG", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell4.setFixedHeight(20);
        headCell4.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable1.addCell(headCell4);
        
        headCell4 = new PdfPCell(new Phrase("Description Of Goods(Said To Contain)", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell4.setFixedHeight(20);
        headCell4.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable1.addCell(headCell4);
            
        headCell4 = new PdfPCell(new Phrase("Container No", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell4.setFixedHeight(20);
        headCell4.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable1.addCell(headCell4);
        headCell4 = new PdfPCell(new Phrase("Size ", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell4.setFixedHeight(20);
        headCell4.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable1.addCell(headCell4);
        headCell4 = new PdfPCell(new Phrase("S. Line  ", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell4.setFixedHeight(20);
        headCell4.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable1.addCell(headCell4);
        headCell4 = new PdfPCell(new Phrase("Seal No", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell4.setFixedHeight(20);
        headCell4.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable1.addCell(headCell4);
        headCell4 = new PdfPCell(new Phrase("Actual Weight", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell4.setFixedHeight(20);
        headCell4.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable1.addCell(headCell4);
        headCell4 = new PdfPCell(new Phrase("Charged Weight", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell4.setFixedHeight(20);
        headCell4.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable1.addCell(headCell4);
        headCell4 = new PdfPCell(new Phrase("Rate", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell4.setFixedHeight(20);
        headCell4.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable1.addCell(headCell4);
        headCell4 = new PdfPCell(new Phrase("Amount To Pay/Paid Rs.", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell4.setFixedHeight(20);
        headCell4.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable1.addCell(headCell4);
        
        PdfPCell headCell5 = new PdfPCell(new Phrase("Some text here"));
        
        headCell5 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable1.addCell(headCell5);
        
        headCell5 = new PdfPCell(new Phrase(articleName, FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable1.addCell(headCell5);
            
        headCell5 = new PdfPCell(new Phrase(containerNo+"\n"+containerNo1, FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable1.addCell(headCell5);
        headCell5 = new PdfPCell(new Phrase(containerTypeId+"\n"+containerTypeId1, FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable1.addCell(headCell5);
        headCell5 = new PdfPCell(new Phrase(linerName+"\n"+linerName1, FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable1.addCell(headCell5);
        headCell5 = new PdfPCell(new Phrase(sealNo+"\n"+sealNo1, FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable1.addCell(headCell5);
        headCell5 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable1.addCell(headCell5);
        headCell5 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable1.addCell(headCell5);
        headCell5 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable1.addCell(headCell5);
        headCell5 = new PdfPCell(new Phrase(grStatus, FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable1.addCell(headCell5);
        
        
        PdfPCell headCell6 = new PdfPCell(new Phrase("Some text here"));
        
        headCell6 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell6.setFixedHeight(60);
        headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable1.addCell(headCell6);
        
        headCell6 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell6.setFixedHeight(60);
        headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable1.addCell(headCell6);
            
        headCell6 = new PdfPCell(new Phrase("ICD Out Date	..............................................	Time	.........................................\n \n" +
         "Factory In Date	..............................................	Time	.........................................\n\n" +
         "Factory Out Date	...............................................	Time	.........................................\n\n" +
         "ICD In Date	...............................................	Time	.........................................", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell6.setFixedHeight(60);
        headCell6.setColspan(8);
        headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable1.addCell(headCell6);
        
        
         PdfPCell headCell7 = new PdfPCell(new Phrase("Some text here"));
        
        headCell7 = new PdfPCell(new Phrase("Party Invoice No & Date: \n Party CST/TIN No:\n" +"Value Of Goods:Rs.", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell7.setFixedHeight(25);
        headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell7);
        
        headCell7 = new PdfPCell(new Phrase("Permit No:", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell7.setFixedHeight(25);
        headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell7);
        
        headCell7 = new PdfPCell(new Phrase("Remarks:", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell7.setFixedHeight(25);
        headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell7);
        
        
         PdfPCell headCell8 = new PdfPCell(new Phrase("Some text here"));
        
        headCell8 = new PdfPCell(new Phrase("1)GST No:06AACCB8054G1ZT,GST UNDER GTA TO BE PAID BYCONSIGNOR OR CONSIGNEE\n" +"2)PAN NO-AACCB8054G       CIN NO:U63040MH2006PTC159885\n"+"3)Goods accepted for carriage on the terms & conditions printed over leaf.", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell8.setFixedHeight(25);
        headCell8.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable3.addCell(headCell8);
        headCell8 = new PdfPCell(new Phrase("Authorised Signature", FontFactory.getFont(FontFactory.HELVETICA, 8)));
        headCell8.setFixedHeight(25);
        headCell8.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable3.addCell(headCell8);
        
        
         PdfPCell headCell9 = new PdfPCell(new Phrase("Some text here"));
        
        headCell9 = new PdfPCell(new Phrase("Regd.Office: Godrej Coliseum,Office No. 801,8th Floor,C-Wing,Behind Everard Nagar,off Somaiya Hospital Road,Sion (East)Mumbai-400022 \n" +
"Branches:Loni,Ludhiana,Dadri,Tughlakbad", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell9.setFixedHeight(25);
        headCell9.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell9.disableBorderSide(3);
        headCell9.disableBorderSide(4);
//                        headCell3.disableBorderSide(2);
        innertable4.addCell(headCell9);
        
        String disclaimer="Declaration\n" +
"1. We hereby declare that we have not available under ST Notification No.12/2003,dated 20/06/2003 of Govt.of India,Ministry of Finance\n" +
"2. We hereby declare that we have not taken the credit of Excise Duty on inputs or capital goods used for providing the \"Transport of Goods by road\"Services under the provision of Cenvat Credit Rules,2004\n" +
"Terms & Condition\n" +
"a. Unless other wise agreed all consignment/goods are carried entirely at OWNERS risk as to packaging ,Goods contents,nature,condition and value of the Goods that are declared and are unknown to the carrier.The consignor shall indemnify the carrier against the loss resulting from inaccuracies or inadequacies of the particulars.\n" +
"b. Carrier will carry all goods on said to contain basis and will not stand liable for any internal discrepancy.\n" +
"c. Goods to be insured by the consignor at their own cost to protect themselves against any losses and/or risk during transit by road.\n" +
"d. Carrier explicity reserves right to refusal for carriage of goods by road and such right shall be absolute.\n" +
"e. Transport charges are payable with in 24 Hours from loading the Goods on transport vehicle of carriers.Failure to pay transport charges prior to the arrival of Goods at destination shall attract penalty @ 2% on the transport charges.Octrol charges any other statutory government levels to be paid by consignor.\n" +
"f. Carrier reserves right to re-measure and/or re-weight the goods of any discrepancy noted on the actual and charged weight, carrier shall impose penalty plus the rates for differential weight.\n" +
"g. Carrier shall not be held liable if goods are detained or security by competent authority for any misdeclaration or duty unpaid or prohibited Goods, Carrier shall not be liable for any losses, damage or deterioration to the Goods caused by such detention for examination or scrutiny.\n" +
"h. Carrier shall deliver the Goods within the normal transit time and shall not be liable for any delay in delivery,except where expressly agreed and such written request for timely delivery is submitted subject to force majeure conditions.\n" +
"i. Carrier shall make available the goods at the disposal of consignee or their agents at premises or carrier and same to be collected with in five (5) days from the date of arrival of Goods,failing which storage charges shall be payable in additional to transport charges.Carrier shall not be responsible for any damage and/or loss whatsoever to Goods stored at premises.\n" +
"j. Carrier shall have right to dispose and/or auction,the perishable goods within forty eight(48) hours of the arrival at premises if uncleared,and thirty(30)days for other goods without any notice to the consignee/ consignor.The disposal charges along with the storage charges for such period shall be payable to the consignor.\n" +
"k. Carrier shall not be responsible for delay in transit due to unforeseen contingencies,or act of god,or due to strike,war,civil commotions,and/or,events which are beyond the control of Carrier.\n" +
"l. Carrier shall not be liable for any loss and/or damages to goods arising out of resulting due to Act of God,war,public or state emergencies,robbery,theft,dacoity,damage due to defect in packaging materials,or due to events that are beyond the control of carrier.\n" +
"m. Carrier shall not be responsible for damages to the inner content of the packages or Goods if the same has been delivered in same condition in which they were tendered for road transportation.Any kind of shortage has to be informed within twenty four(24)hrs of receipt of the goods with documented proof.In case of seal broken upon arrival the count of goods shall be taken jointly.\n" +
"n. No claim for compensation shall be entertained by the carrier after a period of seven(7) days from the date of delivery of the Goods along with documented/evidence and carrier has right to reject the same without assigning any reasons whatsoever.\n" +
"o. No suit or other legal proceeding shall be initiated,unless notice in writing has been served upon carrier before institution of suit or legal proceeding within 180 days from date receipt of Goods or from the date of booking.\n" +
"p. Any special instruction unless expressly written by the consignor shall not be entertained.\n" +
"q. The contract evidenced hereby shall be governed by and construed according to the Indian laws and any difference of opinion or dispute there under shall be settled by arbitration Delhi with each party appointing an arbitrator.\n" +
"r. In respect of any and/or all claims arising under this contract,The Courts of Delhi shall have exclusive jurisdiction.\n" +
"s. Received the Goods Specified overleap in good & proper condition.\n" +
"\n" +
"Signature of consignee or Agent with Rubber Stamp\n" +
"Date";
        
        
         PdfPCell headCell10 = new PdfPCell(new Phrase("Some text here"));
        
        headCell10 = new PdfPCell(new Phrase(disclaimer, FontFactory.getFont(FontFactory.HELVETICA, 5)));
        headCell10.setFixedHeight(100);
        headCell10.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell10.disableBorderSide(3);
        headCell10.disableBorderSide(4);
//                        headCell3.disableBorderSide(2);
        innertable5.addCell(headCell10);
       // Paragraph paragraph = new Paragraph();
    //    paragraph.add(new Chunk("hello!.This is Nithya"));
      //  document.add(paragraph);
        document.add(table);
        document.add(innerHeadertable);
        document.add(innertable1);
        document.add(innertable2);
        document.add(innertable3);
        document.add(innertable4);
        document.add(innertable5);
        document.close();
        //end
        String recTo1 = ThrottleConstants.tripPlannedToMailId;
        String recCc1 = ThrottleConstants.tripPlannedCCMailId;
        String recBcc1 = ThrottleConstants.tripPlannedBccMailId;
       // ownLeasedTripCount = schedulerDAO.getOwnLeasedTripCount();
        String contentData = "";
        StringBuffer contentSB = new StringBuffer();
        contentSB.append("<tr>");
        contentSB.append("<th >Date</th>");
        contentSB.append("<th >Name of Transporter</th>");
        contentSB.append("<th >No. of vehicle Used</th>");
        contentSB.append("<th >No. of Trips</th>");
        contentSB.append("<th >cumulative Trips</th>");
        contentSB.append("<th >Avg. Trips</th>");
        contentSB.append("</tr>");
//        Iterator itr= ownLeasedTripCount.iterator();
//          SchedulerTO schTO=new SchedulerTO();
//          while(itr.hasNext()){
//          schTO=(SchedulerTO)itr.next();
//          contentSB.append("<tr>");
//        contentSB.append("<td >"+schTO.getStartDate()+"</td>");
//        contentSB.append("<td>DICT + Leased</td>");
//        contentSB.append("<td>"+schTO.getVehicleCount()+"</td>");
//        contentSB.append("<td>"+schTO.getTripCount()+"</td>");
//        contentSB.append("<td></td>");
//        contentSB.append("<td></td>");
//        contentSB.append("</tr>");
//       }
    contentData = "Hi ,<br>Below is the Trip Details: <table border='1' cellspacing='4' cellpadding='1' width='600px'>" + contentSB.toString() + "</table>";
         System.out.println(contentData);
//        schTO.setMailStatus("0");
//        schTO.setMailTypeId("2");
//        schTO.setMailSubjectTo("Daywise Trip Report(GR basis): ");
//        schTO.setMailSubjectCc("Daywise Trip Report(GR basis): ");
//        schTO.setMailSubjectBcc("Daywise Trip Report(GR basis): ");
//        schTO.setEmailFormat(contentData);
//        System.out.println("contentData@@@@@@@@@@@@@@@@@@@@@@@" + contentData);
//        schTO.setMailIdTo(recTo1);
//        schTO.setMailIdCc(recCc1);
//        schTO.setMailIdBcc(recBcc1);
        //Integer mails = schedulerDAO.insertMailDetails(schTO, 1);
       
      em.sendPDFMail(grNumber+".pdf", recTo1, recCc1, recBcc1, "Trip planned for container"+containerNo +" "+containerNo1,"Hi,", outputStream);
     } catch (Exception e) {
        e.printStackTrace();
        System.out.println("Exception in email Send Call: " + e.getMessage());
    }
    
}
    
    
    public void getInvoicePDFEmail(String tripSheetId1) throws FPBusinessException, FPRuntimeException {
        ArrayList containerNoList = new ArrayList();
        try{
            System.out.println("getInvoicePDFEmail : called");
             ArrayList tripDetailsList = tripDAO.getPrintTripSheetDetails(tripSheetId1);
             String consignmentNoteOredrId ="";
           // if (tripDetailsList.size() > 0) {
                TripTO tripTo = (TripTO) tripDetailsList.get(0);
                String fromCityName= tripTo.getFromCityName();
                String toCityName= tripTo.getToCityName();
                String interamPoint = tripTo.getInteramPoint();
                String consigneeName= tripTo.getConsigneeName();
                String consigneeMobileNo= tripTo.getConsigneeMobileNo();
                String consigneeAddress= tripTo.getConsigneeAddress();
                String consignorName= tripTo.getConsignorName();
                String consignorMobileNo= tripTo.getConsignorMobileNo();
                String consignorAddress= tripTo.getConsignorAddress();
                String totalWeight=tripTo.getTotalWeight();
                String freightAmount=tripTo.getFreightAmount();
                String consignmentNoteId= tripTo.getConsignmentNoteId();
                String vehicleNo= tripTo.getVehicleNo();
                String grNumber= tripTo.getGrNumber();
                String grDate= tripTo.getTripDate();
                String orderType= tripTo.getOrderType();
                //    String"sealNo", tripTo.getSealNo());
                String articleName= tripTo.getArticleName();
                String grStatus= tripTo.getGrStatus();
                String billingParty= tripTo.getBillingParty();
                String tripStatusId= tripTo.getTripStatusId();
               System.out.println("consignorAddress:"+consignorAddress);
               System.out.println("consigneeAddress:"+consigneeAddress);
               System.out.println("orderType:"+orderType);
                System.out.println("gr no:" + tripTo.getGrNumber() + "date:" + tripTo.getTripDate() + "order type" + tripTo.getOrderType() + "interam Point:" + tripTo.getInteramPoint());
                consignmentNoteOredrId = tripTo.getConsignmentNoteId();

           // }
                String containerNo= "";
                String linerName= "";
                String containerTypeId= "";
                String sealNo= "";
                String containerNo1= "";
                    String linerName1= "";
                    String containerTypeId1= "";
                    String sealNo1= "";
            containerNoList = tripDAO.getConsignmentNoteContainer(consignmentNoteOredrId, tripSheetId1);
            if (containerNoList.size() > 0) {
                TripTO tripTo1 = (TripTO) containerNoList.get(0);
                 containerNo= tripTo1.getContainerNo();
                 linerName= tripTo1.getLinerName();
                 containerTypeId= tripTo1.getContainerTypeId();
                 sealNo= tripTo1.getSealNo();
                if (containerNoList.size() > 1) {
                    TripTO tripTo2 = (TripTO) containerNoList.get(1);
                     containerNo1= tripTo2.getContainerNo();
                     linerName1= tripTo2.getLinerName();
                     containerTypeId1= tripTo2.getContainerTypeId();
                     sealNo1= tripTo2.getSealNo();
                    System.out.println("container 2 details:" + tripTo2.getContainerNo() + tripTo2.getLinerName() + tripTo2.getContainerTypeId());
                }
            }
            
            
            ArrayList consignmentArticleList = tripDAO.getConsignmentArticleList(tripSheetId1);
//            if (consignmentArticleList.size() > 0) {
//                String consignmentArticleList= consignmentArticleList;
//            }
       SendEmail em = new SendEmail();
       Multipart multipart = new MimeMultipart();
        
       for(int i=0;i <2;i++){
               
        //content of pdf start
        ByteArrayOutputStream outputStream=new ByteArrayOutputStream();
        Document document = new Document();PdfWriter.getInstance(document, outputStream);
        System.out.println("HI.pdf is called");
        
         document.addTitle("Test PDF NITHYA");
         document.addSubject("Testing email PDF NITHYA");
         document.addKeywords("iText, email");
         document.addAuthor("Nithya");
         document.addCreator("Nithya");
      
         document.open();
        Paragraph paragraph = new Paragraph();
        paragraph.add(new Chunk("hello!.This is Nithya"+i));
        document.add(paragraph);
        
       document.close();
     //  multipart = em.addMultipleAttachments("Nithya"+i, outputStream,multipart);
        //end
        }
        
        String recTo1 = ThrottleConstants.tripPlannedToMailId;
        String recCc1 = ThrottleConstants.tripPlannedCCMailId;
        String recBcc1 = ThrottleConstants.tripPlannedBccMailId;
        String contentData = "";
        StringBuffer contentSB = new StringBuffer();
        contentSB.append("<tr>");
        contentSB.append("<th >Date</th>");
        contentSB.append("<th >Name of Transporter</th>");
        contentSB.append("<th >No. of vehicle Used</th>");
        contentSB.append("<th >No. of Trips</th>");
        contentSB.append("<th >cumulative Trips</th>");
        contentSB.append("<th >Avg. Trips</th>");
        contentSB.append("</tr>");

         contentData = "Hi ,<br>Below is the Trip Details: <table border='1' cellspacing='4' cellpadding='1' width='600px'>" + contentSB.toString() + "</table>";
         System.out.println(contentData);

       
//     em.sendMultipleAttachPDFMail( recTo1, recCc1, recBcc1, "multiple pdf attachment","Hi,", multipart);
     } catch (Exception e) {
        e.printStackTrace();
        System.out.println("Exception in email Send Call: " + e.getMessage());
    }
    
}
    
     public void getInvoicePDFEmail(TripTO tripTO) throws FPBusinessException, FPRuntimeException {
           System.out.println("getInvoicePDFEmail called");
            BillingTO billingTO = new BillingTO();
            String firstRemarks = "";
            String secondRemarks = "";
            String thirdRemarks = "";
            String billingParty = "";
            String creditNoteNo = "";
            String creditNoteDate = "";
            String createdBy = "";
            String customerName = "";
            String taxValue = "";
            String customerAddress = "";
            String panNo = "";
            String gstNo = "";
            String cityName = "";
            String state = "";
            String invoicecode = "";
            String consignmentOrderNos = "";
            String consignmentOrderDate = "";
            String billDate = "";
            String movementType = "";
            String containerTypeName = "";
            String billingType = "";
            String userName = "";
            String billingState = "";
            String remarks = "";
           // TripTO tripTO = new TripTO();
             Multipart multipart = new MimeMultipart();
            SendEmail em = new SendEmail();
             List<String> invoiceIds = new ArrayList<String>();
        try{
            
            // ArrayList customerList = schedulerDAO.getInvoiceCustomerList(tripTO.getFromDate(),tripTO.getToDate());
             String customerId ="";
             String invoiceId ="";
            
          //  if (customerList.size() > 0) {
             //   Iterator itr1 = customerList.iterator();
             //   SchedulerTO schedulerTO = new SchedulerTO();
             //    while (itr1.hasNext()) {
             //    schedulerTO = (SchedulerTO) itr1.next();
            //    customerId = schedulerTO.getBillingParty();
             
              // Get constructor to the Sun PKCS11 provider
			Class<?> pkcs11Class = Class.forName("sun.security.pkcs11.SunPKCS11");
			Constructor<?> construct = pkcs11Class.getConstructor(new Class[] {String.class});

			// Construct the provider
			String configName = "D://pkcs11.cfg";
			Provider p = (Provider)construct.newInstance(new Object[] {configName});
			Security.addProvider(p);
            
			// Create key store
			KeyStore ks = KeyStore.getInstance("PKCS11");
			ks.load(null, "password@123".toCharArray());
                                             
                         


			// Get the alias of the first entry in the keystore
			Enumeration<?> aliases = ks.aliases();
			if (aliases.hasMoreElements() == false)
			{
				System.out.println ("No digital IDs found in token.");
				System.exit(-1);
			}
			String alias = null;
			while (aliases.hasMoreElements()) {
			    alias = (String)aliases.nextElement();
			    System.out.println(alias);

    }
			System.out.println("p.getName():"+p.getName());
			PrivateKey key = (PrivateKey)ks.getKey(alias,"password@123".toCharArray());
			java.security.cert.Certificate[] chain = ks.getCertificateChain(alias);
               
           FileOutputStream keyStoreOutputStream = new FileOutputStream("data/keystore.ks");   
           FileOutputStream certStoreOutputStream = new FileOutputStream("data/cert.ks");   
             keyStoreOutputStream.write(key.toString().getBytes());
             certStoreOutputStream.write(chain.toString().getBytes());
             System.out.println("tripTO.getCustomerId():"+tripTO.getCustomerId());
             if(tripTO.getCustomerId() != null && !"".equalsIgnoreCase(tripTO.getCustomerId()) ){
             
             
                 ArrayList invoiceIdList = schedulerDAO.getInvoiceIdList(tripTO.getCustomerId(),tripTO.getFromDate(),tripTO.getToDate());
                 String mailId = schedulerDAO.getCustomerMailId(tripTO.getCustomerId());
                 Iterator itr2 = invoiceIdList.iterator();
               SchedulerTO schedulerTO1 = new SchedulerTO();
                while (itr2.hasNext()) {                           // invoice iteration
                schedulerTO1 = (SchedulerTO) itr2.next();
                    System.out.println("invoiceId$"+invoiceId);
                invoiceId = schedulerTO1.getInvoiceId();
                billingTO.setInvoiceId(invoiceId);
                tripTO.setInvoiceId(invoiceId);
                invoiceIds.add(invoiceId);
                    System.out.println("tripTO.getInvoiceId"+tripTO.getInvoiceId());
                    System.out.println("invoiceIds size:"+invoiceIds.size());
                ArrayList invoiceHeader = tripDAO.getOrderInvoiceHeader(tripTO);
                ArrayList invoiceDetails = tripDAO.getInvoiceDetails(tripTO);
                ArrayList invoiceDetailExpenses = tripDAO.getInvoiceDetailExpenses(tripTO);
               // ArrayList invoiceTaxDetails = billingDAO.getInvoiceGSTTaxDetails(billingTO);
            //    System.out.println("invoiceTaxDetailsSize:" + invoiceTaxDetails.size());
                String[] tempRemarks = null;
                Iterator itr = invoiceHeader.iterator();
                TripTO tpTO = new TripTO();

                while (itr.hasNext()) {
                        tpTO = (TripTO) itr.next();
                        billingParty = tpTO.getBillingParty() ;
                        creditNoteNo = tpTO.getCreditNoteNo();
                        creditNoteDate = tpTO.getCreditNoteDate();
                        createdBy = tpTO.getCreatedBy();
                        customerName = tpTO.getCustomerName();
                        taxValue = tpTO.getTaxValue();
                        customerAddress = tpTO.getCustomerAddress();
                        panNo = tpTO.getPanNo();
                        gstNo = tpTO.getGstNo();
                        cityName = tpTO.getCityName();
                        state = tpTO.getState();
                        invoicecode = tpTO.getInvoiceCode();
                        consignmentOrderNos = tpTO.getcNotes();
                        consignmentOrderDate =  tpTO.getConsignmentOrderDate();
                        billDate =  tpTO.getBillDate();
                        movementType = tpTO.getMovementType();
                        containerTypeName = tpTO.getContainerTypeName();
                        billingType = tpTO.getBillingType();
                        userName = tpTO.getUserName();
                        billingState = tpTO.getBillingState();
                        remarks = tpTO.getRemarks();
                
                if (!"".equals(tpTO.getRemarks())) {
                    tempRemarks = tpTO.getRemarks().split(",");
                    if (tempRemarks.length >= 1) {
                        firstRemarks = tempRemarks[0];
                        System.out.println("firstRemarks:" + tempRemarks[0]);
                    }
                    if (tempRemarks.length >= 2) {
                         secondRemarks = tempRemarks[1];
                    }
                    if (tempRemarks.length >= 3) {
                         thirdRemarks = tempRemarks[2];
                    }
                }

            }
                
            ArrayList invoiceDetailsList = new ArrayList();
            invoiceDetailsList = tripDAO.getOrderInvoiceDetailsList(tripTO);
            ArrayList tripDetails = new ArrayList();
         //   tripDetails = billingDAO.getTripDetails(billingTO);
          //  System.out.println("invoiceDetailsList.size():" + invoiceDetailsList.size());
                
               
            
			
            
          
       ByteArrayOutputStream outputStream=new ByteArrayOutputStream();
       Document document = new Document();
        PdfWriter.getInstance(document, outputStream);
            System.out.println("HI.pdf is called");
        document.open();
        //content of pdf start
        
        PdfPTable table = new PdfPTable(2);
        PdfPTable innertable = new PdfPTable(1);
        PdfPTable innertable1 = new PdfPTable(1);

        PdfPTable innertable2 = new PdfPTable(12);
        PdfPTable innertable3 = new PdfPTable(1);
        PdfPTable innertableSign = new PdfPTable(1);
        PdfPTable innertable4 = new PdfPTable(1);
        PdfPTable innertable5 = new PdfPTable(1);
        PdfPTable innerHeadertable = new PdfPTable(3); ///ok
        
        innertableSign.setTotalWidth(new float[]{500});
        innertableSign.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertableSign.setLockedWidth(true);
        
        table.setTotalWidth(new float[]{150,350});
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.setLockedWidth(true);
        
        innerHeadertable.setTotalWidth(new float[]{300,100,100});
        innerHeadertable.setHorizontalAlignment(Element.ALIGN_LEFT);
        innerHeadertable.setLockedWidth(true);
        
        innertable.setTotalWidth(new float[]{500});
        innertable.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable.setLockedWidth(true);
        
        innertable1.setTotalWidth(new float[]{500});
        innertable1.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable1.setLockedWidth(true);
        
        innertable2.setTotalWidth(new float[]{40, 40,60,40, 40,40, 40,40, 40,40, 40,40});
        innertable2.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable2.setLockedWidth(true);
        innertable3.setTotalWidth(new float[]{500});
        innertable3.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable3.setLockedWidth(true);
        innertable4.setTotalWidth(new float[]{500});
        innertable4.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable4.setLockedWidth(true);
        innertable5.setTotalWidth(new float[]{500});
        innertable5.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable5.setLockedWidth(true);
//        document.addTitle("Test PDF NITHYA");
//        document.addSubject("Testing email PDF NITHYA");
//        document.addKeywords("iText, email");
//        document.addAuthor("Nithya");
//        document.addCreator("Nithya");
        
          Image img = Image.getInstance("images/dict-logo11.png");
          img.scalePercent(30);
         PdfPCell headCell1 = new PdfPCell(new Phrase("Header", FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD, GrayColor.BLACK)));
          headCell1.setFixedHeight(40);
//         headCell1.disableBorderSide(4);
        //headCell.setBackgroundColor(Color.lightGray);
        headCell1.addElement(new Chunk(img, 10, -10));
        headCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell1.disableBorderSide(Rectangle.RIGHT);
        table.addCell(headCell1);
        
        headCell1 = new PdfPCell(new Phrase(" International Cargo Terminals And Rail Infrastructure Pvt.Ltd. \n Panchi Gujran,Tehsil-Ganaur, Dist-Sonepat-131101 \n CIN No:U63040MH2006PTC159885", FontFactory.getFont(FontFactory.HELVETICA, 8, Font.BOLD, GrayColor.BLACK)));
        headCell1.setFixedHeight(40);
        
         headCell1.disableBorderSide(4);
        //headCell.setBackgroundColor(Color.lightGray);
       
        headCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(headCell1);
        
        PdfPCell headCell = new PdfPCell(new Phrase("Some text here"));
        
        headCell = new PdfPCell(new Phrase("INVOICE", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell.setFixedHeight(20);
        headCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        innertable.addCell(headCell);
        
        
        PdfPCell headCell3 = new PdfPCell(new Phrase("Billed to \n" +billingParty+"\n"+customerAddress+"\nBilling State"+billingState+"\nPAN No:"+panNo+"\nGST No:"+gstNo, FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
          headCell3.setFixedHeight(40);
         
//         headCell3.disableBorderSide(4);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        innerHeadertable.addCell(headCell3);
        
        headCell3 = new PdfPCell(new Phrase("Invoice No" +invoicecode+"\n\n "+billingType+"\n\n SAC Code: 996791 \n\n", FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
          headCell3.setFixedHeight(40);
         
         headCell3.disableBorderSide(4);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        innerHeadertable.addCell(headCell3);
        
        
        
        headCell3 = new PdfPCell(new Phrase("Bill Date: "+billDate+"\n \n Dispatched Through::", FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
          headCell3.setFixedHeight(40);
        
         headCell3.disableBorderSide(4);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        innerHeadertable.addCell(headCell3);
        
        
          PdfPCell headCell4 = new PdfPCell(new Phrase("Some text here"));
        
        headCell4 = new PdfPCell(new Phrase("PARTICULARS", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell4.setFixedHeight(20);
        headCell4.setHorizontalAlignment(Element.ALIGN_CENTER);
        innertable1.addCell(headCell4);
        
        
        
        
        
        PdfPCell headCell5 = new PdfPCell(new Phrase("Some text here"));
        

        headCell5 = new PdfPCell(new Phrase("GR.No", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell5);
        
        headCell5 = new PdfPCell(new Phrase("Date", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell5);
            
        headCell5 = new PdfPCell(new Phrase("Description", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell5);
        headCell5 = new PdfPCell(new Phrase("No.Of Container ", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell5);
        headCell5 = new PdfPCell(new Phrase("Commodity Category  ", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell5);
        headCell5 = new PdfPCell(new Phrase("Commodity", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell5);
        headCell5 = new PdfPCell(new Phrase("Freight", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell5);
        headCell5 = new PdfPCell(new Phrase("Toll Tax", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell5);
        headCell5 = new PdfPCell(new Phrase("Detention Charges", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell5);
        headCell5 = new PdfPCell(new Phrase("Weightment", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell5);
        
        headCell5 = new PdfPCell(new Phrase("Other Charges", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell5);
        
        headCell5 = new PdfPCell(new Phrase("Total Amount(Rs.)", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell5);
        
        String totalFreightAmount = "";
        Float totalAmount = 0.0f;
        int totalContainerQty = 0;
          Iterator itrInv = invoiceDetailsList.iterator();
                           TripTO tripInvTO = null;
                           int i = 1;
        while (itrInv.hasNext()) {
        tripInvTO = (TripTO)itrInv.next();
        PdfPCell headCell6= new PdfPCell(new Phrase("Some text here"));
        

        headCell6 = new PdfPCell(new Phrase(tripInvTO.getGrNumber(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell6.setFixedHeight(20);
        headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell6.disableBorderSide(2);
        innertable2.addCell(headCell6);
        
        headCell6 = new PdfPCell(new Phrase(tripInvTO.getGrDate(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell6.setFixedHeight(20);
        headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell6.disableBorderSide(2);
        innertable2.addCell(headCell6);
            
        headCell6 = new PdfPCell(new Phrase(tripInvTO.getDestination(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell6.setFixedHeight(20);
        headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell6.disableBorderSide(2);
        innertable2.addCell(headCell6);
        headCell6 = new PdfPCell(new Phrase(tripInvTO.getContainerQty(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell6.setFixedHeight(20);
        headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
        
        totalContainerQty =  Integer.parseInt(tripInvTO.getContainerQty()) +totalContainerQty;
                        headCell6.disableBorderSide(2);
                           System.out.println("tripInvTO.getCommodityCategory():"+tripInvTO.getCommodityCategory());
        innertable2.addCell(headCell6);
        headCell6 = new PdfPCell(new Phrase(tripInvTO.getCommodityCategory(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell6.setFixedHeight(20);
        headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
         headCell6.disableBorderSide(2);
        innertable2.addCell(headCell6);
        headCell6 = new PdfPCell(new Phrase(tripInvTO.getArticleName(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell6.setFixedHeight(20);
        headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell6.disableBorderSide(2);
        innertable2.addCell(headCell6);
        
        headCell6 = new PdfPCell(new Phrase(tripInvTO.getFreightAmount(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell6.setFixedHeight(20);
        headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell6.disableBorderSide(2);
        innertable2.addCell(headCell6);
        
        headCell6 = new PdfPCell(new Phrase(tripInvTO.getTollTax(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell6.setFixedHeight(20);
        headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell6.disableBorderSide(2);
        innertable2.addCell(headCell6);
        headCell6 = new PdfPCell(new Phrase(tripInvTO.getDetaintion(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell6.setFixedHeight(20);
        headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell6);
        headCell6 = new PdfPCell(new Phrase(tripInvTO.getWeightmentExpense(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell6.setFixedHeight(20);
        headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell6.disableBorderSide(2);
        innertable2.addCell(headCell6);
        headCell6 = new PdfPCell(new Phrase(tripInvTO.getOtherExpense(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell6.setFixedHeight(20);
        headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell6.disableBorderSide(2);
        innertable2.addCell(headCell6);
        
        
        totalFreightAmount = Float.parseFloat(tripInvTO.getFreightAmount()) + Float.parseFloat(tripInvTO.getTollTax()) + Float.parseFloat(tripInvTO.getDetaintion()) +
                Float.parseFloat(tripInvTO.getWeightmentExpense()) + Float.parseFloat(tripInvTO.getOtherExpense()) + "";
        totalAmount = Float.parseFloat(totalFreightAmount) +  totalAmount;
        
        headCell6 = new PdfPCell(new Phrase(totalFreightAmount, FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell6.setFixedHeight(20);
        headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell6.disableBorderSide(2);
        innertable2.addCell(headCell6);
        
        }
                 
       PdfPCell headCell7= new PdfPCell(new Phrase("Some text here"));
        

        headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell7.setFixedHeight(20);
        headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell7.disableBorderSide(2);
        innertable2.addCell(headCell7);
        
        headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell7.setFixedHeight(20);
        headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell7.disableBorderSide(2);
        innertable2.addCell(headCell7);
            
        headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell7.setFixedHeight(20);
        headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell7.disableBorderSide(2);
        innertable2.addCell(headCell7);
        headCell7 = new PdfPCell(new Phrase(totalContainerQty+"", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell7.setFixedHeight(20);
        headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
        
        
        innertable2.addCell(headCell7);
        headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell7.setFixedHeight(20);
        headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell7.disableBorderSide(2);
        innertable2.addCell(headCell7);
        headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell7.setFixedHeight(20);
        headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell7.disableBorderSide(2);
        innertable2.addCell(headCell7);
        
        headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell7.setFixedHeight(20);
        headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell7.disableBorderSide(2);
        innertable2.addCell(headCell7);
        
        headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell7.setFixedHeight(20);
        headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell7.disableBorderSide(2);
        innertable2.addCell(headCell7);
        headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell7.setFixedHeight(20);
        headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell7);
        headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell7.setFixedHeight(20);
        headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell7.disableBorderSide(2);
        innertable2.addCell(headCell7);
        headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell7.setFixedHeight(20);
        headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell7.disableBorderSide(2);
        innertable2.addCell(headCell7);
        
        
        
        
        headCell7 = new PdfPCell(new Phrase(totalAmount+"", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell7.setFixedHeight(20);
        headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell7.disableBorderSide(2);
        innertable2.addCell(headCell7);              
        
      
        
        String RUPEES = "";
         NumberWords numberWords = new NumberWords();
        numberWords.setRoundedValue(String.valueOf(java.lang.Math.ceil(totalAmount)));
        numberWords.setNumberInWords(String.valueOf(java.lang.Math.ceil(totalAmount)));
        RUPEES = numberWords.getNumberInWords();

                System.out.println("i m here 1");
        
          PdfPCell headCell8 = new PdfPCell(new Phrase("Some text here"));
        
        headCell8 = new PdfPCell(new Phrase("Amount Chargeable(In words)\n" + "Rs."+RUPEES+"\n\n"+"Remarks\n"+""+"Being Road Transportation Charges Of"+containerTypeName +" "+ movementType+"\n\n"
+""+" Declaration\n" +
"1.All diputes are subject to Delhi Jurisdition\n" +
"2. Input credit on inputs, capital goods and input services, used for providing the taxable service, has not been taken.\n" +
"3. PAN No.AACCB8054G.\n" +
"4. GST Ref No.06AACCB8054G1ZT.\n" +
"5. Tax is payable under RCM.\n" +
"6. We declare that GST input credit on inputs, capital goods and input services used for providing the subject taxable service has not been taken by us under the provisions of the Input tax credit rules of GST. (Notification No.11/2017-Central Tax-rate dt. 28th June 2017 and Notification No.11/2017-State Tax -rate dt. 29th June 2017 ).\n" +
"7. In case of any discrepancy in this invoice, kindly intimate within 7 days from receipt or else it will be deemed to be accepted.\n"+"\n"+
                "                                                                                                                                                                       For International Cargo Terminals And Rail Infrastructure Pvt.Ltd.\n\n"+
                "Ref Code:"+ userName+"                                                                                                                                                                                                             "+"Authorised Signature\n", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell8.setFixedHeight(130);
          System.out.println("i m here 2");
        headCell8.setHorizontalAlignment(Element.ALIGN_LEFT);
       // headCell8.setBorder(Rectangle.NO_BORDER);
//        headCell8.disableBorderSide(Rectangle.TOP);
//        headCell8.disableBorderSide(Rectangle.LEFT);
//        headCell8.disableBorderSide(Rectangle.RIGHT);
        headCell8.disableBorderSide(Rectangle.BOTTOM);
        innertable3.addCell(headCell8);
         // headCell8.setBorder(Rectangle.RIGHT);  
//headCell8.setBorder(Rectangle.LEFT);  
//headCell8.setBorder(Rectangle.TOP);  

          headCell8 = new PdfPCell(new Phrase("Some text here"));
          Image imgsign = Image.getInstance("images/Ragvendra.jpg");
          imgsign.scalePercent(80);
          headCell8.setFixedHeight(50);
           headCell8.addElement(new Chunk(imgsign, 400, -10));
         //   headCell8.setHorizontalAlignment(Element.ALIGN_RIGHT);
           //  headCell8.setBorder(Rectangle.LEFT); 
        //  headCell8.setBorder(Rectangle.BOTTOM); 
        //   headCell8.setBorder(Rectangle.TOP);
          headCell8.disableBorderSide(Rectangle.TOP);
          innertable3.addCell(headCell8);
          System.out.println("i m here 3");
        
        
//         PdfPCell headCellSign = new PdfPCell(new Phrase("Some text here"));
//         headCellSign.setFixedHeight(30);
//          Image imgsign = Image.getInstance("images/Ragvendra.jpg");
//          imgsign.scalePercent(90);
//         headCellSign.addElement(new Chunk(imgsign, 370, -10));
//        headCellSign.setHorizontalAlignment(Element.ALIGN_RIGHT);
//         headCellSign.setBorder(Rectangle.RIGHT); 
//          headCellSign.setBorder(Rectangle.LEFT); 
//           headCellSign.setBorder(Rectangle.BOTTOM); 
//           headCellSign.setBorder(Rectangle.NO_BORDER); 
//           innertableSign.addCell(headCellSign);
        
      
        
        
         PdfPCell headCell9 = new PdfPCell(new Phrase("Some text here"));
        
        headCell9 = new PdfPCell(new Phrase("Regd.Office: Godrej Coliseum,Office No. 801,8th Floor,C-Wing,Behind Everard Nagar,off Somaiya Hospital Road,Sion (East)Mumbai-400022 ", FontFactory.getFont(FontFactory.HELVETICA, 5)));
        headCell9.setFixedHeight(10);
        headCell9.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell9.setBorder(Rectangle.NO_BORDER);  
headCell9.setBorder(Rectangle.LEFT);  
headCell9.setBorder(Rectangle.TOP);  
headCell9.setBorder(Rectangle.NO_BORDER);
//         headCell9.disableBorderSide(1);
//        headCell9.disableBorderSide(2);
//        headCell9.disableBorderSide(3);
//        headCell9.disableBorderSide(4);
        innertable4.addCell(headCell9);
          System.out.println("i m here 5");
       // Paragraph paragraph = new Paragraph();
    //    paragraph.add(new Chunk("hello!.This is Nithya"));
      //  document.add(paragraph);
        document.add(table);
        document.add(innertable);
        document.add(innerHeadertable);
        document.add(innertable1);
        document.add(innertable2);
        document.add(innertable3);
      //  document.add(innertableSign);
        document.add(innertable4);
       // document.add(innertable5);
        document.close();
        //end
          multipart = em.addMultipleAttachments(movementType+"Invoice-"+invoicecode+".pdf", outputStream,multipart,p,key,chain);        
        }
               // }
        String recTo1 = mailId;
        String recCc1 = ThrottleConstants.tripPlannedCCMailId;
        String recBcc1 = ThrottleConstants.tripPlannedBccMailId;
       // ownLeasedTripCount = schedulerDAO.getOwnLeasedTripCount();
        String contentData = "";
        StringBuffer contentSB = new StringBuffer();
        contentSB.append("<tr>");
        contentSB.append("<th >Date</th>");
        contentSB.append("<th >Name of Transporter</th>");
        contentSB.append("<th >No. of vehicle Used</th>");
        contentSB.append("<th >No. of Trips</th>");
        contentSB.append("<th >cumulative Trips</th>");
        contentSB.append("<th >Avg. Trips</th>");
        contentSB.append("</tr>");
        contentData = "Hi ,<br>Below is the Trip Details: <table border='1' cellspacing='4' cellpadding='1' width='600px'>" + contentSB.toString() + "</table>";
         System.out.println(contentData);
         String bodyMsg= "Dear Ma'am / Sir, Greetings!!!"+
                 "\n \n"+ 
               "Many thanks for your business support. Please find attached our Invoice for Transport of your containers. Kindly go through the invoice and if you find any discrepancies please revert within 48 hours to the below mentioned ID’s in mail signatures.We value your association."+
                 "\n"+
       "Thanking You,";
         if(!invoiceIds.isEmpty()){
        em.sendMultipleAttachPDFMail( recTo1, recCc1, recBcc1, billingParty+"-TMS Invoice ",bodyMsg, multipart);
         }
         for(String invId:invoiceIds ){
             schedulerDAO.updateInvoiceEmailNotification(invId, "Y");
                   }
             }else{
             ArrayList customerList = schedulerDAO.getInvoiceCustomerList(tripTO.getFromDate(),tripTO.getToDate());
               if (customerList.size() > 0) {
                Iterator itr1 = customerList.iterator();
                SchedulerTO schedulerTO = new SchedulerTO();
               
                 while (itr1.hasNext()) {
                      try {
                 schedulerTO = (SchedulerTO) itr1.next();
                customerId = schedulerTO.getBillingParty();
                ArrayList invoiceIdList = new ArrayList();
                multipart = new MimeMultipart();
                  invoiceIdList = schedulerDAO.getInvoiceIdList(customerId,tripTO.getFromDate(),tripTO.getToDate());
                          System.out.println("invoiceIdList for customerId "+customerId + " is"+invoiceIdList.size());
                 String mailId = schedulerDAO.getCustomerMailId(customerId);
                 Iterator itr2 = invoiceIdList.iterator();
               SchedulerTO schedulerTO1 = new SchedulerTO();
                while (itr2.hasNext()) {                           // invoice iteration
                schedulerTO1 = (SchedulerTO) itr2.next();
                    System.out.println("invoiceId$"+invoiceId);
                invoiceId = schedulerTO1.getInvoiceId();
                billingTO.setInvoiceId(invoiceId);
                tripTO.setInvoiceId(invoiceId);
                invoiceIds.add(invoiceId);
                    System.out.println("tripTO.getInvoiceId"+tripTO.getInvoiceId());
                    System.out.println("invoiceIds size:"+invoiceIds.size());
                ArrayList invoiceHeader = tripDAO.getOrderInvoiceHeader(tripTO);
                ArrayList invoiceDetails = tripDAO.getInvoiceDetails(tripTO);
                ArrayList invoiceDetailExpenses = tripDAO.getInvoiceDetailExpenses(tripTO);
               // ArrayList invoiceTaxDetails = billingDAO.getInvoiceGSTTaxDetails(billingTO);
            //    System.out.println("invoiceTaxDetailsSize:" + invoiceTaxDetails.size());
                String[] tempRemarks = null;
                Iterator itr = invoiceHeader.iterator();
                TripTO tpTO = new TripTO();

                while (itr.hasNext()) {
                        tpTO = (TripTO) itr.next();
                        billingParty = tpTO.getBillingParty() ;
                        creditNoteNo = tpTO.getCreditNoteNo();
                        creditNoteDate = tpTO.getCreditNoteDate();
                        createdBy = tpTO.getCreatedBy();
                        customerName = tpTO.getCustomerName();
                        taxValue = tpTO.getTaxValue();
                        customerAddress = tpTO.getCustomerAddress();
                        panNo = tpTO.getPanNo();
                        gstNo = tpTO.getGstNo();
                        cityName = tpTO.getCityName();
                        state = tpTO.getState();
                        invoicecode = tpTO.getInvoiceCode();
                        consignmentOrderNos = tpTO.getcNotes();
                        consignmentOrderDate =  tpTO.getConsignmentOrderDate();
                        billDate =  tpTO.getBillDate();
                        movementType = tpTO.getMovementType();
                        containerTypeName = tpTO.getContainerTypeName();
                        billingType = tpTO.getBillingType();
                        userName = tpTO.getUserName();
                        billingState = tpTO.getBillingState();
                        remarks = tpTO.getRemarks();
                
                if (!"".equals(tpTO.getRemarks())) {
                    tempRemarks = tpTO.getRemarks().split(",");
                    if (tempRemarks.length >= 1) {
                        firstRemarks = tempRemarks[0];
                        System.out.println("firstRemarks:" + tempRemarks[0]);
                    }
                    if (tempRemarks.length >= 2) {
                         secondRemarks = tempRemarks[1];
                    }
                    if (tempRemarks.length >= 3) {
                         thirdRemarks = tempRemarks[2];
                    }
                }

            }
                
            ArrayList invoiceDetailsList = new ArrayList();
            invoiceDetailsList = tripDAO.getOrderInvoiceDetailsList(tripTO);
            ArrayList tripDetails = new ArrayList();
         //   tripDetails = billingDAO.getTripDetails(billingTO);
          //  System.out.println("invoiceDetailsList.size():" + invoiceDetailsList.size());
                
                
               
          
            
            
          
       ByteArrayOutputStream outputStream=new ByteArrayOutputStream();
       Document document = new Document();
        PdfWriter.getInstance(document, outputStream);
            System.out.println("HI.pdf is called");
        document.open();
        //content of pdf start
        
        PdfPTable table = new PdfPTable(2);
        PdfPTable innertable = new PdfPTable(1);
        PdfPTable innertable1 = new PdfPTable(1);

        PdfPTable innertable2 = new PdfPTable(12);
        PdfPTable innertable3 = new PdfPTable(1);
        PdfPTable innertableSign = new PdfPTable(1);
        PdfPTable innertable4 = new PdfPTable(1);
        PdfPTable innertable5 = new PdfPTable(1);
        PdfPTable innerHeadertable = new PdfPTable(3);
        
        table.setTotalWidth(new float[]{150,350});
        //table.setTotalWidth(new float[]{191,191,90,92});
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.setLockedWidth(true);
        innerHeadertable.setTotalWidth(new float[]{300,100,100});
        //table.setTotalWidth(new float[]{191,191,90,92});
        innerHeadertable.setHorizontalAlignment(Element.ALIGN_LEFT);
        innerHeadertable.setLockedWidth(true);
        
        innertable.setTotalWidth(new float[]{500});
        innertable.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable.setLockedWidth(true);
        
        innertable1.setTotalWidth(new float[]{500});
        innertable1.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable1.setLockedWidth(true);
        
        innertable2.setTotalWidth(new float[]{40, 40,60,40, 40,40, 40,40, 40,40, 40,40});
        innertable2.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable2.setLockedWidth(true);
        innertable3.setTotalWidth(new float[]{500});
        innertable3.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable3.setLockedWidth(true);
        innertable4.setTotalWidth(new float[]{500});
        innertable4.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable4.setLockedWidth(true);
        innertable5.setTotalWidth(new float[]{500});
        innertable5.setHorizontalAlignment(Element.ALIGN_LEFT);
        innertable5.setLockedWidth(true);
//        document.addTitle("Test PDF NITHYA");
//        document.addSubject("Testing email PDF NITHYA");
//        document.addKeywords("iText, email");
//        document.addAuthor("Nithya");
//        document.addCreator("Nithya");
        
          Image img = Image.getInstance("images/dict-logo11.png");//dict-logo11.png
          img.scalePercent(30);
         PdfPCell headCell1 = new PdfPCell(new Phrase("Header", FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD, GrayColor.BLACK)));
          headCell1.setFixedHeight(40);
//         headCell1.disableBorderSide(4);
        //headCell.setBackgroundColor(Color.lightGray);
        headCell1.addElement(new Chunk(img, 10, -10));
        headCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(headCell1);
        
        headCell1 = new PdfPCell(new Phrase(" International Cargo Terminals And Rail Infrastructure Pvt.Ltd. \n Panchi Gujran,Tehsil-Ganaur, Dist-Sonepat-131101 \n CIN No:U63040MH2006PTC159885", FontFactory.getFont(FontFactory.HELVETICA, 8, Font.BOLD, GrayColor.BLACK)));
        headCell1.setFixedHeight(40);
        
         headCell1.disableBorderSide(4);
        //headCell.setBackgroundColor(Color.lightGray);
       
        headCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(headCell1);
        
        PdfPCell headCell = new PdfPCell(new Phrase("Some text here"));
        
        headCell = new PdfPCell(new Phrase("INVOICE", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell.setFixedHeight(20);
        headCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        innertable.addCell(headCell);
        
        
        PdfPCell headCell3 = new PdfPCell(new Phrase("Billed to \n" +billingParty+"\n"+customerAddress+"\nBilling State"+billingState+"\nPAN No:"+panNo+"\nGST No:"+gstNo, FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
          headCell3.setFixedHeight(40);
         
//         headCell3.disableBorderSide(4);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        innerHeadertable.addCell(headCell3);
        
        headCell3 = new PdfPCell(new Phrase("Invoice No" +invoicecode+"\n\n "+billingType+"\n\n SAC Code: 996791 \n\n", FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
          headCell3.setFixedHeight(40);
         
         headCell3.disableBorderSide(4);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        innerHeadertable.addCell(headCell3);
        
        
        
        headCell3 = new PdfPCell(new Phrase("Bill Date: "+billDate+"\n \n Dispatched Through::", FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
          headCell3.setFixedHeight(40);
        
         headCell3.disableBorderSide(4);
        headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        innerHeadertable.addCell(headCell3);
        
        
          PdfPCell headCell4 = new PdfPCell(new Phrase("Some text here"));
        
        headCell4 = new PdfPCell(new Phrase("PARTICULARS", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell4.setFixedHeight(20);
        headCell4.setHorizontalAlignment(Element.ALIGN_CENTER);
        innertable1.addCell(headCell4);
        
        
        
        
        
        PdfPCell headCell5 = new PdfPCell(new Phrase("Some text here"));
        

        headCell5 = new PdfPCell(new Phrase("GR.No", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell5);
        
        headCell5 = new PdfPCell(new Phrase("Date", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell5);
            
        headCell5 = new PdfPCell(new Phrase("Description", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell5);
        headCell5 = new PdfPCell(new Phrase("No.Of Container ", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell5);
        headCell5 = new PdfPCell(new Phrase("Commodity Category  ", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell5);
        headCell5 = new PdfPCell(new Phrase("Commodity", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell5);
        headCell5 = new PdfPCell(new Phrase("Freight", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell5);
        headCell5 = new PdfPCell(new Phrase("Toll Tax", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell5);
        headCell5 = new PdfPCell(new Phrase("Detention Charges", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell5);
        headCell5 = new PdfPCell(new Phrase("Weightment", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell5);
        
        headCell5 = new PdfPCell(new Phrase("Other Charges", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell5);
        
        headCell5 = new PdfPCell(new Phrase("Total Amount(Rs.)", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell5.setFixedHeight(20);
        headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell5);
        
        String totalFreightAmount = "";
        Float totalAmount = 0.0f;
        int totalContainerQty = 0;
          Iterator itrInv = invoiceDetailsList.iterator();
                           TripTO tripInvTO = null;
                           int i = 1;
        while (itrInv.hasNext()) {
        tripInvTO = (TripTO)itrInv.next();
        PdfPCell headCell6= new PdfPCell(new Phrase("Some text here"));
        

        headCell6 = new PdfPCell(new Phrase(tripInvTO.getGrNumber(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell6.setFixedHeight(20);
        headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell6.disableBorderSide(2);
        innertable2.addCell(headCell6);
        
        headCell6 = new PdfPCell(new Phrase(tripInvTO.getGrDate(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell6.setFixedHeight(20);
        headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell6.disableBorderSide(2);
        innertable2.addCell(headCell6);
            
        headCell6 = new PdfPCell(new Phrase(tripInvTO.getDestination(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell6.setFixedHeight(20);
        headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell6.disableBorderSide(2);
        innertable2.addCell(headCell6);
        headCell6 = new PdfPCell(new Phrase(tripInvTO.getContainerQty(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell6.setFixedHeight(20);
        headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
        
        totalContainerQty =  Integer.parseInt(tripInvTO.getContainerQty()) +totalContainerQty;
                        headCell6.disableBorderSide(2);
                           System.out.println("tripInvTO.getCommodityCategory():"+tripInvTO.getCommodityCategory());
        innertable2.addCell(headCell6);
        headCell6 = new PdfPCell(new Phrase(tripInvTO.getCommodityCategory(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell6.setFixedHeight(20);
        headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
         headCell6.disableBorderSide(2);
        innertable2.addCell(headCell6);
        headCell6 = new PdfPCell(new Phrase(tripInvTO.getArticleName(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell6.setFixedHeight(20);
        headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell6.disableBorderSide(2);
        innertable2.addCell(headCell6);
        
        headCell6 = new PdfPCell(new Phrase(tripInvTO.getFreightAmount(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell6.setFixedHeight(20);
        headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell6.disableBorderSide(2);
        innertable2.addCell(headCell6);
        
        headCell6 = new PdfPCell(new Phrase(tripInvTO.getTollTax(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell6.setFixedHeight(20);
        headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell6.disableBorderSide(2);
        innertable2.addCell(headCell6);
        headCell6 = new PdfPCell(new Phrase(tripInvTO.getDetaintion(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell6.setFixedHeight(20);
        headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell6);
        headCell6 = new PdfPCell(new Phrase(tripInvTO.getWeightmentExpense(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell6.setFixedHeight(20);
        headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell6.disableBorderSide(2);
        innertable2.addCell(headCell6);
        headCell6 = new PdfPCell(new Phrase(tripInvTO.getOtherExpense(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell6.setFixedHeight(20);
        headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell6.disableBorderSide(2);
        innertable2.addCell(headCell6);
        
        
        totalFreightAmount = Float.parseFloat(tripInvTO.getFreightAmount()) + Float.parseFloat(tripInvTO.getTollTax()) + Float.parseFloat(tripInvTO.getDetaintion()) +
                Float.parseFloat(tripInvTO.getWeightmentExpense()) + Float.parseFloat(tripInvTO.getOtherExpense()) + "";
        totalAmount = Float.parseFloat(totalFreightAmount) +  totalAmount;
        
        headCell6 = new PdfPCell(new Phrase(totalFreightAmount, FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell6.setFixedHeight(20);
        headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell6.disableBorderSide(2);
        innertable2.addCell(headCell6);
        
        }
                 
       PdfPCell headCell7= new PdfPCell(new Phrase("Some text here"));
        

        headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell7.setFixedHeight(20);
        headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell7.disableBorderSide(2);
        innertable2.addCell(headCell7);
        
        headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell7.setFixedHeight(20);
        headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell7.disableBorderSide(2);
        innertable2.addCell(headCell7);
            
        headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell7.setFixedHeight(20);
        headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell7.disableBorderSide(2);
        innertable2.addCell(headCell7);
        headCell7 = new PdfPCell(new Phrase(totalContainerQty+"", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell7.setFixedHeight(20);
        headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
        
        
        innertable2.addCell(headCell7);
        headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell7.setFixedHeight(20);
        headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell7.disableBorderSide(2);
        innertable2.addCell(headCell7);
        headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell7.setFixedHeight(20);
        headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell7.disableBorderSide(2);
        innertable2.addCell(headCell7);
        
        headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell7.setFixedHeight(20);
        headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell7.disableBorderSide(2);
        innertable2.addCell(headCell7);
        
        headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell7.setFixedHeight(20);
        headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell7.disableBorderSide(2);
        innertable2.addCell(headCell7);
        headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell7.setFixedHeight(20);
        headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell3.disableBorderSide(2);
        innertable2.addCell(headCell7);
        headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell7.setFixedHeight(20);
        headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                        headCell7.disableBorderSide(2);
        innertable2.addCell(headCell7);
        headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell7.setFixedHeight(20);
        headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell7.disableBorderSide(2);
        innertable2.addCell(headCell7);
        
        
        
        
        headCell7 = new PdfPCell(new Phrase(totalAmount+"", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell7.setFixedHeight(20);
        headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell7.disableBorderSide(2);
        innertable2.addCell(headCell7);              
        
      
        
        String RUPEES = "";
         NumberWords numberWords = new NumberWords();
        numberWords.setRoundedValue(String.valueOf(java.lang.Math.ceil(totalAmount)));
        numberWords.setNumberInWords(String.valueOf(java.lang.Math.ceil(totalAmount)));
        RUPEES = numberWords.getNumberInWords();

                System.out.println("i m here 1");
        
          PdfPCell headCell8 = new PdfPCell(new Phrase("Some text here"));
       
        headCell8 = new PdfPCell(new Phrase("Amount Chargeable(In words)\n" + "Rs."+RUPEES+"\n\n"+"Remarks\n"+""+"Being Road Transportation Charges Of"+containerTypeName +" "+ movementType+"\n\n"
+""+" Declaration\n" +
"1.All diputes are subject to Delhi Jurisdition\n" +
"2. Input credit on inputs, capital goods and input services, used for providing the taxable service, has not been taken.\n" +
"3. PAN No.AACCB8054G.\n" +
"4. GST Ref No.06AACCB8054G1ZT.\n" +
"5. Tax is payable under RCM.\n" +
"6. We declare that GST input credit on inputs, capital goods and input services used for providing the subject taxable service has not been taken by us under the provisions of the Input tax credit rules of GST. (Notification No.11/2017-Central Tax-rate dt. 28th June 2017 and Notification No.11/2017-State Tax -rate dt. 29th June 2017 ).\n" +
"7. In case of any discrepancy in this invoice, kindly intimate within 7 days from receipt or else it will be deemed to be accepted.\n"+"\n"+
                "                                                                                                                                                                       For International Cargo Terminals And Rail Infrastructure Pvt.Ltd.\n\n"+
                "Ref Code:"+ userName+"                                                                                                                                                                                                             "+"Authorised Signature\n", FontFactory.getFont(FontFactory.HELVETICA, 6)));
        headCell8.setFixedHeight(130);
          System.out.println("i m here 2");
          
        headCell8.setHorizontalAlignment(Element.ALIGN_LEFT);
       // headCell8.setBorder(Rectangle.NO_BORDER);
//        headCell8.disableBorderSide(Rectangle.TOP);
//        headCell8.disableBorderSide(Rectangle.LEFT);
//        headCell8.disableBorderSide(Rectangle.RIGHT);
        headCell8.disableBorderSide(Rectangle.BOTTOM);
        innertable3.addCell(headCell8);
         // headCell8.setBorder(Rectangle.RIGHT);  
//headCell8.setBorder(Rectangle.LEFT);  
//headCell8.setBorder(Rectangle.TOP);  

          headCell8 = new PdfPCell(new Phrase("Some text here"));
          Image imgsign = Image.getInstance("images/Ragvendra.jpg");
          imgsign.scalePercent(80);
          headCell8.setFixedHeight(50);
           headCell8.addElement(new Chunk(imgsign, 400, -10));
         //   headCell8.setHorizontalAlignment(Element.ALIGN_RIGHT);
           //  headCell8.setBorder(Rectangle.LEFT); 
        //  headCell8.setBorder(Rectangle.BOTTOM); 
        //   headCell8.setBorder(Rectangle.TOP);
          headCell8.disableBorderSide(Rectangle.TOP);
          innertable3.addCell(headCell8);
          System.out.println("i m here 3");
        
        
//        
//         PdfPCell headCellSign = new PdfPCell(new Phrase("Some text here"));
//         headCellSign.setFixedHeight(30);
//          Image imgsign = Image.getInstance("images/Ragvendra.jpg");
//          imgsign.scalePercent(30);
//         headCellSign.addElement(new Chunk(imgsign, 20, 100));
//        headCellSign.setHorizontalAlignment(Element.ALIGN_RIGHT);
//          headCellSign.setBorder(Rectangle.NO_BORDER);  
//        innertableSign.addCell(headCellSign);
//        
        
        PdfPCell headCell9 = new PdfPCell(new Phrase("Some text here"));
        
        headCell9 = new PdfPCell(new Phrase("Regd.Office: Godrej Coliseum,Office No. 801,8th Floor,C-Wing,Behind Everard Nagar,off Somaiya Hospital Road,Sion (East)Mumbai-400022 ", FontFactory.getFont(FontFactory.HELVETICA, 5)));
        headCell9.setFixedHeight(10);
        headCell9.setHorizontalAlignment(Element.ALIGN_LEFT);
        headCell9.setBorder(Rectangle.NO_BORDER);  
        headCell9.setBorder(Rectangle.LEFT);  
        headCell9.setBorder(Rectangle.TOP);  
        headCell9.setBorder(Rectangle.NO_BORDER);
//         headCell9.disableBorderSide(1);
//        headCell9.disableBorderSide(2);
//        headCell9.disableBorderSide(3);
//        headCell9.disableBorderSide(4);
        innertable4.addCell(headCell9);
          System.out.println("i m here 5");
       // Paragraph paragraph = new Paragraph();
    //    paragraph.add(new Chunk("hello!.This is Nithya"));
      //  document.add(paragraph);
        document.add(table);
        document.add(innertable);
        document.add(innerHeadertable);
        document.add(innertable1);
        document.add(innertable2);
        document.add(innertable3);
        document.add(innertableSign);
        document.add(innertable4);
       // document.add(innertable5);
        document.close();
        //end
          multipart = em.addMultipleAttachments(movementType+"Invoice-"+invoicecode+".pdf", outputStream,multipart,p,key,chain);        
        }
               // }
        String recTo1 = mailId;
        String recCc1 = ThrottleConstants.tripPlannedCCMailId;
        String recBcc1 = ThrottleConstants.tripPlannedBccMailId;
       // ownLeasedTripCount = schedulerDAO.getOwnLeasedTripCount();
      
         String bodyMsg= "Dear Ma'am / Sir, Greetings!!!"+
                 "\n \n"+ 
               "Many thanks for your business support. Please find attached our Invoice for Transport of your containers. Kindly go through the invoice and if you find any discrepancies please revert within 48 hours to the below mentioned ID’s in mail signatures.We value your association."+
                 "\n"+
       "Thanking You,";
         if(!invoiceIds.isEmpty()){
        em.sendMultipleAttachPDFMail( recTo1, recCc1, recBcc1, billingParty+"-TMS Invoice ",bodyMsg, multipart);
         }
         for(String invId:invoiceIds ){
             schedulerDAO.updateInvoiceEmailNotification(invId, "Y");
                   }
                 } catch(Exception e){
                          System.out.println("Null pointer exception in sending mail.updating invoice status .. ");
           for(String invId:invoiceIds ){
             schedulerDAO.updateInvoiceEmailNotification(invId, "N");
                   }
           
                         }
             }
               
               }
               }
             
     // em.sendPDFMail(invoicecode+".pdf", recTo1, recCc1, recBcc1, "Invoice PDF","Hi,", outputStream);
     //}
    } catch(NullPointerException n ){
           System.out.println("Null pointer exception in sending mail.updating invoice status .. ");
           for(String invId:invoiceIds ){
             schedulerDAO.updateInvoiceEmailNotification(invId, "N");
                   }
    }
            catch (Exception e) {
        e.printStackTrace();
        System.out.println("Exception in email Send Call: " + e.getMessage());
         System.out.println("Null pointer exception in sending mail.updating invoice status .. ");
           for(String invId:invoiceIds ){
             schedulerDAO.updateInvoiceEmailNotification(invId, "N");
                   }
           
    }
}
     
      public ArrayList getDailyVehicleUtilised() throws FPRuntimeException, FPBusinessException {
        System.out.println("getDailyVehicleUtilised@@@@@@");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        String systemTime = sdf.format(c.getTime()).toString();
        System.out.println("systemTime = " + systemTime);
        String[] dateTemp = systemTime.split(" ");
        String[] timeTemp = dateTemp[1].split(":");
        ArrayList dvmDetailsDays = new ArrayList();

//             -------DVM scheduler----------
        String rangeStartDate = "";
        String rangeStartDate1 = "";
        String Filename = "";
        String File_Name = "";
        String contentdata = "";
        StringBuffer contentSB = new StringBuffer();
        SendEmail em = new SendEmail();
        ArrayList alertsDetails = new ArrayList();
        ArrayList alertsEmail = new ArrayList();

        File theDir = new File("c:\\VehicleUtilisedAlerts");

        if (!theDir.exists()) {
            boolean result = theDir.mkdir();
            if (result) {
                System.out.println("c:\\VehicleUtilisedAlerts\\DIR created");
            }

        }
//        }

        try {
            int i = 0;

            System.out.println("inside processAlerts function");
            final Properties properties = new Properties();
            String recTo =  ThrottleConstants.vehicleutilisedToMailId;
            String recCc =  ThrottleConstants.vehicleutilisedCCMailId;
            String recBcc = ThrottleConstants.vehicleutilisedBccMailId;
            System.out.println("recipients recTo:  " + recTo);
            System.out.println("recipients recCc: " + recCc);
            System.out.println("recipients recBcc: " + recBcc);

            Date dNow1 = new Date();
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            SimpleDateFormat processDateFormat11 = new SimpleDateFormat("yyyy-MM-dd");
            String dt = dateFormat1.format(dNow1).toString();
            String substr = dt.substring(11, 13);

            int MILLIS_IN_DAY = 1000 * 60 * 60 * 24 * 7;
//                Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
            SimpleDateFormat processDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat processDateFormat1 = new SimpleDateFormat("dd-MM-yyyy");
            rangeStartDate1 = processDateFormat1.format(date.getTime());
            System.out.println("rangeStartDate1@@@" + rangeStartDate1);
            rangeStartDate = processDateFormat.format(date.getTime());
            System.out.println("rangeStartDate@@@" + rangeStartDate);
            System.out.println("recipients: " + rangeStartDate);

            int uStatus = 0;
            ReportTO reportTO = null;
            SchedulerTO schTO = null;

            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("hh:mm:a");
            SimpleDateFormat fHour = new SimpleDateFormat("hh");
            System.out.println("Current Date: " + ft.format(dNow));
            System.out.println("Current Hour: " + fHour.format(dNow));
            String curTime = ft.format(dNow);
            String[] temp = null;
            temp = curTime.split(":");
            int curHour = Integer.parseInt(fHour.format(dNow));
            System.out.println("curTime = " + curTime);
            System.out.println("curHour = " + curHour);
            String curMeridian = temp[2];
            System.out.println("curMeridian = " + curMeridian);

            int time = 0;
            int everyHourStatus = 0;
            int oddEven = curHour % 2;
            if (oddEven > 0) {
                everyHourStatus = 0;
            } else {
                everyHourStatus = 1;
            }

            String timeNow = temp[0] + "_" + temp[1] + "_" + temp[2];
            String filename = "c:\\VehicleUtilisedAlerts\\" + "vehicle_utilised_Summary_" + rangeStartDate1 + ".xls";
            System.out.println("filename = " + filename);
            ArrayList getTotalOrderList = new ArrayList();
            ArrayList result1 = new ArrayList();
            getTotalOrderList = schedulerDAO.vehicleUtilReport();
            System.out.println("getTotalehicleUtilReport.size() = " + getTotalOrderList.size());
            if (getTotalOrderList.size() > 0) {
                result1.addAll(getTotalOrderList);
                System.out.println("Result Waiting = " + result1.size());
            }
            //Iterator itrVehicle = getTotalOrderList.iterator();
            schTO = null;

            System.out.println("result1.size() = " + result1.size());

            //first my_sheet start
           
            
            
            HSSFWorkbook my_workbook = new HSSFWorkbook();
            String sheetName = "vehicles" + rangeStartDate1;
            Sheet my_sheet = (Sheet) my_workbook.createSheet(sheetName);
            Row s1Row1 = my_sheet.createRow((short) 0);
            s1Row1.setHeightInPoints(50); // row hight
            
            HSSFCellStyle style = my_workbook.createCellStyle();
            style.setFillForegroundColor(HSSFColor.CORNFLOWER_BLUE.index);
            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            style.setRightBorderColor(IndexedColors.BLACK.getIndex());
            style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            style.setTopBorderColor(IndexedColors.BLACK.getIndex());
            style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            HSSFFont font = my_workbook.createFont();
            font.setFontName(HSSFFont.FONT_ARIAL);
            font.setFontHeightInPoints((short) 10);
            font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            font.setColor(HSSFColor.BLACK.index);
            style.setFont(font);
            
            Cell cellc1 = s1Row1.createCell((short) 0);
            cellc1.setCellValue("S.No");
            cellc1.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 0, (short) 4000); // cell width
            
            Cell cellc2 = s1Row1.createCell((short) 1);
            cellc2.setCellValue("Vehicle No");
            cellc2.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 1, (short) 4000); // cell width
            
            Cell cellc3 = s1Row1.createCell((short) 2);
            cellc3.setCellValue("Total Trips");
            cellc3.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 2, (short) 4000); // cell width
            
            Cell cellc4 = s1Row1.createCell((short) 3);
            cellc4.setCellValue("Transporter");
            cellc4.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 3, (short) 4000); // cell width
            
            Cell cellc5 = s1Row1.createCell((short) 4);
            cellc5.setCellValue("Total Days");
            cellc5.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 4, (short) 4000); // cell width
            
            Cell cellc6 = s1Row1.createCell((short) 5);
            cellc6.setCellValue("Total Hours");
            cellc6.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 5, (short) 4000); // cell width
            
            Cell cellc7 = s1Row1.createCell((short) 6);
            cellc7.setCellValue("Gr Date");
            cellc7.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 6, (short) 4000); // cell width
            
        
            
            Iterator itr = result1.iterator();
            schTO = null;
            int cntr = 1;
            int order_closed = 0;
            int order_end = 0;
            int order_progress = 0;
            int order_freezed = 0;
            int order_created = 0;

            while (itr.hasNext()) {
                schTO = (SchedulerTO) itr.next();

                s1Row1 = my_sheet.createRow((short) cntr);
                s1Row1.createCell((short) 0).setCellValue(cntr);
                s1Row1.createCell((short) 1).setCellValue(schTO.getVehicleNo());
                s1Row1.createCell((short) 2).setCellValue(schTO.getTotalTrips());
             
                s1Row1.createCell((short) 3).setCellValue(schTO.getTransporter());
                s1Row1.createCell((short) 4).setCellValue(schTO.getTotalDays());
                s1Row1.createCell((short) 5).setCellValue(schTO.getTotalHours());
                s1Row1.createCell((short) 6).setCellValue(schTO.getGrDate());
                


                cntr++;


            }
            System.out.println("Your excel 1 Sheet  created");
            
            my_workbook.setSheetOrder("vehicles" + rangeStartDate1, 0);
            System.out.println("rangeStartDate111112:" + rangeStartDate1);

            // my_workbook.setSheetOrder("Future Trip" + rangeStartDate1, 6);
            System.out.println("testing in the excel sheet creation");
            FileOutputStream fileOut = new FileOutputStream(filename);
            my_workbook.write(fileOut);
            fileOut.close();

            System.out.println("Your excel file has been generated!");

            File_Name = "c:\\VehicleUtilisedAlerts\\" + "vehicle_utilised_Summary_" + rangeStartDate1 + ".xls";
            Filename = "vehicle_utilised_Summary_" + rangeStartDate1 + " " + ft.format(dNow) + ".xls";
            System.out.println(File_Name);
            System.out.println(Filename);
            String bcc = "";
            String mailContent = "vehicle_utilised_Summary Alerts" + rangeStartDate1 + " " + ft.format(dNow);
           
//                recTo = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts.to");
//                recCc = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts.cc");
            Date dNow11 = new Date();
            SimpleDateFormat dateFormat11 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            SimpleDateFormat processDateFormat111 = new SimpleDateFormat("yyyy-MM-dd");
            String dt1 = dateFormat11.format(dNow1).toString();
            String substr1 = dt.substring(11, 13);
               //  String compare=substr+" 10:30:00";
            // substr = substr + " 10:00:00";
//           if (substr1.trim().equals("17")) {
//                    recTo = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts.to");
//                    recCc = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts.cc");
//                } else {
//                    recTo = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts1.to");
//                    recCc = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts1.cc");
//                }

           // recBcc = "";
            String emailFormat1="";
            String emailFormat2="";
            
            String atlMsg = "TMS ALERTS - vehicle_utilised Summary alert as on - " + rangeStartDate1;
            String dueAlt = "vehicle_utilised_Summary_ alert as on " + rangeStartDate1;
            contentdata = "Hi Greetings From TMS, <br><br>";
            contentdata = contentdata + "<table border='1' cellspacing='4' cellpadding='1' width='600px'>" + contentSB.toString() + "</table>";
            contentdata = contentdata + "<br><br> Team Throttle";
            emailFormat1 = "<html>"
                    + "<body>"
                    + "<p style='font-style: italic;'><b>Dear Sir/Madam, <br><br>Please find below Attatchement of vehicle_utilised alert<br><br/>";
            
            emailFormat2 = emailFormat1 + "<br><br>Hi Greetings From TMS"
                    + "</body></html>";
            
            
            SchedulerTO schTO1 = new SchedulerTO();
            schTO1.setMailStatus("1");
            schTO1.setMailTypeId("2");
            schTO1.setMailIdTo(recTo);
            schTO1.setMailIdCc(recCc);
            schTO1.setMailIdBcc(recBcc);
            schTO1.setMailSubjectTo("vehicle_utilised_Summary_Report");
            schTO1.setMailSubjectCc("vehicle_utilised_Summary_Report");
            schTO1.setMailSubjectBcc("vehicle_utilised_Summary_Report");
            schTO1.setFilenameContent(File_Name);
            schTO1.setEmailFormat(emailFormat2);
            System.out.println("filenameContent" + Filename);
            System.out.println("emailFormat4" + emailFormat2);
            
            
            Integer mails =schedulerDAO.insertMailDetails(schTO1,1);
            System.out.println("insertMailDetails@@@"+mails);
            System.out.println("recBcc:--"+recBcc);
//            if (result1.size() > 0) {
            em.sendExcelMail(Filename, recTo, recCc, recBcc, mailContent,emailFormat2, my_workbook);
            System.out.println("closed");
//            }
            

        } catch (Exception excp) {
            System.out.println("ERROR STATUS   " + excp);

            excp.printStackTrace();
        }
        return dvmDetailsDays;
    }
      
      public ArrayList getContractDiffAlert() throws FPRuntimeException, FPBusinessException {
        System.out.println("getContractDiffAlert@@@@@@");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        String systemTime = sdf.format(c.getTime()).toString();
        System.out.println("systemTime = " + systemTime);
        String[] dateTemp = systemTime.split(" ");
        String[] timeTemp = dateTemp[1].split(":");
        ArrayList dvmDetailsDays = new ArrayList();

//             -------DVM scheduler----------
        String rangeStartDate = "";
        String rangeStartDate1 = "";
        String Filename = "";
        String File_Name = "";
        String contentdata = "";
        StringBuffer contentSB = new StringBuffer();
        SendEmail em = new SendEmail();
        ArrayList alertsDetails = new ArrayList();
        ArrayList alertsEmail = new ArrayList();

        File theDir = new File("c:\\ContractDiffAlert");

        if (!theDir.exists()) {
            boolean result = theDir.mkdir();
            if (result) {
                System.out.println("c:\\ContractDiffAlert\\DIR created");
            }

        }
//        }

        try {
            int i = 0;

            System.out.println("inside processAlerts function");
            final Properties properties = new Properties();
            String recTo =  ThrottleConstants.contractdiffToMailId;
            String recCc =  ThrottleConstants.contractdiffCCMailId;
            String recBcc = ThrottleConstants.contractdiffBccMailId;
            System.out.println("recipients recTo:  " + recTo);
            System.out.println("recipients recCc: " + recCc);
            System.out.println("recipients recBcc: " + recBcc);

            Date dNow1 = new Date();
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            SimpleDateFormat processDateFormat11 = new SimpleDateFormat("yyyy-MM-dd");
            String dt = dateFormat1.format(dNow1).toString();
            String substr = dt.substring(11, 13);

            int MILLIS_IN_DAY = 1000 * 60 * 60 * 24 * 7;
//                Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
            SimpleDateFormat processDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat processDateFormat1 = new SimpleDateFormat("dd-MM-yyyy");
            rangeStartDate1 = processDateFormat1.format(date.getTime());
            System.out.println("rangeStartDate1@@@" + rangeStartDate1);
            rangeStartDate = processDateFormat.format(date.getTime());
            System.out.println("rangeStartDate@@@" + rangeStartDate);
            System.out.println("recipients: " + rangeStartDate);

            int uStatus = 0;
            ReportTO reportTO = null;
            SchedulerTO schTO = null;

            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("hh:mm:a");
            SimpleDateFormat fHour = new SimpleDateFormat("hh");
            System.out.println("Current Date: " + ft.format(dNow));
            System.out.println("Current Hour: " + fHour.format(dNow));
            String curTime = ft.format(dNow);
            String[] temp = null;
            temp = curTime.split(":");
            int curHour = Integer.parseInt(fHour.format(dNow));
            System.out.println("curTime = " + curTime);
            System.out.println("curHour = " + curHour);
            String curMeridian = temp[2];
            System.out.println("curMeridian = " + curMeridian);

            int time = 0;
            int everyHourStatus = 0;
            int oddEven = curHour % 2;
            if (oddEven > 0) {
                everyHourStatus = 0;
            } else {
                everyHourStatus = 1;
            }

            String timeNow = temp[0] + "_" + temp[1] + "_" + temp[2];
            String filename = "c:\\ContractDiffAlert\\" + "contract_diff" + rangeStartDate1 + ".xls";
            System.out.println("filename = " + filename);
            ArrayList getTotalOrderList = new ArrayList();
            ArrayList result1 = new ArrayList();
            getTotalOrderList = schedulerDAO.rateDiffReport();
            System.out.println("getTotalrateDiffReport.size() = " + getTotalOrderList.size());
            if (getTotalOrderList.size() > 0) {
                result1.addAll(getTotalOrderList);
                System.out.println("Result Waiting = " + result1.size());
            }
            //Iterator itrVehicle = getTotalOrderList.iterator();
            schTO = null;

            System.out.println("result1.size() = " + result1.size());

            //first my_sheet start
           
            
            
            HSSFWorkbook my_workbook = new HSSFWorkbook();
            String sheetName = "contract" + rangeStartDate1;
            Sheet my_sheet = (Sheet) my_workbook.createSheet(sheetName);
            Row s1Row1 = my_sheet.createRow((short) 0);
            s1Row1.setHeightInPoints(50); // row hight
            
            HSSFCellStyle style = my_workbook.createCellStyle();
            style.setFillForegroundColor(HSSFColor.CORNFLOWER_BLUE.index);
            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            style.setRightBorderColor(IndexedColors.BLACK.getIndex());
            style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            style.setTopBorderColor(IndexedColors.BLACK.getIndex());
            style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            HSSFFont font = my_workbook.createFont();
            font.setFontName(HSSFFont.FONT_ARIAL);
            font.setFontHeightInPoints((short) 10);
            font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            font.setColor(HSSFColor.BLACK.index);
            style.setFont(font);
            
            Cell cellc1 = s1Row1.createCell((short) 0);
            cellc1.setCellValue("S.No");
            cellc1.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 0, (short) 4000); // cell width
            
            Cell cellc2 = s1Row1.createCell((short) 1);
            cellc2.setCellValue("Gr No");
            cellc2.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 1, (short) 4000); // cell width
            
            Cell cellc3 = s1Row1.createCell((short) 2);
            cellc3.setCellValue("container Size");
            cellc3.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 2, (short) 4000); // cell width
            
            Cell cellc4 = s1Row1.createCell((short) 3);
            cellc4.setCellValue("Load  Type");
            cellc4.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 3, (short) 4000); // cell width
            
            Cell cellc5 = s1Row1.createCell((short) 4);
            cellc5.setCellValue("Movement Type");
            cellc5.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 4, (short) 4000); // cell width
            
            Cell cellc6 = s1Row1.createCell((short) 5);
            cellc6.setCellValue("Route");
            cellc6.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 5, (short) 4000); // cell width
            
            Cell cellc7 = s1Row1.createCell((short) 6);
            cellc7.setCellValue("Billing Party");
            cellc7.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 6, (short) 4000); // cell width
            
            Cell cellc8 = s1Row1.createCell((short) 7);
            cellc8.setCellValue("Contract Rate");
            cellc8.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 7, (short) 4000); // cell width
            
            Cell cellc9 = s1Row1.createCell((short) 8);
            cellc9.setCellValue("Market Rate");
            cellc9.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 8, (short) 4000); // cell width
            
            Cell cellc10 = s1Row1.createCell((short) 9);
            cellc10.setCellValue("Difference");
            cellc10.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 9, (short) 4000); // cell width
            
        
            
            Iterator itr = result1.iterator();
            schTO = null;
            int cntr = 1;
            int order_closed = 0;
            int order_end = 0;
            int order_progress = 0;
            int order_freezed = 0;
            int order_created = 0;

            while (itr.hasNext()) {
                schTO = (SchedulerTO) itr.next();

                s1Row1 = my_sheet.createRow((short) cntr);
                s1Row1.createCell((short) 0).setCellValue(cntr);
                s1Row1.createCell((short) 1).setCellValue(schTO.getGrNo());
                s1Row1.createCell((short) 2).setCellValue(schTO.getContainerSize());
             
                s1Row1.createCell((short) 3).setCellValue(schTO.getLoadTypeName());
                s1Row1.createCell((short) 4).setCellValue(schTO.getMovementType());
                s1Row1.createCell((short) 5).setCellValue(schTO.getRouteInfo());
                s1Row1.createCell((short) 6).setCellValue(schTO.getCustomerName());
                s1Row1.createCell((short) 7).setCellValue(schTO.getEstimatedRevenue());
                s1Row1.createCell((short) 8).setCellValue(schTO.getMarketRate());
                s1Row1.createCell((short) 9).setCellValue(Double.parseDouble(schTO.getEstimatedRevenue())- Double.parseDouble(schTO.getMarketRate()));
                


                cntr++;


            }
            System.out.println("Your excel 1 Sheet  created");
            
            my_workbook.setSheetOrder("contract" + rangeStartDate1, 0);
            System.out.println("rangeStartDate111112:" + rangeStartDate1);

            // my_workbook.setSheetOrder("Future Trip" + rangeStartDate1, 6);
            System.out.println("testing in the excel sheet creation");
            FileOutputStream fileOut = new FileOutputStream(filename);
            my_workbook.write(fileOut);
            fileOut.close();

            System.out.println("Your excel file has been generated!");

            File_Name = "c:\\ContractDiffAlert\\" + "contract_diff" + rangeStartDate1 + ".xls";
            Filename = "contract_diff" + rangeStartDate1 + " " + ft.format(dNow) + ".xls";
            System.out.println(File_Name);
            System.out.println(Filename);
            String bcc = "";
            String mailContent = "contract_diff Alerts" + rangeStartDate1 + " " + ft.format(dNow);
           
//                recTo = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts.to");
//                recCc = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts.cc");
            Date dNow11 = new Date();
            SimpleDateFormat dateFormat11 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            SimpleDateFormat processDateFormat111 = new SimpleDateFormat("yyyy-MM-dd");
            String dt1 = dateFormat11.format(dNow1).toString();
            String substr1 = dt.substring(11, 13);
               //  String compare=substr+" 10:30:00";
            // substr = substr + " 10:00:00";
//           if (substr1.trim().equals("17")) {
//                    recTo = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts.to");
//                    recCc = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts.cc");
//                } else {
//                    recTo = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts1.to");
//                    recCc = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts1.cc");
//                }

           // recBcc = "";
            String emailFormat1="";
            String emailFormat2="";
            
            String atlMsg = "TMS ALERTS - contract_diff Summary alert as on - " + rangeStartDate1;
            String dueAlt = "contract_diff_Summary_ alert as on " + rangeStartDate1;
            contentdata = "Hi Greetings From TMS, <br><br>";
            contentdata = contentdata + "<table border='1' cellspacing='4' cellpadding='1' width='600px'>" + contentSB.toString() + "</table>";
            contentdata = contentdata + "<br><br> Team Throttle";
            emailFormat1 = "<html>"
                    + "<body>"
                    + "<p style='font-style: italic;'><b>Dear Sir/Madam, <br><br>Please find below Attatchement of contract diff alert<br><br/>";
            
            emailFormat2 = emailFormat1 + "<br><br>Hi Greetings From TMS"
                    + "</body></html>";
            
            
            SchedulerTO schTO1 = new SchedulerTO();
            schTO1.setMailStatus("1");
            schTO1.setMailTypeId("2");
            schTO1.setMailIdTo(recTo);
            schTO1.setMailIdCc(recCc);
            schTO1.setMailIdBcc(recBcc);
            schTO1.setMailSubjectTo("contract_diff_Report");
            schTO1.setMailSubjectCc("contract_diff_Report");
            schTO1.setMailSubjectBcc("contract_diff_Report");
            schTO1.setFilenameContent(File_Name);
            schTO1.setEmailFormat(emailFormat2);
            System.out.println("filenameContent" + Filename);
            System.out.println("emailFormat4" + emailFormat2);
            
            
            Integer mails =schedulerDAO.insertMailDetails(schTO1,1);
            System.out.println("insertMailDetails@@@"+mails);
            System.out.println("recBcc:--"+recBcc);
//            if (result1.size() > 0) {
            em.sendExcelMail(Filename, recTo, recCc, recBcc, mailContent,emailFormat2, my_workbook);
            System.out.println("closed");
//            }
            

        } catch (Exception excp) {
            System.out.println("ERROR STATUS" + excp);
            excp.printStackTrace();
        }
        return dvmDetailsDays;
    }
      
      public void test(){
      try
		{
                    
                    System.out.println("testing digi");
			// Get constructor to the Sun PKCS11 provider
			Class<?> pkcs11Class = Class.forName("sun.security.pkcs11.SunPKCS11");
			Constructor<?> construct = pkcs11Class.getConstructor(new Class[] {String.class});

			// Construct the provider
			String configName = "D://pkcs11.cfg";
			Provider p = (Provider)construct.newInstance(new Object[] {configName});
			Security.addProvider(p);
            
			// Create key store
			KeyStore ks = KeyStore.getInstance("PKCS11");
			ks.load(null, "password@123".toCharArray());

			// Get the alias of the first entry in the keystore
			Enumeration<?> aliases = ks.aliases();
			if (aliases.hasMoreElements() == false)
			{
				System.out.println ("No digital IDs found in token.");
				System.exit(-1);
			}
			String alias = null;
			while (aliases.hasMoreElements()) {
			    alias = (String)aliases.nextElement();
			    System.out.println(alias);

    }
			System.out.println("p.getName():"+p.getName());
			PrivateKey key = (PrivateKey)ks.getKey(alias,"password@123".toCharArray());
			java.security.cert.Certificate[] chain = ks.getCertificateChain(alias);
			PdfReader reader = new PdfReader("D://ExportInvoice.pdf");
			FileOutputStream fout = new FileOutputStream("D://signed.pdf");
			//BouncyCastleProvider provider = new BouncyCastleProvider();
			//Security.addProvider(provider);
			     PdfStamper stamper = PdfStamper.createSignature(reader, fout, '\0');
		        // Creating the appearance
		        PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
                     appearance.setVisibleSignature(new com.itextpdf.text.Rectangle(100, 100, 200, 200), 1, null);
		        // Custom text and custom font
		      //  appearance.setLayer2Text(p.getName());
		      //  appearance.setLayer2Font(new Font(FontFamily.TIMES_ROMAN));
		        // Creating the signature
//		        PrivateKeySignature pks = new PrivateKeySignature(key, DigestAlgorithms.SHA256, p.getName());
//		        ExternalDigest digest = new BouncyCastleDigest();
//		       MakeSignature.signDetached(appearance, digest, pks, chain, null, null, null, 0, CryptoStandard.CMS);
			
			
			
//			PdfStamper stp = PdfStamper.createSignature(reader, fout, '\0');
//			PdfSignatureAppearance sap = stp.getSignatureAppearance();
//			//sap.setCrypto(key, chain, null, PdfSignatureAppearance.WINCER_SIGNED);
//			sap.setCrypto(key, chain, null, PdfSignatureAppearance.SELF_SIGNED);
//			sap.setReason("I'm the author");
//			sap.setLocation("Lisbon");
//			// comment next line to have an invisible signature
//			sap.setVisibleSignature(new Rectangle(100, 100, 200, 200), 1, null);
//			stp.close();
			
			// Creating the appearance
	        
	        // Custom text and custom font
	        
			//String idAlias = (String)aliases.nextElement();
          //  System.out.println (" digital IDs found in token."+idAlias);
			// Load PDF document with jPDFSecure
			//PDFSecure pdf = new PDFSecure ("D://ExportInvoice.pdf", null);
             //  System.out.println ("pdf info:"+pdf.getDocumentInfo());
			// Add a signature field to the document
			//SignatureField signField = pdf.addSignatureField(0, "SignHere", new Rectangle2D.Double(180, 72, 200, 60));

			// Create signature information from the keystore
			//SigningInformation signingInfo = new SigningInformation(ks, alias, "");
			//SigningInformation signingInfo =null;
			// Sign and save the document
			//pdf.signDocument(signField, signingInfo);
		//	System.out.println("sign:"+pdf.getDocumentInfo().getCreator());
			//FileOutputStream fout=new FileOutputStream("D:\\testout.pdf"); 
		//	pdf.saveDocument("D://signed.pdf");
			//pdf.saveDocument(fout);
		}
		catch (Exception P)
				{
					System.out.println ("error."+P.getMessage());
					P.printStackTrace();
		}
		catch (Throwable t)
		{
			t.printStackTrace();
		}
      }
      
      public int updateTempCreditAmount() throws IOException {
        int updateTempCreditAmount = 0;
        updateTempCreditAmount = schedulerDAO.updateTempCreditAmount();
        return updateTempCreditAmount;
    }
      
      
    
}
