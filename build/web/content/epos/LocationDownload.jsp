<%
    Connection conn = null;
    int status = 0;
    try{
        String applicationPath = application.getRealPath("WEB-INF/classes");
        applicationPath = applicationPath.replace("\\", "/");
        java.io.FileInputStream fis = new java.io.FileInputStream(applicationPath+"/jdbc_url.properties");
        java.util.Properties props = new java.util.Properties();
        props.load(fis);
        String driverClass = props.getProperty("jdbc.driverClassName");
        String jdbcUrl = props.getProperty("jdbc.url");
        String userName = props.getProperty("jdbc.username");
        String password = props.getProperty("jdbc.password");
        Class.forName(driverClass);
        conn = DriverManager.getConnection(jdbcUrl, userName, password);
        Statement stmt = conn.createStatement();
        ResultSet res = stmt.executeQuery("SELECT * FROM ra_operation_location_master_epos WHERE Act_Ind = 'Y'");        
        while(res.next()) {
            out.print((res.getString("Location_Id")+"$"+res.getString("Location_Name")+"$").trim());
        }                   
       if(conn != null) {
            conn.close();
        }
    }catch(Exception e) {
        out.print("0$");
        FPLogUtils.fpErrorLog((new StringBuilder()).append("Exception in EPOS Vehicle IN OUT --> ").append(e.getMessage()).toString());
    } finally{
        if(conn != null) {
            conn.close();
    }
}
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*,ets.domain.util.FPLogUtils" %>