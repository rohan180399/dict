<%--
    Document   : vehicleWiseProfitabilityReport
    Created on : Oct 20, 2013, 6:19:33 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
        <link href="/throttle/css/tableFilter.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/filtergrid.css" rel="stylesheet" type="text/css"/>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>



        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>
    </head>
     <%
     String menuPath = "Report >> Vehicle Wise Profitability";
     request.setAttribute("menuPath",menuPath);
     %>
    <body onload="setFocus();setValues(); getVehicleNos();">
        <form name="vehiclePerformance" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <!-- pointer table -->
            <table width="900" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                    <td align="right"><div style="height:17px;margin-top:0px;"><img src="../images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="../images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Vehicle Wise Profitability Report</li>
                            </ul>
                            <div id="first">
                                <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    <tr>
                                        <td><font color="red">*</font>Vehicle No</td>
                                        <td><input name="regno" id="regno" type="text" class="form-control" size="20" value="" autocomplete="off"></td>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker"  onclick="ressetDate(this);"></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" onclick="ressetDate(this);"></td>

                                        <td> <input type="hidden" name="days" id="days" value="" /></td>
                                        <td><input type="button" class="button"   value="Search" onclick="getDays();
                                                submitPage();"></td>
                                    </tr>


                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>
            <br>
            <br/>
            <table border="0" class="border"  align="center" width="100%" cellpadding="0" cellspacing="1" >
                <thead>
                    <tr>
                        <td rowspan="2" class="contenthead">S.No</td>
                        <td rowspan="2" class="contenthead">Vehicle No</td>
                        <td rowspan="2" class="contenthead">Vehicle Model</td>
                        <td rowspan="2" class="contenthead">MFR Name</td>
                        <td rowspan="2" class="contenthead">Trips</td>
                        <td rowspan="2" class="contenthead">Earnings</td>

                        <td colspan="5" class="contenthead" style="text-align:center">Fixed Expenses </td>
                        <td colspan="4" class="contenthead" style="text-align:center">Operation Expenses </td>
                        <td rowspan="2" class="contenthead" style="text-align:center">Per Day<br> Fixed Expenses </td>
                        <td rowspan="2" class="contenthead" style="text-align:center">Fixed Expenses <br>for the Report Period</td>
                        <td rowspan="2" class="contenthead" style="text-align:center">Operation Expenses</td>
                        <td rowspan="2" class="contenthead">Maint <br>Expenses</td>
                        <td rowspan="2" class="contenthead">Nett <br>Expenses</td>
                        <td rowspan="2" class="contenthead">Profit</td>
                    </tr> 
                    <tr>
                        <td class="contenthead">Insurance</td>
                        <td class="contenthead">Road Tax </td>
                        <td class="contenthead">FC Amount </td>
                        <td class="contenthead">Permit </td>
                        <td class="contenthead">EMI </td>
                        <td class="contenthead">Toll Amount</td>
                        <td class="contenthead">Fuel Amount </td>
                        <td class="contenthead">Other Exp Amount </td>
                        <td class="contenthead">Driver / Cleaner Salary </td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text1" rowspan="2" >1</td>
                        <td class="text1" rowspan="2" >TN 10 AD 6564</td>
                        <td class="text1"  rowspan="2" >2516</td>
                        <td class="text1" rowspan="2" >AL</td>
                        <td rowspan="2" class="text1" >3</td>
                        <td rowspan="2" class="text1" >1,10,000.00</td>
                        <td rowspan="2" class="text1" >2,000.00</td>
                        <td rowspan="2" class="text1" >100.00</td>
                        <td rowspan="2" class="text1" >500.00</td>
                        <td rowspan="2" class="text1" >50.00</td>
                        <td rowspan="2" class="text1" >6,500.00</td>
                        <td rowspan="2" class="text1" >700.00</td>
                        <td rowspan="2" class="text1" >3,422.00</td>
                        <td rowspan="2" class="text1" >10,000.00</td>
                        <td rowspan="2" class="text1" >2,333.00</td>
                        <td rowspan="2" class="text1" >3,500.00</td>
                        <td rowspan="2" class="text1" >17,000.00</td>
                        <td rowspan="2" class="text1" >26,000.00</td>
                        <td rowspan="2" class="text1" >3,250.00</td>
                        <td rowspan="2" class="text1" >43,000.00</td>
                        <td rowspan="2" class="text1" >56,000.00</td>


                    </tr>
                </tbody>
                <tbody>
                    <tr>
                        <td class="text2" rowspan="2" >2</td>
                        <td class="text2" rowspan="2" >TN 10 AD 6564</td>
                        <td class="text2"  rowspan="2" >2516</td>
                        <td class="text2" rowspan="2" >AL</td>
                        <td rowspan="2" class="text2" >3</td>
                        <td rowspan="2" class="text2" >70,000.00</td>
                        <td rowspan="2" class="text2" >1,600.00</td>
                        <td rowspan="2" class="text2" >80.00</td>
                        <td rowspan="2" class="text2" >200.00</td>
                        <td rowspan="2" class="text2" >30.00</td>
                        <td rowspan="2" class="text2" >2,500.00</td>
                        <td rowspan="2" class="text2" >400.00</td>
                        <td rowspan="2" class="text2" >1,422.00</td>
                        <td rowspan="2" class="text2" >4,000.00</td>
                        <td rowspan="2" class="text2" >1,333.00</td>
                        <td rowspan="2" class="text2" >2,500.00</td>
                        <td rowspan="2" class="text2" >7,000.00</td>
                        <td rowspan="2" class="text2" >6,000.00</td>
                        <td rowspan="2" class="text2" >3,250.00</td>
                        <td rowspan="2" class="text2" >23,000.00</td>
                        <td rowspan="2" class="text2" >26,000.00</td>


                    </tr>
                </tbody>
                
            </table>

            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <table border="2" style="border: 1px solid #666666;"  align="center"  cellpadding="0" cellspacing="1" >
                <tr height="25">
                    <td style="background-color: #6374AB; color: #ffffff">Total Trips Carried Out</td>
                    <td width="150">2</td>
                </tr>
                <tr height="25">
                    <td style="background-color: #6374AB; color: #ffffff">Total Income</td>
                    <td width="150">35,000.00</td>
                </tr>
                <tr height="25">
                    <td style="background-color: #6374AB; color: #ffffff">Total Fixed Expenses</td>
                    <td width="150">50,000.00</td>
                </tr>
                <tr height="25">
                    <td style="background-color: #6374AB; color: #ffffff">Total Operation Expenses</td>
                    <td width="150">15,000.00</td>
                </tr>
                <tr height="25">
                    <td style="background-color: #6374AB; color: #ffffff">Total Nett Expenses</td>
                    <td width="150">35,000.00</td>
                </tr>
                <tr height="25">
                    <td style="background-color: #6374AB; color: #ffffff">Total Profit gained</td>
                    <td width="150"><font color="green">35,000.00</font></td>
                </tr>
            </table>
            <br/>
            <br/>
            <br/>
            <br/>



        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>

</html>
