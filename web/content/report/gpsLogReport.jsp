<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<script type="text/javascript">



    function viewLogDetails(tripId) {
        window.open('/throttle/viewTripLog.do?tripId=' + tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }


    //auto com

    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#vehicleNo').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getVehicleNo.do",
                    dataType: "json",
                    data: {
                        vehicleNo: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            alert("Invalid Vehicle No");
                            $('#vehicleNo').val('');
                            $('#vehicleId').val('');
                            $('#vehicleNo').fous();
                        } else {
                            response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var id = ui.item.Id;
                $('#vehicleNo').val(value);
                $('#vehicleId').val(id);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });

    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#tripCode').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getTripCode.do",
                    dataType: "json",
                    data: {
                        tripCode: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            $('#tripId').val('');
                            $('#tripCode').val('');
                        }
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split(',');
                $('#tripId').val(tmp[0]);
                $('#tripCode').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split(',');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });
</script>



<script type="text/javascript">
    function submitPage(value) {
        if (document.getElementById('fromDate').value == '') {
            alert("Please select from Date");
            document.getElementById('fromDate').focus();
        } else if (document.getElementById('toDate').value == '') {
            alert("Please select to Date");
            document.getElementById('toDate').focus();
        } else {
            if (value == 'ExportExcel') {
                document.accountReceivable.action = '/throttle/handleGPSLog.do?param=ExportExcel';
                document.accountReceivable.submit();
            } else {
                document.accountReceivable.action = '/throttle/handleGPSLog.do?param=Search';
                document.accountReceivable.submit();
            }
        }
    }
    function setValue() {
        if ('<%=request.getAttribute("page")%>' != 'null') {
            var page = '<%=request.getAttribute("page")%>';
            if (page == 1) {
                submitPage('search');
            }
        }
        if ('<%=request.getAttribute("tripId")%>' != 'null') {
            document.getElementById('tripId').value = '<%=request.getAttribute("tripId")%>';
        }
    }


    function viewVehicleDetails(vehicleId) {
        window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
</script>


<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
            setValues();
            getVehicleNos();">
    </c:if>

    <!--	  <span style="float: right">
                    <a href="?paramName=en">English</a>
                    |
                    <a href="?paramName=ar">Ø§Ù„Ø¹Ø±Ø¨ÙŠØ©</a>
              </span>-->
    <style>
        #index td {
            color:white;
        }
    </style>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i><spring:message code="operations.reports.label.GPSLogReport" text="default text"/></h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="default text"/>:</span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="head.label.Report" text="default text"/></a></li>
                <li class="active"><spring:message code="operations.reports.label.GPSLogReport" text="default text"/></li>
            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="setValue();
                        sorter.size(20);">
                    <form name="accountReceivable"   method="post">
                        <table class="table table-bordered" id="report">
                            <!--                            <tr height="30" id="index" >
                                                            <td colspan="4"  style="background-color:#5BC0DE;">
                                                                <b><spring:message code="operations.reports.label.GPSLogReport" text="default text"/></b>
                                                            </td></tr>-->

                            <td style="border-color:#5BC0DE;padding:16px;">
                                <table class="table table-info mb30 table-hover" >
                                    <tr height="30">
                                        <td align="center"><spring:message code="operations.reports.label.VehicleNo" text="default text"/>
                                        </td>
                                        <td height="30">
                                            <input type="text" style="width:260px;height:40px;" class="form-control" id="vehicleNo"  name="vehicleNo" autocomplete="off" value="<c:out value="${vehicleNo}"/>"/>
                                            <input type="hidden"  class="form-control" id="vehicleId"  name="vehicleId" autocomplete="off" value="<c:out value="${vehicleId}"/>"/>
                                        </td>
                                        <td align="center"><spring:message code="operations.reports.label.TripCode" text="default text"/>
                                        </td>
                                        <td height="30">
                                            <input type="hidden" name="tripId" id="tripId" value="<c:out value="${tripId}"/>" style="width:260px;height:40px;" class="form-control"><input type="text"  style="width:260px;height:40px;" name="tripCode" id="tripCode" value="<c:out value="${tripCode}"/>" class="form-control">
                                        </td>
                                    </tr>
                                    <tr height="30">
                                        <td align="center"><font color="red">*</font><spring:message code="operations.reports.label.FromDate" text="default text"/></td>
                                        <td height="30"><input name="fromDate" id="fromDate" style="width:260px;height:40px;" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                        <td align="center"><font color="red">*</font><spring:message code="operations.reports.label.ToDate" text="default text"/></td>
                                        <td height="30"><input name="toDate" id="toDate" style="width:260px;height:40px;" type="text" class="datepicker" value="<c:out value="${toDate}"/>" ></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td  align="right"><input type="button" class="btn btn-success" name="ExportExcel"   value="<spring:message code="operations.reports.label.EXPORTEXCEL" text="default text"/>" onclick="submitPage(this.name);">&nbsp;&nbsp;</td>
                                        <td><input type="button" class="btn btn-success" name="Search"  id="Search"  value="<spring:message code="operations.reports.label.SEARCH" text="default text"/>" onclick="submitPage(this.name);"></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </td>
                            </tr>
                        </table>
                        <br>
                        <c:if test="${logDetailsSize != '0'}">
                            <table class="table table-info mb30 table-hover" id="table"  width="100%" >
                                <thead>
                                    <tr>
                                        <th rowspan="2" ><spring:message code="operations.reports.label.SNo" text="default text"/></th>
                                        <th rowspan="2" ><spring:message code="operations.reports.label.VehicleNo" text="default text"/></th>
                                        <th rowspan="2" ><spring:message code="operations.reports.label.TripCode" text="default text"/></th>
                                        <th rowspan="2" ><spring:message code="operations.reports.label.TripDate" text="default text"/></th>
                                        <th rowspan="2" ><spring:message code="operations.reports.label.CurrentLocation" text="default text"/></th>
                                        <th rowspan="2" ><spring:message code="operations.reports.label.DistanceTravelled" text="default text"/></th>
                                        <th rowspan="2" ><spring:message code="operations.reports.label.CurrentTemperature" text="default text"/></th>
                                        <th rowspan="2" ><spring:message code="operations.reports.label.LogDate" text="default text"/></th>
                                    </tr> 
                                </thead>
                                <tbody>
                                    <% int index = 1;%>
                                    <c:forEach items="${logDetails}" var="logDetails">
                                        <%
                                            String classText = "";
                                            int oddEven = index % 2;
                                            if (oddEven > 0) {
                                                classText = "text2";
                                            } else {
                                                classText = "text1";
                                            }
                                        %>
                                        <tr>
                                            <td class="<%=classText%>"><%=index++%></td>
                                            <td  width="90" class="<%=classText%>">
                                                <a href="#" onclick="viewVehicleDetails('<c:out value="${logDetails.vehicleId}"/>')"><c:out value="${logDetails.vehicleNo}"/></a></td>

                                            <td width="90" class="<%=classText%>">
                                                <a href="#" onclick="viewLogDetails('<c:out value="${logDetails.tripId}"/>')"><c:out value="${logDetails.tripCode}"/></a>
                                            </td>
                                            <td class="<%=classText%>" ><c:out value="${logDetails.tripDate}"/></td>
                                            <td class="<%=classText%>"  ><c:out value="${logDetails.location}"/></td>
                                            <td width="140" class="<%=classText%>"  ><c:out value="${logDetails.distance}"/></td>
                                            <td width="140" class="<%=classText%>"  ><c:out value="${logDetails.currentTemperature}"/></td>
                                            <td class="<%=classText%>"  ><c:out value="${logDetails.logDate}"/></td>
                                        </tr>

                                    </c:forEach>
                                </tbody>
                            </table>
                            <br/>
                            <script language="javascript" type="text/javascript">
                                setFilterGrid("table");</script>
                            <div id="controls">
                                <div id="perpage">
                                    <select onchange="sorter.size(this.value)">
                                        <option value="5" >5</option>
                                        <option value="10">10</option>
                                        <option value="20" selected="selected">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <span><spring:message code="operations.reports.label.EntriesPerPage" text="default text"/></span>
                                </div>
                                <div id="navigation">
                                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                </div>
                                <div id="text"><spring:message code="operations.reports.label.DisplayingPage" text="default text"/> <span id="currentpage"></span> <spring:message code="operations.reports.label.of" text="default text"/> <span id="pagelimit"></span></div>
                            </div>
                            <script type="text/javascript">
                                var sorter = new TINY.table.sorter("sorter");
                                sorter.head = "head";
                                sorter.asc = "asc";
                                sorter.desc = "desc";
                                sorter.even = "evenrow";
                                sorter.odd = "oddrow";
                                sorter.evensel = "evenselected";
                                sorter.oddsel = "oddselected";
                                sorter.paginate = true;
                                sorter.currentid = "currentpage";
                                sorter.limitid = "pagelimit";
                                sorter.init("table", 0);
                            </script>
                        </c:if>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                </body>    
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>