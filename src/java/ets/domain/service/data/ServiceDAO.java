/*---------------------------------------------------------------------------
 * ServiceDAO.java
 * Mar 3, 2009
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
-------------------------------------------------------------------------*/
package ets.domain.service.data;

import ets.arch.exception.FPRuntimeException;
import ets.domain.service.business.ServiceTO;
import ets.domain.util.FPLogUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

/****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver              Date                      Author                    Change
 * ---------------------------------------------------------------------------
 * 1.0           Mar 3, 2009              vijay			       Created
 *
 ******************************************************************************/
public class ServiceDAO extends SqlMapClientDaoSupport {

    private final static String CLASS = "ServiceDAO";

    public ArrayList getFreeServiceDetails(int modId, int mfrId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList getFreeServiceDetails = new ArrayList();
        map.put("modId", modId);
        map.put("mfrId", mfrId);

        try {
            getFreeServiceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("service.getFreeServiceDetails", map);


        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFreeServiceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getFreeServiceDetails", sqlException);
        }
        return getFreeServiceDetails;
    }

    public int insertFreeServiceDetails(int month, int km, int hm, String desc, int modId, int mfrId, String aStatus) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        map.put("modId", modId);
        map.put("mfrId", mfrId);
        map.put("month", month);
        map.put("km", km);
        map.put("hm", hm);
        map.put("desc", desc);
        map.put("status", aStatus);
        System.out.println("hmin dao:" + hm);
        int status = 0;

        try {
            status = (Integer) getSqlMapClientTemplate().update("service.insertFreeServiceDetails", map);


        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertFreeServiceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertFreeServiceDetails", sqlException);
        }
        return status;
    }

    public int updateFreeServiceDetails(int fServiceId, int month, int km, int hm, String desc, int modId, int mfrId, String aStatus) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        map.put("modId", modId);
        map.put("mfrId", mfrId);
        map.put("month", month);
        map.put("km", km);
        map.put("hm", hm);
        map.put("desc", desc);
        map.put("fServiceId", fServiceId);
        map.put("status", aStatus);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("service.updateFreeServiceDetails", map);
            System.out.println("status" + status);

        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateFreeServiceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateFreeServiceDetails", sqlException);
        }
        return status;
    }

    public ArrayList getMaintenanceDetails() {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList getServiceDetails = new ArrayList();


        try {
            getServiceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("service.getServiceDetails", map);


        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getServiceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getServiceDetails", sqlException);
        }
        return getServiceDetails;
    }

    public int insertMaintance(ServiceTO serviceTO) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int status = 0;
        map.put("serviceName", serviceTO.getServiceName());
        map.put("userId", serviceTO.getUserId());
        map.put("desc", serviceTO.getDesc());
        try {
            status = (Integer) getSqlMapClientTemplate().update("service.insertMaintance", map);


        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertMaintance Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertMaintance", sqlException);
        }
        return status;
    }

    public int updateMaintance(int serviceId, String serName, String desc, String aStatus, int userId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        System.out.println("vijay");
        int status = 0;
        map.put("serviceName", serName);
        map.put("serviceId", serviceId);
        map.put("userId", userId);
        map.put("status", aStatus);
        map.put("desc", desc);
        try {
            status = (Integer) getSqlMapClientTemplate().update("service.updateMaintance", map);


        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateMaintance Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateMaintance", sqlException);
        }
        return status;
    }
    public int getJobCardServiceCheckListCount(int jobCardId, int serviceId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        System.out.println("vijay");
        int count = 0;
        map.put("jobCardId", jobCardId);
        map.put("serviceId", serviceId);

        try {
            count = ((Integer) getSqlMapClientTemplate().queryForObject("service.getJobCardServiceCheckListCount", map)).intValue();
        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJobCardServiceCheckListCount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateMaintance", sqlException);
        }
        return count;
    }
    public int updateJobCardServiceCheckList(int jobCardId, int serviceId, String checkListId, String checkedStatus,
            String servicedStatus,String remarks, int count, int userId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        System.out.println("vijay");
        int status = 0;
        map.put("jobCardId", jobCardId);
        map.put("serviceId", serviceId);
        map.put("userId", userId);
        map.put("checkListId", checkListId);
        map.put("checkedStatus", checkedStatus);
        map.put("servicedStatus", servicedStatus);
        map.put("remarks", remarks);
        System.out.println("count=="+count);
        System.out.println("map value is="+map);
        try {
            if(count > 0) {//update
                System.out.println("1111");
                status = (Integer) getSqlMapClientTemplate().update("service.updateJobCardServiceCheckList", map);
            }else {//insert
                System.out.println("2222");
                status = (Integer) getSqlMapClientTemplate().update("service.insertJobCardServiceCheckList", map);
            }


        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateMaintance Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateMaintance", sqlException);
        }
        return status;
    }

    /**
     * This method fetches the active vehicle details.
     *
     * @return	activeVehicleList -Returns the list of active vehicle data
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getVehicleTypes() {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList getServiceDetails = new ArrayList();


        try {
            getServiceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("service.getVehicleTypes", map);


        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleTypes Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehicleTypes", sqlException);
        }
        return getServiceDetails;
    }

    /**
     * This method fetches the active mfr details.
     *
     * @return	activeMfrList -Returns the list of active mfr data
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getMfrList() {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList getServiceDetails = new ArrayList();


        try {
            getServiceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("service.getMfrList", map);


        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMfrList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getMfrList", sqlException);
        }
        return getServiceDetails;
    }

    /**
     * This method fetches the configMaintenanceDetails
     *
     * @return	configMaintenanceDetails -Returns the list of active maintenance details
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList checkMaintenanceDetails(ServiceTO serviceTO) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList getServiceDetails = new ArrayList();
        int status = 0;
        map.put("mfrId", serviceTO.getMfrId());
        map.put("modId", serviceTO.getModId());
        map.put("vtypeId", serviceTO.getVtypeId());

        try {

            getServiceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("service.checkMaintenanceDetails", map);
            if (getServiceDetails.size() == 0) {
                status = 0;
            } else {
                status = getServiceDetails.size();
            }

        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkMaintenanceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkMaintenanceDetails", sqlException);
        }
        return getServiceDetails;
    }

    /**
     * This method fetches the configMaintenanceDetails
     *
     * @return	configMaintenanceDetails -Returns the list of active maintenance details
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertConfigMaintenanceDetails(ServiceTO serviceTO) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int status = 0;
        map.put("mfrId", serviceTO.getMfrId());
        map.put("vtypeId", serviceTO.getVtypeId());
        map.put("modId", serviceTO.getModId());
        map.put("serviceId", serviceTO.getServiceId());
        map.put("userId", serviceTO.getUserId());

        try {

            status = (Integer) getSqlMapClientTemplate().update("service.insertConfigMaintenanceDetails", map);


        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertConfigMaintenanceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertConfigMaintenanceDetails", sqlException);
        }
        return status;

    }

    public int updateConfigMaintenance(ServiceTO serviceTO, int serId, int km, String hourMeters, int month,String labo) {
        Map map = new HashMap();
        int status = 0;
        map.put("mfrId", serviceTO.getMfrId());
        map.put("modId", serviceTO.getModId());
        map.put("vtypeId", serviceTO.getVtypeId());
        map.put("serviceId", serId);
        map.put("userId", serviceTO.getUserId());
        map.put("km", km);
        map.put("hourMeter", hourMeters);
        map.put("month", month);
        map.put("labour", labo);
        System.out.println("mappppp"+map);

        try {

            status = (Integer) getSqlMapClientTemplate().update("service.updateConfigMaintenance", map);


        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateConfigMaintenance Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateConfigMaintenance", sqlException);
        }
        return status;
    }

    public ArrayList getSections() {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList sections = new ArrayList();


        try {
            sections = (ArrayList) getSqlMapClientTemplate().queryForList("service.getSections", map);


        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSections Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getSections", sqlException);
        }
        return sections;
    }
    public ArrayList getModels(int mfrId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList sections = new ArrayList();
        map.put("mfrId", mfrId);

        try {
            sections = (ArrayList) getSqlMapClientTemplate().queryForList("service.getModels", map);


        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSections Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getSections", sqlException);
        }
        return sections;
    }

    public ArrayList getProblems(int secId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList tempList = new ArrayList();
        map.put("secId", secId);


        try {
            tempList = (ArrayList) getSqlMapClientTemplate().queryForList("service.getProblems", map);


        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getProblems Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getProblems", sqlException);
        }
        return tempList;
    }

    /**
     * This method fetches the configMaintenanceDetails
     *
     * @return	configMaintenanceDetails -Returns the list of active maintenance details
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getProblemActivities(int secId, int groupId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList tempList = new ArrayList();
        map.put("groupId", groupId);
        map.put("secId", secId);


        try {
            tempList = (ArrayList) getSqlMapClientTemplate().queryForList("service.getProblemActivities", map);


        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getProblemActivities Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getProblemActivities", sqlException);
        }
        return tempList;
    }

    public int insertProblemActivityDetails(String labor, String laborTime, String ratePerHour,String activityName, String desc1, int secId, int groupId, String aStatus, int userId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int status = 0;
        int activityId = 0;
        map.put("activityName", activityName);
        map.put("desc", desc1);
        map.put("labor", labor);
        System.out.println("secId:"+secId);
        /*
        if(secId == 1040) {
            map.put("ratePerHour", labor);
        }else {
            map.put("ratePerHour", ratePerHour);
        }
        */
        
        System.out.println("ratePerHour:"+ratePerHour);
        System.out.println("labor:"+labor);

        map.put("laborTime", laborTime);
        map.put("ratePerHour", ratePerHour);
        map.put("sectionId", secId);
        map.put("groupId", groupId);
        map.put("status", aStatus);
        map.put("userId", userId);
        if(groupId ==1 ){
            map.put("groupPrefix", "R");
        }else if(groupId ==2 ){
            map.put("groupPrefix", "F");
        
        }else if(groupId ==3 ){
            map.put("groupPrefix", "C");

        }else if(groupId ==4 ){
            map.put("groupPrefix", "H");

        }else if(groupId ==5 ){
            map.put("groupPrefix", "O");
        }

        try {

            //activityId = (Integer) getSqlMapClientTemplate().update("service.insertProblemActivityDetails", map);
            activityId = ((Integer) getSqlMapClientTemplate().insert("service.insertProblemActivityDetails", map)).intValue();
            
            System.out.println("activityId:" + activityId);
            int seqNumber = ((Integer) getSqlMapClientTemplate().queryForObject("service.seqNumber", map)).intValue();
            map.put("activityId", activityId);
            map.put("seqNumber", seqNumber);
            status = (Integer) getSqlMapClientTemplate().update("service.updateActivityCode", map);



        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertProblemActivityDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertProblemActivityDetails", sqlException);
        }
        return status;
    }

    public int insertProblemActivityCauses(String activityName, String causeName, int probId, int userId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int status = 0;
        map.put("activityName", activityName);
        map.put("causeName", causeName);
        map.put("probId", probId);
        map.put("userId", userId);

        try {
            status = (Integer) getSqlMapClientTemplate().update("service.insertProblemActivity", map);
            status = (Integer) getSqlMapClientTemplate().update("service.insertProblemCause", map);
        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertProblemActivityDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertProblemActivityDetails", sqlException);
        }
        return status;
    }

    public int updateProblemActivityDetails(String labor, String laborTime, String ratePerHour,int activityId, String activityName, String desc1, int probId, String aStatus) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int status = 0;
        map.put("activityName", activityName);
        map.put("activityId", activityId);
        map.put("desc", desc1);
        map.put("labor", labor);
        map.put("laborTime", laborTime);
        map.put("ratePerHour", ratePerHour);
        map.put("probId", probId);
        map.put("status", aStatus);


        try {

            status = (Integer) getSqlMapClientTemplate().update("service.updateProblemActivityDetails", map);


        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateProblemActivityDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateProblemActivityDetails", sqlException);
        }
        return status;

    }

    public ArrayList checkLabourRates(ServiceTO serviceTO) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList getServiceDetails = new ArrayList();
        int status = 0;
        map.put("secId", serviceTO.getSecId());
        map.put("mfrId", serviceTO.getMfrId());
        map.put("modelId", serviceTO.getModelId());
        System.out.println("secId:" + serviceTO.getSecId());
        System.out.println("mfrId:" + serviceTO.getMfrId());
        System.out.println("modelId:" + serviceTO.getModelId());
        try {

            getServiceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("service.checkLabourRates", map);

        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkMaintenanceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkMaintenanceDetails", sqlException);
        }
        return getServiceDetails;
    }

    public ArrayList fetchMfrActivities(ServiceTO serviceTO) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList getServiceDetails = new ArrayList();
        int status = 0;
        map.put("secId", serviceTO.getSecId());
        map.put("mfrId", serviceTO.getMfrId());
        map.put("modelId", serviceTO.getModelId());
        System.out.println("secId:" + serviceTO.getSecId());
        System.out.println("mfrId:" + serviceTO.getMfrId());
        System.out.println("modelId:" + serviceTO.getModelId());
        try {

            getServiceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("service.selectLabourRates", map);

        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkMaintenanceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkMaintenanceDetails", sqlException);
        }
        return getServiceDetails;
    }

    public boolean checkIfActivityExists(ServiceTO serviceTO) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList getServiceDetails = new ArrayList();
        int status = 0;
        map.put("secId", serviceTO.getSecId());
        map.put("mfrId", serviceTO.getMfrId());
        map.put("modelId", serviceTO.getModelId());
        map.put("modelId", serviceTO.getModelId());
        map.put("activityId", serviceTO.getActivityId());
        System.out.println("secId:" + serviceTO.getSecId());
        System.out.println("mfrId:" + serviceTO.getMfrId());
        System.out.println("modelId:" + serviceTO.getModelId());
        System.out.println("getActivityId:" + serviceTO.getActivityId());
        boolean retStatus = false;
        try {

            //getServiceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("service.selectLabourRates", map);
            status = ((Integer) getSqlMapClientTemplate().queryForObject("service.checkIfActivityExists", map)).intValue();
            if(status > 0){
              retStatus = true;
            }

        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkMaintenanceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkMaintenanceDetails", sqlException);
        }
        return retStatus;
    }

    public int insertConfigLabourRate(int modId, int mfrId, ServiceTO serviceTO) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int status = 0;
        map.put("modId", modId);
        map.put("mfrId", mfrId);
        map.put("activityId", serviceTO.getActivityId());
        map.put("userId", serviceTO.getUserId());

        try {

            status = (Integer) getSqlMapClientTemplate().update("service.insertConfigLabourRate", map);


        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertConfigLabourRate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertConfigLabourRate", sqlException);
        }
        return status;

    }

    public int updateLabourRates(ServiceTO serviceTO, int activityId, String laborTime,int mfrId, int modId, String labo) {
        Map map = new HashMap();
        int status = 0;
        map.put("modId", modId);
        map.put("mfrId", mfrId);
        map.put("laborTime", laborTime);
        map.put("activityId", activityId);
        map.put("amount", labo);
        map.put("userId", serviceTO.getUserId());
        map.put("rph", serviceTO.getRatePerHour());

        try {

            status = (Integer) getSqlMapClientTemplate().update("service.updateLabourRates", map);


        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateLabourRates Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateLabourRates", sqlException);
        }
        return status;
    }

    public ArrayList getProblemCauses(int probId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList tempList = new ArrayList();
        map.put("probId", probId);


        try {
            tempList = (ArrayList) getSqlMapClientTemplate().queryForList("service.getProblemCauses", map);


        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getProblemCauses Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getProblemCauses", sqlException);
        }
        return tempList;
    }
    public ArrayList getJobCardCheckList(int jobCardId, int serviceId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList tempList = new ArrayList();
        map.put("jobCardId", jobCardId);
        map.put("serviceId", serviceId);

        System.out.println("getJobCardCheckList map:"+map);
        try {
            tempList = (ArrayList) getSqlMapClientTemplate().queryForList("service.getJobCardCheckList", map);
            System.out.println("getJobCardCheckList size:"+tempList.size());

        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJobCardCheckList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getProblemCauses", sqlException);
        }
        return tempList;
    }

    public int updateProblemCauseDetails(int activityId, String activityName, String desc1, int probId, String aStatus) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int status = 0;
        map.put("activityName", activityName);
        map.put("activityId", activityId);
        map.put("desc", desc1);
        map.put("probId", probId);
        map.put("status", aStatus);


        try {

            status = (Integer) getSqlMapClientTemplate().update("service.updateProblemCauseDetails", map);


        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateProblemCauseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateProblemCauseDetails", sqlException);
        }
        return status;

    }

    public int insertProblemCauseDetails(String activityName, String desc1, int probId, String aStatus, int userId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int status = 0;
        map.put("activityName", activityName);
        map.put("desc", desc1);
        map.put("probId", probId);
        map.put("status", aStatus);
        map.put("userId", userId);

        try {

            status = (Integer) getSqlMapClientTemplate().update("service.insertProblemCauseDetails", map);


        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertProblemCauseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertProblemCauseDetails", sqlException);
        }
        return status;
    }

    public ArrayList getServiceList() {
            Map map = new HashMap();
            /*
             * set the parameters in the map for sending to ORM
             */
            ArrayList serviceList = new ArrayList();



            try {
                serviceList = (ArrayList) getSqlMapClientTemplate().queryForList("service.getServiceList", map);


            } catch (Exception sqlException) {

                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getProblemCauses Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-SYS-01", CLASS,
                        "getProblemCauses", sqlException);
            }
            return serviceList;
        }

         public ArrayList getServiceCheckList(String serviceId) {
            Map map = new HashMap();
            /*
             * set the parameters in the map for sending to ORM
             */
            ArrayList serviceList = new ArrayList();
                map.put("serviceId",serviceId);
             System.out.println("map:;;;"+map);

            try {
                serviceList = (ArrayList) getSqlMapClientTemplate().queryForList("service.getServiceCheckList", map);

                System.out.println("serviceList size dao:"+serviceList.size());
            } catch (Exception sqlException) {

                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getProblemCauses Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-SYS-01", CLASS,
                        "getProblemCauses", sqlException);
            }
            return serviceList;
        }

          public int insertServiceCheckListDetails(String serviceId, String checkList,  int userId) {
            Map map = new HashMap();
            /*
             * set the parameters in the map for sending to ORM
             */

            int status = 0;
            map.put("serviceId", serviceId);
            map.put("checkList", checkList);
            map.put("userId", userId);
              System.out.println("map..."+map);
            try {

                status = (Integer) getSqlMapClientTemplate().update("service.insertServiceCheckList", map);


            } catch (Exception sqlException) {

                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("insertProblemCauseDetails Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-SYS-01", CLASS,
                        "insertProblemCauseDetails", sqlException);
            }
            return status;
    }

}
