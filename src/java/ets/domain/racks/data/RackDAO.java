package ets.domain.racks.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import ets.domain.util.FPLogUtils;
import ets.arch.exception.FPRuntimeException;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import ets.domain.racks.business.RackTO;

/**
 *
 * @author vidya
 */
public class RackDAO extends SqlMapClientDaoSupport {

    private final int errorStatus = 4;
    private final static String CLASS = "RackDAO";

    public ArrayList getRackList() {
        Map map = new HashMap();
        ArrayList rackList = new ArrayList();
        try {
            rackList = (ArrayList) getSqlMapClientTemplate().queryForList("rack.getRackList", map);
            //////System.out.println("rackList size=" + rackList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-RACK-01", CLASS, "RackList", sqlException);
        }
        return rackList;
    }

    public int doInsertRack(RackTO rackTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("Name", rackTO.getRackName());
        map.put("Description", rackTO.getRackDescription());
        map.put("userId", userId);
        System.out.print("Name" + rackTO.getRackName());
        try {
            //////System.out.println("test1" + status);
            status = (Integer) getSqlMapClientTemplate().update("rack.insertRack", map);
            //////System.out.println("status is" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-RACK-02", CLASS,
                "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }
    public int doUpdateRack(ArrayList List, int userId) {
        Map map = new HashMap();
        int status = 0;   
          try {
            Iterator itr = List.iterator();
            RackTO rackTO = null;
            while (itr.hasNext()) {
            rackTO = (RackTO) itr.next();
            //////System.out.println("getStatus is-->" + rackTO.getRackStatus());
            map.put("userId", userId);
            map.put("RackId", rackTO.getRackId());
            map.put("RackName", rackTO.getRackName());
            map.put("RackDescription", rackTO.getRackDescription());
            map.put("RackStatus", rackTO.getRackStatus());
            status = (Integer) getSqlMapClientTemplate().update("rack.updateRack", map);
            //////System.out.println("status is" + status);
        } 
          }catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-RACK-03", CLASS, "doUpdateCompany", sqlException);
        }
        return status;
    }
     public int doInsertSubRack(RackTO rackTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("RackId", Integer.parseInt(rackTO.getRackId()));
        map.put("Name", rackTO.getSubRackName());
        map.put("Description", rackTO.getSubRackDescription());
        map.put("userId", userId);
        System.out.print("Name" + rackTO.getSubRackName());
        try {
            //////System.out.println("test1" + status);
            status = (Integer) getSqlMapClientTemplate().update("rack.insertSubRack", map);
            //////System.out.println("status is" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-RACK-02", CLASS,
                "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }
     public ArrayList getSubRackList() {
        Map map = new HashMap();
        ArrayList subRackList = new ArrayList();
        try {
            subRackList = (ArrayList) getSqlMapClientTemplate().queryForList("rack.getSubRackList", map);
            //////System.out.println("subRackList size=" + subRackList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-RACK-01", CLASS, "subRackList", sqlException);
        }
        return subRackList;
    }
     public int doUpdateSubRack(ArrayList List, int userId) {
        Map map = new HashMap();        
        int status = 0;
        try {
            Iterator itr = List.iterator();
            RackTO rackTO = null;
            while (itr.hasNext()) {
                rackTO = (RackTO) itr.next();
                //////System.out.println("subRackId"+rackTO.getSubRackId());
                //////System.out.println("subRackName"+rackTO.getSubRackName());         
                //////System.out.println("subRackDescription"+rackTO.getSubRackDescription());
                //////System.out.println("subRackStatus"+rackTO.getSubRackStatus()); 
                 //////System.out.println("RackId"+(Integer.parseInt(rackTO.getRackId()))); 
                map.put("subRackId", (Integer.parseInt(rackTO.getSubRackId())));     
                map.put("subRackName", rackTO.getSubRackName());                 
                map.put("subRackDescription", rackTO.getSubRackDescription());
                map.put("rackId", (Integer.parseInt(rackTO.getRackId())));
                map.put("subRackStatus", rackTO.getSubRackStatus());              
                map.put("userId", userId);
                status = (Integer) getSqlMapClientTemplate().update("rack.updateSubRack", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-03", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;

    }
     //getSectionList
     /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getSectionList() {
        Map map = new HashMap();
        ArrayList sectionList = new ArrayList();
        try {
            
            sectionList = (ArrayList) getSqlMapClientTemplate().queryForList("rack.SectionList", map);
            // //////System.out.println("i am in getMfrList :"+MfrList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "SectionList", sqlException);
        }
        return sectionList;

    }
    
    
     /**
     * This method used to Insert MFR Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doSectionDetails(RackTO rackTO, int UserId) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", UserId);
        map.put("sectionName", rackTO.getSectionName());
        map.put("description", rackTO.getDescription());

        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("rack.insertSection", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;

    }
    
     /**
     * This method used to Modify MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doSectionDetailsModify(ArrayList List, int UserId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int status = 0;
        try {
            Iterator itr = List.iterator();
            RackTO rackTO = null;
            while (itr.hasNext()) {
                rackTO = (RackTO) itr.next();
                map.put("sectionId", rackTO.getSectionId());
                map.put("sectionName", rackTO.getSectionName());
                map.put("description", rackTO.getDescription()); 
                map.put("activeInd", rackTO.getActiveInd());
                map.put("userId", UserId);
                status = (Integer) getSqlMapClientTemplate().update("rack.updateSection", map);
             }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,"updateSection", sqlException);
        }
        return status;
       }
     /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getUomList() {
        Map map = new HashMap();
        ArrayList uomList = new ArrayList();
        try {
            
            uomList = (ArrayList) getSqlMapClientTemplate().queryForList("rack.UomList", map);
              
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "UomList", sqlException);
        }
        return uomList;

    }
    
    
    // parts starts here...
    
  /**
     * This method used to Insert Parts Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doPartsDetails(RackTO rackTO, int UserId) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", UserId);
        map.put("sectionId", rackTO.getSectionId() );
        map.put("mfrId", rackTO.getMfrId());
        map.put("modelId",rackTO.getModelId());
        map.put("itemCode",rackTO.getItemCode());
        map.put("paplCode", rackTO.getPaplCode());
        map.put("itemName", rackTO.getItemName());
        map.put("uom",rackTO.getUomId());
        map.put("specification", rackTO.getSpecification());
        map.put("reConditionable", rackTO.getReConditionable());
        map.put("scrapUomId",rackTO.getScrapUomId());
        map.put("maxQuandity",rackTO.getMaxQuandity());
        map.put("roLevel",rackTO.getRoLevel());
        map.put("subRackId",rackTO.getSubRackId());
        
        
        
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("rack.insertParts", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertParts", sqlException);
        }
        return status;

    }
    
    /**
     * This Ajax method used to Get  ModelList.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception Model
     */
    public ArrayList getAjaxModelList(int mfrId) {
        Map map = new HashMap();
        ArrayList ajaxModelList = new ArrayList();
        map.put("mfrId", mfrId);
        try {
            
            ajaxModelList = (ArrayList) getSqlMapClientTemplate().queryForList("rack.AjaxModelList", map);
              
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "AjaxModelList", sqlException);
        }
        return ajaxModelList;

    }
   
    /**
     * This ajax method used to Get SubRack List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception Model
     */
    public ArrayList getAjaxSubRackList(int rackId) {
        Map map = new HashMap();
        ArrayList ajaxSubRackList = new ArrayList();
        map.put("rackId", rackId);
        //////System.out.println("rackId in DAO"+rackId);
        try {
            
            ajaxSubRackList = (ArrayList) getSqlMapClientTemplate().queryForList("rack.AjaxSubRackList", map);
              
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "AjaxSubRackList", sqlException);
        }
        return ajaxSubRackList;

    }
    
    /**
     * This  method used to Get Parts List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception Model
     */
    public ArrayList getPartsDetails(RackTO rackTO) {
        Map map = new HashMap();
        ArrayList List = new ArrayList();
        
        //////System.out.println("rackTO.getMfrId():"+rackTO.getMfrId());
        //////System.out.println("rackTO.getModelId():"+rackTO.getModelId());
        //////System.out.println("rackTO.getSectionId():"+rackTO.getSectionId());
        //////System.out.println("rackTO.getItemCode():"+rackTO.getItemCode());
        //////System.out.println("rackTO.getPaplCode():"+rackTO.getPaplCode());
        //////System.out.println("rackTO.getItemName():"+rackTO.getItemName());
        //////System.out.println("rackTO.getReConditionable():"+rackTO.getReConditionable());
        map.put("mfrId", rackTO.getMfrId());
        map.put("modelId", rackTO.getModelId() );
        map.put("sectionId",rackTO.getSectionId());
        map.put("itemCode", rackTO.getItemCode());
        map.put("paplCode",rackTO.getPaplCode());
        map.put("itemName", rackTO.getItemName());
        map.put("reConditionable",rackTO.getReConditionable());
        try {

            List = (ArrayList) getSqlMapClientTemplate().queryForList("rack.PartsList", map);
            //////System.out.println("List.size=" + List.size());
            

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "PartsList", sqlException);
        }
        return List;

    }
   
    /**
     * This  method used to Alter Parts List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception Model
     */
     public ArrayList getAlterPartsDetail(int itemId) {
        Map map = new HashMap();
        map.put("itemId", itemId);
        //////System.out.println("partsId in dao" + itemId);
        ArrayList partsDetail = null;
        try {
            partsDetail = (ArrayList) getSqlMapClientTemplate().queryForList("rack.GetPartsDetail", map);
            //////System.out.println("company size in dao " + partsDetail.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "GetPartsDetail", sqlException);
        }
        return partsDetail;
    }
     
   /**
     * This method used to Update Parts  Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doPartsUpdateDetails(RackTO rackTO, int UserId) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", UserId);
        map.put("sectionId", rackTO.getSectionId() );
        map.put("mfrId", rackTO.getMfrId());
        map.put("modelId",rackTO.getModelId());
        map.put("mfrCode",rackTO.getMfrCode());
        map.put("paplCode", rackTO.getPaplCode());
        map.put("itemName", rackTO.getItemName());
        map.put("uomId",rackTO.getUomId());
        map.put("specification", rackTO.getSpecification());
        map.put("reConditionable", rackTO.getReConditionable());
        map.put("scrapUomId",rackTO.getScrapUomId());
        map.put("maxQuandity",rackTO.getMaxQuandity());
        map.put("roLevel",rackTO.getRoLevel());
        map.put("subRackId",rackTO.getSubRackId());
        map.put("rackId",rackTO.getRackId());
        map.put("itemId",rackTO.getItemId());
     
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("rack.updateParts", map);
            } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "updateParts", sqlException);
        }
        return status;

    }
}
