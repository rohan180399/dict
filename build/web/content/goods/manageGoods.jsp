<%-- 
    Document   : manageVendor
    Created on : Mar 8, 2009, 10:51:13 AM
    Author     : karudaiyar Subramaniam
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.util.*" %>      
<%@ page import="java.http.*" %>      
<%@ page import="ets.domain.goods.business.GoodsTO" %>  
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BUS</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    </head>
   <script language="javascript" src="/throttle/js/validate.js"></script>
<script language="javascript">
function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}

function submitpage(value)
{
  
if(value == "save"){
 document.goodsDetail.action='/throttle/saveGoods.do';
 document.goodsDetail.submit();   
}else if(value == 'add'){
document.goodsDetail.action='/throttle/addGoods.do';
 document.goodsDetail.submit();   
}else if(value == 'search'){       
document.goodsDetail.action='/throttle/searchGoods.do';
 document.goodsDetail.submit();   
}
}



 
   function setVariables()
   {
       
                 var goodsStatus='<%=request.getAttribute("goodsStatus")%>';
                 var fromDat='<%=request.getAttribute("fromDat")%>';
                 var toDate='<%=request.getAttribute("toDate")%>';
                 var gdNo='<%=request.getAttribute("gdNo")%>';
                 var gdType='<%=request.getAttribute("gdType")%>';
                 
                 if(goodsStatus!='null'){
                     document.goodsDetail.goodsStatus.value=goodsStatus;
                 }
                 if(fromDat!='null'){
                     document.goodsDetail.goodsFromDate.value=fromDat;
                 }
                 if(toDate!='null'){
                       document.goodsDetail.goodsToDate.value=toDate;
                 }
                 if(gdNo!='null'){
                     document.goodsDetail.gdNo.value=gdNo;
                 }
                 if(gdType!='null'){
                     document.goodsDetail.goodType.value=gdType;
                 }
                 
                 document.goodsDetail.totGoods.value=document.goodsDetail.total.value;
                 document.goodsDetail.inGoods.value=document.goodsDetail.ingood.value;
                 document.goodsDetail.outGoods.value=document.goodsDetail.outgood.value;
                 
       
   }
  
</script>
    
    <body onload="setVariables()" >
        
        <form method="post" name="goodsDetail" >
<%@ include file="/content/common/path.jsp" %>
      
<%@ include file="/content/common/message.jsp" %>




<table width="900" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
        <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
        </h2></td>
        <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
        </tr>
        <tr id="exp_table" >
        <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
            <div class="tabs" align="left" style="width:850;">
        <ul class="tabNavigation">
		<li style="background:#76b3f1">Manage Goods In Out</li>
	</ul>
        <div id="first">
        <table width="900" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
        <tr>
            <td>GD No</td><td><input name="gdNo"  class="textbox"  type="text" value="" size="20"></td>
            <td><font color="red">*</font>GD Type</td><td><select class="textbox"  name="goodType"  style="width:125px;">
              <option selected value="ALL" >ALL</option>
            <option value="RC"  >Recondition</option>
            <option value="ST" >StockTransfer</option>
            <option value="BD"  >BreakDown</option>
          </select></td>
            <td><font color="red">*</font>Status</td><td><select class="textbox" name="goodsStatus" style="width:125px;">
            <option  selected value="BOTH">BOTH</option>
            <option value="IN">IN</option>
            <option value="OUT">OUT</option>
           </select></td>
            </tr>

        <tr>
        <td><font color="red">*</font>From Date</td><td><input name="goodsFromDate" readonly  class="textbox"  type="text" value="" size="20"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.goodsDetail.goodsFromDate,'dd-mm-yyyy',this)"/></td>
        <td><font color="red">*</font>To Date</td><td><input name="goodsToDate" readonly  class="textbox"  type="text" value="" size="20"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.goodsDetail.goodsToDate,'dd-mm-yyyy',this)"/></td>
        <td><input type="button" name="search" value="search" onClick="submitpage(this.value)" class="button" />
           <input type="button" name="add" value="Add" onClick="submitpage(this.name)" class="button" /> </td>
        </tr>
        </table>
        </div></div>
        </td>
        </tr>
        </table>

<table width="900" cellpadding="0" cellspacing="0" border="0" align="center" class="table5">
    <tr>
    <td align="center"><h2 style="font-size:16px; font-weight:bold; ">Summary:</h2></td>
    <td class="bottom1" align="left"><span style="font-size:13px; font-weight:bold; ">In</span></td>
    <td class="bottom1" height="35"><input name="inGoods" size="5" class="bottom1" type="text" readonly value="0" style="border:none;"></td>
    <td class="bottom1" align="left"><span style="font-size:13px; font-weight:bold;">Out</span></td>
    <td class="bottom1"><input name="outGoods"   size="5" class="bottom1" type="text" readonly value="0" style="border:none;" /></td>
    <td class="bottom1" align="left"><span style="font-size:13px; font-weight:bold; ">Total</span></td>
    <td class="bottom1"><input name="totGoods"  size="5"  class="bottom1" type="text" readonly value="0" style="border:none;"></td>
    </tr>
    </table>


 
<% int se = 0;  %> 
 <% int index = 0;  %> 
 <% int index1 = 0;  %>  
            <br>
                 <c:set var="tot" value="0"></c:set>
                    <c:set var="in" value="0"></c:set>
                    <c:set var="out" value="0"></c:set>
            <c:if test = "${SearchGoodsList != null}" >
            
                <table width="700" align="center" cellpadding="0" border="0" cellspacing="0" id="bg" class="border">
                     
                    <tr align="center">
                        <td height="30" class="contentsub"><div class="contentsub">S.No</div></td>
                        <td  height="30" class="contentsub"><div class="contentsub">GD No</div></td>
                        <td  height="30" class="contentsub"><div class="contentsub">GD Date</div></td>
                        <td  height="30" class="contentsub"><div class="contentsub">GD Type</div></td>                         
                        <td  height="30" class="contentsub"><div class="contentsub">Remarks</div></td>                         
                        <td  height="30" class="contentsub"><div class="contentsub">IN-Time</div></td>
                        <td  height="30" class="contentsub"><div class="contentsub">OUT-Time</div></td>
                        <td  height="30" class="contentsub"><div class="contentsub">OUT</div></td>
                        <td  height="30" class="contentsub"><div class="contentsub">IN</div></td>
                        
                        
                    </tr>
                   
                    <c:forEach  items="${SearchGoodsList}" var="list"> 
                    <c:set var="tot" value="${tot+1}"></c:set>
                    
                        <%

            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
            int c=0;
            int b=0;
                        %>
                        <tr>
                            <td class="<%=classText %>" height="30"><%=se + 1%></td>
                              <td class="<%=classText %>" height="30"><c:out value="${list.gdId}"/></td>
                            <input type="hidden" name="gdIds" value='<c:out value="${list.gdId}"/>'>
                             <input type="hidden" name="gdType" value='<c:out value="${list.gdType}"/>'>
                             <td class="<%=classText %>" height="30"><c:out value="${list.createdOn}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${list.gdType}"/></td>                             
                            
                            <td class="<%=classText %>" height="30"> <textarea class="<%=classText %>" name="remarks" ><c:out value="${list.remark}" /></textarea> </td>
                            
                            <c:if test="${list.intime!= ''}" >   
                            <c:set var="in" value="${in+1}"></c:set>
                            <td class="<%=classText%>" height="30"><c:out value="${list.intime}"/></td>                                                         
                            </c:if>  
                            
                            <c:if test="${list.intime== ''}" >                                                                                       
                            <td class="<%=classText%>" height="30"></td>                              
                            </c:if>  
                           
                            
                           
                            <c:if test="${list.outtime!= ''}" >    
                            <td class="<%=classText%>" height="30"><c:out value="${list.outtime}"/></td>                                                                                                                     
                            <td class="<%=classText %>" width="50" height="30"><input type="checkbox" CHECKED DISABLED  name="out"  value='<%=index1%>' ></td>
                            </c:if>  
                            
                             
                            <c:if test="${list.outtime== ''}" >          
                            <td class="<%=classText%>" width="50" height="30"></td>                            
                             <td class="<%=classText %>" width="50" height="30"><input type="checkbox"  name="out"  value='<%=index1%>' ></td>
                            </c:if>  
  
                              <c:if test="${list.intime!= ''}" >    
                            <td class="<%=classText %>" width="50" height="30"><input type="checkbox" CHECKED DISABLED  name="in"  value='<%=index1%>' ></td>
                            </c:if>  
                            
                            <c:if test="${list.intime== ''}" >                                                                                       
                                <c:if test="${list.gdType== 'RC' || list.gdType== 'ST'}" > 
                                    <td class="<%=classText %>" width="50" height="30"><input type="checkbox"  name="in"  value='<%=index1%>' ></td>
                                </c:if> 
                                <c:if test="${list.gdType== 'BD'}" > 
                                    <td class="<%=classText%>" height="30" width="50">&nbsp;</td>                              
                                </c:if> 
                            </c:if>  
                             <c:if test="${list.intime== '' && list.outtime!= ''}" > 
                             <c:set var="out" value="${out+1}"></c:set>                               
                            </c:if> 
 
                            
                            
                            
                           
                            
                            
                           
                            
                            
                             
                            <%                              
                        index++;
                        index1++;
                        se++;
                            %>
                            
                          </tr>
                    </c:forEach>                       
                </table>
            
           
            <br>
            <center>
               <input type="button" name="save" value="Save" onClick="submitpage(this.name)" class="button" />
               </center>
               </c:if> 
            <input name="total" value='<c:out value="${tot}"/>' type="hidden"> 
            <input name="ingood" value='<c:out value="${in}"/>' type="hidden"> 
            <input name="outgood" value='<c:out value="${out}"/>' type="hidden">  
            
            
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>

