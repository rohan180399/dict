// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   PurchaseTO.java

package ets.domain.purchase.business;


public class PurchaseTO
{


        //    CLPL Vat by ashok start
        private String effectiveDate = "" ;
        private String warranty = "" ;

    public PurchaseTO()
    {
   
        purchaseTypeId = 0;
        vendorId = 0;
        desc = "";
        ledgerId = "";
        buyPrice = "";
        userId = 0;
        mprId = 0;
        itemId = 0;
        reqQty = "";
        approvedQty = "";
        roLevel = 0;
        stockBalance = "";
        poId = 0;
        vendorName = "";
        vendorAddress = "";
        vendorPhone = "";
        itemName = "";
        mfrCode = "";
        paplCode = "";
        uomName = "";
        vendorIds = null;
        status = "";
        purchaseType = "";
        mprDate = "";
        elapsedDays = "";
        toDate = "";
        fromDate = "";
        companyId = "";
        approvedBy = "";
        requestId = "";
        fromSpId = "";
        toSpId = "";
        reqDate = "";
        companyName = "";
        createdDate = "";
        dcNumber = "";
        inVoiceNumber = "";
        inVoiceDate = "";
        inVoiceAmount = "";
        inventoryStatus = "";
        mrp = "";
        itemMrp = "";
        receivedQty = "";
        acceptedQty = "";
        itemAmount = "";
        priceId = "";
        prePrice = "";
        supplyId = "";
        tinno = "";
        tyreNo = "";
        isRt = "";
        poRaisedQty = "";
        vehMfr = "";
        categoryId = "";
        searchAll = "";
        jobCardId = "";
        returnedQty = "";
        tyreId = "";
        tax = "";
        discount = "";
        unitPrice = "";
        posId = "";
        remarks = "";
        splitRemarks = null;
        transactionType = "";
        approvedQtys = null;
        itemIds = null;
        buyPrices = null;
        reqQuant = null;
        acceptedQtys = null;
        itemAmounts = null;
        reqQtys = null;
        taxs = null;
        mrps = null;
        unitPrices = null;
        discounts = null;
        retQtys = null;
        address = null;
        freight = "";
        regNo = "";
        vat = "";
        vatId = "";
        activeInd = "";
        orderType = "";
        paymentDate = "";
        paidAmnt = "";
        paymentId = "";
        payDate = "";
//        bala
        vehicleNo = null;
        usageType = null;
        totalQtys = null;
        unusedQtys = null;
//        bala ends

    }
     private String vendorTypeId = "";

    public String getVendorTypeId() {
        return vendorTypeId;
    }

    public void setVendorTypeId(String vendorTypeId) {
        this.vendorTypeId = vendorTypeId;
    }
     

    public String[] getUnusedQtys() {
        return unusedQtys;
    }

    public void setUnusedQtys(String[] unusedQtys) {
        this.unusedQtys = unusedQtys;
    }

    public String[] getTotalQtys() {
        return totalQtys;
    }

    public void setTotalQtys(String[] totalQtys) {
        this.totalQtys = totalQtys;
    }

    public String getUsageType() {
        return usageType;
    }

    public void setUsageType(String usageType) {
        this.usageType = usageType;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }
//bala ends

    public String getRemarks()
    {
        return remarks;
    }

    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }

    public int getPurchaseTypeId()
    {
        return purchaseTypeId;
    }

    public void setPurchaseTypeId(int purchaseTypeId)
    {
        this.purchaseTypeId = purchaseTypeId;
    }

    public int getUserId()
    {
        return userId;
    }

    public void setUserId(int userId)
    {
        this.userId = userId;
    }

    public int getVendorId()
    {
        return vendorId;
    }

    public void setVendorId(int vendorId)
    {
        this.vendorId = vendorId;
    }

    public int getMprId()
    {
        return mprId;
    }

    public void setMprId(int mprId)
    {
        this.mprId = mprId;
    }

    public int[] getVendorIds()
    {
        return vendorIds;
    }

    public void setVendorIds(int vendorIds[])
    {
        this.vendorIds = vendorIds;
    }

    public int getItemId()
    {
        return itemId;
    }

    public void setItemId(int itemId)
    {
        this.itemId = itemId;
    }

    public String getVendorName()
    {
        return vendorName;
    }

    public void setVendorName(String vendorName)
    {
        this.vendorName = vendorName;
    }

    public String getElapsedDays()
    {
        return elapsedDays;
    }

    public void setElapsedDays(String elapsedDays)
    {
        this.elapsedDays = elapsedDays;
    }

    public String getMprDate()
    {
        return mprDate;
    }

    public void setMprDate(String mprDate)
    {
        this.mprDate = mprDate;
    }

    public String getPurchaseType()
    {
        return purchaseType;
    }

    public void setPurchaseType(String purchaseType)
    {
        this.purchaseType = purchaseType;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getMfrCode()
    {
        return mfrCode;
    }

    public void setMfrCode(String mfrCode)
    {
        this.mfrCode = mfrCode;
    }

    public String getPaplCode()
    {
        return paplCode;
    }

    public void setPaplCode(String paplCode)
    {
        this.paplCode = paplCode;
    }

    public String getUomName()
    {
        return uomName;
    }

    public void setUomName(String uomName)
    {
        this.uomName = uomName;
    }

    public String getItemName()
    {
        return itemName;
    }

    public void setItemName(String itemName)
    {
        this.itemName = itemName;
    }

    public String[] getApprovedQtys()
    {
        return approvedQtys;
    }

    public void setApprovedQtys(String approvedQtys[])
    {
        this.approvedQtys = approvedQtys;
    }

    public String getDesc()
    {
        return desc;
    }

    public void setDesc(String desc)
    {
        this.desc = desc;
    }

    public String[] getItemIds()
    {
        return itemIds;
    }

    public void setItemIds(String itemIds[])
    {
        this.itemIds = itemIds;
    }

    public String getFromDate()
    {
        return fromDate;
    }

    public void setFromDate(String fromDate)
    {
        this.fromDate = fromDate;
    }

    public String getToDate()
    {
        return toDate;
    }

    public void setToDate(String toDate)
    {
        this.toDate = toDate;
    }

    public String getCompanyId()
    {
        return companyId;
    }

    public void setCompanyId(String companyId)
    {
        this.companyId = companyId;
    }

    public String getApprovedBy()
    {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy)
    {
        this.approvedBy = approvedBy;
    }

    public String getVendorAddress()
    {
        return vendorAddress;
    }

    public void setVendorAddress(String vendorAddress)
    {
        this.vendorAddress = vendorAddress;
    }

    public int getPoId()
    {
        return poId;
    }

    public void setPoId(int poId)
    {
        this.poId = poId;
    }

    public String getVendorPhone()
    {
        return vendorPhone;
    }

    public void setVendorPhone(String vendorPhone)
    {
        this.vendorPhone = vendorPhone;
    }

    public int getRoLevel()
    {
        return roLevel;
    }

    public void setRoLevel(int roLevel)
    {
        this.roLevel = roLevel;
    }

    public String getFromSpId()
    {
        return fromSpId;
    }

    public void setFromSpId(String fromSpId)
    {
        this.fromSpId = fromSpId;
    }

    public String getRequestId()
    {
        return requestId;
    }

    public void setRequestId(String requestId)
    {
        this.requestId = requestId;
    }

    public String getToSpId()
    {
        return toSpId;
    }

    public void setToSpId(String toSpId)
    {
        this.toSpId = toSpId;
    }

    public String getReqDate()
    {
        return reqDate;
    }

    public void setReqDate(String reqDate)
    {
        this.reqDate = reqDate;
    }

    public String[] getReqQuant()
    {
        return reqQuant;
    }

    public void setReqQuant(String reqQuant[])
    {
        this.reqQuant = reqQuant;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public String getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(String createdDate)
    {
        this.createdDate = createdDate;
    }

    public String getDcNumber()
    {
        return dcNumber;
    }

    public void setDcNumber(String dcNumber)
    {
        this.dcNumber = dcNumber;
    }

    public String getInVoiceDate()
    {
        return inVoiceDate;
    }

    public void setInVoiceDate(String inVoiceDate)
    {
        this.inVoiceDate = inVoiceDate;
    }

    public String getInVoiceNumber()
    {
        return inVoiceNumber;
    }

    public void setInVoiceNumber(String inVoiceNumber)
    {
        this.inVoiceNumber = inVoiceNumber;
    }

    public String getInVoiceAmount()
    {
        return inVoiceAmount;
    }

    public void setInVoiceAmount(String inVoiceAmount)
    {
        this.inVoiceAmount = inVoiceAmount;
    }

    public String getInventoryStatus()
    {
        return inventoryStatus;
    }

    public void setInventoryStatus(String inventoryStatus)
    {
        this.inventoryStatus = inventoryStatus;
    }

    public String getAcceptedQty()
    {
        return acceptedQty;
    }

    public void setAcceptedQty(String acceptedQty)
    {
        this.acceptedQty = acceptedQty;
    }

    public String getItemAmount()
    {
        return itemAmount;
    }

    public void setItemAmount(String itemAmount)
    {
        this.itemAmount = itemAmount;
    }

    public String getMrp()
    {
        return mrp;
    }

    public void setMrp(String mrp)
    {
        this.mrp = mrp;
    }

    public String getReceivedQty()
    {
        return receivedQty;
    }

    public void setReceivedQty(String receivedQty)
    {
        this.receivedQty = receivedQty;
    }

    public String getPrePrice()
    {
        return prePrice;
    }

    public void setPrePrice(String prePrice)
    {
        this.prePrice = prePrice;
    }

    public String getPriceId()
    {
        return priceId;
    }

    public void setPriceId(String priceId)
    {
        this.priceId = priceId;
    }

    public String getSupplyId()
    {
        return supplyId;
    }

    public void setSupplyId(String supplyId)
    {
        this.supplyId = supplyId;
    }

    public String[] getAcceptedQtys()
    {
        return acceptedQtys;
    }

    public void setAcceptedQtys(String acceptedQtys[])
    {
        this.acceptedQtys = acceptedQtys;
    }

    public String[] getItemAmounts()
    {
        return itemAmounts;
    }

    public void setItemAmounts(String itemAmounts[])
    {
        this.itemAmounts = itemAmounts;
    }

    public String getIsRt()
    {
        return isRt;
    }

    public void setIsRt(String isRt)
    {
        this.isRt = isRt;
    }

    public String getTyreNo()
    {
        return tyreNo;
    }

    public void setTyreNo(String tyreNo)
    {
        this.tyreNo = tyreNo;
    }

    public String getPoRaisedQty()
    {
        return poRaisedQty;
    }

    public void setPoRaisedQty(String poRaisedQty)
    {
        this.poRaisedQty = poRaisedQty;
    }

    public String getCategoryId()
    {
        return categoryId;
    }

    public void setCategoryId(String categoryId)
    {
        this.categoryId = categoryId;
    }

    public String getVehMfr()
    {
        return vehMfr;
    }

    public void setVehMfr(String vehMfr)
    {
        this.vehMfr = vehMfr;
    }

    public String getSearchAll()
    {
        return searchAll;
    }

    public void setSearchAll(String searchAll)
    {
        this.searchAll = searchAll;
    }

    public String getJobCardId()
    {
        return jobCardId;
    }

    public void setJobCardId(String jobCardId)
    {
        this.jobCardId = jobCardId;
    }

    public String getReturnedQty()
    {
        return returnedQty;
    }

    public void setReturnedQty(String returnedQty)
    {
        this.returnedQty = returnedQty;
    }

    public String getTyreId()
    {
        return tyreId;
    }

    public void setTyreId(String tyreId)
    {
        this.tyreId = tyreId;
    }

    public String getTax()
    {
        return tax;
    }

    public void setTax(String tax)
    {
        this.tax = tax;
    }

    public String getUnitPrice()
    {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice)
    {
        this.unitPrice = unitPrice;
    }

    public String getPosId()
    {
        return posId;
    }

    public void setPosId(String posId)
    {
        this.posId = posId;
    }

    public String getApprovedQty()
    {
        return approvedQty;
    }

    public void setApprovedQty(String approvedQty)
    {
        this.approvedQty = approvedQty;
    }

    public String getReqQty()
    {
        return reqQty;
    }

    public void setReqQty(String reqQty)
    {
        this.reqQty = reqQty;
    }

    public String[] getReqQtys()
    {
        return reqQtys;
    }

    public void setReqQtys(String reqQtys[])
    {
        this.reqQtys = reqQtys;
    }

    public String getStockBalance()
    {
        return stockBalance;
    }

    public void setStockBalance(String stockBalance)
    {
        this.stockBalance = stockBalance;
    }

    public String getDiscount()
    {
        return discount;
    }

    public void setDiscount(String discount)
    {
        this.discount = discount;
    }

    public String getTransactionType()
    {
        return transactionType;
    }

    public void setTransactionType(String transactionType)
    {
        this.transactionType = transactionType;
    }

    public String[] getDiscounts()
    {
        return discounts;
    }

    public void setDiscounts(String discounts[])
    {
        this.discounts = discounts;
    }

    public String[] getMrps()
    {
        return mrps;
    }

    public void setMrps(String mrps[])
    {
        this.mrps = mrps;
    }

    public String[] getTaxs()
    {
        return taxs;
    }

    public void setTaxs(String taxs[])
    {
        this.taxs = taxs;
    }

    public String[] getUnitPrices()
    {
        return unitPrices;
    }

    public void setUnitPrices(String unitPrices[])
    {
        this.unitPrices = unitPrices;
    }

    public String[] getRetQtys()
    {
        return retQtys;
    }

    public void setRetQtys(String retQtys[])
    {
        this.retQtys = retQtys;
    }

    public String getFreight()
    {
        return freight;
    }

    public void setFreight(String freight)
    {
        this.freight = freight;
    }

    public String[] getAddress()
    {
        return address;
    }

    public void setAddress(String address[])
    {
        this.address = address;
    }

    public String getRegNo()
    {
        return regNo;
    }

    public void setRegNo(String regNo)
    {
        this.regNo = regNo;
    }

    public String getVat()
    {
        return vat;
    }

    public void setVat(String vat)
    {
        this.vat = vat;
    }

    public String getVatId()
    {
        return vatId;
    }

    public void setVatId(String vatId)
    {
        this.vatId = vatId;
    }

    public String getActiveInd()
    {
        return activeInd;
    }

    public void setActiveInd(String activeInd)
    {
        this.activeInd = activeInd;
    }

    public String[] getSplitRemarks()
    {
        return splitRemarks;
    }

    public void setSplitRemarks(String splitRemarks[])
    {
        this.splitRemarks = splitRemarks;
    }

    public String getOrderType()
    {
        return orderType;
    }

    public void setOrderType(String orderType)
    {
        this.orderType = orderType;
    }

    public String getPaymentDate()
    {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate)
    {
        this.paymentDate = paymentDate;
    }

    public String getPaidAmnt()
    {
        return paidAmnt;
    }

    public void setPaidAmnt(String paidAmnt)
    {
        this.paidAmnt = paidAmnt;
    }

    public String getPaymentId()
    {
        return paymentId;
    }

    public void setPaymentId(String paymentId)
    {
        this.paymentId = paymentId;
    }

    public String getPayDate()
    {
        return payDate;
    }

    public void setPayDate(String payDate)
    {
        this.payDate = payDate;
    }

    public String getItemMrp() {
        return itemMrp;
    }

    public void setItemMrp(String itemMrp) {
        this.itemMrp = itemMrp;
    }

    public String getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(String buyPrice) {
        this.buyPrice = buyPrice;
    }

    public String[] getBuyPrices() {
        return buyPrices;
    }

    public void setBuyPrices(String[] buyPrices) {
        this.buyPrices = buyPrices;
    }

    public String getTinno() {
        return tinno;
    }

    public void setTinno(String tinno) {
        this.tinno = tinno;
    }

    

    

    private int purchaseTypeId;
    private int vendorId;
    private String desc;
    private String ledgerId;
    private int userId;
    private int mprId;
    private int itemId;
    private String reqQty;
    private String approvedQty;
    private int roLevel;
    private String stockBalance;
    private int poId;
    private String vendorName;
    private String vendorAddress;
    private String vendorPhone;
    private String itemName;
    private String mfrCode;
    private String paplCode;
    private String uomName;
    private int vendorIds[];
    private String status;
    private String purchaseType;
    private String mprDate;
    private String elapsedDays;
    private String toDate;
    private String fromDate;
    private String companyId;
    private String approvedBy;
    private String requestId;
    private String fromSpId;
    private String toSpId;
    private String reqDate;
    private String companyName;
    private String createdDate;
    private String dcNumber;
    private String inVoiceNumber;
    private String inVoiceDate;
    private String inVoiceAmount;
    private String inventoryStatus;
    private String mrp;
    private String itemMrp;
    private String receivedQty;
    private String acceptedQty;
    private String itemAmount;
    private String priceId;
    private String prePrice;
    private String supplyId;
    private String tinno;
    private String tyreNo;
    private String isRt;
    private String poRaisedQty;
    private String vehMfr;
    private String categoryId;
    private String searchAll;
    private String jobCardId;
    private String returnedQty;
    private String tyreId;
    private String tax;
    private String discount;
    private String unitPrice;
    private String posId;
    private String remarks;
    private String splitRemarks[];
    private String transactionType;
    private String approvedQtys[];
    private String itemIds[];
    private String buyPrices[];
    private String reqQuant[];
    private String acceptedQtys[];
    private String itemAmounts[];
    private String reqQtys[];
    private String taxs[];
    private String mrps[];
    private String unitPrices[];
    private String discounts[];
    private String retQtys[];
    private String address[];
    private String freight;
    private String regNo;
    private String vat;
    private String vatId;
    private String activeInd;
    private String orderType;
    private String paymentDate;
    private String paidAmnt;
    private String paymentId;
    private String payDate;

    private String vehicleNo;
    private String buyPrice;
    private String usageType;

    private String totalQtys[];
    private String unusedQtys[];

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getLedgerId() {
        return ledgerId;
    }

    public void setLedgerId(String ledgerId) {
        this.ledgerId = ledgerId;
    }

    public String getWarranty() {
        return warranty;
    }

    public void setWarranty(String warranty) {
        this.warranty = warranty;
    }
    
    
}
