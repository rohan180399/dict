
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>          
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script> 
        <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"> </script> 
        <title>MRSList</title>
    </head>
    <body>
        
        
        <script>
            function submitPag(){
                
                document.mpr.dat1.value = dateFormat(document.mpr.dat1);
                document.mpr.dat2.value = dateFormat(document.mpr.dat2);
                alert(document.mpr.dat1.value);
                document.mpr.action="/throttle/mprApprovalList.do";
                document.mpr.submit();
            }

            <%--Hari--%>
                function details(indx){                    
    var url = '/throttle/handleReqIdDetail.do?requestId='+indx;
    window.open(url , 'PopupPage', 'height=450,width=650,scrollbars=yes,resizable=yes');
}
            
        </script>
        
                
                 <script>
   function changePageLanguage(langSelection){
   if(langSelection== 'ar'){
   document.getElementById("pAlign").style.direction="rtl";
   }else if(langSelection== 'en'){
   document.getElementById("pAlign").style.direction="ltr";
   }
   }
 </script>

  <c:if test="${jcList != null}">
  <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
  </c:if>
      
  <span style="float: right">
	<a href="?paramName=en">English</a>
	|
	<a href="?paramName=ar">Arabic</a>
  </span>
                
                
        <form name="mpr"  method="post" >                    
            <%@ include file="/content/common/path.jsp" %>            
            <!-- message table -->           
            <%@ include file="/content/common/message.jsp" %>    
            <br>
   
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" id="bg">
                <tr>
                    <td colspan="9" align="center" class="contenthead" height="30"><spring:message code="stores.label.StockTransferRequestList"  text="default text"/>
 </td>
                </tr>
                <tr>
                    <td class="contentsub" height="30"><spring:message code="stores.label.RequestNo"  text="default text"/></td>
                    <td class="contentsub" height="30"><spring:message code="stores.label.ToServicePoint"  text="default text"/></td>
                    <td class="contentsub" height="30"><spring:message code="stores.label.CreatedDate"  text="default text"/></td>
                    <td class="contentsub" height="30"><spring:message code="stores.label.RequiredDate"  text="default text"/></td>
                    <td class="contentsub" height="30"><spring:message code="stores.label.ElapsedDays"  text="default text"/></td>
                    <td class="contentsub" height="30"><spring:message code="stores.label.Status"  text="default text"/></td>
                </tr>
                <% int index = 0;
            String classText = "";
            int oddEven = 0;
                %>
                <c:if test = "${requestList != null}" >
                    <c:forEach items="${requestList}" var="req"> 
                        <%

            oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr>
                            <td class="<%=classText %>" height="30"><a href="" onClick="details('<c:out value="${req.requestId}"/> ')" ><c:out value="${req.requestId}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${req.companyName}"/></td>                       
                            <td class="<%=classText %>" height="30"><c:out value="${req.createdDate}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${req.reqDate}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${req.elapsedDays}"/></td>                                                                        
                            <td class="<%=classText %>" height="30"><c:out value="${req.status}"/></td>                             
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach>
                </c:if>                  
            </table>                        
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
