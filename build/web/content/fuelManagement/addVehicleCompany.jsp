
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
    </head>
    <script language="javascript">
        function submitPage(){
            
            var chek=validation();
            if(chek=='true'){
                
                document.addVeh.action='/throttle/addVehicleCompany.do';
                
                document.addVeh.submit();
            }
        }
        
        function validation(){
            if(textValidation(document.addVeh.regNo,'Vehicle No')){
                return 'false';
            }
            if(document.addVeh.vehTypeId.value=='1'){
                alert("Please Select Vehicle Type");
                return 'false';
            }
            if(document.addVeh.owningCompId.value=='0'){
                alert("Please Select Owning Company");
                return 'false';
            }
            if(document.addVeh.companyId.value=='0'){
                alert("Please Select Using Company");
                return 'false';
            }
            
            
            return 'true';
        }
        function getVehicleNos(){
            
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
        }    
        function chekReg(){
            
           
            ajax();  
        }
        function ajax() {
            var regNo=document.addVeh.regNo.value;
            
            if(regNo!=''){
                var url='/throttle/getVehCompDetails.do?regNo='+regNo;
                
                if (window.ActiveXObject)
                    {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)
                        {
                            httpRequest = new XMLHttpRequest();
                        }
                        
                        httpRequest.open("POST", url, true);
                        httpRequest.onreadystatechange = function() {processAjax();};
                        httpRequest.send(null);
                    }
                }
                
                function processAjax()
                {
                    if (httpRequest.readyState == 4)
                        {
                            if(httpRequest.status == 200)
                                {
                                    temp = httpRequest.responseText.valueOf();
                                    
                                    if(temp !=""){
                                        var result=temp.split('-');
                                        
                                        document.addVeh.vehTypeId.value=result[0];                                
                                        document.addVeh.date.value=result[1]+"-"+result[2]+"-"+result[3];
                                        document.addVeh.owningCompId.value=result[4];
                                        document.addVeh.mfrId.value=result[5];
                                        document.addVeh.modelId.value=result[6];                                        
                                        document.addVeh.usageTypeId.value=result[7];
                                        
                                    }
                                }
                                else
                                    {
                                        alert("Error loading page\n"+ httpReq.status +":"+ httpReq.statusText);
                                    }
                                }
                            }    
                            function setValues(){
                                
                                if('<%=request.getAttribute("vehicle")%>'!='null'){
                                    document.addVeh.vehicle.value='<%=request.getAttribute("vehicle")%>';
                                }
                            }
                           
                               var httpReq1;
var temp = "";
function ajaxData()
{
 
var url = "/throttle/getModels1.do?mfrId="+document.addVeh.mfrId.value;    
if (window.ActiveXObject)
{
httpReq1 = new ActiveXObject("Microsoft.XMLHTTP");
}
else if (window.XMLHttpRequest)
{
httpReq1 = new XMLHttpRequest();
}
httpReq1.open("GET", url, true);
httpReq1.onreadystatechange = function() { processAjax1(); } ;
httpReq1.send(null);
}

function processAjax1()
{
if (httpReq1.readyState == 4)
{
	if(httpReq.status == 200)
	{
	temp = httpReq1.responseText.valueOf();                 
        setOptions(temp,document.addVeh.modelId);        
        if('<%= request.getAttribute("modelId")%>' != 'null' ){
            document.addVeh.modelId.value = <%= request.getAttribute("modelId")%>;
        }
	}
	else
	{
	alert("Error loading page\n"+ httpReq1.status +":"+ httpReq1.statusText);
	}
}
}
    </script>
    <body onload="getVehicleNos();setValues();">
        <form name="addVeh" method="post">
            <%@ include file="/content/common/path.jsp" %>                 
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <table align="center" width="500" border="0" class="border" cellspacing="0" cellpadding="0">
                <tr height="30">
                    <Td colspan="2" class="contenthead">Add vehicle Company</Td>
                </tr>
                
                <tr height="30">
                    <td class="text2"><font color="red">*</font>Vehicle Number</td>
                    <td class="text2"><input name="regNo" id="regno" class="form-control" onfocusout="chekReg();"  type="text"   size="20"></td>
                    <input name="vehicle"   type="hidden"   size="20"> 
                </tr>
                <tr>
                    
                    <td class="text1"><font color="red">*</font>Vehicle Type</td>
                    <td class="text1"><select class="form-control" name="vehTypeId"  style="width:125px">
                            <option value="1">---Select---</option>
                            <c:if test = "${TypeList != null}" >
                                <c:forEach items="${TypeList}" var="veh"> 
                                    <option value='<c:out value="${veh.typeId}" />'><c:out value="${veh.typeName}" /></option>
                                </c:forEach>
                            </c:if>  	
                    </select></td>
                </tr>
                <tr>
                    <td height="30" class="text2"><font color="red">*</font>MFR</td>
                    <td height="30" class="text2"><select name="mfrId" onchange="ajaxData();" class="form-control" style="width:125px">
                            <option value="0">-select-</option>
                            <c:if test = "${MfrLists != null}" >
                                <c:forEach items="${MfrLists}" var="mfr"> 
                                    <option value="<c:out value="${mfr.mfrId}"/>"><c:out value="${mfr.mfrName}"/></option>
                                </c:forEach>
                            </c:if>        
                    </select></td>
                </tr>
                <tr>
                    <td height="30" class="text1"><font color="red">*</font>Model</td>
                    <td height="30" class="text1"><select name="modelId" class="form-control" style="width:125px">
                            <option value="0">-select-</option>  
                             <c:if test = "${ModelLists != null}" >
                                <c:forEach items="${ModelLists}" var="model"> 
                                    <option value="<c:out value="${model.modelId}"/>"><c:out value="${model.modelName}"/></option>
                                </c:forEach>
                            </c:if> 
                    </select></td> 
                </tr>
                <tr>
                    <td height="30" class="text2"><font color="red">*</font>Usage Type</td>
                    <td height="30" class="text2"><select name="usageTypeId" class="form-control" style="width:125px">
                            <option value="0">-select-</option>
                            <c:if test = "${UsageList != null}" >
                                <c:forEach items="${UsageList}" var="type"> 
                                    <option value="<c:out value="${type.usageId}"/>"><c:out value="${type.usageName}"/></option>
                                </c:forEach>
                            </c:if>       
                    </select></td> 
                </tr>
                <tr height="30">
                    <td class="text1"><font color="red">*</font>Owning Company</td>
                    <td height="30" class="text1"><select name="owningCompId" class="form-control" style="width:125px">
                            <option value="0">-select-</option>
                            <c:if test = "${customerList != null}" >
                                <c:forEach items="${customerList}" var="owner"> 
                                    <option value='<c:out value="${owner.custId}"/>'><c:out value="${owner.custName}"/></option>
                                </c:forEach>
                            </c:if>       
                    </select></td> 
                    
                </tr>
                <tr>
                    <td class="text2"><font color="red">*</font>Using Company</td>
                    <td class="text2"><select class="form-control" name="companyId" style="width:125px">
                            <option value="0">---Select---</option>
                            <c:if test = "${UsingCompList != null}" >
                                <c:forEach items="${UsingCompList}" var="comp"> 
                                    <c:if test = "${comp.activeInd=='Y'}" >
                                        <option value='<c:out value="${comp.usingCompId}" />'><c:out value="${comp.companyName}" /></option>
                                    </c:if>
                                </c:forEach>
                            </c:if>  	
                    </select></td>
                </tr>
                <tr height="30">
                    <td class="text1"><font color="red">*</font>Purchase Date</td>
                    <td class="text1"><input name="date" class="form-control"  type="text"   size="20">
                    <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.addVeh.date,'dd-mm-yyyy',this)"/></td>
                    
                </tr>
                 <tr>
                <td class="text2"><font color="red">*</font>Status</td>
                    <td class="text2"> <select class="form-control" name="activeInd" style="width:125px">
                            <option value="0">---Select---</option>
                            <option value="Y">Active</option>
                            <option value="N">InActive</option>                            
                    </select></td>
                </tr>
            </table>
            
            <br><br>
            <center>
                <input type="button" class="button" value="Save" onclick="submitPage();" />
                &emsp;<input type="reset" class="button" value="Clear">
            </center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
