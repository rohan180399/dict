<%-- 
    Document   : PhysicalStockEntry1
    Created on : Nov 1, 2012, 5:11:28 PM
    Author     : admin
--%>

<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*,java.util.*,java.io.*" errorPage="" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script>

            function submitpage(){
                if(document.stockEntry1.itemCode.value != "") {
                document.stockEntry1.action = "PhysicalStockEntry2.jsp";
                document.stockEntry1.method = "post";
                document.stockEntry1.submit();
                } else {
                    alert("Enter Item Code");
                }
            }

        </script>
    </head>
    <body>


        <br>
        <br>
        <form name="stockEntry1">
            <table width="350" cellpadding="0" cellspacing="0" height="150" align="center" class="table2">
                <tr><td colspan="2" align="center" class="contenthead">Stock Manual Entry- Step:1</td>
                </tr>
                <tr>
                    <td>Item Code</td>
                    <td><input type="text" name="itemCode" id="itemCode" /></td>
                </tr>
                <tr><td colspan="2" align="center">
                        <input type="button" name="proceed" value="Proceed" onclick="submitpage();" />
                    </td></tr>
            </table>
        <%
       String message = request.getParameter("message");
       if(message != null && !message.equals("")){
            out.println("<h3 align='center' style='color:#FF0000;'>"+message+"</h3>");
       }

        %>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
