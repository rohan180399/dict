<%--
    Document   : ViewTyreRotationList
    Created on : 15 Mar, 2012, 5:59:24 PM
    Author     : kannan
--%>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<title>ViewTyreRotationDetails</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code=""  text="Tyres"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code=""  text="Tyres"/></a></li>
            <li class="active"><spring:message code=""  text="Rotation"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">
<body>
    <form name="viewTyresRotationList">
        <table class="table table-info mb30 table-hover">
            <tr>
                                    <td>Vehicle No</td><td>
                                        <input type="text" class="textbox" id="regno" name="regNo" value="" />
                                    </td>
                                    <td>Make</td><td><select  name="mfrId" id="mfrId" class="textbox">
                                            <option value="">---Select---</option>
                                            <c:if test = "${mfrList != null}" >
                                                <c:forEach items="${mfrList}" var="mfr">
                                                    <option  value='<c:out value="${mfr.mfrId}" />'>
                                                        <c:out value="${mfr.mfrName}" />
                                                    </c:forEach >
                                                </c:if>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td><font color="red">*</font>From Date</td><td><input name="fromDate" id="fromDate" readonly  class="textbox"  type="text" value="" size="20"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.viewTyresRotationList.fromDate, 'dd-mm-yyyy', this)"/></td>
                                    <td><font color="red">*</font>To Date</td><td><input name="toDate" id="toDate" readonly  class="textbox"  type="text" value="" size="20"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.viewTyresRotationList.toDate, 'dd-mm-yyyy', this)"/></td>
                                    <td colspan="1" rowpan="2"><input type="button" name="search" value="View List" class="button" /></td>
                                </tr>
        </table>
        <script language="javascript">
            var vehicleNo = "<%=request.getAttribute("regNo")%>";
            if (vehicleNo != null && vehicleNo != "null") {
                document.viewTyresRotationList.regno.value = vehicleNo;
            }
            var mfrId = "<%=request.getAttribute("mfrId")%>";
            if (mfrId != null && mfrId != "null") {
                document.viewTyresRotationList.mfrId.value = mfrId;
            }
            var fromDate = "<%=request.getAttribute("fromDate")%>";
            if (fromDate != null && fromDate != "null") {
                document.viewTyresRotationList.fromDate.value = fromDate;
            }
            var toDate = "<%=request.getAttribute("toDate")%>";
            if (toDate != null && toDate != "null") {
                document.viewTyresRotationList.toDate.value = toDate;
            }
        </script>
        <br>
        <table class="table table-info mb30 table-hover" id="table" >
            <thead>
                <tr height="30">
                    <th>S.No</th>
                    <th>Vehicle No</th>
                    <th>Mrf Name</th>
                    <th>Model Name</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <% int sno = 0;%>
                <c:if test = "${tyresRotationList != null}">
                    <c:forEach items="${tyresRotationList}" var="cml">
                        <%
                                    sno++;
                                    String className = "text1";
                                    if ((sno % 1) == 0) {
                                        className = "text1";
                                    } else {
                                        className = "text2";
                                    }
                        %>

                        <tr>
                            <td class="<%=className%>"  align="left"> <%= sno%> </td>
                            <td class="<%=className%>"  align="left"><c:out value="${cml.vehicleRegNo}"/></td>
                            <td class="<%=className%>"  align="left"> <c:out value="${cml.mfrName}" /></td>
                            <td class="<%=className%>"  align="left"> <c:out value="${cml.modelName}" /></td>
                            <td class="<%=className%>"  align="left"><a href="/throttle/tyresRotation.do?vehicleId=<c:out value='${cml.vehicleId}' />&axleTypeId=<c:out value='${cml.axleTypeId}' />">Rotation</a></td>
                        </tr>
                    </c:forEach>
                </tbody>
                <input type="hidden" name="count" id="count" value="<%=sno%>" />
            </c:if>
        </table>
        <br>
        <br>
        <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5" selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>
        <br>
        <br>
        <br>
        <br>
        <br>
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</div>
    </div>
    </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

