<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
    function submitPage() {
        document.popUpCustomerReport.action = '/throttle/viewCustomerWiseProfitDetails.do?param=ExportExcel';
        document.popUpCustomerReport.submit();
    }

    function viewTripDetails(tripId) {
        //alert('333');
        window.open('/throttle/viewTripSheetDetails.do?tripId=' + tripId + '&tripSheetId=' + tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }

</script>
<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
                  setValues();
                  getVehicleNos();">
    </c:if>
    <style>
        #index td {
            color:white;
        }
    </style>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i>Margin Wise Trip Summary</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="general-forms.html">Finance</a></li>
                <li class="active">Margin Wise Trip Summary</li>
            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body>
                    <form name="popUpCustomerReport" action=""  method="post">
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <c:if test = "${popupCustomerProfitReport != null}" >
                            <table class="table table-info mb30 table-hover" id="table" class="sortable">
                                <thead>
                                    <tr >
                                        <th class="contentsub"><spring:message code="operations.reports.label.SNo" text="default text"/></th>
                                        <th class="contentsub"><spring:message code="operations.reports.label.TripCode" text="default text"/></th>
                                        <th class="contentsub"><spring:message code="operations.reports.label.CNote" text="default text"/></th>
                                        <th class="contentsub"><spring:message code="operations.reports.label.TripStartDate" text="default text"/></th>
                                        <th class="contentsub"><spring:message code="operations.reports.label.TripEndDate" text="default text"/></th>
                                        <th class="contentsub"><spring:message code="operations.reports.label.Customer" text="default text"/></th>
                                        <th class="contentsub"><spring:message code="operations.reports.label.Route" text="default text"/></th>
                                        <th class="contentsub"><spring:message code="operations.reports.label.VehicleNo" text="default text"/></th>
                                        <th class="contentsub"><spring:message code="operations.reports.label.Revenue" text="default text"/></th>
                                        <th class="contentsub"><spring:message code="operations.reports.label.Expense" text="default text"/></th>
                                        <th class="contentsub"><spring:message code="operations.reports.label.Profit" text="default text"/></th>
                                        <th class="contentsub"><spring:message code="operations.reports.label.Profit" text="default text"/> %</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% int index = 0,sno = 1;%>
                                    <c:forEach items="${popupCustomerProfitReport}" var="popCus">

                                        <tr height="30">
                                            <td align="left" ><%=sno%></td>
                                            <td align="left" >
                                                <a href="" onclick="viewTripDetails('<c:out value="${popCus.tripId}"/>');"><c:out value="${popCus.tripCode}"/></a>
                                            </td>
                                            <td align="left" ><c:out value="${popCus.consignmentNoteNo}"/></td>
                                            <td align="left" ><c:out value="${popCus.tripStartDate}"/></td>
                                            <td align="left" ><c:out value="${popCus.tripEndDate}"/></td>
                                            <td align="left" ><c:out value="${popCus.customerName}"/></td>
                                            <td align="left" ><c:out value="${popCus.routeName}"/></td>
                                            <td align="left" ><c:out value="${popCus.regno}"/></td>
                                            <td align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${popCus.estimatedRevenue}" /></td>
                                            <td align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${popCus.totalexpenses}" /></td>
                                            <c:if test="${popCus.customerName != 'Empty Trip'}">
                                                <c:set var="profitValue" value="${popCus.estimatedRevenue - popCus.totalexpenses}"/>
                                                <c:set var="profit" value="${profitValue/popCus.estimatedRevenue}"/>
                                                <c:set var="profitPercent" value="${profit*100}"/>
                                                <c:if test="${profitValue > 0}" >
                                                    <td align="right">
                                                        <font color="green">
                                                        <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitValue}" />
                                                        </font>
                                                    </td>
                                                </c:if>
                                                <c:if test="${profitValue <= 0}" >
                                                    <td align="right">
                                                        <font color="red">
                                                        <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitValue}" />
                                                        </font>
                                                    </td>
                                                </c:if>
                                                <c:if test="${profitPercent > 0}" >
                                                    <td align="right">
                                                        <font color="green">
                                                        <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />
                                                        </font>
                                                    </td>
                                                </c:if>
                                                <c:if test="${profitPercent <= 0}" >
                                                    <td align="right">
                                                        <font color="red">
                                                        <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />
                                                        </font>
                                                    </td>
                                                </c:if>
                                            </c:if>
                                            <c:if test="${popCus.customerName == 'Empty Trip'}">
                                                <td align="right"><font color="red">0.00</font></td>
                                                <td align="right"><font color="red">0.00</font></td>
                                                </c:if>
                                        </tr>
                                        <%
                                                   index++;
                                                   sno++;
                                        %>
                                    </c:forEach>

                                </tbody>
                            </table>
                        </c:if>
                        <table style="width: 1100px" align="center" border="0">
                            <tr>
                                <td align="center">
                                    <input type="hidden" name="tripId" id="tripId" value="<c:out value="${tripId}"/>"/>
                                    <input type="button" class="btn btn-success" name="ExportExcel"   value="Export Excel" onclick="submitPage();">
                                </td>
                            </tr>
                        </table>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5" selected="selected">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span>Entries Per Page</span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 1);
                        </script>



                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>