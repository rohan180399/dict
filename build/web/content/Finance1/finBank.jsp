
<%--
    Document   : finBank
    Created on : Oct 19, 2012, 12:58:13 PM
    Author     : ASHOK
--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.finance.business.FinanceTO" %>
        <%@ page import="java.util.*" %>

        <title> Manage Bank</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
    </head>
    <body>
        <form method="post" action="/throttle/addBankPage.do">
<%@ include file="/content/common/path.jsp" %>
<%@ include file="/content/common/message.jsp" %>

     <c:if test = "${bankLists != null}" >
            <table align="center" width="800" border="0" cellspacing="0" cellpadding="0" class="border">

                <tr height="30">
                     <td  align="left" class="contenthead" scope="col"><b>S.No</b></td>
                    <td  align="left" class="contenthead" scope="col"><b>Bank Name</b></td>
                    <td  align="left" class="contenthead" scope="col"><b>Bank code</b></td>
                    <td  align="left" class="contenthead" scope="col"><b>Bank Address</b></td>
                    <td  align="left" class="contenthead" scope="col"><b>Bank Phone No</b></td>
                    <td  align="left" class="contenthead" scope="col"><b>Bank Account Code</b></td>
                    <td  align="left" class="contenthead" scope="col"><b>Bank Ledger Code</b></td>
                    <td  align="left" class="contenthead" scope="col"><b>Description</b></td>
                    <td  align="left" class="contenthead" scope="col"><b>Status</b></td>
                    <td  align="left" class="contenthead" scope="col"><b>Edit</b></td>
                </tr>
                <% int index = 0;%>
                    <c:forEach items="${bankLists}" var="BL">
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text1";
            } else {
                classText = "text2";
            }
                        %>
                        <tr height="30">
                            <td class="<%=classText %>"  align="left"> <%= index + 1 %> </td>
                             <td class="<%=classText %>" align="left"> <c:out value="${BL.bankname}" /></td>
                            <td class="<%=classText %>"  align="left"> <c:out value="${BL.bankcode}"/> </td>
                            <td class="<%=classText %>"  align="left"> <c:out value="${BL.address}"/> </td>
                            <td class="<%=classText %>"  align="left"> <c:out value="${BL.phoneNo}"/> </td>
                            <td class="<%=classText %>"  align="left"> <c:out value="${BL.accntCode}"/> </td>
                            <td class="<%=classText %>"  align="left"> <c:out value="${BL.bankMappingCode}"/> </td>
                            <td class="<%=classText %>"  align="left"> <c:out value="${BL.description}"/> </td>
                            <c:if test="${BL.activeind == 'Y'}">
                                <td class="<%=classText%>"  height="30"><c:out value="Active"/></td>
                            </c:if>
                            <c:if test="${BL.activeind == 'N'}">
                                <td class="<%=classText%>"  height="30"><c:out value="In Active"/></td>
                            </c:if>

                            <td class="<%=classText %>" align="left"> <a href="/throttle/alterBankDetail.do?bankid=<c:out value='${BL.bankid}' />" > Edit </a> </td>
                        </tr>
                        <% index++;%>
                    </c:forEach>
              </c:if>

            </table>
            <br>
                <br>
            <center><input type="submit" class="button" value="Add" /></center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
