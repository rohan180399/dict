<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<!--<title>PAPL</title>-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->


<script>
    function submitPage(val) {

        if (val == 'approve') {
            if (selectedItemValidation(val) == 'pass') {
                if (confirm("Are you sure to submit")) {
                    document.getElementById("buttonStyle").style.visibility = "hidden";
                    document.mpr.status.value = "APPROVED"
                    document.mpr.action = "/throttle/approveMpr.do";
                    document.mpr.submit();
                }
            }
        } else if (val == 'reject') {
            if (selectedItemValidation(val) == 'pass') {
                if (confirm("Are you sure to submit")) {
                    document.getElementById("buttonStyle").style.visibility = "hidden";
                    document.mpr.status.value = "REJECTED"
                    document.mpr.action = "/throttle/approveMpr.do";
                    document.mpr.submit();
                }
            }
        }
    }

    function selectedItemValidation(value) {
        var issueQty = document.getElementsByName("approvedQtys");
        var chec = 0;
        if (value != 'reject') {
            for (var i = 0; (i < issueQty.length && issueQty.length != 0); i++) {
                chec++;
                if (floatValidation(issueQty[i], 'Approve Quantity')) {
                    return 'fail';
                }
            }
        }

        if (value == 'reject') {
            for (var i = 0; (i < issueQty.length && issueQty.length != 0); i++) {
                issueQty[i].value = '0';
            }
        }

        if (textValidation(document.mpr.desc, 'Remarks')) {
            return 'fail';
        }
        return 'pass';
    }


</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.MPR"  text="MPR"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.stores"  text="Stores"/></a></li>
            <li class="active"><spring:message code="stores.label.MPR"  text="MPR"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">
            <body>
                <form name="mpr" method="post" >                    
                    <%@ include file="/content/common/message.jsp" %>    

                    <% int index = 0; %>                
                    <c:if test = "${mprList != null}" >   


                        <c:forEach items="${mprList}" var="mpr">  
                            <% if(index==0){ %>          
                            <input type="hidden" name="mprId" value=<c:out value="${mpr.mprId}"/> >    
                            <table class="table table-info mb30 table-hover">
                                <thead><tr><th colspan="4"><spring:message code="stores.label.MPR"  text="MPR"/></th></tr></thead>
                                <tr>
                                    <td ><spring:message code="stores.label.MPRNO"  text="default text"/></td>
                                    <td > <c:out value="${mpr.mprId}"/> </td>
                                    <!--                </tr>  
                                                    <tr>-->
                                    <td ><spring:message code="stores.label.VendorName"  text="default text"/></td>
                                    <td > <c:out value="${mpr.vendorName}"/> </td>
                                </tr>
                                <tr>
                                    <td ><spring:message code="stores.label.RaisedDate"  text="default text"/></td>
                                    <td > <c:out value="${mpr.mprDate}"/></td>                    
                                </tr>            
                                <% index++; } %>                
                            </c:forEach>

                        </table>  
                        <table class="table table-info mb30 table-hover" id="table">
                            <thead>
                                <%index = 0;
                            String classText = "";
                            int oddEven = 0;
                                %>
                                <c:if test = "${mprList != null}" >
                                    <tr>
                                        <th colspan="6" align="center" ><spring:message code="stores.label.MPRITEMS"  text="default text"/> </th>
                                    </tr>

                                    <tr>
                                        <th ><spring:message code="stores.label.MfrCode"  text="default text"/></th>
                                        <th ><spring:message code="stores.label.PaplCode"  text="default text"/></th>
                                        <th ><spring:message code="stores.label.ItemName"  text="default text"/></th>
                                        <th ><spring:message code="stores.label.Uom"  text="default text"/></th>
                                        <th ><spring:message code="stores.label.RequestedQty"  text="default text"/></th>
                                        <th ><spring:message code="stores.label.Quantity"  text="default text"/> </th>

                                    </tr>
                                </thead>
                                <c:forEach items="${mprList}" var="mpr"> 
                                    <%

                        oddEven = index % 2;
                        if (oddEven > 0) {
                            classText = "text2";
                        } else {
                            classText = "text1";
                        }
                                    %>
                                    <tr>
                                        <td><c:out value="${mpr.mfrCode}"/></td>
                                        <td><c:out value="${mpr.paplCode}"/></td>                       
                                        <td><c:out value="${mpr.itemName}"/></td>
                                        <td><c:out value="${mpr.uomName}"/></td>
                                        <td><c:out value="${mpr.reqQty}"/></td>
                                        <td><input type="text" name="approvedQtys" size="5" class="form-control" value='<c:out value="${mpr.reqQty}"/>' > </td>                                                                        

                                    <input type="hidden" name="itemIds" value=<c:out value="${mpr.itemId}"/> > 
                                    </tr>
                                    <%
                        index++;
                                    %>
                                </c:forEach>

                                <tr>
                                    <td align="right" valign="center" colspan="2" ><spring:message code="stores.label.Remarks"  text="default text"/></td>
                                    <td align="left" colspan="4" ><textarea style="width:260px;height:40px;" name="desc" class="form-control"><spring:message code="stores.label.ApproveMPR"  text="default text"/></textarea></td>
                                </tr>
                            </c:if>                  
                        </table> 

                        <input type="hidden" name="status" value="" >             


                        <div id="buttonStyle" style="visibility:visible;" align="center" >
                            <input type="button" class="btn btn-success" name="approve" value="<spring:message code="stores.label.Approve"  text="default text"/>" onClick="submitPage(this.name);" > &nbsp;
                            <input type="button" class="btn btn-success" name="reject" value="<spring:message code="stores.label.Reject"  text="default text"/>" onClick="submitPage(this.name);" >
                        </div>
                    </c:if>                  
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>




