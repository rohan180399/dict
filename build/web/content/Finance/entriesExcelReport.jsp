<%--
    Document   : tripPlanningExcel
    Created on : Nov 4, 2013, 10:56:05 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.finance.business.FinanceTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
            <%
           Date dNow = new Date();
           SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
           //System.out.println("Current Date: " + ft.format(dNow));
           String curDate = ft.format(dNow);
           String expFile = "Entries_Report_"+curDate+".xls";

           String fileName = "attachment;filename=" + expFile;
           response.setContentType("application/vnd.ms-excel;charset=UTF-8");
           response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
              <c:if test = "${entriesReportList != null && distcheck == 'distDetail'}" >

                <br>
                <table  class="sortable" align="center" width="99%" cellpadding="0" cellspacing="0" id="table">
                    <tr>
                        <td class="contentsub"   height="30">S.No</td>
                        <td class="contentsub"   height="30">Entry Date</td>
                        <td class="contentsub"   height="30">Entry Type</td>
                        <td class="contentsub"   height="30">Voucher Code</td>
                        <td class="contentsub"   height="30">Credit Ledger</td>
                        <td class="contentsub"   height="30">Credit Amount</td>
                        <td class="contentsub"   height="30">Debit Ledger</td>
                        <td class="contentsub"   height="30">Debit Amount</td>
                        <td class="contentsub"   height="30">Narration</td>
                    </tr>
                    <% int index = 0;%>
                    <% int sno = 0;%>
                    <c:set var="accountEntryDate" value=""/>
                    <c:set var="entryStatus" value=""/>
                    <c:set var="totalDebit" value="${0.00}"/>
                    <c:set var="totalCredit" value="${0.00}"/>
                    <c:forEach items="${entriesReportList}" var="PL">
                        <%
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text1";
                                    } else {
                                        classText = "text2";
                                    }
                        %>
                        <tr height="30">
                            <td class="<%=classText%>"  align="left"> 
                                <c:if test="${accountEntryDate != PL.accountEntryDate}">
                                    <%= sno + 1%> 
                                    <% sno++;%>
                                </c:if>
                            </td>
                            <td class="<%=classText%>"  align="left"> 
                                <c:if test="${accountEntryDate != PL.accountEntryDate}">
                                    <c:out value="${PL.accountEntryDate}"/>

                                </c:if>
                                <c:if test = "${accountEntryDate == PL.accountEntryDate}">
                                    &nbsp;
                                </c:if>
                            </td>
                            <td class="<%=classText%>" align="left">
                                <c:if test="${entryStatus != PL.entryStatus}">
                                    <c:if test="${PL.entryStatus == 'CP'}">Cash Payment</c:if>
                                    <c:if test="${PL.entryStatus == 'CR'}">Cash Receipt</c:if>
                                    <c:if test="${PL.entryStatus == 'BP'}">Bank Payment</c:if>
                                    <c:if test="${PL.entryStatus == 'BR'}">Bank Receipt</c:if>
                                    <c:if test="${PL.entryStatus == 'CE'}">Contra Entry</c:if>
                                    <c:if test="${PL.entryStatus == 'JE'}">Journal Entry</c:if>

                                </c:if>
                                <c:if test = "${entryStatus == PL.entryStatus}">
                                    &nbsp;
                                </c:if>

                            </td>
                            <td class="<%=classText%>" align="left"> <c:out value="${PL.voucherCodeNo}" /></td>
                            <c:if test="${entriesReportCreditList != null}">
                                <c:set var="creditLedger" value=""/>
                                <c:set var="totalCreditAmount" value="${0.00}"/>
                                <c:forEach items="${entriesReportCreditList}" var="CL">
                                    <c:if test="${CL.creditVoucherCode == PL.voucherCode}">
                                        <c:set var="totalCreditAmount" value="${totalCreditAmount + CL.creditAmount}"/>
                                        <c:set var="creditLedger" value="${creditLedger}${CL.creditLedgerName}"/>
                                    </c:if>
                                </c:forEach>
                                <td class="<%=classText%>" align="left"> <c:out value="${creditLedger}" /></td>
                            </c:if>
                            <td class="<%=classText%>"  align="left"> <c:out value="${totalCreditAmount}"/> </td>
                            <c:if test="${entriesReportDebitList != null}">
                                <c:set var="debitLedger" value=""/>
                                <c:set var="totalDebitAmount" value="${0.00}"/>
                                <c:forEach items="${entriesReportDebitList}" var="DL">
                                    <c:if test="${DL.debitVoucherCode == PL.voucherCode}">
                                        <c:set var="totalDebitAmount" value="${totalDebitAmount + DL.debitAmount}"/>
                                        <c:set var="debitLedger" value="${debitLedger}${DL.debitLedgerName}"/>

                                    </c:if>
                                </c:forEach>
                                <td class="<%=classText%>" align="left"> <c:out value="${debitLedger}" /></td>
                            </c:if>
                            <td class="<%=classText%>"  align="left"> <c:out value="${totalDebitAmount}"/> </td>
                            <td class="<%=classText%>"  align="left"> <c:out value="${PL.headerNarration}"/> </td>
                            <c:set var="accountEntryDate" value="${PL.accountEntryDate}"/>
                            <c:set var="entryStatus" value="${PL.entryStatus}"/>
                        </tr>
                        <c:set var="totalDebit" value="${totalDebit + totalDebitAmount}"/>
                        <c:set var="totalCredit" value="${totalCredit + totalCreditAmount}"/>


                        <% index++;%>
                    </c:forEach>
                    <tr height="30">
                        <td  colspan="5" align="right"> <b>Total Credit Amount</b></td>
                        <td  align="left"> <b><c:out value="${totalDebit}"/></b> </td>
                        <td  align="right"><b>Total Debit Amount </b></td>
                        <td align="left"> <b><c:out value="${totalCredit}"/></b> </td>
                        <td  align="left"> </td>
                    </tr>

                </table>
                <br>
                <br>
                <script language="javascript" type="text/javascript">
                    setFilterGrid("table");
                </script>

               
                                <script type="text/javascript">
                                    var sorter = new TINY.table.sorter("sorter");
                                    sorter.head = "head";
                                    sorter.asc = "asc";
                                    sorter.desc = "desc";
                                    sorter.even = "evenrow";
                                    sorter.odd = "oddrow";
                                    sorter.evensel = "evenselected";
                                    sorter.oddsel = "oddselected";
                                    sorter.paginate = true;
                                    sorter.currentid = "currentpage";
                                    sorter.limitid = "pagelimit";
                                    sorter.init("table", 0);
                                </script>
            </c:if>
                
            <c:if test = "${entriesReportList != null && distcheck == 'distSummary'}" >

                <br>
                <table align="center" width="100%" border="0" id="table" class="sortable">
                    <tr>
                        <td class="contentsub"   height="30">S.No1</td>
                        <td class="contentsub"   height="30">Entry Type</td>
                        <td class="contentsub"   height="30">Entry Date</td>
                        <td class="contentsub"   height="30">Voucher Code</td>
                        <td class="contentsub"   height="30">Credit Ledger</td>
                        <td class="contentsub"   height="30">Credit Amount</td>
                        <td class="contentsub"   height="30">Debit Ledger</td>
                        <td class="contentsub"   height="30">Debit Amount</td>
                        <td class="contentsub"   height="30">Narration</td>
                    </tr>
                    <% int index = 0;%>
                    <% int sno = 0;%>
                    <c:set var="entryStatus" value=""/>
                    <c:set var="totalDebit" value="${0.00}"/>
                    <c:set var="totalCredit" value="${0.00}"/>
                    <c:forEach items="${entriesReportList}" var="PL">
                        <%
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text1";
                                    } else {
                                        classText = "text2";
                                    }
                        %>
                        <tr height="30">
                            <td class="<%=classText%>"  align="left">
                                <c:if test="${entryStatus != PL.entryStatus}">
                                    <%= sno + 1%> 
                                    <% sno++;%>
                                </c:if>
                                <c:if test="${entryStatus == PL.entryStatus}">
                                    &nbsp;
                                </c:if>
                            </td>
                            <td class="<%=classText%>" align="left">
                                <c:if test="${entryStatus != PL.entryStatus}">
                                    <c:if test="${PL.entryStatus == 'CP'}">Cash Payment</c:if>
                                    <c:if test="${PL.entryStatus == 'CR'}">Cash Receipt</c:if>
                                    <c:if test="${PL.entryStatus == 'BP'}">Bank Payment</c:if>
                                    <c:if test="${PL.entryStatus == 'BR'}">Bank Receipt</c:if>
                                    <c:if test="${PL.entryStatus == 'CE'}">Contra Entry</c:if>
                                    <c:if test="${PL.entryStatus == 'JE'}">Journal Entry</c:if>

                                </c:if>
                                <c:if test = "${entryStatus == PL.entryStatus}">
                                    &nbsp;
                                </c:if>

                            </td>
                            <td class="<%=classText%>"  align="left"> <c:out value="${PL.accountEntryDate}"/> </td>
                            <td class="<%=classText%>" align="left"> <c:out value="${PL.voucherCodeNo}" /></td>
                            <c:if test="${entriesReportCreditList != null}">
                                <c:set var="creditLedger" value=""/>
                                <c:set var="totalCreditAmount" value="${0.00}"/>
                                <c:forEach items="${entriesReportCreditList}" var="CL">
                                    <c:if test="${CL.creditVoucherCode == PL.voucherCode}">
                                        <c:set var="totalCreditAmount" value="${totalCreditAmount + CL.creditAmount}"/>
                                        <c:set var="creditLedger" value="${creditLedger}${CL.creditLedgerName}"/>
                                    </c:if>
                                </c:forEach>
                                <td class="<%=classText%>" align="left"> <c:out value="${creditLedger}" /></td>
                            </c:if>
                            <td class="<%=classText%>"  align="left"> <c:out value="${totalCreditAmount}"/> </td>

                            <c:if test="${entriesReportDebitList != null}">
                                <c:set var="debitLedger" value=""/>
                                <c:set var="totalDebitAmount" value="${0.00}"/>
                                <c:forEach items="${entriesReportDebitList}" var="DL">
                                    <c:if test="${DL.debitVoucherCode == PL.voucherCode}">
                                        <c:set var="totalDebitAmount" value="${totalDebitAmount + DL.debitAmount}"/>
                                        <c:set var="debitLedger" value="${debitLedger}${DL.debitLedgerName}"/>

                                    </c:if>
                                </c:forEach>
                                <td class="<%=classText%>" align="left"> <c:out value="${debitLedger}" /></td>
                            </c:if>
                            <td class="<%=classText%>"  align="left"> <c:out value="${totalDebitAmount}"/> </td>
                            <td class="<%=classText%>"  align="left"> <c:out value="${PL.headerNarration}"/> </td>
                            <c:set var="entryStatus" value="${PL.entryStatus}"/>
                        </tr>
                        <c:set var="totalDebit" value="${totalDebit + totalDebitAmount}"/>
                        <c:set var="totalCredit" value="${totalCredit + totalCreditAmount}"/>
                        <% index++;%>
                    </c:forEach>
                    <tr height="30">
                        <td  colspan="5" align="right"> <b>Total Credit Amount</b></td>
                        <td  align="left"> <b><c:out value="${totalDebit}"/></b> </td>
                        <td  align="right"><b>Total Debit Amount </b></td>
                        <td align="left"> <b><c:out value="${totalCredit}"/></b> </td>
                        <td  align="left"> </td>
                    </tr>

                </table>
            </c:if>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
