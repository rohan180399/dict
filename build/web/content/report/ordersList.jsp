

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
         <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
    </head>
    <script>

function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}
       function submitPage(){
            var chek=validation();

            if(chek=='true'){  
           document.receivedStock.action='/throttle/handleOrderListPage.do';
           document.receivedStock.submit();
           }
           }
             function validation(){
            if(document.receivedStock.companyId.value==0){
                alert("Please Select Data for Company Name");
                document.receivedStock.companyId.focus();
                return 'false';
            }           
            else  if(textValidation(document.receivedStock.fromDate,'From Date')){
                return 'false';
            }
            else  if(textValidation(document.receivedStock.toDate,'TO Date')){
                return'false';
            }
            return 'true';
        }



        function newWindow(indx){
           
            var POId = document.getElementsByName("POId1");
            
            if(document.receivedStock.orderTyp.value == 'PO' ){
                window.open('/throttle/handlePoDetail.do?poId='+POId[indx].value, 'PopupPage', 'height=450,width=700,scrollbars=yes,resizable=yes');
            }else{
            
                //window.open('/throttle/viewGRN.do?supplyId='+poId[indx].value, 'PopupPage', 'height=450,width=700,scrollbars=yes,resizable=yes');
                window.open('/throttle/viewRCWO.do?supplyId='+POId[indx].value, 'PopupPage', 'height=450,width=700,scrollbars=yes,resizable=yes');
            }
        }
        
        
        
        
         function setValues(){           
            if('<%=request.getAttribute("companyId")%>'!='null'){
                document.receivedStock.companyId.value='<%=request.getAttribute("companyId")%>';                
            }
                if('<%=request.getAttribute("orderType")%>'!='null'){
                    document.receivedStock.orderType.value='<%=request.getAttribute("orderType")%>';
                }
                if('<%=request.getAttribute("fromDate")%>'!='null'){
                    document.receivedStock.fromDate.value='<%=request.getAttribute("fromDate")%>';
                }
                if('<%=request.getAttribute("toDate")%>'!='null'){
                    document.receivedStock.toDate.value='<%=request.getAttribute("toDate")%>';    
                }                
                 if('<%=request.getAttribute("vendorId")%>'!='null'){
                document.receivedStock.vendorId.value='<%=request.getAttribute("vendorId")%>';                                
                }
                 if('<%=request.getAttribute("inVoiceId")%>'!='null'){
                document.receivedStock.inVoiceId.value='<%=request.getAttribute("inVoiceId")%>';                                
                }
                 if('<%=request.getAttribute("poId")%>'!='null'){
                document.receivedStock.poId.value='<%=request.getAttribute("poId")%>';                                
                }
            }
    </script>

    <body onload="setValues();">
        <form name="receivedStock" method="post">
            <%@ include file="/content/common/path.jsp" %>           
            <%@ include file="/content/common/message.jsp" %>



<table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:850;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">WO/PO LIST</li>
    </ul>
    <div id="first">
    <table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
    <tr>
        <td height="30"><font color="red">*</font>Location</td>
        <td height="30"><select  class="form-control" name="companyId" style="width:125px">
                            <option value="0">-select-</option>
                            <c:if test = "${LocationLists != null}" > 
                                <c:forEach items="${LocationLists}" var="company">   
                                    <c:choose>
                                        <c:when test="${company.companyTypeId==1012}" >
                                        
                                            <option value="<c:out value="${company.cmpId}"/>"><c:out value="${company.name}"/></option>                                                            
                                        </c:when>
                                    </c:choose>
                                </c:forEach>
                            </c:if>          
                            
                    </select></td>
                    
        <td height="30">Vendor</td>
        <td height="30"><select name="vendorId" class="form-control" style="width:125px">
                            <option value="0">-select-</option>
                            <c:if test = "${VendorLists != null}" >
                                <c:forEach items="${VendorLists}" var="vendor">
                                    <option value="<c:out value="${vendor.vendorId}"/>"><c:out value="${vendor.vendorName}"/></option>
                                </c:forEach>
                            </c:if>
                    </select>
        </td>
        <td height="30">Order No</td>
        <td height="30"><input name="poId" type="text" class="form-control"    height="30"></td>
        <td height="30" rowspan="2" valign="middle"><input type="button" name="" value="Search" class="button" height="30" onclick="submitPage();" ></td>
    </tr>

     <tr>
        <td height="30">Order Type</td>
        <td height="30">
            <select name="orderType" class="form-control" style="width:125px">
                <option value='PO'> Purchase Order </option>
                <option value='WO'> Work Order </option>
            </select>
        </td>
        <td height="30"><font color="red">*</font>From Date</td>
        <td height="30"><input name="fromDate" type="text" class="form-control" value="" size="20"> <span ><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.receivedStock.fromDate,'dd-mm-yyyy',this)"/></span>
        </td>
        <td height="30"><font color="red">*</font>To Date</td>
        <td height="30"><input name="toDate" type="text" class="form-control" value="" size="20"><span ><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.receivedStock.toDate,'dd-mm-yyyy',this)"/></span></td>

    </tr>
    </table>
    </div></div>
    </td>
    </tr>
    </table>
    <br>
                <c:set var="Amount" value="0" />
                 <c:if test = "${ordersList != null}" >
            <table width="650" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" height="30" class="border">
                   <!--DWLayoutTable-->
                   <tr>
                    <td class="contenthead" height="30" colspan="8" align="center"  >Orders List</td>
                </tr>
                <tr>
                    <td  class="contentsub" height="30" >Sno</td>
                    <td  class="contentsub" height="30" >Date</td>
                    <td  class="contentsub" height="30" >PO/WO No</td>                                        
                    <td  class="contentsub" height="30" >Vendor </td>                                                            
                    <td  class="contentsub" height="30" >Approver/Creator</td> 
                    <input type="hidden" name="orderTyp" value='<%=request.getAttribute("orderType")%>' >
                </tr>  
                  <% int index = 0; %> 
                    <c:forEach items="${ordersList}" var="stock"> 
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>	
                        
                <tr>
                    <td class="<%=classText%>" height="30" > <%= index+1 %> </td>
                    <td class="<%=classText%>" height="30" ><c:out value="${stock.createdDate}"/></td>
                    
                
                    <td class="<%=classText%>" height="30" ><input type="hidden"  name="POId1" value="<c:out value='${stock.poId}'/>" > <a href="" onClick="newWindow(<%=index%>);"><c:out value="${stock.poId}"/></a></td>
                
                    <td class="<%=classText%>" height="30" ><c:out value="${stock.vendorName}"/></td>                                     
                    <td class="<%=classText%>" height="30" ><c:out value="${stock.approver}"/></td>                   
                </tr>              
             <% index++;%>
                    </c:forEach>
                   
                </table>
            </c:if>
            <br>

                
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
