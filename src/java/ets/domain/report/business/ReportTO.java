
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.report.business;

import java.util.*;
import java.util.ArrayList;
import ets.arch.util.FPUtil;

public class ReportTO {
    
//    customer role matrix
     private String workNo=null;
     private String pointId=null;
     private String pointType=null;
     private String pointSequence=null;
     private String pointAddress=null;
     private String pointPlanDate="";
     private String pointPlanTime="";
     private String tarrifType=null;
     private String approvalStatus=null;
     private String tripPod=null;
     private String billingTypeName=null;
     private String detailStatusName=null;
     private String masterId=null;
     private String detailId=null;
     private String selectName=null;    
     private String consignmentId=null;
     private String consignmentOrder=null;      
     private String consignorNo=null;
     private String consignerMobile=null;
     private String consignerAddress=null;       
     private String emailDate=null;
     private String customMasterLogid=null;       
     private String customerMasterName=null;
     private String contactPerson=null;
     private String custAddress=null;
     private String custAddress2=null;
     private String custAddress3=null;
     private String custCity=null;
     private String state=null;
     private String pincode=null;
     private String customerMasterPhone=null;
     private String customerMasterMobile=null;
     private String email=null;
     private String ledgerId=null;
     private String ledgerCode=null;
     private String creditDays=null;
     private String customerMasterCreditLimit=null;
     private String secondaryBillingTypeId=null;
     private String customerBillingNameAddress=null;
     private String erpId=null;
     private String custPanNo=null;
     private String customerMasterGstNo=null;
     private String billingTypeId=null;
     private String organizationId=null;
     private String organizationName=null;
     private String companyType=null;
     private String customerMasterActiveInd=null;
     private String customerMasterBillingState=null;
     private String modifiedBy=null;
     private String modifiedOn="";
    
    private String creditNoteId = null;
    private String expenseValue = null;
    private String apiErrorMsg = null;
    private String rolename = null;
    private String menupath = null;
    private String permit = null;
    private String active = null;
    private String ownType = null;

    public String getOwnType() {
        return ownType;
    }

    public void setOwnType(String ownType) {
        this.ownType = ownType;
    }
    
//    customer role matrix

private String grIds[] = null;
private String selectedIds[] = null;
private String pendingGrRemarks[] = null;
private String tripIdss[] = null;

private String gstStatusType = null;
private String ttType = null;
private String sbBillBoeBill = null;
private String movTypeId = null;
private String postStatus = null;
private String gstType = null;
private String commodityId = null;
private String commodityName = null;
private String billOfEntryNo = null;
private String shipingBillNo = null;
private String invIds = null;
private String fileName = null;
private String filePath = null;
private String loadType = null;
private String contractId = null;
private String routePointId = null;
private String contractRateId = null;
private String routeContractId = null;
private String rateWithReefer = "";
private String ratewithoutreefer = "";
private String reportTypeId = null;
private String grId = null;
private String otherExpenseErpId = null;
private String zeroDay = null;
private String oneDay = null;
private String twoDay = null;
private String threeDay = null;
private String fourDay = null;
private String fiveDay = null;
private String sixDay = null;
private String sevenDay = null;
private String moreThanSevenDay = null;
private String nodTotalCounts = null;
private String noOfDays = null;
private String consignmentNo = null;
private String nodstatusId = null;
private String nodTotalCount = null;
private String nodstatusIds = null;
private String totalHours = "";
private String totalTrips = null;
private String marketRate = null;
private String shippingBillNo = null;
private String shippingBillDate = "";
private String gateInDate = "";
private String requestNo = null;
private String updateStatus = null;
private String creditAmount = null;
private String creditNotecode = null;
private String creditNoteDate = "";
private String reason = null;
private String deleteOrder = null;
private String startedTwentyFtContainer = null;
private String startedFourtyFtContainer = null;
private String endedTwentyFtContainer = null;
private String endedFourtyFtContainer = null;
private String noOfStartTrips = null;
private String noOfEndTrips = null;
private String invSubmissionDate = "";
private String podDate = "";
private String gstNo = null;
private String panNo = null;
private String billingState = null;
private String octroiAmount = null;
private String bhati = null;


private String extraFoodingAmount = null;
private String greenTaxAmount = null;
private String expenseId = null;
private String billedPartyname = null;
  private String frieghtAmount = null;
    private String detentionAmount = null;
    private String revenueDetentionAmount = null;
    private String weightMent = null;
    private String revenueWeightMent = null;
    private String revenueOtherExpense = null;
    private String creatorName = null;

      private String id = null;
//    tripDetails


    private String twentyFT = null;
    private String fortyFT = null;
    private String plannedTwentyFT = null;
    private String plannedFortyFT = null;

//    guls
    private String tripExpense = null;
    private String articleName = null;
    private String articleCode = null;
    private String transportType = null;
    private String dieselQty = null;
    private String foodingAmount = null;
    private String detaintionAmount = null;
    private String revenueTollAmount = null;
    private String behatiAmount = null;

//    11/05/16
    private String owndadriTKDemptytwentyft = null;
    private String owndadriTKDemptyfourtyft = null;
    private String owndictemptytwentyft = null;
    private String owndictemptyfourtyft = null;
    private String ownloniemptytwentyft = null;
    private String ownloniemptyfourtyft = null;

    private String leaseddadriTKDemptytwentyft = null;
    private String leaseddadriTKDemptyfourtyft = null;
    private String leaseddictemptytwentyft = null;
    private String leaseddictemptyfourtyft = null;
    private String leasedloniemptytwentyft = null;
    private String leasedloniemptyfourtyft = null;

    private String otherdadriTKDemptytwentyft = null;
    private String otherdadriTKDemptyfourtyft = null;
    private String otherdictemptytwentyft = null;
    private String otherdictemptyfourtyft = null;
    private String otherloniemptytwentyft = null;
    private String otherloniemptyfourtyft = null;

    private String owndadriTKDexporttwentyft = null;
    private String owndadriTKDexportfourtyft = null;
    private String owndictexporttwentyft = null;
    private String owndictexportfourtyft = null;
    private String ownloniexporttwentyft = null;
    private String ownloniexportfourtyft = null;

    private String leaseddadriTKDexporttwentyft = null;
    private String leaseddadriTKDexportfourtyft = null;
    private String leaseddictexporttwentyft = null;
    private String leaseddictexportfourtyft = null;
    private String leasedloniexporttwentyft = null;
    private String leasedloniexportfourtyft = null;

    private String otherdadriTKDexporttwentyft = null;
    private String otherdadriTKDexportfourtyft = null;
    private String otherdictexporttwentyft = null;
    private String otherdictexportfourtyft = null;
    private String otherloniexporttwentyft = null;
    private String otherloniexportfourtyft = null;

    private String owndadriTKDImporttwentyft = null;
    private String owndadriTKDImportfourtyft = null;
    private String owndictImporttwentyft = null;
    private String owndictImportfourtyft = null;
    private String ownloniImporttwentyft = null;
    private String ownloniImportfourtyft = null;

    private String leaseddadriTKDImporttwentyft = null;
    private String leaseddadriTKDImportfourtyft = null;
    private String leaseddictImporttwentyft = null;
    private String leaseddictImportfourtyft = null;
    private String leasedloniImporttwentyft = null;
    private String leasedloniImportfourtyft = null;

    private String otherdadriTKDImporttwentyft = null;
    private String otherdadriTKDImportfourtyft = null;
    private String otherdictImporttwentyft = null;
    private String otherdictImportfourtyft = null;
    private String otherloniImporttwentyft = null;
    private String otherloniImportfourtyft = null;


     public String activeInd = null;
    public String weekDays = null;
    public String executionInterval = null;

    private int aicmSupplierId = 0;
    private String reportDescription = null;
    private String aicmCategoryName = null;
    private String aicmsid = null;

    // Trailer movement
    private String filterentityDisplayName= null;
    private String entityDetailsId= null;
    private String linerNameId= null;
    private String orderTypeId= null;
    private String containerType= null;
    private String revenueWeightmentAmount= null;
    private String revenueOtherAmount= null;
    private String invoiceAmount= null;
    private String weightment= null;
    private String weightmentOld= null;
    private String otherExpenseOld= null;
    private String custCode= null;
    private String customerReferenceId= null;
    private String detenTion= null;
    private String tollErpId= null;
    private String detentionErpId= null;
    private String greenErpId= null;
    private String weightmentErpId= null;
    private String freightAmountErpId= null;
    private String grNumber= null;
    private String container1= null;
    private String container2= null;
    private String gpsSimNo= null;
    private String idleTime= null;
    private String otherExpense= null;
    private String billingPartyIdOld= null; 
    private String billingPartyIdNew= null; 
    private String param= null;
    private String modifiedTime= null;
    private String tripStatusName= null;
    private String shipBillNoOld = null;
    private String shipBillNo = null;
    private String billOfEntryOld = null;
    private String billOfEntry = null;
    private String containerNoOld = null;
    private String detentionChargeOld = null;
    private String detentionCharge = null;
    private String greenTaxOld = null;
    private String greenTax = null;
    private String tollTaxOld = null;
    private String tollTax= null;
    private String dalaAmount= null;
    private String driverBatta= null;


    private String consignmentOrderNo = null;
    private String consignmentOrderId = null;
    private String consignmentDate = "";
    private String challanNo = null;
    private String paidCash = null;
    private String paidDate = null;
    private String monthId = null;
    private String loadedTrip = null;
    private String vehicleStatus = null;
    private String consignorName = null;
    private String transporter = null;
    private String emptyPickup = null;
    private String origin = null;
    private String tripIds = null;
    private String movementType = null;
    private String paidExpense = null;
    private String mobile = null;
    private String plannedStatus = null;
    private String userName = null;
    private String noOfLogins = null;
    private String loginDuration = "";
    private String functionName = null;
    private String activityDateTime = "";

    private String linerName = null;
    private String typeName = null;
    // QueryBuilder Starts Here
    private int qBOuterQuery = 0;
    private String executionQuery = null;
    private String mode = null;
    private String qBfilterFunctionEnd = null;
    private String qBfilterFunction = null;
    private String entityParseValue = null;
    private String qBClosingSyntax = null;
    private String qBAggregateFunction = null;
    private String qBOpenSyntax = null;
    private int queryId = 0;
    private int entityIdqm = 0;
    private int queryMasterEntityId = 0;
//    private int supplierId = 0;
    private int entityColDetailsId = 0;
    private String entityNameList = null;
    private String combinedTableName = null;
    public String conditionTableName = null;
    public String conditionTableColumnName = null;
    private String combinedTableCondition = null;
    private String selectquery = null;
    private List tableList = null;
    private List conditionList = null;
    private String fileextension = null;
    private int schedularOption = 0;
    private String schedularDateTime = "";
    private String reportName = null;
    private String fileExtn = null;
    private String frequency = null;
    private String emailId = null;
    private String scheduleEmailStart = null;
    private String scheduleEmailEnd = null;
    private String emailAttach = null;
    private String queryName = null;
    public String assignedfunc = null;
    public String condition = null;
    public String conditionColumnName = null;
    public String conditionName = null;
    public String operatorValue = null;
    public String userValue = null;
    private int entityId = 0;
    public List SelectionList = null;
    public List editEntityDetails = null;
    public List updatedEntityDetails = null;
    public String displayName = null;
    public String tableName = null;
    public String columnDataTypeName = null;
    public String entityColumnName = null;
    public String entityTableName = null;
    public String entityDataType = null;
    public String entityDisplayName = null;
    private int companyTypeId = 0;
//    private String dataType = null;
    private String columnName = null;
    private String entityName = null;
    private int lastInsertId = 0;
    private String tabStatus = null;
    private String summaryName = null;
    private String summaryValue = null;
//    private String customerCode = null;
    private String tabelsinPriorityVendor = null;
    private String columnname = null;
//    private String startDate = null;
//    private String endDate = null;
    private String auctionRefernceNo = null;
//    private String statusName = null;
    private String summary = null;
    private String statusLink = null;
//    private String statusId = null;
    private String notification = null;
    private int notificationId = 0;
//    private int custId = 0;
//    private int userId = 0;
    private String userColumnValue = null;
    private String tableColumnName = null;
    private String aicmbidDiscPercent = null;
    private String aicmcatInvoiceValue = null;
    private String aicmbidDiscValue = null;
    private String aicmaprPercent = null;
    private String aicmbidType = null;
     private String notUsedGr= null;
    private String usedGr= null;
    private String blockedGr= null;
    private String cancelledGr= null;
    private String totNotUsedGr= null;
    private String totUsedGr= null;
    private String totBlockedGr= null;
    private String totCancelledGr= null;
    private String rateWithReeferNew= "";
    private String rateWithReeferOld= "";
    private String rateWithoutReeferNew= "";
    private String rateWithoutReeferOld= "";
    private String validStatusNew= null;
    private String validStatusOld= null;
    private String loadTypeName= null;
    private String containerQty= null;
    private String expesneDate = "";
    private String grNo = null;
    private String billingParty = null;
    private String grDate = "";
    private String grTime ="";
    private String dieselCost = null;
    private String voucherNo = null;
    private String foodCost = null;
    private String tripExpenseSNo = null;
    private String expenseDate = "";
    private String slipNo = null;
    private String dashboardopstrukCount = null;
    private String trailerId = null;
    private String vehicleMake = null;
    private String count = null;
    private String totalVehicle = null;
    private String trailerRevenue=null;
    private String trailerExpence=null;
    private String revenueProfitPercentage = null;
    private String expenceProfitPercentage = null;
    private String commodity = null;
    private String blNumber = null;
    private String containerNo = null;
    private String containerSize = null;
    private String weight = null;
    private String trailerNo = null;
    private String currentLocation = null;
    private String border1 = null;
    private String border1ArrivalDate = "";
    private String border2 = null;
    private String border2ArrivalDate = "";
    private String clearingAgent = null;
    private String unLoadingDateTime = "";
    private String progress = null;
    // Brattle Foods starts 
    //Arul starts

//    trpiDetails
    private String type = null;
    private String containerAmount3 = null;
    private String referAmount3 = null;
    private String chasisAmount3 = null;
    private String containerAmount1 = null;
    private String referAmount1 = null;
    private String chasisAmount1 = null;
    private String containerAmount2 = null;
    private String referAmount2 = null;
    private String chasisAmount2 = null;
    private String containerAmount = null;
    private String referAmount = null;
    private String chasisAmount = null;
    private String rnmAdvance = null;
    private String foodingAdvance = null;
    private String repairAdvance = null;
    private String accountMgrId = null;
    private String wfuAmount = null;
    private String emptyTripKM = null;
    private String totalCost = null;
    private String tripendtime = "";
    private String customerType = null;
    private String tripEndTime = "";
    private String requestAmount;
    private String paidAmount;
    private String wflHours;
    private String wfuHours;
    private String transitHours;
    private String loadingTransitHours;
    private String endDateTime;
    private String originReportingDateTime;
    private String destinationReportingDateTime;
    private String totalRunKm;
    private String totalRunHm;
    private String rcm;
    private String actualExpense;
    private String startDateTime ="";
    private String primaryDriver =null;
    private String secondaryDriver =null;
    private String advanceRemarks=null;
    private String advancePaidDate ="";
    private String expenseType =null;
    private String expenseName =null;
    private String expenseRemarks =null;
    private String expenseDriverName =null;
    private String orderSequence =null;
    private String emptyExpense =null;
    private String loadedExpense =null;
    private String earnings =null;
    private String tripExtraExpense =null;
    private String tripMergingId =null;
    private String vehicletype =null;
    private String kmReading ="";
    private String kmReadingDate = "";
    private String hmReading = "";
    private String hmReadingDate = "";
    private String closeDate = "";
    private String settledDate = "";
    private String mergingDate = "";
    private String startDate = "";
    private String endDate = "";
    private String issueType = null;
    private String jobCardDays = null;
    private String plannedCompleteDays = null;
    private String outStandingDays = null;
    private String jobCardNo = null;
    private int amountSpend = 0;
    private int tripCount = 0;
    private int percent = 0;
    private String tripRunKm = null;
    private String tripRunHm = null;
    private String tripDays = null;
    private String unLoadingDate = "";
    private String loadingDateTime = "";
    private String not = null;
    private String tripStatusIdTo = null;
    private String startDateFrom = "";
    private String startDateTo = "";
    private String endDateFrom = "";
    private String endDateTo = "";
    private String closedDateFrom = "";
    private String closedDateTo = "";
    private String settledDateFrom = "";
    private String settledDateTo = "";
    private String queryType = null;
    private String serviceTypeName = null;
    private String estimatedRevenue = null;
    private String tripstarttime = "";
    private String location = null;
    private String vehiclewfudate = "";
    private String vehiclewfutime = "";
    private String statusName = null;
    private String status = null;
    private String tonnage = null;
    private String vehicleNo = null;
    private String cityName = null;
    private String zoneName = null;
    private String operationPoint = null;
    private String fleetCenterHead = null;
    private String oldVehicleNo = null;
    private String oldDriverName = null;
    private String businessType = null;
    private String customerOrderReferenceNo = null;
    private String cNotes = null;
    private String routeInfo = null;
    private String fleetCenterName = null;
    private String expectedArrivalDateTime = "";
    private String orderRevenue = null;
    private String orderExpense = null;
    private String actualAdvancePaid = null;
    private String vehicleTonnage = null;
    private String gpsLocation = null;
    private String plannedStartDateTime = "";
    private String actualStartDateTime = "";
    private String plannedEndDateTime = "";
    private String actualEndDateTime = "";
    private String wfuDateTime = "";
    private String wfuRemarks = null;
    private String wfuCreatedOn = "";
    private String endedBy = null;
    private String closedBy = null;
    private String settledBy = null;
    private String fleetCenterNo = null;
    private String mobileNo = null;
    private String extraExpenseValue = null;
//    tripDetails  
    
    private int userId = 0;
    private String cityFromId = null;
    private String zoneId = null;
    private String podStatus = null;
    private String requestedAdvance = null;
    private String paidAdvance = null;
    private String rmDays = "";
    private String days = "";
    private String months = "";
    private String gpsEndAttempt = null;
    private String gpsEndErrorCode = null;
    private String gpsEndErrorMsg = null;
    private String gpsSystem = null;
    private String tripStartDateTime = "";
    private String tripEndDateTime = "";
    private String tsStartInd = "";
    private String gpsStartInd = "";
    private String gpsStartUpdateTime = "";
    private String tsEndInd = "";
    private String gpsEndInd = "";
    private String gpsEndUpdateTime = "";
    private String distanceTravelled = null;
    private String reeferRunHours = "";
    private String reeferAvgTemp = null;
    private String gpsStartAttempt = null;
    private String gpsStartErrorCode = null;
    private String gpsStartErrorMsg = null;
    private String gpsTripDetailsAttempt = null;
    private String gpsTripDetailsErrorCode = null;
    private String gpsTripDetailsErrorMsg = null;
    private String gpsTripDetailsInd = "";
    private String gpsTripDetailsDatetime = "";
    private String transactionHistoryId = null;
    private String bPCLAccountId = null;
    private String dealerName = null;
    private String dealerCity = null;
    private String transactionDate = null;
    private String accountingDate = null;
    private String transactionType = null;
    private String currency = null;
    private String transactionAmount = null;
    private String volumeDocumentNo = null;
    private String amountBalance = null;
    private String petromilesEarned = null;
    private String odometerReading = null;
    private String fuelPrice = null, runHour = null, dieselUsed = null, rcmAllocation = null, bpclAllocation = null, extraExpense = null, totalMiscellaneous = null, bhatta = null, startingBalance = null, endingBalance = null, payMode = null;
    private String currentTemperature = null;
    private String logDate = null;
    private String logTime = null;
    private String billableKM = null;
    private String daysWithOverDue = null;
    private String daysToOverDue = null;
    private String overDueOn = null;
    private String consignmentNote = null;
    private double totalAmountReceivable = 0;
    private double creditLimit = 0;
    private String customerCode = null;
    private double overDueGrandTotal = 0;
    private double profitValue = 0;
    private double profitPercent = 0;
    private String operationTypeId = null;
    private String profitType = null;
    private String approvalstatus = null;
    private String consignmentNoteNo = null;
    String invoiceType = null;
    private String utilisedDays = "";
    private String tripStatusId = null;
    private String fleetCenterId = null;
    private String vehicleCapUtil = null;
    private double vehicleDriverSalary = 0;
    private double tripOtherExpense = 0;
    private double miscAmount = 0;
    private String totalWeight = null;
    private String tripNos = null;
    private String date = null;
    private double insuranceAmount = 0;
    private double fcAmount = 0;
    private double roadTaxAmount = 0;
    private double permitAmount = 0;
    private double emiAmount = 0;
    private double tollAmount = 0;
    private double fuelAmount = 0;
    private double driverIncentive = 0;
    private double driverBata = 0;
    private double driverExpense = 0;
    private double routeExpenses = 0;
    private double maintainExpense = 0;
    private double netExpense = 0;
    private double netProfit = 0;
    private int timePeriod = 0;
    private double fixedExpensePerDay = 0;
    private double totlalFixedExpense = 0;
    private double totlalOperationExpense = 0;
    private String reeferRequired = "";
    private String invoiceCode = null;
    private String invRefCode = null;
    private String invoiceDate = "";
    private String billingType = null;
    private String ownerShip = null;
    private String freightAmount = null;
    private String otherExpenseAmount = null;
    private String grandTotal = null;
    private String invoiceStatus = null;
    private String numberOfTrip = null;
    private String fromDate = "";
    private String toDate = "";
    private String remarks = null;
    private String customerName = null;
    private String CNAmount = null;
    private String DNAmount = null;
    //Arul Ends
    //Arul
    // Brattle Foods  end
    // Brattle Foods starts
    //Arul starts
    //Arul Ends
    //Arul
    // Brattle Foods  end
    private String jobCardId = null;
    private String dueIn = null;
    private String bayNo = null;
    private String groupName = null;
    ArrayList serviceTypeListAll = null;

    public ArrayList getServiceTypeListAll() {
        return serviceTypeListAll;
    }

    public void setServiceTypeListAll(ArrayList serviceTypeListAll) {
        this.serviceTypeListAll = serviceTypeListAll;
    }
    private String invoiceCustomer = null;
    private String lastProblem = null;
    private String lastTech = null;
    private String lastStatus = null;
    private String lastKm = null;
    private String lastRemarks = null;
    private String regNo = null;
    private String billNo = null;
    private String totalAmount = null;
    private String laborAmount = null;
    private String spareAmount = null;
    private String woAmount = null;
    private String discount = null;
    private String billDate = "";
    private String companyName = null;
    private String problemName = null;
    private String empName = null;
    private String user = null;
    private String companyId = null;
    private String serviceName = null;
    private String serviceTypeId = null;
    private String problem = null;
    private String problemStatus = null;
    private String createdDate = "";
    private String rcCode = null;
    private String quantity = null;
    private String phone = null;
    private String rcWoId = null;
    private String categoryName = null;
    private String pcd = "";
    private String acd = "";
    private String technician = null;
    private String technicianId = null;

    private String estHrs= "";
    private String actHrs = "";
    private String sectionId = null;
    private String section = null;
    private String activity = null;
    private String vendorId = null;
    private String period = null;
    private String mfrName = null;
    private String modelName = null;
    private String purchaseAmt = null;
    private String paplCode = null;
    private String itemName = null;
    private String vendorName = null;
    private String poId = null;
    private String aqty = null;
    private String purDate = "";
    private String mfrCode = null;
    private String mfrId = null;
    private String modelId = null;
    private String newQty = null;
    private String rcQty = null;
    private String totalQty = null;
    private String stockValue = null;
    private String processId = null;
    private String newPrice = null;
    private String unitPrice = null;
    private String unusedQty = null;
    private String purType = null;
    private String billAmount = null;
    private String netAmount = null;
    private String scheduledDate = "";
    private String itemAmount = null;
    private String address = null;
    private String itemPrice = null;
    private String spares = null;
    private String labour = null;
    private String tax = null;
    private String contractAmnt = null;
    private String serviceTaxAmnt = null;
    private String itemQty = null;
    private String itemId = null;
    private String activityAmount = null;
    private String woId = null;
    private String nextFc = null;
    private String due = null;
    private String uomName = null;
    private String categoryId = null;
    private String mrsId = null;
    private String day = null;
    private String supplyId = null;
    private String invoiceId = "";
    private String rqty = null;
    private String issueQty = null;
    private String retQty = null;
    private String issueDate = "";
    private String raisedProp = null;
    private String completedProp = null;
    private String completedDate = "";
    private String custName = null;
    private String usageType = null;
    private String totalIssued = null;
    private String schedule = "";
    private String diff = "";
    private String lastDate = "";
    private String arrival = "";
    private String buyDate = "";
    private String buyPrice = null;
    private String sellPrice = null;
    private String profit = null;
    private String nettProfit = null;
    private String age = null;
    private String vehicleId = null;
    private String mrp = null;
    private String km = "";
    private String description = null;
    private String month = "";
    private String newTyre = null;
    private String reTreadTyre = null;
    private String rcId = null;
    private String rcTime = "";
    private String inTime = "";
    private String tyreNo = null;
    private String posName = null;
    private String hikedAmount = null;
    private String margin = null;
    private String payableTax = null;
    static FPUtil fpUtil = FPUtil.getInstance();
    private float hikePercentage = Float.parseFloat(fpUtil.getInstance().getProperty("HIKE_PERCENTAGE"));
    private String sparesAmount = null;
    private String sparesWithTax = null;
    private String freight = null;
    private String approver = null;
    private String orderType = "PO";
    private String[] addressSplit = null;
    private String[] remarksSplit = null;
    private String billType = null;
    private String dcNo = null;
    private String purpose = null;
    private String custId = null;
    private String totJc = null;
    private String itemType = null;
    private String vehCount = null;
    private String year = "";

    private String month1 = "";
    private String month2 = "";
    private String month3 = "";
    private String month1ReTread = "";
    private String month2ReTread = "";
    private String month3ReTread = "";
    private String month1New = "";
    private String month2New = "";
    private String month3New = "";

    private String year1 = "";
    private String year2 = "";
    private String year3 = "";


    private String servicePointId = null;
    private String monthName = "";
    private float amount = 0.0f;
    private String usageTypeIds = null;
    private String usageTypeName = null;
    private String vehicleTypeId = null;
    private String vehicleTypeName = null;
    private String colour = null;
    private String name = null;
    private String district = null;
    String mfr = null;
    String usage = null;
    String vehicleType = null;
    private String completed = "";
    private String notPlanned = "";
    private String billingCompleted = "";
    private String planned = "";
    //shankar
    String itemCode = null;
    int usageTypeId = 0;
    String itemTypes = null;
    String stworth = null;
    
    String invoiceNo = null;
    ArrayList vatValues = null;
    float taxAmount = 0.0f;
    //Hari
    private int reportType = 0;
    private String reportAmount = null;
    private String rcSend = null;
    private String rcReceive = null;
    private int mfrid = 0;
    private int usageid = 0;
    private int typeId = 0;
    private int life = 0;
    private int itemCount = 0;
    private Float price = 0.0f;
    private String manualMrsNo = null;
    private String manualMrsDate = "";
    private int movingAverage = 0;
    private String firstHalfDays = "";
    private String lastHalfDays = "";
    private String spltQty = null;
    private String monthAverage = null;
    private String materialCostInternal = null;
    private String materialCostExternal = null;
    private String laborCharge = null;
    private int rcWorkorderId = 0;
    private int counterId = 0;
//  bala
    private int noOfVehicles = 0;
    private String customerTypeId = "0";
    private String requiredDate = null;
    private int xmlId = 0;
    private int dataType = 0;
    private int noOfCards = 0;
    private String empId = null;
    private String driName = null;
    private String driId = null;
    private String registerNo = null;
    private String settlementId = null;
    private String driverName = null;
    private String cleanerName = null;
    private String totalDays = null;
    private String endKm = null;
    private String startKM = null;
    private String runningKm = null;
    private String runKm = null;
    private String runHm = null;
    private String noOfTrips = null;
    private String totalTonnageAmount = null;
    private String shortage = null;
    private String totalDiesel = null;
    private String actualKM = null;
    private String dieselAmount = null;
    private String income = null;
    private String driverExpenses = null;
    private String generalExpenses = null;
    private String driverSalary = null;
    private String balance = null;
    private String cleanerSalary = null;
    private String totalExpenses = null;
    private String driverAdvance = null;
    private String shortage1 = null;
    private String driverCleanerExpenses = null;
    private String perDaysIncome = null;
    private String duefromDriver = null;
    private String lossOrDay = null;
    private String driverBataSalary = null;
    private String cleanerBataSalary = null;
    private String totalSalary = null;
    private String cBalance = null;
    private String dDueFromDriver = null;
    private String busExpenses = null;
    private String actualSalary = null;
    private String dBalance = null;
    private String noOfTrips5k = null;
    private String kmpl = null;
    private String speedOMeterKm = null;
    private String sKmpl = null;
    private String aboveBelowKM = null;
    private String cDate = "";
    private String cVNo = null;
    private String cRs = null;
    private String dDate = "";
    private String dVNo = null;
    private String dRs = null;
    private String cFRs = null;
    private String pdfName = null;
    //Rathimeena Driver Settlement Start
    private String tripSheetId = null;
    private String tripCode = null;
    private String tripRouteId = null;
    private String tripVehicleId = null;
    private String tripScheduleId = null;
    private String tripDriverId = null;
    private String tripDate = "";
    private String tripDepartureDate = "";
    private String tripArrivalDate = "";
    private String tripKmsOut = null;
    private String tripKmsIn = null;
    private String tripTotalLitres = null;
    private String tripFuelAmount = null;
    private String tripTotalAllowances = null;
    private String tripTotalExpenses = null;
    private String tripTotalKms = null;
    private String tripBalanceAmount = null;
    private String tripStatus = null;
    private String tripId = null;
    private String identityNo = null;
    private String deviceId = null;
    private String routeName = null;
    private String outKM = null;
    private String outDateTime = "";
    private String inKM = null;
    private String inDateTime = "";
    private String settlementFlag = null;
    private String issuerName = null;
    private String designation = null;
    private String advDatetime = "";
    private String fuelName = null;
    private String bunkName = null;
    private String liters = null;
    private String fuelDatetime = "";
    private String inOutIndication = "";
    private String inOutDateTime = "";
    private String locationName = null;
    private String totalTonnage = null;
    private String deliveredTonnage = null;
    private String embarkDate = "";
    private String alightDate = "";
    private String alightStatus = null;
    private String createdOn = "";
    private String expensesDesc = null;
    private String compId = null;
    private String regno = null;
    private String driverDue = null;
    private String statusDate = "";
    private String statusId = null;
    private String tripType = null;
    private String totalTonAmount = null;
    private String tripStartDate = "";
    private String tripEndDate = "";
    private String vehicleInOut = null;
    private String knownLocation = null;
    private String currentStatus = null;
    private String tollExpense = null;
    private String dieselExpense = null;
    private String totalExpense = null;
    private String locationId = null;
    private String permitType = null;
    private String amcCompanyName = null;
    private String amcAmount = null;
    private String amcDuration = null;
    private String createdBy = null;
    private int driverId = 0;
    //Rathimeena Driver Settlement End
    //ASHOK Report start
    private String lpsId = null;
    private String trippedLPS = null;
    private String lpsCount = null;
    private String routeId = null;
    private String customerId = null;
    private String pinkSlipID = null;
    private String orderNo = null;
    private String bags = null;
    private String vehicleid = null;
    private String totalallowance = null;
    private String totalliters = null;
    private String totalamount = null;
    private String totalexpenses = null;
    private String balanceamount = null;
    private String totalkms = null;
    private String totalHms = "";
    private String revenue = null;
    private String vendorSettlementFlag = null;
    private String city = null;
    private String pandL = null;
    private String expenses = null;
    private String estimatedExpenses = null;
    private String customertypeId = null;
    private String customertypeName = null;
    private String ownership = null;
    private String billStatus = null;
    private String destination = null;
    private String consigneeName = null;
    private String productName = null;
    private String gpno = null;
    private String distance = null;
    private String totalFreightAmount = null;
    private String consignmentType = null;
    private String settlementAmount = null;
    private String crossing = null;
    private String consignmentName = null;
    private String settlementDate = "";



    private String trips = null;
    private String sales = null;
    private String wfu = null;
    private String wfl = null;

    private String operationPointId = null;
    private String vehicles = null;

     private String segmentName = "";
     private String qrcode = "";
     private String irn = "";
     private String cityNme[] = null;

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }

    public String getIrn() {
        return irn;
    }

    public void setIrn(String irn) {
        this.irn = irn;
    }

    public String getSegmentName() {
        return segmentName;
    }

    public void setSegmentName(String segmentName) {
        this.segmentName = segmentName;
    }



     
    //ASHOK Report end
    public String getAboveBelowKM() {
        return aboveBelowKM;
    }

    public void setAboveBelowKM(String aboveBelowKM) {
        this.aboveBelowKM = aboveBelowKM;
    }

    public String getActualKM() {
        return actualKM;
    }

    public void setActualKM(String actualKM) {
        this.actualKM = actualKM;
    }

    public String getActualSalary() {
        return actualSalary;
    }

    public void setActualSalary(String actualSalary) {
        this.actualSalary = actualSalary;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getBusExpenses() {
        return busExpenses;
    }

    public void setBusExpenses(String busExpenses) {
        this.busExpenses = busExpenses;
    }

    public String getcBalance() {
        return cBalance;
    }

    public void setcBalance(String cBalance) {
        this.cBalance = cBalance;
    }

    public String getcDate() {
        return cDate;
    }

    public void setcDate(String cDate) {
        this.cDate = cDate;
    }

    public String getcFRs() {
        return cFRs;
    }

    public void setcFRs(String cFRs) {
        this.cFRs = cFRs;
    }

    public String getcRs() {
        return cRs;
    }

    public void setcRs(String cRs) {
        this.cRs = cRs;
    }

    public String getcVNo() {
        return cVNo;
    }

    public void setcVNo(String cVNo) {
        this.cVNo = cVNo;
    }

    public String getCleanerBataSalary() {
        return cleanerBataSalary;
    }

    public void setCleanerBataSalary(String cleanerBataSalary) {
        this.cleanerBataSalary = cleanerBataSalary;
    }

    public String getCleanerName() {
        return cleanerName;
    }

    public void setCleanerName(String cleanerName) {
        this.cleanerName = cleanerName;
    }

    public String getCleanerSalary() {
        return cleanerSalary;
    }

    public void setCleanerSalary(String cleanerSalary) {
        this.cleanerSalary = cleanerSalary;
    }

    public String getdBalance() {
        return dBalance;
    }

    public void setdBalance(String dBalance) {
        this.dBalance = dBalance;
    }

    public String getdDate() {
        return dDate;
    }

    public void setdDate(String dDate) {
        this.dDate = dDate;
    }

    public String getdDueFromDriver() {
        return dDueFromDriver;
    }

    public void setdDueFromDriver(String dDueFromDriver) {
        this.dDueFromDriver = dDueFromDriver;
    }

    public String getdRs() {
        return dRs;
    }

    public void setdRs(String dRs) {
        this.dRs = dRs;
    }

    public String getdVNo() {
        return dVNo;
    }

    public void setdVNo(String dVNo) {
        this.dVNo = dVNo;
    }

    public String getDieselAmount() {
        return dieselAmount;
    }

    public void setDieselAmount(String dieselAmount) {
        this.dieselAmount = dieselAmount;
    }

    public String getDriName() {
        return driName;
    }

    public void setDriName(String driName) {
        this.driName = driName;
    }

    public String getDriverAdvance() {
        return driverAdvance;
    }

    public void setDriverAdvance(String driverAdvance) {
        this.driverAdvance = driverAdvance;
    }

    public String getDriverBataSalary() {
        return driverBataSalary;
    }

    public void setDriverBataSalary(String driverBataSalary) {
        this.driverBataSalary = driverBataSalary;
    }

    public String getDriverCleanerExpenses() {
        return driverCleanerExpenses;
    }

    public void setDriverCleanerExpenses(String driverCleanerExpenses) {
        this.driverCleanerExpenses = driverCleanerExpenses;
    }

    public String getDriverExpenses() {
        return driverExpenses;
    }

    public void setDriverExpenses(String driverExpenses) {
        this.driverExpenses = driverExpenses;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverSalary() {
        return driverSalary;
    }

    public void setDriverSalary(String driverSalary) {
        this.driverSalary = driverSalary;
    }

    public String getDuefromDriver() {
        return duefromDriver;
    }

    public void setDuefromDriver(String duefromDriver) {
        this.duefromDriver = duefromDriver;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getEndKm() {
        return endKm;
    }

    public void setEndKm(String endKm) {
        this.endKm = endKm;
    }

    public String getGeneralExpenses() {
        return generalExpenses;
    }

    public void setGeneralExpenses(String generalExpenses) {
        this.generalExpenses = generalExpenses;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getKmpl() {
        return kmpl;
    }

    public void setKmpl(String kmpl) {
        this.kmpl = kmpl;
    }

    public String getLossOrDay() {
        return lossOrDay;
    }

    public void setLossOrDay(String lossOrDay) {
        this.lossOrDay = lossOrDay;
    }

    public String getNoOfTrips() {
        return noOfTrips;
    }

    public void setNoOfTrips(String noOfTrips) {
        this.noOfTrips = noOfTrips;
    }

    public String getNoOfTrips5k() {
        return noOfTrips5k;
    }

    public void setNoOfTrips5k(String noOfTrips5k) {
        this.noOfTrips5k = noOfTrips5k;
    }

    public String getPerDaysIncome() {
        return perDaysIncome;
    }

    public void setPerDaysIncome(String perDaysIncome) {
        this.perDaysIncome = perDaysIncome;
    }

    public String getRegisterNo() {
        return registerNo;
    }

    public void setRegisterNo(String registerNo) {
        this.registerNo = registerNo;
    }

    public String getRunningKm() {
        return runningKm;
    }

    public void setRunningKm(String runningKm) {
        this.runningKm = runningKm;
    }

    public String getsKmpl() {
        return sKmpl;
    }

    public void setsKmpl(String sKmpl) {
        this.sKmpl = sKmpl;
    }

    public String getSettlementId() {
        return settlementId;
    }

    public void setSettlementId(String settlementId) {
        this.settlementId = settlementId;
    }

    public String getShortage() {
        return shortage;
    }

    public void setShortage(String shortage) {
        this.shortage = shortage;
    }

    public String getShortage1() {
        return shortage1;
    }

    public void setShortage1(String shortage1) {
        this.shortage1 = shortage1;
    }

    public String getSpeedOMeterKm() {
        return speedOMeterKm;
    }

    public void setSpeedOMeterKm(String speedOMeterKm) {
        this.speedOMeterKm = speedOMeterKm;
    }

    public String getStartKM() {
        return startKM;
    }

    public void setStartKM(String startKM) {
        this.startKM = startKM;
    }

    public String getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(String totalDays) {
        this.totalDays = totalDays;
    }

    public String getTotalDiesel() {
        return totalDiesel;
    }

    public void setTotalDiesel(String totalDiesel) {
        this.totalDiesel = totalDiesel;
    }

    public String getTotalExpenses() {
        return totalExpenses;
    }

    public void setTotalExpenses(String totalExpenses) {
        this.totalExpenses = totalExpenses;
    }

    public String getTotalSalary() {
        return totalSalary;
    }

    public void setTotalSalary(String totalSalary) {
        this.totalSalary = totalSalary;
    }

    public String getTotalTonnageAmount() {
        return totalTonnageAmount;
    }

    public void setTotalTonnageAmount(String totalTonnageAmount) {
        this.totalTonnageAmount = totalTonnageAmount;
    }

    public int getNoOfCards() {
        return noOfCards;
    }

    public void setNoOfCards(int noOfCards) {
        this.noOfCards = noOfCards;
    }

    public int getDataType() {
        return dataType;
    }

    public void setDataType(int dataType) {
        this.dataType = dataType;
    }

    public int getXmlId() {
        return xmlId;
    }

    public void setXmlId(int xmlId) {
        this.xmlId = xmlId;
    }

    public String getRequiredDate() {
        return requiredDate;
    }

    public void setRequiredDate(String requiredDate) {
        this.requiredDate = requiredDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(String customerTypeId) {
        this.customerTypeId = customerTypeId;
    }
//  bala ends

    public int getNoOfVehicles() {
        return noOfVehicles;
    }

    public void setNoOfVehicles(int noOfVehicles) {
        this.noOfVehicles = noOfVehicles;
    }

    public int getCounterId() {
        return counterId;
    }

    public void setCounterId(int counterId) {
        this.counterId = counterId;
    }

    public int getRcWorkorderId() {
        return rcWorkorderId;
    }

    public void setRcWorkorderId(int rcWorkorderId) {
        this.rcWorkorderId = rcWorkorderId;
    }

    public String getLaborCharge() {
        return laborCharge;
    }

    public void setLaborCharge(String laborCharge) {
        this.laborCharge = laborCharge;
    }

    public String getMaterialCostExternal() {
        return materialCostExternal;
    }

    public void setMaterialCostExternal(String materialCostExternal) {
        this.materialCostExternal = materialCostExternal;
    }

    public String getMaterialCostInternal() {
        return materialCostInternal;
    }

    public void setMaterialCostInternal(String materialCostInternal) {
        this.materialCostInternal = materialCostInternal;
    }

    public void setSpltQty(String spltQty) {
        this.spltQty = spltQty;
    }

    public String getSpltQty() {
        return spltQty;
    }

    public int getMovingAverage() {
        return movingAverage;
    }

    public String getFirstHalfDays() {
        return firstHalfDays;
    }

    public void setFirstHalfDays(String firstHalfDays) {
        this.firstHalfDays = firstHalfDays;
    }

    public String getLastHalfDays() {
        return lastHalfDays;
    }

    public void setLastHalfDays(String lastHalfDays) {
        this.lastHalfDays = lastHalfDays;
    }

    public String getMonthAverage() {
        return monthAverage;
    }

    public void setMonthAverage(String monthAverage) {
        this.monthAverage = monthAverage;
    }

    public void setMovingAverage(int movingAverage) {
        this.movingAverage = movingAverage;
    }

    public String getManualMrsDate() {
        return manualMrsDate;
    }

    public void setManualMrsDate(String manualMrsDate) {
        this.manualMrsDate = manualMrsDate;
    }

    public String getManualMrsNo() {
        return manualMrsNo;
    }

    public void setManualMrsNo(String manualMrsNo) {
        this.manualMrsNo = manualMrsNo;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public String getRcReceive() {
        return rcReceive;
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    public int getMfrid() {
        return mfrid;
    }

    public void setMfrid(int mfrid) {
        this.mfrid = mfrid;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public int getUsageid() {
        return usageid;
    }

    public void setUsageid(int usageid) {
        this.usageid = usageid;
    }

    public void setRcReceive(String rcReceive) {
        this.rcReceive = rcReceive;
    }

    public String getRcSend() {
        return rcSend;
    }

    public void setRcSend(String rcSend) {
        this.rcSend = rcSend;
    }

    public String getReportAmount() {
        return reportAmount;
    }

    public void setReportAmount(String reportAmount) {
        this.reportAmount = reportAmount;
    }

    public int getReportType() {
        return reportType;
    }

    public void setReportType(int reportType) {
        this.reportType = reportType;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getMfr() {
        return mfr;
    }

    public void setMfr(String mfr) {
        this.mfr = mfr;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getUsageTypeIds() {
        return usageTypeIds;
    }

    public void setUsageTypeIds(String usageTypeIds) {
        this.usageTypeIds = usageTypeIds;
    }

    public String getUsageTypeName() {
        return usageTypeName;
    }

    public void setUsageTypeName(String usageTypeName) {
        this.usageTypeName = usageTypeName;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    // end shankar
    public int getUsageTypeId() {
        return usageTypeId;
    }

    public void setUsageTypeId(int usageTypeId) {
        this.usageTypeId = usageTypeId;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getPosName() {
        return posName;
    }

    public void setPosName(String posName) {
        this.posName = posName;
    }

    public String getUomName() {
        return uomName;
    }

    public void setUomName(String uomName) {
        this.uomName = uomName;
    }

    public String getTyreNo() {
        return tyreNo;
    }

    public void setTyreNo(String tyreNo) {
        this.tyreNo = tyreNo;
    }

    public String getRcTime() {
        return rcTime;
    }

    public void setRcTime(String rcTime) {
        this.rcTime = rcTime;
    }

    public String getRcId() {
        return rcId;
    }

    public void setRcId(String rcId) {
        this.rcId = rcId;
    }

    public String getTechnicianId() {
        return technicianId;
    }

    public void setTechnicianId(String technicianId) {
        this.technicianId = technicianId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKm() {
        return km;
    }

    public void setKm(String km) {
        this.km = km;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getBuyDate() {
        return buyDate;
    }

    public void setBuyDate(String buyDate) {
        this.buyDate = buyDate;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public String getDiff() {
        return diff;
    }

    public void setDiff(String diff) {
        this.diff = diff;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getTotalIssued() {
        return totalIssued;
    }

    public void setTotalIssued(String totalIssued) {
        this.totalIssued = totalIssued;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDue() {
        return due;
    }

    public void setDue(String due) {
        this.due = due;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getIssueQty() {
        return issueQty;
    }

    public void setIssueQty(String issueQty) {
        this.issueQty = issueQty;
    }

    public String getMrsId() {
        return mrsId;
    }

    public void setMrsId(String mrsId) {
        this.mrsId = mrsId;
    }

    public String getNextFc() {
        return nextFc;
    }

    public void setNextFc(String nextFc) {
        this.nextFc = nextFc;
    }

    public String getRqty() {
        return rqty;
    }

    public void setRqty(String rqty) {
        this.rqty = rqty;
    }

    public String getSupplyId() {
        return supplyId;
    }

    public void setSupplyId(String supplyId) {
        this.supplyId = supplyId;
    }

    public String getWoId() {
        return woId;
    }

    public void setWoId(String woId) {
        this.woId = woId;
    }

    public String getActivityAmount() {
        return activityAmount;
    }

    public void setActivityAmount(String activityAmount) {
        this.activityAmount = activityAmount;
    }

    public String getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(String billAmount) {
        this.billAmount = billAmount;
    }

    public String getItemAmount() {
        return itemAmount;
    }

    public void setItemAmount(String itemAmount) {
        this.itemAmount = itemAmount;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(String itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemQty() {
        return itemQty;
    }

    public void setItemQty(String itemQty) {
        this.itemQty = itemQty;
    }

    public String getLabour() {
        return labour;
    }

    public void setLabour(String labour) {
        this.labour = labour;
    }

    public String getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(String netAmount) {
        this.netAmount = netAmount;
    }

    public String getSpares() {
        return spares;
    }

    public void setSpares(String spares) {
        this.spares = spares;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getJobCardId() {
        return jobCardId;
    }

    public void setJobCardId(String jobCardId) {
        this.jobCardId = jobCardId;
    }

    public String getLaborAmount() {
        return laborAmount;
    }

    public void setLaborAmount(String laborAmount) {
        this.laborAmount = laborAmount;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getSpareAmount() {
        return spareAmount;
    }

    public void setSpareAmount(String spareAmount) {
        this.spareAmount = spareAmount;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getWoAmount() {
        return woAmount;
    }

    public void setWoAmount(String woAmount) {
        this.woAmount = woAmount;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(String serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public String getAcd() {
        return acd;
    }

    public void setAcd(String acd) {
        this.acd = acd;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getAqty() {
        return aqty;
    }

    public void setAqty(String aqty) {
        this.aqty = aqty;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getMfrCode() {
        return mfrCode;
    }

    public void setMfrCode(String mfrCode) {
        this.mfrCode = mfrCode;
    }

    public String getMfrId() {
        return mfrId;
    }

    public void setMfrId(String mfrId) {
        this.mfrId = mfrId;
    }

    public String getMfrName() {
        return mfrName;
    }

    public void setMfrName(String mfrName) {
        this.mfrName = mfrName;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getNewPrice() {
        return newPrice;
    }

    public void setNewPrice(String newPrice) {
        this.newPrice = newPrice;
    }

    public String getNewQty() {
        return newQty;
    }

    public void setNewQty(String newQty) {
        this.newQty = newQty;
    }

    public String getPaplCode() {
        return paplCode;
    }

    public void setPaplCode(String paplCode) {
        this.paplCode = paplCode;
    }

    public String getPcd() {
        return pcd;
    }

    public void setPcd(String pcd) {
        this.pcd = pcd;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getPoId() {
        return poId;
    }

    public void setPoId(String poId) {
        this.poId = poId;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public String getProblemStatus() {
        return problemStatus;
    }

    public void setProblemStatus(String problemStatus) {
        this.problemStatus = problemStatus;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getPurDate() {
        return purDate;
    }

    public void setPurDate(String purDate) {
        this.purDate = purDate;
    }

    public String getPurType() {
        return purType;
    }

    public void setPurType(String purType) {
        this.purType = purType;
    }

    public String getPurchaseAmt() {
        return purchaseAmt;
    }

    public void setPurchaseAmt(String purchaseAmt) {
        this.purchaseAmt = purchaseAmt;
    }

    public String getRcQty() {
        return rcQty;
    }

    public void setRcQty(String rcQty) {
        this.rcQty = rcQty;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public String getStockValue() {
        return stockValue;
    }

    public void setStockValue(String stockValue) {
        this.stockValue = stockValue;
    }

    public String getTechnician() {
        return technician;
    }

    public void setTechnician(String technician) {
        this.technician = technician;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(String totalQty) {
        this.totalQty = totalQty;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getUnusedQty() {
        return unusedQty;
    }

    public void setUnusedQty(String unusedQty) {
        this.unusedQty = unusedQty;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getCompletedDate() {
        return completedDate;
    }

    public void setCompletedDate(String completedDate) {
        this.completedDate = completedDate;
    }

    public String getCompletedProp() {
        return completedProp;
    }

    public void setCompletedProp(String completedProp) {
        this.completedProp = completedProp;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getRaisedProp() {
        return raisedProp;
    }

    public void setRaisedProp(String raisedProp) {
        this.raisedProp = raisedProp;
    }

    public String getUsageType() {
        return usageType;
    }

    public void setUsageType(String usageType) {
        this.usageType = usageType;
    }

    public String getHikedAmount() {
        return hikedAmount;
    }

    public void setHikedAmount(String hikedAmount) {
        this.hikedAmount = hikedAmount;
    }

    public String getMargin() {
        return margin;
    }

    public void setMargin(String margin) {
        this.margin = margin;
    }

    public String getPayableTax() {
        return payableTax;
    }

    public void setPayableTax(String payableTax) {
        this.payableTax = payableTax;
    }

    public float getHikePercentage() {
        return hikePercentage;
    }

    public void setHikePercentage(float hikePercentage) {
        this.hikePercentage = hikePercentage;
    }

    public String getSparesAmount() {
        return sparesAmount;
    }

    public void setSparesAmount(String sparesAmount) {
        this.sparesAmount = sparesAmount;
    }

    public String getSparesWithTax() {
        return sparesWithTax;
    }

    public void setSparesWithTax(String sparesWithTax) {
        this.sparesWithTax = sparesWithTax;
    }

    public String getFreight() {
        return freight;
    }

    public void setFreight(String freight) {
        this.freight = freight;
    }

    public String getApprover() {
        return approver;
    }

    public void setApprover(String approver) {
        this.approver = approver;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getRcCode() {
        return rcCode;
    }

    public void setRcCode(String rcCode) {
        this.rcCode = rcCode;
    }

    public String getRcWoId() {
        return rcWoId;
    }

    public void setRcWoId(String rcWoId) {
        this.rcWoId = rcWoId;
    }

    public String[] getAddressSplit() {
        return addressSplit;
    }

    public void setAddressSplit(String[] addressSplit) {
        this.addressSplit = addressSplit;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String[] getRemarksSplit() {
        return remarksSplit;
    }

    public void setRemarksSplit(String[] remarksSplit) {
        this.remarksSplit = remarksSplit;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public String getDcNo() {
        return dcNo;
    }

    public void setDcNo(String dcNo) {
        this.dcNo = dcNo;
    }

    public static FPUtil getFpUtil() {
        return fpUtil;
    }

    public static void setFpUtil(FPUtil fpUtil) {
        ReportTO.fpUtil = fpUtil;
    }

    public String getLastKm() {
        return lastKm;
    }

    public void setLastKm(String lastKm) {
        this.lastKm = lastKm;
    }

    public String getLastProblem() {
        return lastProblem;
    }

    public void setLastProblem(String lastProblem) {
        this.lastProblem = lastProblem;
    }

    public String getLastRemarks() {
        return lastRemarks;
    }

    public void setLastRemarks(String lastRemarks) {
        this.lastRemarks = lastRemarks;
    }

    public String getLastStatus() {
        return lastStatus;
    }

    public void setLastStatus(String lastStatus) {
        this.lastStatus = lastStatus;
    }

    public String getLastTech() {
        return lastTech;
    }

    public void setLastTech(String lastTech) {
        this.lastTech = lastTech;
    }

    public String getProblemName() {
        return problemName;
    }

    public void setProblemName(String problemName) {
        this.problemName = problemName;
    }

    public String getScheduledDate() {
        return scheduledDate;
    }

    public void setScheduledDate(String scheduledDate) {
        this.scheduledDate = scheduledDate;
    }

    public String getLastDate() {
        return lastDate;
    }

    public void setLastDate(String lastDate) {
        this.lastDate = lastDate;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getTotJc() {
        return totJc;
    }

    public void setTotJc(String totJc) {
        this.totJc = totJc;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getVehCount() {
        return vehCount;
    }

    public void setVehCount(String vehCount) {
        this.vehCount = vehCount;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getServicePointId() {
        return servicePointId;
    }

    public void setServicePointId(String servicePointId) {
        this.servicePointId = servicePointId;
    }

    public String getBillingCompleted() {
        return billingCompleted;
    }

    public void setBillingCompleted(String billingCompleted) {
        this.billingCompleted = billingCompleted;
    }

    public String getCompleted() {
        return completed;
    }

    public void setCompleted(String completed) {
        this.completed = completed;
    }

    public String getNotPlanned() {
        return notPlanned;
    }

    public void setNotPlanned(String notPlanned) {
        this.notPlanned = notPlanned;
    }

    public String getPlanned() {
        return planned;
    }

    public void setPlanned(String planned) {
        this.planned = planned;
    }

    public String getItemTypes() {
        return itemTypes;
    }

    public void setItemTypes(String itemTypes) {
        this.itemTypes = itemTypes;
    }

    public String getStworth() {
        return stworth;
    }

    public void setStworth(String stworth) {
        this.stworth = stworth;
    }

    public ArrayList getVatValues() {
        return vatValues;
    }

    public void setVatValues(ArrayList vatValues) {
        this.vatValues = vatValues;
    }

    public float getTaxAmount() {

        return taxAmount;
    }

    public void setTaxAmount(float taxAmount) {

        this.taxAmount = taxAmount;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getInTime() {
        return inTime;
    }

    public void setInTime(String inTime) {
        this.inTime = inTime;
    }

    public String getBayNo() {
        return bayNo;
    }

    public void setBayNo(String bayNo) {
        this.bayNo = bayNo;
    }

    public String getContractAmnt() {
        return contractAmnt;
    }

    public void setContractAmnt(String contractAmnt) {
        this.contractAmnt = contractAmnt;
    }

    public String getServiceTaxAmnt() {
        return serviceTaxAmnt;
    }

    public void setServiceTaxAmnt(String serviceTaxAmnt) {
        this.serviceTaxAmnt = serviceTaxAmnt;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(String buyPrice) {
        this.buyPrice = buyPrice;
    }

    public String getNettProfit() {
        return nettProfit;
    }

    public void setNettProfit(String nettProfit) {
        this.nettProfit = nettProfit;
    }

    public String getProfit() {
        return profit;
    }

    public void setProfit(String profit) {
        this.profit = profit;
    }

    public String getRetQty() {
        return retQty;
    }

    public void setRetQty(String retQty) {
        this.retQty = retQty;
    }

    public String getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(String sellPrice) {
        this.sellPrice = sellPrice;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    //Rathimeena Driver Settlement Start
    public String getAdvDatetime() {
        return advDatetime;
    }

    public void setAdvDatetime(String advDatetime) {
        this.advDatetime = advDatetime;
    }

    public String getAlightDate() {
        return alightDate;
    }

    public void setAlightDate(String alightDate) {
        this.alightDate = alightDate;
    }

    public String getAlightStatus() {
        return alightStatus;
    }

    public void setAlightStatus(String alightStatus) {
        this.alightStatus = alightStatus;
    }

    public String getBunkName() {
        return bunkName;
    }

    public void setBunkName(String bunkName) {
        this.bunkName = bunkName;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getDeliveredTonnage() {
        return deliveredTonnage;
    }

    public void setDeliveredTonnage(String deliveredTonnage) {
        this.deliveredTonnage = deliveredTonnage;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getEmbarkDate() {
        return embarkDate;
    }

    public void setEmbarkDate(String embarkDate) {
        this.embarkDate = embarkDate;
    }

    public String getExpensesDesc() {
        return expensesDesc;
    }

    public void setExpensesDesc(String expensesDesc) {
        this.expensesDesc = expensesDesc;
    }

    public String getFuelDatetime() {
        return fuelDatetime;
    }

    public void setFuelDatetime(String fuelDatetime) {
        this.fuelDatetime = fuelDatetime;
    }

    public String getFuelName() {
        return fuelName;
    }

    public void setFuelName(String fuelName) {
        this.fuelName = fuelName;
    }

    public String getIdentityNo() {
        return identityNo;
    }

    public void setIdentityNo(String identityNo) {
        this.identityNo = identityNo;
    }

    public String getInDateTime() {
        return inDateTime;
    }

    public void setInDateTime(String inDateTime) {
        this.inDateTime = inDateTime;
    }

    public String getInKM() {
        return inKM;
    }

    public void setInKM(String inKM) {
        this.inKM = inKM;
    }

    public String getInOutDateTime() {
        return inOutDateTime;
    }

    public void setInOutDateTime(String inOutDateTime) {
        this.inOutDateTime = inOutDateTime;
    }

    public String getInOutIndication() {
        return inOutIndication;
    }

    public void setInOutIndication(String inOutIndication) {
        this.inOutIndication = inOutIndication;
    }

    public String getIssuerName() {
        return issuerName;
    }

    public void setIssuerName(String issuerName) {
        this.issuerName = issuerName;
    }

    public String getLiters() {
        return liters;
    }

    public void setLiters(String liters) {
        this.liters = liters;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getOutDateTime() {
        return outDateTime;
    }

    public void setOutDateTime(String outDateTime) {
        this.outDateTime = outDateTime;
    }

    public String getOutKM() {
        return outKM;
    }

    public void setOutKM(String outKM) {
        this.outKM = outKM;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getSettlementFlag() {
        return settlementFlag;
    }

    public void setSettlementFlag(String settlementFlag) {
        this.settlementFlag = settlementFlag;
    }

    public String getTotalTonnage() {
        return totalTonnage;
    }

    public void setTotalTonnage(String totalTonnage) {
        this.totalTonnage = totalTonnage;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getCompId() {
        return compId;
    }

    public void setCompId(String compId) {
        this.compId = compId;
    }

    public String getRegno() {
        return regno;
    }

    public void setRegno(String regno) {
        this.regno = regno;
    }

    public String getDriverDue() {
        return driverDue;
    }

    public void setDriverDue(String driverDue) {
        this.driverDue = driverDue;
    }

    public String getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(String statusDate) {
        this.statusDate = statusDate;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getTripArrivalDate() {
        return tripArrivalDate;
    }

    public void setTripArrivalDate(String tripArrivalDate) {
        this.tripArrivalDate = tripArrivalDate;
    }

    public String getTripBalanceAmount() {
        return tripBalanceAmount;
    }

    public void setTripBalanceAmount(String tripBalanceAmount) {
        this.tripBalanceAmount = tripBalanceAmount;
    }

    public String getTripCode() {
        return tripCode;
    }

    public void setTripCode(String tripCode) {
        this.tripCode = tripCode;
    }

    public String getTripDate() {
        return tripDate;
    }

    public void setTripDate(String tripDate) {
        this.tripDate = tripDate;
    }

    public String getTripDepartureDate() {
        return tripDepartureDate;
    }

    public void setTripDepartureDate(String tripDepartureDate) {
        this.tripDepartureDate = tripDepartureDate;
    }

    public String getTripDriverId() {
        return tripDriverId;
    }

    public void setTripDriverId(String tripDriverId) {
        this.tripDriverId = tripDriverId;
    }

    public String getTripFuelAmount() {
        return tripFuelAmount;
    }

    public void setTripFuelAmount(String tripFuelAmount) {
        this.tripFuelAmount = tripFuelAmount;
    }

    public String getTripKmsIn() {
        return tripKmsIn;
    }

    public void setTripKmsIn(String tripKmsIn) {
        this.tripKmsIn = tripKmsIn;
    }

    public String getTripKmsOut() {
        return tripKmsOut;
    }

    public void setTripKmsOut(String tripKmsOut) {
        this.tripKmsOut = tripKmsOut;
    }

    public String getTripRouteId() {
        return tripRouteId;
    }

    public void setTripRouteId(String tripRouteId) {
        this.tripRouteId = tripRouteId;
    }

    public String getTripScheduleId() {
        return tripScheduleId;
    }

    public void setTripScheduleId(String tripScheduleId) {
        this.tripScheduleId = tripScheduleId;
    }

    public String getTripSheetId() {
        return tripSheetId;
    }

    public void setTripSheetId(String tripSheetId) {
        this.tripSheetId = tripSheetId;
    }

    public String getTripStatus() {
        return tripStatus;
    }

    public void setTripStatus(String tripStatus) {
        this.tripStatus = tripStatus;
    }

    public String getTripTotalAllowances() {
        return tripTotalAllowances;
    }

    public void setTripTotalAllowances(String tripTotalAllowances) {
        this.tripTotalAllowances = tripTotalAllowances;
    }

    public String getTripTotalExpenses() {
        return tripTotalExpenses;
    }

    public void setTripTotalExpenses(String tripTotalExpenses) {
        this.tripTotalExpenses = tripTotalExpenses;
    }

    public String getTripTotalKms() {
        return tripTotalKms;
    }

    public void setTripTotalKms(String tripTotalKms) {
        this.tripTotalKms = tripTotalKms;
    }

    public String getTripTotalLitres() {
        return tripTotalLitres;
    }

    public void setTripTotalLitres(String tripTotalLitres) {
        this.tripTotalLitres = tripTotalLitres;
    }

    public String getTripVehicleId() {
        return tripVehicleId;
    }

    public void setTripVehicleId(String tripVehicleId) {
        this.tripVehicleId = tripVehicleId;
    }

    public String getTripEndDate() {
        return tripEndDate;
    }

    public void setTripEndDate(String tripEndDate) {
        this.tripEndDate = tripEndDate;
    }

    public String getTripStartDate() {
        return tripStartDate;
    }

    public void setTripStartDate(String tripStartDate) {
        this.tripStartDate = tripStartDate;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getKnownLocation() {
        return knownLocation;
    }

    public void setKnownLocation(String knownLocation) {
        this.knownLocation = knownLocation;
    }

    public String getVehicleInOut() {
        return vehicleInOut;
    }

    public void setVehicleInOut(String vehicleInOut) {
        this.vehicleInOut = vehicleInOut;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getDriId() {
        return driId;
    }

    public void setDriId(String driId) {
        this.driId = driId;
    }

    public String getDieselExpense() {
        return dieselExpense;
    }

    public void setDieselExpense(String dieselExpense) {
        this.dieselExpense = dieselExpense;
    }

    public String getTollExpense() {
        return tollExpense;
    }

    public void setTollExpense(String tollExpense) {
        this.tollExpense = tollExpense;
    }

    public String getTotalExpense() {
        return totalExpense;
    }

    public void setTotalExpense(String totalExpense) {
        this.totalExpense = totalExpense;
    }

    public String getTotalTonAmount() {
        return totalTonAmount;
    }

    public void setTotalTonAmount(String totalTonAmount) {
        this.totalTonAmount = totalTonAmount;
    }

    public String getDueIn() {
        return dueIn;
    }

    public void setDueIn(String dueIn) {
        this.dueIn = dueIn;
    }

    public String getPermitType() {
        return permitType;
    }

    public void setPermitType(String permitType) {
        this.permitType = permitType;
    }

    public String getAmcAmount() {
        return amcAmount;
    }

    public void setAmcAmount(String amcAmount) {
        this.amcAmount = amcAmount;
    }

    public String getAmcCompanyName() {
        return amcCompanyName;
    }

    public void setAmcCompanyName(String amcCompanyName) {
        this.amcCompanyName = amcCompanyName;
    }

    public String getAmcDuration() {
        return amcDuration;
    }

    public void setAmcDuration(String amcDuration) {
        this.amcDuration = amcDuration;
    }

    public int getDriverId() {
        return driverId;
    }

    public void setDriverId(int driverId) {
        this.driverId = driverId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLpsId() {
        return lpsId;
    }

    public void setLpsId(String lpsId) {
        this.lpsId = lpsId;
    }

    public String getTrippedLPS() {
        return trippedLPS;
    }

    public void setTrippedLPS(String trippedLPS) {
        this.trippedLPS = trippedLPS;
    }

    public String getLpsCount() {
        return lpsCount;
    }

    public void setLpsCount(String lpsCount) {
        this.lpsCount = lpsCount;
    }

    public String getBags() {
        return bags;
    }

    public void setBags(String bags) {
        this.bags = bags;
    }

    public String getBalanceamount() {
        return balanceamount;
    }

    public void setBalanceamount(String balanceamount) {
        this.balanceamount = balanceamount;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getPinkSlipID() {
        return pinkSlipID;
    }

    public void setPinkSlipID(String pinkSlipID) {
        this.pinkSlipID = pinkSlipID;
    }

    public String getRevenue() {
        return revenue;
    }

    public void setRevenue(String revenue) {
        this.revenue = revenue;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getTotalallowance() {
        return totalallowance;
    }

    public void setTotalallowance(String totalallowance) {
        this.totalallowance = totalallowance;
    }

    public String getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(String totalamount) {
        this.totalamount = totalamount;
    }

    public String getTotalexpenses() {
        return totalexpenses;
    }

    public void setTotalexpenses(String totalexpenses) {
        this.totalexpenses = totalexpenses;
    }

    public String getTotalkms() {
        return totalkms;
    }

    public void setTotalkms(String totalkms) {
        this.totalkms = totalkms;
    }

    public String getTotalliters() {
        return totalliters;
    }

    public void setTotalliters(String totalliters) {
        this.totalliters = totalliters;
    }

    public String getVehicleid() {
        return vehicleid;
    }

    public void setVehicleid(String vehicleid) {
        this.vehicleid = vehicleid;
    }

    public String getVendorSettlementFlag() {
        return vendorSettlementFlag;
    }

    public void setVendorSettlementFlag(String vendorSettlementFlag) {
        this.vendorSettlementFlag = vendorSettlementFlag;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPandL() {
        return pandL;
    }

    public void setPandL(String pandL) {
        this.pandL = pandL;
    }

    public String getExpenses() {
        return expenses;
    }

    public void setExpenses(String expenses) {
        this.expenses = expenses;
    }

    public String getCustomertypeId() {
        return customertypeId;
    }

    public void setCustomertypeId(String customertypeId) {
        this.customertypeId = customertypeId;
    }

    public String getCustomertypeName() {
        return customertypeName;
    }

    public void setCustomertypeName(String customertypeName) {
        this.customertypeName = customertypeName;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getOwnership() {
        return ownership;
    }

    public void setOwnership(String ownership) {
        this.ownership = ownership;
    }

    public String getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(String billStatus) {
        this.billStatus = billStatus;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getGpno() {
        return gpno;
    }

    public void setGpno(String gpno) {
        this.gpno = gpno;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getTotalFreightAmount() {
        return totalFreightAmount;
    }

    public void setTotalFreightAmount(String totalFreightAmount) {
        this.totalFreightAmount = totalFreightAmount;
    }

    public String getConsignmentType() {
        return consignmentType;
    }

    public void setConsignmentType(String consignmentType) {
        this.consignmentType = consignmentType;
    }

    public String getCrossing() {
        return crossing;
    }

    public void setCrossing(String crossing) {
        this.crossing = crossing;
    }

    public String getSettlementAmount() {
        return settlementAmount;
    }

    public void setSettlementAmount(String settlementAmount) {
        this.settlementAmount = settlementAmount;
    }

    public String getConsignmentName() {
        return consignmentName;
    }

    public void setConsignmentName(String consignmentName) {
        this.consignmentName = consignmentName;
    }

    public String getSettlementDate() {
        return settlementDate;
    }

    public void setSettlementDate(String settlementDate) {
        this.settlementDate = settlementDate;
    }

    public String getCNAmount() {
        return CNAmount;
    }

    public void setCNAmount(String CNAmount) {
        this.CNAmount = CNAmount;
    }

    public String getDNAmount() {
        return DNAmount;
    }

    public void setDNAmount(String DNAmount) {
        this.DNAmount = DNAmount;
    }

    public String getBillingType() {
        return billingType;
    }

    public void setBillingType(String billingType) {
        this.billingType = billingType;
    }

    public String getFreightAmount() {
        return freightAmount;
    }

    public void setFreightAmount(String freightAmount) {
        this.freightAmount = freightAmount;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getInvRefCode() {
        return invRefCode;
    }

    public void setInvRefCode(String invRefCode) {
        this.invRefCode = invRefCode;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    public String getNumberOfTrip() {
        return numberOfTrip;
    }

    public void setNumberOfTrip(String numberOfTrip) {
        this.numberOfTrip = numberOfTrip;
    }

    public String getOtherExpenseAmount() {
        return otherExpenseAmount;
    }

    public void setOtherExpenseAmount(String otherExpenseAmount) {
        this.otherExpenseAmount = otherExpenseAmount;
    }

    public String getOwnerShip() {
        return ownerShip;
    }

    public void setOwnerShip(String ownerShip) {
        this.ownerShip = ownerShip;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getDriverBata() {
        return driverBata;
    }

    public void setDriverBata(double driverBata) {
        this.driverBata = driverBata;
    }

    public double getDriverExpense() {
        return driverExpense;
    }

    public void setDriverExpense(double driverExpense) {
        this.driverExpense = driverExpense;
    }

    public double getDriverIncentive() {
        return driverIncentive;
    }

    public void setDriverIncentive(double driverIncentive) {
        this.driverIncentive = driverIncentive;
    }

    public double getEmiAmount() {
        return emiAmount;
    }

    public void setEmiAmount(double emiAmount) {
        this.emiAmount = emiAmount;
    }

    public double getFcAmount() {
        return fcAmount;
    }

    public void setFcAmount(double fcAmount) {
        this.fcAmount = fcAmount;
    }

    public double getFixedExpensePerDay() {
        return fixedExpensePerDay;
    }

    public void setFixedExpensePerDay(double fixedExpensePerDay) {
        this.fixedExpensePerDay = fixedExpensePerDay;
    }

    public double getFuelAmount() {
        return fuelAmount;
    }

    public void setFuelAmount(double fuelAmount) {
        this.fuelAmount = fuelAmount;
    }

    public double getInsuranceAmount() {
        return insuranceAmount;
    }

    public void setInsuranceAmount(double insuranceAmount) {
        this.insuranceAmount = insuranceAmount;
    }

    public double getMaintainExpense() {
        return maintainExpense;
    }

    public void setMaintainExpense(double maintainExpense) {
        this.maintainExpense = maintainExpense;
    }

    public double getNetExpense() {
        return netExpense;
    }

    public void setNetExpense(double netExpense) {
        this.netExpense = netExpense;
    }

    public double getNetProfit() {
        return netProfit;
    }

    public void setNetProfit(double netProfit) {
        this.netProfit = netProfit;
    }

    public double getPermitAmount() {
        return permitAmount;
    }

    public void setPermitAmount(double permitAmount) {
        this.permitAmount = permitAmount;
    }

    public double getRoadTaxAmount() {
        return roadTaxAmount;
    }

    public void setRoadTaxAmount(double roadTaxAmount) {
        this.roadTaxAmount = roadTaxAmount;
    }

    public double getRouteExpenses() {
        return routeExpenses;
    }

    public void setRouteExpenses(double routeExpenses) {
        this.routeExpenses = routeExpenses;
    }

    public int getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(int timePeriod) {
        this.timePeriod = timePeriod;
    }

    public double getTollAmount() {
        return tollAmount;
    }

    public void setTollAmount(double tollAmount) {
        this.tollAmount = tollAmount;
    }

    public double getTotlalFixedExpense() {
        return totlalFixedExpense;
    }

    public void setTotlalFixedExpense(double totlalFixedExpense) {
        this.totlalFixedExpense = totlalFixedExpense;
    }

    public double getTotlalOperationExpense() {
        return totlalOperationExpense;
    }

    public void setTotlalOperationExpense(double totlalOperationExpense) {
        this.totlalOperationExpense = totlalOperationExpense;
    }

    public String getTripNos() {
        return tripNos;
    }

    public void setTripNos(String tripNos) {
        this.tripNos = tripNos;
    }

    public double getMiscAmount() {
        return miscAmount;
    }

    public void setMiscAmount(double miscAmount) {
        this.miscAmount = miscAmount;
    }

    public double getTripOtherExpense() {
        return tripOtherExpense;
    }

    public void setTripOtherExpense(double tripOtherExpense) {
        this.tripOtherExpense = tripOtherExpense;
    }

    public double getVehicleDriverSalary() {
        return vehicleDriverSalary;
    }

    public void setVehicleDriverSalary(double vehicleDriverSalary) {
        this.vehicleDriverSalary = vehicleDriverSalary;
    }

    public String getReeferRequired() {
        return reeferRequired;
    }

    public void setReeferRequired(String reeferRequired) {
        this.reeferRequired = reeferRequired;
    }

    public String getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(String totalWeight) {
        this.totalWeight = totalWeight;
    }

    public String rep() {
        return fleetCenterId;
    }

    public String getFleetCenterId() {
        return fleetCenterId;
    }

    public void setFleetCenterId(String fleetCenterId) {
        this.fleetCenterId = fleetCenterId;
    }

    public String getTripStatusId() {
        return tripStatusId;
    }

    public void setTripStatusId(String tripStatusId) {
        this.tripStatusId = tripStatusId;
    }

    public String getVehicleCapUtil() {
        return vehicleCapUtil;
    }

    public void setVehicleCapUtil(String vehicleCapUtil) {
        this.vehicleCapUtil = vehicleCapUtil;
    }

    public String getUtilisedDays() {
        return utilisedDays;
    }

    public void setUtilisedDays(String utilisedDays) {
        this.utilisedDays = utilisedDays;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getApprovalstatus() {
        return approvalstatus;
    }

    public void setApprovalstatus(String approvalstatus) {
        this.approvalstatus = approvalstatus;
    }

    public String getConsignmentNoteNo() {
        return consignmentNoteNo;
    }

    public void setConsignmentNoteNo(String consignmentNoteNo) {
        this.consignmentNoteNo = consignmentNoteNo;
    }

    public String getEstimatedRevenue() {
        return estimatedRevenue;
    }

    public void setEstimatedRevenue(String estimatedRevenue) {
        this.estimatedRevenue = estimatedRevenue;
    }

    public double getProfitPercent() {
        return profitPercent;
    }

    public void setProfitPercent(double profitPercent) {
        this.profitPercent = profitPercent;
    }

    public double getProfitValue() {
        return profitValue;
    }

    public void setProfitValue(double profitValue) {
        this.profitValue = profitValue;
    }

    public String getOperationTypeId() {
        return operationTypeId;
    }

    public void setOperationTypeId(String operationTypeId) {
        this.operationTypeId = operationTypeId;
    }

    public String getProfitType() {
        return profitType;
    }

    public void setProfitType(String profitType) {
        this.profitType = profitType;
    }

    public String getConsignmentNote() {
        return consignmentNote;
    }

    public void setConsignmentNote(String consignmentNote) {
        this.consignmentNote = consignmentNote;
    }

    public double getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(double creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getDaysToOverDue() {
        return daysToOverDue;
    }

    public void setDaysToOverDue(String daysToOverDue) {
        this.daysToOverDue = daysToOverDue;
    }

    public String getDaysWithOverDue() {
        return daysWithOverDue;
    }

    public void setDaysWithOverDue(String daysWithOverDue) {
        this.daysWithOverDue = daysWithOverDue;
    }

    public double getOverDueGrandTotal() {
        return overDueGrandTotal;
    }

    public void setOverDueGrandTotal(double overDueGrandTotal) {
        this.overDueGrandTotal = overDueGrandTotal;
    }

    public String getOverDueOn() {
        return overDueOn;
    }

    public void setOverDueOn(String overDueOn) {
        this.overDueOn = overDueOn;
    }

    public double getTotalAmountReceivable() {
        return totalAmountReceivable;
    }

    public void setTotalAmountReceivable(double totalAmountReceivable) {
        this.totalAmountReceivable = totalAmountReceivable;
    }

    public String getRunHm() {
        return runHm;
    }

    public void setRunHm(String runHm) {
        this.runHm = runHm;
    }

    public String getRunKm() {
        return runKm;
    }

    public void setRunKm(String runKm) {
        this.runKm = runKm;
    }

    public String getCurrentTemperature() {
        return currentTemperature;
    }

    public void setCurrentTemperature(String currentTemperature) {
        this.currentTemperature = currentTemperature;
    }

    public String getLogDate() {
        return logDate;
    }

    public void setLogDate(String logDate) {
        this.logDate = logDate;
    }

    public String getBhatta() {
        return bhatta;
    }

    public void setBhatta(String bhatta) {
        this.bhatta = bhatta;
    }

    public String getBpclAllocation() {
        return bpclAllocation;
    }

    public void setBpclAllocation(String bpclAllocation) {
        this.bpclAllocation = bpclAllocation;
    }

    public String getDieselUsed() {
        return dieselUsed;
    }

    public void setDieselUsed(String dieselUsed) {
        this.dieselUsed = dieselUsed;
    }

    public String getEndingBalance() {
        return endingBalance;
    }

    public void setEndingBalance(String endingBalance) {
        this.endingBalance = endingBalance;
    }

    public String getExtraExpense() {
        return extraExpense;
    }

    public void setExtraExpense(String extraExpense) {
        this.extraExpense = extraExpense;
    }

    public String getFuelPrice() {
        return fuelPrice;
    }

    public void setFuelPrice(String fuelPrice) {
        this.fuelPrice = fuelPrice;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getRcmAllocation() {
        return rcmAllocation;
    }

    public void setRcmAllocation(String rcmAllocation) {
        this.rcmAllocation = rcmAllocation;
    }

    public String getRunHour() {
        return runHour;
    }

    public void setRunHour(String runHour) {
        this.runHour = runHour;
    }

    public String getStartingBalance() {
        return startingBalance;
    }

    public void setStartingBalance(String startingBalance) {
        this.startingBalance = startingBalance;
    }

    public String getTotalMiscellaneous() {
        return totalMiscellaneous;
    }

    public void setTotalMiscellaneous(String totalMiscellaneous) {
        this.totalMiscellaneous = totalMiscellaneous;
    }

    public String getLogTime() {
        return logTime;
    }

    public void setLogTime(String logTime) {
        this.logTime = logTime;
    }

    public String getBillableKM() {
        return billableKM;
    }

    public void setBillableKM(String billableKM) {
        this.billableKM = billableKM;
    }

    public String getAccountingDate() {
        return accountingDate;
    }

    public void setAccountingDate(String accountingDate) {
        this.accountingDate = accountingDate;
    }

    public String getAmountBalance() {
        return amountBalance;
    }

    public void setAmountBalance(String amountBalance) {
        this.amountBalance = amountBalance;
    }

    public String getbPCLAccountId() {
        return bPCLAccountId;
    }

    public void setbPCLAccountId(String bPCLAccountId) {
        this.bPCLAccountId = bPCLAccountId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDealerCity() {
        return dealerCity;
    }

    public void setDealerCity(String dealerCity) {
        this.dealerCity = dealerCity;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getOdometerReading() {
        return odometerReading;
    }

    public void setOdometerReading(String odometerReading) {
        this.odometerReading = odometerReading;
    }

    public String getPetromilesEarned() {
        return petromilesEarned;
    }

    public void setPetromilesEarned(String petromilesEarned) {
        this.petromilesEarned = petromilesEarned;
    }

    public String getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionHistoryId() {
        return transactionHistoryId;
    }

    public void setTransactionHistoryId(String transactionHistoryId) {
        this.transactionHistoryId = transactionHistoryId;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getVolumeDocumentNo() {
        return volumeDocumentNo;
    }

    public void setVolumeDocumentNo(String volumeDocumentNo) {
        this.volumeDocumentNo = volumeDocumentNo;
    }

    public String getDistanceTravelled() {
        return distanceTravelled;
    }

    public void setDistanceTravelled(String distanceTravelled) {
        this.distanceTravelled = distanceTravelled;
    }

    public String getGpsEndAttempt() {
        return gpsEndAttempt;
    }

    public void setGpsEndAttempt(String gpsEndAttempt) {
        this.gpsEndAttempt = gpsEndAttempt;
    }

    public String getGpsEndErrorCode() {
        return gpsEndErrorCode;
    }

    public void setGpsEndErrorCode(String gpsEndErrorCode) {
        this.gpsEndErrorCode = gpsEndErrorCode;
    }

    public String getGpsEndErrorMsg() {
        return gpsEndErrorMsg;
    }

    public void setGpsEndErrorMsg(String gpsEndErrorMsg) {
        this.gpsEndErrorMsg = gpsEndErrorMsg;
    }

    public String getGpsEndInd() {
        return gpsEndInd;
    }

    public void setGpsEndInd(String gpsEndInd) {
        this.gpsEndInd = gpsEndInd;
    }

    public String getGpsEndUpdateTime() {
        return gpsEndUpdateTime;
    }

    public void setGpsEndUpdateTime(String gpsEndUpdateTime) {
        this.gpsEndUpdateTime = gpsEndUpdateTime;
    }

    public String getGpsStartAttempt() {
        return gpsStartAttempt;
    }

    public void setGpsStartAttempt(String gpsStartAttempt) {
        this.gpsStartAttempt = gpsStartAttempt;
    }

    public String getGpsStartErrorCode() {
        return gpsStartErrorCode;
    }

    public void setGpsStartErrorCode(String gpsStartErrorCode) {
        this.gpsStartErrorCode = gpsStartErrorCode;
    }

    public String getGpsStartErrorMsg() {
        return gpsStartErrorMsg;
    }

    public void setGpsStartErrorMsg(String gpsStartErrorMsg) {
        this.gpsStartErrorMsg = gpsStartErrorMsg;
    }

    public String getGpsStartInd() {
        return gpsStartInd;
    }

    public void setGpsStartInd(String gpsStartInd) {
        this.gpsStartInd = gpsStartInd;
    }

    public String getGpsStartUpdateTime() {
        return gpsStartUpdateTime;
    }

    public void setGpsStartUpdateTime(String gpsStartUpdateTime) {
        this.gpsStartUpdateTime = gpsStartUpdateTime;
    }

    public String getGpsSystem() {
        return gpsSystem;
    }

    public void setGpsSystem(String gpsSystem) {
        this.gpsSystem = gpsSystem;
    }

    public String getGpsTripDetailsAttempt() {
        return gpsTripDetailsAttempt;
    }

    public void setGpsTripDetailsAttempt(String gpsTripDetailsAttempt) {
        this.gpsTripDetailsAttempt = gpsTripDetailsAttempt;
    }

    public String getGpsTripDetailsDatetime() {
        return gpsTripDetailsDatetime;
    }

    public void setGpsTripDetailsDatetime(String gpsTripDetailsDatetime) {
        this.gpsTripDetailsDatetime = gpsTripDetailsDatetime;
    }

    public String getGpsTripDetailsErrorCode() {
        return gpsTripDetailsErrorCode;
    }

    public void setGpsTripDetailsErrorCode(String gpsTripDetailsErrorCode) {
        this.gpsTripDetailsErrorCode = gpsTripDetailsErrorCode;
    }

    public String getGpsTripDetailsErrorMsg() {
        return gpsTripDetailsErrorMsg;
    }

    public void setGpsTripDetailsErrorMsg(String gpsTripDetailsErrorMsg) {
        this.gpsTripDetailsErrorMsg = gpsTripDetailsErrorMsg;
    }

    public String getGpsTripDetailsInd() {
        return gpsTripDetailsInd;
    }

    public void setGpsTripDetailsInd(String gpsTripDetailsInd) {
        this.gpsTripDetailsInd = gpsTripDetailsInd;
    }

    public String getReeferAvgTemp() {
        return reeferAvgTemp;
    }

    public void setReeferAvgTemp(String reeferAvgTemp) {
        this.reeferAvgTemp = reeferAvgTemp;
    }

    public String getReeferRunHours() {
        return reeferRunHours;
    }

    public void setReeferRunHours(String reeferRunHours) {
        this.reeferRunHours = reeferRunHours;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getTripEndDateTime() {
        return tripEndDateTime;
    }

    public void setTripEndDateTime(String tripEndDateTime) {
        this.tripEndDateTime = tripEndDateTime;
    }

    public String getTripStartDateTime() {
        return tripStartDateTime;
    }

    public void setTripStartDateTime(String tripStartDateTime) {
        this.tripStartDateTime = tripStartDateTime;
    }

    public String getTsEndInd() {
        return tsEndInd;
    }

    public void setTsEndInd(String tsEndInd) {
        this.tsEndInd = tsEndInd;
    }

    public String getTsStartInd() {
        return tsStartInd;
    }

    public void setTsStartInd(String tsStartInd) {
        this.tsStartInd = tsStartInd;
    }

    public String getSales() {
        return sales;
    }

    public void setSales(String sales) {
        this.sales = sales;
    }

    public String getTrips() {
        return trips;
    }

    public void setTrips(String trips) {
        this.trips = trips;
    }

    public String getWfl() {
        return wfl;
    }

    public void setWfl(String wfl) {
        this.wfl = wfl;
    }

    public String getWfu() {
        return wfu;
    }

    public void setWfu(String wfu) {
        this.wfu = wfu;
    }

    public String getOperationPointId() {
        return operationPointId;
    }

    public void setOperationPointId(String operationPointId) {
        this.operationPointId = operationPointId;
    }

    public String getVehicles() {
        return vehicles;
    }

    public void setVehicles(String vehicles) {
        this.vehicles = vehicles;
    }

    public String getEstimatedExpenses() {
        return estimatedExpenses;
    }

    public void setEstimatedExpenses(String estimatedExpenses) {
        this.estimatedExpenses = estimatedExpenses;
    }

    public String getRmDays() {
        return rmDays;
    }

    public void setRmDays(String rmDays) {
        this.rmDays = rmDays;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getMonths() {
        return months;
    }

    public void setMonths(String months) {
        this.months = months;
    }

    //Rathimeena Driver Settlement End
    public String getRequestedAdvance() {
        return requestedAdvance;
    }

    public void setRequestedAdvance(String requestedAdvance) {
        this.requestedAdvance = requestedAdvance;
    }

    public String getPaidAdvance() {
        return paidAdvance;
    }

    public void setPaidAdvance(String paidAdvance) {
        this.paidAdvance = paidAdvance;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getCityFromId() {
        return cityFromId;
    }

    public void setCityFromId(String cityFromId) {
        this.cityFromId = cityFromId;
    }

    public String getPodStatus() {
        return podStatus;
    }

    public void setPodStatus(String podStatus) {
        this.podStatus = podStatus;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getOldVehicleNo() {
        return oldVehicleNo;
    }

    public void setOldVehicleNo(String oldVehicleNo) {
        this.oldVehicleNo = oldVehicleNo;
    }

    public String getOldDriverName() {
        return oldDriverName;
    }

    public void setOldDriverName(String oldDriverName) {
        this.oldDriverName = oldDriverName;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getCustomerOrderReferenceNo() {
        return customerOrderReferenceNo;
    }

    public void setCustomerOrderReferenceNo(String customerOrderReferenceNo) {
        this.customerOrderReferenceNo = customerOrderReferenceNo;
    }

    public String getcNotes() {
        return cNotes;
    }

    public void setcNotes(String cNotes) {
        this.cNotes = cNotes;
    }

    public String getRouteInfo() {
        return routeInfo;
    }

    public void setRouteInfo(String routeInfo) {
        this.routeInfo = routeInfo;
    }

    public String getFleetCenterName() {
        return fleetCenterName;
    }

    public void setFleetCenterName(String fleetCenterName) {
        this.fleetCenterName = fleetCenterName;
    }

    public String getExpectedArrivalDateTime() {
        return expectedArrivalDateTime;
    }

    public void setExpectedArrivalDateTime(String expectedArrivalDateTime) {
        this.expectedArrivalDateTime = expectedArrivalDateTime;
    }

    public String getOrderRevenue() {
        return orderRevenue;
    }

    public void setOrderRevenue(String orderRevenue) {
        this.orderRevenue = orderRevenue;
    }

    public String getOrderExpense() {
        return orderExpense;
    }

    public void setOrderExpense(String orderExpense) {
        this.orderExpense = orderExpense;
    }

    public String getActualAdvancePaid() {
        return actualAdvancePaid;
    }

    public void setActualAdvancePaid(String actualAdvancePaid) {
        this.actualAdvancePaid = actualAdvancePaid;
    }

    public String getVehicleTonnage() {
        return vehicleTonnage;
    }

    public void setVehicleTonnage(String vehicleTonnage) {
        this.vehicleTonnage = vehicleTonnage;
    }

    public String getGpsLocation() {
        return gpsLocation;
    }

    public void setGpsLocation(String gpsLocation) {
        this.gpsLocation = gpsLocation;
    }

    public String getPlannedStartDateTime() {
        return plannedStartDateTime;
    }

    public void setPlannedStartDateTime(String plannedStartDateTime) {
        this.plannedStartDateTime = plannedStartDateTime;
    }

    public String getActualStartDateTime() {
        return actualStartDateTime;
    }

    public void setActualStartDateTime(String actualStartDateTime) {
        this.actualStartDateTime = actualStartDateTime;
    }

    public String getPlannedEndDateTime() {
        return plannedEndDateTime;
    }

    public void setPlannedEndDateTime(String plannedEndDateTime) {
        this.plannedEndDateTime = plannedEndDateTime;
    }

    public String getActualEndDateTime() {
        return actualEndDateTime;
    }

    public void setActualEndDateTime(String actualEndDateTime) {
        this.actualEndDateTime = actualEndDateTime;
    }

    public String getWfuDateTime() {
        return wfuDateTime;
    }

    public void setWfuDateTime(String wfuDateTime) {
        this.wfuDateTime = wfuDateTime;
    }

    public String getWfuRemarks() {
        return wfuRemarks;
    }

    public void setWfuRemarks(String wfuRemarks) {
        this.wfuRemarks = wfuRemarks;
    }

    public String getWfuCreatedOn() {
        return wfuCreatedOn;
    }

    public void setWfuCreatedOn(String wfuCreatedOn) {
        this.wfuCreatedOn = wfuCreatedOn;
    }

    public String getEndedBy() {
        return endedBy;
    }

    public void setEndedBy(String endedBy) {
        this.endedBy = endedBy;
    }

    public String getClosedBy() {
        return closedBy;
    }

    public void setClosedBy(String closedBy) {
        this.closedBy = closedBy;
    }

    public String getSettledBy() {
        return settledBy;
    }

    public void setSettledBy(String settledBy) {
        this.settledBy = settledBy;
    }

    public String getFleetCenterNo() {
        return fleetCenterNo;
    }

    public void setFleetCenterNo(String fleetCenterNo) {
        this.fleetCenterNo = fleetCenterNo;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getExtraExpenseValue() {
        return extraExpenseValue;
    }

    public void setExtraExpenseValue(String extraExpenseValue) {
        this.extraExpenseValue = extraExpenseValue;
    }

    public String getTonnage() {
        return tonnage;
    }

    public void setTonnage(String tonnage) {
        this.tonnage = tonnage;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public String getOperationPoint() {
        return operationPoint;
    }

    public void setOperationPoint(String operationPoint) {
        this.operationPoint = operationPoint;
    }

    public String getFleetCenterHead() {
        return fleetCenterHead;
    }

    public void setFleetCenterHead(String fleetCenterHead) {
        this.fleetCenterHead = fleetCenterHead;
    }

    public String getVehiclewfudate() {
        return vehiclewfudate;
    }

    public void setVehiclewfudate(String vehiclewfudate) {
        this.vehiclewfudate = vehiclewfudate;
    }

    public String getVehiclewfutime() {
        return vehiclewfutime;
    }

    public void setVehiclewfutime(String vehiclewfutime) {
        this.vehiclewfutime = vehiclewfutime;
    }

    public String getTripstarttime() {
        return tripstarttime;
    }

    public void setTripstarttime(String tripstarttime) {
        this.tripstarttime = tripstarttime;
    }

    public String getServiceTypeName() {
        return serviceTypeName;
    }

    public void setServiceTypeName(String serviceTypeName) {
        this.serviceTypeName = serviceTypeName;
    }

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getClosedDateFrom() {
        return closedDateFrom;
    }

    public void setClosedDateFrom(String closedDateFrom) {
        this.closedDateFrom = closedDateFrom;
    }

    public String getClosedDateTo() {
        return closedDateTo;
    }

    public void setClosedDateTo(String closedDateTo) {
        this.closedDateTo = closedDateTo;
    }

    public String getEndDateFrom() {
        return endDateFrom;
    }

    public void setEndDateFrom(String endDateFrom) {
        this.endDateFrom = endDateFrom;
    }

    public String getEndDateTo() {
        return endDateTo;
    }

    public void setEndDateTo(String endDateTo) {
        this.endDateTo = endDateTo;
    }

    public String getSettledDateFrom() {
        return settledDateFrom;
    }

    public void setSettledDateFrom(String settledDateFrom) {
        this.settledDateFrom = settledDateFrom;
    }

    public String getSettledDateTo() {
        return settledDateTo;
    }

    public void setSettledDateTo(String settledDateTo) {
        this.settledDateTo = settledDateTo;
    }

    public String getStartDateFrom() {
        return startDateFrom;
    }

    public void setStartDateFrom(String startDateFrom) {
        this.startDateFrom = startDateFrom;
    }

    public String getStartDateTo() {
        return startDateTo;
    }

    public void setStartDateTo(String startDateTo) {
        this.startDateTo = startDateTo;
    }

    public String getTripStatusIdTo() {
        return tripStatusIdTo;
    }

    public void setTripStatusIdTo(String tripStatusIdTo) {
        this.tripStatusIdTo = tripStatusIdTo;
    }

    public String getNot() {
        return not;
    }

    public void setNot(String not) {
        this.not = not;
    }

    public String getLoadingDateTime() {
        return loadingDateTime;
    }

    public void setLoadingDateTime(String loadingDateTime) {
        this.loadingDateTime = loadingDateTime;
    }

    public String getUnLoadingDate() {
        return unLoadingDate;
    }

    public void setUnLoadingDate(String unLoadingDate) {
        this.unLoadingDate = unLoadingDate;
    }

    public String getTripDays() {
        return tripDays;
    }

    public void setTripDays(String tripDays) {
        this.tripDays = tripDays;
    }

    public String getTripRunHm() {
        return tripRunHm;
    }

    public void setTripRunHm(String tripRunHm) {
        this.tripRunHm = tripRunHm;
    }

    public String getTripRunKm() {
        return tripRunKm;
    }

    public void setTripRunKm(String tripRunKm) {
        this.tripRunKm = tripRunKm;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public int getAmountSpend() {
        return amountSpend;
    }

    public void setAmountSpend(int amountSpend) {
        this.amountSpend = amountSpend;
    }

    public int getTripCount() {
        return tripCount;
    }

    public void setTripCount(int tripCount) {
        this.tripCount = tripCount;
    }

    public String getJobCardDays() {
        return jobCardDays;
    }

    public void setJobCardDays(String jobCardDays) {
        this.jobCardDays = jobCardDays;
    }

    public String getJobCardNo() {
        return jobCardNo;
    }

    public void setJobCardNo(String jobCardNo) {
        this.jobCardNo = jobCardNo;
    }

    public String getOutStandingDays() {
        return outStandingDays;
    }

    public void setOutStandingDays(String outStandingDays) {
        this.outStandingDays = outStandingDays;
    }

    public String getPlannedCompleteDays() {
        return plannedCompleteDays;
    }

    public void setPlannedCompleteDays(String plannedCompleteDays) {
        this.plannedCompleteDays = plannedCompleteDays;
    }

    public String getIssueType() {
        return issueType;
    }

    public void setIssueType(String issueType) {
        this.issueType = issueType;
    }

    public String getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(String closeDate) {
        this.closeDate = closeDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getMergingDate() {
        return mergingDate;
    }

    public void setMergingDate(String mergingDate) {
        this.mergingDate = mergingDate;
    }

    public String getSettledDate() {
        return settledDate;
    }

    public void setSettledDate(String settledDate) {
        this.settledDate = settledDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getHmReading() {
        return hmReading;
    }

    public void setHmReading(String hmReading) {
        this.hmReading = hmReading;
    }

    public String getHmReadingDate() {
        return hmReadingDate;
    }

    public void setHmReadingDate(String hmReadingDate) {
        this.hmReadingDate = hmReadingDate;
    }

    public String getKmReading() {
        return kmReading;
    }

    public void setKmReading(String kmReading) {
        this.kmReading = kmReading;
    }

    public String getKmReadingDate() {
        return kmReadingDate;
    }

    public void setKmReadingDate(String kmReadingDate) {
        this.kmReadingDate = kmReadingDate;
    }

    public String getVehicletype() {
        return vehicletype;
    }

    public void setVehicletype(String vehicletype) {
        this.vehicletype = vehicletype;
    }

    public String getTripMergingId() {
        return tripMergingId;
    }

    public void setTripMergingId(String tripMergingId) {
        this.tripMergingId = tripMergingId;
    }

    public String getTripExtraExpense() {
        return tripExtraExpense;
    }

    public void setTripExtraExpense(String tripExtraExpense) {
        this.tripExtraExpense = tripExtraExpense;
    }

    public String getEmptyExpense() {
        return emptyExpense;
    }

    public void setEmptyExpense(String emptyExpense) {
        this.emptyExpense = emptyExpense;
    }

    public String getEarnings() {
        return earnings;
    }

    public void setEarnings(String earnings) {
        this.earnings = earnings;
    }


    public String getLoadedExpense() {
        return loadedExpense;
    }

    public void setLoadedExpense(String loadedExpense) {
        this.loadedExpense = loadedExpense;
    }

    public String getOrderSequence() {
        return orderSequence;
    }

    public void setOrderSequence(String orderSequence) {
        this.orderSequence = orderSequence;
    }

    public String getExpenseDate() {
        return expenseDate;
    }

    public void setExpenseDate(String expenseDate) {
        this.expenseDate = expenseDate;
    }

    public String getExpenseDriverName() {
        return expenseDriverName;
    }

    public void setExpenseDriverName(String expenseDriverName) {
        this.expenseDriverName = expenseDriverName;
    }

    public String getExpenseName() {
        return expenseName;
    }

    public void setExpenseName(String expenseName) {
        this.expenseName = expenseName;
    }

    public String getExpenseRemarks() {
        return expenseRemarks;
    }

    public void setExpenseRemarks(String expenseRemarks) {
        this.expenseRemarks = expenseRemarks;
    }

    public String getAdvancePaidDate() {
        return advancePaidDate;
    }

    public void setAdvancePaidDate(String advancePaidDate) {
        this.advancePaidDate = advancePaidDate;
    }

    public String getAdvanceRemarks() {
        return advanceRemarks;
    }

    public void setAdvanceRemarks(String advanceRemarks) {
        this.advanceRemarks = advanceRemarks;
    }

    public String getExpenseType() {
        return expenseType;
    }

    public void setExpenseType(String expenseType) {
        this.expenseType = expenseType;
    }

    public String getPrimaryDriver() {
        return primaryDriver;
    }

    public void setPrimaryDriver(String primaryDriver) {
        this.primaryDriver = primaryDriver;
    }

    public String getSecondaryDriver() {
        return secondaryDriver;
    }

    public void setSecondaryDriver(String secondaryDriver) {
        this.secondaryDriver = secondaryDriver;
    }

    public String getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public String getActualExpense() {
        return actualExpense;
    }

    public void setActualExpense(String actualExpense) {
        this.actualExpense = actualExpense;
    }

    public String getDestinationReportingDateTime() {
        return destinationReportingDateTime;
    }

    public void setDestinationReportingDateTime(String destinationReportingDateTime) {
        this.destinationReportingDateTime = destinationReportingDateTime;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }

    public String getLoadingTransitHours() {
        return loadingTransitHours;
    }

    public void setLoadingTransitHours(String loadingTransitHours) {
        this.loadingTransitHours = loadingTransitHours;
    }

    public String getOriginReportingDateTime() {
        return originReportingDateTime;
    }

    public void setOriginReportingDateTime(String originReportingDateTime) {
        this.originReportingDateTime = originReportingDateTime;
    }

    public String getRcm() {
        return rcm;
    }

    public void setRcm(String rcm) {
        this.rcm = rcm;
    }

    public String getTotalRunHm() {
        return totalRunHm;
    }

    public void setTotalRunHm(String totalRunHm) {
        this.totalRunHm = totalRunHm;
    }

    public String getTotalRunKm() {
        return totalRunKm;
    }

    public void setTotalRunKm(String totalRunKm) {
        this.totalRunKm = totalRunKm;
    }

    public String getTransitHours() {
        return transitHours;
    }

    public void setTransitHours(String transitHours) {
        this.transitHours = transitHours;
    }

    public String getWflHours() {
        return wflHours;
    }

    public void setWflHours(String wflHours) {
        this.wflHours = wflHours;
    }

    public String getWfuHours() {
        return wfuHours;
    }

    public void setWfuHours(String wfuHours) {
        this.wfuHours = wfuHours;
    }

    public String getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(String paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getRequestAmount() {
        return requestAmount;
    }

    public void setRequestAmount(String requestAmount) {
        this.requestAmount = requestAmount;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getTripEndTime() {
        return tripEndTime;
    }

    public void setTripEndTime(String tripEndTime) {
        this.tripEndTime = tripEndTime;
    }

    public String getTripendtime() {
        return tripendtime;
    }

    public void setTripendtime(String tripendtime) {
        this.tripendtime = tripendtime;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }

    public String getTotalHms() {
        return totalHms;
    }

    public void setTotalHms(String totalHms) {
        this.totalHms = totalHms;
    }

    public String getWfuAmount() {
        return wfuAmount;
    }

    public void setWfuAmount(String wfuAmount) {
        this.wfuAmount = wfuAmount;
    }

    public String getEmptyTripKM() {
        return emptyTripKM;
    }

    public void setEmptyTripKM(String emptyTripKM) {
        this.emptyTripKM = emptyTripKM;
    }

    public String getAccountMgrId() {
        return accountMgrId;
    }

    public void setAccountMgrId(String accountMgrId) {
        this.accountMgrId = accountMgrId;
    }

    public String getFoodingAdvance() {
        return foodingAdvance;
    }

    public void setFoodingAdvance(String foodingAdvance) {
        this.foodingAdvance = foodingAdvance;
    }

    public String getRepairAdvance() {
        return repairAdvance;
    }

    public void setRepairAdvance(String repairAdvance) {
        this.repairAdvance = repairAdvance;
    }

    public String getRnmAdvance() {
        return rnmAdvance;
    }

    public void setRnmAdvance(String rnmAdvance) {
        this.rnmAdvance = rnmAdvance;
    }

    public String getChasisAmount() {
        return chasisAmount;
    }

    public void setChasisAmount(String chasisAmount) {
        this.chasisAmount = chasisAmount;
    }

    public String getContainerAmount() {
        return containerAmount;
    }

    public void setContainerAmount(String containerAmount) {
        this.containerAmount = containerAmount;
    }

    public String getReferAmount() {
        return referAmount;
    }

    public void setReferAmount(String referAmount) {
        this.referAmount = referAmount;
    }

    public String getChasisAmount2() {
        return chasisAmount2;
    }

    public void setChasisAmount2(String chasisAmount2) {
        this.chasisAmount2 = chasisAmount2;
    }

    public String getContainerAmount2() {
        return containerAmount2;
    }

    public void setContainerAmount2(String containerAmount1) {
        this.containerAmount2 = containerAmount1;
    }

    public String getReferAmount2() {
        return referAmount2;
    }

    public void setReferAmount2(String referAmount2) {
        this.referAmount2 = referAmount2;
    }

    public String getChasisAmount1() {
        return chasisAmount1;
    }

    public void setChasisAmount1(String chasisAmount1) {
        this.chasisAmount1 = chasisAmount1;
    }

    public String getContainerAmount1() {
        return containerAmount1;
    }

    public void setContainerAmount1(String containerAmount1) {
        this.containerAmount1 = containerAmount1;
    }

    public String getReferAmount1() {
        return referAmount1;
    }

    public void setReferAmount1(String referAmount1) {
        this.referAmount1 = referAmount1;
    }

    public String getChasisAmount3() {
        return chasisAmount3;
    }

    public void setChasisAmount3(String chasisAmount3) {
        this.chasisAmount3 = chasisAmount3;
    }

    public String getContainerAmount3() {
        return containerAmount3;
    }

    public void setContainerAmount3(String containerAmount3) {
        this.containerAmount3 = containerAmount3;
    }

    public String getReferAmount3() {
        return referAmount3;
    }

    public void setReferAmount3(String referAmount3) {
        this.referAmount3 = referAmount3;
    }

   

    public String getNewTyre() {
        return newTyre;
    }

    public void setNewTyre(String newTyre) {
        this.newTyre = newTyre;
    }

    public String getMonth1() {
        return month1;
    }

    public void setMonth1(String month1) {
        this.month1 = month1;
    }

    public String getMonth1New() {
        return month1New;
    }

    public void setMonth1New(String month1New) {
        this.month1New = month1New;
    }

    public String getMonth1ReTread() {
        return month1ReTread;
    }

    public void setMonth1ReTread(String month1ReTread) {
        this.month1ReTread = month1ReTread;
    }

    public String getMonth2() {
        return month2;
    }

    public void setMonth2(String month2) {
        this.month2 = month2;
    }

    public String getMonth2New() {
        return month2New;
    }

    public void setMonth2New(String month2New) {
        this.month2New = month2New;
    }

    public String getMonth2ReTread() {
        return month2ReTread;
    }

    public void setMonth2ReTread(String month2ReTread) {
        this.month2ReTread = month2ReTread;
    }

    public String getMonth3() {
        return month3;
    }

    public void setMonth3(String month3) {
        this.month3 = month3;
    }

    public String getMonth3New() {
        return month3New;
    }

    public void setMonth3New(String month3New) {
        this.month3New = month3New;
    }

    public String getMonth3ReTread() {
        return month3ReTread;
    }

    public void setMonth3ReTread(String month3ReTread) {
        this.month3ReTread = month3ReTread;
    }

    public String getYear1() {
        return year1;
    }

    public void setYear1(String year1) {
        this.year1 = year1;
    }

    public String getYear2() {
        return year2;
    }

    public void setYear2(String year2) {
        this.year2 = year2;
    }

    public String getYear3() {
        return year3;
    }

    public void setYear3(String year3) {
        this.year3 = year3;
    }

    public String getReTreadTyre() {
        return reTreadTyre;
    }

    public void setReTreadTyre(String reTreadTyre) {
        this.reTreadTyre = reTreadTyre;
    }

    public String getCommodity() {
        return commodity;
    }

    public void setCommodity(String commodity) {
        this.commodity = commodity;
    }

    public String getBlNumber() {
        return blNumber;
    }

    public void setBlNumber(String blNumber) {
        this.blNumber = blNumber;
    }

    public String getContainerNo() {
        return containerNo;
    }

    public void setContainerNo(String containerNo) {
        this.containerNo = containerNo;
    }

    public String getContainerSize() {
        return containerSize;
    }

    public void setContainerSize(String containerSize) {
        this.containerSize = containerSize;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getTrailerNo() {
        return trailerNo;
    }

    public void setTrailerNo(String trailerNo) {
        this.trailerNo = trailerNo;
    }

    public String getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }

    public String getBorder1() {
        return border1;
    }

    public void setBorder1(String border1) {
        this.border1 = border1;
    }

    public String getBorder1ArrivalDate() {
        return border1ArrivalDate;
    }

    public void setBorder1ArrivalDate(String border1ArrivalDate) {
        this.border1ArrivalDate = border1ArrivalDate;
    }

    public String getBorder2() {
        return border2;
    }

    public void setBorder2(String border2) {
        this.border2 = border2;
    }

    public String getBorder2ArrivalDate() {
        return border2ArrivalDate;
    }

    public void setBorder2ArrivalDate(String border2ArrivalDate) {
        this.border2ArrivalDate = border2ArrivalDate;
    }

    public String getClearingAgent() {
        return clearingAgent;
    }

    public void setClearingAgent(String clearingAgent) {
        this.clearingAgent = clearingAgent;
    }

    public String getUnLoadingDateTime() {
        return unLoadingDateTime;
    }

    public void setUnLoadingDateTime(String unLoadingDateTime) {
        this.unLoadingDateTime = unLoadingDateTime;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public String getRevenueProfitPercentage() {
        return revenueProfitPercentage;
    }

    public void setRevenueProfitPercentage(String revenueProfitPercentage) {
        this.revenueProfitPercentage = revenueProfitPercentage;
    }

    public String getExpenceProfitPercentage() {
        return expenceProfitPercentage;
    }

    public void setExpenceProfitPercentage(String expenceProfitPercentage) {
        this.expenceProfitPercentage = expenceProfitPercentage;
    }

    public String getTrailerId() {
        return trailerId;
    }

    public void setTrailerId(String trailerId) {
        this.trailerId = trailerId;
    }

    public String getTrailerRevenue() {
        return trailerRevenue;
    }

    public void setTrailerRevenue(String trailerRevenue) {
        this.trailerRevenue = trailerRevenue;
    }

    public String getTrailerExpence() {
        return trailerExpence;
    }

    public void setTrailerExpence(String trailerExpence) {
        this.trailerExpence = trailerExpence;
    }

    public String getTotalVehicle() {
        return totalVehicle;
    }

    public void setTotalVehicle(String totalVehicle) {
        this.totalVehicle = totalVehicle;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getDashboardopstrukCount() {
        return dashboardopstrukCount;
    }

    public void setDashboardopstrukCount(String dashboardopstrukCount) {
        this.dashboardopstrukCount = dashboardopstrukCount;
    }

    public String getActHrs() {
        return actHrs;
    }

    public void setActHrs(String actHrs) {
        this.actHrs = actHrs;
    }

    public String getEstHrs() {
        return estHrs;
    }

    public void setEstHrs(String estHrs) {
        this.estHrs = estHrs;
    }

    public String getBillingParty() {
        return billingParty;
    }

    public void setBillingParty(String billingParty) {
        this.billingParty = billingParty;
    }

    public String getDieselCost() {
        return dieselCost;
    }

    public void setDieselCost(String dieselCost) {
        this.dieselCost = dieselCost;
    }

    public String getFoodCost() {
        return foodCost;
    }

    public void setFoodCost(String foodCost) {
        this.foodCost = foodCost;
    }

    public String getGrDate() {
        return grDate;
    }

    public void setGrDate(String grDate) {
        this.grDate = grDate;
    }

    public String getGrNo() {
        return grNo;
    }

    public void setGrNo(String grNo) {
        this.grNo = grNo;
    }

    public String getSlipNo() {
        return slipNo;
    }

    public void setSlipNo(String slipNo) {
        this.slipNo = slipNo;
    }

    public String getTripExpenseSNo() {
        return tripExpenseSNo;
    }

    public void setTripExpenseSNo(String tripExpenseSNo) {
        this.tripExpenseSNo = tripExpenseSNo;
    }

    public String getVoucherNo() {
        return voucherNo;
    }

    public void setVoucherNo(String voucherNo) {
        this.voucherNo = voucherNo;
    }

    public String getExpesneDate() {
        return expesneDate;
    }

    public void setExpesneDate(String expesneDate) {
        this.expesneDate = expesneDate;
    }

    public List getSelectionList() {
        return SelectionList;
    }

    public void setSelectionList(List SelectionList) {
        this.SelectionList = SelectionList;
    }

    public String getActivityDateTime() {
        return activityDateTime;
    }

    public void setActivityDateTime(String activityDateTime) {
        this.activityDateTime = activityDateTime;
    }

    public String getAicmaprPercent() {
        return aicmaprPercent;
    }

    public void setAicmaprPercent(String aicmaprPercent) {
        this.aicmaprPercent = aicmaprPercent;
    }

    public String getAicmbidDiscPercent() {
        return aicmbidDiscPercent;
    }

    public void setAicmbidDiscPercent(String aicmbidDiscPercent) {
        this.aicmbidDiscPercent = aicmbidDiscPercent;
    }

    public String getAicmbidDiscValue() {
        return aicmbidDiscValue;
    }

    public void setAicmbidDiscValue(String aicmbidDiscValue) {
        this.aicmbidDiscValue = aicmbidDiscValue;
    }

    public String getAicmbidType() {
        return aicmbidType;
    }

    public void setAicmbidType(String aicmbidType) {
        this.aicmbidType = aicmbidType;
    }

    public String getAicmcatInvoiceValue() {
        return aicmcatInvoiceValue;
    }

    public void setAicmcatInvoiceValue(String aicmcatInvoiceValue) {
        this.aicmcatInvoiceValue = aicmcatInvoiceValue;
    }

    public String getAssignedfunc() {
        return assignedfunc;
    }

    public void setAssignedfunc(String assignedfunc) {
        this.assignedfunc = assignedfunc;
    }

    public String getAuctionRefernceNo() {
        return auctionRefernceNo;
    }

    public void setAuctionRefernceNo(String auctionRefernceNo) {
        this.auctionRefernceNo = auctionRefernceNo;
    }

    public String getBillOfEntry() {
        return billOfEntry;
    }

    public void setBillOfEntry(String billOfEntry) {
        this.billOfEntry = billOfEntry;
    }

    public String getBillOfEntryOld() {
        return billOfEntryOld;
    }

    public void setBillOfEntryOld(String billOfEntryOld) {
        this.billOfEntryOld = billOfEntryOld;
    }

    public String getBillingPartyIdNew() {
        return billingPartyIdNew;
    }

    public void setBillingPartyIdNew(String billingPartyIdNew) {
        this.billingPartyIdNew = billingPartyIdNew;
    }

    public String getBillingPartyIdOld() {
        return billingPartyIdOld;
    }

    public void setBillingPartyIdOld(String billingPartyIdOld) {
        this.billingPartyIdOld = billingPartyIdOld;
    }

    public String getBlockedGr() {
        return blockedGr;
    }

    public void setBlockedGr(String blockedGr) {
        this.blockedGr = blockedGr;
    }

    public String getCancelledGr() {
        return cancelledGr;
    }

    public void setCancelledGr(String cancelledGr) {
        this.cancelledGr = cancelledGr;
    }

    public String getChallanNo() {
        return challanNo;
    }

    public void setChallanNo(String challanNo) {
        this.challanNo = challanNo;
    }

    public String getColumnDataTypeName() {
        return columnDataTypeName;
    }

    public void setColumnDataTypeName(String columnDataTypeName) {
        this.columnDataTypeName = columnDataTypeName;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnname() {
        return columnname;
    }

    public void setColumnname(String columnname) {
        this.columnname = columnname;
    }

    public String getCombinedTableCondition() {
        return combinedTableCondition;
    }

    public void setCombinedTableCondition(String combinedTableCondition) {
        this.combinedTableCondition = combinedTableCondition;
    }

    public String getCombinedTableName() {
        return combinedTableName;
    }

    public void setCombinedTableName(String combinedTableName) {
        this.combinedTableName = combinedTableName;
    }

    public int getCompanyTypeId() {
        return companyTypeId;
    }

    public void setCompanyTypeId(int companyTypeId) {
        this.companyTypeId = companyTypeId;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getConditionColumnName() {
        return conditionColumnName;
    }

    public void setConditionColumnName(String conditionColumnName) {
        this.conditionColumnName = conditionColumnName;
    }

    public List getConditionList() {
        return conditionList;
    }

    public void setConditionList(List conditionList) {
        this.conditionList = conditionList;
    }

    public String getConditionName() {
        return conditionName;
    }

    public void setConditionName(String conditionName) {
        this.conditionName = conditionName;
    }

    public String getConditionTableColumnName() {
        return conditionTableColumnName;
    }

    public void setConditionTableColumnName(String conditionTableColumnName) {
        this.conditionTableColumnName = conditionTableColumnName;
    }

    public String getConditionTableName() {
        return conditionTableName;
    }

    public void setConditionTableName(String conditionTableName) {
        this.conditionTableName = conditionTableName;
    }

    public String getConsignmentDate() {
        return consignmentDate;
    }

    public void setConsignmentDate(String consignmentDate) {
        this.consignmentDate = consignmentDate;
    }

    public String getConsignmentOrderNo() {
        return consignmentOrderNo;
    }

    public void setConsignmentOrderNo(String consignmentOrderNo) {
        this.consignmentOrderNo = consignmentOrderNo;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public void setConsignorName(String consignorName) {
        this.consignorName = consignorName;
    }

    public String getContainer1() {
        return container1;
    }

    public void setContainer1(String container1) {
        this.container1 = container1;
    }

    public String getContainer2() {
        return container2;
    }

    public void setContainer2(String container2) {
        this.container2 = container2;
    }

    public String getContainerNoOld() {
        return containerNoOld;
    }

    public void setContainerNoOld(String containerNoOld) {
        this.containerNoOld = containerNoOld;
    }

    public String getContainerQty() {
        return containerQty;
    }

    public void setContainerQty(String containerQty) {
        this.containerQty = containerQty;
    }

    public String getCustCode() {
        return custCode;
    }

    public void setCustCode(String custCode) {
        this.custCode = custCode;
    }

    public String getCustomerReferenceId() {
        return customerReferenceId;
    }

    public void setCustomerReferenceId(String customerReferenceId) {
        this.customerReferenceId = customerReferenceId;
    }

    public String getDetenTion() {
        return detenTion;
    }

    public void setDetenTion(String detenTion) {
        this.detenTion = detenTion;
    }

    public String getDetentionCharge() {
        return detentionCharge;
    }

    public void setDetentionCharge(String detentionCharge) {
        this.detentionCharge = detentionCharge;
    }

    public String getDetentionChargeOld() {
        return detentionChargeOld;
    }

    public void setDetentionChargeOld(String detentionChargeOld) {
        this.detentionChargeOld = detentionChargeOld;
    }

    public String getDetentionErpId() {
        return detentionErpId;
    }

    public void setDetentionErpId(String detentionErpId) {
        this.detentionErpId = detentionErpId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public List getEditEntityDetails() {
        return editEntityDetails;
    }

    public void setEditEntityDetails(List editEntityDetails) {
        this.editEntityDetails = editEntityDetails;
    }

    public String getEmailAttach() {
        return emailAttach;
    }

    public void setEmailAttach(String emailAttach) {
        this.emailAttach = emailAttach;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getEmptyPickup() {
        return emptyPickup;
    }

    public void setEmptyPickup(String emptyPickup) {
        this.emptyPickup = emptyPickup;
    }

    public int getEntityColDetailsId() {
        return entityColDetailsId;
    }

    public void setEntityColDetailsId(int entityColDetailsId) {
        this.entityColDetailsId = entityColDetailsId;
    }

    public String getEntityColumnName() {
        return entityColumnName;
    }

    public void setEntityColumnName(String entityColumnName) {
        this.entityColumnName = entityColumnName;
    }

    public String getEntityDataType() {
        return entityDataType;
    }

    public void setEntityDataType(String entityDataType) {
        this.entityDataType = entityDataType;
    }

    public String getEntityDisplayName() {
        return entityDisplayName;
    }

    public void setEntityDisplayName(String entityDisplayName) {
        this.entityDisplayName = entityDisplayName;
    }

    public int getEntityId() {
        return entityId;
    }

    public void setEntityId(int entityId) {
        this.entityId = entityId;
    }

    public int getEntityIdqm() {
        return entityIdqm;
    }

    public void setEntityIdqm(int entityIdqm) {
        this.entityIdqm = entityIdqm;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getEntityNameList() {
        return entityNameList;
    }

    public void setEntityNameList(String entityNameList) {
        this.entityNameList = entityNameList;
    }

    public String getEntityParseValue() {
        return entityParseValue;
    }

    public void setEntityParseValue(String entityParseValue) {
        this.entityParseValue = entityParseValue;
    }

    public String getEntityTableName() {
        return entityTableName;
    }

    public void setEntityTableName(String entityTableName) {
        this.entityTableName = entityTableName;
    }

    public String getExecutionQuery() {
        return executionQuery;
    }

    public void setExecutionQuery(String executionQuery) {
        this.executionQuery = executionQuery;
    }

    public String getFileExtn() {
        return fileExtn;
    }

    public void setFileExtn(String fileExtn) {
        this.fileExtn = fileExtn;
    }

    public String getFileextension() {
        return fileextension;
    }

    public void setFileextension(String fileextension) {
        this.fileextension = fileextension;
    }

    public String getFreightAmountErpId() {
        return freightAmountErpId;
    }

    public void setFreightAmountErpId(String freightAmountErpId) {
        this.freightAmountErpId = freightAmountErpId;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public String getGpsSimNo() {
        return gpsSimNo;
    }

    public void setGpsSimNo(String gpsSimNo) {
        this.gpsSimNo = gpsSimNo;
    }

    public String getGrNumber() {
        return grNumber;
    }

    public void setGrNumber(String grNumber) {
        this.grNumber = grNumber;
    }

    public String getGreenErpId() {
        return greenErpId;
    }

    public void setGreenErpId(String greenErpId) {
        this.greenErpId = greenErpId;
    }

    public String getGreenTax() {
        return greenTax;
    }

    public void setGreenTax(String greenTax) {
        this.greenTax = greenTax;
    }

    public String getGreenTaxOld() {
        return greenTaxOld;
    }

    public void setGreenTaxOld(String greenTaxOld) {
        this.greenTaxOld = greenTaxOld;
    }

    public String getIdleTime() {
        return idleTime;
    }

    public void setIdleTime(String idleTime) {
        this.idleTime = idleTime;
    }

    public String getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(String invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public int getLastInsertId() {
        return lastInsertId;
    }

    public void setLastInsertId(int lastInsertId) {
        this.lastInsertId = lastInsertId;
    }

    public String getLinerName() {
        return linerName;
    }

    public void setLinerName(String linerName) {
        this.linerName = linerName;
    }

    public String getLoadTypeName() {
        return loadTypeName;
    }

    public void setLoadTypeName(String loadTypeName) {
        this.loadTypeName = loadTypeName;
    }

    public String getLoadedTrip() {
        return loadedTrip;
    }

    public void setLoadedTrip(String loadedTrip) {
        this.loadedTrip = loadedTrip;
    }

    public String getLoginDuration() {
        return loginDuration;
    }

    public void setLoginDuration(String loginDuration) {
        this.loginDuration = loginDuration;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(String modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public String getMonthId() {
        return monthId;
    }

    public void setMonthId(String monthId) {
        this.monthId = monthId;
    }

    public String getMovementType() {
        return movementType;
    }

    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    public String getNoOfLogins() {
        return noOfLogins;
    }

    public void setNoOfLogins(String noOfLogins) {
        this.noOfLogins = noOfLogins;
    }

    public String getNotUsedGr() {
        return notUsedGr;
    }

    public void setNotUsedGr(String notUsedGr) {
        this.notUsedGr = notUsedGr;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }

    public String getOperatorValue() {
        return operatorValue;
    }

    public void setOperatorValue(String operatorValue) {
        this.operatorValue = operatorValue;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getOtherExpense() {
        return otherExpense;
    }

    public void setOtherExpense(String otherExpense) {
        this.otherExpense = otherExpense;
    }

    public String getOtherExpenseOld() {
        return otherExpenseOld;
    }

    public void setOtherExpenseOld(String otherExpenseOld) {
        this.otherExpenseOld = otherExpenseOld;
    }

    public String getPaidCash() {
        return paidCash;
    }

    public void setPaidCash(String paidCash) {
        this.paidCash = paidCash;
    }

    public String getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(String paidDate) {
        this.paidDate = paidDate;
    }

    public String getPaidExpense() {
        return paidExpense;
    }

    public void setPaidExpense(String paidExpense) {
        this.paidExpense = paidExpense;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getPlannedStatus() {
        return plannedStatus;
    }

    public void setPlannedStatus(String plannedStatus) {
        this.plannedStatus = plannedStatus;
    }

    public String getqBAggregateFunction() {
        return qBAggregateFunction;
    }

    public void setqBAggregateFunction(String qBAggregateFunction) {
        this.qBAggregateFunction = qBAggregateFunction;
    }

    public String getqBClosingSyntax() {
        return qBClosingSyntax;
    }

    public void setqBClosingSyntax(String qBClosingSyntax) {
        this.qBClosingSyntax = qBClosingSyntax;
    }

    public String getqBOpenSyntax() {
        return qBOpenSyntax;
    }

    public void setqBOpenSyntax(String qBOpenSyntax) {
        this.qBOpenSyntax = qBOpenSyntax;
    }

    public int getqBOuterQuery() {
        return qBOuterQuery;
    }

    public void setqBOuterQuery(int qBOuterQuery) {
        this.qBOuterQuery = qBOuterQuery;
    }

    public String getqBfilterFunction() {
        return qBfilterFunction;
    }

    public void setqBfilterFunction(String qBfilterFunction) {
        this.qBfilterFunction = qBfilterFunction;
    }

    public String getqBfilterFunctionEnd() {
        return qBfilterFunctionEnd;
    }

    public void setqBfilterFunctionEnd(String qBfilterFunctionEnd) {
        this.qBfilterFunctionEnd = qBfilterFunctionEnd;
    }

    public int getQueryId() {
        return queryId;
    }

    public void setQueryId(int queryId) {
        this.queryId = queryId;
    }

    public int getQueryMasterEntityId() {
        return queryMasterEntityId;
    }

    public void setQueryMasterEntityId(int queryMasterEntityId) {
        this.queryMasterEntityId = queryMasterEntityId;
    }

    public String getQueryName() {
        return queryName;
    }

    public void setQueryName(String queryName) {
        this.queryName = queryName;
    }

    public String getRateWithReeferNew() {
        return rateWithReeferNew;
    }

    public void setRateWithReeferNew(String rateWithReeferNew) {
        this.rateWithReeferNew = rateWithReeferNew;
    }

    public String getRateWithReeferOld() {
        return rateWithReeferOld;
    }

    public void setRateWithReeferOld(String rateWithReeferOld) {
        this.rateWithReeferOld = rateWithReeferOld;
    }

    public String getRateWithoutReeferNew() {
        return rateWithoutReeferNew;
    }

    public void setRateWithoutReeferNew(String rateWithoutReeferNew) {
        this.rateWithoutReeferNew = rateWithoutReeferNew;
    }

    public String getRateWithoutReeferOld() {
        return rateWithoutReeferOld;
    }

    public void setRateWithoutReeferOld(String rateWithoutReeferOld) {
        this.rateWithoutReeferOld = rateWithoutReeferOld;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getRevenueOtherAmount() {
        return revenueOtherAmount;
    }

    public void setRevenueOtherAmount(String revenueOtherAmount) {
        this.revenueOtherAmount = revenueOtherAmount;
    }

    public String getRevenueWeightmentAmount() {
        return revenueWeightmentAmount;
    }

    public void setRevenueWeightmentAmount(String revenueWeightmentAmount) {
        this.revenueWeightmentAmount = revenueWeightmentAmount;
    }

    public String getSchedularDateTime() {
        return schedularDateTime;
    }

    public void setSchedularDateTime(String schedularDateTime) {
        this.schedularDateTime = schedularDateTime;
    }

    public int getSchedularOption() {
        return schedularOption;
    }

    public void setSchedularOption(int schedularOption) {
        this.schedularOption = schedularOption;
    }

    public String getScheduleEmailEnd() {
        return scheduleEmailEnd;
    }

    public void setScheduleEmailEnd(String scheduleEmailEnd) {
        this.scheduleEmailEnd = scheduleEmailEnd;
    }

    public String getScheduleEmailStart() {
        return scheduleEmailStart;
    }

    public void setScheduleEmailStart(String scheduleEmailStart) {
        this.scheduleEmailStart = scheduleEmailStart;
    }

    public String getSelectquery() {
        return selectquery;
    }

    public void setSelectquery(String selectquery) {
        this.selectquery = selectquery;
    }

    public String getShipBillNo() {
        return shipBillNo;
    }

    public void setShipBillNo(String shipBillNo) {
        this.shipBillNo = shipBillNo;
    }

    public String getShipBillNoOld() {
        return shipBillNoOld;
    }

    public void setShipBillNoOld(String shipBillNoOld) {
        this.shipBillNoOld = shipBillNoOld;
    }

    public String getStatusLink() {
        return statusLink;
    }

    public void setStatusLink(String statusLink) {
        this.statusLink = statusLink;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getSummaryName() {
        return summaryName;
    }

    public void setSummaryName(String summaryName) {
        this.summaryName = summaryName;
    }

    public String getSummaryValue() {
        return summaryValue;
    }

    public void setSummaryValue(String summaryValue) {
        this.summaryValue = summaryValue;
    }

    public String getTabStatus() {
        return tabStatus;
    }

    public void setTabStatus(String tabStatus) {
        this.tabStatus = tabStatus;
    }

    public String getTabelsinPriorityVendor() {
        return tabelsinPriorityVendor;
    }

    public void setTabelsinPriorityVendor(String tabelsinPriorityVendor) {
        this.tabelsinPriorityVendor = tabelsinPriorityVendor;
    }

    public String getTableColumnName() {
        return tableColumnName;
    }

    public void setTableColumnName(String tableColumnName) {
        this.tableColumnName = tableColumnName;
    }

    public List getTableList() {
        return tableList;
    }

    public void setTableList(List tableList) {
        this.tableList = tableList;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTollErpId() {
        return tollErpId;
    }

    public void setTollErpId(String tollErpId) {
        this.tollErpId = tollErpId;
    }

    public String getTollTax() {
        return tollTax;
    }

    public void setTollTax(String tollTax) {
        this.tollTax = tollTax;
    }

    public String getTollTaxOld() {
        return tollTaxOld;
    }

    public void setTollTaxOld(String tollTaxOld) {
        this.tollTaxOld = tollTaxOld;
    }

    public String getTotBlockedGr() {
        return totBlockedGr;
    }

    public void setTotBlockedGr(String totBlockedGr) {
        this.totBlockedGr = totBlockedGr;
    }

    public String getTotCancelledGr() {
        return totCancelledGr;
    }

    public void setTotCancelledGr(String totCancelledGr) {
        this.totCancelledGr = totCancelledGr;
    }

    public String getTotNotUsedGr() {
        return totNotUsedGr;
    }

    public void setTotNotUsedGr(String totNotUsedGr) {
        this.totNotUsedGr = totNotUsedGr;
    }

    public String getTotUsedGr() {
        return totUsedGr;
    }

    public void setTotUsedGr(String totUsedGr) {
        this.totUsedGr = totUsedGr;
    }

    public String getTransporter() {
        return transporter;
    }

    public void setTransporter(String transporter) {
        this.transporter = transporter;
    }

    public String getTripIds() {
        return tripIds;
    }

    public void setTripIds(String tripIds) {
        this.tripIds = tripIds;
    }

    public String getTripStatusName() {
        return tripStatusName;
    }

    public void setTripStatusName(String tripStatusName) {
        this.tripStatusName = tripStatusName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public List getUpdatedEntityDetails() {
        return updatedEntityDetails;
    }

    public void setUpdatedEntityDetails(List updatedEntityDetails) {
        this.updatedEntityDetails = updatedEntityDetails;
    }

    public String getUsedGr() {
        return usedGr;
    }

    public void setUsedGr(String usedGr) {
        this.usedGr = usedGr;
    }

    public String getUserColumnValue() {
        return userColumnValue;
    }

    public void setUserColumnValue(String userColumnValue) {
        this.userColumnValue = userColumnValue;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserValue() {
        return userValue;
    }

    public void setUserValue(String userValue) {
        this.userValue = userValue;
    }

    public String getValidStatusNew() {
        return validStatusNew;
    }

    public void setValidStatusNew(String validStatusNew) {
        this.validStatusNew = validStatusNew;
    }

    public String getValidStatusOld() {
        return validStatusOld;
    }

    public void setValidStatusOld(String validStatusOld) {
        this.validStatusOld = validStatusOld;
    }

    public String getVehicleStatus() {
        return vehicleStatus;
    }

    public void setVehicleStatus(String vehicleStatus) {
        this.vehicleStatus = vehicleStatus;
    }

    public String getWeightment() {
        return weightment;
    }

    public void setWeightment(String weightment) {
        this.weightment = weightment;
    }

    public String getWeightmentErpId() {
        return weightmentErpId;
    }

    public void setWeightmentErpId(String weightmentErpId) {
        this.weightmentErpId = weightmentErpId;
    }

    public String getWeightmentOld() {
        return weightmentOld;
    }

    public void setWeightmentOld(String weightmentOld) {
        this.weightmentOld = weightmentOld;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getContainerType() {
        return containerType;
    }

    public void setContainerType(String containerType) {
        this.containerType = containerType;
    }

    public String getEntityDetailsId() {
        return entityDetailsId;
    }

    public void setEntityDetailsId(String entityDetailsId) {
        this.entityDetailsId = entityDetailsId;
    }

    public String getExecutionInterval() {
        return executionInterval;
    }

    public void setExecutionInterval(String executionInterval) {
        this.executionInterval = executionInterval;
    }

    public String getLinerNameId() {
        return linerNameId;
    }

    public void setLinerNameId(String linerNameId) {
        this.linerNameId = linerNameId;
    }

    public String getOrderTypeId() {
        return orderTypeId;
    }

    public void setOrderTypeId(String orderTypeId) {
        this.orderTypeId = orderTypeId;
    }

    public String getFilterentityDisplayName() {
        return filterentityDisplayName;
    }

    public void setFilterentityDisplayName(String filterentityDisplayName) {
        this.filterentityDisplayName = filterentityDisplayName;
    }

    public String getAicmCategoryName() {
        return aicmCategoryName;
    }

    public void setAicmCategoryName(String aicmCategoryName) {
        this.aicmCategoryName = aicmCategoryName;
    }

    public int getAicmSupplierId() {
        return aicmSupplierId;
    }

    public void setAicmSupplierId(int aicmSupplierId) {
        this.aicmSupplierId = aicmSupplierId;
    }

    public String getAicmsid() {
        return aicmsid;
    }

    public void setAicmsid(String aicmsid) {
        this.aicmsid = aicmsid;
    }

    public String getReportDescription() {
        return reportDescription;
    }

    public void setReportDescription(String reportDescription) {
        this.reportDescription = reportDescription;
    }

    public String getWeekDays() {
        return weekDays;
    }

    public void setWeekDays(String weekDays) {
        this.weekDays = weekDays;
    }

    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public String getBehatiAmount() {
        return behatiAmount;
    }

    public void setBehatiAmount(String behatiAmount) {
        this.behatiAmount = behatiAmount;
    }

    public String getDetaintionAmount() {
        return detaintionAmount;
    }

    public void setDetaintionAmount(String detaintionAmount) {
        this.detaintionAmount = detaintionAmount;
    }

    public String getDieselQty() {
        return dieselQty;
    }

    public void setDieselQty(String dieselQty) {
        this.dieselQty = dieselQty;
    }

    public String getFoodingAmount() {
        return foodingAmount;
    }

    public void setFoodingAmount(String foodingAmount) {
        this.foodingAmount = foodingAmount;
    }

    public String getFortyFT() {
        return fortyFT;
    }

    public void setFortyFT(String fortyFT) {
        this.fortyFT = fortyFT;
    }

    public String getLeaseddadriTKDImportfourtyft() {
        return leaseddadriTKDImportfourtyft;
    }

    public void setLeaseddadriTKDImportfourtyft(String leaseddadriTKDImportfourtyft) {
        this.leaseddadriTKDImportfourtyft = leaseddadriTKDImportfourtyft;
    }

    public String getLeaseddadriTKDImporttwentyft() {
        return leaseddadriTKDImporttwentyft;
    }

    public void setLeaseddadriTKDImporttwentyft(String leaseddadriTKDImporttwentyft) {
        this.leaseddadriTKDImporttwentyft = leaseddadriTKDImporttwentyft;
    }

    public String getLeaseddadriTKDemptyfourtyft() {
        return leaseddadriTKDemptyfourtyft;
    }

    public void setLeaseddadriTKDemptyfourtyft(String leaseddadriTKDemptyfourtyft) {
        this.leaseddadriTKDemptyfourtyft = leaseddadriTKDemptyfourtyft;
    }

    public String getLeaseddadriTKDemptytwentyft() {
        return leaseddadriTKDemptytwentyft;
    }

    public void setLeaseddadriTKDemptytwentyft(String leaseddadriTKDemptytwentyft) {
        this.leaseddadriTKDemptytwentyft = leaseddadriTKDemptytwentyft;
    }

    public String getLeaseddadriTKDexportfourtyft() {
        return leaseddadriTKDexportfourtyft;
    }

    public void setLeaseddadriTKDexportfourtyft(String leaseddadriTKDexportfourtyft) {
        this.leaseddadriTKDexportfourtyft = leaseddadriTKDexportfourtyft;
    }

    public String getLeaseddadriTKDexporttwentyft() {
        return leaseddadriTKDexporttwentyft;
    }

    public void setLeaseddadriTKDexporttwentyft(String leaseddadriTKDexporttwentyft) {
        this.leaseddadriTKDexporttwentyft = leaseddadriTKDexporttwentyft;
    }

    public String getLeaseddictImportfourtyft() {
        return leaseddictImportfourtyft;
    }

    public void setLeaseddictImportfourtyft(String leaseddictImportfourtyft) {
        this.leaseddictImportfourtyft = leaseddictImportfourtyft;
    }

    public String getLeaseddictImporttwentyft() {
        return leaseddictImporttwentyft;
    }

    public void setLeaseddictImporttwentyft(String leaseddictImporttwentyft) {
        this.leaseddictImporttwentyft = leaseddictImporttwentyft;
    }

    public String getLeaseddictemptyfourtyft() {
        return leaseddictemptyfourtyft;
    }

    public void setLeaseddictemptyfourtyft(String leaseddictemptyfourtyft) {
        this.leaseddictemptyfourtyft = leaseddictemptyfourtyft;
    }

    public String getLeaseddictemptytwentyft() {
        return leaseddictemptytwentyft;
    }

    public void setLeaseddictemptytwentyft(String leaseddictemptytwentyft) {
        this.leaseddictemptytwentyft = leaseddictemptytwentyft;
    }

    public String getLeaseddictexportfourtyft() {
        return leaseddictexportfourtyft;
    }

    public void setLeaseddictexportfourtyft(String leaseddictexportfourtyft) {
        this.leaseddictexportfourtyft = leaseddictexportfourtyft;
    }

    public String getLeaseddictexporttwentyft() {
        return leaseddictexporttwentyft;
    }

    public void setLeaseddictexporttwentyft(String leaseddictexporttwentyft) {
        this.leaseddictexporttwentyft = leaseddictexporttwentyft;
    }

    public String getLeasedloniImportfourtyft() {
        return leasedloniImportfourtyft;
    }

    public void setLeasedloniImportfourtyft(String leasedloniImportfourtyft) {
        this.leasedloniImportfourtyft = leasedloniImportfourtyft;
    }

    public String getLeasedloniImporttwentyft() {
        return leasedloniImporttwentyft;
    }

    public void setLeasedloniImporttwentyft(String leasedloniImporttwentyft) {
        this.leasedloniImporttwentyft = leasedloniImporttwentyft;
    }

    public String getLeasedloniemptyfourtyft() {
        return leasedloniemptyfourtyft;
    }

    public void setLeasedloniemptyfourtyft(String leasedloniemptyfourtyft) {
        this.leasedloniemptyfourtyft = leasedloniemptyfourtyft;
    }

    public String getLeasedloniemptytwentyft() {
        return leasedloniemptytwentyft;
    }

    public void setLeasedloniemptytwentyft(String leasedloniemptytwentyft) {
        this.leasedloniemptytwentyft = leasedloniemptytwentyft;
    }

    public String getLeasedloniexportfourtyft() {
        return leasedloniexportfourtyft;
    }

    public void setLeasedloniexportfourtyft(String leasedloniexportfourtyft) {
        this.leasedloniexportfourtyft = leasedloniexportfourtyft;
    }

    public String getLeasedloniexporttwentyft() {
        return leasedloniexporttwentyft;
    }

    public void setLeasedloniexporttwentyft(String leasedloniexporttwentyft) {
        this.leasedloniexporttwentyft = leasedloniexporttwentyft;
    }

    public String getOtherdadriTKDImportfourtyft() {
        return otherdadriTKDImportfourtyft;
    }

    public void setOtherdadriTKDImportfourtyft(String otherdadriTKDImportfourtyft) {
        this.otherdadriTKDImportfourtyft = otherdadriTKDImportfourtyft;
    }

    public String getOtherdadriTKDImporttwentyft() {
        return otherdadriTKDImporttwentyft;
    }

    public void setOtherdadriTKDImporttwentyft(String otherdadriTKDImporttwentyft) {
        this.otherdadriTKDImporttwentyft = otherdadriTKDImporttwentyft;
    }

    public String getOtherdadriTKDemptyfourtyft() {
        return otherdadriTKDemptyfourtyft;
    }

    public void setOtherdadriTKDemptyfourtyft(String otherdadriTKDemptyfourtyft) {
        this.otherdadriTKDemptyfourtyft = otherdadriTKDemptyfourtyft;
    }

    public String getOtherdadriTKDemptytwentyft() {
        return otherdadriTKDemptytwentyft;
    }

    public void setOtherdadriTKDemptytwentyft(String otherdadriTKDemptytwentyft) {
        this.otherdadriTKDemptytwentyft = otherdadriTKDemptytwentyft;
    }

    public String getOtherdadriTKDexportfourtyft() {
        return otherdadriTKDexportfourtyft;
    }

    public void setOtherdadriTKDexportfourtyft(String otherdadriTKDexportfourtyft) {
        this.otherdadriTKDexportfourtyft = otherdadriTKDexportfourtyft;
    }

    public String getOtherdadriTKDexporttwentyft() {
        return otherdadriTKDexporttwentyft;
    }

    public void setOtherdadriTKDexporttwentyft(String otherdadriTKDexporttwentyft) {
        this.otherdadriTKDexporttwentyft = otherdadriTKDexporttwentyft;
    }

    public String getOtherdictImportfourtyft() {
        return otherdictImportfourtyft;
    }

    public void setOtherdictImportfourtyft(String otherdictImportfourtyft) {
        this.otherdictImportfourtyft = otherdictImportfourtyft;
    }

    public String getOtherdictImporttwentyft() {
        return otherdictImporttwentyft;
    }

    public void setOtherdictImporttwentyft(String otherdictImporttwentyft) {
        this.otherdictImporttwentyft = otherdictImporttwentyft;
    }

    public String getOtherdictemptyfourtyft() {
        return otherdictemptyfourtyft;
    }

    public void setOtherdictemptyfourtyft(String otherdictemptyfourtyft) {
        this.otherdictemptyfourtyft = otherdictemptyfourtyft;
    }

    public String getOtherdictemptytwentyft() {
        return otherdictemptytwentyft;
    }

    public void setOtherdictemptytwentyft(String otherdictemptytwentyft) {
        this.otherdictemptytwentyft = otherdictemptytwentyft;
    }

    public String getOtherdictexportfourtyft() {
        return otherdictexportfourtyft;
    }

    public void setOtherdictexportfourtyft(String otherdictexportfourtyft) {
        this.otherdictexportfourtyft = otherdictexportfourtyft;
    }

    public String getOtherdictexporttwentyft() {
        return otherdictexporttwentyft;
    }

    public void setOtherdictexporttwentyft(String otherdictexporttwentyft) {
        this.otherdictexporttwentyft = otherdictexporttwentyft;
    }

    public String getOtherloniImportfourtyft() {
        return otherloniImportfourtyft;
    }

    public void setOtherloniImportfourtyft(String otherloniImportfourtyft) {
        this.otherloniImportfourtyft = otherloniImportfourtyft;
    }

    public String getOtherloniImporttwentyft() {
        return otherloniImporttwentyft;
    }

    public void setOtherloniImporttwentyft(String otherloniImporttwentyft) {
        this.otherloniImporttwentyft = otherloniImporttwentyft;
    }

    public String getOtherloniemptyfourtyft() {
        return otherloniemptyfourtyft;
    }

    public void setOtherloniemptyfourtyft(String otherloniemptyfourtyft) {
        this.otherloniemptyfourtyft = otherloniemptyfourtyft;
    }

    public String getOtherloniemptytwentyft() {
        return otherloniemptytwentyft;
    }

    public void setOtherloniemptytwentyft(String otherloniemptytwentyft) {
        this.otherloniemptytwentyft = otherloniemptytwentyft;
    }

    public String getOtherloniexportfourtyft() {
        return otherloniexportfourtyft;
    }

    public void setOtherloniexportfourtyft(String otherloniexportfourtyft) {
        this.otherloniexportfourtyft = otherloniexportfourtyft;
    }

    public String getOtherloniexporttwentyft() {
        return otherloniexporttwentyft;
    }

    public void setOtherloniexporttwentyft(String otherloniexporttwentyft) {
        this.otherloniexporttwentyft = otherloniexporttwentyft;
    }

    public String getOwndadriTKDImportfourtyft() {
        return owndadriTKDImportfourtyft;
    }

    public void setOwndadriTKDImportfourtyft(String owndadriTKDImportfourtyft) {
        this.owndadriTKDImportfourtyft = owndadriTKDImportfourtyft;
    }

    public String getOwndadriTKDImporttwentyft() {
        return owndadriTKDImporttwentyft;
    }

    public void setOwndadriTKDImporttwentyft(String owndadriTKDImporttwentyft) {
        this.owndadriTKDImporttwentyft = owndadriTKDImporttwentyft;
    }

    public String getOwndadriTKDemptyfourtyft() {
        return owndadriTKDemptyfourtyft;
    }

    public void setOwndadriTKDemptyfourtyft(String owndadriTKDemptyfourtyft) {
        this.owndadriTKDemptyfourtyft = owndadriTKDemptyfourtyft;
    }

    public String getOwndadriTKDemptytwentyft() {
        return owndadriTKDemptytwentyft;
    }

    public void setOwndadriTKDemptytwentyft(String owndadriTKDemptytwentyft) {
        this.owndadriTKDemptytwentyft = owndadriTKDemptytwentyft;
    }

    public String getOwndadriTKDexportfourtyft() {
        return owndadriTKDexportfourtyft;
    }

    public void setOwndadriTKDexportfourtyft(String owndadriTKDexportfourtyft) {
        this.owndadriTKDexportfourtyft = owndadriTKDexportfourtyft;
    }

    public String getOwndadriTKDexporttwentyft() {
        return owndadriTKDexporttwentyft;
    }

    public void setOwndadriTKDexporttwentyft(String owndadriTKDexporttwentyft) {
        this.owndadriTKDexporttwentyft = owndadriTKDexporttwentyft;
    }

    public String getOwndictImportfourtyft() {
        return owndictImportfourtyft;
    }

    public void setOwndictImportfourtyft(String owndictImportfourtyft) {
        this.owndictImportfourtyft = owndictImportfourtyft;
    }

    public String getOwndictImporttwentyft() {
        return owndictImporttwentyft;
    }

    public void setOwndictImporttwentyft(String owndictImporttwentyft) {
        this.owndictImporttwentyft = owndictImporttwentyft;
    }

    public String getOwndictemptyfourtyft() {
        return owndictemptyfourtyft;
    }

    public void setOwndictemptyfourtyft(String owndictemptyfourtyft) {
        this.owndictemptyfourtyft = owndictemptyfourtyft;
    }

    public String getOwndictemptytwentyft() {
        return owndictemptytwentyft;
    }

    public void setOwndictemptytwentyft(String owndictemptytwentyft) {
        this.owndictemptytwentyft = owndictemptytwentyft;
    }

    public String getOwndictexportfourtyft() {
        return owndictexportfourtyft;
    }

    public void setOwndictexportfourtyft(String owndictexportfourtyft) {
        this.owndictexportfourtyft = owndictexportfourtyft;
    }

    public String getOwndictexporttwentyft() {
        return owndictexporttwentyft;
    }

    public void setOwndictexporttwentyft(String owndictexporttwentyft) {
        this.owndictexporttwentyft = owndictexporttwentyft;
    }

    public String getOwnloniImportfourtyft() {
        return ownloniImportfourtyft;
    }

    public void setOwnloniImportfourtyft(String ownloniImportfourtyft) {
        this.ownloniImportfourtyft = ownloniImportfourtyft;
    }

    public String getOwnloniImporttwentyft() {
        return ownloniImporttwentyft;
    }

    public void setOwnloniImporttwentyft(String ownloniImporttwentyft) {
        this.ownloniImporttwentyft = ownloniImporttwentyft;
    }

    public String getOwnloniemptyfourtyft() {
        return ownloniemptyfourtyft;
    }

    public void setOwnloniemptyfourtyft(String ownloniemptyfourtyft) {
        this.ownloniemptyfourtyft = ownloniemptyfourtyft;
    }

    public String getOwnloniemptytwentyft() {
        return ownloniemptytwentyft;
    }

    public void setOwnloniemptytwentyft(String ownloniemptytwentyft) {
        this.ownloniemptytwentyft = ownloniemptytwentyft;
    }

    public String getOwnloniexportfourtyft() {
        return ownloniexportfourtyft;
    }

    public void setOwnloniexportfourtyft(String ownloniexportfourtyft) {
        this.ownloniexportfourtyft = ownloniexportfourtyft;
    }

    public String getOwnloniexporttwentyft() {
        return ownloniexporttwentyft;
    }

    public void setOwnloniexporttwentyft(String ownloniexporttwentyft) {
        this.ownloniexporttwentyft = ownloniexporttwentyft;
    }

    public String getPlannedFortyFT() {
        return plannedFortyFT;
    }

    public void setPlannedFortyFT(String plannedFortyFT) {
        this.plannedFortyFT = plannedFortyFT;
    }

    public String getPlannedTwentyFT() {
        return plannedTwentyFT;
    }

    public void setPlannedTwentyFT(String plannedTwentyFT) {
        this.plannedTwentyFT = plannedTwentyFT;
    }

    public String getRevenueTollAmount() {
        return revenueTollAmount;
    }

    public void setRevenueTollAmount(String revenueTollAmount) {
        this.revenueTollAmount = revenueTollAmount;
    }

    public String getTransportType() {
        return transportType;
    }

    public void setTransportType(String transportType) {
        this.transportType = transportType;
    }

    public String getTripExpense() {
        return tripExpense;
    }

    public void setTripExpense(String tripExpense) {
        this.tripExpense = tripExpense;
    }

    public String getTwentyFT() {
        return twentyFT;
    }

    public void setTwentyFT(String twentyFT) {
        this.twentyFT = twentyFT;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBilledPartyname() {
        return billedPartyname;
    }

    public void setBilledPartyname(String billedPartyname) {
        this.billedPartyname = billedPartyname;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public String getDetentionAmount() {
        return detentionAmount;
    }

    public void setDetentionAmount(String detentionAmount) {
        this.detentionAmount = detentionAmount;
    }

    public String getFrieghtAmount() {
        return frieghtAmount;
    }

    public void setFrieghtAmount(String frieghtAmount) {
        this.frieghtAmount = frieghtAmount;
    }

    public String getWeightMent() {
        return weightMent;
    }

    public void setWeightMent(String weightMent) {
        this.weightMent = weightMent;
    }

    public String getExpenseId() {
        return expenseId;
    }

    public void setExpenseId(String expenseId) {
        this.expenseId = expenseId;
    }

    public String getGreenTaxAmount() {
        return greenTaxAmount;
    }

    public void setGreenTaxAmount(String greenTaxAmount) {
        this.greenTaxAmount = greenTaxAmount;
    }

    public String getExtraFoodingAmount() {
        return extraFoodingAmount;
    }

    public void setExtraFoodingAmount(String extraFoodingAmount) {
        this.extraFoodingAmount = extraFoodingAmount;
    }

    public String getDalaAmount() {
        return dalaAmount;
    }

    public void setDalaAmount(String dalaAmount) {
        this.dalaAmount = dalaAmount;
    }

    public String getDriverBatta() {
        return driverBatta;
    }

    public void setDriverBatta(String driverBatta) {
        this.driverBatta = driverBatta;
    }

    public String getBhati() {
        return bhati;
    }

    public void setBhati(String bhati) {
        this.bhati = bhati;
    }

    public String getOctroiAmount() {
        return octroiAmount;
    }

    public void setOctroiAmount(String octroiAmount) {
        this.octroiAmount = octroiAmount;
    }

    public String getRevenueDetentionAmount() {
        return revenueDetentionAmount;
    }

    public void setRevenueDetentionAmount(String revenueDetentionAmount) {
        this.revenueDetentionAmount = revenueDetentionAmount;
    }

    public String getRevenueOtherExpense() {
        return revenueOtherExpense;
    }

    public void setRevenueOtherExpense(String revenueOtherExpense) {
        this.revenueOtherExpense = revenueOtherExpense;
    }

    public String getRevenueWeightMent() {
        return revenueWeightMent;
    }

    public void setRevenueWeightMent(String revenueWeightMent) {
        this.revenueWeightMent = revenueWeightMent;
    }

    public String getGstNo() {
        return gstNo;
    }

    public void setGstNo(String gstNo) {
        this.gstNo = gstNo;
    }

    public String getPanNo() {
        return panNo;
    }

    public void setPanNo(String panNo) {
        this.panNo = panNo;
    }

    public String getBillingState() {
        return billingState;
    }

    public void setBillingState(String billingState) {
        this.billingState = billingState;
    }

    public String getInvSubmissionDate() {
        return invSubmissionDate;
    }

    public void setInvSubmissionDate(String invSubmissionDate) {
        this.invSubmissionDate = invSubmissionDate;
    }

    public String getPodDate() {
        return podDate;
    }

    public void setPodDate(String podDate) {
        this.podDate = podDate;
    }

    public String getNoOfStartTrips() {
        return noOfStartTrips;
    }

    public void setNoOfStartTrips(String noOfStartTrips) {
        this.noOfStartTrips = noOfStartTrips;
    }

    public String getNoOfEndTrips() {
        return noOfEndTrips;
    }

    public void setNoOfEndTrips(String noOfEndTrips) {
        this.noOfEndTrips = noOfEndTrips;
    }

    public String getStartedTwentyFtContainer() {
        return startedTwentyFtContainer;
    }

    public void setStartedTwentyFtContainer(String startedTwentyFtContainer) {
        this.startedTwentyFtContainer = startedTwentyFtContainer;
    }

    public String getStartedFourtyFtContainer() {
        return startedFourtyFtContainer;
    }

    public void setStartedFourtyFtContainer(String startedFourtyFtContainer) {
        this.startedFourtyFtContainer = startedFourtyFtContainer;
    }

    public String getEndedTwentyFtContainer() {
        return endedTwentyFtContainer;
    }

    public void setEndedTwentyFtContainer(String endedTwentyFtContainer) {
        this.endedTwentyFtContainer = endedTwentyFtContainer;
    }

    public String getEndedFourtyFtContainer() {
        return endedFourtyFtContainer;
    }

    public void setEndedFourtyFtContainer(String endedFourtyFtContainer) {
        this.endedFourtyFtContainer = endedFourtyFtContainer;
    }

    public String getDeleteOrder() {
        return deleteOrder;
    }

    public void setDeleteOrder(String deleteOrder) {
        this.deleteOrder = deleteOrder;
    }

    public String getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(String creditAmount) {
        this.creditAmount = creditAmount;
    }

    public String getCreditNotecode() {
        return creditNotecode;
    }

    public void setCreditNotecode(String creditNotecode) {
        this.creditNotecode = creditNotecode;
    }

    public String getCreditNoteDate() {
        return creditNoteDate;
    }

    public void setCreditNoteDate(String creditNoteDate) {
        this.creditNoteDate = creditNoteDate;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getUpdateStatus() {
        return updateStatus;
    }

    public void setUpdateStatus(String updateStatus) {
        this.updateStatus = updateStatus;
    }

    public String getRequestNo() {
        return requestNo;
    }

    public void setRequestNo(String requestNo) {
        this.requestNo = requestNo;
    }

    public String getGateInDate() {
        return gateInDate;
    }

    public void setGateInDate(String gateInDate) {
        this.gateInDate = gateInDate;
    }

    public String getShippingBillDate() {
        return shippingBillDate;
    }

    public void setShippingBillDate(String shippingBillDate) {
        this.shippingBillDate = shippingBillDate;
    }

    public String getShippingBillNo() {
        return shippingBillNo;
    }

    public void setShippingBillNo(String shippingBillNo) {
        this.shippingBillNo = shippingBillNo;
    }

    public String getMarketRate() {
        return marketRate;
    }

    public void setMarketRate(String marketRate) {
        this.marketRate = marketRate;
    }

    public String getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(String totalHours) {
        this.totalHours = totalHours;
    }

    public String getTotalTrips() {
        return totalTrips;
    }

    public void setTotalTrips(String totalTrips) {
        this.totalTrips = totalTrips;
    }

    public String getConsignmentNo() {
        return consignmentNo;
    }

    public void setConsignmentNo(String consignmentNo) {
        this.consignmentNo = consignmentNo;
    }

    public String getNodstatusId() {
        return nodstatusId;
    }

    public void setNodstatusId(String nodstatusId) {
        this.nodstatusId = nodstatusId;
    }

    public String getNodTotalCount() {
        return nodTotalCount;
    }

    public void setNodTotalCount(String nodTotalCount) {
        this.nodTotalCount = nodTotalCount;
    }

    public String getNodstatusIds() {
        return nodstatusIds;
    }

    public void setNodstatusIds(String nodstatusIds) {
        this.nodstatusIds = nodstatusIds;
    }

    public String getNodTotalCounts() {
        return nodTotalCounts;
    }

    public void setNodTotalCounts(String nodTotalCounts) {
        this.nodTotalCounts = nodTotalCounts;
    }

    public String getNoOfDays() {
        return noOfDays;
    }

    public void setNoOfDays(String noOfDays) {
        this.noOfDays = noOfDays;
    }

    public String getZeroDay() {
        return zeroDay;
    }

    public void setZeroDay(String zeroDay) {
        this.zeroDay = zeroDay;
    }

    public String getOneDay() {
        return oneDay;
    }

    public void setOneDay(String oneDay) {
        this.oneDay = oneDay;
    }

    public String getTwoDay() {
        return twoDay;
    }

    public void setTwoDay(String twoDay) {
        this.twoDay = twoDay;
    }

    public String getThreeDay() {
        return threeDay;
    }

    public void setThreeDay(String threeDay) {
        this.threeDay = threeDay;
    }

    public String getFourDay() {
        return fourDay;
    }

    public void setFourDay(String fourDay) {
        this.fourDay = fourDay;
    }

    public String getFiveDay() {
        return fiveDay;
    }

    public void setFiveDay(String fiveDay) {
        this.fiveDay = fiveDay;
    }

    public String getSixDay() {
        return sixDay;
    }

    public void setSixDay(String sixDay) {
        this.sixDay = sixDay;
    }

    public String getSevenDay() {
        return sevenDay;
    }

    public void setSevenDay(String sevenDay) {
        this.sevenDay = sevenDay;
    }

    public String getMoreThanSevenDay() {
        return moreThanSevenDay;
    }

    public void setMoreThanSevenDay(String moreThanSevenDay) {
        this.moreThanSevenDay = moreThanSevenDay;
    }

    public String getOtherExpenseErpId() {
        return otherExpenseErpId;
    }

    public void setOtherExpenseErpId(String otherExpenseErpId) {
        this.otherExpenseErpId = otherExpenseErpId;
    }

    public String getGrId() {
        return grId;
    }

    public void setGrId(String grId) {
        this.grId = grId;
    }

    public String getReportTypeId() {
        return reportTypeId;
    }

    public void setReportTypeId(String reportTypeId) {
        this.reportTypeId = reportTypeId;
    }

    public String getConsignmentOrderId() {
        return consignmentOrderId;
    }

    public void setConsignmentOrderId(String consignmentOrderId) {
        this.consignmentOrderId = consignmentOrderId;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getContractRateId() {
        return contractRateId;
    }

    public void setContractRateId(String contractRateId) {
        this.contractRateId = contractRateId;
    }

    public String getRouteContractId() {
        return routeContractId;
    }

    public void setRouteContractId(String routeContractId) {
        this.routeContractId = routeContractId;
    }

    public String getRateWithReefer() {
        return rateWithReefer;
    }

    public void setRateWithReefer(String rateWithReefer) {
        this.rateWithReefer = rateWithReefer;
    }

    public String getRatewithoutreefer() {
        return ratewithoutreefer;
    }

    public void setRatewithoutreefer(String ratewithoutreefer) {
        this.ratewithoutreefer = ratewithoutreefer;
    }

    public String getRoutePointId() {
        return routePointId;
    }

    public void setRoutePointId(String routePointId) {
        this.routePointId = routePointId;
    }

    public String getLoadType() {
        return loadType;
    }

    public void setLoadType(String loadType) {
        this.loadType = loadType;
    }

    public String getArticleCode() {
        return articleCode;
    }

    public void setArticleCode(String articleCode) {
        this.articleCode = articleCode;
    }

    public String getInvIds() {
        return invIds;
    }

    public void setInvIds(String invIds) {
        this.invIds = invIds;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getPdfName() {
        return pdfName;
    }

    public void setPdfName(String pdfName) {
        this.pdfName = pdfName;
    }

    public String getBillOfEntryNo() {
        return billOfEntryNo;
    }

    public void setBillOfEntryNo(String billOfEntryNo) {
        this.billOfEntryNo = billOfEntryNo;
    }

    public String getShipingBillNo() {
        return shipingBillNo;
    }

    public void setShipingBillNo(String shipingBillNo) {
        this.shipingBillNo = shipingBillNo;
    }

    public String getGstType() {
        return gstType;
    }

    public void setGstType(String gstType) {
        this.gstType = gstType;
    }

    public String getCommodityId() {
        return commodityId;
    }

    public void setCommodityId(String commodityId) {
        this.commodityId = commodityId;
    }

    public String getCommodityName() {
        return commodityName;
    }

    public void setCommodityName(String commodityName) {
        this.commodityName = commodityName;
    }

    public String getPostStatus() {
        return postStatus;
    }

    public void setPostStatus(String postStatus) {
        this.postStatus = postStatus;
    }

    public String getSbBillBoeBill() {
        return sbBillBoeBill;
    }

    public void setSbBillBoeBill(String sbBillBoeBill) {
        this.sbBillBoeBill = sbBillBoeBill;
    }

    public String getMovTypeId() {
        return movTypeId;
    }

    public void setMovTypeId(String movTypeId) {
        this.movTypeId = movTypeId;
    }

    public String[] getGrIds() {
        return grIds;
    }

    public void setGrIds(String[] grIds) {
        this.grIds = grIds;
    }

    public String[] getSelectedIds() {
        return selectedIds;
    }

    public void setSelectedIds(String[] selectedIds) {
        this.selectedIds = selectedIds;
    }

    public String[] getPendingGrRemarks() {
        return pendingGrRemarks;
    }

    public void setPendingGrRemarks(String[] pendingGrRemarks) {
        this.pendingGrRemarks = pendingGrRemarks;
    }

    public String[] getTripIdss() {
        return tripIdss;
    }

    public void setTripIdss(String[] tripIdss) {
        this.tripIdss = tripIdss;
    }

    public String getTtType() {
        return ttType;
    }

    public void setTtType(String ttType) {
        this.ttType = ttType;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public String getMenupath() {
        return menupath;
    }

    public void setMenupath(String menupath) {
        this.menupath = menupath;
    }

    public String getPermit() {
        return permit;
    }

    public void setPermit(String permit) {
        this.permit = permit;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getApiErrorMsg() {
        return apiErrorMsg;
    }

    public void setApiErrorMsg(String apiErrorMsg) {
        this.apiErrorMsg = apiErrorMsg;
    }

    public String getExpenseValue() {
        return expenseValue;
    }

    public void setExpenseValue(String expenseValue) {
        this.expenseValue = expenseValue;
    }

    public String getCreditNoteId() {
        return creditNoteId;
    }

    public void setCreditNoteId(String creditNoteId) {
        this.creditNoteId = creditNoteId;
    }

    public String getGstStatusType() {
        return gstStatusType;
    }

    public void setGstStatusType(String gstStatusType) {
        this.gstStatusType = gstStatusType;
    }

    public String getCustomMasterLogid() {
        return customMasterLogid;
    }

    public void setCustomMasterLogid(String customMasterLogid) {
        this.customMasterLogid = customMasterLogid;
    }

    public String getCustomerMasterName() {
        return customerMasterName;
    }

    public void setCustomerMasterName(String customerMasterName) {
        this.customerMasterName = customerMasterName;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getCustAddress() {
        return custAddress;
    }

    public void setCustAddress(String custAddress) {
        this.custAddress = custAddress;
    }

    public String getCustAddress2() {
        return custAddress2;
    }

    public void setCustAddress2(String custAddress2) {
        this.custAddress2 = custAddress2;
    }

    public String getCustAddress3() {
        return custAddress3;
    }

    public void setCustAddress3(String custAddress3) {
        this.custAddress3 = custAddress3;
    }

    public String getCustCity() {
        return custCity;
    }

    public void setCustCity(String custCity) {
        this.custCity = custCity;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCustomerMasterPhone() {
        return customerMasterPhone;
    }

    public void setCustomerMasterPhone(String customerMasterPhone) {
        this.customerMasterPhone = customerMasterPhone;
    }

    public String getCustomerMasterMobile() {
        return customerMasterMobile;
    }

    public void setCustomerMasterMobile(String customerMasterMobile) {
        this.customerMasterMobile = customerMasterMobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLedgerId() {
        return ledgerId;
    }

    public void setLedgerId(String ledgerId) {
        this.ledgerId = ledgerId;
    }

    public String getLedgerCode() {
        return ledgerCode;
    }

    public void setLedgerCode(String ledgerCode) {
        this.ledgerCode = ledgerCode;
    }

    public String getCreditDays() {
        return creditDays;
    }

    public void setCreditDays(String creditDays) {
        this.creditDays = creditDays;
    }

    public String getCustomerMasterCreditLimit() {
        return customerMasterCreditLimit;
    }

    public void setCustomerMasterCreditLimit(String customerMasterCreditLimit) {
        this.customerMasterCreditLimit = customerMasterCreditLimit;
    }

    public String getSecondaryBillingTypeId() {
        return secondaryBillingTypeId;
    }

    public void setSecondaryBillingTypeId(String secondaryBillingTypeId) {
        this.secondaryBillingTypeId = secondaryBillingTypeId;
    }

    public String getCustomerBillingNameAddress() {
        return customerBillingNameAddress;
    }

    public void setCustomerBillingNameAddress(String customerBillingNameAddress) {
        this.customerBillingNameAddress = customerBillingNameAddress;
    }

    public String getErpId() {
        return erpId;
    }

    public void setErpId(String erpId) {
        this.erpId = erpId;
    }

    public String getCustPanNo() {
        return custPanNo;
    }

    public void setCustPanNo(String custPanNo) {
        this.custPanNo = custPanNo;
    }

    public String getCustomerMasterGstNo() {
        return customerMasterGstNo;
    }

    public void setCustomerMasterGstNo(String customerMasterGstNo) {
        this.customerMasterGstNo = customerMasterGstNo;
    }

    public String getBillingTypeId() {
        return billingTypeId;
    }

    public void setBillingTypeId(String billingTypeId) {
        this.billingTypeId = billingTypeId;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getCompanyType() {
        return companyType;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    public String getCustomerMasterActiveInd() {
        return customerMasterActiveInd;
    }

    public void setCustomerMasterActiveInd(String customerMasterActiveInd) {
        this.customerMasterActiveInd = customerMasterActiveInd;
    }

    public String getCustomerMasterBillingState() {
        return customerMasterBillingState;
    }

    public void setCustomerMasterBillingState(String customerMasterBillingState) {
        this.customerMasterBillingState = customerMasterBillingState;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public String getEmailDate() {
        return emailDate;
    }

    public void setEmailDate(String emailDate) {
        this.emailDate = emailDate;
    }

    public String getConsignorNo() {
        return consignorNo;
    }

    public void setConsignorNo(String consignorNo) {
        this.consignorNo = consignorNo;
    }

    public String getConsignerMobile() {
        return consignerMobile;
    }

    public void setConsignerMobile(String consignerMobile) {
        this.consignerMobile = consignerMobile;
    }

    public String getConsignerAddress() {
        return consignerAddress;
    }

    public void setConsignerAddress(String consignerAddress) {
        this.consignerAddress = consignerAddress;
    }

    public String getSelectName() {
        return selectName;
    }

    public void setSelectName(String selectName) {
        this.selectName = selectName;
    }

    public String getConsignmentId() {
        return consignmentId;
    }

    public void setConsignmentId(String consignmentId) {
        this.consignmentId = consignmentId;
    }

    public String getConsignmentOrder() {
        return consignmentOrder;
    }

    public void setConsignmentOrder(String consignmentOrder) {
        this.consignmentOrder = consignmentOrder;
    }

    public String getGrTime() {
        return grTime;
    }

    public void setGrTime(String grTime) {
        this.grTime = grTime;
    }

    public String getDetailStatusName() {
        return detailStatusName;
    }

    public void setDetailStatusName(String detailStatusName) {
        this.detailStatusName = detailStatusName;
    }

    public String getMasterId() {
        return masterId;
    }

    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }

    public String getDetailId() {
        return detailId;
    }

    public void setDetailId(String detailId) {
        this.detailId = detailId;
    }

    public String getInvoiceCustomer() {
        return invoiceCustomer;
    }

    public void setInvoiceCustomer(String invoiceCustomer) {
        this.invoiceCustomer = invoiceCustomer;
    }

    public String getBillingTypeName() {
        return billingTypeName;
    }

    public void setBillingTypeName(String billingTypeName) {
        this.billingTypeName = billingTypeName;
    }

    public String getTripPod() {
        return tripPod;
    }

    public void setTripPod(String tripPod) {
        this.tripPod = tripPod;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getTarrifType() {
        return tarrifType;
    }

    public void setTarrifType(String tarrifType) {
        this.tarrifType = tarrifType;
    }

    public String getPointId() {
        return pointId;
    }

    public void setPointId(String pointId) {
        this.pointId = pointId;
    }

    public String getPointType() {
        return pointType;
    }

    public void setPointType(String pointType) {
        this.pointType = pointType;
    }

    public String getPointSequence() {
        return pointSequence;
    }

    public void setPointSequence(String pointSequence) {
        this.pointSequence = pointSequence;
    }

    public String getPointAddress() {
        return pointAddress;
    }

    public void setPointAddress(String pointAddress) {
        this.pointAddress = pointAddress;
    }

    public String getPointPlanDate() {
        return pointPlanDate;
    }

    public void setPointPlanDate(String pointPlanDate) {
        this.pointPlanDate = pointPlanDate;
    }

    public String getPointPlanTime() {
        return pointPlanTime;
    }

    public void setPointPlanTime(String pointPlanTime) {
        this.pointPlanTime = pointPlanTime;
    }

    public String[] getCityNme() {
        return cityNme;
    }

    public void setCityNme(String[] cityNme) {
        this.cityNme = cityNme;
    }

    public String getWorkNo() {
        return workNo;
    }

    public void setWorkNo(String workNo) {
        this.workNo = workNo;
    }
    
}
