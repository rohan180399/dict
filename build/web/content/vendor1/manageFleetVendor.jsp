<%-- 
    Document   : manageVendor
    Created on : Mar 8, 2009, 10:51:13 AM
    Author     : karudaiyar Subramaniam
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BUS</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
</head>
<body>

    <form method="post" name="fleetVendorDetail">
        <%@ include file="/content/common/path.jsp" %>


        <%@ include file="/content/common/message.jsp" %>

        <% int index = 1;  
        %>
        <br>
        <c:if test = "${VendorList != null}" >
            <table align="center" border="0" id="table" class="sortable">
                <tr align="center">
                    <td height="30" class="contentsub">S.No</td>
                    <td height="30" class="contentsub">Vendor Name</td>
                    <td height="30" class="contentsub">Vendor Type</td>
                    <td height="30" class="contentsub">Price Type</td>
                    <td height="30" class="contentsub">Credit Days</td>
                    <td height="30" class="contentsub">TIN NO</td>
                    <td height="30" class="contentsub">Vendor Address</td>
                    <td height="30" class="contentsub">Vendor PhoneNo</td>
                    <td height="30" class="contentsub">Vendor Mail Id</td>
                    <td height="30" class="contentsub">Vendor Is Cridatable</td>
                    <td height="30" class="contentsub">Status</td>
                    <td class="contentsub"></td>
                </tr>
                <%

                %>

                <c:forEach items="${VendorList}" var="list"> 

                    <%

        String classText = "";
        int oddEven = index % 2;
        if (oddEven > 0) {
            classText = "text2";
        } else {
            classText = "text1";
        }
                    %>
                    <tr  width="208" height="40" > 
                        <td class="<%=classText %>" height="20"><%=index%></td>
                        <td class="<%=classText %>" height="20"><c:out value="${list.vendorName}"/></td>
                        <td class="<%=classText %>" height="20"><c:out value="${list.vendorTypeValue}"/></td>
                        <td class="<%=classText %>" height="20"><c:out value="${list.priceType}"/></td>
                        <td class="<%=classText %>" height="20"><c:out value="${list.creditDays}"/></td>
                        <td class="<%=classText %>" height="20"><c:out value="${list.tinNo}"/></td>
                        <td class="<%=classText %>" height="20"><c:out value="${list.vendorAddress}"/></td>
                        <td class="<%=classText %>" height="20"><c:out value="${list.vendorPhoneNo}"/></td>
                        <td class="<%=classText %>" height="20"><c:out value="${list.vendorMailId}"/></td>
                        <td class="<%=classText %>" height="20">                                
                            <c:if test = "${list.settlementType == '1' }" >
                                No
                            </c:if>
                            <c:if test = "${list.settlementType == '2'}" >
                                Yes
                            </c:if>
                        </td>
                        <td class="<%=classText %>" height="20">                                
                            <a href="/throttle/createVehicleVendorContract.do?vendorId=<c:out value='${list.vendorId}' />&vendor=<c:out value='${list.vendorName}' />" >Create</a> 
                            <c:if test = "${list.vendorContractStatus == '1' }" >
                                <a href="/throttle/viewVendorVehicleContractList.do?vendorId=<c:out value='${list.vendorId}' />&vendor=<c:out value='${list.vendorName}' />" > View / Alter </a>
                            </c:if>
                        </td>
                    </tr>
                    <%
        index++;
                    %>
                </c:forEach >

            </table>
        </c:if> 
        <br>
        <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5" >5</option>
                    <option value="10">10</option>
                    <option value="20" selected="selected">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>

