<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>
<script type="text/javascript">
    function setValues() {
        var status = document.getElementById("statuss").value;
        document.invoice.action = "/throttle/autoMailStatus.do?status=" + status + "&param=update";
        document.invoice.submit();
    }
    document.getElementById("statuss").value=<c:out value="${currStatus}"/>;
</script>

</head>



<style>
    #index td {
        color:white;
    }
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="finance.label.Auto Mail On/Off"  text="Auto Mail On/Off"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance"  text="default text"/></a></li>
            <li class="active"><spring:message code="finance.label.Auto Mail On/Off"  text="Auto Mail On/Off"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body"> 



            <body>
                <form name="invoice" method="post" >
                    <%@ include file="/content/common/message.jsp" %>

                    <table class="table table-info mb30 table-hover">
                        <thead>
                            <tr >
                                <th colspan="6"  align="center">
                                    <spring:message code="finance.label.Auto Mail On/Off"  text="Auto Mail On/Off"/></th>
                            </tr>
                        </thead>
                        <tr align="center">
                            <td width="80" align="center"><b><spring:message code="finance.label.Status"  text="Status"/></b> &nbsp;&nbsp; </td>
                            <td>  <select name="statuss" id="statuss"  class="form-control" style="width:220px;height:40px;" >
                                    <option value='1' selected>-Mail-On-</option>
                                    <option value='0' selected>-Mail-Off-</option>
                                </select></td>
                        </tr>
                    </table>
                             <script>
                                                document.invoice.statuss.value = '<c:out value="${currStatus}" />' ;
                                            </script>
                    <br/>
                    <table align="center" width="100%" border="0">
                        <tr >
                            <td colspan="2" align="center"> <input type="button" id="Update" value="Update" class="button" onClick="setValues();">
                        </tr>
                    </table>


            </body>
            </form>

        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>