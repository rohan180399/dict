
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>
 <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script>
        <title>BUS</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
            <script language="javascript" src="/throttle/js/validate.js"></script>
    </head>
    <script language="javascript">
        function submitPage(value)  {
            

            if(confirm("Are you sure to submit")){
                document.jobCardBill.action = "extJobCardBillStore.do";
                document.jobCardBill.submit();     
            }                
        }
        
    </script>
    
    <body>
        
        <form method="post" name="jobCardBill" action= "jobCardBillStore.do">
            <!-- copy there from end -->
        <div id="print" >
            <div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
                <div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
                    <!-- pointer table -->
                    <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                        <tr>
                            <td >
                                <%@ include file="/content/common/path.jsp" %>
                    </td></tr></table>
                    <!-- pointer table -->
                  
                </div>
            </div>
           
            <!-- message table -->
            <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
                <tr>
                    <td >
                        <%@ include file="/content/common/message.jsp" %>
                    </td>
                </tr>
            </table>
                    <br>
                    <br>
                    <br>
            
            <% int c = 0; %>
            <c:if test = "${extJobCardBillDetails != null}" >
                <c:forEach items="${extJobCardBillDetails}" var="list">
                    
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="90%" class="border">
                        <tbody><tr>
                                <td class="contenthead" colspan="6" height="30"><div class="contenthead"> Vendor Bill Details</div></td>
                            </tr>
                            
                            <tr>
                                
                                <td class="text1" height="30"><b>JobCardNo</b></td>
                                <td class="text1" height="30"><c:out value="${list.jcMYFormatNo}"/>                              
                                
                                </td>

                                <td class="text1" height="30"><b>Vehicle No</b></td>
                                <td class="text1" height="30"><c:out value="${list.vehicleNo}"/></td>
                                <td class="text1" height="30"><b>Service Vendor</b></td>
                                <td class="text1" height="30"><c:out value="${list.serviceVendor}"/></td>
                            </tr>
                            
                    </tbody></table>
                    
                    
                    
               
    <br>
        <table width="90%" cellpadding="0" cellspacing="0" border="0" align="center" class="table5">
            <tr>
            <td class="bottom" width="8%" align="left"><img src="/throttle/images/left_status.jpg" alt=""  /></td>

            <td  width="12%" align="right"><h2>Total SAR.</h2></td>
            <td  width="10%" align="left"><h2><div id="totalValue"><c:out value="${list.totalAmt}"/></div></h2></td>

            </tr>

        </table>
        
        <br>

           <table align="center" border="0" cellpadding="0" cellspacing="0" width="90%" class="border">
		<tbody><tr>
		<td class="text2" align="center" colspan="8" height="30"><strong>Vendor Bill Particulars</strong></td>
		</tr>
		<tr height="30">
                    <td class="text1" align="left"><b>Invoice No</b></td>
                    <td class="text1" align="left"><c:out value="${list.invoiceNo}"/> </td>
                    <td class="text1" align="left"><b>Invoice Remarks</b></td>
                    <td class="text1" align="left"><c:out value="${list.invoiceRemarks}"/></td>
                    <td class="text1" align="left"><b>Invoice Date</b></td>
                    <td class="text1" align="left"><c:out value="${list.invoiceDate}"/></td>
		</tr>
		<tr height="30">
                    <td class="text2" align="left"><b>Spares Value</b></td>
                    <td class="text2" align="left"><c:out value="${list.sparesAmt}"/> </td>
                    <td class="text2" align="left"><b>Spares Remarks / Details</b></td>
                    <td class="text2" align="left"><c:out value="${list.sparesRemarks}"/></td>
                    <td class="text2" align="left">&nbsp;</td>
                    <td class="text2" align="left">&nbsp;</td>
		</tr>
		<tr height="30">
                    <td class="text1" align="left"><b>Consumables Value</b></td>
                    <td class="text1" align="left"><c:out value="${list.consumableAmt}"/> </td>
                    <td class="text1" align="left"><b>Consumables Remarks / Details</b></td>
                    <td class="text1" align="left"><c:out value="${list.consumableRemarks}"/></td>
                    <td class="text1" align="left">&nbsp;</td>
                    <td class="text1" align="left">&nbsp;</td>
		</tr>
		<tr height="30">
                    <td class="text2" align="left"><b>Labour Value</b></td>
                    <td class="text2" align="left"><c:out value="${list.laborAmt}"/> </td>
                    <td class="text2" align="left"><b>Labour Remarks / Details</b></td>
                    <td class="text2" align="left"><c:out value="${list.laborRemarks}"/></td>
                    <td class="text2" align="left">&nbsp;</td>
                    <td class="text2" align="left">&nbsp;</td>
		</tr>
		<tr height="30">
                    <td class="text1" align="left"><b>Others Value</b></td>
                    <td class="text1" align="left"><c:out value="${list.othersAmt}"/> </td>
                    <td class="text1" align="left"><b>Others Remarks / Details</b></td>
                    <td class="text1" align="left"><c:out value="${list.othersRemarks}"/></td>
                    <td class="text1" align="left">&nbsp;</td>
                    <td class="text1" align="left">&nbsp;</td>
		</tr>
		<tr height="30">
                    <td class="text1" align="left"><b>Vat %age</b></td>
                    <td class="text1" align="left"><c:out value="${list.vatPercent}"/></td>
                    <td class="text1" align="left"><b>Vat Value</b></td>
                    <td class="text1" align="left"><c:out value="${list.vatAmt}"/></td>
                    <td class="text1" align="left">&nbsp;</td>
                    <td class="text1" align="left">&nbsp;</td>
		</tr>
                <tr>
                    <td class="text2" align="left"><b>Service Tax %age</b></td>
                    <td class="text2" align="left"><c:out value="${list.serviceTaxPercent}"/> </td>
                    <td class="text2" align="left"><b>Service Tax Value</b></td>
                    <td class="text2" align="left"><c:out value="${list.serviceTaxAmt}"/> </td>
                    <td class="text2" align="left">&nbsp;</td>
                    <td class="text2" align="left">&nbsp;</td>
		</tr>

                </table>
             </c:forEach >

            </c:if>
    
             
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
