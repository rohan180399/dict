package ets.domain.customer.data;

/**
 *
 * @author vidya
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.io.File;
import ets.domain.util.FPLogUtils;
import ets.arch.exception.FPRuntimeException;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import ets.domain.customer.business.CustomerTO;
import ets.domain.util.ThrottleConstants;
import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.ParamPart;
import java.io.FileInputStream;
import com.oreilly.servlet.multipart.Part;
import static oracle.net.aso.C07.n;

;

public class CustomerDAO extends SqlMapClientDaoSupport {

    private final int errorStatus = 4;
    private final static String CLASS = "CustomerDAO";


    public int doInsertCustomer(CustomerTO customerTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int insertContractRouteMater = 0;
        map.put("customerId", insertContractRouteMater);
        String custCode = "";
        String[] temp1 = null;
        custCode = (String) getSqlMapClientTemplate().queryForObject("customer.getCustomerCode", map);
        if (custCode == null) {
            custCode = "CC-0001";
        } else if (custCode.length() == 1) {
            custCode = "CC-000" + custCode;
        } else if (custCode.length() == 2) {
            custCode = "CC-00" + custCode;
        } else if (custCode.length() == 3) {
            custCode = "CC-0" + custCode;
        } else if (custCode.length() == 4) {
            custCode = "CC-" + custCode;
        }
        map.put("custCode", custCode);
        map.put("erpId", customerTO.getErpId());
        map.put("panNo", customerTO.getPanNo());
        map.put("companyType", customerTO.getCompanyType());
        map.put("custType", customerTO.getCustTypeId());
        map.put("gtaType", customerTO.getGtaType());
        if ("1".equalsIgnoreCase(customerTO.getCustomerTypeId())) {
            map.put("paymentType", customerTO.getPaymentType());
            map.put("Code", customerTO.getCustomerCode());
            map.put("Name", customerTO.getCustName());
            map.put("billingTypeId", customerTO.getBillingTypeId());
             String add=customerTO.getCustAddress();
        String Address = add.replaceAll("\\r\\n|\\r|\\n\\t\\b|\\t|\\b\\s", "");
            map.put("Address", Address);
            String add2=customerTO.getCustAddresstwo();
        String Address2 = add2.replaceAll("\\r\\n|\\r|\\n\\t\\b|\\t|\\b\\s", "");
            map.put("Addresstwo",Address2 );
            String add3=customerTO.getCustAddressthree();
        String Address3 = add3.replaceAll("\\r\\n|\\r|\\n\\t\\b|\\t|\\b\\s", "");
            map.put("Addressthree",Address3 );
            map.put("City", customerTO.getCustCity());
            map.put("Contact", customerTO.getCustContactPerson());
            map.put("State", customerTO.getCustState());
            map.put("Phone", customerTO.getCustPhone());
            map.put("Mobile", customerTO.getCustMobile());
            map.put("Email", customerTO.getCustEmail());
            map.put("pinCode", customerTO.getPinCode());
           
            if("".equalsIgnoreCase(customerTO.getCreditDays()) || customerTO.getCreditDays() == null){
            map.put("creditDays", "0");
             }else{
               map.put("creditDays", customerTO.getCreditDays());
             }
             if("".equalsIgnoreCase(customerTO.getCreditLimit()) || customerTO.getCreditLimit() == null){
            map.put("creditLimit", "0");
             }else{
               map.put("creditLimit", customerTO.getCreditLimit());
             }
            if("".equalsIgnoreCase(customerTO.getAdvanceAmount()) || customerTO.getAdvanceAmount() == null){
                 map.put("advanceAmount", "0");
            }else{
            map.put("advanceAmount", customerTO.getAdvanceAmount());
            }
            if("".equalsIgnoreCase(customerTO.getFixedCreditLimit()) || customerTO.getFixedCreditLimit() == null){
                 map.put("fixedCreditLimit", "0");
                 map.put("creditLimit", "0");
                 map.put("availLimit", "0");
            }else{
            map.put("fixedCreditLimit", customerTO.getFixedCreditLimit());
            map.put("creditLimit", customerTO.getFixedCreditLimit());
            map.put("availLimit", customerTO.getFixedCreditLimit());
            }
            if("".equalsIgnoreCase(customerTO.getUsedLimit()) || customerTO.getUsedLimit() == null){
                 map.put("usedLimit", "0");
            }else{
            map.put("usedLimit", customerTO.getUsedLimit());
            }
            map.put("accountManagerId", customerTO.getAccountManagerId());
            map.put("secondaryBillingTypeId", customerTO.getSecondaryBillingTypeId());
            map.put("billingNameAddress", customerTO.getBillingNameAddress());
            map.put("customerTypeId", customerTO.getCustomerTypeId());
            map.put("customerGroupId", customerTO.getCustomerGroupId());
            map.put("stateId", customerTO.getStateId());
            map.put("gstNo", customerTO.getGstNo());
            map.put("organizationId", customerTO.getOrganizationId());
            //        map.put("tancem", customerTO.getTancem());
            map.put("userId", userId);
            System.out.print("customerTO.getCustName()" + customerTO.getCustName());
        } else {
            map.put("paymentType", "1");
            map.put("Code", customerTO.getCustomerCode());
            map.put("Name", customerTO.getCustName());
            map.put("billingTypeId", "0");
             String add=customerTO.getCustAddress();
        String Address = add.replaceAll("\\r\\n|\\r|\\n\\t\\b|\\t|\\b\\s", "");
            map.put("Address", Address);
             String add2=customerTO.getCustAddresstwo();
        String Address2 = add2.replaceAll("\\r\\n|\\r|\\n\\t\\b|\\t|\\b\\s", "");
            map.put("Addresstwo", Address2);
            String add3=customerTO.getCustAddressthree();
        String Address3 = add3.replaceAll("\\r\\n|\\r|\\n\\t\\b|\\t|\\b\\s", "");
            map.put("Addressthree",Address3 );
            map.put("City", customerTO.getCustCity());
            map.put("Contact", "ADMIN");
            map.put("State", customerTO.getCustState());
            map.put("Phone", "0");
            map.put("Mobile", customerTO.getCustMobile());
            map.put("Email", customerTO.getCustEmail());
            map.put("creditDays", "0");
            map.put("creditLimit", "0");
            map.put("accountManagerId", "0");
            map.put("secondaryBillingTypeId", "0");
              String billNameAd=customerTO.getBillingNameAddress();
        String billNameAddress = billNameAd.replaceAll("\\r\\n|\\r|\\n\\s", "");
            map.put("billingNameAddress", billNameAddress);
            map.put("customerTypeId", customerTO.getCustomerTypeId());
            map.put("customerGroupId", "0");
            //        map.put("tancem", customerTO.getTancem());
            map.put("userId", userId);
            map.put("stateId", customerTO.getStateId());
            map.put("gstNo", customerTO.getGstNo());
            map.put("organizationId", customerTO.getOrganizationId());
//            map.put("advanceAmount", "0");
//            map.put("fixedCreditLimit", "0");
            map.put("usedLimit", "0");
        }
        try {
            System.out.println("test1" + status);
            //            if ("2".equals(customerTO.getCustomerType())) {
            String code = (String) getSqlMapClientTemplate().queryForObject("customer.getLedgerCode", map);
            String[] temp = code.split("-");
            System.out.println("temp[1] = " + temp[1]);
            int codeval = Integer.parseInt(temp[1]);
            int codev = codeval + 1;
            System.out.println("codev = " + codev);
            String ledgercode = "LEDGER-" + codev;
            System.out.println("ledgercode = " + ledgercode);
            map.put("ledgercode", ledgercode);

            //current year and month start
            String accYear = (String) getSqlMapClientTemplate().queryForObject("customer.accYearVal", map);
            System.out.println("accYear:" + accYear);
            map.put("accYear", ThrottleConstants.financialYear);
            map.put("customerGroupCode", ThrottleConstants.customerGroupCode);
            map.put("customerLevelId", ThrottleConstants.customerLevelId);
            //current year end

            status = (Integer) getSqlMapClientTemplate().insert("customer.insertCustomerLedger", map);
            map.put("ledgerId", status);
            //            } else {
            //                map.put("ledgerId", "0");
            //            }
             System.out.println("map" + map);
            status = (Integer) getSqlMapClientTemplate().insert("customer.insertCustomer", map);
            if (customerTO.getSecondaryBillingTypeId()!=null && !"0".equals(customerTO.getSecondaryBillingTypeId())) {
                int insertOperationPointForSecondary = (Integer) getSqlMapClientTemplate().update("customer.insertOperationPointForSecondary", map);
            }
            map.put("customerId", status);
            int insertCustomerOutStand = 0;
            map.put("transactionName", "OpeningBalance");  
            map.put("transactionType", "Credit");  //credit
            map.put("creditAmount", customerTO.getFixedCreditLimit());
            map.put("debitAmount", "0.00");
            map.put("type", customerTO.getCustTypeId());
            map.put("used_limit", "0.00");
            map.put("blocked_amount", "0.00");
            System.out.println("map in the customer adding screen = " + map);
            insertCustomerOutStand = (Integer) getSqlMapClientTemplate().update("customer.insertCustomerCredit", map);
            insertCustomerOutStand = (Integer) getSqlMapClientTemplate().update("customer.insertCustomerOutStand", map);
            insertCustomerOutStand = (Integer) getSqlMapClientTemplate().update("customer.insertCustomerOutStandDetails", map);
            System.out.println("insertCustomerOutStand---"+insertCustomerOutStand);
            int insertCustomerSecondary = 0;
            if (customerTO.getSecondaryBillingTypeId() != null && !customerTO.getSecondaryBillingTypeId().equals("0")) {
                insertCustomerOutStand = (Integer) getSqlMapClientTemplate().update("customer.insertCustomerSecondaryApproval", map);
            }

            System.out.println("status customer details is is" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }






    public ArrayList getCustomerList() {
        Map map = new HashMap();
        ArrayList customerList = new ArrayList();
        try {
            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCustomerList", map);
            System.out.println("customerList size=" + customerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "customerList", sqlException);
        }
        return customerList;
    }

    public ArrayList getLPSCustomerList() {
        Map map = new HashMap();
        ArrayList customerList = new ArrayList();
        try {
            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getLPSCustomerList", map);
            System.out.println("customerList size=" + customerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "customerList", sqlException);
        }
        return customerList;
    }

    public int doUpdateCustomer(ArrayList List, int userId) {
        Map map = new HashMap();
        int status = 0;
        try {
            Iterator itr = List.iterator();
            CustomerTO customerTO = null;
            while (itr.hasNext()) {
                customerTO = (CustomerTO) itr.next();
                System.out.println("getCustId" + customerTO.getCustId());
                System.out.println("custName" + customerTO.getCustName());
                System.out.println("custContact" + customerTO.getCustContactPerson());
                System.out.println("custAddress" + customerTO.getCustAddress());
                System.out.println("customerTO.getCustCity());" + customerTO.getCustCity());
                System.out.println("custState" + customerTO.getCustState());
                System.out.println("custPhone" + customerTO.getCustPhone());
                System.out.println("custMobile" + customerTO.getCustMobile());
                System.out.println("custStatus" + customerTO.getCustStatus());
                map.put("custId", customerTO.getCustId());
                map.put("custName", customerTO.getCustName());
                map.put("custType", customerTO.getCustomerType());
                map.put("custContact", customerTO.getCustContactPerson());
                map.put("custAddress", customerTO.getCustAddress());
                map.put("custCity", customerTO.getCustCity());
                map.put("custState", customerTO.getCustState());
                map.put("custPhone", customerTO.getCustPhone());
                map.put("custMobile", customerTO.getCustMobile());
                map.put("custEmail", customerTO.getCustEmail());
                map.put("custStatus", customerTO.getCustStatus());
                map.put("tancem", customerTO.getTancem());
                map.put("userId", userId);
                map.put("erpId", customerTO.getErpId());
                status = (Integer) getSqlMapClientTemplate().update("customer.updateCustomer", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-03", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;

    }

    public int doInsertContract(CustomerTO customerTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int ManufacturerId = 1023;
        int ModelId = 1512;
        map.put("ManufacturerId", ManufacturerId);
        map.put("ModelId", ModelId);
        map.put("Spare", customerTO.getContPercentAgeUpSpares());
        map.put("Labour", customerTO.getContPercentAgeUpLabour());
        map.put("Status", customerTO.getContStatus());
        map.put("userId", userId);
        System.out.print("customerTO.getContManufacturerName()" + customerTO.getContPercentAgeUpLabour());
        try {
            status = (Integer) getSqlMapClientTemplate().update("customer.insertContract", map);
            System.out.println("status is" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CONT-02", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public ArrayList getBunkList(String bunkName) {
        Map map = new HashMap();
        ArrayList bunkList = new ArrayList();
        try {
            System.out.println("bunkName.." + bunkName);
            if (bunkName.length() != 0) {
                map.put("bunkname", bunkName);
            }

            System.out.println("map.size().." + map.size());
            if (map.size() > 0) {
                bunkList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getselBunk", map);
            } else {
                bunkList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getBunkList", map);
            }
            System.out.println("bunkselList size=" + bunkList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "bunkList", sqlException);
        }
        return bunkList;
    }

    public int doInsertBunk(CustomerTO customerTO, int userId) {
        Map mapmaster = new HashMap();
        Map mapdetail = new HashMap();
        Map map = new HashMap();
        ArrayList bunkmasterList = new ArrayList();
        int status = 0;
        int bunkid = 0;
        int id = 0;

        try {
            id = (Integer) getSqlMapClientTemplate().queryForObject("customer.getMaxIDList", mapmaster);
            String FuelLocateId = customerTO.getBunkName().substring(0, 1) + id;

            System.out.println("FuelLocateId" + FuelLocateId);

            mapmaster.put("FuelLocateId", FuelLocateId);
            mapmaster.put("bunkname", customerTO.getBunkName());
            mapmaster.put("location", customerTO.getCurrlocation());
            mapmaster.put("state", customerTO.getBunkState());
            mapmaster.put("act_ind", customerTO.getBunkStatus());
            mapmaster.put("createdby", userId);

            System.out.println("test1" + status);
            int bunkId = (Integer) getSqlMapClientTemplate().insert("customer.insertBunkmaster", mapmaster);
            System.out.println("bunkId master is" + bunkId);

            //Ledger Code start
            if (bunkId != 0) {
                String code = (String) getSqlMapClientTemplate().queryForObject("customer.getLedgerCode", mapmaster);
                String[] temp = code.split("-");
                int codeval = Integer.parseInt(temp[1]);
                int codev = codeval + 1;
                String ledgercode = "LEDGER-" + codev;
                mapmaster.put("ledgercode", ledgercode);

                //current year and month start
                String accYear = (String) getSqlMapClientTemplate().queryForObject("customer.accYearVal", mapmaster);
                System.out.println("accYear:" + accYear);
                mapmaster.put("accYear", accYear);
                //current year end

                String bunkName = customerTO.getBunkName() + "-" + bunkId;
                System.out.println("bunkName =====> " + bunkName);

                int VendorLedgerId = (Integer) getSqlMapClientTemplate().insert("customer.insertVendorLedger", mapmaster);
                System.out.println("VendorLedgerId......." + VendorLedgerId);
                if (VendorLedgerId != 0) {
                    mapmaster.put("ledgerId", VendorLedgerId);
                    mapmaster.put("bunkId", bunkId);
                    System.out.println("map update ::::=> " + mapmaster);
                    status = (Integer) getSqlMapClientTemplate().update("customer.updateBunkInfo", mapmaster);
                }
            }
            //Ledger Code end

            //bunkid = (Integer) getSqlMapClientTemplate().queryForObject("customer.getBunkselectedList", mapmaster);
            System.out.println("bunkid..." + bunkId);
            mapdetail.put("bunkid", bunkId);
            mapdetail.put("fueltype", customerTO.getFuelType());
            mapdetail.put("currentrate", customerTO.getCurrRate());
            mapdetail.put("bunkremarks", customerTO.getRemarks());
            mapdetail.put("act_ind", customerTO.getBunkStatus());
            mapdetail.put("createdby", userId);

            System.out.println("test2" + status);
            status = (Integer) getSqlMapClientTemplate().update("customer.insertBunkdetails", mapdetail);
            System.out.println("status details is" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS, "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public ArrayList getBunkalterList(String bunkId) {
        Map map = new HashMap();
        ArrayList bunkList = new ArrayList();
        try {
            if (bunkId != null) {
                map.put("bunkid", bunkId);
            }
            bunkList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getalterBunk", map);

            System.out.println("bunkList size=" + bunkList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "bunkList", sqlException);
        }
        return bunkList;
    }

    public int doUpdateBunk(CustomerTO customerTO, int userId, String bunkId) {
        Map mapmaster = new HashMap();
        Map mapdetail = new HashMap();
        Map map = new HashMap();
        ArrayList bunkmasterList = new ArrayList();
        int status = 0;
        mapmaster.put("bunkname", customerTO.getBunkName());
        mapmaster.put("location", customerTO.getCurrlocation());
        mapmaster.put("state", customerTO.getBunkState());
        mapmaster.put("act_ind", customerTO.getBunkStatus());
        mapmaster.put("createdby", userId);
        mapmaster.put("bunkid", bunkId);

        try {
            System.out.println("test1=" + status);
            status = (Integer) getSqlMapClientTemplate().update("customer.updateBunkmaster", mapmaster);
            System.out.println("status master is=" + status);
            System.out.println("bunkid..." + bunkId);
            mapdetail.put("bunkid", bunkId);
            mapdetail.put("fueltype", customerTO.getFuelType());
            mapdetail.put("currentrate", customerTO.getCurrRate());
            mapdetail.put("bunkremarks", customerTO.getRemarks());
            mapdetail.put("act_ind", customerTO.getBunkStatus());
            mapdetail.put("createdby", userId);

            System.out.println("\nbunkid.." + bunkId);
            System.out.println("\ncustomerTO.getFuelType()=" + customerTO.getFuelType());
            System.out.println("\ncustomerTO.getCurrRate()=" + customerTO.getCurrRate());
            System.out.println("\ncustomerTO.getRemarks()=" + customerTO.getRemarks());
            System.out.println("\ncustomerTO.getBunkStatus()=" + customerTO.getBunkStatus());
            System.out.println("\ncreatedby=" + userId);
            status = (Integer) getSqlMapClientTemplate().update("customer.updateBunkdetails", mapdetail);
            System.out.println("status details is" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS, "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public ArrayList getPackingList() {
        Map map = new HashMap();
        ArrayList packList = new ArrayList();
        try {
            packList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getPackingList", map);
            System.out.println("packList size=" + packList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "customerList", sqlException);
        }
        return packList;
    }

    public String getToDestination(String destination) {
        Map map = new HashMap();
        map.put("destination", destination);
        String suggestions = "";
        CustomerTO custTO = new CustomerTO();
        try {
            ArrayList destinations = new ArrayList();
            System.out.println("map = " + map);
            destinations = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getDestinations", map);
            Iterator itr = destinations.iterator();
            while (itr.hasNext()) {
                custTO = new CustomerTO();
                custTO = (CustomerTO) itr.next();
                suggestions = custTO.getToLocation() + "~" + suggestions;
                System.out.println("suggestions: " + suggestions);
            }
            if ("".equals(suggestions)) {
                suggestions = "no match found";
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getToDestination Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getToDestination", sqlException);
        }
        return suggestions;
    }

    public ArrayList getBillingTypeList() {
        Map map = new HashMap();
        ArrayList billingTypeList = new ArrayList();
        try {
            billingTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getBillingTypeList", map);
            System.out.println("billingTypeList =" + billingTypeList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBillingTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getBillingTypeList", sqlException);
        }
        return billingTypeList;
    }
    public ArrayList getAccountManagerList() {
        Map map = new HashMap();
        ArrayList accountManagerList = new ArrayList();
        try {
            accountManagerList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getAccountManagerList", map);
            System.out.println("billingTypeList =" + accountManagerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBillingTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getBillingTypeList", sqlException);
        }
        return accountManagerList;
    }

    public ArrayList getCustomerGroupList() {
        Map map = new HashMap();
        ArrayList result = new ArrayList();
        try {
            result = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCustomerGroupList", map);
            System.out.println("getCustomerGroupList =" + result.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerGroupList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getCustomerGroupList", sqlException);
        }
        return result;
    }

    public String getCustomerCode(CustomerTO customerTO, int insertContractRouteMater) {
        Map map = new HashMap();
        String custCode = "";
        map.put("customerId", customerTO.getCustomerId());
        try {
            custCode = (String) getSqlMapClientTemplate().queryForObject("customer.getCustomerCode", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerCode Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerCode", sqlException);
        }

        return custCode;
    }

    public ArrayList getCustomerName(CustomerTO customerTO) {
        Map map = new HashMap();
        ArrayList getCustomerName = new ArrayList();
        map.put("custName", customerTO.getCustName() + "%");
        try {
            System.out.println("getCustomerName map" + map);
            getCustomerName = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCustomerName", map);
            System.out.println("getCustomerName =" + getCustomerName.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getCustomerName", sqlException);
        }
        return getCustomerName;
    }

    public ArrayList getCustomerCodes(CustomerTO customerTO) {
        Map map = new HashMap();
        ArrayList getCustomerCodes = new ArrayList();
        map.put("customerCode", customerTO.getCustomerCode() + "%");
        try {
            System.out.println("getCustomerCode map" + map);
            getCustomerCodes = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCustomerCodes", map);
            System.out.println("getCustomerCodes =" + getCustomerCodes.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerCodes Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getCustomerCodes", sqlException);
        }
        return getCustomerCodes;
    }

    public ArrayList getCustomerDetails(CustomerTO customerTO) {
        Map map = new HashMap();
        //         CustomerTO customerTO = new CustomerTO();
        ArrayList getCustomerDetails = new ArrayList();
        map.put("customerId", customerTO.getCustomerId());

        try {
            System.out.println("getCustomerDetails map" + map);
            getCustomerDetails = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCustomerDetails", map);
            System.out.println("getCustomerGroupList =" + getCustomerDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerGroupList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getCustomerGroupList", sqlException);
        }
        return getCustomerDetails;
    }
    
    public ArrayList getCustomerCreditLimitDetails(CustomerTO customerTO) {
        Map map = new HashMap();
        //         CustomerTO customerTO = new CustomerTO();
        ArrayList getCustomerDetails = new ArrayList();
        map.put("customerId", customerTO.getCustomerId());

        try {
            System.out.println("getCustomerDetails map" + map);
            getCustomerDetails = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCustomerCreditLimitDetails", map);
            System.out.println("getCustomerGroupList =" + getCustomerDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerGroupList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getCustomerGroupList", sqlException);
        }
        return getCustomerDetails;
    }

//    public ArrayList getCustomerLists(CustomerTO customerTO) {
//        Map map = new HashMap();
//        ArrayList customerList = new ArrayList();
//        map.put("customerId", customerTO.getCustomerId());
//        map.put("secondaryContractSatus", customerTO.getContractType());
//        if ("1".equals(customerTO.getCustomerType())) {
//            map.put("primaryCustomerType", customerTO.getCustomerType());
//        } else if ("2".equals(customerTO.getCustomerType())) {
//            map.put("secondaryCustomerType", customerTO.getCustomerType());
//        }
//
//        map.put("roleId", customerTO.getRoleId());
//        map.put("userId", customerTO.getUserId());
//        String empId = (String) getSqlMapClientTemplate().queryForObject("customer.getEmployeeId", map);
//        map.put("empId", empId);
//
//        System.out.println("map size=" + map);
//        try {
//            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCustomerList", map);
//            System.out.println("customerList size=" + customerList.size());
//        } catch (Exception sqlException) {
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-CUST-01", CLASS, "customerList", sqlException);
//        }
//        return customerList;
//    }

    public int updateCustomer(CustomerTO customerTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int insertContractRouteMater = 0;
        map.put("customerId", customerTO.getCustomerId());
        String custCode = "";
        String[] temp1 = null;
        custCode = (String) getSqlMapClientTemplate().queryForObject("customer.getCustomerCode", map);
        if (custCode == null) {
            custCode = "CC-0001";
        } else {
            custCode = "CC-0001";
        }
        map.put("custCode", custCode);
        map.put("paymentType", customerTO.getPaymentType());
        map.put("Code", customerTO.getCustomerCode());
        map.put("Name", customerTO.getCustomerName());
        map.put("billingTypeId", customerTO.getBillingTypeId());
        String add=customerTO.getCustAddress();
        String Address = add.replaceAll("\\r\\n|\\r|\\n\\t\\b|\\t|\\b\\s", "");
        map.put("Address", Address);
        
        String add2=customerTO.getCustAddresstwo();
        String Address2 = add2.replaceAll("\\r\\n|\\r|\\n\\t\\b|\\t|\\b\\s", "");
      
        map.put("Addresstwo", Address2);
        String add3=customerTO.getCustAddressthree();
        String Address3 = add3.replaceAll("\\r\\n|\\r|\\n\\t\\b|\\t|\\b\\s", "");
        map.put("Addressthree", Address3);
        map.put("City", customerTO.getCustCity());
        map.put("Contact", customerTO.getCustContactPerson());
        map.put("State", customerTO.getCustState());
        map.put("Phone", customerTO.getCustPhone());
        map.put("Mobile", customerTO.getCustMobile());
        map.put("Email", customerTO.getCustEmail());
        map.put("creditDays", customerTO.getCreditDays());
        map.put("accountManagerId", customerTO.getAccountManagerId());
        map.put("secondaryBillingTypeId", customerTO.getSecondaryBillingTypeId());
         String billNameAdd=customerTO.getBillingNameAddress();
        String billNameAdds = billNameAdd.replaceAll("\\r\\n|\\r|\\n\\t\\b|\\t|\\b\\s", "");
        map.put("billingNameAddress", billNameAdds);
        map.put("customerTypeId", 1);
        map.put("customerGroupId", customerTO.getCustomerGroupId());
        map.put("activeInd", customerTO.getCustStatus());
        map.put("userId", userId);
        map.put("erpId", customerTO.getErpId());
        map.put("panNo", customerTO.getPanNo());
        map.put("gstNo", customerTO.getGstNo());
        map.put("stateId", customerTO.getStateId());
        map.put("organizationId", customerTO.getOrganizationId());
        map.put("companyType", customerTO.getCompanyType());
        map.put("custTypeId", customerTO.getCustTypeId());
        map.put("billingLocation", customerTO.getCustCity());
        map.put("pinCode", customerTO.getPinCode());
        map.put("gtaType", customerTO.getGtaType());
        System.out.println("customerTO.getPinCode()#####"+customerTO.getPinCode());
        
            if("".equalsIgnoreCase(customerTO.getAdvanceAmount()) || customerTO.getAdvanceAmount() == null){
                 map.put("advanceAmount", "0");
            }else{
            map.put("advanceAmount", customerTO.getAdvanceAmount());
            }
             
            if("".equalsIgnoreCase(customerTO.getCreditLimit()) || customerTO.getCreditLimit() == null){
                 map.put("creditLimit", "0");
            }else{
                map.put("creditLimit", customerTO.getCreditLimit());
            }
            
            if("".equalsIgnoreCase(customerTO.getFixedCreditLimit()) || customerTO.getFixedCreditLimit() == null){
                 map.put("fixedCreditLimit", "0");
            }else{
            map.put("fixedCreditLimit", customerTO.getFixedCreditLimit());
            }
      
        System.out.print("customerTO.getCustName()" + customerTO.getCustName());
        System.out.print("Update====" + map);
        try {
            System.out.println("test1" + status);

            
            status = (Integer) getSqlMapClientTemplate().update("customer.updateEditCustomerEInvoice", map);
            System.out.println("status updateEditCustomerEInvoice is is" + status);
            
            status = (Integer) getSqlMapClientTemplate().update("customer.updateEditCustomerESuppInvoice", map);
            System.out.println("status updateEditCustomerESuppInvoice is is" + status);
            
            status = (Integer) getSqlMapClientTemplate().update("customer.updateEditCustomerECreditInvoice", map);
            System.out.println("status updateEditCustomerECreditInvoice is is" + status);
            
            status = (Integer) getSqlMapClientTemplate().update("customer.updateEditCustomerECreditSuppInvoice", map);
            System.out.println("status updateEditCustomerECreditInvoice is is" + status);
            
            status = (Integer) getSqlMapClientTemplate().update("customer.updateEditCustomer", map);
            System.out.println("status customer details is is" + status);
            
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }
    public int editUpdateCustomerCredit(CustomerTO customerTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int insertContractRouteMater = 0;
        map.put("userId", userId);
        map.put("customerType", customerTO.getCustomerType());
        map.put("customerId", customerTO.getCustomerId());
        map.put("advanceAmount", customerTO.getAdvanceAmount());
        
        if("1".equals(customerTO.getCustomerType())){
            map.put("availAmount", customerTO.getFixedCreditLimit());
        }else{  
            map.put("availAmount", "0");
        }
        
        map.put("fixedCreditLimit", customerTO.getFixedCreditLimit());
        map.put("creditLimit", customerTO.getCreditLimit());
        map.put("usedLimit", customerTO.getUsedLimit());
        map.put("creditDays", customerTO.getCreditDays());
        System.out.print("customerTO.getCustName()" + customerTO.getCustName());
        System.out.print("Update====" + map);
        try {
            System.out.println("test1" + status);

            status = (Integer) getSqlMapClientTemplate().update("customer.insertCustomerCredit", map);
            
            status = (Integer) getSqlMapClientTemplate().update("customer.updateEditCustomerCredit", map);
            
            System.out.println("status customer details is is" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }
public int checkCustomerName(CustomerTO customerTO) {
        Map map = new HashMap();
        int customerName = 0;
        map.put("customerName", customerTO.getCustomerName());
        try {
            customerName = (Integer) getSqlMapClientTemplate().queryForObject("customer.checkCustomerName", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkCustomerName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkCustomerName", sqlException);
        }

        return customerName;
    }

public ArrayList getModelFuelList(String vehicleTypeId,String contractRateId) {
        Map map = new HashMap();
//        String modelFuelIds = "";
        String checkContractId = "";
        ArrayList getModelFuelList = new ArrayList();
        try {
            map.put("vehicleTypeId",vehicleTypeId);
            map.put("contractRateId",contractRateId);
            System.out.println("map@@List"+map);
            checkContractId = (String) getSqlMapClientTemplate().queryForObject("customer.checkContractId", map);
            System.out.println("checkContractId" + checkContractId);
            if("0".equals(checkContractId)){
            getModelFuelList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getModelList", map);
            System.out.println("getModelList =" + getModelFuelList.size());
            }else{
            getModelFuelList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getModelFuelList", map);
            System.out.println("getModelFuelList =" + getModelFuelList.size());
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getModelFuellist Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getModelFuelList", sqlException);
        }
        return getModelFuelList;
    }

     public int saveModelFuelDetails(String[] fuelTypeId,String[] fuelModelId,String contractRateId,String[] modelId,String[] fuelVehicle,String[] fuelDG,String[] totalFuel,Integer userId) {
        Map map = new HashMap();
        Integer insertStatus = 0;
        Integer updateStatus = 0;
        String modelFuelIds = "";
        int response = 0;

        map.put("userId", userId);
        System.out.println("map@@@@" + map);
        System.out.println("modelId.length;" + modelId.length);
        try {
            for (int i = 0; i < modelId.length; i++) {
//                map.put("fuelTypeId", fuelTypeId[i]);
                map.put("fuelModelId", fuelModelId[i]);
                map.put("modelId", modelId[i]);
                map.put("fuelVehicle", fuelVehicle[i]);
                map.put("fuelDG", fuelDG[i]);
                map.put("totalFuel", totalFuel[i]);
                map.put("contractRateId", contractRateId);
                System.out.println("map" + map);
                System.out.println("fuelModelId[i]"+fuelModelId[i]);
                if ("0".equals(fuelModelId[i]) || "".equals(fuelModelId[i])) {
                    System.out.println("map@@@" + map);
                    insertStatus = (Integer) getSqlMapClientTemplate().update("customer.saveModelFuelDetails", map);
                    System.out.println("insertStatus@@" + insertStatus);
                    response=1;
                } else{
                    System.out.println("map###" + map);
                    updateStatus = (Integer) getSqlMapClientTemplate().update("customer.updateModelFuelDetails", map);
                    System.out.println("updateStatus@@" + updateStatus);
                    response=2;
                    }
            }
//            }

            //System.out.println("checkRouteStage " + checkRouteStage);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertStatus", sqlException);
        }

        return response;
    }
     
public ArrayList getStateList() {
        Map map = new HashMap();
        //         CustomerTO customerTO = new CustomerTO();
        ArrayList getStateList = new ArrayList();

        try {
            System.out.println("getStateList map" + map);
            getStateList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getStateList", map);
            System.out.println("getStateList =" + getStateList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerGroupList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getCustomerGroupList", sqlException);
        }
        return getStateList;
    }
public ArrayList getOrganizationList() {
        Map map = new HashMap();
        //         CustomerTO customerTO = new CustomerTO();
        ArrayList getOrganizationList = new ArrayList();

        try {
            System.out.println("getOrganizationList map" + map);
            getOrganizationList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getOrganizationList", map);
            System.out.println("getOrganizationList =" + getOrganizationList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrganizationList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getOrganizationList", sqlException);
        }
        return getOrganizationList;
    }
   public int checkGstNo(String gstNo) {
        Map map = new HashMap();
        int gst = 0;
        map.put("gstNo", gstNo);
        System.out.println("map for gst count:"+map);
        try {
            if(gstNo!= null &&!"".equals(gstNo)){
            gst = (Integer) getSqlMapClientTemplate().queryForObject("customer.checkGstNo", map);
            }
            System.out.println("gst count:"+gst);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkCustomerName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkCustomerName", sqlException);
        }

        return gst;
    }
   
   
      public ArrayList getTripCustomerList() {
        Map map = new HashMap();
        ArrayList customerList = null;
        try {
            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getCustomerList", map);
            //////System.out.println("getCustomerList.size() = " + customerList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerList List", sqlException);
        }
        return customerList;
    }
//Customer master changes
      
       public ArrayList getCustomerLists(CustomerTO customerTO) {
        Map map = new HashMap();
        ArrayList customerList = new ArrayList();
        map.put("customerCode",customerTO.getCustomerCode());
        map.put("customerId", customerTO.getCustomerId());
        map.put("secondaryContractSatus", customerTO.getContractType());
        if ("1".equals(customerTO.getCustomerType())) {
            map.put("primaryCustomerType", customerTO.getCustomerType());
        } else if ("2".equals(customerTO.getCustomerType())) {
            map.put("secondaryCustomerType", customerTO.getCustomerType());
        }

        map.put("roleId", customerTO.getRoleId());
        map.put("userId", customerTO.getUserId());
        String empId = (String) getSqlMapClientTemplate().queryForObject("customer.getEmployeeId", map);
        map.put("empId", empId);

        System.out.println("map size=" + map);
        try {
            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCustomerList", map);
            System.out.println("customerList size=" + customerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "customerList", sqlException);
        }
        return customerList;
    }
       
public ArrayList getCustomerAttachments(CustomerTO customerTO) {
        Map map = new HashMap();
        ArrayList statusDetails = new ArrayList();
        try {
            map.put("customerId", customerTO.getCustomerId());
            System.out.println(" Trip map is::" + map);
            statusDetails = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCustomerAttachments", map);
            System.out.println("getPODDetails.size() = " + statusDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPODDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPODDetails List", sqlException);
        }
        return statusDetails;
    }



public int saveTripAttachments(String actualFilePath, String customerId1, String podRemarks1, String fileSaved, CustomerTO customerTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        FileInputStream fis = null;
        int customerId = 0;
        try {
                map.put("customerId", customerTO.getCustomerId());

            map.put("podRemarks", podRemarks1);
            map.put("fileName", fileSaved);
            map.put("userId", userId);
            System.out.println("actualFilePath = " + actualFilePath);
            map.put("actualFilePath", actualFilePath);
            File file = new File(actualFilePath);
            System.out.println("file = " + file);
            fis = new FileInputStream(file);
            System.out.println("fis = " + fis);
            byte[] podFile = new byte[(int) file.length()];
            System.out.println("podFile = " + podFile);
            fis.read(podFile);
            fis.close();
            map.put("podFile", null);
            System.out.println("mapp==" + map);
            System.out.println("the saveTripPodDetails123455" + map);
            status = (Integer) getSqlMapClientTemplate().update("customer.customerFilesUpload", map);

            System.out.println("saveTripPodDetails size=" + status);


        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripPodDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripPodDetails List", sqlException);
        }

        return status;
    }

}
