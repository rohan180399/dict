
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>


<script type="text/javascript">


    function replaceSpecialCharacters()
    {
        var content = document.getElementById("requestremarks").value;

        //alert(content.replace(/[^a-zA-Z0-9]/g,'_'));
        // content=content.replace(/[^a-zA-Z0-9]/g,'');
        content = content.replace(/[@&\/\\#,+()$~%'":*?<>{}]/g, ' ');
        document.getElementById("requestremarks").value = content;
        //alert("Repalced");

    }

    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#consignorName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getConsignorName.do",
                    dataType: "json",
                    data: {
                        consignorName: request.term,
                        customerId: document.getElementById('customerId').value

                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }

                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                $('#consignorName').val(value);
                $('#customerId').val(ui.item.custId);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });
</script>

<script language="javascript">
    function  searchGr() {
        document.approve.action = '/throttle/cancelBlockedGr.do';
        document.approve.submit();
    }
</script>
<script language="javascript">
    function submitPage() {

        document.approve.action = '/throttle/saveCancelledGr.do';
        document.approve.submit();

    }

    function setActiveInd(sno) {
        if (document.getElementById("checkStatus" + sno).checked) {
            document.getElementById("activeInd" + sno).value = 'Y';
        } else {
            document.getElementById("activeInd" + sno).value = "N";
        }
    }


</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Operation</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Operation</a></li>
            <li class="active">Cancel Blocked Grs</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body >
                <!--&emsp;&emsp; Operation >> Cancel Blocked Grs-->
                <form name="approve"  method="post" >
                    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
                    <%@ include file="/content/common/message.jsp" %>
                    <table class="table table-info mb30 table-hover" style="width:50%">
                         <tr>
                            <td  style="padding-right: 29px;"><font color="red">*</font>Customer Name</td>
                            <td  style="padding-right: 29px;"><input type="hidden" name="customerId" id="customerId" value="<c:out value="${customerId}"/>" class="form-control" style="width:240px;height:40px;" />
                                <input type="text" name="consignorName" onKeyPress="return onKeyPressBlockNumbers(event);" class="form-control" style="width:240px;height:40px;"  id="consignorName" value="<c:out value="${customerName}"/>"/></td>
                            <td><input type="button" class="btn btn-info" value="Search" onclick="searchGr()"  class="btn btn-info" /></td>
                        </tr>

                    </table>

                    <c:if test="${blockedGRDeatils != null}">
                        <table class="table table-info mb30 table-hover"  id="table" style="width:1000px;">
                        <thead height="30">
                            <tr>
                            <th  >SNo</th>
                            <th  >Block Date</th>
                            <th >Block GR No </th>
                            <th  align="center">Edit </th>
                            </tr>
                            </thead>
                            <tbody>
                                <%int sno=0;%>
                                <c:forEach items="${blockedGRDeatils}" var="tripGr">
                                    <%
                                                    sno++;
                                                    String className = "text1";
                                                    if ((sno % 1) == 0) {
                                                        className = "form-control";
                                                    } else {
                                                        className = "form-control";
                                                    }
                                    %>


                                    <tr>
                                        <td><%=sno%></td>
                                        <td>
                                            <c:out value="${tripGr.grDate}"/></td>
                                        <td>
                                            <c:out value="${tripGr.grNo}"/>
                                        </td>
                                        <td>
                                            <input type="checkbox" id="checkStatus<%=sno%>" name="checkStatus" onclick="setActiveInd(<%=sno%>);" />
                                        </td>
                                    </tr>
                                <input type="hidden" id="grNo<%=sno%>" name="grNo" value='' />
                                <input type="hidden" id="grId<%=sno%>" name="grId" value='<c:out value="${tripGr.grId}"/>' />
                                <input type="hidden" id="activeInd<%=sno%>" name="activeInd" value='N' />
                                <input type="hidden" name="count" id="count" value="<%=sno%>" />
                            </c:forEach>
                            
                            <tr >
                                <td align="right">Remarks</td>
                                <td colspan="2" align="left">
                                    <textarea rows="2" cols="30" class="form-control" name="remarks" id="remarks"   style="width:200px"></textarea>
                                </td>
                                <td align="left"><input type="button" value="Save" onclick="submitPage()"  class="btn btn-info" /> </td>
                            </tr>
                            </tbody>
                        </table>                        
                        <center></center>
                        </c:if>
                         <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 1);
                    </script>
                    <br/>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>