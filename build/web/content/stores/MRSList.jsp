<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
     <%@page language="java" contentType="text/html; charset=UTF-8"%>
 <%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
                <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.mrs.business.MrsTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script>
        <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>


        <link rel="stylesheet" href="/throttle/css/jquery-ui.css">
        <script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
        <script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>




<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
                    format: "dd-mm-yyyy",
                    autoclose: true,
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
                    format: "dd-mm-yyyy",
                    autoclose: true
        });

    });


</script>




<script>
    function addRow(val) {
        //if(document.getElementById(val).innerHTML == ""){
        document.getElementById(val).innerHTML = "<input type='text' size='7' class='form-control' >";
        //}else{
        //document.getElementById(val).innerHTML = "";
        //}
    }

    function showTable()
    {
        var selectedMrs = document.getElementsByName("selectedIndex");
        var counter = 0;
        for (var i = 0; i < selectedMrs.length; i++) {
            if (selectedMrs[i].checked == 1) {
                counter++;
                break;
            }
        }
        if (counter == 0) {
            alert("Please Select MRS");
        } else {
            document.spareIssue.action = "/throttle/mrsSummary.do";
            document.spareIssue.submit();
        }
    }
    function newWO() {
        window.open('/throttle/content/stores/OtherServiceStockAvailability.html', 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
    }

    function submitPag(val) {
        if (validate() == '0') {
            return;
        } else if (val == 'purchaseOrder') {
            document.spareIssue.purchaseType.value = "1012"
            document.spareIssue.action = '/throttle/generateMpr.do'
            document.spareIssue.submit();
        } else if (val == 'localPurchase') {
            document.spareIssue.purchaseType.value = "1011"
            document.spareIssue.action = '/throttle/generateMpr.do'
            document.spareIssue.submit();
        }
    }



    function searchSubmit() {

        if (document.spareIssue.toDate.value == '') {
            alert("Please Enter From Date");
        } else if (document.spareIssue.fromDate.value == '') {
            alert("Please Enter to Date");
        }
        document.spareIssue.action = '/throttle/MRSList.do'
        document.spareIssue.submit();


    }


    function validate()
    {
        var selectedMrs = document.getElementsByName("selectedIndex1");
        var reqQtys = document.getElementsByName("reqQtys");
        var vendorIds = document.getElementsByName("vendorIds");
        var counter = 0;
        for (var i = 0; i < selectedMrs.length; i++) {
            if (selectedMrs[i].checked == 1) {
                counter++;
                if (parseFloat(reqQtys[i].value) == 0 || isFloat(reqQtys[i].value)) {
                    alert("Please Enter Valid Required Quantity");
                    reqQtys[i].select();
                    reqQtys[i].focus();
                    return '0'
                }
                if (vendorIds[i].value == '0') {
                    alert("Please Select Vendor");
                    vendorIds[i].focus();
                    return '0'
                }
            }
        }
        if (counter == 0) {
            alert("Please Select Atleast one item");
            return '0';
        }
        return counter;
    }


    function selectBox(val) {
        var checkBoxs = document.getElementsByName("selectedIndex1");
        checkBoxs[val].checked = 1;
    }

    function setDate(fDate, tDate) {
        if (fDate != 'null') {
            document.spareIssue.fromDate.value = fDate;
        }
        if (tDate != 'null') {
            document.spareIssue.toDate.value = tDate;
        }

    }


    <%--function setServicePoint()
    {    
        if('<%=request.getAttribute("compId")%>' !=null)
            {            
                document.spareIssue.compId.value= '<%=request.getAttribute("compId")%>';
            }
    }--%>
</script>


<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>


<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.ViewMRS"  text="MRS"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.stores"  text="Stores"/></a></li>
            <li class="active"><spring:message code="service.label.ViewMRS"  text="MRS"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body onLoad="setDate('<%= request.getAttribute("fromDate") %>', '<%= request.getAttribute("toDate") %>');">

                <form name="spareIssue"  method="post" >
                            <table class="table table-info mb30 table-hover">
                                <thead>
                                    <tr>
                                        <th colspan="8"><spring:message code="stores.label.ViewMRS"  text="default text"/></th>
                                    </tr>
                                </thead>
                   
                                <tr>
                                    <td ><spring:message code="stores.label.FromDate"  text="default text"/>
                                    </td>
                                    <td >
                                        <input autocomplete="off" type="text"  style="width:260px;height:40px;" class="form-control pull-right datepicker"  name="fromDate" value="" >
                                        <!--                        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.spareIssue.fromDate,'dd-mm-yyyy',this)"/>-->
                                    </td>
                                    <td ><spring:message code="stores.label.TODate"  text="default text"/></td>
                                    <td>
                                        <input type="text"  style="width:260px;height:40px;"  class="form-control pull-right "  readonly name="toDate" value="" >
                                        <!--                        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.spareIssue.toDate,'dd-mm-yyyy',this)"/>-->
                                    </td>
                                    <td></td><td></td><td></td><td></td>

                                </tr>
                                <tr>
                                    <td></td>
                                <!--<center>-->
                                    
                                    <td colspan="4" align="center">
                                        <input type="button" class="btn btn-success" readonly name="search" value="<spring:message code="stores.label.SEARCH"  text="default text"/>" onClick="searchSubmit();" >
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    
                                    <!--</center>-->
                                </tr>

                            </table>
                       

                    <% int index=0;
                        String classText = "";
                        int oddEven =0;
                    %>
                    <c:if test = "${mrsLists != null}" >


                        <table class="table table-info mb30 table-hover" id="table">
                            <thead>

                                <tr>
                                    <th ><spring:message code="stores.label.QueueNo"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.JobCardNo"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.WorkOrderNo"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.CounterSaleNo"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.MRSNo"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.VehicleNo"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.CreatedDate/Time"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.ApprovalStatus"  text="default text"/></th>
                                    <th >&nbsp;</th>
                                    <th ><spring:message code="stores.label.StockDetails"  text="default text"/></th>
                                </tr>
                            </thead>
                            <c:forEach items="${mrsLists}" var="mrs">
                                <%

                                    oddEven = index % 2;
                                    if (oddEven > 0) {
                                    classText = "text2";
                                    } else {
                                    classText = "text1";
                                    }
                                %>
                                <tr>
                                    <td ><%= index+100 %></td>
                                    <td ><c:out value="${mrs.mrsJobCardNumber}"/></td>
                                    <td ><c:out value="${mrs.rcWorkId}"/></td>
                                    <c:if test="${mrs.counterId!=0 && mrs.nettAmount!=0.00}">
                                        <td ><a href="/throttle/generateMrsGdn.do?counterId=<c:out value="${mrs.counterId}"/>&reqFor=print"><c:out value="${mrs.counterId}"/></a></td>
                                        </c:if>
                                        <c:if test="${mrs.counterId==0 || mrs.nettAmount==0.00}">
                                        <td ><c:out value="${mrs.counterId}"/></td>
                                    </c:if>
                                    <td ><a href="/throttle/ViewMRSDetail.do?mrsId=<c:out value="${mrs.mrsNumber}"/>&counterId=<c:out value="${mrs.counterId}"/>" ><c:out value="${mrs.mrsNumber}"/></a></td>
                                    <td ><c:out value="${mrs.mrsVehicleNumber}"/></td>
                                    <td ><c:out value="${mrs.mrsCreatedDate}"/></td>
                                    <td ><c:out value="${mrs.mrsApprovalStatus}"/></td>
                                    <c:if test="${mrs.nettAmount!=0.00}">
                                        <td >Issue</td>
                                    </c:if>

                                    <c:if test="${mrs.nettAmount==0.00}">
                                        <td align="right"><a href="/throttle/handleViewMrsIssue.do?mrsId=<c:out value="${mrs.mrsNumber}"/>&counterId=<c:out value="${mrs.counterId}"/>" ><spring:message code="stores.label.Issue"  text="default text"/></a></td>
                                    </c:if>

                                    <td align="right"><input type="checkbox" name='selectedIndex' value=<%=index %> ></td>
                                <input type="hidden" name="mrsIds" value=<c:out value="${mrs.mrsNumber}"/> >
                                </tr>
                                <%
                                  index++;
                                %>
                            </c:forEach>

                        </table>
                        <br>
                        <center> <input type="button" value="<spring:message code="stores.label.SHOW"  text="default text"/>" onClick="showTable();"  class="btn btn-success" >&nbsp;</center>
                        </c:if>
                    <br>


                    <c:if test="${ mrsSummaryList!=null }" >
                        <table class="table table-info mb30 table-hover" id="table">
                            <thead>
                                <tr>
                                    <th colspan="12" align="center" ><spring:message code="stores.label.RequiredItemsInfo"  text="default text"/> </th>
                                </tr>
                                <tr>
                                    <th ><spring:message code="stores.label.Item"  text="default text"/> <br><spring:message code="stores.label.Code"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.PAPL"  text="default text"/> <br><spring:message code="stores.label.Code"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.Item"  text="default text"/> <br><spring:message code="stores.label.Name"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.New"  text="default text"/> <br><spring:message code="stores.label.Stock"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.RC"  text="default text"/> <br><spring:message code="stores.label.Stock"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.OtherServPoint"  text="default text"/><br><spring:message code="stores.label.Stock"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.Required"  text="default text"/><br><spring:message code="stores.label.Qty"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.PORaised"  text="default text"/><br><spring:message code="stores.label.Qty"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.Local"  text="default text"/><br><spring:message code="stores.label.PurchQty"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.Vendors"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.Quantity"  text="default text"/></th>
                                    <th >&nbsp;</th>
                                    <th >&nbsp;</th>
                                </tr>
                            </thead>
                            <%  index = 0; %>

                            <c:forEach items="${mrsSummaryList}" var="mrsSumm">
                                <%
                                    oddEven = index % 2;
                                    if (oddEven > 0) {
                                    classText = "text2";
                                    } else {
                                    classText = "text1";
                                    }
                                %>

                                <tr>
                                <input type="hidden" name="itemIds" value=<c:out value="${mrsSumm.mrsItemId}" /> >
                                <td > <c:out value="${mrsSumm.mrsItemMfrCode }" /> </td>
                                <td ><c:out value="${mrsSumm.mrsPaplCode}" /></td>
                                <td ><c:out value="${mrsSumm.mrsItemName}" /></td>
                                <td ><c:out value="${mrsSumm.mrsLocalServiceItemSum}" /></td>
                                <td ><c:out value="${mrsSumm.rcQty}" /></td>
                                <td ><c:out value="${mrsSumm.mrsOtherServiceItemSum}" /></td>
                                <td ><c:out value="${mrsSumm.requiredQty}" /></td>
                                <td ><c:out value="${mrsSumm.vendorPoQty}" /></td>
                                <td ><c:out value="${mrsSumm.localPoQty}" /></td>
                                <td > <select name="vendorIds" class="form-control">
                                        <c:if test = "${vendorList != null}" >
                                            <option value='0' >--select--</option>
                                            <c:forEach items="${vendorList}" var="vend">
                                                <c:out value="${vend.vendorId}" />
                                                <c:if test="${vend.itemId==mrsSumm.mrsItemId}" >
                                                    <option value=<c:out value="${vend.vendorId}" /> > <c:out value="${vend.vendorName}" />  </option>
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                    </select>
                                </td>
                                <td ><input type="text" name="reqQtys" size="5" class="form-control" value="" onChange="selectBox(<%= index %>);"  > </td>
                                <td ><input type="checkbox" name="selectedIndex1" class="form-control" value="<%= index %>"   > </td>
                                </tr>
                                <% index++; %>
                            </c:forEach>

                        </table>

                        <input type="hidden" name="purchaseType" value="" >
                        <br>
                    </c:if>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>


