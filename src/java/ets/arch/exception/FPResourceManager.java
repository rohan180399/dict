/*---------------------------------------------------------------------------
 * FPResourceManager.java
 * Jan 26, 2008
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
 -----------------------------------------------------------------------------*/

package ets.arch.exception;
/**********************************************************************
 * Modification Log
 * ---------------------------------------------------------------------
 * Ver   Date              Modified By               Description
 * ---------------------------------------------------------------------
 * 1.0   26 Jan 2008       Srinivasan.R             Initial Version
 *
 ***********************************************************************/
import java.net.URL;
import java.util.ResourceBundle;
/**
 * FPResourceManager:<p>Class that helps to get the value from the property file</p>
 * A property file is used to store values which should not be hard coded
 * in the classes. <br> eg: All the names of mapping xmls and the root element
 * of the service are present in FP.properties. <BR>
 * All oracle SQLs are present in query.properties.
 * @author Srinivasan.R
 * @version 1.0 26 Jan 2008
 * @since   Event Portal Iteration 0
 */
public class FPResourceManager{


  //variable to hold the value of the FPResourceManager.
   private static FPResourceManager instance;

   /**
    * CLASS_NAME - constant to hold the class name
    */
   public static final String CLASS_NAME = "FPResourceManager";
	/**
	 * PROPERTIES - constant to hold the property value
	 */

   public static final String PROPERTIES = "parveen";
	/**
	 * PROPERTY_FILE_PATH - constant to hold the property file path's value
	 */

   public static final String PROPERTY_FILE_PATH = "";

   private ResourceBundle globalProperties;

   private static String realPath = "";

    /**
     * <p>Private constructor</p>
     */

   private FPResourceManager()
    {

   }
    /**
     * <p>Gets the instance of the FPResourceManager</p>
     * This method when called for the first time creates an instance of
     * FPResourceManager. The method loads the properties for a globally
     * declared properties file.
     * @return instance The instance of the class FPResourceManager.
     */

   public static FPResourceManager getInstance() {
      if(instance == null) {
         instance = new FPResourceManager();
         instance.loadGlobalProperties();
      }
      return instance;
   }

   /**
    * <p>Get the absolute path of a properties file</p>
    *
    * @param None
    * @return realPath The path of PROPERTY_FILE_PATH.
    */

   public String getRealPath() {

      // get the URL of the file.
      URL url = getClass().getClassLoader().getResource(PROPERTY_FILE_PATH);
      // get the real path of the file.
      realPath = url.getPath();
      realPath = realPath.replace('\\', '/');
      return realPath;
   }
   /**
    * <p>Gets the absolute path of the given properties file</p>
    * This is the method commonly used. When there are more than
    * one property file, the path for the passed property file is retrieved.
    * @param property The file for which the path is retrieved.
    * @return realPath The path of the passed property file.
    */
  public String getRealPath(String property) {

      // get the URL of the file.
      URL url = getClass().getClassLoader().getResource(property);
      // get the real path of the file.
      realPath = url.getPath();
      realPath = realPath.replace('\\', '/');
      return realPath;
   }

   /**
    * <p>Loads the Global Properties</p>
    * This loads the properties for the given path into the properties variable.
    *
    */


   public void loadGlobalProperties() {
      try{
         globalProperties = ResourceBundle.getBundle(PROPERTIES);

      }catch (Exception e){
           throw new FPRuntimeException("EM-SYS-08","FPResourceManager",
        		   "loadGlobalProperties",e);

      }

    }


  /**
   * <p>Gets property value from the file</p>
   * @param key This is the key using which an entry in the Properties file is read.
   * @return value The actual value to be used by the calling method.
   */

   public String getProperties(String key) {
      if(globalProperties != null) {
          return globalProperties.getString(key);
      }else{
          return null;
      }
   }

}
