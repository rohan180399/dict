<%-- 
    Document   : alterType
    Created on : Feb 27, 2009, 11:29:00 AM
    Author     : karudaiyar Subramaniam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="ets.domain.vehicle.business.VehicleTO" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>PAPL</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript">
            function submitpage(value)
            {
                var checValidate = selectedItemValidation();
                if(checValidate !='fail' ){
                    document.modify.action='/throttle/modifyRoute.do';
                    document.modify.submit();
                }
            }
            function setSelectbox(i)
            {
                var selected=document.getElementsByName("selectedIndex") ;
                selected[i].checked = 1;
            }
            function selectedItemValidation(){
                var index = document.getElementsByName("selectedIndex");
                var routeCode = document.getElementsByName("routeCodes");
                var fromLocation = document.getElementsByName("fromLocations");
                var toLocation = document.getElementsByName("toLocations");
                var viaRoute = document.getElementsByName("viaRoutes");
                var km = document.getElementsByName("kms");
                var tollAmount = document.getElementsByName("tollAmounts");                
                var driverBata = document.getElementsByName("driverBatas");
                var chec=0;
                for(var i=0;(i<index.length && index.length!=0);i++){
                    if(index[i].checked){
                        chec++;
                        if(textValidation(routeCode[i],"Route Code")){
                            return 'fail';
                        }else if(textValidation(fromLocation[i],"From Location")){
                            return 'fail';
                        }else if(textValidation(toLocation[i],"To Location")){
                            return 'fail';
                        }else if(textValidation(viaRoute[i],"Via Route")){
                            return 'fail';
                        }else if(textValidation(km[i],"KM")){
                            return 'fail';
                        }else if(textValidation(tollAmount[i],"Toll Amount")){
                            return 'fail';
                        }else if(textValidation(driverBata[i],"driver Bata")){
                            return 'fail';
                        }
                    }
                }
                if(chec == 0){
                    alert("Please Select Any One And Then Proceed");
                    return 'fail';
                    //desigName[0].focus();
                    //break;
                }
                //document.modify.action='/throttle/modifyModel.do';
                //document.modify.submit();
                return 'pass';
            }

            function isChar(s){
                if(!(/^-?\d+$/.test(s))){
                    return false;
                }
                return true;
            }
        </script>
    </head>
    <!--[if lte IE 7]>
    <style type="text/css">

    #fixme {display:block;
    top:0px; left:0px;  position:fixed;  }
    </style>
    <![endif]-->

    <!--[if lte IE 6]>
    <style type="text/css">
    body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
    #fixme {display:block;
    top:0px; left:0px;  position:fixed;  }
    * html #fixme  {position:absolute;}
    </style>
    <![endif]-->

    <!--[if lte IE 6]>
    <style type="text/css">
    /*<![CDATA[*/
    html {overflow-x:auto; overflow-y:hidden;}
    /*]]>*/
    </style>
    <![endif]-->

    <body >
        <form method="post" name="modify">
            <!-- copy there from end -->
            <div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
                <div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
                    <!-- pointer table -->
                    <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                        <tr>
                            <td >
                                <%@ include file="/content/common/path.jsp" %>
                            </td></tr></table>
                    <!-- pointer table -->                </div>
            </div>

            <!-- message table -->
            <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                <tr>
                    <td >
                        <%@ include file="/content/common/message.jsp" %>
                    </td></tr></table>
            <!-- message table -->
            <!-- copy there  end -->
            <br><br><br>
            <% int index = 0;%>

            <table width="500" align="center" id="bg" cellpadding="0" cellspacing="0" class="border">
                <c:if test = "${getRouteList != null}" >
                    <tr>
                        <td height="30" class="contentsub"><div class="contentsub">S.No</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">Route Code </div></td>
                        <td class="contentsub" height="30"><div class="contentsub">From Location</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">To Location</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">Via Route</div> </td>
                        <td class="contentsub" height="30"><div class="contentsub">KM</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">Toll Amount</div></td>                        
                        <td class="contentsub" height="30"><div class="contentsub">Driver Bata</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">Status</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">select</div></td>
                    </tr>
                </c:if>
                <c:if test = "${getRouteList != null}" >
                    <c:forEach items="${getRouteList}" var="routeList">
                        <%

                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text2";
                                    } else {
                                        classText = "text1";
                                    }
                        %>
                        <tr>
                            <td class="<%=classText%>" height="30"><%=index + 1%><input type="hidden" name="routeIds" value=<c:out value="${routeList.routeId}" />  ></td>
                            <td class="<%=classText%>"  ><input type="text" class="form-control" name="routeCodes" value="<c:out value="${routeList.routeCode}"/>" onchange="setSelectbox(<%= index%>)" /></td>
                            <td class="<%=classText%>" height="30"><input type="text" class="form-control" name="fromLocations" value="<c:out value="${routeList.fromLocation}"/>" onchange="setSelectbox(<%= index%>)" /></td>
                            <td class="<%=classText%>"  ><input type="text" class="form-control" name="toLocations" value="<c:out value="${routeList.toLocation}"/>" onchange="setSelectbox(<%= index%>)" /> </td>
                            <td class="<%=classText%>" ><input type="text" class="form-control" name="viaRoutes" value="<c:out value="${routeList.viaRoute}"/>" onchange="setSelectbox(<%= index%>)" /> </td>
                            <td class="<%=classText%>" height="30"><input type="text" class="form-control" name="kms" value="<c:out value="${routeList.km}"/>" onchange="setSelectbox(<%= index%>)" /></td>
                            <td class="<%=classText%>" ><input type="text" class="form-control" name="tollAmounts" value="<c:out value="${routeList.tollAmount}"/>" onchange="setSelectbox(<%= index%>)" /> </td>                            
                            <td class="<%=classText%>" ><input type="text" class="form-control" name="driverBatas" value="<c:out value="${routeList.driverBata}"/>" onchange="setSelectbox(<%= index%>)" /> </td>
                            <td><select class="form-control" name="activeInds" onchange="setSelectbox(<%= index %>) " >
                                <c:choose>
                                    <c:when test="${routeList.activeInd == 'Y'}">
                                        <option value="Y" selected>Active</option>
                                        <option value="N">InActive</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="Y">Active</option>
                                        <option value="N" selected>InActive</option>
                                    </c:otherwise>
                                </c:choose>
                            </select>

                            </td>
                            <td width="77" height="30" class="<%=classText%>"><input type="checkbox" name="selectedIndex" value='<%= index%>'></td>
                        </tr>
                        <%
                                    index++;
                        %>
                    </c:forEach >
                </c:if>
            </table>
            <center>
                <br>
                <input type="button" name="save" value="Save" onClick="submitpage(this.name)" class="button" />
                <input type="hidden" name="reqfor" value="designa" />
            </center>
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>