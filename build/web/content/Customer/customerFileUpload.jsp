<%-- 
    Document   : tripAttachments
    Created on : 23 May, 2021, 1:06:05 PM
    Author     : mahendiran
--%>



<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2YEWWi-juKcJyjyIKB3-8lEw9GZxNiCc&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>
<script>
    function uploadContract() {
        var custId  = '<c:out value="${custId}"/>';
        var contractId  = '<c:out value="${contractId}"/>';
        if (document.getElementById('importCnote').value == '' || document.getElementById('importCnote').value == '0') {
            alert("Please Upload Excel")
            return;
        }
        document.upload.action = "/throttle/uploadFueldetails.do";
        document.upload.method = "post";
        document.upload.submit();
    }
    
      function savePage() {
        document.upload.action = "/throttle/uploadFueldetails.do";
        document.upload.method = "post";
        document.upload.submit();
    }

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.CityMaster" text="Upload Documents"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Sales/Ops"/></a></li>
            <li class=""><spring:message code="hrms.label.CityMaster" text="Upload Customer Documents"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body >
                <form name="upload"  method="POST" enctype="multipart/form-data">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>

                    <%@ include file="/content/common/message.jsp" %>


                    
                      <div id="ptp" style="overflow: auto">
                                        <div class="inpad">

                                 <table class="table table-info mb30 table-hover" id="POD1">
                        <thead><tr  id="tableDesingTD">
                            <td colspan="4" style="background-color:#5BC0DE;"><font color="white">Attachment</font></td>
                        </tr></thead>
                        <% int index = 1;%>
                        <tr>
                            <input type="hidden" name="tripSheetId" id="tripSheetId" value="<%=request.getParameter("tripId")%>" />
                            <input type="hidden" name="customerId" id="customerId" value="<%=request.getParameter("customerId")%>" />
                            <input type="hidden" name="type" id="type" value="<%=request.getParameter("type")%>" />
                           
                                <input type="hidden" name="selectedRowCount" id="selectedRowCount" value="1"/>
                                    </tr>
                                    <tr>
                                    <td colspan="4" align="center">
                                        <input type="button" class="btn btn-success" name="saveAttach" value="Save Files" onclick="saveAttachment(<%=request.getParameter("tripId")%>,<%=request.getParameter("type")%>);"/>
                                        <input type="button" class="btn btn-success" name="add" value="add" onclick="addRows();"/>
                                    </td>
                                </tr>
                      
                        
                        <script>
                                    var podRowCount1 = 1;
                                    function addRows() {
                                        var podSno1 = document.getElementById('selectedRowCount').value;
                                        var tab = document.getElementById("POD1");
                                        var newrow = tab.insertRow(podSno1);

                                        var newrow = tab.insertRow(podSno1);
                                        //                alert(newrow.id);
                                        newrow.id = 'rowId' + podSno1;

                                        var cell = newrow.insertCell(0);
                                        var cell0 = "<td class='text1' height='25' >" + podSno1 + "</td>";
                                        cell.innerHTML = cell0;

                                        cell = newrow.insertCell(1);
                                        var cell0 = "<td class='text1' height='25' ><input type='file'   id='podFile" + podSno1 + "' name='podFile' class='form-control' value='' onchange='validateLocation(" + podSno1 + ");checkName(" + podSno1 + ");' ><br/><font size='2' color='blue'> Allowed file type:pdf & image</td>";
                                        cell.innerHTML = cell0;

                                        cell = newrow.insertCell(2);
                                        cell0 = "<td class='text1' height='25' ><textarea rows='3' cols='30' class='form-control'  style='width:250px;height:45px' name='podRemarks' id='podRemarks" + podSno1 + "'></textarea><br/><font size='2' color='blue'> Attachment Description</td>";
                                        cell.innerHTML = cell0;

                                        cell = newrow.insertCell(3);
                                            cell0 = "<td class='text1' height='25' align='left'><input type='button' class='button'  style='width:90px;'  name='delete'  id='delete" + podSno1 + "'   value='Delete Row' onclick='deleteRow(" + podSno1 + ");' ></td>";
                                            cell.innerHTML = cell0;



                                        podSno1++;
                                        if (podSno1 > 0) {
                                            document.getElementById('selectedRowCount').value = podSno1;
                                        }

                                    }


                                        </script>
                                        <script type="text/javascript">
                            function deleteRow(sno) {
                                var rowId = "rowId" + sno;
                                $("#" + rowId).remove();
                                document.getElementById('selectedRowCount').value--;
                            }
                            //                   function validateLocation(sno){
                            //                       var cityId = document.getElementById("cityId"+sno).value;
                            //                       if(cityId == "" || cityId == 0){
                            //                           alert("Please select location and then upload file");
                            //                           document.getElementById("cityId"+sno).focus();
                            //                       }
                            //                   }

                            var ar_ext = ['pdf', 'gif', 'jpeg', 'jpg', 'png', 'Gif', 'GIF', 'Png', 'PNG', 'JPG', 'Jpg'];        // array with allowed extensions

                            function checkName(sno) {
                                var name = document.getElementById("podFile" + sno).value;
                                var ar_name = name.split('.');

                                var ar_nm = ar_name[0].split('\\');
                                for (var i = 0; i < ar_nm.length; i++)
                                    var nm = ar_nm[i];

                                var re = 0;
                                for (var i = 0; i < ar_ext.length; i++) {
                                    if (ar_ext[i] == ar_name[1]) {
                                        re = 1;
                                        break;
                                    }
                                }

                                if (re == 1) {
                                }
                                else {
                                    alert('".' + ar_name[1] + '" is not an file type allowed for upload');
                                    document.getElementById("podFile" + sno).value = '';
                                }
                            }
                        </script>
                        <script>
                            
                            function saveAttachment(tripSheetId,type) {
        var podFiles = document.getElementsByName("podFile");
        var statusCheck = true;
        for (var i = 0; i < podFiles.length; i++) {

           

            if (podFiles[i].value == '' || podFiles[i].value == null) {
                alert('upload the pod attachment..')
                statusCheck = false;
            }
        }

        if (statusCheck) {
            
            document.upload.action = '/throttle/saveCustomerAttachments.do?&tripId=' + tripSheetId  +  '&tripSheetId=' + tripSheetId +'&type=' + type ;
            document.upload.submit();
        }
    }
                            </script>
                        <%index++;%>
                        
                    </table> 
                        <c:if test="${viewPODDetails != null}">
                                                <table  border="0" class="table table-info mb30 table-hover" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                                    <thead><tr>
                                                            <th class="text2" width="50" >S No&nbsp;</th>
                                                            <th class="text2">Attachment file Name</th>
                                                            <th class="text2">Attachment Description</th>
                                                            <th class="text2">Attachment Download</th>
                                                        </tr></thead>
                                                        <% int index20 = 1;%>
                                                        <c:forEach items="${viewPODDetails}" var="viewPODDetails">
                                                            <%
                                                                        String classText3 = "";
                                                                        int oddEven = index20 % 2;
                                                                        if (oddEven > 0) {
                                                                            classText3 = "text1";
                                                                        } else {
                                                                            classText3 = "text2";
                                                                        }
                                                            %>
                                                        <tr>
                                                            <td class="<%=classText3%>" ><%=index20++%></td>
                                                            <td class="<%=classText3%>" ><c:out value="${viewPODDetails.podFile}"/></td>
                                                            <td class="<%=classText3%>" ><c:out value="${viewPODDetails.podRemarks}"/></td>
                                                            <td>  <a class='btn btn-info' style='width:100px;height:30px' download href='content/upload/<c:out value="${viewPODDetails.podFile}"/>'/>Download</a></td>
                                                        </tr>
                                                    </c:forEach>

                                                </table>
                                            </c:if>
                                      
                                        <script>
                                            function viewPODFiles(tripPodId) {
                                                window.open('/throttle/content/trip/displayBlobDataForTrip.jsp?tripPodId=' + tripPodId, 'PopupPage', 'height = 500, width = 500, scrollbars = yes, resizable = yes');
                                            }
                                        </script>
                                          </div>     
                    
                </form>
            </body>
        </div>
    </div>
</div>


<%@ include file="/content/common/NewDesign/settings.jsp" %>


