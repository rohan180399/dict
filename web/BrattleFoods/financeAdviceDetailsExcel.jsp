<%--
    Document   : searchCustomerWiseProfitability
    Created on : Oct 30, 2013, 5:06:46 PM
    Author     : srinivasan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>
<html>
    <head>
       <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
    </head>
    <%
                String menuPath = "Finance >> Daily Advance Advice";
                request.setAttribute("menuPath", menuPath);
                String dateval = request.getParameter("dateval");
                String active = request.getParameter("active");
                String type = request.getParameter("type");
    %>

    <style type="text/css">
        .contentsub {
            padding:3px;
            height:24px;
            text-align:left;
            font-weight:bold;
            font-size:14px;
            background:#129fd4;
            color:#ffffff;
            background:url(/images/title_bg1.jpg) repeat-x top left #7f8ba5;
        }
        .text1 {
            height:25px;
            border-bottom:1px solid;
            border-bottom-color: #f2f2f2;
            font-size:12px;
            font-weight:normal;
        }

        .text2 {
            height:25px;
            border-bottom:1px solid;
            border-bottom-color: #f2f2f2;
            background:#f2f2f2;
            font-size:12px;
            font-weight:normal;
        }

    </style>
    <body>
        <form name="customerWise" method="post">
            <%
                      Date dNow = new Date();
                      SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                      //System.out.println("Current Date: " + ft.format(dNow));
                      String curDate = ft.format(dNow);
                      String expFile = "FinanceAdviceDetails-" + curDate + ".xls";

                      String fileName = "attachment;filename=" + expFile;
                      response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                      response.setHeader("Content-disposition", fileName);
            %>
            <br><br>
            <br>
            <br>


            <c:if test = "${financeAdviceDetails != null}" >
                    <table border="1"  align="center" cellpadding="0" cellspacing="1" >
                        <thead>
                            <tr height="40">
                                <td rowspan="2" class="contentsub">S.No</td>
                                <td rowspan="2" class="contentsub">Type</td>
                                <td rowspan="2" class="contentsub">Customer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td rowspan="2" class="contentsub">CNoteNo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td rowspan="2" class="contentsub">TripNo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td rowspan="2" class="contentsub">VehicleNo</td>
                                <td rowspan="2" class="contentsub">VehicleType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td rowspan="2" class="contentsub">Route</td>
                                <td rowspan="2" class="contentsub">Driver</td>
                                <td rowspan="2" class="contentsub">TripStart</td>
                                <td rowspan="2" class="contentsub">Nett Expenses</td>
                                <td rowspan="2" class="contentsub">Already PaidAmount</td>
                                <td rowspan="2" class="contentsub">Transit Days</td>
                                <td rowspan="2" class="contentsub">Journey Day</td>
                                <td rowspan="2" class="contentsub">ToBePaid Today</td>
                                <td rowspan="2" class="contentsub">Paid</td>
                                <td rowspan="2" class="contentsub">action</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                             </thead>
                            <% int index = 0;%>
                            <c:forEach items="${financeAdviceDetails}" var="FD">
                                <%
                                            String classText = "";
                                            int oddEven = index % 2;
                                            if (oddEven > 0) {
                                %>
                                <tr height="30">
                                    <td class="text2"  align="left"> <%= index + 1%> </td>
                                    <td class="text2"  ><c:out value="${FD.batchType}"/></td>
                                    <td class="text2"  ><c:out value="${FD.customerName}"/></td>
                                    <td class="text2" >
                                        <c:out value="${FD.cnoteName}"/>
                                    </td>
                                    <td class="text2"  align="left"> <c:out value="${FD.tripcode}" />||<c:out value="${FD.tripid}" /></td>
                                    <td class="text2"  align="left"><c:out value="${FD.regNo}" /></td>
                                    <td class="text2"  ><c:out value="${FD.vehicleTypeName}"/></td>
                                    <td class="text2"  ><c:out value="${FD.routeName}"/></td>
                                    <td class="text2"  align="left"><c:out value="${FD.driverName}"/></td>
                                    <td class="text2"   align="left"> <c:out value="${FD.planneddate}"/> </td>
                                    <td class="text2"   align="right"> <c:out value="${FD.estimatedexpense}"/> </td>
                                    <td class="text2"  align="right"> <c:out value="${FD.actualadvancepaid}" /></td>
                                    <td class="text2"   align="left"> <c:out value="${FD.estimatedtransitday}"/> </td>
                                    <td class="text2"  align="left"> <c:out value="${FD.tripday}" /></td>


                                    <td class="text2"  align="right">
                                        <c:if test="${FD.approvalstatus==0 || FD.approvalstatus==3}">
                                            <c:out value="${FD.estimatedadvance}" />
                                        </c:if>
                                        <c:if test="${FD.approvalstatus==1}">
                                            <c:out value="${FD.requestedadvance}" />
                                        </c:if>

                                    </td>

                                    <%if(active.equals("1")){%>
                                    <c:if test="${FD.approvalstatus==0}">
                                        <td>-</td>
                                        <td class="text2"  align="left">Waiting for Approval</td>
                                    </c:if>




                                    <%--c:if test="${FD.approvalstatus==3 && FD.paidstatus=='N'}">
                                    <td class="text2"  align="left"><a href="/throttle/tripSheetApprove.do?tripid=<c:out value="${FD.tripid}" />&tripday=<c:out value="${FD.tripday}" />
                                                                               &estimatedadvance=<c:out value="${FD.estimatedadvance}"/>&cnoteName=<c:out value="${FD.cnoteName}"/>&vehicleTypeName=<c:out value="${FD.vehicleTypeName}"/>&routeName=<c:out value="${FD.routeName}"/>
                                                                               &driverName=<c:out value="${FD.driverName}"/>&planneddate=<c:out value="${FD.planneddate}"/>&estimatedexpense=<c:out value="${FD.estimatedexpense}"/>
                                                                               &actualadvancepaid=<c:out value="${FD.actualadvancepaid}"/>&vegno=<c:out value="${FD.regNo}" />&advicedate=<%=dateval%>&billtype=<%=type%>">Request</a>&nbsp;&nbsp;</td>                                
                                    </c:if--%>





                                    <c:if test="${(FD.approvalstatus==3) && FD.paidstatus=='N'}">
                                        <%if(!type.equalsIgnoreCase("M")){%>
                                        <td>-</td>
                                        <td class="text2"  align="left"><a href="/throttle/tripSheetPay.do?customerName=<c:out value="${FD.customerName}"/>&tripCode=<c:out value="${FD.tripcode}" />&tripid=<c:out value="${FD.tripid}" />&tripday=<c:out value="${FD.tripday}" />
                                                                                   &requestedadvance=<c:out value="${FD.requestedadvance}"/>&estimatedadvance=<c:out value="${FD.estimatedadvance}"/>&cnoteName=<c:out value="${FD.cnoteName}"/>&vehicleTypeName=<c:out value="${FD.vehicleTypeName}"/>&routeName=<c:out value="${FD.routeName}"/>
                                                                                   &driverName=<c:out value="${FD.driverName}"/>&planneddate=<c:out value="${FD.planneddate}"/>&estimatedexpense=<c:out value="${FD.estimatedexpense}"/>&actualadvancepaid=<c:out value="${FD.actualadvancepaid}"/>&vegno=<c:out value="${FD.regNo}" />&tripAdvaceId=<c:out value="${FD.tripAdvaceId}" />&advicedate=<%=dateval%>&status=0">pay</a></td>
                                            <%}%>
                                        </c:if>
                                        <c:if test="${(FD.approvalstatus==1 && FD.paidstatus=='N')}">
                                            <%if(!type.equalsIgnoreCase("M")){%>
                                        <td>-</td>
                                        <td class="text2"  align="left"><a href="/throttle/tripSheetPay.do?customerName=<c:out value="${FD.customerName}"/>&tripCode=<c:out value="${FD.tripcode}" />&tripid=<c:out value="${FD.tripid}" />&tripday=<c:out value="${FD.tripday}" />
                                                                                   &requestedadvance=<c:out value="${FD.requestedadvance}"/>&estimatedadvance=<c:out value="${FD.estimatedadvance}"/>&cnoteName=<c:out value="${FD.cnoteName}"/>&vehicleTypeName=<c:out value="${FD.vehicleTypeName}"/>&routeName=<c:out value="${FD.routeName}"/>
                                                                                   &driverName=<c:out value="${FD.driverName}"/>&planneddate=<c:out value="${FD.planneddate}"/>&estimatedexpense=<c:out value="${FD.estimatedexpense}"/>&actualadvancepaid=<c:out value="${FD.actualadvancepaid}"/>&vegno=<c:out value="${FD.regNo}" />&tripAdvaceId=<c:out value="${FD.tripAdvaceId}" />&advicedate=<%=dateval%>&status=1">pay</a></td>
                                            <%}%>
                                        </c:if>

                                    <c:if test="${(FD.paidstatus!='Y')&& (FD.approvalstatus==1)}">
                                        <%if(type.equalsIgnoreCase("M")){%>
                                        <td>-</td>
                                        <td class="text2"  align="left"><a href="/throttle/tripSheetPay.do?customerName=<c:out value="${FD.customerName}"/>&tripCode=<c:out value="${FD.tripcode}" />&tripid=<c:out value="${FD.tripid}" />&tripday=<c:out value="${FD.tripday}" />
                                                                                   &requestedadvance=<c:out value="${FD.requestedadvance}"/>&estimatedadvance=<c:out value="${FD.estimatedadvance}"/>&cnoteName=<c:out value="${FD.cnoteName}"/>&vehicleTypeName=<c:out value="${FD.vehicleTypeName}"/>&routeName=<c:out value="${FD.routeName}"/>
                                                                                   &driverName=<c:out value="${FD.driverName}"/>&planneddate=<c:out value="${FD.planneddate}"/>&estimatedexpense=<c:out value="${FD.estimatedexpense}"/>&actualadvancepaid=<c:out value="${FD.actualadvancepaid}"/>&vegno=<c:out value="${FD.regNo}" />&tripAdvaceId=<c:out value="${FD.tripAdvaceId}" />&advicedate=<%=dateval%>&status=1">pay</a></td>
                                            <%}%>
                                        </c:if>

                                    <c:if test="${(FD.approvalstatus ==2) && FD.paidstatus !='Y' }">
                                        <td>-</td>
                                        <td class="text2"  align="left">
                                            <c:if test="${(FD.approvalstatus ==2)}">
                                                Rejected
                                            </c:if>
                                            <c:if test="${(FD.approvalstatus ==1)}">
                                                Approved
                                            </c:if>
                                        </td>
                                    </c:if>




                                    <c:if test="${FD.paidstatus=='Y'}">                                
                                        <td class="text2"  align="left"><c:out value="${FD.paidamt}"/></td>
                                        <td class="text2"  align="left">Already Paid</td>
                                    </c:if>


                                    <%}else{%>

                                    <c:if test="${FD.paidstatus=='Y'}">
                                        <td class="text2"  align="right"><font color="green"><c:out value="${FD.paidamt}"/></font></td>
                                        <td class="text2"  align="left"><font color="green">Paid</font></td>
                                            </c:if>

                                    <c:if test="${FD.paidstatus !='Y'}">
                                        <td class="text2"  align="center"><font color="red">-</font></td>
                                        <td class="text2"  align="center"><font color="red">Not Paid</font></td>
                                            </c:if>



                                    <%}%>
                                    <%--
                                    <td class="text2"  align="left"><a href="/throttle/manualfinanceadvice.do?tripid=<c:out value="${FD.tripid}" />&tripday=<c:out value="${FD.tripday}" />
                                                                               &estimatedadvance=<c:out value="${FD.estimatedadvance}"/>&cnoteName=<c:out value="${FD.cnoteName}"/>&vehicleTypeName=<c:out value="${FD.vehicleTypeName}"/>&routeName=<c:out value="${FD.routeName}"/>
                                                                               &driverName=<c:out value="${FD.driverName}"/>&planneddate=<c:out value="${FD.planneddate}"/>&estimatedexpense=<c:out value="${FD.estimatedexpense}"/>
                                                                               &actualadvancepaid=<c:out value="${FD.actualadvancepaid}"/>&vegno=<c:out value="${FD.regNo}" />&advicedate=<%=dateval%>&billtype=<%=type%>">Manual Request</a>&nbsp;&nbsp;
                                    
                                    </td>
                                    --%>


                                </tr>
                                <%}else{%>
                                <tr height="30">
                                    <td class="text1"  align="left"> <%= index + 1%> </td>
                                    <td class="text1"  ><c:out value="${FD.batchType}"/></td>
                                    <td class="text1"  ><c:out value="${FD.customerName}"/></td>
                                    <td class="text1" >
                                        <c:out value="${FD.cnoteName}"/>
                                    </td>
                                    <td class="text1"  align="left"> <c:out value="${FD.tripcode}" />||<c:out value="${FD.tripid}" /></td>
                                    <td class="text1"  align="left"><c:out value="${FD.regNo}" /></td>
                                    <td class="text1"  ><c:out value="${FD.vehicleTypeName}"/></td>
                                    <td class="text1"  ><c:out value="${FD.routeName}"/></td>
                                    <td class="text1"  align="left"><c:out value="${FD.driverName}"/></td>
                                    <td class="text1"   align="left"> <c:out value="${FD.planneddate}"/> </td>
                                    <td class="text1"   align="right"> <c:out value="${FD.estimatedexpense}"/> </td>
                                    <td class="text1"  align="right"> <c:out value="${FD.actualadvancepaid}" /></td>
                                    <td class="text1"   align="left"> <c:out value="${FD.estimatedtransitday}"/> </td>
                                    <td class="text1"  align="left"> <c:out value="${FD.tripday}" /></td>


                                    <td class="text1"  align="right">
                                        <c:if test="${FD.approvalstatus==0 || FD.approvalstatus==3}">
                                            <c:out value="${FD.estimatedadvance}" />
                                        </c:if>
                                        <c:if test="${FD.approvalstatus==1}">
                                            <c:out value="${FD.requestedadvance}" />
                                        </c:if>

                                    </td>

                                    <%if(active.equals("1")){%>
                                    <c:if test="${FD.approvalstatus==0}">
                                        <td>-</td>
                                        <td class="text1"  align="left">Waiting for Approval</td>
                                    </c:if>




                                    <%--c:if test="${FD.approvalstatus==3 && FD.paidstatus=='N'}">
                                    <td class="text1"  align="left"><a href="/throttle/tripSheetApprove.do?tripid=<c:out value="${FD.tripid}" />&tripday=<c:out value="${FD.tripday}" />
                                                                               &estimatedadvance=<c:out value="${FD.estimatedadvance}"/>&cnoteName=<c:out value="${FD.cnoteName}"/>&vehicleTypeName=<c:out value="${FD.vehicleTypeName}"/>&routeName=<c:out value="${FD.routeName}"/>
                                                                               &driverName=<c:out value="${FD.driverName}"/>&planneddate=<c:out value="${FD.planneddate}"/>&estimatedexpense=<c:out value="${FD.estimatedexpense}"/>
                                                                               &actualadvancepaid=<c:out value="${FD.actualadvancepaid}"/>&vegno=<c:out value="${FD.regNo}" />&advicedate=<%=dateval%>&billtype=<%=type%>">Request</a>&nbsp;&nbsp;</td>                                
                                    </c:if--%>





                                    <c:if test="${(FD.approvalstatus==3) && FD.paidstatus=='N'}">
                                        <%if(!type.equalsIgnoreCase("M")){%>
                                        <td>-</td>
                                        <td class="text1"  align="left"><a href="/throttle/tripSheetPay.do?customerName=<c:out value="${FD.customerName}"/>&tripCode=<c:out value="${FD.tripcode}" />&tripid=<c:out value="${FD.tripid}" />&tripday=<c:out value="${FD.tripday}" />
                                                                                   &requestedadvance=<c:out value="${FD.requestedadvance}"/>&estimatedadvance=<c:out value="${FD.estimatedadvance}"/>&cnoteName=<c:out value="${FD.cnoteName}"/>&vehicleTypeName=<c:out value="${FD.vehicleTypeName}"/>&routeName=<c:out value="${FD.routeName}"/>
                                                                                   &driverName=<c:out value="${FD.driverName}"/>&planneddate=<c:out value="${FD.planneddate}"/>&estimatedexpense=<c:out value="${FD.estimatedexpense}"/>&actualadvancepaid=<c:out value="${FD.actualadvancepaid}"/>&vegno=<c:out value="${FD.regNo}" />&tripAdvaceId=<c:out value="${FD.tripAdvaceId}" />&advicedate=<%=dateval%>&status=0">pay</a></td>
                                            <%}%>
                                        </c:if>
                                        <c:if test="${(FD.approvalstatus==1 && FD.paidstatus=='N')}">
                                            <%if(!type.equalsIgnoreCase("M")){%>
                                        <td>-</td>
                                        <td class="text1"  align="left"><a href="/throttle/tripSheetPay.do?customerName=<c:out value="${FD.customerName}"/>&tripCode=<c:out value="${FD.tripcode}" />&tripid=<c:out value="${FD.tripid}" />&tripday=<c:out value="${FD.tripday}" />
                                                                                   &requestedadvance=<c:out value="${FD.requestedadvance}"/>&estimatedadvance=<c:out value="${FD.estimatedadvance}"/>&cnoteName=<c:out value="${FD.cnoteName}"/>&vehicleTypeName=<c:out value="${FD.vehicleTypeName}"/>&routeName=<c:out value="${FD.routeName}"/>
                                                                                   &driverName=<c:out value="${FD.driverName}"/>&planneddate=<c:out value="${FD.planneddate}"/>&estimatedexpense=<c:out value="${FD.estimatedexpense}"/>&actualadvancepaid=<c:out value="${FD.actualadvancepaid}"/>&vegno=<c:out value="${FD.regNo}" />&tripAdvaceId=<c:out value="${FD.tripAdvaceId}" />&advicedate=<%=dateval%>&status=1">pay</a></td>
                                            <%}%>
                                        </c:if>

                                    <c:if test="${(FD.paidstatus!='Y')&& (FD.approvalstatus==1)}">
                                        <%if(type.equalsIgnoreCase("M")){%>
                                        <td>-</td>
                                        <td class="text1"  align="left"><a href="/throttle/tripSheetPay.do?customerName=<c:out value="${FD.customerName}"/>&tripCode=<c:out value="${FD.tripcode}" />&tripid=<c:out value="${FD.tripid}" />&tripday=<c:out value="${FD.tripday}" />
                                                                                   &requestedadvance=<c:out value="${FD.requestedadvance}"/>&estimatedadvance=<c:out value="${FD.estimatedadvance}"/>&cnoteName=<c:out value="${FD.cnoteName}"/>&vehicleTypeName=<c:out value="${FD.vehicleTypeName}"/>&routeName=<c:out value="${FD.routeName}"/>
                                                                                   &driverName=<c:out value="${FD.driverName}"/>&planneddate=<c:out value="${FD.planneddate}"/>&estimatedexpense=<c:out value="${FD.estimatedexpense}"/>&actualadvancepaid=<c:out value="${FD.actualadvancepaid}"/>&vegno=<c:out value="${FD.regNo}" />&tripAdvaceId=<c:out value="${FD.tripAdvaceId}" />&advicedate=<%=dateval%>&status=1">pay</a></td>
                                            <%}%>
                                        </c:if>

                                    <c:if test="${(FD.approvalstatus ==2) && FD.paidstatus !='Y' }">
                                        <td>-</td>
                                        <td class="text1"  align="left">
                                            <c:if test="${(FD.approvalstatus ==2)}">
                                                Rejected
                                            </c:if>
                                            <c:if test="${(FD.approvalstatus ==1)}">
                                                Approved
                                            </c:if>
                                        </td>
                                    </c:if>




                                    <c:if test="${FD.paidstatus=='Y'}">                                
                                        <td class="text1"  align="right"><c:out value="${FD.paidamt}"/></td>
                                        <td class="text1"  align="left">Already Paid</td>
                                    </c:if>


                                    <%}else{%>

                                    <c:if test="${FD.paidstatus=='Y'}">
                                        <td class="text1"  align="right"><font color="green"><c:out value="${FD.paidamt}"/></font></td>
                                        <td class="text1"  align="left"><font color="green">Paid</font></td>
                                            </c:if>

                                    <c:if test="${FD.paidstatus !='Y'}">
                                        <td class="text1"  align="center"><font color="red">-</font></td>
                                        <td class="text1"  align="center"><font color="red">Not Paid</font></td>
                                            </c:if>



                                    <%}%>
                                    <%--
                                    <td class="text1"  align="left"><a href="/throttle/manualfinanceadvice.do?tripid=<c:out value="${FD.tripid}" />&tripday=<c:out value="${FD.tripday}" />
                                                                               &estimatedadvance=<c:out value="${FD.estimatedadvance}"/>&cnoteName=<c:out value="${FD.cnoteName}"/>&vehicleTypeName=<c:out value="${FD.vehicleTypeName}"/>&routeName=<c:out value="${FD.routeName}"/>
                                                                               &driverName=<c:out value="${FD.driverName}"/>&planneddate=<c:out value="${FD.planneddate}"/>&estimatedexpense=<c:out value="${FD.estimatedexpense}"/>
                                                                               &actualadvancepaid=<c:out value="${FD.actualadvancepaid}"/>&vegno=<c:out value="${FD.regNo}" />&advicedate=<%=dateval%>&billtype=<%=type%>">Manual Request</a>&nbsp;&nbsp;
                                    
                                    </td>
                                    --%>


                                </tr>
                                <%}%>
                            <% index++;%>
                            </c:forEach>


                    
                </table>
            </c:if>


        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

    </body>
</html>
