/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.billing.business;

import com.ibatis.sqlmap.client.SqlMapClient;

/**
 *
 * @author Nivan
 */
public class BillingTO {

    private SqlMapClient session = null;

    private String invoiceType = "";
    private String setGrandRevenue = "";
    private String commodityId = "";
    private String refInvoiceDate = "";
    private String createddate = "";
    private String invoicecustomer = "";
    private String billingTypeName = "";
    private String grandTotal = "";
    private String tripPod = "";
    private String billingParty = "";
    private String grNo = "";
    private String containerNo = "";
    private String approvalStatus = "";
    private String pincode = "";
    private String city = "";
    private String creditNoteId = "";
    private String refInvoicecode = "";
    private String creditNotecode = "";
    private String billNo = "";
    private String creditNoteAmount = "";
    private String billlingLocation = "";
    private String apiErrorMsg = "";
    private String upload_status = "";
    private String commodityName = "";
    private String documentType = "";
    private String sellerGst = "";
    private String buyerGst = "";
    private String accNum = "";
    private String accDate = "";
    private String irn = "";
    private String signedQr = "";
    private String ewbNum = "";
    private String ewbDate = "";
    private String ewbValid = "";
    private String info = "";
    private String error = "";

    private String documentDate = null;
    private String documentNumber = null;
    private String invType = null;
    private String supTyp = null;
    private String billingLegalName = null;
    private String billingTradeName = null;
    private String billingGstin = null;
    private String placeOfSupply = null;
    private String billlingAddress = null;
    private String billingState = null;
    private String placeOfState = null;
    private String billingPin = null;
    private String sno = null;
    private String itemDescription = null;
    private String GorS = null;
    private String hsnCd = null;
    private String qty = null;
    private String unit = null;
    private String itemPrice = null;
    private String grossAmount = null;
    private String itemDiscount = null;
    private String itemTaxableValue = null;
    private String gstPercent = null;
    private String igst = null;
    private String cgst = null;
    private String sgst = null;
    private String cessValue = null;
    private String statecessValue = null;
    private String netamount = null;
    private String totalTaxableValue = null;
    private String totalIgst = null;
    private String totalCgst = null;
    private String totalSgst = null;
    private String totalNetAmount = null;
    private String compLegalName = null;
    private String compGstin = null;
    private String compAddress = null;
    private String compLocation = null;
    private String compState = null;
    private String compPin = null;
    private String isServc = null;
    private String prdDesc = null;
    private String hsnCode = null;
    private String unitPrice = null;
    private String totAmount = null;
    private String assAmt = null;
    private String cgstPercent = null;
    private String sgstPercent = null;
    private String igstPercent = null;
    private String totalValue = null;
    private String expenseDesc = null;

    private String billingCustId = "";
    private String sellerDetails = "";
    private String invoiceId = "";
    private String invoiceCode = "";
    private String invoiceDate = "";
    private String billingCustName = "";
    private String billingAddress = "";
    private String billingPanNo = "";
    private String billingPincode = "";
    private String billingPos = "";
    private String billingLocation = "";

    private String creditadjustno = "";
    private String orderCntNo = "";
    private String orderGrNos = "";
    private String orderCntSize = "";
    private String orderCntType = "";
    private String supExpense = "";
    private String creditExpense = "";
    private String supcreditExpense = "";

    private String creditGrNos = "";
    private String creditCntType = "";
    private String creditCntSize = "";
    private String creditCntNo = "";

    private String suppcreditGrNos = "";
    private String suppcreditCntNo = "";
    private String suppcreditCntSize = "";
    private String suppcreditCntType = "";

    private String createdOn = "";
    private String createdBy = "";
    private String usedLimit = "";
    private String finYear = "";
    private String availLimit = "";
    private String blockedLimit = "";
    private String paymentMode = "";
    private String suppInvoiceId = "";
    private String suppCreditnoteId = "";
    private String creditnoteId = "";
    private String suppInvoiceNo = "";
    private String bankName = "";
    private String reason = "";
    private String creditNoteDate = "";
    private String creditAmount = "";
    private String debitAmount = "";
    private String companyType = "";
    private String customerAddress = "";
    private String gstType = "";
    private String articleName = "";
    private String commodityCategory = "";
    private String articleCategory = "";
    private String panNo = "";
    private String grDate = "";
    private String totalTax = "";
    private String cgstAmount = "";
    private String sgstAmount = "";
    private String igstAmount = "";
    private String cgstPercentage = "";
    private String sgstPercentage = "";
    private String igstPercentage = "";
    private String gstCode = "";
    private String gstNo = "";
    private String gstPercentage = "";
    private String gstName = "";
    private String organizationId = "";
    private String activeInd = "";
    private String tripSheetIds = "";
    private String weightmentExpenseType = "";
    private String weightmentCharge = "";
    private String otherExpense = "";
    private String shippingBillNo = null;
    private String remarks = null;
    private String greenTax = null;
    private String tripContainerId = null;
    private String consignmentConatinerId = null;
    private String vehicleId = null;
    private String empId = null;
    private String suppgrNo = null;
    private String suppcntNo = null;
    private String suppcntSize = null;
    private String detaintionCharge = null;
    private String tollCharge = null;
    private String movementType = null;
    private String timeElapsedValue = null;
    private String shipingBillNo = null;
    private String billOfEntryNo = null;
    private String containerTypeName = null;
    private String suppcontTypeName = null;
    private String billingPartyId = null;
    private String freightAmount = null;
    private String podCount = null;
    private String totalAmount = null;
    private String otherExpenseAmount = null;
    private String userId = null;
    private String fromDate = null;
    private String toDate = null;
    private String customerName = null;
    private String customerId = null;
    private String tripId = null;
    private String totalKmRun = null;
    private String totalWeightage = null;
    private String consignmentNoteNo = null;
    private String billingTypeId = null;
    private String consigmentDestination = null;
    private String freightCharges = null;
    private String consignmentOrderId = null;
    private String tripCode = null;
    private String status = null;
    private String tempReeferRequired = null;
    private String routeInfo = null;
    private String tripStartTime = null;
    private String totalDays = null;
    private String tripTransitHours = null;
    private String billingType = null;
    private String customerType = null;
    private String vehicleType = null;
    private String startDate = null;
    private String startTime = null;
    private String endDate = null;
    private String endTime = null;
    private String estimatedRevenue = null;
    private String totalKM = null;
    private String totalHrs = null;
    private String tripTransitDays = null;
    private String driverName = null;
    private String totalExpenses = null;
    private String expenseToBeBilledToCustomer = null;
    private String expenseName = null;
    private String expenseRemarks = null;
    private String passThroughStatus = null;
    private String marginValue = null;
    private String taxPercentage = null;
    private String taxPercentageD = null;
    private String expenseValue = null;
    private String expenseId = null;
    private String noOfTrips = null;
    private String totalRevenue = null;
    private String totalExpToBeBilled = null;
    private String invoiceNo = null;
    private String creditNo = null;
    private String suppcreditNo = null;
    private String invoiceDetailId = null;
    private String statusId = null;
    private String tripSheetId = null;
    private String orderCount = null;
    private String transactionDate = null;
    private String custCode = null;
    private String transactionName = null;
    private String transactionType = null;
    private String receiptCode = null;
    private String receiptdetails = null;
    private String fixedLimit = null;
    private String paymentid = null;
    private String tempCode = null;
    private String adjustCode = null;
    private String cntSize = null;
    private String invExpense = null;
    private String[] invoiceIds = null;

    public String getCntSize() {
        return cntSize;
    }

    public void setCntSize(String cntSize) {
        this.cntSize = cntSize;
    }

    public String getTempCode() {
        return tempCode;
    }

    public void setTempCode(String tempCode) {
        this.tempCode = tempCode;
    }

    public String getAdjustCode() {
        return adjustCode;
    }

    public void setAdjustCode(String adjustCode) {
        this.adjustCode = adjustCode;
    }

    public String getFixedLimit() {
        return fixedLimit;
    }

    public void setFixedLimit(String fixedLimit) {
        this.fixedLimit = fixedLimit;
    }

    public String getTransactionName() {
        return transactionName;
    }

    public void setTransactionName(String transactionName) {
        this.transactionName = transactionName;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getCustCode() {
        return custCode;
    }

    public void setCustCode(String custCode) {
        this.custCode = custCode;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getCreditNo() {
        return creditNo;
    }

    public void setCreditNo(String creditNo) {
        this.creditNo = creditNo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getTotalKmRun() {
        return totalKmRun;
    }

    public void setTotalKmRun(String totalKmRun) {
        this.totalKmRun = totalKmRun;
    }

    public String getTotalWeightage() {
        return totalWeightage;
    }

    public void setTotalWeightage(String totalWeightage) {
        this.totalWeightage = totalWeightage;
    }

    public String getConsignmentNoteNo() {
        return consignmentNoteNo;
    }

    public void setConsignmentNoteNo(String consignmentNoteNo) {
        this.consignmentNoteNo = consignmentNoteNo;
    }

    public String getBillingTypeId() {
        return billingTypeId;
    }

    public void setBillingTypeId(String billingTypeId) {
        this.billingTypeId = billingTypeId;
    }

    public String getConsigmentDestination() {
        return consigmentDestination;
    }

    public void setConsigmentDestination(String consigmentDestination) {
        this.consigmentDestination = consigmentDestination;
    }

    public String getFreightCharges() {
        return freightCharges;
    }

    public void setFreightCharges(String freightCharges) {
        this.freightCharges = freightCharges;
    }

    public String getConsignmentOrderId() {
        return consignmentOrderId;
    }

    public void setConsignmentOrderId(String consignmentOrderId) {
        this.consignmentOrderId = consignmentOrderId;
    }

    public String getTripCode() {
        return tripCode;
    }

    public void setTripCode(String tripCode) {
        this.tripCode = tripCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTempReeferRequired() {
        return tempReeferRequired;
    }

    public void setTempReeferRequired(String tempReeferRequired) {
        this.tempReeferRequired = tempReeferRequired;
    }

    public String getRouteInfo() {
        return routeInfo;
    }

    public void setRouteInfo(String routeInfo) {
        this.routeInfo = routeInfo;
    }

    public String getTripStartTime() {
        return tripStartTime;
    }

    public void setTripStartTime(String tripStartTime) {
        this.tripStartTime = tripStartTime;
    }

    public String getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(String totalDays) {
        this.totalDays = totalDays;
    }

    public String getTripTransitHours() {
        return tripTransitHours;
    }

    public void setTripTransitHours(String tripTransitHours) {
        this.tripTransitHours = tripTransitHours;
    }

    public String getBillingType() {
        return billingType;
    }

    public void setBillingType(String billingType) {
        this.billingType = billingType;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getEstimatedRevenue() {
        return estimatedRevenue;
    }

    public void setEstimatedRevenue(String estimatedRevenue) {
        this.estimatedRevenue = estimatedRevenue;
    }

    public String getTotalKM() {
        return totalKM;
    }

    public void setTotalKM(String totalKM) {
        this.totalKM = totalKM;
    }

    public String getTotalHrs() {
        return totalHrs;
    }

    public void setTotalHrs(String totalHrs) {
        this.totalHrs = totalHrs;
    }

    public String getTripTransitDays() {
        return tripTransitDays;
    }

    public void setTripTransitDays(String tripTransitDays) {
        this.tripTransitDays = tripTransitDays;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getTotalExpenses() {
        return totalExpenses;
    }

    public void setTotalExpenses(String totalExpenses) {
        this.totalExpenses = totalExpenses;
    }

    public String getExpenseToBeBilledToCustomer() {
        return expenseToBeBilledToCustomer;
    }

    public void setExpenseToBeBilledToCustomer(String expenseToBeBilledToCustomer) {
        this.expenseToBeBilledToCustomer = expenseToBeBilledToCustomer;
    }

    public String getExpenseName() {
        return expenseName;
    }

    public void setExpenseName(String expenseName) {
        this.expenseName = expenseName;
    }

    public String getExpenseRemarks() {
        return expenseRemarks;
    }

    public void setExpenseRemarks(String expenseRemarks) {
        this.expenseRemarks = expenseRemarks;
    }

    public String getPassThroughStatus() {
        return passThroughStatus;
    }

    public void setPassThroughStatus(String passThroughStatus) {
        this.passThroughStatus = passThroughStatus;
    }

    public String getMarginValue() {
        return marginValue;
    }

    public void setMarginValue(String marginValue) {
        this.marginValue = marginValue;
    }

    public String getTaxPercentage() {
        return taxPercentage;
    }

    public void setTaxPercentage(String taxPercentage) {
        this.taxPercentage = taxPercentage;
    }

    public String getTaxPercentageD() {
        return taxPercentageD;
    }

    public void setTaxPercentageD(String taxPercentageD) {
        this.taxPercentageD = taxPercentageD;
    }

    public String getExpenseValue() {
        return expenseValue;
    }

    public void setExpenseValue(String expenseValue) {
        this.expenseValue = expenseValue;
    }

    public String getExpenseId() {
        return expenseId;
    }

    public void setExpenseId(String expenseId) {
        this.expenseId = expenseId;
    }

    public String getNoOfTrips() {
        return noOfTrips;
    }

    public void setNoOfTrips(String noOfTrips) {
        this.noOfTrips = noOfTrips;
    }

    public String getTotalRevenue() {
        return totalRevenue;
    }

    public void setTotalRevenue(String totalRevenue) {
        this.totalRevenue = totalRevenue;
    }

    public String getTotalExpToBeBilled() {
        return totalExpToBeBilled;
    }

    public void setTotalExpToBeBilled(String totalExpToBeBilled) {
        this.totalExpToBeBilled = totalExpToBeBilled;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceDetailId() {
        return invoiceDetailId;
    }

    public void setInvoiceDetailId(String invoiceDetailId) {
        this.invoiceDetailId = invoiceDetailId;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getTripSheetId() {
        return tripSheetId;
    }

    public void setTripSheetId(String tripSheetId) {
        this.tripSheetId = tripSheetId;
    }

    public String getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(String orderCount) {
        this.orderCount = orderCount;
    }

    public String getFreightAmount() {
        return freightAmount;
    }

    public void setFreightAmount(String freightAmount) {
        this.freightAmount = freightAmount;
    }

    public String getPodCount() {
        return podCount;
    }

    public void setPodCount(String podCount) {
        this.podCount = podCount;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getOtherExpenseAmount() {
        return otherExpenseAmount;
    }

    public void setOtherExpenseAmount(String otherExpenseAmount) {
        this.otherExpenseAmount = otherExpenseAmount;
    }

    public String getBillingParty() {
        return billingParty;
    }

    public void setBillingParty(String billingParty) {
        this.billingParty = billingParty;
    }

    public String getContainerTypeName() {
        return containerTypeName;
    }

    public void setContainerTypeName(String containerTypeName) {
        this.containerTypeName = containerTypeName;
    }

    public String getBillingPartyId() {
        return billingPartyId;
    }

    public void setBillingPartyId(String billingPartyId) {
        this.billingPartyId = billingPartyId;
    }

    public String getBillOfEntryNo() {
        return billOfEntryNo;
    }

    public void setBillOfEntryNo(String billOfEntryNo) {
        this.billOfEntryNo = billOfEntryNo;
    }

    public String getShipingBillNo() {
        return shipingBillNo;
    }

    public void setShipingBillNo(String shipingBillNo) {
        this.shipingBillNo = shipingBillNo;
    }

    public String getTimeElapsedValue() {
        return timeElapsedValue;
    }

    public void setTimeElapsedValue(String timeElapsedValue) {
        this.timeElapsedValue = timeElapsedValue;
    }

    public String getMovementType() {
        return movementType;
    }

    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    public String getDetaintionCharge() {
        return detaintionCharge;
    }

    public void setDetaintionCharge(String detaintionCharge) {
        this.detaintionCharge = detaintionCharge;
    }

    public String getTollCharge() {
        return tollCharge;
    }

    public void setTollCharge(String tollCharge) {
        this.tollCharge = tollCharge;
    }

    public String getGrNo() {
        return grNo;
    }

    public void setGrNo(String grNo) {
        this.grNo = grNo;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getConsignmentConatinerId() {
        return consignmentConatinerId;
    }

    public void setConsignmentConatinerId(String consignmentConatinerId) {
        this.consignmentConatinerId = consignmentConatinerId;
    }

    public String getTripContainerId() {
        return tripContainerId;
    }

    public void setTripContainerId(String tripContainerId) {
        this.tripContainerId = tripContainerId;
    }

    public String getContainerNo() {
        return containerNo;
    }

    public void setContainerNo(String containerNo) {
        this.containerNo = containerNo;
    }

    public String getGreenTax() {
        return greenTax;
    }

    public void setGreenTax(String greenTax) {
        this.greenTax = greenTax;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getShippingBillNo() {
        return shippingBillNo;
    }

    public void setShippingBillNo(String shippingBillNo) {
        this.shippingBillNo = shippingBillNo;
    }

    public String getOtherExpense() {
        return otherExpense;
    }

    public void setOtherExpense(String otherExpense) {
        this.otherExpense = otherExpense;
    }

    public String getWeightmentCharge() {
        return weightmentCharge;
    }

    public void setWeightmentCharge(String weightmentCharge) {
        this.weightmentCharge = weightmentCharge;
    }

    public String getWeightmentExpenseType() {
        return weightmentExpenseType;
    }

    public void setWeightmentExpenseType(String weightmentExpenseType) {
        this.weightmentExpenseType = weightmentExpenseType;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getTripSheetIds() {
        return tripSheetIds;
    }

    public void setTripSheetIds(String tripSheetIds) {
        this.tripSheetIds = tripSheetIds;
    }

    public SqlMapClient getSession() {
        return session;
    }

    public void setSession(SqlMapClient session) {
        this.session = session;
    }

    public String getBillingState() {
        return billingState;
    }

    public void setBillingState(String billingState) {
        this.billingState = billingState;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getGstCode() {
        return gstCode;
    }

    public void setGstCode(String gstCode) {
        this.gstCode = gstCode;
    }

    public String getGstName() {
        return gstName;
    }

    public void setGstName(String gstName) {
        this.gstName = gstName;
    }

    public String getGstPercentage() {
        return gstPercentage;
    }

    public void setGstPercentage(String gstPercentage) {
        this.gstPercentage = gstPercentage;
    }

    public String getGstNo() {
        return gstNo;
    }

    public void setGstNo(String gstNo) {
        this.gstNo = gstNo;
    }

    public String getCgstAmount() {
        return cgstAmount;
    }

    public void setCgstAmount(String cgstAmount) {
        this.cgstAmount = cgstAmount;
    }

    public String getIgstAmount() {
        return igstAmount;
    }

    public void setIgstAmount(String igstAmount) {
        this.igstAmount = igstAmount;
    }

    public String getSgstAmount() {
        return sgstAmount;
    }

    public void setSgstAmount(String sgstAmount) {
        this.sgstAmount = sgstAmount;
    }

    public String getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(String totalTax) {
        this.totalTax = totalTax;
    }

    public String getCgstPercentage() {
        return cgstPercentage;
    }

    public void setCgstPercentage(String cgstPercentage) {
        this.cgstPercentage = cgstPercentage;
    }

    public String getIgstPercentage() {
        return igstPercentage;
    }

    public void setIgstPercentage(String igstPercentage) {
        this.igstPercentage = igstPercentage;
    }

    public String getSgstPercentage() {
        return sgstPercentage;
    }

    public void setSgstPercentage(String sgstPercentage) {
        this.sgstPercentage = sgstPercentage;
    }

    public String getPanNo() {
        return panNo;
    }

    public void setPanNo(String panNo) {
        this.panNo = panNo;
    }

    public String getGrDate() {
        return grDate;
    }

    public void setGrDate(String grDate) {
        this.grDate = grDate;
    }

    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public String getGstType() {
        return gstType;
    }

    public void setGstType(String gstType) {
        this.gstType = gstType;
    }

    public String getArticleCategory() {
        return articleCategory;
    }

    public void setArticleCategory(String articleCategory) {
        this.articleCategory = articleCategory;
    }

    public String getCommodityCategory() {
        return commodityCategory;
    }

    public void setCommodityCategory(String commodityCategory) {
        this.commodityCategory = commodityCategory;
    }

    public String getCompanyType() {
        return companyType;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(String creditAmount) {
        this.creditAmount = creditAmount;
    }

    public String getCreditNoteDate() {
        return creditNoteDate;
    }

    public void setCreditNoteDate(String creditNoteDate) {
        this.creditNoteDate = creditNoteDate;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getSuppInvoiceId() {
        return suppInvoiceId;
    }

    public void setSuppInvoiceId(String suppInvoiceId) {
        this.suppInvoiceId = suppInvoiceId;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getSuppInvoiceNo() {
        return suppInvoiceNo;
    }

    public void setSuppInvoiceNo(String suppInvoiceNo) {
        this.suppInvoiceNo = suppInvoiceNo;
    }

    public String getUsedLimit() {
        return usedLimit;
    }

    public void setUsedLimit(String usedLimit) {
        this.usedLimit = usedLimit;
    }

    public String getAvailLimit() {
        return availLimit;
    }

    public void setAvailLimit(String availLimit) {
        this.availLimit = availLimit;
    }

    public String getBlockedLimit() {
        return blockedLimit;
    }

    public void setBlockedLimit(String blockedLimit) {
        this.blockedLimit = blockedLimit;
    }

    public String getDebitAmount() {
        return debitAmount;
    }

    public void setDebitAmount(String debitAmount) {
        this.debitAmount = debitAmount;
    }

    public String getReceiptCode() {
        return receiptCode;
    }

    public void setReceiptCode(String receiptCode) {
        this.receiptCode = receiptCode;
    }

    public String getFinYear() {
        return finYear;
    }

    public void setFinYear(String finYear) {
        this.finYear = finYear;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getSuppCreditnoteId() {
        return suppCreditnoteId;
    }

    public void setSuppCreditnoteId(String suppCreditnoteId) {
        this.suppCreditnoteId = suppCreditnoteId;
    }

    public String getCreditnoteId() {
        return creditnoteId;
    }

    public void setCreditnoteId(String creditnoteId) {
        this.creditnoteId = creditnoteId;
    }

    public String getReceiptdetails() {
        return receiptdetails;
    }

    public void setReceiptdetails(String receiptdetails) {
        this.receiptdetails = receiptdetails;
    }

    public String getSuppcreditNo() {
        return suppcreditNo;
    }

    public void setSuppcreditNo(String suppcreditNo) {
        this.suppcreditNo = suppcreditNo;
    }

    public String getPaymentid() {
        return paymentid;
    }

    public void setPaymentid(String paymentid) {
        this.paymentid = paymentid;
    }

    public String[] getInvoiceIds() {
        return invoiceIds;
    }

    public void setInvoiceIds(String[] invoiceIds) {
        this.invoiceIds = invoiceIds;
    }

    public String getCreditadjustno() {
        return creditadjustno;
    }

    public void setCreditadjustno(String creditadjustno) {
        this.creditadjustno = creditadjustno;
    }

    public String getOrderCntNo() {
        return orderCntNo;
    }

    public void setOrderCntNo(String orderCntNo) {
        this.orderCntNo = orderCntNo;
    }

    public String getOrderGrNos() {
        return orderGrNos;
    }

    public void setOrderGrNos(String orderGrNos) {
        this.orderGrNos = orderGrNos;
    }

    public String getOrderCntSize() {
        return orderCntSize;
    }

    public void setOrderCntSize(String orderCntSize) {
        this.orderCntSize = orderCntSize;
    }

    public String getOrderCntType() {
        return orderCntType;
    }

    public void setOrderCntType(String orderCntType) {
        this.orderCntType = orderCntType;
    }

    public String getCreditGrNos() {
        return creditGrNos;
    }

    public void setCreditGrNos(String creditGrNos) {
        this.creditGrNos = creditGrNos;
    }

    public String getCreditCntType() {
        return creditCntType;
    }

    public void setCreditCntType(String creditCntType) {
        this.creditCntType = creditCntType;
    }

    public String getCreditCntSize() {
        return creditCntSize;
    }

    public void setCreditCntSize(String creditCntSize) {
        this.creditCntSize = creditCntSize;
    }

    public String getCreditCntNo() {
        return creditCntNo;
    }

    public void setCreditCntNo(String creditCntNo) {
        this.creditCntNo = creditCntNo;
    }

    public String getSuppcreditGrNos() {
        return suppcreditGrNos;
    }

    public void setSuppcreditGrNos(String suppcreditGrNos) {
        this.suppcreditGrNos = suppcreditGrNos;
    }

    public String getSuppcreditCntNo() {
        return suppcreditCntNo;
    }

    public void setSuppcreditCntNo(String suppcreditCntNo) {
        this.suppcreditCntNo = suppcreditCntNo;
    }

    public String getSuppcreditCntSize() {
        return suppcreditCntSize;
    }

    public void setSuppcreditCntSize(String suppcreditCntSize) {
        this.suppcreditCntSize = suppcreditCntSize;
    }

    public String getSuppcreditCntType() {
        return suppcreditCntType;
    }

    public void setSuppcreditCntType(String suppcreditCntType) {
        this.suppcreditCntType = suppcreditCntType;
    }

    public String getSuppgrNo() {
        return suppgrNo;
    }

    public void setSuppgrNo(String suppgrNo) {
        this.suppgrNo = suppgrNo;
    }

    public String getSuppcntNo() {
        return suppcntNo;
    }

    public void setSuppcntNo(String suppcntNo) {
        this.suppcntNo = suppcntNo;
    }

    public String getSuppcntSize() {
        return suppcntSize;
    }

    public void setSuppcntSize(String suppcntSize) {
        this.suppcntSize = suppcntSize;
    }

    public String getSuppcontTypeName() {
        return suppcontTypeName;
    }

    public void setSuppcontTypeName(String suppcontTypeName) {
        this.suppcontTypeName = suppcontTypeName;
    }

    public String getInvExpense() {
        return invExpense;
    }

    public void setInvExpense(String invExpense) {
        this.invExpense = invExpense;
    }

    public String getSupExpense() {
        return supExpense;
    }

    public void setSupExpense(String supExpense) {
        this.supExpense = supExpense;
    }

    public String getCreditExpense() {
        return creditExpense;
    }

    public void setCreditExpense(String creditExpense) {
        this.creditExpense = creditExpense;
    }

    public String getSupcreditExpense() {
        return supcreditExpense;
    }

    public void setSupcreditExpense(String supcreditExpense) {
        this.supcreditExpense = supcreditExpense;
    }

    public String getSellerDetails() {
        return sellerDetails;
    }

    public void setSellerDetails(String sellerDetails) {
        this.sellerDetails = sellerDetails;
    }

    public String getInvType() {
        return invType;
    }

    public void setInvType(String invType) {
        this.invType = invType;
    }

    public String getBillingCustName() {
        return billingCustName;
    }

    public void setBillingCustName(String billingCustName) {
        this.billingCustName = billingCustName;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getBillingPanNo() {
        return billingPanNo;
    }

    public void setBillingPanNo(String billingPanNo) {
        this.billingPanNo = billingPanNo;
    }

    public String getBillingPincode() {
        return billingPincode;
    }

    public void setBillingPincode(String billingPincode) {
        this.billingPincode = billingPincode;
    }

    public String getBillingPos() {
        return billingPos;
    }

    public void setBillingPos(String billingPos) {
        this.billingPos = billingPos;
    }

    public String getBillingLocation() {
        return billingLocation;
    }

    public void setBillingLocation(String billingLocation) {
        this.billingLocation = billingLocation;
    }

    public String getCgstPercent() {
        return cgstPercent;
    }

    public void setCgstPercent(String cgstPercent) {
        this.cgstPercent = cgstPercent;
    }

    public String getSgstPercent() {
        return sgstPercent;
    }

    public void setSgstPercent(String sgstPercent) {
        this.sgstPercent = sgstPercent;
    }

    public String getIgstPercent() {
        return igstPercent;
    }

    public void setIgstPercent(String igstPercent) {
        this.igstPercent = igstPercent;
    }

    public String getCgst() {
        return cgst;
    }

    public void setCgst(String cgst) {
        this.cgst = cgst;
    }

    public String getSgst() {
        return sgst;
    }

    public void setSgst(String sgst) {
        this.sgst = sgst;
    }

    public String getIgst() {
        return igst;
    }

    public void setIgst(String igst) {
        this.igst = igst;
    }

    public String getIsServc() {
        return isServc;
    }

    public void setIsServc(String isServc) {
        this.isServc = isServc;
    }

    public String getPrdDesc() {
        return prdDesc;
    }

    public void setPrdDesc(String prdDesc) {
        this.prdDesc = prdDesc;
    }

    public String getHsnCode() {
        return hsnCode;
    }

    public void setHsnCode(String hsnCode) {
        this.hsnCode = hsnCode;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getTotAmount() {
        return totAmount;
    }

    public void setTotAmount(String totAmount) {
        this.totAmount = totAmount;
    }

    public String getAssAmt() {
        return assAmt;
    }

    public void setAssAmt(String assAmt) {
        this.assAmt = assAmt;
    }

    public String getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(String totalValue) {
        this.totalValue = totalValue;
    }

    public String getExpenseDesc() {
        return expenseDesc;
    }

    public void setExpenseDesc(String expenseDesc) {
        this.expenseDesc = expenseDesc;
    }

    public String getBillingCustId() {
        return billingCustId;
    }

    public void setBillingCustId(String billingCustId) {
        this.billingCustId = billingCustId;
    }

    public String getDocumentDate() {
        return documentDate;
    }

    public void setDocumentDate(String documentDate) {
        this.documentDate = documentDate;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getSupTyp() {
        return supTyp;
    }

    public void setSupTyp(String supTyp) {
        this.supTyp = supTyp;
    }

    public String getBillingLegalName() {
        return billingLegalName;
    }

    public void setBillingLegalName(String billingLegalName) {
        this.billingLegalName = billingLegalName;
    }

    public String getBillingTradeName() {
        return billingTradeName;
    }

    public void setBillingTradeName(String billingTradeName) {
        this.billingTradeName = billingTradeName;
    }

    public String getBillingGstin() {
        return billingGstin;
    }

    public void setBillingGstin(String billingGstin) {
        this.billingGstin = billingGstin;
    }

    public String getPlaceOfSupply() {
        return placeOfSupply;
    }

    public void setPlaceOfSupply(String placeOfSupply) {
        this.placeOfSupply = placeOfSupply;
    }

    public String getBilllingAddress() {
        return billlingAddress;
    }

    public void setBilllingAddress(String billlingAddress) {
        this.billlingAddress = billlingAddress;
    }

    public String getPlaceOfState() {
        return placeOfState;
    }

    public void setPlaceOfState(String placeOfState) {
        this.placeOfState = placeOfState;
    }

    public String getBillingPin() {
        return billingPin;
    }

    public void setBillingPin(String billingPin) {
        this.billingPin = billingPin;
    }

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getGorS() {
        return GorS;
    }

    public void setGorS(String GorS) {
        this.GorS = GorS;
    }

    public String getHsnCd() {
        return hsnCd;
    }

    public void setHsnCd(String hsnCd) {
        this.hsnCd = hsnCd;
    }

    public String getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(String itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getGrossAmount() {
        return grossAmount;
    }

    public void setGrossAmount(String grossAmount) {
        this.grossAmount = grossAmount;
    }

    public String getItemDiscount() {
        return itemDiscount;
    }

    public void setItemDiscount(String itemDiscount) {
        this.itemDiscount = itemDiscount;
    }

    public String getItemTaxableValue() {
        return itemTaxableValue;
    }

    public void setItemTaxableValue(String itemTaxableValue) {
        this.itemTaxableValue = itemTaxableValue;
    }

    public String getGstPercent() {
        return gstPercent;
    }

    public void setGstPercent(String gstPercent) {
        this.gstPercent = gstPercent;
    }

    public String getCessValue() {
        return cessValue;
    }

    public void setCessValue(String cessValue) {
        this.cessValue = cessValue;
    }

    public String getStatecessValue() {
        return statecessValue;
    }

    public void setStatecessValue(String statecessValue) {
        this.statecessValue = statecessValue;
    }

    public String getNetamount() {
        return netamount;
    }

    public void setNetamount(String netamount) {
        this.netamount = netamount;
    }

    public String getTotalTaxableValue() {
        return totalTaxableValue;
    }

    public void setTotalTaxableValue(String totalTaxableValue) {
        this.totalTaxableValue = totalTaxableValue;
    }

    public String getTotalIgst() {
        return totalIgst;
    }

    public void setTotalIgst(String totalIgst) {
        this.totalIgst = totalIgst;
    }

    public String getTotalCgst() {
        return totalCgst;
    }

    public void setTotalCgst(String totalCgst) {
        this.totalCgst = totalCgst;
    }

    public String getTotalSgst() {
        return totalSgst;
    }

    public void setTotalSgst(String totalSgst) {
        this.totalSgst = totalSgst;
    }

    public String getTotalNetAmount() {
        return totalNetAmount;
    }

    public void setTotalNetAmount(String totalNetAmount) {
        this.totalNetAmount = totalNetAmount;
    }

    public String getCompLegalName() {
        return compLegalName;
    }

    public void setCompLegalName(String compLegalName) {
        this.compLegalName = compLegalName;
    }

    public String getCompGstin() {
        return compGstin;
    }

    public void setCompGstin(String compGstin) {
        this.compGstin = compGstin;
    }

    public String getCompAddress() {
        return compAddress;
    }

    public void setCompAddress(String compAddress) {
        this.compAddress = compAddress;
    }

    public String getCompLocation() {
        return compLocation;
    }

    public void setCompLocation(String compLocation) {
        this.compLocation = compLocation;
    }

    public String getCompState() {
        return compState;
    }

    public void setCompState(String compState) {
        this.compState = compState;
    }

    public String getCompPin() {
        return compPin;
    }

    public void setCompPin(String compPin) {
        this.compPin = compPin;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getSellerGst() {
        return sellerGst;
    }

    public void setSellerGst(String sellerGst) {
        this.sellerGst = sellerGst;
    }

    public String getBuyerGst() {
        return buyerGst;
    }

    public void setBuyerGst(String buyerGst) {
        this.buyerGst = buyerGst;
    }

    public String getAccNum() {
        return accNum;
    }

    public void setAccNum(String accNum) {
        this.accNum = accNum;
    }

    public String getAccDate() {
        return accDate;
    }

    public void setAccDate(String accDate) {
        this.accDate = accDate;
    }

    public String getIrn() {
        return irn;
    }

    public void setIrn(String irn) {
        this.irn = irn;
    }

    public String getSignedQr() {
        return signedQr;
    }

    public void setSignedQr(String signedQr) {
        this.signedQr = signedQr;
    }

    public String getEwbNum() {
        return ewbNum;
    }

    public void setEwbNum(String ewbNum) {
        this.ewbNum = ewbNum;
    }

    public String getEwbDate() {
        return ewbDate;
    }

    public void setEwbDate(String ewbDate) {
        this.ewbDate = ewbDate;
    }

    public String getEwbValid() {
        return ewbValid;
    }

    public void setEwbValid(String ewbValid) {
        this.ewbValid = ewbValid;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getCommodityName() {
        return commodityName;
    }

    public void setCommodityName(String commodityName) {
        this.commodityName = commodityName;
    }

    public String getUpload_status() {
        return upload_status;
    }

    public void setUpload_status(String upload_status) {
        this.upload_status = upload_status;
    }


    public void setBilllingLocation(String billlingLocation) {
        this.billlingLocation = billlingLocation;
    }

    public void setApiErrorMsg(String apiErrorMsg) {
        this.apiErrorMsg = apiErrorMsg;
    }

    public String getCreditNoteAmount() {
        return creditNoteAmount;
    }

    public void setCreditNoteAmount(String creditNoteAmount) {
        this.creditNoteAmount = creditNoteAmount;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getCreditNotecode() {
        return creditNotecode;
    }

    public void setCreditNotecode(String creditNotecode) {
        this.creditNotecode = creditNotecode;
    }


    public String getCreateddate() {
        return createddate;
    }

    public void setCreateddate(String createddate) {
        this.createddate = createddate;
    }

    public String getInvoicecustomer() {
        return invoicecustomer;
    }

    public void setInvoicecustomer(String invoicecustomer) {
        this.invoicecustomer = invoicecustomer;
    }

    public String getBillingTypeName() {
        return billingTypeName;
    }

    public void setBillingTypeName(String billingTypeName) {
        this.billingTypeName = billingTypeName;
    }

    public String getTripPod() {
        return tripPod;
    }

    public void setTripPod(String tripPod) {
        this.tripPod = tripPod;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getCreditNoteId() {
        return creditNoteId;
    }

    public void setCreditNoteId(String creditNoteId) {
        this.creditNoteId = creditNoteId;
    }

    public String getRefInvoicecode() {
        return refInvoicecode;
    }

    public void setRefInvoicecode(String refInvoicecode) {
        this.refInvoicecode = refInvoicecode;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRefInvoiceDate() {
        return refInvoiceDate;
    }

    public void setRefInvoiceDate(String refInvoiceDate) {
        this.refInvoiceDate = refInvoiceDate;
    }

    public String getCommodityId() {
        return commodityId;
    }

    public void setCommodityId(String commodityId) {
        this.commodityId = commodityId;
    }

    public String getSetGrandRevenue() {
        return setGrandRevenue;
    }

    public void setSetGrandRevenue(String setGrandRevenue) {
        this.setGrandRevenue = setGrandRevenue;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }
    
    
}
