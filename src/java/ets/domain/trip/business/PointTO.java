/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ets.domain.trip.business;

/**
 *
 * @author srini
 */
public class PointTO {
    private String containerKm = "";
    private String pointDistance = "";
    private String consignmentOrderDate = "";
    private String containerLocation = "";
    private String containerLocationId = "";
    private String startReportingDate = "";
    private String startReportingTime = "";
    private String loadingTime = "";
    private String loadingDate = "";
    private String loadingTemperature = "";
    private String consignmentOrderId = "";
    private String consignmentOrderNo = "";
    private String tripId = "";
    private String tripRouteCourseId = "";
    private String consignmentRouteCourseId = "";
    private String pointId = "";
    private String pointName = "";
    private String pointType = "";
    private String pointSequence = "";
    private String pointAddress = "";
    private String pointPlanDate = "";
    private String pointPlanTime = "";
    private String pointPlanTimeHrs = "";
    private String pointPlanTimeMins = "";
    private String destinationId = "";
    private String destinationname = "";
    private String dropAdress = "";


    public String getConsignmentOrderId() {
        return consignmentOrderId;
    }

    public void setConsignmentOrderId(String consignmentOrderId) {
        this.consignmentOrderId = consignmentOrderId;
    }

    public String getPointAddress() {
        return pointAddress;
    }

    public void setPointAddress(String pointAddress) {
        this.pointAddress = pointAddress;
    }

    public String getPointId() {
        return pointId;
    }

    public void setPointId(String pointId) {
        this.pointId = pointId;
    }

    public String getPointName() {
        return pointName;
    }

    public void setPointName(String pointName) {
        this.pointName = pointName;
    }

    public String getPointPlanDate() {
        return pointPlanDate;
    }

    public void setPointPlanDate(String pointPlanDate) {
        this.pointPlanDate = pointPlanDate;
    }

    public String getPointPlanTime() {
        return pointPlanTime;
    }

    public void setPointPlanTime(String pointPlanTime) {
        this.pointPlanTime = pointPlanTime;
    }

    public String getPointSequence() {
        return pointSequence;
    }

    public void setPointSequence(String pointSequence) {
        this.pointSequence = pointSequence;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getPointPlanTimeHrs() {
        return pointPlanTimeHrs;
    }

    public void setPointPlanTimeHrs(String pointPlanTimeHrs) {
        this.pointPlanTimeHrs = pointPlanTimeHrs;
    }

    public String getPointPlanTimeMins() {
        return pointPlanTimeMins;
    }

    public void setPointPlanTimeMins(String pointPlanTimeMins) {
        this.pointPlanTimeMins = pointPlanTimeMins;
    }

    public String getPointType() {
        return pointType;
    }

    public void setPointType(String pointType) {
        this.pointType = pointType;
    }

    public String getTripRouteCourseId() {
        return tripRouteCourseId;
    }

    public void setTripRouteCourseId(String tripRouteCourseId) {
        this.tripRouteCourseId = tripRouteCourseId;
    }

    public String getConsignmentRouteCourseId() {
        return consignmentRouteCourseId;
    }

    public void setConsignmentRouteCourseId(String consignmentRouteCourseId) {
        this.consignmentRouteCourseId = consignmentRouteCourseId;
    }

    public String getConsignmentOrderNo() {
        return consignmentOrderNo;
    }

    public void setConsignmentOrderNo(String consignmentOrderNo) {
        this.consignmentOrderNo = consignmentOrderNo;
    }

    public String getLoadingDate() {
        return loadingDate;
    }

    public void setLoadingDate(String loadingDate) {
        this.loadingDate = loadingDate;
    }

    public String getLoadingTemperature() {
        return loadingTemperature;
    }

    public void setLoadingTemperature(String loadingTemperature) {
        this.loadingTemperature = loadingTemperature;
    }

    public String getLoadingTime() {
        return loadingTime;
    }

    public void setLoadingTime(String loadingTime) {
        this.loadingTime = loadingTime;
    }

    public String getStartReportingDate() {
        return startReportingDate;
    }

    public void setStartReportingDate(String startReportingDate) {
        this.startReportingDate = startReportingDate;
    }

    public String getStartReportingTime() {
        return startReportingTime;
    }

    public void setStartReportingTime(String startReportingTime) {
        this.startReportingTime = startReportingTime;
    }

    public String getConsignmentOrderDate() {
        return consignmentOrderDate;
    }

    public void setConsignmentOrderDate(String consignmentOrderDate) {
        this.consignmentOrderDate = consignmentOrderDate;
    }

    public String getContainerLocation() {
        return containerLocation;
    }

    public void setContainerLocation(String containerLocation) {
        this.containerLocation = containerLocation;
    }

    public String getContainerLocationId() {
        return containerLocationId;
    }

    public void setContainerLocationId(String containerLocationId) {
        this.containerLocationId = containerLocationId;
    }

    public String getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(String destinationId) {
        this.destinationId = destinationId;
    }

    public String getDestinationname() {
        return destinationname;
    }

    public void setDestinationname(String destinationname) {
        this.destinationname = destinationname;
    }

    public String getDropAdress() {
        return dropAdress;
    }

    public void setDropAdress(String dropAdress) {
        this.dropAdress = dropAdress;
    }

    public String getContainerKm() {
        return containerKm;
    }

    public void setContainerKm(String containerKm) {
        this.containerKm = containerKm;
    }

    public String getPointDistance() {
        return pointDistance;
    }

    public void setPointDistance(String pointDistance) {
        this.pointDistance = pointDistance;
    }

    

}
