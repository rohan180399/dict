<%-- 
    Document   : tripSheetPrePrint
    Created on : Feb 5, 2013, 1:03:32 PM
    Author     : Entitle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <link rel="stylesheet" href="/throttle/css/rupees.css"  type="text/css" />

        <%@ page import="ets.domain.renderservice.business.RenderServiceTO" %>
        <%@ page import="ets.domain.renderservice.business.JobCardItemTO" %>
        <%@ page import="ets.domain.problemActivities.business.ProblemActivitiesTO" %>


        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <%@ page import="java.util.*" %>

        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <title>Pink Slip</title>
    </head>

    <script>



        function print()
        {
            var DocumentContainer = document.getElementById("printDiv");
            var WindowObject = window.open('', "TrackHistoryData",
            "width=850,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }

        function back()
        {
            window.history.back()
        }

    </script>


    <body>
        <form name="suggestedPS" method="post">
            <div id="printDiv">
                <br>
                <br>
                <br>


                <table align="center" width="850" border="1" cellspacing="0" cellpadding="0"  style="border:1px; border-color:#E8E8E8; border-style:solid;" >
                    <tr>
                        <td>
                            <table align="center" width="850" border="0" cellspacing="0" cellpadding="0"  style="border:0px; border-color:#E8E8E8; border-style:solid;" >
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <c:if test="${tripDetails != null}">
                                    <c:forEach items="${tripDetails}" var="td">
                                        <c:set var="routeName" value="${td.routeName}"/>
                                        <c:set var="tolocation" value="${td.tolocation}"/>
                                        <c:set var="tripVehicleId" value="${td.tripVehicleId}"/>
                                        <c:set var="depHours" value="${td.depHours}"/>
                                        <c:set var="depMints" value="${td.depMints}"/>
                                        <c:set var="tripDate" value="${td.tripDate}"/>
                                        <c:set var="tripdateP" value="${td.tripdateP}"/>
                                        <c:set var="tripSheetId" value="${td.tripSheetId}"/>
                                        <c:set var="custId" value="${td.custId}"/>
                                        <c:set var="orderNumber" value="${td.orderNumber}"/>
                                        <c:set var="productId" value="${td.productId}"/>
                                        <c:set var="bags" value="${td.bags}"/>
                                        <c:set var="totalTonnage" value="${td.totalTonnage}"/>
                                        <c:set var="tripTotalKms" value="${td.tripTotalKms}"/>
                                        <c:set var="GPSKm" value="${td.GPSKm}"/>
                                        <c:set var="billStatus" value="${td.billStatus}"/>
                                        <c:set var="tripTotalLitres" value="${td.tripTotalLitres}"/>
                                        <c:set var="tripTotalExpenses" value="${td.tripTotalExpenses}"/>
                                        <c:set var="tripTotalAllowances" value="${td.tripTotalAllowances}"/>
                                        <c:set var="tripFuelAmount" value="${td.tripFuelAmount}"/>
                                        <c:set var="tripBalanceAmount" value="${td.tripBalanceAmount}"/>
                                    </c:forEach>
                                </c:if>
                                <tr>
                                    <td width="14%">&emsp;</td>
                                    <td width="14%" ><b>:</b>&nbsp;<c:out value="${depHours}" />:<c:out value="${depMints}"/>:00</td>
                                    <td width="14%">&emsp;</td>
                                    <td width="14%">&emsp;</td>
                                    <td width="14%">&emsp;</td>
                                    <td width="14%"><b>:</b>&nbsp;<c:out value="${tripdateP}" /></td>
                                    <td width="14%">&emsp;</td>                                    
                                </tr>
                                <tr><td>&emsp;</td></tr>
                                <tr><td>&emsp;</td></tr>
                                <tr><td>&emsp;</td></tr>
                                <tr>
                                    <td width="14%" colspan="2">&emsp;</td>
                                    <td width="14%" align="center" ><c:out value="${tripTotalLitres}" /></td>
                                    <td width="14%" align="center" ><c:out value="${tripTotalAllowances}" /></td>
                                    <td width="14%" align="center" ><c:out value="${tripTotalKms}" /></td>
                                    <td width="14%" colspan="2">&emsp;</td>
                                </tr>
                                <tr><td>&emsp;</td></tr>
                                <tr><td>&emsp;</td></tr>
                                <tr><td>&emsp;</td></tr>
                                <tr>
                                    <c:if test = "${customerList != null}" >
                                        <c:forEach items="${customerList}" var="cn">
                                            <c:if test = "${cn.custId == custId}">
                                                <td width="25%" colspan="4" align="center"><c:out value="${cn.custName}"/> </td>
                                            </c:if>
                                        </c:forEach >
                                    </c:if>
                                    <td width="15%" colspan="3" align="left"><c:out value="${tolocation}"/> </td>
                                </tr>
                                <tr><td>&emsp;</td></tr>
                                <tr><td>&emsp;</td></tr>
                                <tr><td>&emsp;</td></tr>
                                <tr><td>&emsp;</td></tr>
                                <tr><td>&emsp;</td></tr>
                                <tr><td>&emsp;</td></tr>
                                <tr>
                                    <td width="14%" colspan="2">&emsp;</td>
                                    <td width="14%" align="center" ><c:out value="${bags}" /></td>
                                    <td width="14%" colspan="4">&emsp;</td>
                                </tr>
                                <tr>
                                    <td width="14%">&emsp;</td>
                                    <td width="14%" align="center" ><c:out value="${orderNumber}" /></td>
                                    <td width="14%" align="center" ><c:out value="${totalTonnage}" /></td>
                                    <c:if test = "${vehicleRegNos != null}" >
                                        <c:forEach items="${vehicleRegNos}" var="vd">
                                            <c:if test = "${vd.vehicleId == tripVehicleId}">
                                                <td width="14%" align="center" ><c:out value="${vd.regNo}" /></td>
                                            </c:if>
                                        </c:forEach >
                                    </c:if>
                                    <td width="14%" align="center" ><c:out value="${totalTonnage}" /></td>
                                    <td width="14%" align="center" ><c:out value="${billStatus}" /></td>
                                    <td width="14%">&emsp;</td>
                                </tr>
                                <tr><td>&emsp;</td></tr>
                                <tr><td>&emsp;</td></tr>
                                <tr>
                                    <td width="14%" colspan="2">&emsp;</td>
                                    <td width="14%" align="center" ><c:out value="${bags}" /></td>
                                    <td width="14%" colspan="4">&emsp;</td>
                                </tr>
                                <tr><td>&emsp;</td></tr>
                                <tr><td>&emsp;</td></tr>
                                <tr><td>&emsp;</td></tr>
                                <tr><td>&emsp;</td></tr>
                                <tr><td>&emsp;</td></tr>
                                <tr><td>&emsp;</td></tr>
                                <tr><td>&emsp;</td></tr>
                                <tr>
                                    <td width="14%" colspan="2">&emsp;</td>
                                    <c:if test = "${vehicleRegNos != null}" >
                                        <c:forEach items="${vehicleRegNos}" var="vd">
                                            <c:if test = "${vd.vehicleId == tripVehicleId}">
                                                <td width="14%" align="left" >&nbsp;<c:out value="${vd.regNo}" /></td>
                                            </c:if>
                                        </c:forEach >
                                    </c:if>
                                    <td width="14%" colspan="4">&emsp;</td>
                                </tr>

                            </table>
                            <table align="center" width="850" border="0" cellspacing="0" cellpadding="0"  style="border:1px; border-color:#E8E8E8; border-style:solid;" >
                                <br>
                                <br>
                                <br>
                                <br>
                                <tr align="center">
                                    <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        <input align="center" type="button" onclick="print();" value = " Print "   />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
