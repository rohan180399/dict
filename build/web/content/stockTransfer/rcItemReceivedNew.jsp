
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ page import="ets.domain.stockTransfer.business.StockTransferTO" %> 
        
        <title>Received Rc ItemList</title>
    </head>
    
    
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script language="javascript">
        
        function submitPage()
        {      
            
         document.stockTransfer.action='/throttle/receivedRcStock.do?gdId='+'<%=request.getAttribute("gdId")%>';
          document.stockTransfer.submit();       
        }        
        function setSelectbox(i)
        {
            var selected=document.getElementsByName("selectedIndex") ;
            selected[i].checked=1;
    }
   function backPage(){
       // document.stockTransfer.action='/throttle/receivedItems.do';
       // document.stockTransfer.submit();
       }
         
    </script>
    <body onload="">
        <form method="post"  name="stockTransfer">                    
<%@ include file="/content/common/path.jsp" %>                        
<%@ include file="/content/common/pageTitle.jsp" %>           
<br>           
<%@ include file="/content/common/message.jsp" %>    
<br>
          
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="550" id="bg">
                <tr>
                    <td colspan="9" height="80" align="center" class="contenthead" >Receive Item</td>                    
                </tr>
                
                
                
                <tr>
                    <td class="text1" height="30">FROM SERVICE POINT</td>
                    <td class="text1" height="30"><%=request.getAttribute("FromServicePoint")%></td>
                    <input type="hidden" name="companyName" value="<%=request.getAttribute("FromServicePoint")%>">
                </tr> 
                <% int index = 0;%> 
                <c:if test = "${receivedStockListAll != null}" >
                    <c:forEach items="${receivedStockListAll}" var="toService"> 
                        <% if (index == 0) {%>    
                        <tr>
                            <td class="text2" height="30">TO SERVICE POINT</td>
                            <td class="text2" height="30"><c:out value="${toService.companyName}"/> </td>
                           
                        </tr>
                        <% index++;
            }%> 
                    </c:forEach>
                </c:if>
          <input type="hidden" name="servicePtId" value="<%=request.getAttribute("servicePtId")%>">
          <input type="hidden" name="gdId" value="<%=request.getAttribute("gdId")%>">       
            </table>
            <br>
            <br>
            <table width="600" align="center" border="0" cellpadding="0" cellspacing="0"  id="bg">              
                
                <c:if test = "${receivedRcItems != null}" >
                <c:if test = "${receivedItems != null}" >
                    <tr>
                        <td colspan="9" align="center" class="contenthead" height="30">Receive Items </td>
                    </tr>  
                    <tr>
                        <td class="text2" height="30"><b>Mfr Code</b></td>
                        <td class="text2" height="30"><b>Papl Code</b></td>                       
                        <td class="text2" height="30"><b>Item Name</b></td>
                        <td class="text2" height="30"><b>Uom</b></td>                                               
                        <td class="text2" height="30"><b>Accepted Qty</b></td>                       
                        <td class="text2" height="30"><b>Price</b></td>                       
                        <td class="text2" height="30"><b>RcItemId</b></td>                       
                    </tr>  
                    <%  index = 0;%>
                    <c:forEach items="${receivedRcItems}" var="rc"> 
                    <c:forEach items="${receivedItems}" var="item"> 
                    <c:if test="${Asl.itemId == Ril.itemId}" >   
                    
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr>
                            <td class="<%=classText %>" height="30"><c:out value="${item.mfrCode}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.paplCode}"/></td>                       
                            <td class="<%=classText %>" height="30"><input type="hidden" name=itemIds value="<c:out value="${item.itemId}"/>"><c:out value="${item.itemName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.uomName}"/></td>                          
                            <td class="<%=classText %>" height="30"><input type="text" name="acceptedQtys" size="5" readonly class="form-control" value="1" > </td>                                                                                                                              
                            <td class="<%=classText %>" height="30"><input type="text" name="prices" size="5" readonly value="<c:out value="${rc.price}"/>"></td>
                            <td class="<%=classText %>" height="30"><input type="text" name="rcItemIds" size="5" readonly value="<c:out value="${rc.rcItemId}"/>"></td>
                            <td width="77" height="30" class="<%=classText %>" ><input type="checkbox" name="selectedIndex" value='<%= index %>'></td>
                        </tr>
                        
                        <%
            index++;
                        %>
                    </c:if>    
                    </c:forEach>
                     </c:forEach> 
                                
            </table>                         
            <br>           
            <br>
            <center>   
                <input type="button" class="button" name="Add" value="Add" onClick="submitPage();" > 
            </c:if>  
                
            </center>
            </c:if>  
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>

