<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
<script type="text/javascript" src="/throttle/js/suest"></script>
<script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
<script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<script language="javascript" src="/throttle/js/validate.js"></script>
<%@ page import="ets.domain.operation.business.OperationTO" %>
<%@ page import="java.util.*" %>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
<link href="/throttle/css/tableFilter.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->


<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
</script>



<script>
    function show_src() {
        document.getElementById('exp_table').style.display = 'none';
    }
    function show_exp() {
        document.getElementById('exp_table').style.display = 'block';
    }
    function show_close() {
        document.getElementById('exp_table').style.display = 'none';
    }


    var httpReq;
    var temp = "";
    function ajaxData()
    {
        if (document.serviceCost.mfrId.value != '0') {
            var url = "/throttle/getModels1.do?mfrId=" + document.serviceCost.mfrId.value;
            if (window.ActiveXObject)
            {
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest)
            {
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function() {
                processAjax();
            };
            httpReq.send(null);
        }
    }
    function processAjax()
    {
        if (httpReq.readyState == 4)
        {
            if (httpReq.status == 200)
            {
                temp = httpReq.responseText.valueOf();
                setOptions(temp, document.serviceCost.modelId);
                if ('<%=request.getAttribute("modelId")%>' != 'null') {
                    document.serviceCost.modelId.value = '<%=request.getAttribute("modelId")%>';
                }

            }
            else
            {
                alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
            }
        }
    }
    function submitPag(val) {

    }



    function searchSubmit() {

        if (document.serviceCost.fromDate.value == '') {
            alert("Please Enter From Date");
            return;
        } else if (document.serviceCost.toDate.value == '') {
            alert("Please Enter to Date");
            return;
        }
        document.serviceCost.action = '/throttle/vehicleServiceCostReport.do'
        document.serviceCost.submit();

    }



    function rateDetails(indx) {
        var regNo = document.getElementsByName("vehicleNo");
        var fromDate = document.getElementsByName("fromDate");
        var toDate = document.getElementsByName("toDate");
        var url = '/throttle/serviceCostBillDetails.do?regNo=' + regNo[indx].value + '&fromDate=' + document.serviceCost.fromDate.value + '&toDate=' + document.serviceCost.toDate.value;
        window.open(url, 'PopupPage', 'height=450,width=650,scrollbars=yes,resizable=yes');
    }
    function setValues() {
        document.serviceCost.regNo.focus();
        if ('<%=request.getAttribute("fromDate")%>' != 'null') {
                document.serviceCost.fromDate.value = '<%=request.getAttribute("fromDate")%>';
                document.serviceCost.toDate.value = '<%=request.getAttribute("toDate")%>';
        }
        if ('<%=request.getAttribute("regNo")%>' != 'null') {
                document.serviceCost.regNo.value = '<%=request.getAttribute("regNo")%>';
        }
        if ('<%=request.getAttribute("usageType")%>' != 'null') {
                document.serviceCost.usageType.value = '<%=request.getAttribute("usageType")%>';
        }
        if ('<%=request.getAttribute("companyId")%>' != 'null') {
                document.serviceCost.companyId.value = '<%=request.getAttribute("companyId")%>';
        }
        if ('<%=request.getAttribute("custId")%>' != 'null') {
                document.serviceCost.custId.value = '<%=request.getAttribute("custId")%>';
        }
        if ('<%=request.getAttribute("spId")%>' != 'null') {
                document.serviceCost.spId.value = '<%=request.getAttribute("spId")%>';
        }
    }

    function printPage()
    {
        var DocumentContainer = document.getElementById("printPage");
        var WindowObject = window.open('', "TrackHistoryData",
                "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        //WindowObject.close();
    }



</script>

        <script type="text/javascript">

     $(document).ready(function() {
                      $('#regNo').autocomplete({
                    source: function (request, response) {
//                     alert("am here...");
                        $.ajax({
                            url: "/throttle/getVehicleNos.do",
                            dataType: "json",
                            data: {
                                regno: request.term
                            },
                            success: function (data, textStatus, jqXHR) {
                                var items = data;
                                response(items);
                            },
                            error: function (data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function (event, ui) {
                     var value = ui.item.Name;
                    $('#regNo').val(value);
                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    var itemVal = item.Name;
                    itemVal = '<font color="green">' + itemVal + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };

            // Use the .autocomplete() method to compile the list based on input from user



        });
        </script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="fmsvehicle.label.VehicleServiceCost" text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="fmsvehicle.label.FMSVehicle" text="default text"/></a></li>
            <li class="active"><spring:message code="fmsvehicle.label.VehicleServiceCost" text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body onload="setValues();">

                <form name="serviceCost"  method="post" >
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>

                    <!--<table width="900" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                        <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                        <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                        </tr>
                        <tr id="exp_table" >
                        <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                            <div class="tabs" align="left" style="width:900;">
                        <ul class="tabNavigation">
                                <li style="background:#76b3f1">Vehicle Service Cost</li>
                        </ul>
                        <div id="first">-->
                    <table class="table table-info mb30 table-hover">
                        <thead><tr><th colspan="4"><spring:message code="fmsvehicle.label.VehicleServiceCost"  text="default text"/></th></tr></thead>
                        <tr>
                            <td align="left" height="30"><spring:message code="fmsvehicle.label.Customer"  text="default text"/></td>
                            <td><select class="form-control" style="width:260px;height:40px;" name="custId"  >
                                    <option value="">---<spring:message code="fmsvehicle.label.Select"  text="default text"/>---</option>
                                    <option value="0"><spring:message code="fmsvehicle.label.Others"  text="default text"/></option>
                                    <c:if test = "${customerList != null}" >
                                        <c:forEach items="${customerList}" var="cust">
                                            <option value='<c:out value="${cust.custId}" />'><c:out value="${cust.custName}" /></option>
                                        </c:forEach >
                                    </c:if>
                                </select> </td>
                            <td align="left" height="30"><spring:message code="fmsvehicle.label.UsageType"  text="default text"/></td>
                            <td height="30"><select class="form-control" style="width:260px;height:40px;" name="usageType"  >
                                    <option value="">---<spring:message code="fmsvehicle.label.Select"  text="default text"/>---</option>
                                    <option value="0"><spring:message code="fmsvehicle.label.Others"  text="default text"/></option>
                                    <c:if test = "${UsageList != null}" >
                                        <c:forEach items="${UsageList}" var="Dept">
                                            <option value='<c:out value="${Dept.usageId}" />'><c:out value="${Dept.usageName}" /></option>
                                        </c:forEach >
                                    </c:if>
                                </select> </td>
                        </tr>
                        <tr>
                            <td align="left" height="30"><spring:message code="fmsvehicle.label.Location"  text="default text"/></td>
                            <td height="30"><select class="form-control" style="width:260px;height:40px;" name="companyId"  >
                                    <option value="">---<spring:message code="fmsvehicle.label.Select"  text="default text"/>---</option>
                                    <option value="0"><spring:message code="fmsvehicle.label.Others"  text="default text"/></option>
                                    <option value='EXTERNAL SERVICE' > <spring:message code="fmsvehicle.label.EXTERNALSERVICE"  text="default text"/> </option>
                                    <c:if test = "${operationPointList != null}" >
                                        <c:forEach items="${operationPointList}" var="Dept">
                                            <option value='<c:out value="${Dept.opId}" />'> <c:out value="${Dept.opName}" /></option>
                                        </c:forEach >
                                    </c:if>
                                </select> </td>
                            <td><spring:message code="fmsvehicle.label.VehicleNo"  text="default text"/></td>
                            <td><input name="regNo" id="regNo" type="text" class="form-control" style="width:260px;height:40px;" size="20"></td>
                        </tr>
                        <tr>
                            <td><spring:message code="fmsvehicle.label.FromDate"  text="default text"/></td>
                            <td><input type="text" name="fromDate" class="datepicker form-control" style="width:260px;height:40px;" >
                            <td><spring:message code="fmsvehicle.label.ToDate"  text="default text"/></td>
                            <td><input name="toDate" type="text" class="datepicker form-control" style="width:260px;height:40px;" >
                        </tr>
                        <tr>
                            <td><spring:message code="fmsvehicle.label.ServicePoint"  text="default text"/></td>
                            <td><select class="form-control" style="width:260px;height:40px;" name="spId"  >
                                    <c:if test = "${companyId == 1011}" >
                                        <option value="0">---<spring:message code="fmsvehicle.label.Select"  text="default text"/>---</option>
                                    </c:if>
                                    <c:if test = "${servicePointList != null}" >
                                        <c:forEach items="${servicePointList}" var="Dept">
                                            <c:if test = "${Dept.spId == companyId && companyId != 1011}" >x
                                                <option selected value='<c:out value="${Dept.spId}" />'> <c:out value="${Dept.spName}" /></option>
                                            </c:if>
                                            <c:if test = "${companyId==1011}" >

                                                <option  value='<c:out value="${Dept.spId}" />'> <c:out value="${Dept.spName}" /></option>
                                            </c:if>
                                        </c:forEach >

                                    </c:if>
                                </select></td>
                            <td></td>

                            <td ><input type="button" class="btn btn-success" readonly name="search" value="<spring:message code="fmsvehicle.label.Search"  text="default text"/>" onClick="searchSubmit();" ></td>
                        </tr>
                    </table>
                    <!--    </div></div>
                        </td>
                        </tr>
                        </table>-->


                    <% int index = 0;
                    String classText = "";
                    int oddEven = 0;
                    %>
                    <c:if test = "${serviceCostList != null}" >

                        <center> <input type="button" class="btn btn-success" name="print" value="<spring:message code="fmsvehicle.label.Print"  text="default text"/>" onClick="printPage();" > </center>
                        <br>                        <center> <input type="button" class="btn btn-success" name="print" value="<spring:message code="fmsvehicle.label.Print"  text="default text"/>" onClick="printPage();" > </center>

                        <div id="printPage" >
                            <table class="table table-info mb30 table-hover" id="bg" >
                                <thead>
                                    <tr >
                                        <th  ><spring:message code="fmsvehicle.label.Sno"  text="default text"/></th>
                                        <th  height="30" ><spring:message code="fmsvehicle.label.MFR"  text="default text"/> </th>
                                        <th  height="30" ><spring:message code="fmsvehicle.label.VehicleNo"  text="default text"/></th>
                                        <th  ><spring:message code="fmsvehicle.label.UsageType"  text="default text"/></th>
                                        <th  ><spring:message code="fmsvehicle.label.Location"  text="default text"/> </th>
                                        <th  ><spring:message code="fmsvehicle.label.Total"  text="default text"/> <br><spring:message code="fmsvehicle.label.Jobcards"  text="default text"/></th>
                                        <th  height="30" ><spring:message code="fmsvehicle.label.SpareAmnt"  text="default text"/> </th>
                                        <th  height="30" ><spring:message code="fmsvehicle.label.LabourAmnt"  text="default text"/> </th>
                                        <th  height="30" ><spring:message code="fmsvehicle.label.ContractAmnt"  text="default text"/> </th>
                                        <th  height="30" ><spring:message code="fmsvehicle.label.TotalAmount"  text="default text"/></th>

                                    </tr>
                                </thead>

                                <c:set var="totJc" value="0" />

                                <c:forEach items="${serviceCostList}" var="service">
                                    <%

                        oddEven = index % 2;
                        if (oddEven > 0) {
                            classText = "text2";
                        } else {
                            classText = "text1";
                        }
                                    %>


                                    <tr>
                                        <td class="<%=classText %>" height="30"> <%= index+1 %> </td>
                                        <td class="<%=classText %>" height="30"><c:out value="${service.mfrName}"/></td>
                                        <td class="<%=classText %>" height="30"><input type="hidden" name="vehicleNo" value="<c:out value="${service.regNo}"/>"><c:out value="${service.regNo}"/></td>
                                        <td class="<%=classText %>" height="30"><c:out value="${service.usageType}"/></td>
                                        <td class="<%=classText %>" height="30"><c:out value="${service.companyName}"/></td>
                                        <td class="<%=classText %>" height="30"><c:out value="${service.totJc}"/></td>
                                        <td class="<%=classText %>" height="30" align="right" ><c:out value="${service.spareAmount}"/></td>
                                        <td class="<%=classText %>" height="30" align="right" ><c:out value="${service.laborAmount}"/></td>
                                        <td class="<%=classText %>" height="30" align="right" ><c:out value="${service.woAmount}"/></td>
                                        <c:set var="billTotal" value="${service.spareAmount + service.laborAmount + service.woAmount }" />
                                        <c:set var="spareTotal" value="${spareTotal + service.spareAmount }" />
                                        <c:set var="laborTotal" value="${laborTotal + service.laborAmount }" />
                                        <c:set var="contractTotal" value="${ contractTotal + service.woAmount}" />
                                        <c:set var="tot" value="${billTotal}" />
                                        <c:set var="totJc" value="${ totJc + service.totJc }" />
                                        <td class="<%=classText %>" height="30" align="right" > <fmt:setLocale value="en_US" /><b> <fmt:formatNumber value="${billTotal}" pattern="##.00"/></b> </td>
                                    </tr>


                                    <%
                        index++;
                                    %>
                                </c:forEach>

                            </table>

                            <style>
                                .text1 {
                                    font-family:Tahoma;
                                    font-size:12px;
                                    color:#333333;
                                    background-color:#E9FBFF;
                                    padding-left:10px;
                                }
                                .text2 {
                                    font-family:Tahoma;
                                    font-size:12px;
                                    color:#333333;
                                    background-color:#FFFFFF;
                                    padding-left:10px;
                                }
                                .contentsub {
                                    background-image:url(/throttle/images/button.gif);
                                    background-repeat:repeat-x;
                                    height:15px;
                                    font-family:Tahoma;
                                    padding-left:5px;
                                    font-size:11px;
                                    font-weight:bold;
                                    color:#0070D4;
                                    text-align:left;
                                    padding-bottom:8px;
                                }

                                .contenthead {
                                    background-image:url(/throttle/images/button.gif);
                                    background-repeat:repeat-x;
                                    height:15px;
                                    font-family:Tahoma;
                                    padding-left:5px;
                                    font-size:11px;
                                    font-weight:bold;
                                    color:#0070D4;
                                    text-align:center;
                                    padding-bottom:8px;
                                }

                            </style>
                        </div>

                        <br>
                        <table  align="center" border="0" cellpadding="0" cellspacing="0" width="500" id="bg" class="border">
                            <tr>
                                <td class="text1" height="30"><spring:message code="fmsvehicle.label.TotalJobcards"  text="default text"/> </td>
                                <td class="text1" height="30"> <b><c:out value="${totJc}"/> </b> </td>
                            </tr>
                            <tr>
                                <td class="text1" height="30"><spring:message code="fmsvehicle.label.SpareAmnt"  text="default text"/> </td>
                                <td class="text1" height="30"> <fmt:setLocale value="en_US" /><b><spring:message code="fmsvehicle.label.SAR"  text="default text"/>: <fmt:formatNumber value="${spareTotal}" pattern="##.00"/></b> </td>
                            </tr>
                            <tr>
                                <td class="text1" height="30"><spring:message code="fmsvehicle.label.LabourAmnt"  text="default text"/> </td>
                                <td class="text1" height="30"> <fmt:setLocale value="en_US" /><b><spring:message code="fmsvehicle.label.SAR"  text="default text"/>: <fmt:formatNumber value="${laborTotal}" pattern="##.00"/></b></td>
                            </tr>
                            <tr>
                                <td class="text1" height="30"><spring:message code="fmsvehicle.label.ContractAmnt"  text="default text"/> </td>
                                <td class="text1" height="30"> <fmt:setLocale value="en_US" /><b><spring:message code="fmsvehicle.label.SAR"  text="default text"/>: <fmt:formatNumber value="${contractTotal}" pattern="##.00"/></b></td>
                            </tr>
                            <tr>
                                <td class="text1" height="30"><spring:message code="fmsvehicle.label.TotalAmount"  text="default text"/> </td>
                                <td class="text1" height="30"> <fmt:setLocale value="en_US" /><b><spring:message code="fmsvehicle.label.SAR"  text="default text"/>: <fmt:formatNumber value="${spareTotal + laborTotal + contractTotal}" pattern="##.00"/></b></td>
                            </tr>
                        </table>
                    </c:if>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
