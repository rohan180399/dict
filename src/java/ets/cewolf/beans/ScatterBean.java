package ets.cewolf.beans;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.data.xy.DefaultWindDataset;
import de.laures.cewolf.DatasetProduceException;
import de.laures.cewolf.DatasetProducer;


public class ScatterBean implements DatasetProducer, Serializable {

	    /**
	 *
	 */
	private static final long serialVersionUID = 1L;
		private static final Log log = LogFactory.getLog(ScatterBean.class);


		/**
		 *  Produces some random data.
		 */
	    public Object produceDataset(Map params) throws DatasetProduceException {
	    	log.debug("producing data.");
	    	  // Dataset can either be populated by hard-coded or else from a datasource
	    	String ds1 = "January 1, 2000";
	    	 String ds2 = "February 1, 2000";
	    	 String ds3 = "March 1, 2000";
	    	 String ds4 = "April 1, 2000";
	    	 String ds5 = "May 1, 2000";


	    	 Date d1= new Date();
	    	 Date d2= new Date();
	    	 Date d3= new Date();
	    	 Date d4= new Date();
	    	 Date d5= new Date();
	    	 DateFormat df = DateFormat.getDateInstance();
	         try {
	        	d1 = df.parse(ds1);
				d2 = df.parse(ds2);
				d3 = df.parse(ds3);
				d4 = df.parse(ds4);
				d5 = df.parse(ds5);
			 } catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}



	    	Object[] item1 = new Object[] {d1, new Integer(10), new Integer(3)};
	    	Object[] item2 = new Object[] {d2, new Integer(5), new Integer(3)};
	    	Object[] item3 = new Object[] {d3, new Integer(4), new Integer(7)};
	    	Object[] item4 = new Object[] {d4, new Integer(1), new Integer(12)};
	    	Object[] item5 = new Object[] {d5, new Integer(9), new Integer(12)};
	    	Object[][] series1 = new Object[][] {item1, item2, item3, item4, item5};
	    	Object[][][] data = new Object[][][] {series1};

	    	DefaultWindDataset datasetWind = new DefaultWindDataset(data){

				/**
				 *
				 */
				private static final long serialVersionUID = 1L;

				protected void finalize() throws Throwable {
					super.finalize();
					log.debug(this +" finalized.");
				}


	        };

				 return datasetWind;




	    }

	    /**
	     * This producer's data is invalidated after 5 seconds. By this method the
	     * producer can influence Cewolf's caching behaviour the way it wants to.
	     */
		public boolean hasExpired(Map params, Date since) {
	        log.debug(getClass().getName() + "hasExpired()");
			return (System.currentTimeMillis() - since.getTime())  > 5000;
		}

		/**
		 * Returns a unique ID for this DatasetProducer
		 */
		public String getProducerId() {
			return "PageViewCountData DatasetProducer";
		}


		/**
		 * @see java.lang.Object#finalize()
		 */
		protected void finalize() throws Throwable {
			super.finalize();
			log.debug(this + " finalized.");
		}


}





