<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        // alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>
<script type="text/javascript">
    function submitPage(value) {
        if (value == 'ExportExcel') {
            document.accountReceivable.action = '/throttle/handleAccountsReceivable.do?param=ExportExcel';
            document.accountReceivable.submit();
        } else {
            document.accountReceivable.action = '/throttle/handleAccountsReceivable.do?param=Search';
            document.accountReceivable.submit();
        }
    }

    function setValue() {
        if ('<%=request.getAttribute("page")%>' != 'null') {
            var page = '<%=request.getAttribute("page")%>';
            if (page == 1) {
                submitPage('search');
            }
        }
    }


    function viewInvoiceDetails(customerId) {
        var fromDate = document.getElementById("fromDate").value;
        var toDate = document.getElementById("toDate").value;
        var invoiceType = document.getElementById("invoiceType").value;
        window.open('/throttle/handleAccountsReceivableDetails.do?customerId=' + customerId + '&param=Search&fromDate=' + fromDate + '&toDate=' + toDate, 'PopupPage', 'height = 800, width = 1150, scrollbars = yes, resizable = yes');
    }
</script>
<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>
<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
            setValues();
            getVehicleNos();">
    </c:if>
    <!--        <span style="float: right">
                <a href="?paramName=en">English</a>
                |
                <a href="?paramName=ar">???????</a>
            </span>-->
    <style type="text/css">





        .container {width: 960px; margin: 0 auto; overflow: hidden;}
        .content {width:800px; margin:0 auto; padding-top:50px;}
        .contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

        /* STOP ANIMATION */



        /* Second Loadin Circle */

        .circle1 {
            background-color: rgba(0,0,0,0);
            border:5px solid rgba(100,183,229,0.9);
            opacity:.9;
            border-left:5px solid rgba(0,0,0,0);
            /*	border-right:5px solid rgba(0,0,0,0);*/
            border-radius:50px;
            /*box-shadow: 0 0 15px #2187e7; */
            /*	box-shadow: 0 0 15px blue;*/
            width:40px;
            height:40px;
            margin:0 auto;
            position:relative;
            top:-50px;
            -moz-animation:spinoffPulse 1s infinite linear;
            -webkit-animation:spinoffPulse 1s infinite linear;
            -ms-animation:spinoffPulse 1s infinite linear;
            -o-animation:spinoffPulse 1s infinite linear;
        }

        @-moz-keyframes spinoffPulse {
            0% { -moz-transform:rotate(0deg); }
            100% { -moz-transform:rotate(360deg);  }
        }
        @-webkit-keyframes spinoffPulse {
            0% { -webkit-transform:rotate(0deg); }
            100% { -webkit-transform:rotate(360deg);  }
        }
        @-ms-keyframes spinoffPulse {
            0% { -ms-transform:rotate(0deg); }
            100% { -ms-transform:rotate(360deg);  }
        }
        @-o-keyframes spinoffPulse {
            0% { -o-transform:rotate(0deg); }
            100% { -o-transform:rotate(360deg);  }
        }



    </style>
    <style>
        #index td {
            color:white;
        }
    </style>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i><spring:message code="operations.reports.label.AccountsReceivableReport" text="default text"/></h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="operations.reports.label.Report" text="default text"/></a></li>
                <li class="active"><spring:message code="operations.reports.label.AccountsReceivableReport" text="default text"/></li>
            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="setValue();">
                    <form name="accountReceivable" action=""  method="post">
                        <!--        <table  id="report" class="table table-bordered" border="0">
                                   <table class="table table-bordered" border="0" >
                                      <tr height="30" id="index">
                                                     <td colspan="4"  style="background-color:#5BC0DE;">
                        <spring:message code="operations.reports.label.AccountsReceivableReport" text="default text"/>
                         </td></tr>
                    <div id="first">
                         <td style="border-color:#5BC0DE;padding:16px;">-->
                        <table class="table table-info mb30 table-hover" border="0" >
                            <thead><tr><th colspan="6"><spring:message code="operations.reports.label.AccountsReceivableReport" text="default text"/></th></tr></thead>
                            <tr>
                                <td><font color="red">*</font><spring:message code="operations.reports.label.Customer" text="default text"/>
                                </td>
                                <td height="30" width="30">
                                    <select  name="customerId" id="customerId" class="form-control"   style="width:260px;height:40px;">
                                        <option value="">---<spring:message code="operations.reports.label.Select" text="default text"/>
                                            ---</option>
                                            <c:forEach items="${customerList}" var="customerList">
                                            <option value='<c:out value="${customerList.custId}"/>'><c:out value="${customerList.custName}"/></option>
                                        </c:forEach>
                                    </select>
                                    <script>
                                        document.getElementById('customerId').value = <c:out value="${customerId}"/>;
                                    </script>
                                </td>
                                <td><font color="red">*</font><spring:message code="operations.reports.label.FromDate" text="default text"/>
                                </td>
                                <td height="30"><input name="fromDate" id="fromDate" type="text"  autocomplete="off" style="width:260px;height:40px;" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                <td><font color="red">*</font><spring:message code="operations.reports.label.ToDate" text="default text"/>
                                </td>
                                <td height="30"><input name="toDate" id="toDate" type="text" autocomplete="off" style="width:260px;height:40px;" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                            </tr>
                            <c:if test="${invoiceType == null}">
                                <tr>
                                    <td><font color="red"></font><spring:message code="operations.reports.label.PaidInvoice" text="default text"/>
                                    </td>
                                    <td height="30"><input name="invoiceType" id="invoiceType" type="radio" value="paid"></td>
                                    <td><font color="red"></font><spring:message code="operations.reports.label.OverDueInvoice" text="default text"/>
                                    </td>
                                    <td height="30"><input name="invoiceType" id="invoiceType" type="radio" value="overDue"></td>
                                    <td colspan="2"><font color="red"></font></td>
                                </tr>
                            </c:if>
                            <c:if test="${invoiceType != null && invoiceType == 'paid'}">
                                <tr>
                                    <td><font color="red"></font><spring:message code="operations.reports.label.PaidInvoice" text="default text"/>
                                    </td>
                                    <td height="30"><input name="invoiceType" id="invoiceType" type="radio" value="paid" checked></td>
                                    <td><font color="red"></font><spring:message code="operations.reports.label.OverDueInvoice" text="default text"/>
                                    </td>
                                    <td height="30"><input name="invoiceType" id="invoiceType" type="radio" value="overDue"></td>
                                    <td colspan="2"><font color="red"></font></td>
                                </tr>
                            </c:if>    
                            <c:if test="${invoiceType != null && invoiceType == 'overDue'}">
                                <tr>
                                    <td><font color="red"></font><spring:message code="operations.reports.label.PaidInvoice" text="default text"/>
                                    </td>
                                    <td height="30"><input name="invoiceType" id="invoiceType" type="radio" value="paid"></td>
                                    <td><font color="red"></font><spring:message code="operations.reports.label.OverDueInvoice" text="default text"/>
                                    </td>
                                    <td height="30"><input name="invoiceType" id="invoiceType" type="radio" value="overDue" checked></td>
                                    <td colspan="2"><font color="red"></font></td>
                                </tr>
                            </c:if>    
                            <tr>
                                <td colspan="3" align="right"><input type="button" class="btn btn-success" name="ExportExcel"   value="<spring:message code="operations.reports.label.EXPORTEXCEL" text="default text"/> " onclick="submitPage(this.name);">&nbsp;&nbsp;</td>
                                <td colspan="3"><input type="button" class="btn btn-success" name="Search"   value="<spring:message code="operations.reports.label.SEARCH" text="default text"/>" onclick="submitPage(this.name);"></td>
                            </tr>
                            <!--                            </table>
                                                    </div></div>
                                            </td>
                                        </tr>
                                    </table>
                                    </table>-->
                            <c:if test = "${customerOverDueList == null && customerOverDueListSize == null}" >
                                <center>
                                    <font color="blue"><spring:message code="operations.reports.label.PleaseWaitYourRequestisProcessing" text="default text"/></font>
                                    <div class="container">
                                        <div class="content">
                                            <div class="circle"></div>
                                            <div class="circle1"></div>
                                        </div>
                                    </div>
                                </center>
                            </c:if>
                            <c:if test = "${customerOverDueList != null}" >
                                <table class="table table-info mb30 table-hover" id="table" >
                                    <thead>
                                        <tr>
                                            <th><spring:message code="operations.reports.label.SNo" text="default text"/></th>
                                            <th><spring:message code="operations.reports.label.CustomerName" text="default text"/></th>
                                            <th><spring:message code="operations.reports.label.TotalAmountReceivable" text="default text"/></th>
                                            <th><spring:message code="operations.reports.label.ReceivableOverDue" text="default text"/></th>
                                            <th><spring:message code="operations.reports.label.ViewDetails" text="default text"/></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <% int index = 0,sno = 1;%>
                                        <c:forEach items="${customerOverDueList}" var="odList">
                                            <%
                                                        String classText = "";
                                                        int oddEven = index % 2;
                                                        if (oddEven > 0) {
                                                            classText = "text2";
                                                        } else {
                                                            classText = "text1";
                                                        }
                                            %>
                                            <c:if test="${invoiceType == null || invoiceType == 'paid'}">
                                                <tr height="30">
                                                    <td align="left" ><%=sno%></td>
                                                    <td align="left" ><c:out value="${odList.customerName}"/> </td>
                                                    <td align="right" ><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${odList.overDueGrandTotal}" /></td>
                                                    <c:set var="overDue" value="0.00"/>
                                                    <c:if test="${odList.overDueGrandTotal > odList.creditLimit && odList.creditLimit >  0.00}">
                                                        <c:set var="overDue" value="${odList.overDueGrandTotal - odList.creditLimit}"/>
                                                    </c:if>
                                                    <c:if test="${odList.overDueGrandTotal == odList.creditLimit && odList.overDueGrandTotal < odList.creditLimit}">
                                                        <c:set var="overDue" value="${0.00}"/>
                                                    </c:if>
                                                    <c:if test="${odList.overDueGrandTotal > odList.creditLimit && odList.creditLimit ==  0.00}">
                                                        <c:set var="overDue" value="${odList.overDueGrandTotal}"/>
                                                    </c:if>
                                            <td align="right" ><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${overDue}" /></td>
                                            </td>
                                            <td align="left" ><a href="#" onclick="viewInvoiceDetails('<c:out value="${odList.customerId}"/>')"><c:out value="${odList.customerCode}"/></a></td>
                                            </tr>
                                            <%
                                              index++;
                                              sno++;
                                            %>
                                        </c:if>
                                        <c:if test="${invoiceType != null && invoiceType == 'overDue'}">
                                            <c:if test="${odList.overDueGrandTotal > odList.creditLimit && odList.creditLimit >  0.00}">
                                                <c:set var="overDue" value="${odList.overDueGrandTotal - odList.creditLimit}"/>
                                            </c:if>
                                            <c:if test="${odList.overDueGrandTotal == odList.creditLimit && odList.overDueGrandTotal < odList.creditLimit}">
                                                <c:set var="overDue" value="${0.00}"/>
                                            </c:if>
                                            <c:if test="${odList.overDueGrandTotal > odList.creditLimit && odList.creditLimit ==  0.00}">
                                                <c:set var="overDue" value="${odList.overDueGrandTotal}"/>
                                            </c:if>

                                            <c:if test="${overDue > 0}">
                                                <tr height="30">    
                                                    <td align="left" ><%=sno%></td>
                                                    <td align="left" ><c:out value="${odList.customerName}"/> </td>
                                                    <td align="right" ><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${odList.overDueGrandTotal}" /></td>
                                                <td align="right" c><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${overDue}" /></td>
                                                </td>
                                                <td align="left" ><a href="#" onclick="viewInvoiceDetails('<c:out value="${odList.customerId}"/>')"><c:out value="${odList.customerCode}"/></a></td>
                                                </tr>
                                                <%
                                                  index++;
                                                  sno++;
                                                %>
                                            </c:if>
                                        </c:if>
                                    </c:forEach>
                                    </tbody>
                                </table>
                                <script language="javascript" type="text/javascript">
                                    setFilterGrid("table");
                                </script>
                                <div id="controls">
                                    <div id="perpage">
                                        <select onchange="sorter.size(this.value)">
                                            <option value="5" selected="selected">5</option>
                                            <option value="10">10</option>
                                            <option value="20">20</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select>
                                        <span><spring:message code="operations.reports.label.EntriesPerPage" text="default text"/></span>
                                    </div>
                                    <div id="navigation">
                                        <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                        <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                        <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                        <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                    </div>
                                    <div id="text"><spring:message code="operations.reports.label.DisplayingPage" text="default text"/> <span id="currentpage"></span><spring:message code="operations.reports.label.of" text="default text"/>  <span id="pagelimit"></span></div>
                                </div>
                                <script type="text/javascript">
                                    var sorter = new TINY.table.sorter("sorter");
                                    sorter.head = "head";
                                    sorter.asc = "asc";
                                    sorter.desc = "desc";
                                    sorter.even = "evenrow";
                                    sorter.odd = "oddrow";
                                    sorter.evensel = "evenselected";
                                    sorter.oddsel = "oddselected";
                                    sorter.paginate = true;
                                    sorter.currentid = "currentpage";
                                    sorter.limitid = "pagelimit";
                                    sorter.init("table", 1);
                                </script>
                        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                    </c:if>
                </body>    
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>