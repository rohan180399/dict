

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
    </head>
    <script language="javascript">
function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}

        function submitPage(){
            var chek=validation();
            if(chek=='true'){                              
                document.workOrderReport.action = '/throttle/workOrderReport.do';
                document.workOrderReport.submit();
            }
        }
        function validation(){
            if(document.workOrderReport.companyId.value==0){
                alert("Please Select Data for Company Name");
                document.workOrderReport.companyId.focus();
                return 'false';
            }           
            else  if(textValidation(document.workOrderReport.fromDate,'From Date')){
                return 'false';
            }
            else  if(textValidation(document.workOrderReport.toDate,'TO Date')){
                return'false';
            }
            return 'true';
        }
        
        function setValues(){
            
            if('<%=request.getAttribute("companyId")%>'!='null'){
                document.workOrderReport.companyId.value='<%=request.getAttribute("companyId")%>';               
                document.workOrderReport.fromDate.value='<%=request.getAttribute("fromDate")%>';
                document.workOrderReport.toDate.value='<%=request.getAttribute("toDate")%>';
            }            
        }
         function details(indx){
               var vehicleId=document.getElementsByName("vehicleId");
               var woId=document.getElementsByName("woId");
               alert(woId[indx].value);
               alert(vehicleId[indx].value);
                window.open('/throttle/getWorkOrderdetails.do?workOrderId='+woId[indx].value+'&vehicleId='+vehicleId[indx].value,'PopupPage', 'height=450,width=800,scrollbars=yes,resizable=yes');
            }
    </script>
    <body onload="setValues();">
        <form name="workOrderReport">
            <%@ include file="/content/common/path.jsp" %>           
            <%@ include file="/content/common/message.jsp" %>

    <table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:850;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">Work Order Report</li>
    </ul>
    <div id="first">
    <table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
  <tr>
        <td height="30">Operation Point</td>
        <td  height="30" class="text1">
            <select  class="form-control" name="companyId" style="width:125px">
            <option value="0">-select-</option>
            <c:if test = "${LocationLists != null}" >
                <c:forEach items="${LocationLists}" var="company">
                    <c:choose>
                        <c:when test="${company.companyTypeId==1011}" >

                            <option value="<c:out value="${company.cmpId}"/>"><c:out value="${company.name}"/></option>
                        </c:when>
                    </c:choose>
                </c:forEach>
            </c:if>
        </select>
        </td>
        <td height="30">From Date</td>
        <td height="30" class="text1"><input name="fromDate" type="text" class="form-control" value="" size="13"> <span class="text2"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.workOrderReport.fromDate,'dd-mm-yyyy',this)"/></span></td>
        <td height="30">To Date</td>
        <td height="30"><input name="toDate" type="text" class="form-control" value="" size="13"> <img src="/throttle/images/cal.gif" name="Calendar" onclick="displayCalendar(document.workOrderReport.toDate,'dd-mm-yyyy',this)"/></td>
        <td height="30"><input type="button" name="" value="Search" class="button" onclick="submitPage();"></td>
        </tr>
    </table>
    </div></div>
    </td>
    </tr>
    </table>

           <br>
            <c:if test = "${woList != null}" >
            <table width="537" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
                <tr>
                    <td class="contenthead" height="30" colspan="5" align="center">Work Order Report Details</td>
                </tr>
                <tr>
                    <td height="30" class="contentsub">S.No</td>
                    <td height="30" class="contentsub">Date</td>
                    <td height="30" class="contentsub">Work Order No</td>
                    <td height="30" class="contentsub">OPeration Point</td>
                    <td height="30" class="contentsub">Status</td>
                </tr>
                <% int index = 0;%> 
                <c:forEach items="${woList}" var="wo"> 
                    <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                    %>
                    <tr>
                    <td  class="<%=classText%>" height="30"><div align="left"><%=index+1%></div></td>
                    <td  class="<%=classText%>" height="30"><div align="left"><c:out value="${wo.createdDate}" /></div></td>
                    <td  class="<%=classText%>" height="30"><div align="left"><input type="hidden" name="woId" value="<c:out value='${wo.woId}' />"><a href="" onClick="details(<%=index%>);"><c:out value="${wo.woId}" /></a></div></td>
                    <td class="<%=classText%>" height="30"><div align="left"><c:out value="${wo.companyName}" /></div></td>
                    <td class="<%=classText%>" height="30"><div align="left"><c:out value="${wo.status}" /></td>  
                    <input type="hidden" name="vehicleId" value="<c:out value="${wo.vehicleId}" />">
                    <tr>
                    <% index++;%>
                </c:forEach> 
            </table>
        </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
