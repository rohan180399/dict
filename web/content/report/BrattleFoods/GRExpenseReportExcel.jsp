<%-- 
    Document   : GRProfitabilityExcel
    Created on : May 2, 2016, 4:22:35 PM
    Author     : gulshan kumar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
            <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "GRSummaryReport-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
            <c:if test = "${GRExpesneList != null}" >
                <table>
                    <center>
                        <font size="4" color="Black"><b>GR Wise Expense </b></font>
                    </center>
                </table>
                <br>
                <table border="1"  align="center" width="800" cellpadding="0" cellspacing="1" >
                    <thead>
                          <tr  height="30">
                            <th  >Sr. No</th>
                            <th  >GR No.</th>
                              <th  >Shipping Bill No.</th>
                                    <th  >BOE</th>
                            <th  >GR Issuing Date</th>
                            <th  >Container No.</th>
                            <th  >Container Size</th>
                            <th  >Movement Type</th>

                             <th  >Status</th>
                             <th  >Trip Sheet No</th>
                              <th  >Transport Type</th>
                             <th  >TT Owner Name</th>
                            <th  >Vehicle No</th>
                             <th  >RouteInfo</th>
                             <th  >Route Distance</th>
                            <th  >Party Name</th>
                            <th  >Diesel Qty</th>
                            <th  >Diesel Expense</th>
                             <th >Detention</th>
                            <th >Toll charges</th>
                            <th >Fooding</th>
                            <th >Misc</th>
                            <th >Other expense</th>
                            <th >Extra Fooding</th>
                            <th >Weightment</th>

                            <th >Behti</th>
                            <th >Green tax</th>
                             <th >Dala</th>
                            <th >Driver Batta</th>
                            <th >Diesel(Cash)</th>
                            <th >Trip Expense</th>
                            <th >R & M</th>
                        </tr>

                    </thead>
                    <% int index = 0, sno = 1;%>
                     
                    <c:forEach items="${GRExpesneList}" var="csList">

           <tr  height="30">
                                <td align="center" class="text1"><%=sno%></td>
                                <td align="center" class="text1"><c:out value="${csList.grNo}"/></td>
                                 <td align="center" class="text1"><c:out value="${csList.shippingBillNo}"/></td>
                                <td align="center" class="text1"><c:out value="${csList.billOfEntryNo}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.grDate}"/></td>
                           <td align="center" class="text1"><c:out value="${csList.containerNo}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.containerSize}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.movementType}"/></td>
                           <td align="center" class="text1"><c:out value="${csList.statusName}"/></td>
                           <td align="center" class="text1"><c:out value="${csList.tripCode}"/></td>
                           <td align="center" class="text1"><c:out value="${csList.transportType}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.transporter}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.vehicleNo}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.routeInfo}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.totalRunKm}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.billingParty}"/></td>
                               <td align="center" class="text1"><c:out value="${csList.dieselQty}"/></td>
                                <td align="center" class="text1"><c:out value="${csList.fuelAmount}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.detaintionAmount}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.tollTax}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.foodingAmount}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.miscAmount}"/></td>

                            <td align="center" class="text1"><c:out value="${csList.otherExpenseAmount}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.extraFoodingAmount}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.weightment}"/></td>

                            <td align="center" class="text1"><c:out value="${csList.behatiAmount}"/></td>

                            <td align="center" class="text1"><c:out value="${csList.greenTaxAmount}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.dalaAmount}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.driverBatta}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.dieselExpense}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.tripExpense}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.rnmAdvance}"/></td>




                        </tr>
                        <%
                            index++;
                            sno++;
                        %>
                    </c:forEach>
                </table>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>

