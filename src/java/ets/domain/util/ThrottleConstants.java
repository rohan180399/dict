/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.util;

import java.util.HashMap;

/**
 *
 * @author M D MADHAN
 */
public class ThrottleConstants {

    public static HashMap<String, String> PROPS_VALUES = new HashMap<String, String>();
    public static String serverIp = PROPS_VALUES.containsKey("serverIp") ? PROPS_VALUES.get("serverIp") : FPUtil.getInstance().getProperty("serverIp");
    public static String webServiceIp = PROPS_VALUES.containsKey("webServiceIp") ? PROPS_VALUES.get("webServiceIp") : FPUtil.getInstance().getProperty("webServiceIp");
    public static String webServiceUpdate = PROPS_VALUES.containsKey("webServiceUpdate") ? PROPS_VALUES.get("webServiceUpdate") : FPUtil.getInstance().getProperty("webServiceUpdate");

    public static String bankAccountsLevelId = PROPS_VALUES.containsKey("bankAccountsLevelId") ? PROPS_VALUES.get("bankAccountsLevelId") : FPUtil.getInstance().getProperty("bankAccountsLevelId");
    public static String bankAccountsGroupCode = PROPS_VALUES.containsKey("bankAccountsGroupCode") ? PROPS_VALUES.get("bankAccountsGroupCode") : FPUtil.getInstance().getProperty("bankAccountsGroupCode");
    public static String vendorLevelId = PROPS_VALUES.containsKey("vendorLevelId") ? PROPS_VALUES.get("vendorLevelId") : FPUtil.getInstance().getProperty("vendorLevelId");
    public static String vendorGroupCode = PROPS_VALUES.containsKey("vendorGroupCode") ? PROPS_VALUES.get("vendorGroupCode") : FPUtil.getInstance().getProperty("vendorGroupCode");
    public static String customerLevelId = PROPS_VALUES.containsKey("customerLevelId") ? PROPS_VALUES.get("customerLevelId") : FPUtil.getInstance().getProperty("customerLevelId");
    public static String customerGroupCode = PROPS_VALUES.containsKey("customerGroupCode") ? PROPS_VALUES.get("customerGroupCode") : FPUtil.getInstance().getProperty("customerGroupCode");
    public static String expenseLevelId = PROPS_VALUES.containsKey("expenseLevelId") ? PROPS_VALUES.get("expenseLevelId") : FPUtil.getInstance().getProperty("expenseLevelId");
    public static String expenseGroupCode = PROPS_VALUES.containsKey("expenseGroupCode") ? PROPS_VALUES.get("expenseGroupCode") : FPUtil.getInstance().getProperty("expenseGroupCode");
    public static String CashOnHandLevelId = PROPS_VALUES.containsKey("CashOnHandLevelId") ? PROPS_VALUES.get("CashOnHandLevelId") : FPUtil.getInstance().getProperty("CashOnHandLevelId");
    public static String CashOnHandLedgerId = PROPS_VALUES.containsKey("CashOnHandLedgerId") ? PROPS_VALUES.get("CashOnHandLedgerId") : FPUtil.getInstance().getProperty("CashOnHandLedgerId");
    public static String CashOnHandLedgerCode = PROPS_VALUES.containsKey("CashOnHandLedgerCode") ? PROPS_VALUES.get("CashOnHandLedgerCode") : FPUtil.getInstance().getProperty("CashOnHandLedgerCode");
    public static String CashOnHandLedgerName = PROPS_VALUES.containsKey("CashOnHandLedgerName") ? PROPS_VALUES.get("CashOnHandLedgerName") : FPUtil.getInstance().getProperty("CashOnHandLedgerName");
    public static String freightIncomeLedgerId = PROPS_VALUES.containsKey("freightIncomeLedgerId") ? PROPS_VALUES.get("freightIncomeLedgerId") : FPUtil.getInstance().getProperty("freightIncomeLedgerId");
    public static String freightIncomeLedgerCode = PROPS_VALUES.containsKey("freightIncomeLedgerCode") ? PROPS_VALUES.get("freightIncomeLedgerCode") : FPUtil.getInstance().getProperty("freightIncomeLedgerCode");
    public static String PLHPLedgerID = PROPS_VALUES.containsKey("PLHPLedgerID") ? PROPS_VALUES.get("PLHPLedgerID") : FPUtil.getInstance().getProperty("PLHPLedgerID");
    public static String PLHPLedgerCode = PROPS_VALUES.containsKey("PLHPLedgerCode") ? PROPS_VALUES.get("PLHPLedgerCode") : FPUtil.getInstance().getProperty("PLHPLedgerCode");
    public static String vehicleInsuranceLedgerId = PROPS_VALUES.containsKey("vehicleInsuranceLedgerId") ? PROPS_VALUES.get("vehicleInsuranceLedgerId") : FPUtil.getInstance().getProperty("vehicleInsuranceLedgerId");
    public static String vehicleInsuranceLedgerCode = PROPS_VALUES.containsKey("vehicleInsuranceLedgerCode") ? PROPS_VALUES.get("vehicleInsuranceLedgerCode") : FPUtil.getInstance().getProperty("vehicleInsuranceLedgerCode");
    public static String vehicleComplianceVendorTypeId = PROPS_VALUES.containsKey("vehicleComplianceVendorTypeId") ? PROPS_VALUES.get("vehicleComplianceVendorTypeId") : FPUtil.getInstance().getProperty("vehicleComplianceVendorTypeId");
    public static String vehicleInsuranceVendorTypeId = PROPS_VALUES.containsKey("vehicleInsuranceVendorTypeId") ? PROPS_VALUES.get("vehicleInsuranceVendorTypeId") : FPUtil.getInstance().getProperty("vehicleInsuranceVendorTypeId");

    public static String trailerInsuranceLedgerCode = PROPS_VALUES.containsKey("trailerInsuranceLedgerCode") ? PROPS_VALUES.get("trailerInsuranceLedgerCode") : FPUtil.getInstance().getProperty("trailerInsuranceLedgerCode");
    public static String vehicleFCLedgerId = PROPS_VALUES.containsKey("vehicleFCLedgerId") ? PROPS_VALUES.get("vehicleFCLedgerId") : FPUtil.getInstance().getProperty("vehicleFCLedgerId");
    public static String vehicleFCLedgerCode = PROPS_VALUES.containsKey("vehicleFCLedgerCode") ? PROPS_VALUES.get("vehicleFCLedgerCode") : FPUtil.getInstance().getProperty("vehicleFCLedgerCode");
    public static String trailerFCLedgerId = PROPS_VALUES.containsKey("trailerFCLedgerId") ? PROPS_VALUES.get("trailerFCLedgerId") : FPUtil.getInstance().getProperty("trailerFCLedgerId");
    public static String trailerFCLedgerCode = PROPS_VALUES.containsKey("trailerFCLedgerCode") ? PROPS_VALUES.get("trailerFCLedgerCode") : FPUtil.getInstance().getProperty("trailerFCLedgerCode");
    public static String trailerInsuranceLedgerId = PROPS_VALUES.containsKey("trailerInsuranceLedgerId") ? PROPS_VALUES.get("trailerInsuranceLedgerId") : FPUtil.getInstance().getProperty("trailerInsuranceLedgerId");
    public static String financialYear = PROPS_VALUES.containsKey("financialYear") ? PROPS_VALUES.get("financialYear") : FPUtil.getInstance().getProperty("financialYear");
    public static String companyName = PROPS_VALUES.containsKey("companyName") ? PROPS_VALUES.get("companyName") : FPUtil.getInstance().getProperty("companyName");
    public static String vehicleInsuranceFormId = PROPS_VALUES.containsKey("vehicleInsuranceFormId") ? PROPS_VALUES.get("vehicleInsuranceFormId") : FPUtil.getInstance().getProperty("vehicleInsuranceFormId");
    public static String trailerInsuranceFormId = PROPS_VALUES.containsKey("trailerInsuranceFormId") ? PROPS_VALUES.get("trailerInsuranceFormId") : FPUtil.getInstance().getProperty("trailerInsuranceFormId");
    public static String invoicePaymentFormId = PROPS_VALUES.containsKey("invoicePaymentFormId") ? PROPS_VALUES.get("invoicePaymentFormId") : FPUtil.getInstance().getProperty("invoicePaymentFormId");
    public static String invoicePaymentFormCode = PROPS_VALUES.containsKey("invoicePaymentFormCode") ? PROPS_VALUES.get("invoicePaymentFormCode") : FPUtil.getInstance().getProperty("invoicePaymentFormCode");
    public static String invoiceReceiptFormId = PROPS_VALUES.containsKey("invoiceReceiptFormId") ? PROPS_VALUES.get("invoiceReceiptFormId") : FPUtil.getInstance().getProperty("invoiceReceiptFormId");
    public static String invoiceReceiptformCode = PROPS_VALUES.containsKey("invoiceReceiptformCode") ? PROPS_VALUES.get("invoiceReceiptformCode") : FPUtil.getInstance().getProperty("invoiceReceiptformCode");
    public static String vehicleFCFormId = PROPS_VALUES.containsKey("vehicleFCFormId") ? PROPS_VALUES.get("vehicleFCFormId") : FPUtil.getInstance().getProperty("vehicleFCFormId");
    public static String trailerFCFormId = PROPS_VALUES.containsKey("trailerFCFormId") ? PROPS_VALUES.get("trailerFCFormId") : FPUtil.getInstance().getProperty("trailerFCFormId");
    public static String invoiceCode = PROPS_VALUES.containsKey("invoiceCode") ? PROPS_VALUES.get("invoiceCode") : FPUtil.getInstance().getProperty("invoiceCode");
    public static String vehicleFCFormCode = PROPS_VALUES.containsKey("vehicleFCFormCode") ? PROPS_VALUES.get("vehicleFCFormCode") : FPUtil.getInstance().getProperty("vehicleFCFormCode");
    public static String trailerFCFormCode = PROPS_VALUES.containsKey("trailerFCFormCode") ? PROPS_VALUES.get("trailerFCFormCode") : FPUtil.getInstance().getProperty("trailerFCFormCode");
    public static String vehicleInsuranceFormCode = PROPS_VALUES.containsKey("vehicleInsuranceFormCode") ? PROPS_VALUES.get("vehicleInsuranceFormCode") : FPUtil.getInstance().getProperty("vehicleInsuranceFormCode");
    public static String trailerInsuranceFormCode = PROPS_VALUES.containsKey("trailerInsuranceFormCode") ? PROPS_VALUES.get("trailerInsuranceFormCode") : FPUtil.getInstance().getProperty("trailerInsuranceFormCode");
    public static String CRJformCode = PROPS_VALUES.containsKey("CRJformCode") ? PROPS_VALUES.get("CRJformCode") : FPUtil.getInstance().getProperty("CRJformCode");
    public static String POcode = PROPS_VALUES.containsKey("POcode") ? PROPS_VALUES.get("POcode") : FPUtil.getInstance().getProperty("POcode");
     public static String CashPaymentFormId = PROPS_VALUES.containsKey("CashPaymentFormId") ? PROPS_VALUES.get("CashPaymentFormId") : FPUtil.getInstance().getProperty("CashPaymentFormId");
    public static String BankPaymentFormId = PROPS_VALUES.containsKey("BankPaymentFormId") ? PROPS_VALUES.get("BankPaymentFormId") : FPUtil.getInstance().getProperty("BankPaymentFormId");
    public static String CashReceiptFormId = PROPS_VALUES.containsKey("CashReceiptFormId") ? PROPS_VALUES.get("CashReceiptFormId") : FPUtil.getInstance().getProperty("CashReceiptFormId");
    public static String BankReceiptFormId = PROPS_VALUES.containsKey("BankReceiptFormId") ? PROPS_VALUES.get("BankReceiptFormId") : FPUtil.getInstance().getProperty("BankReceiptFormId");
    public static String ContraFormId = PROPS_VALUES.containsKey("ContraFormId") ? PROPS_VALUES.get("ContraFormId") : FPUtil.getInstance().getProperty("ContraFormId");
    public static String JournalFormId = PROPS_VALUES.containsKey("JournalFormId") ? PROPS_VALUES.get("JournalFormId") : FPUtil.getInstance().getProperty("JournalFormId");
     public static String CashPaymentVoucherCode = PROPS_VALUES.containsKey("CashPaymentVoucherCode") ? PROPS_VALUES.get("CashPaymentVoucherCode") : FPUtil.getInstance().getProperty("CashPaymentVoucherCode");
    public static String BankPaymentVoucherCode = PROPS_VALUES.containsKey("BankPaymentVoucherCode") ? PROPS_VALUES.get("BankPaymentVoucherCode") : FPUtil.getInstance().getProperty("BankPaymentVoucherCode");
    public static String CashReceiptVoucherCode = PROPS_VALUES.containsKey("CashReceiptVoucherCode") ? PROPS_VALUES.get("CashReceiptVoucherCode") : FPUtil.getInstance().getProperty("CashReceiptVoucherCode");
    public static String BankReceiptVoucherCode = PROPS_VALUES.containsKey("BankReceiptVoucherCode") ? PROPS_VALUES.get("BankReceiptVoucherCode") : FPUtil.getInstance().getProperty("BankReceiptVoucherCode");
    public static String ContraVoucherCode = PROPS_VALUES.containsKey("ContraVoucherCode") ? PROPS_VALUES.get("ContraVoucherCode") : FPUtil.getInstance().getProperty("ContraVoucherCode");
    public static String JournalVoucherCode = PROPS_VALUES.containsKey("JournalVoucherCode") ? PROPS_VALUES.get("JournalVoucherCode") : FPUtil.getInstance().getProperty("JournalVoucherCode");
    public static String bankClearnceCode = PROPS_VALUES.containsKey("bankClearnceCode") ? PROPS_VALUES.get("bankClearnceCode") : FPUtil.getInstance().getProperty("bankClearnceCode");

    public static String Challan = PROPS_VALUES.containsKey("Challan") ? PROPS_VALUES.get("Challan") : FPUtil.getInstance().getProperty("Challan");
    public static String Entry = PROPS_VALUES.containsKey("Entry") ? PROPS_VALUES.get("Entry") : FPUtil.getInstance().getProperty("Entry");
    public static String Loading = PROPS_VALUES.containsKey("Loading") ? PROPS_VALUES.get("Loading") : FPUtil.getInstance().getProperty("Loading");
    public static String Unloading = PROPS_VALUES.containsKey("Unloading") ? PROPS_VALUES.get("Unloading") : FPUtil.getInstance().getProperty("Unloading");
    public static String Other = PROPS_VALUES.containsKey("Other") ? PROPS_VALUES.get("Other") : FPUtil.getInstance().getProperty("Other");
    public static String Marketing = PROPS_VALUES.containsKey("Marketing") ? PROPS_VALUES.get("Marketing") : FPUtil.getInstance().getProperty("Marketing");
    public static String Maintenance = PROPS_VALUES.containsKey("Maintenance") ? PROPS_VALUES.get("Maintenance") : FPUtil.getInstance().getProperty("Maintenance");
    public static String Mamool = PROPS_VALUES.containsKey("Mamool") ? PROPS_VALUES.get("Mamool") : FPUtil.getInstance().getProperty("Mamool");
    public static String TollExpense = PROPS_VALUES.containsKey("TollExpense") ? PROPS_VALUES.get("TollExpense") : FPUtil.getInstance().getProperty("TollExpense");
    public static String ParkingCharge = PROPS_VALUES.containsKey("ParkingCharge") ? PROPS_VALUES.get("ParkingCharge") : FPUtil.getInstance().getProperty("ParkingCharge");
    public static String DetectionCharge = PROPS_VALUES.containsKey("DetectionCharge") ? PROPS_VALUES.get("DetectionCharge") : FPUtil.getInstance().getProperty("DetectionCharge");
    public static String OVERWEIGHT = PROPS_VALUES.containsKey("OVERWEIGHT") ? PROPS_VALUES.get("OVERWEIGHT") : FPUtil.getInstance().getProperty("OVERWEIGHT");
    public static String FINE = PROPS_VALUES.containsKey("FINE") ? PROPS_VALUES.get("FINE") : FPUtil.getInstance().getProperty("FINE");
    public static String BORDERCOST = PROPS_VALUES.containsKey("BORDERCOST") ? PROPS_VALUES.get("BORDERCOST") : FPUtil.getInstance().getProperty("BORDERCOST");
    public static String PERMIT = PROPS_VALUES.containsKey("PERMIT") ? PROPS_VALUES.get("PERMIT") : FPUtil.getInstance().getProperty("PERMIT");
    public static String TRUCKDETENTIONCHARGES = PROPS_VALUES.containsKey("TRUCKDETENTIONCHARGES") ? PROPS_VALUES.get("TRUCKDETENTIONCHARGES") : FPUtil.getInstance().getProperty("TRUCKDETENTIONCHARGES");
    public static String FreightCharge = PROPS_VALUES.containsKey("FreightCharge") ? PROPS_VALUES.get("FreightCharge") : FPUtil.getInstance().getProperty("FreightCharge");
    public static String TransportCharge = PROPS_VALUES.containsKey("TransportCharge") ? PROPS_VALUES.get("TransportCharge") : FPUtil.getInstance().getProperty("TransportCharge");
    public static String CashOnHand = PROPS_VALUES.containsKey("CashOnHand") ? PROPS_VALUES.get("CashOnHand") : FPUtil.getInstance().getProperty("CashOnHand");
    public static String FuelExpense = PROPS_VALUES.containsKey("FuelExpense") ? PROPS_VALUES.get("FuelExpense") : FPUtil.getInstance().getProperty("FuelExpense");
    public static String DriverBhatta = PROPS_VALUES.containsKey("DriverBhatta") ? PROPS_VALUES.get("DriverBhatta") : FPUtil.getInstance().getProperty("DriverBhatta");
    public static String DriverIncentive = PROPS_VALUES.containsKey("DriverIncentive") ? PROPS_VALUES.get("DriverIncentive") : FPUtil.getInstance().getProperty("DriverIncentive");
    public static String MiscCost = PROPS_VALUES.containsKey("MiscCost") ? PROPS_VALUES.get("MiscCost") : FPUtil.getInstance().getProperty("MiscCost");
    public static String HireCharge = PROPS_VALUES.containsKey("HireCharge") ? PROPS_VALUES.get("HireCharge") : FPUtil.getInstance().getProperty("HireCharge");

    public static String cashPaymentFormId = PROPS_VALUES.containsKey("cashPaymentFormId") ? PROPS_VALUES.get("cashPaymentFormId") : FPUtil.getInstance().getProperty("cashPaymentFormId");
    public static String bankPaymentFormId = PROPS_VALUES.containsKey("bankPaymentFormId") ? PROPS_VALUES.get("bankPaymentFormId") : FPUtil.getInstance().getProperty("bankPaymentFormId");
    public static String contraEntryFormId = PROPS_VALUES.containsKey("contraEntryFormId") ? PROPS_VALUES.get("contraEntryFormId") : FPUtil.getInstance().getProperty("contraEntryFormId");
    public static String journalEntryFormId = PROPS_VALUES.containsKey("journalEntryFormId") ? PROPS_VALUES.get("journalEntryFormId") : FPUtil.getInstance().getProperty("journalEntryFormId");
//    public static String invoiceReceiptFormId = PROPS_VALUES.containsKey("invoiceReceiptFormId") ? PROPS_VALUES.get("invoiceReceiptFormId") : FPUtil.getInstance().getProperty("invoiceReceiptFormId");
//    public static String vendorPaymentFormId = PROPS_VALUES.containsKey("vendorPaymentFormId") ? PROPS_VALUES.get("vendorPaymentFormId") : FPUtil.getInstance().getProperty("vendorPaymentFormId");
    public static String creditNoteFormId = PROPS_VALUES.containsKey("creditNoteFormId") ? PROPS_VALUES.get("creditNoteFormId") : FPUtil.getInstance().getProperty("creditNoteFormId");
    public static String debitNoteFormId = PROPS_VALUES.containsKey("debitNoteFormId") ? PROPS_VALUES.get("debitNoteFormId") : FPUtil.getInstance().getProperty("debitNoteFormId");
    public static String tripAdvanceFormId = PROPS_VALUES.containsKey("tripAdvanceFormId") ? PROPS_VALUES.get("tripAdvanceFormId") : FPUtil.getInstance().getProperty("tripAdvanceFormId");
    public static String tripClosureFormId = PROPS_VALUES.containsKey("tripClosureFormId") ? PROPS_VALUES.get("tripClosureFormId") : FPUtil.getInstance().getProperty("tripClosureFormId");
    public static String purchaseFormId = PROPS_VALUES.containsKey("purchaseFormId") ? PROPS_VALUES.get("purchaseFormId") : FPUtil.getInstance().getProperty("purchaseFormId");
    public static String expenseFormId = PROPS_VALUES.containsKey("expenseFormId") ? PROPS_VALUES.get("expenseFormId") : FPUtil.getInstance().getProperty("expenseFormId");
    public static String purchaseVoucherCode = PROPS_VALUES.containsKey("purchaseVoucherCode") ? PROPS_VALUES.get("purchaseVoucherCode") : FPUtil.getInstance().getProperty("purchaseVoucherCode");
    public static String stockInventoryLedgerID = PROPS_VALUES.containsKey("stockInventoryLedgerID") ? PROPS_VALUES.get("stockInventoryLedgerID") : FPUtil.getInstance().getProperty("stockInventoryLedgerID");
    public static String stockInventoryLedgerCode = PROPS_VALUES.containsKey("stockInventoryLedgerCode") ? PROPS_VALUES.get("stockInventoryLedgerCode") : FPUtil.getInstance().getProperty("stockInventoryLedgerCode");
    public static String workshopSparesExpenseLedgerID = PROPS_VALUES.containsKey("workshopSparesExpenseLedgerID") ? PROPS_VALUES.get("workshopSparesExpenseLedgerID") : FPUtil.getInstance().getProperty("workshopSparesExpenseLedgerID");
    public static String workshopSparesExpenseLedgerCode = PROPS_VALUES.containsKey("workshopSparesExpenseLedgerCode") ? PROPS_VALUES.get("workshopSparesExpenseLedgerCode") : FPUtil.getInstance().getProperty("workshopSparesExpenseLedgerCode");
    public static String workshopLaborExpenseLedgerID = PROPS_VALUES.containsKey("workshopLaborExpenseLedgerID") ? PROPS_VALUES.get("workshopLaborExpenseLedgerID") : FPUtil.getInstance().getProperty("workshopLaborExpenseLedgerID");
    public static String workshopLaborExpenseLedgerCode = PROPS_VALUES.containsKey("workshopLaborExpenseLedgerCode") ? PROPS_VALUES.get("workshopLaborExpenseLedgerCode") : FPUtil.getInstance().getProperty("workshopLaborExpenseLedgerCode");
    public static String technicianCostLedgerID = PROPS_VALUES.containsKey("technicianCostLedgerID") ? PROPS_VALUES.get("technicianCostLedgerID") : FPUtil.getInstance().getProperty("technicianCostLedgerID");
    public static String technicianCostLedgerCode = PROPS_VALUES.containsKey("technicianCostLedgerCode") ? PROPS_VALUES.get("technicianCostLedgerCode") : FPUtil.getInstance().getProperty("technicianCostLedgerCode");
    public static String fleetLevelId = PROPS_VALUES.containsKey("fleetLevelId") ? PROPS_VALUES.get("fleetLevelId") : FPUtil.getInstance().getProperty("fleetLevelId");
    public static String fleetGroupCode = PROPS_VALUES.containsKey("fleetGroupCode") ? PROPS_VALUES.get("fleetGroupCode") : FPUtil.getInstance().getProperty("fleetGroupCode");
    public static String tahoosTruckNoPrefix = PROPS_VALUES.containsKey("tahoosTruckNoPrefix") ? PROPS_VALUES.get("tahoosTruckNoPrefix") : FPUtil.getInstance().getProperty("tahoosTruckNoPrefix");
    public static String tahoosLowBedTrailerNoPrefix = PROPS_VALUES.containsKey("tahoosLowBedTrailerNoPrefix") ? PROPS_VALUES.get("tahoosLowBedTrailerNoPrefix") : FPUtil.getInstance().getProperty("tahoosLowBedTrailerNoPrefix");
    public static String tahoosFlatBedTrailerNoPrefix = PROPS_VALUES.containsKey("tahoosFlatBedTrailerNoPrefix") ? PROPS_VALUES.get("tahoosFlatBedTrailerNoPrefix") : FPUtil.getInstance().getProperty("tahoosFlatBedTrailerNoPrefix");
    public static String tahoosWireRugTrailerNoPrefix = PROPS_VALUES.containsKey("tahoosWireRugTrailerNoPrefix") ? PROPS_VALUES.get("tahoosWireRugTrailerNoPrefix") : FPUtil.getInstance().getProperty("tahoosWireRugTrailerNoPrefix");
    public static String fleetPurchaseVendorTypeId = PROPS_VALUES.containsKey("fleetPurchaseVendorTypeId") ? PROPS_VALUES.get("fleetPurchaseVendorTypeId") : FPUtil.getInstance().getProperty("fleetPurchaseVendorTypeId");
    public static String tahoosWaterTankTrailerNoPrefix = PROPS_VALUES.containsKey("tahoosWaterTankTrailerNoPrefix") ? PROPS_VALUES.get("tahoosWaterTankTrailerNoPrefix") : FPUtil.getInstance().getProperty("tahoosWaterTankTrailerNoPrefix");
    public static String tahoosDieselTankTrailerNoPrefix = PROPS_VALUES.containsKey("tahoosDieselTankTrailerNoPrefix") ? PROPS_VALUES.get("tahoosDieselTankTrailerNoPrefix") : FPUtil.getInstance().getProperty("tahoosDieselTankTrailerNoPrefix");
    public static String fleetPurchaseFormCode = PROPS_VALUES.containsKey("fleetPurchaseFormCode") ? PROPS_VALUES.get("fleetPurchaseFormCode") : FPUtil.getInstance().getProperty("fleetPurchaseFormCode");
    public static String tahoosHydrolicLowBedTrailerNoPrefix = PROPS_VALUES.containsKey("tahoosHydrolicLowBedTrailerNoPrefix") ? PROPS_VALUES.get("tahoosHydrolicLowBedTrailerNoPrefix") : FPUtil.getInstance().getProperty("tahoosHydrolicLowBedTrailerNoPrefix");
    public static String driverVendorTypeId = PROPS_VALUES.containsKey("driverVendorTypeId") ? PROPS_VALUES.get("driverVendorTypeId") : FPUtil.getInstance().getProperty("driverVendorTypeId");
    public static String smsSchedulerStatus = PROPS_VALUES.containsKey("smsSchedulerStatus") ? PROPS_VALUES.get("smsSchedulerStatus") : FPUtil.getInstance().getProperty("smsSchedulerStatus");
    public static String tripStatNotificationCCMailId = PROPS_VALUES.containsKey("tripStatNotificationCCMailId") ? PROPS_VALUES.get("tripStatNotificationCCMailId") : FPUtil.getInstance().getProperty("tripStatNotificationCCMailId");
    public static String sourceLatLong = PROPS_VALUES.containsKey("sourceLatLong") ? PROPS_VALUES.get("sourceLatLong") : FPUtil.getInstance().getProperty("sourceLatLong");
    public static String dailyTripReportTo = PROPS_VALUES.containsKey("dailyTripReportTo") ? PROPS_VALUES.get("dailyTripReportTo") : FPUtil.getInstance().getProperty("dailyTripReportTo");
    public static String dailyTripReportCc = PROPS_VALUES.containsKey("dailyTripReportCc") ? PROPS_VALUES.get("dailyTripReportCc") : FPUtil.getInstance().getProperty("dailyTripReportCc");


     public static String smtpServer = PROPS_VALUES.containsKey("smtpServer") ? PROPS_VALUES.get("smtpServer") : FPUtil.getInstance().getProperty("smtpServer");
    public static String smtpPort = PROPS_VALUES.containsKey("smtpPort") ? PROPS_VALUES.get("smtpPort") : FPUtil.getInstance().getProperty("smtpPort");
    public static String fromMailId = PROPS_VALUES.containsKey("fromMailId") ? PROPS_VALUES.get("fromMailId") : FPUtil.getInstance().getProperty("fromMailId");
    public static String fromMailPassword = PROPS_VALUES.containsKey("fromMailPassword") ? PROPS_VALUES.get("fromMailPassword") : FPUtil.getInstance().getProperty("fromMailPassword");
    public static String fuelApprovingMailId = PROPS_VALUES.containsKey("fuelApprovingMailId") ? PROPS_VALUES.get("fuelApprovingMailId") : FPUtil.getInstance().getProperty("fuelApprovingMailId");
    public static String fuelApprovingCCMailId = PROPS_VALUES.containsKey("fuelApprovingCCMailId") ? PROPS_VALUES.get("fuelApprovingCCMailId") : FPUtil.getInstance().getProperty("fuelApprovingCCMailId");
    public static String contractApprovingMailId = PROPS_VALUES.containsKey("contractApprovingMailId") ? PROPS_VALUES.get("contractApprovingMailId") : FPUtil.getInstance().getProperty("contractApprovingMailId");
    public static String contractApprovingCCMailId = PROPS_VALUES.containsKey("contractApprovingCCMailId") ? PROPS_VALUES.get("contractApprovingCCMailId") : FPUtil.getInstance().getProperty("contractApprovingCCMailId");
// //
    public static String csvFileDirectory = PROPS_VALUES.containsKey("csvFileDirectory") ? PROPS_VALUES.get("csvFileDirectory") : FPUtil.getInstance().getProperty("csvFileDirectory");
//
    public static String customerMailStatus = PROPS_VALUES.containsKey("customerMailStatus") ? PROPS_VALUES.get("customerMailStatus") : FPUtil.getInstance().getProperty("customerMailStatus");
//    public static String adminGroupMailIds = PROPS_VALUES.containsKey("adminGroupMailIds") ? PROPS_VALUES.get("adminGroupMailIds") : FPUtil.getInstance().getProperty("adminGroupMailIds");
//
//    scheduler userActivity
    public static String userActivityToMailId = PROPS_VALUES.containsKey("userActivityToMailId") ? PROPS_VALUES.get("userActivityToMailId") : FPUtil.getInstance().getProperty("userActivityToMailId");
    public static String userActivityCcMailId = PROPS_VALUES.containsKey("userActivityCcMailId") ? PROPS_VALUES.get("userActivityCcMailId") : FPUtil.getInstance().getProperty("userActivityCcMailId");
    public static String userActivityBccMailId = PROPS_VALUES.containsKey("userActivityBccMailId") ? PROPS_VALUES.get("userActivityBccMailId") : FPUtil.getInstance().getProperty("userActivityBccMailId");

//    scheduler grSummaryReport
    public static String grSummaryToMailId = PROPS_VALUES.containsKey("grSummaryToMailId") ? PROPS_VALUES.get("grSummaryToMailId") : FPUtil.getInstance().getProperty("grSummaryToMailId");
    public static String grSummaryCcMailId = PROPS_VALUES.containsKey("grSummaryCcMailId") ? PROPS_VALUES.get("grSummaryCcMailId") : FPUtil.getInstance().getProperty("grSummaryCcMailId");
    public static String grSummaryBccMailId = PROPS_VALUES.containsKey("grSummaryBccMailId") ? PROPS_VALUES.get("grSummaryBccMailId") : FPUtil.getInstance().getProperty("grSummaryBccMailId");

//    scheduler DVMReport
    public static String dvmToMailId = PROPS_VALUES.containsKey("dvmToMailId") ? PROPS_VALUES.get("dvmToMailId") : FPUtil.getInstance().getProperty("dvmToMailId");
    public static String dvmCcMailId = PROPS_VALUES.containsKey("dvmCcMailId") ? PROPS_VALUES.get("dvmCcMailId") : FPUtil.getInstance().getProperty("dvmCcMailId");
    public static String dvmBccMailId = PROPS_VALUES.containsKey("dvmBccMailId") ? PROPS_VALUES.get("dvmBccMailId") : FPUtil.getInstance().getProperty("dvmBccMailId");

//    scheduler monthly Order report
    public static String monthlyOrderToMailId = PROPS_VALUES.containsKey("monthlyOrderToMailId") ? PROPS_VALUES.get("monthlyOrderToMailId") : FPUtil.getInstance().getProperty("monthlyOrderToMailId");
    public static String monthlyOrderCcMailId = PROPS_VALUES.containsKey("monthlyOrderCcMailId") ? PROPS_VALUES.get("monthlyOrderCcMailId") : FPUtil.getInstance().getProperty("monthlyOrderCcMailId");
    public static String monthlyOrderBccMailId = PROPS_VALUES.containsKey("monthlyOrderBccMailId") ? PROPS_VALUES.get("monthlyOrderBccMailId") : FPUtil.getInstance().getProperty("monthlyOrderBccMailId");

//    scheduler Daily Order Generated report
    public static String dailyOrderGeneratedToMailId = PROPS_VALUES.containsKey("dailyOrderGeneratedToMailId") ? PROPS_VALUES.get("dailyOrderGeneratedToMailId") : FPUtil.getInstance().getProperty("dailyOrderGeneratedToMailId");
    public static String dailyOrderGeneratedCcMailId = PROPS_VALUES.containsKey("dailyOrderGeneratedCcMailId") ? PROPS_VALUES.get("dailyOrderGeneratedCcMailId") : FPUtil.getInstance().getProperty("dailyOrderGeneratedCcMailId");
    public static String dailyOrderGeneratedBccMailId = PROPS_VALUES.containsKey("dailyOrderGeneratedBccMailId") ? PROPS_VALUES.get("dailyOrderGeneratedBccMailId") : FPUtil.getInstance().getProperty("dailyOrderGeneratedBccMailId");

//    scheduler Daily Process main
    public static String tripEndToMailId = PROPS_VALUES.containsKey("tripEndToMailId") ? PROPS_VALUES.get("tripEndToMailId") : FPUtil.getInstance().getProperty("tripEndToMailId");
    public static String tripEndCcMailId = PROPS_VALUES.containsKey("tripEndCcMailId") ? PROPS_VALUES.get("tripEndCcMailId") : FPUtil.getInstance().getProperty("tripEndCcMailId");
    public static String tripEndBccMailId = PROPS_VALUES.containsKey("tripEndBccMailId") ? PROPS_VALUES.get("tripEndBccMailId") : FPUtil.getInstance().getProperty("tripEndBccMailId");

//    scheduler Daily Process main1
    public static String freezeToMailId = PROPS_VALUES.containsKey("freezeToMailId") ? PROPS_VALUES.get("freezeToMailId") : FPUtil.getInstance().getProperty("freezeToMailId");
    public static String freezeCcMailId = PROPS_VALUES.containsKey("freezeCcMailId") ? PROPS_VALUES.get("freezeCcMailId") : FPUtil.getInstance().getProperty("freezeCcMailId");
    public static String freezeBccMailId = PROPS_VALUES.containsKey("freezeBccMailId") ? PROPS_VALUES.get("freezeBccMailId") : FPUtil.getInstance().getProperty("freezeBccMailId");

    //    scheduler Process main permit
    public static String permitToMailId = PROPS_VALUES.containsKey("permitToMailId") ? PROPS_VALUES.get("permitToMailId") : FPUtil.getInstance().getProperty("permitToMailId");
    public static String permitCcMailId = PROPS_VALUES.containsKey("permitCcMailId") ? PROPS_VALUES.get("permitCcMailId") : FPUtil.getInstance().getProperty("permitCcMailId");
    public static String permitBccMailId = PROPS_VALUES.containsKey("permitBccMailId") ? PROPS_VALUES.get("permitBccMailId") : FPUtil.getInstance().getProperty("permitBccMailId");


    //    scheduler Process main insurance
   public static String insuranceToMailId = PROPS_VALUES.containsKey("insuranceToMailId") ? PROPS_VALUES.get("insuranceToMailId") : FPUtil.getInstance().getProperty("insuranceToMailId");
   public static String insuranceCcMailId = PROPS_VALUES.containsKey("insuranceCcMailId") ? PROPS_VALUES.get("insuranceCcMailId") : FPUtil.getInstance().getProperty("insuranceCcMailId");
   public static String insuranceBccMailId = PROPS_VALUES.containsKey("insuranceBccMailId") ? PROPS_VALUES.get("insuranceBccMailId") : FPUtil.getInstance().getProperty("insuranceBccMailId");

    //    scheduler Process main fc
   public static String fcToMailId = PROPS_VALUES.containsKey("fcToMailId") ? PROPS_VALUES.get("fcToMailId") : FPUtil.getInstance().getProperty("fcToMailId");
   public static String fcCcMailId = PROPS_VALUES.containsKey("fcCcMailId") ? PROPS_VALUES.get("fcCcMailId") : FPUtil.getInstance().getProperty("fcCcMailId");
   public static String fcBccMailId = PROPS_VALUES.containsKey("fcBccMailId") ? PROPS_VALUES.get("fcBccMailId") : FPUtil.getInstance().getProperty("fcBccMailId");

    //    scheduler Process main roadTax
   public static String roadTaxToMailId = PROPS_VALUES.containsKey("fcToMailId") ? PROPS_VALUES.get("roadTaxToMailId") : FPUtil.getInstance().getProperty("roadTaxToMailId");
   public static String roadTaxCcMailId = PROPS_VALUES.containsKey("roadTaxCcMailId") ? PROPS_VALUES.get("roadTaxCcMailId") : FPUtil.getInstance().getProperty("roadTaxCcMailId");
   public static String roadTaxBccMailId = PROPS_VALUES.containsKey("roadTaxBccMailId") ? PROPS_VALUES.get("roadTaxBccMailId") : FPUtil.getInstance().getProperty("roadTaxBccMailId");

    //    scheduler Process main service
   public static String serviceToMailId = PROPS_VALUES.containsKey("serviceToMailId") ? PROPS_VALUES.get("serviceToMailId") : FPUtil.getInstance().getProperty("serviceToMailId");
   public static String serviceCcMailId = PROPS_VALUES.containsKey("serviceCcMailId") ? PROPS_VALUES.get("serviceCcMailId") : FPUtil.getInstance().getProperty("serviceCcMailId");
   public static String serviceBccMailId = PROPS_VALUES.containsKey("serviceBccMailId") ? PROPS_VALUES.get("serviceBccMailId") : FPUtil.getInstance().getProperty("serviceBccMailId");


   //    scheduler Monthly Billing details
   public static String mothlyBillingToMailId = PROPS_VALUES.containsKey("mothlyBillingToMailId") ? PROPS_VALUES.get("mothlyBillingToMailId") : FPUtil.getInstance().getProperty("mothlyBillingToMailId");
   public static String mothlyBillingCcMailId = PROPS_VALUES.containsKey("mothlyBillingCcMailId") ? PROPS_VALUES.get("mothlyBillingCcMailId") : FPUtil.getInstance().getProperty("mothlyBillingCcMailId");
   public static String mothlyBillingBccMailId = PROPS_VALUES.containsKey("mothlyBillingBccMailId") ? PROPS_VALUES.get("mothlyBillingBccMailId") : FPUtil.getInstance().getProperty("mothlyBillingBccMailId");

   // All schedulers Timings
   public static String dailyUserActivityTime = PROPS_VALUES.containsKey("dailyUserActivityTime") ? PROPS_VALUES.get("dailyUserActivityTime") : FPUtil.getInstance().getProperty("dailyUserActivityTime");
   public static String dailyGRSummaryTime = PROPS_VALUES.containsKey("dailyGRSummaryTime") ? PROPS_VALUES.get("dailyGRSummaryTime") : FPUtil.getInstance().getProperty("dailyGRSummaryTime");
   public static String dvmTime = PROPS_VALUES.containsKey("dvmTime") ? PROPS_VALUES.get("dvmTime") : FPUtil.getInstance().getProperty("dvmTime");
   public static String monthlyOrderSummaryTime = PROPS_VALUES.containsKey("monthlyOrderSummaryTime") ? PROPS_VALUES.get("monthlyOrderSummaryTime") : FPUtil.getInstance().getProperty("monthlyOrderSummaryTime");
   public static String dailyOrderSummaryTime = PROPS_VALUES.containsKey("dailyOrderSummaryTime") ? PROPS_VALUES.get("dailyOrderSummaryTime") : FPUtil.getInstance().getProperty("dailyOrderSummaryTime");
   public static String dailyVehicleUpdateTime = PROPS_VALUES.containsKey("dailyVehicleUpdateTime") ? PROPS_VALUES.get("dailyVehicleUpdateTime") : FPUtil.getInstance().getProperty("dailyVehicleUpdateTime");
   public static String dailyProcessTime = PROPS_VALUES.containsKey("dailyProcessTime") ? PROPS_VALUES.get("dailyProcessTime") : FPUtil.getInstance().getProperty("dailyProcessTime");
   public static String vehicleComplianceAlertTime = PROPS_VALUES.containsKey("vehicleComplianceAlertTime") ? PROPS_VALUES.get("vehicleComplianceAlertTime") : FPUtil.getInstance().getProperty("vehicleComplianceAlertTime");

   public static String vendorInvoiceReceipt = PROPS_VALUES.containsKey("vendorInvoiceReceipt") ? PROPS_VALUES.get("vendorInvoiceReceipt") : FPUtil.getInstance().getProperty("vendorInvoiceReceipt");
   public static String leasingVendorTypeId = PROPS_VALUES.containsKey("leasingVendorTypeId") ? PROPS_VALUES.get("leasingVendorTypeId") : FPUtil.getInstance().getProperty("leasingVendorTypeId");
   public static String vendorInvoiceReceiptFormId = PROPS_VALUES.containsKey("vendorInvoiceReceiptFormId") ? PROPS_VALUES.get("vendorInvoiceReceiptFormId") : FPUtil.getInstance().getProperty("vendorInvoiceReceiptFormId");
   public static String vendorInvoiceReceiptVoucherCode = PROPS_VALUES.containsKey("vendorInvoiceReceiptVoucherCode") ? PROPS_VALUES.get("vendorInvoiceReceiptVoucherCode") : FPUtil.getInstance().getProperty("vendorInvoiceReceiptVoucherCode");
   public static String dailyTripStatusTime = PROPS_VALUES.containsKey("dailyTripStatusTime") ? PROPS_VALUES.get("dailyTripStatusTime") : FPUtil.getInstance().getProperty("dailyTripStatusTime");
   public static String tripPlannedToMailId = PROPS_VALUES.containsKey("tripPlannedToMailId") ? PROPS_VALUES.get("tripPlannedToMailId") : FPUtil.getInstance().getProperty("tripPlannedToMailId");
   public static String tripPlannedCCMailId = PROPS_VALUES.containsKey("tripPlannedCCMailId") ? PROPS_VALUES.get("tripPlannedCCMailId") : FPUtil.getInstance().getProperty("tripPlannedCCMailId");
   public static String tripPlannedBccMailId = PROPS_VALUES.containsKey("tripPlannedBccMailId") ? PROPS_VALUES.get("tripPlannedBccMailId") : FPUtil.getInstance().getProperty("tripPlannedBccMailId");
   public static String vehicleutilisedToMailId = PROPS_VALUES.containsKey("vehicleutilisedToMailId") ? PROPS_VALUES.get("vehicleutilisedToMailId") : FPUtil.getInstance().getProperty("vehicleutilisedToMailId");
   public static String vehicleutilisedCCMailId = PROPS_VALUES.containsKey("vehicleutilisedCCMailId") ? PROPS_VALUES.get("vehicleutilisedCCMailId") : FPUtil.getInstance().getProperty("vehicleutilisedCCMailId");
   public static String vehicleutilisedBccMailId = PROPS_VALUES.containsKey("vehicleutilisedBccMailId") ? PROPS_VALUES.get("vehicleutilisedBccMailId") : FPUtil.getInstance().getProperty("vehicleutilisedBccMailId");
   public static String contractdiffToMailId = PROPS_VALUES.containsKey("contractdiffToMailId") ? PROPS_VALUES.get("contractdiffToMailId") : FPUtil.getInstance().getProperty("contractdiffToMailId");
   public static String contractdiffCCMailId = PROPS_VALUES.containsKey("contractdiffCCMailId") ? PROPS_VALUES.get("contractdiffCCMailId") : FPUtil.getInstance().getProperty("contractdiffCCMailId");
   public static String contractdiffBccMailId = PROPS_VALUES.containsKey("contractdiffBccMailId") ? PROPS_VALUES.get("contractdiffBccMailId") : FPUtil.getInstance().getProperty("contractdiffBccMailId");
   public static String vehicleutilisedTime = PROPS_VALUES.containsKey("vehicleutilisedTime") ? PROPS_VALUES.get("vehicleutilisedTime") : FPUtil.getInstance().getProperty("vehicleutilisedTime");
   public static String contractdiffTime = PROPS_VALUES.containsKey("contractdiffTime") ? PROPS_VALUES.get("contractdiffTime") : FPUtil.getInstance().getProperty("contractdiffTime");
   public static String approvalMailId = PROPS_VALUES.containsKey("approvalMailId") ? PROPS_VALUES.get("approvalMailId") : FPUtil.getInstance().getProperty("approvalMailId");
   public static String alertReportPath = PROPS_VALUES.containsKey("alertReportPath") ? PROPS_VALUES.get("alertReportPath") : FPUtil.getInstance().getProperty("alertReportPath");
   
   public static String customerContractExpiryTime = PROPS_VALUES.containsKey("customerContractExpiryTime") ? PROPS_VALUES.get("customerContractExpiryTime") : FPUtil.getInstance().getProperty("customerContractExpiryTime");
   public static String contractExpiryToMailId = PROPS_VALUES.containsKey("contractExpiryToMailId") ? PROPS_VALUES.get("contractExpiryToMailId") : FPUtil.getInstance().getProperty("contractExpiryToMailId");
   public static String contractExpiryCCMailId = PROPS_VALUES.containsKey("contractExpiryCCMailId") ? PROPS_VALUES.get("contractExpiryCCMailId") : FPUtil.getInstance().getProperty("contractExpiryCCMailId");
   public static String contractExpiryBccMailId = PROPS_VALUES.containsKey("contractExpiryBccMailId") ? PROPS_VALUES.get("contractExpiryBccMailId") : FPUtil.getInstance().getProperty("contractExpiryBccMailId");
   
   public static String ownerId = PROPS_VALUES.containsKey("ownerId") ? PROPS_VALUES.get("ownerId") : FPUtil.getInstance().getProperty("ownerId");
   public static String gstIn = PROPS_VALUES.containsKey("gstIn") ? PROPS_VALUES.get("gstIn") : FPUtil.getInstance().getProperty("gstIn");
   public static String tokenId = PROPS_VALUES.containsKey("gstIn") ? PROPS_VALUES.get("gstIn") : FPUtil.getInstance().getProperty("tokenId");
   public static String custInvMailTime = PROPS_VALUES.containsKey("custInvMailTime") ? PROPS_VALUES.get("custInvMailTime") : FPUtil.getInstance().getProperty("custInvMailTime");
   public static String custInvMailIds = PROPS_VALUES.containsKey("custInvMailIds") ? PROPS_VALUES.get("custInvMailIds") : FPUtil.getInstance().getProperty("custInvMailIds");
   public static String custInvFromMailId = PROPS_VALUES.containsKey("custInvFromMailId") ? PROPS_VALUES.get("custInvFromMailId") : FPUtil.getInstance().getProperty("custInvFromMailId");
   public static String custInvpassword = PROPS_VALUES.containsKey("custInvpassword") ? PROPS_VALUES.get("custInvpassword") : FPUtil.getInstance().getProperty("custInvpassword");

    public static void loadProps() {
    serverIp = PROPS_VALUES.containsKey("serverIp") ? PROPS_VALUES.get("serverIp") : FPUtil.getInstance().getProperty("serverIp");
    webServiceIp = PROPS_VALUES.containsKey("webServiceIp") ? PROPS_VALUES.get("webServiceIp") : FPUtil.getInstance().getProperty("webServiceIp");
    webServiceUpdate = PROPS_VALUES.containsKey("webServiceUpdate") ? PROPS_VALUES.get("webServiceUpdate") : FPUtil.getInstance().getProperty("webServiceUpdate");
    alertReportPath = PROPS_VALUES.containsKey("alertReportPath") ? PROPS_VALUES.get("alertReportPath") : FPUtil.getInstance().getProperty("alertReportPath");

    bankAccountsLevelId = PROPS_VALUES.containsKey("bankAccountsLevelId") ? PROPS_VALUES.get("bankAccountsLevelId") : FPUtil.getInstance().getProperty("bankAccountsLevelId");
    bankAccountsGroupCode = PROPS_VALUES.containsKey("bankAccountsGroupCode") ? PROPS_VALUES.get("bankAccountsGroupCode") : FPUtil.getInstance().getProperty("bankAccountsGroupCode");
    vendorLevelId = PROPS_VALUES.containsKey("vendorLevelId") ? PROPS_VALUES.get("vendorLevelId") : FPUtil.getInstance().getProperty("vendorLevelId");
    vendorGroupCode = PROPS_VALUES.containsKey("vendorGroupCode") ? PROPS_VALUES.get("vendorGroupCode") : FPUtil.getInstance().getProperty("vendorGroupCode");
    customerLevelId = PROPS_VALUES.containsKey("customerLevelId") ? PROPS_VALUES.get("customerLevelId") : FPUtil.getInstance().getProperty("customerLevelId");
    customerGroupCode = PROPS_VALUES.containsKey("customerGroupCode") ? PROPS_VALUES.get("customerGroupCode") : FPUtil.getInstance().getProperty("customerGroupCode");
    expenseLevelId = PROPS_VALUES.containsKey("expenseLevelId") ? PROPS_VALUES.get("expenseLevelId") : FPUtil.getInstance().getProperty("expenseLevelId");
    expenseGroupCode = PROPS_VALUES.containsKey("expenseGroupCode") ? PROPS_VALUES.get("expenseGroupCode") : FPUtil.getInstance().getProperty("expenseGroupCode");
    CashOnHandLevelId = PROPS_VALUES.containsKey("CashOnHandLevelId") ? PROPS_VALUES.get("CashOnHandLevelId") : FPUtil.getInstance().getProperty("CashOnHandLevelId");
    CashOnHandLedgerId = PROPS_VALUES.containsKey("CashOnHandLedgerId") ? PROPS_VALUES.get("CashOnHandLedgerId") : FPUtil.getInstance().getProperty("CashOnHandLedgerId");
    CashOnHandLedgerCode = PROPS_VALUES.containsKey("CashOnHandLedgerCode") ? PROPS_VALUES.get("CashOnHandLedgerCode") : FPUtil.getInstance().getProperty("CashOnHandLedgerCode");
    CashOnHandLedgerName = PROPS_VALUES.containsKey("CashOnHandLedgerName") ? PROPS_VALUES.get("CashOnHandLedgerName") : FPUtil.getInstance().getProperty("CashOnHandLedgerName");
    freightIncomeLedgerId = PROPS_VALUES.containsKey("freightIncomeLedgerId") ? PROPS_VALUES.get("freightIncomeLedgerId") : FPUtil.getInstance().getProperty("freightIncomeLedgerId");
    freightIncomeLedgerCode = PROPS_VALUES.containsKey("freightIncomeLedgerCode") ? PROPS_VALUES.get("freightIncomeLedgerCode") : FPUtil.getInstance().getProperty("freightIncomeLedgerCode");
    PLHPLedgerID = PROPS_VALUES.containsKey("PLHPLedgerID") ? PROPS_VALUES.get("PLHPLedgerID") : FPUtil.getInstance().getProperty("PLHPLedgerID");
    PLHPLedgerCode = PROPS_VALUES.containsKey("PLHPLedgerCode") ? PROPS_VALUES.get("PLHPLedgerCode") : FPUtil.getInstance().getProperty("PLHPLedgerCode");
    vehicleInsuranceLedgerId = PROPS_VALUES.containsKey("vehicleInsuranceLedgerId") ? PROPS_VALUES.get("vehicleInsuranceLedgerId") : FPUtil.getInstance().getProperty("vehicleInsuranceLedgerId");
    vehicleInsuranceLedgerCode = PROPS_VALUES.containsKey("vehicleInsuranceLedgerCode") ? PROPS_VALUES.get("vehicleInsuranceLedgerCode") : FPUtil.getInstance().getProperty("vehicleInsuranceLedgerCode");
    vehicleComplianceVendorTypeId = PROPS_VALUES.containsKey("vehicleComplianceVendorTypeId") ? PROPS_VALUES.get("vehicleComplianceVendorTypeId") : FPUtil.getInstance().getProperty("vehicleComplianceVendorTypeId");
    vehicleInsuranceVendorTypeId = PROPS_VALUES.containsKey("vehicleInsuranceVendorTypeId") ? PROPS_VALUES.get("vehicleInsuranceVendorTypeId") : FPUtil.getInstance().getProperty("vehicleInsuranceVendorTypeId");
    trailerInsuranceLedgerCode = PROPS_VALUES.containsKey("trailerInsuranceLedgerCode") ? PROPS_VALUES.get("trailerInsuranceLedgerCode") : FPUtil.getInstance().getProperty("trailerInsuranceLedgerCode");
    vehicleFCLedgerId = PROPS_VALUES.containsKey("vehicleFCLedgerId") ? PROPS_VALUES.get("vehicleFCLedgerId") : FPUtil.getInstance().getProperty("vehicleFCLedgerId");
    vehicleFCLedgerCode = PROPS_VALUES.containsKey("vehicleFCLedgerCode") ? PROPS_VALUES.get("vehicleFCLedgerCode") : FPUtil.getInstance().getProperty("vehicleFCLedgerCode");
    trailerFCLedgerId = PROPS_VALUES.containsKey("trailerFCLedgerId") ? PROPS_VALUES.get("trailerFCLedgerId") : FPUtil.getInstance().getProperty("trailerFCLedgerId");
    trailerFCLedgerCode = PROPS_VALUES.containsKey("trailerFCLedgerCode") ? PROPS_VALUES.get("trailerFCLedgerCode") : FPUtil.getInstance().getProperty("trailerFCLedgerCode");
    trailerInsuranceLedgerId = PROPS_VALUES.containsKey("trailerInsuranceLedgerId") ? PROPS_VALUES.get("trailerInsuranceLedgerId") : FPUtil.getInstance().getProperty("trailerInsuranceLedgerId");
    financialYear = PROPS_VALUES.containsKey("financialYear") ? PROPS_VALUES.get("financialYear") : FPUtil.getInstance().getProperty("financialYear");
    companyName = PROPS_VALUES.containsKey("companyName") ? PROPS_VALUES.get("companyName") : FPUtil.getInstance().getProperty("companyName");
    vehicleInsuranceFormId = PROPS_VALUES.containsKey("vehicleInsuranceFormId") ? PROPS_VALUES.get("vehicleInsuranceFormId") : FPUtil.getInstance().getProperty("vehicleInsuranceFormId");
    trailerInsuranceFormId = PROPS_VALUES.containsKey("trailerInsuranceFormId") ? PROPS_VALUES.get("trailerInsuranceFormId") : FPUtil.getInstance().getProperty("trailerInsuranceFormId");
    invoicePaymentFormId = PROPS_VALUES.containsKey("invoicePaymentFormId") ? PROPS_VALUES.get("invoicePaymentFormId") : FPUtil.getInstance().getProperty("invoicePaymentFormId");
    invoicePaymentFormCode = PROPS_VALUES.containsKey("invoicePaymentFormCode") ? PROPS_VALUES.get("invoicePaymentFormCode") : FPUtil.getInstance().getProperty("invoicePaymentFormCode");
    invoiceReceiptFormId = PROPS_VALUES.containsKey("invoiceReceiptFormId") ? PROPS_VALUES.get("invoiceReceiptFormId") : FPUtil.getInstance().getProperty("invoiceReceiptFormId");
    invoiceReceiptformCode = PROPS_VALUES.containsKey("invoiceReceiptformCode") ? PROPS_VALUES.get("invoiceReceiptformCode") : FPUtil.getInstance().getProperty("invoiceReceiptformCode");
    vehicleFCFormId = PROPS_VALUES.containsKey("vehicleFCFormId") ? PROPS_VALUES.get("vehicleFCFormId") : FPUtil.getInstance().getProperty("vehicleFCFormId");
    trailerFCFormId = PROPS_VALUES.containsKey("trailerFCFormId") ? PROPS_VALUES.get("trailerFCFormId") : FPUtil.getInstance().getProperty("trailerFCFormId");
    invoiceCode = PROPS_VALUES.containsKey("invoiceCode") ? PROPS_VALUES.get("invoiceCode") : FPUtil.getInstance().getProperty("invoiceCode");
    vehicleFCFormCode = PROPS_VALUES.containsKey("vehicleFCFormCode") ? PROPS_VALUES.get("vehicleFCFormCode") : FPUtil.getInstance().getProperty("vehicleFCFormCode");
    vehicleInsuranceFormCode = PROPS_VALUES.containsKey("vehicleInsuranceFormCode") ? PROPS_VALUES.get("vehicleInsuranceFormCode") : FPUtil.getInstance().getProperty("vehicleInsuranceFormCode");
    trailerInsuranceFormCode = PROPS_VALUES.containsKey("trailerInsuranceFormCode") ? PROPS_VALUES.get("trailerInsuranceFormCode") : FPUtil.getInstance().getProperty("trailerInsuranceFormCode");
    CRJformCode = PROPS_VALUES.containsKey("CRJformCode") ? PROPS_VALUES.get("CRJformCode") : FPUtil.getInstance().getProperty("CRJformCode");
    POcode = PROPS_VALUES.containsKey("POcode") ? PROPS_VALUES.get("POcode") : FPUtil.getInstance().getProperty("POcode");
    CashPaymentFormId = PROPS_VALUES.containsKey("CashPaymentFormId") ? PROPS_VALUES.get("CashPaymentFormId") : FPUtil.getInstance().getProperty("CashPaymentFormId");
    BankPaymentFormId = PROPS_VALUES.containsKey("BankPaymentFormId") ? PROPS_VALUES.get("BankPaymentFormId") : FPUtil.getInstance().getProperty("BankPaymentFormId");
    CashReceiptFormId = PROPS_VALUES.containsKey("CashReceiptFormId") ? PROPS_VALUES.get("CashReceiptFormId") : FPUtil.getInstance().getProperty("CashReceiptFormId");
    BankReceiptFormId = PROPS_VALUES.containsKey("BankReceiptFormId") ? PROPS_VALUES.get("BankReceiptFormId") : FPUtil.getInstance().getProperty("BankReceiptFormId");
    ContraFormId = PROPS_VALUES.containsKey("ContraFormId") ? PROPS_VALUES.get("ContraFormId") : FPUtil.getInstance().getProperty("ContraFormId");
    JournalFormId = PROPS_VALUES.containsKey("JournalFormId") ? PROPS_VALUES.get("JournalFormId") : FPUtil.getInstance().getProperty("JournalFormId");
    CashPaymentVoucherCode = PROPS_VALUES.containsKey("CashPaymentVoucherCode") ? PROPS_VALUES.get("CashPaymentVoucherCode") : FPUtil.getInstance().getProperty("CashPaymentVoucherCode");
    BankPaymentVoucherCode = PROPS_VALUES.containsKey("BankPaymentVoucherCode") ? PROPS_VALUES.get("BankPaymentVoucherCode") : FPUtil.getInstance().getProperty("BankPaymentVoucherCode");
    CashReceiptVoucherCode = PROPS_VALUES.containsKey("CashReceiptVoucherCode") ? PROPS_VALUES.get("CashReceiptVoucherCode") : FPUtil.getInstance().getProperty("CashReceiptVoucherCode");
    BankReceiptVoucherCode = PROPS_VALUES.containsKey("BankReceiptVoucherCode") ? PROPS_VALUES.get("BankReceiptVoucherCode") : FPUtil.getInstance().getProperty("BankReceiptVoucherCode");
    ContraVoucherCode = PROPS_VALUES.containsKey("ContraVoucherCode") ? PROPS_VALUES.get("ContraVoucherCode") : FPUtil.getInstance().getProperty("ContraVoucherCode");
    JournalVoucherCode = PROPS_VALUES.containsKey("JournalVoucherCode") ? PROPS_VALUES.get("JournalVoucherCode") : FPUtil.getInstance().getProperty("JournalVoucherCode");
    bankClearnceCode = PROPS_VALUES.containsKey("bankClearnceCode") ? PROPS_VALUES.get("bankClearnceCode") : FPUtil.getInstance().getProperty("bankClearnceCode");


    Challan = PROPS_VALUES.containsKey("Challan") ? PROPS_VALUES.get("Challan") : FPUtil.getInstance().getProperty("Challan");
    Entry = PROPS_VALUES.containsKey("Entry") ? PROPS_VALUES.get("Entry") : FPUtil.getInstance().getProperty("Entry");
    Loading = PROPS_VALUES.containsKey("Loading") ? PROPS_VALUES.get("Loading") : FPUtil.getInstance().getProperty("Loading");
    Unloading = PROPS_VALUES.containsKey("Unloading") ? PROPS_VALUES.get("Unloading") : FPUtil.getInstance().getProperty("Unloading");
    Other = PROPS_VALUES.containsKey("Other") ? PROPS_VALUES.get("Other") : FPUtil.getInstance().getProperty("Other");
    Marketing = PROPS_VALUES.containsKey("Marketing") ? PROPS_VALUES.get("Marketing") : FPUtil.getInstance().getProperty("Marketing");
    Maintenance = PROPS_VALUES.containsKey("Maintenance") ? PROPS_VALUES.get("Maintenance") : FPUtil.getInstance().getProperty("Maintenance");
    Mamool = PROPS_VALUES.containsKey("Mamool") ? PROPS_VALUES.get("Mamool") : FPUtil.getInstance().getProperty("Mamool");
    TollExpense = PROPS_VALUES.containsKey("TollExpense") ? PROPS_VALUES.get("TollExpense") : FPUtil.getInstance().getProperty("TollExpense");
    ParkingCharge = PROPS_VALUES.containsKey("ParkingCharge") ? PROPS_VALUES.get("ParkingCharge") : FPUtil.getInstance().getProperty("ParkingCharge");
    DetectionCharge = PROPS_VALUES.containsKey("DetectionCharge") ? PROPS_VALUES.get("DetectionCharge") : FPUtil.getInstance().getProperty("DetectionCharge");
    OVERWEIGHT = PROPS_VALUES.containsKey("OVERWEIGHT") ? PROPS_VALUES.get("OVERWEIGHT") : FPUtil.getInstance().getProperty("OVERWEIGHT");
    FINE = PROPS_VALUES.containsKey("FINE") ? PROPS_VALUES.get("FINE") : FPUtil.getInstance().getProperty("FINE");
    BORDERCOST = PROPS_VALUES.containsKey("BORDERCOST") ? PROPS_VALUES.get("BORDERCOST") : FPUtil.getInstance().getProperty("BORDERCOST");
    PERMIT = PROPS_VALUES.containsKey("PERMIT") ? PROPS_VALUES.get("PERMIT") : FPUtil.getInstance().getProperty("PERMIT");
    TRUCKDETENTIONCHARGES = PROPS_VALUES.containsKey("TRUCKDETENTIONCHARGES") ? PROPS_VALUES.get("TRUCKDETENTIONCHARGES") : FPUtil.getInstance().getProperty("TRUCKDETENTIONCHARGES");
    FreightCharge = PROPS_VALUES.containsKey("FreightCharge") ? PROPS_VALUES.get("FreightCharge") : FPUtil.getInstance().getProperty("FreightCharge");
    TransportCharge = PROPS_VALUES.containsKey("TransportCharge") ? PROPS_VALUES.get("TransportCharge") : FPUtil.getInstance().getProperty("TransportCharge");
    CashOnHand = PROPS_VALUES.containsKey("CashOnHand") ? PROPS_VALUES.get("CashOnHand") : FPUtil.getInstance().getProperty("CashOnHand");
    FuelExpense = PROPS_VALUES.containsKey("FuelExpense") ? PROPS_VALUES.get("FuelExpense") : FPUtil.getInstance().getProperty("FuelExpense");
    DriverBhatta = PROPS_VALUES.containsKey("DriverBhatta") ? PROPS_VALUES.get("DriverBhatta") : FPUtil.getInstance().getProperty("DriverBhatta");
    DriverIncentive = PROPS_VALUES.containsKey("DriverIncentive") ? PROPS_VALUES.get("DriverIncentive") : FPUtil.getInstance().getProperty("DriverIncentive");
    MiscCost = PROPS_VALUES.containsKey("MiscCost") ? PROPS_VALUES.get("MiscCost") : FPUtil.getInstance().getProperty("MiscCost");
    HireCharge = PROPS_VALUES.containsKey("HireCharge") ? PROPS_VALUES.get("HireCharge") : FPUtil.getInstance().getProperty("HireCharge");
    cashPaymentFormId = PROPS_VALUES.containsKey("cashPaymentFormId") ? PROPS_VALUES.get("cashPaymentFormId") : FPUtil.getInstance().getProperty("cashPaymentFormId");
    bankPaymentFormId = PROPS_VALUES.containsKey("bankPaymentFormId") ? PROPS_VALUES.get("bankPaymentFormId") : FPUtil.getInstance().getProperty("bankPaymentFormId");
    contraEntryFormId = PROPS_VALUES.containsKey("contraEntryFormId") ? PROPS_VALUES.get("contraEntryFormId") : FPUtil.getInstance().getProperty("contraEntryFormId");
    journalEntryFormId = PROPS_VALUES.containsKey("journalEntryFormId") ? PROPS_VALUES.get("journalEntryFormId") : FPUtil.getInstance().getProperty("journalEntryFormId");
//    invoiceReceiptFormId = PROPS_VALUES.containsKey("invoiceReceiptFormId") ? PROPS_VALUES.get("invoiceReceiptFormId") : FPUtil.getInstance().getProperty("invoiceReceiptFormId");
//    vendorPaymentFormId = PROPS_VALUES.containsKey("vendorPaymentFormId") ? PROPS_VALUES.get("vendorPaymentFormId") : FPUtil.getInstance().getProperty("vendorPaymentFormId");
    creditNoteFormId = PROPS_VALUES.containsKey("creditNoteFormId") ? PROPS_VALUES.get("creditNoteFormId") : FPUtil.getInstance().getProperty("creditNoteFormId");
    debitNoteFormId = PROPS_VALUES.containsKey("debitNoteFormId") ? PROPS_VALUES.get("debitNoteFormId") : FPUtil.getInstance().getProperty("debitNoteFormId");
    tripAdvanceFormId = PROPS_VALUES.containsKey("tripAdvanceFormId") ? PROPS_VALUES.get("tripAdvanceFormId") : FPUtil.getInstance().getProperty("tripAdvanceFormId");
    tripClosureFormId = PROPS_VALUES.containsKey("tripClosureFormId") ? PROPS_VALUES.get("tripClosureFormId") : FPUtil.getInstance().getProperty("tripClosureFormId");
    purchaseFormId = PROPS_VALUES.containsKey("purchaseFormId") ? PROPS_VALUES.get("purchaseFormId") : FPUtil.getInstance().getProperty("purchaseFormId");
    purchaseVoucherCode = PROPS_VALUES.containsKey("purchaseVoucherCode") ? PROPS_VALUES.get("purchaseVoucherCode") : FPUtil.getInstance().getProperty("purchaseVoucherCode");
    stockInventoryLedgerID = PROPS_VALUES.containsKey("stockInventoryLedgerID") ? PROPS_VALUES.get("stockInventoryLedgerID") : FPUtil.getInstance().getProperty("stockInventoryLedgerID");
    stockInventoryLedgerCode = PROPS_VALUES.containsKey("stockInventoryLedgerCode") ? PROPS_VALUES.get("stockInventoryLedgerCode") : FPUtil.getInstance().getProperty("stockInventoryLedgerCode");
    driverVendorTypeId = PROPS_VALUES.containsKey("driverVendorTypeId") ? PROPS_VALUES.get("driverVendorTypeId") : FPUtil.getInstance().getProperty("driverVendorTypeId");
    smsSchedulerStatus = PROPS_VALUES.containsKey("smsSchedulerStatus") ? PROPS_VALUES.get("smsSchedulerStatus") : FPUtil.getInstance().getProperty("smsSchedulerStatus");
    tripStatNotificationCCMailId = PROPS_VALUES.containsKey("tripStatNotificationCCMailId") ? PROPS_VALUES.get("tripStatNotificationCCMailId") : FPUtil.getInstance().getProperty("tripStatNotificationCCMailId");
    sourceLatLong = PROPS_VALUES.containsKey("sourceLatLong") ? PROPS_VALUES.get("sourceLatLong") : FPUtil.getInstance().getProperty("sourceLatLong");
    dailyTripReportTo = PROPS_VALUES.containsKey("dailyTripReportTo") ? PROPS_VALUES.get("dailyTripReportTo") : FPUtil.getInstance().getProperty("dailyTripReportTo");
    dailyTripReportCc = PROPS_VALUES.containsKey("dailyTripReportCc") ? PROPS_VALUES.get("dailyTripReportCc") : FPUtil.getInstance().getProperty("dailyTripReportCc");
    dailyTripStatusTime = PROPS_VALUES.containsKey("dailyTripStatusTime") ? PROPS_VALUES.get("dailyTripStatusTime") : FPUtil.getInstance().getProperty("dailyTripStatusTime");

    smtpServer = PROPS_VALUES.containsKey("smtpServer") ? PROPS_VALUES.get("smtpServer") : FPUtil.getInstance().getProperty("smtpServer");
        smtpPort = PROPS_VALUES.containsKey("smtpPort") ? PROPS_VALUES.get("smtpPort") : FPUtil.getInstance().getProperty("smtpPort");
        fromMailId = PROPS_VALUES.containsKey("fromMailId") ? PROPS_VALUES.get("fromMailId") : FPUtil.getInstance().getProperty("fromMailId");
        fromMailPassword = PROPS_VALUES.containsKey("fromMailPassword") ? PROPS_VALUES.get("fromMailPassword") : FPUtil.getInstance().getProperty("fromMailPassword");
        fuelApprovingMailId = PROPS_VALUES.containsKey("fuelApprovingMailId") ? PROPS_VALUES.get("fuelApprovingMailId") : FPUtil.getInstance().getProperty("fuelApprovingMailId");
        fuelApprovingCCMailId = PROPS_VALUES.containsKey("fuelApprovingCCMailId") ? PROPS_VALUES.get("fuelApprovingCCMailId") : FPUtil.getInstance().getProperty("fuelApprovingCCMailId");
        contractApprovingMailId = PROPS_VALUES.containsKey("contractApprovingMailId") ? PROPS_VALUES.get("contractApprovingMailId") : FPUtil.getInstance().getProperty("contractApprovingMailId");
        contractApprovingCCMailId = PROPS_VALUES.containsKey("contractApprovingCCMailId") ? PROPS_VALUES.get("contractApprovingCCMailId") : FPUtil.getInstance().getProperty("contractApprovingCCMailId");

        csvFileDirectory = PROPS_VALUES.containsKey("csvFileDirectory") ? PROPS_VALUES.get("csvFileDirectory") : FPUtil.getInstance().getProperty("csvFileDirectory");

        customerMailStatus = PROPS_VALUES.containsKey("customerMailStatus") ? PROPS_VALUES.get("customerMailStatus") : FPUtil.getInstance().getProperty("customerMailStatus");
//        adminGroupMailIds = PROPS_VALUES.containsKey("adminGroupMailIds") ? PROPS_VALUES.get("adminGroupMailIds") : FPUtil.getInstance().getProperty("adminGroupMailIds");
//
    //    scheduler userActivity
    userActivityToMailId = PROPS_VALUES.containsKey("userActivityToMailId") ? PROPS_VALUES.get("userActivityToMailId") : FPUtil.getInstance().getProperty("userActivityToMailId");
    userActivityCcMailId = PROPS_VALUES.containsKey("userActivityCcMailId") ? PROPS_VALUES.get("userActivityCcMailId") : FPUtil.getInstance().getProperty("userActivityCcMailId");
    userActivityBccMailId = PROPS_VALUES.containsKey("userActivityBccMailId") ? PROPS_VALUES.get("userActivityBccMailId") : FPUtil.getInstance().getProperty("userActivityBccMailId");

//    scheduler grSummaryReport
    grSummaryToMailId = PROPS_VALUES.containsKey("grSummaryToMailId") ? PROPS_VALUES.get("grSummaryToMailId") : FPUtil.getInstance().getProperty("grSummaryToMailId");
    grSummaryCcMailId = PROPS_VALUES.containsKey("grSummaryCcMailId") ? PROPS_VALUES.get("grSummaryCcMailId") : FPUtil.getInstance().getProperty("grSummaryCcMailId");
    grSummaryBccMailId = PROPS_VALUES.containsKey("grSummaryBccMailId") ? PROPS_VALUES.get("grSummaryBccMailId") : FPUtil.getInstance().getProperty("grSummaryBccMailId");

//    scheduler DVMReport
    dvmToMailId = PROPS_VALUES.containsKey("dvmToMailId") ? PROPS_VALUES.get("dvmToMailId") : FPUtil.getInstance().getProperty("dvmToMailId");
    dvmCcMailId = PROPS_VALUES.containsKey("dvmCcMailId") ? PROPS_VALUES.get("dvmCcMailId") : FPUtil.getInstance().getProperty("dvmCcMailId");
    dvmBccMailId = PROPS_VALUES.containsKey("dvmBccMailId") ? PROPS_VALUES.get("dvmBccMailId") : FPUtil.getInstance().getProperty("dvmBccMailId");

//    scheduler monthly Order report
    monthlyOrderToMailId = PROPS_VALUES.containsKey("monthlyOrderToMailId") ? PROPS_VALUES.get("monthlyOrderToMailId") : FPUtil.getInstance().getProperty("monthlyOrderToMailId");
    monthlyOrderCcMailId = PROPS_VALUES.containsKey("monthlyOrderCcMailId") ? PROPS_VALUES.get("monthlyOrderCcMailId") : FPUtil.getInstance().getProperty("monthlyOrderCcMailId");
    monthlyOrderBccMailId = PROPS_VALUES.containsKey("monthlyOrderBccMailId") ? PROPS_VALUES.get("monthlyOrderBccMailId") : FPUtil.getInstance().getProperty("monthlyOrderBccMailId");

 //    scheduler Daily Order Generated report
     dailyOrderGeneratedToMailId = PROPS_VALUES.containsKey("dailyOrderGeneratedToMailId") ? PROPS_VALUES.get("dailyOrderGeneratedToMailId") : FPUtil.getInstance().getProperty("dailyOrderGeneratedToMailId");
    dailyOrderGeneratedCcMailId = PROPS_VALUES.containsKey("dailyOrderGeneratedCcMailId") ? PROPS_VALUES.get("dailyOrderGeneratedCcMailId") : FPUtil.getInstance().getProperty("dailyOrderGeneratedCcMailId");
    dailyOrderGeneratedBccMailId = PROPS_VALUES.containsKey("dailyOrderGeneratedBccMailId") ? PROPS_VALUES.get("dailyOrderGeneratedBccMailId") : FPUtil.getInstance().getProperty("dailyOrderGeneratedBccMailId");


 //    scheduler Daily Process Main trip
    tripEndToMailId = PROPS_VALUES.containsKey("tripEndToMailId") ? PROPS_VALUES.get("tripEndToMailId") : FPUtil.getInstance().getProperty("tripEndToMailId");
    tripEndCcMailId = PROPS_VALUES.containsKey("tripEndCcMailId") ? PROPS_VALUES.get("tripEndCcMailId") : FPUtil.getInstance().getProperty("tripEndCcMailId");
    tripEndBccMailId = PROPS_VALUES.containsKey("tripEndBccMailId") ? PROPS_VALUES.get("tripEndBccMailId") : FPUtil.getInstance().getProperty("tripEndBccMailId");

//    scheduler Daily Process main Freeze
    freezeToMailId = PROPS_VALUES.containsKey("freezeToMailId") ? PROPS_VALUES.get("freezeToMailId") : FPUtil.getInstance().getProperty("freezeToMailId");
    freezeCcMailId = PROPS_VALUES.containsKey("freezeCcMailId") ? PROPS_VALUES.get("freezeCcMailId") : FPUtil.getInstance().getProperty("freezeCcMailId");
    freezeBccMailId = PROPS_VALUES.containsKey("freezeBccMailId") ? PROPS_VALUES.get("freezeBccMailId") : FPUtil.getInstance().getProperty("freezeBccMailId");

//    scheduler Process main permit
    permitToMailId = PROPS_VALUES.containsKey("permitToMailId") ? PROPS_VALUES.get("permitToMailId") : FPUtil.getInstance().getProperty("permitToMailId");
    permitCcMailId = PROPS_VALUES.containsKey("permitCcMailId") ? PROPS_VALUES.get("permitCcMailId") : FPUtil.getInstance().getProperty("permitCcMailId");
    permitBccMailId = PROPS_VALUES.containsKey("permitBccMailId") ? PROPS_VALUES.get("permitBccMailId") : FPUtil.getInstance().getProperty("permitBccMailId");

//    scheduler Process main insurance
    insuranceToMailId = PROPS_VALUES.containsKey("insuranceToMailId") ? PROPS_VALUES.get("insuranceToMailId") : FPUtil.getInstance().getProperty("insuranceToMailId");
    insuranceCcMailId = PROPS_VALUES.containsKey("insuranceCcMailId") ? PROPS_VALUES.get("insuranceCcMailId") : FPUtil.getInstance().getProperty("insuranceCcMailId");
    insuranceBccMailId = PROPS_VALUES.containsKey("insuranceBccMailId") ? PROPS_VALUES.get("insuranceBccMailId") : FPUtil.getInstance().getProperty("insuranceBccMailId");

//    scheduler Process main fc
   fcToMailId = PROPS_VALUES.containsKey("fcToMailId") ? PROPS_VALUES.get("fcToMailId") : FPUtil.getInstance().getProperty("fcToMailId");
   fcCcMailId = PROPS_VALUES.containsKey("fcCcMailId") ? PROPS_VALUES.get("fcCcMailId") : FPUtil.getInstance().getProperty("fcCcMailId");
   fcBccMailId = PROPS_VALUES.containsKey("fcBccMailId") ? PROPS_VALUES.get("fcBccMailId") : FPUtil.getInstance().getProperty("fcBccMailId");

  //    scheduler Process main roadTax
   roadTaxToMailId = PROPS_VALUES.containsKey("fcToMailId") ? PROPS_VALUES.get("roadTaxToMailId") : FPUtil.getInstance().getProperty("roadTaxToMailId");
   roadTaxCcMailId = PROPS_VALUES.containsKey("roadTaxCcMailId") ? PROPS_VALUES.get("roadTaxCcMailId") : FPUtil.getInstance().getProperty("roadTaxCcMailId");
   roadTaxBccMailId = PROPS_VALUES.containsKey("roadTaxBccMailId") ? PROPS_VALUES.get("roadTaxBccMailId") : FPUtil.getInstance().getProperty("roadTaxBccMailId");

    //    scheduler Process main service
   serviceToMailId = PROPS_VALUES.containsKey("serviceToMailId") ? PROPS_VALUES.get("serviceToMailId") : FPUtil.getInstance().getProperty("serviceToMailId");
   serviceCcMailId = PROPS_VALUES.containsKey("serviceCcMailId") ? PROPS_VALUES.get("serviceCcMailId") : FPUtil.getInstance().getProperty("serviceCcMailId");
   serviceBccMailId = PROPS_VALUES.containsKey("serviceBccMailId") ? PROPS_VALUES.get("serviceBccMailId") : FPUtil.getInstance().getProperty("serviceBccMailId");


   //    scheduler Monthly Billing Details
   mothlyBillingToMailId = PROPS_VALUES.containsKey("mothlyBillingToMailId") ? PROPS_VALUES.get("mothlyBillingToMailId") : FPUtil.getInstance().getProperty("mothlyBillingToMailId");
   mothlyBillingCcMailId = PROPS_VALUES.containsKey("mothlyBillingCcMailId") ? PROPS_VALUES.get("mothlyBillingCcMailId") : FPUtil.getInstance().getProperty("mothlyBillingCcMailId");
   mothlyBillingBccMailId = PROPS_VALUES.containsKey("mothlyBillingBccMailId") ? PROPS_VALUES.get("mothlyBillingBccMailId") : FPUtil.getInstance().getProperty("mothlyBillingBccMailId");


   CashPaymentVoucherCode = PROPS_VALUES.containsKey("CashPaymentVoucherCode") ? PROPS_VALUES.get("CashPaymentVoucherCode") : FPUtil.getInstance().getProperty("CashPaymentVoucherCode");
   CashOnHandLedgerId = PROPS_VALUES.containsKey("CashOnHandLedgerId") ? PROPS_VALUES.get("CashOnHandLedgerId") : FPUtil.getInstance().getProperty("CashOnHandLedgerId");
   CashOnHandLedgerCode = PROPS_VALUES.containsKey("CashOnHandLedgerCode") ? PROPS_VALUES.get("CashOnHandLedgerCode") : FPUtil.getInstance().getProperty("CashOnHandLedgerCode");



   //   All schedulers Timings
    dailyUserActivityTime = PROPS_VALUES.containsKey("dailyUserActivityTime") ? PROPS_VALUES.get("dailyUserActivityTime") : FPUtil.getInstance().getProperty("dailyUserActivityTime");
   dailyGRSummaryTime = PROPS_VALUES.containsKey("dailyGRSummaryTime") ? PROPS_VALUES.get("dailyGRSummaryTime") : FPUtil.getInstance().getProperty("dailyGRSummaryTime");
   dvmTime = PROPS_VALUES.containsKey("dvmTime") ? PROPS_VALUES.get("dvmTime") : FPUtil.getInstance().getProperty("dvmTime");
   monthlyOrderSummaryTime = PROPS_VALUES.containsKey("monthlyOrderSummaryTime") ? PROPS_VALUES.get("monthlyOrderSummaryTime") : FPUtil.getInstance().getProperty("monthlyOrderSummaryTime");
   dailyOrderSummaryTime = PROPS_VALUES.containsKey("dailyOrderSummaryTime") ? PROPS_VALUES.get("dailyOrderSummaryTime") : FPUtil.getInstance().getProperty("dailyOrderSummaryTime");
    dailyVehicleUpdateTime = PROPS_VALUES.containsKey("dailyVehicleUpdateTime") ? PROPS_VALUES.get("dailyVehicleUpdateTime") : FPUtil.getInstance().getProperty("dailyVehicleUpdateTime");
   dailyProcessTime = PROPS_VALUES.containsKey("dailyProcessTime") ? PROPS_VALUES.get("dailyProcessTime") : FPUtil.getInstance().getProperty("dailyProcessTime");
   vehicleComplianceAlertTime = PROPS_VALUES.containsKey("vehicleComplianceAlertTime") ? PROPS_VALUES.get("vehicleComplianceAlertTime") : FPUtil.getInstance().getProperty("vehicleComplianceAlertTime");

vendorInvoiceReceipt = PROPS_VALUES.containsKey("vendorInvoiceReceipt") ? PROPS_VALUES.get("vendorInvoiceReceipt") : FPUtil.getInstance().getProperty("vendorInvoiceReceipt");
leasingVendorTypeId = PROPS_VALUES.containsKey("leasingVendorTypeId") ? PROPS_VALUES.get("leasingVendorTypeId") : FPUtil.getInstance().getProperty("leasingVendorTypeId");
vendorInvoiceReceiptFormId = PROPS_VALUES.containsKey("vendorInvoiceReceiptFormId") ? PROPS_VALUES.get("vendorInvoiceReceiptFormId") : FPUtil.getInstance().getProperty("vendorInvoiceReceiptFormId");
vendorInvoiceReceiptVoucherCode = PROPS_VALUES.containsKey("vendorInvoiceReceiptVoucherCode") ? PROPS_VALUES.get("vendorInvoiceReceiptVoucherCode") : FPUtil.getInstance().getProperty("vendorInvoiceReceiptVoucherCode");


   tripPlannedToMailId = PROPS_VALUES.containsKey("tripPlannedToMailId") ? PROPS_VALUES.get("tripPlannedToMailId") : FPUtil.getInstance().getProperty("tripPlannedToMailId");
   tripPlannedCCMailId = PROPS_VALUES.containsKey("tripPlannedCCMailId") ? PROPS_VALUES.get("tripPlannedCCMailId") : FPUtil.getInstance().getProperty("tripPlannedCCMailId");
   tripPlannedBccMailId = PROPS_VALUES.containsKey("tripPlannedBccMailId") ? PROPS_VALUES.get("tripPlannedBccMailId") : FPUtil.getInstance().getProperty("tripPlannedBccMailId");

    vehicleutilisedToMailId = PROPS_VALUES.containsKey("vehicleutilisedToMailId") ? PROPS_VALUES.get("vehicleutilisedToMailId") : FPUtil.getInstance().getProperty("vehicleutilisedToMailId");
    vehicleutilisedCCMailId = PROPS_VALUES.containsKey("vehicleutilisedCCMailId") ? PROPS_VALUES.get("vehicleutilisedCCMailId") : FPUtil.getInstance().getProperty("vehicleutilisedCCMailId");
    vehicleutilisedBccMailId = PROPS_VALUES.containsKey("vehicleutilisedBccMailId") ? PROPS_VALUES.get("vehicleutilisedBccMailId") : FPUtil.getInstance().getProperty("vehicleutilisedBccMailId");
     vehicleutilisedTime = PROPS_VALUES.containsKey("vehicleutilisedTime") ? PROPS_VALUES.get("vehicleutilisedTime") : FPUtil.getInstance().getProperty("vehicleutilisedTime");
   contractdiffTime = PROPS_VALUES.containsKey("contractdiffTime") ? PROPS_VALUES.get("contractdiffTime") : FPUtil.getInstance().getProperty("contractdiffTime");
   approvalMailId = PROPS_VALUES.containsKey("approvalMailId") ? PROPS_VALUES.get("approvalMailId") : FPUtil.getInstance().getProperty("approvalMailId");
  
   customerContractExpiryTime = PROPS_VALUES.containsKey("customerContractExpiryTime") ? PROPS_VALUES.get("customerContractExpiryTime") : FPUtil.getInstance().getProperty("customerContractExpiryTime");
   contractExpiryToMailId = PROPS_VALUES.containsKey("contractExpiryToMailId") ? PROPS_VALUES.get("contractExpiryToMailId") : FPUtil.getInstance().getProperty("contractExpiryToMailId");
   contractExpiryCCMailId = PROPS_VALUES.containsKey("contractExpiryCCMailId") ? PROPS_VALUES.get("contractExpiryCCMailId") : FPUtil.getInstance().getProperty("contractExpiryCCMailId");
   contractExpiryBccMailId = PROPS_VALUES.containsKey("contractExpiryBccMailId") ? PROPS_VALUES.get("contractExpiryBccMailId") : FPUtil.getInstance().getProperty("contractExpiryBccMailId");

   ownerId = PROPS_VALUES.containsKey("ownerId") ? PROPS_VALUES.get("ownerId") : FPUtil.getInstance().getProperty("ownerId");
   gstIn = PROPS_VALUES.containsKey("gstIn") ? PROPS_VALUES.get("gstIn") : FPUtil.getInstance().getProperty("gstIn");
   tokenId = PROPS_VALUES.containsKey("gstIn") ? PROPS_VALUES.get("gstIn") : FPUtil.getInstance().getProperty("tokenId");
   custInvMailTime = PROPS_VALUES.containsKey("custInvMailTime") ? PROPS_VALUES.get("custInvMailTime") : FPUtil.getInstance().getProperty("custInvMailTime");
   custInvMailIds = PROPS_VALUES.containsKey("custInvMailIds") ? PROPS_VALUES.get("custInvMailIds") : FPUtil.getInstance().getProperty("custInvMailIds");
 
   custInvFromMailId = PROPS_VALUES.containsKey("custInvFromMailId") ? PROPS_VALUES.get("custInvFromMailId") : FPUtil.getInstance().getProperty("custInvFromMailId");
   custInvpassword = PROPS_VALUES.containsKey("custInvpassword") ? PROPS_VALUES.get("custInvpassword") : FPUtil.getInstance().getProperty("custInvpassword");

    
    }

}
