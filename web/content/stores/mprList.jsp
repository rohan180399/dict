<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<!--<title>PAPL</title>-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });


</script>

<script>
    function submitPag() {
        if (document.mpr.toDate.value == '') {
            alert("Please Enter From Date");
        } else if (document.mpr.fromDate.value == '') {
            alert("Please Enter to Date");
        }
        document.mpr.action = "/throttle/mprApprovalList.do";
        document.mpr.submit();
    }

    function setDate(fDate, tDate) {
        if (fDate != 'null') {
            document.mpr.fromDate.value = fDate;
        }
        if (tDate != 'null') {
            document.mpr.toDate.value = tDate;
        }

    }

</script>


<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.mpr/lprApprovalQueue"  text="MPR"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="general.label.stores"  text="Stores"/></a></li>
            <li class="active"><spring:message code="service.label.mpr/lprApprovalQueue"  text="MPR"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onLoad="setDate('<%= request.getAttribute("fromDate") %>', '<%= request.getAttribute("toDate") %>')">        
                <form name="mpr" method="post" >                    



                    <%@ include file="/content/common/message.jsp" %>  
                    <table class="table table-info mb30 table-hover">
                        <thead>
                            <tr>
                                <th colspan="4"><spring:message code="service.label.mpr/lprApprovalQueue"  text="MPR"/></th>
                            </tr>
                        </thead>

                        <tr>
                            <td ><spring:message code="stores.label.FromDate"  text="default text"/></td>
                            <td ><input  name="fromDate" style="width:260px;height:40px;" class="form-control pull-right datepicker" type="text" value="" size="20">
                                <!--                    <img src="/throttle/images/cal.gif" name="Calendar"  readonly  onclick="displayCalendar(document.mpr.fromDate,'dd-mm-yyyy',this)"/></td>-->

                            <td ><spring:message code="stores.label.TODate"  text="default text"/></td>
                            <td ><input name="toDate" style="width:260px;height:40px;" class="form-control pull-right datepicker"  type="text" value="" size="20">
                                <!--                    <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.mpr.toDate,'dd-mm-yyyy',this)"/></td>-->
                                <%--</tr>
                                <tr>
                                    <td class="text1" width="150"  height="30">Vendor</td>
                                    <td width="196" class="text1" height="30" ><select class="form-control" name="vendorId">
                                            <option value='0' selected>All</option>
                                    <c:if test = "${vendorList != null}" >
                                    <c:forEach items="${vendorList}" var="vend">                             
                                            <option value=<c:out value="${vend.vendorId}"/> ><c:out value="${vend.vendorName}"/></option>
                                    </c:forEach>
                                    </c:if>                             
                                    </select> </td>
                                </tr>
                                <tr>
                                    <td class="text2"  height="30">Status</td>
                                    <td height="30" class="text2"><select class="form-control" >
                                            <option value="PENDING" selected> Pending</option>
                                            <option value="APPROVED"> Approved</option>
                                            <option value="REJECTED"> Rejected</option>
                                        </select>
                                    </td>
                                </tr> 
                                <tr>--%>
                        </tr>
                        <tr align="center">
                            <td  colspan="4"> <input class="btn btn-success"  type="button" name="search" value="<spring:message code="stores.label.SEARCH"  text="default text"/>" onClick="submitPag();"> </td>
                        </tr>
                    </table> 


                    <c:if test = "${mprList != null}" >            
                        <table class="table table-info mb30 table-hover" id="table">
                            <thead>
                                <tr>
                                    <th ><spring:message code="stores.label.MPR/LPRNO"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.VendorName"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.PurchaseType"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.RaisedDate"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.ElapsedDays"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.Status"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.Action"  text="default text"/></th>
                                </tr>
                            </thead>
                            <% int index = 0;
                        String classText = "";
                        int oddEven = 0;
                            %>

                            <c:forEach items="${mprList}" var="mpr"> 
                                <c:if test="${mpr.status != 'APPROVED' &&  mpr.status != 'REJECTED'}" >
                                    <%

                        oddEven = index % 2;
                        if (oddEven > 0) {
                            classText = "text2";
                        } else {
                            classText = "text1";
                        }
                                    %>
                                    <tr>
                                        <td><c:out value="${mpr.mprId}"/></td>
                                        <td><c:out value="${mpr.vendorName}"/></td>                       
                                        <td><c:out value="${mpr.purchaseType}"/></td>
                                        <td><c:out value="${mpr.mprDate}"/></td>
                                        <td><c:out value="${mpr.elapsedDays}"/></td>                                                                        
                                        <td><c:out value="${mpr.status}"/></td>                                                                        
                                        <td><a href="/throttle/approveMprPage.do?mprId=<c:out value='${mpr.mprId}'/>" > <spring:message code="stores.label.Approve/Reject"  text="default text"/> </a> </td>                                                                        
                                    </tr>
                                    <%
                        index++;
                                    %>
                                </c:if>
                            </c:forEach>
                        </c:if>                  
                    </table>                        
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>



