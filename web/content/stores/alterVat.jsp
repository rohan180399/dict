<%-- 
Document   : modifyDesig
Created on : Nov 03, 2008, 6:14:28 PM
Author     : Vijay
--%>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
 
<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="ets.domain.vehicle.business.VehicleTO" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>PAPL</title>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
 <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });
        </script>

<script language="javascript">

function submitpage(value)
{
 

var checValidate = selectedItemValidation();
var splt = checValidate.split("-");
if(splt[0]=='SubmitForm' && splt[1]!=0 ){
    

document.modify.action='/throttle/handleAlterVat.do';
document.modify.submit();
}
}

function setSelectbox(i)
{
var selected=document.getElementsByName("selectedIndex") ;
selected[i].checked = 1;
}

function selectedItemValidation(){
var index = document.getElementsByName("selectedIndex");
var mfrNames = document.getElementsByName("vats");
var desc = document.getElementsByName("descs");
var effectiveDate = document.getElementsByName("effectiveDate");
var chec=0;

for(var i=0;(i<index.length && index.length!=0);i++){
        if(index[i].checked){
        chec++;
        if(floatValidation(mfrNames[i],"VAT")){
            return;
        }else if(textValidation(desc[i],"Desription")){
            return;                            
        }else if(textValidation(effectiveDate[i],"effectiveDate")){
            return;
        }
        }
}
if(chec == 0){
alert("Please Select Any One And Then Proceed");
desigName[0].focus();
//break;
}
document.modify.action='/throttle/handleAlterVat.do';
document.modify.submit();
}



function addVat(){
    document.modify.action='/throttle/handleAddVatPage.do';
    document.modify.submit();   
}    

function isChar(s){
if(!(/^-?\d+$/.test(s))){
return false;
}
return true;
}
</script>

</head>

<script>
   function changePageLanguage(langSelection){

            if(langSelection== 'ar'){
            document.getElementById("pAlign").style.direction="rtl";
            }else if(langSelection== 'en'){
            document.getElementById("pAlign").style.direction="ltr";
            }
        }

    </script>

        <c:if test="${jcList != null}">
        <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
        </c:if>
        
        
             <span style="float: right">
		                                <a href="?paramName=en">English</a>
		                                |
		                                <a href="?paramName=ar">Arabic</a>
                            </span>

<!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/ 
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->

<body onload="document.modify.desigNames[0].focus();">
<form method="post" name="modify"> 
 <%@ include file="/content/common/path.jsp" %>
     
<%@ include file="/content/common/message.jsp" %>
<br>
 <% int index = 0; %>        

<table width="500" align="center" id="bg" cellpadding="0" cellspacing="0" class="border">
 <c:if test = "${vatList != null}" >   

<tr>
 <td class="contentsub" height="30"> </td>
<td height="30" class="contentsub"><div class="contentsub"><spring:message code="stores.label.S.No"  text="default text"/>
</div></td>  
<td class="contentsub" height="30"><div class="contentsub"><spring:message code="stores.label.VAT(%)"  text="default text"/>
</div> </td> 
<td class="contentsub" height="30"><div class="contentsub"><spring:message code="stores.label.Description"  text="default text"/>
</div></td> 
<td class="contentsub" height="30"><div class="contentsub"><spring:message code="stores.label.Status"  text="default text"/>
</div></td>
<td class="contentsub" height="30"><div class="contentsub"><spring:message code="stores.label.EffectiveDate"  text="default text"/>
</div></td>
<td class="contentsub" height="30"><div class="contentsub"><spring:message code="stores.label.Select"  text="default text"/>
</div></td>

</tr>
</c:if>
<c:if test = "${vatList != null}" >
<c:forEach items="${vatList}" var="mfr"> 		
<%

String classText = "";
int oddEven = index % 2;
if (oddEven > 0) {
classText = "text2";
} else {
classText = "text1";
}
%>
<tr>
 <td class="<%=classText %>" height="30"><input type="hidden" name="vatIds" value='<c:out value="${mfr.vatId}"/>'>  </td>
<td class="<%=classText %>" height="30"><%=index + 1%></td>
<td class="<%=classText %>" height="30"><input type="text" class="form-control" name="vats" value="<c:out value="${mfr.vat}"/>" onchange="setSelectbox(<%= index %>)"></td>
<td class="<%=classText %>" height="30"><textarea name="descs" class="form-control" rows="2" cols="60" onchange="setSelectbox(<%= index %>);" > <c:out value="${mfr.desc}"/> </textarea> </td>
<td height="30" class="<%=classText %>"> <div align="center">
        <select name="activeInds" class="form-control" onchange="setSelectbox(<%= index %>)">
<c:choose>
<c:when test="${mfr.activeInd == 'Y'}">
<option value="Y" selected><spring:message code="stores.label.Active"  text="default text"/></option>
<option value="N"><spring:message code="stores.label.InActive"  text="default text"/></option>
</c:when>
<c:otherwise>
<option value="Y"><spring:message code="stores.label.Active"  text="default text"/></option>
<option value="N" selected><spring:message code="stores.label.InActive"  text="default text"/></option>
</c:otherwise>
</c:choose>
</select>
</div> 
</td>
<td class="<%=classText %>" height="30">
    <input name="effectiveDate" type="text" class="datepicker"  readonly="readonly" id="effectiveDate" value="<c:out value="${mfr.effectiveDate}"/>" onchange="setSelectbox(<%= index %>)"  />
</td>
<td width="77" height="30" class="<%=classText %>"><input type="checkbox" name="selectedIndex" value='<%= index %>'></td>
</tr>
<%
index++;
%>
</c:forEach >
</c:if> 
</table>
<center>
<br>
<input type="button" name="save" value="<spring:message code="stores.label.Save"  text="default text"/>" onClick="submitpage(this.name)" class="button" />
<input type="button" name="add" value="<spring:message code="stores.label.ADD"  text="default text"/>" onClick="addVat(this.name)" class="button" />

</center>
<br>
    <br>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
