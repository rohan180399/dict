<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@page import="java.util.Locale"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>


<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        // alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>
<script type="text/javascript">
    function submitPage(value) {
        if (value == 'ExportExcel') {
            document.fcWiseTripSummary.action = '/throttle/handleJobcardSumaryReport.do?param=ExportExcel';
            document.fcWiseTripSummary.submit();
        } else {
            document.fcWiseTripSummary.action = '/throttle/handleJobcardSumaryReport.do?param=Search';
            document.fcWiseTripSummary.submit();
        }
    }

    function setValue() {
        if ('<%=request.getAttribute("page")%>' != 'null') {
            var page = '<%=request.getAttribute("page")%>';
            if (page == 1) {
                submitPage('search');
            }
        }
    }


</script>

<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
            setValues();
            getVehicleNos();">
    </c:if>

    <!--	  <span style="float: right">
                    <a href="?paramName=en">English</a>
                    |
                    <a href="?paramName=ar">العربية</a>
              </span>-->
    <style type="text/css">





        .container {width: 960px; margin: 0 auto; overflow: hidden;}
        .content {width:800px; margin:0 auto; padding-top:50px;}
        .contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

        /* STOP ANIMATION */



        /* Second Loadin Circle */

        .circle1 {
            background-color: rgba(0,0,0,0);
            border:5px solid rgba(100,183,229,0.9);
            opacity:.9;
            border-left:5px solid rgba(0,0,0,0);
            /*	border-right:5px solid rgba(0,0,0,0);*/
            border-radius:50px;
            /*box-shadow: 0 0 15px #2187e7; */
            /*	box-shadow: 0 0 15px blue;*/
            width:40px;
            height:40px;
            margin:0 auto;
            position:relative;
            top:-50px;
            -moz-animation:spinoffPulse 1s infinite linear;
            -webkit-animation:spinoffPulse 1s infinite linear;
            -ms-animation:spinoffPulse 1s infinite linear;
            -o-animation:spinoffPulse 1s infinite linear;
        }

        @-moz-keyframes spinoffPulse {
            0% { -moz-transform:rotate(0deg); }
            100% { -moz-transform:rotate(360deg);  }
        }
        @-webkit-keyframes spinoffPulse {
            0% { -webkit-transform:rotate(0deg); }
            100% { -webkit-transform:rotate(360deg);  }
        }
        @-ms-keyframes spinoffPulse {
            0% { -ms-transform:rotate(0deg); }
            100% { -ms-transform:rotate(360deg);  }
        }
        @-o-keyframes spinoffPulse {
            0% { -o-transform:rotate(0deg); }
            100% { -o-transform:rotate(360deg);  }
        }



    </style>
    <style>
        #index td {
            color:white;
        }
    </style>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i><spring:message code="operations.reports.label.JobcardSummary" text="default text"/></h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="default text"/>:</span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="head.label.Report" text="default text"/></a></li>
                <li class="active"><spring:message code="operations.reports.label.JobcardSummary" text="default text"/></li>
            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="setValue();">
                    <form name="fcWiseTripSummary" action=""  method="post">
                        <table class="table table-bordered" id="report" >
                            <!--                            <tr height="30" id="index" >
                                                            <td colspan="4"  style="background-color:#5BC0DE;">
                                                                        <b><spring:message code="operations.reports.label.JobcardSummary" text="default text"/></b>
                                                            </td>
                                                        </tr>-->
                            <div id="first">
                                <td style="border-color:#5BC0DE;padding:16px;">
                                    <table  class="table table-info mb30 table-hover">
                                        <tr>
                                            <td><font color="red">*</font><spring:message code="operations.reports.label.FromDate" text="default text"/></td>
                                            <td height="30"><input name="fromDate" style="width:280px;height:40px;" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                            <td><font color="red">*</font><spring:message code="operations.reports.label.ToDate" text="default text"/></td>
                                            <td height="30"><input name="toDate" style="width:280px;height:40px;" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="right"><input type="button" class="btn btn-success" name="ExportExcel"   value="<spring:message code="operations.reports.label.EXPORTEXCEL" text="default text"/>" onclick="submitPage(this.name);">&nbsp;&nbsp;</td>
                                            <td colspan="2"><input type="button" class="btn btn-success" name="Search"   value="<spring:message code="operations.reports.label.SEARCH" text="default text"/>" onclick="submitPage(this.name);"></td>
                                        </tr>
                                    </table>
                                </td>
                                </tr>
                        </table>
                        <br>
                        <c:if test = "${jobcardSummary == null && jobcardSummaryDriverIssue == null}" >
                            <center>
                                <font color="blue"><spring:message code="operations.reports.label.PleaseWaitYourRequestisProcessing" text="default text"/></font>
                                <div class="container">
                                    <div class="content">
                                        <div class="circle"></div>
                                        <div class="circle1"></div>
                                    </div>
                                </div>
                            </center>
                        </c:if>
                        <c:if test = "${jobcardSummaryDriverIssueSize != '0'}" >
                            <div style="text-align: center"><spring:message code="operations.reports.label.DriverIssue" text="default text"/></div>
                            <table class="table table-info mb30 table-hover " id="table" >
                                <thead>
                                    <tr >
                                        <th ><spring:message code="operations.reports.label.SNo" text="default text"/></th>
                                        <th ><spring:message code="operations.reports.label.JobcardNo" text="default text"/></th>
                                        <th ><spring:message code="operations.reports.label.VehicleNo" text="default text"/></th>
                                        <th ><spring:message code="operations.reports.label.ServiceTypeName" text="default text"/></th>
                                        <th ><spring:message code="operations.reports.label.CreatedDate" text="default text"/></th>
                                        <th ><spring:message code="operations.reports.label.JobcardDays" text="default text"/></th>
                                    </tr>
                                </thead>
                                <% int i=1;%>
                                <tbody>
                                    <c:forEach items="${jobcardSummaryDriverIssue}" var="job">
                                        <tr>
                                            <td><%=i%></td>
                                            <td><c:out value="${job.jobCardNo}"/></td>
                                            <td><c:out value="${job.vehicleNo}"/></td>
                                            <td><c:out value="${job.serviceTypeName}"/></td>
                                            <td><c:out value="${job.createdDate}"/></td>
                                            <td><c:out value="${job.jobCardDays}"/></td>
                                        </tr>
                                        <%i++;%>
                                    </c:forEach>
                                </tbody>

                            </table>
                        </c:if>
                        <br>
                        <c:if test = "${jobcardSize != '0'}" >
                            <div style="text-align: center"><spring:message code="operations.reports.label.OtherIssue" text="default text"/></div>
                            <table class="table table-info mb30 table-hover " id="table" >
                                <thead>
                                    <tr >
                                        <th ><spring:message code="operations.reports.label.SNo" text="default text"/></th>
                                        <th ><spring:message code="operations.reports.label.JobcardNo" text="default text"/></th>
                                        <th ><spring:message code="operations.reports.label.VehicleNo" text="default text"/></th>
                                        <th ><spring:message code="operations.reports.label.ServiceTypeName" text="default text"/></th>
                                        <th ><spring:message code="operations.reports.label.CreatedDate" text="default text"/></th>
                                        <th ><spring:message code="operations.reports.label.JobcardDays" text="default text"/></th>
                                    </tr>
                                </thead>
                                <% int j=1;%>
                                <tbody>
                                    <c:forEach items="${jobcardSummary}" var="job">
                                        <tr>
                                            <td><%=j%></td>
                                            <td><c:out value="${job.jobCardNo}"/></td>
                                            <td><c:out value="${job.vehicleNo}"/></td>
                                            <td><c:out value="${job.serviceTypeName}"/></td>
                                            <td><c:out value="${job.createdDate}"/></td>
                                            <td><c:out value="${job.jobCardDays}"/></td>
                                        </tr>
                                        <%j++;%>
                                    </c:forEach>
                                </tbody>
                            </table>
                            <script language="javascript" type="text/javascript">
                                setFilterGrid("table");
                            </script>
                            <div id="controls">
                                <div id="perpage">
                                    <select onchange="sorter.size(this.value)">
                                        <option value="5" >5</option>
                                        <option value="10">10</option>
                                        <option value="20" selected="selected">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <span><spring:message code="operations.reports.label.EntriesPerPage" text="default text"/></span>
                                </div>
                                <div id="navigation">
                                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                </div>
                                <div id="text"><spring:message code="operations.reports.label.DisplayingPage" text="default text"/> <span id="currentpage"></span><spring:message code="operations.reports.label.of" text="default text"/>  <span id="pagelimit"></span></div>
                            </div>
                            <script type="text/javascript">
                                var sorter = new TINY.table.sorter("sorter");
                                sorter.head = "head";
                                sorter.asc = "asc";
                                sorter.desc = "desc";
                                sorter.even = "evenrow";
                                sorter.odd = "oddrow";
                                sorter.evensel = "evenselected";
                                sorter.oddsel = "oddselected";
                                sorter.paginate = true;
                                sorter.currentid = "currentpage";
                                sorter.limitid = "pagelimit";
                                sorter.init("table", 0);
                            </script>
                        </c:if>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                </body>    
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>