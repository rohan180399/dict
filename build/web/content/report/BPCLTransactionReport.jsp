<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>


<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>


<script type="text/javascript">
    function submitPage(value) {

        if (document.getElementById('fromDate').value == '') {
            alert("Please select from Date");
            document.getElementById('fromDate').focus();
        } else if (document.getElementById('toDate').value == '') {
            alert("Please select to Date");
            document.getElementById('toDate').focus();
        } else {
            if (value == "ExportExcel") {
                document.BPCLTransaction.action = '/throttle/handleBPCLTransactionDetailsExcel.do?param=ExportExcel';
                document.BPCLTransaction.submit();
            }
            else {
                document.BPCLTransaction.action = '/throttle/handleBPCLTransactionDetailsExcel.do?param=Search';
                document.BPCLTransaction.submit();
            }
        }        
    }
</script>

<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
            setValues();
            getVehicleNos();">
    </c:if>

    <!--	  <span style="float: right">
                    <a href="?paramName=en">English</a>
                    |
                    <a href="?paramName=ar">???????</a>
              </span>-->

    <style>
        #index td {
            color:white;
        }
    </style>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i><spring:message code="operations.reports.label.BPCLTransactionReport" text="default text"/></h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="default text"/>:</span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="head.label.Report" text="default text"/></a></li>
                <li class="active"><spring:message code="operations.reports.label.BPCLTransactionReport" text="default text"/></li>
            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body>
                    <form name="BPCLTransaction" action=""  method="post">


                        <table class="table table-bordered" id="report" >
                            <!--                            <tr height="30" id="index" >
                                                            <td colspan="4"  style="background-color:#5BC0DE;">
                                                                <b><spring:message code="operations.reports.label.BPCLTransactionReport" text="default text"/>
                                                                </b></td></tr>-->
                            <div id="first">
                                <td style="border-color:#5BC0DE;padding:16px;">
                                    <table class="table table-info mb30 table-hover" >
                                        <!--                                    <tr>
                                                                                <td><font color="red">*</font>Driver Name</td>
                                                                                <td height="30">
                                                                                    <input name="driName" id="driName" type="text" class="textbox" size="20" value="" onKeyPress="getDriverName();" autocomplete="off">
                                                                            </tr>-->
                                        <tr>
                                            <td><font color="red">*</font><spring:message code="operations.reports.label.FromDate" text="default text"/></td>
                                            <td height="30"><input name="fromDate" style="width:260px;height:40px;" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                            <td><font color="red">*</font><spring:message code="operations.reports.label.ToDate" text="default text"/></td>
                                            <td height="30"><input name="toDate" style="width:260px;height:40px;" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                                        </tr>
                                        <tr >

                                            <td colspan="4" align="center" ><input type="button" class="btn btn-success" name="search" onclick="submitPage(this.name);" value="<spring:message code="operations.reports.label.SEARCH" text="default text"/>">&nbsp;&nbsp;
                                                <input type="button" class="btn btn-success" name="ExportExcel" onclick="submitPage(this.name);" value="<spring:message code="operations.reports.label.EXPORTEXCEL" text="default text"/>"></td>
                                        </tr>
                                    </table>
                                </td>
                                </tr>
                        </table>

                        <c:if test="${BPCLTransactionDetails != null}">
                            <table class="table table-info mb30 table-hover sortable" id="table"  width="100%" >

                                <thead>
                                    <tr>
                                        <th align="center"  style="border-left:1px solid white"><spring:message code="operations.reports.label.SNo" text="default text"/></th>
                                        <th align="center"  style="border-left:1px solid white"><spring:message code="operations.reports.label.TransactionHistoryId" text="default text"/></th>
                                        <th align="center"  style="border-left:1px solid white"><spring:message code="operations.reports.label.VehicleNo" text="default text"/></th>
                                        <th align="center"  style="border-left:1px solid white"><spring:message code="operations.reports.label.BPCLAccountId" text="default text"/></th>
                                        <th align="center"  style="border-left:1px solid white"><spring:message code="operations.reports.label.DealerName" text="default text"/></th>
                                        <th align="center"  style="border-left:1px solid white"><spring:message code="operations.reports.label.DealerCity" text="default text"/></th>
                                        <th align="center"  style="border-left:1px solid white"><spring:message code="operations.reports.label.TransactionDate" text="default text"/></th>
                                        <th align="center"  style="border-left:1px solid white"><spring:message code="operations.reports.label.AccountingDate" text="default text"/></th>
                                        <th align="center"  style="border-left:1px solid white"><spring:message code="operations.reports.label.TransactionType" text="default text"/></th>
                                        <th align="center"  style="border-left:1px solid white"><spring:message code="operations.reports.label.Currency" text="default text"/></th>
                                        <th align="center"  style="border-left:1px solid white"><spring:message code="operations.reports.label.TransactionAmount" text="default text"/></th>
                                        <th align="center"  style="border-left:1px solid white"><spring:message code="operations.reports.label.VolumeDocumentNo" text="default text"/></th>
                                        <th align="center"  style="border-left:1px solid white"><spring:message code="operations.reports.label.AmountBalance" text="default text"/></th>
                                        <th align="center"  style="border-left:1px solid white"><spring:message code="operations.reports.label.PetromilesEarned" text="default text"/></th>
                                        <th align="center"  style="border-left:1px solid white"><spring:message code="operations.reports.label.OdometerReading" text="default text"/></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% int index = 1;%>
                                    <c:set var="rechargeAmount" value="${0}" />
                                    <c:set var="transactionAmount" value="${0}" />
                                    <c:forEach items="${BPCLTransactionDetails}" var="BPCLTD">
                                        <%
                                                    String classText = "";
                                                    int oddEven = index % 2;
                                                    if (oddEven > 0) {
                                                        classText = "text2";
                                                    } else {
                                                        classText = "text1";
                                                    }
                                        %>
                                        <tr>
                                            <td class="<%=classText%>"><%=index++%></td>
                                            <td  width="30" class="<%=classText%>"><c:out value="${BPCLTD.transactionHistoryId}"/></td>
                                            <td  width="30" class="<%=classText%>"><c:out value="${BPCLTD.vehicleNo}"/><input type="hidden" value="<c:out value="${BPCLTD.vehicleNo}"/>" name="vehicleNo" id="vehicleNo"/></td>
                                            <td width="30" class="<%=classText%>"><c:out value="${BPCLTD.bPCLAccountId}"/></td>
                                            <td width="30" class="<%=classText%>" ><c:out value="${BPCLTD.dealerName}"/></td>
                                            <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.dealerCity}"/></td>
                                            <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.transactionDate}"/></td>
                                            <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.accountingDate}"/></td>
                                            <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.transactionType}"/>
                                                <c:if test="${BPCLTD.transactionType == 'CMS_RECHARGE'}">
                                                    <c:set var="rechargeAmount" value="${rechargeAmount + BPCLTD.transactionAmount}"/>
                                                </c:if>
                                                <c:if test="${BPCLTD.transactionType != 'CMS_RECHARGE'}">
                                                    <c:set var="transactionAmount" value="${transactionAmount + BPCLTD.transactionAmount}"/>
                                                </c:if>
                                            </td>
                                            <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.currency}"/></td>
                                            <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.transactionAmount}"/></td>
                                            <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.volumeDocumentNo}"/></td>
                                            <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.amountBalance}"/></td>
                                            <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.petromilesEarned}"/></td>
                                            <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.odometerReading}"/></td>
                                        </tr>

                                    </c:forEach>
                                </tbody>
                            </table>
                            <script language="javascript" type="text/javascript">
                                setFilterGrid("table");
                            </script>
                            <div id="controls">
                                <div id="perpage">
                                    <select onchange="sorter.size(this.value)">
                                        <option value="5" selected="selected">5</option>
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <span><spring:message code="operations.reports.label.EntriesPerPage" text="default text"/></span>
                                </div>
                                <div id="navigation">
                                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                </div>
                                <div id="text"><spring:message code="operations.reports.label.DisplayingPage" text="default text"/> <span id="currentpage"></span> <spring:message code="operations.reports.label.of" text="default text"/> <span id="pagelimit"></span></div>
                            </div>
                            <script type="text/javascript">
                                var sorter = new TINY.table.sorter("sorter");
                                sorter.head = "head";
                                sorter.asc = "asc";
                                sorter.desc = "desc";
                                sorter.even = "evenrow";
                                sorter.odd = "oddrow";
                                sorter.evensel = "evenselected";
                                sorter.oddsel = "oddselected";
                                sorter.paginate = true;
                                sorter.currentid = "currentpage";
                                sorter.limitid = "pagelimit";
                                sorter.init("table", 0);
                            </script>
                            <table align="center" border="1">
                                <tr>
                                    <td width="120px"><spring:message code="operations.reports.label.CMS_RECHARGE" text="default text"/></td>
                                    <td  width="120px">
                                <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${rechargeAmount}" /></td>
                                </tr>
                                <tr>
                                    <td  width="120px"><spring:message code="operations.reports.label.BPCLTransaction" text="default text"/></td>
                                    <td  width="120px">
                                <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${transactionAmount}" /></td>
                                </tr>
                                <tr>
                                    <td  width="120px"><spring:message code="operations.reports.label.Balance" text="default text"/></td>
                                    <td  width="120px">
                                <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${rechargeAmount-transactionAmount}" /></td>
                                </tr>
                            </table>
                        </c:if>

                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>