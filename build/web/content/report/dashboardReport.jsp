<%-- 
    Document   : Accounts Receivable
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>


<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        // alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
            setValues();
            getVehicleNos();">
    </c:if>

    <!--    <span style="float: right">
            <a href="?paramName=en">English</a>
            |
            <a href="?paramName=ar">???????</a>
        </span>-->
    <script type="text/javascript">
        function submitPage(value) {
            //alert("am here...");
            if (value == 'ExportExcel') {
                document.accountReceivable.action = '/throttle/dashboardReport.do?param=ExportExcel';
                document.accountReceivable.submit();
            } else {
                document.accountReceivable.action = '/throttle/dashboardReport.do?param=Search';
                document.accountReceivable.submit();
            }
        }



    </script>
<style>
    #index td {
        color:white;
    }
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="operations.reports.label.DashboardReport" text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="head.label.Report" text="default text"/></a></li>
            <li class="active"><spring:message code="operations.reports.label.DashboardReport" text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body >
                <form name="accountReceivable" action=""  method="post">
                    <table class="table table-bordered" id="report" >
                        <!--                        <tr height="30" id="index" >
                                                    <td colspan="4"  style="background-color:#5BC0DE;">
                                                        <b><spring:message code="operations.reports.label.DashboardReport" text="default text"/></b>
                                                    </td></tr>-->
                        <div id="first">
                            <td style="border-color:#5BC0DE;padding:16px;">
                                <table  class="table table-info mb30 table-hover">
                                    <tr>

                                        <td><font color="red">*</font><spring:message code="operations.reports.label.FromDate" text="default text"/></td>
                                        <td height="30"><input name="fromDate" id="fromDate" style="width:260px;height:40px;" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                        <td><font color="red">*</font><spring:message code="operations.reports.label.ToDate" text="default text"/></td>
                                        <td height="30"><input name="toDate" id="toDate" style="width:260px;height:40px;" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                                    </tr>

                                    <tr>
                                        <td colspan="2" align="right"><input type="button" class="btn btn-success" name="ExportExcel"   value="<spring:message code="operations.reports.label.EXPORTEXCEL" text="default text"/>" onclick="submitPage(this.name);">&nbsp;&nbsp;</td>
                                        <td colspan="2"><input type="button" class="btn btn-success" name="Search"   value="<spring:message code="operations.reports.label.SEARCH" text="default text"/>" onclick="submitPage(this.name);"></td>
                                    </tr>
                                </table>
                        </td>
                        </tr>
                    </table>
                    <br>

                    <%
                        int index = 0,sno = 1;
                        String classText = "";
                        int oddEven = index % 2;
                    %>
                    <c:if test = "${salesDashboardSize != '0'}" >
                        <table class="table table-info mb30 table-hover" id="table1" >
                            <thead>
                                <tr >                            
                                    <th ><spring:message code="operations.reports.label.SalesLead" text="default text"/></th>
                                    <th ><spring:message code="operations.reports.label.Trips" text="default text"/></th>
                                    <th ><spring:message code="operations.reports.label.Revenue/Km" text="default text"/></th>
                                    <th ><spring:message code="operations.reports.label.Margin/Km" text="default text"/></th>
                                    <th ><spring:message code="operations.reports.label.WFU" text="default text"/></th>
                                    <th ><spring:message code="operations.reports.label.WFL" text="default text"/></th>
                                </tr>
                            </thead>
                            <tbody>

                                <c:forEach items="${salesDashboard}" var="odList">
                                    <%
                                                classText = "";
                                                oddEven = index % 2;
                                                if (oddEven > 0) {
                                                    classText = "text2";
                                                } else {
                                                    classText = "text1";
                                                }
                                    %>
                                    <tr>
                                        <td align="left" ><c:out value="${odList.empName}"/> </td>
                                        <td align="right" ><c:out value="${odList.trips}"/> </td>
                                        <td align="right" ><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${odList.sales / odList.km}" /></td>
                                <td align="right" ><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${odList.margin / odList.km}" /></td>
                                <td align="right" ><c:out value="${odList.wfu}"/> </td>
                                <td align="right" ><c:out value="${odList.wfl}"/> </td>
                                </tr>
                                <%
                                  index++;
                                  sno++;
                                %>



                            </c:forEach>

                            </tbody>
                        </table>
                        <br>

                        <c:if test = "${opsDashboardSize != '0'}" >
                            <table class="table table-info mb30 table-hover" id="table" >
                                <thead>
                                    <tr >
                                        <th ><spring:message code="operations.reports.label.OpsLead" text="default text"/></th>
                                        <th ><spring:message code="operations.reports.label.TotalVehicles" text="default text"/></th>
                                        <th ><spring:message code="operations.reports.label.TripVehicles" text="default text"/></th>
                                        <th ><spring:message code="operations.reports.label.Trips" text="default text"/></th>
                                        <th ><spring:message code="operations.reports.label.Km" text="default text"/></th>
                                        <th ><spring:message code="operations.reports.label.Kilometer/truck/mnth" text="default text"/></th>
                                        <th ><spring:message code="operations.reports.label.KM/truck/day" text="default text"/></th>
                                        <th ><spring:message code="operations.reports.label.VehicleDaysLostDuetoUnloadingdelays" text="default text"/></th>
                                        <th ><spring:message code="operations.reports.label.VehicleDaysLostDuetoLoadingdelays" text="default text"/></th>
                                        <th ><spring:message code="operations.reports.label.VehicleDaysLostDuetoR&M" text="default text"/></th>
                                        <th ><spring:message code="operations.reports.label.RCMDeviation" text="default text"/></th>
                                        <th ><spring:message code="operations.reports.label.Cost/Km" text="default text"/></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%  index = 0; sno = 1;%>
                                    <c:forEach items="${opsDashboard}" var="odList">
                                        <%
                                                    classText = "";
                                                    oddEven = index % 2;
                                                    if (oddEven > 0) {
                                                        classText = "text2";
                                                    } else {
                                                        classText = "text1";
                                                    }
                                        %>
                                        <tr>
                                            <td align="left" ><c:out value="${odList.empName}"/> </td>
                                            <td align="right" ><c:out value="${odList.noOfVehicles}"/> </td>
                                            <td align="right" ><c:out value="${odList.vehicles}"/> </td>
                                            <td align="right" ><c:out value="${odList.trips}"/> </td>
                                            <td align="right" ><c:out value="${odList.km}"/> </td>
                                            <td align="right" ><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${(odList.km / odList.noOfVehicles) / odList.months}" /></td>
                                    <td align="right" ><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${(odList.km / odList.noOfVehicles) / odList.days}" /></td>


                                    <td align="right" ><c:out value="${odList.rmDays}"/> </td>
                                    <td align="right" ><c:out value="${odList.wfu}"/> </td>
                                    <td align="right" ><c:out value="${odList.wfl}"/> </td>
                                    <td align="right" ><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${odList.estimatedExpenses - odList.expenses}" /></td>
                                    <td align="right" ><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${odList.expenses / odList.km}" /></td>
                                    </tr>
                                    <%
                                      index++;
                                      sno++;
                                    %>

                                </c:forEach>
                                </tbody>
                            </table>
                        </c:if>
                        <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span><spring:message code="operations.reports.label.EntriesPerPage" text="default text"/></span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text"><spring:message code="operations.reports.label.DisplayingPage" text="default text"/> <span id="currentpage"></span> <spring:message code="operations.reports.label.of" text="default text"/> <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 1);
                    </script>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                </c:if>
            </body>    
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>