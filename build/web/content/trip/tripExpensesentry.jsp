<%--
    Document   : tripExpensesentry
    Created on : Apr 28, 2016, 5:12:59 PM
    Author     : benjamin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <script language="javascript" src="/throttle/js/validate.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
       <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>


        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>

         <script type="text/javascript" language="javascript">
            function submitPage() {

                document.tripExpense.action = '/throttle/viewTripexpensesentryadd.do';
                document.tripExpense.submit();
            }
                    </script>


          <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
//                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy",
                    changeMonth: true,changeYear: true
                });
            });

        </script>

         <script>
               function total(val){
              document.getElementById("totalExpenses").value=val;
                                    }
         </script>
          <style>

        .loading {

            border:10px solid red;

           display:none;

        }

        .button1{border: 1px black solid ; color:#000 !important;  }



        .spinner{

            position: fixed;

            top: 50%;

            left: 50%;

            margin-left: -50px; /* half width of the spinner gif */

            margin-top: -50px; /* half height of the spinner gif */

            text-align:center;

            z-index:1234;

            overflow: auto;

            width: 300px; /* width of the spinner gif */

            height: 300px; /*hight of the spinner gif +2px to fix IE8 issue */

        }

        .ui-loader-background {

            width:100%;

            height:100%;

            top:0;

            padding: 0;

            margin: 0;

            background: rgba(0, 0, 0, 0.3);

            display:none;

            position: fixed;

            z-index:100;

        }

        .ui-loader-background {

            display:block;
  }
  </style>
    </head>
    <body >

        <form name="tripExpense" method="get">



         <table width="900" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
            <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                    </h2></td>
                <td align="right"><div style="height:17px;margin-top:0px;"><img src="../images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="../images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
            </tr>
            <tr id="exp_table" >
                <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">

                        <ul class="tabNavigation">
                            <li style="background:#76b3f1">Expense Entry</li>
                        </ul>
                         <input type="hidden" name="tripSheetId" id="tripSheetId" value="<c:out value="${tripSheetId}"/>"  />
                            <table width="100%" cellpadding="0" cellspacing="5" border="0" align="center" class="table4">

                                <tr>
                                    <td width="80" >Expense Name</td>
                                    <td  width="80"> <select name="expenseId" id="expenseId"  class="textbox" style="height:20px; width:122px;" >
                                            <option value="" selected>--Select--</option>
                                            <c:forEach items="${expenseDetails}" var="expenseDetails">
                                           <option value='<c:out value="${expenseDetails.expenseId}"/> '><c:out value="${expenseDetails.expenseName}"/></option>
                                        </c:forEach>
                                            </select></td>
                                    <td  width="80" >Expense By</td>
                                    <td  width="80"> <select name="driverNameex" id="driverNameex" class="textbox" style="height:20px; width:122px;">
                                                             <option value="" selected>--Select--</option>
                                            <c:forEach items="${driverNameDetails}" var="driverNameDetails">
                                           <option value='<c:out value="${driverNameDetails.employeeId}"/> '><c:out value="${driverNameDetails.employeeName}"/></option>
                                        </c:forEach>
                                        </select>
                                    </td>
                                    <td width="80"  >Expense Date</td>
                                     <td width="80"><input name="expenseDate" id="expenseDate" type="text" class="datepicker" style="height:15px; width:120px;" onClick="ressetDate(this);" value="<c:out value="${toDate}"/>"></td>
                                </tr>
                                <tr>
                                    <td width="80" >Expense Type </td>
                                    <td width="80" > <select name="expensesType" id="expensesType" class="textbox" style="height:20px; width:122px;" >
                                            <c:if test="${vehicleTypeList != null}">
                                               <option value='0'>-Select-</option><option value='1'>Bill To Customer</option><option value='2'>Do Not Bill Customer</option>
                                            </c:if>
                                        </select></td>
                                    <td width="80" >Bill Mode</td>
                                    <td><input type="text" name="billMode"   id="billMode" value="0" class="textbox" readonly></td>
                                    <td width="80" >Margin Value</td>
                                    <td width="80" > <input type="text" name="marginValue" id="marginValue" value="0.00" class="textbox"></td>

                                </tr>
                                <tr>

                                    <td width="80">TAX</td>

                                    <td width="80">

                                        <input type="text" name="taxExpenses" id="taxExpenses" value="" class="textbox" />
                                    </td>
                                     &nbsp; &nbsp; &nbsp; &nbsp;
                                    <td width="80">Expense Value</td>
                                    <td  width="80"><input type="text" name="expenseValue" id="expenseValue" value="" class="textbox" onchange="total(this.value);">
                                    </td>

                                     &nbsp; &nbsp; &nbsp; &nbsp;
                                    <td width="80" ><font color="red">*</font>Total Expense</td>
                                    <td width="80"><input type="text" name="totalExpenses" id="totalExpenses" value="" class="textbox" readonly/></td>


                                </tr>
                                <tr>

                                    <td width="80" >Remarks</td>
                                    <td >  <textarea name="remarks" id="remarks" value="" width="200px" height="150px"> </textarea>
                                    </td>
                                </tr>



                                <tr>
                                    <td  colspan="6" align="center">
                                        <input type="hidden"   value="<c:out value="${statusId}"/>" class="text" name="stsId" id="stsId">
                                        <input type="hidden"   value="<c:out value="${tripType}"/>" class="text" name="tripType">
                                        <input type="button"   value="Save" class="button" name="Save" onclick="submitPage();" ></td>


                                </tr>

                            </table>



        </table>


        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

    </body>
</html>
