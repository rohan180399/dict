

<html>
<head>
    
    
    <%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>     
    <link href="/throttle/css/parveen.css" rel="stylesheet"/>
    <%@ page import="ets.domain.mrs.business.MrsTO" %>
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script>
    <title>RCWO</title>




</head>
<body>


<script>
function submitPage(){
    if( document.mpr.vendorId.value == '0' ){
        alert("Please select Vendor");
        document.mpr.vendorId.focus();
        return;
    }
    if( document.mpr.remarks.value == '' ){
        alert("Please enter remarks");
        document.mpr.remarks.focus();
        return;
    }
    document.mpr.action="/throttle/handleAlterRCWO.do";
    document.mpr.submit();
}            


function cancelWo(){
    
}


</script>


<script>
   function changePageLanguage(langSelection){
   if(langSelection== 'ar'){
   document.getElementById("pAlign").style.direction="rtl";
   }else if(langSelection== 'en'){
   document.getElementById("pAlign").style.direction="ltr";
   }
   }
 </script>

  <c:if test="${jcList != null}">
  <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
  </c:if>
      
  <span style="float: right">
	<a href="?paramName=en">English</a>
	|
	<a href="?paramName=ar">Arabic</a>
  </span>

<form name="mpr"  method="post" >                        
    <br>

<% int cntr = 0; %>
    <c:if test="${woDetail != null}" >

    <c:forEach items="${woDetail}" var="work">
      <% if(cntr == 0 ){ %>

    <table width="700" align="center" border="0" cellpadding="0" cellspacing="0"  >
        <tr>
            <Td HEIGHT="30" class="text1" width="175"  ><spring:message code="stores.label.WorkOrderNo"  text="default text"/>
  </td>
            <Td HEIGHT="30"  class="text1" width="175"> <input type="hidden" name="woId" value='<c:out value="${work.woId}" />' > <c:out value="${work.woId}" />  </td>
            <Td HEIGHT="30"  class="text1" width="175"> <spring:message code="stores.label.VendorName"  text="default text"/> </td>
            <Td HEIGHT="30"  class="text1" width="175"  >
                <select name="vendorId" class="form-control" >
                    <option value="0" >-Select-</option>
                    <c:if test="${vendorList != null}" >
                        <c:forEach items="${vendorList}" var="vend" >
                            <c:choose>
                                <c:when test="${vend.vendorId == work.vendorId}" >
                                    <option selected value='<c:out value="${vend.vendorId}" />' > <c:out value="${vend.vendorName}" /> </option>
                                </c:when>
                                <c:otherwise>
                                    <option value='<c:out value="${vend.vendorId}" />' > <c:out value="${vend.vendorName}" /> </option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </c:if>
                </select>
            </td>
        </tr>

        <tr>
            <Td HEIGHT="30"  class="text1" width="175"  ><spring:message code="stores.label.WorkorderDate"  text="default text"/> </td>  </td>
            <Td HEIGHT="30"  class="text1" width="175"  >
                <input type="text" name="createdDate" readonly class="form-control" value='<c:out value="${work.createdDate}" />' >
                    <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.mpr.createdDate,'dd-mm-yyyy',this)"/>
            </td>
            <Td HEIGHT="30"  class="text1" width="175"  > <spring:message code="stores.label.Remarks"  text="default text"/> </td>
            <Td HEIGHT="30"  class="text1" width="175"  >
             <textarea name="remarks" class="form-control" ><c:out value="${work.remarks}" /></textarea>
            </td>
        </tr>

        <% cntr++; } %>

        </c:forEach>

    </table>
        </c:if>



    <br>
    <br>

    
    <% int indx = 0; %>
    <c:if test="${woDetail != null}" >

    <table width="700" align="center" border="0" cellpadding="0" cellspacing="0"  >
           
        <tr>
        <td class="contentsub" height="30"> <spring:message code="stores.label.Sno"  text="default text"/> </td>
                    <td class="contentsub" height="30">MFR Item code</td>
                    <td class="contentsub" height="30"> <spring:message code="stores.label.ItemCode"  text="default text"/> </td>
                    <td class="contentsub" height="30"> <spring:message code="stores.label.ItemName"  text="default text"/> </td>
                    <td class="contentsub" height="30">UOM</td>
                    <td class="contentsub" height="30"> <spring:message code="stores.label.Quantity"  text="default text"/> </td>


        </tr>
         
    <c:forEach items="${woDetail}" var="work">        
                    <%
            String classText = "";
            int oddEven = indx % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                    %>
        
        <tr>
            <Td HEIGHT="30" class="<%= classText %>" width="25" align="right" > <%= indx+1 %> </td>
            <Td HEIGHT="30"  class="<%= classText %>" width="90"  align="center"> <input type="text"  class="form-control" name="mfrCode" value='' >  </td>
            <Td HEIGHT="30"  class="<%= classText %>" width="90"  align="center"><input type="text"  class="form-control"  name="itemCode" value='<c:out value="${work.paplCode}" />' >   </td>
            <Td HEIGHT="30"  class="<%= classText %>" width="400" align="left"><input type="text"  class="form-control"  name="itemName" value='<c:out value="${work.itemName}" />' >  </td>
            <Td HEIGHT="30"  class="<%= classText %>" width="60"  align="center"><input name="uom" size='5'  class='form-control'  type='text' value='<c:out value="${work.uomName}" />' >   </td>
            <Td HEIGHT="30"  class="<%= classText %>" width="60"  align="right"><input name="qty" size='5'  class='form-control'  type='text' value='<c:out value="${work.quantity}" />' >  </td>
            
        </tr>
            <% indx++; %>
        </c:forEach>
        
    </table>
        </c:if>     
    <center>   
        <input type="button" class="button" name="Save" value=" <spring:message code="stores.label.Save"  text="default text"/>" onClick="submitPage();" >
        <%--<input type="button" class="button" name="Cancel" value="Cancel" onClick="" >--%> 
    </center>

<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
