/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.stockTransfer.web;

/**
 *
 * @author sankar
 */
import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;
import ets.arch.exception.FPRuntimeException;
import ets.arch.exception.FPBusinessException;
import ets.arch.web.BaseController;
import ets.domain.stockTransfer.business.StockTransferBP;
import ets.domain.purchase.business.PurchaseBP;
import ets.domain.stockTransfer.business.StockTransferTO;
import ets.domain.mrs.business.MrsBP;
import ets.domain.users.business.LoginBP;
import ets.domain.util.FPLogUtils;
import ets.domain.util.ParveenErrorConstants;
import ets.domain.vehicle.business.VehicleBP;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;

/****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver              Date                      Author                    Change
 * ---------------------------------------------------------------------------
 * 1.0           Mar 3, 2009              vijay			       Created
 *
 ******************************************************************************/
public class StockTransferController extends BaseController {

    StockTransferCommand stockTransferCommand;
    LoginBP loginBP;
    StockTransferBP stocktransferBP;
    PurchaseBP purchaseBP;
    VehicleBP vehicleBP;
    StockTransferTO stocktransferTO;

    public VehicleBP getVehicleBP() {
        return vehicleBP;
    }

    public void setVehicleBP(VehicleBP vehicleBP) {
        this.vehicleBP = vehicleBP;
    }

    public StockTransferBP getStocktransferBP() {
        return stocktransferBP;
    }

    public void setStocktransferBP(StockTransferBP stocktransferBP) {
        this.stocktransferBP = stocktransferBP;
    }

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    public StockTransferCommand getStockTransferCommand() {
        return stockTransferCommand;
    }

    public void setStockTransferCommand(StockTransferCommand stockTransferCommand) {
        this.stockTransferCommand = stockTransferCommand;
    }

    public StockTransferTO getStocktransferTO() {
        return stocktransferTO;
    }

    public void setStocktransferTO(StockTransferTO stocktransferTO) {
        this.stocktransferTO = stocktransferTO;
    }

    public PurchaseBP getPurchaseBP() {
        return purchaseBP;
    }

    public void setPurchaseBP(PurchaseBP purchaseBP) {
        this.purchaseBP = purchaseBP;
    }

    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        binder.closeNoCatch();  initialize(request);
    }

    /**
     * This method caters to get Stock Request
     *
     * @param 	request - Http request object
     *
     * @param 	response - Http response object
     *
     * @param	command - JobCommand object contains the request values.
     *
     * @return ModelAndView
     */
    public ModelAndView handleServicePoint(HttpServletRequest request, HttpServletResponse response, StockTransferCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        int companyType = 1012;
        int companyId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        System.out.println("companyId=" + companyId);
        ArrayList servicePointList = new ArrayList();
        String menuPath = "";
        menuPath = "Stock Transfer >>Approve Request";
        String pageTitle = "Approve Request";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
             setLocale(request, response);

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "StockTransfer-Approve")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                path = "content/stockTransfer/STApprovalList.jsp";
                servicePointList = stocktransferBP.processServicePtList(companyType, companyId);
                request.setAttribute("servicePointList", servicePointList);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (FPBusinessException exception) {

            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view  service Point List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleViewStockRequest(HttpServletRequest request, HttpServletResponse response, StockTransferCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        int companyId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        System.out.println("companyId=" + companyId);
        String path = "";
        int companyType = 1012;
        ArrayList servicePointList = new ArrayList();
        ArrayList requestList = new ArrayList();
        String menuPath = "";
        menuPath = "Stock Transfer >>Approve Request";
        String pageTitle = "Approve Request";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "StockTransfer-RequestList")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                String requestPtId = (String) request.getParameter("servicePoint");
                path = "content/stockTransfer/STApprovalList.jsp";
                System.out.println("before submit requestPtId" + requestPtId);
                servicePointList = stocktransferBP.processServicePtList(companyType, companyId);
                System.out.println("servicePointList" + servicePointList.size());
                request.setAttribute("servicePointList", servicePointList);
                requestList = stocktransferBP.processRequesttList(Integer.parseInt(requestPtId));
                System.out.println("requestPtId=" + requestPtId);
                request.setAttribute("requestList", requestList);
                request.setAttribute("requestPtId", requestPtId);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (FPBusinessException exception) {

            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view  service Point List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleStockApproval(HttpServletRequest request, HttpServletResponse response, StockTransferCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        System.out.println("request=" + request);
        HttpSession session = request.getSession();
        int companyId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        System.out.println("companyId=" + companyId);
        stockTransferCommand = command;
        System.out.println("view Specific Stock Request");
        String menuPath = "";
        String pageTitle = "Stock Approval";
        request.setAttribute("pageTitle", pageTitle);
        ArrayList itemList = new ArrayList();
        ArrayList servicePointItemList = new ArrayList();
        ArrayList mrsDetail = new ArrayList();
        menuPath = "Stock Transfer >> Approve request >> Approve ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            System.out.println("RequestPtId=" + request.getParameter("servicePoint"));
            String requestId = request.getParameter("requestId");
            System.out.println("requestId" + requestId);
            path = "content/stockTransfer/STApproval.jsp";
            stocktransferTO = new StockTransferTO();
            stocktransferTO.setRequestId(requestId);
            stocktransferTO.setRequestPtId((String) request.getParameter("servicePoint"));
            itemList = stocktransferBP.processItemtList(companyId, stocktransferTO);
            request.setAttribute("itemList", itemList);
            System.out.println("itemList size" + itemList.size());
            request.setAttribute("RequestId", requestId);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {

            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewMrsDetails --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleStockApprovedQty(HttpServletRequest request, HttpServletResponse response, StockTransferCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        stockTransferCommand = command;
        ArrayList servicePointList = new ArrayList();
        int companyId = 0;
        System.out.println("companyId=" + session.getAttribute("companyId"));
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        int companyType = 1012;
        String path = "";
        try {
            System.out.println("in Stock Transfer Add Approved Quantity  ");
            String menuPath = "Stores  >> Stock Transfer >> Approve Request ";
            String pageTitle = "Approval List";
            request.setAttribute("pageTitle", pageTitle);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            System.out.println("in Stock Add Approved Quantity  ");
            int status = 0;
            String approvalStatus = "";
            approvalStatus = stockTransferCommand.getApprovalStatus();

            int index = 0;
            int insert = 0;
            String[] ids = stockTransferCommand.getItemIds();
            String[] requestedQtys = stockTransferCommand.getRequestedQtys();
            String[] approvedQtys = stockTransferCommand.getApprovedQtys();
            int userId = (Integer) session.getAttribute("userId");
            System.out.println("userId" + userId);
            StockTransferTO stocktransferTO = new StockTransferTO();
            stocktransferTO.setApprovalStatus(stockTransferCommand.getApprovalStatus());
            stocktransferTO.setRemarks(stockTransferCommand.getRemarks());
            stocktransferTO.setRequestedQty(stockTransferCommand.getRequestedQty());
            stocktransferTO.setRequestId(stockTransferCommand.getRequestId());
            System.out.println("stockTransferCommand.getRequestId()" + stocktransferTO.getRequestId());
            ArrayList List = new ArrayList();
            StockTransferTO listTO = null;
            for (int i = 0; i < ids.length; i++) {
                listTO = new StockTransferTO();
                index = i;
                System.out.println("index" + index);
                listTO.setItemId((ids[index]));
                System.out.println("listTO.getItemId()" + listTO.getItemId());
                listTO.setRequestedQty(requestedQtys[index]);
                listTO.setApprovedQty(approvedQtys[index]);
                List.add(listTO);
            }
            path = "content/stockTransfer/STApprovalList.jsp";
            insert = stocktransferBP.processApprovedQty(userId, List, stocktransferTO);
            System.out.println("status=" + insert);
            servicePointList = stocktransferBP.processServicePtList(companyType, companyId);
            request.setAttribute("servicePointList", servicePointList);
            if (insert > 0) {
                if (stocktransferTO.getApprovalStatus().equalsIgnoreCase("REJECTED")) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Stock Request Rejected  Successfully");
                } else {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Stock Request Approved  Successfully");
                }
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to ADD data  --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleIssueStockTransfer(HttpServletRequest request, HttpServletResponse response, StockTransferCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        int companyId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        System.out.println("companyId=" + companyId);
        String menuPath = "Stock Transfer >>Transfer Stock   ";
        try {
             setLocale(request, response);

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "StockTransfer-IssueList")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                path = "content/stockTransfer/manageStockIssue.jsp";
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                String pageTitle = "View Stock";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList approvedStockList = new ArrayList();
                // get values from DB to display in manage Screen
                approvedStockList = stocktransferBP.processGetapprovedStockList(companyId);
                request.setAttribute("ApprovedStockList", approvedStockList);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Vendor data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //handleIssueItemPage
    /**
     * This method used to View Vendor Page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleIssueItemPage(HttpServletRequest request, HttpServletResponse response, StockTransferCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "Stock Transfer >> Transfer Stock >>Issue";
        int companyId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        System.out.println("companyId=" + companyId);
        ArrayList approvedStockListAll = new ArrayList();
        ArrayList priceList = new ArrayList();
        ArrayList rcItemList = new ArrayList();
        ArrayList tyreList = new ArrayList();
        try {
            int requestId = Integer.parseInt(request.getParameter("requestId"));
            System.out.println("requestId" + requestId);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "Stock Issue ";
            request.setAttribute("pageTitle", pageTitle);
            path = "content/stockTransfer/itemIssue.jsp";
            // get values from DB to display in manage Screen
            approvedStockListAll = stocktransferBP.processGetapprovedStockListAll(requestId);
            request.setAttribute("ApprovedStockListAll", approvedStockListAll);
            ArrayList newArray = new ArrayList();
            
            System.out.println("Nex price Id");
            priceList = stocktransferBP.processGetPriceList(requestId, companyId);
            System.out.println("pricelist-->" + priceList.size());
            tyreList = stocktransferBP.processTyresList(companyId);
            System.out.println("tyrelist-->" + tyreList.size());



            int i = 0;
            String itemId = "";
            String itemId1 = "";


            StockTransferTO stocktransfer = null;
            StockTransferTO stocktransfer1 = null;
            StockTransferTO stocktransfer2 = null;

            Iterator itr1 = approvedStockListAll.iterator();
            

            
            while (itr1.hasNext()) {
                stocktransfer1 = new StockTransferTO();
                stocktransfer1 = (StockTransferTO) itr1.next();
                itemId1 = stocktransfer1.getItemId();
                System.out.println("In Approves-->" + itemId1);
                Iterator itr = priceList.iterator();

                while (itr.hasNext()) {
                    stocktransfer = new StockTransferTO();
                    stocktransfer2 = new StockTransferTO();
                    stocktransfer = (StockTransferTO) itr.next();
                    itemId = stocktransfer.getItemId();
                    System.out.println("In Approves-->" + itemId);

                    if (itemId1.equals(itemId)) {
                        System.out.println("Equals If");
                        stocktransfer2.setItemId(stocktransfer.getItemId());
                        stocktransfer2.setPrice(stocktransfer.getPrice());
                        stocktransfer2.setPriceId(stocktransfer.getPriceId());
                        stocktransfer2.setStockQty(stocktransfer.getStockQty());
                        stocktransfer2.setTax(stocktransfer.getTax());
                        stocktransfer2.setPricetype(stocktransfer.getPricetype());
                        stocktransfer2.setRequestId(stocktransfer1.getRequestId());
                        stocktransfer2.setItemName(stocktransfer1.getItemName());
                        stocktransfer2.setPaplCode(stocktransfer1.getPaplCode());
                        stocktransfer2.setMfrCode(stocktransfer1.getMfrCode());
                        stocktransfer2.setCategoryId(stocktransfer1.getCategoryId());
                        stocktransfer2.setUomName(stocktransfer1.getUomName());
                        stocktransfer2.setServicePointName(stocktransfer1.getServicePointName());
                        stocktransfer2.setApprovedQuandity(stocktransfer1.getApprovedQuandity());
                        stocktransfer2.setApprovedId(stocktransfer1.getApprovedId());

                        System.out.println("PriceList Item Id-->" + itemId + "i value-->" + i);
                        System.out.println("Approval List Item Id-->" + itemId1 + "i value-->" + i);
                        newArray.add(stocktransfer2);
                    }
                    System.out.println("IN While Loop");
                }
            }
            
            System.out.println("New Array List size-->" + newArray.size());
            

            request.setAttribute("newArray", newArray);
            System.out.println("tyres size=" + tyreList.size());
            request.setAttribute("tyreList", tyreList);
            request.setAttribute("PriceList", priceList);
            System.out.println("PriceList " + priceList.size());
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Retrieve priceList--> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    public ModelAndView handleRCIssueItemPage(HttpServletRequest request, HttpServletResponse response, StockTransferCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "Stock Transfer >> Transfer Stock >>Issue";
        int companyId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        System.out.println("companyId=" + companyId);
        ArrayList approvedStockListAll = new ArrayList();
        ArrayList priceList = new ArrayList();
        ArrayList rcItemList = new ArrayList();
        ArrayList tyreList = new ArrayList();
        try {
            int requestId = Integer.parseInt(request.getParameter("requestId"));
            System.out.println("requestId" + requestId);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "Stock Issue ";
            request.setAttribute("pageTitle", pageTitle);
            path = "content/stockTransfer/rcItemIssueNew.jsp";
            // get values from DB to display in manage Screen
            approvedStockListAll = stocktransferBP.processGetRCApprovedStockListAll(requestId);
            request.setAttribute("ApprovedStockListAll", approvedStockListAll);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Retrieve priceList--> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleIssueRcItemPage(HttpServletRequest request, HttpServletResponse response, StockTransferCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "Stock Transfer >> Transfer Stock >>Issue >> RcItem";
        int companyId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        System.out.println("companyId=" + companyId);
        ArrayList approvedStockListAll = new ArrayList();
        ArrayList priceList = new ArrayList();
        ArrayList rcItemList = new ArrayList();
        try {
            int requestId = Integer.parseInt(request.getParameter("requestId"));
            int itemId = Integer.parseInt(request.getParameter("itemId"));
            String approvedQtys = (request.getParameter("approvedQtys"));
            int rowIndex = Integer.parseInt(request.getParameter("rowIndex"));
            System.out.println("requestId" + requestId);
            System.out.println("approvedQtys HHHH" + approvedQtys);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            request.setAttribute("rowIndex", rowIndex);
            request.setAttribute("approvedQtys", approvedQtys);
            String pageTitle = "Stock Issue ";
            request.setAttribute("pageTitle", pageTitle);
            path = "content/stockTransfer/rcItemIssue.jsp";
            // get values from DB to display in manage Screen
            approvedStockListAll = stocktransferBP.processGetapprovedStockListAll(requestId);
            System.out.println("papl");
            request.setAttribute("ApprovedStockListAll", approvedStockListAll);
            rcItemList = stocktransferBP.processRcItemList(requestId, itemId, companyId);
            System.out.println("rcItemList in Controller" + rcItemList.size());
            if (rcItemList.size() != 0) {
                request.setAttribute("rcItemList", rcItemList);
                System.out.println("rcItemList " + rcItemList.size());
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Retrieve priceList--> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleIssuedItemToTransfer(HttpServletRequest request, HttpServletResponse response, StockTransferCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        stockTransferCommand = command;
        System.out.println("request=" + request);
        int userId = (Integer) session.getAttribute("userId");
        System.out.println("userId" + userId);
        ArrayList approvedStockList = new ArrayList();
        ArrayList tyreList = new ArrayList();
        int companyId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        System.out.println("companyId=" + companyId);
        String path = "";
        String pageTitle = "Approved  StockList";
        request.setAttribute("pageTitle", pageTitle);
        try {
            String menuPath = "Stock Transfer >> Transfer Stock";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int index = 0;
            String approvedId = (String) request.getParameter("approvedId");
            System.out.println("approvedId" + approvedId);
            String[] itemIds = stockTransferCommand.getItemIds();
            String[] issuedQtys = stockTransferCommand.getIssuedQtys();
            String[] prices = stockTransferCommand.getPriceIds();
            String[] pricesTypes = stockTransferCommand.getPriceTypes();
            String[] taxs = stockTransferCommand.getTaxs();
            String[] rcQtys = stockTransferCommand.getRcQtys();
            String[] selectedIndex = stockTransferCommand.getSelectedIndex();
            String[] categoryIds = stockTransferCommand.getCategoryId();
            String[] tyreIds = stockTransferCommand.getTyreId();
            String[] tyreType = request.getParameterValues("tyreType");
            System.out.println("remarks" + stockTransferCommand.getRemarks());
            System.out.println("itemsIds" + itemIds.length);
            System.out.println("itemsIds" + issuedQtys.length);
            System.out.println("itemsIds" + selectedIndex.length);
            System.out.println("itemsIds" + prices.length);
            stocktransferTO = new StockTransferTO();
            stocktransferTO.setRemarks(stockTransferCommand.getRemarks());
            stocktransferTO.setApprovedId(approvedId);
            stocktransferTO.setUserId(String.valueOf(userId));
            stocktransferTO.setCompanyId(String.valueOf(companyId));
            ArrayList List = new ArrayList();
            StockTransferTO listTO = null;
            for (int i = 0; i < selectedIndex.length; i++) {
                index = Integer.parseInt(selectedIndex[i]);
                if (!categoryIds[index].equals("1011")) {
                    listTO = new StockTransferTO();
                    System.out.println("index" + index);
                    listTO.setItemId((itemIds[index]));
                    System.out.println("Item id to transfer" + itemIds[index]);
                    listTO.setIssuedQty((issuedQtys[index]));
                    listTO.setRcQty(rcQtys[index]);
                    listTO.setPrice(prices[index]);
                    listTO.setPricetype(pricesTypes[index]);
                    System.out.println("Item Price type-->" + pricesTypes[index]);
                    listTO.setTax(taxs[index]);
                    float price = Float.valueOf(listTO.getPrice());
                    float qty = Float.valueOf(listTO.getIssuedQty());
                    String amount = (String.valueOf(price * qty));
                    listTO.setAmount(amount);
                    System.out.println("amount=" + listTO.getAmount());
                    List.add(listTO);
                }
                if (categoryIds[index].equals("1011")) {
                    System.out.println("In Tyre Block Transfer");
                    listTO = new StockTransferTO();
                    System.out.println("index" + index);
                    listTO.setItemId((itemIds[index]));
                    listTO.setIssuedQty((issuedQtys[index]));
                    listTO.setTyreId((tyreIds[index]));
                    listTO.setPrice(prices[index]);
                    listTO.setTax(taxs[index]);
                    float price = Float.valueOf(listTO.getPrice());
                    float qty = Float.valueOf(listTO.getIssuedQty());
                    String amount = (String.valueOf(price * qty));
                    listTO.setAmount(amount);
                    listTO.setTyreType(tyreType[index]);
                    tyreList.add(listTO);
                }


            }
            path = "content/stockTransfer/manageStockIssue.jsp";
            System.out.println("List In Controller" + List.size());
            System.out.println("tyreList In Controller" + tyreList.size());
            int insert = stocktransferBP.processTransferItems(List, tyreList, stocktransferTO);
            System.out.println("insert=" + insert);
            approvedStockList = stocktransferBP.processGetapprovedStockList(companyId);
            request.setAttribute("ApprovedStockList", approvedStockList);
            if (insert > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Stock Transfered Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Add data  --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    // handleIssuedItemToTransfer
    /**
     * This method used to Modify Grade Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleRcTransfer(HttpServletRequest request, HttpServletResponse response, StockTransferCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        stockTransferCommand = command;
        System.out.println("request=" + request);
        int userId = (Integer) session.getAttribute("userId");
        System.out.println("userId" + userId);
        ArrayList approvedStockList = new ArrayList();
        ArrayList approvedStockListAll = new ArrayList();
        ArrayList priceList = new ArrayList();
        int companyId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        System.out.println("companyId=" + companyId);
        String path = "";
        String pageTitle = "Approved  StockList";
        request.setAttribute("pageTitle", pageTitle);
        try {
            String menuPath = "Stock Transfer >> Transfer Stock";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int index = 0;
            String approvedId = (String) request.getParameter("approvedId");
            System.out.println("approvedId" + approvedId);
            String[] itemIds = stockTransferCommand.getItemIds();
            String[] rcItemIds = stockTransferCommand.getRcItemIds();
            String[] prices = stockTransferCommand.getPrices();
            String[] selectedIndex = stockTransferCommand.getSelectedIndex();
            stocktransferTO = new StockTransferTO();
            stocktransferTO.setApprovedId(approvedId);
            stocktransferTO.setUserId(String.valueOf(userId));
            stocktransferTO.setCompanyId(String.valueOf(companyId));
            ArrayList List = new ArrayList();
            StockTransferTO listTO = null;
            for (int i = 0; i < selectedIndex.length; i++) {
                listTO = new StockTransferTO();
                index = Integer.parseInt(selectedIndex[i]);
                System.out.println("index" + index);
                listTO.setItemId((itemIds[index]));
                listTO.setPrice(prices[index]);
                listTO.setRcItemId(rcItemIds[index]);
                List.add(listTO);
            }
            path = "content/stockTransfer/rcItemIssueNew.jsp";
            int requestId = Integer.parseInt(request.getParameter("requestId"));
            System.out.println("requestId" + requestId);
            approvedStockListAll = stocktransferBP.processGetapprovedStockListAll(requestId);
            request.setAttribute("ApprovedStockListAll", approvedStockListAll);
            priceList = stocktransferBP.processGetPriceList(requestId, companyId);
            request.setAttribute("PriceList", priceList);
            System.out.println("PriceList " + priceList.size());
            int insert = stocktransferBP.processTransferRcItems(List, stocktransferTO);
            System.out.println("insert=" + insert);

            if (insert > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Rc Added Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Add data  --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    public ModelAndView handleRcTransferNew(HttpServletRequest request, HttpServletResponse response, StockTransferCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        stockTransferCommand = command;
        System.out.println("request=" + request);
        int userId = (Integer) session.getAttribute("userId");
        System.out.println("userId" + userId);
        ArrayList approvedStockList = new ArrayList();
        ArrayList approvedStockListAll = new ArrayList();
        ArrayList priceList = new ArrayList();
        int companyId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        System.out.println("companyId=" + companyId);
        String path = "";
        String pageTitle = "Approved  StockList";
        request.setAttribute("pageTitle", pageTitle);
        try {
            String menuPath = "Stock Transfer >> Transfer Stock";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int index = 0;
            String approvedId = (String) request.getParameter("approvedId");
            System.out.println("approvedId" + approvedId);
            String[] itemIds = stockTransferCommand.getItemIds();
            String[] issuedQtys = stockTransferCommand.getIssuedQtys();
            stocktransferTO = new StockTransferTO();
            stocktransferTO.setApprovedId(approvedId);
            stocktransferTO.setUserId(String.valueOf(userId));
            stocktransferTO.setCompanyId(String.valueOf(companyId));
            ArrayList List = new ArrayList();
            StockTransferTO listTO = null;
            for (int i = 0; i < itemIds.length; i++) {
                listTO = new StockTransferTO();
                listTO.setItemId((itemIds[i]));
                listTO.setIssuedQty((issuedQtys[i]));
                List.add(listTO);
                
            }
            


            int requestId = Integer.parseInt(request.getParameter("requestId"));
            System.out.println("requestId" + requestId);

            path = "content/stockTransfer/manageStockIssue.jsp";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            pageTitle = "View Stock";
            request.setAttribute("pageTitle", pageTitle);
            approvedStockList = new ArrayList();
            // get values from DB to display in manage Screen
            approvedStockList = stocktransferBP.processGetapprovedStockList(companyId);
            request.setAttribute("ApprovedStockList", approvedStockList);


            int insert = stocktransferBP.processTransferRcItemsNew(List, stocktransferTO);
            System.out.println("insert=" + insert);

            if (insert > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "RC Transfer Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Add data  --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleServicePointList(HttpServletRequest request, HttpServletResponse response, StockTransferCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        int companyType = 1012;
        int companyId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        ArrayList servicePointList = new ArrayList();
        ArrayList otherServicePtList = new ArrayList();
        String menuPath = "";
        menuPath = "Stock Transfer >>Receive Stock";
        String pageTitle = "Receive Stock";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
             setLocale(request, response);

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "StockTransfer-Receive")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                path = "content/stockTransfer/receiveStock.jsp";
//            otherServicePtList = stocktransferBP.processServicePtList(companyType, companyId);           
//            request.setAttribute("otherservicePtList", servicePointList);
                servicePointList = stocktransferBP.processServicePtList(companyType, companyId);
                request.setAttribute("servicePointList", servicePointList);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (FPBusinessException exception) {

            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view  service Point List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleReceiveList(HttpServletRequest request, HttpServletResponse response, StockTransferCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/login.jsp");
//        }
        HttpSession session = request.getSession();
        String path = "";
        int companyId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        ArrayList servicePointList = new ArrayList();
        ArrayList receivedStockListAll = new ArrayList();
        int companyType = 1012;
        stockTransferCommand = command;
        String menuPath = "Stock Transfer>>Receive Stock";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            stocktransferTO = new StockTransferTO();
            String pageTitle = "Received StockList";
            request.setAttribute("pageTitle", pageTitle);
            path = "content/stockTransfer/receiveStock.jsp";
            stocktransferTO.setRequestPtId(String.valueOf(companyId));
            stocktransferTO.setServicePointId(stockTransferCommand.getServicePtId());
            servicePointList = stocktransferBP.processServicePtList(companyType, companyId);
            request.setAttribute("servicePointList", servicePointList);
            receivedStockListAll = stocktransferBP.processReceivedStockListAll(stocktransferTO, companyId);
            request.setAttribute("receivedStockListAll", receivedStockListAll);
            request.setAttribute("FromServicePoint", stockTransferCommand.getCompanyName());
            request.setAttribute("servicePtId", stockTransferCommand.getServicePtId());
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve stockList--> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

//    public ModelAndView handleReceiveItemPage(HttpServletRequest request, HttpServletResponse response, StockTransferCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/login.jsp");
//        }
//        HttpSession session = request.getSession();
//        String path = "";
//        ArrayList receivedItems = new ArrayList();
//        ArrayList receivedRcItems = new ArrayList();
//        ArrayList receivedStockListAll = new ArrayList();
//        stockTransferCommand = command;
//        int companyId = 0;
//        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
//        String menuPath = "Stock Transfer >> Receive Stock >> Receive ";
//        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//        try {
//            String gdId = (String) (request.getParameter("gdId"));
//            String requestId = (String) (request.getParameter("requestId"));
//            stocktransferTO.setServicePointId(stockTransferCommand.getServicePtId());
//            String pageTitle = "Receive Stock";
//            request.setAttribute("pageTitle", pageTitle);
//            path = "content/stockTransfer/receiveItems.jsp";
//            receivedStockListAll = stocktransferBP.processReceivedStockListAll(stocktransferTO, companyId);
//            request.setAttribute("receivedStockListAll", receivedStockListAll);
//            receivedItems = stocktransferBP.processTransferedItemList(gdId, stocktransferTO);
//            request.setAttribute("receivedItems", receivedItems);
//            request.setAttribute("servicePtId", stockTransferCommand.getServicePtId());
//            request.setAttribute("gdId", stockTransferCommand.getGdId());
//            request.setAttribute("requestId", requestId);
//            request.setAttribute("FromServicePoint", stockTransferCommand.getCompanyName());
//            request.setAttribute("TOServicePoint", stockTransferCommand.getServicePtName());
//
//        } catch (FPRuntimeException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        } catch (FPBusinessException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
//                    exception.getErrorMessage());
//        } catch (Exception exception) {
//            exception.printStackTrace();
//            FPLogUtils.fpErrorLog("Failed to retrieve priceList--> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        }
//        return new ModelAndView(path);
//    }
    public ModelAndView handleReceiveItemPage(HttpServletRequest request, HttpServletResponse response, StockTransferCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ArrayList receivedItems = new ArrayList();
        ArrayList receivedRcItems = new ArrayList();
        ArrayList receivedStockListAll = new ArrayList();
        stockTransferCommand = command;
        int companyId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String menuPath = "Stock Transfer >> Receive Stock >> Receive ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            String gdId = (String) (request.getParameter("gdId"));
            String reqType = (String) (request.getParameter("reqType"));
            System.out.println("reqType:"+reqType);
            request.setAttribute("reqType", reqType);
            String requestId = (String) (request.getParameter("requestId"));
            System.out.println("requestId in contB4--->" + requestId);
            stocktransferTO.setServicePointId(stockTransferCommand.getServicePtId());
            String pageTitle = "Receive Stock";
            request.setAttribute("pageTitle", pageTitle);
            path = "content/stockTransfer/receiveItems.jsp";
            receivedStockListAll = stocktransferBP.processReceivedStockListAll(stocktransferTO, companyId);
            request.setAttribute("receivedStockListAll", receivedStockListAll);
            receivedItems = stocktransferBP.processTransferedItemList(gdId, stocktransferTO);
            request.setAttribute("receivedItems", receivedItems);
            request.setAttribute("servicePtId", stockTransferCommand.getServicePtId());
            request.setAttribute("gdId", stockTransferCommand.getGdId());
            request.setAttribute("requestId", stockTransferCommand.getRequestId());
            System.out.println("requestId in contA4--->" + stockTransferCommand.getRequestId());
            request.setAttribute("FromServicePoint", stockTransferCommand.getCompanyName());
            request.setAttribute("TOServicePoint", stockTransferCommand.getServicePtName());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve priceList--> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleReceiveRcItemPage(HttpServletRequest request, HttpServletResponse response, StockTransferCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        System.out.println("success");
        HttpSession session = request.getSession();
        String path = "";
        ArrayList receivedItems = new ArrayList();
        ArrayList receivedRcItems = new ArrayList();
        ArrayList receivedStockListAll = new ArrayList();
        stockTransferCommand = command;
        int companyId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String menuPath = "Stock Transfer >> Receive Stock >> Receive >Rc Item";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            String gdId = (String) (request.getParameter("gdId"));

            System.out.println("gdId" + gdId);
            System.out.println("companyName" + stockTransferCommand.getCompanyName());
            request.setAttribute("gdId", gdId);
            stocktransferTO.setServicePointId(stockTransferCommand.getServicePtId());
            String pageTitle = "Receive RC Stock";
            request.setAttribute("pageTitle", pageTitle);
            path = "content/stockTransfer/rcItemReceived.jsp";
            receivedItems = stocktransferBP.processTransferedItemList(gdId, stocktransferTO);
            System.out.println("receivedItems" + receivedItems.size());
            receivedStockListAll = stocktransferBP.processReceivedStockListAll(stocktransferTO, companyId);
            request.setAttribute("receivedStockListAll", receivedStockListAll);
            request.setAttribute("receivedItems", receivedItems);
            request.setAttribute("servicePtId", stockTransferCommand.getServicePtId());
            request.setAttribute("FromServicePoint", stockTransferCommand.getCompanyName());
            request.setAttribute("TOServicePoint", stockTransferCommand.getServicePtName());
            receivedRcItems = stocktransferBP.processTransferedRcItem(gdId, stocktransferTO);
            System.out.println("receivedRcItems" + receivedRcItems.size());
            if (receivedRcItems.size() != 0) {
                request.setAttribute("receivedRcItems", receivedRcItems);
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve RcList--> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    public ModelAndView handleReceiveRcItemPageNew(HttpServletRequest request, HttpServletResponse response, StockTransferCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        System.out.println("success");
        HttpSession session = request.getSession();
        String path = "";
        ArrayList receivedItems = new ArrayList();
        ArrayList receivedRcItems = new ArrayList();
        ArrayList receivedStockListAll = new ArrayList();
        stockTransferCommand = command;

        int companyId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String menuPath = "Stock Transfer >> Receive Stock >> Receive >Rc Item";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            String gdId = (String) (request.getParameter("gdId"));

            System.out.println("gdId" + gdId);
            System.out.println("companyName" + stockTransferCommand.getCompanyName());
            request.setAttribute("gdId", gdId);
            stocktransferTO.setServicePointId(stockTransferCommand.getServicePtId());
            String pageTitle = "Receive RC Stock";
            request.setAttribute("pageTitle", pageTitle);
            path = "content/stockTransfer/rcItemReceivedNew.jsp";

            receivedItems = stocktransferBP.processTransferedItemList(gdId, stocktransferTO);
            System.out.println("receivedItems" + receivedItems.size());
            receivedStockListAll = stocktransferBP.processReceivedStockListAll(stocktransferTO, companyId);
            request.setAttribute("receivedStockListAll", receivedStockListAll);
            request.setAttribute("receivedItems", receivedItems);

            request.setAttribute("servicePtId", stockTransferCommand.getServicePtId());
            request.setAttribute("FromServicePoint", stockTransferCommand.getCompanyName());
            request.setAttribute("TOServicePoint", stockTransferCommand.getServicePtName());
            receivedRcItems = stocktransferBP.processTransferedRcItem(gdId, stocktransferTO);
            System.out.println("receivedRcItems" + receivedRcItems.size());
            if (receivedRcItems.size() != 0) {
                request.setAttribute("receivedRcItems", receivedRcItems);
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve RcList--> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleUpdateReceivedItem(HttpServletRequest request, HttpServletResponse response, StockTransferCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        stockTransferCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList approvedStockList = new ArrayList();
        ArrayList servicePointList = new ArrayList();
        ArrayList NewTyreList = new ArrayList();
        ArrayList rtTyreList = new ArrayList();
        ArrayList newItemList = new ArrayList();
        ArrayList rcItemList = new ArrayList();
        int companyId = 0;
        int grId = 0;
        int companyType = 1012;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String path = "";
        try {
            String menuPath = "Stores   >>  Stock Transfer    >> Receive Stock ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "Receive Stock";
            request.setAttribute("pageTitle", pageTitle);

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int index = 0;
            String[] itemIds = stockTransferCommand.getItemIds();
            String[] acceptedQtys = stockTransferCommand.getAcceptedQtys();
            String[] rcQtys = stockTransferCommand.getRcQtys();
            String[] taxs = stockTransferCommand.getTaxs();
            String[] prices = stockTransferCommand.getPriceIds();
            String[] selectedIndex = stockTransferCommand.getSelectedIndex();
            String[] tyreId = stockTransferCommand.getTyreId();
            String[] itemType = stockTransferCommand.getItemType();
            String[] tyreNo = stockTransferCommand.getTyreNo(); // if tyreno is 0 it is non tyre Item else it is tyre item

            stocktransferTO = new StockTransferTO();
            stocktransferTO.setRemarks(stockTransferCommand.getRemarks());
            stocktransferTO.setUserId(String.valueOf(userId));
            stocktransferTO.setCompanyId(String.valueOf(companyId));
            stocktransferTO.setGdId(stockTransferCommand.getGdId());
            ArrayList List = new ArrayList();
            StockTransferTO listTO = null;
            for (int i = 0; i < selectedIndex.length; i++) {
                listTO = new StockTransferTO();
                index = Integer.parseInt(selectedIndex[i]);
                listTO.setTyreId((tyreId[index]));
                listTO.setItemId((itemIds[index]));
                listTO.setAcceptedQty(acceptedQtys[index]);
                listTO.setPrice(prices[index]);
                //listTO.setPriceId(prices[index]);
                listTO.setTax(taxs[index]);
                listTO.setTyreNo(tyreNo[index]);

                if (itemType[index].equalsIgnoreCase("NEW")) {
                    newItemList.add(listTO);
                } else {
                    rcItemList.add(listTO);
                }
            }
            System.out.println("newItemList size :-" + newItemList.size());
            System.out.println("rcItemList size :-" + rcItemList.size());

            path = "content/stockTransfer/receiveStock.jsp";

            grId = stocktransferBP.processReceivedNewItems(newItemList, stocktransferTO);
            request.setAttribute("grId", grId);
            System.out.println("grId in Contr-->" + grId);
            stocktransferTO.setGrId(String.valueOf(grId));
            System.out.println("grId in Contr A4-->" + String.valueOf(grId));
            servicePointList = stocktransferBP.processServicePtList(companyType, companyId);
            System.out.println("serviceptList size" + servicePointList.size());
            request.setAttribute("servicePointList", servicePointList);
            if (grId != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "New Stock Added Successfully");
            }
            if (rcItemList.size() != 0) {
                int update = stocktransferBP.processUpdateRcItems(rcItemList, stocktransferTO);
                System.out.println("update status" + update);
                if (update > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "RcStock Added Successfully");
                }
            }
            System.out.println("Final am Exit");
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Add data  --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleUpdateRcItem(HttpServletRequest request, HttpServletResponse response, StockTransferCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        stockTransferCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList receivedStockListAll = new ArrayList();
        ArrayList receivedItems = new ArrayList();
        ArrayList receivedRcItems = new ArrayList();
        int companyId = 0;
        int companyType = 1012;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String path = "";
        try {
            String menuPath = "Stores   >>  Stock Transfer  >> Receive Stock ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "Receive RC Stock";
            request.setAttribute("pageTitle", pageTitle);
            int index = 0;
            String gdId = (String) (request.getParameter("gdId"));
            System.out.println("gdId" + gdId);
            System.out.println("stockTransferCommand .getGdId()" + stockTransferCommand.getGdId());
            String[] itemIds = stockTransferCommand.getItemIds();
            String[] rcItemIds = stockTransferCommand.getRcItemIds();
            String[] prices = stockTransferCommand.getPrices();
            String[] selectedIndex = stockTransferCommand.getSelectedIndex();
            stocktransferTO = new StockTransferTO();
            stocktransferTO.setServicePointId(stockTransferCommand.getServicePtId());
            stocktransferTO.setUserId(String.valueOf(userId));
            stocktransferTO.setCompanyId(String.valueOf(companyId));
            stocktransferTO.setGdId(gdId);
            ArrayList List = new ArrayList();
            StockTransferTO listTO = null;
            for (int i = 0; i < selectedIndex.length; i++) {
                listTO = new StockTransferTO();
                index = Integer.parseInt(selectedIndex[i]);
                listTO.setItemId((itemIds[index]));
                listTO.setRcItemId(rcItemIds[index]);
                listTO.setPrice(prices[index]);
                List.add(listTO);
            }
            path = "content/stockTransfer/rcItemReceived.jsp";
            int update = stocktransferBP.processUpdateRcItems(List, stocktransferTO);
            receivedItems = stocktransferBP.processTransferedItemList(gdId, stocktransferTO);
            request.setAttribute("receivedItems", receivedItems);
            receivedStockListAll = stocktransferBP.processReceivedStockListAll(stocktransferTO, companyId);
            request.setAttribute("receivedStockListAll", receivedStockListAll);
            receivedRcItems = stocktransferBP.processTransferedRcItem(gdId, stocktransferTO);
            if (receivedRcItems.size() != 0) {
                request.setAttribute("receivedRcItems", receivedRcItems);
            }
            request.setAttribute("servicePtId", stockTransferCommand.getServicePtId());
            request.setAttribute("gdId", gdId);
            request.setAttribute("FromServicePoint", stockTransferCommand.getCompanyName());
            request.setAttribute("TOServicePoint", stockTransferCommand.getServicePtName());
            if (update > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "RC Stock Added Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Add data  --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    public ModelAndView handleUpdateRcItemNew(HttpServletRequest request, HttpServletResponse response, StockTransferCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        stockTransferCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList receivedStockListAll = new ArrayList();
        ArrayList receivedItems = new ArrayList();
        ArrayList receivedRcItems = new ArrayList();
        int companyId = 0;
        int companyType = 1012;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String path = "";
        try {
            String menuPath = "Stores   >>  Stock Transfer  >> Receive Stock ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "Receive RC Stock";
            request.setAttribute("pageTitle", pageTitle);
            int index = 0;
            String gdId = (String) (request.getParameter("gdId"));
            System.out.println("gdId" + gdId);
            System.out.println("stockTransferCommand .getGdId()" + stockTransferCommand.getGdId());
            String[] itemIds = stockTransferCommand.getItemIds();
            //String[] rcItemIds = stockTransferCommand.getRcItemIds();
            String[] rcItemIds = stockTransferCommand.getTyreId();
            String[] prices = stockTransferCommand.getPriceIds();
            String[] selectedIndex = stockTransferCommand.getSelectedIndex();
            stocktransferTO = new StockTransferTO();
            stocktransferTO.setServicePointId(stockTransferCommand.getServicePtId());
            stocktransferTO.setUserId(String.valueOf(userId));
            stocktransferTO.setCompanyId(String.valueOf(companyId));
            stocktransferTO.setGdId(gdId);
            ArrayList List = new ArrayList();
            StockTransferTO listTO = null;
            for (int i = 0; i < selectedIndex.length; i++) {
                listTO = new StockTransferTO();
                index = Integer.parseInt(selectedIndex[i]);
                listTO.setItemId((itemIds[index]));
                listTO.setRcItemId(rcItemIds[index]);
                listTO.setPrice(prices[index]);
                List.add(listTO);
            }
            //path = "content/stockTransfer/rcItemReceived.jsp";
            int update = stocktransferBP.processUpdateRcItemsNew(List, stocktransferTO);


            //receivedItems = stocktransferBP.processTransferedItemList(gdId, stocktransferTO);
            //request.setAttribute("receivedItems", receivedItems);

            receivedStockListAll = stocktransferBP.processReceivedStockListAll(stocktransferTO, companyId);
            request.setAttribute("receivedStockListAll", receivedStockListAll);
            
            path = "content/stockTransfer/receiveStock.jsp";
            /*
            receivedRcItems = stocktransferBP.processTransferedRcItem(gdId, stocktransferTO);
            if (receivedRcItems.size() != 0) {
                request.setAttribute("receivedRcItems", receivedRcItems);
            }
            */



            request.setAttribute("servicePtId", stockTransferCommand.getServicePtId());
            request.setAttribute("gdId", gdId);
            request.setAttribute("FromServicePoint", stockTransferCommand.getCompanyName());
            request.setAttribute("TOServicePoint", stockTransferCommand.getServicePtName());
            if (update > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "RC Stock Added Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to Add data  --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

// Raja
    // Stock Transfer 
    public ModelAndView handleStPage(HttpServletRequest request, HttpServletResponse response, StockTransferCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        stockTransferCommand = command;
        HttpSession session = request.getSession();
        String menuPath = "Stores >> Stock Transfer ";
        String pageTitle = "STOCK TRANSFER";
        request.setAttribute("pageTitle", pageTitle);
        String path = "";
        int companyId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        ArrayList servicePointList = new ArrayList();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int companyType = 1012;
        try {
             setLocale(request, response);

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "StockTransfer-Request")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                path = "content/stockTransfer/generateStRequest.jsp";
                servicePointList = stocktransferBP.processServicePtList(companyType, companyId);
                request.setAttribute("servicePointList", servicePointList);
            }
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (FPBusinessException exception1) {
            System.out.println("Exception handling Failed2");
            FPLogUtils.fpErrorLog("Business exception --> " + exception1.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY, exception1.getErrorMessage());
        } catch (Exception exception2) {
            System.out.println("Exception handling Failed3");
            exception2.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to handleGenerateMpr --> " + exception2);
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleGenerateStRequest(HttpServletRequest request, HttpServletResponse response, StockTransferCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        System.out.println("request=" + request);
        stockTransferCommand = command;
        String menuPath = "Stores >> Stock Transfer ";
        String pageTitle = "STOCK TRANSFER";
        request.setAttribute("pageTitle", pageTitle);
        String path = "";
        ArrayList mprList = new ArrayList();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String toPoId = request.getParameter("toSpId");
        String reqDate = request.getParameter("reqDate");
        String reqType = request.getParameter("reqType");
        String desc = request.getParameter("desc");
        String[] itemIds = request.getParameterValues("itemIds");
        String[] reqQtys = request.getParameterValues("reqQtys");
        ArrayList requestList = new ArrayList();

        try { 
            setLocale(request, response);
            
            System.out.println("tospId =" + toPoId);
            path = "content/stockTransfer/requestList.jsp";
            HttpSession session = request.getSession();
            String companyId = (String) session.getAttribute("companyId");
            int userId = (Integer) session.getAttribute("userId");
            StockTransferTO purchaseTO = new StockTransferTO();
            purchaseTO.setFromSpId(companyId);
            purchaseTO.setToSpId(toPoId);
            purchaseTO.setDesc(desc);
            purchaseTO.setReqDate(reqDate);
            purchaseTO.setItemIds(itemIds);
            purchaseTO.setReqQuant(reqQtys);
            purchaseTO.setReqType(reqType);
            purchaseTO.setUserId(String.valueOf(userId));
            stocktransferBP.processGenerateStRequest(purchaseTO);
            purchaseTO = new StockTransferTO();
            purchaseTO.setCompanyId(companyId);
            requestList = stocktransferBP.processStRequestList(purchaseTO);
            System.out.println("size of list=" + requestList.size());
            request.setAttribute("requestList", requestList);
        } catch (FPRuntimeException exception) {
            System.out.println("Exception handling Failed1");
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (FPBusinessException exception1) {
            System.out.println("Exception handling Failed2");
            FPLogUtils.fpErrorLog("Business exception --> " + exception1.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY, exception1.getErrorMessage());
        } catch (Exception exception2) {
            System.out.println("Exception handling Failed3");
            exception2.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to handleGenerateStRequest --> " + exception2);
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleStRequestList(HttpServletRequest request, HttpServletResponse response, StockTransferCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        System.out.println("request=" + request);
        stockTransferCommand = command;
        String menuPath = "Stores >> Stock Transfer >> RequestList ";
        String pageTitle = "STOCK TRANSFER";
        request.setAttribute("pageTitle", pageTitle);
        String path = "";
        int companyType = 1012;
        ArrayList requestList = new ArrayList();
        ArrayList servicePointList = new ArrayList();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/stockTransfer/requestList.jsp";
            StockTransferTO purchaseTO = new StockTransferTO();
            requestList = stocktransferBP.processStRequestList(purchaseTO);
            request.setAttribute("requestList", requestList);
            servicePointList = stocktransferBP.processServicePtList(companyType);
            request.setAttribute("servicePointList", servicePointList);
        } catch (FPRuntimeException exception) {

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
//        } 
//            catch (FPBusinessException exception1) {            
//            System.out.println("Exception handling Failed2");
//            FPLogUtils.fpErrorLog("Business exception --> " + exception1.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,exception1.getErrorMessage());
        } catch (Exception exception2) {
            System.out.println("Exception handleStRequestList");
            exception2.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to handleStockRequestList --> " + exception2);
        }
        return new ModelAndView(path);
    }

//Hari
    public ModelAndView handleReqIdDetail(HttpServletRequest request, HttpServletResponse response, StockTransferCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        System.out.println("request=" + request);
        stockTransferCommand = command;
        String menuPath = "Stores >> Stock Transfer >> RequestList ";
        String pageTitle = "STOCK TRANSFER";
        request.setAttribute("pageTitle", pageTitle);
        String path = "";
        int companyType = 1012;
        int requestId = Integer.parseInt(request.getParameter("requestId"));
        ArrayList reqIdDetail = new ArrayList();
        ArrayList spList = new ArrayList();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/stockTransfer/reqIdDetail.jsp";
            StockTransferTO purchaseTO = new StockTransferTO();
            reqIdDetail = stocktransferBP.processReqIdDetail(requestId);
            request.setAttribute("reqIdDetail", reqIdDetail);

            spList = vehicleBP.processGetspList();
            request.setAttribute("operationPointList", spList);
        } catch (FPRuntimeException exception) {

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
//        }
//            catch (FPBusinessException exception1) {
//            System.out.println("Exception handling Failed2");
//            FPLogUtils.fpErrorLog("Business exception --> " + exception1.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,exception1.getErrorMessage());
        } catch (Exception exception2) {
            System.out.println("Exception handleStRequestList");
            exception2.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to handleStockRequestList --> " + exception2);
        }
        return new ModelAndView(path);
    }
}
