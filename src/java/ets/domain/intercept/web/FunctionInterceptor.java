/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.intercept.web;

import ets.domain.users.business.LoginBP;
import ets.domain.users.business.LoginTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 *
 * @author Arul
 */
public class FunctionInterceptor extends HandlerInterceptorAdapter {

    LoginBP loginBP;

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    private static final Logger logger = Logger.getLogger(FunctionInterceptor.class);

    //before the actual handler will be executed
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        boolean status = true;

        long startTime = System.currentTimeMillis();
        int roleId = 0;
        int userId = 0;
        int userIdStatus = 0;
        String roleNameStatus = "0";
        String[] methodName = null;
        request.setAttribute("startTime", startTime);
        System.out.println("startTime = " + startTime);
        System.out.println("request.getMethod() = " + request.getRequestURI());
        LoginTO loginTO = new LoginTO();
        methodName = request.getRequestURI().split("/");
        loginTO.setMethodName(methodName[2]);
        String sessionPageParam = "";
        String requestPageParam = "";
        if (!methodName[2].equals("login.do") && !methodName[2].equals("updateContractApproval.do") && !methodName[2].equals("updateContractApprovalVendor.do") && !methodName[2].equals("updateFuelPriceMaster.do")) {
            HttpSession session = request.getSession();
            sessionPageParam = (String)session.getAttribute("sessionPageParam");
            requestPageParam = request.getParameter("sessionPageParam");
            System.out.println("sessionPageParam:"+sessionPageParam);
            System.out.println("requestPageParam:"+requestPageParam);
            if(sessionPageParam !=null && requestPageParam !=null){//valid check
                requestPageParam = requestPageParam.split(",")[0];
                if(sessionPageParam.equals(requestPageParam)){//valid access
                    sessionPageParam = session.getId() + "" + System.currentTimeMillis();
                    System.out.println("sessionPageParam1:"+sessionPageParam);
                    session.setAttribute("sessionPageParam",sessionPageParam);
                }else {//invaid access
                    System.out.println(" invlaid access");
//                    session.invalidate();
                    response.sendRedirect("/throttle/logout.do");
                    status = true;
                }
            }
            /*
            String lastAccessTime = request.getParameter("lastAccessTime");
            if (lastAccessTime == null) {
                System.out.println("lastAccessTime first time = " + lastAccessTime);
                request.setAttribute("LastAccessTime", session.getId() + "" + System.currentTimeMillis());
                loginTO.setLastAccessTime(session.getId() + "" + System.currentTimeMillis());
                roleNameStatus = loginBP.checkRoleMenuStatus(loginTO);
            } else {
                System.out.println("lastAccessTime subsequence time = " + lastAccessTime);
                request.setAttribute("LastAccessTime", session.getId() + "" + System.currentTimeMillis());
                loginTO.setLastAccessTime(session.getId() + "" + System.currentTimeMillis());
                System.out.println("lastAccessTime subsequence time from to object = " + loginTO.getLastAccessTime());
                if (!loginTO.getLastAccessTime().equals(lastAccessTime)) {
                    roleNameStatus = loginBP.checkRoleMenuStatus(loginTO);
                } else {
                    response.sendRedirect("/throttle/logout.do");
                    status = true;
                }

            }
            System.out.println("roleNameStatus = " + roleNameStatus);
            if (roleNameStatus != null) {
                if (roleNameStatus.equals("0")) {
                    System.out.println("roleNameStatus = " + roleNameStatus);
                    response.sendRedirect("/throttle/invalidAccess.do");
                    status = false;
                } else {
                    status = true;
                }
            } else {
                status = true;
            }
            */

            status = true;
        } else {

            status = true;
        }
        return status;
    }

}
