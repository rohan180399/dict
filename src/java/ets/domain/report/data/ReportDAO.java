/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.report.data;

import com.ibatis.sqlmap.client.SqlMapClient;
import ets.arch.exception.FPBusinessException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import ets.domain.util.FPLogUtils;
import ets.arch.exception.FPRuntimeException;
import ets.domain.operation.business.OperationTO;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import ets.domain.report.business.ReportTO;
import ets.domain.trip.business.TripTO;
import ets.domain.report.business.DprTO;
import ets.domain.scheduler.business.SchedulerTO;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.io.*;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import org.json.simple.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author sabreesh
 */
public class ReportDAO extends SqlMapClientDaoSupport {

    private final int errorStatus = 4;
    private final static String CLASS = "ReportDAO";

    public ArrayList getBillList(ReportTO repTO, String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList mrsList = new ArrayList();
        String regNo = repTO.getRegNo();
        try {
//            map.put("companyId", mrsTO.getCompanyId());
            regNo = regNo.replace(" ", "");
            //////System.out.println("regNo=" + regNo);
            //////System.out.println("repTO.getCompanyName()=" + repTO.getCompanyName());
            //////System.out.println("repTO.getServicePointId()=" + repTO.getServicePointId());
            //////System.out.println("repTO.getServiceTypeId()=" + repTO.getServiceTypeId());
            //////System.out.println("repTO.getReportType()=" + repTO.getReportType());
            int reportType = repTO.getReportType();
            //////System.out.println("Report Type In DAO -->" + reportType);
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("regNo", regNo);
            map.put("reportPoint", repTO.getCompanyName());
            map.put("servicePointId", repTO.getServicePointId());
            map.put("serviceType", repTO.getServiceTypeId());
            map.put("customerType", repTO.getCustomerTypeId());
            if (reportType == 1) {

                mrsList = (ArrayList) getSqlMapClientTemplate().queryForList("report.jobCardBillList", map);

            } else {

                mrsList = (ArrayList) getSqlMapClientTemplate().queryForList("report.BillList", map);

            }
            //////System.out.println("getBillList size=" + mrsList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBillList", sqlException);
        }
        return mrsList;
    }

    public ArrayList getSalesBillTaxSummary(ReportTO repTO, String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList mrsList = new ArrayList();
        String regNo = repTO.getRegNo();
        try {
//            map.put("companyId", mrsTO.getCompanyId());
            int reportType = repTO.getReportType();
            //////System.out.println("Report Type In Sales Tax DAO -->" + reportType);
            regNo = regNo.replace(" ", "");
            //////System.out.println("regNo=" + regNo);
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("regNo", regNo);
            map.put("reportPoint", repTO.getCompanyName());
            map.put("serviceType", repTO.getServiceTypeId());
            map.put("servicePointId", repTO.getServicePointId());
            //////System.out.println("sp" + repTO.getServicePointId());
            //////System.out.println("op" + repTO.getCompanyName());
            //////System.out.println("fromDate" + fromDate);
            //////System.out.println("toDate" + toDate);
            if (reportType == 1) {
                ////////System.out.println("JobcardSale list exec Before");
                mrsList = (ArrayList) getSqlMapClientTemplate().queryForList("report.jobCardSalesTaxSummary", map);
                // //////System.out.println("JobcardSale list exec After");
            } else {

                // //////System.out.println("Bill DateSale list exec Before");
                mrsList = (ArrayList) getSqlMapClientTemplate().queryForList("report.salesTaxSummary", map);
                // //////System.out.println("Bill DateSale list exec After");
            }
            //////System.out.println("salesTaxSummary=" + mrsList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBillList", sqlException);
        }
        return mrsList;
    }

    public ArrayList getBillParticulars(ReportTO repTO, String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList mrsList = new ArrayList();
        String regNo = repTO.getRegNo();
        try {
//            map.put("companyId", mrsTO.getCompanyId());
            regNo = regNo.replace(" ", "");
            //////System.out.println("regNo=" + regNo);
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("regNo", regNo);
            map.put("reportPoint", repTO.getCompanyName());
            map.put("serviceType", repTO.getServiceTypeId());
            mrsList = (ArrayList) getSqlMapClientTemplate().queryForList("report.billList", map);
            //////System.out.println("getBillList size=" + mrsList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBillList", sqlException);
        }
        return mrsList;
    }

    public ArrayList getPurchaseList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList purchaseList = new ArrayList();
        try {
            //////System.out.println("repTO.getVendorId()" + repTO.getVendorId());
            //////System.out.println("repTO.getSectionId()" + repTO.getCategoryId());
            //////System.out.println("repTO.getPurType()" + repTO.getPurType());

            map.put("vendorId", repTO.getVendorId());
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("categoryId", repTO.getCategoryId());
            map.put("companyId", repTO.getCompanyId());
            map.put("mfrId", repTO.getMfrId());
            map.put("modelId", repTO.getModelId());
            map.put("purType", repTO.getPurType());
            map.put("poId", repTO.getPoId());
            map.put("itemName", repTO.getItemName());
            map.put("mfrCode", repTO.getMfrCode());
            map.put("paplCode", repTO.getPaplCode());
            purchaseList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getPurchaseReport", map);
            //////System.out.println("getpurchaseList Report size=" + purchaseList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "purchaseList", sqlException);
        }
        return purchaseList;
    }

    public ArrayList getComplaintList(ReportTO repTO, String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList complaintList = new ArrayList();
        try {
            map.put("vehicleNumber", repTO.getRegNo());
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("sectionId", repTO.getSectionId());
            map.put("problemId", repTO.getProblem());
            map.put("technicianId", repTO.getTechnicianId());
            System.out.println("repTO.getSectionId())" + repTO.getSectionId() + " repTO.getRegNo():" + repTO.getRegNo());
            System.out.println("fromDate " + fromDate + " toDate:" + toDate);
            complaintList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleComplaints", map);
            System.out.println("getVehicleComplaints size=" + complaintList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "VehicleComplaintList", sqlException);
        }
        return complaintList;
    }

    public ArrayList getStockWorthList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList stockWorthList = new ArrayList();
        try {

            map.put("fromDate", repTO.getFromDate());
            map.put("sectionId", repTO.getSectionId());
            map.put("companyId", repTO.getCompanyId());
            map.put("mfrId", repTO.getMfrId());
            map.put("modelId", repTO.getModelId());
            map.put("itemName", repTO.getItemName());
            map.put("mfrCode", repTO.getMfrCode());
            map.put("paplCode", repTO.getPaplCode());
            if ("New".equalsIgnoreCase(repTO.getItemType())) {
                stockWorthList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getStockWorthReportNew", map);
            } else {
                stockWorthList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getStockWorthReportRc", map);
            }

            //////System.out.println("stockWorthList size=" + stockWorthList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("stockWorthList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "stockWorthList", sqlException);
        }
        return stockWorthList;
    }

    public ArrayList getRateList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList rateList = new ArrayList();
        try {

            map.put("itemName", repTO.getItemName());
            map.put("processId", repTO.getProcessId());

            rateList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getRateList", map);
            //////System.out.println("rateList size=" + rateList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("rateList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "rateList", sqlException);
        }
        return rateList;
    }

    public ArrayList getPeriodicServiceList(int compId, String regNo, String fromDate) {
        Map map = new HashMap();
        ArrayList serviceList = new ArrayList();
        try {
            map.put("compId", compId);
            map.put("date", fromDate);
            map.put("regNo", regNo);

            serviceList = (ArrayList) getSqlMapClientTemplate().queryForList("report.periodicServiceList", map);
            //////System.out.println("rateList size=" + serviceList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPeriodicServiceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getPeriodicServiceList", sqlException);
        }
        return serviceList;
    }

    public ArrayList getbodyBillList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList bodyBillList = new ArrayList();
        try {
            map.put("companyId", repTO.getCompanyId());
            map.put("vendorId", repTO.getVendorId());
            map.put("jobCardId", repTO.getJobCardId());
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("woId", repTO.getWoId());
            map.put("regNo", repTO.getRegNo());
            bodyBillList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getbodyBillList", map);
            //////System.out.println("bodyBillList size=" + bodyBillList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("bodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getbodyBillList", sqlException);
        }
        return bodyBillList;
    }

    public ArrayList getContractorActivities(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList contractorActivities = new ArrayList();
        try {
            map.put("companyId", repTO.getCompanyId());
            map.put("vendorId", repTO.getVendorId());
            map.put("jobCardId", repTO.getJobCardId());
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("woId", repTO.getWoId());
            map.put("regNo", repTO.getRegNo());
            contractorActivities = (ArrayList) getSqlMapClientTemplate().queryForList("report.getContractorActivities", map);
            //////System.out.println("contractorActivities size=" + contractorActivities.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("bodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getbodyBillList", sqlException);
        }
        return contractorActivities;
    }

    public ArrayList getbodyBillDetails(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList bodyBillDetails = new ArrayList();
        try {
            map.put("vendorId", repTO.getVendorId());
            map.put("jobCardId", repTO.getJobCardId());
            bodyBillDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getbodyBillDetails", map);
            //////System.out.println("getbodyBillDetails size=" + bodyBillDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("bodyBillDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "bodyBillDetails", sqlException);
        }
        return bodyBillDetails;
    }

    public ArrayList getHikedbodyBillDetails(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList bodyBillDetails = new ArrayList();
        try {
            map.put("vendorId", repTO.getVendorId());
            map.put("jobCardId", repTO.getJobCardId());
            bodyBillDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getHikedbodyBillDetails", map);
            //////System.out.println("getbodyBillDetails size=" + bodyBillDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("bodyBillDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "HikedbodyBillDetails", sqlException);
        }
        return bodyBillDetails;
    }

    public ArrayList getBillDetails(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList BillDetails = new ArrayList();
        try {

            map.put("billId", repTO.getBillNo());
            map.put("jobCardId", repTO.getJobCardId());

            BillDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getBillDetails", map);
            //////System.out.println("BillDetails size=" + BillDetails);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("BillDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "BillDetails", sqlException);
        }
        return BillDetails;
    }

    public ArrayList getActivityList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList ActivityList = new ArrayList();
        try {

            map.put("billId", repTO.getBillNo());

            ActivityList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getActivityList", map);
            //////System.out.println("ActivityList size=" + ActivityList);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("BillDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "ActivityList", sqlException);
        }
        return ActivityList;
    }

    public ArrayList getTotalPrices(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList TotalPrices = new ArrayList();
        try {

            map.put("billId", repTO.getBillNo());

            TotalPrices = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTotalPrices", map);
            //////System.out.println("TotalPrices size=" + TotalPrices);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("TotalPrices Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "TotalPrices", sqlException);
        }
        return TotalPrices;
    }

    public ArrayList getStockIssueReport(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList issueList = new ArrayList();
        try {
            //////System.out.println("from date:" + repTO.getFromDate());
            //////System.out.println("to date:" + repTO.getToDate());
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("companyId", repTO.getCompanyId());
            map.put("categoryId", repTO.getCategoryId());
            map.put("mfrId", repTO.getMfrId());
            map.put("itemName", repTO.getItemName());
            map.put("mfrCode", repTO.getMfrCode());
            map.put("paplCode", repTO.getPaplCode());
            map.put("regNo", repTO.getRegNo());
            map.put("rcWorkorderId", repTO.getRcWorkorderId());
            map.put("counterId", repTO.getCounterId());

            //////System.out.println("categoryid" + repTO.getCategoryId());
            //////System.out.println("mfrid" + repTO.getMfrId());
            //////System.out.println("Company Id" + repTO.getCompanyId());
            //////System.out.println("Item Name" + repTO.getItemName());
            //////System.out.println("Mfr Code" + repTO.getMfrCode());
            //////System.out.println("PAPL Code" + repTO.getPaplCode());
            //////System.out.println("CounterId" + repTO.getCounterId());
            //////System.out.println("RcWorkorder" + repTO.getRcWorkorderId());
            issueList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getStockIssueReport", map);
            //////System.out.println("getStockIssueReport Report size=" + issueList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("StockIssueReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "StockIssueReport", sqlException);
        }
        return issueList;
    }

    public ArrayList getStoresEffReport(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList issueList = new ArrayList();
        try {
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("companyId", repTO.getCompanyId());
            map.put("categoryId", repTO.getCategoryId());
            //////System.out.println("categoryid" + repTO.getCategoryId());
            map.put("mfrId", repTO.getMfrId());
            map.put("modelId", repTO.getModelId());
            map.put("itemName", repTO.getItemName());
            map.put("mfrCode", repTO.getMfrCode());
            map.put("paplCode", repTO.getPaplCode());
            issueList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getStoresEffReport", map);
            //////System.out.println("getStoresEffReport size=" + issueList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("StoresEffReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "StoresEffReport", sqlException);
        }
        return issueList;
    }

    public ArrayList getReceivedStockReport(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList receivedList = new ArrayList();
        ReportTO tax = new ReportTO();
        try {
            map.put("vendorId", repTO.getVendorId());
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("companyId", repTO.getCompanyId());
            map.put("poId", repTO.getPoId());
            map.put("inVoiceId", repTO.getInvoiceId());
            map.put("billType", repTO.getBillType());
            //////System.out.println("repTO.getVendorId()=" + repTO.getVendorId());
            receivedList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getReceivedStockReport", map);
            //////System.out.println("getReceivedStockReport size=" + receivedList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ReceivedStockReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "ReceivedStockReport", sqlException);
        }
        return receivedList;
    }

    public ArrayList getReceivedStockTaxSummary(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList receivedList = new ArrayList();
        try {
            map.put("vendorId", repTO.getVendorId());
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("companyId", repTO.getCompanyId());
            map.put("poId", repTO.getPoId());
            map.put("inVoiceId", repTO.getInvoiceId());
            map.put("hikePercentage", repTO.getHikePercentage());
            map.put("billType", repTO.getBillType());
            //////System.out.println("tax in resources=" + repTO.getHikePercentage());
            receivedList = (ArrayList) getSqlMapClientTemplate().queryForList("report.taxSummary", map);
            //////System.out.println("getReceivedStockReport size=" + receivedList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ReceivedStockReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "ReceivedStockReport", sqlException);
        }
        return receivedList;
    }

    public ArrayList getInvoiceItems(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList InvoiceItems = new ArrayList();
        try {
            map.put("supplyId", repTO.getSupplyId());
            InvoiceItems = (ArrayList) getSqlMapClientTemplate().queryForList("report.getInvoiceReport", map);
            //////System.out.println("InvoiceItems size=" + InvoiceItems.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ReceivedStockReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "ReceivedStockReport", sqlException);
        }
        return InvoiceItems;
    }

    public ArrayList getRCWOItems(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList woDetail = new ArrayList();
        try {
            map.put("woId", repTO.getSupplyId());
            //////System.out.println(" repTO.getSupplyId()   " + repTO.getSupplyId());
            woDetail = (ArrayList) getSqlMapClientTemplate().queryForList("report.woDetail", map);
            //////System.out.println("woDetail size=" + woDetail.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ReceivedStockReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "woDetail", sqlException);
        }
        return woDetail;
    }

    public ArrayList getGdDetail(String gdId) {
        Map map = new HashMap();
        ArrayList reqList = new ArrayList();
        try {
            map.put("gdId", gdId);

            reqList = (ArrayList) getSqlMapClientTemplate().queryForList("report.gdDetail", map);
            //////System.out.println("getGdDetail size=" + reqList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getStDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getStDetail", sqlException);
        }
        return reqList;
    }

    public ArrayList getServiceSummary(String usageTypeId, String ServiceTypId, String custId, String compId, String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList InvoiceItems = new ArrayList();
        try {
            map.put("compId", compId);
            map.put("usageTypeId", usageTypeId);
            map.put("custId", custId);
            map.put("serviceTypeId", ServiceTypId);
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            InvoiceItems = (ArrayList) getSqlMapClientTemplate().queryForList("report.getServiceSummary", map);
            //////System.out.println("getServiceSummary size=" + InvoiceItems.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getServiceSummary Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getServiceSummary", sqlException);
        }
        return InvoiceItems;
    }

    public ArrayList getMovingAvgReport(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList issueList = new ArrayList();
        try {

            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("companyId", repTO.getCompanyId());
            map.put("categoryId", repTO.getCategoryId());
            //////System.out.println("categoryid" + repTO.getCategoryId());
            //////System.out.println("mfrid" + repTO.getMfrId());
            //////System.out.println("modelid" + repTO.getModelId());
            map.put("mfrId", repTO.getMfrId());
            map.put("modelId", repTO.getModelId());
            map.put("itemName", repTO.getItemName());
            map.put("mfrCode", repTO.getMfrCode());
            map.put("paplCode", repTO.getPaplCode());
            issueList = (ArrayList) getSqlMapClientTemplate().queryForList("report.movingAvgReport", map);
            //////System.out.println("getStockIssueReport Report size=" + issueList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("StockIssueReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "StockIssueReport", sqlException);
        }
        return issueList;
    }

    public ArrayList getSerEffReport(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList serEffList = new ArrayList();
        try {
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("companyId", repTO.getCompanyId());
            map.put("mfrId", repTO.getMfrId());
            map.put("modelId", repTO.getModelId());
            map.put("regNo", repTO.getRegNo());
            map.put("woId", repTO.getWoId());
            map.put("jobCardId", repTO.getJobCardId());
            serEffList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getSerEffReport", map);
            //////System.out.println("serEffList Report size=" + serEffList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("serEffList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "serEffList", sqlException);
        }
        return serEffList;
    }

    public ArrayList getStRequestList(String fromDate, String toDate, int fromId, int toId, String type) {
        Map map = new HashMap();
        ArrayList reqList = new ArrayList();
        try {
            //////System.out.println("type" + type);
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("fromId", fromId);
            map.put("toId", toId);
            //////System.out.println("fromId" + fromId);
            //////System.out.println("toId" + toId);
            //////System.out.println("fromDate" + fromDate);
            //////System.out.println("toDate" + toDate);
            if (type.equalsIgnoreCase("GR")) {
                reqList = (ArrayList) getSqlMapClientTemplate().queryForList("report.stockTransferList", map);
                //////System.out.println("stockTransferList GR Report size=" + reqList.size());
            } else {
                //////System.out.println("in else");
                reqList = (ArrayList) getSqlMapClientTemplate().queryForList("report.stockTransferGDList", map);
            }
            //////System.out.println("stockTransferList GD Report size=" + reqList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("serEffList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "reqList", sqlException);
        }
        return reqList;
    }

    public ArrayList getStDetail(String reqId) {
        Map map = new HashMap();
        ArrayList reqList = new ArrayList();
        try {
            map.put("requestId", reqId);

            reqList = (ArrayList) getSqlMapClientTemplate().queryForList("report.stockTransferDetails", map);
            //////System.out.println("stockTransferDetails size=" + reqList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getStDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getStDetail", sqlException);
        }
        return reqList;
    }

    public ArrayList getRcBillList(String vendorId, String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList reqList = new ArrayList();
        try {
            map.put("vendorId", vendorId);
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);

            reqList = (ArrayList) getSqlMapClientTemplate().queryForList("report.rcBillList", map);
            //////System.out.println("getRcBillList size=" + reqList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRcBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getRcBillList", sqlException);
        }
        return reqList;
    }

    public ArrayList getRcBillTaxSummary(String vendorId, String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList reqList = new ArrayList();
        ReportTO repTo = new ReportTO();
        try {
            map.put("vendorId", vendorId);
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("hikePercentage", repTo.getHikePercentage());

            reqList = (ArrayList) getSqlMapClientTemplate().queryForList("report.rcBillTaxSummary", map);
            //////System.out.println("getRcBillList size=" + reqList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRcBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getRcBillList", sqlException);
        }
        return reqList;
    }

    public ArrayList getRcBillDetail(String billId) {
        Map map = new HashMap();
        ArrayList reqList = new ArrayList();
        try {
            map.put("billId", billId);

            reqList = (ArrayList) getSqlMapClientTemplate().queryForList("report.rcBillDetail", map);
            //////System.out.println("rcBillDetail size=" + reqList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("rcBillDetail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "rcBillDetail", sqlException);
        }
        return reqList;
    }

    public ArrayList getServiceCostList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList serviceCostList = new ArrayList();
        try {
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("companyId", repTO.getCompanyId());
            map.put("regNo", repTO.getRegNo());
            map.put("usageTypeId", repTO.getUsageType());
            map.put("custId", repTO.getCustId());
            map.put("servicePointId", repTO.getServicePointId());
            System.out.println("from-->" + repTO.getFromDate() + "TO date-->" + repTO.getToDate()
                    + "companyId" + repTO.getCompanyId() + "regno-->" + repTO.getRegNo()
                    + "usage type-->" + repTO.getUsageType() + "custId-->" + repTO.getCustId()
                    + "service" + repTO.getServicePointId());
            serviceCostList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getServiceCostList", map);
            System.out.println("serviceCostList size=" + serviceCostList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("serviceCostList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "serviceCostList", sqlException);
        }
        return serviceCostList;
    }

    public ArrayList getWoList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList woList = new ArrayList();
        try {
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("companyId", repTO.getCompanyId());

            woList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getWoList", map);
            //////System.out.println("woList Report size=" + woList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("woList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "woList", sqlException);
        }
        return woList;
    }

    public ArrayList getServiceChartData(String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList woList = new ArrayList();
        try {
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            //////System.out.println(" repTO.getFromDate()=" + fromDate + "()" + toDate);
            woList = (ArrayList) getSqlMapClientTemplate().queryForList("report.serviceChartQry", map);
            //////System.out.println("serviceChartQry Report size=" + woList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getServiceChartData Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getServiceChartData", sqlException);
        }
        return woList;
    }

    public ArrayList handleServiceDailyMIS(String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList woList = new ArrayList();
        try {
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            System.out.println(" repTO.getFromDate()=" + fromDate + "()" + toDate);
            woList = (ArrayList) getSqlMapClientTemplate().queryForList("report.handleServiceDailyMIS", map);
            System.out.println("serviceChartQry Report size=" + woList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getServiceChartData Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getServiceChartData", sqlException);
        }
        return woList;
    }

    public ArrayList getWarrantyServiceList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList serviceList = new ArrayList();
        try {
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("companyId", repTO.getCompanyId());
            map.put("regNo", repTO.getRegNo());
            map.put("status", repTO.getStatus());
            serviceList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getWarrantyServiceList", map);
            //////System.out.println("serviceList size=" + serviceList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("serviceCostList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "WarrantyServiceList", sqlException);
        }
        return serviceList;
    }

    public ArrayList techniciansList() {
        Map map = new HashMap();
        ArrayList techniciansList = new ArrayList();

        try {

            techniciansList = (ArrayList) getSqlMapClientTemplate().queryForList("report.techniciansList", map);
            //////System.out.println("techniciansList size=" + techniciansList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("techniciansList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "techniciansList", sqlException);
        }
        return techniciansList;

    }

    public ArrayList getRcItemList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList RcItemList = new ArrayList();
        try {
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("companyId", repTO.getCompanyId());
            map.put("rcId", repTO.getRcId());
            map.put("itemName", repTO.getItemName());
            map.put("paplCode", repTO.getPaplCode());
            map.put("categoryId", repTO.getCategoryId());
            //////System.out.println("repTO.getCompanyId()" + repTO.getCompanyId());
            //////System.out.println("repTO.getCategoryId()" + repTO.getCategoryId());
            //////System.out.println("rcId" + repTO.getRcId());
            //////System.out.println("repTO.getPaplCode()" + repTO.getPaplCode());
            //////System.out.println("itemName" + repTO.getItemName());
            RcItemList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getRcItemList", map);
            //////System.out.println("RcItemList size=" + RcItemList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("RcItemList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "RcItemList", sqlException);
        }
        return RcItemList;
    }

    public ArrayList getRcHistoryList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList RcHistoryList = new ArrayList();
        try {
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("rcId", repTO.getRcId());
            RcHistoryList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getRcHistoryList", map);
            //////System.out.println("RcHistoryList size=" + RcHistoryList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("RcHistoryList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "RcHistoryList", sqlException);
        }
        return RcHistoryList;
    }

    public ArrayList getTyreList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList tyreList = new ArrayList();
        try {
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("tyreNo", repTO.getTyreNo());
            map.put("status", repTO.getStatus());
            //////System.out.println("repTO.getStatus()" + repTO.getStatus());
            tyreList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTyreList", map);
            //////System.out.println("tyreList size=" + tyreList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("tyreList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "tyreList", sqlException);
        }
        return tyreList;
    }

    public ArrayList getOrdersList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList tyreList = new ArrayList();
        try {
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("poId", repTO.getPoId());
            map.put("vendorId", repTO.getVendorId());
            map.put("companyId", repTO.getCompanyId());

            if (repTO.getOrderType().equals("PO")) {
                tyreList = (ArrayList) getSqlMapClientTemplate().queryForList("report.poList", map);
            } else {
                tyreList = (ArrayList) getSqlMapClientTemplate().queryForList("report.ordersList", map);
            }
            //////System.out.println("orders size=" + tyreList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrders Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getOrders", sqlException);
        }
        return tyreList;
    }

    public ArrayList getStockPurchase(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList InvoiceItems = new ArrayList();
        try {
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("vendorId", repTO.getVendorId());
            map.put("billType", repTO.getBillType());
            map.put("companyId", repTO.getCompanyId());
            map.put("purpose", repTO.getPurpose());
            InvoiceItems = (ArrayList) getSqlMapClientTemplate().queryForList("report.stockPurchaseReport", map);
            //////System.out.println("InvoiceItems size=" + InvoiceItems.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ReceivedStockReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "ReceivedStockReport", sqlException);
        }
        return InvoiceItems;
    }

    public ArrayList getRcBillsReport(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList InvoiceItems = new ArrayList();
        try {
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("vendorId", repTO.getVendorId());
            map.put("purpose", repTO.getPurpose());

            InvoiceItems = (ArrayList) getSqlMapClientTemplate().queryForList("report.rcBillsReport", map);
            //////System.out.println("InvoiceItems size=" + InvoiceItems.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ReceivedStockReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "ReceivedStockReport", sqlException);
        }
        return InvoiceItems;
    }

    public ArrayList getStatusReport(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList List = new ArrayList();
        try {

            map.put("regNo", repTO.getRegNo());
            String[] dat = repTO.getFromDate().split("-");

            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            //////System.out.println("fromDate" + repTO.getFromDate());
            //////System.out.println("toDate" + repTO.getToDate());
            List = (ArrayList) getSqlMapClientTemplate().queryForList("report.vehicleStatusList", map);
            //////System.out.println("vehicleStatusList size=" + List.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehicleStatusList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "vehicleStatusList", sqlException);
        }
        return List;
    }

    public ArrayList getPrevious(String jobCardId, String regNo) {
        Map map = new HashMap();
        ArrayList List = new ArrayList();
        try {

            map.put("jobCardId", jobCardId);
            map.put("regNo", regNo);

            List = (ArrayList) getSqlMapClientTemplate().queryForList("report.vehicleLastStatus", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehicleLastStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "vehicleLastStatus", sqlException);
        }
        return List;
    }

    public String getLastProblem(String jobCardId) {
        Map map = new HashMap();
        String problem = "";
        try {

            map.put("jobCardId", jobCardId);

            problem = (String) getSqlMapClientTemplate().queryForObject("report.vehicleLastProblem", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehicleLastStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "vehicleLastStatus", sqlException);
        }
        return problem;
    }

    public ArrayList getTaxwiseItems(ReportTO rep) {
        Map map = new HashMap();
        ArrayList itemList = new ArrayList();
        try {
            map.put("vendorId", rep.getVendorId());
            map.put("tax", rep.getTax());
            map.put("paplCode", rep.getPaplCode());
            map.put("itemName", rep.getItemName());

            itemList = (ArrayList) getSqlMapClientTemplate().queryForList("report.taxWiseItems", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehicleLastStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "vehicleLastStatus", sqlException);
        }
        return itemList;
    }

    public ArrayList getMfrVehComparisionReport(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList list = new ArrayList();
        try {

            String[] dat = repTO.getFromDate().split("-");
            String date = dat[2] + "-" + dat[1] + "-" + dat[0];
            map.put("date", date);
            list = (ArrayList) getSqlMapClientTemplate().queryForList("report.getMfrVehComparisionReport", map);
            //////System.out.println("getMfrVehComparisionReport" + list.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehComparisionReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehComparisionReport", sqlException);
        }
        return list;
    }

    public ArrayList getModelVehComparisionReport(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList list = new ArrayList();
        try {

            String[] dat = repTO.getFromDate().split("-");
            String date = dat[2] + "-" + dat[1] + "-" + dat[0];
            map.put("date", date);
            map.put("mfrId", repTO.getMfrId());
            //////System.out.println("repTO.getMfrId()" + repTO.getMfrId());
            list = (ArrayList) getSqlMapClientTemplate().queryForList("report.getModelVehComparisionReport", map);
            //////System.out.println("list" + list.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehComparisionReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehComparisionReport", sqlException);
        }
        return list;
    }

    public ArrayList getVehAgeComparisionReport(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList list = new ArrayList();
        try {

            String[] dat = repTO.getFromDate().split("-");
            String date = dat[2] + "-" + dat[1] + "-" + dat[0];
            map.put("date", date);
            map.put("mfrId", repTO.getMfrId());
            //////System.out.println("fromdate" + date);
            //////System.out.println("repTO.getMfrId()" + repTO.getMfrId());
            list = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehAgeComparisionReport", map);
            //////System.out.println("list" + list.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehComparisionReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehComparisionReport", sqlException);
        }
        return list;
    }

    public String getVehAgeCount(ReportTO repTO) {
        Map map = new HashMap();
        String count = "";
        try {

            String[] dat = repTO.getFromDate().split("-");
            String date = dat[2] + "-" + dat[1] + "-" + dat[0];
            map.put("date", date);
            map.put("mfrId", repTO.getMfrId());
            //////System.out.println("repTO.getMfrId()" + repTO.getMfrId());
            count = (String) getSqlMapClientTemplate().queryForObject("report.getVehAgeCount", map);
            //////System.out.println("count" + count);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehComparisionReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehComparisionReport", sqlException);
        }
        return count;
    }

    public String getMfrVehCount(String mfrId) {
        Map map = new HashMap();
        String count = "";
        try {

            map.put("mfrId", mfrId);

            count = (String) getSqlMapClientTemplate().queryForObject("report.getMfrVehCount", map);
            //////System.out.println("getMfrVehCount" + count);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMfrVehCount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getMfrVehCount", sqlException);
        }
        return count;
    }

    public String getModelVehCount(String mfrId, String modelId) {
        Map map = new HashMap();
        String count = "";
        try {

            map.put("mfrId", mfrId);
            map.put("modelId", modelId);
            //////System.out.println("mfrId" + mfrId);
            //////System.out.println("modelId" + modelId);
            count = (String) getSqlMapClientTemplate().queryForObject("report.getModelVehCount", map);
            //////System.out.println("getMfrVehCount" + count);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMfrVehCount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getModelVehCount", sqlException);
        }
        return count;
    }

    public ArrayList salesTrendGraph(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList salesTrendGraph = new ArrayList();
        try {

            String[] fdate = reportTO.getFromDate().split("-");
            String fromDate = fdate[2] + "-" + fdate[1] + "-" + fdate[0];
            map.put("fromDate", fromDate);

            String[] edate = reportTO.getToDate().split("-");
            String toDate = edate[2] + "-" + edate[1] + "-" + edate[0];
            map.put("toDate", toDate);

            salesTrendGraph = (ArrayList) getSqlMapClientTemplate().queryForList("report.salesTrendGraph", map);
            //////System.out.println("salesTrendGraph list" + salesTrendGraph.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehComparisionReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehComparisionReport", sqlException);
        }
        return salesTrendGraph;
    }

    public ArrayList vendorTrendGraph(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList vendorTrendGraph = new ArrayList();
        try {

            String[] fdate = reportTO.getFromDate().split("-");
            String fromDate = fdate[2] + "-" + fdate[1] + "-" + fdate[0];
            map.put("fromDate", fromDate);

            String[] edate = reportTO.getToDate().split("-");
            String toDate = edate[2] + "-" + edate[1] + "-" + edate[0];
            map.put("toDate", toDate);

            map.put("itemCode", reportTO.getItemCode());
            map.put("itemName", reportTO.getItemName());
            map.put("vendorName", reportTO.getVendorName());

            vendorTrendGraph = (ArrayList) getSqlMapClientTemplate().queryForList("report.vendorTrendGraph", map);
            //////System.out.println("vendorTrendGraph list" + vendorTrendGraph.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehComparisionReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehComparisionReport", sqlException);
        }
        return vendorTrendGraph;
    }

    public ArrayList mfr() {
        Map map = new HashMap();
        ArrayList mfr = new ArrayList();
        try {

            mfr = (ArrayList) getSqlMapClientTemplate().queryForList("report.mfr", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehComparisionReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehComparisionReport", sqlException);
        }
        return mfr;
    }

    public ArrayList usage() {
        Map map = new HashMap();
        ArrayList usage = new ArrayList();
        try {

            usage = (ArrayList) getSqlMapClientTemplate().queryForList("report.usage", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehComparisionReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehComparisionReport", sqlException);
        }
        return usage;
    }

    public ArrayList vehicleType() {
        Map map = new HashMap();
        ArrayList vehicleType = new ArrayList();
        try {

            vehicleType = (ArrayList) getSqlMapClientTemplate().queryForList("report.vehicleType", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehComparisionReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehComparisionReport", sqlException);
        }
        return vehicleType;
    }

    public String getItemSuggests(String vendorName) {
        Map map = new HashMap();
        map.put("vendorName", vendorName);
        String suggestions = "";
        ReportTO repTO = new ReportTO();
        try {
            ArrayList getItemList = new ArrayList();
            getItemList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getItemNames", map);
            Iterator itr = getItemList.iterator();
            while (itr.hasNext()) {
                repTO = new ReportTO();
                repTO = (ReportTO) itr.next();
                suggestions = repTO.getVendorName() + "~" + suggestions;
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getItemSuggests Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getItemSuggests", sqlException);
        }
        return suggestions;
    }

    public String getproblemSuggests(String problem) {
        Map map = new HashMap();
        map.put("problem", problem);
        String suggestions = "";
        ReportTO repTO = new ReportTO();
        try {
            ArrayList getItemList = new ArrayList();
            getItemList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getproblemNames", map);
            Iterator itr = getItemList.iterator();
            while (itr.hasNext()) {
                repTO = new ReportTO();
                repTO = (ReportTO) itr.next();
                suggestions = repTO.getProblemName() + "~" + suggestions;
                //////System.out.println("-----------------------" + suggestions);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getItemSuggests Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getItemSuggests", sqlException);
        }
        return suggestions;
    }

    public ArrayList problemDistributionGraph(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList problemDistributionGraph = new ArrayList();
        try {
            String mfrId = "";
            String usage = "";
            String vehicleType = "";
            String[] fdate = reportTO.getFromDate().split("-");
            String fromDate = fdate[2] + "-" + fdate[1] + "-" + fdate[0];
            map.put("fromDate", fromDate);

            String[] edate = reportTO.getToDate().split("-");
            String toDate = edate[2] + "-" + edate[1] + "-" + edate[0];
            map.put("toDate", toDate);
            if (!reportTO.getMfrId().equals("0")) {
                map.put("mfrId", reportTO.getMfrId());
            } else {
                map.put("mfrId", mfrId);
            }
            if (!reportTO.getUsage().equals("0")) {
                map.put("usage", reportTO.getUsage());
            } else {
                map.put("usage", usage);
            }
            if (!reportTO.getVehicleType().equals("0")) {
                map.put("vehicleType", reportTO.getVehicleType());
            } else {
                map.put("vehicleType", vehicleType);
            }

            problemDistributionGraph = (ArrayList) getSqlMapClientTemplate().queryForList("report.problemDistributionGraph", map);
            //////System.out.println("problemDistributionGraph list" + problemDistributionGraph.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehComparisionReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehComparisionReport", sqlException);
        }
        return problemDistributionGraph;
    }

    public ArrayList getColorList(int sizeValue) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        map.put("sizeValue", sizeValue);

        ArrayList colorList = new ArrayList();

        try {
            colorList = (ArrayList) getSqlMapClientTemplate().queryForList("report.colorList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehComparisionReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehComparisionReport", sqlException);
        }
        return colorList;
    }

    public ArrayList categoryRcReport(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList categoryRcReport = new ArrayList();
        try {

            String[] fdate = reportTO.getFromDate().split("-");
            String fromDate = fdate[2] + "-" + fdate[1] + "-" + fdate[0];
            map.put("fromDate", fromDate);

            String[] edate = reportTO.getToDate().split("-");
            String toDate = edate[2] + "-" + edate[1] + "-" + edate[0];
            map.put("toDate", toDate);

            categoryRcReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.categoryRcReport", map);
            //////System.out.println("categoryRcReport list" + categoryRcReport.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehComparisionReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehComparisionReport", sqlException);
        }
        return categoryRcReport;
    }

    public ArrayList categoryNewReport(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList categoryNewReport = new ArrayList();
        try {

            String[] fdate = reportTO.getFromDate().split("-");
            String fromDate = fdate[2] + "-" + fdate[1] + "-" + fdate[0];
            map.put("fromDate", fromDate);

            String[] edate = reportTO.getToDate().split("-");
            String toDate = edate[2] + "-" + edate[1] + "-" + edate[0];
            map.put("toDate", toDate);

            categoryNewReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.categoryNewReport", map);
            //////System.out.println("categoryNewReport list" + categoryNewReport.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehComparisionReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehComparisionReport", sqlException);
        }
        return categoryNewReport;
    }
    // shankar

    public ArrayList getActiveCategories() {
        Map map = new HashMap();
        ArrayList categories = new ArrayList();
        try {

            categories = (ArrayList) getSqlMapClientTemplate().queryForList("report.getActiveCategories", map);
            //////System.out.println("categories size in DAO is " + categories.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getStockWorthSearch Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getStockWorthSearch", sqlException);
        }
        return categories;
    }

    public ArrayList stockWorthGraph(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList stockWorthGraph = new ArrayList();
        try {

            String[] fdate = reportTO.getFromDate().split("-");
            String fromDate = fdate[2] + "-" + fdate[1] + "-" + fdate[0];
            map.put("fromDate", fromDate);
            String[] edate = reportTO.getToDate().split("-");
            String toDate = edate[2] + "-" + edate[1] + "-" + edate[0];
            map.put("toDate", toDate);
            map.put("categoryId", reportTO.getCategoryId());
            stockWorthGraph = (ArrayList) getSqlMapClientTemplate().queryForList("report.stockWorthGraph", map);
            //////System.out.println("stockWorthGraph size in DAO is : " + stockWorthGraph.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getstockWorthGraph Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getstockWorthGraph", sqlException);
        }
        return stockWorthGraph;
    }

    public ArrayList CompanyNameList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList companyNameList = new ArrayList();
        try {

            String[] fdate = reportTO.getFromDate().split("-");
            String fromDate = fdate[2] + "-" + fdate[1] + "-" + fdate[0];
            map.put("fromDate", fromDate);
            String[] edate = reportTO.getToDate().split("-");
            String toDate = edate[2] + "-" + edate[1] + "-" + edate[0];
            map.put("toDate", toDate);
            map.put("categoryId", reportTO.getCategoryId());
            companyNameList = (ArrayList) getSqlMapClientTemplate().queryForList("report.CompanyNameList", map);
            //////System.out.println("stockWorthGraph size in DAO is : " + companyNameList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getstockWorthGraph Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getstockWorthGraph", sqlException);
        }
        return companyNameList;
    }

    public ArrayList monthNameList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList monthNameList = new ArrayList();
        try {

            String[] fdate = reportTO.getFromDate().split("-");
            String fromDate = fdate[2] + "-" + fdate[1] + "-" + fdate[0];
            map.put("fromDate", fromDate);
            String[] edate = reportTO.getToDate().split("-");
            String toDate = edate[2] + "-" + edate[1] + "-" + edate[0];
            map.put("toDate", toDate);
            map.put("categoryId", reportTO.getCategoryId());
            monthNameList = (ArrayList) getSqlMapClientTemplate().queryForList("report.monthNameList", map);
            //////System.out.println("stockWorthGraph size in DAO is : " + monthNameList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getstockWorthGraph Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getstockWorthGraph", sqlException);
        }
        return monthNameList;
    }

    public ArrayList getManufacturerList() {
        Map map = new HashMap();
        ArrayList manufacturerList = new ArrayList();
        try {

            manufacturerList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getManufacturerList", map);
            //////System.out.println("manufacturerList size in DAO is " + manufacturerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getManufacturerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getManufacturerList", sqlException);
        }
        return manufacturerList;
    }

    public ArrayList getUsageTypeList() {
        Map map = new HashMap();
        ArrayList usageTypeList = new ArrayList();
        try {

            usageTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getUsageTypeList", map);
            //////System.out.println("UsageTypeList size in DAO is " + usageTypeList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUsageTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getUsageTypeList", sqlException);
        }
        return usageTypeList;
    }

    public ArrayList getVehicleTypeList() {
        Map map = new HashMap();
        ArrayList vehicleTypeList = new ArrayList();
        try {

            vehicleTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleTypeList", map);
            //////System.out.println("vehicleTypeList size in DAO is " + vehicleTypeList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleTypeList", sqlException);
        }
        return vehicleTypeList;
    }

    public ArrayList processCategoryStRequestList(String fromDate, String toDate, int fromId, int toId) {
        Map map = new HashMap();
        ArrayList reqList = new ArrayList();
        try {

            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("fromId", fromId);
            map.put("toId", toId);
            //////System.out.println("fromId" + fromId);
            //////System.out.println("toId" + toId);
            //////System.out.println("fromDate" + fromDate);
            //////System.out.println("toDate" + toDate);
//      if(type.equalsIgnoreCase("GR")){
//            reqList = (ArrayList) getSqlMapClientTemplate().queryForList("report.stockTransferList", map);
//             //////System.out.println("stockTransferList GR Report size=" + reqList.size());
//      }else{
            //////System.out.println("in else");
            reqList = (ArrayList) getSqlMapClientTemplate().queryForList("report.stockTransferCategoryGDList", map);
            //}
            //////System.out.println("stockTransferList GD Report size=" + reqList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("serEffList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "reqList", sqlException);
        }
        return reqList;
    }

    // end shankar
    public ArrayList processTaxwiseServiceCostList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList serviceCostList = new ArrayList();
        try {
            int type = repTO.getReportType();
            //////System.out.println("Report Type In Tax-->" + type);
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("regNo", repTO.getRegNo());
            map.put("custId", repTO.getCustId());
            if (type == 1) {

                serviceCostList = (ArrayList) getSqlMapClientTemplate().queryForList("report.processTaxwiseServiceCostList", map);

            } else {

                serviceCostList = (ArrayList) getSqlMapClientTemplate().queryForList("report.processTaxwiseServiceCostLists", map);

            }
            //////System.out.println("tax wise serviceCostList size=" + serviceCostList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("processTaxwiseServiceCostList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "processTaxwiseServiceCostList", sqlException);
        }
        return serviceCostList;
    }

    public String getVatPercentages() {
        Map map = new HashMap();
        String vat = "";
        try {
            vat = (String) getSqlMapClientTemplate().queryForObject("report.getVatPercentages", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVatPercentages Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVatPercentages", sqlException);
        }
        return vat;
    }

    public float getVatTotalAmount(String[] billNo, String vatPer) {
        Map map = new HashMap();
        String vat = "";
        float vatValue = 0.0f;

        try {
            map.put("billNo", billNo);
            map.put("vatPer", vatPer);
            vat = (String) getSqlMapClientTemplate().queryForObject("report.getVatTotalAmount", map);
            if (vat != null) {
                vatValue = Float.parseFloat(vat);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVatPercentages Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVatPercentages", sqlException);
        }
        return vatValue;
    }

    public float getContractAmount(String[] billNo) {
        Map map = new HashMap();
        String vat = "";
        float vatValue = 0.0f;
        for (int i = 0; i < billNo.length; i++) {
            //////System.out.println("val" + billNo[i]);
        }
        try {
            map.put("billNo", billNo);
            vat = (String) getSqlMapClientTemplate().queryForObject("report.getContractAmount", map);
            if (vat != null) {
                vatValue = Float.parseFloat(vat);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVatPercentages Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVatPercentages", sqlException);
        }
        return vatValue;
    }

    public ArrayList processTaxwisePO(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList serviceCostList = new ArrayList();

        try {
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("VendorId", repTO.getCustId());

            serviceCostList = (ArrayList) getSqlMapClientTemplate().queryForList("report.processTaxwisePO", map);
            //////System.out.println("tax wise serviceCostList size=" + serviceCostList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("processTaxwiseServiceCostList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "processTaxwiseServiceCostList", sqlException);
        }
        return serviceCostList;
    }

    public float getPOVatTotalAmount(String supplyId, String vatPer) {
        Map map = new HashMap();
        String vat = "";
        float vatValue = 0.0f;

        try {
            map.put("supplyId", supplyId);
            map.put("vatPer", vatPer);
            vat = (String) getSqlMapClientTemplate().queryForObject("report.getPOVatTotalAmount", map);
            if (vat != null) {
                vatValue = Float.parseFloat(vat);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVatPercentages Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getPOVatTotalAmount", sqlException);
        }
        return vatValue;
    }

    public ArrayList gettopProblem(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList gettopProblem = new ArrayList();
        try {
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("mfrId", Integer.parseInt(repTO.getMfrId()));
            map.put("usageTypeId", repTO.getUsageTypeId());
            map.put("age", Integer.parseInt(repTO.getAge()));

            gettopProblem = (ArrayList) getSqlMapClientTemplate().queryForList("report.gettopProblem", map);
            //////System.out.println("gettopProblem" + gettopProblem.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("gettopProblem Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "gettopProblem", sqlException);
        }
        return gettopProblem;
    }

    //Hari
    public ArrayList getRcExpenseData(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList rcExpenseSummary = new ArrayList();
        try {
            //////System.out.println("In getRcExpenseData DAO");
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("vendorId", repTO.getVendorId());

            rcExpenseSummary = (ArrayList) getSqlMapClientTemplate().queryForList("report.getRcExpenseData", map);
            //////System.out.println("rcExpenseSummary" + rcExpenseSummary.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRcExpenseData Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getRcExpenseData", sqlException);
        }
        return rcExpenseSummary;
    }

    public ArrayList getColourList() {
        Map map = new HashMap();
        ArrayList colourList = new ArrayList();
        try {
            //////System.out.println("In getColourList DAO");

            colourList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getColourList", map);
            //////System.out.println("colourList Report size=" + colourList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getColourList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getColourList", sqlException);
        }
        return colourList;
    }

    public ArrayList getScrapGraphData(String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList scrapValue = new ArrayList();
        try {
            //////System.out.println("In getScrapGraphData DAO");
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            //////System.out.println(" repTO.getFromDate()=" + fromDate + "()" + toDate);
            scrapValue = (ArrayList) getSqlMapClientTemplate().queryForList("report.getScrapGraphData", map);
            //////System.out.println("scrapValue Report size=" + scrapValue.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getScrapGraphData Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getScrapGraphData", sqlException);
        }
        return scrapValue;
    }

    public ArrayList getContractVendor(int vendorId) {
        Map map = new HashMap();
        ArrayList contractVendor = new ArrayList();
        try {
            //////System.out.println("In getContractVendor DAO");
            map.put("vendorId", vendorId);

            //////System.out.println(" repTO.vendorId()=" + vendorId);
            contractVendor = (ArrayList) getSqlMapClientTemplate().queryForList("report.getContractVendor", map);
            //////System.out.println("contractVendor Report size=" + contractVendor.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractVendor Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getContractVendor", sqlException);
        }
        return contractVendor;
    }

    public ArrayList getExternalLabourBillGraphData(String fromDate, String toDate, int vendorId) {
        Map map = new HashMap();
        ArrayList externalLabour = new ArrayList();
        try {
            //////System.out.println("In getExternalLabourBillGraphData DAO");
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("vendorId", vendorId);
            //////System.out.println(" repTO.getFromDate()=" + fromDate + "()" + toDate);
            externalLabour = (ArrayList) getSqlMapClientTemplate().queryForList("report.getExternalLabourBillGraphData", map);
            //////System.out.println("externalLabour Report size=" + externalLabour.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getExternalLabourBillGraphData Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getExternalLabourBillGraphData", sqlException);
        }
        return externalLabour;
    }

    public ArrayList getRcTrendGraphData(String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList rcServiceData = new ArrayList();
        try {
            //////System.out.println("In getRcTrendGraphData DAO");
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);

            //////System.out.println(" repTO.getFromDate()=" + fromDate + "()" + toDate);
            rcServiceData = (ArrayList) getSqlMapClientTemplate().queryForList("report.getRcTrendGraphData", map);
            //////System.out.println("rcServiceData Report size=" + rcServiceData.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRcTrendGraphData Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getRcTrendGraphData", sqlException);
        }
        return rcServiceData;
    }

    public ArrayList getVehicleServiceGraphData(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList vehicleServiceData = new ArrayList();
        try {
            //////System.out.println("In getVehicleServiceGraphData DAO");
            //////System.out.println("reportTO.getFromDate()" + reportTO.getFromDate() + "-"
//                    + reportTO.getToDate() + "-" + reportTO.getMfrid() + "-"
//                    + reportTO.getUsageid() + "-" + reportTO.getTypeId()
//                    + "-" + reportTO.getLife());
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            map.put("mfrId", reportTO.getMfrid());
            map.put("usageType", reportTO.getUsageid());
            map.put("vehicleType", reportTO.getTypeId());
            map.put("year", reportTO.getLife());

            if (reportTO.getMfrid() != 0 && reportTO.getUsageid() == 0 && reportTO.getTypeId() == 0 && reportTO.getLife() == 0) {
                //////System.out.println("Mfr Value is Only given Here");
                vehicleServiceData = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleModelServiceGraphData", map);
            } else {
                //////System.out.println("Mfr Value is O Here anything to be executed");
                vehicleServiceData = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleServiceGraphData", map);
            }

            //////System.out.println("vehicleServiceData Report size=" + vehicleServiceData.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleServiceGraphData Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleServiceGraphData", sqlException);
        }
        return vehicleServiceData;
    }

    public ArrayList getMileageGraphData(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList mileageData = new ArrayList();
        try {
            //////System.out.println("In getMileageGraphData DAO");
            System.out.println(
                    reportTO.getToDate() + "-" + reportTO.getMfrid() + "-"
                    + reportTO.getUsageid() + "-" + reportTO.getTypeId()
                    + "-" + reportTO.getLife());

            map.put("toDate", reportTO.getToDate());
            map.put("mfrId", reportTO.getMfrid());
            map.put("usageType", reportTO.getUsageid());
            map.put("vehicleType", reportTO.getTypeId());
            map.put("year", reportTO.getLife());

            //  if(reportTO.getMfrid()!=0 && reportTO.getUsageid()==0 && reportTO.getTypeId()==0 && reportTO.getLife()==0)
            {
                //////System.out.println("Mfr Value is Only given Here");
                mileageData = (ArrayList) getSqlMapClientTemplate().queryForList("report.getMileageGraphData", map);
            }
            //else
            {
                //   //////System.out.println("Mfr Value is O Here anything to be executed");
                // mileageData = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleServiceGraphData", map);
            }

            //////System.out.println("mileageData Report size=" + mileageData.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleServiceGraphData Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleServiceGraphData", sqlException);
        }
        return mileageData;
    }

    public ArrayList getRcVendorList() {
        Map map = new HashMap();
        ArrayList rcVendorList = new ArrayList();
        try {
            //////System.out.println("In getRcVendorList DAO");
            rcVendorList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getRcVendorList", map);
            //////System.out.println("getRcVendorList  size=" + rcVendorList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRcVendorList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getRcVendorList", sqlException);
        }
        return rcVendorList;
    }

    public ArrayList getRcExpenseAmount(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList rcItemIdList = new ArrayList();
        try {
            //////System.out.println("In getRcExpenseAmount DAO");
            System.out.println(
                    reportTO.getToDate() + "-" + reportTO.getFromDate() + "-"
                    + reportTO.getVendorId());
            int vendorId = Integer.valueOf(reportTO.getVendorId()).intValue();
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("vendorId", vendorId);

            rcItemIdList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getRcExpenseAmount", map);
            //////System.out.println("getRcExpenseAmount  size=" + rcItemIdList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRcExpenseItemList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getRcExpenseItemList", sqlException);
        }
        return rcItemIdList;
    }

    public Float getActualPrice(ReportTO reportTO) {
        Map map = new HashMap();
        Float actualPrice = 0.0f;
        Float price = 0.0f;
        int count = 0;
        try {
            //////System.out.println("In getActualPrice DAO");

            map.put("itemId", reportTO.getItemId());
            //////System.out.println("reportTO.getItemId()-->" + reportTO.getItemId());
            map.put("createdDate", reportTO.getCreatedDate());
            //////System.out.println("reportTO.getCreatedDate()-->" + reportTO.getCreatedDate());

            count = (Integer) getSqlMapClientTemplate().queryForObject("report.getItemCount", map);
            //////System.out.println("Count Of Items-->" + count);
            map.put("count", count);
            if (getSqlMapClientTemplate().queryForObject("report.getActualPrice", map) != null) {
                price = (Float) getSqlMapClientTemplate().queryForObject("report.getActualPrice", map);
            } else {
                price = 0.0f;
            }
            actualPrice = price * count;
            //////System.out.println("getActualPrice  size=" + actualPrice);

            //////System.out.println("Normal Exit");
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getActualPrice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getActualPrice", sqlException);
        }
        return actualPrice;
    }

    public int getItemId(String paplCode) {
        int itemId = 0;
        Map map = new HashMap();
        map.put("paplCode", paplCode);
        //////System.out.println("B4 Query" + paplCode);
        itemId = (Integer) getSqlMapClientTemplate().queryForObject("report.getItemId", map);
        //////System.out.println("A4 Query");
        //////System.out.println("Item_id-->" + itemId);
        return itemId;
    }

//bala
    public ArrayList getUsageTypewiseData(ReportTO report) {
        Map map = new HashMap();
        ArrayList usageList = new ArrayList();
        try {
            map.put("fromDate", report.getFromDate());
            map.put("toDate", report.getToDate());
            map.put("companyId", report.getCompanyId());
            //////System.out.println("From Date" + report.getFromDate() + "To Date" + report.getToDate() + "CompanyId" + report.getCompanyId());
            usageList = (ArrayList) getSqlMapClientTemplate().queryForList("report.usageTypewiseChartQry", map);
            //////System.out.println("UsageTypewiseData Report size=" + usageList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUsageTypewiseData Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getUsageTypewiseData", sqlException);
        }
        return usageList;
    }

//    public ArrayList getServiceTypewiseData(ReportTO report) {
//            Map map = new HashMap();
//            ArrayList serviceList = new ArrayList();
//            ArrayList serviceTypeListAll = new ArrayList();
//            ArrayList usageTypeName = new ArrayList();
//            ArrayList serviceType = null;
//            ArrayList mainServiceList=null;
//            try {
//                map.put("fromDate", report.getFromDate());
//                map.put("toDate", report.getToDate());
//                map.put("companyId", report.getCompanyId());
//                //////System.out.println("From Date"+report.getFromDate()+"To Date"+report.getToDate()+"CompanyId"+report.getCompanyId());
//                serviceType= (ArrayList) getSqlMapClientTemplate().queryForList("report.getServiceTypeList", map);
//                usageTypeName= (ArrayList) getSqlMapClientTemplate().queryForList("report.getUsageTypeName", map);
//                //////System.out.println("serviceType size="+serviceType.size());
//                //////System.out.println("usageType size="+usageTypeName.size());
//                Iterator itr=serviceType.iterator();
//                Iterator itr1=null;
//                while(itr.hasNext())
//                {
//                    report=new ReportTO();
//                    report=(ReportTO) itr.next();
//                    map.put("servieTypeId",report.getServiceTypeId());
//                    //////System.out.println("servieTypeId"+report.getServiceTypeId());
//                    itr1=usageTypeName.iterator();
//                    while(itr1.hasNext())
//                    {
//                    report=new ReportTO();
//                    report=(ReportTO) itr1.next();
//                    map.put("usageTypeId",report.getUsageTypeId());
//                    //////System.out.println("usageTypeId"+report.getUsageTypeId());
//                    serviceList = new ArrayList();
//                    serviceList = (ArrayList) getSqlMapClientTemplate().queryForList("report.serviceTypewiseChartQry", map);
//                    //////System.out.println("UsageTypewiseData serviceList size=" + serviceList.size());
//                    //////System.out.println("UsageTypewiseData serviceList Data=" + serviceList);
//                    serviceTypeListAll.addAll(serviceList);
//                    report.setServiceTypeListAll(serviceTypeListAll);
//                    //////System.out.println("serviceTypeListAll size=" + serviceTypeListAll.size());
//                    }
//                    //////System.out.println("serviceTypeListAll size="+serviceTypeListAll.size());
//                    mainServiceList = new ArrayList();
//                    mainServiceList.addAll(serviceTypeListAll);
//                    //////System.out.println("mainServiceList="+mainServiceList.size());
//                }
//                //////System.out.println("mainServiceList Size"+mainServiceList.size());
//               } catch (Exception sqlException) {
//                /*
//                 * Log the exception and propagate to the calling class
//                 */
//                FPLogUtils.fpDebugLog("getUsageTypewiseData Error" + sqlException.toString());
//                FPLogUtils.fpErrorLog("sqlException" + sqlException);
//                throw new FPRuntimeException("EM-GEN-01", CLASS, "getServiceTypewiseData", sqlException);
//            }
//            return mainServiceList;
//        }
// bala ends
    public ArrayList getTallyXMLSummary(int companyId, String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList tallyXMLSummary = new ArrayList();
        try {
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("companyId", Integer.valueOf(companyId));
            //////System.out.println("From Date" + fromDate + "To Date" + toDate + "CompanyId" + Integer.valueOf(companyId));
            tallyXMLSummary = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTallyXMLDetails", map);
            //////System.out.println("tallyXMLSummary Report size=" + tallyXMLSummary.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTallyXMLSummary Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTallyXMLSummary", sqlException);
        }
        return tallyXMLSummary;
    }

    public int getTallyXMLReport(ReportTO report) {
        Map map = new HashMap();
        int tallyXMLId = 0;
        try {
            map.put("fromDate", report.getFromDate());
            map.put("toDate", report.getToDate());
            map.put("reqDate", report.getRequiredDate());
            map.put("dataType", report.getDataType());
            map.put("companyId", report.getCompanyId());
            map.put("activeInd", "Y");
            //////System.out.println("From Date" + report.getFromDate() + "To Date" + report.getToDate() + "ReqDate" + report.getRequiredDate() + "CompanyId" + report.getCompanyId() + "DataType" + report.getDataType());
            //////System.out.println("insertStatus B4--->" + tallyXMLId);
            tallyXMLId = Integer.valueOf(getSqlMapClientTemplate().update("report.getTallyXMLId", map)).intValue();
            //////System.out.println("insertStatus B4--->" + tallyXMLId);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTallyXMLId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTallyXMLId", sqlException);
        }
        return tallyXMLId;
    }

    public ArrayList getModifyTallyXMLPage(String xmlId) {
        Map map = new HashMap();
        ArrayList modifyTallyXMLPage = new ArrayList();
        try {
            map.put("xmlId", xmlId);
            //////System.out.println("xmlId" + xmlId);
            modifyTallyXMLPage = (ArrayList) getSqlMapClientTemplate().queryForList("report.getModifyTallyXMLPage", map);
            //////System.out.println("modifyTallyXMLPage Report size=" + modifyTallyXMLPage.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getModifyTallyXMLPage Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getModifyTallyXMLPage", sqlException);
        }
        return modifyTallyXMLPage;
    }

    public int getModifyTallyXMLReport(ReportTO report, String xmlId) {
        Map map = new HashMap();
        int modifyTallyXMLId = 0;
        try {
            map.put("fromDate", report.getFromDate());
            map.put("toDate", report.getToDate());
            map.put("reqDate", report.getRequiredDate());
            map.put("dataType", report.getDataType());
            map.put("xmlId", xmlId);
            //////System.out.println("From Date" + report.getFromDate() + "To Date" + report.getToDate() + "ReqDate" + report.getRequiredDate() + "DataType" + report.getDataType());
            //////System.out.println("insertStatus B4--->" + modifyTallyXMLId);
            modifyTallyXMLId = Integer.valueOf(getSqlMapClientTemplate().update("report.getModifyTallyXMLId", map)).intValue();
            //////System.out.println("insertStatus B4--->" + modifyTallyXMLId);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getModifyTallyXMLId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getModifyTallyXMLId", sqlException);
        }
        return modifyTallyXMLId;
    }

    public String handleDriverSettlement(String driName) {
        Map map = new HashMap();
        map.put("driName", driName);
        String suggestions = "";
        ReportTO repTO = new ReportTO();
        try {
            ArrayList driverNames = new ArrayList();
            //////System.out.println("map = " + map);
            driverNames = (ArrayList) getSqlMapClientTemplate().queryForList("report.getDriverNames", map);
            Iterator itr = driverNames.iterator();
            while (itr.hasNext()) {
                repTO = new ReportTO();
                repTO = (ReportTO) itr.next();
                suggestions = repTO.getDriName() + "~" + suggestions;
            }
            if ("".equals(suggestions)) {
                suggestions = "no match found";
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverNames Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getDriverNames", sqlException);
        }
        return suggestions;
    }

    public String handleVehicleNo(String regno) {
        Map map = new HashMap();
        map.put("regno", regno);
        String suggestions = "";
        ReportTO repTO = new ReportTO();
        try {
            ArrayList vehicleNo = new ArrayList();
            //////System.out.println("map = " + map);
            vehicleNo = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleNo", map);
            Iterator itr = vehicleNo.iterator();
            while (itr.hasNext()) {
                repTO = new ReportTO();
                repTO = (ReportTO) itr.next();
                suggestions = repTO.getRegisterNo() + "~" + suggestions;
            }
            if ("".equals(suggestions)) {
                suggestions = "no match found";
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleNo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehicleNo", sqlException);
        }
        return suggestions;
    }

    public ArrayList getDriverSettlementReport(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList driverSettlementReport = new ArrayList();
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            String driName = "%" + reportTO.getDriName() + "%";
            map.put("driName", driName);
            map.put("registerNo", reportTO.getRegisterNo());
            //////System.out.println("map = " + map);
            driverSettlementReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.getDriverSettlementReport", map);
            //////System.out.println("driverSettlementReport=" + driverSettlementReport.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverSettlementReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getDriverSettlementReport", sqlException);
        }
        return driverSettlementReport;
    }

    //Rathimeena Code Start
    public ArrayList searchProDriverSettlement(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList proDriverSettlement = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);

        //////System.out.println("searchProDriverSettlement....: " + map);
        //////System.out.println("fromDate" + fromDate);
        //////System.out.println("toDate" + toDate);
        try {
            if (getSqlMapClientTemplate().queryForList("report.proDriverSettlement", map) != null) {
                proDriverSettlement = (ArrayList) getSqlMapClientTemplate().queryForList("report.proDriverSettlement", map);
            }
            //////System.out.println("searchProDriverSettlement size=" + proDriverSettlement.size());
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("searchProDriverSettlement Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "searchProDriverSettlement", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("searchProDriverSettlement Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "searchProDriverSettlement", sqlException);
        }
        return proDriverSettlement;
    }

    public ArrayList cleanerTripDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList cleanerTrip = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        try {
            if (getSqlMapClientTemplate().queryForList("report.cleanerTripDetails", map) != null) {
                cleanerTrip = (ArrayList) getSqlMapClientTemplate().queryForList("report.cleanerTripDetails", map);
            }
            //////System.out.println("searchProDriverSettlement size=" + cleanerTrip.size());
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("cleanerTripDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "cleanerTripDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("cleanerTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "cleanerTripDetails", sqlException);
        }
        return cleanerTrip;
    }

    public ArrayList getFixedExpDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList fixedExpDetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);

        try {
            if (getSqlMapClientTemplate().queryForList("report.getFixedExpDetails", map) != null) {
                fixedExpDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getFixedExpDetails", map);
                //////System.out.println("fixedExpDetails.size().. DAO : " + fixedExpDetails.size());
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getAdvDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getAdvDetails", sqlException);
        }
        return fixedExpDetails;
    }

    public ArrayList getDriverExpDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList driverExpDetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        try {
            if (getSqlMapClientTemplate().queryForList("report.getDriverExpDetails", map) != null) {
                driverExpDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getDriverExpDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getAdvDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getAdvDetails", sqlException);
        }
        return driverExpDetails;
    }

    public ArrayList getAdvDetails(String fromDate, String toDate, String regNo, int driId, String tripIds) {
        Map map = new HashMap();
        ArrayList AdvDetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        try {
            if (getSqlMapClientTemplate().queryForList("report.getAdvDetails", map) != null) {
                AdvDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getAdvDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getAdvDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getAdvDetails", sqlException);
        }
        return AdvDetails;
    }

    public ArrayList getFuelDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList fuelDetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        try {
            /*if (getSqlMapClientTemplate().queryForList("operation.getAdvDetails", map) != null) {
             fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getFuelDetails", map);
             }*/
            if (getSqlMapClientTemplate().queryForList("report.getFuelsDetail", map) != null) {
                fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getFuelsDetail", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFuelDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFuelDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFuelDetails", sqlException);
        }
        return fuelDetails;
    }

    public ArrayList getHaltDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList fuelDetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        try {
            /*if (getSqlMapClientTemplate().queryForList("operation.getAdvDetails", map) != null) {
             fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getFuelDetails", map);
             }*/
            if (getSqlMapClientTemplate().queryForList("report.getHaltDetails", map) != null) {
                fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getHaltDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return fuelDetails;
    }

    public ArrayList getRemarkDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList fuelDetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        try {
            /*if (getSqlMapClientTemplate().queryForList("operation.getAdvDetails", map) != null) {
             fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getFuelDetails", map);
             }*/
            if (getSqlMapClientTemplate().queryForList("report.getRemarkDetails", map) != null) {
                fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getRemarkDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRemarkDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRemarkDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRemarkDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRemarkDetails", sqlException);
        }
        return fuelDetails;
    }

    public ArrayList getSummaryDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList fuelDetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        try {
            /*if (getSqlMapClientTemplate().queryForList("operation.getAdvDetails", map) != null) {
             fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getFuelDetails", map);
             }*/
            if (getSqlMapClientTemplate().queryForList("report.getSummaryDetails", map) != null) {
                fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getSummaryDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRemarkDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRemarkDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRemarkDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRemarkDetails", sqlException);
        }
        return fuelDetails;
    }

    public int insertEposTripExpenses(String tripId, String expenseDesc, String expenseAmt, String expenseDate, String expenseRemarks, int userId) {
        Map map = new HashMap();
        int lastInsertedId = 0;
        try {
            //map.put("tripId", tripId);
            map.put("tripId", tripId);
            map.put("expenseDesc", expenseDesc);
            map.put("expenseAmt", expenseAmt);
            map.put("expenseDate", expenseDate);
            map.put("expenseRemarks", expenseRemarks);
            map.put("userId", userId);
            map.put("flag", "0");
            //////System.out.println("map in insertEposTripExpenses " + map);
            lastInsertedId = (Integer) getSqlMapClientTemplate().update("report.insertEposTripExpenses", map);
            //////System.out.println("lastInsertedId -->" + lastInsertedId);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return lastInsertedId;
    }

    public int getTripCountDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int tripCount = 0;
        try {
            if (getSqlMapClientTemplate().queryForObject("report.getTripCountDetails", map) != null) {
                tripCount = (Integer) getSqlMapClientTemplate().queryForObject("report.getTripCountDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return tripCount;
    }

    public int getTotalTonnageDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList settleDetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int totalTonnage = 0;
        try {
            if (getSqlMapClientTemplate().queryForObject("report.getTotalTonnageDetails", map) != null) {
                totalTonnage = (Integer) getSqlMapClientTemplate().queryForObject("report.getTotalTonnageDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return totalTonnage;
    }

    public int getOutKmDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int outKm = 0;
        try {
            if (getSqlMapClientTemplate().queryForObject("report.getOutKmDetails", map) != null) {
                outKm = (Integer) getSqlMapClientTemplate().queryForObject("report.getOutKmDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return outKm;
    }

    public int getInKmDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int inKm = 0;
        try {
            if (getSqlMapClientTemplate().queryForObject("report.getInKmDetails", map) != null) {
                inKm = (Integer) getSqlMapClientTemplate().queryForObject("report.getInKmDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return inKm;
    }

    public ArrayList getTotFuelDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        ArrayList totalFuelDetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int totFuel = 0;
        try {
            if (getSqlMapClientTemplate().queryForList("report.getTotFuelDetails", map) != null) {
                totalFuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTotFuelDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return totalFuelDetails;
    }

    public int getDriverExpenseDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int driExpense = 0;
        try {
            if (getSqlMapClientTemplate().queryForObject("report.getDriverExpenseDetails", map) != null) {
                driExpense = (Integer) getSqlMapClientTemplate().queryForObject("report.getDriverExpenseDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return driExpense;
    }

    public int getDriverSalaryDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int driExpense = 0;
        try {
            if (getSqlMapClientTemplate().queryForObject("report.getDriverSalaryDetails", map) != null) {
                driExpense = (Integer) getSqlMapClientTemplate().queryForObject("report.getDriverSalaryDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return driExpense;
    }

    public int getGeneralExpenseDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int genExpense = 0;
        try {
            if (getSqlMapClientTemplate().queryForObject("operation.getGeneralExpenseDetails", map) != null) {
                genExpense = (Integer) getSqlMapClientTemplate().queryForObject("operation.getGeneralExpenseDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return genExpense;
    }

    public int getDriverAdvanceDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int driAdvance = 0;
        try {
            if (getSqlMapClientTemplate().queryForObject("report.getDriverAdvanceDetails", map) != null) {
                driAdvance = (Integer) getSqlMapClientTemplate().queryForObject("report.getDriverAdvanceDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return driAdvance;
    }

    public int getDriverBataDetails(String fromDate, String toDate, String regNo, int driId) {
        Map map = new HashMap();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        map.put("driId", driId);
        int driAdvance = 0;
        try {
            if (getSqlMapClientTemplate().queryForObject("report.getDriverBataDetails", map) != null) {
                driAdvance = (Integer) getSqlMapClientTemplate().queryForObject("report.getDriverBataDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHaltDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHaltDetails", sqlException);
        }
        return driAdvance;
    }

    public ArrayList getSettleCloseDetails(String fromDate, String toDate, String regNo, String driName) {
        Map map = new HashMap();
        ArrayList fuelDetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("regNo", regNo);
        String dName = "%" + driName + "%";
        map.put("driName", dName);
        try {
            /*if (getSqlMapClientTemplate().queryForList("report.getAdvDetails", map) != null) {
             fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getFuelDetails", map);
             }*/
            if (getSqlMapClientTemplate().queryForList("report.getSettlementCloseDetails", map) != null) {
                fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getSettlementCloseDetails", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRemarkDetails Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRemarkDetails", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRemarkDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRemarkDetails", sqlException);
        }
        return fuelDetails;
    }
    //Rathimeena Code End

    public ArrayList getVehicleCurrentStatus(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList vehicleCurrentStatus = new ArrayList();
        try {
            map.put("statusDate", reportTO.getStatusDate());
            map.put("statusId", reportTO.getStatusId());
            map.put("locationId", reportTO.getLocationId());
            String regNo = "%" + reportTO.getRegisterNo() + "%";
            map.put("regNo", regNo);
            //////System.out.println("Vehicle Status map = " + map);

            vehicleCurrentStatus = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleCurrentStatus", map);
            //////System.out.println("vehicleCurrentStatus=" + vehicleCurrentStatus.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleCurrentStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleCurrentStatus", sqlException);
        }
        return vehicleCurrentStatus;
    }

    public ArrayList getCompanyWiseTripReport(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList companyWiseTripReport = new ArrayList();
        try {
            //////System.out.println("reportTO.getCompanyId(): " + reportTO.getCompanyId());
            //////System.out.println("reportTO.getFromDate(): " + reportTO.getFromDate());
            //////System.out.println("reportTO.getToDate(): " + reportTO.getToDate());
            map.put("customerId", reportTO.getCompanyId());
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            //////System.out.println("Company Wise Trip map = " + map);

            companyWiseTripReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.getCompanyWiseTripReport", map);
            //////System.out.println("companyWiseTripReport=" + companyWiseTripReport.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCompanyWiseTripReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getCompanyWiseTripReport", sqlException);
        }
        return companyWiseTripReport;
    }

    public ArrayList getVehicleList() {
        Map map = new HashMap();
        ArrayList vehicleList = new ArrayList();
        try {
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getCompanyWiseTripReport", map);
            //////System.out.println("vehicleList=" + vehicleList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getCompanyWiseTripReport", sqlException);
        }
        return vehicleList;
    }

    public ArrayList getVehiclePerformance(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList vehiclePerformance = new ArrayList();
        String regNo = "%" + reportTO.getRegNo() + "%";
        map.put("regNo", regNo);
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        try {
            //////System.out.println("getVehiclePerformance...map: " + map);
            vehiclePerformance = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehiclePerformance", map);
            //////System.out.println("vehiclePerformance alist: " + vehiclePerformance.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehiclePerformance Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehiclePerformance", sqlException);
        }
        return vehiclePerformance;
    }

    public ArrayList getDriverPerformance(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList driverPerformance = new ArrayList();
        map.put("driId", reportTO.getDriverId());
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        try {
            //////System.out.println("getDriverPerformance...map: " + map);
            driverPerformance = (ArrayList) getSqlMapClientTemplate().queryForList("report.getDriverPerformance", map);
            //////System.out.println("driverPerformance alist: " + driverPerformance.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverPerformance Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getDriverPerformance", sqlException);
        }
        return driverPerformance;
    }

    public String getDriverEmbarkedDate(ReportTO reportTO) {
        Map map = new HashMap();
        String driEmbarked = "";
        map.put("driId", reportTO.getDriId());
        try {
            //////System.out.println("getDriverEmbarkedDate...map: " + map);
            driEmbarked = (String) getSqlMapClientTemplate().queryForObject("report.getDriverEmbarkedDate", map);
            //////System.out.println("getDriverEmbarkedDate alist: " + driEmbarked);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverEmbarkedDate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getDriverEmbarkedDate", sqlException);
        }
        return driEmbarked;
    }

    public ArrayList getPeriodicServiceVehicleList(int compId, String regNo, String fromDate) {
        Map map = new HashMap();
        ArrayList vehicleList = new ArrayList();
        ArrayList periodicServiceList = new ArrayList();
        ArrayList finalVehicleList = new ArrayList();
        try {
            OperationTO repTO = null;
            map.put("regNo", regNo);
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getPeriodicServiceVehicleList", map);
            //////System.out.println("getPeriodicServiceVehicleList size=" + vehicleList.size());

            Iterator itr = vehicleList.iterator();
            while (itr.hasNext()) {
                periodicServiceList = new ArrayList();
                repTO = new OperationTO();
                repTO = (OperationTO) itr.next();
                map.put("mfrId", repTO.getMfrId());
                map.put("modelId", repTO.getModel());
                map.put("vehicleTypeId", repTO.getVehicleTypeId());
                map.put("vehicleId", repTO.getVehicleId());
                map.put("currentKM", repTO.getCurrentKM());
                //////System.out.println("repTO.getVehicleId():" + repTO.getVehicleId());
                //////System.out.println("repTO.getCurrentKM():" + repTO.getCurrentKM());
                periodicServiceList = (ArrayList) getSqlMapClientTemplate().queryForList("report.vehiclePeriodicServiceList", map);
                //////System.out.println("periodicServiceList size=" + periodicServiceList.size());
                repTO.setPeriodicServiceList(periodicServiceList);
                //finalVehicleList.add(repTO);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPeriodicServiceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getPeriodicServiceList", sqlException);
        }
        return vehicleList;
    }

    public ArrayList getCompanyList() {
        Map map = new HashMap();
        ArrayList companyList = new ArrayList();
        try {
            companyList = (ArrayList) getSqlMapClientTemplate().queryForList("company.getCompanyList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "GradeList", sqlException);
        }
        return companyList;
    }

    public ArrayList getDueList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList dueList = new ArrayList();
        try {
            map.put("regNo", repTO.getRegNo());
            map.put("companyId", repTO.getCompanyId());
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("dueIn", repTO.getDueIn());
            //////System.out.println("regno" + repTO.getRegNo());
            //////System.out.println("companyId" + repTO.getCompanyId());
            //////System.out.println("fromDate" + repTO.getFromDate());
            //////System.out.println("toDate" + repTO.getToDate());
            dueList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getFcDues", map);
            //////System.out.println("dueList size=" + dueList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("dueList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "dueList", sqlException);
        }
        return dueList;
    }

    public ArrayList getInsuranceDueList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList dueList = new ArrayList();
        try {
            map.put("regNo", repTO.getRegNo());
            map.put("companyId", repTO.getCompanyId());
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("dueIn", repTO.getDueIn());
            //////System.out.println("regno" + repTO.getRegNo());
            //////System.out.println("companyId" + repTO.getCompanyId());
            //////System.out.println("fromDate" + repTO.getFromDate());
            //////System.out.println("toDate" + repTO.getToDate());
            dueList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getInsuranceDues", map);
            //////System.out.println("dueList size=" + dueList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("dueList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "dueList", sqlException);
        }
        return dueList;
    }

    public ArrayList getRoadTaxDueList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList dueList = new ArrayList();
        try {
            map.put("regNo", repTO.getRegNo());
            map.put("companyId", repTO.getCompanyId());
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("dueIn", repTO.getDueIn());
            //////System.out.println("regno" + repTO.getRegNo());
            //////System.out.println("companyId" + repTO.getCompanyId());
            //////System.out.println("fromDate" + repTO.getFromDate());
            //////System.out.println("toDate" + repTO.getToDate());
            dueList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getRoadTaxDueList", map);
            //////System.out.println("dueList size=" + dueList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("dueList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "dueList", sqlException);
        }
        return dueList;
    }

    public ArrayList getPermitDueList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList dueList = new ArrayList();
        try {
            map.put("regNo", repTO.getRegNo());
            map.put("companyId", repTO.getCompanyId());
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("dueIn", repTO.getDueIn());
            //////System.out.println("regno" + repTO.getRegNo());
            //////System.out.println("companyId" + repTO.getCompanyId());
            //////System.out.println("fromDate" + repTO.getFromDate());
            //////System.out.println("toDate" + repTO.getToDate());
            dueList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getPermitDueList", map);
            //////System.out.println("dueList size=" + dueList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("dueList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "dueList", sqlException);
        }
        return dueList;
    }

    public ArrayList processAMCDueList(ReportTO repTO) {
        Map map = new HashMap();
        ArrayList dueList = new ArrayList();
        try {
            map.put("regNo", repTO.getRegNo());
            map.put("companyId", repTO.getCompanyId());
            map.put("fromDate", repTO.getFromDate());
            map.put("toDate", repTO.getToDate());
            map.put("dueIn", repTO.getDueIn());
            //////System.out.println("regno" + repTO.getRegNo());
            //////System.out.println("companyId" + repTO.getCompanyId());
            //////System.out.println("fromDate" + repTO.getFromDate());
            //////System.out.println("toDate" + repTO.getToDate());
            dueList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getAMCDueList", map);
            //////System.out.println("dueList size=" + dueList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("dueList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "dueList", sqlException);
        }
        return dueList;
    }

    /**
     * This method used to LPS Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getlpsCount(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList lpsCount = new ArrayList();
        try {
            map.put("LpsId", reportTO.getLpsId());
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            //////System.out.println("map = " + map);
            lpsCount = (ArrayList) getSqlMapClientTemplate().queryForList("report.getlpsCount", map);
            //////System.out.println("lpsCount size=" + lpsCount.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("lpsCount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "lpsCount", sqlException);
        }
        return lpsCount;
    }

    public ArrayList getlpsTrippedList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList lpsTrippedList = new ArrayList();
        try {
            map.put("LpsId", reportTO.getLpsId());
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            //////System.out.println("map = " + map);
            lpsTrippedList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getlpsTrippedList", map);
            //////System.out.println("lpsTrippedList size=" + lpsTrippedList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("lpsTrippedList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "lpsTrippedList", sqlException);
        }
        return lpsTrippedList;
    }

    public ArrayList gettripList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList tripList = new ArrayList();
        try {
            map.put("LpsId", reportTO.getLpsId());
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            //////System.out.println("map = " + map);
            tripList = (ArrayList) getSqlMapClientTemplate().queryForList("report.gettripList", map);
            //////System.out.println("lpsTrippedList size=" + tripList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("tripList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "tripList", sqlException);
        }
        return tripList;
    }

    /**
     * This method used to LPS Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getlocationList() {
        Map map = new HashMap();
        ArrayList locationList = new ArrayList();
        try {
            locationList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getlocationList", map);
            //////System.out.println("locationList size=" + locationList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("locationList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "locationList", sqlException);
        }
        return locationList;
    }

    public ArrayList getTripWisePandL(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList tripWiseList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("companyId", reportTO.getCompanyId());
            map.put("userId", userId);
            //////System.out.println("map = " + map);
            tripWiseList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripWisePandL", map);
            //////System.out.println("tripWiseList size=" + tripWiseList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripWisePandL Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTripWisePandL", sqlException);
        }
        return tripWiseList;
    }

    public ArrayList getTripWisetotal(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList tripWiseTotal = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("companyId", reportTO.getCompanyId());
            map.put("userId", userId);
            //////System.out.println("map = " + map);
            tripWiseTotal = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripWisetotal", map);
            //////System.out.println("tripWiseList size=" + tripWiseTotal.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripWisePandL Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTripWisePandL", sqlException);
        }
        return tripWiseTotal;
    }

    /**
     * This method used to LPS Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleWisePandL(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList vehicleWiseList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("companyId", reportTO.getCompanyId());
            map.put("vehicleType", reportTO.getVehicleType());
            map.put("vehicleId", reportTO.getVehicleId());
            map.put("userId", userId);
            //////System.out.println("map =44444444====> " + map);
            vehicleWiseList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleWisePandL", map);
            //////System.out.println("tripWiseList size=" + vehicleWiseList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleWisePandL Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleWisePandL", sqlException);
        }
        return vehicleWiseList;
    }

    public ArrayList getVehicleWisetotal(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList tripWiseTotal = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("companyId", reportTO.getCompanyId());
            map.put("vehicleType", reportTO.getVehicleType());
            map.put("vehicleId", reportTO.getVehicleId());
            map.put("userId", userId);
            //////System.out.println("map = " + map);
            tripWiseTotal = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleWisetotal", map);
            //////System.out.println("tripWiseList size=" + tripWiseTotal.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleWisetotal Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleWisetotal", sqlException);
        }
        return tripWiseTotal;
    }

    public ArrayList getCustomerList() {
        Map map = new HashMap();
        ArrayList customerList = new ArrayList();
        try {
            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getCustomerList", map);
            //  //////System.out.println("customerTypes size=" + customerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getServiceTypes Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getServiceTypes List", sqlException);
        }

        return customerList;
    }

    public ArrayList getDistrictNameList() {
        Map map = new HashMap();
        ArrayList districtNameList = new ArrayList();
        try {
            districtNameList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getDistrictNameList", map);
            ////////System.out.println("districtNameList size=" + districtNameList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("districtNameList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "districtNameList List", sqlException);
        }

        return districtNameList;
    }

    /**
     * This method used to get District Wise Summart Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getDistrictSummaryList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList districtSummaryList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("ownership", reportTO.getOwnership());
            map.put("district", reportTO.getDistrict());
            map.put("consignmentType", reportTO.getConsignmentType());
            map.put("userId", userId);
            // //////System.out.println("map =44444444====> " + map);
            districtSummaryList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getDistrictSummaryList", map);
            //   //////System.out.println("districtSummaryList size=" + districtSummaryList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDistrictSummaryList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getDistrictSummaryList", sqlException);
        }
        return districtSummaryList;
    }

    /**
     * This method used to get District Wise Summart Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getDistrictDetailsList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList districtDetailsList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("ownership", reportTO.getOwnership());
            map.put("consignmentType", reportTO.getConsignmentType());
            map.put("district", reportTO.getDistrict());
            map.put("userId", userId);
            //  //////System.out.println("map =44444444====> " + map);
            districtDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getDistrictDetailsList", map);
            //  //////System.out.println("districtDetailsList size=" + districtDetailsList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDistrictDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getDistrictDetailsList", sqlException);
        }
        return districtDetailsList;
    }

    /**
     * This method used to get Tripsheet Wise Summart Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getTripsheetDetailsList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList tripSheetDetailsList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("ownership", reportTO.getOwnership());
            map.put("consignmentType", reportTO.getConsignmentType());
            map.put("userId", userId);
            ////////System.out.println("map =44444444====> " + map);
            tripSheetDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("report.tripSheetDetailsList", map);
            // //////System.out.println("tripSheetDetailsList size=" + tripSheetDetailsList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsheetDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTripsheetDetailsList", sqlException);
        }
        return tripSheetDetailsList;
    }

    /**
     * This method used to get Consignee Wise Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getConsigneeWiseList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList consigneeWiseDetailsList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            String consigneeName = "%" + reportTO.getConsigneeName() + "%";
            map.put("consigneeName", consigneeName);
            map.put("userId", userId);
            // //////System.out.println("map =44444444====> " + map);
            consigneeWiseDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getConsigneeWiseList", map);
            //  //////System.out.println("consigneeWiseDetailsList size=" + consigneeWiseDetailsList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsigneeWiseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getConsigneeWiseList", sqlException);
        }
        return consigneeWiseDetailsList;
    }

    /**
     * This method used to get Tripsettlement Wise Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getTripsettlementWiseList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList tripsettlementDetailsList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("tripId", reportTO.getTripId());
            map.put("vehicleNo", reportTO.getVehicleNo());
            map.put("ownership", reportTO.getOwnership());
            map.put("userId", userId);
            // //////System.out.println("map =44444444====> " + map);
            tripsettlementDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripsettlementWiseList", map);
            //  //////System.out.println("tripsettlementDetailsList size=" + tripsettlementDetailsList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsettlementWiseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTripsettlementWiseList", sqlException);
        }
        return tripsettlementDetailsList;
    }

    public ArrayList getDieselReportList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList dieselReportList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("tripId", reportTO.getTripId());
            map.put("vehicleNo", reportTO.getVehicleNo());
            map.put("userId", userId);
            // //////System.out.println("map =44444444====> " + map);
            dieselReportList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getDieselReportList", map);
            //////System.out.println("dieselReportList size=" + dieselReportList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDieselReportList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getDieselReportList", sqlException);
        }
        return dieselReportList;
    }

    /**
     * This method used to get ConsigneeName Suggestions..
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String getConsigneeNameSuggestions(String consigneeName) {
        Map map = new HashMap();
        map.put("consigneeName", "%" + consigneeName + "%");
        String suggestions = "";
        ReportTO reportTO = new ReportTO();
        try {
            ArrayList consigneeNameList = new ArrayList();
            ////////System.out.println("map = " + map);
            consigneeNameList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getConsigneeNameSuggestions", map);
            Iterator itr = consigneeNameList.iterator();
            while (itr.hasNext()) {
                reportTO = new ReportTO();
                reportTO = (ReportTO) itr.next();
                ////////System.out.println("repTO.getConsigneeName() = " + reportTO.getConsigneeName());
                suggestions = reportTO.getConsigneeName() + "~" + suggestions;
                ////////System.out.println("suggestions = " + suggestions);
            }
            if ("".equals(suggestions)) {
                suggestions = "no match found";
            } else if (!"".equals(suggestions)) {
                suggestions = consigneeName + "~" + suggestions;
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsigneeNameSuggestions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getConsigneeNameSuggestions", sqlException);
        }
        return suggestions;
    }

    /**
     * This method used to get Trip Id Suggestions..
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String getTripIdSuggestions(String tripId) {
        Map map = new HashMap();
        map.put("tripId", "%" + tripId + "%");
        String suggestions = "";
        ReportTO reportTO = new ReportTO();
        try {
            ArrayList tripIdList = new ArrayList();
            //////System.out.println("map = " + map);
            tripIdList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripIdSuggestions", map);
            Iterator itr = tripIdList.iterator();
            while (itr.hasNext()) {
                reportTO = new ReportTO();
                reportTO = (ReportTO) itr.next();
                suggestions = reportTO.getTripId() + "~" + suggestions;
            }
            if ("".equals(suggestions)) {
                suggestions = "no match found";
            } else if (!"".equals(suggestions)) {
                suggestions = tripId + "~" + suggestions;
            }
            //////System.out.println("suggestions = " + suggestions);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripIdSuggestions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTripIdSuggestions", sqlException);
        }
        return suggestions;
    }

    /**
     * This method used to get Trip Id Suggestions..
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String getVehicleNumberSuggestions(String vehicleNo) {
        Map map = new HashMap();
        map.put("vehicleNo", "%" + vehicleNo + "%");
        String suggestions = "";
        ReportTO reportTO = new ReportTO();
        try {
            ArrayList vehicleNoList = new ArrayList();
            ////////System.out.println("map = " + map);
            vehicleNoList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleNumberSuggestions", map);
            Iterator itr = vehicleNoList.iterator();
            while (itr.hasNext()) {
                reportTO = new ReportTO();
                reportTO = (ReportTO) itr.next();
                suggestions = reportTO.getVehicleNo() + "~" + suggestions;
                ////////System.out.println("suggestions = " + suggestions);
            }
            if ("".equals(suggestions)) {
                suggestions = "no match found";
            } else if (!"".equals(suggestions)) {
                suggestions = vehicleNo + "~" + suggestions;
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleNumberSuggestions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehicleNumberSuggestions", sqlException);
        }
        return suggestions;
    }

    /**
     * This method used to get Account Head List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    /**
     * This method used to get Customer Wise Profit List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getCustomerOverDueList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList customerOverDueList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("customerId", reportTO.getCustomerId());
            if ("paid".equals(reportTO.getInvoiceType())) {
                map.put("invoiceType", "Paid");
            } else {
                map.put("invoiceType", "Not Paid");
            }
            //////System.out.println("map = " + map);
            customerOverDueList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getCustomerOverDueList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAccountReceivableList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getAccountReceivableList", sqlException);
        }
        return customerOverDueList;
    }

    public ArrayList getSalesDashboard(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList response = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            //////System.out.println("map = " + map);
            response = (ArrayList) getSqlMapClientTemplate().queryForList("report.getSalesDashboard", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAccountReceivableList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getSalesDashboard", sqlException);
        }
        return response;
    }

    public ArrayList getOpsDashboard(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList response = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            //////System.out.println("map = " + map);
            response = (ArrayList) getSqlMapClientTemplate().queryForList("report.getOpsDashboard", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOpsDashboard Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getOpsDashboard", sqlException);
        }
        return response;
    }

    /**
     * This method used to get Account Head List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getAccountReceivableList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList accountReceivableList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("customerId", reportTO.getCustomerId());
            if ("paid".equals(reportTO.getInvoiceType())) {
                map.put("invoiceType", "Paid");
            } else {
                map.put("invoiceType", "Not Paid");
            }
            //////System.out.println("map = " + map);
            accountReceivableList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getAccountReceivableList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAccountReceivableList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getAccountReceivableList", sqlException);
        }
        return accountReceivableList;
    }

    /**
     * This method used to get Customer Wise Profit List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getCustomerWiseProfitList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList customerWiseProfitList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("customerId", reportTO.getCustomerId());
            //////System.out.println("map = " + map);
            customerWiseProfitList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getCustomerWiseProfitList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerWiseProfitList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getCustomerWiseProfitList", sqlException);
        }
        return customerWiseProfitList;
    }

    /**
     * This method used to get Vehicle Wise Profit List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleDetailsList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList vehicleDetailsList = new ArrayList();
        try {
            map.put("vehicleId", reportTO.getVehicleId());
            map.put("fleetCenterId", reportTO.getFleetCenterId());
            System.out.println("map = " + map);
            vehicleDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleDetailsList", map);
            System.out.println("vehicleDetailsList.size() = " + vehicleDetailsList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleDetailsList", sqlException);
        }
        return vehicleDetailsList;
    }

    /**
     * This method used to get Vehicle Earnings List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleEarningsList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList vehicleEarnings = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("vehicleId", reportTO.getVehicleId());
            //  System.out.println("map = " + map);
            vehicleEarnings = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleEarningsList", map);
            // System.out.println("vehicleEarnings.size() = " + vehicleEarnings.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleEarningsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleEarningsList", sqlException);
        }
        return vehicleEarnings;
    }

    /**
     * This method used to get Vehicle Fixed Expense List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleFixedExpenseList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList vehicleEarnings = new ArrayList();
        double insuranceAmount = 0;
        double fcAmount = 0;
        double roadTaxAmount = 0;
        double permitAmount = 0;
        double emiAmount = 0;
        DprTO dprTO = new DprTO();
        String[] dateList = getDateList(reportTO.getFromDate(), reportTO.getToDate());
        ReportTO rpTO1 = null;
        try {
            map.put("vehicleId", reportTO.getVehicleId());
            int i = 0;
            for (int j = 0; j < dateList.length; j++) {
                rpTO1 = new ReportTO();
                rpTO1.setDate(dateList[j]);
                map.put("date", dateList[j]);
                System.out.println("map = " + map);
                insuranceAmount += (Double) getSqlMapClientTemplate().queryForObject("report.getVehicleInsurance", map);
                System.out.println("insuranceAmount = " + insuranceAmount);
                fcAmount += (Double) getSqlMapClientTemplate().queryForObject("report.getVehicleFc", map);
                System.out.println("fcAmount = " + fcAmount);
                roadTaxAmount += (Double) getSqlMapClientTemplate().queryForObject("report.getVehicleRoadTax", map);
                System.out.println("roadTaxAmount = " + roadTaxAmount);
                permitAmount += (Double) getSqlMapClientTemplate().queryForObject("report.getVehiclePermit", map);
                System.out.println("permitAmount = " + permitAmount);
                if (getSqlMapClientTemplate().queryForObject("report.getVehicleEmi", map) != null) {
                    emiAmount += (Double) getSqlMapClientTemplate().queryForObject("report.getVehicleEmi", map);
                } else {
                    emiAmount += 0;
                }

                ////////System.out.println("emiAmount = " + emiAmount);
                i++;
            }
            //////System.out.println("i in the date function= " + i);
            dprTO.setInsuranceAmount(insuranceAmount);
            dprTO.setFcAmount(fcAmount);
            dprTO.setRoadTaxAmount(roadTaxAmount);
            dprTO.setPermitAmount(permitAmount);
            dprTO.setEmiAmount(emiAmount);
            dprTO.setVehicleDriverSalary(reportTO.getVehicleDriverSalary() * i);
            dprTO.setFixedExpensePerDay((insuranceAmount + fcAmount + roadTaxAmount + permitAmount + emiAmount) / i);
            dprTO.setTotlalFixedExpense(dprTO.getFixedExpensePerDay() * i);
            vehicleEarnings.add(dprTO);
            //vehicleEarnings = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleFixedExpenseList", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleFixedExpenseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleFixedExpenseList", sqlException);
        }
        return vehicleEarnings;
    }

    /**
     * This method used to get Vehicle Driver Salary..
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public double getVehicleDriverSalary(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        double driverSalary = 0;
        try {
            map.put("vehicleId", reportTO.getVehicleId());
            //////System.out.println("map = " + map);
            driverSalary = (Double) getSqlMapClientTemplate().queryForObject("report.getVehicleDriverSalary", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleDriverSalary Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehicleDriverSalary", sqlException);
        }
        return driverSalary;
    }

    /**
     * This method used to get Vehicle Fixed Expense List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleOperationExpenseList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList vehicleOperationExpenses = new ArrayList();
        try {
            map.put("vehicleId", reportTO.getVehicleId());
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            vehicleOperationExpenses = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleOperationExpenses", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();;
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleOperationExpenses Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleOperationExpenses", sqlException);
        }
        return vehicleOperationExpenses;
    }

    /**
     * This method used to get Vehicle Maintain Expense List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleMaintainExpenses(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList vehicleMaintainExpenses = new ArrayList();
        try {
            map.put("vehicleId", reportTO.getVehicleId());
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            vehicleMaintainExpenses = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleMaintainExpenses", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleMaintainExpenses Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleMaintainExpenses", sqlException);
        }
        return vehicleMaintainExpenses;
    }

    public String[] getDateList(String startDate, String endDate) {
        String[] dateList = new String[100];
        String[] datList = null;
        try {
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            Date startDt = df.parse(startDate);
            Date endDt = df.parse(endDate);
            Calendar startCal, endCal;
            startCal = Calendar.getInstance();
            startCal.setTime(startDt);
            endCal = Calendar.getInstance();
            endCal.setTime(endDt);
            //Just in case the dates were transposed this prevents infinite loop
            if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
                startCal.setTime(endDt);
                endCal.setTime(startDt);
            }
            startCal.add(Calendar.DATE, -1);
            int i = 0;
            do {
                startCal.add(Calendar.DAY_OF_MONTH, 1);
                ////////System.out.println("Day: " + df.format(startCal.getTime()));
                dateList[i] = df.format(startCal.getTime());
                i++;
            } while (startCal.getTimeInMillis() < endCal.getTimeInMillis());
            datList = new String[i];
            for (int m = 0; m < datList.length; m++) {
                datList[m] = dateList[m];
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return datList;
    }

    public ArrayList getVehicleUtilizationList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList vehicleUtilizationList = new ArrayList();
        try {
            map.put("vehicleId", reportTO.getVehicleId());
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            map.put("fleetCenterId", reportTO.getFleetCenterId());
            //////System.out.println("map = " + map);
            vehicleUtilizationList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleUtilizationList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleUtilizationList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleUtilizationList", sqlException);
        }
        return vehicleUtilizationList;
    }

    public ArrayList getTripSheetDetails(ReportTO reportTO) {
        Map map = new HashMap();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        map.put("tripSheetId", reportTO.getTripSheetId());
        map.put("fleetCenterId", reportTO.getFleetCenterId());
        //////System.out.println("map = " + map);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripSheetDetails", map);
            //////System.out.println("getTripSheetDetail1234s size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripSheetDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getTripEarningsList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList vehicleEarnings = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("tripId", reportTO.getTripId());
            //////System.out.println("map = " + map);
            vehicleEarnings = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripEarningsList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripEarningsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTripEarningsList", sqlException);
        }
        return vehicleEarnings;
    }

    public ArrayList getTripOperationExpenseList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList vehicleOperationExpenses = new ArrayList();
        try {
            map.put("tripId", reportTO.getTripId());
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            vehicleOperationExpenses = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripOperationExpenses", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripOperationExpenseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTripOperationExpenseList", sqlException);
        }
        return vehicleOperationExpenses;
    }

    public double getTripDriverSalary(ReportTO reportTO) {
        Map map = new HashMap();
        double driverSalary = 0;
        try {
            map.put("tripId", reportTO.getTripId());
            //////System.out.println("map = " + map);
            driverSalary = (Double) getSqlMapClientTemplate().queryForObject("report.getTripDriverSalary", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDriverSalary Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTripDriverSalary", sqlException);
        }
        return driverSalary;
    }

    public ArrayList getPopupCustomerProfitReportDetails(ReportTO reportTO) {
        ArrayList popupCustomerProfitReport = new ArrayList();
        Map map = new HashMap();
        int tripStatusId = 0;
        try {
            tripStatusId = Integer.parseInt(reportTO.getTripStatusId());
            if (!reportTO.getTripId().contains(",")) {
                reportTO.setTripId(reportTO.getTripId() + "," + 0);
            }
            if (reportTO.getTripId().contains(",")) {
                String[] tripId = reportTO.getTripId().split(",");
                List tripIds = new ArrayList(tripId.length);
                for (int i = 0; i < tripId.length; i++) {
                    //////System.out.println("value:" + tripId[i]);
                    tripIds.add(tripId[i]);
                }
                map.put("tripId", tripIds);
            } else {
                map.put("tripId", reportTO.getTripId());
            }

            map.put("tripStatusId", tripStatusId);
            //////System.out.println("map--tripst"+map);

            popupCustomerProfitReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.getPopupCustomerProfitReportDetails", map);

            //////System.out.println("map size=" + map);
            //////System.out.println("getPopupTripDetails size=" + popupCustomerProfitReport.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("PopupCustomerProfitReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "PopupCustomerProfitReport List", sqlException);
        }

        return popupCustomerProfitReport;
    }

    public ArrayList getGPSLogDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList logDetails = new ArrayList();
        try {
            map.put("vehicleId", reportTO.getVehicleId());
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            map.put("tripId", reportTO.getTripCode());
            //////System.out.println("map = " + map);
            logDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getGPSLogDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getGPSLogDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getGPSLogDetails", sqlException);
        }
        return logDetails;
    }

    public ArrayList getDriverSettlementDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList logDetails = new ArrayList();
        try {
            map.put("vehicleId", reportTO.getVehicleId());
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            map.put("tripId", reportTO.getTripCode());
            map.put("empId", reportTO.getEmpId());
            //////System.out.println("map = " + map);
            logDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getDriverSettlementDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getGPSLogDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getGPSLogDetails", sqlException);
        }
        return logDetails;
    }

    public ArrayList getTripLogDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList tripLogDetails = new ArrayList();
        try {
            map.put("tripId", reportTO.getTripCode());
            //////System.out.println("map = " + map);
            tripLogDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripLogDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getGPSLogDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getGPSLogDetails", sqlException);
        }
        return tripLogDetails;
    }

    public ArrayList getBPCLTransactionDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList BPCLTransactionDetails = new ArrayList();
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            //////System.out.println("map = " + map);
            BPCLTransactionDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getBPCLTransactionDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBPCLTransactionDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getBPCLTransactionDetails", sqlException);
        }
        return BPCLTransactionDetails;
    }

    public ArrayList getTripGpsStatusDetails(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList tripGpsStatusDetails = new ArrayList();
        try {
            map.put("tripId", reportTO.getTripId());
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            map.put("tripStatus", reportTO.getTripStatus());
            //////System.out.println("map = " + map);
            tripGpsStatusDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripGpsStatusDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripGpsStatusDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTripGpsStatusDetails", sqlException);
        }
        return tripGpsStatusDetails;
    }

    public ArrayList getTripCodeList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList getTripCodeList = new ArrayList();
        map.put("tripCode", reportTO.getTripCode() + "%");
        try {
            //////System.out.println(" getTripCodeList = map is tht" + map);
            getTripCodeList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripCodeList", map);
            //////System.out.println(" getTripCodeList =" + getTripCodeList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getZoneList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getZoneList List", sqlException);
        }

        return getTripCodeList;
    }

    public ArrayList getTripDetails(ReportTO reportTO) {
        Map map = new HashMap();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        map.put("tripSheetId", reportTO.getTripSheetId());
        map.put("fleetCenterId", reportTO.getFleetCenterId());
        //////System.out.println("map = " + map);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripDetails", map);
            //////System.out.println("getTripSheetDetail1234s size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getWflList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList wflList = new ArrayList();
        try {
            wflList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getWflList", map);
            //////System.out.println("getWflList size=" + wflList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWflList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getWflList List", sqlException);
        }

        return wflList;
    }

    public ArrayList getWfuList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList wfuList = new ArrayList();
        try {
            wfuList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getWfuList", map);
            //////System.out.println("getWfuList size=" + wfuList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWfuList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getWfuList List", sqlException);
        }

        return wfuList;
    }

    public ArrayList getTripInProgressList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList tripInProgressList = new ArrayList();
        try {
            tripInProgressList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripInProgressList", map);
            //////System.out.println("getWfuList size=" + tripInProgressList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripInProgressList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripInProgressList List", sqlException);
        }

        return tripInProgressList;
    }

    public ArrayList getTripNotStartedList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList tripNotStartedList = new ArrayList();
        try {
            tripNotStartedList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripNotStartedList", map);
            //////System.out.println("tripNotStartedList size=" + tripNotStartedList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripNotStartedList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripNotStartedList List", sqlException);
        }

        return tripNotStartedList;
    }

    public ArrayList getJobCardList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList jobCardList = new ArrayList();
        try {
            jobCardList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getJobCardList", map);
            //////System.out.println("getJobCardList size=" + jobCardList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJobCardList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getJobCardList List", sqlException);
        }

        return jobCardList;
    }

    public ArrayList getFutureTripList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList futureTripList = new ArrayList();
        try {
            futureTripList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getFutureTripList", map);
            //////System.out.println("getFutureTripList size=" + futureTripList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJobCardList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFutureTripList List", sqlException);
        }

        return futureTripList;
    }

    public int getWflWfuStatus(String currentDate) {
        Map map = new HashMap();
        int wflwfuStatus = 0;
        map.put("currentDate", currentDate);
        try {
            //////System.out.println("getWflWfuStatus...map: " + map);
            wflwfuStatus = (Integer) getSqlMapClientTemplate().queryForObject("report.getWflWfuStatus", map);
            //////System.out.println("wflwfuStatus alist: " + wflwfuStatus);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("wflwfuStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "wflwfuStatus", sqlException);
        }
        return wflwfuStatus;
    }

    public int getWflWfuLogId(String currentDate) {
        Map map = new HashMap();
        int wflwfuLogId = 0;
        map.put("currentDate", currentDate);
        try {
            //////System.out.println("getWflWfuLogId...map: " + map);
            wflwfuLogId = (Integer) getSqlMapClientTemplate().queryForObject("report.getWflWfuLogId", map);
            //////System.out.println("wflwfuLogId alist: " + wflwfuLogId);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWflWfuLogId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getWflWfuLogId", sqlException);
        }
        return wflwfuLogId;
    }

    public String getTotalTripSummaryDetails(ReportTO reportTO) {
        Map map = new HashMap();
        String totalTripSummary = "";
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        map.put("queryType", reportTO.getQueryType());
        map.put("operationPointId", reportTO.getOperationPointId());
        map.put("businessType", reportTO.getBusinessType());
        map.put("tripType", reportTO.getTripType());
        map.put("tripStatusId", reportTO.getTripStatusId());
        map.put("extraExpenseStatusId", reportTO.getExtraExpense());
        try {
            //////System.out.println("getWflWfuLogId...map: " + map);
            totalTripSummary = (String) getSqlMapClientTemplate().queryForObject("report.getTotalTripSummaryDetails", map);
            //////System.out.println("totalTripSummary alist: " + totalTripSummary);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTotalTripSummaryDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTotalTripSummaryDetails", sqlException);
        }
        return totalTripSummary;
    }

    public ArrayList getTripSheetList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList tripSheetList = new ArrayList();
        map.put("fleetCenterId", reportTO.getFleetCenterId());
        map.put("vehicleId", reportTO.getVehicleId());
        map.put("vehicleTypeId", reportTO.getVehicleTypeId());
        map.put("customerId", reportTO.getCustomerId());
        map.put("tripStatusId", reportTO.getTripStatusId());
        map.put("tripStatusIdTo", reportTO.getTripStatusIdTo());
        map.put("startDateFrom", reportTO.getStartDateFrom());
        map.put("startDateTo", reportTO.getStartDateTo());
        map.put("endDateFrom", reportTO.getEndDateFrom());
        map.put("endDateTo", reportTO.getEndDateTo());

        map.put("not", reportTO.getNot());
        if (!reportTO.getTripId().contains(",")) {
            reportTO.setTripId(reportTO.getTripId() + "," + 0);
        }
        if (reportTO.getTripId().contains(",")) {
            String[] tripId = reportTO.getTripId().split(",");
            List tripIds = new ArrayList(tripId.length);
            for (int i = 0; i < tripId.length; i++) {
                //////System.out.println("value:" + tripId[i]);
                tripIds.add(tripId[i]);
            }
            map.put("tripId", tripIds);
        } else {
            map.put("tripId", reportTO.getTripId());
        }
        try {
            //////System.out.println("map for Pop Up = " + map);
            if (reportTO.getTripId().equals(" ") || reportTO.getTripId() == null) {
                // For Trip sheet Report
                tripSheetList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripSheetList", map);
            } else {
                //for Truck Driver Report
                tripSheetList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripSheetListPopUp", map);
            }
            //////System.out.println("getTripSheetList size=" + tripSheetList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripSheetList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripSheetList List", sqlException);
        }

        return tripSheetList;
    }

    public String getMarginWiseTripSummary(ReportTO reportTO) {
        Map map = new HashMap();
        String marginWiseTripSummary = "";
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        map.put("percent", reportTO.getPercent());
        map.put("customerId", reportTO.getCustomerId());
        try {
            //////System.out.println("getMarginWiseTripSummary...map: " + map);
            marginWiseTripSummary = (String) getSqlMapClientTemplate().queryForObject("report.getMarginWiseTripSummary", map);
            //////System.out.println("getMarginWiseTripSummary alist: " + marginWiseTripSummary);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMarginWiseTripSummary Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getMarginWiseTripSummary", sqlException);
        }
        return marginWiseTripSummary;
    }

    public ArrayList getMonthWiseEmptyRunSummary(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList monthWiseEmptyRunSummary = new ArrayList();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        map.put("percent", reportTO.getPercent());
        map.put("customerId", reportTO.getCustomerId());
        try {
            //////System.out.println("getMonthWiseEmptyRunSummary...map: " + map);
            monthWiseEmptyRunSummary = (ArrayList) getSqlMapClientTemplate().queryForList("report.getMonthWiseEmptyRunSummary", map);
            //////System.out.println("getMonthWiseEmptyRunSummary alist: " + monthWiseEmptyRunSummary);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMonthWiseEmptyRunSummary Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getMonthWiseEmptyRunSummary", sqlException);
        }
        return monthWiseEmptyRunSummary;
    }

    public ArrayList getJobcardSummary(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList jobcardSummary = new ArrayList();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        map.put("serviceTypeId", reportTO.getServiceTypeId());
        try {
            //////System.out.println("getJobcardSummary...map: " + map);
            jobcardSummary = (ArrayList) getSqlMapClientTemplate().queryForList("report.getJobcardSummary", map);
            //////System.out.println("getJobcardSummary alist: " + jobcardSummary);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJobcardSummary Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getJobcardSummary", sqlException);
        }
        return jobcardSummary;
    }

    public ArrayList getTripMergingDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList TripMergingDetails = new ArrayList();
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            //////System.out.println("map = " + map);
            TripMergingDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getMergeTripDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBPCLTransactionDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getBPCLTransactionDetails", sqlException);
        }
        return TripMergingDetails;
    }

    public ArrayList getVehicleReadingDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList vehicleReadingDetails = new ArrayList();
        try {
            //  map.put("fromDate", reportTO.getFromDate());
            //  map.put("toDate", reportTO.getToDate());
            map.put("vehicletype", reportTO.getVehicletype());
            //////System.out.println("map = " + map);
            vehicleReadingDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getvehicleReadingDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBPCLTransactionDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getBPCLTransactionDetails", sqlException);
        }
        return vehicleReadingDetails;
    }

    public ArrayList getTripMergingList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList tripMergingList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("customerId", reportTO.getCustomerId());
            //////System.out.println("map = " + map);
            if (!reportTO.getCustomerId().equals("1040")) {
                tripMergingList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripMergingList", map);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripMergingList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTripMergingList", sqlException);
        }
        return tripMergingList;
    }

    public ArrayList getTripNotMergingList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList tripNotMergingList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("customerId", reportTO.getCustomerId());
            //////System.out.println("map = " + map);
            if (!reportTO.getCustomerId().equals("1040")) {
                tripNotMergingList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripNotMergingList", map);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripNotMergingList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTripNotMergingList", sqlException);
        }
        return tripNotMergingList;
    }

    public ArrayList getCustomerMergingList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList customerWiseProfitList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("customerId", reportTO.getCustomerId());
            map.put("tripMergingId", reportTO.getTripMergingId());
            customerWiseProfitList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getCustomerMergingtList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerWiseProfitList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getCustomerWiseProfitList", sqlException);
        }
        return customerWiseProfitList;
    }

    public ArrayList getCustomerTripNotMergingList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList customerTripNotMergingList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("customerId", reportTO.getCustomerId());
            map.put("tripId", reportTO.getTripId());
            //////System.out.println("map = " + map);
            customerTripNotMergingList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getCustomerTripNotMergingList", map);

        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerTripNotMergingList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getCustomerTripNotMergingList", sqlException);
        }
        return customerTripNotMergingList;
    }

    public ArrayList getCustomerLists(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList customerList = new ArrayList();
        try {
            map.put("customerId", reportTO.getCustomerId());
            //////System.out.println("map = " + map);
            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getCustomerLists", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerLists Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getCustomerLists", sqlException);
        }
        return customerList;
    }

    public ArrayList getPopupCustomerMergingProfitReportDetails(ReportTO reportTO) {
        ArrayList popupCustomerProfitReport = new ArrayList();
        Map map = new HashMap();
        try {
            if (!reportTO.getTripId().contains(",")) {
                reportTO.setTripId(reportTO.getTripId() + "," + 0);
            }
            if (reportTO.getTripId().contains(",")) {
                String[] tripId = reportTO.getTripId().split(",");
                List tripIds = new ArrayList(tripId.length);
                for (int i = 0; i < tripId.length; i++) {
                    //////System.out.println("value:" + tripId[i]);
                    tripIds.add(tripId[i]);
                }
                map.put("tripId", tripIds);
            } else {
                map.put("tripId", reportTO.getTripId());
            }
            popupCustomerProfitReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.getPopupCustomerMergingProfitReportDetails", map);
            //////System.out.println("map size=" + map);
            //////System.out.println("getPopupTripDetails size=" + popupCustomerProfitReport.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPopupCustomerMergingProfitReportDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPopupCustomerMergingProfitReportDetails List", sqlException);
        }

        return popupCustomerProfitReport;
    }

    public ArrayList getTripExtraExpenseDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList TripExtraExpenseDetails = new ArrayList();
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            //////System.out.println("map = " + map);
            TripExtraExpenseDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripExtraExpenseDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripExtraExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTripExtraExpenseDetails", sqlException);
        }
        return TripExtraExpenseDetails;
    }

    public ArrayList getVehicleDriverAdvanceDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList vehicleDriverAdvanceDetails = new ArrayList();
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            map.put("expensetype", reportTO.getExpenseType());
            map.put("usagetype", reportTO.getUsageType());
            //////System.out.println("map = " + map);
            vehicleDriverAdvanceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleDriverAdvanceReport", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBPCLTransactionDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getBPCLTransactionDetails", sqlException);
        }
        return vehicleDriverAdvanceDetails;
    }

    public ArrayList getVehicleList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList vehicleList = new ArrayList();
        try {
            map.put("fleetCenterId", reportTO.getFleetCenterId());
            map.put("vehicleId", reportTO.getVehicleId());
            map.put("vehicleTypeId", reportTO.getVehicleTypeId());
            System.out.println((new StringBuilder()).append("map = ").append(map).toString());
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripVehicleList", map);
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog("getVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleList", sqlException);
        }
        return vehicleList;
    }

    public ArrayList getTripVehicleList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList vehicleList = new ArrayList();
        try {
            map.put("fleetCenterId", reportTO.getFleetCenterId());
            map.put("vehicleId", "");
            map.put("vehicleTypeId", reportTO.getVehicleTypeId());
            System.out.println((new StringBuilder()).append("map = ").append(map).toString());
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripVehicleList", map);
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog("getTripVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTripVehicleList", sqlException);
        }
        return vehicleList;
    }

    public ArrayList getTripSheetListWfl(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList tripSheetList = new ArrayList();
        map.put("fleetCenterId", reportTO.getFleetCenterId());
        map.put("vehicleId", reportTO.getVehicleId());
        map.put("vehicleTypeId", reportTO.getVehicleTypeId());
        map.put("customerId", reportTO.getCustomerId());
        map.put("tripStatusId", reportTO.getTripStatusId());
        map.put("tripStatusIdTo", reportTO.getTripStatusIdTo());
        map.put("startDateFrom", reportTO.getStartDateFrom());
        map.put("startDateTo", reportTO.getStartDateTo());
        map.put("endDateFrom", reportTO.getEndDateFrom());
        map.put("endDateTo", reportTO.getEndDateTo());
        map.put("not", reportTO.getNot());
        try {
            tripSheetList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripSheetListWfl", map);

        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog("getTripSheetListWfl Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTripSheetListWfl", sqlException);
        }
        return tripSheetList;
    }

    public String getWflHours(ReportTO reportTO) {
        Map map = new HashMap();
        String totalTripSummary = "";
        map.put("startDateTime", reportTO.getStartDateTime());
        map.put("vehicleId", reportTO.getVehicleId());
        map.put("tripId", reportTO.getTripId());
        try {
            System.out.println((new StringBuilder()).append("getWflWfuLogId...map: ").append(map).toString());
            totalTripSummary = (String) getSqlMapClientTemplate().queryForObject("report.getWflHours", map);
            System.out.println((new StringBuilder()).append("totalTripSummary alist: ").append(totalTripSummary).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog("getWflHours Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getWflHours", sqlException);
        }
        return totalTripSummary;
    }

    public ArrayList getLatestUpdates(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList tripSheetList = new ArrayList();
        try {
            tripSheetList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getLatestUpdates", map);

        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog("getLatestUpdates Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getLatestUpdates", sqlException);
        }
        return tripSheetList;
    }

    public ArrayList getToPayCustomerTripDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList ToPayCustomerTripDetails = new ArrayList();
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            //////System.out.println("map = " + map);
            ToPayCustomerTripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getToPayCustomerTripDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ToPayCustomerTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getToPayCustomerTripDetails", sqlException);
        }
        return ToPayCustomerTripDetails;
    }

    public ArrayList getTripVmrDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList TripVmrDetails = new ArrayList();
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            //////System.out.println("map = " + map);
            TripVmrDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripVmrDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripVmrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTripVmrDetails", sqlException);
        }
        return TripVmrDetails;
    }

    public ArrayList getFcWiseTripBudgetDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList budgetListsDetails = new ArrayList();
        ArrayList budgetDetails = new ArrayList();
        ArrayList driverAdvanceDetails = new ArrayList();
        ArrayList vehicleLists = new ArrayList();
        ArrayList totalKmLists = new ArrayList();
        String wfuAmount = "";
        String emptyTripKM = "";
        String noOfVehicles = "";
        float totalkms = 0;
        float totalhms = 0;
        float foodingAdvance = 0;
        float rnmAdvance = 0;
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());

            budgetDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripExpenseAndTotalCostDetails", map);
            ReportTO rpTO = new ReportTO();
            Iterator itr1 = budgetDetails.iterator();
            ReportTO repTO1 = new ReportTO();
            while (itr1.hasNext()) {
                ReportTO reportTo = new ReportTO();
                repTO1 = (ReportTO) itr1.next();
                reportTo.setCompanyId(repTO1.getCompanyId());
                reportTo.setCompanyName(repTO1.getCompanyName());
                reportTo.setTotalCost(repTO1.getTotalCost());
                reportTo.setRcm(repTO1.getRcm());
                map.put("companyId", repTO1.getCompanyId());
                wfuAmount = (String) getSqlMapClientTemplate().queryForObject("report.getTripWfuAdvanceForBudgetDetails", map);
                reportTo.setWfuAmount(wfuAmount);
                emptyTripKM = (String) getSqlMapClientTemplate().queryForObject("report.getEmptyKMForBudgetDetails", map);
                noOfVehicles = (String) getSqlMapClientTemplate().queryForObject("report.getTotalVehicleForBudgetDetails", map);
                reportTo.setNoOfVehicles(Integer.parseInt(noOfVehicles));
                reportTo.setEmptyTripKM(emptyTripKM);
                vehicleLists = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleIdForBudgetReport", map);
                //////System.out.println("map = " + vehicleLists.size());
                Iterator itr2 = vehicleLists.iterator();
                ReportTO repTO3 = new ReportTO();
                totalkms = 0;
                totalhms = 0;
                foodingAdvance = 0;
                rnmAdvance = 0;
                while (itr2.hasNext()) {
                    repTO3 = (ReportTO) itr2.next();
                    reportTo.setCompanyId(repTO3.getCompanyId());
                    map.put("vehicleId", repTO3.getVehicleId());
                    totalKmLists = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTotalKmForBudgetReport", map);
                    Iterator itr3 = totalKmLists.iterator();
                    ReportTO repTO4 = new ReportTO();
                    while (itr3.hasNext()) {
                        repTO4 = (ReportTO) itr3.next();
                        totalkms = totalkms + Float.parseFloat(repTO4.getTotalkms());
                        totalhms = totalhms + Float.parseFloat(repTO4.getTotalHms());
                    }
                    driverAdvanceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getDriverAdvanceForBudgetDetails", map);
                    if (driverAdvanceDetails.size() > 0) {
                        Iterator itr4 = driverAdvanceDetails.iterator();
                        ReportTO repTO5 = new ReportTO();
                        while (itr4.hasNext()) {
                            repTO5 = (ReportTO) itr4.next();
                            foodingAdvance = foodingAdvance + Float.parseFloat(repTO5.getFoodingAdvance());
                            rnmAdvance = rnmAdvance + Float.parseFloat(repTO5.getRepairAdvance());

                        }
                    }
                }
                reportTo.setTotalkms(String.valueOf(totalkms));
                reportTo.setTotalHms(String.valueOf(totalhms));
                reportTo.setFoodingAdvance(String.valueOf(foodingAdvance));
                reportTo.setRepairAdvance(String.valueOf(rnmAdvance));
                budgetListsDetails.add(reportTo);
            }
            //////System.out.println("budgetListsDetails = " + budgetListsDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFcWiseTripBudgetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getFcWiseTripBudgetDetails", sqlException);
        }
        return budgetListsDetails;
    }

    public ArrayList getAccountMgrPerformanceReportDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList accountMgrLists = new ArrayList();
        ArrayList budgetListsDetails = new ArrayList();
        ArrayList revenueCostLists = new ArrayList();
        ArrayList totalKmLists = new ArrayList();
        String customerId = "";
        String emptyTripKM = "";
        String noOfVehicles = "";

        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            accountMgrLists = (ArrayList) getSqlMapClientTemplate().queryForList("report.getAccountMgrId", map);
            ReportTO rpTO = new ReportTO();
            Iterator itr1 = accountMgrLists.iterator();
            ReportTO repTO1 = new ReportTO();
            while (itr1.hasNext()) {
                float totalkms = 0;
                float totalhms = 0;
                int tripNos = 0;
                int wfuHours = 0;
                float revenue = 0;
                float totalCost = 0;
                ReportTO reportTo = new ReportTO();
                repTO1 = (ReportTO) itr1.next();
                reportTo.setEmpId(repTO1.getEmpId());
                reportTo.setEmpName(repTO1.getEmpName());
                map.put("empId", repTO1.getEmpId());
                //////System.out.println("repTO1.getEmpId() = " + repTO1.getEmpId());
                customerId = "";
                customerId = (String) getSqlMapClientTemplate().queryForObject("report.getCustomerId", map);
                //////System.out.println("customerId = " + customerId);
                if (!"".equals(customerId) && customerId != null) {
                    //////System.out.println("customerId not null condition = " + customerId);
                    String[] customerIds = customerId.split(",");
                    List customerIdsList = new ArrayList(customerIds.length);
                    for (int i = 0; i < customerIds.length; i++) {
                        customerIdsList.add(customerIds[i]);
                    }
                    map.put("customerId", customerIdsList);
                    revenueCostLists = (ArrayList) getSqlMapClientTemplate().queryForList("report.getRevenueAndCost", map);
                    //////System.out.println("customerId = " + customerId);
                    //////System.out.println("map = " + map);
                    //////System.out.println("revenueCostListssize = " + revenueCostLists.size());
                    totalkms = 0;
                    totalhms = 0;
                    tripNos = 0;
                    wfuHours = 0;
                    revenue = 0;
                    totalCost = 0;
                    String tripId = "";
                    ReportTO repTO3 = new ReportTO();
                    Iterator itr2 = revenueCostLists.iterator();
                    while (itr2.hasNext()) {
                        repTO3 = (ReportTO) itr2.next();
                        tripNos = tripNos + Integer.parseInt(repTO3.getTripNos());
                        wfuHours = wfuHours + Integer.parseInt(repTO3.getWfuHours());
                        revenue = revenue + Float.parseFloat(repTO3.getRevenue());
                        totalCost = totalCost + Float.parseFloat(repTO3.getTotalCost());
                        tripId = repTO3.getTripId();
                        if (tripId != null) {
                            String[] tripIds = tripId.split(",");
                            List tripIdList = new ArrayList(tripIds.length);
                            for (int i = 0; i < tripIds.length; i++) {
                                tripIdList.add(tripIds[i]);
                            }
                            map.put("tripId", tripIdList);
                            //////System.out.println("repTO3.getTripId() = " + repTO3.getTripId());
                            totalKmLists = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTotalKm", map);
                            Iterator itr3 = totalKmLists.iterator();
                            ReportTO repTO4 = new ReportTO();
                            while (itr3.hasNext()) {
                                repTO4 = (ReportTO) itr3.next();
                                totalkms = totalkms + Float.parseFloat(repTO4.getTotalkms());
                                totalhms = totalhms + Float.parseFloat(repTO4.getTotalHms());
                            }
                        }
                    }
                    reportTo.setTripNos(String.valueOf(tripNos));
                    reportTo.setWfuHours(String.valueOf(wfuHours));
                    reportTo.setRevenue(String.valueOf(revenue));
                    reportTo.setTotalCost(String.valueOf(totalCost));
                    reportTo.setTotalkms(String.valueOf(totalkms));
                    reportTo.setTotalHms(String.valueOf(totalhms));
                } else {
                    reportTo.setTripNos(String.valueOf(tripNos));
                    reportTo.setWfuHours(String.valueOf(wfuHours));
                    reportTo.setRevenue(String.valueOf(revenue));
                    reportTo.setTotalCost(String.valueOf(totalCost));
                    reportTo.setTotalkms(String.valueOf(totalkms));
                    reportTo.setTotalHms(String.valueOf(totalhms));
                }
                budgetListsDetails.add(reportTo);
            }
            //////System.out.println("budgetListsDetails = " + budgetListsDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFcWiseTripBudgetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getFcWiseTripBudgetDetails", sqlException);
        }
        return budgetListsDetails;
    }

    public ArrayList getRNMExpenseReportDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList vehicleList = new ArrayList();
        ArrayList rnmListsDetails = new ArrayList();
        ArrayList rnmAdvanceLists = new ArrayList();
        String jobCardId = "";

        String rnmAdvance = "";

        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            map.put("type", reportTO.getType());
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleIdForRNMReport", map);
            Iterator itr1 = vehicleList.iterator();
            ReportTO repTO1 = new ReportTO();
            while (itr1.hasNext()) {
                String serviceTypeId = "17,16,15";
                String temp[] = serviceTypeId.split(",");
                ReportTO reportTo = new ReportTO();
                repTO1 = (ReportTO) itr1.next();
                reportTo.setVehicleId(repTO1.getVehicleId());
                reportTo.setRegNo(repTO1.getRegNo());
                map.put("vehicleId", repTO1.getVehicleId());
                rnmAdvanceLists = (ArrayList) getSqlMapClientTemplate().queryForList("report.getJobCardIdForRNMReport", map);
                if (rnmAdvanceLists.size() > 0) {
                    Iterator itr4 = rnmAdvanceLists.iterator();
                    ReportTO repTO5 = new ReportTO();
                    while (itr4.hasNext()) {
                        repTO5 = (ReportTO) itr4.next();
                        reportTo.setChasisAmount(repTO5.getChasisAmount());
                        reportTo.setReferAmount(repTO5.getReferAmount());
                        reportTo.setContainerAmount(repTO5.getContainerAmount());
                    }
                } else {
                    reportTo.setChasisAmount("0.00");
                    reportTo.setReferAmount("0.00");
                    reportTo.setContainerAmount("0.00");
                }
                rnmListsDetails.add(reportTo);
            }
            //////System.out.println("getRNMExpenseReportDetails = " + rnmListsDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRNMExpenseReportDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getRNMExpenseReportDetails", sqlException);
        }
        return rnmListsDetails;
    }

    public ArrayList getTyerExpenseReportDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList vehicleList = new ArrayList();
        ArrayList rnmListsDetails = new ArrayList();
        ArrayList rnmAdvanceLists = new ArrayList();
        String jobCardId = "";

        String rnmAdvance = "";

        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            map.put("type", reportTO.getType());
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleIdForRNMReport", map);
            Iterator itr1 = vehicleList.iterator();
            ReportTO repTO1 = new ReportTO();
            while (itr1.hasNext()) {
                String TyreType = "Newtyer,Rethread";
                String temp[] = TyreType.split(",");
                ReportTO reportTo = new ReportTO();
                repTO1 = (ReportTO) itr1.next();
                reportTo.setVehicleId(repTO1.getVehicleId());
                reportTo.setRegNo(repTO1.getRegNo());
                map.put("vehicleId", repTO1.getVehicleId());
                rnmAdvanceLists = (ArrayList) getSqlMapClientTemplate().queryForList("report.getJobCardIdForRNMReport", map);
                if (rnmAdvanceLists.size() > 0) {
                    Iterator itr4 = rnmAdvanceLists.iterator();
                    ReportTO repTO5 = new ReportTO();
                    while (itr4.hasNext()) {
                        repTO5 = (ReportTO) itr4.next();
                        reportTo.setChasisAmount(repTO5.getChasisAmount());
                        reportTo.setReferAmount(repTO5.getReferAmount());
                        reportTo.setContainerAmount(repTO5.getContainerAmount());
                    }
                } else {
                    reportTo.setChasisAmount("0.00");
                    reportTo.setReferAmount("0.00");
                    reportTo.setContainerAmount("0.00");
                }
                rnmListsDetails.add(reportTo);
            }
            //////System.out.println("getRNMExpenseReportDetails = " + rnmListsDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRNMExpenseReportDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getRNMExpenseReportDetails", sqlException);
        }
        return rnmListsDetails;
    }

    public ArrayList getRNMVehicleList(String type) {
        Map map = new HashMap();
        map.put("type", type);
        ArrayList vehicleList = new ArrayList();
        try {
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleIdForRNMReport", map);
            //////System.out.println("getRNMVehicleList size=" + vehicleList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRNMVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRNMVehicleList", sqlException);
        }
        return vehicleList;
    }

    public ArrayList getVehicleTyreList(String type) {
        Map map = new HashMap();
        map.put("type", type);
        ArrayList vehicleList = new ArrayList();
        try {
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleTyreList", map);
            //////System.out.println("getVehicleTyreList size=" + vehicleList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRNMVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleTyreList", sqlException);
        }
        return vehicleList;
    }

    public ArrayList getReportList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList reportList = new ArrayList();
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            map.put("vehicleId", reportTO.getVehicleId());
            //////System.out.println("getReportList************"+map);
            reportList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getReportList", map);
            //////System.out.println("reportList size=" + reportList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "reportList", sqlException);
        }
        return reportList;
    }

    public ArrayList getVehicleNoforReport(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList vehicleNoList = new ArrayList();
        map.put("vehicleNo", "%" + reportTO.getVehicleNo() + "%");
        //////System.out.println("getVehicleNo map"+map);
        try {
            vehicleNoList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleNoforReport", map);
            //////System.out.println(" vehicleNoList =" + vehicleNoList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getZoneList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "driverList", sqlException);
        }

        return vehicleNoList;
    }

    public ArrayList getOrderExpenseRevenue(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList orderExpenseRevenue = new ArrayList();
        //        map.put("fromDate", reportTO.getFromDate());
        //        map.put("toDate", reportTO.getToDate());
        //        map.put("percent", reportTO.getPercent());

        try {
            //////System.out.println("getMonthWiseEmptyRunSummary...map: " + map);
            orderExpenseRevenue = (ArrayList) getSqlMapClientTemplate().queryForList("report.getOrderExpenseRevenue", map);
            //////System.out.println("orderExpenseRevenue alist: " + orderExpenseRevenue);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrderExpenseRevenue Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getOrderExpenseRevenue", sqlException);
        }
        return orderExpenseRevenue;
    }

    public ArrayList getTrailerMovementList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList orderExpenseRevenue = new ArrayList();
        //        map.put("fromDate", reportTO.getFromDate());
        //        map.put("toDate", reportTO.getToDate());
        //        map.put("percent", reportTO.getPercent());

        try {
            //////System.out.println("getMonthWiseEmptyRunSummary...map: " + map);
            orderExpenseRevenue = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTrailerMovementList", map);
            //////System.out.println("orderExpenseRevenue alist: " + orderExpenseRevenue);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTrailerMovementList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTrailerMovementList", sqlException);
        }
        return orderExpenseRevenue;
    }

    public ArrayList getTripTrailerProfitDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList tripTrailerProfitDetails = new ArrayList();
        map.put("toDate", reportTO.getToDate());
        map.put("fromDate", reportTO.getFromDate());
        map.put("vehicleId", reportTO.getVehicleId());
        System.out.println("map" + map);
        try {
            //////System.out.println("getMonthWiseEmptyRunSummary...map: " + map);
            tripTrailerProfitDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripTrailerProfitDetails", map);
            //////System.out.println("orderExpenseRevenue alist: " + orderExpenseRevenue);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTrailerMovementList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTrailerMovementList", sqlException);
        }
        return tripTrailerProfitDetails;
    }

    public ArrayList getTrailerEarningsList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList trailerEarnings = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("trailerId", reportTO.getTrailerId());
            //////System.out.println("map = " + map);
            trailerEarnings = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTrailerEarningsList", map);
            //////System.out.println("vehicleEarnings.size() = " + vehicleEarnings.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTrailerEarningsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTrailerEarningsList", sqlException);
        }
        return trailerEarnings;
    }

    public ArrayList getTrailerFixedExpenseList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList vehicleEarnings = new ArrayList();
        double insuranceAmount = 0;
        double fcAmount = 0;
        double roadTaxAmount = 0;
        double permitAmount = 0;
        double emiAmount = 0;
        DprTO dprTO = new DprTO();
        String[] dateList = getDateList(reportTO.getFromDate(), reportTO.getToDate());
        ReportTO rpTO1 = null;
        try {
            map.put("trailerId", reportTO.getTrailerId());
            int i = 0;
            for (int j = 0; j < dateList.length; j++) {
                rpTO1 = new ReportTO();
                rpTO1.setDate(dateList[j]);
                map.put("date", dateList[j]);
                ////////System.out.println("map = " + map);
                insuranceAmount += (Double) getSqlMapClientTemplate().queryForObject("report.getTrailerInsurance", map);
                ////////System.out.println("insuranceAmount = " + insuranceAmount);
                fcAmount += (Double) getSqlMapClientTemplate().queryForObject("report.getTrailerFc", map);
                ////////System.out.println("fcAmount = " + fcAmount);
                roadTaxAmount += (Double) getSqlMapClientTemplate().queryForObject("report.getTrailerRoadTax", map);
                ////////System.out.println("roadTaxAmount = " + roadTaxAmount);
                permitAmount += (Double) getSqlMapClientTemplate().queryForObject("report.getTrailerPermit", map);
                ////////System.out.println("permitAmount = " + permitAmount);
                if (getSqlMapClientTemplate().queryForObject("report.getTrailerEmi", map) != null) {
                    emiAmount += (Double) getSqlMapClientTemplate().queryForObject("report.getTrailerEmi", map);
                } else {
                    emiAmount += 0;
                }

                ////////System.out.println("emiAmount = " + emiAmount);
                i++;
            }
            //////System.out.println("i in the date function= " + i);
            dprTO.setInsuranceAmount(insuranceAmount);
            dprTO.setFcAmount(fcAmount);
            dprTO.setRoadTaxAmount(roadTaxAmount);
            dprTO.setPermitAmount(permitAmount);
            dprTO.setEmiAmount(emiAmount);
            dprTO.setVehicleDriverSalary(reportTO.getVehicleDriverSalary() * i);
            dprTO.setFixedExpensePerDay((insuranceAmount + fcAmount + roadTaxAmount + permitAmount + emiAmount) / i);
            dprTO.setTotlalFixedExpense(dprTO.getFixedExpensePerDay() * i);
            vehicleEarnings.add(dprTO);
            //vehicleEarnings = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleFixedExpenseList", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleFixedExpenseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleFixedExpenseList", sqlException);
        }
        return vehicleEarnings;
    }

    public ArrayList getTrailerOperationExpenseList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList vehicleOperationExpenses = new ArrayList();
        try {
            map.put("trailerId", reportTO.getTrailerId());
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            System.out.println("map" + map);
            vehicleOperationExpenses = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTrailerOperationExpenses", map);
            System.out.println("vehicleOperationExpenses" + vehicleOperationExpenses.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTrailerOperationExpenses Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTrailerOperationExpenses", sqlException);
        }
        return vehicleOperationExpenses;
    }

    public ArrayList getTrailerMaintainExpenses(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList vehicleMaintainExpenses = new ArrayList();
        try {
            map.put("trailerId", reportTO.getVehicleId());
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            vehicleMaintainExpenses = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTrailerMaintainExpenses", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleMaintainExpenses Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleMaintainExpenses", sqlException);
        }
        return vehicleMaintainExpenses;
    }

    public ArrayList getTrailerDetailsList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList vehicleDetailsList = new ArrayList();
        try {
            map.put("trailerId", reportTO.getTrailerId());
            //////System.out.println("map = " + map);
            vehicleDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTrailerDetailsList", map);
            //////System.out.println("vehicleDetailsList.size() = " + vehicleDetailsList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTrailerDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getTrailerDetailsList", sqlException);
        }
        return vehicleDetailsList;
    }

    public ArrayList getDashboardopsTruckNos(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList getDashboardopsTruckNos = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("dashboardopstrukCount", reportTO.getDashboardopstrukCount() + "%");
        //////System.out.println("map = " + map);
        try {
            getDashboardopsTruckNos = (ArrayList) getSqlMapClientTemplate().queryForList("report.getDashboardopsTruckNos", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDashboardopsTruckNos Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getDashboardopsTruckNos", sqlException);
        }
        return getDashboardopsTruckNos;
    }

    public ArrayList getDashboardWorkShop(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList getDashboardWorkShop = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("dashboardopstrukCount", reportTO.getDashboardopstrukCount() + "%");
        //////System.out.println("map = " + map);
        try {
            getDashboardWorkShop = (ArrayList) getSqlMapClientTemplate().queryForList("report.getDashboardWorkShop", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDashboardWorkShop Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getDashboardWorkShop", sqlException);
        }
        return getDashboardWorkShop;
    }

    public ArrayList getVehicleUT() throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList vehicleUT = new ArrayList();
        try {
            vehicleUT = (ArrayList) getSqlMapClientTemplate().queryForList("report.vehicleUT", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getDriverList", sqlException);
        }
        return vehicleUT;
    }

    public ArrayList dbTrailerMake() throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList trailerMake = new ArrayList();
        try {
            trailerMake = (ArrayList) getSqlMapClientTemplate().queryForList("report.dbTrailerMake", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("trailerMake Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "trailerMake", sqlException);
        }
        return trailerMake;
    }

    public ArrayList dbTruckType() throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList truckType = new ArrayList();
        try {
            truckType = (ArrayList) getSqlMapClientTemplate().queryForList("report.dbTruckType", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("truckType Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "truckType", sqlException);
        }
        return truckType;
    }

    public ArrayList dbTrailerType() throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList trailerType = new ArrayList();
        try {
            trailerType = (ArrayList) getSqlMapClientTemplate().queryForList("report.dbTrailerType", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("dbTrailerType Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "dbTrailerType", sqlException);
        }
        return trailerType;
    }

    public ArrayList getTrailerJobCardMTD() throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList getTrailerJobCardMTD = new ArrayList();
        try {
            getTrailerJobCardMTD = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTrailerJobCardMTD", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTrailerjobcardMTD Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTrailerjobcardMTD", sqlException);
        }
        return getTrailerJobCardMTD;
    }

    public ArrayList getTruckJobCardMTD() throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList getTruckJobCardMTD = new ArrayList();
        try {
            getTruckJobCardMTD = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTruckJobCardMTD", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTruckJobCardMTD Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTruckJobCardMTD", sqlException);
        }
        return getTruckJobCardMTD;
    }

    public ArrayList processMechPerformanceList(ReportTO repTO, String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList mechPerformanceList = new ArrayList();
        try {

            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("technicianId", repTO.getTechnicianId());
            map.put("servicePointId", repTO.getServicePointId());
            System.out.println("map in mechanic performance :" + map);
            mechPerformanceList = (ArrayList) getSqlMapClientTemplate().queryForList("report.processMechPerformanceList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("mechPerformanceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "mechPerformanceList", sqlException);
        }
        return mechPerformanceList;
    }

    public ArrayList processMechPerformanceData(ReportTO repTO, String fromDate, String toDate) {
        Map map = new HashMap();
        ArrayList mechPerformanceList = new ArrayList();
        try {

            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("technicianId", repTO.getTechnicianId());
            System.out.println("technicianId:" + repTO.getTechnicianId());
            System.out.println("fromDate:" + fromDate);
            System.out.println("toDate:" + toDate);
            mechPerformanceList = (ArrayList) getSqlMapClientTemplate().queryForList("report.processMechPerformanceData", map);
            System.out.println("mechPerformanceList size=" + mechPerformanceList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("mechPerformanceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "mechPerformanceList", sqlException);
        }
        return mechPerformanceList;
    }

    public ArrayList ViewQBEntityList() throws FPRuntimeException, FPBusinessException {

        Map map = new HashMap();
        ArrayList ViewQBEntityList = new ArrayList();
//        System.out.println("I am in the ViewQBEntityList");
        try {

            ViewQBEntityList = (ArrayList) getSqlMapClientTemplate().queryForList("report.ViewQBEntityList", map);
            System.out.println(" get List size= " + ViewQBEntityList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ViewQBEntityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "ViewQBEntityList", sqlException);
        }
        return ViewQBEntityList;

    }

    public ArrayList viewQueryBuilder() {
        Map map = new HashMap();
        String viewQueryBuilder = "";

        ArrayList viewQueryBuilderlist = new ArrayList();
        Properties dbProps = new Properties();
        try {
            String fileName = "jdbc_url.properties";
            InputStream is = getClass().getResourceAsStream("/" + fileName);
            dbProps.load(is);
//            System.out.println("i am entering to view query builder");
            String dbUrl = dbProps.getProperty("jdbc.url");
            System.out.println("dbUrl = " + dbUrl);
            String[] dbUrlsplit = dbUrl.split("/");
            String dbname = dbUrlsplit[3];
            System.out.println("dbname = " + dbUrlsplit[3]);
            map.put("dbname", dbname);
            System.out.println("map = " + map);
//            System.out.println("Map getPanNo=" + map);
            viewQueryBuilderlist = (ArrayList) getSqlMapClientTemplate().queryForList("report.viewQueryBuilder", map);
            System.out.println(" get table size= " + viewQueryBuilderlist.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("viewQueryBuilder Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "viewQueryBuilder", sqlException);
        }
        return viewQueryBuilderlist;
    }

    public int saveQueryBuilderValues(ReportTO reportTO) {
        Map map = new HashMap();
        System.out.println("map" + map);
        int QBReferenceId = 0;
        String viewQueryBuilder = "";
        String entityName = reportTO.getEntityName();
        int status = 0;
        int selectionstatus = 0;
        try {
            int userId = reportTO.getUserId();
            System.out.println("userId = " + userId);
            status = (Integer) getSqlMapClientTemplate().insert("report.QueryBuilderMasterValues", reportTO);
            if (status > 0) {
                reportTO.setLastInsertId(status);
                status = (Integer) getSqlMapClientTemplate().update("report.QueryBuilderEntityValues", reportTO);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("viewQueryBuilder Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "viewQueryBuilder", sqlException);
        }

        return status;
    }

    public int editQBEntityDetails(ReportTO reportTO) {

        Map map = new HashMap();
        ArrayList viewQBEntityDetails = new ArrayList();
        System.out.println("map" + map);
        String entityName = reportTO.getEntityName();
        int updatestatus = 0;
        int insertstatus = 0;
        boolean status = false;
        System.out.println("editQBEntityDetails in DAO ");

        try {
            int entityId = 0;
            System.out.println("entityId = " + reportTO.getEntityId());
            entityId = reportTO.getEntityId();
            map.put("entityId", entityId);
            System.out.println("Map of editQBEntityDetails " + map);
            ArrayList entityDetailsList = new ArrayList();
            ArrayList editEntityDetails = new ArrayList();
            ArrayList updatedEntityDetails = new ArrayList();
            viewQBEntityDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.viewQBEntityDetails", map);
            viewQBEntityDetails.size();
            entityDetailsList = (ArrayList) reportTO.getTableList();
            for (int i = 0; i < viewQBEntityDetails.size(); i++) {

            }
            System.out.println("viewQBEntityDetails.size() = " + viewQBEntityDetails.size());
            Iterator itr1 = entityDetailsList.iterator();
            int i = 0;
            int j = 0;
            while (itr1.hasNext()) {
                System.out.println("j = " + j++);
                ReportTO repoTO = new ReportTO();
                repoTO = (ReportTO) itr1.next();
                if (i < viewQBEntityDetails.size()) {
                    System.out.println("repoTO.getEntityDetailsId() = " + repoTO.getEntityDetailsId());
                    String entityDetailsId = repoTO.getEntityDetailsId();
                    repoTO.setEntityDetailsId(entityDetailsId);
                    System.out.println("entityDetailsId = " + entityDetailsId);
                    map.put("entityDetailsId", entityDetailsId);
                    String tablename = repoTO.getTableName();
                    System.out.println("tablename = " + tablename);
                    repoTO.setTableName(tablename);
                    map.put("tablename", tablename);
                    String columnName = repoTO.getColumnName();
                    repoTO.setColumnName(columnName);
                    map.put("columnName", columnName);
                    System.out.println("columnName = " + columnName);
                    String displayName = repoTO.getDisplayName();
                    repoTO.setDisplayName(displayName);
                    map.put("displayName", displayName);
                    System.out.println("displayName = " + displayName);
                    String columnDataTypeName = repoTO.getColumnDataTypeName();
                    repoTO.setColumnDataTypeName(columnDataTypeName);
                    map.put("columnDataTypeName", columnDataTypeName);
                    System.out.println("columnDataTypeName = " + columnDataTypeName);
                    String activeInd = repoTO.getActiveInd();
                    repoTO.setActiveInd(activeInd);
                    map.put("activeInd", activeInd);
                    System.out.println("activeInd = " + activeInd);
                    String qBOpenSyntax = repoTO.getqBOpenSyntax();
                    repoTO.setqBOpenSyntax(qBOpenSyntax);
                    map.put("qBOpenSyntax", qBOpenSyntax);
                    System.out.println("qBOpenSyntax = " + qBOpenSyntax);
                    String qBAggregateFunction = repoTO.getqBAggregateFunction();
                    repoTO.setqBAggregateFunction(qBAggregateFunction);
                    map.put("qBAggregateFunction", qBAggregateFunction);
                    System.out.println("qBAggregateFunction = " + qBAggregateFunction);
                    String qBClosingSyntax = repoTO.getqBClosingSyntax();
                    repoTO.setqBClosingSyntax(qBClosingSyntax);
                    map.put("qBClosingSyntax", qBClosingSyntax);
                    System.out.println("qBClosingSyntax = " + qBClosingSyntax);
                    String entityParseValue = repoTO.getEntityParseValue();
                    repoTO.setEntityParseValue(entityParseValue);
                    map.put("entityParseValue", entityParseValue);
                    System.out.println("entityParseValue = " + entityParseValue);
                    editEntityDetails.add(repoTO);
                    System.out.println("Map of editQBEntityDetails " + map);
                    updatestatus = (Integer) getSqlMapClientTemplate().update("report.updateQBEntityDetails", map);
                    i++;
                } else {
                    System.out.println("i am in else condition ");
                    String tablename = repoTO.getTableName();
                    repoTO.setTableName(tablename);
                    System.out.println("tablename = " + tablename);
                    String columnName = repoTO.getColumnName();
                    repoTO.setColumnName(columnName);
                    System.out.println("columnName = " + columnName);
                    String displayName = repoTO.getDisplayName();
                    repoTO.setDisplayName(displayName);
                    System.out.println("displayName = " + displayName);
                    String columnDataTypeName = repoTO.getColumnDataTypeName();
                    repoTO.setColumnDataTypeName(columnDataTypeName);
                    System.out.println("columnDataTypeName = " + columnDataTypeName);
                    String activeInd = repoTO.getActiveInd();
                    repoTO.setActiveInd(activeInd);
                    System.out.println("activeInd = " + activeInd);
                    String qBOpenSyntax = repoTO.getqBOpenSyntax();
                    repoTO.setqBOpenSyntax(qBOpenSyntax);
                    map.put("qBOpenSyntax", qBOpenSyntax);
                    System.out.println("qBOpenSyntax = " + qBOpenSyntax);
                    String qBAggregateFunction = repoTO.getqBAggregateFunction();
                    repoTO.setqBAggregateFunction(qBAggregateFunction);
                    map.put("qBAggregateFunction", qBAggregateFunction);
                    System.out.println("qBAggregateFunction = " + qBAggregateFunction);
                    String qBClosingSyntax = repoTO.getqBClosingSyntax();
                    repoTO.setqBClosingSyntax(qBClosingSyntax);
                    map.put("qBClosingSyntax", qBClosingSyntax);
                    System.out.println("qBClosingSyntax = " + qBClosingSyntax);
                    String entityParseValue = repoTO.getEntityParseValue();
                    repoTO.setEntityParseValue(entityParseValue);
                    map.put("entityParseValue", entityParseValue);
                    System.out.println("entityParseValue = " + entityParseValue);
                    updatedEntityDetails.add(repoTO);
                    status = true;
                }
            }
            //            reportTO.setEditEntityDetails(editEntityDetails);
            //            updatestatus = (Integer) getSqlMapClientTemplate().update("report.updateQBEntityDetails", reportTO);
            reportTO.setUpdatedEntityDetails(updatedEntityDetails);
            if (status) {
                System.out.println("entity id in insert = " + reportTO.getEntityId());
                insertstatus = (Integer) getSqlMapClientTemplate().update("report.insertQBEntityDetails", reportTO);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("editQBEntityDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "editQBEntityDetails", sqlException);
        }

        return insertstatus;
    }

    public ArrayList ViewQBEntityListFilter(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList ViewQBEntityListFilter = new ArrayList();
        System.out.println("I am in the ViewQBEntityListFilter DAO ");

        try {

            if (reportTO.getEntityName() != null && !"".equals(reportTO.getEntityName())) {
                map.put("entityName", "%" + reportTO.getEntityName() + "%");
            }

            if (reportTO.getEntityId() != 0 && !"".equals(reportTO.getEntityId())) {
                map.put("entityId", "%" + reportTO.getEntityId() + "%");
            }

            if (reportTO.getCreatedBy() != null && !"".equals(reportTO.getCreatedBy())) {
                map.put("createdBy", "%" + reportTO.getCreatedBy() + "%");
            }

            if (reportTO.getStatus() != null && !"".equals(reportTO.getStatus())) {
                map.put("status", "%" + reportTO.getStatus() + "%");
            }

            if (reportTO.getCreatedOn() != null && !"".equals(reportTO.getCreatedOn())) {
                map.put("createdOn", reportTO.getCreatedOn());
            }

            System.out.println("map = " + map);

            ViewQBEntityListFilter = (ArrayList) getSqlMapClientTemplate().queryForList("report.ViewQBEntityListFilter", map);
            System.out.println(" get List size= " + ViewQBEntityListFilter.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ViewQBEntityListFilter Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "ViewQBEntityListFilter", sqlException);
        }
        return ViewQBEntityListFilter;

    }

    public ArrayList viewQBReportList() throws FPRuntimeException, FPBusinessException {

        Map map = new HashMap();
        ArrayList viewQBReportList = new ArrayList();

        try {

            viewQBReportList = (ArrayList) getSqlMapClientTemplate().queryForList("report.viewQBReportList", map);
            System.out.println(" get List size= " + viewQBReportList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("viewQueryBuilder Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "viewQueryBuilder", sqlException);
        }
        return viewQBReportList;

    }

    public ArrayList getQBEntityList() throws FPRuntimeException, FPBusinessException {
        ArrayList entitylist = new ArrayList();
        Map map = new HashMap();
        try {

            entitylist = (ArrayList) getSqlMapClientTemplate().queryForList("report.getEntityList", map);
            System.out.println("Size in entitylist " + entitylist.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return entitylist;
    }

    public ArrayList viewQBReportHeader(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        System.out.println("viewQBReportHeader DAO");
        Map map = new HashMap();
        ArrayList viewQBReportHeader = new ArrayList();
        String outputquery = "";
        int queryid = 0;
        //        if(reportTO.getQueryId() != 0){
        queryid = reportTO.getQueryId();
        //        }

        try {
            map.put("queryId", queryid);
            System.out.println("map = " + map);
            viewQBReportHeader = (ArrayList) getSqlMapClientTemplate().queryForList("report.viewQBReportHeader", map);
            System.out.println(" get List size= " + viewQBReportHeader.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("viewQBReportHeader Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "viewQueryBuilder", sqlException);
        }
        return viewQBReportHeader;

    }

    public String getReportName(ReportTO reportTO) {
        Map map = new HashMap();
        String reportName = "";
        int updateQueryStatus = 0;
        try {
            map.put("queryId", reportTO);
            System.out.println("queryId = " + map);
            reportName = (String) getSqlMapClientTemplate().queryForObject("report.getReportName", reportTO);
            System.out.println("reportName----------->" + reportName);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            //             outputquery= sqlException.getMessage();
            FPLogUtils.fpDebugLog("reportName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "reportName", sqlException);
        }
        return reportName;
    }

    public String viewQBIndividualReportList(ReportTO reportTO) {

        Map map = new HashMap();
        String viewQBIndividualReportList = "";
        String outputquery = "";
        int queryid = 0;
        //        if(reportTO.getQueryId() != 0){
        //           reportTO.getQueryId();
        //        }
        //        map.put("queryid",queryid);

        try {
            //                ReportTO reportTO = new ReportTO();
            outputquery = (String) getSqlMapClientTemplate().queryForObject("report.viewQBIndividualReportList", reportTO);
            System.out.println(" get List size= " + outputquery);
            reportTO.setSelectquery(outputquery);

            viewQBIndividualReportList = (String) getSqlMapClientTemplate().queryForObject("report.queryBuilderColumn", reportTO);
            System.out.println("ReportValue----------->" + viewQBIndividualReportList);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("viewQBIndividualReportList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "viewQueryBuilder", sqlException);
        }
        return viewQBIndividualReportList;

    }

    public ArrayList getAvailableQBColumn(int entityId) throws FPRuntimeException, FPBusinessException {
        ArrayList availableFunction = new ArrayList();
        Map map = new HashMap();
        System.out.println("IN Assigned Function ");
        try {
            map.put("entityId", entityId);
            System.out.println("map" + map);
            availableFunction = (ArrayList) getSqlMapClientTemplate().queryForList("report.getAvailableQBColumn", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return availableFunction;
    }

    public ArrayList reportQBFilterValue(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        System.out.println("reportQBFilterValue DAO");
        Map map = new HashMap();
        ArrayList reportQBFilterValue = new ArrayList();
        String concatedQBreportFilterValue = "";
        String outputquery = "";
        int queryid = 0;
        //        if(reportTO.getQueryId() != 0){
        queryid = reportTO.getQueryId();
        //        }

        try {
            //                map.put("queryId",queryid);
            //                System.out.println("map = " + map);
            concatedQBreportFilterValue = (String) getSqlMapClientTemplate().queryForObject("report.reportQBFilterValue", reportTO);

            String[] concatedQBreportFilterSplit = concatedQBreportFilterValue.split("~");

            String[] tempqueryid = concatedQBreportFilterSplit[0].split(",");

            System.out.println("tempqueryid = " + concatedQBreportFilterSplit[0]);

            String[] tempConditionValue = concatedQBreportFilterSplit[1].split(",");

            System.out.println("tempConditionValue = " + concatedQBreportFilterSplit[1]);

            String[] tempColumnValue = concatedQBreportFilterSplit[2].split(",");

            System.out.println("tempColumnValue = " + concatedQBreportFilterSplit[2]);

            String[] tempOperatorValue = concatedQBreportFilterSplit[3].split(",");

            System.out.println("tempOperatorValue = " + concatedQBreportFilterSplit[3]);

            String[] tempUserValue = concatedQBreportFilterSplit[4].split(",");

            System.out.println("tempUserValue = " + concatedQBreportFilterSplit[4]);

            String[] tempentityTableName = concatedQBreportFilterSplit[5].split(",");

            String[] tempentityColumnName = concatedQBreportFilterSplit[6].split(",");

            String[] tempFilterFunction = concatedQBreportFilterSplit[7].split(",");

            String[] tempFilterFunctionEnd = concatedQBreportFilterSplit[8].split(",");

            for (int j = 0; j < tempConditionValue.length; j++) {
                ReportTO repoTO = new ReportTO();
                //                    int queryId = Integer.parseInt(tempcolumnName[0]);
                //                    System.out.println("queryId = " + queryId);
                //                    reportTO.setQueryId(queryId);
                String conditionName = tempConditionValue[j];
                repoTO.setConditionName(conditionName);
                System.out.println("conditionName = " + conditionName);
                String filterentityDisplayName = tempColumnValue[j];
                repoTO.setFilterentityDisplayName(filterentityDisplayName);
                System.out.println("filterentityDisplayName = " + filterentityDisplayName);
                String operatorValue = tempOperatorValue[j];
                repoTO.setOperatorValue(operatorValue);
                System.out.println("operatorValue = " + operatorValue);
                String userValue = tempUserValue[j];
                System.out.println("userValue = " + userValue);
                userValue = userValue.replace("$", ",");
                userValue = userValue.replace("\"", "@");
                if (userValue.contains("DATE_FORMAT")) {
                    userValue = userValue.replace("\'", "#");
                }
                //                    userValue = userValue.replace("\"", "\\\"");

                repoTO.setUserValue(userValue);
                System.out.println("userValue = " + userValue);
                String filterFunction = tempFilterFunction[j];
                repoTO.setqBfilterFunction(filterFunction);
                System.out.println("filterFunction = " + filterFunction);
                String filterFunctionEnd = tempFilterFunctionEnd[j];
                repoTO.setqBfilterFunctionEnd(filterFunctionEnd);
                System.out.println("filterFunctionEnd = " + filterFunctionEnd);
                reportQBFilterValue.add(j, repoTO);
            }

            Iterator itr2 = reportQBFilterValue.iterator();

            System.out.println(" reportQBFilterValue Size " + reportQBFilterValue.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("selectedReportQBColumn Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "selectedReportQBColumn", sqlException);
        }
        return reportQBFilterValue;

    }

    public ArrayList clientQBReportFilter(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList clientQBReportFilter = new ArrayList();
        System.out.println("clientQBReportFilter DAO");
        try {

            System.out.println("reportTO.getReportName-------------> " + reportTO.getReportName());
            System.out.println("reportTO.getEntityName()-----------> " + reportTO.getEntityName());
            System.out.println("reportTO.getMode()-----------------> " + reportTO.getMode());
            System.out.println("reportTO.getFrequency()------------> " + reportTO.getFrequency());
            System.out.println("reportTO.getExecutionInterval()----> " + reportTO.getExecutionInterval());
            System.out.println("reportTO.getEmailId()--------------> " + reportTO.getEmailId());

            if (reportTO.getReportName() != null && !"".equals(reportTO.getReportName())) {
                map.put("reportName", "%" + reportTO.getReportName() + "%");
            }

            if (reportTO.getEntityName() != null && !"".equals(reportTO.getEntityName())) {
                map.put("entityName", "%" + reportTO.getEntityName() + "%");
            }

            if (reportTO.getMode() != null && !"".equals(reportTO.getMode())) {
                map.put("mode", "%" + reportTO.getMode() + "%");
            }

            if (reportTO.getFrequency() != null && !"".equals(reportTO.getFrequency())) {
                map.put("frequency", "%" + reportTO.getFrequency() + "%");
            }

            if (reportTO.getExecutionInterval() != null && !"".equals(reportTO.getExecutionInterval())) {
                map.put("executionInterval", reportTO.getExecutionInterval());
            }

            if (reportTO.getEmailId() != null && !"".equals(reportTO.getEmailId())) {
                map.put("emailId", "%" + reportTO.getEmailId() + "%");
            }

            System.out.println("map = " + map);

            clientQBReportFilter = (ArrayList) getSqlMapClientTemplate().queryForList("report.clientQBReportFilter", map);
            System.out.println(" get List size= " + clientQBReportFilter.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("clientQBReportFilter Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "clientQBReportFilter", sqlException);
        }
        return clientQBReportFilter;

    }

    public ArrayList selectedReportQBColumn(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        System.out.println("selectedReportQBColumn DAO");
        Map map = new HashMap();
        ArrayList selectedReportQBColumn = new ArrayList();
        String concatedReportQBColumn = "";
        String outputquery = "";
        int queryid = 0;
        int entityId = 0;
//        if(reportTO.getQueryId() != 0){
        queryid = reportTO.getQueryId();
//        }
        ArrayList entityDisplayName = new ArrayList();
        ArrayList entTableName = new ArrayList();
        try {
//                map.put("queryId",queryid);
//                System.out.println("map = " + map);
            ArrayList availableColumn = new ArrayList();
            entityId = reportTO.getEntityId();
            map.put("entityId", entityId);

            System.out.println("entityId = " + entityId);

            System.out.println("map = " + map);

//                availableColumn = (ArrayList) getSqlMapClientTemplate().queryForList("report.getAvailableQBColumn", map);
//
//                System.out.println("availableFunction = " + availableColumn.size());
            concatedReportQBColumn = (String) getSqlMapClientTemplate().queryForObject("report.selectedReportQBColumn", reportTO);

            System.out.println("concatedReportQBColumn = " + concatedReportQBColumn);

            String[] concatedReportQBColumnSplit = concatedReportQBColumn.split("~");

            String[] tempqueryid = concatedReportQBColumnSplit[0].split(",");

            String[] tempcolumnName = concatedReportQBColumnSplit[1].split(",");

            String[] tempDataType = concatedReportQBColumnSplit[2].split(",");

            String[] tempentityTableName = concatedReportQBColumnSplit[3].split(",");

            String[] tempentityColumnName = concatedReportQBColumnSplit[4].split(",");
            System.out.println("tempentityColumnName = " + tempentityColumnName);

            for (int j = 0; j < tempcolumnName.length; j++) {
                System.out.println("tempcolumnName = " + tempcolumnName.length);
                ReportTO repoTO = new ReportTO();
//                    int queryId = Integer.parseInt(tempcolumnName[0]);
//                    System.out.println("queryId = " + queryId);
//                    reportTO.setQueryId(queryId);
                String columnName = tempcolumnName[j];
                repoTO.setColumnName(columnName);
                System.out.println("columnName = " + columnName);
                String dataType = tempDataType[j];
                repoTO.setDataType(Integer.parseInt(dataType));
                System.out.println("dataType = " + dataType);
                String entityTableName = tempentityTableName[j];
                repoTO.setEntityTableName(entityTableName);
                System.out.println("entityTableName = " + entityTableName);
                String entityColumnName = tempentityColumnName[j];
                repoTO.setEntityColumnName(entityColumnName);
                System.out.println("entityColumnName = " + entityColumnName);
                selectedReportQBColumn.add(j, repoTO);
            }
            Iterator itr2 = selectedReportQBColumn.iterator();
            while (itr2.hasNext()) {
                ReportTO repoTO1 = new ReportTO();
                repoTO1 = (ReportTO) itr2.next();
                String selectedColumn = repoTO1.getColumnName();
                System.out.println("SELECTEDCOLUMN = " + selectedColumn);
                System.out.println("DATATYPE = " + repoTO1.getDataType());
                System.out.println("ENTITYTABLENAME = " + repoTO1.getEntityTableName());
                System.out.println("ENTITYCOLUMNNAME = " + repoTO1.getEntityColumnName());
            }

            System.out.println(" get List size= " + selectedReportQBColumn.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("selectedReportQBColumn Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "selectedReportQBColumn", sqlException);
        }
        return selectedReportQBColumn;

    }

    public ArrayList editQBOnDemandReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        System.out.println("editQBOnDemandReport DAO");
        Map map = new HashMap();
        ArrayList editQBOndemandReport = new ArrayList();
        String outputquery = "";
        int queryid = 0;
//        if(reportTO.getQueryId() != 0){
        queryid = reportTO.getQueryId();
//        }

        try {
            map.put("queryId", queryid);
            System.out.println("map = " + map);
            editQBOndemandReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.editQBOnDemandReport", map);
            System.out.println(" get List size= " + editQBOndemandReport.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("editQBOnDemandReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "editQBOnDemandReport", sqlException);
        }
        return editQBOndemandReport;

    }

    public ArrayList viewQBEntityDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        Map map = new HashMap();
        ArrayList viewQBEntityDetails = new ArrayList();
//        System.out.println("I am in the ViewQBEntityList");
        System.out.println("viewQBEntityDetails DAO");
        try {
            int entityId = 0;
            entityId = reportTO.getEntityId();
            map.put("entityId", entityId);
            viewQBEntityDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.viewQBEntityDetails", map);
            System.out.println(" get List size= " + viewQBEntityDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ViewQBEntityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "ViewQBEntityList", sqlException);
        }
        return viewQBEntityDetails;

    }

    public ArrayList viewQBEntity(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        Map map = new HashMap();
        ArrayList viewQBEntity = new ArrayList();
//        System.out.println("I am in the ViewQBEntityList");

        try {
            int entityId = 0;
            entityId = reportTO.getEntityId();
            map.put("entityId", entityId);
            viewQBEntity = (ArrayList) getSqlMapClientTemplate().queryForList("report.viewQBEntity", map);
            System.out.println(" get List size= " + viewQBEntity.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("viewQBEntity Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "viewQBEntity", sqlException);
        }
        return viewQBEntity;

    }

    // QueryBuilder Ends Here
    public ArrayList getDailyTripPlaningDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList tripDetails = new ArrayList();
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            map.put("param", reportTO.getParam());
            System.out.println("map = " + map);
            if ("ExportExcel".equals(reportTO.getParam())) {
                tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.dailyTripPlanReportExcel", map);
            } else {
                tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.dailyTripPlanReport", map);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBPCLTransactionDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getBPCLTransactionDetails", sqlException);
        }
        return tripDetails;
    }

    public ArrayList getUserLoginList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList userLoginList = new ArrayList();
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            System.out.println("map = " + map);
            userLoginList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getUserLoginList", map);
            System.out.println("userLoginList.size" + userLoginList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserLoginList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getUserLoginList", sqlException);
        }
        return userLoginList;
    }

    public ArrayList getUserLoginActivityDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList userLoginActivityList = new ArrayList();
        try {
            map.put("userId", userId);
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            System.out.println("map = " + map);
            userLoginActivityList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getUserLoginActivityDetails", map);
            System.out.println("userLoginActivityList.size" + userLoginActivityList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("userLoginActivityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "userLoginActivityList", sqlException);
        }
        return userLoginActivityList;
    }

    public ArrayList getOrderWiseProfitDetails(ReportTO reportTO) {
        Map map = new HashMap();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        map.put("tripSheetId", reportTO.getTripSheetId());
        map.put("fleetCenterId", reportTO.getFleetCenterId());
        System.out.println("map = " + map);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getOrderWiseProfitDetails", map);
            System.out.println("getOrderWiseProfitDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrderWiseProfitDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOrderWiseProfitDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getEndedTripDetails(ReportTO reportTO) {
        Map map = new HashMap();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        map.put("vehicleId", reportTO.getVehicleId());

        System.out.println("map for Ended Trip List = " + map);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getEndedTripDetails", map);
            System.out.println("getEndedTripDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEndedTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEndedTripDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getTripExpensePrintDetails(ReportTO reportTO) {
        Map map = new HashMap();

        map.put("tripId", reportTO.getTripId());

        System.out.println("map for print Trip List = " + map);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripExpensePrintDetails", map);
            System.out.println("getTripExpensePrintDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripExpensePrintDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripExpensePrintDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getTripExpenseSummaryDetails(ReportTO reportTO) {
        Map map = new HashMap();

        map.put("tripId", reportTO.getTripId());

        System.out.println("map for print Trip List = " + map);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getExpneseSummary", map);
            System.out.println("getTripExpensePrintDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripExpenseSummaryDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripExpenseSummaryDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getTripReportDetails(ReportTO reportTO) {
        Map map = new HashMap();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        map.put("vehicleId", reportTO.getVehicleId());
        map.put("containerType", reportTO.getContainerType());
        map.put("billingPartyId", reportTO.getBillingParty());
        map.put("orderType", reportTO.getOrderTypeId());
        map.put("customerId", reportTO.getCustomerId());
        map.put("linerId", reportTO.getLinerNameId());

        System.out.println("map for getOrderDetails Trip List = " + map);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getOrderDetails", map);
            System.out.println("getOrderDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripExpensePrintDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripExpensePrintDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getTripDetailsForOrder(ReportTO reportTO) {
        Map map = new HashMap();
        System.out.println("reportTO.getNoOfTrips():" + reportTO.getNoOfTrips());
        String[] tripIds = reportTO.getTripIds().split(",");
        List tripIdsList = new ArrayList(tripIds.length);
        System.out.println("length:" + tripIds.length);
        for (int i = 0; i < tripIds.length; i++) {
            tripIdsList.add(tripIds[i]);
        }
        map.put("tripIds", tripIdsList);
        System.out.println("map for gettripOrderDetails Trip List = " + map);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getTripReportDetails", map);
            System.out.println("getOrderDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripExpensePrintDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripExpensePrintDetails List", sqlException);
        }

        return tripDetails;

    }

    public ArrayList getShipperName(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList getShipperList = new ArrayList();
        map.put("linerName", reportTO.getLinerName() + "%");
        try {
            System.out.println(" getShipperList = map is tht" + map);
            getShipperList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getShipperList", map);
            System.out.println(" getShipperList =" + getShipperList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getZoneList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getZoneList List", sqlException);
        }

        return getShipperList;
    }

    public ArrayList processGRSummaryList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList processGRSummaryList = new ArrayList();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        map.put("grNo", reportTO.getGrNo());
        map.put("containerNo", reportTO.getContainerNo());
        map.put("vehicleNo", reportTO.getVehicleNo());
        map.put("movementType", reportTO.getMovementType());
        map.put("tripStatus", reportTO.getTripStatus());
        System.out.println("map" + map);
        try {
            System.out.println(" processGRSummaryList = map is tht" + map);
            processGRSummaryList = (ArrayList) getSqlMapClientTemplate().queryForList("report.processGRSummaryList", map);
            System.out.println(" processGRSummaryList =" + processGRSummaryList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("processGRSummaryList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "processGRSummaryList List", sqlException);
        }

        return processGRSummaryList;
    }

    public ArrayList dailyVehicleStatus(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList dailyVehicleStatusList = new ArrayList();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        map.put("vendorId", reportTO.getVendorId());
        System.out.println("map " + map);
        try {
            System.out.println(" dailyVehicleStatus = map is tht" + map);
            dailyVehicleStatusList = (ArrayList) getSqlMapClientTemplate().queryForList("report.dailyVehicleStatus", map);
            System.out.println(" dailyVehicleStatus =" + dailyVehicleStatusList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("processGRSummaryList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "processGRSummaryList List", sqlException);
        }

        return dailyVehicleStatusList;
    }

    public ArrayList getVendorList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList vendorList = new ArrayList();

        try {
            System.out.println(" getVendorList = map is tht" + map);
            vendorList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVendorList", map);
            System.out.println(" getVendorList =" + vendorList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("processGRSummaryList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "processGRSummaryList List", sqlException);
        }

        return vendorList;
    }

    public String getTripDieselCost(ReportTO reportTO) {
        Map map = new HashMap();
        String dieselCost = "";
        try {

            map.put("tripId", reportTO.getTripId());

            dieselCost = (String) getSqlMapClientTemplate().queryForObject("report.getTripDieselCost", map);
            System.out.println("dieselCost" + dieselCost);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehicleLastStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "vehicleLastStatus", sqlException);
        }
        return dieselCost;
    }

    public ArrayList getVehicleVendorwiseList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList customerWiseProfitList = new ArrayList();
        try {
            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            System.out.println("mapgetVehicleVendorwiseList = " + map);
            customerWiseProfitList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleVendorwiseList", map);
            System.out.println("customerWiseProfitList" + customerWiseProfitList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleVendorwiseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getCustomerWiseProfitList", sqlException);
        }
        return customerWiseProfitList;
    }

    public ArrayList getDailyCashDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList dailyCashDetails = new ArrayList();
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());

            System.out.println("dailyCashDetails = " + map);
            dailyCashDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.dailyCashDetails", map);
            System.out.println("dailyCashDetails" + dailyCashDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleVendorwiseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getCustomerWiseProfitList", sqlException);
        }
        return dailyCashDetails;
    }

    public String getCashReceiveDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList dailyCashDetails = new ArrayList();
        String cashReceived = "";
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            cashReceived = (String) getSqlMapClientTemplate().queryForObject("report.cashReceive", map);
            System.out.println("cashReceived" + cashReceived);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleVendorwiseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getCustomerWiseProfitList", sqlException);
        }
        return cashReceived;
    }

    public ArrayList processGRprofitabilityList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList processGRprofitabilityList = new ArrayList();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        System.out.println("map" + map);
        try {
            System.out.println(" processGRprofitabilityList = map is tht" + map);
            processGRprofitabilityList = (ArrayList) getSqlMapClientTemplate().queryForList("report.processGRprofitabilityList", map);
            System.out.println(" processGRprofitabilityList===" + processGRprofitabilityList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("processGRprofitabilityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "processGRprofitabilityList List", sqlException);
        }

        return processGRprofitabilityList;
    }

    public String getOrdersDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList dailyCashDetails = new ArrayList();
        String ordersPlaced = "";
        String openTrip = "";
        String closedTrip = "";
        String invoicegenerated = "";
        String Orderdetails = "", totalDays = "";
        String totalOrderInCurrentMonth = "", totalOpenTripInCurrentMonth = "", totalClosedTripInCurrentMonth = "", totalInvoiceInCurrentMonth = "";
        String totalOrderInPreviousMonth = "", totalOpenTripInPreviousMonth = "", totalClosedTripInPreviousMonth = "", totalInvoiceInPreviousMonth = "";
        try {
            map.put("fromDate", reportTO.getFromDate());
            System.out.println("map order===" + map);
            ordersPlaced = (String) getSqlMapClientTemplate().queryForObject("report.ordersPlaced", map);
            openTrip = (String) getSqlMapClientTemplate().queryForObject("report.openTripDetails", map);
            closedTrip = (String) getSqlMapClientTemplate().queryForObject("report.closedTripDetails", map);
            invoicegenerated = (String) getSqlMapClientTemplate().queryForObject("report.invoiceGeneratedDetails", map);
            totalOrderInCurrentMonth = (String) getSqlMapClientTemplate().queryForObject("report.totalOrdersPlacedInCurrentMonth", map);
            totalOpenTripInCurrentMonth = (String) getSqlMapClientTemplate().queryForObject("report.tottalOpenTripDetailsInCurrentMonth", map);
            totalClosedTripInCurrentMonth = (String) getSqlMapClientTemplate().queryForObject("report.totalClosedTripDetailsInCurrentMonth", map);
            totalInvoiceInCurrentMonth = (String) getSqlMapClientTemplate().queryForObject("report.totalInvoiceGeneratedDetailsInCurrentMonth", map);
            totalOrderInPreviousMonth = (String) getSqlMapClientTemplate().queryForObject("report.totalOrdersPlacedInPreviousMonth", map);
            totalOpenTripInPreviousMonth = (String) getSqlMapClientTemplate().queryForObject("report.tottalOpenTripDetailsInPreviousMonth", map);
            totalClosedTripInPreviousMonth = (String) getSqlMapClientTemplate().queryForObject("report.totalClosedTripDetailsInPreviousMonth", map);
            totalInvoiceInPreviousMonth = (String) getSqlMapClientTemplate().queryForObject("report.totalInvoiceGeneratedDetailsInPreviousMonth", map);
            totalDays = (String) getSqlMapClientTemplate().queryForObject("report.totaldays", map);

            Orderdetails = ordersPlaced + "~" + openTrip + "~" + closedTrip + "~" + invoicegenerated + "~" + totalOrderInCurrentMonth + "~" + totalOpenTripInCurrentMonth + "~" + totalClosedTripInCurrentMonth + "~" + totalInvoiceInCurrentMonth
                    + "~" + totalOrderInPreviousMonth + "~" + totalOpenTripInPreviousMonth + "~" + totalClosedTripInPreviousMonth + "~" + totalInvoiceInPreviousMonth + "~" + totalDays;
            System.out.println("Orderdetails==" + Orderdetails);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ordersPlaced Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "ordersPlaced", sqlException);
        }
        return Orderdetails;
    }

    public ArrayList getContainerMovementList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList containerEmptyMovementList = new ArrayList();
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
//            map.put("month", reportTO.getMonthId());
            System.out.println("getemptyContainerTripDetails = " + map);
            containerEmptyMovementList = (ArrayList) getSqlMapClientTemplate().queryForList("report.emptyContainerTripDetails", map);
            System.out.println("containerEmptyMovementList" + containerEmptyMovementList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("containerEmptyMovementList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "containerEmptyMovementList", sqlException);
        }
        return containerEmptyMovementList;
    }

    public ArrayList getcontainerExportMovementList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList containerExportMovementList = new ArrayList();
        try {
//            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            System.out.println("getcontainerExportMovementList = " + map);

            containerExportMovementList = (ArrayList) getSqlMapClientTemplate().queryForList("report.exportContainerTripDetails", map);
            System.out.println("containerExportMovementList" + containerExportMovementList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("containerEmptyMovementList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "containerEmptyMovementList", sqlException);
        }
        return containerExportMovementList;
    }

    public ArrayList getcontainerImportMovementList(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList containerImportMovementList = new ArrayList();
        try {
//            map.put("toDate", reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            System.out.println("getcontainerImportMovementList = " + map);
            containerImportMovementList = (ArrayList) getSqlMapClientTemplate().queryForList("report.importContainerTripDetails", map);
            System.out.println("containerImportMovementList" + containerImportMovementList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("containerEmptyMovementList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "containerEmptyMovementList", sqlException);
        }
        return containerImportMovementList;
    }

    public ArrayList getVehicleDetainDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList VehicleDetainDetails = new ArrayList();
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            System.out.println("VehicleDetainDetails = " + map);

            VehicleDetainDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.vehicleDetainDetails", map);
            System.out.println("VehicleDetainDetails" + VehicleDetainDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("VehicleDetainDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "VehicleDetainDetails", sqlException);
        }
        return VehicleDetainDetails;
    }

    public ArrayList getInvoiceEditLogDetails(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList invoiceEditLogDetails = new ArrayList();
        try {
            map.put("grNo", reportTO.getGrNo());
            System.out.println("invoiceEditLogDetails = " + map);

            invoiceEditLogDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getInvoiceEditLogDetails", map);
            System.out.println("invoiceEditLogDetails" + invoiceEditLogDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("invoiceEditLogDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "invoiceEditLogDetails", sqlException);
        }
        return invoiceEditLogDetails;
    }

    public ArrayList tripStatusList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList tripStatusList = new ArrayList();

        System.out.println("map" + map);
        try {
            System.out.println(" tripStatusList = map is tht" + map);
            tripStatusList = (ArrayList) getSqlMapClientTemplate().queryForList("report.tripStatusList", map);
            System.out.println(" tripStatusList =" + tripStatusList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("tripStatusList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "tripStatusList List", sqlException);
        }

        return tripStatusList;
    }

    public ArrayList getGpsVehiclelist(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList gpsVehiclelist = new ArrayList();
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            System.out.println("map = " + map);
            gpsVehiclelist = (ArrayList) getSqlMapClientTemplate().queryForList("report.getGpsVehiclelist", map);
            System.out.println("gpsVehiclelist.size" + gpsVehiclelist.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getGpsVehiclelist Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getGpsVehiclelist", sqlException);
        }
        return gpsVehiclelist;
    }

    public ArrayList invoiceDetailsReport(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList invoiceDetailsReport = new ArrayList();
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            map.put("type", reportTO.getType());

            System.out.println("invoiceDetailsReport = " + map);
            invoiceDetailsReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.invoiceDetailsReport", map);
            System.out.println("dailyCashDetails" + invoiceDetailsReport.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleVendorwiseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getCustomerWiseProfitList", sqlException);
        }
        return invoiceDetailsReport;
    }

    public ArrayList suppInvoiceDetailsReport(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList invoiceDetailsReport = new ArrayList();
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            // map.put("type", reportTO.getType());

            System.out.println("invoiceDetailsReport = " + map);
            invoiceDetailsReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.suppInvoiceDetailsReport", map);
            System.out.println("suppInvoiceDetailsReport size" + invoiceDetailsReport.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleVendorwiseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getCustomerWiseProfitList", sqlException);
        }
        return invoiceDetailsReport;
    }

    public int saveInvoiceSubmissionDate(String[] invSubmissionDate, String[] podDate, String[] invoiceIds, int userId) {
        Map map = new HashMap();
        int status = 0;
        try {
            for (int i = 0; i < invSubmissionDate.length; i++) {
                if (invSubmissionDate[i] != null && !"".equalsIgnoreCase(invSubmissionDate[i])) {
                    map.put("invSubmissionDate", invSubmissionDate[i]);
                    map.put("podDate", podDate[i]);
                    map.put("invoiceId", invoiceIds[i]);

                    System.out.println("invSubmissionDate map = " + map);

                    status = (Integer) getSqlMapClientTemplate().update("report.updateInvoiceSubmissionDate", map);
                    System.out.println("status" + status);
                }
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleVendorwiseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getCustomerWiseProfitList", sqlException);
        }
        return status;
    }

    public String getInvoiceId(String invoiceCode) {
        Map map = new HashMap();
        int status = 0;
        String InvoiceId = "";
        try {

            map.put("invoiceCode", invoiceCode);

            System.out.println("invSubmissionDate map = " + map);

            InvoiceId = (String) getSqlMapClientTemplate().queryForObject("report.getInvoiceId", map);
            System.out.println("InvoiceId" + InvoiceId);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleVendorwiseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getCustomerWiseProfitList", sqlException);
        }
        return InvoiceId;
    }

    public ArrayList getContractRateEditLogDetails(ReportTO reportTO, int userId) {

        Map map = new HashMap();
        ArrayList contractRateEditLogDetails = new ArrayList();
        try {
            map.put("customerId", reportTO.getCustomerId());
            System.out.println("invoiceEditLogDetails = " + map);

            contractRateEditLogDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getContractRateEditLogDetails", map);
            System.out.println("contractRateEditLogDetails" + contractRateEditLogDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("contractRateEditLogDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "contractRateEditLogDetails", sqlException);
        }
        return contractRateEditLogDetails;
    }

    public ArrayList getBlockedGrSummaryDetails(ReportTO reportTO) {

        Map map = new HashMap();
        ArrayList grSummaryList = new ArrayList();
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            map.put("customerId", reportTO.getCustomerId());
            System.out.println("getBlockedGrDetails = " + map);

            grSummaryList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getBlockedGrSummaryDetails", map);
            System.out.println("grSummaryList" + grSummaryList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBlockedGrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getBlockedGrDetails", sqlException);
        }
        return grSummaryList;
    }

//    public ArrayList getInvoiceReport(ReportTO reportTO, String status, int userId) {
//        Map map = new HashMap();
//        System.out.println("IN dao....");
//        ArrayList invoiceReport = new ArrayList();
//        try {
//            map.put("reportTypeId", reportTO.getReportTypeId());
//            map.put("fromDate", reportTO.getFromDate());
//            map.put("toDate", reportTO.getToDate());
//            map.put("customerId", reportTO.getCustomerId());
//            map.put("status", status);
//            map.put("movTypeId", reportTO.getMovTypeId());
//            map.put("sbBillBoeBill", reportTO.getSbBillBoeBill());
//
//            System.out.println("map = " + map);
//            if ("invoiceGr".equals(status)) {
//                invoiceReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.dailyInvoiceReport", map);
//                System.out.println("invoiceReport.size===" + invoiceReport.size());
//            }
//            if ("pendingGr".equals(status)) {
//                invoiceReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.pendingInvoiceReport", map);
//                System.out.println("invoiceReport.size===" + invoiceReport.size());
//            }
//        } catch (Exception sqlException) {
//            sqlException.printStackTrace();
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("dailyInvoiceReport Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-GEN-01", CLASS, "invoiceReport", sqlException);
//        }
//        return invoiceReport;
//    }
    public ArrayList getDispatchReport(ReportTO reportTO, int userId, String param) {
        Map map = new HashMap();
        ArrayList dispatchReport = new ArrayList();
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            map.put("param", param);
            System.out.println("mapAAA = " + map);
            if ("Search".equals(param)) {
                dispatchReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.dailyDispatchReport", map);
                System.out.println("dispatchReport===" + dispatchReport.size());
            } else {
                dispatchReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.dailyDispatchExcelReport", map);
                System.out.println("dispatchReportExcel===" + dispatchReport.size());
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("dailyDispatchReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "dailyDispatchReport", sqlException);
        }
        return dispatchReport;
    }

    public ArrayList getExpenseTypeList() {
        Map map = new HashMap();

        ArrayList expenseList = new ArrayList();
        try {
            expenseList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getExpenseTypeList", map);
            System.out.println("getExpenseTypeList size=" + expenseList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripExpenseSummaryDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripExpenseSummaryDetails List", sqlException);
        }

        return expenseList;
    }

    public ArrayList getGrExpenseDetails(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList grExpenseDetails = new ArrayList();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        System.out.println("map" + map);
        try {
            System.out.println(" getGrExpenseDetails = map is tht" + map);
            grExpenseDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getGrExpenseDetails", map);
            System.out.println(" getGrExpenseDetails===" + grExpenseDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("processGRprofitabilityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "processGRprofitabilityList List", sqlException);
        }

        return grExpenseDetails;
    }

    public ArrayList getFuelStat(String fromDate, String toDate) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        ArrayList fuelData = new ArrayList();

        try {
            fuelData = (ArrayList) getSqlMapClientTemplate().queryForList("report.getFuelData", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehComparisionReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehComparisionReport", sqlException);
        }
        return fuelData;
    }

    public ArrayList creditNoteDetailsReport(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList invoiceDetailsReport = new ArrayList();
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
//            map.put("type", reportTO.getType());

            System.out.println("creditNoteDetailsReport = " + map);
            invoiceDetailsReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.creditNoteDetailsReport", map);
            System.out.println("creditNoteDetailsReport====" + invoiceDetailsReport.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleVendorwiseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getCustomerWiseProfitList", sqlException);
        }
        return invoiceDetailsReport;
    }

    public ArrayList grStatusReport(String grNo, String containerNo, int userId) {
        Map map = new HashMap();
        ArrayList grStatusReport = new ArrayList();
        try {
            map.put("grNo", "%" + grNo + "%");
            if (containerNo != null && !"".equalsIgnoreCase(containerNo)) {
                map.put("containerNo", "%" + containerNo + "%");
            } else {
                map.put("containerNo", containerNo);
            }

//            map.put("type", reportTO.getType());
            System.out.println("grStatusReport = " + map);
            grStatusReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.grStatusReport", map);
            System.out.println("grStatusReport====" + grStatusReport.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleVendorwiseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getCustomerWiseProfitList", sqlException);
        }
        return grStatusReport;
    }

    public ArrayList tripEndedNotClosedBeyond24(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList tripList = new ArrayList();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        System.out.println("map" + map);
        try {
            System.out.println(" processGRprofitabilityList = map is tht" + map);
            tripList = (ArrayList) getSqlMapClientTemplate().queryForList("report.tripEndedNotClosedBeyond24", map);
            System.out.println(" tripEndedNotClosedBeyond24===" + tripList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("processGRprofitabilityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "processGRprofitabilityList List", sqlException);
        }

        return tripList;
    }

    public Map payablePartReport(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList tripList = new ArrayList();
        String openGr = "", issuedGr = "", closedGr = "", lessthn7daysGrs = "", expenseDuringDay = "", pendingExpense = "", sevenDayExpense = "";
        ArrayList<String> statusList = new ArrayList<String>();
        statusList.add("own");
        statusList.add("leased");
        statusList.add("hire");
        Map<String, String> openingGr = new HashMap<String, String>();
        String[] load = new String[]{"1", "2"};
        String[] dso = new String[]{"5", "4"};
        String[] empty = new String[]{"3"};
        String[] btt = new String[]{"6"};
        List<String[]> movenmentType = new ArrayList<String[]>();
        movenmentType.add(load); //export/import
        movenmentType.add(dso); // dso
        movenmentType.add(empty); //empty
        movenmentType.add(btt); // back to town
        map.put("movementType", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        System.out.println("map" + map);
        try {
            for (String statusType : statusList) {
                for (String[] mvType : movenmentType) {
                    map.put("movementType", mvType);
                    System.out.println(" openGr = map is tht" + map + "for status type:" + statusType);
                    // total open gr
                    ;
                    if ("own".equalsIgnoreCase(statusType)) {
                        openGr = (String) getSqlMapClientTemplate().queryForObject("report.openingGrOwn", map);
                        System.out.println("openGr" + openGr);
                        if (btt.equals(mvType)) {
                            openingGr.put("own-btt-openGR", openGr);
                        } else if (empty.equals(mvType)) {
                            openingGr.put("own-empty-openGR", openGr);
                        } else if (load.equals(mvType)) {
                            openingGr.put("own-load-openGR", openGr);
                        } else if (dso.equals(mvType)) {
                            openingGr.put("own-dso-openGR", openGr);
                        }
                    }
                    System.out.println("openingGr own 111" + openingGr);
                    if ("leased".equalsIgnoreCase(statusType)) {
                        openGr = (String) getSqlMapClientTemplate().queryForObject("report.openingGrLeased", map);
                        if (btt.equals(mvType)) {
                            openingGr.put("leased-btt-openGR", openGr);
                        } else if (empty.equals(mvType)) {
                            openingGr.put("leased-empty-openGR", openGr);
                        } else if (load.equals(mvType)) {
                            openingGr.put("leased-load-openGR", openGr);
                        } else if (dso.equals(mvType)) {
                            openingGr.put("leased-dso-openGR", openGr);
                        }
                    }
                    System.out.println("openingGr leased 111" + openingGr);
                    if ("hire".equalsIgnoreCase(statusType)) {
                        openGr = (String) getSqlMapClientTemplate().queryForObject("report.openingGrHire", map);
                        if (btt.equals(mvType)) {
                            openingGr.put("hire-btt-openGR", openGr);
                        } else if (empty.equals(mvType)) {
                            openingGr.put("hire-empty-openGR", openGr);
                        } else if (load.equals(mvType)) {
                            openingGr.put("hire-load-openGR", openGr);
                        } else if (dso.equals(mvType)) {
                            openingGr.put("hire-dso-openGR", openGr);
                        }
                    }
                    //  gr issued

                    System.out.println("issuedGr" + issuedGr);
                    if ("own".equalsIgnoreCase(statusType)) {
                        issuedGr = (String) getSqlMapClientTemplate().queryForObject("report.grIssuedOwn", map);
                        if (btt.equals(mvType)) {
                            openingGr.put("own-btt-issueGR", issuedGr);
                        } else if (empty.equals(mvType)) {
                            openingGr.put("own-empty-issueGR", issuedGr);
                        } else if (load.equals(mvType)) {
                            openingGr.put("own-load-issueGR", issuedGr);
                        } else if (dso.equals(mvType)) {
                            openingGr.put("own-dso-issueGR", issuedGr);
                        }
                    }
                    System.out.println("openingGr own 222" + openingGr);
                    if ("leased".equalsIgnoreCase(statusType)) {
                        issuedGr = (String) getSqlMapClientTemplate().queryForObject("report.grIssuedLeased", map);
                        if (btt.equals(mvType)) {
                            openingGr.put("leased-btt-issueGR", issuedGr);
                        } else if (empty.equals(mvType)) {
                            openingGr.put("leased-empty-issueGR", issuedGr);
                        } else if (load.equals(mvType)) {
                            openingGr.put("leased-load-issueGR", issuedGr);
                        } else if (dso.equals(mvType)) {
                            openingGr.put("leased-dso-issueGR", issuedGr);
                        }
                    }
                    System.out.println("openingGr leased 222" + openingGr);
                    if ("hire".equalsIgnoreCase(statusType)) {
                        issuedGr = (String) getSqlMapClientTemplate().queryForObject("report.grIssuedHire", map);
                        if (btt.equals(mvType)) {
                            openingGr.put("hire-btt-issueGR", issuedGr);
                        } else if (empty.equals(mvType)) {
                            openingGr.put("hire-empty-issueGR", issuedGr);
                        } else if (load.equals(mvType)) {
                            openingGr.put("hire-load-issueGR", issuedGr);
                        } else if (dso.equals(mvType)) {
                            openingGr.put("hire-dso-issueGR", issuedGr);
                        }
                    }
                    // closed gr

                    System.out.println("closedGr" + closedGr);
                    if ("own".equalsIgnoreCase(statusType)) {
                        closedGr = (String) getSqlMapClientTemplate().queryForObject("report.grClosedOwn", map);
                        if (btt.equals(mvType)) {
                            openingGr.put("own-btt-grClosed", closedGr);
                        } else if (empty.equals(mvType)) {
                            openingGr.put("own-empty-grClosed", closedGr);
                        } else if (load.equals(mvType)) {
                            openingGr.put("own-load-grClosed", closedGr);
                        } else if (dso.equals(mvType)) {
                            openingGr.put("own-dso-grClosed", closedGr);
                        }
                    }

                    if ("leased".equalsIgnoreCase(statusType)) {
                        closedGr = (String) getSqlMapClientTemplate().queryForObject("report.grClosedLeased", map);
                        if (btt.equals(mvType)) {
                            openingGr.put("leased-btt-grClosed", closedGr);
                        } else if (empty.equals(mvType)) {
                            openingGr.put("leased-empty-grClosed", closedGr);
                        } else if (load.equals(mvType)) {
                            openingGr.put("leased-load-grClosed", closedGr);
                        } else if (dso.equals(mvType)) {
                            openingGr.put("leased-dso-grClosed", closedGr);
                        }
                    }

                    if ("hire".equalsIgnoreCase(statusType)) {
                        closedGr = (String) getSqlMapClientTemplate().queryForObject("report.grClosedHire", map);
                        if (btt.equals(mvType)) {
                            openingGr.put("hire-btt-grClosed", closedGr);
                        } else if (empty.equals(mvType)) {
                            openingGr.put("hire-empty-grClosed", closedGr);
                        } else if (load.equals(mvType)) {
                            openingGr.put("hire-load-grClosed", closedGr);
                        } else if (dso.equals(mvType)) {
                            openingGr.put("hire-dso-grClosed", closedGr);
                        }
                    }
                    // gr not closed less thn 7 days

                    if ("own".equalsIgnoreCase(statusType)) {
                        lessthn7daysGrs = (String) getSqlMapClientTemplate().queryForObject("report.grNotClosedlessthn7daysOwn", map);
                        System.out.println("lessthn7daysGrs" + lessthn7daysGrs);
                        if (btt.equals(mvType)) {
                            openingGr.put("own-btt-grlessthn7", lessthn7daysGrs);
                        } else if (empty.equals(mvType)) {
                            openingGr.put("own-empty-grlessthn7", lessthn7daysGrs);
                        } else if (load.equals(mvType)) {
                            openingGr.put("own-load-grlessthn7", lessthn7daysGrs);
                        } else if (dso.equals(mvType)) {
                            openingGr.put("own-dso-grlessthn7", lessthn7daysGrs);
                        }
                    }
                    System.out.println("openingGr own 333" + openingGr);
                    if ("leased".equalsIgnoreCase(statusType)) {
                        lessthn7daysGrs = (String) getSqlMapClientTemplate().queryForObject("report.grNotClosedlessthn7daysLeased", map);
                        System.out.println("lessthn7daysGrs" + lessthn7daysGrs);
                        if (btt.equals(mvType)) {
                            openingGr.put("leased-btt-grlessthn7", lessthn7daysGrs);
                        } else if (empty.equals(mvType)) {
                            openingGr.put("leased-empty-grlessthn7", lessthn7daysGrs);
                        } else if (load.equals(mvType)) {
                            openingGr.put("leased-load-grlessthn7", lessthn7daysGrs);
                        } else if (dso.equals(mvType)) {
                            openingGr.put("leased-dso-grlessthn7", lessthn7daysGrs);
                        }
                    }

                    if ("hire".equalsIgnoreCase(statusType)) {
                        lessthn7daysGrs = (String) getSqlMapClientTemplate().queryForObject("report.grNotClosedlessthn7daysHire", map);
                        System.out.println("lessthn7daysGrs" + lessthn7daysGrs);
                        if (btt.equals(mvType)) {
                            openingGr.put("hire-btt-grlessthn7", lessthn7daysGrs);
                        } else if (empty.equals(mvType)) {
                            openingGr.put("hire-empty-grlessthn7", lessthn7daysGrs);
                        } else if (load.equals(mvType)) {
                            openingGr.put("hire-load-grlessthn7", lessthn7daysGrs);
                        } else if (dso.equals(mvType)) {
                            openingGr.put("hire-dso-grlessthn7", lessthn7daysGrs);
                        }
                    }
                    // expesne booked during day

                    if ("own".equalsIgnoreCase(statusType)) {
                        expenseDuringDay = (String) getSqlMapClientTemplate().queryForObject("report.expenseDuringDayOwn", map);
                        System.out.println("expenseDuringDay" + expenseDuringDay);
                        if (btt.equals(mvType)) {
                            openingGr.put("own-btt-expenseDuringDay", expenseDuringDay);
                        } else if (empty.equals(mvType)) {
                            openingGr.put("own-empty-expenseDuringDay", expenseDuringDay);
                        } else if (load.equals(mvType)) {
                            openingGr.put("own-load-expenseDuringDay", expenseDuringDay);
                        } else if (dso.equals(mvType)) {
                            openingGr.put("own-dso-expenseDuringDay", expenseDuringDay);
                        }
                    }
                    System.out.println("openingGr own 444" + openingGr);
                    if ("leased".equalsIgnoreCase(statusType)) {
                        expenseDuringDay = (String) getSqlMapClientTemplate().queryForObject("report.expenseDuringDayLeased", map);
                        System.out.println("expenseDuringDay" + expenseDuringDay);
                        if (btt.equals(mvType)) {
                            openingGr.put("leased-btt-expenseDuringDay", expenseDuringDay);
                        } else if (empty.equals(mvType)) {
                            openingGr.put("leased-empty-expenseDuringDay", expenseDuringDay);
                        } else if (load.equals(mvType)) {
                            openingGr.put("leased-load-expenseDuringDay", expenseDuringDay);
                        } else if (dso.equals(mvType)) {
                            openingGr.put("leased-dso-expenseDuringDay", expenseDuringDay);
                        }
                    }

                    if ("hire".equalsIgnoreCase(statusType)) {
                        expenseDuringDay = (String) getSqlMapClientTemplate().queryForObject("report.expenseDuringDayHire", map);
                        System.out.println("expenseDuringDay" + expenseDuringDay);
                        if (btt.equals(mvType)) {
                            openingGr.put("hire-btt-expenseDuringDay", expenseDuringDay);
                        } else if (empty.equals(mvType)) {
                            openingGr.put("hire-empty-expenseDuringDay", expenseDuringDay);
                        } else if (load.equals(mvType)) {
                            openingGr.put("hire-load-expenseDuringDay", expenseDuringDay);
                        } else if (dso.equals(mvType)) {
                            openingGr.put("hire-dso-expenseDuringDay", expenseDuringDay);
                        }
                    }
                    //pending expense

                    if ("own".equalsIgnoreCase(statusType)) {
                        pendingExpense = (String) getSqlMapClientTemplate().queryForObject("report.pendingExpenseOwn", map);
                        System.out.println("pendingExpense" + pendingExpense);
                        if (btt.equals(mvType)) {
                            openingGr.put("own-btt-pendingExpense", pendingExpense);
                        } else if (empty.equals(mvType)) {
                            openingGr.put("own-empty-pendingExpense", pendingExpense);
                        } else if (load.equals(mvType)) {
                            openingGr.put("own-load-pendingExpense", pendingExpense);
                        } else if (dso.equals(mvType)) {
                            openingGr.put("own-dso-pendingExpense", pendingExpense);
                        }
                    }

                    if ("leased".equalsIgnoreCase(statusType)) {
                        pendingExpense = (String) getSqlMapClientTemplate().queryForObject("report.pendingExpenseLeased", map);
                        System.out.println("expenseDuringDay" + pendingExpense);
                        if (btt.equals(mvType)) {
                            openingGr.put("leased-btt-pendingExpense", pendingExpense);
                        } else if (empty.equals(mvType)) {
                            openingGr.put("leased-empty-pendingExpense", pendingExpense);
                        } else if (load.equals(mvType)) {
                            openingGr.put("leased-load-pendingExpense", pendingExpense);
                        } else if (dso.equals(mvType)) {
                            openingGr.put("leased-dso-pendingExpense", pendingExpense);
                        }
                    }

                    if ("hire".equalsIgnoreCase(statusType)) {
                        pendingExpense = (String) getSqlMapClientTemplate().queryForObject("report.pendingExpenseHire", map);
                        System.out.println("pendingExpense" + pendingExpense);
                        if (btt.equals(mvType)) {
                            openingGr.put("hire-btt-pendingExpense", pendingExpense);
                        } else if (empty.equals(mvType)) {
                            openingGr.put("hire-empty-pendingExpense", pendingExpense);
                        } else if (load.equals(mvType)) {
                            openingGr.put("hire-load-pendingExpense", pendingExpense);
                        } else if (dso.equals(mvType)) {
                            openingGr.put("hire-dso-pendingExpense", pendingExpense);
                        }
                    }
                    // seven days pending expense

                    if ("own".equalsIgnoreCase(statusType)) {
                        sevenDayExpense = (String) getSqlMapClientTemplate().queryForObject("report.sevenDaysPendingExpenseOwn", map);
                        System.out.println("sevenDayExpense" + sevenDayExpense);
                        if (btt.equals(mvType)) {
                            openingGr.put("own-btt-pendingSevenDayExpense", sevenDayExpense);
                        } else if (empty.equals(mvType)) {
                            openingGr.put("own-empty-pendingSevenDayExpense", sevenDayExpense);
                        } else if (load.equals(mvType)) {
                            openingGr.put("own-load-pendingSevenDayExpense", sevenDayExpense);
                        } else if (dso.equals(mvType)) {
                            openingGr.put("own-dso-pendingSevenDayExpense", sevenDayExpense);
                        }
                    }

                    if ("leased".equalsIgnoreCase(statusType)) {
                        sevenDayExpense = (String) getSqlMapClientTemplate().queryForObject("report.sevenDaysPendingExpenseLeased", map);
                        System.out.println("sevenDayExpense" + sevenDayExpense);
                        if (btt.equals(mvType)) {
                            openingGr.put("leased-btt-pendingSevenDayExpense", sevenDayExpense);
                        } else if (empty.equals(mvType)) {
                            openingGr.put("leased-empty-pendingSevenDayExpense", sevenDayExpense);
                        } else if (load.equals(mvType)) {
                            openingGr.put("leased-load-pendingSevenDayExpense", sevenDayExpense);
                        } else if (dso.equals(mvType)) {
                            openingGr.put("leased-dso-pendingSevenDayExpense", sevenDayExpense);
                        }
                    }

                    if ("hire".equalsIgnoreCase(statusType)) {
                        sevenDayExpense = (String) getSqlMapClientTemplate().queryForObject("report.sevenDaysPendingExpenseHire", map);
                        System.out.println("sevenDayExpense" + sevenDayExpense);
                        if (btt.equals(mvType)) {
                            openingGr.put("hire-btt-pendingSevenDayExpense", sevenDayExpense);
                        } else if (empty.equals(mvType)) {
                            openingGr.put("hire-empty-pendingSevenDayExpense", sevenDayExpense);
                        } else if (load.equals(mvType)) {
                            openingGr.put("hire-load-pendingSevenDayExpense", sevenDayExpense);
                        } else if (dso.equals(mvType)) {
                            openingGr.put("hire-dso-pendingSevenDayExpense", sevenDayExpense);
                        }
                    }

                }
            }
            System.out.println("openingGr:" + openingGr);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("processGRprofitabilityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "processGRprofitabilityList List", sqlException);
        }

        return openingGr;
    }

    public Map receivablePartReport(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList tripList = new ArrayList();
        String openGr = "", issuedGr = "", closedGr = "", lessthn7daysGrs = "", expenseDuringDay = "", pendingExpense = "", billedGr = "";
        String totalInvAmtToParty = "", totalInvAmtNotToParty = "", pendingInvAmtToParty = "", pendingInvAmtNotToParty = "";
        ArrayList<String> statusList = new ArrayList<String>();
        statusList.add("own");
        statusList.add("leased");
        statusList.add("hire");
        Map<String, String> openingGr = new HashMap<String, String>();
        String[] load = new String[]{"1", "2"};
        String[] dso = new String[]{"5", "4"};
        String[] empty = new String[]{"3"};
        String[] btt = new String[]{"6"};
        List<String[]> movenmentType = new ArrayList<String[]>();
        movenmentType.add(load); //export/import
        movenmentType.add(dso); // dso
        movenmentType.add(empty); //empty
        movenmentType.add(btt); // back to town
        map.put("movementType", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        System.out.println("map" + map);
        try {

            for (String[] mvType : movenmentType) {
                map.put("movementType", mvType);
                // total open gr excluding  curdate 
                ;

                openGr = (String) getSqlMapClientTemplate().queryForObject("report.openingBillingToPartyGrOwn", map);
                System.out.println("openGr" + openGr);
                if (btt.equals(mvType)) {
                    openingGr.put("own-btt-billing-openGR", openGr);
                } else if (empty.equals(mvType)) {
                    openingGr.put("own-empty-billing-openGR", openGr);
                } else if (load.equals(mvType)) {
                    openingGr.put("own-load-billing-openGR", openGr);
                } else if (dso.equals(mvType)) {
                    openingGr.put("own-dso-billing-openGR", openGr);
                }

                System.out.println("openingGr own 111" + openingGr);

                //  gr issued curdate
                System.out.println("issuedGr" + issuedGr);

                issuedGr = (String) getSqlMapClientTemplate().queryForObject("report.grIssuedBillingToPartyOwn", map);
                if (btt.equals(mvType)) {
                    openingGr.put("own-btt-billing-issueGR", issuedGr);
                } else if (empty.equals(mvType)) {
                    openingGr.put("own-empty-billing-issueGR", issuedGr);
                } else if (load.equals(mvType)) {
                    openingGr.put("own-load-billing-issueGR", issuedGr);
                } else if (dso.equals(mvType)) {
                    openingGr.put("own-dso-billing-issueGR", issuedGr);
                }

                // billed gr
                billedGr = (String) getSqlMapClientTemplate().queryForObject("report.grBilledOwn", map);
                if (btt.equals(mvType)) {
                    openingGr.put("own-btt-grBilled", billedGr);
                } else if (empty.equals(mvType)) {
                    openingGr.put("own-empty-grBilled", billedGr);
                } else if (load.equals(mvType)) {
                    openingGr.put("own-load-grBilled", billedGr);
                } else if (dso.equals(mvType)) {
                    openingGr.put("own-dso-grBilled", billedGr);
                }

                // gr not billed less thn 7 days
                lessthn7daysGrs = (String) getSqlMapClientTemplate().queryForObject("report.grNotBilledlessthn7days", map);
                System.out.println("lessthn7daysGrs" + lessthn7daysGrs);
                if (btt.equals(mvType)) {
                    openingGr.put("own-btt-grlessthn7", lessthn7daysGrs);
                } else if (empty.equals(mvType)) {
                    openingGr.put("own-empty-grlessthn7", lessthn7daysGrs);
                } else if (load.equals(mvType)) {
                    openingGr.put("own-load-grlessthn7", lessthn7daysGrs);
                } else if (dso.equals(mvType)) {
                    openingGr.put("own-dso-grlessthn7", lessthn7daysGrs);
                }

                // total invoiced amt to bill to party
                totalInvAmtToParty = (String) getSqlMapClientTemplate().queryForObject("report.totalRaiseInvoiceAmtToParty", map);
                System.out.println("totalInvAmtToParty" + totalInvAmtToParty);
                if (btt.equals(mvType)) {
                    openingGr.put("own-btt-totInvAmt", totalInvAmtToParty);
                } else if (empty.equals(mvType)) {
                    openingGr.put("own-empty-totInvAmt", totalInvAmtToParty);
                } else if (load.equals(mvType)) {
                    openingGr.put("own-load-totInvAmt", totalInvAmtToParty);
                } else if (dso.equals(mvType)) {
                    openingGr.put("own-dso-totInvAmt", totalInvAmtToParty);
                }

                // total invoiced amt to bill not to party
                totalInvAmtNotToParty = (String) getSqlMapClientTemplate().queryForObject("report.totalRaiseInvoiceAmtNotToParty", map);
                System.out.println("expenseDuringDay" + totalInvAmtNotToParty);
                if (btt.equals(mvType)) {
                    openingGr.put("own-btt-totInvAmtNotToParty", totalInvAmtNotToParty);
                } else if (empty.equals(mvType)) {
                    openingGr.put("own-empty-totInvAmtNotToParty", totalInvAmtNotToParty);
                } else if (load.equals(mvType)) {
                    openingGr.put("own-load-totInvAmtNotToParty", totalInvAmtNotToParty);
                } else if (dso.equals(mvType)) {
                    openingGr.put("own-dso-totInvAmtNotToParty", totalInvAmtNotToParty);
                }

                //pending Invoice amt bill to party
                pendingInvAmtToParty = (String) getSqlMapClientTemplate().queryForObject("report.totalPendingInvoiceAmtToParty", map);
                System.out.println("pendingExpense" + pendingExpense);
                if (btt.equals(mvType)) {
                    openingGr.put("own-btt-pendingInvToParty", pendingInvAmtToParty);
                } else if (empty.equals(mvType)) {
                    openingGr.put("own-empty-pendingInvToParty", pendingInvAmtToParty);
                } else if (load.equals(mvType)) {
                    openingGr.put("own-load-pendingInvToParty", pendingInvAmtToParty);
                } else if (dso.equals(mvType)) {
                    openingGr.put("own-dso-pendingInvToParty", pendingInvAmtToParty);
                }

                //  pending inv amt bill not to party
                pendingInvAmtNotToParty = (String) getSqlMapClientTemplate().queryForObject("report.totalPendingInvoiceAmtNotToParty", map);

                if (btt.equals(mvType)) {
                    openingGr.put("own-btt-pendingInvNotToParty", pendingInvAmtNotToParty);
                } else if (empty.equals(mvType)) {
                    openingGr.put("own-empty-pendingInvNotToParty", pendingInvAmtNotToParty);
                } else if (load.equals(mvType)) {
                    openingGr.put("own-load-pendingInvNotToParty", pendingInvAmtNotToParty);
                } else if (dso.equals(mvType)) {
                    openingGr.put("own-dso-pendingInvNotToParty", pendingInvAmtNotToParty);
                }

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("processGRprofitabilityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "processGRprofitabilityList List", sqlException);
        }

        return openingGr;
    }

    public ArrayList shippingBillReport(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList tripList = new ArrayList();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        System.out.println("map" + map);
        try {
            System.out.println(" shippingBillReport = map is tht" + map);
            tripList = (ArrayList) getSqlMapClientTemplate().queryForList("report.shippingBillReport", map);
            System.out.println(" tripEndedNotClosedBeyond24===" + tripList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("processGRprofitabilityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "processGRprofitabilityList List", sqlException);
        }

        return tripList;
    }

    public ArrayList getRateDiffReport(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList tripList = new ArrayList();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        System.out.println("map" + map);
        try {
            System.out.println(" rateDiffReport = map is tht" + map);
            tripList = (ArrayList) getSqlMapClientTemplate().queryForList("report.rateDiffReport", map);
            System.out.println(" rateDiffReport===" + tripList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("processGRprofitabilityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "rateDiffReport List", sqlException);
        }

        return tripList;
    }

    public ArrayList vehicleUtilReport(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList tripList = new ArrayList();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        System.out.println("map" + map);
        try {
            System.out.println(" vehicleUtilReport = map is tht" + map);
            tripList = (ArrayList) getSqlMapClientTemplate().queryForList("report.vehicleUtilReport", map);
            System.out.println(" vehicleUtilReport===" + tripList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("processGRprofitabilityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "rateDiffReport List", sqlException);
        }

        return tripList;
    }

    public ArrayList nodReport(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList statusList = new ArrayList();
        ArrayList dayDataList = new ArrayList();
        String[] dayArray = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8"};
        ReportTO statusTO = new ReportTO();
        ReportTO statusTO1 = new ReportTO();
        Iterator itr = null;
        Iterator itr1 = null;
        try {
            statusList = (ArrayList) getSqlMapClientTemplate().queryForList("report.statusList", map);
            for (int i = 0; i < dayArray.length; i++) {
                System.out.println(dayArray[i]);
                map.put("day", dayArray[i]);
                System.out.println("map  for day data list = " + map);
                dayDataList = (ArrayList) getSqlMapClientTemplate().queryForList("report.dayDataList", map);
                itr = statusList.iterator();
                itr1 = dayDataList.iterator();
                while (itr.hasNext()) {
                    statusTO = new ReportTO();
                    statusTO = (ReportTO) itr.next();
                    itr1 = dayDataList.iterator();
                    while (itr1.hasNext()) {
                        statusTO1 = new ReportTO();
                        statusTO1 = (ReportTO) itr1.next();
                        if (statusTO.getStatusId().equals(statusTO1.getNodstatusId())) {
                            if (dayArray[i].equals("0")) {
                                statusTO.setZeroDay(statusTO1.getNodTotalCount());
                                System.out.println("zero day=====" + statusTO1.getNodTotalCount());
                            } else if (dayArray[i].equals("1")) {
                                statusTO.setOneDay(statusTO1.getNodTotalCount());
                                System.out.println("one day=====" + statusTO1.getNodTotalCount());
                            } else if (dayArray[i].equals("2")) {
                                statusTO.setTwoDay(statusTO1.getNodTotalCount());
                                System.out.println("two day=====" + statusTO1.getNodTotalCount());
                            } else if (dayArray[i].equals("3")) {
                                statusTO.setThreeDay(statusTO1.getNodTotalCount());
                                System.out.println("three day=====" + statusTO1.getNodTotalCount());
                            } else if (dayArray[i].equals("4")) {
                                statusTO.setFourDay(statusTO1.getNodTotalCount());
                                System.out.println("four day=====" + statusTO1.getNodTotalCount());
                            } else if (dayArray[i].equals("5")) {
                                statusTO.setFiveDay(statusTO1.getNodTotalCount());
                                System.out.println("five day=====" + statusTO1.getNodTotalCount());
                            } else if (dayArray[i].equals("6")) {
                                statusTO.setSixDay(statusTO1.getNodTotalCount());
                                System.out.println("six day=====" + statusTO1.getNodTotalCount());
                            } else if (dayArray[i].equals("7")) {
                                statusTO.setSevenDay(statusTO1.getNodTotalCount());
                                System.out.println("seven day=====" + statusTO1.getNodTotalCount());
                            } else if (dayArray[i].equals("8")) {
                                statusTO.setMoreThanSevenDay(statusTO1.getNodTotalCount());
                                System.out.println("eight day=====" + statusTO1.getNodTotalCount());
                            }
                            break;
                        }
                    }
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("dayDataList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "nodReport", sqlException);
        }
        return statusList;

    }

    public ArrayList nodReports(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList statusLists = new ArrayList();
        ArrayList dayDataList1 = new ArrayList();
        String[] dayArrays = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8"};
        ReportTO statusTO = new ReportTO();
        ReportTO statusTO1 = new ReportTO();
        Iterator itr = null;
        Iterator itr1 = null;
        try {
            statusLists = (ArrayList) getSqlMapClientTemplate().queryForList("report.statusLists", map);
            for (int j = 0; j < dayArrays.length; j++) {
                System.out.println(dayArrays[j]);
                map.put("days", dayArrays[j]);
                System.out.println("map  for day data listssssss = " + map);
                dayDataList1 = (ArrayList) getSqlMapClientTemplate().queryForList("report.dayDataList1", map);
                itr = statusLists.iterator();
                itr1 = dayDataList1.iterator();
                while (itr.hasNext()) {
                    statusTO = new ReportTO();
                    statusTO = (ReportTO) itr.next();
                    itr1 = dayDataList1.iterator();
                    while (itr1.hasNext()) {
                        statusTO1 = new ReportTO();
                        statusTO1 = (ReportTO) itr1.next();
                        if (statusTO.getStatusId().equals(statusTO1.getNodstatusIds())) {
                            if (dayArrays[j].equals("0")) {
                                statusTO.setZeroDay(statusTO1.getNodTotalCounts());
                                System.out.println("zero day=ss====" + statusTO1.getNodTotalCounts());
                            } else if (dayArrays[j].equals("1")) {
                                statusTO.setOneDay(statusTO1.getNodTotalCounts());
                                System.out.println("one day==ss===" + statusTO1.getNodTotalCounts());
                            } else if (dayArrays[j].equals("2")) {
                                statusTO.setTwoDay(statusTO1.getNodTotalCounts());
                                System.out.println("two day==ss===" + statusTO1.getNodTotalCounts());
                            } else if (dayArrays[j].equals("3")) {
                                statusTO.setThreeDay(statusTO1.getNodTotalCounts());
                                System.out.println("three day==ss===" + statusTO1.getNodTotalCounts());
                            } else if (dayArrays[j].equals("4")) {
                                statusTO.setFourDay(statusTO1.getNodTotalCounts());
                                System.out.println("four day==ss===" + statusTO1.getNodTotalCounts());
                            } else if (dayArrays[j].equals("5")) {
                                statusTO.setFiveDay(statusTO1.getNodTotalCounts());
                                System.out.println("five day=ss====" + statusTO1.getNodTotalCounts());
                            } else if (dayArrays[j].equals("6")) {
                                statusTO.setSixDay(statusTO1.getNodTotalCounts());
                                System.out.println("six day==ss===" + statusTO1.getNodTotalCounts());
                            } else if (dayArrays[j].equals("7")) {
                                statusTO.setSevenDay(statusTO1.getNodTotalCounts());
                                System.out.println("seven day==ss===" + statusTO1.getNodTotalCounts());
                            } else if (dayArrays[j].equals("8")) {
                                statusTO.setMoreThanSevenDay(statusTO1.getNodTotalCounts());
                                System.out.println("eight day===ss==" + statusTO1.getNodTotalCounts());
                            }
                            break;
                        }
                    }
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("dayDataList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "nodReport", sqlException);
        }
        return statusLists;

    }

    public ArrayList nodReportLevel2(ReportTO reportTO) {
        Map map = new HashMap();
        System.out.println("im in DAO Muthu...");
        ArrayList nodLevel2List = new ArrayList();
        map.put("statusId", reportTO.getStatusId());
        map.put("day", reportTO.getNoOfDays());
        System.out.println("map = " + map);
        try {
            nodLevel2List = (ArrayList) getSqlMapClientTemplate().queryForList("report.nodReportLevel2", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAgentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "nodReportLevel2", sqlException);
        }
        return nodLevel2List;

    }

    public ArrayList nodReportLevel3(ReportTO reportTO) {
        Map map = new HashMap();
        System.out.println("im in DAO Muthu.222..");
        ArrayList nodLevel3List = new ArrayList();
        map.put("statusId", reportTO.getStatusId());
        map.put("day", reportTO.getNoOfDays());
        System.out.println("map = " + map);
        try {
            nodLevel3List = (ArrayList) getSqlMapClientTemplate().queryForList("report.nodReportLevel3", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAgentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "nodReportLevel2", sqlException);
        }
        return nodLevel3List;

    }

    public ArrayList invoiceEmailReport(ReportTO reportTO) {
        Map map = new HashMap();
        System.out.println("im ininvoiceEmailReport");
        ArrayList nodLevel3List = new ArrayList();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        map.put("customerId", reportTO.getCustomerId());
        System.out.println("map = " + map);
        try {
            nodLevel3List = (ArrayList) getSqlMapClientTemplate().queryForList("report.invoiceEmailReport", map);
            System.out.println("invoiceEmailReport size:" + nodLevel3List.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAgentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "nodReportLevel2", sqlException);
        }
        return nodLevel3List;

    }

    public ArrayList customerWiseProfitability(ReportTO reportTO) {
        Map map = new HashMap();
        System.out.println("im customerWiseProfitability");
        ArrayList customerWiseProfitability = new ArrayList();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        map.put("customerId", reportTO.getCustomerId());
        System.out.println("map = " + map);
        try {
            customerWiseProfitability = (ArrayList) getSqlMapClientTemplate().queryForList("report.customerwiseprofitability", map);
            System.out.println("customerwiseprofitability size:" + customerWiseProfitability.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAgentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "customerWiseProfitability", sqlException);
        }
        return customerWiseProfitability;

    }

    public ArrayList getGrPrintDetails(ReportTO reportTO) {
        Map map = new HashMap();
        System.out.println("im getGrPrintDetails");
        ArrayList printDetails = new ArrayList();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        map.put("grNo", reportTO.getGrNo());
        System.out.println("map = " + map);
        try {
            printDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getGrPrintDetails", map);
            System.out.println("getGrPrintDetails size:" + printDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAgentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "customerWiseProfitability", sqlException);
        }
        return printDetails;

    }

//     public int updateGrPendingRemarks(String[] grId,String[] selectedId,String[] pendingGrRemarks,String[] gstType,String[] commodityId,String[] commodityName,String[] tripId,int userId){
//        Map map = new HashMap();
//        int status = 0;
//        int articleStatus = 0;
//        double count = 0.00;
//        double val= 0.00D;
//        map.put("userId", userId);
//        try {
//            System.out.println("grId.length-----"+grId.length);
//            for(int i=0;i<grId.length;i++){
//                
//              if("1".equals(selectedId[i])){
//                  System.out.println("selectedId[i]--"+selectedId[i]);
//                map.put("selectedId",selectedId[i]);
//                map.put("grId",grId[i]);
//                map.put("pendingGrRemarks",pendingGrRemarks[i]);
//                map.put("gstType",gstType[i]);
//                map.put("commodityId",commodityId[i]);
//                map.put("commodityName",commodityName[i]);
//                map.put("tripId",tripId[i]);
//                System.out.println("map@@@ GR pending---= " + map);
//                status = (Integer) getSqlMapClientTemplate().update("report.updateCommodityTripMaster", map); // update in trip_master table
//                System.out.println("updateCommodityTripMaster----"+status);
//                status = (Integer) getSqlMapClientTemplate().update("report.updateCommodityTripArticle", map); // update in trip_article table
//                System.out.println("updateCommodityTripArticle----"+status);
//                status = (Integer) getSqlMapClientTemplate().update("report.updateGrPendingRemarks", map); // update in trip_gr table
//                System.out.println("updateGrPendingRemarks@@@ = " + status);
//              }
//            }
//        } catch (Exception sqlException) {
//            sqlException.printStackTrace();
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("updateGrPendingRemarks Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-OPR-01", CLASS, "updateGrPendingRemarks", sqlException);
//        }
//        return status;
//
//    } 
//        public int updateGrPendingRemarks(String[] grId,String[] selectedId,String[] pendingGrRemarks,String gstType,String commodityId,String commodityName,String[] tripId,int userId){
//        public int updateGrPendingRemarks(ReportTO report,int userId){
//        Map map = new HashMap();
//        int status = 0;
//        int articleStatus = 0;
//        double count = 0.00;
//        double val= 0.00D;
//        map.put("userId", userId);
//        map.put("gstType",report.getGstType());
//        map.put("commodityId",report.getCommodityId());
//        map.put("commodityName",report.getCommodityName());
//         System.out.println("MAP----"+map);
//        try {
//            System.out.println("grId.length-----"+report.getGrIds().length);
//            for(int i=0;i<report.getGrIds().length;i++){
////                System.out.println("selectedId[i]---"+selectedId[i]);
//              if("1".equals(report.getSelectedIds()[i])){
//                System.out.println("selectedId[i]--"+report.getSelectedIds()[i]);
//                map.put("selectedId",report.getSelectedIds()[i]);
//                map.put("grId",report.getGrIds()[i]);
//                map.put("pendingGrRemarks",report.getPendingGrRemarks()[i]);
////                map.put("gstType",gstType[i]);
////                map.put("commodityId",commodityId[i]);
////                map.put("commodityName",commodityName[i]);
//                map.put("tripId",report.getTripIdss()[i]);
//                System.out.println("map@@@ GR pending---= " + map);
//                status = (Integer) getSqlMapClientTemplate().update("report.updateCommodityTripMaster", map); // update in trip_master table
//                System.out.println("updateCommodityTripMaster----"+status);
//                status = (Integer) getSqlMapClientTemplate().update("report.updateCommodityTripArticle", map); // update in trip_article table
//                System.out.println("updateCommodityTripArticle----"+status);
//                status = (Integer) getSqlMapClientTemplate().update("report.updateGrPendingRemarks", map); // update in trip_gr table
//                System.out.println("updateGrPendingRemarks@@@ = " + status);
//              }
//            }
//        } catch (Exception sqlException) {
//            sqlException.printStackTrace();
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("updateGrPendingRemarks Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-OPR-01", CLASS, "updateGrPendingRemarks", sqlException);
//        }
//        return status;
//
//    } 
//   public ArrayList getInvoiceList(ReportTO reportTO) {
//        Map map = new HashMap();
//        ArrayList getInvoiceList = new ArrayList();
//        map.put("invoiceCode", "%"+reportTO.getInvoiceCode()+ "%");
//        map.put("InvoiceType", reportTO.getInvoiceType());
//        try {
//            System.out.println(" getInvoiceList = map is tht" + map);
//            if("1".equals(reportTO.getInvoiceType())){
//                System.out.println("invoice type 1");
//            getInvoiceList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getInvoiceList", map);
//            System.out.println(" getInvoiceList =" + getInvoiceList.size());
//            }else if("2".equals(reportTO.getInvoiceType())){
//            getInvoiceList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getSuppInvoiceList", map);
//            System.out.println(" getInvoiceList =" + getInvoiceList.size());
//            }else if("3".equals(reportTO.getInvoiceType())){
//            getInvoiceList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getCreditNoteInvoiceList", map);
//            System.out.println(" getInvoiceList =" + getInvoiceList.size());
//            }
//        } catch (Exception sqlException) {
//            sqlException.printStackTrace();
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("getInvoiceList Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceList List", sqlException);
//        }
//
//        return getInvoiceList;
//    }
    public ArrayList getInvoiceList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList getInvoiceList = new ArrayList();
        map.put("invoiceCode", "%" + reportTO.getInvoiceCode() + "%");
        map.put("InvoiceType", reportTO.getInvoiceType());
        try {
            System.out.println(" getInvoiceList = map is tht" + map);
            if ("1".equals(reportTO.getInvoiceType())) {
                System.out.println("invoice type 1");
                getInvoiceList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getInvoiceList", map);
                System.out.println(" getInvoiceList =" + getInvoiceList.size());
            } else if ("2".equals(reportTO.getInvoiceType())) {
                getInvoiceList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getSuppInvoiceList", map);
                System.out.println(" getInvoiceList =" + getInvoiceList.size());
            } else if ("3".equals(reportTO.getInvoiceType())) {
                getInvoiceList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getCreditNoteInvoiceList", map);
                System.out.println(" getInvoiceList =" + getInvoiceList.size());
            } else if ("4".equals(reportTO.getInvoiceType())) {
                getInvoiceList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getSuppCreditNoteInvoiceList", map);
                System.out.println(" getInvoiceList =" + getInvoiceList.size());
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceList List", sqlException);
        }

        return getInvoiceList;
    }

    public ArrayList suppCreditNoteDetailsReport(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList invoiceDetailsReport = new ArrayList();
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            //            map.put("type", reportTO.getType());

            System.out.println("creditNoteDetailsReport = " + map);
            invoiceDetailsReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.suppCreditNoteDetailsReport", map);
            System.out.println("creditNoteDetailsReport====" + invoiceDetailsReport.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleVendorwiseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getCustomerWiseProfitList", sqlException);
        }
        return invoiceDetailsReport;
    }

    public int cancelGRno(ReportTO reportTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        map.put("userId", userId);
        try {
            map.put("grId", reportTO.getGrId());
            System.out.println("map@@@ = " + map);
            status = (Integer) session.update("report.updateGrActiveStatus", map); // update in trip_gr table

            tripId = (Integer) session.queryForObject("report.getTripId", map);
            map.put("tripId", tripId);

            status = (Integer) session.update("report.updateGRTripCancel", map); // update in trip_gr table
            System.out.println("cancelGRno@@@ = " + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("cancelGRno Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "cancelGRno", sqlException);
        }
        return status;

    }

    public ArrayList getGRNoList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList getGRNoList = new ArrayList();
        map.put("grNo", "%" + reportTO.getGrNo() + "%");
        try {
            System.out.println(" getGRNoList = map is tht" + map);
            getGRNoList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getGRNoList", map);
            System.out.println(" getGRNoList =" + getGRNoList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getGRNoList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getGRNoList List", sqlException);
        }

        return getGRNoList;
    }

    public ArrayList getSuppInvoiceReport(ReportTO reportTO, String status, int userId) {
        Map map = new HashMap();
        System.out.println("IN dao....");
        ArrayList invoiceReport = new ArrayList();
        try {
            map.put("reportTypeId", reportTO.getReportTypeId());
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            map.put("customerId", reportTO.getCustomerId());
            map.put("status", status);
            System.out.println("map ######= " + map);
            if ("invoiceGr".equals(status)) {
                invoiceReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.grSuppInvoiceGenerator", map);
                System.out.println("invoiceReport.size===" + invoiceReport.size());
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("dailyInvoiceReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "invoiceReport", sqlException);
        }
        return invoiceReport;
    }

    public ArrayList getEmailInvoiceHeader() {
        Map map = new HashMap();
        System.out.println("IN dao....");
        ArrayList invoiceReport = new ArrayList();
        try {
            invoiceReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.getEmailInvoiceHeader", map);
            System.out.println("getEmailInvoiceHeader.size===" + invoiceReport.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmailInvoiceHeader Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getEmailInvoiceHeader", sqlException);
        }
        return invoiceReport;
    }

    public ArrayList getEmailInvoiceDetails(String invoiceId) {
        Map map = new HashMap();
        ArrayList invoiceReport = new ArrayList();
        try {
            map.put("invoiceId", invoiceId);
            System.out.println("IN dao...." + map);
            invoiceReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.getEmailInvoiceDetails", map);
            System.out.println("getEmailInvoiceDetails.size===" + invoiceReport.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmailInvoiceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getEmailInvoiceDetails", sqlException);
        }
        return invoiceReport;
    }

    public ArrayList getInvoiceListEmail() {
        Map map = new HashMap();
        System.out.println("IN dao....");
        ArrayList invoiceReport = new ArrayList();
        try {
            invoiceReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.getInvoiceListEmail", map);
            System.out.println("getInvoiceListEmail.size===" + invoiceReport.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceListEmail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getInvoiceListEmail", sqlException);
        }
        return invoiceReport;
    }

    public ArrayList getInvoiceDetailsEmail(String invId) {
        Map map = new HashMap();
        //        System.out.println("IN dao....");
        ArrayList invoiceReport = new ArrayList();
        try {
            map.put("invId", invId);
            System.out.println("map---" + map);
            invoiceReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.getInvoiceDetailsEmail", map);
            System.out.println("getInvoiceDetailsEmail.size===" + invoiceReport.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceDetailsEmail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getInvoiceDetailsEmail", sqlException);
        }
        return invoiceReport;
    }

    public int insertInvoiceFileDetails(ReportTO reportTO, SqlMapClient session) {
        Map map = new HashMap();
        int insertInvoiceFileDetails = 0;
        map.put("invoiceid", reportTO.getInvoiceId());
        map.put("custId", reportTO.getCustId());
        map.put("fileName", reportTO.getFileName());
        //System.out.println("mapSSSS = " + map);
        try {
            System.out.println("sessionDAO:" + map);
            if (session != null) {
                insertInvoiceFileDetails = (Integer) session.update("report.insertInvoiceFileDetails", map);
            } else {
                insertInvoiceFileDetails = (Integer) getSqlMapClientTemplate().update("report.insertInvoiceFileDetails", map);
            }
            System.out.println("insertInvoiceFileDetails" + insertInvoiceFileDetails);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertInvoiceFileDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertInvoiceFileDetails", sqlException);
        }
        map = null;

        return insertInvoiceFileDetails;

    }

    public int updateInvoiceCustomerMail(ReportTO reportTO) {
        Map map = new HashMap();
        int updateInvoiceCustomerMail = 0;
        try {
            map.put("invId", reportTO.getInvoiceId());
            System.out.println("map = " + map);
            updateInvoiceCustomerMail = (Integer) getSqlMapClientTemplate().update("report.updateInvoiceCustomerMail", map);
            System.out.println("updateInvoiceCustomerMail==" + updateInvoiceCustomerMail);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateInvoiceCustomerMail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateSecondaryCustomerMail", sqlException);
        }
        return updateInvoiceCustomerMail;

    }

    public String getMailTemplate(String eventId, SqlMapClient session) {
        Map map = new HashMap();
        String template = "";
        map.put("eventId", eventId);
        try {
            if (session != null) {
                template = (String) session.queryForObject("report.getMailTemplate", map);
            } else {
                template = (String) getSqlMapClientTemplate().queryForObject("report.getMailTemplate", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSMSTemplate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSMSTemplate", sqlException);
        }

        return template;
    }

    public ArrayList getCustomerListForEmail(String customerId) {
        Map map = new HashMap();
        map.put("customerId", customerId);
        System.out.println("IN dao....et c l f m" + map);
        ArrayList customerList = new ArrayList();
        try {
            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getCustomerListForEmail", map);
            System.out.println("customerList.size===" + customerList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("customerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "customerList", sqlException);
        }
        return customerList;
    }

    public ArrayList getInvoiceListSendEmail(String custId) {
        Map map = new HashMap();
        //        System.out.println("IN dao....");
        ArrayList customerList = new ArrayList();
        try {
            map.put("custId", custId);
            System.out.println("map==" + map);
            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getInvoiceListSendEmail", map);
            System.out.println("getInvoiceListSendEmail.size===" + customerList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("customerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "customerList", sqlException);
        }
        return customerList;
    }

    public int updateInvoiceCustomerSendMail(ReportTO reportTO) {
        Map map = new HashMap();
        int updateInvoiceCustomerSendMail = 0;
        try {
            if (reportTO.getInvIds().contains(",")) {
                String[] invId = reportTO.getInvIds().split(",");
                List invIds = new ArrayList(invId.length);
                for (int i = 0; i < invId.length; i++) {
                    invIds.add(invId[i]);
                }
                map.put("invId", invIds);
            } else {
                List invIds = new ArrayList();
                invIds.add(reportTO.getInvIds());
                map.put("invId", invIds);
            }
            System.out.println("map = " + map);
            int updateInvoiceFileStatus = (Integer) getSqlMapClientTemplate().update("report.updateInvoiceFileStatus", map);
            System.out.println("updateInvoiceFileStatus==" + updateInvoiceFileStatus);

            updateInvoiceCustomerSendMail = (Integer) getSqlMapClientTemplate().update("report.updateInvoiceCustomerSendMail", map);
            System.out.println("updateInvoiceCustomerSendMail==" + updateInvoiceCustomerSendMail);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */

            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("updateInvoiceCustomerSendMail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateSecondaryCustomerMail", sqlException);
        }
        return updateInvoiceCustomerSendMail;

    }

    public ArrayList getCustomerContractMailList() {
        Map map = new HashMap();
        ArrayList getCustomerContractMailList = new ArrayList();
        try {
            System.out.println(" getCustomerContractMailList = map is tht" + map);
            getCustomerContractMailList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getCustomerContractMailList", map);
            System.out.println(" getCustomerContractMailList===" + getCustomerContractMailList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerContractMailList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerContractMailList List", sqlException);
        }

        return getCustomerContractMailList;
    }

    public ArrayList getCustomerContractExpiryList(SchedulerTO schedulerTO) {
        Map map = new HashMap();
        ArrayList getCustomerContractExpiryList = new ArrayList();
        map.put("createdBy", schedulerTO.getCreatedBy());
        System.out.println("getCustomerContractExpiryList map" + map);
        try {
            System.out.println(" getCustomerContractExpiryList = map is tht" + map);
            getCustomerContractExpiryList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getCustomerContractExpiryList", map);
            System.out.println(" getCustomerContractExpiryList===" + getCustomerContractExpiryList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("processGRprofitabilityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerContractExpiryList List", sqlException);
        }

        return getCustomerContractExpiryList;
    }

    public int insertMailDetails(SchedulerTO schedulerTO, int userId) {
        Map map = new HashMap();
        int insertMailDetails = 0;
        map.put("mailStatus", schedulerTO.getMailStatus());
        map.put("mailTypeId", schedulerTO.getMailTypeId());
        map.put("mailSubjectTo", schedulerTO.getMailSubjectTo());
        map.put("mailSubjectCc", schedulerTO.getMailSubjectCc());
        map.put("mailSubjectBcc", schedulerTO.getMailSubjectBcc());
        map.put("mailContentTo", schedulerTO.getEmailFormat());
        map.put("mailContentCc", schedulerTO.getEmailFormat());
        map.put("mailContentBcc", "");
        map.put("mailExcelFilePath", schedulerTO.getFilenameContent());
        map.put("mailTo", schedulerTO.getMailIdTo());
        map.put("mailCc", schedulerTO.getMailIdCc());
        map.put("mailBcc", schedulerTO.getMailIdBcc());
        map.put("userId", userId);
        System.out.println("map ###= " + map);
        try {
            insertMailDetails = (Integer) getSqlMapClientTemplate().insert("report.insertMailDetails", map);
            System.out.println("insertMailDetails###==" + insertMailDetails);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertMailDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertMailDetails", sqlException);
        }
        return insertMailDetails;

    }

    public ArrayList getEInvoiceList(int statusId) {

        Map map = new HashMap();
        map.put("statusId", statusId);
        ArrayList getEInvoiceList = new ArrayList();
        try {
            System.out.println(" getCustomerContractMailList = map is tht" + map);
            getEInvoiceList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getEInvoiceList", map);

            int invoiceUpdate = (Integer) getSqlMapClientTemplate().update("report.invoiceUpdate", map);
            System.out.println("invoiceUpdate------------------------------------------------" + invoiceUpdate);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerContractMailList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerContractMailList List", sqlException);
        }

        return getEInvoiceList;

    }

    public ArrayList getItemDetails(String invoiceId, int statusId) {

        Map map = new HashMap();
        map.put("invoiceId", invoiceId);
        map.put("statusId", statusId);
        System.out.println("statusId----" + statusId);
        ArrayList getItemDetails = new ArrayList();
        try {
            System.out.println(" getCustomerContractMailList = map is tht" + map);
            getItemDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getItemDetails", map);
            System.out.println(" getItemDetails===" + getItemDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getItemDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getItemDetails", sqlException);
        }

        return getItemDetails;

    }

    public int updateApiResponse(String request, String response, String type) {
        Map map = new HashMap();
        int updateApiResponse = 0;
        map.put("request", request);
        map.put("response", response);
        map.put("type", type);

        System.out.println("map ###= " + map);
        try {
            updateApiResponse = (Integer) getSqlMapClientTemplate().insert("report.updateApiResponse", map);
            System.out.println("updateApiResponse###==" + updateApiResponse);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateApiResponse Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateApiResponse", sqlException);
        }
        return updateApiResponse;

    }

    public int updateUploadEInvoice(String invoiceId, String irn, String SignedQRCode) {
        Map map = new HashMap();
        int updateApiResponse = 0;
        map.put("invoiceId", invoiceId);
        map.put("irn", irn);
        map.put("SignedQRCode", SignedQRCode);
        map.put("status", "1");
        map.put("msg", "SUCCESS");

        System.out.println("map ###= " + map);
        try {
            updateApiResponse = (Integer) getSqlMapClientTemplate().update("report.updateUploadEInvoice", map);
            System.out.println("updateApiResponse###==" + updateApiResponse);
            if (updateApiResponse > 0) {
                int updateStatusInHeader = (Integer) getSqlMapClientTemplate().update("report.updateStatusInHeader", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateUploadEInvoice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateUploadEInvoice", sqlException);
        }
        return updateApiResponse;

    }

    public int updateErrorStatus(String invoiceId, String error) {
        Map map = new HashMap();
        int updateStatusInHeader = 0;
        map.put("invoiceId", invoiceId);
        map.put("msg", error);
        map.put("status", "2");

        System.out.println("map in error###= " + map);
        try {

            updateStatusInHeader = (Integer) getSqlMapClientTemplate().update("report.updateStatusInHeader", map);
            System.out.println("updateApiResponsefor error###==" + updateStatusInHeader);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStatusInHeader Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateStatusInHeader", sqlException);
        }
        return updateStatusInHeader;

    }

//     suppinvoice
    public ArrayList getESuppInvoiceList(int statusId) {

        Map map = new HashMap();
        ArrayList getEInvoiceList = new ArrayList();
        map.put("statusId", statusId);
        try {
            System.out.println(" getESuppInvoiceList = map is tht" + map);
            getEInvoiceList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getESuppInvoiceList", map);
            System.out.println(" getCustomerContractMailList===" + getEInvoiceList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerContractMailList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerContractMailList List", sqlException);
        }

        return getEInvoiceList;

    }

    public ArrayList getSuppItemDetails(String invoiceId, int statusId) {

        Map map = new HashMap();
        map.put("invoiceId", invoiceId);
        map.put("statusId", statusId);
        System.out.println("statusId----" + statusId);
        ArrayList getItemDetails = new ArrayList();
        try {
            System.out.println(" getCustomerContractMailList = map is tht" + map);
            getItemDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getSuppItemDetails", map);
            System.out.println(" getItemDetails===" + getItemDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getItemDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getItemDetails", sqlException);
        }

        return getItemDetails;

    }

    public int updateSuppApiResponse(String request, String response, String type) {
        Map map = new HashMap();
        int updateApiResponse = 0;
        map.put("request", request);
        map.put("response", response);
        map.put("type", type);

        System.out.println("map ###= " + map);
        try {
            updateApiResponse = (Integer) getSqlMapClientTemplate().insert("report.updateSuppApiResponse", map);
            System.out.println("updateApiResponse###==" + updateApiResponse);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateApiResponse Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateApiResponse", sqlException);
        }
        return updateApiResponse;

    }

    public int updateUploadESuppInvoice(String invoiceId, String irn, String SignedQRCode) {
        Map map = new HashMap();
        int updateApiResponse = 0;
        map.put("invoiceId", invoiceId);
        map.put("irn", irn);
        map.put("SignedQRCode", SignedQRCode);
        map.put("status", "1");
        map.put("msg", "SUCCESS");

        System.out.println("map ###= " + map);
        try {
            updateApiResponse = (Integer) getSqlMapClientTemplate().update("report.updateUploadESuppInvoice", map);
            System.out.println("updateApiResponse###==" + updateApiResponse);
            if (updateApiResponse > 0) {
                int updateStatusInHeader = (Integer) getSqlMapClientTemplate().update("report.updateSuppStatusInHeader", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateUploadEInvoice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateUploadEInvoice", sqlException);
        }
        return updateApiResponse;

    }

    public int updateSuppErrorStatus(String invoiceId, String error) {
        Map map = new HashMap();
        int updateStatusInHeader = 0;
        map.put("invoiceId", invoiceId);
        map.put("msg", error);
        map.put("status", "2");

        System.out.println("map in error###= " + map);
        try {

            updateStatusInHeader = (Integer) getSqlMapClientTemplate().update("report.updateSuppStatusInHeader", map);
            System.out.println("updateApiResponsefor error###==" + updateStatusInHeader);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStatusInHeader Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateStatusInHeader", sqlException);
        }
        return updateStatusInHeader;

    }

    public ArrayList getECreditInvoiceList(int statusId) {

        Map map = new HashMap();
        map.put("statusId", statusId);
        ArrayList getEInvoiceList = new ArrayList();
        try {
            System.out.println(" getCustomerContractMailList = map is tht" + map);
            getEInvoiceList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getECreditInvoiceList", map);
            System.out.println(" getCustomerContractMailList===" + getEInvoiceList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerContractMailList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerContractMailList List", sqlException);
        }

        return getEInvoiceList;

    }

    public ArrayList getCreditItemDetails(String invoiceId, int statusId) {

        Map map = new HashMap();
        map.put("invoiceId", invoiceId);
        map.put("statusId", statusId);
        System.out.println("statusId----" + statusId);
        ArrayList getItemDetails = new ArrayList();
        try {
            System.out.println(" getCustomerContractMailList = map is tht" + map);
            getItemDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getCreditItemDetails", map);
            System.out.println(" getItemDetails===" + getItemDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getItemDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getItemDetails", sqlException);
        }

        return getItemDetails;

    }

    public ArrayList getECreditSuppInvoiceList(int statusId) {

        Map map = new HashMap();
        map.put("statusId", statusId);
        ArrayList getEInvoiceList = new ArrayList();
        try {
            System.out.println(" getECreditSuppInvoiceList = map is tht" + map);
            getEInvoiceList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getECreditSuppInvoiceList", map);
            System.out.println(" getCustomerContractMailList===" + getEInvoiceList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerContractMailList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerContractMailList List", sqlException);
        }

        return getEInvoiceList;

    }

    public ArrayList getCreditSuppItemDetails(String invoiceId, int statusId) {

        Map map = new HashMap();
        map.put("invoiceId", invoiceId);
        map.put("statusId", statusId);
        System.out.println("statusId----" + statusId);
        ArrayList getItemDetails = new ArrayList();
        try {
            System.out.println(" getCreditSuppItemDetails = map is tht" + map);
            getItemDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getCreditSuppItemDetails", map);
            System.out.println(" getItemDetails===" + getItemDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getItemDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getItemDetails", sqlException);
        }

        return getItemDetails;

    }

//      credit invoice upload
    public int updateCreditApiResponse(String request, String response, String type) {
        Map map = new HashMap();
        int updateApiResponse = 0;
        map.put("request", request);
        map.put("response", response);
        map.put("type", type);

        System.out.println("map ###= " + map);
        try {
            updateApiResponse = (Integer) getSqlMapClientTemplate().insert("report.updateCreditApiResponse", map);
            System.out.println("updateApiResponse###==" + updateApiResponse);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateApiResponse Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateApiResponse", sqlException);
        }
        return updateApiResponse;

    }

    public int updateUploadECreditInvoice(String invoiceId, String irn, String SignedQRCode) {
        Map map = new HashMap();
        int updateApiResponse = 0;
        map.put("invoiceId", invoiceId);
        map.put("irn", irn);
        map.put("SignedQRCode", SignedQRCode);
        map.put("status", "1");
        map.put("msg", "SUCCESS");

        System.out.println("map ###= " + map);
        try {
            updateApiResponse = (Integer) getSqlMapClientTemplate().update("report.updateUploadECreditInvoice", map);
            System.out.println("updateApiResponse###==" + updateApiResponse);
            if (updateApiResponse > 0) {
                int updateStatusInHeader = (Integer) getSqlMapClientTemplate().update("report.updateCreditStatusInHeader", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateUploadEInvoice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateUploadEInvoice", sqlException);
        }
        return updateApiResponse;

    }

    public int updateCreditErrorStatus(String invoiceId, String error) {
        Map map = new HashMap();
        int updateStatusInHeader = 0;
        map.put("invoiceId", invoiceId);
        map.put("msg", error);
        map.put("status", "2");

        System.out.println("map in error###= " + map);
        try {

            updateStatusInHeader = (Integer) getSqlMapClientTemplate().update("report.updateCreditStatusInHeader", map);
            System.out.println("updateApiResponsefor error###==" + updateStatusInHeader);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStatusInHeader Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateStatusInHeader", sqlException);
        }
        return updateStatusInHeader;

    }

//      credit supp invoice upload
    public int updateCreditSuppApiResponse(String request, String response, String type) {
        Map map = new HashMap();
        int updateApiResponse = 0;
        map.put("request", request);
        map.put("response", response);
        map.put("type", type);

        System.out.println("map ###= " + map);
        try {
            updateApiResponse = (Integer) getSqlMapClientTemplate().insert("report.updateCreditSuppApiResponse", map);
            System.out.println("updateApiResponse###==" + updateApiResponse);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateApiResponse Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateApiResponse", sqlException);
        }
        return updateApiResponse;

    }

    public int updateUploadECreditSuppInvoice(String invoiceId, String irn, String SignedQRCode) {
        Map map = new HashMap();
        int updateApiResponse = 0;
        map.put("invoiceId", invoiceId);
        map.put("irn", irn);
        map.put("SignedQRCode", SignedQRCode);
        map.put("status", "1");
        map.put("msg", "SUCCESS");

        System.out.println("map ###= " + map);
        try {
            updateApiResponse = (Integer) getSqlMapClientTemplate().update("report.updateUploadECreditSuppInvoice", map);
            System.out.println("updateApiResponse###==" + updateApiResponse);
            if (updateApiResponse > 0) {
                int updateStatusInHeader = (Integer) getSqlMapClientTemplate().update("report.updateCreditSuppStatusInHeader", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateUploadEInvoice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateUploadEInvoice", sqlException);
        }
        return updateApiResponse;

    }

    public int updateCreditSuppErrorStatus(String invoiceId, String error) {
        Map map = new HashMap();
        int updateStatusInHeader = 0;
        map.put("invoiceId", invoiceId);
        map.put("msg", error);
        map.put("status", "2");

        System.out.println("map in error###= " + map);
        try {

            updateStatusInHeader = (Integer) getSqlMapClientTemplate().update("report.updateCreditSuppStatusInHeader", map);
            System.out.println("updateApiResponsefor error###==" + updateStatusInHeader);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStatusInHeader Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateStatusInHeader", sqlException);
        }
        return updateStatusInHeader;

    }

    public ArrayList invoiceCommodityDetailsReport(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList invoiceDetailsReport = new ArrayList();
        try {
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            map.put("type", reportTO.getType());

            System.out.println("invoiceDetailsReport = " + map);
            if ("1".equalsIgnoreCase(reportTO.getType())) {
                invoiceDetailsReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.commodityInvoiceDetailsReport", map);
            } else if ("2".equalsIgnoreCase(reportTO.getType())) {
                invoiceDetailsReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.commodityCreditInvoiceDetailsReport", map);
            } else if ("3".equalsIgnoreCase(reportTO.getType())) {
                invoiceDetailsReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.commoditySuppInvoiceDetailsReport", map);
            } else if ("4".equalsIgnoreCase(reportTO.getType())) {
                invoiceDetailsReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.commoditySuppCreditInvoiceDetailsReport", map);
            }
            System.out.println("invoiceDetailsReport" + invoiceDetailsReport.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleVendorwiseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getCustomerWiseProfitList", sqlException);
        }
        return invoiceDetailsReport;
    }
    //    customer role matrix

    public ArrayList getUserList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList userlist = new ArrayList();
        try {
            userlist = (ArrayList) getSqlMapClientTemplate().queryForList("report.getUserList", map);
            System.out.println(" getUserList===" + userlist.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getUserList List", sqlException);
        }

        return userlist;
    }

    public ArrayList getGRNoLists(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList getGRNoLists = new ArrayList();
        map.put("grNo", "%" + reportTO.getGrNo() + "%");
        try {
            System.out.println(" getGRNoList = map is tht" + map);
            getGRNoLists = (ArrayList) getSqlMapClientTemplate().queryForList("report.getGRNoLists", map);
            System.out.println(" getGRNoLists =" + getGRNoLists.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getGRNoLists Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getGRNoLists List", sqlException);
        }

        return getGRNoLists;
    }

    public ArrayList getInvoiceReport(ReportTO reportTO, String status, String gstStatusType, int userId) {
        Map map = new HashMap();
        System.out.println("IN dao....");
        ArrayList invoiceReport = new ArrayList();
        try {
            map.put("reportTypeId", reportTO.getReportTypeId());
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            map.put("customerId", reportTO.getCustomerId());
            map.put("status", status);
            map.put("movTypeId", reportTO.getMovTypeId());
            map.put("sbBillBoeBill", reportTO.getSbBillBoeBill());
            map.put("invoiceId", reportTO.getInvoiceId());
            map.put("invoiceType", reportTO.getInvoiceType());
            map.put("tripId", reportTO.getTripId());
            map.put("grId", reportTO.getGrId());
            map.put("gstStatusType", gstStatusType);
//            System.out.println("reportTO.getGstStatusType()----"+reportTO.getGstStatusType());
            System.out.println("map SSSS= " + map);
            if ("invoiceGr".equals(status)) {
                invoiceReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.dailyInvoiceReport", map);
                System.out.println("invoiceReport.size 1 ===" + invoiceReport.size());
            }

            if ("pendingGr".equals(status)) {
//            invoiceReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.pendingInvoiceReport", map);
//            System.out.println("invoiceReport.size 2 ==="+invoiceReport.size());
//            }
                if ("1".equals(reportTO.getInvoiceId())) {
                    invoiceReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.pendingInvoiceReport", map);
                    System.out.println("invoiceReport.size 2 ===" + invoiceReport.size());
                    if (!"".equals(reportTO.getGrId()) && !"null".equals(reportTO.getGrId())) {
                        invoiceReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.pendingInvoiceReport", map);
                        System.out.println("invoiceReport.size 11 ===" + invoiceReport.size());
                    }
                }
                if ("2".equals(reportTO.getInvoiceId())) {

                    if ("1".equals(reportTO.getInvoiceType())) {
                        if ("".equals(reportTO.getGrId()) || "null".equals(reportTO.getGrId())) {
                            invoiceReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.dailyAfterInvoiceReport", map);
                            System.out.println("invoiceReport.size 3 ===" + invoiceReport.size());
                        }
                        if (!"".equals(reportTO.getGrId()) && !"null".equals(reportTO.getGrId())) {
                            invoiceReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.dailyAfterInvoiceReport", map);
                            System.out.println("invoiceReport.size 10 ===" + invoiceReport.size());
                        }
                    }
                    if ("2".equals(reportTO.getInvoiceType())) {
                        if ("".equals(reportTO.getGrId()) || "null".equals(reportTO.getGrId())) {
                            invoiceReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.mainCreditNoteInvoiceReport", map);
                            System.out.println("invoiceReport.size 4 ===" + invoiceReport.size());
                        }
                        if (!"".equals(reportTO.getGrId()) && !"null".equals(reportTO.getGrId())) {
                            invoiceReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.mainCreditNoteInvoiceReport", map);
                            System.out.println("invoiceReport.size 9 ===" + invoiceReport.size());
                        }

                    }
                    if ("3".equals(reportTO.getInvoiceType())) {
                        if ("".equals(reportTO.getGrId()) || "null".equals(reportTO.getGrId())) {
                            invoiceReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.supplementaryInvoiceReport", map);
                            System.out.println("invoiceReport.size 5 ===" + invoiceReport.size());
                        }
                        if (!"".equals(reportTO.getGrId()) && !"null".equals(reportTO.getGrId())) {
                            invoiceReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.supplementaryInvoiceReport", map);
                            System.out.println("invoiceReport.size 8 ===" + invoiceReport.size());
                        }

                    }
                    if ("4".equals(reportTO.getInvoiceType())) {
                        if ("".equals(reportTO.getGrId()) || "null".equals(reportTO.getGrId())) {
                            invoiceReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.supplementaryCreditNoteInvoiceReport", map);
                            System.out.println("invoiceReport.size 6 ===" + invoiceReport.size());
                        }
                        if (!"".equals(reportTO.getGrId()) && !"null".equals(reportTO.getGrId())) {
                            invoiceReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.supplementaryCreditNoteInvoiceReport", map);
                            System.out.println("invoiceReport.size 7 ===" + invoiceReport.size());
                        }
                    }

                }
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("dailyInvoiceReport Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "invoiceReport", sqlException);
        }
        return invoiceReport;
    }

    public int updateGrPendingRemarks(ReportTO report, int userId) {
        Map map = new HashMap();
        int status = 0;
        int articleStatus = 0;
        double count = 0.00;
        double val = 0.00D;
        ArrayList getInvoice = new ArrayList();
        map.put("userId", userId);
        map.put("gstType", report.getGstType());
        map.put("commodityId", report.getCommodityId());
        map.put("commodityName", report.getCommodityName());
        map.put("invoiceType", report.getInvoiceType());
        System.out.println("MAP----" + map);
        try {
            System.out.println("grId.length-----" + report.getGrIds().length);
            System.out.println("SelectedIds.length-----" + report.getSelectedIds().length);

            for (int i = 0; i < report.getGrIds().length; i++) {
//                System.out.println("selectedId[i]---"+selectedId[i]);
                if ("1".equals(report.getSelectedIds()[i])) {
                    System.out.println("selectedId[i]--" + report.getSelectedIds()[i]);
                    map.put("selectedId", report.getSelectedIds()[i]);
                    map.put("grId", report.getGrIds()[i]);
                    map.put("pendingGrRemarks", report.getPendingGrRemarks()[i]);
                    map.put("tripId", report.getTripIdss()[i]);
                    System.out.println("map@@@ GR pending---= " + map);

                    status = (Integer) getSqlMapClientTemplate().update("report.updateCommodityTripMaster", map); // update in trip_master table
                    System.out.println("updateCommodityTripMaster----" + status);
                    status = (Integer) getSqlMapClientTemplate().update("report.updateCommodityTripArticle", map); // update in trip_article table
                    System.out.println("updateCommodityTripArticle----" + status);
                    status = (Integer) getSqlMapClientTemplate().update("report.updateGrPendingRemarks", map); // update in trip_gr table
                    System.out.println("updateGrPendingRemarks@@@ = " + status);

                    if ("1".equals(report.getInvoiceType())) {
                        getInvoice = (ArrayList) getSqlMapClientTemplate().queryForList("report.getInvoice", map); // get invoice_id from finance_invoicedetail table
                        System.out.println("getInvoice@@@ = " + getInvoice.size());

                        Iterator itr = getInvoice.iterator();
                        while (itr.hasNext()) {
                            ReportTO repTo = new ReportTO();
                            repTo = (ReportTO) itr.next();

                            map.put("invoiceId", repTo.getInvoiceId());
                            System.out.println("invoiceId===" + repTo.getInvoiceId());

                            status = (Integer) getSqlMapClientTemplate().update("report.updateGstType", map); // update GSTtype in finance_invoiceheader table
                            System.out.println("updateGstType@@@ = " + status);
                        }
                    } else if ("2".equals(report.getInvoiceType())) {
                        getInvoice = (ArrayList) getSqlMapClientTemplate().queryForList("report.getCreditInvoice", map); // get invoice_id from finance_invoicedetail table
                        System.out.println("getInvoice@@@ = " + getInvoice.size());

                        Iterator itr = getInvoice.iterator();
                        while (itr.hasNext()) {
                            ReportTO repTo = new ReportTO();
                            repTo = (ReportTO) itr.next();

                            map.put("invoiceId", repTo.getInvoiceId());
                            System.out.println("invoiceId===" + repTo.getInvoiceId());

                            status = (Integer) getSqlMapClientTemplate().update("report.updateCreditInvGstType", map); // update GSTtype in finance_invoiceheader table
                            System.out.println("updateCreditInvGstType@@@ = " + status);
                        }
                    } else if ("3".equals(report.getInvoiceType())) {
                        getInvoice = (ArrayList) getSqlMapClientTemplate().queryForList("report.getSuppInvoice", map); // get invoice_id from finance_invoicedetail table
                        System.out.println("getSuppInvoice@@@ = " + getInvoice.size());

                        Iterator itr = getInvoice.iterator();
                        while (itr.hasNext()) {
                            ReportTO repTo = new ReportTO();
                            repTo = (ReportTO) itr.next();
                            map.put("gstType", report.getGstType());
                            map.put("invoiceId", repTo.getInvoiceId());
                            System.out.println("gstType===" + report.getGstType());
                            System.out.println("invoiceId===" + repTo.getInvoiceId());

                            status = (Integer) getSqlMapClientTemplate().update("report.updateSuppInvGstType", map); // update GSTtype in finance_invoiceheader table
                            System.out.println("updateSuppInvGstType@@@ = " + status);
                        }
                    } else if ("4".equals(report.getInvoiceType())) {
                        getInvoice = (ArrayList) getSqlMapClientTemplate().queryForList("report.getSuppCreditInvoice", map); // get invoice_id from finance_invoicedetail table
                        System.out.println("getInvoice@@@ = " + getInvoice.size());

                        Iterator itr = getInvoice.iterator();
                        while (itr.hasNext()) {
                            ReportTO repTo = new ReportTO();
                            repTo = (ReportTO) itr.next();

                            map.put("invoiceId", repTo.getInvoiceId());
                            System.out.println("invoiceId===" + repTo.getInvoiceId());

                            status = (Integer) getSqlMapClientTemplate().update("report.updateSuppCreditInvGstType", map); // update GSTtype in finance_invoiceheader table
                            System.out.println("updateGstType@@@ = " + status);
                        }
                    }
                }

            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateGrPendingRemarks Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "updateGrPendingRemarks", sqlException);
        }
        return status;

    }

//    public ArrayList getInvoiceNo(ReportTO reportTO) {
//        Map map = new HashMap();
//        ArrayList getInvoiceNo = new ArrayList();
//        map.put("invoiceNo", reportTO.getInvoiceNo() + "%");
//        try {
//            System.out.println(" getInvoiceNo = map is tht" + map);
//            getInvoiceNo = (ArrayList) getSqlMapClientTemplate().queryForList("report.getInvoiceNo", map);
//            System.out.println(" getInvoiceNo =" + getInvoiceNo.size());
//        } catch (Exception sqlException) {
//            sqlException.printStackTrace();
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("getInvoiceNo Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceNo List", sqlException);
//        }
//
//        return getInvoiceNo;
//    }
    public ArrayList getBillingParty(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList billingParty = new ArrayList();
//        map.put("invoiceNo", reportTO.getInvoiceNo()+ "%");
        try {
//            System.out.println(" getBillingParty = map is tht" + map);
            billingParty = (ArrayList) getSqlMapClientTemplate().queryForList("report.getBillingParty", map);
            System.out.println(" getBillingParty =" + billingParty.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBillingParty Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBillingParty List", sqlException);
        }

        return billingParty;
    }

    public int updateInvoice(ReportTO reportTO, SqlMapClient session) {
        Map map = new HashMap();
        int updateInvoice = 0;
//        String getTripId = "";
        String getInvoiceId = "";
        map.put("invoiceNo", reportTO.getInvoiceNo());
        map.put("customerId", reportTO.getCustomerId());
        map.put("custId", reportTO.getCustId());
        try {

            ArrayList getTripId = (ArrayList) session.queryForList("report.getTripIdForUpdate", map);
            System.out.println("getTripId ###==" + getTripId);

            Iterator itr = getTripId.iterator();
            while (itr.hasNext()) {
                reportTO = new ReportTO();
                reportTO = (ReportTO) itr.next();
                map.put("tripId", reportTO.getTripId());
                System.out.println("tripId--------------" + reportTO.getTripId());
            }

            getInvoiceId = (String) session.queryForObject("report.getInvoiceIdForUpdate", map);
            System.out.println("getInvoiceId ###==" + getInvoiceId);

            map.put("invoiceId", "InvId-" + getInvoiceId);

            System.out.println("map in error###= " + map);

//            updateInvoice = (Integer) session.update("report.updateInvoiceInTripMaster", map);
//            System.out.println("updateInvoiceInTripMaster ###==" + updateInvoice);
//
//            updateInvoice = (Integer) session.update("report.updateInvoiceInTripStatusdetails", map);
//            System.out.println("updateInvoiceInTripStatusdetails ###==" + updateInvoice);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateInvoice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "updateInvoice", sqlException);
        }
        return updateInvoice;

    }

    //    supplementary mail
    public ArrayList getSuppInvoiceListEmail() {
        Map map = new HashMap();
        System.out.println("IN dao....");
        ArrayList invoiceReport = new ArrayList();
        try {
            invoiceReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.getSuppInvoiceListEmail", map);
            System.out.println("getSuppInvoiceListEmail.size===" + invoiceReport.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceListEmail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getInvoiceListEmail", sqlException);
        }
        return invoiceReport;
    }

    public ArrayList getSuppInvoiceDetailsEmail(String invId) {
        Map map = new HashMap();
        //        System.out.println("IN dao....");
        ArrayList invoiceReport = new ArrayList();
        try {
            map.put("invId", invId);
//            System.out.println("map---" + map);

            invoiceReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.getSuppInvoiceDetailsEmail", map);
            System.out.println("getInvoiceDetailsEmail.size===" + invoiceReport.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceDetailsEmail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getInvoiceDetailsEmail", sqlException);
        }
        return invoiceReport;
    }

    public int insertSuppInvoiceFileDetails(ReportTO reportTO, SqlMapClient session) {
        Map map = new HashMap();
        int insertInvoiceFileDetails = 0;
        map.put("invoiceid", reportTO.getInvoiceId());
        map.put("custId", reportTO.getCustId());
        map.put("fileName", reportTO.getFileName());
        System.out.println("mapSSSS insertSuppInvoiceFileDetails=== " + map);
        try {
            System.out.println("sessionDAO:" + map);
            if (session != null) {

                insertInvoiceFileDetails = (Integer) session.update("report.insertSuppInvoiceFileDetails", map);
            } else {

                insertInvoiceFileDetails = (Integer) getSqlMapClientTemplate().update("report.insertSuppInvoiceFileDetails", map);
            }
            System.out.println("insertInvoiceFileDetails" + insertInvoiceFileDetails);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertInvoiceFileDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertInvoiceFileDetails", sqlException);
        }
        map = null;

        return insertInvoiceFileDetails;

    }

    public int updateSuppInvoiceCustomerMail(ReportTO reportTO) {
        Map map = new HashMap();
        int updateInvoiceCustomerMail = 0;
        try {
            map.put("invId", reportTO.getInvoiceId());
            System.out.println("map = " + map);
            updateInvoiceCustomerMail = (Integer) getSqlMapClientTemplate().update("report.updateSuppInvoiceCustomerMail", map);
            System.out.println("updateInvoiceCustomerMail==" + updateInvoiceCustomerMail);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateInvoiceCustomerMail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateSecondaryCustomerMail", sqlException);
        }
        return updateInvoiceCustomerMail;

    }

    public ArrayList getSuppCustomerListForEmail(String customerId) {
        Map map = new HashMap();
        //        System.out.println("IN dao....");
        ArrayList customerList = new ArrayList();
        map.put("customerId", customerId);
        try {
            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getSuppCustomerListForEmail", map);
            System.out.println("customerList.size===" + customerList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("customerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "customerList", sqlException);
        }
        return customerList;
    }

    public int updateSuppInvoiceCustomerSendMail(ReportTO reportTO) {
        Map map = new HashMap();
        int updateInvoiceCustomerSendMail = 0;
        try {
            if (reportTO.getInvIds().contains(",")) {
                String[] invId = reportTO.getInvIds().split(",");
                List invIds = new ArrayList(invId.length);
                for (int i = 0; i < invId.length; i++) {
                    invIds.add(invId[i]);
                }
                map.put("invId", invIds);
            } else {
                List invIds = new ArrayList();
                invIds.add(reportTO.getInvIds());
                map.put("invId", invIds);
            }
            System.out.println("map = " + map);

            int updateInvoiceFileStatus = (Integer) getSqlMapClientTemplate().update("report.updateSuppInvoiceFileStatus", map);
            System.out.println("updateInvoiceFileStatus==" + updateInvoiceFileStatus);

            updateInvoiceCustomerSendMail = (Integer) getSqlMapClientTemplate().update("report.updateSuppInvoiceCustomerSendMail", map);
            System.out.println("updateInvoiceCustomerSendMail==" + updateInvoiceCustomerSendMail);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */

            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("updateInvoiceCustomerSendMail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateSecondaryCustomerMail", sqlException);
        }
        return updateInvoiceCustomerSendMail;

    }

    public ArrayList getEmailSuppInvoiceDetails(String invoiceId) {
        Map map = new HashMap();
        ArrayList invoiceReport = new ArrayList();
        try {
            map.put("invoiceId", invoiceId);
            System.out.println("IN dao...." + map);
            invoiceReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.getEmailSuppInvoiceDetails", map);
            System.out.println("getEmailSuppInvoiceDetails.size===" + invoiceReport.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmailSuppInvoiceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getEmailSuppInvoiceDetails", sqlException);
        }
        return invoiceReport;
    }

    public int updateExpense(ReportTO reportTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        String expenceValue = "";
        String getTripExpenceId = "";
        String getTripVehicleId = "";
        map.put("userId", userId);
        try {
            map.put("grId", reportTO.getGrId());
            map.put("tripId", reportTO.getTripId());
            map.put("expenceValue1", reportTO.getExpenseValue());
            map.put("expenseName", reportTO.getExpenseName());
            System.out.println("map@@@ = " + map);
//            getTripExpenceId = (String) session.queryForObject("report.getTripExpenceId", map);

            System.out.println("expenceValueexpenceValue" + expenceValue);

            status = (Integer) session.update("report.updateTripFreight", map);

//            if (getTripExpenceId != "" && getTripExpenceId != null) {
//                map.put("getTripExpenceId", getTripExpenceId);
//                status = (Integer) session.update("report.updateTripExpense", map);
//
//            } else {
//                getTripVehicleId = (String) session.queryForObject("report.getTripVehicleId", map);
//                map.put("vehicleId", getTripVehicleId);
//                status = (Integer) session.update("report.insertTripExpense", map);
//            }
            // update in trip_gr table
            System.out.println("cancelGRno@@@ = " + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("cancelGRno Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "cancelGRno", sqlException);
        }
        return status;

    }

    public ArrayList getExpenseNameList() {
        Map map = new HashMap();
        ArrayList expenseNameList = new ArrayList();

        try {
            expenseNameList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getExpenseNameList", map);
            System.out.println(" expenseNameList =" + expenseNameList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("expenseNameList Error" + sqlException.toString());

            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleTypeList", sqlException);
        }
        return expenseNameList;
    }

//     billing party change
    public ArrayList getNonInvoiceGrNo(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList getNonInvoiceGrNo = new ArrayList();
        ArrayList getsupplyInvoiceNo = new ArrayList();
        map.put("invoiceNo", reportTO.getInvoiceNo() + "%");
        map.put("invoiceType", reportTO.getInvoiceType());
        try {
            String type = reportTO.getInvoiceType();
            System.out.println("type=========" + type);
            System.out.println(" getNonInvoiceGrNo = map is tht" + map);
            if ("1".equals(reportTO.getInvoiceType())) {
                System.out.println("in 1");
                getNonInvoiceGrNo = (ArrayList) getSqlMapClientTemplate().queryForList("report.getNonInvoiceGrNo", map);
                System.out.println("getNonInvoiceGrNo =" + getNonInvoiceGrNo.size());

            } else if ("2".equals(reportTO.getInvoiceType())) {
                System.out.println("in 2");
                getNonInvoiceGrNo = (ArrayList) getSqlMapClientTemplate().queryForList("report.getReopenedInvoiceNo", map);
                System.out.println("getReopenedInvoiceNo=" + getNonInvoiceGrNo.size());

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceNo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceNo List", sqlException);
        }

        return getNonInvoiceGrNo;
    }

    public int billingPartyChange(ReportTO reportTO, SqlMapClient session) {
        Map map = new HashMap();
        int billingPartyChange = 0;

//        String[] tripIdss = reportTO.getTripIdss();
        map.put("invoiceNo", reportTO.getInvoiceNo());
        map.put("customerId", reportTO.getCustomerId());
        map.put("invoiceId", reportTO.getInvoiceId());
        map.put("custId", reportTO.getCustId());
        map.put("invoiceType", reportTO.getInvoiceType());
        map.put("customerId", reportTO.getCustomerId());
        map.put("oldCustId", reportTO.getBillingPartyIdOld());
        String invoiceType = reportTO.getInvoiceType();
        System.out.println("MAP----" + map);
        try {
            if ("1".equals(reportTO.getInvoiceType())) {
                String custName = (String) session.queryForObject("report.getCustomerName", map);
                map.put("custName", custName);

                String CustomerAddress = (String) session.queryForObject("report.getCustomerAddress", map);
                map.put("CustomerAddress", CustomerAddress);

                for (int i = 0; i < reportTO.getTripIdss().length; i++) {
                    map.put("tripIdss", reportTO.getTripIdss()[i]);

                    map.put("selectedIds", reportTO.getSelectedIds()[i]);
                    {
                        System.out.println("tripIdss" + reportTO.getTripIdss()[i]);
                        System.out.println("tripIdss" + reportTO.getSelectedIds()[i]);
                        if ("1".equals(reportTO.getSelectedIds()[i])) {
                            String consignmentId = (String) session.queryForObject("report.getconsignmentId", map);
                            System.out.println("consignmentId=======================>" + consignmentId);

                            billingPartyChange = (Integer) session.update("report.updateBillingPartyChange", map);
                            billingPartyChange = (Integer) session.update("report.changeBillingPartyInvoice", map);

                        }
                    }
                }
            } else {
                String custName = (String) session.queryForObject("report.getCustomerName", map);
                map.put("custName", custName);
                System.out.println("custName------------" + custName);
                String CustomerAddress = (String) session.queryForObject("report.getCustomerAddress", map);
                map.put("CustomerAddress", CustomerAddress);
                System.out.println("CustomerAddress------------" + CustomerAddress);

                String CustomerGst = (String) session.queryForObject("report.CustomerGst", map);
                map.put("CustomerGst", CustomerGst);
                System.out.println("CustomerGst------------" + CustomerGst);

                String CustomerPan = (String) session.queryForObject("report.CustomerPan", map);
                map.put("CustomerPan", CustomerPan);
                System.out.println("CustomerPan------------" + CustomerPan);

                for (int i = 0; i < reportTO.getTripIdss().length; i++) {
                    map.put("tripIdss", reportTO.getTripIdss()[i]);

                    map.put("selectedIds", reportTO.getSelectedIds()[i]);
                    {

                        if ("1".equals(reportTO.getSelectedIds()[i])) {

                            String consignmentId = (String) session.queryForObject("report.getconsignmentId", map);
                            System.out.println("consignmentId=======================>" + consignmentId);

                            billingPartyChange = (Integer) session.update("report.updateBillingPartyChange", map);
                            billingPartyChange = (Integer) session.update("report.changeBillingPartyInvoice", map);

                            billingPartyChange = (Integer) session.update("report.changeBillingPartyInInvoice", map);

                        }
                    }
                }
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateInvoice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "updateInvoice", sqlException);
        }
        return billingPartyChange;

    }

    public ArrayList billPartyChangeDetails(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList billPartyChangeDetails = new ArrayList();
//        map.put("invoiceNo", reportTO.getInvoiceNo()+ "%");
        map.put("invoiceType", reportTO.getInvoiceType());
        map.put("invoiceNo", reportTO.getInvoiceNo());
        map.put("invoiceId", reportTO.getInvoiceId());
        try {
            if ("1".equals(reportTO.getInvoiceType())) {
                billPartyChangeDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.grNobillPartyChangeDetails", map);
                System.out.println(" billPartyChangeDetails =" + billPartyChangeDetails.size());
            }
            if ("2".equals(reportTO.getInvoiceType())) {
                billPartyChangeDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.invoiceNoPartyChangeDetails", map);
                System.out.println(" billPartyChangeDetails =" + billPartyChangeDetails.size());
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBillingParty Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBillingParty List", sqlException);
        }

        return billPartyChangeDetails;
    }

    public ArrayList getVehicleTripDetails(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList getVehicleTripDetails = new ArrayList();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        System.out.println("map" + map);
        try {
            System.out.println(" getVehicleTripDetails======== = map is tht" + map);
            getVehicleTripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getVehicleTripDetails", map);
            System.out.println(" processGRSummaryList =" + getVehicleTripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("processGRSummaryList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleTripDetails List", sqlException);
        }

        return getVehicleTripDetails;
    }

    public ArrayList getInvoiceNo(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList getInvoiceNo = new ArrayList();
        ArrayList getsupplyInvoiceNo = new ArrayList();
        map.put("invoiceNo", "%" + reportTO.getInvoiceNo() + "%");
        map.put("invoiceType", reportTO.getInvoiceType());
        try {
            String type = reportTO.getInvoiceType();
            System.out.println("type=========" + type);
            System.out.println(" getInvoiceNo = map is tht" + map);
            if ("1".equals(reportTO.getInvoiceType())) {
                System.out.println("in 1");
                getInvoiceNo = (ArrayList) getSqlMapClientTemplate().queryForList("report.getInvoiceNo", map);
                System.out.println(" getInvoiceNo =" + getInvoiceNo.size());
            }
            if ("2".equals(reportTO.getInvoiceType())) {
                System.out.println("in 2");
                getInvoiceNo = (ArrayList) getSqlMapClientTemplate().queryForList("report.getsupplyInvoiceNo", map);
                System.out.println(" getInvoiceNo =" + getInvoiceNo.size());

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceNo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceNo List", sqlException);
        }

        return getInvoiceNo;
    }

    public int creditUpdate(ReportTO reportTO, SqlMapClient session) {
        Map map = new HashMap();
        int creditUpdate = 0;

//        String[] tripIdss = reportTO.getTripIdss();
        map.put("invoiceNo", reportTO.getInvoiceNo());
        map.put("customerId", reportTO.getCustomerId());
        map.put("invoiceId", reportTO.getInvoiceId());
        map.put("custId", reportTO.getCustId());
        map.put("invoiceType", reportTO.getInvoiceType());
        map.put("tripIdss", reportTO.getTripIdss());

        System.out.println("MAP----" + map);
        try {

            for (int i = 0; i < reportTO.getTripIdss().length; i++) {
                map.put("tripIdss", reportTO.getTripIdss()[i]);

                map.put("selectedIds", reportTO.getSelectedIds()[i]);

                System.out.println("tripIdss" + reportTO.getTripIdss()[i]);
                System.out.println("tripIdss" + reportTO.getSelectedIds()[i]);
                if ("1".equals(reportTO.getSelectedIds()[i])) {
                    System.out.println("insideeeee=================================111111111111111111");
                    if ("1".equals(reportTO.getInvoiceType())) {
                        System.out.println("map in error###= " + map);
                        System.out.println("inide 1");
                        creditUpdate = (Integer) session.update("report.updateTripInvoiceInTripMaster", map);
                        System.out.println("updateInvoiceInTripMaster ###==" + creditUpdate);

                        creditUpdate = (Integer) session.update("report.updateTripInvoiceInTripStatusdetails", map);
                        System.out.println("updateInvoiceInTripStatusdetails ###==" + creditUpdate);
                    }
                    if ("2".equals(reportTO.getInvoiceType())) {

                        System.out.println("insideeeee=================================2222222222222222");
                        System.out.println("map in error###= " + map);
                        System.out.println("inide 2");
                        creditUpdate = (Integer) session.update("report.updateTripsupplyInvoiceInTripMaster", map);
                        System.out.println("updatesupplyInvoice ###==" + creditUpdate);

                        creditUpdate = (Integer) session.update("report.updateTripsupplyInvoiceInTripStatusdetails", map);
                        System.out.println("updatesupplyInvoice ###==" + creditUpdate);

                    }
                }
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateInvoice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "updateInvoice", sqlException);
        }
        return creditUpdate;

    }

    public ArrayList invoicecreditList(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList invoicecreditList = new ArrayList();
//        map.put("invoiceNo", reportTO.getInvoiceNo()+ "%");
        map.put("invoiceType", reportTO.getInvoiceType());
        map.put("invoiceId", reportTO.getInvoiceId());
        try {
            System.out.println(" getBillingParty = map is tht=============" + map);
            if ("2".equals(reportTO.getInvoiceType())) {
                System.out.println("2222222222222222222222222222222222222222222222222");
                invoicecreditList = (ArrayList) getSqlMapClientTemplate().queryForList("report.invoicecreditList", map);
                System.out.println(" invoicecreditList =" + invoicecreditList.size());
            }
            if ("1".equals(reportTO.getInvoiceType())) {
                System.out.println("11111111111111111111111111111111111111111111111111");
                invoicecreditList = (ArrayList) getSqlMapClientTemplate().queryForList("report.invoicecreditLists", map);
                System.out.println(" invoicecreditList =" + invoicecreditList.size());
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBillingParty Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBillingParty List", sqlException);
        }

        return invoicecreditList;
    }

    public int shippingBillNoUpdate() {
        Map map = new HashMap();
        int shippingBillNoUpdate = 0;
        int insertShippingLineNo = 0;
        int status = 0;
        ArrayList shippingLineNoList = new ArrayList();
        System.out.println("shippingLineNoList---------------------");
        TripTO tripTO = new TripTO();
        try {
            System.out.println("shippingLineNoList---------------------inside");
            shippingLineNoList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getSbillNoListTemp", map);
            System.out.println("shippingLineNoList--------------------" + shippingLineNoList.size());
            Iterator itr = shippingLineNoList.iterator();
            while (itr.hasNext()) {
                tripTO = (TripTO) itr.next();
                map.put("containerNo", tripTO.getContainerNo());
                map.put("containerNoLike", "%" + tripTO.getContainerNo() + "%");
                map.put("id", tripTO.getId());
                map.put("containerSize", tripTO.getContainerCapacity());
                map.put("containerType", tripTO.getContainerTypeName());
                map.put("shippingBillNo", tripTO.getShipingLineNo());
                String shippingDate = tripTO.getShippingBillDate();
                String gateInDate = tripTO.getGateInDate();
                System.out.println("shippingDate---" + shippingDate);
                System.out.println("gateInDate---" + gateInDate);
                map.put("shippingBillDate", shippingDate);
                map.put("gateInDate", gateInDate);
                System.out.println("accTemp-----" + tripTO.getGateInDate());
                map.put("requestNo", tripTO.getRequestNo());
                map.put("vehicleNo", tripTO.getVehicleNo());
                map.put("containerQty", tripTO.getContainerQty());
                map.put("commodityId", tripTO.getCommodityId());
                map.put("commodityName", tripTO.getCommodityName());
                map.put("createdBy", tripTO.getCreatedBy());
                System.out.println("map AAAA value is:" + map);

                int containerExist = (Integer) getSqlMapClientTemplate().queryForObject("report.existingContainers", map);
                int shippingNo = (Integer) getSqlMapClientTemplate().queryForObject("report.getContainerExistsForShippingBillNo", map);
                if (containerExist > 0) {
                    if (shippingNo == 0) {
                        String tripId = (String) getSqlMapClientTemplate().queryForObject("trip.getTripIdForShippingBillNo", map);
                        System.out.println("map value is:tripIdtripId" + tripId);
                        if (!"".equals(tripId) && tripId != null) {
                            String[] temp = tripId.split("~");
                            String[] tempTripIds = temp[0].split(",");
                            for (int i = 0; i < tempTripIds.length; i++) {

                                map.put("tripId", tempTripIds[i]);
                                map.put("statusCount", temp[1]);
                                System.out.println("map value is:" + map);
                                insertShippingLineNo += (Integer) getSqlMapClientTemplate().update("trip.insertShippingLineNoList", map);
                                System.out.println("insertShippingLineNo value is:" + status);
                                status = (Integer) getSqlMapClientTemplate().update("trip.updateShippingBillNoForTrip", map);
                                System.out.println("updateShippingBillNoForTrip value is:" + status);

                                status = (Integer) getSqlMapClientTemplate().update("trip.updateTripMasterforShippingLineNo", map);
                                System.out.println("updateTripMasterforShippingLineNo value is:" + status);

                                status = (Integer) getSqlMapClientTemplate().update("trip.updateTripArticleforShippingLineNo", map);
                                System.out.println("updateTripArticleforShippingLineNo value is:" + status);

                                if (status == 1) {
                                    status = (Integer) getSqlMapClientTemplate().update("trip.updateShippingBillNoCount", map);
                                    System.out.println("updateShippingBillNoCount value is:" + status);
                                }
                            }
                        }
                    }
                }
                int updateStatus = (Integer) getSqlMapClientTemplate().update("report.updateStatus", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBillingParty Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBillingParty List", sqlException);
        }

        return insertShippingLineNo;
    }

    public ArrayList getInvoiceforFtp(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList getInvoiceforFtp = new ArrayList();
        try {
            map.put("vehicleId", reportTO.getVehicleId());
            map.put("fleetCenterId", reportTO.getFleetCenterId());
            System.out.println("map = " + map);
            getInvoiceforFtp = (ArrayList) getSqlMapClientTemplate().queryForList("report.getInvoiceforFtp", map);
            System.out.println("vehicleDetailsList.size() = " + getInvoiceforFtp.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleDetailsList", sqlException);
        }
        return getInvoiceforFtp;
    }

    public int updateFlagInvoice(String invoiceId) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        try {
            map.put("invoiceId", invoiceId);
            System.out.println("map@@@ = " + map);
            status = (Integer) getSqlMapClientTemplate().update("report.updateFlagInvoice", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("cancelGRno Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "cancelGRno", sqlException);
        }
        return status;

    }

    // credit
    public ArrayList getInvoiceforFtpCredit(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList getInvoiceforFtpCredit = new ArrayList();
        try {
            map.put("vehicleId", reportTO.getVehicleId());
            map.put("fleetCenterId", reportTO.getFleetCenterId());
            System.out.println("map = " + map);
            getInvoiceforFtpCredit = (ArrayList) getSqlMapClientTemplate().queryForList("report.getInvoiceforFtpCredit", map);
            System.out.println("vehicleDetailsList.size() = " + getInvoiceforFtpCredit.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleDetailsList", sqlException);
        }
        return getInvoiceforFtpCredit;
    }

    public int updateFlagInvoiceCredit(String creditNoteId) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        try {
            map.put("invoiceId", creditNoteId);
            System.out.println("map@@@ = " + map);
            status = (Integer) getSqlMapClientTemplate().update("report.updateFlagInvoiceCredit", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("cancelGRno Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "cancelGRno", sqlException);
        }
        return status;

    }

    // SUPLLEMENT INVOICE
    public ArrayList getSuppInvoiceforFtp(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList getSuppInvoiceforFtp = new ArrayList();
        try {
            map.put("vehicleId", reportTO.getVehicleId());
            map.put("fleetCenterId", reportTO.getFleetCenterId());
            System.out.println("map = " + map);
            getSuppInvoiceforFtp = (ArrayList) getSqlMapClientTemplate().queryForList("report.getSuppInvoiceforFtp", map);
            System.out.println("vehicleDetailsList.size() = " + getSuppInvoiceforFtp.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleDetailsList", sqlException);
        }
        return getSuppInvoiceforFtp;
    }

    public int updateFlagSuppInvoice(String invoiceId) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        try {
            map.put("invoiceId", invoiceId);
            System.out.println("map@@@ = " + map);
            status = (Integer) getSqlMapClientTemplate().update("report.updateFlagSuppInvoice", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("cancelGRno Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "cancelGRno", sqlException);
        }
        return status;

    }

    // Supp. Creditnote Tables
    public ArrayList getSuppInvoiceforFtpCredit(ReportTO reportTO, int userId) {
        Map map = new HashMap();
        ArrayList getSuppInvoiceforFtpCredit = new ArrayList();
        try {
            map.put("vehicleId", reportTO.getVehicleId());
            map.put("fleetCenterId", reportTO.getFleetCenterId());
            System.out.println("map = " + map);
            getSuppInvoiceforFtpCredit = (ArrayList) getSqlMapClientTemplate().queryForList("report.getSuppInvoiceforFtpCredit", map);
            System.out.println("vehicleDetailsList.size() = " + getSuppInvoiceforFtpCredit.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getVehicleDetailsList", sqlException);
        }
        return getSuppInvoiceforFtpCredit;
    }

    public int updateFlagSuppInvoiceCredit(String invoiceId) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        try {
            map.put("invoiceId", invoiceId);
            System.out.println("map@@@ = " + map);
            status = (Integer) getSqlMapClientTemplate().update("report.updateFlagSuppInvoiceCredit", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("cancelGRno Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "cancelGRno", sqlException);
        }
        return status;

    }

    public int getEinvoiceList() {

        Map map = new HashMap();
        int getEinvoiceList = 0;
        try {
            getEinvoiceList = (Integer) getSqlMapClientTemplate().queryForObject("report.getEinvoiceListMail", map);
            System.out.println(" getEinvoiceList===" + getEinvoiceList);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEinvoiceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEinvoiceList List", sqlException);
        }

        return getEinvoiceList;

    }

    public int getESuppinvoiceList() {

        Map map = new HashMap();
        int getESuppinvoiceList = 0;
        try {
            getESuppinvoiceList = (Integer) getSqlMapClientTemplate().queryForObject("report.getESuppinvoiceList", map);
            System.out.println(" getESuppinvoiceList===" + getESuppinvoiceList);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEinvoiceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEinvoiceList List", sqlException);
        }

        return getESuppinvoiceList;

    }

    public int getECreditinvoiceList() {

        Map map = new HashMap();
        int getECreditinvoiceList = 0;
        try {
            getECreditinvoiceList = (Integer) getSqlMapClientTemplate().queryForObject("report.getECreditinvoiceList", map);
            System.out.println(" getECreditinvoiceList===" + getECreditinvoiceList);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEinvoiceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEinvoiceList List", sqlException);
        }

        return getECreditinvoiceList;

    }

    public int getECreditSuppinvoiceList() {

        Map map = new HashMap();
        int getECreditSuppinvoiceList = 0;
        try {
            getECreditSuppinvoiceList = (Integer) getSqlMapClientTemplate().queryForObject("report.getECreditSuppinvoiceList", map);
            System.out.println(" getECreditSuppinvoiceList===" + getECreditSuppinvoiceList);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEinvoiceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEinvoiceList List", sqlException);
        }

        return getECreditSuppinvoiceList;

    }

    public int getAutoEmailList() {

        Map map = new HashMap();
        int getAutoEmailList = 0;
        try {
            getAutoEmailList = (Integer) getSqlMapClientTemplate().queryForObject("report.getAutoEmailList", map);
            System.out.println(" getAutoEmailList===" + getAutoEmailList);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEinvoiceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEinvoiceList List", sqlException);
        }

        return getAutoEmailList;

    }

    public ArrayList resendInvoiceList() {
        Map map = new HashMap();
        ArrayList resendInvoiceList = new ArrayList();
        try {
            resendInvoiceList = (ArrayList) getSqlMapClientTemplate().queryForList("report.resendInvoiceList", map);
            System.out.println("resendInvoiceList.size() = " + resendInvoiceList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("resendInvoiceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "resendInvoiceList", sqlException);
        }
        return resendInvoiceList;
    }

    /////
    public ArrayList getResendInvoiceListEmail(String fromDate, String toDate, String custId) {
        Map map = new HashMap();
        System.out.println("IN dao....");
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("custId", custId);
        System.out.println("map----------" + map);
        ArrayList getResendInvoiceListEmail = new ArrayList();
        try {
            getResendInvoiceListEmail = (ArrayList) getSqlMapClientTemplate().queryForList("report.getResendInvoiceListEmail", map);
            System.out.println("getResendInvoiceListEmail.size===" + getResendInvoiceListEmail.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceListEmail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getResendInvoiceListEmail", sqlException);
        }
        return getResendInvoiceListEmail;
    }

    public int updateResendCustomerMail(String id) {
        Map map = new HashMap();
        int updateResendCustomerMail = 0;
        try {
            map.put("id", id);
            System.out.println("map = " + map);
            updateResendCustomerMail = (Integer) getSqlMapClientTemplate().update("report.updateResendCustomerMail", map);
            System.out.println("updateResendCustomerMail==" + updateResendCustomerMail);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateResendCustomerMail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateResendCustomerMail", sqlException);
        }
        return updateResendCustomerMail;

    }

    public ArrayList getUniqueCustomerList() {
        Map map = new HashMap();
        System.out.println("IN dao....et c l f m");
        ArrayList getUniqueCustomerList = new ArrayList();
        try {
            getUniqueCustomerList = (ArrayList) getSqlMapClientTemplate().queryForList("report.getUniqueCustomerList", map);
            System.out.println("getUniqueCustomerList.size===" + getUniqueCustomerList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("customerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getUniqueCustomerList", sqlException);
        }
        return getUniqueCustomerList;
    }

    public ArrayList getCustomerMasterLogReport(ReportTO reportTO) {
        Map map = new HashMap();
        System.out.println("IN dao....et c l f m");
        ArrayList customerMasterLogReport = new ArrayList();
        try {

            System.out.println("fromdate--------" + reportTO.getFromDate());
            System.out.println("todate--------" + reportTO.getToDate());
            map.put("fromDate", reportTO.getFromDate());
            map.put("toDate", reportTO.getToDate());
            map.put("custId", reportTO.getCustomerId());
            customerMasterLogReport = (ArrayList) getSqlMapClientTemplate().queryForList("report.customerMasterLogReport", map);
            System.out.println("customerMasterLogReport.size===" + customerMasterLogReport.size());
            System.out.println("customerMasterLogReport==========" + customerMasterLogReport);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("customerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getUniqueCustomerList", sqlException);
        }
        return customerMasterLogReport;
    }

    public ArrayList uniqueSuppCustomerList() {
        Map map = new HashMap();
        System.out.println("IN dao....et c l f m");
        ArrayList uniqueSuppCustomerList = new ArrayList();
        try {
            uniqueSuppCustomerList = (ArrayList) getSqlMapClientTemplate().queryForList("report.uniqueSuppCustomerList", map);
            System.out.println("uniqueSuppCustomerList.size===" + uniqueSuppCustomerList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("customerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getUniqueCustomerList", sqlException);
        }
        return uniqueSuppCustomerList;
    }

    public int getAutoEmailSuppList() {

        Map map = new HashMap();
        int getAutoEmailSuppList = 0;
        try {
            getAutoEmailSuppList = (Integer) getSqlMapClientTemplate().queryForObject("report.getAutoEmailSuppList", map);
            System.out.println(" getAutoEmailSuppList===" + getAutoEmailSuppList);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEinvoiceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getAutoEmailSuppList List", sqlException);
        }

        return getAutoEmailSuppList;

    }

    public ArrayList invoiceListForXml() {
        Map map = new HashMap();
        ArrayList invoiceListForXml = new ArrayList();
        try {
            invoiceListForXml = (ArrayList) getSqlMapClientTemplate().queryForList("report.invoiceListForXml", map);
            System.out.println("invoiceListForXml.size===" + invoiceListForXml.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("invoiceListForXml Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "invoiceListForXml", sqlException);
        }
        return invoiceListForXml;
    }

    public ArrayList suppInvoiceListForXml() {
        Map map = new HashMap();
        ArrayList suppInvoiceListForXml = new ArrayList();
        try {
            suppInvoiceListForXml = (ArrayList) getSqlMapClientTemplate().queryForList("report.suppInvoiceListForXml", map);
            System.out.println("suppInvoiceListForXml.size===" + suppInvoiceListForXml.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("invoiceListForXml Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "invoiceListForXml", sqlException);
        }
        return suppInvoiceListForXml;
    }

    public ArrayList creaditInvoiceListForXml() {
        Map map = new HashMap();
        ArrayList creaditInvoiceListForXml = new ArrayList();
        try {
            creaditInvoiceListForXml = (ArrayList) getSqlMapClientTemplate().queryForList("report.creaditInvoiceListForXml", map);
            System.out.println("creaditInvoiceListForXml.size===" + creaditInvoiceListForXml.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("creaditInvoiceListForXml Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "creaditInvoiceListForXml", sqlException);
        }
        return creaditInvoiceListForXml;
    }

    public ArrayList suppCreaditInvoiceListForXml() {
        Map map = new HashMap();
        ArrayList suppCreaditInvoiceListForXml = new ArrayList();
        try {
            suppCreaditInvoiceListForXml = (ArrayList) getSqlMapClientTemplate().queryForList("report.suppCreaditInvoiceListForXml", map);
            System.out.println("creaditInvoiceListForXml.size===" + suppCreaditInvoiceListForXml.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("creaditInvoiceListForXml Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "creaditInvoiceListForXml", sqlException);
        }
        return suppCreaditInvoiceListForXml;
    }

    public String getTodayDate(int ii) {

        Map map = new HashMap();
        String getTodayDate = "";
        map.put("ii", ii);
        try {
            getTodayDate = (String) getSqlMapClientTemplate().queryForObject("report.getTodayDate", map);
            System.out.println(" getTodayDate===" + getTodayDate);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEinvoiceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEinvoiceList List", sqlException);
        }

        return getTodayDate;

    }

    public int updateInvoiceLog(String File, String type, String invoiceId) {

        Map map = new HashMap();
        int insertLog = 0;
        Integer updateInHeader = 0;
        map.put("fileName", File);
        map.put("type", type);
        map.put("invoiceId", invoiceId);
        map.put("status", 0);
        map.put("userId", "1403");
        try {
            if ("1".equals(type)) {
                insertLog = (Integer) getSqlMapClientTemplate().insert("report.insertLogInvoice", map);
//                updateInHeader = (Integer) getSqlMapClientTemplate().update("report.updateInvHeader", map);
            }
            if ("2".equals(type)) {
                insertLog = (Integer) getSqlMapClientTemplate().insert("report.insertLogSuppInvoice", map);
//                updateInHeader = (Integer) getSqlMapClientTemplate().update("report.updateSuppInvHeader", map);
            }
            if ("3".equals(type)) {
                insertLog = (Integer) getSqlMapClientTemplate().insert("report.insertLogCreditInvoice", map);
//                updateInHeader = (Integer) getSqlMapClientTemplate().update("report.updateCreditInvHeader", map);
            }
            if ("4".equals(type)) {
                insertLog = (Integer) getSqlMapClientTemplate().insert("report.insertLogSuppCreditInvoice", map);
//                updateInHeader = (Integer) getSqlMapClientTemplate().update("report.updateSuppCreditInvHeader", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEinvoiceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEinvoiceList List", sqlException);
        }

        return insertLog;

    }

    public ArrayList getInvoiceFileForMove() {
        Map map = new HashMap();
        System.out.println("IN dao....");

        ArrayList getInvoiceFileForMove = new ArrayList();
        try {
            getInvoiceFileForMove = (ArrayList) getSqlMapClientTemplate().queryForList("report.getInvoiceFileForMove", map);
            System.out.println("getInvoiceFileForMove.size===" + getInvoiceFileForMove.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceFileForMove Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "getInvoiceFileForMove", sqlException);
        }
        return getInvoiceFileForMove;
    }

    public ArrayList suppInvoiceFileForMove() {
        Map map = new HashMap();
        System.out.println("IN dao....");

        ArrayList suppInvoiceFileForMove = new ArrayList();
        try {
            suppInvoiceFileForMove = (ArrayList) getSqlMapClientTemplate().queryForList("report.suppInvoiceFileForMove", map);
            System.out.println("getInvoiceFileForMove.size===" + suppInvoiceFileForMove.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("suppInvoiceFileForMove Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "suppInvoiceFileForMove", sqlException);
        }
        return suppInvoiceFileForMove;
    }

    public ArrayList creditInvoiceFileForMove() {
        Map map = new HashMap();
        System.out.println("IN dao....");

        ArrayList creditInvoiceFileForMove = new ArrayList();
        try {
            creditInvoiceFileForMove = (ArrayList) getSqlMapClientTemplate().queryForList("report.creditInvoiceFileForMove", map);
            System.out.println("creditInvoiceFileForMove.size===" + creditInvoiceFileForMove.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("suppInvoiceFileForMove Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "suppInvoiceFileForMove", sqlException);
        }
        return creditInvoiceFileForMove;
    }

    public ArrayList creditSuppInvoiceFileForMove() {
        Map map = new HashMap();
        System.out.println("IN dao....");

        ArrayList creditSuppInvoiceFileForMove = new ArrayList();
        try {
            creditSuppInvoiceFileForMove = (ArrayList) getSqlMapClientTemplate().queryForList("report.creditSuppInvoiceFileForMove", map);
            System.out.println("creditSuppInvoiceFileForMove.size===" + creditSuppInvoiceFileForMove.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("creditSuppInvoiceFileForMove Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "creditSuppInvoiceFileForMove", sqlException);
        }
        return creditSuppInvoiceFileForMove;
    }

    public int updateStatusFtpMoveStatus(String id, String type) {

        Map map = new HashMap();
        int updateStatusFtpMoveStatus = 0;

        map.put("id", id);
        map.put("type", type);

        try {
            if ("1".equals(type)) {
                updateStatusFtpMoveStatus = (Integer) getSqlMapClientTemplate().update("report.updateInvHeaderFtp", map);
            }
            if ("2".equals(type)) {
                updateStatusFtpMoveStatus = (Integer) getSqlMapClientTemplate().update("report.updateSuppInvHeaderFtp", map);
            }
            if ("3".equals(type)) {
                updateStatusFtpMoveStatus = (Integer) getSqlMapClientTemplate().update("report.updateCreditInvHeaderFtp", map);
            }
            if ("4".equals(type)) {
                updateStatusFtpMoveStatus = (Integer) getSqlMapClientTemplate().update("report.updateSuppCreditInvHeaderFtp", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEinvoiceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEinvoiceList List", sqlException);
        }

        return updateStatusFtpMoveStatus;

    }

    public int updateInHeaderCsvStatus(String invoiceNo, String xmlStatus, String reMarks) {

        Map map = new HashMap();
        int updateInHeaderCsvStatus = 0;
        int invoiceCount = 0;
        int invoiceCsvUpdate = 0;
        int suppInvoiceCount = 0;
        int suppInvoiceCsvUpdate = 0;
        int creditInvoiceCount = 0;
        int creditInvoiceCsvUpdate = 0;
        int suppCreditInvoiceCount = 0;
        int suppCreditInvoiceCsvUpdate = 0;

        map.put("invoiceCode", invoiceNo);

        try {
            invoiceCount = (Integer) getSqlMapClientTemplate().queryForObject("report.invoiceCount", map);
            System.out.println("invoiceCount" + invoiceCount);

            suppInvoiceCount = (Integer) getSqlMapClientTemplate().queryForObject("report.suppInvoiceCount", map);
            System.out.println("suppInvoiceCount" + suppInvoiceCount);

            creditInvoiceCount = (Integer) getSqlMapClientTemplate().queryForObject("report.creditInvoiceCount", map);
            System.out.println("creditInvoiceCount" + creditInvoiceCount);

            suppCreditInvoiceCount = (Integer) getSqlMapClientTemplate().queryForObject("report.suppCreditInvoiceCount", map);
            System.out.println("suppCreditInvoiceCount" + suppCreditInvoiceCount);

            if (invoiceCount > 0) {

                String invoiceId = (String) getSqlMapClientTemplate().queryForObject("report.getInvoiceId", map);
                map.put("invoiceId", invoiceId);
                map.put("reMarks", reMarks);
                if ("Posted".equals(xmlStatus)) {
                    String[] splitRemarks = reMarks.split(" ");
                    String sapCode = splitRemarks[0];
                    map.put("sapCode", sapCode);
                    map.put("updateStatus", "1");
                } else {
                    map.put("sapCode", "0");
                    map.put("updateStatus", "2");
                }
//                int updateInvoiceXml = (Integer) getSqlMapClientTemplate().update("report.updateInvoiceXml", map);
                invoiceCsvUpdate = (Integer) getSqlMapClientTemplate().update("report.invoiceCsvUpdate", map);
            }
            if (suppInvoiceCount > 0) {
                String invoiceId = (String) getSqlMapClientTemplate().queryForObject("report.getSuppInvoiceId", map);
                map.put("invoiceId", invoiceId);
                map.put("reMarks", reMarks);
                if ("Posted".equals(xmlStatus)) {
                    String[] splitRemarks = reMarks.split(" ");
                    String sapCode = splitRemarks[0];
                    map.put("sapCode", sapCode);
                    map.put("updateStatus", "1");
                } else {
                    map.put("sapCode", "0");
                    map.put("updateStatus", "2");
                }
//                int updateInvoiceXml = (Integer) getSqlMapClientTemplate().update("report.updateSuppInvoiceXml", map);
                suppInvoiceCsvUpdate = (Integer) getSqlMapClientTemplate().update("report.suppInvoiceCsvUpdate", map);
            }
            if (creditInvoiceCount > 0) {
                String invoiceId = (String) getSqlMapClientTemplate().queryForObject("report.getCreditInvoiceId", map);
                map.put("invoiceId", invoiceId);
                map.put("reMarks", reMarks);
                if ("Posted".equals(xmlStatus)) {
                    String[] splitRemarks = reMarks.split(" ");
                    String sapCode = splitRemarks[0];
                    map.put("sapCode", sapCode);
                    map.put("updateStatus", "1");
                } else {
                    map.put("sapCode", "0");
                    map.put("updateStatus", "2");
                }
//                int updateInvoiceXml = (Integer) getSqlMapClientTemplate().update("report.updateCreditnvoiceXml", map);
                creditInvoiceCsvUpdate = (Integer) getSqlMapClientTemplate().update("report.creditInvoiceCsvUpdate", map);
            }
            if (suppCreditInvoiceCount > 0) {
                String invoiceId = (String) getSqlMapClientTemplate().queryForObject("report.getSuppCreditInvoiceId", map);
                map.put("invoiceId", invoiceId);
                map.put("reMarks", reMarks);
                if ("Posted".equals(xmlStatus)) {
                    String[] splitRemarks = reMarks.split(" ");
                    String sapCode = splitRemarks[0];
                    map.put("sapCode", sapCode);
                    map.put("updateStatus", "1");
                } else {
                    map.put("sapCode", "0");
                    map.put("updateStatus", "2");
                }
//                int updateInvoiceXml = (Integer) getSqlMapClientTemplate().update("report.updateSuppCreditnvoiceXml", map);
                suppCreditInvoiceCsvUpdate = (Integer) getSqlMapClientTemplate().update("report.suppCreditInvoiceCsvUpdate", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEinvoiceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEinvoiceList List", sqlException);
        }

        return updateInHeaderCsvStatus;

    }

    public ArrayList getConsignorChange(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList getConsignorChange = new ArrayList();
        ArrayList getsupplyConsignorNo = new ArrayList();
        map.put("consignorNo", "%" + reportTO.getInvoiceNo() + "%");
        map.put("invoiceType", reportTO.getInvoiceType());
        try {
            String type = reportTO.getInvoiceType();
            System.out.println("type=========" + type);
            System.out.println(" getConsignorChange = map is tht" + map);

            System.out.println("in 1");
            getConsignorChange = (ArrayList) getSqlMapClientTemplate().queryForList("report.getConsignorChanges", map);
            System.out.println("getConsignorChange =" + getConsignorChange.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignorNo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConsignorNo List", sqlException);
        }

        return getConsignorChange;
    }

    public ArrayList getConsignmentOrder(ReportTO reportTO) {
        Map map = new HashMap();
        ArrayList getConsignmentOrder = new ArrayList();
        ArrayList getsupplyConsignmentOrder = new ArrayList();
        map.put("consignmentOrder", "%" + reportTO.getConsignmentOrder() + "%");
//        map.put("invoiceType", reportTO.getInvoiceType());
        try {
            String type = reportTO.getInvoiceType();
            System.out.println("type=========" + type);
            System.out.println(" getConsignmentOrder = map is tht" + map);

            System.out.println("in 1");
            getConsignmentOrder = (ArrayList) getSqlMapClientTemplate().queryForList("report.getConsignmentOrder", map);
            System.out.println("getConsignmentOrder =" + getConsignmentOrder.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentOrder Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConsignmentOrder List", sqlException);
        }

        return getConsignmentOrder;
    }

    public int updateConsignorChanges(ReportTO reportTO) {
        Map map = new HashMap();
        int status = 0;
        int selectName = 0;

//        ArrayList getConsignmentOrder = new ArrayList();
//        ArrayList getsupplyConsignmentOrder = new ArrayList();
        map.put("selectName", reportTO.getSelectName());
        map.put("consignorName", reportTO.getConsignorName());
        map.put("consignmentOrder", reportTO.getConsignmentOrder());
        map.put("customerId", reportTO.getCustomerId());
        map.put("consignmentId", reportTO.getConsignmentId());
        map.put("consignerMobile", reportTO.getConsignerMobile());
        map.put("consignerAddress", reportTO.getConsignerAddress());
        try {
            if ("1".equals(reportTO.getSelectName())) {
                System.out.println("===================================================================");
                System.out.println(" updateConsignorChanges = map is tht" + map);
                status = (Integer) getSqlMapClientTemplate().update("report.updateConsignorChanges1", map);
                System.out.println("updateConsignorChanges =" + status);
            } else if ("2".equals(reportTO.getSelectName())) {
                System.out.println(" updateConsignorChanges = map is tht" + map);
                status = (Integer) getSqlMapClientTemplate().update("report.updateConsignorChanges2", map);
                System.out.println("updateConsignorChanges =" + status);

            }

        } catch (Exception sqlException) {
//            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateConsignorChanges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "updateConsignorChanges", sqlException);
        }

        return status;
    }

    public int updateInvoiceLogManual(String File, String type, int userId) {

        Map map = new HashMap();
        int insertLog = 0;
        Integer updateInHeader = 0;
        map.put("fileName", File);
        map.put("type", type);
        map.put("invoiceId", 0);
        map.put("status", 0);
        map.put("userId", userId);
        try {
            if ("1".equals(type)) {
                insertLog = (Integer) getSqlMapClientTemplate().insert("report.insertLogInvoice", map);
//                updateInHeader = (Integer) getSqlMapClientTemplate().update("report.updateInvHeader", map);
            }
            if ("2".equals(type)) {
                insertLog = (Integer) getSqlMapClientTemplate().insert("report.insertLogSuppInvoice", map);
//                updateInHeader = (Integer) getSqlMapClientTemplate().update("report.updateSuppInvHeader", map);
            }
            if ("3".equals(type)) {
                insertLog = (Integer) getSqlMapClientTemplate().insert("report.insertLogCreditInvoice", map);
//                updateInHeader = (Integer) getSqlMapClientTemplate().update("report.updateCreditInvHeader", map);
            }
            if ("4".equals(type)) {
                insertLog = (Integer) getSqlMapClientTemplate().insert("report.insertLogSuppCreditInvoice", map);
//                updateInHeader = (Integer) getSqlMapClientTemplate().update("report.updateSuppCreditInvHeader", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEinvoiceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEinvoiceList List", sqlException);
        }

        return insertLog;

    }

    public ArrayList invoiceForEinv() {
        Map map = new HashMap();
        ArrayList invoiceForEinv = new ArrayList();
        try {
            System.out.println("hello-----------------");
            invoiceForEinv = (ArrayList) getSqlMapClientTemplate().queryForList("report.invoiceForEinv", map);
            //////System.out.println("rateList size=" + rateList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("rateList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "rateList", sqlException);
        }
        return invoiceForEinv;
    }

    public ArrayList invoiceForESuppinv() {
        Map map = new HashMap();
        ArrayList invoiceForESuppinv = new ArrayList();
        try {
            System.out.println("hello-----------------");
            invoiceForESuppinv = (ArrayList) getSqlMapClientTemplate().queryForList("report.invoiceForESuppinv", map);
            //////System.out.println("rateList size=" + rateList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("rateList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "rateList", sqlException);
        }
        return invoiceForESuppinv;
    }

    public ArrayList invoiceForECreditinv() {
        Map map = new HashMap();
        ArrayList invoiceForECreditinv = new ArrayList();
        try {
            System.out.println("hello-----------------");
            invoiceForECreditinv = (ArrayList) getSqlMapClientTemplate().queryForList("report.invoiceForECreditinv", map);
            //////System.out.println("rateList size=" + rateList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("rateList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "rateList", sqlException);
        }
        return invoiceForECreditinv;
    }

    public ArrayList invoiceForESuppCreditinv() {
        Map map = new HashMap();
        ArrayList invoiceForESuppCreditinv = new ArrayList();
        try {
            System.out.println("hello-----------------");
            invoiceForESuppCreditinv = (ArrayList) getSqlMapClientTemplate().queryForList("report.invoiceForESuppCreditinv", map);
            //////System.out.println("rateList size=" + rateList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("rateList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-GEN-01", CLASS, "rateList", sqlException);
        }
        return invoiceForESuppCreditinv;
    }

    public ArrayList getInvoiceCountValues(ReportTO report) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList getInvoiceCountValues = new ArrayList();
        try {
            map.put("companyId", report.getCompanyId());
            System.out.println("primeMoverDocumentsValidity: DAO" + map);
            getInvoiceCountValues = (ArrayList) getSqlMapClientTemplate().queryForList("report.getInvoiceCountValues", map);
            System.out.println("primeMoverDocumentsValidity:" + getInvoiceCountValues.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCountValues Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getInvoiceCountValues", sqlException);
        }
        return getInvoiceCountValues;
    }

    public ArrayList getFinanceEInvoiceValues(ReportTO reportTO) {
        Map map = new HashMap();
        System.out.println("im getGrPrintDetails");
        ArrayList printDetails = new ArrayList();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        map.put("grNo", reportTO.getGrNo());
        System.out.println("map = " + map);
        try {
            printDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getFinanceEInvoiceValues", map);
            System.out.println("getGrPrintDetails size:" + printDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAgentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "customerWiseProfitability", sqlException);
        }
        return printDetails;

    }

    public ArrayList getSuppFinanceEInvoiceValues(ReportTO reportTO) {
        Map map = new HashMap();
        System.out.println("im getGrPrintDetails");
        ArrayList printDetails = new ArrayList();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        map.put("grNo", reportTO.getGrNo());
        System.out.println("map = " + map);
        try {
            printDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getSuppFinanceEInvoiceValues", map);
            System.out.println("getGrPrintDetails size:" + printDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAgentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "customerWiseProfitability", sqlException);
        }
        return printDetails;

    }

    public ArrayList getCreditFinanceEInvoiceValues(ReportTO reportTO) {
        Map map = new HashMap();
        System.out.println("im getGrPrintDetails");
        ArrayList printDetails = new ArrayList();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        map.put("grNo", reportTO.getGrNo());
        System.out.println("map = " + map);
        try {
            printDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getCreditFinanceEInvoiceValues", map);
            System.out.println("getGrPrintDetails size:" + printDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAgentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "customerWiseProfitability", sqlException);
        }
        return printDetails;

    }

    public ArrayList getSuppCreditFinanceEInvoiceValues(ReportTO reportTO) {
        Map map = new HashMap();
        System.out.println("im getGrPrintDetails");
        ArrayList printDetails = new ArrayList();
        map.put("fromDate", reportTO.getFromDate());
        map.put("toDate", reportTO.getToDate());
        map.put("grNo", reportTO.getGrNo());
        System.out.println("map = " + map);
        try {
            printDetails = (ArrayList) getSqlMapClientTemplate().queryForList("report.getSuppCreditFinanceEInvoiceValues", map);
            System.out.println("getGrPrintDetails size:" + printDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAgentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "customerWiseProfitability", sqlException);
        }
        return printDetails;

    }

    public String getDetentionChargeDetails(ReportTO reportTO) {
        Map map = new HashMap();
        System.out.println("im getGrPrintDetails");
        String getDetentionChargeDetails = "";
        map.put("tripSheetId", reportTO.getTripSheetId());
        map.put("totalDays", reportTO.getTotalDays());
        String totalDays = reportTO.getTotalDays();
        map.put("tarrifType", reportTO.getTarrifType());
        try {
            System.out.println("map = " + map);
            String getVehicleTypeId = (String) getSqlMapClientTemplate().queryForObject("report.getVehicleTypeId", map);
            map.put("vehicleType", getVehicleTypeId);
            System.out.println("map2 = " + map);
            String getCustomerId = (String) getSqlMapClientTemplate().queryForObject("report.getCustomerIdFromTrip", map);
            map.put("customerId", getCustomerId);
            System.out.println("map3 = " + map);
            String getContractId = (String) getSqlMapClientTemplate().queryForObject("report.getContractId", map);
            map.put("ContractId", getContractId);
            String getdetentionRate = (String) getSqlMapClientTemplate().queryForObject("report.getdetentionRate", map);
            map.put("getdetentionRate", getdetentionRate);
            String detentionRate = getdetentionRate;
            if (detentionRate == null) {
                detentionRate = "0";
            }
            System.out.println("detentionRate = " + detentionRate);
            System.out.println("map3 = " + map);

            getDetentionChargeDetails = String.valueOf(Integer.parseInt(detentionRate) * Integer.parseInt(totalDays));
            System.out.println("map3 = " + map);

            System.out.println("getDetentionChargeDetails size:" + getDetentionChargeDetails);
//            getDetentionChargeDetails = (String) getSqlMapClientTemplate().queryForObject("report.getDetentionChargeDetails", map);
            System.out.println("getDetentionChargeDetails size:" + getDetentionChargeDetails);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAgentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "customerWiseProfitability", sqlException);
        }
        return getDetentionChargeDetails;

    }

    public ArrayList getSuppInvoiceReport() {
        Map map = new HashMap();
        ArrayList suppinvoice = new ArrayList();
        try {
            suppinvoice = (ArrayList) getSqlMapClientTemplate().queryForList("report.getSuppInvoiceReport", map);
            System.out.println("getGrPrintDetails size:" + suppinvoice.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAgentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "customerWiseProfitability", sqlException);
        }
        return suppinvoice;

    }

    public ArrayList getMainInvoiceReport() {
        Map map = new HashMap();
        ArrayList mainInvoice = new ArrayList();
        try {
            mainInvoice = (ArrayList) getSqlMapClientTemplate().queryForList("report.getMainInvoiceReport", map);
            System.out.println("getGrPrintDetails size:" + mainInvoice.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAgentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "customerWiseProfitability", sqlException);
        }
        return mainInvoice;

    }
    public int getGpsDetails() {
        Map map = new HashMap();
        int status = 0;
        String vehicleId = null;
        String regno = null;
        String dateTime = null;
        String fromDate = null;
        TripTO tripTONew = new TripTO();
        String gpsId = null;
//        String result = null;
        String companyId = null;
        ArrayList getVehicleGpsData = new ArrayList();
        String Veh_reg = null;
        String Updated = null;
        String Location = null;
        Double Latitude = null;
        Double Longitude = null;
        String KM_Travelled = null;
        String Speed = null;
        String geoLocation = null;
        String geoLocationInd = null;
        String geoLocationDT = null;
        ReportTO reportTO1 = new ReportTO();

        try {

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            URL url = new URL("https://maxtrax.in/dict.ashx");
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            /* Payload support */
//            con.setDoOutput(true);
//            DataOutputStream out = new DataOutputStream(con.getOutputStream());
//            out.writeBytes("client_id=2917567e-0e49-4fa0-b7fc-da9bba718f1e&client_secret=08ea52eb-4084-49d8-8cf1-8b964aa50928&grant_type=client_credentials");
//            out.flush();
//            out.close();
            int statuss = con.getResponseCode();
            System.out.println("=============================");
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            con.disconnect();

            JSONObject jObject = new JSONObject(content.toString());
            String result = jObject.get("result").toString();
            System.out.println("result ======" + result);

//            JSONArray jsonobj = (JSONArray) jObject.get("result");
            JSONArray jsonobj = (JSONArray) new JSONParser().parse(result);
            System.out.println("jsonobj ============" + jsonobj);

            for (int i = 0; i < jsonobj.size(); i++) {

                org.json.simple.JSONObject jsonob = (org.json.simple.JSONObject) jsonobj.get(i);

                regno = (String) jsonob.get("vname");
                System.out.println("regno ========================================" + regno);
                fromDate = (String) jsonob.get("dttime");
                System.out.println("fromDate ========================================" + fromDate);
                dateTime = fromDate.replace(".000","");
                System.out.println("dateTime ========================================" + dateTime);
                if("0001-01-01 00:00:00".equalsIgnoreCase(dateTime)){
                    Latitude = 0.00;
                    Longitude = 0.00;
                map.put("dateTime", "0000-00-00 00:00:00");
                map.put("Latitude", Latitude);
                map.put("Longitude", Longitude);
                } else {
                Latitude = (Double) jsonob.get("lat");
                Longitude = (Double) jsonob.get("lngt");
                map.put("dateTime", dateTime);
                map.put("Latitude", Latitude);
                map.put("Longitude", Longitude);
                }
                System.out.println("Latitude ========================================" + Latitude);
                System.out.println("Longitude ========================================" + Longitude);

                map.put("regno", regno);

                vehicleId = (String) getSqlMapClientTemplate().queryForObject("report.getVehicleId", map);
                map.put("vehicleId", vehicleId);
                System.out.println("vehicleId ========================================" + vehicleId);
                System.out.println("vehicleId ========================================" + vehicleId);

                if (vehicleId != null) {
                    System.out.println("entered.................................");
                    String vehicleCount = (String) getSqlMapClientTemplate().queryForObject("report.getvehicleCount", map);
                    System.out.println("vehicleCount................................." + vehicleCount);
                    if ("0".equals(vehicleCount)) {
                        System.out.println("insert.................................");

                        int insertVehicleLocation = (Integer) getSqlMapClientTemplate().update("report.insertVehicleLocation", map);
                        System.out.println("insertssssss................................." + insertVehicleLocation);
                    } else {
                        System.out.println("update.................................");
                        int updateVehicleLocation = (Integer) getSqlMapClientTemplate().update("report.updateVehicleLocation", map);
                        System.out.println("updatesssssss................................." + updateVehicleLocation);

                    }

                }

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripDriver Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripDriver", sqlException);
        }
        return status;
    }

    public int updateCnoteChanges(String tripId, String consignmentId, String consignmentIdNew, String consignmentOrder, String consignmentOrderNew, SqlMapClient session) {
        Map map = new HashMap();
        int updateCnoteChanges = 0;
        int updateIntripConsignment = 0;
        ArrayList conId = new ArrayList();
        ArrayList tripPoints = new ArrayList();
        ReportTO repTO = new ReportTO();
        String pointCities = "";
        String cityName = "";

        String routeInfo = "";
        try {
            map.put("tripId", tripId);  // trip
            map.put("consignmentId", consignmentId); // current cnote id
            map.put("consignmentIdNew", consignmentIdNew);  // new cnote id
            map.put("consignmentOrder", consignmentOrder);  // current cnote no
            map.put("consignmentOrderNew", consignmentOrderNew);  // new cnote id
            System.out.println("map---" + map);

            updateIntripConsignment = (Integer) session.update("report.updateIntripConsignment", map);
            System.out.println("updateIntripConsignment---" + updateIntripConsignment);
            conId = (ArrayList) session.queryForList("report.getConId", map);
            Iterator itr = conId.iterator();
            while (itr.hasNext()) {
                repTO = new ReportTO();
                repTO = (ReportTO) itr.next();
                map.put("containerId", repTO.getDetailId());
                int updateInConsignmentContainer = (Integer) session.update("report.updateInConsignmentContainer", map);
                System.out.println("updateInConsignmentContainer---" + updateInConsignmentContainer);
            }
            int updateInTripContainer = (Integer) session.update("report.updateInTripContainer", map);
            System.out.println("updateInTripContainer---" + updateInTripContainer);
            int updateInTripRouteCourse = (Integer) session.update("report.updateInTripRouteCourse", map);
            System.out.println("updateInTripRouteCourse---" + updateInTripRouteCourse);

            tripPoints = (ArrayList) session.queryForList("report.tripPoints", map);
            Iterator itr2 = tripPoints.iterator();
            while (itr2.hasNext()) {
                repTO = new ReportTO();
                repTO = (ReportTO) itr2.next();

                map.put("containerId", repTO.getVendorName());
                map.put("pointId", repTO.getPointId());
                map.put("pointType", repTO.getPointType());
                map.put("pointOrder", repTO.getPointSequence());
                map.put("pointAddresss", repTO.getPointAddress());
                map.put("pointPlanDate", repTO.getPointPlanDate());
                map.put("pointPlanTime", repTO.getPointPlanTime());
                map.put("userId", repTO.getCreatedOn());

                System.out.println("map 2---" + map);
                int updateTripInRoute = (Integer) session.update("report.updateTripInRoute", map);
                System.out.println("updateTripInRoute---" + updateTripInRoute);
            }
            System.out.println("pointCities size:" + pointCities);
            pointCities = (String) session.queryForObject("report.getPointCities", map);
            System.out.println("pointCities------------" + pointCities);
            routeInfo = pointCities.replace(",", "-");
            System.out.println("routeInfo-------------->>>" + routeInfo);

            map.put("routeInfo", routeInfo);

            int updateInTripMaster = (Integer) session.update("report.updateInTripMaster", map);
            System.out.println("updateInTripMaster---" + updateInTripMaster);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAgentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-OPR-01", CLASS, "customerWiseProfitability", sqlException);
        }
        return updateIntripConsignment;

    }

//    public int updateCnoteChanges(String tripId, String consignmentId, String consignmentIdNew, String consignmentOrder, String consignmentOrderNew, SqlMapClient session) {
//        Map map = new HashMap();
//        int updateCnoteChanges = 0;
//        int updateIntripConsignment = 0;
//        ArrayList conId = new ArrayList();
//        ArrayList tripPoints = new ArrayList();
//        ReportTO repTO = new ReportTO();
//        String pointCities = "";
//        String cityName = "";
//
//        String routeInfo = "";
//        try {
//            map.put("tripId", tripId);  // trip
//            map.put("consignmentId", consignmentId); // current cnote id
//            map.put("consignmentIdNew", consignmentIdNew);  // new cnote id
//            map.put("consignmentOrder", consignmentOrder);  // current cnote no
//            map.put("consignmentOrderNew", consignmentOrderNew);  // new cnote id
//            System.out.println("map---" + map);
//
//            updateIntripConsignment = (Integer) session.update("report.updateIntripConsignment", map);
//            System.out.println("updateIntripConsignment---" + updateIntripConsignment);
//            conId = (ArrayList) session.queryForList("report.getConId", map);
//            Iterator itr = conId.iterator();
//            while (itr.hasNext()) {
//                repTO = new ReportTO();
//                repTO = (ReportTO) itr.next();
//                map.put("containerId", repTO.getDetailId());
//                int updateInConsignmentContainer = (Integer) session.update("report.updateInConsignmentContainer", map);
//                System.out.println("updateInConsignmentContainer---" + updateInConsignmentContainer);
//            }
//            int updateInTripContainer = (Integer) session.update("report.updateInTripContainer", map);
//            System.out.println("updateInTripContainer---" + updateInTripContainer);
//            int updateInTripRouteCourse = (Integer) session.update("report.updateInTripRouteCourse", map);
//            System.out.println("updateInTripRouteCourse---" + updateInTripRouteCourse);
//
//            tripPoints = (ArrayList) session.queryForList("report.tripPoints", map);
//            Iterator itr2 = tripPoints.iterator();
//            while (itr2.hasNext()) {
//                repTO = new ReportTO();
//                repTO = (ReportTO) itr2.next();
//
//                map.put("containerId", repTO.getVendorName());
//                map.put("pointId", repTO.getPointId());
//                map.put("pointType", repTO.getPointType());
//                map.put("pointOrder", repTO.getPointSequence());
//                map.put("pointAddresss", repTO.getPointAddress());
//                map.put("pointPlanDate", repTO.getPointPlanDate());
//                map.put("pointPlanTime", repTO.getPointPlanTime());
//                map.put("userId", repTO.getCreatedOn());
//
//                System.out.println("map 2---" + map);
//                int updateTripInRoute = (Integer) session.update("report.updateTripInRoute", map);
//                System.out.println("updateTripInRoute---" + updateTripInRoute);
//            }
//            System.out.println("pointCities size:" + pointCities);
//            pointCities = (String) session.queryForObject("report.getPointCities", map);
//            System.out.println("pointCities------------" + pointCities);
//            routeInfo = pointCities.replace(",", "-");
//            System.out.println("routeInfo-------------->>>" + routeInfo);
//
//            map.put("routeInfo", routeInfo);
//
//            int updateInTripMaster = (Integer) session.update("report.updateInTripMaster", map);
//            System.out.println("updateInTripMaster---" + updateInTripMaster);
//
//        } catch (Exception sqlException) {
//            sqlException.printStackTrace();
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("getAgentList Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-OPR-01", CLASS, "customerWiseProfitability", sqlException);
//        }
//        return updateIntripConsignment;
//
//    }

}
