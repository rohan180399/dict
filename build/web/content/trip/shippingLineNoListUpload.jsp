
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">
    function submitPage(value) {
        if (value == 'Proceed') {
             $('#saveShipping').show();
            
            document.upload.action = '/throttle/saveShippingBillNo.do?param='+value;
            document.upload.submit();
        }else if(value == 'Download Sample'){
            document.upload.action = '/throttle/downloadSampleExcel.do?param='+value;
            document.upload.submit();
            
            
        }else {
             $('#uploadShipping').show();
            document.upload.action = '/throttle/shippingBillNoUpload.do?param='+value;
            document.upload.submit();
        }
    }
</script>

<html>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body>
     <c:if test="${shippingBillNoListSize <= 0}">
      <form name="upload" method="post" enctype="multipart/form-data">
      </c:if>
  <c:if test="${shippingBillNoListSize > 0}">
        <form name="upload" method="post">
      </c:if>
                    <%@ include file="/content/common/message.jsp" %>
                    <br>
                    <br>

                    <%
                    if(request.getAttribute("errorMessage")!=null){
                    String errorMessage=(String)request.getAttribute("errorMessage");                
                    %>
                <center><b><font color="red" size="1"><%=errorMessage%></font></b></center>
                        <%}%>
                <table class="table table-info mb30 table-hover" style="width:100%">
                    <tr>
                        <td style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;" colspan="4">ShippingBillNo Upload</td>
                    </tr>
                    <tr>
                        <td class="text2">Select file</td>
                        <td class="text2"><input type="file" name="importBpclTransaction" id="importBpclTransaction" class="importBpclTransaction"></td>   
                        
                        <td>  <input type="button" class="btn btn-info" value="Download Sample" name="downloadSample" onclick="submitPage(this.value)" ></td>
                    </tr>
                    <tr>
                        <td colspan="4" class="text1" align="center" ><input type="button" class="btn btn-info" value="Submit" name="Submit" onclick="submitPage(this.value)" >
                    </tr>
                </table>
                <br>
                <br>
                <br>
                <% int i = 1 ;%>
                <% int index = 0;%> 
                <%int oddEven = 0;%>
                <%  String classText = "";%>    
                <c:if test="${shippingBillNoListSize > 0}">
                    <table class="table table-info mb30 table-hover" style="width:100%">
                        <td  style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;" colspan="14">File Uploaded Details&nbsp;:</td>
                        <tr>
                            <td >S No</td>
                            <td >ContainerNo</td>
                            <td >ContainerSize</td>
                            <td >ContainerType</td>
                            <td >ShippingBillNo</td>
                            <td >RequestNo</td>
                            <td >ShippingBillDate</td>
                            <td >GateInDate</td>
                            <td >VehicleNo</td>
                            <td >NoOfContainer</td>
                            <td >Commodity</td>
                            <td >Commodity Id</td>
                            <td >Status</td>
                        </tr>
                        <c:forEach items="${shippingBillNoList}" var="bpcl">
                            <%
                          oddEven = index % 2;
                          if (oddEven > 0) {
                              classText = "text2";
                          } else {
                              classText = "text1";
                            }
                            %>
                            <tr>
                                <td class="<%=classText%>" ><%=i++%></font></td>
                                <td class="<%=classText%>" ><input type="hidden" name="containerNo" id="containerNo" value="<c:out value="${bpcl.containerNo}"/>" /><c:out value="${bpcl.containerNo}"/></font></td>
                                <td class="<%=classText%>" ><input type="hidden" name="containerSize" id="containerSize" value="<c:out value="${bpcl.containerCapacity}"/>" /><c:out value="${bpcl.containerCapacity}"/></font></td>
                                <td class="<%=classText%>" ><input type="hidden" name="containerType" id="containerType" value="<c:out value="${bpcl.containerTypeName}"/>" /><c:out value="${bpcl.containerTypeName}"/></font></td>
                                <td class="<%=classText%>" ><input type="hidden" name="shippingBillNo" id="shippingBillNo" value="<c:out value="${bpcl.shipingLineNo}"/>" /><c:out value="${bpcl.shipingLineNo}"/></font></td>
                                <td class="<%=classText%>" ><input type="hidden" name="requestNo" id="requestNo" value="<c:out value="${bpcl.requestNo}"/>" /><c:out value="${bpcl.requestNo}"/></font></td>
                                <td class="<%=classText%>" ><input type="hidden" name="shippingBillDate" id="shippingBillDate" value="<c:out value="${bpcl.shippingBillDate}"/>" /><c:out value="${bpcl.shippingBillDate}"/></font></td>
                                <td class="<%=classText%>" ><input type="hidden" name="gateInDate" id="gateInDate" value="<c:out value="${bpcl.gateInDate}"/>" /><c:out value="${bpcl.gateInDate}"/></font></td>
                                <td class="<%=classText%>" ><input type="hidden" name="vehicleNo" id="vehicleNo" value="<c:out value="${bpcl.vehicleNo}"/>" /><c:out value="${bpcl.vehicleNo}"/></font></td>
                                <td class="<%=classText%>" ><input type="hidden" name="containerQty" id="containerQty" value="<c:out value="${bpcl.containerQty}"/>" /><c:out value="${bpcl.containerQty}"/></font></td>
                                <td class="<%=classText%>" ><input type="hidden" name="commodityName" id="commodityName" value="<c:out value="${bpcl.commodityName}"/>" /><c:out value="${bpcl.commodityName}"/></font></td>
                                <td class="<%=classText%>" ><input type="hidden" name="commodityId" id="commodityId" value="<c:out value="${bpcl.commodityId}"/>" /><c:out value="${bpcl.commodityId}"/></font></td>
                                <td class="<%=classText%>" ><input type="hidden" name="flag" id="flag" value="<c:out value="${bpcl.flag}"/>" />
                                    <c:if test="${bpcl.flag == 0}">
                                    <font size="4" color="green"><c:out value="${bpcl.statusName}"/></font>
                                    </c:if>
                                    <c:if test="${bpcl.flag == 1}">
                                    <font size="4" color="red"><c:out value="${bpcl.statusName}"/></font>
                                    </c:if>
                                </td>

                            </tr>
                            <%index++;%>
                        </c:forEach>
                    </table>
                    <br>
                    <br>
                    <br>
                    <center>
                        <input type="button"  class="btn btn-info" value="Proceed" onclick="submitPage(this.value)"/>
                    </center>

                </c:if>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>