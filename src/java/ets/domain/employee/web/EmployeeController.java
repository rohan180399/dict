/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.employee.web;

import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.arch.web.BaseController;

import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.ParamPart;
import com.oreilly.servlet.multipart.Part;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import ets.domain.util.FPLogUtils;
import ets.domain.util.ParveenErrorConstants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;

import ets.domain.employee.business.EmployeeBP;
import ets.domain.employee.business.EmployeeTO;
import ets.domain.employee.web.EmployeeCommand;
import ets.domain.designation.business.DesignationBP;
import ets.domain.department.business.DepartmentBP;
import ets.domain.company.business.CompanyBP;
import ets.domain.users.business.LoginBP;
import ets.domain.users.business.LoginTO;
import ets.domain.users.web.CryptoLibrary;
import java.io.File;
import java.io.FileInputStream;

import java.util.*;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Enumeration;

/**
 *
 * @author karudaiyar Subramaniam
 */
public class EmployeeController extends BaseController {

    EmployeeCommand employeecommand;
    EmployeeBP employeeBP;
    DesignationBP designationBP;
    DepartmentBP departmentBP;
    LoginBP loginBP;
    CompanyBP companyBP;

    public EmployeeBP getEmployeeBP() {
        return employeeBP;
    }

    public void setEmployeeBP(EmployeeBP employeeBP) {
        this.employeeBP = employeeBP;
    }

    public DesignationBP getDesignationBP() {
        return designationBP;
    }

    public void setDesignationBP(DesignationBP desigBP) {
        this.designationBP = desigBP;
    }

    public DepartmentBP getDepartmentBP() {
        return departmentBP;
    }

    public void setDepartmentBP(DepartmentBP deptBP) {
        this.departmentBP = deptBP;
    }

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setloginBP(LoginBP logBP) {
        this.loginBP = logBP;
    }

    public CompanyBP getCompanyBP() {
        return companyBP;
    }

    public void setCompanyBP(CompanyBP logBP) {
        this.companyBP = logBP;
    }

    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        //////System.out.println("request.getRequestURI() = " + request.getRequestURI());
        binder.closeNoCatch();  initialize(request);

    }

    //EmployeeController
    public ModelAndView handleAddEmployeePage(HttpServletRequest request, HttpServletResponse response, EmployeeCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/Login.jsp");
        }

        //////System.out.println("i am in addEmployee page");
        // designationCommand = command;
        HttpSession session = request.getSession();
        String path = "";

        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        if (!loginBP.checkAuthorisation(userFunctions, "Employee-Add")) {
            path = "content/common/NotAuthorized.jsp";
        } else {
            String menuPath = "HRMS  >>  Manage Employee  >> Add ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "Add Designation";
            request.setAttribute("pageTitle", pageTitle);
            ArrayList deptList = new ArrayList();
            ArrayList DesignaList = new ArrayList();
            ArrayList GradeList = new ArrayList();
            ArrayList companyList = new ArrayList();
            ArrayList roleList = new ArrayList();
            ArrayList relationList = new ArrayList();
            ArrayList companyLists = new ArrayList();

            companyLists = loginBP.getCompanyList();
            request.setAttribute("companyLists", companyLists);
            deptList = departmentBP.processActiveDeptList();
            DesignaList = designationBP.processActiveDesignaList();
            GradeList = designationBP.processActiveGradeList();
            companyList = companyBP.processActiveCompanyList();
            roleList = loginBP.getRoles();
            relationList = loginBP.getRelations();

            ArrayList vendorList = new ArrayList();
            vendorList = employeeBP.getDriverVendorList();
            request.setAttribute("vendorList", vendorList);

            request.setAttribute("roleList", roleList);
            request.setAttribute("DeptList", deptList);
            request.setAttribute("DesignaList", DesignaList);
            request.setAttribute("GradeList", GradeList);
            request.setAttribute("companyList", companyList);
            request.setAttribute("relationList", relationList);
            path = "content/employee/addEmployee.jsp";
        }
        return new ModelAndView(path);
    }

   public ModelAndView handleInsertEmployee(HttpServletRequest request, HttpServletResponse response, EmployeeCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        String pageTitle = "View Staff";
        request.setAttribute("pageTitle", pageTitle);
        employeecommand = command;
        HttpSession session = request.getSession();
        String menuPath = "";
        menuPath = "HRMS  >>  Manage Staff >> Add  ";
        String userStatus = request.getParameter("userStatus");
        String userStatus1 = request.getParameter("userStatus1");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String fileSavedAs = "", fileName = "", actualFilePath = "", tempFilePath = "";
        String employeePhoto = "", employeeLicense = "", passportPhotoFile="",visaPhoto = "" ;
        String name = null, dateOfBirth = null, dateOfJoining = null, gender = null, bloodGrp = null, martialStatus = null, fatherName = null, mobile = "";
        String phone = null, qualification = null, email = null, deptid = null, designationId = null, gradeId = null, cmpId = null, referenceName = null, contractDriver = null, vendorCompany = null, drivingLicenseNo = null, licenseExpDate = null, licenseType = null, licenseState = "";
        String addr = null, city = null, state = null, pincode = null, chec = null, addr1 = null, city1 = null, state1 = null, pincode1 = null, salaryType = null, basicSalary = null, esiEligible = null, pfEligible = null, bankAccountNo = null, bankName = null, bankBranchName = null, accountHolderName = "";
        String nomineeName = null, nomineeRelation = null, nomineeDob = null, nomineeAge = null, yearOfExperience = null, preEmpDesignation = null, prevCompanyName = null, prevCompanyAddress = null, prevCompanyCity = null, preEmpState = null, preEmpPincode = null, prevCompanyContact = "";
        String userName = null, password = null, RoleIds = null, branchId = null, companyId = null;
        String passportName = null,passportNo= null, passportType = null, nationality = null, palceOfIssue = null, palceOfBirth = null, dateOfIssue= null,dateOfExpiry=null,uploadRemarks=null;
        String visaProvider = null,visaType= null, visaDateOfValidity = null, countryEnteringDate = null, visaValidity = null, visaDateOfIssue = null,visaRemarks=null;
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        int i = 0;
        int j = 0;

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

            EmployeeTO employeeTO = new EmployeeTO();
            EmployeeTO empTO = new EmployeeTO();
            String filename[] = new String[10];
            //             String fileSavedAs = "";
            String fileSavedAs1[] = new String[10];
            String fileSavedAs2[] = new String[10];
            String certificateIds[] = new String[10];
            //                int i = 0;
            //             Part _part = null;
            //                int j = 0;
            int k = 0;
            int l = 0;
            int m = 0;
            int n = 0;
            int p = 0;
            //            String[] relationName = null;
            //            String[] relation = null;
            //            String[] relationDOB = null;
            //            String[] relationAge = null;
            String[] relationName = new String[100];
            String[] relation = new String[100];
            String[] relationDOB = new String[100];
            String[] relationAge = new String[100];

            isMultipart = ServletFileUpload.isMultipartContent(request);
            if (isMultipart) {
                //System.out.println("this is tht");
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    //                    //System.out.println("part Name:" + _part.getName());
                    String filePath = "";
                    String userId = "";
                    //                    filePath = "/employee";
                    filePath = getServletContext().getRealPath("/uploadFiles/Files");
                    //                    filePath = "C:/EmployeePhotoCopies";
                    System.out.println("REAL filePath = " + filePath);
                    String filePath1 = filePath.replace("\\", "\\\\");
                    Date now = new Date();
                    String date = now.getDate() + "" + (now.getMonth() + 1) + "" + (now.getYear() + 1900);
                    String time = now.getHours() + "" + now.getMinutes() + "" + now.getSeconds() + userId;

                    //System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        fPart = (FilePart) partObj;
                        filename[j] = fPart.getFileName();
                        System.out.println("filename"+filename[0]);
                        System.out.println("filenamejjj"+filename[1]);
                        System.out.println("filenamekk"+filename[2]);
                        if (!"".equals(filename[j]) && filename[j] != null) {
                            String[] fp = filename[j].split("\\.");
                            System.out.println("fp"+fp[0]);
                            System.out.println("fp"+fp[1]);
                            fileSavedAs = fp[0] + "." + fp[1];
                            fileSavedAs1[j] = fp[0] + "." + fp[1];
                            if ("employeePhoto".equals(partObj.getName())) {
                                employeePhoto = filePath1 + "\\" + fileSavedAs1[j];
                            } else if ("employeeLicense".equals(partObj.getName())) {
                                employeeLicense = filePath1 + "\\" + fileSavedAs1[j];
                            }
                            else if ("passportPhotoFile".equals(partObj.getName())) {
                                passportPhotoFile = filePath1 + "\\" + fileSavedAs1[j];
                            }
                            else if ("visaPhoto".equals(partObj.getName())) {
                                visaPhoto = filePath1 + "\\" + fileSavedAs1[j];
                            }

//                            String tempPath = filePath1 + "/" + fileSavedAs1[j];
//                            String actPath = filePath + "/" + filename[j];
                            String tempPath = filePath1 + "\\" + fileSavedAs1[j];
                            String actPath = filePath + "\\" + filename[j];
                            //System.out.println("tempPath..." + tempPath);
                            //System.out.println("actPath..." + actPath);
                            long fileSize = fPart.writeTo(new java.io.File(actPath));
                            //System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actPath);
                            f1.renameTo(new File(tempPath));

                        }

                        j++;

                    } else if (partObj.isParam()) {
                        ParamPart paramPart = (ParamPart) partObj;
                        if (partObj.getName().equals("relationName")) {
                            relationName[k] = paramPart.getStringValue();
                            k++;
                        }
                        if (partObj.getName().equals("relation")) {
                            relation[l] = paramPart.getStringValue();
                            l++;
                        }
                        if (partObj.getName().equals("relationDOB")) {
                            relationDOB[m] = paramPart.getStringValue();
                            m++;
                        }
                        if (partObj.getName().equals("relationAge")) {
                            relationAge[n] = paramPart.getStringValue();
                            n++;
                        }
                        if (partObj.getName().equals("name")) {
                            name = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("qualification")) {
                            qualification = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("dateOfBirth")) {
                            dateOfBirth = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("dateOfJoining")) {
                            dateOfJoining = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("martialStatus")) {
                            martialStatus = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("gender")) {
                            gender = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("bloodGrp")) {
                            bloodGrp = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("fatherName")) {
                            fatherName = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("mobile")) {
                            mobile = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("phone")) {
                            phone = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("email")) {
                            email = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("addr")) {
                            addr = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("city")) {
                            city = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("state")) {
                            state = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("pincode")) {
                            pincode = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("deptid")) {
                            deptid = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("designationId")) {
                            designationId = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("gradeId")) {
                            gradeId = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("referenceName")) {
                            referenceName = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("drivingLicenseNo")) {
                            drivingLicenseNo = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("licenseExpDate")) {
                            licenseExpDate = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("licenseType")) {
                            licenseType = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("chec")) {
                            chec = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("bankAccountNo")) {
                            bankAccountNo = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("bankName")) {
                            bankName = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("bankBranchName")) {
                            bankBranchName = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("accountHolderName")) {
                            accountHolderName = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("yearOfExperience")) {
                            yearOfExperience = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("preEmpDesignation")) {
                            preEmpDesignation = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("prevCompanyName")) {
                            prevCompanyName = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("prevCompanyAddress")) {
                            prevCompanyAddress = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("prevCompanyCity")) {
                            prevCompanyCity = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("preEmpState")) {
                            preEmpState = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("preEmpPincode")) {
                            preEmpPincode = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("prevCompanyContact")) {
                            prevCompanyContact = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("yearOfExperience")) {
                            yearOfExperience = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("salaryType")) {
                            salaryType = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("basicSalary")) {
                            basicSalary = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("esiEligible")) {
                            esiEligible = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("pfEligible")) {
                            pfEligible = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("nomineeName")) {
                            nomineeName = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("nomineeRelation")) {
                            nomineeRelation = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("nomineeDob")) {
                            nomineeDob = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("nomineeAge")) {
                            nomineeAge = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("licenseState")) {
                            licenseState = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("cmpId")) {
                            cmpId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("contractDriver")) {
                            contractDriver = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("pincode1")) {
                            pincode1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("addr1")) {
                            addr1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("city1")) {
                            city1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("state1")) {
                            state1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("vendorCompany")) {
                            vendorCompany = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("userName")) {
                            userName = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("password")) {
                            password = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("roleId")) {
                            RoleIds = paramPart.getStringValue();
                        }
//                        if (partObj.getName().equals("branchId")) {
//                            branchId = paramPart.getStringValue();
//                        }
                        if (partObj.getName().equals("companyId")) {
                            companyId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("passportNo")) {
                            passportNo = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("passportName")) {
                            passportName = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("passportType")) {
                            passportType = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("nationality")) {
                            nationality = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("palceOfIssue")) {
                            palceOfIssue = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("dateOfIssue")) {
                            dateOfIssue = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("dateOfExpiry")) {
                            dateOfExpiry = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("palceOfBirth")) {
                            palceOfBirth = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("uploadRemarks")) {
                            uploadRemarks = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("visaProvider")) {
                            visaProvider = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("visaType")) {
                            visaType = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("visaDateOfIssue")) {
                            visaDateOfIssue = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("visaDateOfValidity")) {
                            visaDateOfValidity = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("countryEnteringDate")) {
                            countryEnteringDate = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("visaValidity")) {
                            visaValidity = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("visaRemarks")) {
                            visaRemarks = paramPart.getStringValue();
                        }
//                        if (partObj.getName().equals("staffUserId")) {
//                            staffUserId = paramPart.getStringValue();
//                        }
                    }
                }
            }

            String file = "";
            String remarks = "";
            String lrNumber = "";
            String saveFile = "";

            int status = 0;
            //System.out.println("name" + name);
//            System.out.println("branchId testing = " + branchId);
//            employeeTO.setBranchId(branchId);
            employeeTO.setCompanyId(companyId);

            employeeTO.setRoleIds(RoleIds);
            employeeTO.setUserName(userName);
            employeeTO.setPassword(password);
            employeeTO.setBankName(bankName);
            employeeTO.setBankBranchName(bankBranchName);
            employeeTO.setAccountHolderName(accountHolderName);
            employeeTO.setPincode1(pincode1);
            employeeTO.setAddr1(addr1);
            employeeTO.setCity1(city1);
            employeeTO.setState1(state1);
            employeeTO.setVendorCompany(vendorCompany);
            //                employeeTO.setSalaryType(salaryType);
            employeeTO.setLicenseState(licenseState);
            employeeTO.setContractDriver(contractDriver);

            employeeTO.setPhotoFile(employeePhoto);
            System.out.println("employeePhoto value is " + employeePhoto);
            employeeTO.setLicenseFile(employeeLicense);
            System.out.println("employeeLicense value is " + employeeLicense);
            employeeTO.setSalaryType(salaryType);
            employeeTO.setBasicSalary(basicSalary);
            employeeTO.setEsiEligible(esiEligible);
            employeeTO.setPfEligible(pfEligible);
            employeeTO.setNomineeName(nomineeName);
            employeeTO.setNomineeRelation(nomineeRelation);
            employeeTO.setNomineeDob(nomineeDob);
            employeeTO.setNomineeAge(nomineeAge);

            employeeTO.setName(name);

            employeeTO.setQualification(qualification);

            employeeTO.setDateOfBirth(dateOfBirth);

            employeeTO.setDOJ(dateOfJoining);

            employeeTO.setMartialStatus(martialStatus);

            employeeTO.setGender(gender);

            employeeTO.setBloodGrp(bloodGrp);

            employeeTO.setFatherName(fatherName);

            employeeTO.setMobile(mobile);

            employeeTO.setPhone(phone);

            employeeTO.setEmail(email);

            employeeTO.setAddr(addr);

            employeeTO.setCity(city);

            employeeTO.setState(state);
            employeeTO.setPincode(pincode);

            employeeTO.setDeptId(Integer.parseInt(deptid));

            employeeTO.setDesigId(Integer.parseInt(designationId));

            employeeTO.setGradeId(Integer.parseInt(gradeId));

            employeeTO.setReferenceName(referenceName);

            employeeTO.setDrivingLicenseNo(drivingLicenseNo);

            employeeTO.setLicenseExpDate(licenseExpDate);

            employeeTO.setLicenseType(licenseType);

//             employeeTO.setAddressVerified(addressVerified);
//
//             employeeTO.setAddressVerified1(addressVerified1);
//
//             employeeTO.setLicenseIssuesDate(licenseIssuesDate);
//
//             employeeTO.setLicenseVerified(licenseVerified);
//
//             employeeTO.setGuarantorName(guarantorName);
            employeeTO.setYearOfExperience(yearOfExperience);
            employeeTO.setPreEmpDesignation(preEmpDesignation);
            employeeTO.setPrevCompanyName(prevCompanyName);
            employeeTO.setPrevCompanyAddress(prevCompanyAddress);
            employeeTO.setPrevCompanyCity(prevCompanyCity);
            employeeTO.setPreEmpState(preEmpState);
            employeeTO.setPreEmpPincode(preEmpPincode);
            employeeTO.setPrevCompanyContact(prevCompanyContact);

//             employeeTO.setGuarantorRelation(guarantorRelation);
//             employeeTO.setGuarantorMobile(guarantorMobile);
//             employeeTO.setGuarantorPhone(guarantorPhone);
//             employeeTO.setGuarantorAddr(guarantorAddr);
//             employeeTO.setGuarantorCity(guarantorCity);
//             employeeTO.setGuarantorState(guarantorState);
//             employeeTO.setGuarantorPincode(guarantorPincode);
//             employeeTO.setNameOfRtoDepartment(nameOfRtoDepartment);
            employeeTO.setRelationName(relationName);
            employeeTO.setRelation(relation);
            employeeTO.setRelationDOB(relationDOB);
            employeeTO.setRelationAge(relationAge);
            employeeTO.setChec(chec);
            employeeTO.setBankAccountNo(bankAccountNo);


            employeeTO.setCmpId(cmpId);
            employeeTO.setPassportPhotoFile(passportPhotoFile);
            employeeTO.setPassportNo(passportNo);
            employeeTO.setPassportName(passportName);
            employeeTO.setPassportType(passportType);
            employeeTO.setNationality(nationality);
            employeeTO.setPalceOfIssue(palceOfIssue);
            employeeTO.setPalceOfBirth(palceOfBirth);
            employeeTO.setDateOfIssue(dateOfIssue);
            employeeTO.setDateOfExpiry(dateOfExpiry);
            employeeTO.setRemarks(uploadRemarks);

            employeeTO.setVisaProvider(visaProvider);
            employeeTO.setVisaType(visaType);
            employeeTO.setVisaDateOfIssue(visaDateOfIssue);
            employeeTO.setVisaDateOfValidity(visaDateOfValidity);
            employeeTO.setCountryEnteringDate(countryEnteringDate);
            employeeTO.setVisaValidity(visaValidity);
            employeeTO.setVisaRemarks(visaRemarks);
            employeeTO.setVisaPhoto(visaPhoto);
            System.out.println("passportPhoto value is " + passportPhotoFile);
            System.out.println("visaPhoto value is " + visaPhoto);
            int UserId = (Integer) session.getAttribute("userId");
            //System.out.println("UserId in con" + UserId);
            //System.out.println("UserId in relationName" + relationName.length);
            //System.out.println("UserId in relationName" + relationName[0]);
            //System.out.println("UserId in relationName" + relationName[1]);
            //System.out.println("UserId in relation" + relation.length);
            //System.out.println("UserId in relation" + relation[0]);
            //System.out.println("UserId in relation" + relation[1]);
            //System.out.println("UserId in relationDOB" + relationDOB.length);
            //System.out.println("UserId in relationDOB" + relationDOB[0]);
            //System.out.println("UserId in relationDOB" + relationDOB[1]);
            //System.out.println("UserId in relationAge" + relationAge.length);
            //System.out.println("UserId in relationAge" + relationAge[0]);
            //System.out.println("UserId in relationAge" + relationAge[1]);
            //System.out.println("userStatus1" + userStatus1);
            int staffUserId = 0;
            if (userStatus1 != "") {
                System.out.println("if");
                staffUserId = getEmployeeBP().addNewUser(employeeTO, UserId);
                System.out.println("staffUserId"+staffUserId);
            } else {
                System.out.println("else");
                staffUserId = 0;
            }
            int insertStatus = 0;
            insertStatus = getEmployeeBP().insertEmployeeDetails(employeeTO, UserId, staffUserId);
            if (insertStatus != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Employee Added Successfully");
            }

            ArrayList employeeDetails = new ArrayList();
            //employeeDetails = employeeBP.getEmployeeDetails();
            employeeDetails = employeeBP.getEmployeeDetails(empTO);
            request.setAttribute("employeeDetails", employeeDetails);

            path = "content/employee/viewEmployee.jsp";

            //          }
            //    }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to add Lecture data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleSearchAlterEmp(HttpServletRequest request, HttpServletResponse response, EmployeeCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "HRMS  >>  Manage Employee  >>  Alter";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Alter Staff";
        request.setAttribute("pageTitle", pageTitle);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Employee-Alter")) {
                menuPath = "Not Authorised";
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                path = "content/common/NotAuthorized.jsp";
            } else {
                path = "content/employee/searchAlterEmp.jsp";
                request.setAttribute("staffCode", "");
                request.setAttribute("staffName", "");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed alter lecture data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }

    public ModelAndView handleAlterEmpPage(HttpServletRequest request, HttpServletResponse response, EmployeeCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        employeecommand = command;
        String menuPath = "HRMS  >>  Manage Employee  >>  Alter";
        String pageTitle = "Alter Employee";
        request.setAttribute("pageTitle", pageTitle);
        HttpSession session = request.getSession();
        CryptoLibrary cl = new CryptoLibrary();
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Staff-Modify")) {
//                menuPath = "Not Authorised";
//                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//                path = "content/common/NotAuthorized.jsp";
//            } else {

            ArrayList staffList = new ArrayList();
            String staffId = "";
            String staffCode = "";
            String staffName = "";
            if (employeecommand.getStaffCode() != null && employeecommand.getStaffCode() != "") {
                staffCode = employeecommand.getStaffCode();
                request.setAttribute("staffCode", staffCode);
            }
            if (employeecommand.getStaffName() != null && employeecommand.getStaffName() != "") {
                staffName = employeecommand.getStaffName();
                request.setAttribute("staffName", staffName);
            }
            staffId = employeeBP.getStaffId(staffCode, staffName);
            request.setAttribute("staffId", staffId);
//            path = "content/employee/searchAlterEmp.jsp";
            path = "content/employee/addEmployee.jsp";
            staffList = employeeBP.processEmpDetails(staffId, staffName);
            Iterator itr = staffList.iterator();
            EmployeeTO empTO = null;
            int desigId = 0, deptId = 0, gradeId = 0;
            String empCode = "", name = "", qualification = "", dateOfBirth = "", DOJ = "", gender = "", fatherName = "",
                    bloodGrp = "", martialStatus = "", mobile = "", phone = "", email = "", addr = "", city = "", state = "",
                    pincode = "", chec = "", referenceName = "", addr1 = "", city1 = "", state1 = "", pincode1 = "", cmpId = "",
                    drivingLicenseNo = "", driverType = "", vendorCompany = "", licenseDate = "", licenseType = "", licenseState = "",
                    salaryType = "", basicSalary = "", esiEligible = "", pfEligible = "", bankAccountNo = "", bankName = "",
                    bankBranchName = "", accountHolderName = "", yearOfExperience = "", prevEmpDesignation = "",
                    prevCompanyName = "", prevCompanyAddress = "", prevCompanyCity = "", prevEmpState = "", prevEmployeePincode = "",
                    prevCompanyContact = "", nomineeName = "", nomineeRelation = "", nomineeDob = "", nomineeAge = "",status="",password1="";
            while (itr.hasNext()) {
                empTO = (EmployeeTO) itr.next();
//                staffId = empTO.getStaffId();
                desigId = empTO.getDesigId();
                empCode = empTO.getEmpCode();
                name = empTO.getName();
                qualification = empTO.getQualification();
                dateOfBirth = empTO.getDateOfBirth();
                DOJ = empTO.getDOJ();
                gender = empTO.getGender();
                fatherName = empTO.getFatherName();
                bloodGrp = empTO.getBloodGrp();
                martialStatus = empTO.getMartialStatus();
                mobile = empTO.getMobile();
                phone = empTO.getPhone();
                email = empTO.getEmail();
                addr = empTO.getAddr();
                city = empTO.getCity();
                state = empTO.getState();
                pincode = empTO.getPincode();
                chec = empTO.getChec();
                addr1 = empTO.getAddr1();
                city1 = empTO.getCity1();
                state1 = empTO.getState1();
                pincode1 = empTO.getPincode1();
                deptId = empTO.getDeptId();
                gradeId = empTO.getGradeId();
                cmpId = empTO.getCmpId();
                drivingLicenseNo = empTO.getDrivingLicenseNo();
                referenceName = empTO.getReferenceName();
                driverType = empTO.getDriverType();
                vendorCompany = empTO.getVendorCompany();
                licenseDate = empTO.getLicenseDate();
                licenseType = empTO.getLicenseType();
                licenseState = empTO.getLicenseState();
                salaryType = empTO.getSalaryType();
                basicSalary = empTO.getBasicSalary();
                esiEligible = empTO.getEsiEligible();
                pfEligible = empTO.getPfEligible();
                bankAccountNo = empTO.getBankAccountNo();
                bankName = empTO.getBankName();
                bankBranchName = empTO.getBankBranchName();
                accountHolderName = empTO.getAccountHolderName();
                yearOfExperience = empTO.getYearOfExperience();
                prevEmpDesignation = empTO.getPrevEmpDesignation();
                prevCompanyName = empTO.getPrevCompanyName();
                prevCompanyAddress = empTO.getPrevCompanyAddress();
                prevCompanyCity = empTO.getPrevCompanyCity();
                prevEmpState = empTO.getPrevEmpState();
                prevEmployeePincode = empTO.getPrevEmployeePincode();
                prevCompanyContact = empTO.getPrevCompanyContact();
                nomineeName = empTO.getNomineeName();
                nomineeRelation = empTO.getNomineeRelation();
                nomineeDob = empTO.getNomineeDob();
                nomineeAge = empTO.getNomineeAge();
                status = empTO.getStatus();
            }
//            request.setAttribute("staffId", staffId);
            request.setAttribute("name", name);
            request.setAttribute("empCode", empCode);
            request.setAttribute("dateOfBirth", dateOfBirth);
            request.setAttribute("dateOfJoining", DOJ);
            request.setAttribute("gender", gender);
            request.setAttribute("bloodGrp", bloodGrp);
            request.setAttribute("martialStatus", martialStatus);
            request.setAttribute("fatherName", fatherName);
            request.setAttribute("mobile", mobile);
            request.setAttribute("phone", phone);
            request.setAttribute("qualification", qualification);
            request.setAttribute("email", email);
            request.setAttribute("addr", addr);
            request.setAttribute("city", city);
            request.setAttribute("state", state);
            request.setAttribute("pincode", pincode);
            request.setAttribute("chec", chec);
            request.setAttribute("addr1", addr1);
            request.setAttribute("city1", city1);
            request.setAttribute("state1", state1);
            request.setAttribute("pincode1", pincode1);
            request.setAttribute("deptId", deptId);
            request.setAttribute("gradeId", gradeId);
            request.setAttribute("desigId", desigId);
            request.setAttribute("cmpId", cmpId);
            request.setAttribute("drivingLicenseNo", drivingLicenseNo);
            request.setAttribute("referenceName", referenceName);
            request.setAttribute("driverType", driverType);
            request.setAttribute("vendorCompany", vendorCompany);
            request.setAttribute("licenseDate", licenseDate);
            request.setAttribute("licenseType", licenseType);
            request.setAttribute("licenseState", licenseState);
            request.setAttribute("salaryType", salaryType);
            request.setAttribute("basicSalary", basicSalary);
            request.setAttribute("pfEligible", pfEligible);
            request.setAttribute("esiEligible", esiEligible);
            request.setAttribute("bankAccountNo", bankAccountNo);
            request.setAttribute("bankName", bankName);
            request.setAttribute("bankBranchName", bankBranchName);
            request.setAttribute("accountHolderName", accountHolderName);
            request.setAttribute("yearOfExperience", yearOfExperience);
            request.setAttribute("prevEmpDesignation", prevEmpDesignation);
            request.setAttribute("prevCompanyName", prevCompanyName);
            request.setAttribute("prevCompanyAddress", prevCompanyAddress);
            request.setAttribute("prevCompanyCity", prevCompanyCity);
            request.setAttribute("prevEmpState", prevEmpState);
            request.setAttribute("prevEmployeePincode", prevEmployeePincode);
            request.setAttribute("prevCompanyContact", prevCompanyContact);
            request.setAttribute("nomineeName", nomineeName);
            request.setAttribute("nomineeRelation", nomineeRelation);
            request.setAttribute("nomineeDob", nomineeDob);
            request.setAttribute("nomineeAge", nomineeAge);
            request.setAttribute("status", status);
//            System.out.println("desigId  " + desigId);
            request.setAttribute("staffId", staffId);
            //System.out.println("in addlect cont");

            ArrayList DeptList = new ArrayList();
            ArrayList DesignaList = new ArrayList();
            ArrayList GradeList = new ArrayList();
            ArrayList companyList = new ArrayList();
            ArrayList roleList = new ArrayList();
            ArrayList userDetails = new ArrayList();
            ArrayList relationList = new ArrayList();
            ArrayList familyList = new ArrayList();
            ArrayList possportDeatils = new ArrayList();
            ArrayList visaDetails = new ArrayList();
            DeptList = departmentBP.processActiveDeptList();
            DesignaList = designationBP.processActiveDesignaList();
            GradeList = designationBP.processActiveGradeList();
            companyList = companyBP.processActiveCompanyList();
            roleList = loginBP.getRoles();
            relationList = loginBP.getRelations();
            request.setAttribute("roleList", roleList);
            request.setAttribute("DeptList", DeptList);
            request.setAttribute("DesignaList", DesignaList);
            request.setAttribute("GradeList", GradeList);
            request.setAttribute("companyList", companyList);
            request.setAttribute("relationList", relationList);

//            ArrayList branchList = new ArrayList();
//            branchList = employeeBP.getBranchList();
//            request.setAttribute("branchList", branchList);

            familyList = employeeBP.processEmpFamilyDetails(staffId, staffName);
            request.setAttribute("StaffList", staffList);
            userDetails = loginBP.getUser(staffId);
            System.out.println("userDetails"+userDetails.size());
            possportDeatils = employeeBP.getPassportDetails(staffId);
            visaDetails = employeeBP.getVisaDetails(staffId);
            System.out.println("visaDeatils"+visaDetails.size());
            System.out.println("possportDeatils size()"+possportDeatils.size());
            request.setAttribute("visaDetails", visaDetails);
            request.setAttribute("possportDeatils", possportDeatils);
            request.setAttribute("userDetails", userDetails);
            request.setAttribute("familyList", familyList);
            request.setAttribute("familyListSize", familyList.size());
            Iterator itr1 = userDetails.iterator();
            LoginTO loginTO = null;
            EmployeeTO empTO1 = null;
            int userId=0,roleId=0;
            String branchId="",userName="",password="",activeInd="",appUserCode="",appUserName="";
            while (itr1.hasNext()) {
                loginTO = (LoginTO) itr1.next();
//                branchId=loginTO.getBranchId();
                userId=loginTO.getUserId();
                roleId=loginTO.getRoleId();
                userName=loginTO.getUserName();
                password=loginTO.getPassword();
                activeInd=loginTO.getActiveInd();
                appUserCode=loginTO.getAppUserCode();
                appUserName=loginTO.getAppUserName();
                if (!"".equals(password)) {
                password1 = cl.decrypt(password);
                System.out.println("pass " + password1);
            }
            }
            System.out.println(" userName value is "+userName);
//            request.setAttribute("branchId", branchId);
            request.setAttribute("userId", userId);
            request.setAttribute("staffUserId", userId);
            request.setAttribute("roleId", roleId);
            request.setAttribute("userName", userName);
            request.setAttribute("password", password1);
            request.setAttribute("activeInd", activeInd);
            request.setAttribute("appUserCode", appUserCode);
            request.setAttribute("appUserName", appUserName);
            Iterator itr2 = possportDeatils.iterator();
            EmployeeTO empTO2 = null;
            String uploadId=null, passportName = null,passportNo= null, passportType = null, nationality = null, palceOfIssue = null,
            palceOfBirth = null, dateOfIssue= null,dateOfExpiry=null,uploadRemarks=null;
            while (itr2.hasNext()) {
                empTO2 = (EmployeeTO) itr2.next();
//                branchId=loginTO.getBranchId();
                uploadId=empTO2.getUploadId();
                passportNo=empTO2.getPassportNo();
                passportName=empTO2.getPassportName();
                passportType=empTO2.getPassportType();
                nationality=empTO2.getNationality();
                palceOfIssue=empTO2.getPalceOfIssue();
                palceOfBirth=empTO2.getPalceOfBirth();
                dateOfIssue=empTO2.getDateOfIssue();
                dateOfExpiry=empTO2.getDateOfExpiry();
                uploadRemarks=empTO2.getRemarks();

            }

//            request.setAttribute("branchId", branchId);
            request.setAttribute("uploadId", uploadId);
            request.setAttribute("passportNo", passportNo);
            request.setAttribute("passportName", passportName);
            request.setAttribute("passportType", passportType);
            request.setAttribute("nationality", nationality);
            request.setAttribute("palceOfIssue", palceOfIssue);
            request.setAttribute("palceOfBirth", palceOfBirth);
            request.setAttribute("dateOfIssue", dateOfIssue);
            request.setAttribute("dateOfExpiry", dateOfExpiry);
            request.setAttribute("uploadRemarks", uploadRemarks);


            Iterator itr3 = visaDetails.iterator();
            EmployeeTO empTO3 = null;
            String visaId= null,visaProvider = null,visaType= null, visaDateOfValidity = null, countryEnteringDate = null,
            visaValidity = null, visaDateOfIssue = null,visaRemarks=null;
            while (itr3.hasNext()) {
                empTO3 = (EmployeeTO) itr3.next();
                visaId=empTO3.getVisaId();
                visaProvider=empTO3.getVisaProvider();
                visaType=empTO3.getVisaType();
                visaDateOfValidity=empTO3.getVisaDateOfValidity();
                countryEnteringDate=empTO3.getCountryEnteringDate();
                visaValidity=empTO3.getVisaValidity();
                visaDateOfIssue=empTO3.getVisaDateOfIssue();
                visaRemarks=empTO3.getVisaRemarks();

            }
            request.setAttribute("visaId", visaId);
            request.setAttribute("visaProvider", visaProvider);
            request.setAttribute("visaType", visaType);
            request.setAttribute("visaDateOfValidity", visaDateOfValidity);
            request.setAttribute("countryEnteringDate", countryEnteringDate);
            request.setAttribute("visaValidity", visaValidity);
            request.setAttribute("visaDateOfIssue", visaDateOfIssue);
            request.setAttribute("visaRemarks", visaRemarks);
//                ArrayList DesignaList = new ArrayList();
//                DesignaList = designationBP.getDesignaList();
//                request.setAttribute("DesignaList", DesignaList);
//
//                ArrayList GradeList = new ArrayList();
//                GradeList = designationBP.getGradeList(desigId);
//                request.setAttribute("GradeList", GradeList);
            ArrayList companyLists = new ArrayList();
            companyLists = loginBP.getCompanyList();
            request.setAttribute("companyLists", companyLists);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }



  public ModelAndView handleModifyEmp(HttpServletRequest request, HttpServletResponse response, EmployeeCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        String pageTitle = "View Staff";
        request.setAttribute("pageTitle", pageTitle);
        employeecommand = command;
        HttpSession session = request.getSession();
        String menuPath = "";
        menuPath = "HRMS  >>  Manage Staff >> Add  ";
        String userStatus = request.getParameter("userStatus");
        String userStatus1 = request.getParameter("userStatus1");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String fileSavedAs = "", fileName = "", actualFilePath = "", tempFilePath = "" ;
        String employeePhoto = "", employeeLicense = "", passportPhotoFile="" ,visaPhoto="";
        String name = null, dateOfBirth = null, dateOfJoining = null, gender = null, bloodGrp = null, martialStatus = null, fatherName = null, mobile = "";
        String phone = null, qualification = null, email = null, deptid = null, designationId = null, gradeId = null, cmpId = null, referenceName = null, contractDriver = null, vendorCompany = null,
        drivingLicenseNo = null, licenseExpDate = null, licenseType = null, licenseState = "",empRelationSize="";
        String addr = null, city = null, state = null, pincode = null, chec = null, addr1 = null, city1 = null, state1 = null, pincode1 = null, salaryType = null, basicSalary = null, esiEligible = null, pfEligible = null, bankAccountNo = null, bankName = null, bankBranchName = null, accountHolderName = "";
        String nomineeName = null, nomineeRelation = null,
                nomineeDob = null, nomineeAge = null, yearOfExperience = null, preEmpDesignation = null, prevCompanyName = null, prevCompanyAddress = null, prevCompanyCity = null, preEmpState = null, preEmpPincode = null, prevCompanyContact = "";
        String userName = "", password = "", RoleIds = null, staffId = "", activeInd = "", branchId = null, companyId = null;
        String passportName = null,passportNo= null, passportType = null, nationality = null, palceOfIssue = null, palceOfBirth = null, dateOfIssue= null,dateOfExpiry=null,uploadRemarks=null,uploadId="";
        String visaProvider = null,visaType= null, visaDateOfValidity = null, countryEnteringDate = null, visaValidity = null, visaDateOfIssue = null,visaRemarks=null,visaId="";
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        int i = 0;
        int j = 0;

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

            EmployeeTO employeeTO = new EmployeeTO();
            EmployeeTO empTO = new EmployeeTO();
            String filename[] = new String[10];
            //             String fileSavedAs = "";
            String fileSavedAs1[] = new String[10];
            String certificateIds[] = new String[10];
            //                int i = 0;
            //             Part _part = null;
            //                int j = 0;
            int o = 0;
            int k = 0;
            int l = 0;
            int m = 0;
            int n = 0;
            int p = 0;
//                        String[] relationName = null;
//                        String[] relation = null;
//                        String[] relationDOB = null;
//                        String[] relationAge = null;
//                        String[] empRelationIds = null;
            String[] relationName = new String[100];
            String[] relation = new String[100];
            String[] relationDOB = new String[100];
            String[] relationAge = new String[100];
            String[] empRelationIds = new String[100];
            int staffUserId = 0;
            int roleId = 0;
            int staffUser = 0;
            int userId = (Integer) session.getAttribute("userId");
            isMultipart = ServletFileUpload.isMultipartContent(request);
            if (isMultipart) {
                //System.out.println("this is tht");
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    //                    //System.out.println("part Name:" + _part.getName());
                    String filePath = "";
                    //                    filePath = "/employee";
                    filePath = getServletContext().getRealPath("/uploadFiles/Files");
                    //                    filePath = "C:/EmployeePhotoCopies";
                    //System.out.println("REAL filePath = " + filePath);
                    String filePath1 = filePath.replace("\\", "\\\\");
                    Date now = new Date();
                    String date = now.getDate() + "" + (now.getMonth() + 1) + "" + (now.getYear() + 1900);
                    String time = now.getHours() + "" + now.getMinutes() + "" + now.getSeconds() + userId;

                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        fPart = (FilePart) partObj;
                        filename[j] = fPart.getFileName();

                        if (!"".equals(filename[j]) && filename[j] != null) {
                            String[] fp = filename[j].split("\\.");
                            fileSavedAs = fp[0] + "." + fp[1];
                            fileSavedAs1[j] = fp[0] + "." + fp[1];
                            if ("employeePhoto".equals(partObj.getName())) {
                                employeePhoto = filePath1 + "\\" + fileSavedAs1[j];
                            } else if ("employeeLicense".equals(partObj.getName())) {
                                employeeLicense = filePath1 + "\\" + fileSavedAs1[j];
                            }
                            else if ("passportPhotoFile".equals(partObj.getName())) {
                                passportPhotoFile = filePath1 + "\\" + fileSavedAs1[j];
                            }
                            else if ("visaPhoto".equals(partObj.getName())) {
                                visaPhoto = filePath1 + "\\" + fileSavedAs1[j];
                            }
                            String tempPath = filePath1 + "\\" + fileSavedAs1[j];
                            String actPath = filePath + "\\" + filename[j];
//                            String tempPath = filePath1 + "/" + fileSavedAs1[j];
//                            String actPath = filePath + "/" + filename[j];
                            //System.out.println("tempPath..." + tempPath);
                            //System.out.println("actPath..." + actPath);
                            long fileSize = fPart.writeTo(new java.io.File(actPath));
                            //System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actPath);
                            f1.renameTo(new File(tempPath));

                        }

                        j++;

                    } else if (partObj.isParam()) {
                        ParamPart paramPart = (ParamPart) partObj;
                        if (partObj.getName().equals("relationName")) {
                            relationName[k] = paramPart.getStringValue();
                            k++;
                        }
                        if (partObj.getName().equals("empRelationIds")) {
                            empRelationIds[o] = paramPart.getStringValue();
                            o++;
                        }
                        if (partObj.getName().equals("relation")) {
                            relation[l] = paramPart.getStringValue();
                            System.out.println("relation"+relation[l]);
                            l++;
                        }
                        if (partObj.getName().equals("relationDOB")) {
                            relationDOB[m] = paramPart.getStringValue();
                            m++;
                        }
                        if (partObj.getName().equals("relationAge")) {
                            relationAge[n] = paramPart.getStringValue();
                            n++;
                        }
                        if (partObj.getName().equals("staffId")) {
                            staffId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("name")) {
                            name = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("qualification")) {
                            qualification = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("dateOfBirth")) {
                            dateOfBirth = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("dateOfJoining")) {
                            dateOfJoining = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("martialStatus")) {
                            martialStatus = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("gender")) {
                            gender = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("bloodGrp")) {
                            bloodGrp = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("fatherName")) {
                            fatherName = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("mobile")) {
                            mobile = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("phone")) {
                            phone = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("email")) {
                            email = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("addr")) {
                            addr = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("city")) {
                            city = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("state")) {
                            state = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("pincode")) {
                            pincode = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("deptid")) {
                            deptid = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("designationId")) {
                            designationId = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("gradeId")) {
                            gradeId = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("referenceName")) {
                            referenceName = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("drivingLicenseNo")) {
                            drivingLicenseNo = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("licenseExpDate")) {
                            licenseExpDate = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("licenseType")) {
                            licenseType = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("chec")) {
                            chec = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("bankAccountNo")) {
                            bankAccountNo = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("bankName")) {
                            bankName = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("bankBranchName")) {
                            bankBranchName = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("accountHolderName")) {
                            accountHolderName = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("yearOfExperience")) {
                            yearOfExperience = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("preEmpDesignation")) {
                            preEmpDesignation = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("prevCompanyName")) {
                            prevCompanyName = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("prevCompanyAddress")) {
                            prevCompanyAddress = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("prevCompanyCity")) {
                            prevCompanyCity = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("preEmpState")) {
                            preEmpState = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("preEmpPincode")) {
                            preEmpPincode = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("prevCompanyContact")) {
                            prevCompanyContact = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("yearOfExperience")) {
                            yearOfExperience = paramPart.getStringValue();
                        }

                        if (partObj.getName().equals("salaryType")) {
                            salaryType = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("basicSalary")) {
                            basicSalary = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("esiEligible")) {
                            esiEligible = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("pfEligible")) {
                            pfEligible = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("nomineeName")) {
                            nomineeName = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("nomineeRelation")) {
                            nomineeRelation = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("nomineeDob")) {
                            nomineeDob = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("nomineeAge")) {
                            nomineeAge = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("licenseState")) {
                            licenseState = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("cmpId")) {
                            cmpId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("contractDriver")) {
                            contractDriver = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("pincode1")) {
                            pincode1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("addr1")) {
                            addr1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("city1")) {
                            city1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("state1")) {
                            state1 = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("vendorCompany")) {
                            vendorCompany = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("staffUserId")) {
                            staffUserId = Integer.parseInt(paramPart.getStringValue());
                            staffUser = Integer.parseInt(paramPart.getStringValue());
                        }
                        if (partObj.getName().equals("userName")) {
                            userName = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("password")) {
                            password = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("roleId")) {
                            roleId = Integer.parseInt(paramPart.getStringValue());
                            RoleIds = paramPart.getStringValue();
//                                roleIds= Integer.parseInt(paramPart.getStringValue());
                        }
                        if (partObj.getName().equals("activeInd")) {
                            activeInd = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("branchId")) {
                            branchId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("companyId")) {
                            companyId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("empRelationSize")) {
                            empRelationSize = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("passportNo")) {
                            passportNo = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("passportName")) {
                            passportName = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("passportType")) {
                            passportType = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("nationality")) {
                            nationality = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("palceOfIssue")) {
                            palceOfIssue = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("dateOfIssue")) {
                            dateOfIssue = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("dateOfExpiry")) {
                            dateOfExpiry = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("palceOfBirth")) {
                            palceOfBirth = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("uploadRemarks")) {
                            uploadRemarks = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("uploadId")) {
                            uploadId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("visaProvider")) {
                            visaProvider = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("visaType")) {
                            visaType = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("visaDateOfIssue")) {
                            visaDateOfIssue = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("visaDateOfValidity")) {
                            visaDateOfValidity = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("countryEnteringDate")) {
                            countryEnteringDate = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("visaValidity")) {
                            visaValidity = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("visaRemarks")) {
                            visaRemarks = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("visaId")) {
                            visaId = paramPart.getStringValue();
                        }

//                            if (partObj.getName().equals("RoleId")) {
//                                RoleId = paramPart.getStringValue();
//                            }
//                            if (partObj.getName().equals("userName")) {
//                                userName = paramPart.getStringValue();
//                            }
//                            if (partObj.getName().equals("password")) {
//                                password = paramPart.getStringValue();
//                            }
                    }
                }
            }

            String file = "";
            String remarks = "";
            String lrNumber = "";
            String saveFile = "";

            int status = 0;
            //System.out.println("branchId idhfshasgdj" + branchId);
            //System.out.println("role idhfshasgdj" + roleId);
            //System.out.println("name" + name);
            employeeTO.setCompanyId(companyId);
            employeeTO.setBranchId(branchId);
            employeeTO.setRoleId(roleId);
            employeeTO.setRoleIds(RoleIds);
            //System.out.println("roleId" + employeeTO.getRoleId());
            //System.out.println("roleId2" + employeeTO.getRoleIds());
            employeeTO.setUserName(userName);
            employeeTO.setPassword(password);
            employeeTO.setStatus(activeInd);
            employeeTO.setStaffId(staffId);
            employeeTO.setBankName(bankName);
            employeeTO.setBankBranchName(bankBranchName);
            employeeTO.setAccountHolderName(accountHolderName);
            employeeTO.setPincode1(pincode1);
            employeeTO.setAddr1(addr1);
            employeeTO.setCity1(city1);
            employeeTO.setState1(state1);
            employeeTO.setVendorCompany(vendorCompany);
            employeeTO.setLicenseState(licenseState);
            employeeTO.setContractDriver(contractDriver);

            employeeTO.setPhotoFile(employeePhoto);
            employeeTO.setLicenseFile(employeeLicense);
            employeeTO.setSalaryType(salaryType);
            employeeTO.setBasicSalary(basicSalary);
            employeeTO.setEsiEligible(esiEligible);
            employeeTO.setPfEligible(pfEligible);
            employeeTO.setNomineeName(nomineeName);
            employeeTO.setNomineeRelation(nomineeRelation);
            employeeTO.setNomineeDob(nomineeDob);
            employeeTO.setNomineeAge(nomineeAge);

            employeeTO.setName(name);

            employeeTO.setQualification(qualification);

            employeeTO.setDateOfBirth(dateOfBirth);

            employeeTO.setDOJ(dateOfJoining);

            employeeTO.setMartialStatus(martialStatus);

            employeeTO.setGender(gender);

            employeeTO.setBloodGrp(bloodGrp);

            employeeTO.setFatherName(fatherName);

            employeeTO.setMobile(mobile);

            employeeTO.setPhone(phone);

            employeeTO.setEmail(email);

            employeeTO.setAddr(addr);

            employeeTO.setCity(city);

            employeeTO.setState(state);
            employeeTO.setPincode(pincode);

            employeeTO.setDeptId(Integer.parseInt(deptid));

            employeeTO.setDesigId(Integer.parseInt(designationId));

            employeeTO.setGradeId(Integer.parseInt(gradeId));

            employeeTO.setReferenceName(referenceName);

            employeeTO.setDrivingLicenseNo(drivingLicenseNo);

            employeeTO.setLicenseExpDate(licenseExpDate);

            employeeTO.setLicenseType(licenseType);

            employeeTO.setYearOfExperience(yearOfExperience);
            employeeTO.setPreEmpDesignation(preEmpDesignation);
            employeeTO.setPrevCompanyName(prevCompanyName);
            employeeTO.setPrevCompanyAddress(prevCompanyAddress);
            employeeTO.setPrevCompanyCity(prevCompanyCity);
            employeeTO.setPreEmpState(preEmpState);
            employeeTO.setPreEmpPincode(preEmpPincode);
            employeeTO.setPrevCompanyContact(prevCompanyContact);

            employeeTO.setEmpRelationSize(empRelationSize);
            employeeTO.setEmpRelationIds(empRelationIds);
            employeeTO.setRelationName(relationName);
            employeeTO.setRelation(relation);
            employeeTO.setRelationDOB(relationDOB);
            employeeTO.setRelationAge(relationAge);
            employeeTO.setChec(chec);
            employeeTO.setBankAccountNo(bankAccountNo);

            employeeTO.setPassportPhotoFile(passportPhotoFile);
            employeeTO.setPassportNo(passportNo);
            employeeTO.setPassportName(passportName);
            employeeTO.setPassportType(passportType);
            employeeTO.setNationality(nationality);
            employeeTO.setPalceOfIssue(palceOfIssue);
            employeeTO.setPalceOfBirth(palceOfBirth);
            employeeTO.setDateOfIssue(dateOfIssue);
            employeeTO.setDateOfExpiry(dateOfExpiry);
            employeeTO.setRemarks(uploadRemarks);
            employeeTO.setUploadId(uploadId);
            employeeTO.setCmpId(cmpId);

            employeeTO.setVisaProvider(visaProvider);
            employeeTO.setVisaType(visaType);
            employeeTO.setVisaDateOfIssue(visaDateOfIssue);
            employeeTO.setVisaDateOfValidity(visaDateOfValidity);
            employeeTO.setCountryEnteringDate(countryEnteringDate);
            employeeTO.setVisaValidity(visaValidity);
            employeeTO.setVisaRemarks(visaRemarks);
            employeeTO.setVisaPhoto(visaPhoto);
            employeeTO.setVisaId(visaId);
//            System.out.println("UserId in relationName" + relationName.length);
            //System.out.println("UserId in relationName" + relationName[0]);
            //System.out.println("UserId in relationName" + relationName[1]);
            //System.out.println("UserId in relation" + relation.length);
            //System.out.println("UserId in relation" + relation[0]);
            //System.out.println("UserId in relation" + relation[1]);
            //System.out.println("UserId in relationDOB" + relationDOB.length);
            //System.out.println("UserId in relationDOB" + relationDOB[0]);
            //System.out.println("UserId in relationDOB" + relationDOB[1]);
            //System.out.println("UserId in relationAge" + relationAge.length);
            //System.out.println("UserId in relationAge" + relationAge[0]);
            //System.out.println("UserId in relationAge" + relationAge[1]);

            ArrayList staffList = new ArrayList();
            ArrayList familyList = new ArrayList();
            System.out.println("staffUser: " + staffUser);
            System.out.println("staffUserId: " + staffUserId);
            System.out.println("userStatus: " + userStatus);
            System.out.println("userStatus1" + userStatus1);
            System.out.println("roleId" + roleId);
            System.out.println("userId" + userId);
            if (staffUserId != 0 || employeeTO.getRoleIds() != "") {
                staffUserId = getEmployeeBP().updateUser(employeeTO, userId, staffUserId, roleId);
                //System.out.println("staffUserId1: " + staffUserId);
            } else if (employeeTO.getRoleIds() != "" || staffUserId == 0) {
                staffUserId = getEmployeeBP().addNewUser(employeeTO, userId);
                //System.out.println("staffUserId: " + staffUserId);
            }
            employeeTO.setEmpUserId(String.valueOf(staffUser));
            System.out.println("setEmpUserId: " + employeeTO.getEmpUserId());
            status = employeeBP.processUpdateEmpDetails(employeeTO, userId, staffUserId);
            //System.out.println("Update Emp Details: " + status);

            ArrayList employeeDetails = new ArrayList();
            //employeeDetails = employeeBP.getEmployeeDetails();
            employeeDetails = employeeBP.getEmployeeDetails(empTO);
            request.setAttribute("employeeDetails", employeeDetails);

            if (status != 0) {
                //path = "content/employee/viewEmp.jsp";
                path = "content/employee/viewEmployee.jsp";
                staffList = employeeBP.processEmpDetails(employeeTO.getStaffId(), employeeTO.getName());

                familyList = employeeBP.processEmpFamilyDetails(employeeTO.getStaffId(), employeeTO.getName());

                ////System.out.println("size is  " + staffList.size());
                request.setAttribute("StaffList", staffList);
                request.setAttribute("staffId", employeeTO.getStaffId());
                request.setAttribute("staffName", employeeTO.getName());
                request.setAttribute("familyList", familyList);

                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Employee Modified Successfully");
            } else {
                ////System.out.println("status in controller" + status);

                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Employee Not Modified");

                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                path = "content/employee/searchAlterEmp.jsp";
            }
            //          }
            //    }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to add Lecture data -->" + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }



    public ModelAndView handleEmpViewSearchPage(HttpServletRequest request, HttpServletResponse response, EmployeeCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String pageTitle = "View Employee";
        request.setAttribute("pageTitle", pageTitle);

        String path = "";
        HttpSession session = request.getSession();
        try {
            String menuPath = "";
            menuPath = "HRMS >> Manage Employee >> View";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Employee-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                request.setAttribute("staffCode", "");
                path = "content/employee/viewEmp.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
//        } catch (FPBusinessException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
//                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view lecture data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

public ModelAndView handleEmpViewPage(HttpServletRequest request, HttpServletResponse response, EmployeeCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String pageTitle = "View Employee";
        request.setAttribute("pageTitle", pageTitle);
         employeecommand = command;
          EmployeeTO empTO = new EmployeeTO();
        String path = "";
        String param = "";
        HttpSession session = request.getSession();
        try {
            String menuPath = "";
            menuPath = "HRMS >> Manage Employee >> View";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            param = request.getParameter("param");
            if (!loginBP.checkAuthorisation(userFunctions, "Employee-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                if ("ExcelExport".equals(param)) {
                    path = "content/employee/viewEmployeeExcel.jsp";
                } else {
                    request.setAttribute("staffCode", "");
                    path = "content/employee/viewEmployee.jsp";
                }
            }
             if (employeecommand.getDesigId() != null && employeecommand.getDesigId() != "") {
                empTO.setDesigId(Integer.parseInt(employeecommand.getDesigId()));
            }
            String action = request.getParameter("action");
            request.setAttribute("action", action);

            ArrayList employeeDetails = new ArrayList();
            employeeDetails = employeeBP.getEmployeeDetails(empTO);
            request.setAttribute("employeeDetails", employeeDetails);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
//        } catch (FPBusinessException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
//                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view lecture data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
public ModelAndView handleViewPrimaryDriver(HttpServletRequest request, HttpServletResponse response, EmployeeCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String pageTitle = "View Employee";
        request.setAttribute("pageTitle", pageTitle);
         employeecommand = command;
          EmployeeTO empTO = new EmployeeTO();
        String path = "";
        String param = "";
        HttpSession session = request.getSession();
        try {
            String menuPath = "";
            menuPath = "HRMS >> Manage Employee >> View";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            param = request.getParameter("param");
            if (!loginBP.checkAuthorisation(userFunctions, "Employee-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                if ("ExcelExport".equals(param)) {
                    path = "content/employee/viewEmployeeExcel.jsp";
                } else {
                    request.setAttribute("staffCode", "");
                    path = "content/employee/primaryDriverDetails.jsp";
                }
            }
             if (employeecommand.getDesigId() != null && employeecommand.getDesigId() != "") {
                empTO.setDesigId(Integer.parseInt(employeecommand.getDesigId()));
            }
            String action = request.getParameter("action");
            request.setAttribute("action", action);

            ArrayList employeeDetails = new ArrayList();
            employeeDetails = employeeBP.getPrimaryDriverDetails(empTO);
            request.setAttribute("employeeDetails", employeeDetails);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
//        } catch (FPBusinessException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
//                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view lecture data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleSearchViewEmp(HttpServletRequest request, HttpServletResponse response, EmployeeCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        String menuPath = "HRMS  >>  Manage Employee  >> View";
        employeecommand = command;
        HttpSession session = request.getSession();
        String pageTitle = "View Staff";
        request.setAttribute("pageTitle", pageTitle);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Staff-ViewPage")) {
//                menuPath = "Not Authorised";
//                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//                path = "content/common/NotAuthorized.jsp";
//            } else {

            ArrayList staffList = new ArrayList();
            ArrayList familyList = new ArrayList();
            String staffId = "";
            String staffCode = "";
            String staffName = "";
            if (employeecommand.getStaffCode() != null && employeecommand.getStaffCode() != "") {
                staffCode = employeecommand.getStaffCode();
            }
            if (employeecommand.getStaffName() != null && employeecommand.getStaffName() != "") {
                staffName = employeecommand.getStaffName();
            }
            staffId = employeeBP.getStaffId(staffCode, staffName);

            //////System.out.println(" id is " + staffId);
            //////System.out.println(" Name is " + staffName);

            path = "content/employee/viewEmp.jsp";
            staffList = employeeBP.processEmpDetails(staffId, staffName);
            familyList = employeeBP.processEmpFamilyDetails(staffId, staffName);
            //////System.out.println("size is  " + staffList.size());
            request.setAttribute("StaffList", staffList);
            request.setAttribute("staffId", staffId);
            request.setAttribute("staffName", staffName);
            request.setAttribute("familyList", familyList);
            request.setAttribute("staffCode", staffCode);
            request.setAttribute("staffName", staffName);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search lecture data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }

    public void handleGetEmpName(HttpServletRequest request, HttpServletResponse response, EmployeeCommand command) throws IOException {
        //////System.out.println("i am in handleGetEmpName ajax ");
        HttpSession session = request.getSession();
        employeecommand = command;

        // ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        //if (!loginBP.checkAuthorisation(userFunctions, "Designation-ViewPage")) {
        //  path = "content/common/NotAuthorized.jsp";
        //} else {
        String staffName = employeecommand.getStaffName();
        //////System.out.println("staffName  " + staffName);
        String suggestions = employeeBP.processEmpNameSuggestions(staffName);
        PrintWriter writer = response.getWriter();
        System.out.println(suggestions);
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.println(suggestions);
        writer.close();
    }

    public void handleGetEmpCode(HttpServletRequest request, HttpServletResponse response, EmployeeCommand command) throws IOException {
        //////System.out.println("i am in handleGetEmpCode ajax ");
        HttpSession session = request.getSession();
        employeecommand = command;

        // ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        //if (!loginBP.checkAuthorisation(userFunctions, "Designation-ViewPage")) {
        //  path = "content/common/NotAuthorized.jsp";
        //} else {
        String mode = "";
        String empCode = employeecommand.getStaffCode();
        //////System.out.println("empCode = " + empCode);
        String suggestions = "";
        suggestions = employeeBP.processEmpCode(empCode);
        PrintWriter writer = response.getWriter();
        System.out.println(suggestions);
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        if (suggestions != "" && suggestions != null) {
            writer.println(suggestions);
        } else {
            writer.println("No Records Found");
        }
        writer.close();
    }

    public ModelAndView handleUploadEmployee(HttpServletRequest request, HttpServletResponse response, EmployeeCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        FileInputStream fis = null;
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        int userId = 0;
        EmployeeTO empTO = new EmployeeTO();
        String pageTitle = "Upload Employee Details";
        menuPath = "Employee >> Upload Employe Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;

        ArrayList employeeHistoryDetails = new ArrayList();
        String empName = "", dateOfBirth = "", dateOfJoining = "", gender = "", bloodGroup = "", maritalStatus = "";
        String fatherName = "", mobile = "", phone = "", qualification = "", email = "", deptName = "", desigName = "";
        String gradeName = "", companyName = "", referenceName = "", contractDriver = "", drivingLicenseNo = "", licenseDate = "", licenseType = "";
        String licenseState = "", addr = "", city = "", state = "", pincode = "", addr1 = "", city1 = "";
        String state1 = "", pincode1 = "", salaryType = "", basicSalary = "", esiEligible = "", pfEligible = "", bankAccountNo = "", nomineeName = "";
        String userName = "", password = "", roleName = "", userAccess = "", bankName = "", bankBranchName = "";
        int status = 0;
        Map map = new HashMap();
        HashSet hs = new HashSet();
        try {
            Object mapValue = null;
            employeecommand = command;
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            isMultipart = ServletFileUpload.isMultipartContent(request);
            //////System.out.println("part Name:" + isMultipart);
            if (isMultipart) {

                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    //////System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files");
                        //////System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        //////System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();

                        if (!"".equals(uploadedFileName) && uploadedFileName != null) {

                            String[] splitFileName = uploadedFileName.split("\\.");
                            fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath = tempServerFilePath + "\\" + fileSavedAs;
                            actualFilePath = actualServerFilePath + "\\" + uploadedFileName;
                            //////System.out.println("tempPath..." + tempFilePath);
                            //////System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                            //////System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
                            //////System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath));
                            //////System.out.println("tempPath = " + tempFilePath);
                            //////System.out.println("actPath = " + actualFilePath);
                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
                            String part1 = parts.replace("\\", "");

                            WorkbookSettings ws = new WorkbookSettings();
                            ws.setLocale(new Locale("en", "EN"));
                            Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                            Sheet s = workbook.getSheet(0);
                            //////System.out.println("rows" + userId + s.getRows());
                            
                            for (int i = 1; i < s.getRows(); i++) {

                                EmployeeTO employeeTO = new EmployeeTO();

                                empName = s.getCell(1, i).getContents();
                                employeeTO.setName(empName);

                                dateOfBirth = s.getCell(2, i).getContents();
                                employeeTO.setDateOfBirth(dateOfBirth);

                                dateOfJoining = s.getCell(3, i).getContents();
                                employeeTO.setDOJ(dateOfJoining);

                                gender = s.getCell(4, i).getContents();
                                employeeTO.setGender(gender);

                                bloodGroup = s.getCell(5, i).getContents();
                                employeeTO.setBloodGrp(bloodGroup);

                                maritalStatus = s.getCell(6, i).getContents();
                                employeeTO.setMartialStatus(maritalStatus);

                                fatherName = s.getCell(7, i).getContents();
                                employeeTO.setFatherName(fatherName);

                                mobile = s.getCell(8, i).getContents();
                                employeeTO.setMobile(mobile);

                                phone = s.getCell(9, i).getContents();
                                employeeTO.setPhone(phone);

                                qualification = s.getCell(10, i).getContents();
                                employeeTO.setQualification(qualification);

                                email = s.getCell(11, i).getContents();
                                employeeTO.setEmail(email);

                                deptName = s.getCell(12, i).getContents();
                                employeeTO.setDeptName(deptName);

                                desigName = s.getCell(13, i).getContents();
                                employeeTO.setDesigName(desigName);

                                gradeName = s.getCell(14, i).getContents();
                                employeeTO.setGradeName(gradeName);

                                companyName = s.getCell(15, i).getContents();
                                employeeTO.setCompanyName(companyName);

                                referenceName = s.getCell(16, i).getContents();
                                employeeTO.setReferenceName(referenceName);

                                contractDriver = s.getCell(17, i).getContents();
                                employeeTO.setContractDriver(contractDriver);

                                drivingLicenseNo = s.getCell(18, i).getContents();
                                employeeTO.setDrivingLicenseNo(drivingLicenseNo);

                                licenseDate = s.getCell(19, i).getContents();
                                employeeTO.setLicenseExpDate(licenseDate);

                                licenseType = s.getCell(20, i).getContents();
                                employeeTO.setLicenseType(licenseType);

                                licenseState = s.getCell(21, i).getContents();
                                employeeTO.setLicenseState(licenseState);

                                addr = s.getCell(22, i).getContents();
                                employeeTO.setAddr(addr);

                                city = s.getCell(23, i).getContents();
                                employeeTO.setCity(city);

                                state = s.getCell(24, i).getContents();
                                employeeTO.setState(state);

                                pincode = s.getCell(25, i).getContents();
                                employeeTO.setPincode(pincode);

                                addr1 = s.getCell(26, i).getContents();
                                employeeTO.setAddr1(addr1);

                                city1 = s.getCell(27, i).getContents();
                                employeeTO.setCity1(city1);

                                state1 = s.getCell(28, i).getContents();
                                employeeTO.setState1(state1);

                                pincode1 = s.getCell(29, i).getContents();
                                employeeTO.setPincode1(pincode1);

                                salaryType = s.getCell(30, i).getContents();
                                employeeTO.setSalaryType(salaryType);

                                basicSalary = s.getCell(31, i).getContents();
                                employeeTO.setBasicSalary(basicSalary);

                                esiEligible = s.getCell(32, i).getContents();
                                employeeTO.setEsiEligible(esiEligible);

                                pfEligible = s.getCell(33, i).getContents();
                                employeeTO.setPfEligible(pfEligible);

                                bankAccountNo = s.getCell(34, i).getContents();
                                employeeTO.setBankAccountNo(bankAccountNo);

                                bankName = s.getCell(35, i).getContents();
                                employeeTO.setBankName(bankName);

                                bankBranchName = s.getCell(36, i).getContents();
                                employeeTO.setBankBranchName(bankBranchName);

                                nomineeName = s.getCell(37, i).getContents();
                                employeeTO.setNomineeName(nomineeName);

//                                userAccess = s.getCell(38, i).getContents();
//                                employeeTO.setUserAccess(userAccess);
//
//                                userName = s.getCell(39, i).getContents();
//                                employeeTO.setUserName(userName);
//
//                                password = s.getCell(40, i).getContents();
//                                employeeTO.setPassword(password);
//
//                                roleName = s.getCell(41, i).getContents();
//                                employeeTO.setRoleName(roleName);
                                String checkEmployeeDetails = "";
                                //////System.out.println("map.get = " + map.get("empName"));
                                String checkName = empName+"~"+dateOfBirth+"~"+gender+"~"+bloodGroup+"~"+maritalStatus+"~"+fatherName+"~"+mobile+"~"+phone+"~"+qualification+"~"+email+"~"+deptName+"~"+desigName+"~"+gradeName+"~"+companyName+"~"+referenceName+"~"+contractDriver+"~"+drivingLicenseNo+"~"+licenseDate+"~"+licenseType+"~"+licenseState+"~"+addr+"~"+city+"~"+state+"~"+pincode+"~"+addr1+"~"+city1+"~"+state1+"~"+pincode1+"~"+salaryType+"~"+basicSalary+"~"+esiEligible+"~"+pfEligible+"~"+bankAccountNo+"~"+bankName+"~"+bankBranchName+"~"+nomineeName+"~"+dateOfJoining;
                                //////System.out.println("hs = " +hs);
                                if(!hs.contains(checkName)){
                                checkEmployeeDetails = employeeBP.checkEmployeeDetails(employeeTO);
                                if (checkEmployeeDetails == null) {
                                map.put("status","N");
                                map.put("name",empName);
                                map.put("empName", "N~"+empName+"~"+dateOfBirth+"~"+gender+"~"+bloodGroup+"~"+maritalStatus+"~"+fatherName+"~"+mobile+"~"+phone+"~"+qualification+"~"+email+"~"+deptName+"~"+desigName+"~"+gradeName+"~"+companyName+"~"+referenceName+"~"+contractDriver+"~"+drivingLicenseNo+"~"+licenseDate+"~"+licenseType+"~"+licenseState+"~"+addr+"~"+city+"~"+state+"~"+pincode+"~"+addr1+"~"+city1+"~"+state1+"~"+pincode1+"~"+salaryType+"~"+basicSalary+"~"+esiEligible+"~"+pfEligible+"~"+bankAccountNo+"~"+bankName+"~"+bankBranchName+"~"+nomineeName+"~"+dateOfJoining);
                                hs.add(empName+"~"+dateOfBirth+"~"+gender+"~"+bloodGroup+"~"+maritalStatus+"~"+fatherName+"~"+mobile+"~"+phone+"~"+qualification+"~"+email+"~"+deptName+"~"+desigName+"~"+gradeName+"~"+companyName+"~"+referenceName+"~"+contractDriver+"~"+drivingLicenseNo+"~"+licenseDate+"~"+licenseType+"~"+licenseState+"~"+addr+"~"+city+"~"+state+"~"+pincode+"~"+addr1+"~"+city1+"~"+state1+"~"+pincode1+"~"+salaryType+"~"+basicSalary+"~"+esiEligible+"~"+pfEligible+"~"+bankAccountNo+"~"+bankName+"~"+bankBranchName+"~"+nomineeName+"~"+dateOfJoining);
                                request.setAttribute("count", 1);
                                }else{
                                map.put("status","Y");
                                map.put("name",empName);
                                map.put("empName", "Y~"+empName+"~"+dateOfBirth+"~"+gender+"~"+bloodGroup+"~"+maritalStatus+"~"+fatherName+"~"+mobile+"~"+phone+"~"+qualification+"~"+email+"~"+deptName+"~"+desigName+"~"+gradeName+"~"+companyName+"~"+referenceName+"~"+contractDriver+"~"+drivingLicenseNo+"~"+licenseDate+"~"+licenseType+"~"+licenseState+"~"+addr+"~"+state+"~"+pincode+"~"+addr1+"~"+state1+"~"+pincode1+"~"+salaryType+"~"+basicSalary+"~"+esiEligible+"~"+pfEligible+"~"+bankAccountNo+"~"+bankName+"~"+bankBranchName+"~"+nomineeName+"~"+dateOfJoining);
                                hs.add(empName+"~"+dateOfBirth+"~"+gender+"~"+bloodGroup+"~"+maritalStatus+"~"+fatherName+"~"+mobile+"~"+phone+"~"+qualification+"~"+email+"~"+deptName+"~"+desigName+"~"+gradeName+"~"+companyName+"~"+referenceName+"~"+contractDriver+"~"+drivingLicenseNo+"~"+licenseDate+"~"+licenseType+"~"+licenseState+"~"+addr+"~"+city+"~"+state+"~"+pincode+"~"+addr1+"~"+city1+"~"+state1+"~"+pincode1+"~"+salaryType+"~"+basicSalary+"~"+esiEligible+"~"+pfEligible+"~"+bankAccountNo+"~"+bankName+"~"+bankBranchName+"~"+nomineeName+"~"+dateOfJoining);
                                }
                                }else{
                                map.put("status","Y");
                                map.put("name",empName);
                                map.put("empName", "Y~"+empName+"~"+dateOfBirth+"~"+gender+"~"+bloodGroup+"~"+maritalStatus+"~"+fatherName+"~"+mobile+"~"+phone+"~"+qualification+"~"+email+"~"+deptName+"~"+desigName+"~"+gradeName+"~"+companyName+"~"+referenceName+"~"+contractDriver+"~"+drivingLicenseNo+"~"+licenseDate+"~"+licenseType+"~"+licenseState+"~"+addr+"~"+state+"~"+pincode+"~"+addr1+"~"+state1+"~"+pincode1+"~"+salaryType+"~"+basicSalary+"~"+esiEligible+"~"+pfEligible+"~"+bankAccountNo+"~"+bankName+"~"+bankBranchName+"~"+nomineeName+"~"+dateOfJoining);
                                hs.add(empName+"~"+dateOfBirth+"~"+gender+"~"+bloodGroup+"~"+maritalStatus+"~"+fatherName+"~"+mobile+"~"+phone+"~"+qualification+"~"+email+"~"+deptName+"~"+desigName+"~"+gradeName+"~"+companyName+"~"+referenceName+"~"+contractDriver+"~"+drivingLicenseNo+"~"+licenseDate+"~"+licenseType+"~"+licenseState+"~"+addr+"~"+city+"~"+state+"~"+pincode+"~"+addr1+"~"+city1+"~"+state1+"~"+pincode1+"~"+salaryType+"~"+basicSalary+"~"+esiEligible+"~"+pfEligible+"~"+bankAccountNo+"~"+bankName+"~"+bankBranchName+"~"+nomineeName+"~"+dateOfJoining);
                                }
                                if ("".equals(empName) && "".equals(dateOfBirth)) {
                                    break;
                                }
                                Object mapStatus = map.get("status");
                                mapValue = map.get("empName");
                                Object mapNameValue = map.get("name");
                                employeeTO.setEmpCode(String.valueOf(mapValue));
                                employeeTO.setStatus(String.valueOf(mapStatus));
                                employeeTO.setName(String.valueOf(mapNameValue));
                                employeeHistoryDetails.add(employeeTO);

                            }
                        }
                    }
                }
            }
            request.setAttribute("employeeHistoryDetails", employeeHistoryDetails);

            path = "content/employee/employeeHistoryDetails.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    public ModelAndView saveUploadEmployee(HttpServletRequest request, HttpServletResponse response, EmployeeCommand command) {

        //////System.out.println("command TESTING = " );
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        //////System.out.println("command TESTING = " );
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        int userId = 0;
        EmployeeTO empTO = new EmployeeTO();
        String pageTitle = "Employee Uploading";
        menuPath = "HRMS >> Upload Employee";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//        boolean isMultipart = false;
        ArrayList employeeHistoryDetails = new ArrayList();
        int insertStatus = 0;
        try {
            employeecommand = command;
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            //isMultipart = ServletFileUpload.isMultipartContent(request);
            String[] status = request.getParameterValues("status");
            String[] empCode = request.getParameterValues("empCode");
            String email = "";
            String dateOfBirth = "";
            String dateOfJoining = "";
            String gender = "";
            String bloodGrp = "";
            String martialStatus = "";
            String fatherName = "";
            String mobile = "";
            String phone = "";
            String qualification = "";
            String referenceName = "";
            String contractDriver = "";
            String drivingLicenseNo = "";
            String licenseExpDate = "";
            String licenseType = "";
            String licenseState = "";
            String addr = "";
            String city = "";
            String state = "";
            String pincode = "";
            String addr1 = "";
            String city1 = "";
            String state1 = "";
            String pincode1 = "";
            String salaryType = "";
            String basicSalary = "";
            String esiEligible = "";
            String pfEligible = "";
            String nomineeName = "";
            String deptName = "";
            String desigName = "";
            String gradeName = "";
            String companyName = "";
            String bankAccountNo = "";
            String bankName = "";
            String bankBranchName = "";
            String empName = "";
            int UserId = (Integer) session.getAttribute("userId");
            //////System.out.println("status.length = " + status.length);
            int staffUserId = 0;
            String temp[] = null;
            for (int i = 0; i < status.length; i++) {
                EmployeeTO employeeTO = new EmployeeTO();
                temp =  empCode[i].split("~"); 
                if(temp[0].equals("N")){
                    employeeTO.setName(temp[1]);
                    employeeTO.setDateOfBirth(temp[2]);
                    employeeTO.setGender(temp[3]);
                    employeeTO.setBloodGrp(temp[4]);
                    employeeTO.setMartialStatus(temp[5]);
                    employeeTO.setFatherName(temp[6]);
                    employeeTO.setMobile(temp[7]);
                    employeeTO.setPhone(temp[8]);
                    employeeTO.setQualification(temp[9]);
                    employeeTO.setEmail(temp[10]);
                    employeeTO.setDeptName(temp[11]);
                    employeeTO.setDesigName(temp[12]);
                    employeeTO.setGradeName(temp[13]);
                    employeeTO.setCompanyName(temp[14]);
                    employeeTO.setReferenceName(temp[15]);
                    employeeTO.setContractDriver(temp[16]);
                    employeeTO.setDrivingLicenseNo(temp[17]);
                    employeeTO.setLicenseExpDate(temp[18]);
                    employeeTO.setLicenseType(temp[19]);
                    employeeTO.setLicenseState(temp[20]);
                    employeeTO.setAddr(temp[21]);
                    employeeTO.setCity(temp[22]);
                    employeeTO.setState(temp[23]);
                    employeeTO.setPincode(temp[24]);
                    employeeTO.setAddr1(temp[25]);
                    employeeTO.setCity1(temp[26]);
                    employeeTO.setState1(temp[27]);
                    employeeTO.setPincode1(temp[28]);
                    employeeTO.setSalaryType(temp[29]);
                    employeeTO.setBasicSalary(temp[30]);
                    employeeTO.setEsiEligible(temp[31]);
                    employeeTO.setPfEligible(temp[32]);
                    employeeTO.setBankAccountNo(temp[33]);
                    employeeTO.setBankName(temp[34]);
                    employeeTO.setBankBranchName(temp[35]);
                    employeeTO.setNomineeName(temp[36]);
                    employeeTO.setDOJ(temp[37]);
                    employeeTO.setNomineeDob("00-00-0000");
                    String ids = employeeBP.getDeptId(employeeTO);
                    String temp1[] = ids.split("-");
                    employeeTO.setCmpId(temp1[2]);
                    employeeTO.setDeptId(Integer.parseInt(temp1[0]));
                    employeeTO.setDesigId(Integer.parseInt(temp1[1]));
                    employeeTO.setGradeId(Integer.parseInt(temp1[3]));
                    employeeTO.setEmpCode("123");
                    employeeTO.setVendorCompany("0");
                    insertStatus = getEmployeeBP().insertEmployeeDetails(employeeTO, UserId, 0);
                }

            }

            if (insertStatus != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Employee Added Successfully");
            }

            ArrayList employeeDetails = new ArrayList();
            //employeeDetails = employeeBP.getEmployeeDetails();
            employeeDetails = employeeBP.getEmployeeDetails(empTO);
            request.setAttribute("employeeDetails", employeeDetails);

            path = "content/employee/viewEmployee.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    public ModelAndView alterEmployeeStatus(HttpServletRequest request, HttpServletResponse response, EmployeeCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        String menuPath = "HRMS  >>  Manage Employee  >> View";
        employeecommand = command;
          EmployeeTO empTO = new EmployeeTO();
        HttpSession session = request.getSession();
        String pageTitle = "View Staff";
        request.setAttribute("pageTitle", pageTitle);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

            String staffId = "";
            String staffCode = "";
            String staffName = "";
            if (employeecommand.getStaffCode() != null && employeecommand.getStaffCode() != "") {
                staffCode = employeecommand.getStaffCode();
            }
            if (employeecommand.getStaffId() != null && employeecommand.getStaffId() != "") {
                staffId = employeecommand.getStaffId();
            }
            if (employeecommand.getStaffName() != null && employeecommand.getStaffName() != "") {
                staffName = employeecommand.getStaffName();
            }

            //////System.out.println(" id is " + staffId);
            //////System.out.println(" Name is " + staffName);
            
            String tripId =   employeeBP.checkDriverStatus(staffId);

            path = "content/employee/alterEmployeeStatus.jsp";

            request.setAttribute("staffCode", staffCode);
            request.setAttribute("staffName", staffName);
            request.setAttribute("staffId", staffId);
            request.setAttribute("tripId", tripId);
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");


        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search lecture data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }

     public ModelAndView saveEmployeeStatus(HttpServletRequest request, HttpServletResponse response, EmployeeCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        String menuPath = "HRMS  >>  Manage Employee  >> View";
        employeecommand = command;
         ModelAndView mv = null;
          EmployeeTO empTO = new EmployeeTO();
        HttpSession session = request.getSession();
        String pageTitle = "View Staff";
        request.setAttribute("pageTitle", pageTitle);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

            if (employeecommand.getStaffId() != null && employeecommand.getStaffId() != "") {
               empTO.setStaffId(employeecommand.getStaffId());
            }
            if (employeecommand.getActiveInd() != null && employeecommand.getActiveInd() != "") {
               empTO.setActiveInd(employeecommand.getActiveInd());
            }
            if (employeecommand.getRemarks() != null && employeecommand.getRemarks() != "") {
               empTO.setRemarks(employeecommand.getRemarks());
            }
            int status = employeeBP.saveEmployeeStatus(empTO);


            path = "content/employee/alterEmployeeStatus.jsp";

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
             mv = handleViewPrimaryDriver(request, response, command);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search lecture data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }
 public void displayLogoBlobData(HttpServletRequest request, HttpServletResponse response, EmployeeCommand command) {
        String uploadId = "";
        try {
            uploadId = request.getParameter("uploadId");
            System.out.println("uploadId" + uploadId);
            ArrayList profileList = new ArrayList();
            profileList = employeeBP.getDisplayLogoBlobData(uploadId);
            if (profileList.size() > 0) {

                EmployeeTO empTO = (EmployeeTO) profileList.get(0);
                byte[] profileImg = empTO.getProfileImg();
                response.setContentType("image/jpeg");
                OutputStream out = response.getOutputStream();
                out.write(profileImg);
                out.close();
            }
        } catch (FPRuntimeException exception) {
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }
    public void displayLogoBlobDataEmp(HttpServletRequest request, HttpServletResponse response, EmployeeCommand command) {
        String staffId = "";
        try {
            staffId = request.getParameter("staffId");
            System.out.println("staffId" + staffId);
            ArrayList profileList = new ArrayList();
            profileList = employeeBP.getDisplayLogoBlobDataEmp(staffId);
            if (profileList.size() > 0) {

                EmployeeTO empTO = (EmployeeTO) profileList.get(0);
                byte[] empImg = empTO.getEmpImg();
                response.setContentType("image/jpeg");
                OutputStream out = response.getOutputStream();
                out.write(empImg);
                out.close();
            }
        } catch (FPRuntimeException exception) {
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }
    public void displayLogoBlobDataLicense(HttpServletRequest request, HttpServletResponse response, EmployeeCommand command) {
        String staffId = "";
        try {
            staffId = request.getParameter("staffId");
            System.out.println("staffIdLis" + staffId);
            ArrayList profileList = new ArrayList();
            profileList = employeeBP.getDisplayLogoBlobDataLicense(staffId);
            if (profileList.size() > 0) {

                EmployeeTO empTO = (EmployeeTO) profileList.get(0);
                byte[] licenseImg = empTO.getLicenseImg();
                response.setContentType("image/jpeg");
                OutputStream out = response.getOutputStream();
                out.write(licenseImg);
                out.close();
            }
        } catch (FPRuntimeException exception) {
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }
    public void displayLogoBlobDataVisa(HttpServletRequest request, HttpServletResponse response, EmployeeCommand command) {
        String visaId = "";
        try {
            visaId = request.getParameter("visaId");
            ArrayList profileList = new ArrayList();
            profileList = employeeBP.getDisplayLogoBlobDataVisa(visaId);
            if (profileList.size() > 0) {

                EmployeeTO empTO = (EmployeeTO) profileList.get(0);
                byte[] visaImg = empTO.getVisaImg();
                response.setContentType("image/jpeg");
                OutputStream out = response.getOutputStream();
                out.write(visaImg);
                out.close();
            }
        } catch (FPRuntimeException exception) {
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }
      public ModelAndView displayEmployeeImport(HttpServletRequest request, HttpServletResponse response, EmployeeCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/Login.jsp");
        }

        HttpSession session = request.getSession();
        String path = "";

        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        if (!loginBP.checkAuthorisation(userFunctions, "Employee-Add")) {
            path = "content/common/NotAuthorized.jsp";
        } else {
            String menuPath = "HRMS  >>  Manage Employee  >> Upload Employee details ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "Upload Employee";
            request.setAttribute("pageTitle", pageTitle);
           
            path = "content/employee/employeeImport.jsp";
        }
        return new ModelAndView(path);
    }

}
