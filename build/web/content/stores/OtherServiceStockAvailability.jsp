<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/parveencss/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ page import="ets.domain.mrs.business.MrsTO" %>


<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.MRS"  text="MRS"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.stores"  text="Stores"/></a></li>
            <li class="active"><spring:message code="stores.label.MRS"  text="MRS"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
<!--                <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                    <tr>
                        <td >-->
                            <%--<%@ include file="/content/common/path.jsp" %>--%>
                            <%@ include file="/content/common/message.jsp" %>
                        <!--</td></tr></table>-->
                <!-- pointer table -->
                <!-- title table -->
                <!--<table class="table table-info mb30 table-hover">-->
<!--                    <tr>
                        <td >
                            <%@ include file="/content/common/message.jsp" %>
                        </td></tr></table>-->
                <table class="table table-info mb30 table-hover" id="bg" >
                    <thead>
                    <tr>
                        <th  height="30"><div ><spring:message code="stores.label.MFRItemcode"  text="MRS"/></div></th>
                        <th  height="30"><div ><spring:message code="stores.label.PAPLCode"  text="MRS"/></div></th>
                        <th  height="30"><spring:message code="stores.label.ItemName"  text="MRS"/></th>
                        <th  height="30"><div ><spring:message code="stores.label.ServicePoint"  text="MRS"/></div></th>
                        <th  height="30"><div ><spring:message code="stores.label.StkAvailbty"  text="MRS"/></div></th>
                        <th  height="30"><div ><spring:message code="stores.label.RCQty"  text="MRS"/></div></th>
                    </tr>
                            </thead>
                    <% int index=0; %>
                    <c:if test = "${servicePointItemLists != null}" >
                        <c:forEach items="${servicePointItemLists}" var="item"> 
                            <%
                                String classText = "";
                                int oddEven = index % 2;
                                if (oddEven > 0) {
                                classText = "text2";
                                } else {
                                classText = "text1";
                                }
                            %>
                            <tr>
                                <td class="<%=classText %>" height="30"><c:out value="${item.mrsItemMfrCode}"/></td>
                                <td class="<%=classText %>" height="30"><c:out value="${item.mrsPaplCode}"/></td>
                                <td class="<%=classText %>" height="30"><c:out value="${item.mrsItemName}"/></td>
                                <td class="<%=classText %>" height="30"><c:out value="${item.mrsCompanyName}"/></td>
                                <td class="<%=classText %>" height="30"><c:out value="${item.newStock}"/></td>
                                <td class="<%=classText %>" height="30"><c:out value="${item.rcStock}"/></td>
                            </tr>
                            <%
                              index++;
                            %>
                        </c:forEach>
                    </c:if> 

                </table>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
