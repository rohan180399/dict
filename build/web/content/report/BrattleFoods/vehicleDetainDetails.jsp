
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        // alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>
<script type="text/javascript">
    function submitPage(value) {
        if (document.getElementById('fromDate').value == '') {
            alert("please select From Date");
            document.getElementById('fromDate').focus();
        }
        if (document.getElementById('toDate').value == '') {
            alert("please select To Date");
            document.getElementById('toDate').focus();
        }
        else {
            if (value == 'ExportExcel') {
                document.accountReceivable.action = '/throttle/vehicleDetainDetailsReport.do?param=ExportExcel';
                document.accountReceivable.submit();
            } else {
                document.accountReceivable.action = '/throttle/vehicleDetainDetailsReport.do?param=Search';
                document.accountReceivable.submit();
            }
        }
    }

    function setValue() {
        if ('<%=request.getAttribute("page")%>' != 'null') {
            var page = '<%=request.getAttribute("page")%>';
            if (page == 1) {
                submitPage('search');
            }
        }
    }


    function viewCustomerProfitDetails(tripIds) {
        //alert(tripIds);
        window.open('/throttle/viewCustomerWiseProfitDetails.do?tripId=' + tripIds + "&param=Search", 'PopupPage', 'height = 500, width = 1150, scrollbars = yes, resizable = yes');
    }
</script>

<style type="text/css">





    .container {width: 960px; margin: 0 auto; overflow: hidden;}
    .content {width:800px; margin:0 auto; padding-top:50px;}
    .contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

    /* STOP ANIMATION */



    /* Second Loadin Circle */

    .circle1 {
        background-color: rgba(0,0,0,0);
        border:5px solid rgba(100,183,229,0.9);
        opacity:.9;
        border-left:5px solid rgba(0,0,0,0);
        /*	border-right:5px solid rgba(0,0,0,0);*/
        border-radius:50px;
        /*box-shadow: 0 0 15px #2187e7; */
        /*	box-shadow: 0 0 15px blue;*/
        width:40px;
        height:40px;
        margin:0 auto;
        position:relative;
        top:-50px;
        -moz-animation:spinoffPulse 1s infinite linear;
        -webkit-animation:spinoffPulse 1s infinite linear;
        -ms-animation:spinoffPulse 1s infinite linear;
        -o-animation:spinoffPulse 1s infinite linear;
    }

    @-moz-keyframes spinoffPulse {
        0% { -moz-transform:rotate(0deg); }
        100% { -moz-transform:rotate(360deg);  }
    }
    @-webkit-keyframes spinoffPulse {
        0% { -webkit-transform:rotate(0deg); }
        100% { -webkit-transform:rotate(360deg);  }
    }
    @-ms-keyframes spinoffPulse {
        0% { -ms-transform:rotate(0deg); }
        100% { -ms-transform:rotate(360deg);  }
    }
    @-o-keyframes spinoffPulse {
        0% { -o-transform:rotate(0deg); }
        100% { -o-transform:rotate(360deg);  }
    }



</style>
</head><div class="pageheader">
    <h2><i class="fa fa-edit"></i> Report</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Report</a></li>
            <li class="active">Container Movement Report</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="setValue();
            sorter.size(10);">
                <form name="accountReceivable" action=""  method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>
                                        <table class="table table-info mb30 table-hover" style="width:100%">
                                    <tr height="30"   ><td colspan="4" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">Container Movement Report</td></tr>

                                            <tr>
                                                <td  height="30"><font color="red">*</font>From Date :</td>
                                                <td  height="30"><input type="text" name="fromDate" id="fromDate" class="datepicker" value="<c:out value="${fromDate}"/>" style="width:240px;height:40px;"/>  </td>
                                                <td  height="30"><font color="red">*</font>To Date : </td>
                                                <td  height="30"><input type="text" name="toDate" id="toDate" class="datepicker" value="<c:out value="${toDate}"/>" style="width:240px;height:40px;"/></td>
                                            </tr>
                                            <script>
                                        document.getElementById("monthId").value = "<c:out value="${monthId}"/>";
                                            </script>
                                            <tr>
                                                <td colspan="4" align="center"><input type="button" class="btn btn-info" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);">
                                            <input type="button" class="btn btn-info" name="Search"   value="Search" onclick="submitPage(this.name);"></td>
                                            </tr>
                                        </table>

                    <table  id="table" class="table table-info mb30" style="width:1000px;" >
                    <thead height="30">
                        <tr id="tableDesingTH" height="30">
                                <th>Transporter</th>
                        <th>Vehicle No</th>
                        <th><center>No.of.Trips</center></th>
                        <th><center>BD</center></th>
                        <th><center>DTN</center></th>
                        <th><center>IDL</center></th>
                        <th>OT</th>
                        <th>Total Days</th>
                        </tr>
                        </thead>
                        <tbody>
                            <c:set var="OnTrip" value="0"/>
                            <c:set var="TotalTrips" value="0"/>
                            <c:set var="TotalBD" value="0"/>
                            <c:set var="TotalDTN" value="0"/>
                            <c:set var="TotalIDL" value="0"/>
                            <c:set var="TotalOT" value="0"/>

                            <% int index = 1;%>
                            <%
                                String classText = "";
                                int oddEven = index % 2;
                                if (oddEven > 0) {
                                    classText = "text2";
                                } else {
                                    classText = "text1";
                                }
                            %>
                            <c:set var="vendorName" value=""/>  
                            <c:forEach items="${VehicleDetainDetails}" var="Vehicle">

                                <c:if test = "${vendorName != ''}" >
                                    <c:if test = "${vendorName !=Vehicle.vendorName}" >

                                        <tr height="30">
                                            <td align="center"><b><c:out value="${vendorName}"/> Total()</b></td>
                                            <td align="center"></td>
                                            <td align="center"><b><c:out value="${TotalTrips}"/></b></td>
                                            <td align="center"><b><c:out value="${TotalBD}"/></b></td>
                                            <td align="center"><b><c:out value="${TotalDTN}"/></b></td>
                                            <td align="center"><b><c:out value="${TotalIDL}"/></b></td>
                                            <td align="center"><b><c:out value="${TotalOT}"/></b></td>
                                            <td align="center"></td>
                                        </tr>
                                        <c:set var="TotalTrips" value="${0}"/>
                                        <c:set var="TotalBD" value="${0}"/>
                                        <c:set var="TotalDTN" value="${0}"/>
                                        <c:set var="TotalIDL" value="${0}"/>
                                        <c:set var="TotalOT" value="${0}"/>  
                                    </c:if>
                                </c:if>
                                <tr height="30">
                                    <c:set var="vendorName" value="${Vehicle.vendorName}"/>
                                    <c:set var="OnTrip" value="${Vehicle.totalDays - Vehicle.tripDays}"/>
                                    <c:set var="TotalTrips" value="${TotalTrips + Vehicle.noOfTrips}"/>
                                    <c:set var="TotalBD" value="${TotalBD + 0}"/>
                                    <c:set var="TotalDTN" value="${TotalDTN + 0}"/>
                                    <c:set var="TotalIDL" value="${TotalIDL + OnTrip}"/>
                                    <c:set var="TotalOT" value="${TotalOT + Vehicle.tripDays}"/>
                                    <td align="center" class="<%=classText%>"><c:out value="${Vehicle.vendorName}"/></td>
                                    <td align="center" class="<%=classText%>"><c:out value="${Vehicle.regNo}"/></td>
                                    <td align="center" class="<%=classText%>"><c:out value="${Vehicle.noOfTrips}"/></td>
                                    <td align="center" class="<%=classText%>"><c:out value="${0}"/></td>
                                    <td align="center" class="<%=classText%>"><c:out value="${0}"/></td>
                                    <td align="center" class="<%=classText%>"><c:out value="${OnTrip}"/></td>
                                    <td align="center" class="<%=classText%>"><c:out value="${Vehicle.tripDays}"/></td>
                                    <td align="center" class="<%=classText%>"><c:out value="${Vehicle.totalDays}"/></td>

                                </tr>

                            </c:forEach>
                            <tr height="30">
                                <td align="center"><b><c:out value="${vendorName}"/> Total()</b></td>
                                <td align="center"></td>
                                <td align="center"><b><c:out value="${TotalTrips}"/></b></td>
                                <td align="center"><b><c:out value="${TotalBD}"/></b></td>
                                <td align="center"><b><c:out value="${TotalDTN}"/></b></td>
                                <td align="center"><b><c:out value="${TotalIDL}"/></b></td>
                                <td align="center"><b><c:out value="${TotalOT}"/></b></td>
                                <td align="center"></td>
                            </tr>
                        </tbody>
                    </table>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>    
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
