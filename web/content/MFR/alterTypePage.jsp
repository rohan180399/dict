
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="ets.domain.vehicle.business.VehicleTO" %>


<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script language="javascript">

    function submitpage(value)
    {


        var checValidate = selectedItemValidation();
        var splt = checValidate.split("-");
        if (splt[0] == 'SubmitForm' && splt[1] != 0) {


            document.modify.action = '/throttle/modifyType.do';
            document.modify.submit();
        }
    }

    function setSelectbox(i)
    {
        var selected = document.getElementsByName("selectedIndex");
        selected[i].checked = 1;
    }

    function selectedItemValidation() {
        var index = document.getElementsByName("selectedIndex");
        var mfrNames = document.getElementsByName("typeNames");
        var desc = document.getElementsByName("descriptions");
        var chec = 0;

        for (var i = 0; (i < index.length && index.length != 0); i++) {
            if (index[i].checked) {
                chec++;
                if (textValidation(mfrNames[i], "Manufacturer Name")) {
                    return;
                } else if (textValidation(desc[i], "Desription")) {
                    return;
                }
            }
        }
        if (chec == 0) {
            alert("Please Select Any One And Then Proceed");
            desigName[0].focus();
//break;
        }
        document.modify.action = '/throttle/modifyType.do';
        document.modify.submit();
    }

    function isChar(s) {
        if (!(/^-?\d+$/.test(s))) {
            return false;
        }
        return true;
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="trucks.label.VehicleType"  text="VehicleType"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.Vehicle"  text="Vehicle"/></a></li>
            <li class="active"><spring:message code="trucks.label.VehicleType"  text="VehicleType"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">

            <body onload="document.modify.typeNames[0].focus();">
                <form method="post" name="modify"> 

                    <%@ include file="/content/common/message.jsp" %>
                    <% int index = 0; %>        

                    <table class="table table-info mb30 table-hover" >
                        <thead>
                            <c:if test = "${TypeList != null}" >

                                <tr>
                                    <th ><div ></div></th>
                            <th ><div ><spring:message code="trucks.label.SNo"  text="default text"/></div></th>
                            <th ><div ><spring:message code="trucks.label.VehicleType"  text="default text"/></div> </th>
                            <th ><div ><spring:message code="trucks.label.Tonnage"  text="default text"/></div> </th>
                            <th ><div ><spring:message code="trucks.label.VehicleCapacity"  text="default text"/></div> </th>
                            <th ><div ><spring:message code="trucks.label.Description"  text="default text"/></div></th>
                            <th ><div ><spring:message code="trucks.label.Status"  text="default text"/></div></th>
                            <th ><div ><spring:message code="trucks.label.Select"  text="default text"/></div></th>
                            </tr>
                            </thead>
                        </c:if>
                        <c:if test = "${TypeList != null}" >
                            <c:forEach items="${TypeList}" var="type"> 		
                                <%

                                String classText = "";
                                int oddEven = index % 2;
                                if (oddEven > 0) {
                                classText = "text2";
                                } else {
                                classText = "text1";
                                }
                                %>
                                <tr>
                                    <td><input type="hidden" name="typeIds" value='<c:out value="${type.typeId}"/>'>  </td>
                                    <td><%=index + 1%></td>
                                    <td><input type="text" class="form-control" name="typeNames" value="<c:out value="${type.typeName}"/>" onchange="setSelectbox(<%= index %>)"></td>
                                    <td><input type="text" class="form-control" name="tonnages" value="<c:out value="${type.tonnage}"/>" onchange="setSelectbox(<%= index %>)"></td>
                                    <td><input type="text" class="form-control" name="capacities" value="<c:out value="${type.capacity}"/>" onchange="setSelectbox(<%= index %>)"></td>
                                    <td><input type="text" class="form-control" name="descriptions" value="<c:out value="${type.description}"/>" onchange="setSelectbox(<%= index %>)"></td>
                                    <td> <div align="center"><select name="activeInds" class="form-control" onchange="setSelectbox(<%= index %>)">
                                                <c:choose>
                                                    <c:when test="${type.activeInd == 'Y'}">
                                                        <option value="Y" selected><spring:message code="trucks.label.Active"  text="default text"/></option>
                                                        <option value="N"><spring:message code="trucks.label.InActive"  text="default text"/></option>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <option value="Y"><spring:message code="trucks.label.Active"  text="default text"/></option>
                                                        <option value="N" selected><spring:message code="trucks.label.InActive"  text="default text"/></option>
                                                    </c:otherwise>
                                                </c:choose>
                                            </select>
                                        </div> 
                                    </td>
                                    <td width="77"><input type="checkbox" name="selectedIndex" value='<%= index %>'></td>
                                </tr>
                                <%
                                index++;
                                %>
                            </c:forEach >
                        </c:if> 
                    </table>
                    <center>
                        <br>
                        <input type="button" name="save" value="<spring:message code="trucks.label.Save"  text="default text"/>" onClick="submitpage(this.name)" class="btn btn-success" />
                        <input type="hidden" name="reqfor" value="designa" />
                    </center>
                    <br>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>