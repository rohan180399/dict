<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $( "#datepicker" ).datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $( ".datepicker" ).datepicker({

            /*altField: "#alternate",
                        altFormat: "DD, d MM, yy"*/
            changeMonth: true,changeYear: true
        });

    });
</script>

    </head>
    <script language="javascript">
        function submitPage(){
            document.approve.action = '/throttle/saveTripClosureapprove.do';
            document.approve.submit();
        }
        function setFocus(){
            document.approve.approveamt.focus();
        }
    </script>


    <body onload="setFocus();">
        <form name="approve"  method="post" >
            <%
            request.setAttribute("menuPath"," Trip Closure >> Approval");
            String tripid = request.getParameter("tripid");
            %>
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <c:if test = "${viewTripClosureRequest != null}" >
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="50%" id="bg" class="border">
                <tr align="center">
                    <td colspan="2" align="center" class="contenthead" height="30">
                        <div class="contenthead">Trip Closure Approval</div></td>
                </tr>
                <c:forEach items="${viewTripClosureRequest}" var="fd">
                <tr>
                    <td class="text2" height="30">Cnote Name</td>
                    <td class="text2" height="30"><c:out value="${fd.cnoteName}"/>
                        <input type="hidden" name="cnote" value="<c:out value="${fd.cnoteName}"/>"/>
                    </td>
                </tr>
                <tr>
                    <td class="text1" height="30">Vehicle Type</td>
                    <td class="text1" height="30"><c:out value="${fd.vehicleTypeName}"/></td>
                </tr>
                <tr>
                    <td class="text2" height="30">Vehicle No</td>
                    <td class="text2" height="30"><c:out value="${fd.regNo}"/>
                        <input type="hidden" name="vehicleno" value="<c:out value="${fd.regNo}"/>"/>
                    </td>
                </tr>
                <tr>
                    <td class="text1" height="30">Route Name</td>
                    <td class="text1" height="30"><c:out value="${fd.routeName}"/>
                        <input type="hidden" name="routename" value="<c:out value="${fd.routeName}"/>"/>
                    </td>
                </tr>
                <tr>
                    <td class="text2" height="30">Driver Name</td>
                    <td class="text2" height="30"><c:out value="${fd.driverName}"/></td>
                </tr>
                <tr>
                    <td class="text1" height="30">Planned Date</td>
                    <td class="text1" height="30"><c:out value="${fd.planneddate}"/></td>
                </tr>
                
                <tr>
                    <td class="text2" height="30">Actual Advance Paid</td>
                    <td class="text2" height="30"><c:out value="${fd.actualadvancepaid}"/></td>
                </tr>

                <c:if test="${fd.requesttype==1}">
                <tr>
                    <td class="text1" height="30">Request Type</td>
                    <td class="text1" height="30">Using Odometer</td>
                </tr>
                <tr>
                    <td class="text2" height="30">Total Run Km</td>
                    <td class="text2" height="30"><c:out value="${fd.totalrunkm}"/></td>
                </tr>
                <tr>
                    <td class="text1" height="30">Total Run Hm</td>
                    <td class="text1" height="30"><c:out value="${fd.totalrunhm}"/></td>
                </tr>
                </c:if>
                <c:if test="${fd.requesttype==2}">
                <tr>
                    <td class="text1" height="30">Request Type</td>
                    <td class="text1" height="30">Expense Deviation</td>
                </tr>
                <tr>
                    <td class="text2" height="30">RCM Expense</td>
                    <td class="text2" height="30"><c:out value="${fd.rcmexpense}"/></td>
                </tr>
                <tr>
                    <td class="text1" height="30">System Expense</td>
                    <td class="text1" height="30"><c:out value="${fd.systemexpense}"/></td>
                </tr>
                </c:if>



                <input type="hidden" name="tripid" value="<%=tripid%>"/>
                <input type="hidden" name="tripclosureid" value="<c:out value="${fd.tripclosureid}"/>"/>

                <tr>
                    <td class="text2" height="30">Request On</td>
                    <td class="text2" height="30">
                        <input name="requeston" class="textbox" type="text" value='<c:out value="${fd.requeston}"/>' readonly></td>
                </tr>

                <tr>
                    <td class="text1" height="30"><font color="red">*</font>Status</td>
                    <td class="text1" height="30"><select name="approvestatus">
                            <option value="" >-Select Any One-</option>
                            <option value="1">Approved</option>
                            <option value="2">Rejected</option>
                </select></td>
                </tr>


                <tr>
                    <td class="text1" height="30"><font color="red">*</font>Approve Remarks</td>
                    <td class="text1" height="30"><textarea class="textbox" name="approveremarks"></textarea></td>
                </tr>
</c:forEach>
            </table>
                </c:if>
            <br>
            <center>
                <input type="button" value="Save" class="button" onClick="submitPage();">
<!--                &emsp;<input type="reset" class="button" value="Clear">-->
            </center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
