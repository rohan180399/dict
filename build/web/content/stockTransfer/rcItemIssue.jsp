
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
    <%@ page import="ets.domain.stockTransfer.business.StockTransferTO" %> 
    
    <title>MRSList</title>
</head>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script language="javascript">
    
    function submitPage(value)
    {

        var flag=document.stockTransfer.flag.value;
        if(flag==1)
            {            
       document.stockTransfer.action='/throttle/transferRc.do';
       document.stockTransfer.submit();
            }
            else{
                alert("Selected Quantity is Greater than Approved Quantity");
                document.stockTransfer.flag.value=0;
                return false;
            }
       //window.close();     
    }
    
    function setSelectbox(i)
    {
        var selected=document.getElementsByName("selectedIndex") ;
        selected[i].checked=1;
    }
     
    
        function getMultipleOptions(optionName,appendName,index )
        {
        var temp = "";
        var test = document.getElementsByName(optionName);//stockTransfer.rcItemIds.value;    
        var appendNam = document.getElementsByName(appendName);        
        for (var i = 0; i < test[index].options.length; i++){                
                if (test[index].options[i].selected ){
                        temp = temp + "~" + test[index].options[ i ].value;
                }
        }
        appendNam[index].value = temp;        
        }    
     function backPage(){
        document.stockTransfer.action='/throttle/issueItemPage.do';
        document.stockTransfer.submit();
       }
   

function test()
{
    var test = document.stockTransfer.rcItemIds.value;
     
    
}    
   
function setParentWindowValues()
{
    var rcItemQty = self.opener.document.getElementsByName("rcQtys");    
    var approvedQtys = document.stockTransfer.approvedQtys.value;    
    var selectedInd = document.getElementsByName('selectedIndex');
    var qty=0;
    for(var i=0;i<selectedInd.length;i++){
        if(selectedInd[i].checked ==true){
            qty++;
        }            
    }
    var qqty = parseFloat(qty);       
    if(qqty > parseFloat(approvedQtys))
        {
         alert("False");
            document.stockTransfer.flag.value= 0;
        }
        else
            {
           document.stockTransfer.flag.value= 1;
            }
            
         
alert(document.stockTransfer.approved.value);
    rcItemQty[parseInt(document.stockTransfer.rowIndex.value)].value = qty;
}


   
   
    </script>

<form method="post"  name="stockTransfer">  
<body>
<%@ include file="/content/common/path.jsp" %>            

        
<%@ include file="/content/common/message.jsp" %>    

<% int index = 0;%>          
<c:if test = "${ApprovedStockListAll != null}" >  
 
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="550" id="bg" class="border">
        <tr>
            <td colspan="9" height="80" align="center" class="contenthead" ><div class="contenthead">Issue Item</div></td>            
        </tr>
        
        <input type="hidden" name="approvedQtys" value=<%= request.getAttribute("approvedQtys") %>  >
        <input type="hidden" name="rowIndex" value=<%= request.getAttribute("rowIndex") %>  >
        <input type="hidden" name="flag"   >
        <c:forEach items="${ApprovedStockListAll}" var="mpr">  
            <% if (index == 0) {%>          
            <input type="hidden" name="requestId" value=<c:out value="${mpr.requestId}"/> >    
                   <tr>
            <td class="text1" height="30">REQUEST ID</td>
            <td class="text1" height="30"> <c:out value="${mpr.requestId}"/> </td>
            </tr>  
            <tr>
                <td class="text2" height="30">SERVICE POINT</td>
                <td class="text2" height="30"> <c:out value="${mpr.servicePointName}"/> </td>
            </tr>
            <input type="hidden" name="approvedId" value="<c:out value="${mpr.approvedId}"/>">       
            <% index++;
            }%>                
        </c:forEach>
       
    </table>
    <br>
    <br>   
     <c:if test = "${rcItemList != null}" > 
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="750" id="bg">              
        <%index = 0;
            String classText = "";
            int oddEven = 0;
        %>
         
        <c:if test = "${ApprovedStockListAll != null}" >
           
            <tr>
                <td colspan="9" align="center" class="contenthead" height="30">Approve Items</td>
            </tr>  
            <tr>
                <td class="text2" height="30"><b>Mfr Code</b></td>
                <td class="text2" height="30"><b>Papl Code</b></td>                       
                <td class="text2" height="30"><b>Item Name</b></td>
                <td class="text2" height="30"><b>Uom</b></td>                                                                                  
                 <td class="text2" height="30"><b>RcItemId</b></td>
                 <td class="text2" height="30"><b>RcItemPrice</b></td>
                 <td class="text2" height="30"><b>Issue</b></td> 
            </tr>        
            
            <c:forEach items="${ApprovedStockListAll}" var="Asl">             
            <c:forEach items="${rcItemList}" var="Ril"> 
            <c:if test="${Asl.itemId == Ril.itemId}" >                                             
            
                <%

            oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                %>
                <tr>
                    <td class="<%=classText %>" height="30"><c:out value="${Asl.mfrCode}"/></td>
                    <td class="<%=classText %>" height="30"><c:out value="${Asl.paplCode}"/></td>                       
                    <td class="<%=classText %>" height="30"><input type="hidden" name=itemIds value="<c:out value="${Asl.itemId}"/>"><c:out value="${Asl.itemName}"/></td>
                    <td class="<%=classText %>" height="30"><c:out value="${Asl.uomName}"/></td>                                       
                                                              
                        <td class=" <%=classText %>"><input type="text" name="rcItemIds" size="5" readonly value="<c:out value="${Ril.rcItemId}"/>">
                         <td class=" <%=classText %>"><input type="text" name="prices" size="5" readonly value="<c:out value="${Ril.price}"/>">
                                                                                                                           	                                                                               	
                   </td>                           
                    <td width="77" height="30" class="<%=classText %>"><input type="checkbox" name="selectedIndex" value='<%= index %>' onClick="setParentWindowValues();" ></td>
                </tr>
                <%
            index++;
                %>
                
            </c:if>       
            </c:forEach>
            </c:forEach >   
            
        </c:if>                  
    </table> 
 
    <br>
        <br>
   
    <center>   
        <input type="button" class="button" name="Add" value="Issue" onClick="submitPage(this.name);" >   
         </center>
         </c:if>  
    
</c:if>    
</body>              
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</html>

