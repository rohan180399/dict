<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $("#tabs").tabs();
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {                
                $( ".datepicker" ).datepicker({                    
                    changeMonth: true,changeYear: true
                });
            });
        </script>
        <script type="text/javascript" language="javascript">
            var poItems = 0;
            var rowCount='';
            var sno='';
            var snumber = '';            
            function setDefaultValue(){
                //alert("i'm here");

                var sdate='<%=request.getAttribute("fromDate")%>';
                var edate='<%=request.getAttribute("toDate")%>';
                var regno='<%=request.getAttribute("regNo")%>';
                var driName='<%=request.getAttribute("driName")%>';
                
                if(sdate!='null' && edate!='null'){
                    document.settle.fromDate.value=sdate;
                    document.settle.toDate.value=edate;
                    document.settle.toDate.value=edate;
                    document.getElementById("tabs").style.display='block';
                } if(regno!='null'){
                    document.settle.regno.value=regno;
                }
                if(driName!='null'){
                    document.settle.driName.value=driName;
                }

                /*document.settle.suDriverName.value = '<%= request.getAttribute("driName")%>';
                document.settle.suVehicleNo.value = '<%= request.getAttribute("regNo")%>';
                document.settle.suFromDate.value = '<%= request.getAttribute("fromDate")%>';
                document.settle.suToDate.value = '<%= request.getAttribute("toDate")%>';               
                
                document.settle.startKM.value = '<%= request.getAttribute("outKm")%>';
                document.settle.endKM.value = '<%= request.getAttribute("inKm")%>';
                document.settle.totalDiesel.value = '<%= request.getAttribute("totFuel")%>';
                document.settle.driverExpense.value = '<%= request.getAttribute("driExpense")%>';
                
                document.settle.expenses.value = '<%= request.getAttribute("genExpense")%>';
                document.settle.driverAdvance.value = '<%= request.getAttribute("driAdvance")%>';
                document.settle.runningKM.value = (parseInt(document.settle.endKM.value) - parseInt(document.settle.startKM.value));
                document.settle.totalTonAmount.value = '<%= request.getAttribute("totalTonnage")%>';
                document.settle.income.value = '<%= request.getAttribute("totalTonnage")%>';
                document.settle.balance.value = (parseFloat(document.settle.income.value) - parseFloat(document.settle.expenses.value));
                document.settle.suTotalTon.value = '<%= request.getAttribute("totalTonnage")%>';                
                document.settle.driverBataSalary.value = '<%= request.getAttribute("driBata")%>';                

                var tripNos = parseInt(document.settle.runningKM.value) / 150;

                var oneDay = 24*60*60*1000;
                var d = '<%= request.getAttribute("fromDate")%>';
                var temp1 = d.split("-");
                var firstDate = new Date(temp1[2],temp1[1],temp1[0]);
                
                var dd = '<%= request.getAttribute("toDate")%>';
                var temp2 = dd.split("-");
                var secondDate = new Date(temp2[2],temp2[1],temp2[0]);
                
                var days = Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay));
                if(days==0){
                   days=1;
                }
                document.settle.totalDays.value = parseInt(days);
                document.settle.totalTrips.value = parseInt(tripNos);
                //var tCount = '<%= request.getAttribute("tripCount")%>';
                if(tripNos > days){                    
                    document.settle.totalTripAmount.value = parseFloat(tripNos * 10000);
                    document.settle.fiveNoOfTrips.value = parseFloat(tripNos * 5000);
                    document.settle.perDayIncome.value = (parseInt(document.settle.income.value) / parseInt(tripNos));
                    document.settle.driverSalary.value = (parseInt(tripNos)*100);
                    document.settle.cleanerSalary.value = (parseInt(tripNos)*40);

                } else{                    
                    document.settle.totalTripAmount.value = parseFloat(days * 10000);
                    document.settle.fiveNoOfTrips.value = parseFloat(days * 5000);
                    document.settle.perDayIncome.value = (parseInt(document.settle.income.value) / parseInt(days));
                    document.settle.driverSalary.value = (parseInt(days)*100);
                    document.settle.cleanerSalary.value = (parseInt(days)*40);
                }
                document.settle.shortage.value = (parseInt(document.settle.totalTripAmount.value) - parseInt(document.settle.totalTonAmount.value));
                document.settle.shortage1.value = (parseInt(document.settle.fiveNoOfTrips.value) - parseInt(document.settle.balance.value));
                document.settle.totalExpenses.value = (parseInt(document.settle.driverExpense.value) + parseInt(document.settle.driverSalary.value)+parseInt(document.settle.cleanerSalary.value));
                document.settle.driverExpenses.value = (parseInt(document.settle.driverExpense.value) + parseInt(document.settle.driverSalary.value)+parseInt(document.settle.cleanerSalary.value));
                document.settle.kmpl.value = (parseInt(document.settle.totalDiesel.value) / parseInt(document.settle.runningKM.value));*/
            }
            function calcTotalExpenses(obj){
                if(obj.value!=""){
                    document.settle.totalExpenses.value = (parseFloat(document.settle.totalExpenses.value) + parseFloat(obj.value));
                    document.settle.driverExpenses.value = (parseFloat(document.settle.driverExpenses.value) + parseFloat(obj.value));
                }
            }
            function submitPage(obj){
                //if(obj.name=="search"){                    
                    //if((!(isEmpty(fromDate) && isEmpty(toDate)) || !isEmpty(driName) )) {
                    //if((!(isEmpty(fromDate) && isEmpty(toDate)))) {      
                        document.settle.buttonName.value = "search";
                        obj.name="none";
                        document.settle.action="/throttle/statusDriverSettlement.do";
                        document.settle.submit();
                    /*}else {
                        alert("Please enter any one value ");
                    }*/
                //}
                
            }
        </script>
    </head>

    <body onload="setDefaultValue();">      
        <form name="settle" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <!-- pointer table -->
            <!-- message table -->
            <%@ include file="/content/common/message.jsp"%>            

            <table width="75%" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr>
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <b>Driver Settlement</b>
                            <div id="first">
                                <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4">
                                    <tr>
                                        <td><font color="red">*</font>Driver Name</td>
                                        <td height="30">
                                            <input name="driName" id="driName" type="text" class="form-control" size="20" value="">
                                        </td>
                                        <td><!--Vehicle No--></td>
                                        <td><input name="regno" id="regno" type="hidden" class="form-control" size="20" value=""></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="30"><font color="red">*</font>From Date</td>
                                        <td height="30"><input type="text" name="fromDate" class="form-control" ><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.settle.fromDate,'dd-mm-yyyy',this)"/></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td><input type="text" name="toDate" class="form-control" ><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.settle.toDate,'dd-mm-yyyy',this)"/></td>
                                        <td><input type="button" class="button" name="search"  onclick="submitPage(this);" value="Search"></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br/>
            
            <br/>
            <div id="tabs" style="display: none;">
                <ul>
                    <li><a href="#tripDetail"><span>Trip Details</span></a></li>
                    <li><a href="#expenseDetail"><span>Expenses Entry</span></a></li>
                    <li><a href="#advDetail"><span>Advance Details</span></a></li>
                    <li><a href="#fuelDetail"><span>Fuel Details</span></a></li>
                    <li><a href="#haltDetail"><span>Vehicle In/Out Details</span></a></li>
                    <li><a href="#Remarks"><span>Remarks</span></a></li>
                    <li><a href="#summary"><span>summary</span></a></li>
                </ul>
                <div id="tripDetail">                    
                    <c:if test = "${searchProDriverSettlement != null}" >
                        <%
                                    int index = 0;
                                    ArrayList searchProDriverSettlement = (ArrayList) request.getAttribute("searchProDriverSettlement");
                                    if (searchProDriverSettlement.size() != 0) {
                        %>
                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <!--<tr>
                                <td height="30" class="contenthead" colspan="11">Proceed to Driver Settlement</td>
                            </tr>-->
                            <tr>
                                <td class="contentsub"  height="30">Trip Id</td>
                                <td class="contentsub" height="30">Identity No</td>                                
                                <td class="contentsub" height="30">Route Name</td>
                                <td class="contentsub" height="30">OUT KM</td>
                                <td class="contentsub" height="30">Driver Name</td>
                                <td class="contentsub" height="30">OUT DateTime</td>
                                <td class="contentsub" height="30">IN KM</td>
                                <td class="contentsub" height="30">IN DateTime</td>
                                <td class="contentsub" height="30">Total Tonnage</td>
                                <td class="contentsub" height="30">Delivered Tonnage</td>
                                <td class="contentsub" height="30">Shortage</td>
                                <td class="contentsub" height="30">Trip Status</td>
                            </tr>
                            <c:forEach items="${searchProDriverSettlement}" var="pdriversettle">
                                <c:set var="total" value="${total+1}"></c:set>
                                <%
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text2";
                                    } else {
                                        classText = "text1";
                                    }
                                %>
                                <tr>
                                    <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.tripId}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.identityNo}"/></td>                                    
                                    <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.routeName}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.outKM}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.driverName}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.outDateTime}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.inKM}"/></td>                                    
                                    <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.inDateTime}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.totalTonnage}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.deliveredTonnage}"/></td>                                    
                                    <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.shortage}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${pdriversettle.status}"/></td>
                                </tr>
                                <%index++;%>
                            </c:forEach>
                        </table>
                        <%
                                    }
                        %>

                    </c:if>
                    <input name="check" type="hidden" value='0'>
                    <br/>
                    <br/>
                </div>
                <div id="expenseDetail" align="center">                    
                    <c:if test = "${fixedExpDetails != null}" >
                        <%
                                    int index5 = 0;
                                    ArrayList fixedExpDetails = (ArrayList) request.getAttribute("fixedExpDetails");
                                    if (fixedExpDetails.size() != 0) {
                        %>
                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <tr>
                                <td height="30" class="contenthead" colspan="11" align="center"><b>Fixed Expenses</b></td>
                            </tr>
                            <tr>
                                <td class="contentsub"  height="30">Trip Id</td>
                                <td class="contentsub" height="30">Trip Date</td>
                                <td class="contentsub" height="30">Amount</td>
                                <td class="contentsub" height="30">Remarks</td>
                                <td class="contentsub" height="30">Created Date</td>
                            </tr>
                            <c:forEach items="${fixedExpDetails}" var="fixedExp">
                                <c:set var="total5" value="${total5+1}"></c:set>
                                <%
                                                                    String classText = "";
                                                                    int oddEven = index5 % 2;
                                                                    if (oddEven > 0) {
                                                                        classText = "text2";
                                                                    } else {
                                                                        classText = "text1";
                                                                    }
                                %>
                                <tr>
                                    <td class="<%=classText%>"  height="30"><c:out value="${fixedExp.tripId}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${fixedExp.tripDate}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${fixedExp.amount}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${fixedExp.remarks}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${fixedExp.createdDate}"/></td>
                                </tr>
                                <%index5++;%>
                            </c:forEach>
                        </table>
                        <%
                                    }
                        %>

                    </c:if>
                    <br/>
                    <c:if test = "${driverExpDetails != null}" >
                        <%
                                    int index7 = 0;
                                    ArrayList driverExpDetails = (ArrayList) request.getAttribute("driverExpDetails");
                                    if (driverExpDetails.size() != 0) {
                        %>
                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <tr>
                                <td height="30" class="contenthead" colspan="11" align="center"><b>Driver Expenses</b></td>
                            </tr>
                            <tr>
                                <td class="contentsub"  height="30">Trip Id</td>
                                <td class="contentsub" height="30">Trip Date</td>
                                <td class="contentsub" height="30">Description</td>
                                <td class="contentsub" height="30">Amount</td>
                                <td class="contentsub" height="30">Remarks</td>
                                <td class="contentsub" height="30">Created Date</td>
                            </tr>
                            <c:forEach items="${driverExpDetails}" var="driverExp">
                                <c:set var="total7" value="${total7+1}"></c:set>
                                <%
                                                                    String classText = "";
                                                                    int oddEven = index7 % 2;
                                                                    if (oddEven > 0) {
                                                                        classText = "text2";
                                                                    } else {
                                                                        classText = "text1";
                                                                    }
                                %>
                                <tr>
                                    <td class="<%=classText%>"  height="30"><c:out value="${driverExp.tripId}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${driverExp.tripDate}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${driverExp.expensesDesc}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${driverExp.amount}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${driverExp.remarks}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${driverExp.createdDate}"/></td>
                                </tr>
                                <%index7++;%>
                            </c:forEach>
                        </table>
                        <%
                                    }
                        %>

                    </c:if>
                        <input type="hidden" name="buttonName" id="buttonName" />
                    <br/>
                </div>
                <div id="advDetail">
                    <c:if test = "${AdvDetails != null}" >
                        <%
                                    int index1 = 0;
                                    ArrayList AdvDetails = (ArrayList) request.getAttribute("AdvDetails");
                                    if (AdvDetails.size() != 0) {
                        %>
                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <!--<tr>
                                <td height="30" class="contenthead" colspan="11">Proceed to Driver Settlement</td>
                            </tr>-->
                            <tr>
                                <td class="contentsub"  height="30">Trip Id</td>
                                <td class="contentsub" height="30">Vehicle No</td>
                                <td class="contentsub" height="30">Issuer Name</td>
                                <td class="contentsub" height="30">Designation</td>
                                <td class="contentsub" height="30">Driver Name</td>
                                <td class="contentsub" height="30">Advance</td>
                                <td class="contentsub" height="30">Route Name</td>
                                <td class="contentsub" height="30">Date</td>
                                <td class="contentsub" height="30">Settlement Status</td>
                                <!--<td class="contentsub" height="30">Trip Status</td>-->
                            </tr>
                            <c:forEach items="${AdvDetails}" var="advDetail">
                                <c:set var="total1" value="${total1+1}"></c:set>
                                <%
                                                                    String classText = "";
                                                                    int oddEven = index1 % 2;
                                                                    if (oddEven > 0) {
                                                                        classText = "text2";
                                                                    } else {
                                                                        classText = "text1";
                                                                    }
                                %>
                                <tr>
                                    <td class="<%=classText%>"  height="30"><c:out value="${advDetail.tripId}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${advDetail.identityNo}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${advDetail.issuerName}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${advDetail.designation}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${advDetail.driverName}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${advDetail.amount}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${advDetail.routeName}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${advDetail.advDatetime}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${advDetail.settlementFlag}"/></td>

                                </tr>
                                <%index1++;%>
                            </c:forEach>
                        </table>
                        <%
                                    }
                        %>

                    </c:if>
                    <br/>
                    <br/>
                </div>
                <div id="fuelDetail">
                    <c:if test = "${fuelDetails != null}" >
                        <%
                                    int index2 = 0;
                                    ArrayList fuelDetails = (ArrayList) request.getAttribute("fuelDetails");
                                    if (fuelDetails.size() != 0) {
                        %>
                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <!--<tr>
                                <td height="30" class="contenthead" colspan="11">Proceed to Driver Settlement</td>
                            </tr>-->
                            <tr>
                                <td class="contentsub"  height="30">Trip Id</td>
                                <td class="contentsub" height="30">Vehicle No</td>
                                <td class="contentsub" height="30">Fuel Name</td>
                                <td class="contentsub" height="30">Bunk Name</td>
                                <td class="contentsub" height="30">Driver Name</td>
                                <td class="contentsub" height="30">Liters</td>
                                <td class="contentsub" height="30">Route Name</td>
                                <td class="contentsub" height="30">Date</td>
                                <!--<td class="contentsub" height="30">Settlement Status</td>
                                <td class="contentsub" height="30">Trip Status</td>-->
                            </tr>
                            <c:forEach items="${fuelDetails}" var="fuelDetail">
                                <c:set var="total2" value="${total2+1}"></c:set>
                                <%
                                                                    String classText = "";
                                                                    int oddEven = index2 % 2;
                                                                    if (oddEven > 0) {
                                                                        classText = "text2";
                                                                    } else {
                                                                        classText = "text1";
                                                                    }
                                %>
                                <tr>
                                    <td class="<%=classText%>"  height="30"><c:out value="${fuelDetail.tripId}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${fuelDetail.identityNo}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${fuelDetail.fuelName}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${fuelDetail.bunkName}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${fuelDetail.driverName}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${fuelDetail.liters}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${fuelDetail.routeName}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${fuelDetail.fuelDatetime}"/></td>

                                </tr>
                                <%index2++;%>
                            </c:forEach>
                        </table>
                        <%
                                    }
                        %>

                    </c:if>
                    <br/>
                    <br/>
                </div>
                <div id="haltDetail">
                    <c:if test = "${fuelDetails != null}" >
                        <%
                                    int index3 = 0;
                                    ArrayList haltDetails = (ArrayList) request.getAttribute("haltDetails");
                                    if (haltDetails.size() != 0) {
                        %>
                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <!--<tr>
                                <td height="30" class="contenthead" colspan="11">Proceed to Driver Settlement</td>
                            </tr>-->
                            <tr>
                                <td class="contentsub"  height="30">Trip Id</td>
                                <td class="contentsub" height="30">Vehicle No</td>
                                <td class="contentsub" height="30">In Out Indication</td>
                                <td class="contentsub" height="30">In Out Date Time</td>
                                <td class="contentsub" height="30">Location Name</td>
                                <td class="contentsub" height="30">Driver Name</td>
                                <td class="contentsub" height="30">Route Name</td>
                                <!--<td class="contentsub" height="30">Settlement Status</td>
                                <td class="contentsub" height="30">Trip Status</td>-->
                            </tr>
                            <c:forEach items="${haltDetails}" var="haltDetail">
                                <c:set var="total3" value="${total3+1}"></c:set>
                                <%
                                                                    String classText = "";
                                                                    int oddEven = index3 % 2;
                                                                    if (oddEven > 0) {
                                                                        classText = "text2";
                                                                    } else {
                                                                        classText = "text1";
                                                                    }
                                %>
                                <tr>
                                    <td class="<%=classText%>"  height="30"><c:out value="${haltDetail.tripId}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${haltDetail.identityNo}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${haltDetail.inOutIndication}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${haltDetail.inOutDateTime}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${haltDetail.locationName}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${haltDetail.driverName}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${haltDetail.routeName}"/></td>
                                </tr>
                                <%index3++;%>
                            </c:forEach>
                        </table>
                        <%
                                    }
                        %>

                    </c:if>
                    <br/>
                    <br/>
                </div>
                <div id="Remarks">
                    <c:choose>
                      <c:when test = "${remarkDetails != null}">
                            <%
                                    int index6 = 0;
                                    ArrayList remarkDetails = (ArrayList) request.getAttribute("remarkDetails");
                                    if (remarkDetails.size() != 0) {
                        %>
                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <!--<tr>
                                <td height="30" class="contenthead" colspan="11">Proceed to Driver Settlement</td>
                            </tr>-->
                            <tr>
                                <td class="contentsub"  height="30">Driver Name</td>
                                <td class="contentsub" height="30">Vehicle No</td>
                                <td class="contentsub" height="30">Embarkment Date</td>
                                <td class="contentsub" height="30">Alighting Date</td>
                                <td class="contentsub" height="30">Alighting Status</td>
                                <td class="contentsub" height="30">Remarks</td>
                                <td class="contentsub" height="30">Remark Date</td>
                                <!--<td class="contentsub" height="30">Settlement Status</td>
                                <td class="contentsub" height="30">Trip Status</td>-->
                            </tr>
                            <c:forEach items="${remarkDetails}" var="remarkDetail">
                                <c:set var="total6" value="${total6+1}"></c:set>
                                <%
                                                                    String classText = "";
                                                                    int oddEven = index6 % 2;
                                                                    if (oddEven > 0) {
                                                                        classText = "text2";
                                                                    } else {
                                                                        classText = "text1";
                                                                    }
                                %>
                                <tr>
                                    <td class="<%=classText%>"  height="30"><c:out value="${remarkDetail.empName}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${remarkDetail.vehicleNo}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${remarkDetail.embarkDate}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${remarkDetail.alightDate}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${remarkDetail.alightStatus}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${remarkDetail.remarks}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${remarkDetail.createdOn}"/></td>
                                </tr>
                                <%index6++;%>
                            </c:forEach>
                        </table>
                        <%
                                    }
                        %>
                      </c:when>                      
                      <c:when test = "${remarkDetails == null}">
                          <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0">
                              <tr>
                                  <td>
                                        No Remarks Found...
                                  </td>
                              </tr>
                          </table>
                      </c:when>
                    </c:choose>                    
                    <br/>                    
                </div>
                <div id="summary" align="center">

                    <c:if test = "${fuelDetails != null}" >
                        <%
                                    int index8 = 0;
                                    ArrayList settleColseDetails = (ArrayList) request.getAttribute("settleColseDetails");
                                    if (settleColseDetails.size() != 0) {
                        %>
                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <!--<tr>
                                <td height="30" class="contenthead" colspan="11">Proceed to Driver Settlement</td>
                            </tr>-->
                            <tr>
                                <td class="contentsub"  height="30">Driver Name</td>
                                <td class="contentsub" height="30">Vehicle No</td>
                                <td class="contentsub" height="30">From Date</td>
                                <td class="contentsub" height="30">To Date</td>
                                <td class="contentsub" height="30">Total Days</td>
                                <td class="contentsub" height="30">Total Salary</td>
                                <td class="contentsub" height="30">Due from Driver</td>
                                <td class="contentsub" height="30">Total Amount</td>
                                <td class="contentsub" height="30">Remarks</td>
                                <td class="contentsub" height="30">Remarks</td>
                                <td class="contentsub" height="30">&nbsp;</td>
                            </tr>
                            <c:forEach items="${settleColseDetails}" var="settleColseDetail">
                                <c:set var="total8" value="${total8+1}"></c:set>
                                <%
                                                                    String classText = "";
                                                                    int oddEven = index8 % 2;
                                                                    if (oddEven > 0) {
                                                                        classText = "text2";
                                                                    } else {
                                                                        classText = "text1";
                                                                    }
                                %>
                                <tr>
                                    <td class="<%=classText%>"  height="30"><c:out value="${settleColseDetail.empName}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${settleColseDetail.vehicleNo}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${settleColseDetail.fromDate}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${settleColseDetail.toDate}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${settleColseDetail.totalDays}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${settleColseDetail.totalSalary}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${settleColseDetail.driverDue}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${settleColseDetail.totalAmount}"/></td>
                                    <td class="<%=classText%>"  height="30"><c:out value="${settleColseDetail.remarks}"/></td>
                                    <td class="<%=classText%>"  height="30"><a href="" >Details</a></td>
                                </tr>
                                <%index8++;%>
                            </c:forEach>
                        </table>
                        <%
                                    }
                        %>

                    </c:if>

                    <table cellpadding="0" cellspacing="4" border="0" width="80%" class="border">
                        <tr style="width:50px;">
                            <th colspan="8" class="contenthead">Driver Details</th>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Driver Name
                            </td>
                            <td>
                                <input id="suDriverName" name="suDriverName" value="" />
                            </td>
                            <td class="contenletter">&nbsp;
                                <input type="hidden" id="suTotalTon" name="suTotalTon" value=""  />
                            </td>
                            <td>&nbsp;

                            </td>

                            <td class="contenletter">
                                Cleaner Name
                            </td>
                            <td>
                                <input id="cleanerName" name="cleanerName" value=""  />
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Vehicle No
                            </td>
                            <td>
                                <input id="suVehicleNo" name="suVehicleNo" value=""  />
                            </td>
                            <td class="contenletter">
                                From Date
                            </td>
                            <td>
                                <input id="suFromDate" name="suFromDate" value=""  readonly="true" />
                            </td>
                            <td class="contenletter">
                                To Date
                            </td>
                            <td>
                                <input id="suToDate" name="suToDate" value=""  readonly="true" />
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Total Days
                            </td>
                            <td>
                                <input id="totalDays" name="totalDays" value=""  readonly="true" />
                            </td>
                            <td class="contenletter">
                                Total Trips
                            </td>
                            <td>
                                <input id="totalTrips" name="totalTrips" value=""  readonly="true" />
                            </td>                            
                            <td class="contenletter">
                                End KM
                            </td>
                            <td>
                                <input id="endKM" name="endKM" value=""  readonly="true" />
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                10000 * No.of Trips
                            </td>
                            <td>
                                <input id="totalTripAmount" name="totalTripAmount" value=""  readonly="true" />
                            </td>
                            <td class="contenletter">&nbsp;

                            </td>
                            <td>&nbsp;

                            </td>
                            <td class="contenletter">
                                Start KM
                            </td>
                            <td>
                                <input id="startKM" name="startKM" value=""  readonly="true" />
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Total Tonnage Amount
                            </td>
                            <td>
                                <input id="totalTonAmount" name="totalTonAmount" value=""  readonly="true" />
                            </td>
                            <td class="contenletter">&nbsp;

                            </td>
                            <td>&nbsp;

                            </td>
                            <td class="contenletter">
                                Running KM
                            </td>
                            <td>
                                <input id="runningKM" name="runningKM" value=""  readonly="true" />
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Shortage
                            </td>
                            <td>
                                <input id="shortage" name="shortage" value=""  />
                            </td>
                            <td class="contenletter">&nbsp;

                            </td>
                            <td>&nbsp;

                            </td>
                            <td class="contenletter">
                                Total Diesel (Lit)
                            </td>
                            <td>
                                <input id="totalDiesel" name="totalDiesel" value=""  />
                            </td>                            
                        </tr>
                    </table>
                    <br/>
                    <table cellpadding="0" cellspacing="4" border="0" width="70%" class="border">                        
                        <tr>
                            <td class="contenletter">
                                Actual KM
                            </td>
                            <td>
                                <input id="actualKM" name="actualKM" value=""  />
                            </td>
                            <td class="contenletter">
                                Kmpl
                            </td>
                            <td>
                                <input id="kmpl" name="kmpl" value=""  />
                            </td>                            
                        </tr>                        
                        <tr>
                            <td class="contenletter">
                                Speed 'O' Meter Km
                            </td>
                            <td>
                                <input id="speedOMeterKM" name="speedOMeterKM" value=""  />
                            </td>
                            <td class="contenletter">
                                sKmpl
                            </td>
                            <td>
                                <input id="sKmpl" name="sKmpl" value=""  />
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Above / Below KM
                            </td>
                            <td>
                                <input id="aboveBelowKM" name="aboveBelowKM" value=""  />
                            </td>
                            <td class="contenletter">&nbsp;

                            </td>
                            <td>&nbsp;

                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table cellpadding="0" cellspacing="4" border="0" width="80%" class="border">
                        <tr>
                            <td class="contenletter">
                                Diesel Amount
                            </td>
                            <td>
                                <input id="dieselAmt" name="dieselAmt" value="" onblur="calcTotalExpenses(this);"  />
                            </td>
                            <td class="contenletter">
                                Income
                            </td>
                            <td>
                                <input id="income" name="income" value=""  />
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Driver Expenses
                            </td>
                            <td>
                                <input id="driverExpense" name="driverExpense" value=""  readonly="true" />
                            </td>
                            <td class="contenletter">
                                Expenses
                            </td>
                            <td>
                                <input id="expenses" name="expenses" value=""  />
                            </td>                           
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Driver Salary
                            </td>
                            <td>
                                <input id="driverSalary" name="driverSalary" value="" onblur="calcTotalExpenses(this);"  />
                            </td>
                            <td class="contenletter">
                                Balance
                            </td>
                            <td>
                                <input id="balance" name="balance" value=""  />
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Cleaner Salary
                            </td>
                            <td>
                                <input id="cleanerSalary" name="cleanerSalary" value="" onblur="calcTotalExpenses(this);"  />
                            </td>
                            <td class="contenletter">&nbsp;

                            </td>
                            <td>&nbsp;

                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Total Expenses
                            </td>
                            <td>
                                <input id="totalExpenses" name="totalExpenses" value=""  />
                            </td>
                            <td class="contenletter">
                                5000* No.of Trips
                            </td>
                            <td>
                                <input id="fiveNoOfTrips" name="fiveNoOfTrips" value=""  />
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Driver Advance
                            </td>
                            <td>
                                <input id="driverAdvance" name="driverAdvance" value=""  />
                            </td>
                            <td class="contenletter">
                                Shortage
                            </td>
                            <td>
                                <input id="shortage1" name="shortage1" value=""  />
                            </td>                            
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Driver + Cleaner Expenses
                            </td>
                            <td>
                                <input id="driverExpenses" name="driverExpenses" value=""  />
                            </td>
                            <td class="contenletter">
                                Per Days Income
                            </td>
                            <td>
                                <input id="perDayIncome" name="perDayIncome" value=""  />
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Due from Driver
                            </td>
                            <td>
                                <input id="dueFromDriver" name="dueFromDriver" value=""  />
                            </td>
                            <td class="contenletter">
                                Loss / Day
                            </td>
                            <td>
                                <input id="lossDay" name="lossDay" value=""  />
                            </td>
                        </tr>
                    </table>
                    <br>
                    <table cellpadding="0" cellspacing="4" border="0" width="80%" class="border">
                        <tr>
                            <td width="20%" class="contenletter">
                                Driver Bata + Salary
                            </td>
                            <td width="20%">
                                <input id="driverBataSalary" name="driverBataSalary" value=""  />
                            </td>
                            <td width="7%">&nbsp;</td>
                            <td width="16%">&nbsp;</td>
                            <td width="7%">&nbsp;</td>
                            <td width="16%">&nbsp;</td>
                            <td width="10%" class="contenletter">
                                C/F SAR
                            </td>
                            <td width="14%">
                                <input id="cFRs" name="cFRs" value=""  />
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Cleaner Bata + Salary
                            </td>
                            <td>
                                <input id="cleanerBataSalary" name="cleanerBataSalary" value=""  />
                            </td>
                            <td class="contenletter">
                                Date
                            </td>
                            <td>
                                <input id="cDate" name="cDate" value="" class="datepicker"  />
                            </td>
                            <td class="contenletter">
                                V.No
                            </td>
                            <td>
                                <input id="cVNo" name="cVNo" value=""  />
                            </td>
                            <td class="contenletter">
                                SAR
                            </td>
                            <td>
                                <input id="cRs" name="cRs" value=""  />
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Total Salary
                            </td>
                            <td>
                                <input id="totalSalary" name="totalSalary" value=""  />
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td class="contenletter">
                                Balance
                            </td>
                            <td >
                                <input id="cBalance" name="cBalance" value=""  />
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Due From Driver
                            </td>
                            <td>
                                <input id="dDueFomDriver" name="dDueFomDriver" value=""  />
                            </td>
                            <td class="contenletter">
                                Date
                            </td>
                            <td>
                                <input id="dDate" name="dDate" value="" class="datepicker"  />
                            </td>
                            <td class="contenletter">
                                V.No
                            </td>
                            <td>
                                <input id="dVNo" name="dVNo" value=""  />
                            </td>
                            <td class="contenletter">
                                SAR
                            </td>
                            <td>
                                <input id="dRs" name="dRs" value=""  />
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Actual Salary
                            </td>
                            <td>
                                <input id="actualSalary" name="actualSalary" value=""  />
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td class="contenletter">
                                Balance
                            </td>
                            <td>
                                <input id="dBalance" name="dBalance" value=""  />
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Bus Expenses
                            </td>
                            <td>
                                <input id="busExpenses" name="busExpenses" value=""  />
                            </td>
                            <td rowspan="2" colspan="1" class="contenletter">
                                Remarks
                            </td>
                            <td rowspan="2" colspan="5">
                                <input id="settleRemarks" name="settleRemarks" value=""  />
                                <!--<textarea cols="80" rows="5" id="settleRemarks" name="settleRremarks" ></textarea>-->
                            </td>
                        </tr>
                        <tr>
                            <td class="contenletter">
                                Total Amount
                            </td>
                            <td>
                                <input id="totalAmount" name="totalAmount" value=""  />
                            </td>
                        </tr>
                        <tr>&nbsp;</tr>
                        <tr>
                            <td colspan="8" align="center">
                                <input type="button" name="proceed" value="Proceed to Settlement" onclick="submitPage(this)" id="proceed" class="button" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>