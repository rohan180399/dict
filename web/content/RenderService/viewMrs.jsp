<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
     <%@page language="java" contentType="text/html; charset=UTF-8"%>
 <%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>  
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ page import="ets.domain.mrs.business.MrsTO" %>  


<script>
 function setSelectbox(i)
    {       
        var selected=document.getElementsByName("selectedIndex") ;
        selected[i].checked = 1;    
        
    }
    
    function submitPage(){
    document.mrs.action="/throttle/updateMrsItems.do";
    document.mrs.submit();
    }
    
</script>

<script>
   function changePageLanguage(langSelection){

            if(langSelection== 'ar'){
            document.getElementById("pAlign").style.direction="rtl";
            }else if(langSelection== 'en'){
            document.getElementById("pAlign").style.direction="ltr";
            }
        }

    </script>

<div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.MRS"  text="MRS"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="general.label.stores"  text="Stores"/></a></li>
          <li class="active"><spring:message code="service.label.MRS"  text="MRS"/></li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">

        <body>
            
            

<form name="mrs" method="post">
    
<%
    
    int index=0;
    %>

<c:if test = "${vehicleDetails != null}" >    
<table class="table table-info mb30 table-hover" >
<thead>
<tr>
<th colspan="4">
    <spring:message code="service.label.SystemMRSNo"  text="default text"/>:<%=request.getAttribute("mrsId")%>
    &nbsp;&nbsp;<spring:message code="service.label.ManualMRSNo"  text="default text"/>:<%=request.getAttribute("manualMrsNo")%>
</th>
</tr>
</thead>
<c:forEach items="${vehicleDetails}" var="fservice"> 
  <%  
String classText = "";
int oddEven = index % 2;
if (oddEven > 0) {
classText = "text1";
} else {
classText = "text2";
}
%>
<tr>
<td ><spring:message code="service.label.JobCardNo"  text="default text"/>
</td>
<input name="mrsJobCardNumber" type="hidden" value='<c:out value="${fservice.jobCardId}"/>'>
<td height="30"><c:out value="${fservice.jobCardId}"/></td>
<td ><spring:message code="service.label.VehicleNo"  text="default text"/></td>
<td ><c:out value="${fservice.mrsVehicleNumber}"/></td>
</tr>
<tr>
<td ><spring:message code="service.label.ServiceType"  text="default text"/></td>
<td ><c:out value="${fservice.serviceTypeName}"/></td>
<td ><spring:message code="service.label.ChassisNo"  text="default text"/></td>
<td ><c:out value="${fservice.mrsVehicleChassisNumber}"/></td>
</tr>
<tr>
<td ><spring:message code="service.label.VehicleType"  text="default text"/></td>
<td ><c:out value="${fservice.mrsVehicleType}"/></td>
<td ><spring:message code="service.label.UsageType"  text="default text"/></td>
<td ><c:out value="${fservice.mrsVehicleUsageType}"/></td>
</tr>
<!--<tr>

<td >Technician</td>
<td ><select class='form-control' id='technicianId'  name='technicianId' >
<option selected   value=0>---Select---</option>
<c:if test = "${technicians != null}" >
<c:forEach items="${technicians}" var="mfr">
<option  value='<c:out value="${mfr.empId}" />'>
<c:out value="${mfr.empName}" />
</c:forEach >
</c:if> 
</select></td>
<td >&nbsp;</td>
<td >&nbsp;</td>
</tr>-->
<%index++;%>
</c:forEach>
</table>

</c:if>
<br><br>

 <%
    
    int index1=0;
    %>

<c:if test = "${mrsDetails != null}" >       
    
<table class="table table-info mb30 table-hover" >
<thead>

<tr>
<th colspan="8" align="center" height="30"><strong><spring:message code="service.label.JobCardNo"  text="default text"/> </strong></th>
</tr>
<tr>
<th><spring:message code="service.label.SNo"  text="default text"/>
</th>
<th><spring:message code="service.label.MfrCode"  text="default text"/>
</th>
<th><spring:message code="service.label.ItemCode"  text="default text"/></th>
<th><spring:message code="service.label.ItemName"  text="default text"/></th>
<th><spring:message code="service.label.UOM"  text="default text"/></th>
<th><spring:message code="service.label.RequestedQty"  text="default text"/></th>
<th><spring:message code="service.label.ApprovedQty"  text="default text"/></th>
<th><spring:message code="service.label.IssuedQty"  text="default text"/></th>

</tr>
</thead>
<c:forEach items="${mrsDetails}" var="service"> 
  <%  
String classText1="";
int oddEven1 = index1% 2;
if (oddEven1 > 0) {
classText1 = "text1";
} else {
classText1 = "text2";
}
%>
<input name="itemId" class="form-control" type="hidden" value='<c:out value="${service.mrsItemId}" />'/>
<tr>
<td><%=index1+1%></td>
<td><c:out value="${service.mrsItemMfrCode}" /></td>
<td><c:out value="${service.mrsPaplCode}" /></td>
<td><c:out value="${service.mrsItemName}" /></td>
<td><c:out value="${service.uomName}" /></td>
<td><c:out value="${service.mrsRequestedItemNumber}"/></td>
<td><c:out value="${service.approvedQty}"/>&nbsp;</td>
<td><c:out value="${service.mrsIssueQuantity}"/>&nbsp;</td>
</tr>
<%index1++;%>
</c:forEach>
</table>
</c:if>
<br>


<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
  </body>

  </div>


    </div>
    </div>





<%@ include file="/content/common/NewDesign/settings.jsp" %>


