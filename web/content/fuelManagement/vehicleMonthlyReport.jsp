
<html>
    <head>
     <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
    </head>
    <script language="javascript">

function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}

        function submitPage(){
            var chek=validation();
            if(chek=='true'){
            document.report.action='/throttle/vehicleReport.do';
            document.report.submit();
        }
        }
        function submitExcel(){
            document.report.action='/throttle/vehicleReportExcel.do';
            document.report.submit();
            }
        function validation(){
            if(textValidation(document.report.fromDate,'From Date')){
                return 'false';                
                }
            if(textValidation(document.report.toDate,'TO Date')){
                return 'false';
                
                }
               
            return 'true';
            }
        function getVehicleNos(){
            
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
        }       
        function setValues(){
            if('<%=request.getAttribute("fromDate")%>' !='null'){
            document.report.fromDate.value='<%=request.getAttribute("fromDate")%>';
            document.report.toDate.value='<%=request.getAttribute("toDate")%>';
            
            }if('<%=request.getAttribute("regNo")%>'!='null'){
            document.report.regNo.value='<%=request.getAttribute("regNo")%>';
            }
            }
    
    </script>
    <body onload="getVehicleNos();setValues();">
        <form name="report" method="post">
            <%@ include file="/content/common/path.jsp" %>                 
            <%@ include file="/content/common/message.jsp" %>


<table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
<tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
</h2></td>
<td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
</tr>
<tr id="exp_table" >
<td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
    <div class="tabs" align="left" style="width:850;">
<ul class="tabNavigation">
        <li style="background:#76b3f1">Vehicle Report</li>
</ul>
<div id="first">
<table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
<tr height="30">
    <td>VehicleNo</td>
    <td><input name="regNo" id="regno" class="form-control" type="text" value="" size="20"></td>
    <td><font color="red">*</font>From Date</td>
    <td><input name="fromDate" class="form-control" type="text"  size="20"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.report.fromDate,'dd-mm-yyyy',this)"/></td>
    <td><font color="red">*</font>To Date</td>
    <td><input name="toDate" class="form-control" type="text"  size="20"> <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.report.toDate,'dd-mm-yyyy',this)"/></td>
    <td><input type="button" class="button" name="Search" value="Search" onclick="submitPage();"></td>
    </tr>
</table>
</div></div>
</td>
</tr>
</table>
            <br>

           <c:set var="total" value="0"/>
           <c:set var="totLtr" value="0"/>
           <c:set var="avg" value="0"/>
           <c:if test = "${VehicleReport!= null}" >
                
                <table align="center" width="800" class="border" border="0" cellspacing="0" cellpadding="0">
                    <!--<tr height="30">
                        <td colspan="9" class="contenthead">Vehicle Fuel Report</td>
                    </tr>
                    -->
                    <tr height="30">                                                                                                    
                        <td class="contentsub">Vehicle Number</td> 
                        <td class="contentsub">Vehicle Type</td>
                        <td class="contentsub">Usage Type</td>
                        <td class="contentsub">Fuel Filled In</td>                        
                        <td class="contentsub">Fuel Filled Out</td>                        
                        <td class="contentsub">Total Filled</td>                        
                        <td class="contentsub">Total Km</td>
                        <td class="contentsub">Average</td>
                       <td class="contentsub">Amount</td>
                    </tr>
                    <% int index = 0;%>
                    <c:forEach items="${VehicleReport}" var="fuel"> 
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>	
                        <tr height="30">
                            
                           
                            <td class="<%=classText %>"><input type="hidden" name="regNos" value='<c:out value="${fuel.regNo}"/>'><c:out value="${fuel.regNo}"/></td>                                                        
                            <td class="<%=classText %>"><input type="hidden" name="types" value='<c:out value="${fuel.typeName}"/>'><c:out value="${fuel.typeName}" /></td>
                            <td class="<%=classText %>"><input type="hidden" name="usageTypes" value='<c:out value="${fuel.usageType}"/>'><c:out value="${fuel.usageType}" /></td>
                            <td class="<%=classText %>"><input type="hidden" name="fuels" value='<c:out value="${fuel.fuelFilled}"/>'><c:out value="${fuel.fuelFilled}" /></td>
                            <td class="<%=classText %>"><input type="hidden" name="outFills" value='<c:out value="${fuel.outFill}"/>'><c:out value="${fuel.outFill}" /></td>
                            <td class="<%=classText %>"><input type="hidden" name="totFills" value='<c:out value="${fuel.outFill}"/>'><c:out value="${fuel.outFill + fuel.fuelFilled }" /></td>
                            <td class="<%=classText %>"><input type="hidden" name="totalKms" value='<c:out value="${fuel.totalKm}"/>'><c:out value="${fuel.totalKm}"/></td>                                                       
                            <td class="<%=classText %>"><input type="hidden" name="avgs" value='<fmt:formatNumber value="${fuel.totalKm/(fuel.outFill + fuel.fuelFilled)}" pattern="##.00"/>'><fmt:formatNumber value="${fuel.totalKm/(fuel.outFill + fuel.fuelFilled)}" pattern="##.00"/></td>                                                       
                            <td class="<%=classText %>"><input type="hidden" name="rates" value='<c:out value="${fuel.rate}"/>'><c:out value="${fuel.rate}"/></td>                                                       
                            <c:set var="total" value="${total + fuel.rate}"/>
                            <c:set var="totLtr" value="${totLtr + fuel.fuelFilled}"/>
                            
                       </tr>
                        <%index++;%>
                    </c:forEach>
                    
                    <tr>                                           
                        <td class="text2" height="30">&nbsp;</td>                                                                                              
                                 <td class="text2" height="30">&nbsp;</td>
                                 <td class="text2" height="30">&nbsp;</td>
                        <td class="text2" height="30"><b>Total InFill Amount</b></td>                                       
                        <td class="text2" align="left"  height="30"> 
                        <input type="hidden" name="amount" value='SAR<fmt:formatNumber value="${total}" pattern="##.00"/>'>
                            <fmt:setLocale value="en_US" /><b> SAR: <fmt:formatNumber value="${total}" pattern="##.00"/></b>            
                        </td>  
                        <td class="text2" height="30">&nbsp;</td>                        
                        <td class="text2" height="30">&nbsp;</td>     
                        <td class="text2" height="30">&nbsp;</td>  
                        <td class="text2" height="30">&nbsp;</td>
                    </tr>  
                    <tr>                                           
                        <td class="text2" height="30">&nbsp;</td>                                                                                              
                                 <td class="text2" height="30">&nbsp;</td>
                                 <td class="text2" height="30">&nbsp;</td>
                        <td class="text2" height="30"><b>TotalIn Fill Litres</b></td>                                       
                        <td class="text2" align="left"  height="30"> 
                        <input type="hidden" name="amount" value='<c:out value="${total}"/>'>
                            <fmt:setLocale value="en_US" /><b> Ltrs: <fmt:formatNumber value="${totLtr}" pattern="##.00"/></b>            
                        </td>  
                         <input type="hidden" name="litres" value='Ltrs <fmt:formatNumber value="${totLtr}" pattern="##.00"/>'>
                        <td class="text2" height="30">&nbsp;</td>                        
                        <td class="text2" height="30">&nbsp;</td>     
                        <td class="text2" height="30">&nbsp;</td>  
                        <td class="text2" height="30">&nbsp;</td>
                    </tr>  
                </table>
                <br>
                <center><input type="button" class="button" name="Excel" value="To Excel" onclick="submitExcel();"></center>
            </c:if>
            <br><br>
            
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
