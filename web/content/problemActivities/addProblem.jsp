<%@ include file="/content/common/NewDesign/header.jsp" %>
    <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="ets.domain.problemActivities.business.ProblemActivitiesTO" %>

<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
  
 <script language="javascript" src="/throttle/js/validate.js"></script>   


<script>
    function submitpage()
    {
      
       
       if(textValidation(document.dept.problemName,"problemName")){
           return;
       }else if(textValidation(document.dept.discription,"discription")){
           return;
       
       }else if(isSelect(document.dept.sectionId,"Section")){
           return;
       }else
       {
         document.dept.action= "/throttle/addProblem.do";
         document.dept.submit();   
       }
       
       
      
    }
</script>
 <div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="serviceplan.label.AddProblem" text="default text"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="serviceplan.label.ServicePlan" text="default text"/></a></li>
          <li class=""><spring:message code="serviceplan.label.ManageProblem" text="default text"/></li>
          <li class=""><spring:message code="serviceplan.label.AddProblem" text="default text"/></li>

        </ol>
      </div>
      </div>
 <div class="contentpanel">
<div class="panel panel-default">
 <div class="panel-body">
<body>
<form name="dept"  method="post">
 
       

<%@ include file="/content/common/message.jsp" %>
 
 <table class="table table-info mb30 table-hover" >
     <thead>
<tr height="30">
<th colspan="5" ><spring:message code="serviceplan.label.AddProblem" text="default text"/></th>
</tr>
</thead>
 <tr >
<td  height="30"><font color=red>*</font><spring:message code="serviceplan.label.Section" text="default text"/></td>
<td   ><select style="width:260px;height:40px;"  class="form-control" name="sectionId"  >
<option value="0">---<spring:message code="serviceplan.label.Select" text="default text"/>---</option>
<c:if test = "${SectionList != null}" >
<c:forEach items="${SectionList}" var="Dept"> 
<c:if test="${ (Dept.activeInd=='Y') || (Dept.activeInd=='y') }">
<option value='<c:out value="${Dept.sectionId}" />'><c:out value="${Dept.sectionName}" /></option>
</c:if>
</c:forEach >

</c:if>  	
</select></td>

    <td  height="30"><font color=red>*</font><spring:message code="serviceplan.label.ProblemName" text="default text"/></td>
<td  height="30"><input name="problemName" type="text" style="width:260px;height:40px;"  class="form-control" value=""></td>
</tr>
<tr>
<td  height="30"><font color=red>*</font><spring:message code="serviceplan.label.Description" text="default text"/> </td>
<td  height="30"><textarea style="width:260px;height:40px;"  class="form-control" name="discription"></textarea></td>
<td ></td>

<td><input type="button" value="<spring:message code="serviceplan.label.ADD" text="default text"/>" class="btn btn-success"  onClick="submitpage();">
&emsp;<input type="reset" class="btn btn-success"  value="<spring:message code="serviceplan.label.CLEAR" text="default text"/>"></td>
</tr>
</table>

<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
