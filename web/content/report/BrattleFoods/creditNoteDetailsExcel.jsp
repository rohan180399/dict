
<%-- 
    Document   : InvoiceDetailsReportExcel
    Created on : Apr 28, 2016, 7:06:41 PM
    Author     : Gulshan kumar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
            <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "InvoiceDetailsReport-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
            <c:if test = "${invoiceDetailsReport != null}" >
                <table border="1" bgcolor="#F3FAB6" align="center" width="100%" cellpadding="0" cellspacing="0" >
                    <thead>
                        <tr>
                            <td class="contenthead">S.no</td>
                            <td class="contenthead" >CreditNoteNo</td>
                            <td class="contenthead" >CreditNoteDate</td>
                           
                            <td class="contenthead" >CreditNoteReason</td>
                            <td class="contenthead">InvoiceCode</td>
                            <td class="contenthead">InvoiceDate</td>
                            <td class="contenthead">Customer Name</td>
                             <td class="contenthead">GST No</td>
                            <td class="contenthead">customer ReferenceId</td>
                            <td class="contenthead"> Invoice Total Amount</td>
                            <td class="contenthead" >CreditNoteAmount</td>
                            <td class="contenthead">freight Amount</td>
                            <td class="contenthead">OtherCharges</td>
                            <td class="contenthead">detention</td>
                            <td class="contenthead">toll Tax</td>
                            <td class="contenthead">green Tax</td>
                            <td class="contenthead">weightment</td>
                            <td class="contenthead">toll ErpId</td>
                            <td class="contenthead">detention ErpId</td>
                            <td class="contenthead">green ErpId</td>
                            <td class="contenthead">weightment ErpId</td>
                            <td class="contenthead">freightAmount ErpId</td>
                            <td class="contenthead">Reason</td>
                            <td class="contenthead">Remarks</td>
                            <th class="contenthead" >GstType</th>
                            <th class="contenthead" >Commodity Name</th>
                            <th class="contenthead" >Post Status</th>
                            <th class="contenthead" >Api Error Msg</th>
                        </tr>
                    </thead>
                    <% int index = 0,sno = 1;%>
                    <c:forEach items="${invoiceDetailsReport}" var="csList">
                        <tr>
                             <td align="center"><%=sno%></td>
                             <td align="center"><c:out value="${csList.creditNotecode}"/></td>
                            <td align="center"><c:out value="${csList.creditNoteDate}"/></td>
                            <td align="center"><c:out value="${csList.reason}"/></td>
                            <td align="center"><c:out value="${csList.invoiceCode}"/></td>
                            <td align="center"><c:out value="${csList.invoiceDate}"/></td>
                            <td align="center"><c:out value="${csList.custName}"/></td>
                            <td align="center"><c:out value="${csList.gstNo}"/></td>
                            <td align="left"><c:out value="${csList.customerReferenceId}"/></td>
                            <td align="center"><c:out value="${csList.grandTotal}"/></td>
                            <td align="center"><c:out value="${csList.creditAmount}"/></td>
                            <td align="center"><c:out value="${csList.totalAmount}"/></td>
                            <td align="center"><c:out value="${csList.otherExpense}"/></td>
                            <td align="center"><c:out value="${csList.detenTion}"/></td>
                            <td align="center"><c:out value="${csList.tollTax}"/></td>
                            <td align="center"><c:out value="${csList.greenTax}"/></td>
                            <td align="center"><c:out value="${csList.weightMent}"/></td>
                            <td align="center"><c:out value="${csList.tollErpId}"/></td>
                            <td align="center"><c:out value="${csList.detentionErpId}"/></td>
                            <td align="center"><c:out value="${csList.greenErpId}"/></td>
                            <td align="center"><c:out value="${csList.weightmentErpId}"/></td>
                            <td align="center"><c:out value="${csList.freightAmountErpId}"/></td>
                            <td align="center"><c:out value="${csList.reason}"/></td>
                            <td align="center"><c:out value="${csList.remarks}"/></td>
                            <td align="center"><c:out value="${csList.gstType}"/></td>
                            <td align="center"><c:out value="${csList.commodityName}"/></td>
                            <td align="center"><c:out value="${csList.postStatus}"/></td>
                            <td align="center"><c:out value="${csList.apiErrorMsg}"/></td>
                        </tr>
                        <%  
                   index++;
                     sno++;
                        %>
                    </c:forEach>
                </table>

                    <br>
                <br>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>

           
 

</html>

