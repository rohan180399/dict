/*---------------------------------------------------------------------------
 * ContractBP.java
 * Mar 3, 2009
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
-------------------------------------------------------------------------*/

package ets.domain.contract.data;
import ets.arch.exception.FPRuntimeException;
import ets.domain.util.FPLogUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
/****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver              Date                      Author                    Change
 * ---------------------------------------------------------------------------
 * 1.0           Mar 3, 2009              vijay			       Created
 *
 ******************************************************************************/
public class ContractDAO extends SqlMapClientDaoSupport{
     private final int errorStatus = 4;
    private final static String CLASS = "ContractDAO";

    public ContractDAO() {
    }

   

    public ArrayList getContractDetails() {
          Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
         ArrayList contractDetails=new ArrayList();
        

        try {
            
            int status=0;
            status=(Integer)getSqlMapClientTemplate().update("contract.setInActiveContract",map);
            contractDetails = (ArrayList) getSqlMapClientTemplate().queryForList("contract.getContractDetails", map);
            

        } catch (Exception sqlException) {
          
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getContractDetails", sqlException);
        }
        return contractDetails;
    }

    public ArrayList getMaintenanceDetails() {
        throw new UnsupportedOperationException("Not yet implemented");
    }





    public ArrayList getMfrList() {
          Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
         ArrayList contractDetails=new ArrayList();
        

        try {
            contractDetails = (ArrayList) getSqlMapClientTemplate().queryForList("contract.getMfrList", map);
            

        } catch (Exception sqlException) {
          
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMfrList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getMfrList", sqlException);
        }
        return contractDetails;
    }

    public ArrayList getModList(int mfrId) {
          Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
         ArrayList contractDetails=new ArrayList();
        map.put("mfrId", mfrId);

        try {
            if(mfrId!=0){
            contractDetails = (ArrayList) getSqlMapClientTemplate().queryForList("contract.getModList", map);
            }else
            {
                contractDetails = (ArrayList) getSqlMapClientTemplate().queryForList("contract.getAllModList", map);
            }

        } catch (Exception sqlException) {
          
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getModList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getModList", sqlException);
        }
        return contractDetails;
    }
    
        public ArrayList getCustList() {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
         ArrayList CustList=new ArrayList();
       

        try {
            CustList = (ArrayList) getSqlMapClientTemplate().queryForList("contract.getCustList", map);
            

        } catch (Exception sqlException) {
          
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCustList", sqlException);
        }
        return CustList;
    }

    public int insertContractDetails(int mfrvalues, int modvalues, float sparvalues, float labovalues,int contractId,int userId) {
         Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
int status=0;
       
            map.put("mfrId",mfrvalues);
            map.put("modId", modvalues);
            map.put("spare", sparvalues);
            map.put("labour",labovalues);            
            map.put("contractId",contractId);                        
            map.put("userId",userId);                        
           
        try {
            status = (Integer) getSqlMapClientTemplate().update("contract.insertContractDetails", map);
            
            
        } catch (Exception sqlException) {
          
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertContractDetails", sqlException);
        }
        return status;

    }
    
      public int insertContractDetails(int mfrvalues, int modvalues, String sparvalues, String labovalues,int contractId,int userId) {
         Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
int status=0;
       
            map.put("mfrId",mfrvalues);
            map.put("modId", modvalues);
            map.put("spare",Float.parseFloat(sparvalues));
            map.put("labour",Float.parseFloat(labovalues));            
            map.put("contractId",contractId);                        
            map.put("userId",userId);                        
           
        try {
            status = (Integer) getSqlMapClientTemplate().update("contract.insertContractDetails", map);
            
            
        } catch (Exception sqlException) {
          
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertContractDetails", sqlException);
        }
        return status;

    }
    
    
        public int getContractId(String sdate, String edate, String custId,int userId) {
              Map map = new HashMap();
            map.put("sdate",sdate);
            map.put("edate",edate);
            map.put("custId",custId);
            map.put("userId",userId);                        
            int status=0;
            int contractId=0;
            String temp="";
              try {          
            
            temp=(String) getSqlMapClientTemplate().queryForObject("contract.lastContractId", map);
            //////System.out.println("temp"+ temp);
            
            if(temp==null){
            contractId = (Integer) getSqlMapClientTemplate().insert("contract.insertContract", map);                              
           // temp=(String) getSqlMapClientTemplate().queryForObject("contract.lastContractId", map);            
            }else{
            contractId=Integer.parseInt(temp);
            }
            //////System.out.println("lastContractId"+contractId);
            
        } catch (Exception sqlException) {
          
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getContractId", sqlException);
        }
        return contractId;
    }

    public int modifyContractDetails(int custId, String startDate, String endDate, int userId) {
          Map map = new HashMap();
            map.put("sdate",startDate);
            map.put("edate",endDate);
            map.put("custId",custId);
            map.put("userId",userId);                        
            int status=0;
            String temp="";
            
              try {          
                  
            status = (Integer) getSqlMapClientTemplate().update("contract.updateContract", map);  
            temp=(String) getSqlMapClientTemplate().queryForObject("contract.lastContractId", map);                        
            status=Integer.parseInt(temp);
            //////System.out.println("status"+status);
            
        } catch (Exception sqlException) {
          
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("modifyContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "modifyContractDetails", sqlException);
        }
        return status;

    }

    public int updateContractDetails(int custId, int userId, float spare, float labour, int modId, int mfrId,String acStatus,int contractId) {
        Map map = new HashMap();
            map.put("spare",spare);                        
            map.put("labour",labour);            
            map.put("custId",custId);
            map.put("userId",userId);                        
            map.put("modId",modId);     
            map.put("mfrId",mfrId);                 
            map.put("contractId",contractId);                        
            map.put("status",acStatus);                        
            int status=0;
            int sta=0;
              try {          
                  
                  if(acStatus.equals("N")){
                
            status = (Integer) getSqlMapClientTemplate().update("contract.setInactiveContractDetails", map);                                                          
            //////System.out.println("status vijay"+status);
            }else{                      
            status = (Integer) getSqlMapClientTemplate().update("contract.setInactiveContractDetails", map);                                          
            //////System.out.println("status 1"+status);
            status = (Integer) getSqlMapClientTemplate().update("contract.insertContractDetails", map);                                              
            //////System.out.println("status 2"+status);            
            }
            
            
        } catch (Exception sqlException) {
          
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("modifyContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "modifyContractDetails", sqlException);
        }
        return status;
    }

    //Rathimeena Contract Start
    public ArrayList custContractDetails(int custId) {
          Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
         ArrayList contractDetails=new ArrayList();
        map.put("custId",custId);

        try {
            contractDetails = (ArrayList) getSqlMapClientTemplate().queryForList("contract.custContractDetails", map);


        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("custContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "custContractDetails", sqlException);
        }
        return contractDetails;
    }
    
    public ArrayList getRouteList() {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList routeList = new ArrayList();
        try {
            routeList = (ArrayList) getSqlMapClientTemplate().queryForList("contract.getRouteList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMfrList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getMfrList", sqlException);
        }
        return routeList;
    }

    public int insertCustContractDetails(int rId, int tonRate, int contractId, int userId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        int status = 0;
        map.put("rId", rId);
        map.put("tonRate", tonRate);
        map.put("contractId", contractId);
        map.put("userId", userId);
        //////System.out.println("insertCustContractDetails...");
        //////System.out.println("rId: "+rId);
        //////System.out.println("tonRate: "+tonRate);
        //////System.out.println("contractId: "+contractId);
        try {
            status = (Integer) getSqlMapClientTemplate().update("contract.insertCustContractDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertCustContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertCustContractDetails", sqlException);
        }
        return status;
    }

    public ArrayList rathiCustContractDetails(int custId) {
          Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
         ArrayList custContractDetails=new ArrayList();
        map.put("custId",custId);

        try {
            custContractDetails = (ArrayList) getSqlMapClientTemplate().queryForList("contract.rathiCustContractDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("custContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "custContractDetails", sqlException);
        }
        return custContractDetails;
    }

    public int updateCustContractDetails(int custId, int userId, String rId, String tonRate, String acStatus,int contractId) {
        Map map = new HashMap();
            map.put("custId",custId);
            map.put("userId",userId);
            map.put("rId",rId);
            map.put("tonRate",tonRate);
            map.put("contractId",contractId);
            map.put("status",acStatus);

            /*//////System.out.println("updateCustContractDetails....DAO");
            //////System.out.println("rId: "+rId);
            //////System.out.println("tonRate: "+tonRate);
            //////System.out.println("contractId: "+contractId);
            //////System.out.println("acStatus: "+acStatus);*/
            int status=0;
            int sta=0;
            try {
                if(acStatus.equals("N")){
                    status = (Integer) getSqlMapClientTemplate().update("contract.setInactiveCustContractDetails", map);                    
                }else{
                    status = (Integer) getSqlMapClientTemplate().update("contract.setInactiveCustContractDetails", map);
                    ////////System.out.println("status in active: "+status);
                    status = (Integer) getSqlMapClientTemplate().update("contract.insertCustContractDetails", map);
                    ////////System.out.println("status insert: "+status);
                }
            } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("modifyContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "modifyCustContractDetails", sqlException);
        }
        return status;
    }

    public int insertCustContractDetails(String rId, String tonRate, int contractId,int userId) {
         Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        int status=0;
        map.put("rId",rId);
        map.put("tonRate",tonRate);
        map.put("contractId",contractId);
        map.put("userId",userId);
        try {
            status = (Integer) getSqlMapClientTemplate().update("contract.insertCustContractDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertCustContractDetails", sqlException);
        }
        return status;
    }
    //Rathimeena Contract End
    

}
