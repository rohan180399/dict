
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    </head>
    <script language="javascript">

        function show_src() {
            document.getElementById('exp_table').style.display = 'none';
        }
        function show_exp() {
            document.getElementById('exp_table').style.display = 'block';
        }
        function show_close() {
            document.getElementById('exp_table').style.display = 'none';
        }


        var httpReq;
        var temp = "";
        function ajaxData()
        {

            var url = "/throttle/getModels1.do?mfrId=" + document.mileage.mfrId.value;
            if (window.ActiveXObject)
            {
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest)
            {
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function() {
                processAjax();
            };
            httpReq.send(null);
        }

        function processAjax()
        {
            if (httpReq.readyState == 4)
            {
                if (httpReq.status == 200)
                {
                    temp = httpReq.responseText.valueOf();
                    setOptions(temp, document.mileage.modelId);
                    setTimeout("fun()", 1);
                }
                else
                {
                    alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
                }
            }
        }
        function submitGo() {
            document.mileage.action = '/throttle/viewVehicleMilleage.do';
            document.mileage.submit();

        }
        function deleteMilleage(indx) {
            var mfrId = document.getElementsByName("mfrIds");
            var modelId = document.getElementsByName("modelIds");
            var fromYear = document.getElementsByName("fromYears");
            var toYear = document.getElementsByName("toYears");

            document.mileage.action = '/throttle/deleteVehicleMilleage.do?mfrId=' + mfrId[indx].value + '&modelId=' + modelId[indx].value + '&fromYear=' + fromYear[indx].value + '&toYear=' + toYear[indx].value;
            document.mileage.submit();

        }

        function fun() {
            document.mileage.modelId.value = '<%=request.getAttribute("modelId")%>';
        }



        function setValues() {
            if ('<%=request.getAttribute("vehicleTypeId")%>' != 'null') {
                document.mileage.vehicleTypeId.value = '<%=request.getAttribute("vehicleTypeId")%>';
            }
            ajaxData();
        }

        function setSelectbox(i,obj) {
            var selected = document.getElementsByName("selectedIndex");
            if(obj.value != ''){
            selected[i].checked = 1;
            }    
        }
        
        function submitPage() {
            var checValidate = selectedItemValidation();
//                document.mileage.action = '/throttle/saveVehicleMilleage.do';
//                document.mileage.submit();
        }
        function selectedItemValidation(){
            var index = document.getElementsByName("selectedIndex");
            var chec=0;
            for(var i=0;(i<index.length && index.length!=0);i++){
                if(index[i].checked){
                chec++;
                }
            }
            if(chec == 0){
            alert("Please Select Any One And Then Proceed");
            }else{
            document.mileage.action = '/throttle/saveVehicleMilleage.do';
            document.mileage.submit();
            }
        }
    </script>
    <body onload="setValues();">
        <form name="mileage" method="post">
            <%@ include file="/content/common/path.jsp" %>                 
            <%@ include file="/content/common/message.jsp" %>

            <table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Milleage Configuration Search</li>
                            </ul>
                            <div id="first">
                                <table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                                    <tr align="center">
                                        <td height="30">Vehicle Type</td>
                                        <td height="30"><select class="form-control" name="vehicleTypeId" id="vehicleTypeId">
                                                <option value="0">---Select---</option>
                                                <c:if test = "${vehicleTypeList != null}" >
                                                    <c:forEach items="${vehicleTypeList}" var="vtList">
                                                        <option value='<c:out value="${vtList.vehicleTypeId}" />'><c:out value="${vtList.vehicleTypeName}" /></option>
                                                    </c:forEach >
                                                </c:if>
                                            </select></td>
                                        
                                        <td><input type="button" class="button" onclick="submitGo();" value="Go"></td>
                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>


            <br>   

            <c:if test = "${milleageList!= null}">

                <table align="center" width="950" class="border" border="0" cellspacing="0" cellpadding="0">

                    <tr >

                        <td class="contentsub">S no</td>                              
                        <td class="contentsub">Vehicle Type</td>                              
                        <td class="contentsub">Vehicle Mileage</td>
                        <td class="contentsub">Reefer Mileage</td>
                        <td class="contentsub">Select</td>

                    </tr>
                    <% int index = 0;
                    int sno = 1;%>
                    <c:forEach items="${milleageList}" var="fuel"> 
                        <%
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }
                            
                        %>	
                        <tr height="30">

                            <td class="<%=classText %>"><%=sno%></td>                                                         
                            <td class="<%=classText %>"><input type="hidden" name="vehicleTypeIds" value='<c:out value="${fuel.vehicleTypeId}"/>'><c:out value="${fuel.vehicleTypeName}" /></td>                                                         
                            <td class="<%=classText %>"><input type="text" name="vehicleMileage" value="<c:out value="${fuel.milleage}" />" onchange="setSelectbox('<%= index %>',this);"/></td>
                            <td class="<%=classText %>"><input type="text" name="reeferMileage" value="<c:out value="${fuel.reeferMileagePerKm}" />" onchange="setSelectbox('<%= index %>',this);"/></td>
                            <td class="<%=classText %>"><input type="checkbox" name="selectedIndex" value="<%=index%>"/></td>
                        </tr>
                        <%
                        index++;
                        sno++;
                        %>
                    </c:forEach>
                </table>
            </c:if>
            <br>
            <center><input type="button" class="button" value="Update Mileage" name="update" onclick="submitPage();" >
            </center><br>
            <br>
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
