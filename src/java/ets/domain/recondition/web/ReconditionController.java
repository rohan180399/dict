/*-------------------------------------------------------------------------
 * __NAME__.java
 * __DATE__
 *
 * Copyright (c) Entitle.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Entitle ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Entitle.
-------------------------------------------------------------------------*/
/******************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver    Date                               Author                    Change
 * ----------------------------------------------------------------------------
 * 1.0   __DATE__                Your_Name ,Entitle      Created
 *
 ******************************************************************************/
package ets.domain.recondition.web;

import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.springframework.web.bind.ServletRequestDataBinder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.ModelAndView;
import java.util.ArrayList;

import ets.arch.web.BaseController;
import ets.domain.recondition.web.ReconditionCommand;
import ets.domain.recondition.business.ReconditionBP;
import ets.domain.recondition.business.ReconditionTO;
import ets.domain.mrs.business.MrsTO;
import ets.domain.purchase.business.PurchaseBP;
import ets.domain.users.business.LoginBP;


import ets.domain.util.FPLogUtils;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.mrs.business.MrsBP;
import ets.domain.util.ParveenErrorConstants;
import ets.domain.vehicle.business.VehicleBP;
import ets.domain.vehicle.business.VehicleTO;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

public class ReconditionController extends BaseController {

    ReconditionCommand reconditionCommand;
    ReconditionBP reconditionBP;
    LoginBP loginBP;
    PurchaseBP purchaseBP;
    VehicleBP vehicleBP;
    MrsBP mrsBP;
//    HttpSession session = request.getSession();
//    private static int COMPANY_ID=Integer.parseInt((String)session.getAttribute("companyId"));;

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    public ReconditionBP getReconditionBP() {
        return reconditionBP;
    }

    public void setReconditionBP(ReconditionBP reconditionBP) {
        this.reconditionBP = reconditionBP;
    }

    public PurchaseBP getPurchaseBP() {
        return purchaseBP;
    }

    public void setPurchaseBP(PurchaseBP purchaseBP) {
        this.purchaseBP = purchaseBP;
    }

    public ReconditionCommand getReconditionCommand() {
        return reconditionCommand;
    }

    public void setReconditionCommand(ReconditionCommand reconditionCommand) {
        this.reconditionCommand = reconditionCommand;
    }

    public MrsBP getMrsBP() {
        return mrsBP;
    }

    public void setMrsBP(MrsBP mrsBP) {
        this.mrsBP = mrsBP;
    }

    public VehicleBP getVehicleBP() {
        return vehicleBP;
    }

    public void setVehicleBP(VehicleBP vehicleBP) {
        this.vehicleBP = vehicleBP;
    }

    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        binder.closeNoCatch();  initialize(request);

    }

    public ModelAndView handleRcQueue(HttpServletRequest request, HttpServletResponse response) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String menuPath = "Stores >> Recondition >> RcQueue ";
        String pageTitle = "RC QUEUE";
        request.setAttribute("pageTitle", pageTitle);
        String companyId = (String) session.getAttribute("companyId");
        request.setAttribute("companyId", companyId);

        String path = "/content/recondition/rcQueue.jsp";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList rcQueue = new ArrayList();
        ArrayList vendorList = new ArrayList();
        ArrayList availableRc = new ArrayList();
        ArrayList previousRcs = new ArrayList();
        ReconditionTO reconditionTO = new ReconditionTO();
        reconditionTO.setCompanyId(companyId);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Recondition-RcQueue")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                rcQueue = reconditionBP.processRcQueue(reconditionTO);
                //availableRc = reconditionBP.processAvailableRcItems(Integer.parseInt(companyId));
                previousRcs = reconditionBP.processVehicleRcRtItems(Integer.parseInt(companyId));
                vendorList = purchaseBP.processVendorList(1012);
                //////System.out.println("vendorList" + vendorList.size());
                request.setAttribute("rcQueue", rcQueue);
                request.setAttribute("previousRcs", previousRcs);
                //request.setAttribute("rcItems",availableRc);
                request.setAttribute("vendorList", vendorList);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception in handleGenerateMpr --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to handleGenerateMpr --> " + exception);
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleRcGoodsTransfer(HttpServletRequest request, HttpServletResponse response) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String menuPath = "Stores >> Recondition >> RC Goods Transfer";
        String pageTitle = "RC QUEUE";
        request.setAttribute("pageTitle", pageTitle);
        String companyId = (String) session.getAttribute("companyId");
        String path = "/content/recondition/rcTransfer.jsp";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList rcQueue = new ArrayList();
        ReconditionTO reconditionTO = new ReconditionTO();
        try {
             setLocale(request, response);

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Recondition-RcQueue")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                rcQueue = reconditionBP.processRcGoodsTransfer(Integer.parseInt(companyId));
                //availableRc = reconditionBP.processAvailableRcItems(Integer.parseInt(companyId));
                //////System.out.println("vendorList" + rcQueue.size());
                request.setAttribute("rcQueue", rcQueue);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception in handleGenerateMpr --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to handleGenerateMpr --> " + exception);
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleSetSenderStatus(HttpServletRequest request, HttpServletResponse response) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String menuPath = "Stores >> Recondition >> RC Goods Transfer";
        String pageTitle = "RC QUEUE";
        request.setAttribute("pageTitle", pageTitle);
        String companyId = (String) session.getAttribute("companyId");
        String path = "/content/recondition/rcTransfer.jsp";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList rcQueue = new ArrayList();
        ReconditionTO reconditionTO = new ReconditionTO();
        String rcQueueId[], selectedIndex[], sentStatus;
        rcQueueId = request.getParameterValues("rcQueueIds");
        selectedIndex = request.getParameterValues("selectedIndex");
        sentStatus = "Y";
        int index = 0;

        int status = 0;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Recondition-RcQueue")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                for (int i = 0; i < selectedIndex.length; i++) {
                    index = Integer.parseInt(selectedIndex[i]);
                    status = reconditionBP.processSenderStatus(Integer.parseInt(rcQueueId[index]), sentStatus);
                }

                rcQueue = reconditionBP.processRcGoodsTransfer(Integer.parseInt(companyId));
                //availableRc = reconditionBP.processAvailableRcItems(Integer.parseInt(companyId));
                //////System.out.println("vendorList" + rcQueue.size());
                request.setAttribute("rcQueue", rcQueue);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception in handleGenerateMpr --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to handleGenerateMpr --> " + exception);
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleScrapRc(HttpServletRequest request, HttpServletResponse response, ReconditionCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        reconditionCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        request.setAttribute("companyId", companyId);
        String gdType = "RC";
        String menuPath = "Stores >> Recondition >> RcQueue  ";
        String pageTitle = "RC QUEUE";
        request.setAttribute("pageTitle", pageTitle);
        String path = "/content/recondition/rcQueue.jsp";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList rcItems = new ArrayList();
        ArrayList rcQueue = new ArrayList();
        ArrayList previousRcs = new ArrayList();
        ArrayList vendorList = new ArrayList();
        ReconditionTO reconditionTO = new ReconditionTO();
        ReconditionTO reconTO = new ReconditionTO();
        reconTO.setCompanyId(String.valueOf(companyId));
        String[] catId = request.getParameterValues("categoryId");
        reconTO.setRemarks(reconditionCommand.getRemarks());
        reconTO.setGdType(gdType);
        int selectedInd = 0;
        try {
            for (int i = 0; i < reconditionCommand.getSelectedIndex().length; i++) {
                reconditionTO = new ReconditionTO();
                selectedInd = Integer.parseInt(reconditionCommand.getSelectedIndex()[i]);
                reconditionTO.setItemId(Integer.parseInt(reconditionCommand.getItemIds()[selectedInd]));
                reconditionTO.setVendorId((reconditionCommand.getVendorId()));
                reconditionTO.setRcNumber((reconditionCommand.getRcNumbers()[selectedInd]));
                reconditionTO.setRcQueueId((reconditionCommand.getRcQueueIds()[selectedInd]));
                rcItems.add(reconditionTO);
            }

            previousRcs = reconditionBP.processVehicleRcRtItems(companyId);
            vendorList = purchaseBP.processVendorList(1012);
            request.setAttribute("previousRcs", previousRcs);
            request.setAttribute("vendorList", vendorList);
            int status = reconditionBP.processScrapRc(rcItems, companyId, userId);
            rcQueue = reconditionBP.processRcQueue(reconTO);
            request.setAttribute("rcQueue", rcQueue);
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "RcItem  Scrapped  Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception in handleGenerateRcWo --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to handleGenerateRcWo --> " + exception);
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleGenerateRcWo(HttpServletRequest request, HttpServletResponse response, ReconditionCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        reconditionCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        request.setAttribute("companyId", companyId);
        String gdType = "RC";
        String menuPath = "Stores >> Recondition >> RcQueue ";
        //Rc Zone
        String jobCardId = "0";
        String customerType = "1";
        String customerName = "-";
        String customerAddress = "-";
        String mobileNo = "-";
        //Hari End
        String pageTitle = "RC QUEUE";
        request.setAttribute("pageTitle", pageTitle);
        String path = "/content/recondition/rcQueue.jsp";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList rcItems = new ArrayList();
        ArrayList rcQueue = new ArrayList();
        ArrayList previousRcs = new ArrayList();
        ArrayList vendorList = new ArrayList();
        ReconditionTO reconditionTO = new ReconditionTO();
        ReconditionTO reconTO = new ReconditionTO();
        reconTO.setCompanyId(String.valueOf(companyId));
        reconTO.setRemarks(reconditionCommand.getRemarks());
        reconTO.setGdType(gdType);
        //////System.out.println("remarks" + reconTO.getRemarks());
        String[] categoryId = request.getParameterValues("categoryId");

        int selectedInd = 0;
        try {
            for (int i = 0; i < reconditionCommand.getSelectedIndex().length; i++) {
                reconditionTO = new ReconditionTO();
                selectedInd = Integer.parseInt(reconditionCommand.getSelectedIndex()[i]);
                reconditionTO.setItemId(Integer.parseInt(reconditionCommand.getItemIds()[selectedInd]));
                reconditionTO.setVendorId((reconditionCommand.getVendorId()));
                reconditionTO.setRcNumber((reconditionCommand.getRcNumbers()[selectedInd]));
                reconditionTO.setRcQueueId((reconditionCommand.getRcQueueIds()[selectedInd]));
                reconditionTO.setCategoryId((categoryId[selectedInd]));
                //////System.out.println("categoryId=" + categoryId[selectedInd]);
                rcItems.add(reconditionTO);
            }
            reconditionBP.processGenerateRcWo(rcItems,jobCardId,customerType,customerName,customerAddress,mobileNo,companyId, userId, reconTO);
            rcQueue = reconditionBP.processRcQueue(reconTO);
//            availableRc = reconditionBP.processAvailableRcItems(companyId);
            previousRcs = reconditionBP.processVehicleRcRtItems(companyId);
            vendorList = purchaseBP.processVendorList(1012);
            request.setAttribute("rcQueue", rcQueue);
            request.setAttribute("previousRcs", previousRcs);
            request.setAttribute("vendorList", vendorList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception in handleGenerateRcWo --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to handleGenerateRcWo --> " + exception);
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleUpdateRcQueueRcs(HttpServletRequest request, HttpServletResponse response, ReconditionCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        reconditionCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        request.setAttribute("companyId", companyId);
        String gdType = "RC";
        String menuPath = "Stores >> Recondition >> RcQueue ";
        String pageTitle = "RC QUEUE";
        request.setAttribute("pageTitle", pageTitle);
        String path = "/content/recondition/rcQueue.jsp";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ArrayList rcItems = new ArrayList();
        ArrayList rcQueue = new ArrayList();
        ArrayList previousRcs = new ArrayList();
        ArrayList vendorList = new ArrayList();
        ReconditionTO reconditionTO = new ReconditionTO();
        ReconditionTO reconTO = new ReconditionTO();
        reconTO.setRemarks(reconditionCommand.getRemarks());
        reconTO.setCompanyId(String.valueOf(companyId));
        reconTO.setGdType(gdType);
        //////System.out.println("remarks" + reconTO.getRemarks());
        int queueId = 0;
        int rcItemId = 0;

        int selectedInd = 0;
        try {
            for (int i = 0; i < reconditionCommand.getSelectedIndex().length; i++) {
                selectedInd = Integer.parseInt(reconditionCommand.getSelectedIndex()[i]);
                rcItemId = Integer.parseInt(reconditionCommand.getRcNo()[selectedInd]);
                queueId = Integer.parseInt(reconditionCommand.getRcQueueIds()[selectedInd]);
                //////System.out.println("selectedInd=" + selectedInd);
                //////System.out.println("rcitemId=" + rcItemId);
                //////System.out.println("rcQueueId=" + queueId);
                reconditionBP.processAvailableRcItems(rcItemId, queueId);
            }
            rcQueue = reconditionBP.processRcQueue(reconTO);
//            availableRc = reconditionBP.processAvailableRcItems(companyId);
            previousRcs = reconditionBP.processVehicleRcRtItems(companyId);
            vendorList = purchaseBP.processVendorList(1012);
            request.setAttribute("rcQueue", rcQueue);
            request.setAttribute("previousRcs", previousRcs);
            request.setAttribute("vendorList", vendorList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception in handleUpdateRcQueueRcs --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to handleUpdateRcQueueRcs --> " + exception);
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleGenerateRcItem(HttpServletRequest request, HttpServletResponse response, ReconditionCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        reconditionCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        request.setAttribute("companyId", companyId);
        ArrayList rcItems = new ArrayList();
        ArrayList rcQueue = new ArrayList();
        ArrayList previousRcs = new ArrayList();
        ArrayList vendorList = new ArrayList();
        String menuPath = "Stores >> Recondition >> RcQueue ";
        String pageTitle = "RC QUEUE";
        request.setAttribute("pageTitle", pageTitle);
        String path = "/content/recondition/rcQueue.jsp";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        ReconditionTO reconditionTO = new ReconditionTO();

        int rcItemId = 0;

        int selectedInd = 0;
        try {
            String itemId = request.getParameter("itemId");
            //////System.out.println("rcQueueId = " + request.getParameter("rcQueueId"));
            int queueId = Integer.parseInt(request.getParameter("rcQueueId"));
            String availability = "N";
            String status = "Y";

            reconditionTO.setItemId(Integer.parseInt(itemId));
            reconditionTO.setAvailability(availability);
            reconditionTO.setCompanyId(String.valueOf(companyId));
            reconditionTO.setStatus(status);


            rcItemId = reconditionBP.processGenerateRcItemId(reconditionTO, userId);
            //////System.out.println("generated rcId = " + rcItemId);
            reconditionBP.processAvailableRcItems(rcItemId, queueId);

            rcQueue = reconditionBP.processRcQueue(reconditionTO);
            previousRcs = reconditionBP.processVehicleRcRtItems(companyId);
//            availableRc = reconditionBP.processAvailableRcItems(companyId);
            vendorList = purchaseBP.processVendorList(1012);
            request.setAttribute("rcQueue", rcQueue);
            request.setAttribute("previousRcs", previousRcs);
            request.setAttribute("vendorList", vendorList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception in handleUpdateRcQueueRcs --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to handleUpdateRcQueueRcs --> " + exception);
        }
        return new ModelAndView(path);
    }

    /**
     *  RC Receive WO
     *
     * @param 	request - Http request object
     *
     * @param 	response - Http response object
     *
     * @param	command - JobCommand object contains the request values.
     *
     * @return ModelAndView
     */
    public ModelAndView handleRcVendorList(HttpServletRequest request, HttpServletResponse response, ReconditionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList vendorList = new ArrayList();
        HttpSession session = request.getSession();
        reconditionCommand = command;
        //////System.out.println("view RC ");
        String menuPath = "";
        String pageTitle = "Receive InVoice";
        menuPath = "Stores  >>Receive InVoice ";
        request.setAttribute("pageTitle", pageTitle);
        int vendorType = 1012;
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
             setLocale(request, response);

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //////System.out.println("size is  " + userFunctions.size());
            if (!loginBP.checkAuthorisation(userFunctions, "Recondition-ReceiveWO")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                path = "content/recondition/receiveRcParts.jsp";
                vendorList = purchaseBP.processVendorList(vendorType);
                request.setAttribute("vendorLists", vendorList);
                //////System.out.println("vendorList size" + vendorList.size());
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewvendorList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void handleWorkOrderList(HttpServletRequest request, HttpServletResponse response, ReconditionCommand command) throws IOException {
        //////System.out.println("i am in ajax ");
        reconditionCommand = command;
        ReconditionTO reconditionTO = new ReconditionTO();
        HttpSession session = request.getSession();
        String companyId = "";
        companyId = ((String) session.getAttribute("companyId"));
        //////System.out.println("request values=" + request);
        String vendorId = ((String) request.getParameter("vendorId"));
        String woList = "";
        ArrayList list = new ArrayList();
        PrintWriter pw = response.getWriter();

        try {
            String mfrId = "";
            String fleetTypeId = "";
            response.setContentType("text/html");
            mfrId = request.getParameter("mfrId");
            fleetTypeId = request.getParameter("fleetTypeId");
            reconditionTO.setVendorId(vendorId);
            reconditionTO.setCompanyId(companyId);
            list = reconditionBP.getWoList(reconditionTO);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = list.iterator();
            ReconditionTO rcTO = new ReconditionTO();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                rcTO = (ReconditionTO) itr.next();
                jsonObject.put("Id", rcTO.getWoId());
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve PriceList data in Ajax --> " + exception);
        }
    }

    public ModelAndView handleWorkOrderItems(HttpServletRequest request, HttpServletResponse response, ReconditionCommand command) {
        //////System.out.println("request:" + request);
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        //////System.out.println("view InVoice");
        String path = "";
        ArrayList itemList = new ArrayList();
        ArrayList vendorList = new ArrayList();
        ArrayList vatList = new ArrayList();
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        reconditionCommand = command;
        ReconditionTO reconditionTO = new ReconditionTO();
        String menuPath = "";
        int vendorType = 1012;
        int status = 0;
        String pageTitle = "Receive Rc Items";
        menuPath = "Stores  >>Reconditioning >>Receive Rc";
        request.setAttribute("pageTitle", pageTitle);
        //////System.out.println("woId" + request.getParameter("woIds"));
        //////System.out.println("vendorId" + request.getParameter("vendorId"));
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            
             setLocale(request, response);

            // ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            ////////System.out.println("size is  " + userFunctions.size());
            /* if (!loginBP.checkAuthorisation(userFunctions, "Contract-Details")) {
            path = "content/common/NotAuthorized.jsp";
            } else { */
            reconditionTO.setVendorId(request.getParameter("vendorId"));
            reconditionTO.setWoId(request.getParameter("woIds"));
            path = "content/recondition/receiveRcParts.jsp";
            vendorList = purchaseBP.processVendorList(vendorType);
            request.setAttribute("vendorLists", vendorList);
            itemList = reconditionBP.processItemList(reconditionTO);
            vatList = purchaseBP.processActiveVatList();
            request.setAttribute("ItemList", itemList);
            //////System.out.println("itemList size" + itemList.size());
            request.setAttribute("woIds", request.getParameter("woIds"));
            request.setAttribute("vatList", vatList);
            request.setAttribute("vendorId", request.getParameter("vendorId"));
            request.setAttribute("inVoiceNo", request.getParameter("remarks"));
            request.setAttribute("receivedDate", request.getParameter("receivedDate"));
            request.setAttribute("billAmount", request.getParameter("billAmount"));

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view RC ItemList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleSaveRcItems(HttpServletRequest request, HttpServletResponse response, ReconditionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        //////System.out.println("req=" + request);
        HttpSession session = request.getSession();
        String companyId = "";
        companyId = ((String) session.getAttribute("companyId"));
        //////System.out.println("companyId" + companyId);
        reconditionCommand = command;
        ArrayList List = new ArrayList();
        int vendorType = 1012;
        float rcPriceWithOutTax = 0;
        String path = "";
        try {
            String menuPath = "Stores  >>  Receive RC Wo";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //       if (!loginBP.checkAuthorisation(userFunctions, "Grade-Modify")) {
            //          path = "content/common/NotAuthorized.jsp";
            //       } else {
            //////System.out.println("in RC items insert");
            int index = 0;
            int insert = 0;
            ReconditionTO reconditionTO = new ReconditionTO();
            String[] itemIds = reconditionCommand.getItemIds();
            //////System.out.println("reconditionCommand.getItemIds()" + reconditionCommand.getItemIds());

            String[] taxs = reconditionCommand.getVats();
            String[] spareAmounts = reconditionCommand.getSparesAmounts();
            String[] spareAmountsExternal = reconditionCommand.getSparesAmountsExternal();
            String[] spareWithTax = reconditionCommand.getSparesWithTaxs();
            String[] laborAmnts = reconditionCommand.getLaborAmounts();
            String[] itemAmounts = reconditionCommand.getItemAmounts();
            String[] rcStatuses = reconditionCommand.getRcStatuses();
            String[] selectedIndex = reconditionCommand.getSelectedIndex();
            String[] categoryId = reconditionCommand.getCategoryIds();

            String[] rcItemIds = reconditionCommand.getRcItemIds();
            String[] tyreIds = request.getParameterValues("tyreId");
            reconditionTO.setCompanyId(companyId);
            //////System.out.println("reconditionCommand.getBillAmount()" + reconditionCommand.getBillAmount());
            //////System.out.println("reconditionCommand.getReceivedDate()" + reconditionCommand.getReceivedDate());
            reconditionTO.setBillAmount(reconditionCommand.getBillAmount());
            reconditionTO.setReceivedDate(reconditionCommand.getReceivedDate());
            reconditionTO.setRemarks(reconditionCommand.getRemarks());
            reconditionTO.setWoId(request.getParameter("woIds"));
            //////System.out.println("woid" + reconditionTO.getWoId());
            ReconditionTO listTO = null;
            for (int i = 0; i < selectedIndex.length; i++) {
                listTO = new ReconditionTO();
                index = Integer.parseInt(selectedIndex[i]);
                listTO.setItemId(Integer.parseInt(itemIds[index]));
                //////System.out.println("itemId" + listTO.getItemId());
                listTO.setAmount(itemAmounts[index]);
                //////System.out.println("itemAmount" + listTO.getAmount());
                //////System.out.println("rcStatuses[index]" + rcStatuses[index]);
                listTO.setSparesAmount(spareAmounts[index]);
                listTO.setSparesAmountExternal(spareAmountsExternal[index]);
                listTO.setLaborAmount(laborAmnts[index]);
                listTO.setRcstatus(rcStatuses[index]);
                listTO.setRcItemId(rcItemIds[index]);
                listTO.setCategoryId(categoryId[index]);
                listTO.setTax(taxs[index]);
                listTO.setSparesWithTax(spareWithTax[index]);
                listTO.setCompanyId(companyId);
                listTO.setTyreId(tyreIds[index]);

                // Price For RcItem Excluding Tax From Material Cost
                rcPriceWithOutTax = Float.parseFloat(listTO.getSparesAmount())+Float.parseFloat(listTO.getSparesAmountExternal()) + Float.parseFloat(listTO.getLaborAmount());
                //////System.out.println("rcPriceWithOutTax"+rcPriceWithOutTax);
                listTO.setRcAmntWithOutTax(String.valueOf(rcPriceWithOutTax));


                listTO.setStatus("Y");  // used to set Is_RT status
                listTO.setStkStatus("Y");  // used to set Stock Status
                if (categoryId[i].equals("1011")) {
                    //////System.out.println("categoryId=" + categoryId[i]);
                    reconditionBP.processVehicleRcRtItems(listTO);
                }
                List.add(listTO);
            }
            int userId = (Integer) session.getAttribute("userId");
            //////System.out.println("userId" + userId);
            path = "content/recondition/receiveRcParts.jsp";
            insert = reconditionBP.processInsertItem(List, userId, reconditionTO);
            insert = reconditionBP.processUpdateRcPrice(List);
            String pageTitle = "Receive Invoice";
            request.setAttribute("pageTitle", pageTitle);
            ArrayList vendorList = new ArrayList();
            vendorList = purchaseBP.processVendorList(vendorType);
            request.setAttribute("vendorLists", vendorList);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Rc InVoice Details Added  Successfully");
            //      }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to update data  --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAddRtTyre(HttpServletRequest request, HttpServletResponse response) {
        //////System.out.println("request:" + request);
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "content/recondition/refreshPag.jsp";
        HttpSession session = request.getSession();
        ReconditionTO tyreTO = new ReconditionTO();
        int userId = (Integer) session.getAttribute("userId");
        int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String menuPath = "";

        String pageTitle = "Receive Rc Items";
        menuPath = "Stores  >>Reconditioning >>Receive Rc";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            // ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            ////////System.out.println("size is  " + userFunctions.size());
            /* if (!loginBP.checkAuthorisation(userFunctions, "Contract-Details")) {
            path = "content/common/NotAuthorized.jsp";
            } else { */
            int rcQueueId = Integer.parseInt(request.getParameter("rcQueueId"));
            int itemId = Integer.parseInt(request.getParameter("itemId"));
            int tyreId = 0;
            String rcItemId = request.getParameter("tyreNo");
            String tyreExists = request.getParameter("tyreExists");
            tyreTO.setTyreNo(String.valueOf(rcItemId));
            tyreTO.setItemId(itemId);
            tyreTO.setRcQueueId(String.valueOf(rcQueueId));
            tyreTO.setRcItemId(String.valueOf(rcItemId));

            if (tyreExists.equalsIgnoreCase("Y")) {
                tyreId = Integer.parseInt(rcItemId);
                //////System.out.println("Have This Tyre No ALready Tyre Id-->" + tyreId + "Rc QueueId-->" + rcQueueId);
                reconditionBP.processAvailableRcItems(tyreId, rcQueueId);
            } else {
                reconditionBP.processRcTyreUpdate(tyreTO, userId, companyId);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view RC ItemList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void handleGetTyreNo(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        int companyId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String tyreNo = "";

        try {
            String tyreId = (String) request.getParameter("tyreId");
            tyreNo = reconditionBP.processTyreNo(tyreId, companyId);
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(tyreNo);
            writer.close();
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve PriceList data in Ajax --> " + exception);
        }
    }

    public void handleGetTyreNoVehicleName(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        int companyId = 0;
        companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String tyreNo = "";

        try {
            String tyreId = (String) request.getParameter("tyreId");
            tyreNo = reconditionBP.processTyreNo(tyreId, companyId);
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(tyreNo);
            writer.close();
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve PriceList data in Ajax --> " + exception);
        }
    }

    public ModelAndView handleFaultItemPage(HttpServletRequest request, HttpServletResponse response, ReconditionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        ArrayList vendorList = new ArrayList();
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "Reports >>Stores >> Fault Items";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/recondition/reconditionItems.jsp";
            vendorList = purchaseBP.processVendorList(1012);
            request.setAttribute("vendorList", vendorList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleFaultItems(HttpServletRequest request, HttpServletResponse response, ReconditionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";        
        ArrayList vendorList = new ArrayList();
        ArrayList jobCardList = new ArrayList();
        ArrayList customerTypeList = new ArrayList();
        HttpSession session = request.getSession();
        int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String menuPath = "";
        String pageTitle = "Fault Items";
        String vendorId = "";
        menuPath = "Recondition >>Fault Items";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        reconditionCommand = command;
        try {
             setLocale(request, response);

            // ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            ////////System.out.println("size is  " + userFunctions.size());
            /* if (!loginBP.checkAuthorisation(userFunctions, "Contract-Details")) {
            path = "content/common/NotAuthorized.jsp";
            } else { */
            int userId = (Integer) session.getAttribute("userId");
            String[] temp1 = request.getParameterValues("hiddenElement1");
            String[] temp2 = request.getParameterValues("hiddenElement2");
            int itemId = 0;
            int qty = 0;
            int status = 0;
            int woNo = 0;

            //Hari Rc Zone
            String jobCardId = "0";
            String customerType = "1";
            String customerName = "-";
            String customerAddress = "-";
            String mobileNo = "-";

            ////////System.out.println("ReqFor"+request.getParameter("reqFor"));

            if (request.getParameter("reqFor") != null) {

                vendorId = "1232";
                jobCardId = command.getJobCardId();
                if(command.getCustomerName().equals("null") && !"".equals(command.getCustomerName()))
                {
                customerName = command.getCustomerName();
                }
                if(command.getCustomerTypeId().equals("null") && !"".equals(command.getCustomerTypeId()))
                {
                customerType = command.getCustomerTypeId();
                }
                if(command.getCustomerAddress().equals("null") && !"".equals(command.getCustomerAddress()))
                {
                customerAddress = command.getCustomerAddress();
                }
                if(command.getMobileNo().equals("null") && !"".equals(command.getMobileNo()))
                {
                mobileNo = command.getMobileNo();
                }

                //////System.out.println("Job" + jobCardId + "Name" + customerName + "Address" + customerAddress
//                        + "Mobil" + mobileNo + "custType" + customerType
//                        +"item Name length"+temp1.length+"Qty length"+temp2.length);

                path = "content/rcZone/generateRcWo.jsp";

                customerTypeList = reconditionBP.getCustomerTypeList();
                jobCardList = reconditionBP.getJobCardList();

                request.setAttribute("customerTypeList", customerTypeList);
                request.setAttribute("jobCardList", jobCardList);

            } else {
                vendorId = request.getParameter("vendorId");
                path = "content/recondition/reconditionItems.jsp";
            }

            vendorList = purchaseBP.processVendorList(1012);
            request.setAttribute("vendorList", vendorList);
            woNo = reconditionBP.processRcWoNo(companyId, userId, vendorId,jobCardId,customerType,customerName,customerAddress,mobileNo, reconditionCommand.getRemarks());
            for (int i = 0; i < temp1.length; i++) {
                if ((request.getParameter(temp1[i]) != null) && (!request.getParameter(temp1[i]).equals(""))) {
                    itemId = Integer.parseInt(request.getParameter(temp1[i]));
                   // //////System.out.println("itemid=" + itemId);
                    qty = (Integer.parseInt(request.getParameter(temp2[i])));
                   // //////System.out.println("qty=" + qty);
                    status = reconditionBP.processFaultItems(itemId, qty, woNo, companyId, userId);
                }
            }
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Fault Items Added Successfully");

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Issue --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void getItemSuggestions(HttpServletRequest request, HttpServletResponse response) throws IOException {

        HttpSession session = request.getSession();

        // ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        //if (!loginBP.checkAuthorisation(userFunctions, "Designation-ViewPage")) {
        //  path = "content/common/NotAuthorized.jsp";
        //} else {

        String mfrCode = request.getParameter("mfrCode");
        String itemCode = request.getParameter("itemCode");
        String itemName = request.getParameter("itemName");

        //////System.out.println("mfrCode  " + mfrCode);
        //////System.out.println("itemCode  " + itemCode);
        //////System.out.println("itemName  " + itemName);
        int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String suggestions = reconditionBP.getItemSuggestions(mfrCode, itemCode, itemName, companyId);
        //////System.out.println("suggestions" + suggestions);
        PrintWriter writer = response.getWriter();
        System.out.println(suggestions);
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.print(suggestions);
        writer.close();

    }

    public void getItemSuggestionsToScrap(HttpServletRequest request, HttpServletResponse response) throws IOException {

        HttpSession session = request.getSession();

        // ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        //if (!loginBP.checkAuthorisation(userFunctions, "Designation-ViewPage")) {
        //  path = "content/common/NotAuthorized.jsp";
        //} else {

        String itemCode = request.getParameter("itemCode");
        String itemName = request.getParameter("itemName");
         setLocale(request, response);


        //////System.out.println("itemCode  " + itemCode);
        //////System.out.println("itemName  " + itemName);
//        int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String suggestions = reconditionBP.getItemSuggestionsToScrap(itemCode, itemName);
        //////System.out.println("suggestions" + suggestions);
        PrintWriter writer = response.getWriter();
        System.out.println(suggestions);
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.print(suggestions);
        writer.close();
    }

    public ModelAndView handleAddScrapItemsPage(HttpServletRequest request, HttpServletResponse response, ReconditionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
         setLocale(request, response);

        String path = "content/scrap/scrapItems.jsp";
        HttpSession session = request.getSession();
        int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String menuPath = "";
        String pageTitle = "Scraps";
        menuPath = "Scrap >> Scrap Request";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        return new ModelAndView(path);
    }

    public ModelAndView handleAddScrapItems(HttpServletRequest request, HttpServletResponse response, ReconditionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "content/scrap/scrapItems.jsp";
        HttpSession session = request.getSession();
        int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String menuPath = "";
        String pageTitle = "Scraps";
        menuPath = "Scrap >> Scrap Request";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        reconditionCommand = command;
        try {
            // ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            ////////System.out.println("size is  " + userFunctions.size());
            /* if (!loginBP.checkAuthorisation(userFunctions, "Contract-Details")) {
            path = "content/common/NotAuthorized.jsp";
            } else { */
             setLocale(request, response);

            int userId = (Integer) session.getAttribute("userId");
            String vendorId = request.getParameter("vendorId");
            String[] temp1 = request.getParameterValues("hiddenElement1");
            String[] temp2 = request.getParameterValues("hiddenElement2");
            String[] temp3 = request.getParameterValues("hiddenElement3");
            int itemId = 0;
            int qty = 0;
            int priceId = 0;
            int status = 0;
            int woNo = 0;
            path = "content/scrap/scrapItems.jsp";


            for (int i = 0; i < temp1.length; i++) {
                if ((request.getParameter(temp1[i]) != null) && (!request.getParameter(temp1[i]).equals(""))) {
                    itemId = Integer.parseInt(request.getParameter(temp1[i]));
                    qty = (Integer.parseInt(request.getParameter(temp2[i])));
                    priceId = (Integer.parseInt(request.getParameter(temp3[i])));
                    //////System.out.println("itemid=" + itemId + "--qty=" + qty + "--PriceId=" + priceId);
                    status = reconditionBP.addScrapItems(itemId, qty, priceId, userId);
                }
            }
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Items Scrapped Added Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Issue --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAlterRCWOPage(HttpServletRequest request, HttpServletResponse response, ReconditionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList RCWODetail = new ArrayList();
        ArrayList vendorList = new ArrayList();
        ReconditionCommand reportCommand = command;
        String path = "";
        String menuPath = "Reports >>Stores >> RC WO Report";
        try {
             setLocale(request, response);

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/workOrder/alterWO.jsp";
            ReconditionTO recTO = new ReconditionTO();
            recTO.setWoId(reportCommand.getWoId());
            RCWODetail = reconditionBP.processRCWOItems(recTO);
            vendorList = purchaseBP.processVendorList(1012);
            request.setAttribute("woDetail", RCWODetail);
            request.setAttribute("vendorList", vendorList);
            //////System.out.println("woDetail=" + RCWODetail.size());
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAlterRCWO(HttpServletRequest request, HttpServletResponse response, ReconditionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList RCWODetail = new ArrayList();
        ArrayList vendorList = new ArrayList();
        String path = "";
        String menuPath = "Reports >>Stores >> RC WO Report";
        ModelAndView mv = new ModelAndView();
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ReconditionTO recTO = new ReconditionTO();
            recTO.setWoId(command.getWoId());
            recTO.setVendorId(command.getVendorId());
            recTO.setCreatedDate(command.getCreatedDate());
            recTO.setRemarks(command.getRemarks());
            int stat = reconditionBP.updateRcWo(recTO);
            if (stat == 1) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Work order details modified successfully");
            }
            mv = handleAlterRCWOPage(request, response, command);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView handleFaultTyreItems(HttpServletRequest request, HttpServletResponse response, ReconditionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        ArrayList vendorList = new ArrayList();
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "Reports >>Stores >> Fault Tyre Items";
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/recondition/reconditionTyreItems.jsp";
            ArrayList tyreItemList = new ArrayList();
            tyreItemList = vehicleBP.processTyreItems();
            request.setAttribute("tyreItemList", tyreItemList);

            ArrayList positionList = new ArrayList();
            positionList = mrsBP.getTyrePostions();
            request.setAttribute("positionList", positionList);
            ArrayList vendorList1 = new ArrayList();
            vendorList1 = purchaseBP.processVendorList(1012);
            request.setAttribute("vendorList", vendorList1);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAddTyreFaultItems(HttpServletRequest request, HttpServletResponse response, ReconditionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        ModelAndView mv = null;
        String path = "";
        int status = 0;
        HttpSession session = request.getSession();
        int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String menuPath = "";
        String pageTitle = "Fault Items";
        //Rc Zone
        String jobCardId = "0";
        String customerType = "1";
        String customerName = "-";
        String customerAddress = "-";
        String mobileNo = "-";
        //Hari End
        menuPath = "Recondition >>Fault Tyre Items";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        reconditionCommand = command;
        MrsTO mrsTO = null;
        ReconditionTO reconditionTO = null;
        try {
            // ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            ////////System.out.println("size is  " + userFunctions.size());
            /* if (!loginBP.checkAuthorisation(userFunctions, "Contract-Details")) {
            path = "content/common/NotAuthorized.jsp";
            } else { */
            int userId = (Integer) session.getAttribute("userId");
            int rcItemId = 0;
            ArrayList rcItems = new ArrayList();
            ArrayList TyreList = new ArrayList();
//            reconditionCommand.getTyreIds();
//            reconditionCommand.getPositionIds();
//            reconditionCommand.getItemIds();
//            reconditionCommand.getRegNo();

            path = "content/recondition/reconditionTyreItems.jsp";
            //insert  tyre details
            TyreList = reconditionBP.doFalutTyreItems(reconditionCommand.getRegNo(), reconditionCommand.getTyreIds(), reconditionCommand.getPositionIds(), reconditionCommand.getItemIds(), companyId, userId);
            //insert in rc queue
            VehicleTO vehicleTO = null;
            String availability = "N";
            String status1 = "Y";

            String[] ItemIds = reconditionCommand.getItemIds();
            Iterator itr = TyreList.iterator();
            for (int i = 0; itr.hasNext(); i++) {
                vehicleTO = new VehicleTO();
                vehicleTO = (VehicleTO) itr.next();
                reconditionTO = new ReconditionTO();
                //////System.out.println("vijay tyreID" + vehicleTO.getTypeId());
                mrsTO = new MrsTO();
                mrsTO.setMrsItemId(ItemIds[i]);
                mrsTO.setMrsNumber("0");
                mrsTO.setCompanyId(companyId);
                mrsTO.setFaultQty("1");
                reconditionTO.setItemId(Integer.parseInt(ItemIds[i]));
                reconditionTO.setVendorId(reconditionCommand.getVendorId());
                reconditionTO.setAvailability(availability);
                reconditionTO.setCompanyId(String.valueOf(companyId));
                reconditionTO.setStatus(status1);

                // rcItemId = reconditionBP.processGenerateRcItemId(reconditionTO, userId);
                mrsTO.setRcItemId(vehicleTO.getTypeId());
                status = mrsBP.processInsertRcQueue(mrsTO, userId);
                reconditionTO.setRcNumber(vehicleTO.getTypeId());
                reconditionTO.setRcQueueId(String.valueOf(status));
                reconditionTO.setCategoryId("1011");
                rcItems.add(reconditionTO);
            }

            ReconditionTO reconTO = new ReconditionTO();
            reconTO.setCompanyId(String.valueOf(companyId));
            reconTO.setRemarks("R.T");
            reconTO.setGdType("RC");
            reconditionBP.processGenerateRcWo(rcItems,jobCardId,customerType,customerName,customerAddress,mobileNo, companyId, userId, reconTO);

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Fault tyre Items Added Successfully");
                mv = handleFaultTyreItems(request, response, command);

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Issue --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView handleGenerateRcMrs(HttpServletRequest request, HttpServletResponse response, ReconditionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";

        ArrayList rcWorkOrder = new ArrayList();
        HttpSession session = request.getSession();
        int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String menuPath = "";
        String pageTitle = "Genearte MRS";
        menuPath = "Reconditioning >>Generate MRS ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute("menuPath", menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            System.out.println((new StringBuilder()).append("size is  ").append(userFunctions.size()).toString());
            if (!loginBP.checkAuthorisation(userFunctions, "RenderService-GenerateMrs")) {
                path = "content/common/NotAuthorized.jsp";
            } else {


                path = "content/recondition/generateRcMrs.jsp";
                rcWorkOrder = reconditionBP.getRcWorkOrderList(companyId);
                request.setAttribute("rcWorkOrder", rcWorkOrder);
                //////System.out.println("In Recondition Controller-->" + rcWorkOrder.size());
            }

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to view jobcard --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleGenerateRcMrsPage(HttpServletRequest request, HttpServletResponse response, ReconditionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        int rcWorkId = 0;
        HttpSession session = request.getSession();
        ArrayList rcWorkOrderDetails = new ArrayList();
        ArrayList compTechnician = new ArrayList();
        ArrayList rcWOItems = new ArrayList();
        ReconditionTO reconditionTO = new ReconditionTO();
        //////System.out.println("CompanyId" + session.getAttribute("companyId"));
        int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String menuPath = "";
        String pageTitle = "Genearte MRS";
        menuPath = "Reconditioning >>Genearte MRS ";
        request.setAttribute("pageTitle", pageTitle);
        //////System.out.println("IN handleGenerateRcMrsPage ");
        request.setAttribute("menuPath", menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            System.out.println((new StringBuilder()).append("size is  ").append(userFunctions.size()).toString());
            if (!loginBP.checkAuthorisation(userFunctions, "RenderService-GenerateMrs")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                path = "content/recondition/generateRcMrsPage.jsp";
                if (command.getRcWorkId() != null && !"".equals(command.getRcWorkId())) {
                    rcWorkId = Integer.parseInt(command.getRcWorkId());
                    reconditionTO.setRcWorkId(Integer.parseInt(command.getRcWorkId()));
                    //////System.out.println("reconditionTO.getRcWorkId()" + reconditionTO.getRcWorkId());
                }
                rcWorkOrderDetails = reconditionBP.getRcWorkOrderDetails(rcWorkId, companyId);
                request.setAttribute("rcWorkOrderDetails", rcWorkOrderDetails);

                compTechnician = reconditionBP.getCompanyEmployee(companyId);
                request.setAttribute("compTechnician", compTechnician);

                rcWOItems = reconditionBP.getRcWOItems(rcWorkId);
                request.setAttribute("rcWOItems", rcWOItems);
                request.setAttribute("rcWorkId", rcWorkId);

                ArrayList tyrePostions = new ArrayList();
                tyrePostions = mrsBP.getTyrePostions();
                request.setAttribute("tyrePostions", tyrePostions);


                //////System.out.println("In Recondition Controller rcWorkOrderDetails-->" + rcWorkOrderDetails.size());
                //////System.out.println("In Recondition Controller rcWOItems-->" + rcWOItems.size());
                //////System.out.println("In Recondition Controller compTechnician-->" + compTechnician.size());


            }


        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to view jobcard --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
//Hari

    public ModelAndView handleViewAlterRcWoPage(HttpServletRequest request, HttpServletResponse response, ReconditionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Genearte MRS";
        menuPath = "Reconditioning >>Alter Rc WO ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute("menuPath", menuPath);
        //////System.out.println("handleViewAlterRcWoPage");
        try {
             setLocale(request, response);

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            System.out.println((new StringBuilder()).append("size is  ").append(userFunctions.size()).toString());
            if (!loginBP.checkAuthorisation(userFunctions, "RenderService-GenerateMrs")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                path = "/content/workOrder/getWo.jsp";
            }
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to view jobcard --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    //Hari End

    public ModelAndView handleInsertRcMrs(HttpServletRequest request, HttpServletResponse response, ReconditionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList jobCardlist = new ArrayList();
        ArrayList mrsList = new ArrayList();
        HttpSession session = request.getSession();
        int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String menuPath = "";
        String pageTitle = "Genearte MRS";
        menuPath = "Reconditioning  >> Genearte MRS ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute("menuPath", menuPath);
        try {
            path = "content/recondition/generateRcMrs.jsp";
            int userId = ((Integer) session.getAttribute("userId")).intValue();
            int rcWorkId = Integer.parseInt(request.getParameter("rcWorkId"));
            int technicianId = Integer.parseInt(request.getParameter("technicianId"));
            String remarks = request.getParameter("remarks");
            String manualMrsDate = request.getParameter("manualMrsDate");
            String temp1[] = request.getParameterValues("hiddenElement1");
            String temp2[] = request.getParameterValues("hiddenElement2");
            String position[] = request.getParameterValues("hiddenElement3");
            String rcItems[] = request.getParameterValues("hiddenElement4");
            int itemId = 0;
            float qty = 0.0F;
            int mrsId = 0;
            int status = 0;
            int posId = 0;
            int jobCardId = 0;
            int rcItem = 0;
            int counterId = 0;
            //////System.out.println("rcWorkId-->" + rcWorkId);
            //////System.out.println("technicianId-->" + technicianId);
            //////System.out.println("rcItems-->" + rcItems.length);

            mrsId = mrsBP.insertMRS(technicianId, jobCardId, rcWorkId,counterId,remarks,companyId,manualMrsDate, userId);
            System.out.println((new StringBuilder()).append("mrsId").append(mrsId).toString());
            for (int i = 0; i < temp1.length; i++) {
                System.out.println((new StringBuilder()).append("temp1[i]=").append(request.getParameter(temp1[i])).toString());
                System.out.println((new StringBuilder()).append("rcItems[i]=").append(request.getParameter(rcItems[i])).toString());
                if (request.getParameter(temp1[i]) != null && !request.getParameter(temp1[i]).equals("")) {
                    itemId = Integer.parseInt(request.getParameter(temp1[i]));
                    rcItem = Integer.parseInt(request.getParameter(rcItems[i]));
                    qty = Float.parseFloat(request.getParameter(temp2[i]));
                    posId = Integer.parseInt(request.getParameter(position[i]));
                    //////System.out.println("itemId[" + i + "]" + itemId);
                    //////System.out.println("rcItems[" + i + "]" + rcItem);
                    //////System.out.println("qty[" + i + "]" + qty);
                    //////System.out.println("posId[" + i + "]" + posId);
                    status = mrsBP.insertRcSpareDetails(rcWorkId, rcItem, itemId, userId, qty);
                    //////System.out.println("insertRcSpareDetails-->" + status);
                    status = mrsBP.insertMrsItems(itemId, qty, mrsId, posId, rcItem, userId);
                    //////System.out.println("insertMrsItems-->" + status);
                }
            }

            ArrayList rcWorkOrder = new ArrayList();
            rcWorkOrder = reconditionBP.getRcWorkOrderList(companyId);
            request.setAttribute("rcWorkOrder", rcWorkOrder);


            //jobCardlist = mrsBP.getJobCardList(companyId);
            //request.setAttribute("jobCardlist", jobCardlist);
            //request.setAttribute("mrsList", mrsList);
            request.setAttribute("successMessage", "MRS Generated Successfully");

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to view Issue --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleSearchRcMrs(HttpServletRequest request, HttpServletResponse response, ReconditionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList jobCardlist = new ArrayList();
        ArrayList rcMrsList = new ArrayList();
        ArrayList rcWorkOrder = new ArrayList();
        HttpSession session = request.getSession();
        int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String menuPath = "";
        String pageTitle = "Genearte MRS";
        menuPath = "Recondition >> Genearte MRS ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute("menuPath", menuPath);
        //////System.out.println("In handleSearchRcMrs");
        try {
            path = "content/recondition/generateRcMrs.jsp";
            int rcWorkId = Integer.parseInt(request.getParameter("rcWorkId"));
            rcMrsList = reconditionBP.getRcMrsList(rcWorkId);
            System.out.println((new StringBuilder()).append("mrsList vijay size").append(rcMrsList.size()).toString());

            rcWorkOrder = reconditionBP.getRcWorkOrderList(companyId);
            request.setAttribute("rcWorkOrder", rcWorkOrder);

            //////System.out.println("In Recondition Controller-->" + rcWorkOrder.size());
            //////System.out.println("In Recondition Controller rcMrsList-->" + rcMrsList.size());

            request.setAttribute("rcMrsList", rcMrsList);
            request.setAttribute("rcWorkId", Integer.valueOf(rcWorkId));

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to search mrs details --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleSearchRcMrsStatus(HttpServletRequest request, HttpServletResponse response, ReconditionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Genearte MRS";
        menuPath = "Reconditioning >> Genearte MRS ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute("menuPath", menuPath);
        try {
            String status = request.getParameter("status");
            if (status.equals("Approved") || status.equalsIgnoreCase("Rejected")) {
                path = "content/recondition/viewRcMrs.jsp";
            }
            int rcWorkId = Integer.parseInt(request.getParameter("rcWorkId"));
            int mrsId = Integer.parseInt(request.getParameter("mrsId"));
            int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
            ArrayList rcMrsDetails = new ArrayList();
            rcMrsDetails = mrsBP.getRcMrsItems(mrsId, status);


            ArrayList rcMrsWorkOrderDetails = new ArrayList();
            rcMrsWorkOrderDetails = reconditionBP.getRcMrsWorkOrderDetails(rcWorkId, companyId, mrsId);


            request.setAttribute("rcMrsWorkOrderDetails", rcMrsWorkOrderDetails);
            request.setAttribute("rcMrsDetails", rcMrsDetails);
            request.setAttribute("mrsId", Integer.valueOf(mrsId));

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to view Issue --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //Hari
    public ModelAndView handleModifyWOPage(HttpServletRequest request, HttpServletResponse response) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String menuPath = "Stores >> Recondition >> ModifyWO ";
        String pageTitle = "RC QUEUE";
        request.setAttribute("pageTitle", pageTitle);
        String path = "/content/recondition/getWO.jsp";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {
             setLocale(request, response);

            
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Recondition-RcQueue")) {
                path = "content/common/NotAuthorized.jsp";
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception in handleGenerateMpr --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to handleModifyWO --> " + exception);
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleWorkorderItemDetails(HttpServletRequest request, HttpServletResponse response) {


        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList vendorList = new ArrayList();
        String path = "";
        try {
             setLocale(request, response);

            if (!loginBP.checkAuthorisation(userFunctions, "Mfr-Alter")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                String menuPath = "Stores  >>  Recondition >>  Modify WO";
                ArrayList woItems = new ArrayList();
                woItems = reconditionBP.getWorkorderItems(Integer.parseInt(request.getParameter("woId")));
                if (woItems.size() != 0) {
                    path = "content/recondition/modifyWO.jsp";
                } else {
                    path = "content/recondition/getWO.jsp";
                    request.setAttribute("errorMessage", "Work order not found");
                }
                request.setAttribute("menuPath", menuPath);
                vendorList = purchaseBP.processVendorList(1012);
                String pageTitle = "Modify PO";
                request.setAttribute("woItems", woItems);
                request.setAttribute("pageTitle", pageTitle);
                request.setAttribute("vendorList", vendorList);
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception in handleGenerateMpr --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to handleModifyWO --> " + exception);
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleModifyWO(HttpServletRequest request, HttpServletResponse response, ReconditionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        int i = 0;
        int j = 0;
        int quantity = 0;
        int status = 0;
        path = "content/recondition/modifyWO.jsp";
        ModelAndView mv = new ModelAndView();

        ReconditionTO reconditionT = new ReconditionTO();
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList woItems = new ArrayList();
        ArrayList vendorList = new ArrayList();
        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Mfr-Alter")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                String menuPath = "Stores  >>  Purchase Order >>  Modify PO";
                request.setAttribute("menuPath", menuPath);

                int userId = (Integer) session.getAttribute("userId");
                String companyId = (String) session.getAttribute("companyId");
                String itemId[] = request.getParameterValues("itemId");
                String qty[] = request.getParameterValues("qty");
                String rcQueueIds[] = request.getParameterValues("rcQueueIds");
                String rcItemIds[] = request.getParameterValues("rcItemIds");

                reconditionT.setVendorId(command.getVendorId());
                reconditionT.setRemarks(command.getRemarks());
                reconditionT.setCreatedDate(command.getCreatedDate());
                reconditionT.setRcWorkId(Integer.parseInt(command.getRcWorkId()));
                //////System.out.println("Mrs Id from handleModifyWO-->" + request.getParameter("mrsId"));
                //reconditionT.setMrsId(Integer.parseInt(command.getMrsId()));
                reconditionT.setCompanyId(companyId);
                reconditionT.setUserId(userId);
                for (i = 0; i < rcItemIds.length; i++) {
                    //////System.out.println("ItemIds[" + i + "]" + itemId[i]);
                    //////System.out.println("Qty[" + i + "]" + qty[i]);
                    //////System.out.println("rcItemIds[" + i + "]" + rcItemIds[i]);
                }
                //////System.out.println("B4 Modify");
                status = reconditionBP.modifyWOHeader(reconditionT);
                //////System.out.println("A4 Modify");
                //////System.out.println("Item Id Length-->" + itemId.length);
                for (i = 0; i < itemId.length; i++) {

                    if (Integer.parseInt(rcItemIds[i]) == 0 && itemId[i] != null && !"".equals(itemId[i])) {
                        //////System.out.println("I Values is-->" + i);
                        //////System.out.println("ItemId-->" + itemId[i]);
                        //////System.out.println("Qty-->" + qty[i]);
                        //////System.out.println("RcItem Id-->" + rcItemIds[i]);
                        quantity = Integer.parseInt(qty[i]);
                        for (j = 0; j < quantity; j++) {
                            reconditionT.setItemId(Integer.parseInt(itemId[i]));
                            //////System.out.println("reconditionT.getItemId()" + reconditionT.getItemId());
                            status = reconditionBP.insertRCNewItem(reconditionT);
                            //////System.out.println("Item " + itemId[i] + "Inserted Successfully-->" + status);
                        }
                    }
                }
                if (status > 0) {
                    woItems = reconditionBP.getWorkorderItems(Integer.parseInt(command.getRcWorkId()));
                    vendorList = purchaseBP.processVendorList(1012);
                    request.setAttribute("woItems", woItems);
                    request.setAttribute("vendorList", vendorList);
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "WOItem  Modified  Successfully");
                }
            }
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to retrieve handlePurhcaseorderItems--> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    public ModelAndView handleDeleteWoItems(HttpServletRequest request, HttpServletResponse response, ReconditionCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        int status = 0;
        path = "content/recondition/modifyWO.jsp";
        ModelAndView mv = new ModelAndView();
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        ArrayList woItems = new ArrayList();
        ArrayList vendorList = new ArrayList();
        try {
            if (!loginBP.checkAuthorisation(userFunctions, "Mfr-Alter")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                String menuPath = "Stores  >>  Recondition >>  Modify WO";
                ArrayList poItems = new ArrayList();
                ReconditionTO recondition = null;
                //////System.out.println("In handleDeletePoItems");
                String itemId[] = request.getParameterValues("itemId");
                String rcQueueIds[] = request.getParameterValues("rcQueueIds");
                String rcItemIds[] = request.getParameterValues("rcItemIds");
                String deleteInd[] = request.getParameterValues("deleteInd");
                //////System.out.println("Deleted Index Length" + deleteInd.length);
                recondition = new ReconditionTO();

                int index = 0;
                for (int i = 0; i < deleteInd.length; i++) {
                    index = Integer.parseInt(deleteInd[i]);
                    recondition = new ReconditionTO();
                    recondition.setItemId(Integer.parseInt(itemId[index]));
                    recondition.setRcWorkId(Integer.parseInt(command.getRcWorkId()));
                    recondition.setRcItemId(rcItemIds[index]);
                    recondition.setRcQueueId(rcQueueIds[index]);
                    System.out.println((new StringBuilder()).append("itemId").append(itemId[index]).toString());
                    System.out.println((new StringBuilder()).append("command.getRcWorkId()=").append(command.getRcWorkId()).toString());
                    System.out.println((new StringBuilder()).append("command.getRcItemId()=").append(rcItemIds[index]).toString());
                    System.out.println((new StringBuilder()).append("command.getRcQueueId()=").append(rcQueueIds[index]).toString());
                    status = reconditionBP.deleteWoItems(recondition);
                }

                request.setAttribute("menuPath", menuPath);
                if (status > 0) {
                    woItems = reconditionBP.getWorkorderItems(Integer.parseInt(command.getRcWorkId()));
                    vendorList = purchaseBP.processVendorList(1012);
                    request.setAttribute("woItems", woItems);
                    request.setAttribute("vendorList", vendorList);
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "WoItem  Deleted  Successfully");
                }
                String pageTitle = "Modify WO";
                request.setAttribute("pageTitle", pageTitle);

            }
        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to Delete handleDeletePoItems--> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //Rc Zone
    public ModelAndView handleGenerateRcZoneWo(HttpServletRequest request, HttpServletResponse response, ReconditionCommand command) {
        //////System.out.println("Inside  handleGenerateRcZoneWo");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList jobCardList = new ArrayList();
        ArrayList customerTypeList = new ArrayList();
        HttpSession session = request.getSession();
        int companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String menuPath = "";
        String pageTitle = "Genearte MRS";
        menuPath = "Reconditioning >> GenerateRcWo ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute("menuPath", menuPath);
        try {
             setLocale(request, response);

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            System.out.println((new StringBuilder()).append("size is  ").append(userFunctions.size()).toString());
            if (!loginBP.checkAuthorisation(userFunctions, "RenderService-GenerateMrs")) {
                path = "content/common/NotAuthorized.jsp";
            } else {


                path = "content/rcZone/generateRcWo.jsp";

                customerTypeList = reconditionBP.getCustomerTypeList();
                jobCardList = reconditionBP.getJobCardList();
                request.setAttribute("customerTypeList", customerTypeList);
                request.setAttribute("jobCardList", jobCardList);

                //////System.out.println("In Recondition Controller customerTypeList-->" + customerTypeList.size());
                //////System.out.println("In Recondition Controller-->" + jobCardList.size());
            }

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Run time exception --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Business exception --> ").append(exception.getErrorDetails()).toString());
            request.setAttribute("errorMessage", exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Failed to view jobcard --> ").append(exception).toString());
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    //Hari End
}






