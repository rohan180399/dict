
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.scheduler.business;

import java.util.List;


public class SchedulerTO {


    
    private String custCode = "";
    private String emailId = "";
    private String createdBy = "";
    private String diffDays = "";
    private String custName = "";
    private String contactPerson = "";
    private String address = "";
    private String mobile = "";
    private String fromDate = "";
    private String toDate = "";
    private String gstNo = "";
    private String panNo = "";
    
    private String containerSize = "";
    private String estimatedRevenue = "";
    private String marketRate = "";
    private String loadTypeName = "";
    private String grNo = "";
    private String totalTrips = "";
    private String transporter = "";
    private String totalDays = "";
    private String totalHours = "";
    private String invoiceId = "";
    private String travelHours = "";
    private String travelMinutes = "";
    private String location = "";
    private String tripCount = "";
    private String vehicleCount = "";
    private String latitude = "";
    private String longitude = "";
    private String mailStatus = "";
    private String ownerShip = "";
    private String noOfVehicles = "";
    private String noOfTrips = "";
    private String tripDays = "";
    private String startDate = "";
    private String endDate = "";
    private String userId = "";
    private String userName = "";
    private String noOfLogins = "";
    private String loginDuration = "";
    
    private String serviceDues = "";
    private String name = "";
    private String regNo = "";
    private String dueDate = "";
    private String dueDays = "";
    private String PermitType = "";
    private String operationPoint = "";
    
    private String vehicleId = "";
    private String filePath = "";
    private String mailTypeId = "";
    private String mailSubjectTo = "";
    private String MailSubjectCc = "";
    private String mailSubjectBcc = "";
    private String mailContentTo = "";
    private String mailContentCc = "";
    private String mailContentBcc = "";
    private String mailIdTo = "";
    private String mailIdCc = "";
    private String mailIdBcc = "";
    
    private String filenameContent = "";
    private String emailFormat = "";
    private String fleetCenter = "";
    private String originReportingDateTime = "";
    private String destinationReportingDateTime = "";
    private String vehicleStartDateTime = "";
    private String vehicleEndDateTime = "";
    private String billingType = "";
    private String productInfo = "";
    private String travelDays = "";
    private String revenue = "";
    private String expense = "";
    private String productCategory = "";
    private String reeferRequirement = "";
    private String actualTransitDays = "";
    private String estimatedTransitDays = "";
    private String empName = "";
    private String toBePaid = "";
    private String journeyDays = "";
    private String transitDays = "";
    private String nettExpense = "";
    private String paidAmount = "";
    private String driver = "";
    
    
    private String mobileNo = "";
    private String Customer_Name = "";
    private String customerOrderReferenceNo = "";
    private String consignmentOrderDate = "";
    private String customerCode = "";
    private String vehicleTypeName = "";
    private String reeferRequired = "";
    private String productCategoryName = "";
    private String consignmentOrderId = "";
    private String tripId = "";
    private String differenceHour = "";
    private String grNumber = "";
    private String cNote = "";
    private String vehicleType = "";
    private String route = "";
    private String tripStartDate = "";
    private String tripEndDate = "";
    private String grDate = "";
    private String containerNo = "";
    private String containerType = "";
    private String vehicleNo = "";
    
    private String containerTypeName = "";
    private String twentyftcontainerType = "";
    private String fourtyftcontainerType = "";
    private String plannedTwentyftcontainerType = "";
    private String plannedFourtyftcontainerType = "";
    
    private String consignmentOrderNo = "";
    private String billingParty = "";
    private String customerName = "";
    private String movementType = "";
    private String vehicleRequiredDateTime = "";
    private String statusname = "";
    private String routeInfo = "";
    private String totalWeight = "";
    private String totalVolume = "";
    private String segmentName = "";
    
    private String noOfGR9AM = "";
    private String noOfOrder9AM = "";
    private String noOfTripClosed9AM = "";
    
    private String noOfGR9PM = "";
    private String noOfOrder9PM = "";
    private String noOfTripClosed9PM = "";
    
    
    List vehicleInfo = null;
    List barCodeList = null;
    private String privateMarking = "";
    private String branchCode = "";
    private String unLoadedStatus = "";
    private String unLoadedDateTime = "";
    private String unLoadedUserId = "";
    private String loadedStatus = "";
    private String loadedDateTime = "";
    private String loadedUserId = "";
    private String ogplId = "";
    private String ogplNo = "";
    private String tripCode = "";
    private String wayBillId = "";
    private String wayBillNo = "";
    private String wayBillDate = "";
    private String origin = "";
    private String destination = "";
    private String noOfArticles = "";
    private String totalFreight = "";
    private String wayBillMode = "";
    private String paymentMode = "";
    private String barCodeNo = "";
    
    private String custId = "";
    private String tempId = "";
    private String availAmt = "";
    private String receiptCode = "";

    public String getReceiptCode() {
        return receiptCode;
    }

    public void setReceiptCode(String receiptCode) {
        this.receiptCode = receiptCode;
    }   
    

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getTempId() {
        return tempId;
    }

    public void setTempId(String tempId) {
        this.tempId = tempId;
    }

    public String getAvailAmt() {
        return availAmt;
    }

    public void setAvailAmt(String availAmt) {
        this.availAmt = availAmt;
    }

    public List getVehicleInfo() {
        return vehicleInfo;
    }

    public void setVehicleInfo(List vehicleInfo) {
        this.vehicleInfo = vehicleInfo;
    }

    public String getWayBillId() {
        return wayBillId;
    }

    public void setWayBillId(String wayBillId) {
        this.wayBillId = wayBillId;
    }

    public String getWayBillNo() {
        return wayBillNo;
    }

    public void setWayBillNo(String wayBillNo) {
        this.wayBillNo = wayBillNo;
    }

    public String getWayBillDate() {
        return wayBillDate;
    }

    public void setWayBillDate(String wayBillDate) {
        this.wayBillDate = wayBillDate;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getNoOfArticles() {
        return noOfArticles;
    }

    public void setNoOfArticles(String noOfArticles) {
        this.noOfArticles = noOfArticles;
    }

    public String getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(String totalWeight) {
        this.totalWeight = totalWeight;
    }

    public String getTotalFreight() {
        return totalFreight;
    }

    public void setTotalFreight(String totalFreight) {
        this.totalFreight = totalFreight;
    }

    public String getWayBillMode() {
        return wayBillMode;
    }

    public void setWayBillMode(String wayBillMode) {
        this.wayBillMode = wayBillMode;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getBarCodeNo() {
        return barCodeNo;
    }

    public void setBarCodeNo(String barCodeNo) {
        this.barCodeNo = barCodeNo;
    }

    public String getOgplId() {
        return ogplId;
    }

    public void setOgplId(String ogplId) {
        this.ogplId = ogplId;
    }

    public String getOgplNo() {
        return ogplNo;
    }

    public void setOgplNo(String ogplNo) {
        this.ogplNo = ogplNo;
    }

    public String getTripCode() {
        return tripCode;
    }

    public void setTripCode(String tripCode) {
        this.tripCode = tripCode;
    }

    public List getBarCodeList() {
        return barCodeList;
    }

    public void setBarCodeList(List barCodeList) {
        this.barCodeList = barCodeList;
    }

    public String getLoadedStatus() {
        return loadedStatus;
    }

    public void setLoadedStatus(String loadedStatus) {
        this.loadedStatus = loadedStatus;
    }

    public String getLoadedDateTime() {
        return loadedDateTime;
    }

    public void setLoadedDateTime(String loadedDateTime) {
        this.loadedDateTime = loadedDateTime;
    }

    public String getLoadedUserId() {
        return loadedUserId;
    }

    public void setLoadedUserId(String loadedUserId) {
        this.loadedUserId = loadedUserId;
    }

    public String getUnLoadedStatus() {
        return unLoadedStatus;
    }

    public void setUnLoadedStatus(String unLoadedStatus) {
        this.unLoadedStatus = unLoadedStatus;
    }

    public String getUnLoadedDateTime() {
        return unLoadedDateTime;
    }

    public void setUnLoadedDateTime(String unLoadedDateTime) {
        this.unLoadedDateTime = unLoadedDateTime;
    }

    public String getUnLoadedUserId() {
        return unLoadedUserId;
    }

    public void setUnLoadedUserId(String unLoadedUserId) {
        this.unLoadedUserId = unLoadedUserId;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getPrivateMarking() {
        return privateMarking;
    }

    public void setPrivateMarking(String privateMarking) {
        this.privateMarking = privateMarking;
    }

    public String getNoOfGR9AM() {
        return noOfGR9AM;
    }

    public void setNoOfGR9AM(String noOfGR9AM) {
        this.noOfGR9AM = noOfGR9AM;
    }

    public String getNoOfOrder9AM() {
        return noOfOrder9AM;
    }

    public void setNoOfOrder9AM(String noOfOrder9AM) {
        this.noOfOrder9AM = noOfOrder9AM;
    }

    public String getNoOfTripClosed9AM() {
        return noOfTripClosed9AM;
    }

    public void setNoOfTripClosed9AM(String noOfTripClosed9AM) {
        this.noOfTripClosed9AM = noOfTripClosed9AM;
    }

    public String getNoOfGR9PM() {
        return noOfGR9PM;
    }

    public void setNoOfGR9PM(String noOfGR9PM) {
        this.noOfGR9PM = noOfGR9PM;
    }

    public String getNoOfOrder9PM() {
        return noOfOrder9PM;
    }

    public void setNoOfOrder9PM(String noOfOrder9PM) {
        this.noOfOrder9PM = noOfOrder9PM;
    }

    public String getNoOfTripClosed9PM() {
        return noOfTripClosed9PM;
    }

    public void setNoOfTripClosed9PM(String noOfTripClosed9PM) {
        this.noOfTripClosed9PM = noOfTripClosed9PM;
    }

    public String getConsignmentOrderId() {
        return consignmentOrderId;
    }

    public void setConsignmentOrderId(String consignmentOrderId) {
        this.consignmentOrderId = consignmentOrderId;
    }

    public String getConsignmentOrderNo() {
        return consignmentOrderNo;
    }

    public void setConsignmentOrderNo(String consignmentOrderNo) {
        this.consignmentOrderNo = consignmentOrderNo;
    }

    public String getBillingParty() {
        return billingParty;
    }

    public void setBillingParty(String billingParty) {
        this.billingParty = billingParty;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getMovementType() {
        return movementType;
    }

    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    public String getVehicleRequiredDateTime() {
        return vehicleRequiredDateTime;
    }

    public void setVehicleRequiredDateTime(String vehicleRequiredDateTime) {
        this.vehicleRequiredDateTime = vehicleRequiredDateTime;
    }

    public String getStatusname() {
        return statusname;
    }

    public void setStatusname(String statusname) {
        this.statusname = statusname;
    }

    public String getRouteInfo() {
        return routeInfo;
    }

    public void setRouteInfo(String routeInfo) {
        this.routeInfo = routeInfo;
    }

    public String getTotalVolume() {
        return totalVolume;
    }

    public void setTotalVolume(String totalVolume) {
        this.totalVolume = totalVolume;
    }

    public String getSegmentName() {
        return segmentName;
    }

    public void setSegmentName(String segmentName) {
        this.segmentName = segmentName;
    }

    public String getContainerTypeName() {
        return containerTypeName;
    }

    public void setContainerTypeName(String containerTypeName) {
        this.containerTypeName = containerTypeName;
    }

    public String getTwentyftcontainerType() {
        return twentyftcontainerType;
    }

    public void setTwentyftcontainerType(String twentyftcontainerType) {
        this.twentyftcontainerType = twentyftcontainerType;
    }

    public String getFourtyftcontainerType() {
        return fourtyftcontainerType;
    }

    public void setFourtyftcontainerType(String fourtyftcontainerType) {
        this.fourtyftcontainerType = fourtyftcontainerType;
    }

    public String getPlannedTwentyftcontainerType() {
        return plannedTwentyftcontainerType;
    }

    public void setPlannedTwentyftcontainerType(String plannedTwentyftcontainerType) {
        this.plannedTwentyftcontainerType = plannedTwentyftcontainerType;
    }

    public String getPlannedFourtyftcontainerType() {
        return plannedFourtyftcontainerType;
    }

    public void setPlannedFourtyftcontainerType(String plannedFourtyftcontainerType) {
        this.plannedFourtyftcontainerType = plannedFourtyftcontainerType;
    }

    public String getGrNumber() {
        return grNumber;
    }

    public void setGrNumber(String grNumber) {
        this.grNumber = grNumber;
    }

    public String getcNote() {
        return cNote;
    }

    public void setcNote(String cNote) {
        this.cNote = cNote;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getTripStartDate() {
        return tripStartDate;
    }

    public void setTripStartDate(String tripStartDate) {
        this.tripStartDate = tripStartDate;
    }

    public String getTripEndDate() {
        return tripEndDate;
    }

    public void setTripEndDate(String tripEndDate) {
        this.tripEndDate = tripEndDate;
    }

    public String getGrDate() {
        return grDate;
    }

    public void setGrDate(String grDate) {
        this.grDate = grDate;
    }

    public String getContainerNo() {
        return containerNo;
    }

    public void setContainerNo(String containerNo) {
        this.containerNo = containerNo;
    }

    public String getContainerType() {
        return containerType;
    }

    public void setContainerType(String containerType) {
        this.containerType = containerType;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getCustomer_Name() {
        return Customer_Name;
    }

    public void setCustomer_Name(String Customer_Name) {
        this.Customer_Name = Customer_Name;
    }

    public String getCustomerOrderReferenceNo() {
        return customerOrderReferenceNo;
    }

    public void setCustomerOrderReferenceNo(String customerOrderReferenceNo) {
        this.customerOrderReferenceNo = customerOrderReferenceNo;
    }

    public String getConsignmentOrderDate() {
        return consignmentOrderDate;
    }

    public void setConsignmentOrderDate(String consignmentOrderDate) {
        this.consignmentOrderDate = consignmentOrderDate;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    public String getReeferRequired() {
        return reeferRequired;
    }

    public void setReeferRequired(String reeferRequired) {
        this.reeferRequired = reeferRequired;
    }

    public String getProductCategoryName() {
        return productCategoryName;
    }

    public void setProductCategoryName(String productCategoryName) {
        this.productCategoryName = productCategoryName;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getDifferenceHour() {
        return differenceHour;
    }

    public void setDifferenceHour(String differenceHour) {
        this.differenceHour = differenceHour;
    }

    public String getFleetCenter() {
        return fleetCenter;
    }

    public void setFleetCenter(String fleetCenter) {
        this.fleetCenter = fleetCenter;
    }

    public String getOriginReportingDateTime() {
        return originReportingDateTime;
    }

    public void setOriginReportingDateTime(String originReportingDateTime) {
        this.originReportingDateTime = originReportingDateTime;
    }

    public String getDestinationReportingDateTime() {
        return destinationReportingDateTime;
    }

    public void setDestinationReportingDateTime(String destinationReportingDateTime) {
        this.destinationReportingDateTime = destinationReportingDateTime;
    }

    public String getVehicleStartDateTime() {
        return vehicleStartDateTime;
    }

    public void setVehicleStartDateTime(String vehicleStartDateTime) {
        this.vehicleStartDateTime = vehicleStartDateTime;
    }

    public String getVehicleEndDateTime() {
        return vehicleEndDateTime;
    }

    public void setVehicleEndDateTime(String vehicleEndDateTime) {
        this.vehicleEndDateTime = vehicleEndDateTime;
    }

    public String getBillingType() {
        return billingType;
    }

    public void setBillingType(String billingType) {
        this.billingType = billingType;
    }

    public String getProductInfo() {
        return productInfo;
    }

    public void setProductInfo(String productInfo) {
        this.productInfo = productInfo;
    }

    public String getTravelDays() {
        return travelDays;
    }

    public void setTravelDays(String travelDays) {
        this.travelDays = travelDays;
    }

    public String getRevenue() {
        return revenue;
    }

    public void setRevenue(String revenue) {
        this.revenue = revenue;
    }

    public String getExpense() {
        return expense;
    }

    public void setExpense(String expense) {
        this.expense = expense;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public String getReeferRequirement() {
        return reeferRequirement;
    }

    public void setReeferRequirement(String reeferRequirement) {
        this.reeferRequirement = reeferRequirement;
    }

    public String getActualTransitDays() {
        return actualTransitDays;
    }

    public void setActualTransitDays(String actualTransitDays) {
        this.actualTransitDays = actualTransitDays;
    }

    public String getEstimatedTransitDays() {
        return estimatedTransitDays;
    }

    public void setEstimatedTransitDays(String estimatedTransitDays) {
        this.estimatedTransitDays = estimatedTransitDays;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getToBePaid() {
        return toBePaid;
    }

    public void setToBePaid(String toBePaid) {
        this.toBePaid = toBePaid;
    }

    public String getJourneyDays() {
        return journeyDays;
    }

    public void setJourneyDays(String journeyDays) {
        this.journeyDays = journeyDays;
    }

    public String getTransitDays() {
        return transitDays;
    }

    public void setTransitDays(String transitDays) {
        this.transitDays = transitDays;
    }

    public String getNettExpense() {
        return nettExpense;
    }

    public void setNettExpense(String nettExpense) {
        this.nettExpense = nettExpense;
    }

    public String getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(String paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getFilenameContent() {
        return filenameContent;
    }

    public void setFilenameContent(String filenameContent) {
        this.filenameContent = filenameContent;
    }

    public String getEmailFormat() {
        return emailFormat;
    }

    public void setEmailFormat(String emailFormat) {
        this.emailFormat = emailFormat;
    }

    
    public String getMailTypeId() {
        return mailTypeId;
    }

    public void setMailTypeId(String mailTypeId) {
        this.mailTypeId = mailTypeId;
    }

    public String getMailSubjectTo() {
        return mailSubjectTo;
    }

    public void setMailSubjectTo(String mailSubjectTo) {
        this.mailSubjectTo = mailSubjectTo;
    }

    public String getMailSubjectCc() {
        return MailSubjectCc;
    }

    public void setMailSubjectCc(String MailSubjectCc) {
        this.MailSubjectCc = MailSubjectCc;
    }

    public String getMailSubjectBcc() {
        return mailSubjectBcc;
    }

    public void setMailSubjectBcc(String mailSubjectBcc) {
        this.mailSubjectBcc = mailSubjectBcc;
    }

    public String getMailContentTo() {
        return mailContentTo;
    }

    public void setMailContentTo(String mailContentTo) {
        this.mailContentTo = mailContentTo;
    }

    public String getMailContentCc() {
        return mailContentCc;
    }

    public void setMailContentCc(String mailContentCc) {
        this.mailContentCc = mailContentCc;
    }

    public String getMailContentBcc() {
        return mailContentBcc;
    }

    public void setMailContentBcc(String mailContentBcc) {
        this.mailContentBcc = mailContentBcc;
    }

    public String getMailIdTo() {
        return mailIdTo;
    }

    public void setMailIdTo(String mailIdTo) {
        this.mailIdTo = mailIdTo;
    }

    public String getMailIdCc() {
        return mailIdCc;
    }

    public void setMailIdCc(String mailIdCc) {
        this.mailIdCc = mailIdCc;
    }

    public String getMailIdBcc() {
        return mailIdBcc;
    }

    public void setMailIdBcc(String mailIdBcc) {
        this.mailIdBcc = mailIdBcc;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getDueDays() {
        return dueDays;
    }

    public void setDueDays(String dueDays) {
        this.dueDays = dueDays;
    }

    public String getPermitType() {
        return PermitType;
    }

    public void setPermitType(String PermitType) {
        this.PermitType = PermitType;
    }

    public String getOperationPoint() {
        return operationPoint;
    }

    public void setOperationPoint(String operationPoint) {
        this.operationPoint = operationPoint;
    }

    public String getServiceDues() {
        return serviceDues;
    }

    public void setServiceDues(String serviceDues) {
        this.serviceDues = serviceDues;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNoOfLogins() {
        return noOfLogins;
    }

    public void setNoOfLogins(String noOfLogins) {
        this.noOfLogins = noOfLogins;
    }

    public String getLoginDuration() {
        return loginDuration;
    }

    public void setLoginDuration(String loginDuration) {
        this.loginDuration = loginDuration;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getTripDays() {
        return tripDays;
    }

    public void setTripDays(String tripDays) {
        this.tripDays = tripDays;
    }

    public String getOwnerShip() {
        return ownerShip;
    }

    public void setOwnerShip(String ownerShip) {
        this.ownerShip = ownerShip;
    }

    public String getNoOfVehicles() {
        return noOfVehicles;
    }

    public void setNoOfVehicles(String noOfVehicles) {
        this.noOfVehicles = noOfVehicles;
    }

    public String getNoOfTrips() {
        return noOfTrips;
    }

    public void setNoOfTrips(String noOfTrips) {
        this.noOfTrips = noOfTrips;
    }

    public String getMailStatus() {
        return mailStatus;
    }

    public void setMailStatus(String mailStatus) {
        this.mailStatus = mailStatus;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getTripCount() {
        return tripCount;
    }

    public void setTripCount(String tripCount) {
        this.tripCount = tripCount;
    }

    public String getVehicleCount() {
        return vehicleCount;
    }

    public void setVehicleCount(String vehicleCount) {
        this.vehicleCount = vehicleCount;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTravelHours() {
        return travelHours;
    }

    public void setTravelHours(String travelHours) {
        this.travelHours = travelHours;
    }

    public String getTravelMinutes() {
        return travelMinutes;
    }

    public void setTravelMinutes(String travelMinutes) {
        this.travelMinutes = travelMinutes;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getTotalTrips() {
        return totalTrips;
    }

    public void setTotalTrips(String totalTrips) {
        this.totalTrips = totalTrips;
    }

    public String getTransporter() {
        return transporter;
    }

    public void setTransporter(String transporter) {
        this.transporter = transporter;
    }

    public String getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(String totalDays) {
        this.totalDays = totalDays;
    }

    public String getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(String totalHours) {
        this.totalHours = totalHours;
    }

    public String getContainerSize() {
        return containerSize;
    }

    public void setContainerSize(String containerSize) {
        this.containerSize = containerSize;
    }

    public String getEstimatedRevenue() {
        return estimatedRevenue;
    }

    public void setEstimatedRevenue(String estimatedRevenue) {
        this.estimatedRevenue = estimatedRevenue;
    }

    public String getMarketRate() {
        return marketRate;
    }

    public void setMarketRate(String marketRate) {
        this.marketRate = marketRate;
    }

    public String getLoadTypeName() {
        return loadTypeName;
    }

    public void setLoadTypeName(String loadTypeName) {
        this.loadTypeName = loadTypeName;
    }

    public String getGrNo() {
        return grNo;
    }

    public void setGrNo(String grNo) {
        this.grNo = grNo;
    }

    public String getDiffDays() {
        return diffDays;
    }

    public void setDiffDays(String diffDays) {
        this.diffDays = diffDays;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getGstNo() {
        return gstNo;
    }

    public void setGstNo(String gstNo) {
        this.gstNo = gstNo;
    }

    public String getPanNo() {
        return panNo;
    }

    public void setPanNo(String panNo) {
        this.panNo = panNo;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCustCode() {
        return custCode;
    }

    public void setCustCode(String custCode) {
        this.custCode = custCode;
    }

   
    
    
    
    
}
