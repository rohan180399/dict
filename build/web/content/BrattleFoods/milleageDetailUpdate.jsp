<%--
    Document   : newfuelprice
    Created on : Oct 27, 2013, 5:04:09 PM
    Author     : Arul
--%>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>




<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body >
        <form name="fuleprice"  method="post" >
            <table class="table table-info mb30 table-hover">
                <thead>
                <tr height="30">
                <th><h3>Vehicle Age</h3></th>
                <th><h3>Empty (Km / Litter)</h3></th>
                <th><h3>Loaded (Km / Litter)</h3></th>
                </tr>
                </thead>
                <tbody>
                    <% int sno = 0;%>
                    <c:if test = "${viewMfrMilleageConfigDetails != null}" >
                        <%
                                            sno++;
                                           String className = "text1";
                                           if ((sno % 1) == 0) {
                                               className = "text1";
                                           } else {
                                               className = "text2";
                                           }
                        %>
                        <c:forEach items="${viewMfrMilleageConfigDetails}" var="mfr">
                            <tr height="30">
                                <td class="<%=className%>" align="left" ><c:out value="${mfr.mfrAge}"/> To <c:out value="${mfr.mfrToAge}"/>&nbsp;Year</td>
                                <td class="<%=className%>" align="left" ><input type="text" name="kmPerLitter" id="kmPerLitter" value="<c:out value="${mfr.kmPerLitter}"/>" /></td>
                                <td class="<%=className%>" align="left" >
                                    <input type="hidden" name="ageingId" id="ageingId" value="<c:out value="${mfr.ageingId}"/>" />
                                    <input type="hidden" name="vehicleTypeId" id="vehicleTypeId" value="<c:out value="${mfr.vehicleTypeId}"/>" />
                                    <input type="hidden" name="modelId" id="modelId" value="<c:out value="${mfr.modelId}"/>" />
                                    <input type="hidden" name="mfrId" id="mfrId" value="<c:out value="${mfr.mfrId}"/>" />
                                    <input type="text" name="kmPerLitterLoad" id="kmPerLitterLoad" value="<c:out value="${mfr.kmPerLitterLoaded}"/>" />
                                </td>
                            </tr>
                        </c:forEach>
                    </c:if>
                            <tr>
                                <td colspan="3">
                                <center>
                                <input type="button" class="btn btn-success" value="SaveMilleage" onclick="saveMilleage()"/>
                                </center>
                                </td>
                            </tr>       
                </tbody>
            </table>
            <script type="text/javascript">
                function saveMilleage(){
                   var ageingIds="";
                   var vehicleTypeIds="";
                   var modelIds="";
                   var mfrIds="";
                   var kmPerLitters ="";
                   var kmPerLitterLoads ="";
                   var  ageingId = document.getElementsByName("ageingId");
                   var  vehicleTypeId = document.getElementsByName("vehicleTypeId");
                   var  modelId = document.getElementsByName("modelId");
                   var  mfrId = document.getElementsByName("mfrId");
                   var  kmPerLitter = document.getElementsByName("kmPerLitter");
                   var  kmPerLitterLoad = document.getElementsByName("kmPerLitterLoad");
                   for(var i=0; i<ageingId.length; i++){
                       if(i==0){
                        ageingIds = ageingId[i].value;   
                        vehicleTypeIds = vehicleTypeId[i].value;   
                        modelIds = modelId[i].value;   
                        mfrIds = mfrId[i].value;   
                        kmPerLitters = kmPerLitter[i].value;   
                        kmPerLitterLoads = kmPerLitterLoad[i].value;   
                       }else{
                        ageingIds = ageingIds+"~"+ageingId[i].value;   
                        vehicleTypeIds = vehicleTypeIds+"~"+vehicleTypeId[i].value;   
                        modelIds = modelIds+"~"+modelId[i].value;   
                        mfrIds = mfrIds+"~"+mfrId[i].value;   
                        kmPerLitters = kmPerLitters+"~"+kmPerLitter[i].value;   
                        kmPerLitterLoads = kmPerLitterLoads+"~"+kmPerLitterLoad[i].value;   
                       }
                   }
                   upateMfrMilleage(kmPerLitters,kmPerLitterLoads,ageingIds,vehicleTypeIds,modelIds,mfrIds);
                }
                function upateMfrMilleage(kmPerLitters,kmPerLitterLoads,ageingId,vehicleTypeId,modelId,mfrId){
                    url = './upateMfrMilleage.do';
                    $.ajax({
                        url: url,
                        data: {kmPerLitter: kmPerLitters,kmPerLitterLoaded:kmPerLitterLoads,ageingId: ageingId,vehicleTypeId: vehicleTypeId,modelId: modelId,mfrId: mfrId
                        },
                        type: "GET",
                        success: function(response) {
                            
                        },
                        error: function(xhr, status, error) {
                        }
                    });
                }
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>

