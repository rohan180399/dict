/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.report.business;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream; 
import java.text.DateFormat;     
import java.text.SimpleDateFormat;
import java.util.Scanner;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import java.io.*;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.message.SOAPBodyElement;
import org.apache.axis.message.SOAPEnvelope;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

//import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.GrayColor;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.opencsv.CSVReader;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.arch.util.FPUtil;
import ets.domain.operation.business.OperationTO;
import ets.domain.report.data.ReportDAO;
import ets.domain.report.web.GenerateQrCode;
import ets.domain.scheduler.business.SchedulerTO;
import ets.domain.section.data.SectionDAO;

import ets.domain.thread.web.SendEmail;
import ets.domain.util.ThrottleConstants;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import javax.mail.Multipart;
import javax.mail.internet.MimeMultipart;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.message.SOAPBodyElement;
import org.apache.axis.message.SOAPEnvelope;
import static org.apache.commons.mail.ByteArrayDataSource.BUFFER_SIZE;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author sabreesh
 */
public class ReportBP {

    private ReportDAO reportDAO;
    private SectionDAO sectionDAO;
    static FPUtil fpUtil = FPUtil.getInstance();
    static final int poTextSplitSize = Integer.parseInt(fpUtil.getInstance().getProperty("PO_ADDRESS_SPLIT"));

    public ReportDAO getReportDAO() {
        return reportDAO;
    }

    public void setReportDAO(ReportDAO repDAO) {
        this.reportDAO = repDAO;
    }

    public static FPUtil getFpUtil() {
        return fpUtil;
    }

    public static void setFpUtil(FPUtil fpUtil) {
        ReportBP.fpUtil = fpUtil;
    }

    public SectionDAO getSectionDAO() {
        return sectionDAO;
    }

    public void setSectionDAO(SectionDAO sectionDAO) {
        this.sectionDAO = sectionDAO;
    }

    public ArrayList processBillList(ReportTO mrsTO, String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList MfrList = new ArrayList();
        MfrList = reportDAO.getBillList(mrsTO, fromDate, toDate);
        if (MfrList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return MfrList;
    }

    public ArrayList processSalesTaxSummary(ReportTO mrsTO, String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList MfrSalesList = new ArrayList();
        MfrSalesList = reportDAO.getSalesBillTaxSummary(mrsTO, fromDate, toDate);
        ////////System.out.println("salesTax size=" + MfrSalesList.size());
        if (MfrSalesList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return MfrSalesList;
    }

    public ArrayList processPurchaseReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList purchaseList = new ArrayList();
        purchaseList = reportDAO.getPurchaseList(repTO);
        if (purchaseList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return purchaseList;
    }

    public ArrayList processComplaintList(ReportTO repTO, String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList complaintList = new ArrayList();
        complaintList = reportDAO.getComplaintList(repTO, fromDate, toDate);

        if (complaintList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return complaintList;
    }

    public ArrayList processStockWorthReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList stockWorthList = new ArrayList();
        stockWorthList = reportDAO.getStockWorthList(repTO);
        if (stockWorthList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return stockWorthList;
    }

    public ArrayList processRateList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList rateList = new ArrayList();
        rateList = reportDAO.getRateList(repTO);
        if (rateList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return rateList;
    }

    public ArrayList processPeriodicServiceList(int companyId, String regNo, String date) throws FPRuntimeException, FPBusinessException {

        ArrayList rateList = new ArrayList();
        rateList = reportDAO.getPeriodicServiceVehicleList(companyId, regNo, date);
        //rateList = reportDAO.getPeriodicServiceList(companyId, regNo, date);
        if (rateList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return rateList;
    }

    public ArrayList processDueList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList dueList = new ArrayList();
        dueList = reportDAO.getDueList(repTO);
        if (dueList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return dueList;
    }

    public ArrayList processInsuranceDueList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList dueList = new ArrayList();
        dueList = reportDAO.getInsuranceDueList(repTO);
        if (dueList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return dueList;
    }

    public ArrayList processRoadTaxDueList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList dueList = new ArrayList();
        dueList = reportDAO.getRoadTaxDueList(repTO);
        if (dueList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return dueList;
    }

    public ArrayList processPermitDueList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList dueList = new ArrayList();
        dueList = reportDAO.getPermitDueList(repTO);
        if (dueList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return dueList;
    }

    public ArrayList processAMCDueList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList dueList = new ArrayList();
        dueList = reportDAO.processAMCDueList(repTO);
        if (dueList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return dueList;
    }

    public ArrayList processBodyBillList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList bodyBillList = new ArrayList();
        bodyBillList = reportDAO.getbodyBillList(repTO);
        if (bodyBillList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return bodyBillList;
    }

    public ArrayList processContractorActivities(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList ContractorActivities = new ArrayList();
        ContractorActivities = reportDAO.getContractorActivities(repTO);
        if (ContractorActivities.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return ContractorActivities;
    }

    public ArrayList processBodyBillDetails(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList bodyBillDetails = new ArrayList();
        bodyBillDetails = reportDAO.getbodyBillDetails(repTO);
        return bodyBillDetails;
    }

    public ArrayList processHikedBodyBillDetails(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList bodyBillDetails = new ArrayList();
        bodyBillDetails = reportDAO.getHikedbodyBillDetails(repTO);
        return bodyBillDetails;
    }

    public ArrayList processBillDetails(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList BillDetails = new ArrayList();
        BillDetails = reportDAO.getBillDetails(repTO);
        return BillDetails;
    }

    public ArrayList processActivityList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList ActivityList = new ArrayList();
        ActivityList = reportDAO.getActivityList(repTO);
        return ActivityList;
    }

    public ArrayList processTotalPrices(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList TotalPrices = new ArrayList();
        TotalPrices = reportDAO.getTotalPrices(repTO);
        return TotalPrices;
    }

    public ArrayList processStockIssueReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList IssueList = new ArrayList();
        IssueList = reportDAO.getStockIssueReport(repTO);
        if (IssueList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return IssueList;
    }

    public ArrayList processStoresEffReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList IssueList = new ArrayList();
        IssueList = reportDAO.getStoresEffReport(repTO);
        if (IssueList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return IssueList;
    }

    public ArrayList processReceivedStockReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList receivedList = new ArrayList();
        receivedList = reportDAO.getReceivedStockReport(repTO);
        if (receivedList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return receivedList;
    }

    public ArrayList processReceivedStockTaxSummary(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList receivedList = new ArrayList();
        receivedList = reportDAO.getReceivedStockTaxSummary(repTO);
        if (receivedList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return receivedList;
    }

    public ArrayList processInVoiceItems(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList receivedList = new ArrayList();
        receivedList = reportDAO.getInvoiceItems(repTO);
        if (receivedList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return receivedList;
    }

    public ArrayList processRCWOItems(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList receivedList = new ArrayList();
        ArrayList List = new ArrayList();
        receivedList = reportDAO.getRCWOItems(repTO);
        Iterator itr;
        itr = receivedList.iterator();
        ReportTO purch = null;
        String remarks = "";
        while (itr.hasNext()) {
            purch = new ReportTO();
            purch = (ReportTO) itr.next();
            remarks = purch.getRemarks();
            purch.setRemarks(remarks);
            /*
             if (remarks.length() >= 150) {
             remarks = remarks.substring(0, 150);
             }
             */
            purch.setAddressSplit(addressSplit(purch.getAddress()));
            purch.setRemarksSplit(remarksSplit(remarks, 75));
            List.add(purch);
        }

        if (receivedList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return List;
    }

    public ArrayList processGdDetail(String reqId) throws FPRuntimeException, FPBusinessException {

        ArrayList serviceEffList = new ArrayList();
        String fromService = "";
        String toService = "";
        serviceEffList = reportDAO.getGdDetail(reqId);
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");

        }
        return serviceEffList;
    }

    public String[] addressSplit(String longText) {
        String[] splitText = null;
        int lower = 0, higher;
        int splitSize = poTextSplitSize;
        int size = longText.length();
        splitText = new String[(size / splitSize) + 1];
        for (int i = 0; i < (size / splitSize) + 1; i++) {
            if ((size - lower) < splitSize) {
                higher = (size);
            } else {
                higher = lower + splitSize;
            }
            splitText[i] = longText.substring(lower, higher);
            //System.out.println(splitText[i]);
            lower = lower + splitSize;
        }
        return splitText;
    }

    public String[] remarksSplit(String longText, int textLength) {
        String[] splitText = null;
        int lower = 0, higher;
        int splitSize = textLength;
        int size = longText.length();
        splitText = new String[(size / splitSize) + 1];
        for (int i = 0; i < (size / splitSize) + 1; i++) {
            if ((size - lower) < splitSize) {
                higher = (size);
            } else {
                higher = lower + splitSize;
            }
            splitText[i] = longText.substring(lower, higher);
            //System.out.println(splitText[i]);
            lower = lower + splitSize;
        }
        return splitText;
    }

    public ArrayList processServiceSummary(String usageTypeId, String ServiceTypId, String custId, String compId, String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList receivedList = new ArrayList();
        receivedList = reportDAO.getServiceSummary(usageTypeId, ServiceTypId, custId, compId, fromDate, toDate);
        if (receivedList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return receivedList;
    }

    public ArrayList processMovingAvgReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList MovingAvg = new ArrayList();
        ArrayList MovingAverage = new ArrayList();
        float stockBalance = 0.0f;
        int itemId = 0;
        int companyId = Integer.parseInt(repTO.getCompanyId());
        String lastPurchased = "";

        ////////System.out.println("Moving avg Com-->" + companyId);
        MovingAvg = reportDAO.getMovingAvgReport(repTO);
        if (MovingAvg.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        } else {

            Iterator itr = MovingAvg.iterator();
            ReportTO report = new ReportTO();
            while (itr.hasNext()) {
                report = (ReportTO) itr.next();
                //////////System.out.println("report.getSpltQty()" + report.getSpltQty());
                String monthSplit[] = report.getSpltQty().split("-");
                //////////System.out.println("MonthSplit-->length" + monthSplit.length);

                report.setFirstHalfDays(monthSplit[0]);
                report.setLastHalfDays(monthSplit[1]);

                itemId = reportDAO.getItemId(report.getPaplCode());
                stockBalance = sectionDAO.getItemStock(itemId, Integer.parseInt(repTO.getCompanyId()));
                //////////System.out.println("stockBalance-->"+stockBalance);
                report.setItemQty(stockBalance + "");

                lastPurchased = sectionDAO.getItemStockLastPurchased(itemId, companyId);
                //////////System.out.println("In Section BP LastPurchased Have-->" + lastPurchased);

                String temp[] = lastPurchased.split("-");
                //////////System.out.println("Qty-->" + temp[0] + "Price-->" + temp[1]);

                report.setPrice(Float.valueOf(temp[1]));
                MovingAverage.add(report);
            }
        }
        return MovingAverage;
    }

    public ArrayList processSerEffReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList serviceEffList = new ArrayList();
        serviceEffList = reportDAO.getSerEffReport(repTO);
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return serviceEffList;
    }

    public ArrayList processStRequestList(String fromdate, String toDate, int fromId, int toId, String type) throws FPRuntimeException, FPBusinessException {

        ArrayList serviceEffList = new ArrayList();
        serviceEffList = reportDAO.getStRequestList(fromdate, toDate, fromId, toId, type);
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return serviceEffList;
    }

    public ArrayList processStDetail(String reqId) throws FPRuntimeException, FPBusinessException {

        ArrayList serviceEffList = new ArrayList();
        serviceEffList = reportDAO.getStDetail(reqId);
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return serviceEffList;
    }

    public ArrayList processRcBillList(String vendorId, String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList serviceEffList = new ArrayList();
        serviceEffList = reportDAO.getRcBillList(vendorId, fromDate, toDate);
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return serviceEffList;
    }

    public ArrayList processRcBillTaxSummary(String vendorId, String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList serviceEffList = new ArrayList();
        serviceEffList = reportDAO.getRcBillTaxSummary(vendorId, fromDate, toDate);
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return serviceEffList;
    }

    public ArrayList processRcBillDetail(String reqId) throws FPRuntimeException, FPBusinessException {

        ArrayList serviceEffList = new ArrayList();
        serviceEffList = reportDAO.getRcBillDetail(reqId);
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return serviceEffList;
    }

    public ArrayList processServiceCostList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList serviceCostList = new ArrayList();
        serviceCostList = reportDAO.getServiceCostList(repTO);
        if (serviceCostList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return serviceCostList;
    }

    public ArrayList processWoList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList woList = new ArrayList();
        woList = reportDAO.getWoList(repTO);
        if (woList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return woList;
    }

    public ArrayList processWarrantyServiceList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList serviceList = new ArrayList();
        serviceList = reportDAO.getWarrantyServiceList(repTO);
        if (serviceList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return serviceList;
    }

    public ArrayList processTechniciansList() throws FPBusinessException, FPRuntimeException {
        ArrayList tempList = new ArrayList();
        tempList = reportDAO.techniciansList();
        if (tempList.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-03");
        }
        return tempList;

    }

    public ArrayList processRcItemList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList RcItemList = new ArrayList();
        RcItemList = reportDAO.getRcItemList(repTO);
        if (RcItemList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return RcItemList;
    }

    public ArrayList processRcHistoryList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList RcHistoryList = new ArrayList();
        RcHistoryList = reportDAO.getRcHistoryList(repTO);
        if (RcHistoryList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return RcHistoryList;
    }

    public ArrayList processTyreList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList tyreList = new ArrayList();
        tyreList = reportDAO.getTyreList(repTO);
        if (tyreList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return tyreList;
    }

    public ArrayList processOrdersList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList tyreList = new ArrayList();
        tyreList = reportDAO.getOrdersList(repTO);
        if (tyreList.size() == 0) {
            //throw new FPBusinessException("EM-GEN-01");
        }
        return tyreList;
    }

    public ArrayList processStockPurchase(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList itemList = new ArrayList();
        itemList = reportDAO.getStockPurchase(repTO);
        if (itemList.size() == 0) {
            //throw new FPBusinessException("EM-GEN-01");
        }
        return itemList;
    }

    public ArrayList processRcBillsReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList itemList = new ArrayList();
        itemList = reportDAO.getRcBillsReport(repTO);
        if (itemList.size() == 0) {
            //throw new FPBusinessException("EM-GEN-01");
        }
        return itemList;
    }

    public ArrayList processStatusReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList List = new ArrayList();
        ArrayList lastList = new ArrayList();
        List = reportDAO.getStatusReport(repTO);
        if (List.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        } else {
            Date dat = new Date();
            ////////System.out.println("st" + dat.getSeconds());
            Iterator itr = List.iterator();
            ReportTO reportTO = null;
            while (itr.hasNext()) {
                reportTO = new ReportTO();
                reportTO = (ReportTO) itr.next();
                lastList = reportDAO.getPrevious(reportTO.getJobCardId(), reportTO.getRegNo());
                Iterator itr1 = lastList.iterator();
                ReportTO repotTO = null;
                if (itr1.hasNext()) {
                    repotTO = new ReportTO();
                    repotTO = (ReportTO) itr1.next();

                    reportTO.setLastProblem(reportDAO.getLastProblem(repotTO.getJobCardId()));
                    reportTO.setLastRemarks(repotTO.getLastRemarks());
                    reportTO.setLastKm(repotTO.getLastKm());
                    reportTO.setLastStatus(repotTO.getLastStatus());
                    reportTO.setLastTech(repotTO.getLastTech());
                    reportTO.setLastDate(repotTO.getLastDate());
                }
            }
            Date dat1 = new Date();
            ////////System.out.println("et" + dat1.getSeconds());
            ////////System.out.println("tt" + (dat1.getSeconds() - dat.getSeconds()));

        }
        return List;
    }

    public ArrayList processTaxwiseItems(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList itemList = new ArrayList();
        itemList = reportDAO.getTaxwiseItems(repTO);
        if (itemList.size() == 0) {
            //throw new FPBusinessException("EM-GEN-01");
        }
        return itemList;
    }

    public ArrayList processVehMfrComparisionReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList List = new ArrayList();
        List = reportDAO.getMfrVehComparisionReport(repTO);
        Iterator itr = List.iterator();
        ReportTO reportTO = null;
        while (itr.hasNext()) {
            reportTO = (ReportTO) itr.next();
            reportTO.setVehCount(reportDAO.getMfrVehCount(reportTO.getMfrId()));
        }
        return List;
    }

    public ArrayList processVehModelComparisionReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList List = new ArrayList();
        List = reportDAO.getModelVehComparisionReport(repTO);
        Iterator itr = List.iterator();
        ReportTO reportTO = null;
        while (itr.hasNext()) {
            reportTO = (ReportTO) itr.next();
            reportTO.setVehCount(reportDAO.getModelVehCount(reportTO.getMfrId(), reportTO.getModelId()));
        }
        return List;
    }

    public ArrayList processVehAgeComparisionReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList List = new ArrayList();
        ArrayList List1 = new ArrayList();
        int count = 0;
        List = reportDAO.getVehAgeComparisionReport(repTO);
        Iterator itr = List.iterator();
        ReportTO reportTO = null;
        while (itr.hasNext()) {
            reportTO = (ReportTO) itr.next();
            if (reportTO.getVehCount().equalsIgnoreCase("0")) {
                count++;

            }
            ////////System.out.println("reportTO.getVehCount()" + reportTO.getVehCount());
        }
        if (count == 6) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return List;
    }

    // rajesh
    public ArrayList salesTrendGraph(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList salesTrendGraph = new ArrayList();
        salesTrendGraph = reportDAO.salesTrendGraph(reportTO);
        return salesTrendGraph;
    }

    public ArrayList vendorTrendGraph(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList vendorTrendGraph = new ArrayList();
        vendorTrendGraph = reportDAO.vendorTrendGraph(reportTO);
        return vendorTrendGraph;
    }

    public ArrayList mfr() throws FPRuntimeException, FPBusinessException {

        ArrayList mfr = new ArrayList();
        mfr = reportDAO.mfr();
        return mfr;
    }

    public ArrayList usage() throws FPRuntimeException, FPBusinessException {

        ArrayList usage = new ArrayList();
        usage = reportDAO.usage();
        return usage;
    }

    public ArrayList vehicleType() throws FPRuntimeException, FPBusinessException {

        ArrayList vehicleType = new ArrayList();
        vehicleType = reportDAO.vehicleType();
        return vehicleType;
    }

    public String processItemSuggests(String vendorName) {
        String vendorNames = "";
        vendorName = vendorName + "%";
        vendorNames = reportDAO.getItemSuggests(vendorName);
        return vendorNames;
    }

    public String problemItemSuggests(String problem) {
        String problems = "";
        problem = problem + "%";
        problems = reportDAO.getproblemSuggests(problem);
        return problems;
    }

    public ArrayList problemDistributionGraph(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList problemDistributionGraph = new ArrayList();
        problemDistributionGraph = reportDAO.problemDistributionGraph(reportTO);
        return problemDistributionGraph;
    }

    public ArrayList getColorList(int sizeValue) throws FPRuntimeException, FPBusinessException {
        ArrayList colorList = new ArrayList();
        colorList = reportDAO.getColorList(sizeValue);
        return colorList;
    }

    public ArrayList categoryRcReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList categoryRcReport = new ArrayList();
        categoryRcReport = reportDAO.categoryRcReport(reportTO);
        return categoryRcReport;
    }

    public ArrayList categoryNewReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList categoryNewReport = new ArrayList();
        categoryNewReport = reportDAO.categoryNewReport(reportTO);
        return categoryNewReport;
    }
    // end rajesh

    //shankar
    public ArrayList getActiveCategories() throws FPRuntimeException, FPBusinessException {

        ArrayList categories = new ArrayList();
        categories = reportDAO.getActiveCategories();
        return categories;
    }

    public ArrayList stockWorthGraph(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList stockWorthGraph = new ArrayList();
        stockWorthGraph = reportDAO.stockWorthGraph(reportTO);
        return stockWorthGraph;
    }

    public ArrayList CompanyNameList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList companyNameList = new ArrayList();
        companyNameList = reportDAO.CompanyNameList(reportTO);
        return companyNameList;
    }

    public ArrayList monthNameList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList monthNameList = new ArrayList();
        monthNameList = reportDAO.monthNameList(reportTO);
        return monthNameList;
    }

    public ArrayList getManufacturerList() throws FPRuntimeException, FPBusinessException {

        ArrayList manufacturerList = new ArrayList();
        manufacturerList = reportDAO.getManufacturerList();
        return manufacturerList;
    }

    public ArrayList getUsageTypeList() throws FPRuntimeException, FPBusinessException {

        ArrayList usageTypeList = new ArrayList();
        usageTypeList = reportDAO.getUsageTypeList();
        return usageTypeList;
    }

    public ArrayList getVehicleTypeList() throws FPRuntimeException, FPBusinessException {

        ArrayList vehicleTypeList = new ArrayList();
        vehicleTypeList = reportDAO.getVehicleTypeList();
        return vehicleTypeList;
    }

    public ArrayList processCategoryStRequestList(String fromDate, String toDate, int fromSpId, int toSpId) throws FPRuntimeException, FPBusinessException {
        ArrayList serviceEffList = new ArrayList();
        serviceEffList = reportDAO.processCategoryStRequestList(fromDate, toDate, fromSpId, toSpId);
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return serviceEffList;
    }
    // end shankar

    public String getVatPercentages() throws FPRuntimeException, FPBusinessException {
        String vatList = "";
        vatList = reportDAO.getVatPercentages();
        return vatList;
    }

    public ArrayList processTaxwiseServiceCostList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {
        ArrayList serviceEffList = new ArrayList();
        String vatList = "";
        String[] temp = null;
        String[] billNo = null;
        ArrayList vatValues = null;
        ArrayList billList = new ArrayList();
        serviceEffList = reportDAO.processTaxwiseServiceCostList(repTO);
        vatList = getVatPercentages();
        ReportTO reportTO = null;

        ReportTO report = null;
        float Amount = 0.0f;
        float taxAmount = 0.0f;
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        } else {
            Iterator itr = serviceEffList.iterator();
            while (itr.hasNext()) {
                vatValues = new ArrayList();
                reportTO = new ReportTO();
                reportTO = (ReportTO) itr.next();
                billNo = reportTO.getBillNo().split(",");
                temp = vatList.split(",");

                for (int j = 0; j < temp.length; j++) {
                    Amount = 0.0f;
                    Amount = reportDAO.getVatTotalAmount(billNo, temp[j]);
                    report = new ReportTO();
                    report.setAmount(Amount);
                    taxAmount = Float.parseFloat(temp[j]) * Amount;
                    report.setTaxAmount(taxAmount / 100);
                    vatValues.add(report);
                }
                Amount = 0.0f;
                report = new ReportTO();
                Amount = reportDAO.getContractAmount(billNo);
                report.setAmount(Amount);
                reportTO.setContractAmnt(Amount + "");
                report.setTaxAmount(0.0f);
                ////////System.out.println("contr" + Amount);
                vatValues.add(report);
                reportTO.setVatValues(vatValues);
                ////////System.out.println("service tax amount in BP++:" + reportTO.getTax());
            }

        }
        return serviceEffList;
    }

    public ArrayList processTaxwisePO(ReportTO repTO) throws FPRuntimeException, FPBusinessException {
        ArrayList serviceEffList = new ArrayList();
        String vatList = "";
        String[] temp = null;
        String supplyId = null;
        ArrayList vatValues = null;
        ArrayList billList = new ArrayList();
        serviceEffList = reportDAO.processTaxwisePO(repTO);
        vatList = getVatPercentages();
        ReportTO reportTO = null;

        ReportTO report = null;
        float Amount = 0.0f;
        float taxAmount = 0.0f;
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        } else {
            Iterator itr = serviceEffList.iterator();
            while (itr.hasNext()) {
                vatValues = new ArrayList();
                reportTO = new ReportTO();

                reportTO = (ReportTO) itr.next();
                supplyId = reportTO.getSupplyId();
                temp = vatList.split(",");

                for (int j = 0; j < temp.length; j++) {
                    Amount = 0.0f;
                    ////////System.out.println("supplyId" + supplyId + "tax per" + temp[j]);
                    Amount = reportDAO.getPOVatTotalAmount(supplyId, temp[j]);
                    report = new ReportTO();
                    report.setAmount(Amount);

                    taxAmount = Float.parseFloat(temp[j]) * Amount;
                    report.setTaxAmount(taxAmount / 100);
                    ////////System.out.println("Amount" + Amount + "tax" + report.getTaxAmount());
                    vatValues.add(report);
                }
                reportTO.setVatValues(vatValues);
            }

        }
        return serviceEffList;
    }

    public ArrayList gettopProblem(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList gettopProblem = new ArrayList();
        gettopProblem = reportDAO.gettopProblem(reportTO);
        return gettopProblem;
    }

    //Hari
    public ArrayList processServiceChartData(String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList woList = new ArrayList();
        woList = reportDAO.getServiceChartData(fromDate, toDate);
        if (woList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return woList;
    }

    public ArrayList handleServiceDailyMIS(String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList woList = new ArrayList();
        ArrayList newList = new ArrayList();
        woList = reportDAO.handleServiceDailyMIS(fromDate, toDate);
        if (woList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        } else {
            Iterator itr;
            itr = woList.iterator();
            ReportTO rto = null;
            ReportTO rtoNew = new ReportTO();
            String problem = "";
            int jcId = 0;
            while (itr.hasNext()) {
                rto = new ReportTO();
                rto = (ReportTO) itr.next();
                if (jcId != Integer.parseInt(rto.getJobCardId())) {
                    if (jcId != 0) {
                        newList.add(rtoNew);
                    }
                    rtoNew = new ReportTO();
                    rtoNew = rto;
                } else {
                    rtoNew.setProblemName(rtoNew.getProblemName() + "," + rto.getProblemName());
                }
                jcId = Integer.parseInt(rto.getJobCardId());

            }
        }
        //System.out.println("wolist:" + woList.size());
        //System.out.println("newlist:" + newList.size());
        return newList;
    }

    public ArrayList processRcExpenseGraph(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList rcExpenseSummary = new ArrayList();
        rcExpenseSummary = reportDAO.getRcExpenseData(reportTO);
        if (rcExpenseSummary.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return rcExpenseSummary;
    }

    public ArrayList processScrapGraphData(String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList scrapValue = new ArrayList();
        scrapValue = reportDAO.getScrapGraphData(fromDate, toDate);
        if (scrapValue.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return scrapValue;
    }

    public ArrayList processGetColourList() throws FPRuntimeException, FPBusinessException {

        ArrayList colourList = new ArrayList();
        colourList = reportDAO.getColourList();
        if (colourList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return colourList;
    }

    public ArrayList processGetContractVendor(int vendorId) throws FPRuntimeException, FPBusinessException {

        ArrayList contractVendor = new ArrayList();
        contractVendor = reportDAO.getContractVendor(vendorId);
        if (contractVendor.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return contractVendor;
    }

    public ArrayList processExternalLabourBillGraphData(String fromDate, String toDate, int vendorId) throws FPRuntimeException, FPBusinessException {

        ArrayList externalLabour = new ArrayList();
        externalLabour = reportDAO.getExternalLabourBillGraphData(fromDate, toDate, vendorId);
        if (externalLabour.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return externalLabour;
    }


    /*
     * Return The Count of Parts send for Serviced and received by Service point
     */
    public ArrayList processRcTrendGraphData(String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList rcServiceData = new ArrayList();
        rcServiceData = reportDAO.getRcTrendGraphData(fromDate, toDate);
        if (rcServiceData.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return rcServiceData;
    }

    public ArrayList processVehicleServiceGraphData(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList vehicleServiceData = new ArrayList();
        vehicleServiceData = reportDAO.getVehicleServiceGraphData(reportTO);
        if (vehicleServiceData.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return vehicleServiceData;
    }

    public ArrayList processMileageGraphData(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList mileageData = new ArrayList();
        mileageData = reportDAO.getMileageGraphData(reportTO);
        if (mileageData.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return mileageData;
    }

    public ArrayList processRcVendorList() throws FPRuntimeException, FPBusinessException {

        ArrayList rcVendorList = new ArrayList();
        rcVendorList = reportDAO.getRcVendorList();
        if (rcVendorList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return rcVendorList;
    }

    public ArrayList processRcExpenseAmount(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList rcExpenseAmount = new ArrayList();
        rcExpenseAmount = reportDAO.getRcExpenseAmount(reportTO);
        if (rcExpenseAmount.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return rcExpenseAmount;
    }

    public Float processActualPrice(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        Float actualPrice = 0.0f;
        actualPrice = reportDAO.getActualPrice(reportTO);

        return actualPrice;
    }
//    bala

    public ArrayList processUsageTypewiseData(ReportTO report) throws FPRuntimeException, FPBusinessException {

        ArrayList usageList = new ArrayList();
        usageList = reportDAO.getUsageTypewiseData(report);
        if (usageList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return usageList;
    }
//    public ArrayList processServiceTypewiseSummary(ReportTO report) throws FPRuntimeException, FPBusinessException {
//
//            ArrayList serviceList = new ArrayList();
//            serviceList = reportDAO.getServiceTypewiseData(report);
//            if (serviceList.size() == 0) {
//                throw new FPBusinessException("EM-GEN-01");
//            }
//            return serviceList;
//    }
//    bala ends

    public ArrayList processTallyXMLSummary(int companyId, String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList tallyXMLSummary = new ArrayList();
        tallyXMLSummary = reportDAO.getTallyXMLSummary(companyId, fromDate, toDate);
        if (tallyXMLSummary.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        ////////System.out.println("tallyXMLSummarySize in BP:" + tallyXMLSummary.size());
        return tallyXMLSummary;
    }

    public int processTallyXMLReport(ReportTO report) throws FPRuntimeException, FPBusinessException {

        int tallyXMLReport = 0;
        tallyXMLReport = reportDAO.getTallyXMLReport(report);
        if (tallyXMLReport == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        ////////System.out.println("tallyXMLReportSize in BP:" + tallyXMLReport);
        return tallyXMLReport;
    }

    public ArrayList processModifyTallyXMLPage(String xmlId) throws FPRuntimeException, FPBusinessException {

        ArrayList modifyTallyXMLPage = new ArrayList();
        modifyTallyXMLPage = reportDAO.getModifyTallyXMLPage(xmlId);
        if (modifyTallyXMLPage.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        ////////System.out.println("tallyXMLReportSize in BP:" + modifyTallyXMLPage.size());
        return modifyTallyXMLPage;
    }

    public int processModifyTallyXMLReport(ReportTO report, String xmlId) throws FPRuntimeException, FPBusinessException {

        int modifyTallyXMLReport = 0;
        modifyTallyXMLReport = reportDAO.getModifyTallyXMLReport(report, xmlId);
        if (modifyTallyXMLReport == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        ////////System.out.println("modifyTallyXMLReportSize in BP:" + modifyTallyXMLReport);
        return modifyTallyXMLReport;
    }

    public String handleDriverSettlement(String driName) {
        String driverName = "";
        driName = driName + "%";
        driverName = reportDAO.handleDriverSettlement(driName);
        return driverName;
    }

    public String handleVehicleNo(String regno) {
        String vehileRegNo = "";
        regno = regno + "%";
        vehileRegNo = reportDAO.handleVehicleNo(regno);
        return vehileRegNo;
    }

    public ArrayList driverSettlementReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList driverSettlementReport = new ArrayList();
        driverSettlementReport = reportDAO.getDriverSettlementReport(reportTO);
        if (driverSettlementReport.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        ////////System.out.println("tallyXMLReportSize in BP:" + driverSettlementReport.size());
        return driverSettlementReport;
    }

    public ArrayList searchProDriverSettlement(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList proDriverSettlement = new ArrayList();
        String[] temp = null;
        /*if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        proDriverSettlement = reportDAO.searchProDriverSettlement(fromDate, toDate, regNo, driId);
        if (proDriverSettlement.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return proDriverSettlement;
    }

    public ArrayList cleanerTripDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList cleanerTrip = new ArrayList();
        String[] temp = null;
        /*if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        cleanerTrip = reportDAO.cleanerTripDetails(fromDate, toDate, regNo, driId);
        if (cleanerTrip.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return cleanerTrip;
    }

    public ArrayList getFixedExpDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList fixedExpDetails = new ArrayList();
        String[] temp = null;
        /*if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        fixedExpDetails = reportDAO.getFixedExpDetails(fromDate, toDate, regNo, driId);
        ////////System.out.println("fixedExpDetails.size().. BP : " + fixedExpDetails.size());
        if (fixedExpDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return fixedExpDetails;
    }

    public ArrayList getDriverExpDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList driverExpDetails = new ArrayList();
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        driverExpDetails = reportDAO.getDriverExpDetails(fromDate, toDate, regNo, driId);
        if (driverExpDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return driverExpDetails;
    }

    public ArrayList getAdvDetails(String fromDate, String toDate, String regNo, int driId, String tripIds) throws FPBusinessException, FPRuntimeException {
        ArrayList AdvDetails = new ArrayList();
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        AdvDetails = reportDAO.getAdvDetails(fromDate, toDate, regNo, driId, tripIds);
        if (AdvDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return AdvDetails;
    }

    public ArrayList getFuelDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList fuelDetails = new ArrayList();
        /**
         * String[] temp = null; if (fromDate != "" && toDate != "") { temp =
         * fromDate.split("-"); String sDate = temp[2] + "-" + temp[1] + "-" +
         * temp[0]; fromDate = sDate; temp = toDate.split("-");
         *
         * String eDate = temp[2] + "-" + temp[1] + "-" + temp[0]; toDate =
         * eDate; }
         */
        fuelDetails = reportDAO.getFuelDetails(fromDate, toDate, regNo, driId);
        if (fuelDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return fuelDetails;
    }

    public ArrayList getHaltDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList haltDetails = new ArrayList();
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        haltDetails = reportDAO.getHaltDetails(fromDate, toDate, regNo, driId);
        if (haltDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return haltDetails;
    }

    public ArrayList getRemarkDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList remarkDetails = new ArrayList();
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        remarkDetails = reportDAO.getRemarkDetails(fromDate, toDate, regNo, driId);
        if (remarkDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return remarkDetails;
    }

    public ArrayList getSummaryDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList remarkDetails = new ArrayList();
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        remarkDetails = reportDAO.getSummaryDetails(fromDate, toDate, regNo, driId);
        if (remarkDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return remarkDetails;
    }

    public int insertEposTripExpenses(String tripId, String expenseDesc, String expenseAmt, String expenseDate, String expenseRemarks, int userId) {
        int status = 0;
        status = reportDAO.insertEposTripExpenses(tripId, expenseDesc, expenseAmt, expenseDate, expenseRemarks, userId);
        return status;
    }

    public int getTripCountDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int tripCount = 0;
        tripCount = reportDAO.getTripCountDetails(fromDate, toDate, regNo, driId);
        return tripCount;
    }

    public int getTotalTonnageDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int totalTonnage = 0;
        totalTonnage = reportDAO.getTotalTonnageDetails(fromDate, toDate, regNo, driId);
        return totalTonnage;
    }

    public int getOutKmDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int outKm = 0;
        outKm = reportDAO.getOutKmDetails(fromDate, toDate, regNo, driId);
        return outKm;
    }

    public int getInKmDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int inKm = 0;
        inKm = reportDAO.getInKmDetails(fromDate, toDate, regNo, driId);
        return inKm;
    }

    public ArrayList getTotFuelDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        ArrayList totalFuelDetails = new ArrayList();
        totalFuelDetails = reportDAO.getTotFuelDetails(fromDate, toDate, regNo, driId);
        return totalFuelDetails;
    }

    public int getDriverExpenseDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int driExpense = 0;
        driExpense = reportDAO.getDriverExpenseDetails(fromDate, toDate, regNo, driId);
        return driExpense;
    }

    public int getDriverSalaryDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int driExpense = 0;
        driExpense = reportDAO.getDriverSalaryDetails(fromDate, toDate, regNo, driId);
        return driExpense;
    }

    public int getGeneralExpenseDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int getExpense = 0;
        getExpense = reportDAO.getGeneralExpenseDetails(fromDate, toDate, regNo, driId);
        return getExpense;
    }

    public int getDriverAdvanceDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int advDetails = 0;
        advDetails = reportDAO.getDriverAdvanceDetails(fromDate, toDate, regNo, driId);
        return advDetails;
    }

    public int getDriverBataDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int advDetails = 0;
        advDetails = reportDAO.getDriverBataDetails(fromDate, toDate, regNo, driId);
        return advDetails;
    }

    public ArrayList getSettleCloseDetails(String fromDate, String toDate, String regNo, String driName) throws FPBusinessException, FPRuntimeException {
        ArrayList remarkDetails = new ArrayList();
        remarkDetails = reportDAO.getSettleCloseDetails(fromDate, toDate, regNo, driName);
        if (remarkDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return remarkDetails;
    }

    public ArrayList getVehicleCurrentStatus(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleCurrentStatus = new ArrayList();
        vehicleCurrentStatus = reportDAO.getVehicleCurrentStatus(reportTO);
        return vehicleCurrentStatus;
    }

    public ArrayList getCompanyWiseTripReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList companyWiseTripReport = new ArrayList();
        companyWiseTripReport = reportDAO.getCompanyWiseTripReport(reportTO);
        return companyWiseTripReport;
    }

    public ArrayList getVehicleList() throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleList = new ArrayList();
        vehicleList = reportDAO.getVehicleList();
        return vehicleList;
    }

    public ArrayList getVehiclePerformance(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehiclePerformance = new ArrayList();
        vehiclePerformance = reportDAO.getVehiclePerformance(reportTO);
        return vehiclePerformance;
    }

    public ArrayList getDriverPerformance(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList driverPerformance = new ArrayList();
        driverPerformance = reportDAO.getDriverPerformance(reportTO);
        return driverPerformance;
    }

    public String getDriverEmbarkedDate(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        String driverEmbarked = "";
        driverEmbarked = reportDAO.getDriverEmbarkedDate(reportTO);
        return driverEmbarked;
    }

    /**
     * This method used to LPS Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getlpsCount(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList lpsCount = new ArrayList();
        lpsCount = reportDAO.getlpsCount(reportTO);
        return lpsCount;
    }

    public ArrayList getlpsTrippedList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList lpsTrippedList = new ArrayList();
        lpsTrippedList = reportDAO.getlpsTrippedList(reportTO);
        return lpsTrippedList;
    }

    public ArrayList gettripList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripList = new ArrayList();
        tripList = reportDAO.gettripList(reportTO);
        return tripList;
    }

    /**
     * This method used to Trip wise P&L Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getlocationList() throws FPRuntimeException, FPBusinessException {
        ArrayList locationList = new ArrayList();
        locationList = reportDAO.getlocationList();
        return locationList;
    }

    public ArrayList getTripWisePandL(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripWiseList = new ArrayList();
        tripWiseList = reportDAO.getTripWisePandL(reportTO, userId);
        return tripWiseList;
    }

    public ArrayList getTripWisetotal(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripWiseTotal = new ArrayList();
        tripWiseTotal = reportDAO.getTripWisetotal(reportTO, userId);
        return tripWiseTotal;
    }

    /**
     * This method used to Trip wise P&L Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleWisePandL(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleWiseList = new ArrayList();
        vehicleWiseList = reportDAO.getVehicleWisePandL(reportTO, userId);
        return vehicleWiseList;
    }

    public ArrayList getVehicleWisetotal(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleWiseTotal = new ArrayList();
        vehicleWiseTotal = reportDAO.getVehicleWisetotal(reportTO, userId);
        return vehicleWiseTotal;
    }

    public ArrayList getCustomerList() throws FPBusinessException, FPRuntimeException {
        ArrayList customerList = new ArrayList();
        customerList = reportDAO.getCustomerList();
        return customerList;
    }

    /**
     * This method used to District names List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getDistrictNameList() throws FPRuntimeException, FPBusinessException {
        ArrayList districtNameList = new ArrayList();
        districtNameList = reportDAO.getDistrictNameList();
        return districtNameList;
    }

    /**
     * This method used to get District Wise Summart Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getDistrictSummaryList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList districtSummaryList = new ArrayList();
        districtSummaryList = reportDAO.getDistrictSummaryList(reportTO, userId);
        return districtSummaryList;
    }

    /**
     * This method used to get District Wise Details Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getDistrictDetailsList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList districtDetailsList = new ArrayList();
        districtDetailsList = reportDAO.getDistrictDetailsList(reportTO, userId);
        return districtDetailsList;
    }

    /**
     * This method used to get Trip Sheet Wise Summart Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getTripsheetDetailsList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripSheetDetailsList = new ArrayList();
        tripSheetDetailsList = reportDAO.getTripsheetDetailsList(reportTO, userId);
        return tripSheetDetailsList;
    }

    /**
     * This method used to get Consignee Wise Wise Summart Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getConsigneeWiseList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList consigneeWiseDetailsList = new ArrayList();
        consigneeWiseDetailsList = reportDAO.getConsigneeWiseList(reportTO, userId);
        return consigneeWiseDetailsList;
    }

    /**
     * This method used to get Trip Settlement Wise Summart Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getTripsettlementWiseList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripsettlementDetailsList = new ArrayList();
        tripsettlementDetailsList = reportDAO.getTripsettlementWiseList(reportTO, userId);
        return tripsettlementDetailsList;
    }

    public ArrayList getDieselReportList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList dieselReportList = new ArrayList();
        dieselReportList = reportDAO.getDieselReportList(reportTO, userId);
        return dieselReportList;
    }

    /**
     * This method used to get ConsigneeName Suggestions..
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String getConsigneeNameSuggestions(String consigneeName) {
        //  ////////System.out.println("i am in consignee name ajax ");
        String sugggestions = "";
        //////////System.out.println("consigneeName =  " + consigneeName);
        sugggestions = reportDAO.getConsigneeNameSuggestions(consigneeName);
        return sugggestions;
    }

    /**
     * This method used to get Trip Id Suggestions..
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String getTripIdSuggestions(String tripId) {
        String sugggestions = "";
        sugggestions = reportDAO.getTripIdSuggestions(tripId);
        return sugggestions;
    }

    /**
     * This method used to get Vehicle Number Suggestions..
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String getVehicleNumberSuggestions(String vehicleNo) {
        String sugggestions = "";
        sugggestions = reportDAO.getVehicleNumberSuggestions(vehicleNo);
        return sugggestions;
    }

    public ArrayList getCustomerOverDueList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList customerOverDueList = new ArrayList();
        customerOverDueList = reportDAO.getCustomerOverDueList(reportTO, userId);
        return customerOverDueList;
    }

    public ArrayList getSalesDashboard(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList response = new ArrayList();
        response = reportDAO.getSalesDashboard(reportTO, userId);
        return response;
    }

    public ArrayList getOpsDashboard(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList response = new ArrayList();
        response = reportDAO.getOpsDashboard(reportTO, userId);
        return response;
    }

    /**
     * This method used to get Account Head List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getAccountReceivableList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList accountReceivableList = new ArrayList();
        accountReceivableList = reportDAO.getAccountReceivableList(reportTO, userId);
        return accountReceivableList;
    }

    /**
     * This method used to get Customer Wise Profit List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getCustomerWiseProfitList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList customerWiseProfitList = new ArrayList();
        customerWiseProfitList = reportDAO.getCustomerWiseProfitList(reportTO, userId);
        return customerWiseProfitList;
    }

    /**
     * This method used to get Vehicle Details List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleDetailsList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleDetailsList = new ArrayList();
        vehicleDetailsList = reportDAO.getVehicleDetailsList(reportTO, userId);
        return vehicleDetailsList;

    }

    public ArrayList getTripSheetDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        ArrayList OperationExpensesList = new ArrayList();
        ArrayList tripEarnings = new ArrayList();
        ArrayList tripEarningsList = new ArrayList();
        tripDetails = reportDAO.getTripSheetDetails(reportTO);
        double freightAmount = 0.0;
        DprTO dprTO = new DprTO();
        ReportTO rpTO = new ReportTO();
        Iterator itr1 = tripDetails.iterator();
        ReportTO repTO1 = new ReportTO();

        /*
         while (itr1.hasNext()) {
         rpTO = new ReportTO();
         repTO1 = (ReportTO) itr1.next();
         tripEarnings = reportDAO.getTripEarningsList(reportTO);
         reportTO.setTripId(repTO1.getTripSheetId());
         rpTO.setTripId(reportTO.getTripId());
         rpTO.setConsignmentName(repTO1.getConsignmentName());
         rpTO.setTripCode(repTO1.getTripCode());
         rpTO.setCustomerName(repTO1.getCustomerName());
         rpTO.setConsignmentName(repTO1.getConsignmentName());
         rpTO.setCustomerTypeId(repTO1.getCustomerTypeId());
         rpTO.setBillingType(repTO1.getBillingType());
         rpTO.setRouteName(repTO1.getRouteName());
         rpTO.setVehicleTypeName(repTO1.getVehicleTypeName());
         rpTO.setReeferRequired(repTO1.getReeferRequired());
         rpTO.setVehicleNo(repTO1.getVehicleNo());
         rpTO.setVehicleId(repTO1.getVehicleId());
         rpTO.setDriverName(repTO1.getDriverName());
         rpTO.setCompanyName(repTO1.getCompanyName());
         ////////System.out.println("the tripearnings list" + tripEarnings.size());
         if (tripEarnings.size() > 0) {
         Iterator itr2 = tripEarnings.iterator();
         while (itr2.hasNext()) {
         dprTO = (DprTO) itr2.next();
         ////////System.out.println("repTO2.getTripNos() = " + dprTO.getTripNos());
         ////////System.out.println("repTO2.getFreightAmount() = " + dprTO.getFreightAmount());
         rpTO.setVehicleId(dprTO.getVehicleId());
         rpTO.setTripId(dprTO.getTripNos());
         rpTO.setFreightAmount(dprTO.getFreightAmount());
         rpTO.setOtherExpenseAmount(dprTO.getOtherExpenseAmount());
         //vehicleEarningsList.add(rpTO);
         }
         } else {
         rpTO.setVehicleId(repTO1.getVehicleId());
         rpTO.setTripNos("0");
         rpTO.setFreightAmount("0.00");
         rpTO.setOtherExpenseAmount("0.00");
         //vehicleEarningsList.add(rpTO);
         }
         DprTO dpr = new DprTO();
         double driverSalary = 0;
         driverSalary = reportDAO.getTripDriverSalary(reportTO);
         reportTO.setVehicleDriverSalary(driverSalary);
         DprTO dpr1 = new DprTO();
         OperationExpensesList = reportDAO.getTripOperationExpenseList(reportTO);
         if (OperationExpensesList.size() > 0) {
         Iterator itr4 = OperationExpensesList.iterator();
         while (itr4.hasNext()) {
         dpr1 = (DprTO) itr4.next();
         rpTO.setTollAmount(dpr1.getTollAmount());
         rpTO.setFuelAmount(dpr1.getFuelAmount());
         rpTO.setDriverIncentive(dpr1.getDriverIncentive());
         rpTO.setDriverBata(dpr1.getDriverBata());
         rpTO.setMiscAmount(dpr1.getMiscAmount());
         rpTO.setDriverExpense(dpr1.getDriverBata() + dpr1.getDriverIncentive() + dpr1.getMiscAmount());
         rpTO.setRouteExpenses(dpr1.getRouteExpenses());
         rpTO.setTripOtherExpense(dpr1.getTripOtherExpense());
         rpTO.setTotlalOperationExpense(dpr1.getTollAmount() + dpr1.getFuelAmount() + dpr1.getDriverIncentive() + dpr1.getMiscAmount() + dpr1.getDriverBata());
         ////////System.out.println("repTO2.getmiscamount() = " + dpr1.getTollAmount());
         //vehicleEarningsList.add(rpTO);
         }
         } else {
         rpTO.setVehicleId(repTO1.getVehicleId());
         rpTO.setTollAmount(0.00);
         rpTO.setFuelAmount(0.00);
         rpTO.setDriverIncentive(0.00);
         rpTO.setDriverBata(0.00);
         rpTO.setDriverExpense(0.00);
         rpTO.setRouteExpenses(0.00);
         rpTO.setTripOtherExpense(0.00);
         rpTO.setTotlalOperationExpense(0.00);
         //vehicleEarningsList.add(rpTO);
         }
         rpTO.setNetExpense(rpTO.getTotlalOperationExpense());
         freightAmount = Double.parseDouble(rpTO.getFreightAmount());
         rpTO.setNetProfit(freightAmount - rpTO.getNetExpense());
         tripEarningsList.add(rpTO);
         }
         */
        return tripDetails;
    }

    /**
     * This method used to get Vehicle Wise Profit List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleWiseProfitList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleDetailsList = new ArrayList();
        ArrayList vehicleEarnings = new ArrayList();
        ArrayList vehicleFixedExpensesList = new ArrayList();
        ArrayList vehicleOperationExpensesList = new ArrayList();
        ArrayList vehicleMaintainExpensesList = new ArrayList();
        ArrayList vehicleEarningsList = new ArrayList();
        ArrayList EarningsList = new ArrayList();
        vehicleDetailsList = reportDAO.getVehicleDetailsList(reportTO, userId);
        double freightAmount = 0.0;
        DprTO dprTO = new DprTO();
        ReportTO rpTO = new ReportTO();
        Iterator itr1 = vehicleDetailsList.iterator();
        ReportTO repTO1 = new ReportTO();
        while (itr1.hasNext()) {
            rpTO = new ReportTO();
            repTO1 = (ReportTO) itr1.next();
            reportTO.setVehicleId(repTO1.getVehicleId());
            vehicleEarnings = reportDAO.getVehicleEarningsList(reportTO, userId);
            if (vehicleEarnings.size() > 0) {
                Iterator itr2 = vehicleEarnings.iterator();
                while (itr2.hasNext()) {
                    dprTO = (DprTO) itr2.next();
                    ////////System.out.println("repTO2.getTripNos() = " + dprTO.getTripNos());
                    ////////System.out.println("repTO2.getFreightAmount() = " + dprTO.getFreightAmount());
                    rpTO.setVehicleId(dprTO.getVehicleId());
                    rpTO.setTripId(dprTO.getTripId());
                    rpTO.setTripNos(dprTO.getTripNos());
                    rpTO.setFreightAmount(dprTO.getFreightAmount());
                    rpTO.setOtherExpenseAmount(dprTO.getOtherExpenseAmount());
                    //vehicleEarningsList.add(rpTO);
                }
            } else {
                rpTO.setVehicleId(repTO1.getVehicleId());
                rpTO.setTripNos("0");
                rpTO.setFreightAmount("0.00");
                rpTO.setOtherExpenseAmount("0.00");
                //vehicleEarningsList.add(rpTO);
            }
            DprTO dpr = new DprTO();
            double driverSalary = 0;
            driverSalary = reportDAO.getVehicleDriverSalary(reportTO, userId);
            reportTO.setVehicleDriverSalary(driverSalary);
            vehicleFixedExpensesList = reportDAO.getVehicleFixedExpenseList(reportTO, userId);
            Iterator itr3 = vehicleFixedExpensesList.iterator();
            while (itr3.hasNext()) {
                dpr = (DprTO) itr3.next();
                rpTO.setInsuranceAmount(dpr.getInsuranceAmount());
                rpTO.setFcAmount(dpr.getFcAmount());
                rpTO.setRoadTaxAmount(dpr.getRoadTaxAmount());
                rpTO.setPermitAmount(dpr.getPermitAmount());
                rpTO.setEmiAmount(dpr.getEmiAmount());
                rpTO.setFixedExpensePerDay(dpr.getFixedExpensePerDay());
                rpTO.setTotlalFixedExpense(dpr.getTotlalFixedExpense());
                rpTO.setVehicleDriverSalary(dpr.getVehicleDriverSalary());
                driverSalary = dpr.getVehicleDriverSalary();
                //vehicleEarningsList.add(rpTO);
            }
            DprTO dpr1 = new DprTO();

            vehicleOperationExpensesList = reportDAO.getVehicleOperationExpenseList(reportTO, userId);
            if (vehicleOperationExpensesList.size() > 0) {
                Iterator itr4 = vehicleOperationExpensesList.iterator();
                while (itr4.hasNext()) {
                    dpr1 = (DprTO) itr4.next();
                    rpTO.setTollAmount(dpr1.getTollAmount());
                    rpTO.setFuelAmount(dpr1.getFuelAmount());
                    rpTO.setDriverIncentive(dpr1.getDriverIncentive());
                    rpTO.setDriverBata(dpr1.getDriverBata());
                    rpTO.setMiscAmount(dpr1.getMiscAmount());
                    rpTO.setDriverExpense(dpr1.getDriverBata() + dpr1.getDriverIncentive() + dpr1.getMiscAmount());
                    rpTO.setRouteExpenses(dpr1.getRouteExpenses());
                    rpTO.setTripOtherExpense(dpr1.getTripOtherExpense());
                    rpTO.setTotlalOperationExpense(dpr1.getTollAmount() + dpr1.getFuelAmount() + dpr1.getDriverIncentive() + dpr1.getMiscAmount() + dpr1.getDriverBata() + dpr1.getTripOtherExpense() + driverSalary);
                    //vehicleEarningsList.add(rpTO);
                }
            } else {
                rpTO.setVehicleId(repTO1.getVehicleId());
                rpTO.setTollAmount(0.00);
                rpTO.setFuelAmount(0.00);
                rpTO.setDriverIncentive(0.00);
                rpTO.setDriverBata(0.00);
                rpTO.setDriverExpense(0.00);
                rpTO.setRouteExpenses(0.00);
                rpTO.setTripOtherExpense(0.00);
                rpTO.setTotlalOperationExpense(0.00);
                //vehicleEarningsList.add(rpTO);
            }
            DprTO dpr2 = new DprTO();
            vehicleMaintainExpensesList = reportDAO.getVehicleMaintainExpenses(reportTO, userId);
            if (vehicleMaintainExpensesList.size() > 0) {
                Iterator itr5 = vehicleMaintainExpensesList.iterator();
                while (itr5.hasNext()) {
                    dpr2 = (DprTO) itr5.next();
                    rpTO.setMaintainExpense(dpr2.getMaintainExpense());
                    //vehicleEarningsList.add(rpTO);
                }
            } else {
                rpTO.setVehicleId(repTO1.getVehicleId());
                rpTO.setMaintainExpense(0.00);
                //vehicleEarningsList.add(rpTO);
            }
            rpTO.setNetExpense(rpTO.getTotlalFixedExpense() + rpTO.getTotlalOperationExpense() + rpTO.getMaintainExpense());
            freightAmount = Double.parseDouble(rpTO.getFreightAmount());
            rpTO.setNetProfit(freightAmount - rpTO.getNetExpense());
            vehicleEarningsList.add(rpTO);
        }
        return vehicleEarningsList;
    }

    public ArrayList getVehicleUtilizationList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleUtilizationList = new ArrayList();
        vehicleUtilizationList = reportDAO.getVehicleUtilizationList(reportTO, userId);
        return vehicleUtilizationList;

    }

    public ArrayList getPopupCustomerProfitReportDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList popupCustomerProfitReport = new ArrayList();
        popupCustomerProfitReport = reportDAO.getPopupCustomerProfitReportDetails(reportTO);
        return popupCustomerProfitReport;

    }

    public ArrayList getGPSLogDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList logDetails = new ArrayList();
        logDetails = reportDAO.getGPSLogDetails(reportTO, userId);
        return logDetails;

    }

    public ArrayList getDriverSettlementDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList driverSettlementDetails = new ArrayList();
        driverSettlementDetails = reportDAO.getDriverSettlementDetails(reportTO, userId);
        return driverSettlementDetails;

    }

    public ArrayList getTripLogDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripLogDetails = new ArrayList();
        tripLogDetails = reportDAO.getTripLogDetails(reportTO, userId);
        return tripLogDetails;

    }

    public ArrayList getBPCLTransactionDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList BPCLTransactionDetails = new ArrayList();
        BPCLTransactionDetails = reportDAO.getBPCLTransactionDetails(reportTO, userId);
        return BPCLTransactionDetails;

    }

    public ArrayList getTripGpsStatusDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripGpsStatusDetails = new ArrayList();
        tripGpsStatusDetails = reportDAO.getTripGpsStatusDetails(reportTO);
        return tripGpsStatusDetails;

    }

    public ArrayList getTripCodeList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripCodeList = new ArrayList();
        tripCodeList = reportDAO.getTripCodeList(reportTO);
        return tripCodeList;
    }

    public ArrayList getTripDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = reportDAO.getTripDetails(reportTO);
        return tripDetails;
    }

    public ArrayList getWflList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList wflList = new ArrayList();
        wflList = reportDAO.getWflList(reportTO);
        return wflList;
    }

    public ArrayList getWfuList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList wfuList = new ArrayList();
        wfuList = reportDAO.getWfuList(reportTO);
        return wfuList;
    }

    public ArrayList getTripInProgressList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripInProgressList = new ArrayList();
        tripInProgressList = reportDAO.getTripInProgressList(reportTO);
        return tripInProgressList;
    }

    public ArrayList getTripNotStartedList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripNotStartedList = new ArrayList();
        tripNotStartedList = reportDAO.getTripNotStartedList(reportTO);
        return tripNotStartedList;
    }

    public ArrayList getJobCardList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList jobCardList = new ArrayList();
        jobCardList = reportDAO.getJobCardList(reportTO);
        return jobCardList;
    }

    public ArrayList getFutureTripList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList futureTripList = new ArrayList();
        futureTripList = reportDAO.getFutureTripList(reportTO);
        return futureTripList;
    }

    public int getWflWfuStatus(String currentDate) {
        int wflWfuStatus = 0;
        wflWfuStatus = reportDAO.getWflWfuStatus(currentDate);
        return wflWfuStatus;
    }

    public int getWflWfuLogId(String currentDate) {
        int wflwfuLogId = 0;
        wflwfuLogId = reportDAO.getWflWfuLogId(currentDate);
        return wflwfuLogId;
    }

    public String getTotalTripSummaryDetails(ReportTO reportTO) {
        String totalTripSummaryDetails = "";
        totalTripSummaryDetails = reportDAO.getTotalTripSummaryDetails(reportTO);
        return totalTripSummaryDetails;
    }

    public ArrayList getTripSheetList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripSheetList = new ArrayList();
        tripSheetList = reportDAO.getTripSheetList(reportTO);
        return tripSheetList;
    }

    public String getMarginWiseTripSummary(ReportTO reportTO) {
        String totalTripSummaryDetails = "";
        totalTripSummaryDetails = reportDAO.getMarginWiseTripSummary(reportTO);
        return totalTripSummaryDetails;
    }

    public ArrayList getMonthWiseEmptyRunSummary(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList monthWiseEmptyRunSummary = new ArrayList();
        monthWiseEmptyRunSummary = reportDAO.getMonthWiseEmptyRunSummary(reportTO);
        return monthWiseEmptyRunSummary;
    }

    public ArrayList getJobcardSummary(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList jobcardSummary = new ArrayList();
        jobcardSummary = reportDAO.getJobcardSummary(reportTO);
        return jobcardSummary;
    }

    public ArrayList getTripMergingDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList TripMergingDetails = new ArrayList();
        TripMergingDetails = reportDAO.getTripMergingDetails(reportTO, userId);
        return TripMergingDetails;

    }

    public ArrayList getVehicleReadingDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleReadingDetails = new ArrayList();
        vehicleReadingDetails = reportDAO.getVehicleReadingDetails(reportTO, userId);
        return vehicleReadingDetails;

    }

    public ArrayList getTripMergingList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripMergingList = new ArrayList();
        tripMergingList = reportDAO.getTripMergingList(reportTO, userId);
        return tripMergingList;
    }

    public ArrayList getTripNotMergingList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripNotMergingList = new ArrayList();
        tripNotMergingList = reportDAO.getTripNotMergingList(reportTO, userId);
        return tripNotMergingList;
    }

    public ArrayList getCustomerMergingList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList customerTripMergingList = new ArrayList();
        customerTripMergingList = reportDAO.getCustomerMergingList(reportTO, userId);
        return customerTripMergingList;
    }

    public ArrayList getCustomerTripNotMergingList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList customerTripNotMergingList = new ArrayList();
        customerTripNotMergingList = reportDAO.getCustomerTripNotMergingList(reportTO, userId);
        return customerTripNotMergingList;
    }

    public ArrayList getCustomerLists(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList customerList = new ArrayList();
        customerList = reportDAO.getCustomerLists(reportTO, userId);
        return customerList;
    }

    public ArrayList getPopupCustomerMergingProfitReportDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList popupCustomerMergingProfitReport = new ArrayList();
        popupCustomerMergingProfitReport = reportDAO.getPopupCustomerMergingProfitReportDetails(reportTO);
        return popupCustomerMergingProfitReport;

    }

    public ArrayList getTripExtraExpenseDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList BPCLTransactionDetails = new ArrayList();
        BPCLTransactionDetails = reportDAO.getTripExtraExpenseDetails(reportTO, userId);
        return BPCLTransactionDetails;

    }

    public ArrayList getVehicleDriverAdvanceDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleDriverAdvanceDetails = new ArrayList();
        vehicleDriverAdvanceDetails = reportDAO.getVehicleDriverAdvanceDetails(reportTO, userId);
        return vehicleDriverAdvanceDetails;

    }

    public ArrayList getVehicleList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleList = new ArrayList();
        vehicleList = reportDAO.getVehicleList(reportTO);
        return vehicleList;
    }

    public ArrayList getTripVehicleList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleList = new ArrayList();
        vehicleList = reportDAO.getTripVehicleList(reportTO);
        return vehicleList;
    }

    public ArrayList getTripSheetListWfl(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripSheetList = new ArrayList();
        tripSheetList = reportDAO.getTripSheetListWfl(reportTO);
        return tripSheetList;
    }

    public String getWflHours(ReportTO reportTO) {
        String totalTripSummaryDetails = "";
        totalTripSummaryDetails = reportDAO.getWflHours(reportTO);
        return totalTripSummaryDetails;
    }

    public ArrayList getLatestUpdates(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList latestUpdates = new ArrayList();
        latestUpdates = reportDAO.getLatestUpdates(reportTO);
        return latestUpdates;
    }

    public ArrayList getToPayCustomerTripDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList ToPayCustomerTripDetails = new ArrayList();
        ToPayCustomerTripDetails = reportDAO.getToPayCustomerTripDetails(reportTO, userId);
        return ToPayCustomerTripDetails;

    }

    public ArrayList getTripVmrDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList TripVmrDetails = new ArrayList();
        TripVmrDetails = reportDAO.getTripVmrDetails(reportTO, userId);
        return TripVmrDetails;

    }

    public ArrayList getFcWiseTripBudgetDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList budgetDetails = new ArrayList();
        budgetDetails = reportDAO.getFcWiseTripBudgetDetails(reportTO, userId);
        return budgetDetails;

    }

    public ArrayList getAccountMgrPerformanceReportDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList budgetDetails = new ArrayList();
        budgetDetails = reportDAO.getAccountMgrPerformanceReportDetails(reportTO, userId);
        return budgetDetails;

    }

    public ArrayList getRNMExpenseReportDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList RnMExpenseDetails = new ArrayList();
        RnMExpenseDetails = reportDAO.getRNMExpenseReportDetails(reportTO, userId);
        return RnMExpenseDetails;

    }

    public ArrayList getTyreExpenseReportDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList RnMExpenseDetails = new ArrayList();
        RnMExpenseDetails = reportDAO.getRNMExpenseReportDetails(reportTO, userId);
        return RnMExpenseDetails;

    }

    public ArrayList getRNMVehicleList(String type) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleList = new ArrayList();
        vehicleList = reportDAO.getRNMVehicleList(type);
        return vehicleList;

    }

    public ArrayList getVehicleTyreList(String type) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleList = new ArrayList();
        vehicleList = reportDAO.getVehicleTyreList(type);
        return vehicleList;

    }

    public ArrayList processReportList(ReportTO reportTO) throws FPBusinessException, FPRuntimeException {
        ArrayList reportList = new ArrayList();
        reportList = reportDAO.getReportList(reportTO);
        ////////System.out.println("reportList bp size"+reportList.size());

        return reportList;
    }

    public ArrayList getVehicleNoforReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleNoList = new ArrayList();
        vehicleNoList = reportDAO.getVehicleNoforReport(reportTO);
        return vehicleNoList;
    }

    public ArrayList getOrderExpenseRevenue(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList orderExpenseRevenue = new ArrayList();
        orderExpenseRevenue = reportDAO.getOrderExpenseRevenue(reportTO);
        return orderExpenseRevenue;
    }

    public ArrayList getTrailerMovementList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList trailerMovementList = new ArrayList();
        trailerMovementList = reportDAO.getTrailerMovementList(reportTO);
        return trailerMovementList;
    }

    public ArrayList getTripTrailerProfitDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripTrailerProfitDetails = new ArrayList();
        tripTrailerProfitDetails = reportDAO.getTripTrailerProfitDetails(reportTO, userId);
        return tripTrailerProfitDetails;
    }

    public ArrayList getTrailerWiseProfitList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleDetailsList = new ArrayList();
        ArrayList vehicleEarnings = new ArrayList();
        ArrayList vehicleFixedExpensesList = new ArrayList();
        ArrayList vehicleOperationExpensesList = new ArrayList();
        ArrayList vehicleMaintainExpensesList = new ArrayList();
        ArrayList vehicleEarningsList = new ArrayList();
        ArrayList EarningsList = new ArrayList();
        vehicleDetailsList = reportDAO.getTrailerDetailsList(reportTO, userId);
        double freightAmount = 0.0;
        DprTO dprTO = new DprTO();
        ReportTO rpTO = new ReportTO();
        Iterator itr1 = vehicleDetailsList.iterator();
        ReportTO repTO1 = new ReportTO();
        while (itr1.hasNext()) {
            rpTO = new ReportTO();
            repTO1 = (ReportTO) itr1.next();
            reportTO.setTrailerId(repTO1.getTrailerId());
            vehicleEarnings = reportDAO.getTrailerEarningsList(reportTO, userId);
            if (vehicleEarnings.size() > 0) {
                Iterator itr2 = vehicleEarnings.iterator();
                while (itr2.hasNext()) {
                    dprTO = (DprTO) itr2.next();
                    ////////System.out.println("repTO2.getTripNos() = " + dprTO.getTripNos());
                    ////////System.out.println("repTO2.getFreightAmount() = " + dprTO.getFreightAmount());
                    rpTO.setTrailerId(dprTO.getTrailerId());
                    rpTO.setTripId(dprTO.getTripId());
                    rpTO.setTripNos(dprTO.getTripNos());
                    rpTO.setFreightAmount(dprTO.getFreightAmount());
                    rpTO.setOtherExpenseAmount(dprTO.getOtherExpenseAmount());
                    //vehicleEarningsList.add(rpTO);
                }
            } else {
                rpTO.setTrailerId(repTO1.getTrailerId());
                rpTO.setTripNos("0");
                rpTO.setFreightAmount("0.00");
                rpTO.setOtherExpenseAmount("0.00");
                //vehicleEarningsList.add(rpTO);
            }
            DprTO dpr = new DprTO();
            double driverSalary = 0;
            driverSalary = reportDAO.getVehicleDriverSalary(reportTO, userId);
            reportTO.setVehicleDriverSalary(driverSalary);
            vehicleFixedExpensesList = reportDAO.getTrailerFixedExpenseList(reportTO, userId);
            Iterator itr3 = vehicleFixedExpensesList.iterator();
            while (itr3.hasNext()) {
                dpr = (DprTO) itr3.next();
                rpTO.setInsuranceAmount(dpr.getInsuranceAmount());
                rpTO.setFcAmount(dpr.getFcAmount());
                rpTO.setRoadTaxAmount(dpr.getRoadTaxAmount());
                rpTO.setPermitAmount(dpr.getPermitAmount());
                rpTO.setEmiAmount(dpr.getEmiAmount());
                rpTO.setFixedExpensePerDay(dpr.getFixedExpensePerDay());
                rpTO.setTotlalFixedExpense(dpr.getTotlalFixedExpense());
                rpTO.setVehicleDriverSalary(dpr.getVehicleDriverSalary());
                driverSalary = dpr.getVehicleDriverSalary();
                //vehicleEarningsList.add(rpTO);
            }
            DprTO dpr1 = new DprTO();
            double totalOperationExpense = 0;
            vehicleOperationExpensesList = reportDAO.getTrailerOperationExpenseList(reportTO, userId);
            if (vehicleOperationExpensesList.size() > 0) {

                Iterator itr4 = vehicleOperationExpensesList.iterator();
                while (itr4.hasNext()) {
                    dpr1 = (DprTO) itr4.next();
                    rpTO.setTrailerExpence(dpr1.getTrailerExpence());
                    rpTO.setTrailerRevenue(dpr1.getTrailerRevenue());
                    totalOperationExpense = Double.parseDouble(dpr1.getTrailerExpence());
                    rpTO.setTotlalOperationExpense(totalOperationExpense);
                    //vehicleEarningsList.add(rpTO);
                }
            } else {
                rpTO.setTrailerId(repTO1.getTrailerId());
                rpTO.setTrailerExpence("0.00");
                rpTO.setTrailerRevenue("0.00");
                rpTO.setTotlalOperationExpense(0.00);

                //vehicleEarningsList.add(rpTO);
            }
            DprTO dpr2 = new DprTO();
            vehicleMaintainExpensesList = reportDAO.getTrailerMaintainExpenses(reportTO, userId);
            if (vehicleMaintainExpensesList.size() > 0) {

                Iterator itr5 = vehicleMaintainExpensesList.iterator();
                while (itr5.hasNext()) {
                    dpr2 = (DprTO) itr5.next();
                    rpTO.setMaintainExpense(dpr2.getMaintainExpense());
                    //vehicleEarningsList.add(rpTO);
                }
            } else {
                rpTO.setTrailerId(repTO1.getTrailerId());
                rpTO.setMaintainExpense(0.00);
                //vehicleEarningsList.add(rpTO);
            }
            rpTO.setNetExpense(rpTO.getTotlalFixedExpense() + rpTO.getTotlalOperationExpense() + rpTO.getMaintainExpense());
            freightAmount = Double.parseDouble(rpTO.getFreightAmount());
            rpTO.setNetProfit(freightAmount - rpTO.getNetExpense());
            vehicleEarningsList.add(rpTO);
        }
        return vehicleEarningsList;
    }

    public ArrayList getTrailerDetailsList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleDetailsList = new ArrayList();
        vehicleDetailsList = reportDAO.getTrailerDetailsList(reportTO, userId);
        return vehicleDetailsList;

    }

    public ArrayList getDashboardopsTruckNos(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getTruckNos = new ArrayList();
        getTruckNos = reportDAO.getDashboardopsTruckNos(reportTO);
        return getTruckNos;
    }

    public ArrayList getDashboardWorkShop(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getDashboardWorkShop = new ArrayList();
        getDashboardWorkShop = reportDAO.getDashboardWorkShop(reportTO);
        return getDashboardWorkShop;
    }

    public ArrayList getVehicleUT() throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleMake = new ArrayList();
        vehicleMake = reportDAO.getVehicleUT();
        return vehicleMake;
    }

    public ArrayList dbTrailerMake() throws FPRuntimeException, FPBusinessException {
        ArrayList trailerMake = new ArrayList();
        trailerMake = reportDAO.dbTrailerMake();
        return trailerMake;
    }

    public ArrayList dbTruckType() throws FPRuntimeException, FPBusinessException {
        ArrayList truckType = new ArrayList();
        truckType = reportDAO.dbTruckType();
        return truckType;
    }

    public ArrayList dbTrailerType() throws FPRuntimeException, FPBusinessException {
        ArrayList trailerType = new ArrayList();
        trailerType = reportDAO.dbTrailerType();
        return trailerType;
    }

    public ArrayList getTrailerJobCardMTD() throws FPRuntimeException, FPBusinessException {
        ArrayList jobcardMTD = new ArrayList();
        jobcardMTD = reportDAO.getTrailerJobCardMTD();
        return jobcardMTD;
    }

    public ArrayList getTruckJobCardMTD() throws FPRuntimeException, FPBusinessException {
        ArrayList truckjobcardMTD = new ArrayList();
        truckjobcardMTD = reportDAO.getTruckJobCardMTD();
        return truckjobcardMTD;
    }

    public ArrayList processMechPerformanceList(ReportTO repTO, String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList mechPerformanceList = new ArrayList();
        mechPerformanceList = reportDAO.processMechPerformanceList(repTO, fromDate, toDate);

        if (mechPerformanceList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return mechPerformanceList;
    }

    public ArrayList processMechPerformanceData(ReportTO repTO, String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList mechPerformanceList = new ArrayList();
        mechPerformanceList = reportDAO.processMechPerformanceData(repTO, fromDate, toDate);

        if (mechPerformanceList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return mechPerformanceList;
    }

    public ArrayList ViewQBEntityList() throws FPRuntimeException, FPBusinessException {
        ArrayList ViewQBEntityList = new ArrayList();
        ViewQBEntityList = reportDAO.ViewQBEntityList();
        return ViewQBEntityList;
    }

    public ArrayList viewQueryBuilder() throws FPRuntimeException, FPBusinessException {
        String getPanNo = "";

        ArrayList viewTabels = new ArrayList();
        try {
            viewTabels = reportDAO.viewQueryBuilder();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return viewTabels;
    }

    public int saveQueryBuilderValues(ReportTO reportTO) {
        int status = 0;
        status = reportDAO.saveQueryBuilderValues(reportTO);
        return status;
    }

    public int editQBEntityDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = reportDAO.editQBEntityDetails(reportTO);
        return status;

    }

    public ArrayList ViewQBEntityListFilter(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList ViewQBEntityListFilter = new ArrayList();
        ViewQBEntityListFilter = reportDAO.ViewQBEntityListFilter(reportTO);
        return ViewQBEntityListFilter;
    }

    public ArrayList viewQBReportList() throws FPRuntimeException, FPBusinessException {
        ArrayList viewQBReportList = new ArrayList();
        viewQBReportList = reportDAO.viewQBReportList();
        return viewQBReportList;

    }

    public ArrayList getQBEntityList() throws FPRuntimeException, FPBusinessException {
        ArrayList entitylist = new ArrayList();
        entitylist = reportDAO.getQBEntityList();
        return entitylist;
    }

    public ArrayList viewQBReportHeader(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList viewQBReportHeader = new ArrayList();
        viewQBReportHeader = reportDAO.viewQBReportHeader(reportTO);
        return viewQBReportHeader;
    }

    public String getReportName(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        String reportName = "";
        reportName = reportDAO.getReportName(reportTO);
        return reportName;
    }

    public String viewQBIndividualReportList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        String viewQBIndividualReportList = "";
        viewQBIndividualReportList = reportDAO.viewQBIndividualReportList(reportTO);
        return viewQBIndividualReportList;

    }

    public ArrayList getAvailableQBColumn(int entityId) throws FPRuntimeException, FPBusinessException {
        ArrayList getfuncrole = new ArrayList();
        getfuncrole = reportDAO.getAvailableQBColumn(entityId);
        return getfuncrole;
    }

    public ArrayList reportQBFilterValue(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList reportQBFilterValue = new ArrayList();
        reportQBFilterValue = reportDAO.reportQBFilterValue(reportTO);
        return reportQBFilterValue;
    }

    public ArrayList clientQBReportFilter(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList clientQBReportFilter = new ArrayList();
        clientQBReportFilter = reportDAO.clientQBReportFilter(reportTO);
        return clientQBReportFilter;
    }

    public ArrayList selectedReportQBColumn(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList selectedReportQBColumn = new ArrayList();
        selectedReportQBColumn = reportDAO.selectedReportQBColumn(reportTO);
        return selectedReportQBColumn;
    }

    public ArrayList editQBOnDemandReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList editQBOnDemandReport = new ArrayList();
        editQBOnDemandReport = reportDAO.editQBOnDemandReport(reportTO);
        return editQBOnDemandReport;
    }

    public ArrayList viewQBEntityDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList viewQBEntityDetails = new ArrayList();
        viewQBEntityDetails = reportDAO.viewQBEntityDetails(reportTO);
        return viewQBEntityDetails;
    }

    public ArrayList viewQBEntity(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList viewQBEntity = new ArrayList();
        viewQBEntity = reportDAO.viewQBEntity(reportTO);
        return viewQBEntity;
    }

    // QuertBuilder Ends Here
    public ArrayList getDailyTripPlaningDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = reportDAO.getDailyTripPlaningDetails(reportTO, userId);
        return tripDetails;

    }

    public ArrayList getUserLoginList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList userLoginList = new ArrayList();
        userLoginList = reportDAO.getUserLoginList(reportTO, userId);
        return userLoginList;

    }

    public ArrayList getUserLoginActivityDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList userLoginActivityList = new ArrayList();
        userLoginActivityList = reportDAO.getUserLoginActivityDetails(reportTO, userId);
        return userLoginActivityList;
    }

    public ArrayList getOrderWiseProfitDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList orderWiseProfitDetails = new ArrayList();
        orderWiseProfitDetails = reportDAO.getOrderWiseProfitDetails(reportTO);
        return orderWiseProfitDetails;
    }

    public ArrayList getEndedTripDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = reportDAO.getEndedTripDetails(reportTO);
        return tripDetails;
    }

    public ArrayList getTripExpensePrintDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = reportDAO.getTripExpensePrintDetails(reportTO);
        return tripDetails;
    }

    public ArrayList getTripExpenseSummaryDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = reportDAO.getTripExpenseSummaryDetails(reportTO);
        return tripDetails;
    }

    public ArrayList getTripReportDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = reportDAO.getTripReportDetails(reportTO);
        return tripDetails;
    }

    public ArrayList getTripDetailsForOrder(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = reportDAO.getTripDetailsForOrder(reportTO);
        return tripDetails;
    }

    public ArrayList getShipperName(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getShipperList = new ArrayList();
        getShipperList = reportDAO.getShipperName(reportTO);
        return getShipperList;
    }

    public ArrayList processGRSummaryList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getShipperList = new ArrayList();
        getShipperList = reportDAO.processGRSummaryList(reportTO);
        return getShipperList;
    }

    public ArrayList dailyVehicleStatus(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getShipperList = new ArrayList();
        getShipperList = reportDAO.dailyVehicleStatus(reportTO);
        return getShipperList;
    }

    public ArrayList getVendorList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vendorList = new ArrayList();
        vendorList = reportDAO.getVendorList(reportTO);
        return vendorList;
    }

    public String getTripDieselCost(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        String dieselCost = "";
        dieselCost = reportDAO.getTripDieselCost(reportTO);
        return dieselCost;
    }

    public ArrayList getVehicleVendorwiseList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList getVehicleVendorwiseList = new ArrayList();
        getVehicleVendorwiseList = reportDAO.getVehicleVendorwiseList(reportTO, userId);
        return getVehicleVendorwiseList;
    }

    public ArrayList getDailyCashDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList dailyCashDetails = new ArrayList();
        dailyCashDetails = reportDAO.getDailyCashDetails(reportTO, userId);
        return dailyCashDetails;
    }

    public String getCashReceiveDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        String cashReceived = "";
        cashReceived = reportDAO.getCashReceiveDetails(reportTO, userId);
        return cashReceived;
    }

    public ArrayList processGRprofitabilityList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList processGRprofitabilityList = new ArrayList();
        processGRprofitabilityList = reportDAO.processGRprofitabilityList(reportTO);
        return processGRprofitabilityList;
    }

    public String getOrdersDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        String Orderdetails = "";
        Orderdetails = reportDAO.getOrdersDetails(reportTO, userId);
        return Orderdetails;
    }

    public ArrayList getContainerMovementList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList containerEmptyMovementList = new ArrayList();
        containerEmptyMovementList = reportDAO.getContainerMovementList(reportTO, userId);
        return containerEmptyMovementList;
    }

    public ArrayList getcontainerExportMovementList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList containerEmptyMovementList = new ArrayList();
        containerEmptyMovementList = reportDAO.getcontainerExportMovementList(reportTO, userId);
        return containerEmptyMovementList;
    }

    public ArrayList getcontainerImportMovementList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList containerEmptyMovementList = new ArrayList();
        containerEmptyMovementList = reportDAO.getcontainerImportMovementList(reportTO, userId);
        return containerEmptyMovementList;
    }

    public ArrayList getVehicleDetainDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList VehicleDetainDetails = new ArrayList();
        VehicleDetainDetails = reportDAO.getVehicleDetainDetails(reportTO, userId);
        return VehicleDetainDetails;
    }

    public ArrayList getInvoiceEditLogDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList invoiceEditLogDetails = new ArrayList();
        invoiceEditLogDetails = reportDAO.getInvoiceEditLogDetails(reportTO, userId);
        return invoiceEditLogDetails;
    }

    public ArrayList tripStatusList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripStatusList = new ArrayList();
        tripStatusList = reportDAO.tripStatusList(reportTO);
        return tripStatusList;
    }

    public ArrayList getGpsVehiclelist(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList gpsVehiclelist = new ArrayList();
        gpsVehiclelist = reportDAO.getGpsVehiclelist(reportTO);
        return gpsVehiclelist;

    }

    public ArrayList invoiceDetailsReport(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList invoiceDetailsReport = new ArrayList();
        invoiceDetailsReport = reportDAO.invoiceDetailsReport(reportTO, userId);
        return invoiceDetailsReport;
    }

    public ArrayList suppInvoiceDetailsReport(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList invoiceDetailsReport = new ArrayList();
        invoiceDetailsReport = reportDAO.suppInvoiceDetailsReport(reportTO, userId);
        return invoiceDetailsReport;
    }

    public ArrayList getContractRateEditLogDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList contractRateEditLogDetails = new ArrayList();
        contractRateEditLogDetails = reportDAO.getContractRateEditLogDetails(reportTO, userId);
        return contractRateEditLogDetails;
    }

    public ArrayList getBlockedGrSummaryDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList grSummaryList = new ArrayList();
        grSummaryList = reportDAO.getBlockedGrSummaryDetails(reportTO);
        return grSummaryList;
    }

    public ArrayList getInvoiceReport(ReportTO reportTO, String status, String gstStatusType, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList invoiceReport = new ArrayList();
        invoiceReport = reportDAO.getInvoiceReport(reportTO, status, gstStatusType, userId);
        return invoiceReport;

    }

    public ArrayList getDispatchReport(ReportTO reportTO, int userId, String param) throws FPRuntimeException, FPBusinessException {
        ArrayList dispatchReport = new ArrayList();
        dispatchReport = reportDAO.getDispatchReport(reportTO, userId, param);
        return dispatchReport;

    }

    public ArrayList getExpenseTypeList() throws FPRuntimeException, FPBusinessException {
        ArrayList expenseList = new ArrayList();
        expenseList = reportDAO.getExpenseTypeList();
        return expenseList;
    }

    public ArrayList getGrExpenseDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList grExpenseDetails = new ArrayList();
        grExpenseDetails = reportDAO.getGrExpenseDetails(reportTO);
        return grExpenseDetails;
    }

    public int saveInvoiceSubmissionDate(String[] invSubmissionDate, String[] podDate, String[] invoiceIds, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = reportDAO.saveInvoiceSubmissionDate(invSubmissionDate, podDate, invoiceIds, userId);
        return status;
    }

    public String getInvoiceId(String invoiceCode) throws FPRuntimeException, FPBusinessException {
        String invoiceId = "";
        invoiceId = reportDAO.getInvoiceId(invoiceCode);
        return invoiceId;
    }

    public ArrayList getFuelStat(String fromDate, String toDate) {
        ArrayList fuelData = new ArrayList();

        fuelData = reportDAO.getFuelStat(fromDate, toDate);
        return fuelData;
    }

    public ArrayList creditNoteDetailsReport(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList invoiceDetailsReport = new ArrayList();
        invoiceDetailsReport = reportDAO.creditNoteDetailsReport(reportTO, userId);
        return invoiceDetailsReport;
    }

    public ArrayList grStatusReport(String grNo, String containerNo, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList grStatusReport = new ArrayList();
        grStatusReport = reportDAO.grStatusReport(grNo, containerNo, userId);
        return grStatusReport;
    }

    public ArrayList tripEndedNotClosedBeyond24(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripList = new ArrayList();
        tripList = reportDAO.tripEndedNotClosedBeyond24(reportTO);
        return tripList;
    }

    public Map payablePartReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        Map tripMap = new HashMap();
        tripMap = reportDAO.payablePartReport(reportTO);
        return tripMap;
    }

    public Map receivablePartReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        Map tripMap = new HashMap();
        tripMap = reportDAO.receivablePartReport(reportTO);
        return tripMap;
    }

    public ArrayList shippingBillReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripList = new ArrayList();
        tripList = reportDAO.shippingBillReport(reportTO);
        return tripList;
    }

    public ArrayList getRateDiffReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripList = new ArrayList();
        tripList = reportDAO.getRateDiffReport(reportTO);
        return tripList;
    }

    public ArrayList vehicleUtilReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripList = new ArrayList();
        tripList = reportDAO.vehicleUtilReport(reportTO);
        return tripList;
    }

    public ArrayList nodReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList nodList = new ArrayList();
        nodList = reportDAO.nodReport(reportTO);
        return nodList;
    }

    public ArrayList nodReports(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList nodLists = new ArrayList();
        nodLists = reportDAO.nodReports(reportTO);
        return nodLists;
    }

    public ArrayList nodReportLevel2(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList nodLevel2List = new ArrayList();
        nodLevel2List = reportDAO.nodReportLevel2(reportTO);
        return nodLevel2List;
    }

    public ArrayList nodLevel3List(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList nodLevel3List = new ArrayList();
        nodLevel3List = reportDAO.nodReportLevel3(reportTO);
        return nodLevel3List;
    }

    public ArrayList invoiceEmailReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList invList = new ArrayList();
        invList = reportDAO.invoiceEmailReport(reportTO);
        return invList;
    }

    public ArrayList customerWiseProfitability(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList invList = new ArrayList();
        invList = reportDAO.customerWiseProfitability(reportTO);
        return invList;
    }

    public ArrayList getGrPrintDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList invList = new ArrayList();
        invList = reportDAO.getGrPrintDetails(reportTO);
        return invList;
    }

//    public int updateGrPendingRemarks(String[] grId, String[] selectedId, String[] pendingGrRemarks, String[] gstType, String[] commodityId, String[] commodityName, String[] tripId, int userId) throws FPRuntimeException, FPBusinessException {
//        int updateStatus = 0;
//        updateStatus = reportDAO.updateGrPendingRemarks(grId, selectedId, pendingGrRemarks, gstType, commodityId, commodityName, tripId, userId);
//        return updateStatus;
//    }
//
//     public int updateGrPendingRemarks(String[] grId, String[] selectedId, String[] pendingGrRemarks, String gstType, String commodityId, String commodityName, String[] tripId, int userId) throws FPRuntimeException, FPBusinessException {
    public int updateGrPendingRemarks(ReportTO report, int userId) throws FPRuntimeException, FPBusinessException {
        int updateStatus = 0;
        updateStatus = reportDAO.updateGrPendingRemarks(report, userId);
        return updateStatus;
    }

    public ArrayList getInvoiceList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getInvoiceList = new ArrayList();
        getInvoiceList = reportDAO.getInvoiceList(reportTO);
        return getInvoiceList;
    }

    public ArrayList suppCreditNoteDetailsReport(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList invoiceDetailsReport = new ArrayList();
        invoiceDetailsReport = reportDAO.suppCreditNoteDetailsReport(reportTO, userId);
        return invoiceDetailsReport;
    }

    public int cancelGRno(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        int updateStatus = 0;
        SqlMapClient session = reportDAO.getSqlMapClient();
        try {
            session.startTransaction();
            updateStatus = reportDAO.cancelGRno(reportTO, userId, session);
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                session.getSession().close();
            }
        }
        return updateStatus;
    }

    public ArrayList getGRNoList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getGRNoList = new ArrayList();
        getGRNoList = reportDAO.getGRNoList(reportTO);
        return getGRNoList;
    }

    public ArrayList getSuppInvoiceReport(ReportTO reportTO, String status, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList invoiceReport = new ArrayList();
        invoiceReport = reportDAO.getSuppInvoiceReport(reportTO, status, userId);
        return invoiceReport;
    }

    public ArrayList getEmailInvoiceHeader() throws FPRuntimeException, FPBusinessException {
        ArrayList invoiceReport = new ArrayList();
        invoiceReport = reportDAO.getEmailInvoiceHeader();
        return invoiceReport;
    }

    public ArrayList getEmailInvoiceDetails(String invoiceId) throws FPRuntimeException, FPBusinessException {
        ArrayList invoiceReport = new ArrayList();
        invoiceReport = reportDAO.getEmailInvoiceDetails(invoiceId);
        return invoiceReport;
    }

    public int getInvPDFList() throws DocumentException, BadElementException, IOException, com.itextpdf.text.DocumentException, GeneralSecurityException, Exception {

        //System.out.println("inside getInvPDFList..");

        String billingParty = "";
        String customerAddress = "";
        String panNo = "";
        String gstNo = "";
        String invoicecode = "";
        String billDate = "";
        String movementType = "";
        String containerTypeName = "";
        String billingType = "";
        String userName = "";
        String billingState = "";
        ArrayList invIds = new ArrayList();
        int check = 0;

        String[] tempRemarks = null;
        String recTo1 = "";
        String recCc1 = "";
        String recBcc1 = "";

        try {

            ArrayList invheader = new ArrayList();
            invheader = getEmailInvoiceHeader();

            Iterator itr;
            itr = invheader.iterator();
            ReportTO rep = null;

            while (itr.hasNext()) {
                rep = new ReportTO();
                Multipart multipart = new MimeMultipart();
                SendEmail em = new SendEmail();
                rep = (ReportTO) itr.next();
                String invoiceid = rep.getInvoiceId();
                billingParty = rep.getBillingParty();

                customerAddress = rep.getAddress();
                panNo = rep.getPanNo();
                gstNo = rep.getGstNo();
                invoicecode = rep.getInvoiceCode();
                billDate = rep.getBillDate();
                movementType = rep.getMovementType();
                containerTypeName = rep.getContainerType();
                billingType = rep.getBillingType();
                userName = rep.getUserName();
                billingState = rep.getBillingState();
                recTo1 = rep.getEmailId();

                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                Document document = new Document();
                PdfWriter.getInstance(document, outputStream);
                //System.out.println("HI.pdf is called");

                document.open();

                PdfPTable table = new PdfPTable(2);
                PdfPTable innertable = new PdfPTable(1);
                PdfPTable innertable1 = new PdfPTable(1);

                PdfPTable innertable2 = new PdfPTable(12);
                innertable2.setWidths(new float[]{8, 8, 12, 8, 8, 8, 8, 8, 8, 8, 8, 8});
                PdfPTable innertable3 = new PdfPTable(1);
                PdfPTable innertable31 = new PdfPTable(1);
                PdfPTable innertable32 = new PdfPTable(1);
                PdfPTable innertableSign = new PdfPTable(1);
                PdfPTable innertable4 = new PdfPTable(1);
                PdfPTable innertable5 = new PdfPTable(1);
                PdfPTable innerHeadertable = new PdfPTable(3); ///ok

                innertableSign.setTotalWidth(new float[]{500});
                innertableSign.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertableSign.setLockedWidth(true);

                table.setTotalWidth(new float[]{150, 350});
                table.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.setLockedWidth(true);

                innerHeadertable.setTotalWidth(new float[]{300, 100, 100});
                innerHeadertable.setHorizontalAlignment(Element.ALIGN_LEFT);
                innerHeadertable.setLockedWidth(true);

                innertable.setTotalWidth(new float[]{500});
                innertable.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable.setLockedWidth(true);

                innertable1.setTotalWidth(new float[]{500});
                innertable1.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable1.setLockedWidth(true);

                innertable2.setTotalWidth(new float[]{40, 40, 60, 40, 40, 40, 40, 40, 40, 40, 40, 40});
                innertable2.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable2.setLockedWidth(true);

                innertable3.setTotalWidth(new float[]{500});
                innertable3.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable3.setLockedWidth(true);

                innertable31.setTotalWidth(new float[]{500});
                innertable31.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable31.setLockedWidth(true);

                innertable32.setTotalWidth(new float[]{500});
                innertable32.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable32.setLockedWidth(true);

                innertable4.setTotalWidth(new float[]{500});
                innertable4.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable4.setLockedWidth(true);
                innertable5.setTotalWidth(new float[]{500});
                innertable5.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable5.setLockedWidth(true);

                Image img = Image.getInstance("/var/lib/tomcat7/webapps/throttle/images/dict-logo11.png");
//                Image img = Image.getInstance("C:/GIT-Projects/dict/web/images/dict-logo11.png");
//                Image img = Image.getInstance("D:/Dict/v2/web/images/dict-logo11.png");
                img.scalePercent(30);
                PdfPCell headCell1 = new PdfPCell(new Phrase("Header", FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD, GrayColor.BLACK)));
                headCell1.setFixedHeight(40);

                headCell1.addElement(new Chunk(img, 10, -10));
                headCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell1.disableBorderSide(Rectangle.RIGHT);
                table.addCell(headCell1);

                headCell1 = new PdfPCell(new Phrase(" Delhi International Cargo Terminal Pvt.Ltd. \n Panchi Gujran,Tehsil-Ganaur, Dist-Sonepat-131101 \n CIN No:U63040MH2006PTC159885", FontFactory.getFont(FontFactory.HELVETICA, 8, Font.BOLD, GrayColor.BLACK)));
                headCell1.setFixedHeight(40);
                headCell1.disableBorderSide(4);

                headCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(headCell1);

                PdfPCell headCell = new PdfPCell(new Phrase("Some text here"));

                headCell = new PdfPCell(new Phrase("INVOICE", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell.setFixedHeight(20);
                headCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                innertable.addCell(headCell);

                //System.out.println("gstNo in pdf  gen:" + gstNo);
                PdfPCell headCell3 = new PdfPCell(new Phrase("Billed to :\n" + billingParty + "\n" + customerAddress + "\nBilling State:" + billingState + "\nPAN No:" + panNo + "\nGST No:" + gstNo, FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
                headCell3.setFixedHeight(60);

//         headCell3.disableBorderSide(4);
                headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                innerHeadertable.addCell(headCell3);
                //System.out.println("invoice code inside pdf generation ::" + invoicecode);
                headCell3 = new PdfPCell(new Phrase("Invoice No:" + invoicecode + "\n\n " + billingType + "\n\n SAC Code: 996511 \n\n", FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
                headCell3.setFixedHeight(40);

                headCell3.disableBorderSide(4);
                headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                innerHeadertable.addCell(headCell3);

                headCell3 = new PdfPCell(new Phrase("Bill Date: " + billDate + "\n \n Dispatched Through::", FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
                headCell3.setFixedHeight(40);

                headCell3.disableBorderSide(4);
                headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                innerHeadertable.addCell(headCell3);

                PdfPCell headCell4 = new PdfPCell(new Phrase("Some text here"));

                headCell4 = new PdfPCell(new Phrase("PARTICULARS", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell4.setFixedHeight(20);
                headCell4.setHorizontalAlignment(Element.ALIGN_CENTER);
                innertable1.addCell(headCell4);

                PdfPCell headCell5 = new PdfPCell(new Phrase("Some text here"));

                headCell5 = new PdfPCell(new Phrase("GR.No", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);

                headCell5 = new PdfPCell(new Phrase("Date", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);

                headCell5 = new PdfPCell(new Phrase("Description", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);

                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);
                headCell5 = new PdfPCell(new Phrase("No.Of Container ", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);
                headCell5 = new PdfPCell(new Phrase("Commodity Category  ", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);
                headCell5 = new PdfPCell(new Phrase("Commodity", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);
                headCell5 = new PdfPCell(new Phrase("Freight", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);
                headCell5 = new PdfPCell(new Phrase("Toll Tax", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);
                headCell5 = new PdfPCell(new Phrase("Detention Charges", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);
                headCell5 = new PdfPCell(new Phrase("Weightment", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);

                headCell5 = new PdfPCell(new Phrase("Other Charges", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);

                headCell5 = new PdfPCell(new Phrase("Total Amount(Rs.)", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
//                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);

                String totalFreightAmount = "";
                Float totalAmount = 0.0f;
                int totalContainerQty = 0;

                ArrayList invoiceDetailsList = new ArrayList();
                invoiceDetailsList = getEmailInvoiceDetails(invoiceid);

                Iterator itr1;
                itr1 = invoiceDetailsList.iterator();
                ReportTO repDet = null;

                while (itr1.hasNext()) {
                    repDet = new ReportTO();
                    repDet = (ReportTO) itr1.next();

                    PdfPCell headCell6 = new PdfPCell(new Phrase("Some text here"));

                    headCell6 = new PdfPCell(new Phrase(repDet.getGrNo(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getGrDate(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getDestination(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(30);
                    headCell6.setNoWrap(false);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getContainerQty(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    totalContainerQty = Integer.parseInt(repDet.getContainerQty()) + totalContainerQty;
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getArticleCode(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getArticleName(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getFreightAmount(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getTollTax(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getDetaintionAmount(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell3.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getWeightMent(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getOtherExpense(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    totalFreightAmount = Float.parseFloat(repDet.getFreightAmount()) + Float.parseFloat(repDet.getTollTax()) + Float.parseFloat(repDet.getDetaintionAmount())
                            + Float.parseFloat(repDet.getWeightMent()) + Float.parseFloat(repDet.getOtherExpense()) + "";
                    totalAmount = Float.parseFloat(totalFreightAmount) + totalAmount;

                    headCell6 = new PdfPCell(new Phrase(totalFreightAmount, FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);
                }

                //System.out.println(" i am in outeeeerrrrrrr parrrrrtttt");

                PdfPCell headCell7 = new PdfPCell(new Phrase("Some text here"));

                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);

                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);

                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);
                headCell7 = new PdfPCell(new Phrase(totalContainerQty + "", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);

                innertable2.addCell(headCell7);
                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);
                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);

                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);

                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);
                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell3.disableBorderSide(2);
                innertable2.addCell(headCell7);
                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);
                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);

                headCell7 = new PdfPCell(new Phrase(totalAmount + "", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);

                String RUPEES = "";
                NumberWords numberWords = new NumberWords();
                numberWords.setRoundedValue(String.valueOf(java.lang.Math.ceil(totalAmount)));
                numberWords.setNumberInWords(String.valueOf(java.lang.Math.ceil(totalAmount)));
                RUPEES = numberWords.getNumberInWords();

                PdfPCell headCell8 = new PdfPCell(new Phrase("Some text here"));

                headCell8 = new PdfPCell(new Phrase("Amount Chargeable(In words)\n" + "Rs." + RUPEES + "\n\n" + "Remarks\n" + "" + "Being Road Transportation Charges Of " + containerTypeName + " " + movementType + "\n\n"
                        + "" + " Declaration\n"
                        + "1.All diputes are subject to Delhi Jurisdition\n"
                        + "2. Input credit on inputs, capital goods and input services, used for providing the taxable service, has not been taken.\n"
                        + "3. PAN No.AACCB8054G.\n"
                        + "4. GST Ref No.06AACCB8054G1ZT.\n"
                        + "5. Tax is payable under RCM.\n"
                        + "6. We declare that GST input credit on inputs, capital goods and input services used for providing the subject taxable service has not been taken by us under the provisions of the Input tax credit rules of GST. (Notification No.11/2017-Central Tax-rate dt. 28th June 2017 and Notification No.11/2017-State Tax -rate dt. 29th June 2017 ).\n"
                        + "7. In case of any discrepancy in this invoice, kindly intimate within 7 days from receipt or else it will be deemed to be accepted.\n" + "\n"
                        + " IN FAVOUR :- Delhi International Cargo Terminal Pvt. Ltd.\n"
                        + " BANKER    :-  YES BANK \n"
                        + " BRANCH    :-  Ground Floor, Sheela Shoppee Sanjay Chowk , Panipat - 132103\n"
                        + " A/C NO    :-  007081400000021\n"
                        + " IFSC CODE :-  YESB0000070"
                        + "                                                                                                                                     For Delhi International Cargo Terminal Pvt.Ltd.\n\n", FontFactory.getFont(FontFactory.HELVETICA, 6)));

                headCell8.setFixedHeight(165);
                //System.out.println("i m here 2");
                headCell8.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell8.disableBorderSide(Rectangle.BOTTOM);
                innertable3.addCell(headCell8);

                Image signimg = Image.getInstance("/var/lib/tomcat7/webapps/throttle/images/authsign.jpg");
//                Image signimg = Image.getInstance("D:/Dict/v2/web/images/authsign.jpg");
                signimg.scalePercent(30);

                PdfPCell headCellimg = new PdfPCell(new Phrase(""));
                headCellimg.setFixedHeight(30);

                headCellimg.addElement(new Chunk(signimg, 400, -8));
                headCellimg.setHorizontalAlignment(Element.ALIGN_RIGHT);
                headCellimg.disableBorderSide(Rectangle.RIGHT);
                headCellimg.setBorder(Rectangle.TOP);
                headCellimg.setBorder(Rectangle.NO_BORDER);
                innertable31.addCell(headCellimg);

                PdfPCell headCell10 = new PdfPCell(new Phrase("Some text here"));
                headCell10 = new PdfPCell(new Phrase("                                                                                                                                                                                                                                               " + "Authorised Signature\n", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell10.setFixedHeight(20);
                headCell10.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell10.disableBorderSide(Rectangle.BOTTOM);
                headCell10.setBorder(Rectangle.TOP);
                headCell10.setBorder(Rectangle.NO_BORDER);
                innertable32.addCell(headCell10);

                PdfPCell headCell9 = new PdfPCell(new Phrase("Some text here"));
                headCell9 = new PdfPCell(new Phrase("Regd.Office: Godrej Coliseum,Office No. 801,8th Floor,C-Wing,Behind Everard Nagar,off Somaiya Hospital Road,Sion (East)Mumbai-400022 ", FontFactory.getFont(FontFactory.HELVETICA, 5)));
                headCell9.setFixedHeight(20);
                headCell9.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell9.setBorder(Rectangle.NO_BORDER);
                headCell9.setBorder(Rectangle.ALIGN_BOTTOM);
                innertable4.addCell(headCell9);

                document.add(table);
                document.add(innertable);
                document.add(innerHeadertable);
                document.add(innertable1);
                document.add(innertable2);
                document.add(innertable3);
                document.add(innertable31);
                document.add(innertable32);
                document.add(innertable4);
                document.close();

                multipart = em.addMultipleAttachments(movementType + "-" + invoicecode + ".pdf", outputStream, multipart);

                recTo1 = "arunkumar.kanagaraj@newage-global.com";
                recCc1 = "arunkumar.kanagaraj@newage-global.com";
//                recTo1 = "pankajs@ict.in,sonams@ict.in,sudhakarp@ict.in,puneetsingh@ict.in,yogeshn@ict.in,ragvendrat@ict.in";
//                recCc1 = "priyeshr@ict.in";
                recBcc1 = "";

                String contentData = "";
                StringBuffer contentSB = new StringBuffer();
                contentSB.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n"
                        + " <html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\">\n"
                        + " <head>\n<meta charset=\"utf-8\">\n<meta name=\"viewport\" content=\"height=device-height, initial-scale=0.5\">\n<title>DICT Alerts</title>\n"
                        + " <style >.wrapper {\n     "
                        + "                        width: 100%; }\n"
                        + "                    #outlook a {\n"
                        + "                        padding: 0; }\n"
                        + "                    body\n"
                        + "                    {\n"
                        + "                        width: 700px !important;\n"
                        + "                        max-width:700px; max-height: 800px; background:#FFFFFF;\n"
                        + "                        margin:0 auto !important;\n"
                        + "                        padding:5px;\n"
                        + "                        font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;\n"
                        + "                        font-size: 14px;\n"
                        + "                        font-style: normal;\n"
                        + "                        font-variant: normal;\n"
                        + "                        font-weight: 400;\n"
                        + "                        line-height: 20px\n"
                        + "                    }\n"
                        + "                    img {\n"
                        + "                        outline: none;\n"
                        + "                        text-decoration: none;\n"
                        + "                        -ms-interpolation-mode: bicubic;\n"
                        + "                        width: auto;\n"
                        + "                        max-width: 100%;\n"
                        + "                        clear: both;\n"
                        + "                        display: block; }\n"
                        + "                    table.main {\n"
                        + "                        border:0px solid #053587;\n"
                        + "                        padding:0px 10px;\n"
                        + "                        width: 700px !important;\n"
                        + "                        max-width:700px;\n"
                        + "                    }\n"
                        + "                    td.fontsize12\n"
                        + "                    {\n"
                        + "                        font-size:12px;\n"
                        + "                    }\n"
                        + "                    td a.trackbutton\n"
                        + "                    {\n"
                        + "                        text-decoration: none; width:80px !important; float: right;text-align:center;background:#053587;\n"
                        + "                        line-height: 30px; font-weight: bold; height: 25px; color: #FFFFFF; margin: 0px 5px\n"
                        + "                    }\n"
                        + "                    td.tollfree\n"
                        + "                    {font-size:14px;color:#053587; padding:5px 2px;}\n"
                        + "                    table.tableinline\n"
                        + "                    {\n"
                        + "                        font-size:16px;\n"
                        + "                        font-weight:bold;\n"
                        + "                        color:#FFFFFF;\n"
                        + "                        height:30px\n"
                        + "                    }\n"
                        + "                    table.text\n"
                        + "                    {\n"
                        + "                        font-size:16px; font-weight:bold;height:30px\n"
                        + "                    }\n"
                        + "                    table.tablesub\n"
                        + "                    {\n"
                        + "                        font-size:16px; font-weight:bold;color:#000000;height:30px;margin:10px 0px;\n"
                        + "                    }\n"
                        + "                    td.tabletdheader\n"
                        + "                    {\n"
                        + "                        padding:0px 15px; width:50%; margin: 0px;border:1px solid #EEEEEE;\n"
                        + "                    }\n"
                        + "                    td.listhead table td ul.coming_soon\n"
                        + "                    {\n"
                        + "                        line-height:30px;color: #FFFFFF; font-size:13px;\n"
                        + "                    }\n"
                        + "                    td.listhead table td ul\n"
                        + "                    {\n"
                        + "                        line-height:25px;color: #053587; font-size:13px;\n"
                        + "                    }\n"
                        + "                    table.fontsize10\n"
                        + "                    {\n"
                        + "                        font-size:10px; font-weight:bold;color:#000000;height:15px\n"
                        + "                    }\n"
                        + "                    table.amount_table\n"
                        + "                    {\n"
                        + "                        background:#DFDFDF; width:100%; font-size: 12px; line-height:30px;text-align:center; border: 1px dashed #b5b4c6 ;font-weight:bold;\n"
                        + "                    }\n"
                        + "                    table.tablebottom\n"
                        + "                    {\n"
                        + "                        width:90%; color:#053587; font-size: 12px; line-height:20px; margin: 5px 0px; font-weight:bold;\n"
                        + "                    }\n"
                        + "                    div.tablebottom_head\n"
                        + "                    {\n"
                        + "                        font-size:16px; display: block;line-height: 25px; font-weight:bold; color:#053587;\n"
                        + "                    }\n"
                        + "                    div.tablebottom_address\n"
                        + "                    {\n"
                        + "                        font-size:12px;line-height:18px\n"
                        + "                    }\n"
                        + "                    td.listhead\n"
                        + "                    {\n"
                        + "                        border:1px solid #EEEEEE;padding:0px 5px; width:50%; padding:5px;\n"
                        + "                    }\n"
                        + "                    td.service_head\n"
                        + "                    {\n"
                        + "                        border:1px solid #EEEEEE;padding:0px 5px; width:50%\n"
                        + "                    }\n"
                        + "                    .service_head_upper\n"
                        + "                    {\n"
                        + "                        font-size:14px; text-align: center; margin: 0px;\n"
                        + "                    }\n"
                        + "                    td.service_head ul\n"
                        + "                    {\n"
                        + "                        line-height:30px;color: #053587; font-size:13px;\n"
                        + "                    }\n"
                        + "                    h3\n"
                        + "                    {\n"
                        + "                        font-size: 15px;\n"
                        + "                        color: 053587;\n"
                        + "                        line-height: 20px;      margin: 0px;\n"
                        + "                    }\n"
                        + "                    p\n"
                        + "                    {\n"
                        + "                        font-size: 13px;\n"
                        + "                        color: #000000; padding-left: 45px;\n"
                        + "                        margin: 0px;\n"
                        + "                    }\n"
                        + "                    span\n"
                        + "                    {\n"
                        + "                        font-size: 14px;padding-left: 0px;\n"
                        + "                        color: #053587;\n"
                        + "\n"
                        + "                    }\n"
                        + "                    .left li\n"
                        + "                    {\n"
                        + "                         float: left;\n"
                        + "                         display: block;\n"
                        + "                         padding: 5px;margin:0px 0px 0px 5px\n"
                        + "                    }\n"
                        + "\n"
                        + "                </style>\n"
                        + "                </head>\n"
                        + "                <body>\n"
                        + "<table cellpadding=\"0\" cellspacing=\"0\"  width=\"90%\">\n"
                        + "<tr>\n"
                        + "<td rowspan=\"2\" >\n"
                        + "<img src=\"http://dict.throttletms.com/throttle/images/dict-logo11.png\"  width=\"100\" height=\"75\" >"
                        + "</td>\n"
                        + "<td align=\"right\" style=\"font-size:14px;color:#053587; padding:5px 2px\" colspan=\"2\">\n"
                        + "</td>\n"
                        + "</tr>\n"
                        + "</table>\n"
                        + "<h3>Dear Sir / Mam, Greetings!!!</h3>\n"
                        + "<br>\n"
                        + "<span>Many thanks for your business support. Please find attached our Invoice for Transport of your containers. \n"
                        + "Kindly go through the invoice and if you find any discrepancies please revert within 48 hours to the below mentioned IDs\n "
                        + "in mail signatures. <br>We value your association.</span>\n"
                        + "<br><br>\n"
                        + "<span>Thanking You,</span>\n"
                        + "<br>\n"
                        + "<table cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"white\" class=\"tablebottom\" width=\"90%\">\n"
                        + " <tr>\n"
                        + " <td valign=\"center\" width=\"50%\" style=\"padding:5px;\">\n"
                        + " <div class=\"tablebottom_head\">Best Regards,<br>DICT.</div>\n"
                        + " <div class=\"tablebottom_address\">\n"
                        + " Panchi Gujran,Tehsil-Ganaur,\n"
                        + " Dist-Sonepat-131101\n"
                        + "<br/>\n"
                        + "CIN No:U63040MH2006PTC159885\n"
                        + "</div>\n"
                        + "</td>\n"
                        + "                                        <td valign=\"bottom\" align=\"right\">\n"
                        + "                                            powered by <a href=\"http://www.entitlesolutions.com/\" target=\"_blank\">\n"
                        + "                                                <img src=\"http://calog.throttletms.com:8899/throttle/images/Throttle_Logo.png\"  width=\"60\" height=\"15\" >\n"
                        + "                                            </a>\n"
                        + "                                        </td>\n"
                        + "                                    </tr>\n"
                        + "\n"
                        + "</table>\n"
                        + "</body>\n"
                        + "</html>");
                contentData = contentSB.toString();
                //System.out.println(contentData);

                //check = em.sendMultipleAttachPDFMail(recTo1, recCc1, recBcc1, billingParty + "-TMS Invoice ", contentData, multipart, invIds);
                if (check > 0) {
                    int invUpdate = updateInvoiceCustomerMail(rep);
                    //System.out.println("invUpdate : " + invUpdate);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            //System.out.println("error in pdf inv:" + e);

        }
        return check;
    }

    /////////////////////// Email Part Start ///////////////
    public ArrayList getInvoiceListEmail() throws FPRuntimeException, FPBusinessException {
        ArrayList invoiceReport = new ArrayList();
        invoiceReport = reportDAO.getInvoiceListEmail();
        return invoiceReport;
    }

    public String createInvoiceFile(String invId, String invoiceCode) throws DocumentException, BadElementException, IOException, com.itextpdf.text.DocumentException, GeneralSecurityException, Exception {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        String systemTime = sdf.format(c.getTime()).toString();
        //System.out.println("system date = " + systemTime);

        //System.out.println("inside getInvPDFList..");

        String billingParty = "";
        String customerAddress = "";
        String panNo = "";
        String gstNo = "";
        String invoicecode = "";
        String billDate = "";
        String movementType = "";
        String containerTypeName = "";
        String billingType = "";
        String userName = "";
        String billingState = "";
        String[] tempRemarks = null;
        String recTo1 = "";
        String emailFormat = "";
        String qrcode = "";
        String irn = "";
        String workNo = "";

        try {

            ArrayList invoiceList = new ArrayList();
            invoiceList = getInvoiceDetailsEmail(invId);

            Iterator itr;
            itr = invoiceList.iterator();
            ReportTO rep = null;

            Multipart multipart = new MimeMultipart();
            SendEmail em = new SendEmail();
            while (itr.hasNext()) {
                rep = new ReportTO();
                rep = (ReportTO) itr.next();
                String invoiceid = rep.getInvoiceId();
                billingParty = rep.getBillingParty();

                customerAddress = rep.getAddress();
                panNo = rep.getPanNo();
                gstNo = rep.getGstNo();
                invoicecode = rep.getInvoiceCode();
                billDate = rep.getBillDate();
                movementType = rep.getMovementType();
                containerTypeName = rep.getContainerType();
                billingType = rep.getBillingType();
                userName = rep.getUserName();
                billingState = rep.getBillingState();
                recTo1 = rep.getEmailId();
                qrcode = rep.getQrcode();
                irn = rep.getIrn();
                workNo = rep.getWorkNo();

                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                Document document = new Document();
                PdfWriter.getInstance(document, outputStream);
                //System.out.println("HI.pdf is called");

                document.open();

                PdfPTable table = new PdfPTable(2);
                PdfPTable innertable = new PdfPTable(1);
                PdfPTable innertable1 = new PdfPTable(1);

                PdfPTable innertable2 = new PdfPTable(12);
                innertable2.setWidths(new float[]{8, 8, 12, 8, 8, 8, 8, 8, 8, 8, 8, 8});
                PdfPTable innertable3 = new PdfPTable(1);
                PdfPTable innertable31 = new PdfPTable(1);
                PdfPTable innertable32 = new PdfPTable(1);
                PdfPTable innertableSign = new PdfPTable(1);
                PdfPTable innertable4 = new PdfPTable(1);
                PdfPTable innertable5 = new PdfPTable(1);
                PdfPTable innerHeadertable = new PdfPTable(3); ///ok

                innertableSign.setTotalWidth(new float[]{500});
                innertableSign.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertableSign.setLockedWidth(true);

                table.setTotalWidth(new float[]{150, 350});
                table.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.setLockedWidth(true);

                innerHeadertable.setTotalWidth(new float[]{300, 100, 100});
                innerHeadertable.setHorizontalAlignment(Element.ALIGN_LEFT);
                innerHeadertable.setLockedWidth(true);

                innertable.setTotalWidth(new float[]{500});
                innertable.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable.setLockedWidth(true);

                innertable1.setTotalWidth(new float[]{500});
                innertable1.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable1.setLockedWidth(true);

                innertable2.setTotalWidth(new float[]{40, 40, 60, 40, 40, 40, 40, 40, 40, 40, 40, 40});
                innertable2.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable2.setLockedWidth(true);

                innertable3.setTotalWidth(new float[]{500});
                innertable3.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable3.setLockedWidth(true);

                innertable31.setTotalWidth(new float[]{500});
                innertable31.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable31.setLockedWidth(true);

                innertable32.setTotalWidth(new float[]{500});
                innertable32.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable32.setLockedWidth(true);

                innertable4.setTotalWidth(new float[]{500});
                innertable4.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable4.setLockedWidth(true);
                innertable5.setTotalWidth(new float[]{500});
                innertable5.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable5.setLockedWidth(true);

//                Image img = Image.getInstance("/home/benjamin/Desktop/Project-amp/dict/web/images/dict-logo11.png");
//                Image img = Image.getInstance("C:/GIT-Projects/DICT-GIT/dict/web/images/dict-logo11.png");
                Image img = Image.getInstance("/var/lib/tomcat7/webapps/throttle/images/dict-logo11.png");

                img.scalePercent(30);
                PdfPCell headCell1 = new PdfPCell(new Phrase("Header", FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD, GrayColor.BLACK)));
                headCell1.setFixedHeight(40);

                headCell1.addElement(new Chunk(img, 10, -10));
                headCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell1.disableBorderSide(Rectangle.RIGHT);
                table.addCell(headCell1);

                headCell1 = new PdfPCell(new Phrase("Delhi International Cargo Terminal Pvt.Ltd. \n Panchi Gujran,Tehsil-Ganaur, Dist-Sonepat-131101 \n CIN No:U63040MH2006PTC159885", FontFactory.getFont(FontFactory.HELVETICA, 8, Font.BOLD, GrayColor.BLACK)));
                headCell1.setFixedHeight(40);
                headCell1.disableBorderSide(4);

                headCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(headCell1);

                PdfPCell headCell = new PdfPCell(new Phrase("Some text here"));

                headCell = new PdfPCell(new Phrase("INVOICE", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell.setFixedHeight(20);
                headCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                innertable.addCell(headCell);

                //System.out.println("gstNo in pdf  gen:" + gstNo);
                PdfPCell headCell3 = new PdfPCell(new Phrase("Billed to :\n" + billingParty + "\n" + customerAddress + "\nBilling State:" + billingState + "\nPAN No:" + panNo + "\nGST No:" + gstNo, FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
                headCell3.setFixedHeight(60);

                //         headCell3.disableBorderSide(4);
                headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                innerHeadertable.addCell(headCell3);
                //System.out.println("invoice code inside pdf generation ::" + invoicecode);
                if (!"".equals(irn) && irn != null) {
                    headCell3 = new PdfPCell(new Phrase("Invoice No:" + invoicecode + "\n\n" + "Work Order Number:"+ workNo + "\n\n " + "IRN :" + irn + "\n\n" + billingType, FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
                } else {
                    headCell3 = new PdfPCell(new Phrase("Invoice No:" + invoicecode + "\n\n" + billingType, FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
                }
                headCell3.setFixedHeight(60);

                headCell3.disableBorderSide(4);
                headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                innerHeadertable.addCell(headCell3);

                headCell3 = new PdfPCell(new Phrase("Bill Date: " + billDate + "\n \n Dispatched Through:: \n \n \n SAC Code: 996511 \n\n", FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
                headCell3.setFixedHeight(40);

                headCell3.disableBorderSide(4);
                headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                innerHeadertable.addCell(headCell3);

                PdfPCell headCell4 = new PdfPCell(new Phrase("Some text here"));

                headCell4 = new PdfPCell(new Phrase("PARTICULARS", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell4.setFixedHeight(20);
                headCell4.setHorizontalAlignment(Element.ALIGN_CENTER);
                innertable1.addCell(headCell4);

                PdfPCell headCell5 = new PdfPCell(new Phrase("Some text here"));

                headCell5 = new PdfPCell(new Phrase("GR.No", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);

                headCell5 = new PdfPCell(new Phrase("Date", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);

                headCell5 = new PdfPCell(new Phrase("Description", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);

                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);
                headCell5 = new PdfPCell(new Phrase("No.Of Container ", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);
                headCell5 = new PdfPCell(new Phrase("Commodity Category  ", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);
                headCell5 = new PdfPCell(new Phrase("Commodity", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);
                headCell5 = new PdfPCell(new Phrase("Freight", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);
                headCell5 = new PdfPCell(new Phrase("Toll Tax", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);
                headCell5 = new PdfPCell(new Phrase("Detention Charges", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);
                headCell5 = new PdfPCell(new Phrase("Weightment", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);

                headCell5 = new PdfPCell(new Phrase("Other Charges", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);

                headCell5 = new PdfPCell(new Phrase("Total Amount(Rs.)", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);

                String totalFreightAmount = "";
                Float totalAmount = 0.0f;
                int totalContainerQty = 0;

                ArrayList invoiceDetailsList = new ArrayList();
                invoiceDetailsList = getEmailInvoiceDetails(invoiceid);

                Iterator itr1;
                itr1 = invoiceDetailsList.iterator();
                ReportTO repDet = null;

                while (itr1.hasNext()) {
                    repDet = new ReportTO();
                    repDet = (ReportTO) itr1.next();

                    PdfPCell headCell6 = new PdfPCell(new Phrase("Some text here"));

                    headCell6 = new PdfPCell(new Phrase(repDet.getGrNo(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getGrDate(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getDestination(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(30);
                    headCell6.setNoWrap(false);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getContainerQty(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    totalContainerQty = Integer.parseInt(repDet.getContainerQty()) + totalContainerQty;
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getArticleCode(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getArticleName(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getFreightAmount(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getTollTax(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getDetaintionAmount(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell3.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getWeightMent(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getOtherExpense(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    totalFreightAmount = Float.parseFloat(repDet.getFreightAmount()) + Float.parseFloat(repDet.getTollTax()) + Float.parseFloat(repDet.getDetaintionAmount())
                            + Float.parseFloat(repDet.getWeightMent()) + Float.parseFloat(repDet.getOtherExpense()) + "";
                    totalAmount = Float.parseFloat(totalFreightAmount) + totalAmount;

                    headCell6 = new PdfPCell(new Phrase(totalFreightAmount, FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);
                }

                //System.out.println(" i am in outeeeerrrrrrr parrrrrtttt");

                PdfPCell headCell7 = new PdfPCell(new Phrase("Some text here"));

                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);

                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);

                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);
                headCell7 = new PdfPCell(new Phrase(totalContainerQty + "", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);

                innertable2.addCell(headCell7);
                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);
                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);

                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);

                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);
                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell3.disableBorderSide(2);
                innertable2.addCell(headCell7);
                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);
                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);

                headCell7 = new PdfPCell(new Phrase(totalAmount + "", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);

                String RUPEES = "";
                NumberWords numberWords = new NumberWords();
                numberWords.setRoundedValue(String.valueOf(java.lang.Math.ceil(totalAmount)));
                numberWords.setNumberInWords(String.valueOf(java.lang.Math.ceil(totalAmount)));
                RUPEES = numberWords.getNumberInWords();

                PdfPCell headCell8 = new PdfPCell(new Phrase("Some text here"));

                headCell8 = new PdfPCell(new Phrase("Amount Chargeable(In words)\n" + "Rs." + RUPEES + "\n\n" + "Remarks\n" + "" + "Being Road Transportation Charges Of " + containerTypeName + " " + movementType + "\n\n"
                        + "" + " Declaration\n"
                        + "1.All diputes are subject to Delhi Jurisdition\n"
                        + "2. Input credit on inputs, capital goods and input services, used for providing the taxable service , has not been taken.\n"
                        + "3. PAN No.AACCB8054G.\n"
                        + "4. GST Ref No.06AACCB8054G1ZT.\n"
                        + "5. Tax is payable under RCM.\n"
                        + "6. We declare that GST input credit on inputs, capital goods and input services used for providing the subject taxable service has not been taken by us under the provisions of the Input tax credit rules of GST. (Notification No.11/2017-Central Tax-rate dt. 28th June 2017 and Notification No.11/2017-State Tax -rate dt. 29th June 2017 ).\n"
                        + "7. In case of any discrepancy in this invoice, kindly intimate within 7 days from receipt or else it will be deemed to be accepted.\n" + "\n"
                        + " IN FAVOUR :-  Delhi International Cargo Terminal Pvt. Ltd.\n"
                        + " BANKER    :-  YES BANK\n"
                        + " BRANCH    :-  Ground Floor, Sheela Shoppee Sanjay Chowk , Panipat - 132103\n"
                        + " A/C NO    :-  007081400000021\n"
                        + " IFSC CODE :-  YESB0000070"
                        + "                                                                                                                                     For Delhi International Cargo Terminal Pvt.Ltd.\n\n", FontFactory.getFont(FontFactory.HELVETICA, 6)));

                headCell8.setFixedHeight(165);
                //System.out.println("i m here 2");
                headCell8.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell8.disableBorderSide(Rectangle.BOTTOM);
                innertable3.addCell(headCell8);

                GenerateQrCode qr = new GenerateQrCode();
                qr.setPathQRCode(invoicecode, qrcode);
//Image img = Image.getInstance("/home/benjamin/Desktop/Project-amp/dict/web/images/dict-logo11.png");

//                Image signimg = Image.getInstance("/var/lib/tomcat7/webapps/throttle/images/authsign.jpg");
                Image signimg = Image.getInstance("/var/lib/tomcat7/webapps/throttle/images/authsign.jpg");
//                Image signimg = Image.getInstance("C:/GIT-Projects/DICT-GIT/dict/web/images/authsign.jpg");
                signimg.scalePercent(30);

                PdfPCell headCellimg = new PdfPCell(new Phrase(""));
                headCellimg.setFixedHeight(30);

                headCellimg.addElement(new Chunk(signimg, 400, -8));

                headCellimg.setHorizontalAlignment(Element.ALIGN_RIGHT);
                headCellimg.disableBorderSide(Rectangle.RIGHT);
                headCellimg.setBorder(Rectangle.TOP);
                headCellimg.setBorder(Rectangle.NO_BORDER);
                innertable31.addCell(headCellimg);

                if (!"".equals(irn) && irn != null) {
                    Image signimg2 = Image.getInstance("/var/lib/tomcat7/webapps/Qrimages/QR" + invoiceCode + ".png");
//                    Image signimg2 = Image.getInstance("C:/GIT-Projects/DICT-GIT/dict/Qrimages/QR"+invoicecode+".png");
//                    Image signimg2 = Image.getInstance("D:/projects/DICT/dict/Qrimages/QR" + invoiceCode + ".png");
                    signimg2.scalePercent(20);
//                    signimg2.scaleToFit(300, 100);/home/benjamin/Desktop/Project-amp/dict/web/images/dict-logo11.png

                    PdfPCell headCellimg2 = new PdfPCell(new Phrase(""));
                    headCellimg2.setFixedHeight(0);

                    headCellimg2.addElement(new Chunk(signimg2, 10, 1));

                    headCellimg2.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCellimg2.disableBorderSide(Rectangle.LEFT);
                    headCellimg2.setBorder(Rectangle.BOTTOM);
                    headCellimg2.setBorder(Rectangle.NO_BORDER);
                    innertable31.addCell(headCellimg2);
                }

                PdfPCell headCell9 = new PdfPCell(new Phrase("Some text here"));
                headCell9 = new PdfPCell(new Phrase("Regd.Office: Godrej Coliseum,Office No. 801,8th Floor,C-Wing,Behind Everard Nagar,off Somaiya Hospital Road,Sion (East)Mumbai-400022 ", FontFactory.getFont(FontFactory.HELVETICA, 5)));
                headCell9.setFixedHeight(20);
                headCell9.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell9.setBorder(Rectangle.NO_BORDER);
                headCell9.setBorder(Rectangle.ALIGN_BOTTOM);
                innertable4.addCell(headCell9);

                document.add(table);
                document.add(innertable);
                document.add(innerHeadertable);
                document.add(innertable1);
                document.add(innertable2);
                document.add(innertable3);
                document.add(innertable31);
                document.add(innertable32);
                document.add(innertable4);
                document.close();

                multipart = em.addMultipleAttachments(movementType + "-" + invoicecode + ".pdf", outputStream, multipart);

            }

            //System.out.println("pdf created successfully..." + invoicecode);
            emailFormat = movementType + "-" + invoicecode + ".pdf";
            //System.out.println("emailFormat---" + emailFormat);
        } catch (Exception e) {
            e.printStackTrace();
            //System.out.println("error in pdf inv:" + e);

        }

        return emailFormat;
    }

    public ArrayList getInvoiceDetailsEmail(String invId) throws FPRuntimeException, FPBusinessException {
        ArrayList invoiceReport = new ArrayList();
        invoiceReport = reportDAO.getInvoiceDetailsEmail(invId);
        return invoiceReport;
    }

    public int insertInvoiceFileDetails(ReportTO reportTO, SqlMapClient session) {
        int insertInvoiceFileDetails = 0;
        insertInvoiceFileDetails = reportDAO.insertInvoiceFileDetails(reportTO, session);
        return insertInvoiceFileDetails;
    }

    public String getMailTemplate(String eventId, SqlMapClient session) throws FPRuntimeException, FPBusinessException {
        String mailTemplate = "";
        mailTemplate = reportDAO.getMailTemplate(eventId, session);
        return mailTemplate;
    }

    public ArrayList getCustomerListForEmail(String customerId) throws FPRuntimeException, FPBusinessException {
        ArrayList customerList = new ArrayList();
        customerList = reportDAO.getCustomerListForEmail(customerId);
        return customerList;
    }

    public ArrayList getInvoiceListSendEmail(String custId) throws FPRuntimeException, FPBusinessException {
        ArrayList invoiceReport = new ArrayList();
        invoiceReport = reportDAO.getInvoiceListSendEmail(custId);
        return invoiceReport;
    }

    public int updateInvoiceCustomerSendMail(ReportTO reportTO) {
        int updateSecondaryCustomerMail = 0;
        //System.out.println("Report BP innn");
        updateSecondaryCustomerMail = reportDAO.updateInvoiceCustomerSendMail(reportTO);
        return updateSecondaryCustomerMail;
    }

    public ArrayList getCustomerContractMailList() throws FPRuntimeException, FPBusinessException {
        ArrayList getCustomerContractMailList = new ArrayList();
        getCustomerContractMailList = reportDAO.getCustomerContractMailList();
        return getCustomerContractMailList;
    }

    public ArrayList getCustomerContractExpiryList(SchedulerTO schedulerTO) throws FPRuntimeException, FPBusinessException {
        //System.out.println("getCustomerContractExpiryList@@@@@@");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        String systemTime = sdf.format(c.getTime()).toString();
        //System.out.println("systemTime = " + systemTime);
        String[] dateTemp = systemTime.split(" ");
        String[] timeTemp = dateTemp[1].split(":");
        ArrayList contractExpiryList = new ArrayList();

//             -------CustomerContractExpiry scheduler----------
        String rangeStartDate = "";
        String rangeStartDate1 = "";
        String Filename = "";
        String File_Name = "";
        String contentdata = "";
        StringBuffer contentSB = new StringBuffer();
        SendEmail em = new SendEmail();
        ArrayList alertsDetails = new ArrayList();
        ArrayList alertsEmail = new ArrayList();

        File theDir = new File("/var/lib/tomcat7/webapps/CustomerContractExpiryAlert/");
//        File theDir = new File("D:\\DICT\\CustomerContractExpiryAlert\\");

        if (!theDir.exists()) {
            boolean result = theDir.mkdir();
            if (result) {
                //System.out.println("/var/lib/tomcat7/webapps/CustomerContractExpiryAlert/ DIR created");
//                //System.out.println("D:\\DICT\\CustomerContractExpiryAlert\\ DIR created");
            }
        }

        try {
            int i = 0;

            //System.out.println("inside processAlerts function");
            final Properties properties = new Properties();

            //System.out.println("schedulerTO.getEmailId()----" + schedulerTO.getEmailId());
            //System.out.println("schedulerTO.getCreatedBy()----" + schedulerTO.getCreatedBy());
            String recTo = schedulerTO.getEmailId();
            String recCc = ThrottleConstants.contractExpiryCCMailId;
            String recBcc = ThrottleConstants.contractExpiryBccMailId;
            //System.out.println("recipients recTo:  " + recTo);
            //System.out.println("recipients recCc: " + recCc);
            //System.out.println("recipients recBcc: " + recBcc);

            Date dNow1 = new Date();
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            SimpleDateFormat processDateFormat11 = new SimpleDateFormat("yyyy-MM-dd");
            String dt = dateFormat1.format(dNow1).toString();
            String substr = dt.substring(11, 13);

            int MILLIS_IN_DAY = 1000 * 60 * 60 * 24 * 7;
//                Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
            SimpleDateFormat processDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat processDateFormat1 = new SimpleDateFormat("dd-MM-yyyy");
            rangeStartDate1 = processDateFormat1.format(date.getTime());
            //System.out.println("rangeStartDate1@@@" + rangeStartDate1);
            rangeStartDate = processDateFormat.format(date.getTime());
            //System.out.println("rangeStartDate@@@" + rangeStartDate);
            //System.out.println("recipients: " + rangeStartDate);

            int uStatus = 0;
            ReportTO reportTO = null;
            SchedulerTO schTO = null;

            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("hh:mm:a");
            SimpleDateFormat fHour = new SimpleDateFormat("hh");
            //System.out.println("Current Date: " + ft.format(dNow));
            //System.out.println("Current Hour: " + fHour.format(dNow));
            String curTime = ft.format(dNow);
            String[] temp = null;
            temp = curTime.split(":");
            int curHour = Integer.parseInt(fHour.format(dNow));
            //System.out.println("curTime = " + curTime);
            //System.out.println("curHour = " + curHour);
            String curMeridian = temp[2];
            //System.out.println("curMeridian = " + curMeridian);

            int time = 0;
            int everyHourStatus = 0;
            int oddEven = curHour % 2;
            if (oddEven > 0) {
                everyHourStatus = 0;
            } else {
                everyHourStatus = 1;
            }

            String timeNow = temp[0] + "_" + temp[1] + "_" + temp[2];
            String filename = "/var/lib/tomcat7/webapps/CustomerContractExpiryAlert/" + "CustContractExpiryAlert" + rangeStartDate1 + ".xls";
//            String filename = "D:\\DICT\\" + "CustContractExpiryAlert" + rangeStartDate1 + ".xls";
            //System.out.println("filename = " + filename);
            ArrayList getCustomerContractExpiryList = new ArrayList();
            ArrayList result1 = new ArrayList();
            getCustomerContractExpiryList = reportDAO.getCustomerContractExpiryList(schedulerTO);
            //System.out.println("getCustomerContractExpiryList.size() = " + getCustomerContractExpiryList.size());
            if (getCustomerContractExpiryList.size() > 0) {
                result1.addAll(getCustomerContractExpiryList);
                //System.out.println("Result Waiting = " + result1.size());
            }
            //Iterator itrVehicle = getTotalOrderList.iterator();
            schTO = null;

            //System.out.println("result1.size() = " + result1.size());

            //first my_sheet start
            HSSFWorkbook my_workbook = new HSSFWorkbook();
            String sheetName = "contract" + rangeStartDate1;
            Sheet my_sheet = (Sheet) my_workbook.createSheet(sheetName);
            Row s1Row1 = my_sheet.createRow((short) 0);
            s1Row1.setHeightInPoints(50); // row hight

            HSSFCellStyle style = my_workbook.createCellStyle();
            style.setFillForegroundColor(HSSFColor.CORNFLOWER_BLUE.index);
            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            style.setRightBorderColor(IndexedColors.BLACK.getIndex());
            style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            style.setTopBorderColor(IndexedColors.BLACK.getIndex());
            style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            HSSFFont font = my_workbook.createFont();
            font.setFontName(HSSFFont.FONT_ARIAL);
            font.setFontHeightInPoints((short) 10);
            font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            font.setColor(HSSFColor.BLACK.index);
            style.setFont(font);

            Cell cellc1 = s1Row1.createCell((short) 0);
            cellc1.setCellValue("S.No");
            cellc1.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 0, (short) 4000); // cell width

            Cell cellc2 = s1Row1.createCell((short) 1);
            cellc2.setCellValue("CustomerCode");
            cellc2.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 1, (short) 4000); // cell width

            Cell cellc3 = s1Row1.createCell((short) 2);
            cellc3.setCellValue("CustomerName");
            cellc3.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 2, (short) 4000); // cell width

            Cell cellc4 = s1Row1.createCell((short) 3);
            cellc4.setCellValue("Contact Person");
            cellc4.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 3, (short) 4000); // cell width

            Cell cellc5 = s1Row1.createCell((short) 4);
            cellc5.setCellValue("Address");
            cellc5.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 4, (short) 4000); // cell width

            Cell cellc6 = s1Row1.createCell((short) 5);
            cellc6.setCellValue("Mobile");
            cellc6.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 5, (short) 4000); // cell width

            Cell cellc7 = s1Row1.createCell((short) 6);
            cellc7.setCellValue("ContractFromDate");
            cellc7.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 6, (short) 4000); // cell width

            Cell cellc8 = s1Row1.createCell((short) 7);
            cellc8.setCellValue("ContractToDate");
            cellc8.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 7, (short) 4000); // cell width

            Cell cellc9 = s1Row1.createCell((short) 8);
            cellc9.setCellValue("GSTNo");
            cellc9.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 8, (short) 4000); // cell width

            Cell cellc10 = s1Row1.createCell((short) 9);
            cellc10.setCellValue("PAN No");
            cellc10.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 9, (short) 4000); // cell width

            Cell cellc11 = s1Row1.createCell((short) 10);
            cellc11.setCellValue("Expiry Days");
            cellc11.setCellStyle((CellStyle) style);
            my_sheet.setColumnWidth((short) 10, (short) 4000); // cell width

            Iterator itr = result1.iterator();
            schTO = null;
            int cntr = 1;
            int order_closed = 0;
            int order_end = 0;
            int order_progress = 0;
            int order_freezed = 0;
            int order_created = 0;

            while (itr.hasNext()) {
                schTO = (SchedulerTO) itr.next();

                s1Row1 = my_sheet.createRow((short) cntr);
                s1Row1.createCell((short) 0).setCellValue(cntr);
                s1Row1.createCell((short) 1).setCellValue(schTO.getCustCode());
                s1Row1.createCell((short) 2).setCellValue(schTO.getCustName());
                s1Row1.createCell((short) 3).setCellValue(schTO.getContactPerson());

                s1Row1.createCell((short) 4).setCellValue(schTO.getAddress());
                s1Row1.createCell((short) 5).setCellValue(schTO.getMobile());
                s1Row1.createCell((short) 6).setCellValue(schTO.getFromDate());
                s1Row1.createCell((short) 7).setCellValue(schTO.getToDate());
                s1Row1.createCell((short) 8).setCellValue(schTO.getGstNo());
                s1Row1.createCell((short) 9).setCellValue(schTO.getPanNo());
                s1Row1.createCell((short) 10).setCellValue(schTO.getDiffDays());
                cntr++;
            }

            //System.out.println("Your excel 1 Sheet  created");

            my_workbook.setSheetOrder("contract" + rangeStartDate1, 0);
            //System.out.println("rangeStartDate111112:" + rangeStartDate1);

            // my_workbook.setSheetOrder("Future Trip" + rangeStartDate1, 6);
            //System.out.println("testing in the excel sheet creation");
            FileOutputStream fileOut = new FileOutputStream(filename);
            my_workbook.write(fileOut);
            fileOut.close();

            //System.out.println("Your excel file has been generated!");

            File_Name = "/var/lib/tomcat7/webapps/CustomerContractExpiryAlert/" + "CustContractExpiryAlert" + rangeStartDate1 + ".xls";
//            File_Name = "D:\\DICT\\" + "CustContractExpiryAlert" + rangeStartDate1 + ".xls";
            Filename = "CustomerContractExpiryAlert" + rangeStartDate1 + " " + ft.format(dNow) + ".xls";
            //System.out.println(File_Name);
            //System.out.println(Filename);
            String bcc = "";
            String mailContent = "Customer Contract Expiry Alerts" + rangeStartDate1 + " " + ft.format(dNow);

//                recTo = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts.to");
//                recCc = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts.cc");
            Date dNow11 = new Date();
            SimpleDateFormat dateFormat11 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            SimpleDateFormat processDateFormat111 = new SimpleDateFormat("yyyy-MM-dd");
            String dt1 = dateFormat11.format(dNow1).toString();
            String substr1 = dt.substring(11, 13);
            //  String compare=substr+" 10:30:00";
            // substr = substr + " 10:00:00";
//           if (substr1.trim().equals("17")) {
//                    recTo = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts.to");
//                    recCc = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts.cc");
//                } else {
//                    recTo = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts1.to");
//                    recCc = batchUtil.getProperty("mail.recipients.wfuAgeingAlerts1.cc");
//                }

            // recBcc = "";
            String emailFormat1 = "";
            String emailFormat2 = "";

            String atlMsg = "TMS ALERTS - Customer Contract Expiry Alert as on - " + rangeStartDate1;
            String dueAlt = "Customer_Contract_Expiry_Alert as on " + rangeStartDate1;
            contentdata = "Hi Greetings From TMS, <br><br>";
            contentdata = contentdata + "<table border='1' cellspacing='4' cellpadding='1' width='600px'>" + contentSB.toString() + "</table>";
            contentdata = contentdata + "<br><br> Team Throttle";
            emailFormat1 = "<html>"
                    + "<body>"
                    + "<p style='font-style: italic;'><b>Dear Sir/Madam, <br><br>Please find below Attatchement of Contract Expiry Alert<br><br/>";

            emailFormat2 = emailFormat1 + "<br><br>Hi Greetings From TMS"
                    + "</body></html>";

            SchedulerTO schTO1 = new SchedulerTO();
            schTO1.setMailStatus("1");
            schTO1.setMailTypeId("5");
            schTO1.setMailIdTo(recTo);
            schTO1.setMailIdCc(recCc);
            schTO1.setMailIdBcc(recBcc);
            schTO1.setMailSubjectTo("Customer Contract Expiry Alert");
            schTO1.setMailSubjectCc("Customer Contract Expiry Alert");
            schTO1.setMailSubjectBcc("Customer Contract Expiry Alert");
            schTO1.setFilenameContent(File_Name);
            schTO1.setEmailFormat(emailFormat2);
            //System.out.println("filenameContent" + Filename);
            //System.out.println("emailFormat4" + emailFormat2);

            Integer mails = reportDAO.insertMailDetails(schTO1, 1);
            //System.out.println("insertMailDetails@@@" + mails);
            //System.out.println("recBcc:--" + recBcc);
//            if (result1.size() > 0) {
            em.sendExcelMail(Filename, recTo, recCc, recBcc, mailContent, emailFormat2, my_workbook);
            //System.out.println("closed");
//            }

        } catch (Exception excp) {
            //System.out.println("ERROR STATUS   " + excp);

            excp.printStackTrace();
        }
        return contractExpiryList;
    }

    public int callEInvoice(int statusId) throws FPRuntimeException, FPBusinessException, MalformedURLException, IOException, ParseException {
        int callEInvoice = 0;
        OperationTO OpTO = new OperationTO();
        OperationTO OpTO1 = new OperationTO();
        //System.out.println("statusId--controller-" + statusId);
        ArrayList getEInvoiceList = new ArrayList();
        try {
            getEInvoiceList = reportDAO.getEInvoiceList(statusId);
////
            Iterator itr1 = getEInvoiceList.iterator();
            while (itr1.hasNext()) {
                OpTO = (OperationTO) itr1.next();
//
                String invoiceId = OpTO.getInvoiceId();
                String documentDate = OpTO.getDocumentDate();
                String documentNumber = OpTO.getDocumentNumber();
                String invType = OpTO.getInvType();
                String supTyp = OpTO.getSupTyp();
                String billingState = OpTO.getBillingState();
                String billingPin = OpTO.getBillingPin();
                String billingLocation = OpTO.getBillingLocation();
                String billlingAddress = OpTO.getBillingAddress();
                String billingLegalName = OpTO.getBillingLegalName();
                String billingTradeName = OpTO.getBillingTradeName();
                String billingGstin = OpTO.getBillingGstin();
                String grossAmount = OpTO.getGrossAmount();
                String cgst_header = OpTO.getCgst();
                String sgst_header = OpTO.getSgst();
                String igst_header = OpTO.getIgst();
                String cessValue = OpTO.getCessValue();
                String statecessValue = OpTO.getStatecessValue();
                String roundoff = OpTO.getRoundoff();
                String totalNetAmount = OpTO.getTotalNetAmount();
                String version = OpTO.getVersion();
                String igstOnIntra = OpTO.getIgstOnIntra();
                String regRev = OpTO.getRegRev();
                String taxsch = OpTO.getTaxSch();
                //System.out.println("billlingAddress============einvoice--------" + billlingAddress);
                ArrayList getItemDetails = new ArrayList();
                getItemDetails = reportDAO.getItemDetails(invoiceId, statusId);
                //System.out.println("getItemDetails :" + getItemDetails.size());

                String itemList = "";

                Iterator itr11 = getItemDetails.iterator();
                String serialNo = "";
                int cnt = 1;
                while (itr11.hasNext()) {
                    OpTO1 = (OperationTO) itr11.next();

                    String slno = OpTO1.getSnos();
                    String isservc = OpTO1.getServiceTypeName();
                    String hsnCd = OpTO1.getHsnCd();
                    String qty = OpTO1.getQty();
                    String units = OpTO1.getUnit();
                    String unitPrice = OpTO1.getUnitPrice();
                    String totItemPrice = OpTO1.getItemPrice();
                    String itemAssAmt = OpTO1.getItemAssAount();
                    String igst_percent = OpTO1.getIgstPercent();
                    double i = Double.parseDouble(igst_percent);
                    String sgst_percent = OpTO1.getSgstPercent();
                    double s = Double.parseDouble(sgst_percent);
                    String igstAmt = OpTO1.getTotalIgst();
                    String cgstAmt = OpTO1.getTotalCgst();
                    String sgstAmt = OpTO1.getTotalSgst();
                    String totItemVal = OpTO1.getTotalamount();
                    String GstRt = "";
                    String productDesc = OpTO1.getProductDesc();
                    if (i > 0) {
                        GstRt = igst_percent;
                    } else {
                        GstRt = sgst_percent;
                    }

                    if (cnt == 1) {
                        itemList = "{"
                                + "\"SlNo\":\"" + slno + "\","
                                + "\"IsServc\":\"" + isservc + "\","
                                + "\"HsnCd\":\"" + hsnCd + "\","
                                + "\"Qty\":\"" + qty + "\","
                                + "\"Unit\":\"" + units + "\","
                                + "\"UnitPrice\":" + unitPrice + ","
                                + "\"TotAmt\":" + totItemPrice + ","
                                + "\"AssAmt\":" + itemAssAmt + ","
                                + "\"GstRt\":" + GstRt + ","
                                + "\"SgstAmt\":" + sgstAmt + ","
                                + "\"IgstAmt\":" + igstAmt + ","
                                + "\"CgstAmt\":" + cgstAmt + ","
                                + "\"TotItemVal\":" + totItemVal + ","
                                + "\"PrdDesc\":\"" + productDesc + "\"}";
                    } else {
                        itemList += ",{"
                                + "\"SlNo\":\"" + slno + "\","
                                + "\"IsServc\":\"" + isservc + "\","
                                + "\"HsnCd\":\"" + hsnCd + "\","
                                + "\"Qty\":\"" + qty + "\","
                                + "\"Unit\":\"" + units + "\","
                                + "\"UnitPrice\":" + unitPrice + ","
                                + "\"TotAmt\":" + totItemPrice + ","
                                + "\"AssAmt\":" + itemAssAmt + ","
                                + "\"GstRt\":" + GstRt + ","
                                + "\"SgstAmt\":" + sgstAmt + ","
                                + "\"IgstAmt\":" + igstAmt + ","
                                + "\"CgstAmt\":" + cgstAmt + ","
                                + "\"TotItemVal\":" + totItemVal + ","
                                + "\"PrdDesc\":\"" + productDesc + "\"}";
                    }
                    cnt++;

                }

//            String reasonsforNondelivery = repTO.getReasonsforNondelivery();
//            String reasonsforMemberRefusal = repTO.getReasonsforMemberRefusal();
                final String URLLink = "https://api-einv.cleartax.in/v2/eInvoice/generate";
//            final String user = ThrottleConstants.userHS;
//            final String password = ThrottleConstants.passHS;
                //System.out.println("URLLink-----------" + URLLink);
                URL myurl = new URL(URLLink);
                HttpURLConnection connection = null;
                StringBuffer jsonString = new StringBuffer();
                String line;

                String payload = "[ {"
                        + "  \"transaction\": {"
                        + "    \"Version\": \"" + version + "\","
                        + "    \"TranDtls\": {"
                        + "      \"IgstOnIntra\": \"" + igstOnIntra + "\","
                        + "      \"RegRev\": \"" + regRev + "\","
                        + "      \"TaxSch\": \"" + taxsch + "\","
                        + "      \"SupTyp\": \"" + supTyp + "\""
                        + "    },"
                        + "    \"DocDtls\": {"
                        + "      \"Dt\":\"" + documentDate + "\","
                        + "      \"No\": \"" + documentNumber + "\","
                        + "      \"Typ\": \"" + invType + "\""
                        + "    },"
                        + "    \"SellerDtls\": {"
                        + "      \"Stcd\": \"06\","
                        + "      \"Pin\": \"131101\","
                        + "      \"Loc\": \"Sonepat\","
                        + "      \"Addr2\": \"billing@ict.in\","
                        + "      \"Addr1\": \"Panchi Gujran, Ganaur, Sonepat - 131101 Haryana. INDIA\","
                        + "      \"TrdNm\": \"INTERNATIONAL CARGO TERMINALS AND RAIL INFRASTRUCTURE PVT. LTD.\","
                        + "      \"LglNm\": \"INTERNATIONAL CARGO TERMINALS AND RAIL INFRASTRUCTURE PVT. LTD.\","
                        + "      \"Gstin\": \"06AACCB8054G1ZT\""
                        + "    },"
                        + "    \"BuyerDtls\": {"
                        + "      \"Pos\": \"" + billingState + "\","
                        + "      \"Stcd\": \"" + billingState + "\","
                        + "      \"Pin\": \"" + billingPin + "\","
                        + "      \"Loc\": \"" + billingLocation + "\","
                        + "      \"Addr1\": \"" + billlingAddress + "\","
                        + "      \"TrdNm\": \"" + billingTradeName + "\","
                        + "      \"LglNm\": \"" + billingLegalName + "\","
                        + "      \"Gstin\": \"" + billingGstin + "\""
                        + "    },"
                        + " \"ItemList\": [" + itemList + ""
                        + "],"
                        + "    \"ValDtls\": {"
                        + "      \"AssVal\":" + grossAmount + ","
                        + "      \"CgstVal\":" + cgst_header + ","
                        + "      \"SgstVal\": " + sgst_header + ","
                        + "      \"IgstVal\": " + igst_header + ","
                        + "      \"CesVal\": " + cessValue + ","
                        + "      \"StCesVal\": " + statecessValue + ","
                        + "      \"RndOffAmt\": " + roundoff + ","
                        + "      \"TotInvVal\": " + totalNetAmount + ""
                        + "    }"
                        + "  }"
                        + "} ]";
//            String ownerId = "5325f8c6-9765-4456-a6b4-f5818cf758c8";
//            String AuthToken = "1.f62be703-c2e1-4b7a-bf1e-156720b6a9f1_4204d3da2db983b54c1e8b056a784f2675b0be48add5e9dd7fd51248bb11109c";
//            String GSTIN  = "07AAFCD5862R007";

                String ownerId = ThrottleConstants.ownerId;
                String AuthToken = ThrottleConstants.tokenId;
                String GSTIN = ThrottleConstants.gstIn;

                //System.out.println("Einvoice ownerId===" + ownerId);
                //System.out.println("Einvoice AuthToken===" + AuthToken);
                //System.out.println("Einvoice GSTIN===" + GSTIN);

                //System.out.println("payload " + payload);

                connection = (HttpURLConnection) myurl.openConnection();
                connection.setDoOutput(true);

                connection.setRequestMethod("PUT");
                connection.setRequestProperty("Accept", "application/json");
                connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                connection.setRequestProperty("owner_id", ownerId);
                connection.setRequestProperty("gstin", GSTIN);
                connection.setRequestProperty("X-Cleartax-Auth-Token", AuthToken);
                OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
                writer.write(payload);
                writer.close();

                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while ((line = br.readLine()) != null) {
                    jsonString.append(line);
                }
                br.close();
                connection.disconnect();
                //System.out.println("jsonString :" + jsonString);
                String response = jsonString.toString();
                //System.out.println("response====" + response);

//                int updateApiResponse = reportDAO.updateApiResponse(payload, response, invType);
//                //System.out.println("updateApiResponse in dump==" + updateApiResponse);
                JSONParser parser = new JSONParser();
                Object obj1 = parser.parse(jsonString.toString());
//            //System.out.println("obj1===="+obj1);
                org.json.simple.JSONArray jsonarry = (org.json.simple.JSONArray) obj1;
                org.json.simple.JSONObject jsonobj = (org.json.simple.JSONObject) jsonarry.get(0);
//            //System.out.println("jsonobj======="+jsonobj);

                JSONObject gov_res = (JSONObject) jsonobj.get("govt_response");
                String Success = gov_res.get("Success").toString();
                //System.out.println("Success=======" + Success);

                if (Success.equals("Y")) {
                    String Irn = gov_res.get("Irn").toString();
                    String SignedQRCode = gov_res.get("SignedQRCode").toString();
                    //System.out.println("Irn=======" + Irn);
                    //System.out.println("SignedQRCode=======" + SignedQRCode);

                    int updateUploadEInvoice = reportDAO.updateUploadEInvoice(invoiceId, Irn, SignedQRCode);
                    //System.out.println("updateUploadEInvoice==" + updateUploadEInvoice);

                } else {
                    JSONArray error_details = (JSONArray) gov_res.get("ErrorDetails");
                    String error = error_details.toString();
                    //System.out.println("error=========" + error);
                    int updateUploadEInvoice = reportDAO.updateErrorStatus(invoiceId, error);
                    //System.out.println("updateUploadEInvoice-----" + updateUploadEInvoice);
                }

            }
        } catch (Exception ex) {
            //System.out.println("Einvoice exception caught---" + ex);
            ex.printStackTrace();
        }
        return callEInvoice;
    }

    public int callESuppInvoice(int statusId) throws FPRuntimeException, FPBusinessException, MalformedURLException, IOException, ParseException {
        int callEInvoice = 0;
        OperationTO OpTO = new OperationTO();
        OperationTO OpTO1 = new OperationTO();
        //System.out.println("statusId--controller-" + statusId);
        try {
            ArrayList getEInvoiceList = new ArrayList();
            getEInvoiceList = reportDAO.getESuppInvoiceList(statusId);
            Iterator itr1 = getEInvoiceList.iterator();
            while (itr1.hasNext()) {
                OpTO = (OperationTO) itr1.next();
//
                String invoiceId = OpTO.getInvoiceId();
                String documentDate = OpTO.getDocumentDate();
                String documentNumber = OpTO.getDocumentNumber();
                String invType = OpTO.getInvType();
                String supTyp = OpTO.getSupTyp();
                String billingState = OpTO.getBillingState();
                String billingPin = OpTO.getBillingPin();
                String billingLocation = OpTO.getBillingLocation();
                String billlingAddress = OpTO.getBillingAddress();
                String billingLegalName = OpTO.getBillingLegalName();
                String billingTradeName = OpTO.getBillingTradeName();
                String billingGstin = OpTO.getBillingGstin();
                String grossAmount = OpTO.getGrossAmount();
                String cgst_header = OpTO.getCgst();
                String sgst_header = OpTO.getSgst();
                String igst_header = OpTO.getIgst();
                String cessValue = OpTO.getCessValue();
                String statecessValue = OpTO.getStatecessValue();
                String roundoff = OpTO.getRoundoff();
                String totalNetAmount = OpTO.getTotalNetAmount();
                String version = OpTO.getVersion();
                String igstOnIntra = OpTO.getIgstOnIntra();
                String regRev = OpTO.getRegRev();
                String taxsch = OpTO.getTaxSch();

                //System.out.println("---supp---invoiceId---" + invoiceId);
                ArrayList getItemDetails = new ArrayList();
                getItemDetails = reportDAO.getSuppItemDetails(invoiceId, statusId);
                //System.out.println("getItemDetails :" + getItemDetails.size());

                String itemList = "";

                Iterator itr11 = getItemDetails.iterator();
                String serialNo = "";
                int cnt = 1;
                while (itr11.hasNext()) {
                    OpTO1 = (OperationTO) itr11.next();

                    String slno = OpTO1.getSnos();
                    String isservc = OpTO1.getServiceTypeName();
                    String hsnCd = OpTO1.getHsnCd();
                    String qty = OpTO1.getQty();
                    String units = OpTO1.getUnit();
                    String unitPrice = OpTO1.getUnitPrice();
                    String totItemPrice = OpTO1.getItemPrice();
                    String itemAssAmt = OpTO1.getItemAssAount();
                    String igst_percent = OpTO1.getIgstPercent();
                    double i = Double.parseDouble(igst_percent);
                    String sgst_percent = OpTO1.getSgstPercent();
                    double s = Double.parseDouble(sgst_percent);
                    String igstAmt = OpTO1.getTotalIgst();
                    String cgstAmt = OpTO1.getTotalCgst();
                    String sgstAmt = OpTO1.getTotalSgst();
                    String totItemVal = OpTO1.getTotalamount();
                    String GstRt = "";
                    String productDesc = OpTO1.getProductDesc();
                    if (i > 0) {
                        GstRt = igst_percent;
                    } else {
                        GstRt = sgst_percent;
                    }

                    if (cnt == 1) {
                        itemList = "{"
                                + "\"SlNo\":\"" + slno + "\","
                                + "\"IsServc\":\"" + isservc + "\","
                                + "\"HsnCd\":\"" + hsnCd + "\","
                                + "\"Qty\":\"" + qty + "\","
                                + "\"Unit\":\"" + units + "\","
                                + "\"UnitPrice\":" + unitPrice + ","
                                + "\"TotAmt\":" + totItemPrice + ","
                                + "\"AssAmt\":" + itemAssAmt + ","
                                + "\"GstRt\":" + GstRt + ","
                                + "\"SgstAmt\":" + sgstAmt + ","
                                + "\"IgstAmt\":" + igstAmt + ","
                                + "\"CgstAmt\":" + cgstAmt + ","
                                + "\"TotItemVal\":" + totItemVal + ","
                                + "\"PrdDesc\":\"" + productDesc + "\"}";
                    } else {
                        itemList += ",{"
                                + "\"SlNo\":\"" + slno + "\","
                                + "\"IsServc\":\"" + isservc + "\","
                                + "\"HsnCd\":\"" + hsnCd + "\","
                                + "\"Qty\":\"" + qty + "\","
                                + "\"Unit\":\"" + units + "\","
                                + "\"UnitPrice\":" + unitPrice + ","
                                + "\"TotAmt\":" + totItemPrice + ","
                                + "\"AssAmt\":" + itemAssAmt + ","
                                + "\"GstRt\":" + GstRt + ","
                                + "\"SgstAmt\":" + sgstAmt + ","
                                + "\"IgstAmt\":" + igstAmt + ","
                                + "\"CgstAmt\":" + cgstAmt + ","
                                + "\"TotItemVal\":" + totItemVal + ","
                                + "\"PrdDesc\":\"" + productDesc + "\"}";
                    }
                    cnt++;

                }

//            String reasonsforNondelivery = repTO.getReasonsforNondelivery();
//            String reasonsforMemberRefusal = repTO.getReasonsforMemberRefusal();
                final String URLLink = "https://api-einv.cleartax.in/v2/eInvoice/generate";
//            final String user = ThrottleConstants.userHS;
//            final String password = ThrottleConstants.passHS;

                URL myurl = new URL(URLLink);
                HttpURLConnection connection = null;
                StringBuffer jsonString = new StringBuffer();
                String line;

                String payload = "[ {"
                        + "  \"transaction\": {"
                        + "    \"Version\": \"" + version + "\","
                        + "    \"TranDtls\": {"
                        + "      \"IgstOnIntra\": \"" + igstOnIntra + "\","
                        + "      \"RegRev\": \"" + regRev + "\","
                        + "      \"TaxSch\": \"" + taxsch + "\","
                        + "      \"SupTyp\": \"" + supTyp + "\""
                        + "    },"
                        + "    \"DocDtls\": {"
                        + "      \"Dt\":\"" + documentDate + "\","
                        + "      \"No\": \"" + documentNumber + "\","
                        + "      \"Typ\": \"" + invType + "\""
                        + "    },"
                        + "    \"SellerDtls\": {"
                        + "      \"Stcd\": \"06\","
                        + "      \"Pin\": \"131101\","
                        + "      \"Loc\": \"Sonepat\","
                        + "      \"Addr2\": \"billing@ict.in\","
                        + "      \"Addr1\": \"Panchi Gujran, Ganaur, Sonepat - 131101 Haryana. INDIA\","
                        + "      \"TrdNm\": \"INTERNATIONAL CARGO TERMINALS AND RAIL INFRASTRUCTURE PVT. LTD.\","
                        + "      \"LglNm\": \"INTERNATIONAL CARGO TERMINALS AND RAIL INFRASTRUCTURE PVT. LTD.\","
                        + "      \"Gstin\": \"06AACCB8054G1ZT\""
                        + "    },"
                        + "    \"BuyerDtls\": {"
                        + "      \"Pos\": \"" + billingState + "\","
                        + "      \"Stcd\": \"" + billingState + "\","
                        + "      \"Pin\": \"" + billingPin + "\","
                        + "      \"Loc\": \"" + billingLocation + "\","
                        + "      \"Addr1\": \"" + billlingAddress + "\","
                        + "      \"TrdNm\": \"" + billingTradeName + "\","
                        + "      \"LglNm\": \"" + billingLegalName + "\","
                        + "      \"Gstin\": \"" + billingGstin + "\""
                        + "    },"
                        + " \"ItemList\": [" + itemList + ""
                        + "],"
                        + "    \"ValDtls\": {"
                        + "      \"AssVal\":" + grossAmount + ","
                        + "      \"CgstVal\":" + cgst_header + ","
                        + "      \"SgstVal\": " + sgst_header + ","
                        + "      \"IgstVal\": " + igst_header + ","
                        + "      \"CesVal\": " + cessValue + ","
                        + "      \"StCesVal\": " + statecessValue + ","
                        + "      \"RndOffAmt\": " + roundoff + ","
                        + "      \"TotInvVal\": " + totalNetAmount + ""
                        + "    }"
                        + "  }"
                        + "} ]";
//            String ownerId = "5325f8c6-9765-4456-a6b4-f5818cf758c8";
//            String AuthToken = "1.f62be703-c2e1-4b7a-bf1e-156720b6a9f1_4204d3da2db983b54c1e8b056a784f2675b0be48add5e9dd7fd51248bb11109c";
//            String GSTIN  = "07AAFCD5862R007";

                String ownerId = ThrottleConstants.ownerId;
                String AuthToken = ThrottleConstants.tokenId;
                String GSTIN = ThrottleConstants.gstIn;

                //System.out.println("ESuppinvoice ownerId===" + ownerId);
                //System.out.println("ESuppinvoice AuthToken===" + AuthToken);
                //System.out.println("ESuppinvoice GSTIN===" + GSTIN);

                //System.out.println("payload supplement invoice---" + payload);
                connection = (HttpURLConnection) myurl.openConnection();
                connection.setDoOutput(true);

                connection.setRequestMethod("PUT");
                connection.setRequestProperty("Accept", "application/json");
                connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                connection.setRequestProperty("owner_id", ownerId);
                connection.setRequestProperty("gstin", GSTIN);
                connection.setRequestProperty("X-Cleartax-Auth-Token", AuthToken);
                OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
                writer.write(payload);
                writer.close();

                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while ((line = br.readLine()) != null) {
                    jsonString.append(line);
                }
                br.close();
                connection.disconnect();
//            //System.out.println("jsonString :" + jsonString);
                String response = jsonString.toString();
//            //System.out.println("response===="+response);

//                int updateApiResponse = reportDAO.updateSuppApiResponse(payload, response, invType);
//                //System.out.println("updateApiResponse in dump==" + updateApiResponse);
                JSONParser parser = new JSONParser();
                Object obj1 = parser.parse(jsonString.toString());
//            //System.out.println("obj1===="+obj1);
                org.json.simple.JSONArray jsonarry = (org.json.simple.JSONArray) obj1;
                org.json.simple.JSONObject jsonobj = (org.json.simple.JSONObject) jsonarry.get(0);
//            //System.out.println("jsonobj======="+jsonobj);

                JSONObject gov_res = (JSONObject) jsonobj.get("govt_response");
                String Success = gov_res.get("Success").toString();
                //System.out.println("Success=======" + Success);

                if (Success.equals("Y")) {
                    String Irn = gov_res.get("Irn").toString();
                    String SignedQRCode = gov_res.get("SignedQRCode").toString();
                    //System.out.println("Irn=======" + Irn);
                    //System.out.println("SignedQRCode=======" + SignedQRCode);

                    int updateUploadEInvoice = reportDAO.updateUploadESuppInvoice(invoiceId, Irn, SignedQRCode);
                    //System.out.println("updateUploadEInvoice==" + updateUploadEInvoice);

                } else {
                    JSONArray error_details = (JSONArray) gov_res.get("ErrorDetails");
                    String error = error_details.toString();
                    //System.out.println("error=========" + error);
                    int updateUploadEInvoice = reportDAO.updateSuppErrorStatus(invoiceId, error);
                    //System.out.println("updateSuppErrorStatus--" + updateUploadEInvoice);
                }

            }
        } catch (Exception ex) {
//            //System.out.println("ESuppinvoice exception caught---" + ex);
            ex.printStackTrace();
        }

        return callEInvoice;
    }

    public int callECreditInvoice(int statusId) throws FPRuntimeException, FPBusinessException, MalformedURLException, IOException, ParseException {
        int callEInvoice = 0;
        OperationTO OpTO = new OperationTO();
        OperationTO OpTO1 = new OperationTO();
        //System.out.println("callECreditInvoice Bp-" + statusId);
        try {
            ArrayList getEInvoiceList = new ArrayList();
            getEInvoiceList = reportDAO.getECreditInvoiceList(statusId);
            //System.out.println("getECreditInvoiceList.size()----" + getEInvoiceList.size());
            Iterator itr1 = getEInvoiceList.iterator();
            while (itr1.hasNext()) {
                OpTO = (OperationTO) itr1.next();
//
                String invoiceId = OpTO.getInvoiceId();
                String documentDate = OpTO.getDocumentDate();
                String documentNumber = OpTO.getDocumentNumber();
                String invType = OpTO.getInvType();
                String supTyp = OpTO.getSupTyp();
                String billingState = OpTO.getBillingState();
                String billingPin = OpTO.getBillingPin();
                String billingLocation = OpTO.getBillingLocation();
                String billlingAddress = OpTO.getBillingAddress();
                String billingLegalName = OpTO.getBillingLegalName();
                String billingTradeName = OpTO.getBillingTradeName();
                String billingGstin = OpTO.getBillingGstin();
                String grossAmount = OpTO.getGrossAmount();
                String cgst_header = OpTO.getCgst();
                String sgst_header = OpTO.getSgst();
                String igst_header = OpTO.getIgst();
                String cessValue = OpTO.getCessValue();
                String statecessValue = OpTO.getStatecessValue();
                String roundoff = OpTO.getRoundoff();

                String totalNetAmount = OpTO.getTotalNetAmount();
                String version = OpTO.getVersion();
                String igstOnIntra = OpTO.getIgstOnIntra();
                String regRev = OpTO.getRegRev();
                String taxsch = OpTO.getTaxSch();
                String invoiceNo = OpTO.getInvoiceNo();
                String invoiceDate = OpTO.getInvoiceDate();

                ArrayList getItemDetails = new ArrayList();
                getItemDetails = reportDAO.getCreditItemDetails(invoiceId, statusId);
                //System.out.println("getCreditItemDetails :" + getItemDetails.size());

                String itemList = "";

                Iterator itr11 = getItemDetails.iterator();
                String serialNo = "";
                int cnt = 1;
                while (itr11.hasNext()) {
                    OpTO1 = (OperationTO) itr11.next();

                    String slno = OpTO1.getSnos();
                    String isservc = OpTO1.getServiceTypeName();
                    String hsnCd = OpTO1.getHsnCd();
                    String qty = OpTO1.getQty();
                    String units = OpTO1.getUnit();
                    String unitPrice = OpTO1.getUnitPrice();
                    String totItemPrice = OpTO1.getItemPrice();
                    String itemAssAmt = OpTO1.getItemAssAount();
                    String igst_percent = OpTO1.getIgstPercent();
                    double i = Double.parseDouble(igst_percent);
                    String sgst_percent = OpTO1.getSgstPercent();
                    double s = Double.parseDouble(sgst_percent);
                    String igstAmt = OpTO1.getTotalIgst();
                    String cgstAmt = OpTO1.getTotalCgst();
                    String sgstAmt = OpTO1.getTotalSgst();
                    String totItemVal = OpTO1.getTotalamount();
                    String GstRt = "";
                    String productDesc = OpTO1.getProductDesc();
                    if (i > 0) {
                        GstRt = igst_percent;
                    } else {
                        GstRt = sgst_percent;
                    }

                    if (cnt == 1) {
                        itemList = "{"
                                + "\"SlNo\":\"" + slno + "\","
                                + "\"IsServc\":\"" + isservc + "\","
                                + "\"HsnCd\":\"" + hsnCd + "\","
                                + "\"Qty\":\"" + qty + "\","
                                + "\"Unit\":\"" + units + "\","
                                + "\"UnitPrice\":" + unitPrice + ","
                                + "\"TotAmt\":" + totItemPrice + ","
                                + "\"AssAmt\":" + itemAssAmt + ","
                                + "\"GstRt\":" + GstRt + ","
                                + "\"SgstAmt\":" + sgstAmt + ","
                                + "\"IgstAmt\":" + igstAmt + ","
                                + "\"CgstAmt\":" + cgstAmt + ","
                                + "\"TotItemVal\":" + totItemVal + ","
                                + "\"PrdDesc\":\"" + productDesc + "\"}";
                    } else {
                        itemList += ",{"
                                + "\"SlNo\":\"" + slno + "\","
                                + "\"IsServc\":\"" + isservc + "\","
                                + "\"HsnCd\":\"" + hsnCd + "\","
                                + "\"Qty\":\"" + qty + "\","
                                + "\"Unit\":\"" + units + "\","
                                + "\"UnitPrice\":" + unitPrice + ","
                                + "\"TotAmt\":" + totItemPrice + ","
                                + "\"AssAmt\":" + itemAssAmt + ","
                                + "\"GstRt\":" + GstRt + ","
                                + "\"SgstAmt\":" + sgstAmt + ","
                                + "\"IgstAmt\":" + igstAmt + ","
                                + "\"CgstAmt\":" + cgstAmt + ","
                                + "\"TotItemVal\":" + totItemVal + ","
                                + "\"PrdDes"
                                + "c\":\"" + productDesc + "\"}";
                    }
                    cnt++;

                }

//            String reasonsforNondelivery = repTO.getReasonsforNondelivery();
//            String reasonsforMemberRefusal = repTO.getReasonsforMemberRefusal();
                final String URLLink = "https://api-einv.cleartax.in/v2/eInvoice/generate";
//            final String user = ThrottleConstants.userHS;
//            final String password = ThrottleConstants.passHS;

                URL myurl = new URL(URLLink);
                HttpURLConnection connection = null;
                StringBuffer jsonString = new StringBuffer();
                String line;
                //System.out.println("supTyp---" + supTyp);

                String payload = "[ {"
                        + "  \"transaction\": {"
                        + "    \"Version\": \"" + version + "\","
                        + "    \"TranDtls\": {"
                        + "      \"IgstOnIntra\": \"" + igstOnIntra + "\","
                        + "      \"RegRev\": \"" + regRev + "\","
                        + "      \"TaxSch\": \"" + taxsch + "\","
                        + "      \"SupTyp\": \"" + supTyp + "\""
                        + "    },"
                        + "    \"DocDtls\": {"
                        + "      \"Dt\":\"" + documentDate + "\","
                        + "      \"No\": \"" + documentNumber + "\","
                        + "      \"Typ\": \"" + invType + "\""
                        + "    },"
                        + "    \"SellerDtls\": {"
                        + "      \"Stcd\": \"06\","
                        + "      \"Pin\": \"131101\","
                        + "      \"Loc\": \"Sonepat\","
                        + "      \"Addr2\": \"billing@ict.in\","
                        + "      \"Addr1\": \"Panchi Gujran, Ganaur, Sonepat - 131101 Haryana. INDIA\","
                        + "      \"TrdNm\": \"INTERNATIONAL CARGO TERMINALS AND RAIL INFRASTRUCTURE PVT. LTD.\","
                        + "      \"LglNm\": \"INTERNATIONAL CARGO TERMINALS AND RAIL INFRASTRUCTURE PVT. LTD.\","
                        + "      \"Gstin\": \"06AACCB8054G1ZT\""
                        + "    },"
                        + "    \"BuyerDtls\": {"
                        + "      \"Pos\": \"" + billingState + "\","
                        + "      \"Stcd\": \"" + billingState + "\","
                        + "      \"Pin\": \"" + billingPin + "\","
                        + "      \"Loc\": \"" + billingLocation + "\","
                        + "      \"Addr1\": \"" + billlingAddress + "\","
                        + "      \"TrdNm\": \"" + billingTradeName + "\","
                        + "      \"LglNm\": \"" + billingLegalName + "\","
                        + "      \"Gstin\": \"" + billingGstin + "\""
                        + "    },"
                        + " \"ItemList\": [" + itemList + ""
                        + "],"
                        + "    \"ValDtls\": {"
                        + "      \"AssVal\":" + grossAmount + ","
                        + "      \"CgstVal\":" + cgst_header + ","
                        + "      \"SgstVal\": " + sgst_header + ","
                        + "      \"IgstVal\": " + igst_header + ","
                        + "      \"CesVal\": " + cessValue + ","
                        + "      \"StCesVal\": " + statecessValue + ","
                        + "      \"RndOffAmt\": " + roundoff + ","
                        + "      \"TotInvVal\": " + totalNetAmount + ""
                        + "    },"
                        + "    \"PrecDocDtls\": {"
                        + "      \"InvDt\":\"" + invoiceDate + "\","
                        + "      \"InvNo\":\"" + invoiceNo + "\""
                        + "    }"
                        + "  }"
                        + "} ]";
//            String ownerId = "5325f8c6-9765-4456-a6b4-f5818cf758c8";
//            String AuthToken = "1.f62be703-c2e1-4b7a-bf1e-156720b6a9f1_4204d3da2db983b54c1e8b056a784f2675b0be48add5e9dd7fd51248bb11109c";
//            String GSTIN  = "07AAFCD5862R007";

                String ownerId = ThrottleConstants.ownerId;
                String AuthToken = ThrottleConstants.tokenId;
                String GSTIN = ThrottleConstants.gstIn;

                //System.out.println("Ecreditinvoice ownerId===" + ownerId);
                //System.out.println("Ecreditinvoice AuthToken===" + AuthToken);
                //System.out.println("Ecreditinvoice GSTIN===" + GSTIN);

                //System.out.println("payload ####" + payload);

                connection = (HttpURLConnection) myurl.openConnection();
                connection.setDoOutput(true);

                connection.setRequestMethod("PUT");
                connection.setRequestProperty("Accept", "application/json");
                connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                connection.setRequestProperty("owner_id", ownerId);
                connection.setRequestProperty("gstin", GSTIN);
                connection.setRequestProperty("X-Cleartax-Auth-Token", AuthToken);
                OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
                writer.write(payload);
                writer.close();

                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while ((line = br.readLine()) != null) {
                    jsonString.append(line);
                }
                br.close();
                connection.disconnect();
//            //System.out.println("jsonString :" + jsonString);
                String response = jsonString.toString();
//            //System.out.println("response===="+response);

//                int updateApiResponse = reportDAO.updateCreditApiResponse(payload, response, invType);
//                //System.out.println("updateApiResponse in dump==" + updateApiResponse);
                JSONParser parser = new JSONParser();
                Object obj1 = parser.parse(jsonString.toString());
//            //System.out.println("obj1===="+obj1);
                org.json.simple.JSONArray jsonarry = (org.json.simple.JSONArray) obj1;
                org.json.simple.JSONObject jsonobj = (org.json.simple.JSONObject) jsonarry.get(0);
//            //System.out.println("jsonobj======="+jsonobj);

                JSONObject gov_res = (JSONObject) jsonobj.get("govt_response");
                String Success = gov_res.get("Success").toString();
                //System.out.println("Success=======" + Success);

                if (Success.equals("Y")) {
                    String Irn = gov_res.get("Irn").toString();
                    String SignedQRCode = gov_res.get("SignedQRCode").toString();
                    //System.out.println("Irn=======" + Irn);
                    //System.out.println("SignedQRCode=======" + SignedQRCode);

                    int updateUploadEInvoice = reportDAO.updateUploadECreditInvoice(invoiceId, Irn, SignedQRCode);
                    //System.out.println("updateUploadEInvoice==" + updateUploadEInvoice);

                } else {
                    JSONArray error_details = (JSONArray) gov_res.get("ErrorDetails");
                    String error = error_details.toString();
                    //System.out.println("error=========" + error);
                    int updateUploadEInvoice = reportDAO.updateCreditErrorStatus(invoiceId, error);
                    //System.out.println("updateCreditErrorStatus--" + updateUploadEInvoice);

                }

            }

        } catch (Exception ex) {
//            //System.out.println("ECreditinvoice exception caught---" + ex);
//            ex.printStackTrace();
        }
        return callEInvoice;
    }

    public int callECreditSuppInvoice(int statusId) throws FPRuntimeException, FPBusinessException, MalformedURLException, IOException, ParseException {
        int callEInvoice = 0;
        OperationTO OpTO = new OperationTO();
        OperationTO OpTO1 = new OperationTO();
        //System.out.println("statusId--controller-" + statusId);
        try {
            ArrayList getEInvoiceList = new ArrayList();
            getEInvoiceList = reportDAO.getECreditSuppInvoiceList(statusId);
////
            Iterator itr1 = getEInvoiceList.iterator();
            while (itr1.hasNext()) {
                OpTO = (OperationTO) itr1.next();
//
                String invoiceId = OpTO.getInvoiceId();
                String documentDate = OpTO.getDocumentDate();
                String documentNumber = OpTO.getDocumentNumber();
                String invType = OpTO.getInvType();
                String supTyp = OpTO.getSupTyp();
                String billingState = OpTO.getBillingState();
                String billingPin = OpTO.getBillingPin();
                String billingLocation = OpTO.getBillingLocation();
                String billlingAddress = OpTO.getBillingAddress();
                String billingLegalName = OpTO.getBillingLegalName();
                String billingTradeName = OpTO.getBillingTradeName();
                String billingGstin = OpTO.getBillingGstin();
                String grossAmount = OpTO.getGrossAmount();
                String cgst_header = OpTO.getCgst();
                String sgst_header = OpTO.getSgst();
                String igst_header = OpTO.getIgst();
                String cessValue = OpTO.getCessValue();
                String statecessValue = OpTO.getStatecessValue();
                String roundoff = OpTO.getRoundoff();
                String totalNetAmount = OpTO.getTotalNetAmount();
                String version = OpTO.getVersion();
                String igstOnIntra = OpTO.getIgstOnIntra();
                String regRev = OpTO.getRegRev();
                String taxsch = OpTO.getTaxSch();
                String invoiceNo = OpTO.getInvoiceNo();
                String invoiceDate = OpTO.getInvoiceDate();

                ArrayList getItemDetails = new ArrayList();
                getItemDetails = reportDAO.getCreditSuppItemDetails(invoiceId, statusId);
                //System.out.println("getItemDetails :" + getItemDetails.size());

                String itemList = "";

                Iterator itr11 = getItemDetails.iterator();
                String serialNo = "";
                int cnt = 1;
                while (itr11.hasNext()) {
                    OpTO1 = (OperationTO) itr11.next();

                    String slno = OpTO1.getSnos();
                    String isservc = OpTO1.getServiceTypeName();
                    String hsnCd = OpTO1.getHsnCd();
                    String qty = OpTO1.getQty();
                    String units = OpTO1.getUnit();
                    String unitPrice = OpTO1.getUnitPrice();
                    String totItemPrice = OpTO1.getItemPrice();
                    String itemAssAmt = OpTO1.getItemAssAount();
                    String igst_percent = OpTO1.getIgstPercent();
                    double i = Double.parseDouble(igst_percent);
                    String sgst_percent = OpTO1.getSgstPercent();
                    double s = Double.parseDouble(sgst_percent);
                    String igstAmt = OpTO1.getTotalIgst();
                    String cgstAmt = OpTO1.getTotalCgst();
                    String sgstAmt = OpTO1.getTotalSgst();
                    String totItemVal = OpTO1.getTotalamount();
                    String GstRt = "";
                    String productDesc = OpTO1.getProductDesc();
                    if (i > 0) {
                        GstRt = igst_percent;
                    } else {
                        GstRt = sgst_percent;
                    }

                    if (cnt == 1) {
                        itemList = "{"
                                + "\"SlNo\":\"" + slno + "\","
                                + "\"IsServc\":\"" + isservc + "\","
                                + "\"HsnCd\":\"" + hsnCd + "\","
                                + "\"Qty\":\"" + qty + "\","
                                + "\"Unit\":\"" + units + "\","
                                + "\"UnitPrice\":" + unitPrice + ","
                                + "\"TotAmt\":" + totItemPrice + ","
                                + "\"AssAmt\":" + itemAssAmt + ","
                                + "\"GstRt\":" + GstRt + ","
                                + "\"SgstAmt\":" + sgstAmt + ","
                                + "\"IgstAmt\":" + igstAmt + ","
                                + "\"CgstAmt\":" + cgstAmt + ","
                                + "\"TotItemVal\":" + totItemVal + ","
                                + "\"PrdDesc\":\"" + productDesc + "\"}";
                    } else {
                        itemList += ",{"
                                + "\"SlNo\":\"" + slno + "\","
                                + "\"IsServc\":\"" + isservc + "\","
                                + "\"HsnCd\":\"" + hsnCd + "\","
                                + "\"Qty\":\"" + qty + "\","
                                + "\"Unit\":\"" + units + "\","
                                + "\"UnitPrice\":" + unitPrice + ","
                                + "\"TotAmt\":" + totItemPrice + ","
                                + "\"AssAmt\":" + itemAssAmt + ","
                                + "\"GstRt\":" + GstRt + ","
                                + "\"SgstAmt\":" + sgstAmt + ","
                                + "\"IgstAmt\":" + igstAmt + ","
                                + "\"CgstAmt\":" + cgstAmt + ","
                                + "\"TotItemVal\":" + totItemVal + ","
                                + "\"PrdDesc\":\"" + productDesc + "\"}";
                    }
                    cnt++;

                }

//            String reasonsforNondelivery = repTO.getReasonsforNondelivery();
//            String reasonsforMemberRefusal = repTO.getReasonsforMemberRefusal();
//LIVE url
                final String URLLink = "https://api-einv.cleartax.in/v2/eInvoice/generate";
//            final String user = ThrottleConstants.userHS;
//            final String password = ThrottleConstants.passHS;

                URL myurl = new URL(URLLink);
                HttpURLConnection connection = null;
                StringBuffer jsonString = new StringBuffer();
                String line;

                String payload = "[ {"
                        + "  \"transaction\": {"
                        + "    \"Version\": \"" + version + "\","
                        + "    \"TranDtls\": {"
                        + "      \"IgstOnIntra\": \"" + igstOnIntra + "\","
                        + "      \"RegRev\": \"" + regRev + "\","
                        + "      \"TaxSch\": \"" + taxsch + "\","
                        + "      \"SupTyp\": \"" + supTyp + "\""
                        + "    },"
                        + "    \"DocDtls\": {"
                        + "      \"Dt\":\"" + documentDate + "\","
                        + "      \"No\": \"" + documentNumber + "\","
                        + "      \"Typ\": \"" + invType + "\""
                        + "    },"
                        + "    \"SellerDtls\": {"
                        + "      \"Stcd\": \"06\","
                        + "      \"Pin\": \"131101\","
                        + "      \"Loc\": \"Sonepat\","
                        + "      \"Addr2\": \"billing@ict.in\","
                        + "      \"Addr1\": \"Panchi Gujran, Ganaur, Sonepat - 131101 Haryana. INDIA\","
                        + "      \"TrdNm\": \"INTERNATIONAL CARGO TERMINALS AND RAIL INFRASTRUCTURE PVT. LTD.\","
                        + "      \"LglNm\": \"INTERNATIONAL CARGO TERMINALS AND RAIL INFRASTRUCTURE PVT. LTD.\","
                        + "      \"Gstin\": \"06AACCB8054G1ZT\""
                        + "    },"
                        + "    \"BuyerDtls\": {"
                        + "      \"Pos\": \"" + billingState + "\","
                        + "      \"Stcd\": \"" + billingState + "\","
                        + "      \"Pin\": \"" + billingPin + "\","
                        + "      \"Loc\": \"" + billingLocation + "\","
                        + "      \"Addr1\": \"" + billlingAddress + "\","
                        + "      \"TrdNm\": \"" + billingTradeName + "\","
                        + "      \"LglNm\": \"" + billingLegalName + "\","
                        + "      \"Gstin\": \"" + billingGstin + "\""
                        + "    },"
                        + " \"ItemList\": [" + itemList + ""
                        + "],"
                        + "    \"ValDtls\": {"
                        + "      \"AssVal\":" + grossAmount + ","
                        + "      \"CgstVal\":" + cgst_header + ","
                        + "      \"SgstVal\": " + sgst_header + ","
                        + "      \"IgstVal\": " + igst_header + ","
                        + "      \"CesVal\": " + cessValue + ","
                        + "      \"StCesVal\": " + statecessValue + ","
                        + "      \"RndOffAmt\": " + roundoff + ","
                        + "      \"TotInvVal\": " + totalNetAmount + ""
                        + "    },"
                        + "    \"PrecDocDtls\": {"
                        + "      \"InvDt\":\"" + invoiceDate + "\","
                        + "      \"InvNo\":\"" + invoiceNo + "\""
                        + "    }"
                        + "  }"
                        + "} ]";

//            test
//            String ownerId = "5325f8c6-9765-4456-a6b4-f5818cf758c8";
//            String AuthToken = "1.f62be703-c2e1-4b7a-bf1e-156720b6a9f1_4204d3da2db983b54c1e8b056a784f2675b0be48add5e9dd7fd51248bb11109c";
//            String GSTIN  = "07AAFCD5862R007";
//            test
                String ownerId = ThrottleConstants.ownerId;
                String AuthToken = ThrottleConstants.tokenId;
                String GSTIN = ThrottleConstants.gstIn;

                //System.out.println("EcreditSuppinvoice ownerId===" + ownerId);
                //System.out.println("EcreditSuppinvoice AuthToken===" + AuthToken);
                //System.out.println("EcreditSuppinvoice GSTIN===" + GSTIN);

                //System.out.println("payload " + payload);

                connection = (HttpURLConnection) myurl.openConnection();
                connection.setDoOutput(true);

                connection.setRequestMethod("PUT");
                connection.setRequestProperty("Accept", "application/json");
                connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                connection.setRequestProperty("owner_id", ownerId);
                connection.setRequestProperty("gstin", GSTIN);
                connection.setRequestProperty("X-Cleartax-Auth-Token", AuthToken);
                OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
                writer.write(payload);
                writer.close();

                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while ((line = br.readLine()) != null) {
                    jsonString.append(line);
                }
                br.close();
                connection.disconnect();
//            //System.out.println("jsonString :" + jsonString);
                String response = jsonString.toString();
//            //System.out.println("response===="+response);

//                int updateApiResponse = reportDAO.updateCreditSuppApiResponse(payload, response, invType);
//                //System.out.println("updateApiResponse in dump==" + updateApiResponse);
                JSONParser parser = new JSONParser();
                Object obj1 = parser.parse(jsonString.toString());
//            //System.out.println("obj1===="+obj1);
                org.json.simple.JSONArray jsonarry = (org.json.simple.JSONArray) obj1;
                org.json.simple.JSONObject jsonobj = (org.json.simple.JSONObject) jsonarry.get(0);
//            //System.out.println("jsonobj======="+jsonobj);

                JSONObject gov_res = (JSONObject) jsonobj.get("govt_response");
                String Success = gov_res.get("Success").toString();
                //System.out.println("Success=======" + Success);

                if (Success.equals("Y")) {
                    String Irn = gov_res.get("Irn").toString();
                    String SignedQRCode = gov_res.get("SignedQRCode").toString();
                    //System.out.println("Irn=======" + Irn);
                    //System.out.println("SignedQRCode=======" + SignedQRCode);

                    int updateUploadEInvoice = reportDAO.updateUploadECreditSuppInvoice(invoiceId, Irn, SignedQRCode);
                    //System.out.println("updateUploadEInvoice==" + updateUploadEInvoice);

                } else {
                    JSONArray error_details = (JSONArray) gov_res.get("ErrorDetails");
                    String error = error_details.toString();
                    //System.out.println("error=========" + error);
                    int updateUploadEInvoice = reportDAO.updateCreditSuppErrorStatus(invoiceId, error);
                    //System.out.println("updateCreditSuppErrorStatus--" + updateUploadEInvoice);
                }

            }
        } catch (Exception ex) {
//            //System.out.println("ESuppCreditinvoice exception caught---" + ex);
//            ex.printStackTrace();
        }
        return callEInvoice;
    }

    public ArrayList invoiceCommodityDetailsReport(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList invoiceDetailsReport = new ArrayList();
        invoiceDetailsReport = reportDAO.invoiceCommodityDetailsReport(reportTO, userId);
        return invoiceDetailsReport;
    }

//    customer role matrix
    public ArrayList getUserList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList userlist = new ArrayList();
        userlist = reportDAO.getUserList(reportTO);
        return userlist;
    }

    public ArrayList getGRNoLists(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getGRNoLists = new ArrayList();
        getGRNoLists = reportDAO.getGRNoLists(reportTO);
        return getGRNoLists;
    }

//    public ArrayList getInvoiceNo(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
//        ArrayList getInvoiceNo = new ArrayList();
//        getInvoiceNo = reportDAO.getInvoiceNo(reportTO);
//        return getInvoiceNo;
//    }
    public ArrayList getBillingParty(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList billingParty = new ArrayList();
        billingParty = reportDAO.getBillingParty(reportTO);
        return billingParty;
    }

    public int updateInvoice(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        int updateInvoice = 0;
        SqlMapClient session = reportDAO.getSqlMapClient();
        try {
            session.startTransaction();
            updateInvoice = reportDAO.updateInvoice(reportTO, session);
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                session.getSession().close();
            }
        }
        return updateInvoice;
    }

//    supplementary email
    public ArrayList getSuppInvoiceListEmail() throws FPRuntimeException, FPBusinessException {
        ArrayList invoiceReport = new ArrayList();
        invoiceReport = reportDAO.getSuppInvoiceListEmail();
        return invoiceReport;
    }

    public String createSuppInvoiceFile(String invId, String invoiceCode) throws DocumentException, BadElementException, IOException, com.itextpdf.text.DocumentException, GeneralSecurityException, Exception {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        String systemTime = sdf.format(c.getTime()).toString();
        //System.out.println("system date = " + systemTime);

        //System.out.println("inside getSupp InvPDFList..");

        String billingParty = "";
        String customerAddress = "";
        String panNo = "";
        String gstNo = "";
        String invoicecode = "";
        String billDate = "";
        String movementType = "";
        String containerTypeName = "";
        String billingType = "";
        String userName = "";
        String billingState = "";
        String[] tempRemarks = null;
        String recTo1 = "";
        String emailFormat = "";
        String qrcode = "";
        String irn = "";
        String workNo = "";

        try {

            ArrayList invoiceList = new ArrayList();
            invoiceList = getSuppInvoiceDetailsEmail(invId);
            //System.out.println("getSuppInvoiceDetailsEmail......" + invoiceList.size());

//            ArrayList invoiceSuppList = new ArrayList();
//            invoiceSuppList = getSuppInvoiceDetailsEmail(invId);
            Iterator itr;
            itr = invoiceList.iterator();
            ReportTO rep = null;

            Multipart multipart = new MimeMultipart();
            SendEmail em = new SendEmail();
            while (itr.hasNext()) {
                rep = new ReportTO();
                rep = (ReportTO) itr.next();
                String invoiceid = rep.getInvoiceId();
                billingParty = rep.getBillingParty();

                customerAddress = rep.getAddress();
                panNo = rep.getPanNo();
                gstNo = rep.getGstNo();
                invoicecode = rep.getInvoiceCode();
                billDate = rep.getBillDate();
                movementType = rep.getMovementType();
                containerTypeName = rep.getContainerType();
                billingType = rep.getBillingType();
                userName = rep.getUserName();
                billingState = rep.getBillingState();
                recTo1 = rep.getEmailId();
                qrcode = rep.getQrcode();
                irn = rep.getIrn();
                workNo = rep.getWorkNo();

                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                Document document = new Document();
                PdfWriter.getInstance(document, outputStream);
                //System.out.println("HI.pdf is called");

                document.open();

                PdfPTable table = new PdfPTable(2);
                PdfPTable innertable = new PdfPTable(1);
                PdfPTable innertable1 = new PdfPTable(1);

                PdfPTable innertable2 = new PdfPTable(12);
                innertable2.setWidths(new float[]{8, 8, 12, 8, 8, 8, 8, 8, 8, 8, 8, 8});
                PdfPTable innertable3 = new PdfPTable(1);
                PdfPTable innertable31 = new PdfPTable(1);
                PdfPTable innertable32 = new PdfPTable(1);
                PdfPTable innertableSign = new PdfPTable(1);
                PdfPTable innertable4 = new PdfPTable(1);
                PdfPTable innertable5 = new PdfPTable(1);
                PdfPTable innerHeadertable = new PdfPTable(3); ///ok

                innertableSign.setTotalWidth(new float[]{500});
                innertableSign.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertableSign.setLockedWidth(true);

                table.setTotalWidth(new float[]{150, 350});
                table.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.setLockedWidth(true);

                innerHeadertable.setTotalWidth(new float[]{300, 100, 100});
                innerHeadertable.setHorizontalAlignment(Element.ALIGN_LEFT);
                innerHeadertable.setLockedWidth(true);

                innertable.setTotalWidth(new float[]{500});
                innertable.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable.setLockedWidth(true);

                innertable1.setTotalWidth(new float[]{500});
                innertable1.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable1.setLockedWidth(true);

                innertable2.setTotalWidth(new float[]{40, 40, 60, 40, 40, 40, 40, 40, 40, 40, 40, 40});
                innertable2.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable2.setLockedWidth(true);

                innertable3.setTotalWidth(new float[]{500});
                innertable3.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable3.setLockedWidth(true);

                innertable31.setTotalWidth(new float[]{500});
                innertable31.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable31.setLockedWidth(true);

                innertable32.setTotalWidth(new float[]{500});
                innertable32.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable32.setLockedWidth(true);

                innertable4.setTotalWidth(new float[]{500});
                innertable4.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable4.setLockedWidth(true);
                innertable5.setTotalWidth(new float[]{500});
                innertable5.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable5.setLockedWidth(true);
//                Image img = Image.getInstance("/home/benjamin/Desktop/Project-amp/dict/web/images/dict-logo11.png");
//                Image img = Image.getInstance("C:/GIT-Projects/dict/web/images/dict-logo11.png");
                Image img = Image.getInstance("/var/lib/tomcat7/webapps/throttle/images/dict-logo11.png");

                img.scalePercent(30);
                PdfPCell headCell1 = new PdfPCell(new Phrase("Header", FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD, GrayColor.BLACK)));
                headCell1.setFixedHeight(40);

                headCell1.addElement(new Chunk(img, 10, -10));
                headCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell1.disableBorderSide(Rectangle.RIGHT);
                table.addCell(headCell1);

                headCell1 = new PdfPCell(new Phrase(" Delhi International Cargo Terminal Pvt.Ltd. \n Panchi Gujran,Tehsil-Ganaur, Dist-Sonepat-131101 \n CIN No:U63040MH2006PTC159885", FontFactory.getFont(FontFactory.HELVETICA, 8, Font.BOLD, GrayColor.BLACK)));
                headCell1.setFixedHeight(40);
                headCell1.disableBorderSide(4);

                headCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(headCell1);

                PdfPCell headCell = new PdfPCell(new Phrase("Some text here"));

                headCell = new PdfPCell(new Phrase("INVOICE", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell.setFixedHeight(20);
                headCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                innertable.addCell(headCell);

                //System.out.println("gstNo in pdf  gen:" + gstNo);
                PdfPCell headCell3 = new PdfPCell(new Phrase("Billed to :\n" + billingParty + "\n" + customerAddress + "\nBilling State:" + billingState + "\nPAN No:" + panNo + "\nGST No:" + gstNo, FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
                headCell3.setFixedHeight(60);

                //         headCell3.disableBorderSide(4);
                headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                innerHeadertable.addCell(headCell3);
                //System.out.println("supp invoice code inside pdf generation ::" + invoicecode);
                if (!"".equals(irn) && irn != null) {
                    headCell3 = new PdfPCell(new Phrase("Invoice No:" + invoicecode + "\n\n " + "IRN :" + irn + "\n\n" + billingType + "\n\n ", FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
                } else {
                    headCell3 = new PdfPCell(new Phrase("Invoice No:" + invoicecode + "\n\n" + billingType + "\n\n ", FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
                }
                headCell3.setFixedHeight(40);

                headCell3.disableBorderSide(4);
                headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                innerHeadertable.addCell(headCell3);

                headCell3 = new PdfPCell(new Phrase("Bill Date: " + billDate + "\n \n Dispatched Through:: \n \n \n SAC Code: 996511 \n\n", FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
                headCell3.setFixedHeight(40);

                headCell3.disableBorderSide(4);
                headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                innerHeadertable.addCell(headCell3);

                PdfPCell headCell4 = new PdfPCell(new Phrase("Some text here"));

                headCell4 = new PdfPCell(new Phrase("PARTICULARS", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell4.setFixedHeight(20);
                headCell4.setHorizontalAlignment(Element.ALIGN_CENTER);
                innertable1.addCell(headCell4);

                PdfPCell headCell5 = new PdfPCell(new Phrase("Some text here"));

                headCell5 = new PdfPCell(new Phrase("GR.No", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);

                headCell5 = new PdfPCell(new Phrase("Date", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);

                headCell5 = new PdfPCell(new Phrase("Description", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);

                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);
                headCell5 = new PdfPCell(new Phrase("No.Of Container ", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);
                headCell5 = new PdfPCell(new Phrase("Commodity Category  ", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);
                headCell5 = new PdfPCell(new Phrase("Commodity", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);
                headCell5 = new PdfPCell(new Phrase("Freight", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);
                headCell5 = new PdfPCell(new Phrase("Toll Tax", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);
                headCell5 = new PdfPCell(new Phrase("Detention Charges", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);
                headCell5 = new PdfPCell(new Phrase("Weightment", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);

                headCell5 = new PdfPCell(new Phrase("Other Charges", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);

                headCell5 = new PdfPCell(new Phrase("Total Amount(Rs.)", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);

                String totalFreightAmount = "";
                Float totalAmount = 0.0f;
                int totalContainerQty = 0;

                ArrayList invoiceDetailsList = new ArrayList();
                invoiceDetailsList = getEmailSuppInvoiceDetails(invoiceid);
                //System.out.println("getEmailSuppInvoiceDetails.size()@@@@@@" + invoiceDetailsList.size());
                Iterator itr1;
                itr1 = invoiceDetailsList.iterator();
                ReportTO repDet = null;

                while (itr1.hasNext()) {
                    repDet = new ReportTO();
                    repDet = (ReportTO) itr1.next();

                    PdfPCell headCell6 = new PdfPCell(new Phrase("Some text here"));

                    headCell6 = new PdfPCell(new Phrase(repDet.getGrNo(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getGrDate(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getDestination(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(30);
                    headCell6.setNoWrap(false);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getContainerQty(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    totalContainerQty = Integer.parseInt(repDet.getContainerQty()) + totalContainerQty;
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getArticleCode(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getArticleName(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getFreightAmount(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getTollTax(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getDetaintionAmount(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell3.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getWeightMent(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getOtherExpense(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    totalFreightAmount = Float.parseFloat(repDet.getFreightAmount()) + Float.parseFloat(repDet.getTollTax()) + Float.parseFloat(repDet.getDetaintionAmount())
                            + Float.parseFloat(repDet.getWeightMent()) + Float.parseFloat(repDet.getOtherExpense()) + "";
                    totalAmount = Float.parseFloat(totalFreightAmount) + totalAmount;

                    headCell6 = new PdfPCell(new Phrase(totalFreightAmount, FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);
                    //System.out.println("inside details table");
                }

                //System.out.println(" i am in outeeeerrrrrrr parrrrrtttt");

                PdfPCell headCell7 = new PdfPCell(new Phrase("Some text here"));

                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);

                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);

                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);
                headCell7 = new PdfPCell(new Phrase(totalContainerQty + "", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);

                innertable2.addCell(headCell7);
                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);
                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);

                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);

                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);
                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell3.disableBorderSide(2);
                innertable2.addCell(headCell7);
                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);
                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);

                headCell7 = new PdfPCell(new Phrase(totalAmount + "", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);

                String RUPEES = "";
                NumberWords numberWords = new NumberWords();
                numberWords.setRoundedValue(String.valueOf(java.lang.Math.ceil(totalAmount)));
                numberWords.setNumberInWords(String.valueOf(java.lang.Math.ceil(totalAmount)));
                RUPEES = numberWords.getNumberInWords();

                PdfPCell headCell8 = new PdfPCell(new Phrase("Some text here"));

                headCell8 = new PdfPCell(new Phrase("Amount Chargeable(In words)\n" + "Rs." + RUPEES + "\n\n" + "Remarks\n" + "" + "Being Road Transportation Charges Of " + containerTypeName + " " + movementType + "\n\n"
                        + "" + " Declaration\n"
                        + "1.All diputes are subject to Delhi Jurisdition\n"
                        + "2. Input credit on inputs, capital goods and input services, used for providing the taxable service, has not been taken.\n"
                        + "3. PAN No.AACCB8054G.\n"
                        + "4. GST Ref No.06AACCB8054G1ZT.\n"
                        + "5. Tax is payable under RCM.\n"
                        + "6. We declare that GST input credit on inputs, capital goods and input services used for providing the subject taxable service has not been taken by us under the provisions of the Input tax credit rules of GST. (Notification No.11/2017-Central Tax-rate dt. 28th June 2017 and Notification No.11/2017-State Tax -rate dt. 29th June 2017 ).\n"
                        + "7. In case of any discrepancy in this invoice, kindly intimate within 7 days from receipt or else it will be deemed to be accepted.\n" + "\n"
                        + " IN FAVOUR :-  Delhi International Cargo Terminal Pvt. Ltd.\n"
                        + " BANKER    :-  YES BANK\n"
                        + " BRANCH    :-  Ground Floor, Sheela Shoppee Sanjay Chowk , Panipat - 132103\n"
                        + " A/C NO    :-  007081400000021\n"
                        + " IFSC CODE :-  YESB0000070"
                        + "                                                                                                                                     For International Cargo Terminal And Rail Infrastructure Pvt.Ltd.\n\n", FontFactory.getFont(FontFactory.HELVETICA, 6)));

                headCell8.setFixedHeight(165);
                //System.out.println("i m here 2");
                headCell8.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell8.disableBorderSide(Rectangle.BOTTOM);
                innertable3.addCell(headCell8);

                GenerateQrCode qr = new GenerateQrCode();
                qr.setPathQRCode(invoicecode, qrcode);
//Image img = Image.getInstance("/home/benjamin/Desktop/Project-amp/dict/web/images/dict-logo11.png");
//                Image signimg = Image.getInstance("/home/benjamin/Desktop/Project-amp/dict/web/images/authsign.jpg");
                Image signimg = Image.getInstance("/var/lib/tomcat7/webapps/throttle/images/authsign.jpg");
//                Image signimg = Image.getInstance("C:/GIT-Projects/dict/web/images/authsign.jpg");
                signimg.scalePercent(30);

                PdfPCell headCellimg = new PdfPCell(new Phrase(""));
                headCellimg.setFixedHeight(30);

                headCellimg.addElement(new Chunk(signimg, 400, -8));
                headCellimg.setHorizontalAlignment(Element.ALIGN_RIGHT);
                headCellimg.disableBorderSide(Rectangle.RIGHT);
                headCellimg.setBorder(Rectangle.TOP);
                headCellimg.setBorder(Rectangle.NO_BORDER);
                innertable31.addCell(headCellimg);

                if (!"".equals(irn) && irn != null) {
                    Image signimg2 = Image.getInstance("/var/lib/tomcat7/webapps/Qrimages/QR" + invoiceCode + ".png");
//                    Image signimg2 = Image.getInstance("C:/GIT-Projects/dict/Qrimages/QR" + invoiceCode + ".png");
//                   Image signimg2 = Image.getInstance("/home/benjamin/Desktop/Project-amp/dict/Qrimages/QR" + invoicecode + ".png");

                    signimg2.scalePercent(20);
//                    signimg2.scaleToFit(300, 100);

                    PdfPCell headCellimg2 = new PdfPCell(new Phrase(""));
                    headCellimg2.setFixedHeight(0);

                    headCellimg2.addElement(new Chunk(signimg2, 10, 1));

                    headCellimg2.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCellimg2.disableBorderSide(Rectangle.LEFT);
                    headCellimg2.setBorder(Rectangle.BOTTOM);
                    headCellimg2.setBorder(Rectangle.NO_BORDER);
                    innertable31.addCell(headCellimg2);
                }

                PdfPCell headCell10 = new PdfPCell(new Phrase("Some text here"));
                headCell10 = new PdfPCell(new Phrase("                                                                                                                                                                                                                                               " + "Authorised Signature\n", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell10.setFixedHeight(20);
                headCell10.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell10.disableBorderSide(Rectangle.BOTTOM);
                headCell10.setBorder(Rectangle.TOP);
                headCell10.setBorder(Rectangle.NO_BORDER);
                innertable32.addCell(headCell10);

                PdfPCell headCell9 = new PdfPCell(new Phrase("Some text here"));
                headCell9 = new PdfPCell(new Phrase("Regd.Office: Godrej Coliseum,Office No. 801,8th Floor,C-Wing,Behind Everard Nagar,off Somaiya Hospital Road,Sion (East)Mumbai-400022 ", FontFactory.getFont(FontFactory.HELVETICA, 5)));
                headCell9.setFixedHeight(20);
                headCell9.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell9.setBorder(Rectangle.NO_BORDER);
                headCell9.setBorder(Rectangle.ALIGN_BOTTOM);
                innertable4.addCell(headCell9);

                document.add(table);
                document.add(innertable);
                document.add(innerHeadertable);
                document.add(innertable1);
                document.add(innertable2);
                document.add(innertable3);
                document.add(innertable31);
                document.add(innertable32);
                document.add(innertable4);
                document.close();

                multipart = em.addMultipleAttachments(movementType + "-" + invoicecode + ".pdf", outputStream, multipart);

            }

            //System.out.println("pdf created successfully..");
            emailFormat = movementType + "-" + invoicecode + ".pdf";

        } catch (Exception e) {
            e.printStackTrace();
            //System.out.println("error in pdf inv:" + e);

        }

        return emailFormat;
    }

    public ArrayList getSuppInvoiceDetailsEmail(String invId) throws FPRuntimeException, FPBusinessException {
        ArrayList suppInvoiceReport = new ArrayList();
        suppInvoiceReport = reportDAO.getSuppInvoiceDetailsEmail(invId);
        return suppInvoiceReport;
    }

    public int insertSuppInvoiceFileDetails(ReportTO reportTO, SqlMapClient session) {
        int insertInvoiceFileDetails = 0;
//        insertInvoiceFileDetails = reportDAO.insertInvoiceFileDetails(reportTO, session);
        insertInvoiceFileDetails = reportDAO.insertSuppInvoiceFileDetails(reportTO, session);
        return insertInvoiceFileDetails;
    }

    public int updateSuppInvoiceCustomerMail(ReportTO reportTO) {
        int updateSecondaryCustomerMail = 0;
        //System.out.println("Report BP innn");
        updateSecondaryCustomerMail = reportDAO.updateSuppInvoiceCustomerMail(reportTO);
        return updateSecondaryCustomerMail;
    }

    public ArrayList getSuppCustomerListForEmail(String customerId) throws FPRuntimeException, FPBusinessException {
        ArrayList customerList = new ArrayList();
        customerList = reportDAO.getSuppCustomerListForEmail(customerId);
        return customerList;
    }

    public int updateSuppInvoiceCustomerSendMail(ReportTO reportTO) {
        int updateSecondaryCustomerMail = 0;
        //System.out.println("Report BP innn");
        updateSecondaryCustomerMail = reportDAO.updateSuppInvoiceCustomerSendMail(reportTO);
        return updateSecondaryCustomerMail;
    }

    public ArrayList getEmailSuppInvoiceDetails(String invoiceId) throws FPRuntimeException, FPBusinessException {
        ArrayList invoiceReport = new ArrayList();
        invoiceReport = reportDAO.getEmailSuppInvoiceDetails(invoiceId);
        return invoiceReport;
    }

    public ArrayList getExpenseNameList() throws FPBusinessException, FPRuntimeException {
        ArrayList expenseNameList = new ArrayList();
        expenseNameList = reportDAO.getExpenseNameList();

        return expenseNameList;
    }

    public int updateExpense(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        int updateStatus = 0;
        SqlMapClient session = reportDAO.getSqlMapClient();
        try {
            session.startTransaction();
            updateStatus = reportDAO.updateExpense(reportTO, userId, session);
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                session.getSession().close();
            }
        }
        return updateStatus;
    }

//    billing party change
    public ArrayList getNonInvoiceGrNo(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getNonInvoiceGrNo = new ArrayList();
        getNonInvoiceGrNo = reportDAO.getNonInvoiceGrNo(reportTO);
        return getNonInvoiceGrNo;
    }

    public int billingPartyChange(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        int billingPartyChange = 0;
        SqlMapClient session = reportDAO.getSqlMapClient();
        try {
            session.startTransaction();

            billingPartyChange = reportDAO.billingPartyChange(reportTO, session);
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                session.getSession().close();
            }
        }

        return billingPartyChange;
    }

    public ArrayList billPartyChangeDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList billPartyChangeDetails = new ArrayList();
        billPartyChangeDetails = reportDAO.billPartyChangeDetails(reportTO);
        return billPartyChangeDetails;
    }

    public ArrayList getVehicleTripDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getVehicleTripDetails = new ArrayList();
        getVehicleTripDetails = reportDAO.getVehicleTripDetails(reportTO);
        return getVehicleTripDetails;
    }

    public ArrayList getInvoiceNo(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getInvoiceNo = new ArrayList();
        getInvoiceNo = reportDAO.getInvoiceNo(reportTO);
        return getInvoiceNo;
    }

    public int creditUpdate(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        int creditUpdate = 0;
        SqlMapClient session = reportDAO.getSqlMapClient();
        try {
            session.startTransaction();

            creditUpdate = reportDAO.creditUpdate(reportTO, session);
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                session.getSession().close();
            }
        }

        return creditUpdate;
    }

    public ArrayList invoicecreditList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList invoicecreditList = new ArrayList();
        invoicecreditList = reportDAO.invoicecreditList(reportTO);
        return invoicecreditList;
    }

    public int shippingBillNoUpdate() throws FPRuntimeException, FPBusinessException {
        //System.out.println("shippingLineNoList-----------");
        int shippingBillNoUpdate = reportDAO.shippingBillNoUpdate();
        return shippingBillNoUpdate;
    }

    public ArrayList getInvoiceList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList getInvoiceforFtp = new ArrayList();
        getInvoiceforFtp = reportDAO.getInvoiceforFtp(reportTO, userId);
        double freightAmount = 0.0;
        DprTO dprTO = new DprTO();
        ReportTO rpTO = new ReportTO();
        Iterator itr1 = getInvoiceforFtp.iterator();
        ReportTO repTO1 = new ReportTO();
        while (itr1.hasNext()) {
            rpTO = new ReportTO();
            repTO1 = (ReportTO) itr1.next();
            getInvoiceXMLData(repTO1.getInvoiceId());
            //System.out.println("repTO1.getInvoiceId()repTO1.getInvoiceId()" + repTO1.getInvoiceId());
            int status = reportDAO.updateFlagInvoice(repTO1.getInvoiceId());
            //System.out.println("updatestatus" + status);
        }
        return getInvoiceforFtp;
    }

    public int getInvoiceXMLData(String InvoiceId) {

        //System.out.println("i m in XML controller method");
//        final String endpointString = "http://dict.throttletms.com/throttlews/ThrottleServiceManager?wsdl";
        final String endpointString = "http://localhost:8084/throttlews/ThrottleServiceManager?wsdl";
        final String namespace = "http://web.ws.batch.to.ets.com/";
        String endpoint = endpointString;
        String returnValue = "";
        String fromDate = "";
        String toDate = "";
        String invoiceId = "";
        String userName = "throttleapidemo";
        String pwd = "admin123";
        fromDate = "";
        toDate = "";
        invoiceId = InvoiceId;
        int i = 1;
        try {

            Service service = new Service();
            Call call = (Call) service.createCall();
            call.setTargetEndpointAddress(new java.net.URL(endpoint));
            SOAPEnvelope env = new SOAPEnvelope();
            SOAPBodyElement element = new SOAPBodyElement(namespace, "getInvoiceList");
            element.setPrefix("web");
            element.addChildElement("fromDate", null).addTextNode(fromDate);
            element.addChildElement("toDate", null).addTextNode(toDate);
            element.addChildElement("invoiceId", null).addTextNode(invoiceId);
            element.addChildElement("userName", null).addTextNode(userName);
            element.addChildElement("password", null).addTextNode(pwd);
            env.addBodyElement(element);
            //System.out.println("input:" + env.getBody().toString());
            SOAPEnvelope envelope = call.invoke(env);
            returnValue = envelope.getBody().toString();
            //System.out.println("returnValue:" + returnValue);

//            PrintWriter out = response.getWriter();
            String actualServerFilePath = "", tempServerFilePath = "";
            actualServerFilePath = "C:\\TEST";
            //System.out.println("Server Path == " + actualServerFilePath);
            tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
            File file = new File(actualServerFilePath + "\\" + invoiceId + "Invoice_" + fromDate + "_to_" + toDate + ".xml");
            boolean bool = file.createNewFile();
            //System.out.println("file created .." + bool);
            FileWriter fileWriter = new FileWriter(file);
            String fileContent = "";
            fileWriter.write(returnValue);
            //fileWriter.write("a test");
            fileWriter.flush();
            fileWriter.close();
            //String filename = "Invoice_"+fromDate+"_to_"+toDate+".xml";
            String filename = invoiceId + "Invoice_" + fromDate + "_to_" + toDate + ".xml";
            String filepath = actualServerFilePath + "\\";

//            out.close();
//            file.delete();
        } catch (Exception e) {

        }
        return i;

    }

    // CREDIT NOTE TABLES
    public ArrayList getInvoiceListCredit(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList getInvoiceforFtpCredit = new ArrayList();
        getInvoiceforFtpCredit = reportDAO.getInvoiceforFtpCredit(reportTO, userId);
        double freightAmount = 0.0;
        DprTO dprTO = new DprTO();
        ReportTO rpTO = new ReportTO();
        Iterator itr1 = getInvoiceforFtpCredit.iterator();
        ReportTO repTO1 = new ReportTO();
        while (itr1.hasNext()) {
            rpTO = new ReportTO();
            repTO1 = (ReportTO) itr1.next();
            getInvoiceXMLDataCredit(repTO1.getCreditNoteId());
            //System.out.println("repTO1.getInvoiceId()repTO1.getInvoiceId()" + repTO1.getInvoiceId());
            int status = reportDAO.updateFlagInvoiceCredit(repTO1.getInvoiceId());
            //System.out.println("updatestatus" + status);
        }
        return getInvoiceforFtpCredit;
    }

    public int getInvoiceXMLDataCredit(String creditNoteId) {

        //System.out.println("i m in XML controller method");
//        final String endpointString = "http://dict.throttletms.com/throttlews/ThrottleServiceManager?wsdl";
        final String endpointString = "http://localhost:8084/throttlews/ThrottleServiceManager?wsdl";
        final String namespace = "http://web.ws.batch.to.ets.com/";
        String endpoint = endpointString;
        String returnValue = "";
        String fromDate = "";
        String toDate = "";
        String invoiceId = "";
        String userName = "throttleapidemo";
        String pwd = "admin123";
        fromDate = "";
        toDate = "";
        // invoiceId = creditNoteId;
        int i = 1;
        try {

            Service service = new Service();
            Call call = (Call) service.createCall();
            call.setTargetEndpointAddress(new java.net.URL(endpoint));
            SOAPEnvelope env = new SOAPEnvelope();
            SOAPBodyElement element = new SOAPBodyElement(namespace, "getInvoiceList");
            element.setPrefix("web");
            element.addChildElement("fromDate", null).addTextNode(fromDate);
            element.addChildElement("toDate", null).addTextNode(toDate);
            element.addChildElement("creditNoteId", null).addTextNode(creditNoteId);
            element.addChildElement("userName", null).addTextNode(userName);
            element.addChildElement("password", null).addTextNode(pwd);
            env.addBodyElement(element);
            //System.out.println("input:" + env.getBody().toString());
            SOAPEnvelope envelope = call.invoke(env);
            returnValue = envelope.getBody().toString();
            //System.out.println("returnValue:" + returnValue);

//            PrintWriter out = response.getWriter();
            String actualServerFilePath = "", tempServerFilePath = "";
            actualServerFilePath = "C:\\TEST";
            //System.out.println("Server Path == " + actualServerFilePath);
            tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
            File file = new File(actualServerFilePath + "\\" + creditNoteId + "Invoice_" + fromDate + "_to_" + toDate + ".xml");
            boolean bool = file.createNewFile();
            //System.out.println("file created .." + bool);
            FileWriter fileWriter = new FileWriter(file);
            String fileContent = "";
            fileWriter.write(returnValue);
            //fileWriter.write("a test");
            fileWriter.flush();
            fileWriter.close();
            //String filename = "Invoice_"+fromDate+"_to_"+toDate+".xml";
            String filename = creditNoteId + "Invoice_" + fromDate + "_to_" + toDate + ".xml";
            String filepath = actualServerFilePath + "\\";

//            out.close();
//            file.delete();
        } catch (Exception e) {

        }
        return i;

    }

    //Supplement invoice tables
    public ArrayList getSuppInvoiceforFtp(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList getSuppInvoiceforFtp = new ArrayList();
        getSuppInvoiceforFtp = reportDAO.getSuppInvoiceforFtp(reportTO, userId);
        double freightAmount = 0.0;
        DprTO dprTO = new DprTO();
        ReportTO rpTO = new ReportTO();
        Iterator itr1 = getSuppInvoiceforFtp.iterator();
        ReportTO repTO1 = new ReportTO();
        while (itr1.hasNext()) {
            rpTO = new ReportTO();
            repTO1 = (ReportTO) itr1.next();
            getSuppInvoiceXMLDataCredit(repTO1.getCreditNoteId());
            //System.out.println("repTO1.getInvoiceId()repTO1.getInvoiceId()" + repTO1.getInvoiceId());
            int status = reportDAO.updateFlagSuppInvoice(repTO1.getInvoiceId());
            //System.out.println("updatestatus" + status);
        }
        return getSuppInvoiceforFtp;
    }

    public int getSuppInvoiceXMLDataCredit(String creditNoteId) {

        //System.out.println("i m in XML controller method");
//        final String endpointString = "http://dict.throttletms.com/throttlews/ThrottleServiceManager?wsdl";
        final String endpointString = "http://localhost:8084/throttlews/ThrottleServiceManager?wsdl";
        final String namespace = "http://web.ws.batch.to.ets.com/";
        String endpoint = endpointString;
        String returnValue = "";
        String fromDate = "";
        String toDate = "";
        String invoiceId = "";
        String userName = "throttleapidemo";
        String pwd = "admin123";
        fromDate = "";
        toDate = "";
        // invoiceId = creditNoteId;
        int i = 1;
        try {

            Service service = new Service();
            Call call = (Call) service.createCall();
            call.setTargetEndpointAddress(new java.net.URL(endpoint));
            SOAPEnvelope env = new SOAPEnvelope();
            SOAPBodyElement element = new SOAPBodyElement(namespace, "getInvoiceList");
            element.setPrefix("web");
            element.addChildElement("fromDate", null).addTextNode(fromDate);
            element.addChildElement("toDate", null).addTextNode(toDate);
            element.addChildElement("creditNoteId", null).addTextNode(creditNoteId);
            element.addChildElement("userName", null).addTextNode(userName);
            element.addChildElement("password", null).addTextNode(pwd);
            env.addBodyElement(element);
            //System.out.println("input:" + env.getBody().toString());
            SOAPEnvelope envelope = call.invoke(env);
            returnValue = envelope.getBody().toString();
            //System.out.println("returnValue:" + returnValue);

//            PrintWriter out = response.getWriter();
            String actualServerFilePath = "", tempServerFilePath = "";
            actualServerFilePath = "C:\\TEST";
            //System.out.println("Server Path == " + actualServerFilePath);
            tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
            File file = new File(actualServerFilePath + "\\" + creditNoteId + "Invoice_" + fromDate + "_to_" + toDate + ".xml");
            boolean bool = file.createNewFile();
            //System.out.println("file created .." + bool);
            FileWriter fileWriter = new FileWriter(file);
            String fileContent = "";
            fileWriter.write(returnValue);
            //fileWriter.write("a test");
            fileWriter.flush();
            fileWriter.close();
            //String filename = "Invoice_"+fromDate+"_to_"+toDate+".xml";
            String filename = creditNoteId + "Invoice_" + fromDate + "_to_" + toDate + ".xml";
            String filepath = actualServerFilePath + "\\";

//            out.close();
//            file.delete();
        } catch (Exception e) {

        }
        return i;

    }
    // supp credit note tables

    public int getEinvoiceList() throws FPRuntimeException, FPBusinessException {
        int getEinvoiceList = 0;
        getEinvoiceList = reportDAO.getEinvoiceList();
        return getEinvoiceList;
    }

    public int getESuppinvoiceList() throws FPRuntimeException, FPBusinessException {
        int getESuppinvoiceList = 0;
        getESuppinvoiceList = reportDAO.getESuppinvoiceList();
        return getESuppinvoiceList;
    }

    public int getECreditinvoiceList() throws FPRuntimeException, FPBusinessException {
        int getECreditinvoiceList = 0;
        getECreditinvoiceList = reportDAO.getECreditinvoiceList();
        return getECreditinvoiceList;
    }

    public int getECreditSuppinvoiceList() throws FPRuntimeException, FPBusinessException {
        int getECreditSuppinvoiceList = 0;
        getECreditSuppinvoiceList = reportDAO.getECreditSuppinvoiceList();
        return getECreditSuppinvoiceList;
    }

    public int getAutoEmailList() throws FPRuntimeException, FPBusinessException {
        int getAutoEmailList = 0;
        getAutoEmailList = reportDAO.getAutoEmailList();
        return getAutoEmailList;
    }

    public ArrayList resendInvoiceList() throws FPRuntimeException, FPBusinessException {
        ArrayList resendInvoiceList = new ArrayList();
        resendInvoiceList = reportDAO.resendInvoiceList();
        return resendInvoiceList;
    }
    //resend invoice

    public String createResendInvoiceFile(String invId, String invoiceCode) throws DocumentException, BadElementException, IOException, com.itextpdf.text.DocumentException, GeneralSecurityException, Exception {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        String systemTime = sdf.format(c.getTime()).toString();
        //System.out.println("system date = " + systemTime);

        //System.out.println("inside getInvPDFList..");

        String billingParty = "";
        String customerAddress = "";
        String panNo = "";
        String gstNo = "";
        String invoicecode = "";
        String billDate = "";
        String movementType = "";
        String containerTypeName = "";
        String billingType = "";
        String userName = "";
        String billingState = "";
        String[] tempRemarks = null;
        String recTo1 = "";
        String emailFormat = "";
        String qrcode = "";
        String irn = "";

        try {

            ArrayList invoiceList = new ArrayList();
            invoiceList = getInvoiceDetailsEmail(invId);

            Iterator itr;
            itr = invoiceList.iterator();
            ReportTO rep = null;

            Multipart multipart = new MimeMultipart();
            SendEmail em = new SendEmail();
            while (itr.hasNext()) {
                rep = new ReportTO();
                rep = (ReportTO) itr.next();
                String invoiceid = rep.getInvoiceId();
                billingParty = rep.getBillingParty();

                customerAddress = rep.getAddress();
                panNo = rep.getPanNo();
                gstNo = rep.getGstNo();
                invoicecode = rep.getInvoiceCode();
                billDate = rep.getBillDate();
                movementType = rep.getMovementType();
                containerTypeName = rep.getContainerType();
                billingType = rep.getBillingType();
                userName = rep.getUserName();
                billingState = rep.getBillingState();
                recTo1 = rep.getEmailId();
                qrcode = rep.getQrcode();
                irn = rep.getIrn();

                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                Document document = new Document();
                PdfWriter.getInstance(document, outputStream);
                //System.out.println("HI.pdf is called");

                document.open();

                PdfPTable table = new PdfPTable(2);
                PdfPTable innertable = new PdfPTable(1);
                PdfPTable innertable1 = new PdfPTable(1);

                PdfPTable innertable2 = new PdfPTable(12);
                innertable2.setWidths(new float[]{8, 8, 12, 8, 8, 8, 8, 8, 8, 8, 8, 8});
                PdfPTable innertable3 = new PdfPTable(1);
                PdfPTable innertable31 = new PdfPTable(1);
                PdfPTable innertable32 = new PdfPTable(1);
                PdfPTable innertableSign = new PdfPTable(1);
                PdfPTable innertable4 = new PdfPTable(1);
                PdfPTable innertable5 = new PdfPTable(1);
                PdfPTable innerHeadertable = new PdfPTable(3); ///ok

                innertableSign.setTotalWidth(new float[]{500});
                innertableSign.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertableSign.setLockedWidth(true);

                table.setTotalWidth(new float[]{150, 350});
                table.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.setLockedWidth(true);

                innerHeadertable.setTotalWidth(new float[]{300, 100, 100});
                innerHeadertable.setHorizontalAlignment(Element.ALIGN_LEFT);
                innerHeadertable.setLockedWidth(true);

                innertable.setTotalWidth(new float[]{500});
                innertable.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable.setLockedWidth(true);

                innertable1.setTotalWidth(new float[]{500});
                innertable1.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable1.setLockedWidth(true);

                innertable2.setTotalWidth(new float[]{40, 40, 60, 40, 40, 40, 40, 40, 40, 40, 40, 40});
                innertable2.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable2.setLockedWidth(true);

                innertable3.setTotalWidth(new float[]{500});
                innertable3.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable3.setLockedWidth(true);

                innertable31.setTotalWidth(new float[]{500});
                innertable31.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable31.setLockedWidth(true);

                innertable32.setTotalWidth(new float[]{500});
                innertable32.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable32.setLockedWidth(true);

                innertable4.setTotalWidth(new float[]{500});
                innertable4.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable4.setLockedWidth(true);
                innertable5.setTotalWidth(new float[]{500});
                innertable5.setHorizontalAlignment(Element.ALIGN_LEFT);
                innertable5.setLockedWidth(true);

//                Image img = Image.getInstance("/home/benjamin/Desktop/Project-amp/dict/web/images/dict-logo11.png");
//                Image img = Image.getInstance("C:/GIT-Projects/dict/web/images/dict-logo11.png");
                Image img = Image.getInstance("/var/lib/tomcat7/webapps/throttle/images/dict-logo11.png");

                img.scalePercent(30);
                PdfPCell headCell1 = new PdfPCell(new Phrase("Header", FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD, GrayColor.BLACK)));
                headCell1.setFixedHeight(40);

                headCell1.addElement(new Chunk(img, 10, -10));
                headCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell1.disableBorderSide(Rectangle.RIGHT);
                table.addCell(headCell1);

                headCell1 = new PdfPCell(new Phrase("Delhi International Cargo Terminal Pvt.Ltd. \n Panchi Gujran,Tehsil-Ganaur, Dist-Sonepat-131101 \n CIN No:U63040MH2006PTC159885", FontFactory.getFont(FontFactory.HELVETICA, 8, Font.BOLD, GrayColor.BLACK)));
                headCell1.setFixedHeight(40);
                headCell1.disableBorderSide(4);

                headCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(headCell1);

                PdfPCell headCell = new PdfPCell(new Phrase("Some text here"));

                headCell = new PdfPCell(new Phrase("INVOICE", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell.setFixedHeight(20);
                headCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                innertable.addCell(headCell);

                //System.out.println("gstNo in pdf  gen:" + gstNo);
                PdfPCell headCell3 = new PdfPCell(new Phrase("Billed to :\n" + billingParty + "\n" + customerAddress + "\nBilling State:" + billingState + "\nPAN No:" + panNo + "\nGST No:" + gstNo, FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
                headCell3.setFixedHeight(60);

                //         headCell3.disableBorderSide(4);
                headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                innerHeadertable.addCell(headCell3);
                //System.out.println("invoice code inside pdf generation ::" + invoicecode);
                if (!"".equals(irn) && irn != null) {
                    headCell3 = new PdfPCell(new Phrase("Invoice No:" + invoicecode + "\n\n " + "IRN :" + irn + "\n\n" + billingType, FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
                } else {
                    headCell3 = new PdfPCell(new Phrase("Invoice No:" + invoicecode + "\n\n" + billingType, FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
                }
                headCell3.setFixedHeight(60);

                headCell3.disableBorderSide(4);
                headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                innerHeadertable.addCell(headCell3);

                headCell3 = new PdfPCell(new Phrase("Bill Date: " + billDate + "\n \n Dispatched Through:: \n \n \n SAC Code: 996511 \n\n", FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, GrayColor.BLACK)));
                headCell3.setFixedHeight(40);

                headCell3.disableBorderSide(4);
                headCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
                innerHeadertable.addCell(headCell3);

                PdfPCell headCell4 = new PdfPCell(new Phrase("Some text here"));

                headCell4 = new PdfPCell(new Phrase("PARTICULARS", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell4.setFixedHeight(20);
                headCell4.setHorizontalAlignment(Element.ALIGN_CENTER);
                innertable1.addCell(headCell4);

                PdfPCell headCell5 = new PdfPCell(new Phrase("Some text here"));

                headCell5 = new PdfPCell(new Phrase("GR.No", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);

                headCell5 = new PdfPCell(new Phrase("Date", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);

                headCell5 = new PdfPCell(new Phrase("Description", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);

                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);
                headCell5 = new PdfPCell(new Phrase("No.Of Container ", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);
                headCell5 = new PdfPCell(new Phrase("Commodity Category  ", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);
                headCell5 = new PdfPCell(new Phrase("Commodity", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);
                headCell5 = new PdfPCell(new Phrase("Freight", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);
                headCell5 = new PdfPCell(new Phrase("Toll Tax", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);
                headCell5 = new PdfPCell(new Phrase("Detention Charges", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);
                headCell5 = new PdfPCell(new Phrase("Weightment", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);

                headCell5 = new PdfPCell(new Phrase("Other Charges", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);

                headCell5 = new PdfPCell(new Phrase("Total Amount(Rs.)", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell5.setFixedHeight(20);
                headCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
                //                        headCell3.disableBorderSide(2);
                innertable2.addCell(headCell5);

                String totalFreightAmount = "";
                Float totalAmount = 0.0f;
                int totalContainerQty = 0;

                ArrayList invoiceDetailsList = new ArrayList();
                invoiceDetailsList = getEmailInvoiceDetails(invoiceid);

                Iterator itr1;
                itr1 = invoiceDetailsList.iterator();
                ReportTO repDet = null;

                while (itr1.hasNext()) {
                    repDet = new ReportTO();
                    repDet = (ReportTO) itr1.next();

                    PdfPCell headCell6 = new PdfPCell(new Phrase("Some text here"));

                    headCell6 = new PdfPCell(new Phrase(repDet.getGrNo(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getGrDate(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getDestination(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(30);
                    headCell6.setNoWrap(false);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getContainerQty(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    totalContainerQty = Integer.parseInt(repDet.getContainerQty()) + totalContainerQty;
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getArticleCode(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getArticleName(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getFreightAmount(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getTollTax(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getDetaintionAmount(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell3.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getWeightMent(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    headCell6 = new PdfPCell(new Phrase(repDet.getOtherExpense(), FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);

                    totalFreightAmount = Float.parseFloat(repDet.getFreightAmount()) + Float.parseFloat(repDet.getTollTax()) + Float.parseFloat(repDet.getDetaintionAmount())
                            + Float.parseFloat(repDet.getWeightMent()) + Float.parseFloat(repDet.getOtherExpense()) + "";
                    totalAmount = Float.parseFloat(totalFreightAmount) + totalAmount;

                    headCell6 = new PdfPCell(new Phrase(totalFreightAmount, FontFactory.getFont(FontFactory.HELVETICA, 6)));
                    headCell6.setFixedHeight(20);
                    headCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCell6.disableBorderSide(2);
                    innertable2.addCell(headCell6);
                }

                //System.out.println(" i am in outeeeerrrrrrr parrrrrtttt");

                PdfPCell headCell7 = new PdfPCell(new Phrase("Some text here"));

                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);

                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);

                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);
                headCell7 = new PdfPCell(new Phrase(totalContainerQty + "", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);

                innertable2.addCell(headCell7);
                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);
                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);

                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);

                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);
                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell3.disableBorderSide(2);
                innertable2.addCell(headCell7);
                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);
                headCell7 = new PdfPCell(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);

                headCell7 = new PdfPCell(new Phrase(totalAmount + "", FontFactory.getFont(FontFactory.HELVETICA, 6)));
                headCell7.setFixedHeight(20);
                headCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell7.disableBorderSide(2);
                innertable2.addCell(headCell7);

                String RUPEES = "";
                NumberWords numberWords = new NumberWords();
                numberWords.setRoundedValue(String.valueOf(java.lang.Math.ceil(totalAmount)));
                numberWords.setNumberInWords(String.valueOf(java.lang.Math.ceil(totalAmount)));
                RUPEES = numberWords.getNumberInWords();

                PdfPCell headCell8 = new PdfPCell(new Phrase("Some text here"));

                headCell8 = new PdfPCell(new Phrase("Amount Chargeable(In words)\n" + "Rs." + RUPEES + "\n\n" + "Remarks\n" + "" + "Being Road Transportation Charges Of " + containerTypeName + " " + movementType + "\n\n"
                        + "" + " Declaration\n"
                        + "1.All diputes are subject to Delhi Jurisdition\n"
                        + "2. Input credit on inputs, capital goods and input services, used for providing the taxable service , has not been taken.\n"
                        + "3. PAN No.AACCB8054G.\n"
                        + "4. GST Ref No.06AACCB8054G1ZT.\n"
                        + "5. Tax is payable under RCM.\n"
                        + "6. We declare that GST input credit on inputs, capital goods and input services used for providing the subject taxable service has not been taken by us under the provisions of the Input tax credit rules of GST. (Notification No.11/2017-Central Tax-rate dt. 28th June 2017 and Notification No.11/2017-State Tax -rate dt. 29th June 2017 ).\n"
                        + "7. In case of any discrepancy in this invoice, kindly intimate within 7 days from receipt or else it will be deemed to be accepted.\n" + "\n"
                        + " IN FAVOUR :-  Delhi International Cargo Terminal Pvt. Ltd.\n"
                        + " BANKER    :-  YES BANK\n"
                        + " BRANCH    :-  Ground Floor, Sheela Shoppee Sanjay Chowk , Panipat - 132103\n"
                        + " A/C NO    :-  007081400000021\n"
                        + " IFSC CODE :-  YESB0000070"
                        + "                                                                                                                                     For Delhi International Cargo Terminal Pvt.Ltd.\n\n", FontFactory.getFont(FontFactory.HELVETICA, 6)));

                headCell8.setFixedHeight(165);
                //System.out.println("i m here 2");
                headCell8.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell8.disableBorderSide(Rectangle.BOTTOM);
                innertable3.addCell(headCell8);

                GenerateQrCode qr = new GenerateQrCode();
                qr.setPathQRCode(invoicecode, qrcode);
//Image img = Image.getInstance("/home/benjamin/Desktop/Project-amp/dict/web/images/dict-logo11.png");

//                Image signimg = Image.getInstance("/var/lib/tomcat7/webapps/throttle/images/authsign.jpg");
                Image signimg = Image.getInstance("/var/lib/tomcat7/webapps/throttle/images/authsign.jpg");
//                Image signimg = Image.getInstance("C:/GIT-Projects/dict/web/images/authsign.jpg");
                signimg.scalePercent(30);

                PdfPCell headCellimg = new PdfPCell(new Phrase(""));
                headCellimg.setFixedHeight(30);

                headCellimg.addElement(new Chunk(signimg, 400, -8));

                headCellimg.setHorizontalAlignment(Element.ALIGN_RIGHT);
                headCellimg.disableBorderSide(Rectangle.RIGHT);
                headCellimg.setBorder(Rectangle.TOP);
                headCellimg.setBorder(Rectangle.NO_BORDER);
                innertable31.addCell(headCellimg);

                if (!"".equals(irn) && irn != null) {
                    Image signimg2 = Image.getInstance("/var/lib/tomcat7/webapps/Qrimages/QR" + invoiceCode + ".png");
//                    Image signimg2 = Image.getInstance("/home/benjamin/Desktop/Project-amp/dict/Qrimages/QR"+invoicecode+".png");
//                    Image signimg2 = Image.getInstance("C:/GIT-Projects/dict/Qrimages/QR" + invoiceCode + ".png");
                    signimg2.scalePercent(20);
//                    signimg2.scaleToFit(300, 100);/home/benjamin/Desktop/Project-amp/dict/web/images/dict-logo11.png

                    PdfPCell headCellimg2 = new PdfPCell(new Phrase(""));
                    headCellimg2.setFixedHeight(0);

                    headCellimg2.addElement(new Chunk(signimg2, 10, 1));

                    headCellimg2.setHorizontalAlignment(Element.ALIGN_LEFT);
                    headCellimg2.disableBorderSide(Rectangle.LEFT);
                    headCellimg2.setBorder(Rectangle.BOTTOM);
                    headCellimg2.setBorder(Rectangle.NO_BORDER);
                    innertable31.addCell(headCellimg2);
                }

                PdfPCell headCell9 = new PdfPCell(new Phrase("Some text here"));
                headCell9 = new PdfPCell(new Phrase("Regd.Office: Godrej Coliseum,Office No. 801,8th Floor,C-Wing,Behind Everard Nagar,off Somaiya Hospital Road,Sion (East)Mumbai-400022 ", FontFactory.getFont(FontFactory.HELVETICA, 5)));
                headCell9.setFixedHeight(20);
                headCell9.setHorizontalAlignment(Element.ALIGN_LEFT);
                headCell9.setBorder(Rectangle.NO_BORDER);
                headCell9.setBorder(Rectangle.ALIGN_BOTTOM);
                innertable4.addCell(headCell9);

                document.add(table);
                document.add(innertable);
                document.add(innerHeadertable);
                document.add(innertable1);
                document.add(innertable2);
                document.add(innertable3);
                document.add(innertable31);
                document.add(innertable32);
                document.add(innertable4);
                document.close();

                multipart = em.addMultipleAttachments(movementType + "-" + invoicecode + ".pdf", outputStream, multipart);

            }

            //System.out.println("pdf created successfully..." + invoicecode);
            emailFormat = movementType + "-" + invoicecode + ".pdf";
            //System.out.println("emailFormat---" + emailFormat);
        } catch (Exception e) {
            e.printStackTrace();
            //System.out.println("error in pdf inv:" + e);

        }

        return emailFormat;
    }

    public ArrayList getResendInvoiceListEmail(String fromDate, String toDate, String custId) throws FPRuntimeException, FPBusinessException {
        ArrayList getResendInvoiceListEmail = new ArrayList();
        getResendInvoiceListEmail = reportDAO.getResendInvoiceListEmail(fromDate, toDate, custId);
        return getResendInvoiceListEmail;
    }

    public int updateResendCustomerMail(String id) {
        int updateResendCustomerMail = 0;
        //System.out.println("Report BP innn");
        updateResendCustomerMail = reportDAO.updateResendCustomerMail(id);
        return updateResendCustomerMail;
    }

    public int updateInvoiceCustomerMail(ReportTO reportTO) {
        int updateInvoiceCustomerMail = 0;
        //System.out.println("Report BP innn");
        updateInvoiceCustomerMail = reportDAO.updateInvoiceCustomerMail(reportTO);
        return updateInvoiceCustomerMail;
    }

    public ArrayList getUniqueCustomerList() throws FPRuntimeException, FPBusinessException {
        ArrayList getUniqueCustomerList = new ArrayList();
        getUniqueCustomerList = reportDAO.getUniqueCustomerList();
        return getUniqueCustomerList;
    }

    public ArrayList getCustomerMasterLogReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList customerMasterLog = new ArrayList();
        customerMasterLog = reportDAO.getCustomerMasterLogReport(reportTO);
        return customerMasterLog;
    }

    public ArrayList uniqueSuppCustomerList() throws FPRuntimeException, FPBusinessException {
        ArrayList uniqueSuppCustomerList = new ArrayList();
        uniqueSuppCustomerList = reportDAO.uniqueSuppCustomerList();
        return uniqueSuppCustomerList;
    }

    public int getAutoEmailSuppList() throws FPRuntimeException, FPBusinessException {
        int getAutoEmailSuppList = 0;
        getAutoEmailSuppList = reportDAO.getAutoEmailSuppList();
        return getAutoEmailSuppList;
    }

    public ArrayList invoiceListForXml() throws FPRuntimeException, FPBusinessException {
        ArrayList invoiceListForXml = new ArrayList();
        invoiceListForXml = reportDAO.invoiceListForXml();
        return invoiceListForXml;
    }

    public ArrayList suppInvoiceListForXml() throws FPRuntimeException, FPBusinessException {
        ArrayList suppInvoiceListForXml = new ArrayList();
        suppInvoiceListForXml = reportDAO.suppInvoiceListForXml();
        return suppInvoiceListForXml;
    }

    public ArrayList creaditInvoiceListForXml() throws FPRuntimeException, FPBusinessException {
        ArrayList creaditInvoiceListForXml = new ArrayList();
        creaditInvoiceListForXml = reportDAO.creaditInvoiceListForXml();
        return creaditInvoiceListForXml;
    }

    public ArrayList suppCreaditInvoiceListForXml() throws FPRuntimeException, FPBusinessException {
        ArrayList suppCreaditInvoiceListForXml = new ArrayList();
        suppCreaditInvoiceListForXml = reportDAO.suppCreaditInvoiceListForXml();
        return suppCreaditInvoiceListForXml;
    }

    public String getInvoiceXMLData(String fromDate, String toDate, String invoiceId, int ii) throws ServletException, IOException {

        //System.out.println("i m in XML controller method");
        final String endpointString = "http://dict.throttletms.com/throttlews/ThrottleServiceManager?wsdl";
        final String namespace = "http://web.ws.batch.to.ets.com/";
        String endpoint = endpointString;
        String returnValue = "";
        String userName = "throttleapidemo";
        String pwd = "admin123";
        String status = "";

        try {

            ///select date_format(now(),'%d%m%y%h%i%s') from ts_trip_master limit 1
            String todayDate = reportDAO.getTodayDate(ii);
            Service service = new Service();
            Call call = (Call) service.createCall();
            call.setTargetEndpointAddress(new java.net.URL(endpoint));
            SOAPEnvelope env = new SOAPEnvelope();
            SOAPBodyElement element = new SOAPBodyElement(namespace, "getInvoiceList");
            element.setPrefix("web");
            element.addChildElement("fromDate", null).addTextNode(fromDate);
            element.addChildElement("toDate", null).addTextNode(toDate);
            element.addChildElement("invoiceId", null).addTextNode(invoiceId);
            element.addChildElement("userName", null).addTextNode(userName);
            element.addChildElement("password", null).addTextNode(pwd);
            env.addBodyElement(element);
            //System.out.println("input:" + env.getBody().toString());
            SOAPEnvelope envelope = call.invoke(env);
            returnValue = envelope.getBody().toString();
            //System.out.println("returnValue:" + returnValue);
            if (!"".equals(returnValue) && returnValue != null) {
                //  request.setAttribute("Param", true);
            }

            //DD MM YY HH MM I SS
            //get Dateqry from DB and set file name
            //select date_format(now(),'%d%m%y%h%i%s') from ts_trip_master limit 1
            String actualServerFilePath = "", tempServerFilePath = "";
//            actualServerFilePath = "C://FTP//XML//From//";
//            actualServerFilePath = "C://FTP//XML//From//";
            actualServerFilePath = "//var//lib//tomcat7//webapps//throttle//From";
            //System.out.println("Server Path == " + actualServerFilePath);
            tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
            File file = new File(actualServerFilePath + "//" + "TMS_" + todayDate + ".xml");
            String fileName = "TMS_" + todayDate + ".xml";
            boolean bool = file.createNewFile();
            //System.out.println("file created ..");
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(returnValue);
            fileWriter.flush();
            fileWriter.close();
            String type = "1";

            int invoiceLog = reportDAO.updateInvoiceLog(fileName, type, invoiceId);

            /// create new table with flag and update invoice table flag
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    public String getSuppInvoiceXMLData(String fromDate, String toDate, String invoiceId, int ii) throws ServletException, IOException {

        //System.out.println("i m in XML controller method");
        final String endpointString = "http://dict.throttletms.com/throttlews/ThrottleServiceManager?wsdl";
        final String namespace = "http://web.ws.batch.to.ets.com/";
        String endpoint = endpointString;
        String returnValue = "";
        String userName = "throttleapidemo";
        String pwd = "admin123";
        String status = "";

        try {

            ///select date_format(now(),'%d%m%y%h%i%s') from ts_trip_master limit 1
            String todayDate = reportDAO.getTodayDate(ii);
            Service service = new Service();
            Call call = (Call) service.createCall();
            call.setTargetEndpointAddress(new java.net.URL(endpoint));
            SOAPEnvelope env = new SOAPEnvelope();
            SOAPBodyElement element = new SOAPBodyElement(namespace, "getSupplementryInvoiceList");
            element.setPrefix("web");
            element.addChildElement("fromDate", null).addTextNode(fromDate);
            element.addChildElement("toDate", null).addTextNode(toDate);
            element.addChildElement("invoiceId", null).addTextNode(invoiceId);
            element.addChildElement("userName", null).addTextNode(userName);
            element.addChildElement("password", null).addTextNode(pwd);
            env.addBodyElement(element);
            //System.out.println("input:" + env.getBody().toString());
            SOAPEnvelope envelope = call.invoke(env);
            returnValue = envelope.getBody().toString();
            //System.out.println("returnValue:" + returnValue);
            if (!"".equals(returnValue) && returnValue != null) {
                //  request.setAttribute("Param", true);
            }

            //DD MM YY HH MM I SS
            //get Dateqry from DB and set file name
            //select date_format(now(),'%d%m%y%h%i%s') from ts_trip_master limit 1
            String actualServerFilePath = "", tempServerFilePath = "";
//            actualServerFilePath = "C://FTP//XML//From//";
            actualServerFilePath = "//var//lib//tomcat7//webapps//throttle//From";
            //System.out.println("Server Path == " + actualServerFilePath);
            tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
            File file = new File(actualServerFilePath + "//" + "TMSSUP_" + todayDate + ".xml");
            String fileName = "TMSSUP_" + todayDate + ".xml";
            boolean bool = file.createNewFile();
            //System.out.println("file created ..");
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(returnValue);
            fileWriter.flush();
            fileWriter.close();
            String type = "2";

            int invoiceLog = reportDAO.updateInvoiceLog(fileName, type, invoiceId);

            /// create new table with flag and update invoice table flag
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    public String getCreditInvoiceXMLData(String fromDate, String toDate, String invoiceId, int ii) throws ServletException, IOException {

        //System.out.println("i m in XML controller method");
        final String endpointString = "http://dict.throttletms.com/throttlews/ThrottleServiceManager?wsdl";
        final String namespace = "http://web.ws.batch.to.ets.com/";
        String endpoint = endpointString;
        String returnValue = "";
        String userName = "throttleapidemo";
        String pwd = "admin123";
        String status = "";

        try {

            ///select date_format(now(),'%d%m%y%h%i%s') from ts_trip_master limit 1
            String todayDate = reportDAO.getTodayDate(ii);
            Service service = new Service();
            Call call = (Call) service.createCall();
            call.setTargetEndpointAddress(new java.net.URL(endpoint));
            SOAPEnvelope env = new SOAPEnvelope();
            SOAPBodyElement element = new SOAPBodyElement(namespace, "getCreditNoteList");
            element.setPrefix("web");
            element.addChildElement("fromDate", null).addTextNode(fromDate);
            element.addChildElement("toDate", null).addTextNode(toDate);
            element.addChildElement("invoiceId", null).addTextNode(invoiceId);
            element.addChildElement("userName", null).addTextNode(userName);
            element.addChildElement("password", null).addTextNode(pwd);
            env.addBodyElement(element);
            //System.out.println("input:" + env.getBody().toString());
            SOAPEnvelope envelope = call.invoke(env);
            returnValue = envelope.getBody().toString();
            //System.out.println("returnValue:" + returnValue);
            if (!"".equals(returnValue) && returnValue != null) {
                //  request.setAttribute("Param", true);
            }

            //DD MM YY HH MM I SS
            //get Dateqry from DB and set file name
            //select date_format(now(),'%d%m%y%h%i%s') from ts_trip_master limit 1
            String actualServerFilePath = "", tempServerFilePath = "";
//            actualServerFilePath = "C://FTP//XML//From//";
            actualServerFilePath = "//var//lib//tomcat7//webapps//throttle//From";
            //System.out.println("Server Path == " + actualServerFilePath);
            tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
            File file = new File(actualServerFilePath + "//" + "TMSCN_" + todayDate + ".xml");
            String fileName = "TMSCN_" + todayDate + ".xml";
            boolean bool = file.createNewFile();
            //System.out.println("file created ..");
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(returnValue);
            fileWriter.flush();
            fileWriter.close();
            String type = "3";

            int invoiceLog = reportDAO.updateInvoiceLog(fileName, type, invoiceId);
            /// create new table with flag and update invoice table flag

        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    public String getSuppCreditInvoiceXMLData(String fromDate, String toDate, String invoiceId, int ii) throws ServletException, IOException {

        //System.out.println("i m in XML controller method");
        final String endpointString = "http://dict.throttletms.com/throttlews/ThrottleServiceManager?wsdl";
        final String namespace = "http://web.ws.batch.to.ets.com/";
        String endpoint = endpointString;
        String returnValue = "";
        String userName = "throttleapidemo";
        String pwd = "admin123";
        String status = "";

        try {

            ///select date_format(now(),'%d%m%y%h%i%s') from ts_trip_master limit 1
            String todayDate = reportDAO.getTodayDate(ii);
            Service service = new Service();
            Call call = (Call) service.createCall();
            call.setTargetEndpointAddress(new java.net.URL(endpoint));
            SOAPEnvelope env = new SOAPEnvelope();
            SOAPBodyElement element = new SOAPBodyElement(namespace, "getSuppCreditNoteList");
            element.setPrefix("web");
            element.addChildElement("fromDate", null).addTextNode(fromDate);
            element.addChildElement("toDate", null).addTextNode(toDate);
            element.addChildElement("invoiceId", null).addTextNode(invoiceId);
            element.addChildElement("userName", null).addTextNode(userName);
            element.addChildElement("password", null).addTextNode(pwd);
            env.addBodyElement(element);
            //System.out.println("input:" + env.getBody().toString());
            SOAPEnvelope envelope = call.invoke(env);
            returnValue = envelope.getBody().toString();
            //System.out.println("returnValue:" + returnValue);
            if (!"".equals(returnValue) && returnValue != null) {
                //  request.setAttribute("Param", true);
            }

            //DD MM YY HH MM I SS
            //get Dateqry from DB and set file name
            //select date_format(now(),'%d%m%y%h%i%s') from ts_trip_master limit 1
            String actualServerFilePath = "", tempServerFilePath = "";
//            actualServerFilePath = "C://FTP//XML//From//";
            actualServerFilePath = "//var//lib//tomcat7//webapps//throttle//From";
            //System.out.println("Server Path == " + actualServerFilePath);
            tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
            File file = new File(actualServerFilePath + "//" + "TMSSUPCN_" + todayDate + ".xml");
            String fileName = "TMSSUPCN_" + todayDate + ".xml";
            boolean bool = file.createNewFile();
            //System.out.println("file created ..");
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(returnValue);
            fileWriter.flush();
            fileWriter.close();
            String type = "4";

            int invoiceLog = reportDAO.updateInvoiceLog(fileName, type, invoiceId);

            /// create new table with flag and update invoice table flag
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    public ArrayList getInvoiceFileForMove() throws FPRuntimeException, FPBusinessException {
        ArrayList getInvoiceFileForMove = new ArrayList();
        getInvoiceFileForMove = reportDAO.getInvoiceFileForMove();
        return getInvoiceFileForMove;
    }

    public ArrayList suppInvoiceFileForMove() throws FPRuntimeException, FPBusinessException {
        ArrayList suppInvoiceFileForMove = new ArrayList();
        suppInvoiceFileForMove = reportDAO.suppInvoiceFileForMove();
        return suppInvoiceFileForMove;
    }

    public ArrayList creditInvoiceFileForMove() throws FPRuntimeException, FPBusinessException {
        ArrayList creditInvoiceFileForMove = new ArrayList();
        creditInvoiceFileForMove = reportDAO.creditInvoiceFileForMove();
        return creditInvoiceFileForMove;
    }

    public ArrayList creditSuppInvoiceFileForMove() throws FPRuntimeException, FPBusinessException {
        ArrayList creditSuppInvoiceFileForMove = new ArrayList();
        creditSuppInvoiceFileForMove = reportDAO.creditSuppInvoiceFileForMove();
        return creditSuppInvoiceFileForMove;
    }

    public String uploadInvToFtp(String id, String filename) {

        String ftpUrl = "ftp://%s:%s@%s/%s;type=i";
        String host = "117.239.10.237";
        String user = "sap";
        String pass = "sap321";
//        String filePath = "C:/FTP/XML/From/" + filename;
//        String movePath = "C://FTP//XML//To//" + filename;
        String filePath = "//var//lib//tomcat7//webapps//throttle//From//" + filename;
        String movePath = "//var//lib//tomcat7//webapps//throttle//To//" + filename;
        String uploadPath = "/PRD/TMS/IN/" + filename;

        String status = "";

        ftpUrl = String.format(ftpUrl, user, pass, host, uploadPath);
        // //System.out.println("Upload URL: " + ftpUrl);

        try {
            URL url = new URL(ftpUrl);
            URLConnection conn = url.openConnection();
            OutputStream outputStream = conn.getOutputStream();
            FileInputStream inputStream = new FileInputStream(filePath);

            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead = -1;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            inputStream.close();
            outputStream.close();

            Path temp = Files.move(Paths.get(filePath),
                    Paths.get(movePath));

            if (temp != null) {
                //System.out.println("File renamed and moved successfully");
            } else {
                //System.out.println("Failed to move the file");
            }
            String type = "1";
            int statusUpdate = reportDAO.updateStatusFtpMoveStatus(id, type);

            //System.out.println("File uploaded");
//
            ////////////////////////
//                String directoryPath = "//var//lib//tomcat7//webapps//throttle//CSV//FROM//";
           

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return status;
    }

    public String uploadSuppInvToFtp(String id, String filename) {

        String ftpUrl = "ftp://%s:%s@%s/%s;type=i";
        String host = "117.239.10.237";
        String user = "sap";
        String pass = "sap321";
//        String filePath = "C:/FTP/XML/From/" + filename;
//        String movePath = "C:/FTP/XML/To/" + filename;
        String filePath = "//var//lib//tomcat7//webapps//throttle//From//" + filename;
        String movePath = "//var//lib//tomcat7//webapps//throttle//To//" + filename;
        String uploadPath = "/PRD/TMSSUP/IN/" + filename;

        String status = "";

        ftpUrl = String.format(ftpUrl, user, pass, host, uploadPath);
        // //System.out.println("Upload URL: " + ftpUrl);

        try {
            URL url = new URL(ftpUrl);
            URLConnection conn = url.openConnection();
            OutputStream outputStream = conn.getOutputStream();
            FileInputStream inputStream = new FileInputStream(filePath);

            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead = -1;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            inputStream.close();
            outputStream.close();

            Path temp = Files.move(Paths.get(filePath),
                    Paths.get(movePath));

            if (temp != null) {
                //System.out.println("File renamed and moved successfully");
            } else {
                //System.out.println("Failed to move the file");
            }
            String type = "2";
            int statusUpdate = reportDAO.updateStatusFtpMoveStatus(id, type);
            //System.out.println("File uploaded");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return status;
    }

    public String uploadCreditInvToFtp(String id, String filename) {

        String ftpUrl = "ftp://%s:%s@%s/%s;type=i";
        String host = "117.239.10.237";
        String user = "sap";
        String pass = "sap321";
//        String filePath = "C:/FTP/XML/From/" + filename;
//        String movePath = "C:/FTP/XML/To/" + filename;
        String filePath = "//var//lib//tomcat7//webapps//throttle//From//" + filename;
        String movePath = "//var//lib//tomcat7//webapps//throttle//To//" + filename;
        String uploadPath = "/PRD/TMSCN/IN/" + filename;

        String status = "";

        ftpUrl = String.format(ftpUrl, user, pass, host, uploadPath);
        // //System.out.println("Upload URL: " + ftpUrl);

        try {
            URL url = new URL(ftpUrl);
            URLConnection conn = url.openConnection();
            OutputStream outputStream = conn.getOutputStream();
            FileInputStream inputStream = new FileInputStream(filePath);

            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead = -1;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            inputStream.close();
            outputStream.close();

            Path temp = Files.move(Paths.get(filePath),
                    Paths.get(movePath));

            if (temp != null) {
                //System.out.println("File renamed and moved successfully");
            } else {
                //System.out.println("Failed to move the file");
            }
            String type = "3";
            int statusUpdate = reportDAO.updateStatusFtpMoveStatus(id, type);

            //System.out.println("File uploaded");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return status;
    }

    public String uploadSuppCreditInvToFtp(String id, String filename) {

        String ftpUrl = "ftp://%s:%s@%s/%s;type=i";
        String host = "117.239.10.237";
        String user = "sap";
        String pass = "sap321";
//        String filePath = "C:/FTP/XML/From/" + filename;
//        String movePath = "C:/FTP/XML/To/" + filename;
        String filePath = "//var//lib//tomcat7//webapps//throttle//From//" + filename;
        String movePath = "//var//lib//tomcat7//webapps//throttle//To//" + filename;
        String uploadPath = "/PRD/TMSSUPCN/IN/" + filename;

        String status = "";

        ftpUrl = String.format(ftpUrl, user, pass, host, uploadPath);
        // //System.out.println("Upload URL: " + ftpUrl);

        try {
            URL url = new URL(ftpUrl);
            URLConnection conn = url.openConnection();
            OutputStream outputStream = conn.getOutputStream();
            FileInputStream inputStream = new FileInputStream(filePath);

            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead = -1;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            inputStream.close();
            outputStream.close();

            Path temp = Files.move(Paths.get(filePath),
                    Paths.get(movePath));

            if (temp != null) {
                //System.out.println("File renamed and moved successfully");
            } else {
                //System.out.println("Failed to move the file");
            }
            String type = "4";
            int statusUpdate = reportDAO.updateStatusFtpMoveStatus(id, type);

            //System.out.println("File uploaded");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return status;
    }

//    public String downloadInvoiceCsvFile() throws IOException {
//        String server = "117.239.10.237";
//        int port = 21;
//        String user = "sap";
//        String pass = "sap321";
//        String status = "";
//
////           String ftpUrl = "ftp://%s:%s@%s/%s;type=i";
////        String host = "117.239.10.237";
////        String user = "sap";
////        String pass = "sap321";
//        FTPClient ftpClient = new FTPClient();
//
//        try {
//            ftpClient.connect(server, port);
//            ftpClient.login(user, pass);
//            ftpClient.enterLocalPassiveMode();
//            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
//
//            // use local passive mode to pass firewall
//            ftpClient.enterLocalPassiveMode();
//
//            // get details of a file or directory
//            String remoteFilePath = "/PRD/TMS/IN/";
//
//            FTPFile ftpFile = ftpClient.mlistFile(remoteFilePath);
//            if (ftpFile != null) {
//
//                FTPFile[] files1 = ftpClient.listFiles(remoteFilePath);
//
//                for (FTPFile file : files1) {
//                    String details = file.getName();
//
//                    if (details.contains(".CSV")) {
//                        //System.out.println(details);
//
//                        String remoteFile1 = "/PRD/TMS/IN/" + details;
//                        String remoteFile2 = "/PRD/TMS/OUT/" + details;
//                       
//                        //System.out.println("File #1 has been downloaded successfully.");
//                            boolean success1 = ftpClient.rename(remoteFile1, remoteFile2);
//                            if (success1) {
//                                //System.out.println("success : "+success1);
//                            } else {
//                                //System.out.println("fail : "+success1);
//    // rename failed
//                            }
//
//                        boolean success = true;
//
//                        if (success) {
//
//                            InputStream iStream = ftpClient.retrieveFileStream(remoteFile2);
//                            BufferedInputStream bInf = new BufferedInputStream(iStream);
//                            int bytesRead;
//                            byte[] buffer = new byte[10000];
//                            String fileContent = null;
//
//                            while ((bytesRead = bInf.read(buffer)) != -1) {
//                                fileContent = new String(buffer, 0, bytesRead);
//                            }
//
//                            Scanner sc = new Scanner(fileContent);
//
//                            while (sc.hasNext()) {
//                                String n = sc.nextLine();
//
//                                String[] temp = n.split(",");
//
//                                if (!temp[0].equalsIgnoreCase("Invoice No")) {
//                                    String invoiceNo = temp[0];
//                                    String xmlStatus = temp[1];
//                                    String reMarks = temp[2];
//                                    //System.out.println("invoiceNo " + invoiceNo);
//                                    //System.out.println("xmlStatus " + xmlStatus);
//                                    //System.out.println("reMarks " + reMarks);
//                                    int updateInHeader = reportDAO.updateInHeaderCsvStatus(invoiceNo, xmlStatus, reMarks);
//                                }
//                            }
////                          
//                           
//                        }
//                    }
//                }
//
////                //System.out.println("Type: " + type);
//            } else {
//                //System.out.println("The specified file/directory may not exist!");
//            }
//
//            ftpClient.logout();
//            ftpClient.disconnect();
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        } finally {
//            if (ftpClient.isConnected()) {
//                try {
//                    ftpClient.disconnect();
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                }
//            }
//        }
//        return status;
//    }
    public String downloadSuppInvoiceCsvFile() {
        String server = "117.239.10.237";
        int port = 21;
        String user = "sap";
        String pass = "sap321";
        String status = "";

//           String ftpUrl = "ftp://%s:%s@%s/%s;type=i";
//        String host = "117.239.10.237";
//        String user = "sap";
//        String pass = "sap321";
        FTPClient ftpClient = new FTPClient();

        try {
            ftpClient.connect(server, port);
            ftpClient.login(user, pass);
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            // use local passive mode to pass firewall
            ftpClient.enterLocalPassiveMode();

            // get details of a file or directory
            String remoteFilePath = "/PRD/TMSSUP/IN/";

            FTPFile ftpFile = ftpClient.mlistFile(remoteFilePath);
            if (ftpFile != null) {

                FTPFile[] files1 = ftpClient.listFiles(remoteFilePath);

                for (FTPFile file : files1) {
                    String details = file.getName();

                    if (details.contains(".CSV")) {
                        //System.out.println(details);

                        String remoteFile1 = "/PRD/TMSSUP/IN/" + details;
                        String remoteFile2 = "/PRD/TMSSUP/OUT/" + details;

//                        File downloadFile1 = new File("C:/FTP/CSV/FROM/" + details);
                        File downloadFile1 = new File("//var//lib//tomcat7//webapps//throttle//CSV//FROM//" + details);
                        OutputStream outputStream1 = new BufferedOutputStream(new FileOutputStream(downloadFile1));
                        boolean success = ftpClient.retrieveFile(remoteFile1, outputStream1);
                        outputStream1.close();

                        if (success) {

                            InputStream iStream = ftpClient.retrieveFileStream(remoteFile2);
                            BufferedInputStream bInf = new BufferedInputStream(iStream);
                            int bytesRead;
                            byte[] buffer = new byte[10000];
                            String fileContent = null;

                            while ((bytesRead = bInf.read(buffer)) != -1) {
                                fileContent = new String(buffer, 0, bytesRead);
                            }

                            Scanner sc = new Scanner(fileContent);

                            while (sc.hasNext()) {
                                String n = sc.nextLine();

                                String[] temp = n.split(",");

                                if (!temp[0].equalsIgnoreCase("Invoice No")) {
                                    String invoiceNo = temp[0];
                                    String xmlStatus = temp[1];
                                    String reMarks = temp[2];
                                    //System.out.println("invoiceNo " + invoiceNo);
                                    //System.out.println("xmlStatus " + xmlStatus);
                                    //System.out.println("reMarks " + reMarks);
                                    int updateInHeader = reportDAO.updateInHeaderCsvStatus(invoiceNo, xmlStatus, reMarks);
                                }
                            }
//

                        }
                    }
                }

//                //System.out.println("Type: " + type);
            } else {
                //System.out.println("The specified file/directory may not exist!");
            }

            ftpClient.logout();
            ftpClient.disconnect();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (ftpClient.isConnected()) {
                try {
                    ftpClient.disconnect();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return status;
    }

    public String downloadCreditInvoiceCsvFile() {
        String server = "117.239.10.237";
        int port = 21;
        String user = "sap";
        String pass = "sap321";
        String status = "";

//           String ftpUrl = "ftp://%s:%s@%s/%s;type=i";
//        String host = "117.239.10.237";
//        String user = "sap";
//        String pass = "sap321";
        FTPClient ftpClient = new FTPClient();

        try {
            ftpClient.connect(server, port);
            ftpClient.login(user, pass);
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            // use local passive mode to pass firewall
            ftpClient.enterLocalPassiveMode();

            // get details of a file or directory
            String remoteFilePath = "/PRD/TMSCN/IN/";

            FTPFile ftpFile = ftpClient.mlistFile(remoteFilePath);
            if (ftpFile != null) {

                FTPFile[] files1 = ftpClient.listFiles(remoteFilePath);

                for (FTPFile file : files1) {
                    String details = file.getName();

                    if (details.contains(".CSV")) {
                        //System.out.println(details);

                        String remoteFile1 = "/PRD/TMSCN/IN/" + details;
                        String remoteFile2 = "/PRD/TMSCN/OUT/" + details;

//                        File downloadFile1 = new File("C:/FTP/CSV/FROM/" + details);
                        File downloadFile1 = new File("//var//lib//tomcat7//webapps//throttle//CSV//FROM//" + details);
                        OutputStream outputStream1 = new BufferedOutputStream(new FileOutputStream(downloadFile1));
                        boolean success = ftpClient.retrieveFile(remoteFile1, outputStream1);
                        outputStream1.close();

                        if (success) {

                            InputStream iStream = ftpClient.retrieveFileStream(remoteFile2);
                            BufferedInputStream bInf = new BufferedInputStream(iStream);
                            int bytesRead;
                            byte[] buffer = new byte[10000];
                            String fileContent = null;

                            while ((bytesRead = bInf.read(buffer)) != -1) {
                                fileContent = new String(buffer, 0, bytesRead);
                            }

                            Scanner sc = new Scanner(fileContent);

                            while (sc.hasNext()) {
                                String n = sc.nextLine();

                                String[] temp = n.split(",");

                                if (!temp[0].equalsIgnoreCase("Invoice No")) {
                                    String invoiceNo = temp[0];
                                    String xmlStatus = temp[1];
                                    String reMarks = temp[2];
                                    //System.out.println("invoiceNo " + invoiceNo);
                                    //System.out.println("xmlStatus " + xmlStatus);
                                    //System.out.println("reMarks " + reMarks);
                                    int updateInHeader = reportDAO.updateInHeaderCsvStatus(invoiceNo, xmlStatus, reMarks);
                                }
                            }
//

                        }
                    }
                }

//                //System.out.println("Type: " + type);
            } else {
                //System.out.println("The specified file/directory may not exist!");
            }

            ftpClient.logout();
            ftpClient.disconnect();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (ftpClient.isConnected()) {
                try {
                    ftpClient.disconnect();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return status;
    }

    public String downloadSuppCreditInvoiceCsvFile() {
//            String remoteFilePath = "/PRD/TMSSUPCN/IN/";
        String server = "117.239.10.237";
        int port = 21;
        String user = "sap";
        String pass = "sap321";
        String status = "";

//           String ftpUrl = "ftp://%s:%s@%s/%s;type=i";
//        String host = "117.239.10.237";
//        String user = "sap";
//        String pass = "sap321";
        FTPClient ftpClient = new FTPClient();

        try {
            ftpClient.connect(server, port);
            ftpClient.login(user, pass);
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            // use local passive mode to pass firewall
            ftpClient.enterLocalPassiveMode();

            // get details of a file or directory
            String remoteFilePath = "/PRD/TMSSUPCN/IN/";

            FTPFile ftpFile = ftpClient.mlistFile(remoteFilePath);
            if (ftpFile != null) {

                FTPFile[] files1 = ftpClient.listFiles(remoteFilePath);

                for (FTPFile file : files1) {
                    String details = file.getName();

                    if (details.contains(".CSV")) {
                        //System.out.println(details);

                        String remoteFile1 = "/PRD/TMSSUPCN/IN/" + details;
                        String remoteFile2 = "/PRD/TMSSUPCN/OUT/" + details;

                        //System.out.println("File #1 has been downloaded successfully.");
                        boolean success1 = ftpClient.rename(remoteFile1, remoteFile2);
                        if (success1) {
                            //System.out.println("success : " + success1);
                        } else {
                            //System.out.println("fail : " + success1);
                            // rename failed
                        }

                        boolean success = true;

                        if (success) {

                            InputStream iStream = ftpClient.retrieveFileStream(remoteFile2);
                            BufferedInputStream bInf = new BufferedInputStream(iStream);
                            int bytesRead;
                            byte[] buffer = new byte[10000];
                            String fileContent = null;

                            while ((bytesRead = bInf.read(buffer)) != -1) {
                                fileContent = new String(buffer, 0, bytesRead);
                            }

                            Scanner sc = new Scanner(fileContent);

                            while (sc.hasNext()) {
                                String n = sc.nextLine();

                                String[] temp = n.split(",");

                                if (!temp[0].equalsIgnoreCase("Invoice No")) {
                                    String invoiceNo = temp[0];
                                    String xmlStatus = temp[1];
                                    String reMarks = temp[2];
                                    //System.out.println("invoiceNo " + invoiceNo);
                                    //System.out.println("xmlStatus " + xmlStatus);
                                    //System.out.println("reMarks " + reMarks);
                                    int updateInHeader = reportDAO.updateInHeaderCsvStatus(invoiceNo, xmlStatus, reMarks);
                                }
                            }
//

                        }
                    }
                }

//                //System.out.println("Type: " + type);
            } else {
                //System.out.println("The specified file/directory may not exist!");
            }

            ftpClient.logout();
            ftpClient.disconnect();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (ftpClient.isConnected()) {
                try {
                    ftpClient.disconnect();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return status;
    }

    public String readCsvFile() throws IOException {

//        String directoryPath = "C:\\FTP\\CSV\\From\\";
        String directoryPath = "//var//lib//tomcat7//webapps//throttle//CSV//FROM//";
        String sourcePath1 = "//var//lib//tomcat7//webapps//throttle//CSV//TO//";
//        String sourcePath1 = "C:\\FTP\\CSV\\To\\";
        File[] filesInDirectory = new File(directoryPath).listFiles();
        String status = "";
        // //System.out.println("CSV file found -> " + filesInDirectory.length);
        for (File f : filesInDirectory) {
            String filePath = f.getAbsolutePath();
            String filename = f.getName();
            String fileExtenstion = filePath.substring(filePath.lastIndexOf(".") + 1, filePath.length());
            if ("CSV".equals(fileExtenstion)) {
                String line = null;

                // Read all lines in from CSV file and add to studentList
                FileReader fileReader = new FileReader(filePath);
                BufferedReader bufferedReader = new BufferedReader(fileReader);

                while ((line = bufferedReader.readLine()) != null) {
                    String[] temp = line.split(",");
                    if (!temp[0].equalsIgnoreCase("Invoice No")) {
                    }
                    String invoiceNo = temp[0];
                    String xmlStatus = temp[1];
                    String reMarks = temp[2];
                    int updateInHeader = reportDAO.updateInHeaderCsvStatus(invoiceNo, xmlStatus, reMarks);
                }
                bufferedReader.close();

            }

            Path temp = Files.move(Paths.get(directoryPath + filename),
                    Paths.get(sourcePath1 + filename));

            //System.out.println("temp----------" + temp);
            if (temp != null) {
                //System.out.println("File renamed and moved successfully");
            } else {
                //System.out.println("Failed to move the file");
            }

        }
        return status;

    }

    public ArrayList getConsignorChange(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getConsignorChange = new ArrayList();
        getConsignorChange = reportDAO.getConsignorChange(reportTO);
        return getConsignorChange;
    }

    public ArrayList getConsignmentOrder(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getConsignmentOrder = new ArrayList();
        getConsignmentOrder = reportDAO.getConsignmentOrder(reportTO);
        return getConsignmentOrder;
    }

    public int updateConsignorChanges(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = reportDAO.updateConsignorChanges(reportTO);
        return status;
    }

    public String getInvoiceXMLDataManual(String fromDate, String toDate, String invoiceId, int userId) throws ServletException, IOException {

        //System.out.println("i m in XML controller method");
        final String endpointString = "http://dict.throttletms.com/throttlews/ThrottleServiceManager?wsdl";
        final String namespace = "http://web.ws.batch.to.ets.com/";
        String endpoint = endpointString;
        String returnValue = "";
        String userName = "throttleapidemo";
        String pwd = "admin123";
        String status = "1";

        try {

            ///select date_format(now(),'%d%m%y%h%i%s') from ts_trip_master limit 1
            String todayDate = reportDAO.getTodayDate(9);
            Service service = new Service();
            Call call = (Call) service.createCall();
            call.setTargetEndpointAddress(new java.net.URL(endpoint));
            SOAPEnvelope env = new SOAPEnvelope();
            SOAPBodyElement element = new SOAPBodyElement(namespace, "getInvoiceList");
            element.setPrefix("web");
            element.addChildElement("fromDate", null).addTextNode(fromDate);
            element.addChildElement("toDate", null).addTextNode(toDate);
            element.addChildElement("invoiceId", null).addTextNode(invoiceId);
            element.addChildElement("userName", null).addTextNode(userName);
            element.addChildElement("password", null).addTextNode(pwd);
            env.addBodyElement(element);
            //System.out.println("input:" + env.getBody().toString());
            SOAPEnvelope envelope = call.invoke(env);
            returnValue = envelope.getBody().toString();
            //System.out.println("returnValue:" + returnValue);
            if (!"".equals(returnValue) && returnValue != null) {
                //  request.setAttribute("Param", true);
            }

            //DD MM YY HH MM I SS
            //get Dateqry from DB and set file name
            //select date_format(now(),'%d%m%y%h%i%s') from ts_trip_master limit 1
            String actualServerFilePath = "", tempServerFilePath = "";
//            actualServerFilePath = "C://FTP//XML//From//";
//            actualServerFilePath = "C://FTP//XML//From//";
            actualServerFilePath = "//var//lib//tomcat7//webapps//throttle//From";
            //System.out.println("Server Path == " + actualServerFilePath);
            tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");

            File file = new File(actualServerFilePath + "//" + "TMS_" + todayDate + ".xml");
            String fileName = "TMS_" + todayDate + ".xml";

//                        File file = new File(actualServerFilePath + "//" + "Invoice_" + fromDate + "_to_" + toDate + ".xml");
            //String filename = "Invoice_"+fromDate+"_to_"+toDate+".xml";
//            String fileName = "Invoice_" + fromDate + "_to_" + toDate + ".xml";
            boolean bool = file.createNewFile();
            //System.out.println("file created ..");
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(returnValue);
            fileWriter.flush();
            fileWriter.close();
            String type = "1";

            int invoiceLog = reportDAO.updateInvoiceLogManual(fileName, type, userId);

            /// create new table with flag and update invoice table flag
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    public String getSuppInvoiceXMLDataManual(String fromDate, String toDate, String invoiceId, int userId) throws ServletException, IOException {

        //System.out.println("i m in XML controller method");
        final String endpointString = "http://dict.throttletms.com/throttlews/ThrottleServiceManager?wsdl";
        final String namespace = "http://web.ws.batch.to.ets.com/";
        String endpoint = endpointString;
        String returnValue = "";
        String userName = "throttleapidemo";
        String pwd = "admin123";
        String status = "";

        try {

            ///select date_format(now(),'%d%m%y%h%i%s') from ts_trip_master limit 1
            String todayDate = reportDAO.getTodayDate(9);
            Service service = new Service();
            Call call = (Call) service.createCall();
            call.setTargetEndpointAddress(new java.net.URL(endpoint));
            SOAPEnvelope env = new SOAPEnvelope();
            SOAPBodyElement element = new SOAPBodyElement(namespace, "getSupplementryInvoiceList");
            element.setPrefix("web");
            element.addChildElement("fromDate", null).addTextNode(fromDate);
            element.addChildElement("toDate", null).addTextNode(toDate);
            element.addChildElement("invoiceId", null).addTextNode(invoiceId);
            element.addChildElement("userName", null).addTextNode(userName);
            element.addChildElement("password", null).addTextNode(pwd);
            env.addBodyElement(element);
            //System.out.println("input:" + env.getBody().toString());
            SOAPEnvelope envelope = call.invoke(env);
            returnValue = envelope.getBody().toString();
            //System.out.println("returnValue:" + returnValue);
            if (!"".equals(returnValue) && returnValue != null) {
                //  request.setAttribute("Param", true);
            }

            //DD MM YY HH MM I SS
            //get Dateqry from DB and set file name
            //select date_format(now(),'%d%m%y%h%i%s') from ts_trip_master limit 1
            String actualServerFilePath = "", tempServerFilePath = "";
//            actualServerFilePath = "C://FTP//XML//From//";
//            actualServerFilePath = "C://FTP//XML//From//";
            actualServerFilePath = "//var//lib//tomcat7//webapps//throttle//From";
            //System.out.println("Server Path == " + actualServerFilePath);
            tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");

            File file = new File(actualServerFilePath + "//" + "TMSSUP_" + todayDate + ".xml");
            String fileName = "TMSSUP_" + todayDate + ".xml";

//                        File file = new File(actualServerFilePath + "//" + "SupplementryInvoice_" + fromDate + "_to_" + toDate + ".xml");
//            //String filename = "Invoice_"+fromDate+"_to_"+toDate+".xml";
//            String fileName = "SupplementryInvoice_" + fromDate + "_to_" + toDate + ".xml";
            boolean bool = file.createNewFile();
            //System.out.println("file created ..");
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(returnValue);
            fileWriter.flush();
            fileWriter.close();
            String type = "2";

            int invoiceLog = reportDAO.updateInvoiceLogManual(fileName, type, userId);

            /// create new table with flag and update invoice table flag
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    public String getCreditInvoiceXMLDataManual(String fromDate, String toDate, String invoiceId, int userId) throws ServletException, IOException {

        //System.out.println("i m in XML controller method");
        final String endpointString = "http://dict.throttletms.com/throttlews/ThrottleServiceManager?wsdl";
        final String namespace = "http://web.ws.batch.to.ets.com/";
        String endpoint = endpointString;
        String returnValue = "";
        String userName = "throttleapidemo";
        String pwd = "admin123";
        String status = "";

        try {

            ///select date_format(now(),'%d%m%y%h%i%s') from ts_trip_master limit 1
            String todayDate = reportDAO.getTodayDate(9);
            Service service = new Service();
            Call call = (Call) service.createCall();
            call.setTargetEndpointAddress(new java.net.URL(endpoint));
            SOAPEnvelope env = new SOAPEnvelope();
            SOAPBodyElement element = new SOAPBodyElement(namespace, "getCreditNoteList");
            element.setPrefix("web");
            element.addChildElement("fromDate", null).addTextNode(fromDate);
            element.addChildElement("toDate", null).addTextNode(toDate);
            element.addChildElement("invoiceId", null).addTextNode(invoiceId);
            element.addChildElement("userName", null).addTextNode(userName);
            element.addChildElement("password", null).addTextNode(pwd);
            env.addBodyElement(element);
            //System.out.println("input:" + env.getBody().toString());
            SOAPEnvelope envelope = call.invoke(env);
            returnValue = envelope.getBody().toString();
            //System.out.println("returnValue:" + returnValue);
            if (!"".equals(returnValue) && returnValue != null) {
                //  request.setAttribute("Param", true);
            }

            //DD MM YY HH MM I SS
            //get Dateqry from DB and set file name
            //select date_format(now(),'%d%m%y%h%i%s') from ts_trip_master limit 1
            String actualServerFilePath = "", tempServerFilePath = "";
//            actualServerFilePath = "C://FTP//XML//From//";
//            actualServerFilePath = "C://FTP//XML//From//";
            actualServerFilePath = "//var//lib//tomcat7//webapps//throttle//From";
            //System.out.println("Server Path == " + actualServerFilePath);
            tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");

            File file = new File(actualServerFilePath + "//" + "TMSCN_" + todayDate + ".xml");
            String fileName = "TMSCN_" + todayDate + ".xml";

//                        File file = new File(actualServerFilePath + "//" + "CreditNote_" + fromDate + "_to_" + toDate + ".xml");
//            //String filename = "Invoice_"+fromDate+"_to_"+toDate+".xml";
//            String fileName = "CreditNote_" + fromDate + "_to_" + toDate + ".xml";
            boolean bool = file.createNewFile();
            //System.out.println("file created ..");
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(returnValue);
            fileWriter.flush();
            fileWriter.close();
            String type = "3";

            int invoiceLog = reportDAO.updateInvoiceLogManual(fileName, type, userId);

            /// create new table with flag and update invoice table flag
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    public String getSuppCreditInvoiceXMLDataManual(String fromDate, String toDate, String invoiceId, int userId) throws ServletException, IOException {

        //System.out.println("i m in XML controller method");
        final String endpointString = "http://dict.throttletms.com/throttlews/ThrottleServiceManager?wsdl";
        final String namespace = "http://web.ws.batch.to.ets.com/";
        String endpoint = endpointString;
        String returnValue = "";
        String userName = "throttleapidemo";
        String pwd = "admin123";
        String status = "";

        try {

            ///select date_format(now(),'%d%m%y%h%i%s') from ts_trip_master limit 1
            String todayDate = reportDAO.getTodayDate(9);
            Service service = new Service();
            Call call = (Call) service.createCall();
            call.setTargetEndpointAddress(new java.net.URL(endpoint));
            SOAPEnvelope env = new SOAPEnvelope();
            SOAPBodyElement element = new SOAPBodyElement(namespace, "getSuppCreditNoteList");
            element.setPrefix("web");
            element.addChildElement("fromDate", null).addTextNode(fromDate);
            element.addChildElement("toDate", null).addTextNode(toDate);
            element.addChildElement("invoiceId", null).addTextNode(invoiceId);
            element.addChildElement("userName", null).addTextNode(userName);
            element.addChildElement("password", null).addTextNode(pwd);
            env.addBodyElement(element);
            //System.out.println("input:" + env.getBody().toString());
            SOAPEnvelope envelope = call.invoke(env);
            returnValue = envelope.getBody().toString();
            //System.out.println("returnValue:" + returnValue);
            if (!"".equals(returnValue) && returnValue != null) {
                //  request.setAttribute("Param", true);
            }

            //DD MM YY HH MM I SS
            //get Dateqry from DB and set file name
            //select date_format(now(),'%d%m%y%h%i%s') from ts_trip_master limit 1
            String actualServerFilePath = "", tempServerFilePath = "";
//            actualServerFilePath = "C://FTP//XML//From//";
//            actualServerFilePath = "C://FTP//XML//From//";
            actualServerFilePath = "//var//lib//tomcat7//webapps//throttle//From";
            //System.out.println("Server Path == " + actualServerFilePath);
            tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");

            File file = new File(actualServerFilePath + "//" + "TMSSUPCN_" + todayDate + ".xml");
            String fileName = "TMSSUPCN_" + todayDate + ".xml";

//                        File file = new File(actualServerFilePath + "//" + "SupplementCreditNote_" + fromDate + "_to_" + toDate + ".xml");
//            //String filename = "Invoice_"+fromDate+"_to_"+toDate+".xml";
//            String fileName = "SupplementCreditNote_" + fromDate + "_to_" + toDate + ".xml";
            boolean bool = file.createNewFile();
            //System.out.println("file created ..");
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(returnValue);
            fileWriter.flush();
            fileWriter.close();
            String type = "4";

            int invoiceLog = reportDAO.updateInvoiceLogManual(fileName, type, userId);

            /// create new table with flag and update invoice table flag
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    public ArrayList invoiceForEinv() throws FPRuntimeException, FPBusinessException {
        ArrayList invoiceForEinv = new ArrayList();
        invoiceForEinv = reportDAO.invoiceForEinv();
        return invoiceForEinv;
    }

    public ArrayList invoiceForESuppinv() throws FPRuntimeException, FPBusinessException {
        ArrayList invoiceForESuppinv = new ArrayList();
        invoiceForESuppinv = reportDAO.invoiceForESuppinv();
        return invoiceForESuppinv;
    }

    public ArrayList invoiceForECreditinv() throws FPRuntimeException, FPBusinessException {
        ArrayList invoiceForECreditinv = new ArrayList();
        invoiceForECreditinv = reportDAO.invoiceForECreditinv();
        return invoiceForECreditinv;
    }

    public ArrayList invoiceForESuppCreditinv() throws FPRuntimeException, FPBusinessException {
        ArrayList invoiceForESuppCreditinv = new ArrayList();
        invoiceForESuppCreditinv = reportDAO.invoiceForESuppCreditinv();
        return invoiceForESuppCreditinv;
    }

    public ArrayList getInvoiceCountValues(ReportTO repTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getInvoiceCountValues = new ArrayList();
        getInvoiceCountValues = reportDAO.getInvoiceCountValues(repTO);
        return getInvoiceCountValues;
    }

    public ArrayList getFinanceEInvoiceValues(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList invList = new ArrayList();
        invList = reportDAO.getFinanceEInvoiceValues(reportTO);
        return invList;
    }

    public ArrayList getSuppFinanceEInvoiceValues(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList invList = new ArrayList();
        invList = reportDAO.getSuppFinanceEInvoiceValues(reportTO);
        return invList;
    }

    public ArrayList getCreditFinanceEInvoiceValues(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList invList = new ArrayList();
        invList = reportDAO.getCreditFinanceEInvoiceValues(reportTO);
        return invList;
    }

    public ArrayList getSuppCreditFinanceEInvoiceValues(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList invList = new ArrayList();
        invList = reportDAO.getSuppCreditFinanceEInvoiceValues(reportTO);
        return invList;
    }

    public String downloadInvoiceCsvFile() {

        String server = "117.239.10.237";
        int port = 21;
        String user = "sap";
        String pass = "sap321";
        String status = "";
        FTPClient ftpClient = new FTPClient();

        try {

            ftpClient.connect(server, port);
            ftpClient.login(user, pass);
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            // use local passive mode to pass firewall
            ftpClient.enterLocalPassiveMode();

            // get details of a file or directory
            String remoteFilePath = "/PRD/TMS/IN/";

            FTPFile ftpFile = ftpClient.mlistFile(remoteFilePath);
            if (ftpFile != null) {
                FTPFile[] files1 = ftpClient.listFiles(remoteFilePath);
                for (FTPFile file : files1) {
                    String details = file.getName();

                    if (details.contains(".CSV")) {
                        //System.out.println(details);

                        // APPROACH #1: using retrieveFile(String, OutputStream)
                        String remoteFile1 = "/PRD/TMS/IN/" + details;
                        String remoteFile2 = "/PRD/TMS/OUT/" + details;

                        File downloadFile1 = new File("/var/lib/tomcat7/webapps/throttle/CSV/FROM/" + details);
//                        File downloadFile1 = new File("/var/lib/tomcat8/webapps/CSV/" + details);
                        OutputStream outputStream1 = new BufferedOutputStream(new FileOutputStream(downloadFile1));
                        boolean success = ftpClient.retrieveFile(remoteFile1, outputStream1);
                        outputStream1.close();

                        if (success) {
                            //System.out.println("File #1 has been downloaded successfully.");

                            ftpClient.rename(remoteFile1, remoteFile2);
                        }
                    }
                }

                //                //System.out.println("Type: " + type);
            } else {
                //System.out.println("The specified file/directory may not exist!");
            }

            //////////////////////////////////// Read Part ///////////////////////////////////////
            String directoryPath = "/var/lib/tomcat7/webapps/throttle/CSV/FROM/";
            String sourcePath1 = "/var/lib/tomcat7/webapps/throttle/CSV/TO/";
//        String sourcePath1 = "C:\\FTP\\CSV\\To\\";
            File[] filesInDirectory = new File(directoryPath).listFiles();

            // //System.out.println("CSV file found -> " + filesInDirectory.length);
            for (File f : filesInDirectory) {
                String filePath = f.getAbsolutePath();
                String filename = f.getName();
                String fileExtenstion = filePath.substring(filePath.lastIndexOf(".") + 1, filePath.length());
                if ("CSV".equals(fileExtenstion)) {
                    //System.out.println("CSV file found -> " + filename);

                    String line = null;

                    // Read all lines in from CSV file and add to studentList
                    FileReader fileReader = new FileReader(filePath);
                    BufferedReader bufferedReader = new BufferedReader(fileReader);

                    while ((line = bufferedReader.readLine()) != null) {
                        String[] temp = line.split(",");
                        if (!temp[0].equalsIgnoreCase("Invoice No")) {
                            //System.out.println(" invoice " + temp[0]);
                            //System.out.println(" status " + temp[1]);
                            //System.out.println(" doc " + temp[2]);
                        }
                        String invoiceNo = temp[0];
                        String xmlStatus = temp[1];
                        String reMarks = temp[2];
                        int updateInHeader = reportDAO.updateInHeaderCsvStatus(invoiceNo, xmlStatus, reMarks);
                    }
                    bufferedReader.close();

                }

                Path temp = Files.move(Paths.get(directoryPath + filename),
                        Paths.get(sourcePath1 + filename));

                //System.out.println("temp----------" + temp);
                if (temp != null) {
                    //System.out.println("File renamed and moved successfully");
                } else {
                    //System.out.println("Failed to move the file");
                }

            }

            //////////////////////////////////// Read Part ///////////////////////////////////////
            ftpClient.logout();
            ftpClient.disconnect();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (ftpClient.isConnected()) {
                try {
                    ftpClient.disconnect();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return status;
    }
          public String getDetentionChargeDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
       String getDetentionChargeDetails = reportDAO.getDetentionChargeDetails(reportTO);
        return getDetentionChargeDetails;
    }
    public ArrayList getSuppInvoiceReport() throws FPRuntimeException, FPBusinessException {
        ArrayList invList = new ArrayList();
        invList = reportDAO.getSuppInvoiceReport();
        return invList;
    }
    public ArrayList getMainInvoiceReport() throws FPRuntimeException, FPBusinessException {
        ArrayList invList = new ArrayList();
        invList = reportDAO.getMainInvoiceReport();
        return invList;
    }
    public int getGpsDetails() throws FPBusinessException, FPRuntimeException {
        int status = 0;

        status = reportDAO.getGpsDetails();

        return status;
    }
    public int updateCnoteChanges(String tripId,String consignmentId,String consignmentIdNew,String consignmentOrder,String consignmentOrderNew) throws FPRuntimeException, FPBusinessException {
       int updateCnoteChanges=0;
       SqlMapClient session = reportDAO.getSqlMapClient();
       try {
         updateCnoteChanges = reportDAO.updateCnoteChanges(tripId,consignmentId,consignmentIdNew,consignmentOrder,consignmentOrderNew,session);
             session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                session.getSession().close();
            }
        }
        return updateCnoteChanges;
    }

}
