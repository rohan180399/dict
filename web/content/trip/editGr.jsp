
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@page import="java.text.DecimalFormat"%>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>

<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            dateFormat: 'dd-mm-yy',
            changeMonth: true, changeYear: true
        });

    });


    function replaceSpecialCharacters()
    {
        var content = document.getElementById("requestremarks").value;

        //alert(content.replace(/[^a-zA-Z0-9]/g,'_'));
        // content=content.replace(/[^a-zA-Z0-9]/g,'');
        content = content.replace(/[@&\/\\#,+()$~%'":*?<>{}]/g, ' ');
        document.getElementById("requestremarks").value = content;
        //alert("Repalced");

    }
</script>

<script type="text/javascript">
    function onKeyPressBlockCharacters1(sno3, e)
    {
        var fieldLength = document.getElementById('containerNo' + sno3).value.length;

        if (fieldLength <= 3) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            reg = /\d/;
            return !reg.test(keychar);
        } else if (fieldLength <= 10) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            reg = /[a-zA-Z]+$/;
            return !reg.test(keychar);
        }
    }
    function validateContainerNo(sno) {
        var containerNo = document.getElementById("containerNo" + sno).value;
        var fieldLength = document.getElementById("containerNo" + sno).value.length;
        if (fieldLength != 11) {
            alert("Container no length should be 11");
//            document.getElementById("containerNo" + sno).focus();
        
        }
    }
</script>

<script>
    function checkContainerNo(sno, value, type) {
        var consignmentOrderId = document.getElementById("consignmentOrderId").value;

        var tempContainerNo = "";
        $.ajax({
            url: "/throttle/getPlannedContainerNo.do",
            dataType: "text",
            data: {
                orderId: consignmentOrderId

            },
            success: function(temp) {
                if (temp != '') {
                    //   alert("temp:"+temp);
                    tempContainerNo = temp.split(",");
                    for (var i = 0; i < tempContainerNo.length; i++) {
                        if (value == tempContainerNo[i]) {
                            document.getElementById("containerNo" + sno).value = "";
                            alert("This container No has been already planned !check  Container No");
                        }
                    }


                }

            }
        });

    }
</script>
<script>

    var httpRequest;
    function checkContainerNos(sno) {
        var containerNo = document.getElementById("containerNo" + sno).value;
        if (document.getElementById("containerNo" + sno) != '') {
//            alert(containerNo)
            var url = '/throttle/checkContainerExists.do?containerNo=' + containerNo;

            if (window.ActiveXObject)
            {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest)
            {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("POST", url, true);
            httpRequest.onreadystatechange = function() {
                go1(sno);
            };
            httpRequest.send(null);
        }
    }

    function go1(sno) {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var response = httpRequest.responseText;
                var temp = response.split('-');
//                alert(response);
                if (response >= 1) {

                    alert('Container Already Exists');
                    document.getElementById("containerNo" + sno).value = "";
//                    document.getElementById("containerNo").isEmpty();

//                    document.getElementById("StatusMsg").innerHTML = httpRequest.responseText.valueOf() + " Already Exists";
//                    document.cNote.containerName.focus();
//                    document.cNote.containerName.select();
//                    document.cNote.containerNameCheck.value = 'exists';
                } else
                {

//                    document.cNote.containerNameCheck.value = 'Notexists';
//                    document.getElementById("StatusMsg").innerHTML = "";
                }
            }
        }
    }

</script>

<script language="javascript">
    function submitPage() {

        document.approve.action = '/throttle/updateTripGr.do';
        document.approve.submit();
    }
    function setFocus() {
        document.approve.advancerequestamt.select();
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> PrimaryOperation</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">PrimaryOperation</a></li>
            <li class="active">Edit Gr</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body onload="setGrReadOnly();
                    setFocus();">
                <form name="approve"  method="post" >
                    <%
                                request.setAttribute("menuPath", "Fuel >> Edit Fuel");
                                String tripid = request.getParameter("tripid");
                                String type = "M";
                    %>
                    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>


                    <c:if test="${tripGrDetails != null}">
                        <input type="hidden" id="tripId" name="tripId" value="<c:out value="${tripId}" />"/>
                        <input type="hidden" id="consignmentOrderId" name="consignmentOrderId" value="<c:out value="${consignmentOrderId}" />"/>

                        <!--<table style="width: 70%; left: 30px;" class="border" cellpadding="0"  cellspacing="0" align="center" border="0" id="fuelTBL" name="fuelTBL">-->
                        <table class="table table-info mb30 " style="width:100%" id="fuelTBL" name="fuelTBL">
                            <thead style="background-color:#5BC0DE;width:100%;height:30px;color:white;text-align: left;font-size: 13px;">
                                <tr height="30px">
                                    <th >S No</th>
                                    <th >Gr No</th>
                                    <th>Gr Date</th>
                                    <th>Gr Hours</th>
                                    <th>Gr Mins</th>
                                    <th>container No</th>
                                    <th>Seal No</th>
                                    <th>Shipper</th>
                                    <th>Vehicle No</th>
                                    <th>Driver Name</th>
                                    <th>Challan No</th>

                                </tr>
                            </thead>
                            <%int index1=1;%>
                            <c:forEach items="${tripGrDetails}" var="tripGr">
                                <tr height="30px">
                                    <td><%=index1++%></td>
                                    <td>
                                        <input name="grNumber" id="grNumber<%=index1%>"  class="textbox" type="text"  value='<c:out value="${tripGr.grNumber}"/>'   style="width:120px;height:22px;" readOnly/>

                                    </td>
                                    <td>
                                        <input name="grDate" id="grDate<%=index1%>"  class="textbox" type="text"  class="datepicker" value='<c:out value="${tripGr.grDate}"/>'   style="width:120px;height:22px;" />

                                    </td>
                                    <td>
                                        <input name="grHours" id="grHours<%=index1%>"  class="textbox" type="text"   value='<c:out value="${tripGr.grHours}"/>'   style="width:80px;height:22px;" />

                                    </td>
                                    <td>
                                        <input name="grMins" id="grMins<%=index1%>"  class="textbox" type="text"   value='<c:out value="${tripGr.grMins}"/>'   style="width:80px;height:22px;" />

                                    </td>

                                    <td>

                                        <input name="containerNo" id="containerNo<%=index1%>" type="text"   class="textbox" value='<c:out value="${tripGr.containerNo}"/>' onchange="checkContainerNo(<%=index1%>, this.value, 1);" onkeypress="return onKeyPressBlockCharacters1(<%=index1%>, event);" onblur="checkContainerNos(<%=index1%>);
                                                validateContainerNo(<%=index1%>);"  style="width:120px;height:22px;" />
                                        <input name="uniqueId" id="uniqueId<%=index1%>" type="hidden"  value='<c:out value="${tripGr.uniqueId}"/>'  />
                                        <input name="tripContainerId" id="tripContainerId<%=index1%>" type="hidden"  value='<c:out value="${tripGr.tripContainerId}"/>'   />
                                    </td>
                                    <td>
                                        <input name="sealNo" type="text" class="textbox" value="<c:out value="${tripGr.sealNo}"/>" id="sealNo<%=index1%>"   style="width:80px;height:22px;" />
                                    </td>
                                    <td>
                                        <select   name="linerName" id ="linerName<%=index1%>"  class="textbox"  style="width:80px;height:22px;"  >
                                            <option value='0'>--select---</option>
                                            <c:if test="${linerList != null}" >
                                                <c:forEach items="${linerList}" var="details">
                                                    <option  value='<c:out value="${details.linerId}" />'>
                                                        <c:out value="${details.linerName}" />
                                                    </c:forEach >
                                                </c:if>
                                        </select>
                                        <script>
                                            document.getElementById("linerName<%=index1%>").value = '<c:out value="${tripGr.linerId}"/>';
                                        </script>
<!--		                                                    <input name="linerName"  type="text" class="textbox" value="<c:out value="${tripGr.linerName}"/>" id="linerName<%=index1%>"    style="width:80px;height:22px;"  />-->
                                    </td>
                                    <td>
                                        <input name="vehicleNo"  type="text" class="textbox" value="<c:out value="${tripGr.vehicleNo}"/>" id="vehicleNo<%=index1%>"    style="width:80px;height:22px;"  readOnly/>
                                        <input name="vehicleId"  type="hidden" class="textbox" value="<c:out value="${tripGr.vehicleId}"/>" id="vehicleId<%=index1%>" />
                                        <input name="orderType"  type="hidden" class="textbox" value="<c:out value="${tripGr.orderType}"/>" id="orderType<%=index1%>"   />
                                    </td>
                                    <td>
                                        <input name="driverName"  type="text" class="textbox" value="<c:out value="${tripGr.driverName}"/>" id="driverName<%=index1%>"    style="width:80px;height:22px;"  />
                                    </td>
                                    <td>
                                        <input name="challanNo"  type="text" class="textbox" value="<c:out value="${tripGr.challanNo}"/>" id="challanNo<%=index1%>"    style="width:80px;height:22px;"  />

                                        <input name="count"  type="hidden" class="textbox" value="<%=index1%>" id="count" style="width:80px;"  />
                                    </td>


                                </tr>
                                <tr>
                                </c:forEach>

                            </c:if>
                            <td colspan="11" align="center">
                                <input type="button" name="save" value="save" onclick="submitPage()"  class="btn btn-success" style="width:80px;height:30px;"/>

                            </td>
                        </tr>


                    </table>



                    <script>
                        function setGrReadOnly() {
                            var vehicleId = document.getElementsByName("vehicleId");
                            var orderType = document.getElementsByName("orderType");
                            var count = document.getElementsByName("count");
                            //                             alert("vehicleId:"+vehicleId +"orderType:"+orderType);
                            for (var i = 0; i < count.length; i++) {
                                if (vehicleId[i].value == '0' && orderType[i].value == '3') {
                                    document.getElementById("grNumber" + count[i].value).readOnly = false;
                                    document.getElementById("vehicleNo" + count[i].value).readOnly = false;

                                } else {
                                    document.getElementById("grNumber" + count[i].value).readOnly = true;
                                    document.getElementById("vehicleNo" + count[i].value).readOnly = true;
                                }
                            }
                        }
                    </script>

                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

