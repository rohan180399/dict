<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

         <script type="text/javascript">
        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {

            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                dateFormat: 'dd-mm-yy',
                changeMonth: true, changeYear: true
            });

        });
    </script>
     <script type="text/javascript">
            var httpReq;
                            function setVendor(vendorTypeid) { //alert(str);
                            var temp = "";
//                                alert(vendorTypeid);
                                $.ajax({
                                    url: "/throttle/vendorPaymentsJson.do",
                                    dataType: "json",
                                    data: {
                                        vendorType: vendorTypeid
                                    },
                                    success: function (temp) {
//                                         alert(temp);
                                        if (temp != '') {
                                            $('#vendorName').empty();
                                            $('#vendorName').append(
                                                    $('<option style="width:150px"></option>').val(0+"~"+0).html('---Select----')
                                                    )
                                            $.each(temp, function (i, data) {
                                                $('#vendorName').append(
                                                        $('<option value="'+data.vendorId+"~"+data.ledgerId+'" style="width:150px"></option>').val(data.vendorId+"~"+data.ledgerId).html(data.vendorName)
                                                        )
                                            });
                                        } else {
                                            $('#vendorName').empty();
                                        }
                                    }
                                });

                            }
                              </script>


        <script type="text/javascript">
               var httpReq;
                            function getLedgerList(levelId) { //alert(str);
                            var temp = "";
//                                alert(levelId);
                                $.ajax({
                                    url: "/throttle/getLedgerListforGroup.do",
                                    dataType: "json",
                                    data: {
                                        levelId: levelId
                                    },
                                    success: function (temp) {
//                                         alert(temp);
                                        if (temp != '') {
                                            $('#debitLedgerId').empty();
                                            $('#debitLedgerId').append(
                                                    $('<option></option>').val(0).html('---Select----')
                                                    )
                                            $.each(temp, function (i, data) {
                                                $('#debitLedgerId').append(
                                                   $('<option value="'+data.ledgerID+'" ></option>').val(data.ledgerID).html(data.ledgerName)
                                                        )
                                            });
                                        } else {
                                            $('#debitLedgerId').empty();
                                        }
                                    }
                                });

                            }
                              </script>
                               <script type="text/javascript">
             function submitPage1(value)
            {

                var vendorTypeId = document.getElementById("vendorType").value;
                var vendorNameId = document.getElementById("vendorName").value;
//                alert(vendorNameId);
                var crjDate = document.getElementById("crjDate").value;
                var invoiceNo = document.getElementById("invoiceNo").value;
                var invoiceDate = document.getElementById("invoiceDate").value;
                var invoiceAmount = document.getElementById("invoiceAmount").value;
                var remarks = document.getElementById("remarks").value;

                var insertStatus = 0;
                var url = "";
                var crjId = 0;
                url = "./insertCRJ.do";
                $.ajax({
                    url: url,
                    data: {vendorTypeId: vendorTypeId,
                        vendorNameId:vendorNameId, crjDate: crjDate, invoiceNo:invoiceNo,invoiceDate:invoiceDate,
                        invoiceAmount:invoiceAmount, remarks:remarks

                    },
                    type: "GET",
                    success: function (response) {
                        crjId = response.toString().trim();
                        if (crjId == 0) {
                            insertStatus = 0;
                            var str = "CRJ Creation Failed ";
                           // var result = str.fontcolor("red");
                             $("#status1").text(str).css("color", "red");
                        } else {
                            insertStatus = crjId;
                            var str = "CRJ Created Successfully" + " & CRJ No is CRJ/1617/00" + crjId;
                            //var result = str.fontcolor("green");
                           // document.getElementById("Status").innerHTML = result;
                              $("#status1").text(str).css("color", "green");
                            $("#saveButton").hide();

                        }
                    },
                    error: function (xhr, status, error) {
                    }
                });
                return insertStatus;
            }

function submitPage(value)
{
    var vendorTypeId = document.getElementById("vendorType").value;
                var vendorNameId = document.getElementById("vendorName").value;
//                alert(vendorNameId);
                var crjDate = document.getElementById("crjDate").value;
                var invoiceNo = document.getElementById("invoiceNo").value;
                var invoiceDate = document.getElementById("invoiceDate").value;
                var invoiceAmount = document.getElementById("invoiceAmount").value;
                var remarks = document.getElementById("remarks").value;
                var crjCode = document.getElementById("crjCode").value;
                var debitLedgerId = document.getElementById("debitLedgerId").value;
                 var insertStatus = 0;
                var url = "";
                var crjId = 0;
    var count = 0;
    $('input[name="file"]').each(function (index, value)
    {
       // alert("bjk");
        var file = value.files[0];
        //uploadRemarks = $("#uploadRemarks" + count).val();
        if (file)
        {
            var formData = new FormData();
            formData.append('file', file);
            $.ajax({

                url: './insertCRJ.do?vendorTypeId=' + vendorTypeId + '&vendorNameId=' + vendorNameId + '&crjDate=' + crjDate+'&invoiceNo='+invoiceNo+'&invoiceDate='+invoiceDate+'&invoiceAmount='+invoiceAmount+'&remarks='+remarks+'&debitLedgerId='+debitLedgerId,
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                   alert(data);
                  var temp = data.split(":");
                  alert(temp[1]);
                    var temp1 = temp[1].split("}");
                      alert(temp1[0]);
                    var crjId = temp1[0];
                    if (crjId == 0) {
                            var str = "CRJ Creation Failed ";
                             $("#status1").text(str).css("color", "red");
                        } else {
                            var str = "CRJ Created Successfully" + " & CRJ No is " +crjCode+crjId;
                              $("#status1").text(str).css("color", "green");
                            $("#saveButton").hide();

                        }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }
        count++;
    });
     return insertStatus;
    }
       </script>
         <style>
    #index td {
   color:white;
}
</style>
        <div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="finance.label.AddCRJ"  text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance"  text="default text"/></a></li>
            <li class="active"><spring:message code="finance.label.AddCRJ"  text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body>
         <input type="hidden" name="crjCode" id="crjCode" value="<%=ThrottleConstants.CRJformCode%>"/>

      <form name="payment" method="post" class="form-horizontal form-bordered">
           <font style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; ">
                    <div align="center" id="status1">&nbsp;&nbsp;
                    </div>
                    </font>

               <table class="table table-info mb30 table-hover">
                   <thead>
                    <tr height="30" id="index">
                             <th colspan="4" ><b><spring:message code="finance.label.CreditorJournalVoucher"  text="default text"/></b></th>
                    </tr>
                   </thead>
                <tr height="40">
                    <td   ><font color=red>*</font><spring:message code="finance.label.VendorType"  text="default text"/></td>
                    <td   >
                         <select class="form-control"  name="vendorType" id="vendorType" onchange="setVendor(this.value);" style="width:260px;height:40px;">
                                                <option value='0'>---<spring:message code="finance.label.Select"  text="default text"/>----</option>
                                                <c:forEach items="${VendorTypeList}" var="vendor">
                                                    <option value="<c:out value="${vendor.vendorTypeId}"/>"><c:out value="${vendor.vendorTypeValue}"/></option>
                                                </c:forEach>
                                            </select>
                    </td>
<!--                </tr>
                <tr  height="40">-->
                    <td  ><font color=red>*</font><spring:message code="finance.label.Vendor"  text="default text"/></td>
                    <td  >
                        <select class="form-control" name="vendorName" id="vendorName" style="width:260px;height:40px;">

                                                                    </select>
                    </td>
                </tr>
                <tr  height="40">
                   <td><font color=red>*</font><spring:message code="finance.label.LevelName"  text="default text"/></td>
                    <td>
                        <select name="levelId" id="levelId" class="form-control"  onchange="getLedgerList(this.value)" style="width:260px;height:40px;">
                            <c:if test="${expenseGroupList != null}">
                                <option value="0" selected>--<spring:message code="finance.label.Select"  text="default text"/>--</option>
                                <c:forEach items="${expenseGroupList}" var="exp">
                                    <option value='<c:out value="${exp.levelID}"/>'><c:out value="${exp.levelGroupName}"/></option>
                                </c:forEach>
                            </c:if>
                        </select>
                    </td>

<!--                </tr>
                <tr  height="40">-->
                   <td><font color=red>*</font><spring:message code="finance.label.DebitLedgerName"  text="default text"/></td>
                    <td  style="border-right-color:#5BC0DE;">
                        <select name="debitLedgerId" id="debitLedgerId" class="form-control" style="width:260px;height:40px;">
                        </select>
                    </td>

                </tr>
                <tr  height="40">
                    <td   ><font color=red>*</font><spring:message code="finance.label.CRJDate"  text="default text"/></td>
                    <td   >
                          <input type="textbox" name="crjDate" id="crjDate" value="" class="datepicker" style="width:260px;height:40px;"/>
                    </td>
<!--                </tr>
                <tr  height="40">-->
                    <td   ><font color=red>*</font><spring:message code="finance.label.InvoiceNo"  text="default text"/></td>
                    <td   >
                         <input type="textbox" name="invoiceNo" id="invoiceNo" value="" class="form-control" style="width:260px;height:40px;"/>
                    </td>
                </tr>
                <tr height="40">
                    <td ><font color=red>*</font><spring:message code="finance.label.InvoiceDate"  text="default text"/></td>
                    <td >
                         <input type="textbox" name="invoiceDate" id="invoiceDate" value="" class="datepicker" style="width:260px;height:40px;"/>
                    </td>
<!--                </tr>
                <tr  height="40">-->
                    <td  ><font color=red>*</font><spring:message code="finance.label.InvoiceAmount"  text="default text"/></td>
                    <td >
                       <input type="textbox" name="invoiceAmount" id="invoiceAmount" value="" class="form-control" style="width:260px;height:40px;"/>
                    </td>
                </tr>
                <tr  height="40">
                    <td ><font color=red>*</font><spring:message code="finance.label.Narration"  text="default text"/></td>
                    <td >
                        <textarea type="textbox"   name="remarks" id="remarks" value=""  class="form-control" style="width:260px;height:40px;"/></textarea>
                    </td>
<!--                </tr>
                <tr  height="40">-->
                    <td ><font color=red>*</font><spring:message code="finance.label.Attach"  text="default text"/></td>
                    <td >
                       <input type="file" name="file" size="45"  multiple  style="width:260px;height:40px;"/>
                    </td>
                </tr>
            </table>
            <br>
            <center>
                 <input type="button" value="<spring:message code="finance.label.SaveCRJ"  text="default text"/>" class="btn btn-success" id="saveButton" onClick="submitPage(this.value);" style="width:100px;height:35px;">
                &emsp;<input type="reset" class="btn btn-success" value="<spring:message code="finance.label.Clear"  text="default text"/>" style="width:100px;height:35px;">
            </center>
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
