<%-- 
    Document   : GRProfitabilityExcel
    Created on : May 2, 2016, 4:22:35 PM
    Author     : gulshan kumar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
            <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "GRSummaryReport-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
            <c:if test = "${GRProfitList != null}" >
                <table>
                    <center>
                        <font size="4" color="Black"><b>GR Wise Profitability - TT</b></font>
                    </center>
                </table>
                <br>
                <table border="1"  align="center" width="800" cellpadding="0" cellspacing="1" >
                    <thead>
                        <tr>
                            <td rowspan="2" class="contenthead">Sr. No</td>
                            <td rowspan="2" class="contenthead">Rake  No.</td>
                            <td rowspan="2" class="contenthead">Train No.</td>
                              <th rowspan="2" class="contenthead">Trip Code</th>
                            <th rowspan="2" class="contenthead">Trip Start Date</th>
                            <th rowspan="2" class="contenthead">Trip End Date</th>
                            <td rowspan="2" class="contenthead">Container No.</td>
                            <td rowspan="2" class="contenthead">Container Size</td>
                            <td rowspan="2" class="contenthead">I/E/Repo</td>
                            <td rowspan="2" class="contenthead">Commodity</td>
                            <td rowspan="2" class="contenthead">GR No.</td>
                            <td rowspan="2" class="contenthead">GR Issuing Date</td>
                            <td rowspan="2" class="contenthead">Order Created By</td>
                             <td rowspan="2" class="contenthead">Invoice No.</td>
                            <td rowspan="2" class="contenthead">Invoice Date</td>
                            <td rowspan="2" class="contenthead">Status</td>
                            <td rowspan="2" class="contenthead">TT Type</td>
                            <td rowspan="2" class="contenthead">TT Owner Name</td>
                            <td rowspan="2" class="contenthead">TT No.</td>
                            <td rowspan="2" class="contenthead">From</td>
                            <td rowspan="2" class="contenthead">To</td>
                            <td rowspan="2" class="contenthead">RouteInfo</td>
                            <td rowspan="2" class="contenthead">Chargable Y/N</td>
                            <td rowspan="2" class="contenthead">Reposition Y/N</td>
                            <td colspan="5" class="contenthead" style="text-align:center">Party Name</td>
                            <td colspan="6" class="contenthead" style="text-align:center">Revenue</td>
                            <td colspan="16" class="contenthead" style="text-align:center">TT-Expenses </td>
                            <td rowspan="2" class="contenthead">Un Recoverable Reposition Cost</td>
                            <td rowspan="2" class="contenthead">Profit</td>
                        </tr> 
                        <tr>
                            <td class="contenthead">Consignee Name</td>
                            <td class="contenthead">Consigner Name</td>
                            <td class="contenthead">Pay Party</td>
                            <td class="contenthead">GST No</td>
                            <td class="contenthead">Billing Address</td>

                            <td class="contenthead">Freight</td>
                            <td class="contenthead">Detention</td>
                            <td class="contenthead">Toll charges</td>
                            <td class="contenthead">Weightment</td>
                            <td class="contenthead">Other charges</td>
                            <td class="contenthead">Total Revenue</td>

                            <td class="contenthead">Diesel Ltr's</td>
                            <td class="contenthead">Rate</td>
                            <td class="contenthead">Diesel Amount</td>
                            <td class="contenthead">Toll Charges</td>
                            <td class="contenthead">Fooding Expenses</td>
                            <td class="contenthead">Entry Tax</td>
                            <td class="contenthead">RTO</td>
                            <td class="contenthead">Driver Salary</td>
                            <td class="contenthead">Lease Charges</td>
                            <td class="contenthead">Repair</td>
                            <td class="contenthead">Weightment</td>
                            <td class="contenthead">Octroi</td>
                            <td class="contenthead">Behati/Dala</td>
                            <td class="contenthead">Parking</td>
                            <td class="contenthead">Misc Exp</td>
                            <td class="contenthead">Total Expenses</td>
                        </tr>
                    </thead>
                    <% int index = 0, sno = 1;%>
                     <c:set var="unrecoverablecost" value="0" ></c:set>
                     <c:set var="Profit" value="0" ></c:set>
                     <c:set var="TTExpense" value="0" ></c:set>
                    <c:forEach items="${GRProfitList}" var="csList">
                        <c:set var="unrecoverablecost" value="${csList.tripExpense - csList.estimatedRevenue }" ></c:set>

                      

                        <c:set var="Profit" value="${ csList.estimatedRevenue - csList.tripExpense  }" ></c:set>


           <tr>
                                <td align="center"><%=sno%></td>
                            <td align="center"><c:out value=""/></td>
                            <td align="center"><c:out value=""/></td>
                            <td align="center"><c:out value="${csList.tripCode}"/></td>
                            <td align="center"><c:out value="${csList.tripStartDate}"/></td>
                            <td align="center"><c:out value="${csList.tripEndDate}"/></td>
                            <td align="center"><c:out value="${csList.containerNo}"/></td>
                            <td align="center"><c:out value="${csList.containerSize}"/></td>
                            <td align="center"><c:out value="${csList.movementType}"/></td>
                            <td align="center"><c:out value="${csList.articleName}"/></td>
                            <td align="center"><c:out value="${csList.grNo}"/></td>
                            <td align="center">
                                <c:set var="currentDate" value="${csList.grDate}" />
     <fmt:parseDate value="${currentDate}" var="parsedCurrentDate" pattern="dd-MM-yyyy HH:mm" type="date"/>  &nbsp;
     <fmt:formatDate pattern="dd-MM-yyyy HH:mm" value="${parsedCurrentDate}" type="date"/>
                            </td>
                            <td align="center"><c:out value="${csList.createdBy}"/></td>
                            <td align="center"><c:out value="${csList.invoiceCode}"/></td>
                            <td align="center">
                                 <c:set var="current1Date" value="${csList.invoiceDate}" />
     <fmt:parseDate value="${current1Date}" var="parsedCurrent1Date" pattern="dd-MM-yyyy" type="date"/>  &nbsp;
     <fmt:formatDate pattern="dd-MM-yyyy" value="${parsedCurrent1Date}" type="date"/>
                            </td>
                            <td align="center"><c:out value="${csList.statusName}"/></td>
                            <td align="center"><c:out value="${csList.transportType}"/></td>
                            <td align="center"><c:out value="${csList.transporter}"/></td>
                            <td align="center"><c:out value="${csList.vehicleNo}"/></td>
                            <td align="center"><c:out value="${csList.origin}"/></td>
                            <td align="center"><c:out value="${csList.destination}"/></td>
                            <td align="center"><c:out value="${csList.routeInfo}"/></td>
                            <td align="center">
                                <c:if test = "${csList.movementType == 'Repo'}">
                                    N
                                </c:if>
                                <c:if test = "${csList.movementType != 'Repo'}">
                                    Y
                                </c:if>
                            </td>
                            <td align="center">
                                <c:if test = "${csList.movementType == 'Repo'}">
                                    Y
                                </c:if>
                                <c:if test = "${csList.movementType != 'Repo'}">
                                    N
                                </c:if>
                            </td>
                            <td align="center"><c:out value="${csList.consigneeName}"/></td>
                            <td align="center"><c:out value="${csList.consignorName}"/></td>
                            <td align="center"><c:out value="${csList.billingParty}"/></td>
                            <td align="center"><c:out value="${csList.gstNo}"/></td>
                            <td align="center"><c:out value="${csList.address}"/></td>

                            <td align="center"><c:out value="${csList.estimatedRevenue}"/></td>
                            <td align="center"><c:out value="${csList.detaintionAmount}"/></td>
                            <td align="center"><c:out value="${csList.revenueTollAmount}"/></td>
                            <td align="center"><c:out value="${csList.revenueWeightmentAmount}"/></td>
                            <td align="center"><c:out value="${csList.revenueOtherAmount}"/></td>

                            <c:set var="totalRevenue" value="${csList.estimatedRevenue + csList.detaintionAmount + csList.revenueTollAmount +csList.revenueOtherAmount+csList.revenueWeightmentAmount}"></c:set>

                                <td align="center"><c:out value="${totalRevenue}"/></td>

                            <td align="center"><c:out value="${csList.dieselQty}"/></td>
                            <td align="center" class="text1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${csList.tripFuelAmount/csList.dieselQty}" /></td>
                            <td align="center"><c:out value="${csList.tripFuelAmount}"/></td>
                            <td align="center"><c:out value="${csList.tollAmount}"/></td>
                            <td align="center"><c:out value="${csList.foodingAmount}"/></td>
                            <td align="center"><c:out value="${0}"/></td>
                            <td align="center"><c:out value="${0}"/></td>
                            <td align="center"><c:out value="${0}"/></td>
                            <td align="center"><c:out value="${0}"/></td>
                            <td align="center"><c:out value="${0}"/></td>
                            <td align="center"><c:out value="${0}"/></td>
                            <td align="center"><c:out value="${0}"/></td>
                            <td align="center"><c:out value="${csList.behatiAmount}"/></td>
                            <td align="center"><c:out value="${0}"/></td>
                            <td align="center"><c:out value="${csList.miscAmount}"/></td>

                            <c:set var="TTExpense" value="${ csList.tripFuelAmount + csList.tollAmount + csList.foodingAmount + csList.behatiAmount  + csList.miscAmount }"></c:set>

                                <td align="center"><c:out value="${TTExpense}"/></td>
                            <td align="center">
                                <c:if test = "${csList.movementType == 'Repo' }">
                                    <c:if test = "${unrecoverablecost > 0 }">
                                        <c:out value="${unrecoverablecost}"/>
                                    </c:if>
                                </c:if>
                            </td>
                            <c:set var="Profit" value="${totalRevenue - TTExpense}" ></c:set>
                                <td align="center">
                                   <c:if test = "${csList.movementType != 'Repo' }">
                                   <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${Profit}" /> 
                                </c:if>
                            </td>

                             

                  <%--          <td align="center">
                                <c:if test = "${Profit > 0 }">
                                        <c:out value="${Profit}"/>
                                </c:if>
                           </td>  --%>


                        </tr>
                        <%
                            index++;
                            sno++;
                        %>
                    </c:forEach>
                </table>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>

