<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">


<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>


<script type="text/javascript">
    function submitPage(value) {

        if (document.getElementById('fromDate').value == '') {
            alert("Please select from Date");
            document.getElementById('fromDate').focus();
        } else if (document.getElementById('toDate').value == '') {
            alert("Please select to Date");
            document.getElementById('toDate').focus();
        } else {
            if (value == "ExportExcel") {
                document.BPCLTransaction.action = '/throttle/handleToPayCustomerTripDetailsExcel.do?param=ExportExcel';
                document.BPCLTransaction.submit();
            }
            else {
                document.BPCLTransaction.action = '/throttle/handleToPayCustomerTripDetailsExcel.do?param=Search';
                document.BPCLTransaction.submit();
            }
        }
    }
</script>

<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
            setValues();
            getVehicleNos();">
    </c:if>

    <!--	  <span style="float: right">
                    <a href="?paramName=en">English</a>
                    |
                    <a href="?paramName=ar">العربية</a>
              </span>-->

    <style>
        #index td {
            color:white;
        }
    </style>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i><spring:message code="operations.reports.label.TyreAnalysisReport" text="default text"/></h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="operations.reports.label.Finance" text="default text"/></a></li>
                <li class="active"><spring:message code="operations.reports.label.TyreAnalysisReport" text="default text"/></li>
            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body>
                    <form name="BPCLTransaction" method="post">
                        <%--<%@ include file="/content/common/path.jsp" %>--%>
                        <!-- pointer table -->
                        <!-- message table -->
                        <%@ include file="/content/common/message.jsp"%>

                        <!--<table class="table table-bordered" id="report" >-->
                        <!--                        <tr height="30" id="index" >
                                                    <td colspan="4"  style="background-color:#5BC0DE;">
                                                    <b><spring:message code="operations.reports.label.TopayCustomertripReport" text="default text"/></b>
                                                    </td>
                                                </tr>
                                                
                                                        <td style="border-color:#5BC0DE;padding:16px;">-->
                        <table  class="table table-info mb30 table-hover">
                            <thead><tr><th colspan="4"><spring:message code="operations.reports.label.TopayCustomertripReport" text="default text"/></th></tr></thead>
                            <!--                                    <tr>
                                                                    <td><font color="red">*</font>Driver Name</td>
                                                                    <td height="30">
                                                                        <input name="driName" id="driName" type="text" class="textbox" size="20" value="" onKeyPress="getDriverName();" autocomplete="off">
                                                                </tr>-->
                            <tr>
                                <td><font color="red">*</font><spring:message code="operations.reports.label.FromDate" text="default text"/></td>
                                <td height="30"><input name="fromDate"  autocomplete="off" style="width:260px;height:40px;" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                <td><font color="red">*</font><spring:message code="operations.reports.label.ToDate" text="default text"/></td>
                                <td height="30"><input name="toDate" id="toDate"  autocomplete="off" style="width:260px;height:40px;" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center" height="30">
                                    <input type="button" class="btn btn-success"  name="search" onclick="submitPage(this.name);" value="<spring:message code="operations.reports.label.SEARCH" text="default text"/>">
                                    <input type="button" class="btn btn-success"  name="ExportExcel" onclick="submitPage(this.name);" value="<spring:message code="operations.reports.label.EXPORTEXCEL" text="default text"/>">
                                </td>
                            </tr>

                        </table>
                        </td>
                        </table>

                        <c:if test="${ToPayCustomerTripDetails != null}">
                            <table class="table table-info mb30 table-hover" id="table" class="sortable"  >

                                <thead>
                                    <tr height="50">
                                        <th  align="center"><spring:message code="operations.reports.label.SNo" text="default text"/></th>
                                        <th  align="center"><spring:message code="operations.reports.label.TripCode" text="default text"/></th>
                                        <th  align="center"><spring:message code="operations.reports.label.CustomerName" text="default text"/></th>
                                        <th  align="center"><spring:message code="operations.reports.label.CustomerType" text="default text"/></th>
                                        <th  align="center"><spring:message code="operations.reports.label.RouteInfo" text="default text"/></th>
                                        <th  align="center"><spring:message code="operations.reports.label.StartTime" text="default text"/></th>
                                        <th  align="center"><spring:message code="operations.reports.label.EndTime" text="default text"/></th>
                                        <th  align="center"><spring:message code="operations.reports.label.TripStatus" text="default text"/></th>
                                        <th  align="center"><spring:message code="operations.reports.label.EstimatedRevenue" text="default text"/></th>
                                        <th  align="center"><spring:message code="operations.reports.label.EstimatedExpense(RCM)" text="default text"/></th>
                                        <th  align="center"><spring:message code="operations.reports.label.ActualExpense" text="default text"/></th>
                                        <th  align="center"><spring:message code="operations.reports.label.VehicleNo" text="default text"/></th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <% int index = 1;%>
                                    <c:set var="rechargeAmount" value="${0}" />
                                    <c:set var="transactionAmount" value="${0}" />
                                    <c:forEach items="${ToPayCustomerTripDetails}" var="BPCLTD">
                                        <%
                                                    String classText = "";
                                                    int oddEven = index % 2;
                                                    if (oddEven > 0) {
                                                        classText = "text2";
                                                    } else {
                                                        classText = "text1";
                                                    }
                                        %>
                                        <tr>
                                            <td class="<%=classText%>"><%=index++%></td>
                                            <td  width="30" class="<%=classText%>"><c:out value="${BPCLTD.tripCode}"/></td>
                                            <td  width="30" class="<%=classText%>"><c:out value="${BPCLTD.customerName}"/></td>
                                            <td width="30" class="<%=classText%>"><c:out value="${BPCLTD.customerType}"/></td>
                                            <td width="30" class="<%=classText%>" ><c:out value="${BPCLTD.routeInfo}"/></td>
                                            <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.tripstarttime}"/></td>
                                            <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.tripendtime}"/></td>
                                            <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.tripStatus}"/></td>
                                            <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.estimatedRevenue}"/>

                                            </td>
                                            <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.estimatedExpenses}"/></td>
                                            <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.actualExpense}"/></td>
                                            <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.regNo}"/></td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </c:if>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>

