<%@ include file="/content/common/NewDesign/header.jsp" %>
   <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import = "ets.domain.users.business.LoginTO" %>
<%@ page import="java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">

<script language="javascript" src="/throttle/js/validate.js"></script>
<script>
function submitpage(value)
{
var checValidate = selectedItemValidation();
var splt = checValidate.split("-");
if(splt[0]=='SubmitForm' && splt[1]!=0 ){
document.deleteUsrs.action='/throttle/modifyuser.do';
document.deleteUsrs.submit();
}
}


function setSelectbox(i)
{
var selected=document.getElementsByName("selectedIndex") ;
selected[i].checked = 1;
}


function setSelectbox(i)
{
var selected=document.getElementsByName("selectedUsers") ;
selected[i].checked = 1;
}

function selectedItemValidation(){
var index = document.getElementsByName("selectedUsers");
var userNames = document.getElementsByName("userNames");
var activeInds = document.getElementsByName("activeInds");
var chec=0;
var mess = "SubmitForm";
for(var i=0;(i<index.length && index.length!=0);i++){
if(index[i].checked){
chec++;
if(isEmpty(userNames[i].value)){
alert ("Enter The User Name");
userNames[i].focus();
mess = "";
break;
}
}
}
if(chec == 0){
alert("Please Select Any One And Then Proceed");
userNames[0].focus();
//break;
}
return mess+"-"+chec;
}

</script>

<!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/ 
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->
<div class="pageheader">
      <h2><i class="fa fa-edit"></i><spring:message code="settings.label.AlterUser" text="default text"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="header.label.YouAreHere" text="default text"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="header.label.Home" text="default text"/></a></li>
          <li><a href="general-forms.html"><spring:message code="header.label.Setting" text="default text"/></a></li>
          <li class=""><spring:message code="settings.label.AlterUser" text="default text"/> </li>
                  </ol>
      </div>
      </div>
<div class="contentpanel">
            <div class="panel panel-default">
             <div class="panel-body">
<body >

<form method="post" name="deleteUsrs">

<!-- copy there from end -->


<!-- message table -->

<%@ include file="/content/common/message.jsp" %>
<!-- message table -->
<!-- copy there  end -->
<!--<table class="table table-bordered">-->

  <table  class="table table-info mb30 table-hover">
    <thead>
<tr>
<th  height="30"><div ><spring:message code="settings.label.SNo" text="default text"/></div></th>
<th  height="30"><div ><spring:message code="settings.label.UserId" text="default text"/></div></th>
<th  height="30"><div ><spring:message code="settings.label.Username" text="default text"/></div></th>
<th  height="30"><div ><spring:message code="settings.label.Status" text="default text"/></div></th>
<th  height="30"><div ><spring:message code="settings.label.Select" text="default text"/></div></th>
</tr>
    </thead>
<% 
int sno = 1;
int index = 0;
ArrayList usernames = (ArrayList) session.getAttribute("userList");
%>
<c:if test = "${userList != null}">
<c:forEach items="${userList}" var="user"> 


<%

String classText = "";
int oddEven = index % 2;
if (oddEven > 0) {
classText = "text2";
} else {
classText = "text1";
}
%>
<tr>

<td height="30" ><%= sno %></td>
<td height="30" ><input type="hidden" name="userIds" value='<c:out value="${user.userId}"/>'> <div align="center"><c:out value="${user.userId}"/></div> </td>
<td height="30" ><input type="text" style="width:260px;height:40px;"  class="form-control" onChange="setSelectbox(<%= index %>)" name="userNames" class="textbox" value='<c:out value="${user.userName}"/>'></td>
<td height="30" > <div align="center"><select onChange="setSelectbox(<%= index %>)" name="activeInds" style="width:260px;height:40px;"  class="form-control">
<c:choose>
<c:when test="${user.status == 'Y'}">
<option value="Y" selected><spring:message code="settings.label.Active" text="default text"/></option>
<option value="N"><spring:message code="settings.label.InActive" text="default text"/></option>
</c:when>
<c:otherwise>
<option value="Y"><spring:message code="settings.label.Active" text="default text"/></option>
<option value="N" selected><spring:message code="settings.label.InActive" text="default text"/></option>
</c:otherwise>
</c:choose>
</select></div></td>
<td  height="30"> <input type="checkbox"  onchange="setSelectbox(<%= index %>)" name="selectedUsers" value= '<%= index %>' > </td>
</tr>
<%
sno++;
index++;

%>
</c:forEach>
</c:if> 

</td>
</table>

<center>

<input type="button" class="btn btn-success" name="save" value="<spring:message code="settings.label.SAVE" text="default text"/>" onClick="submitpage(this.name)" class="button" />
<input type="hidden" name="reqfor" value="" />
</center>

<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>