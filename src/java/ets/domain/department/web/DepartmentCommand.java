/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.department.web;

/**
 *
 * @author vinoth
 */
public class DepartmentCommand {
  private String toMail = "";
    private String ccMail = "";
    private String[] toMails;
    private String[] ccMails;
    private String departmentId = "";
    private String name = "";
    private String description = "";
    private String status = "";
    private String[] departmentIds;
    private String[] names;
    private String[] descriptions;
    private String[] stats;
     private String[] selectedIndex = null;

    public String[] getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(String[] selectedIndex) {
        this.selectedIndex = selectedIndex;
    }
     

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String[] getDepartmentIds() {
        return departmentIds;
    }

    public void setDepartmentIds(String[] departmentIds) {
        this.departmentIds = departmentIds;
    }

    public String[] getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String[] descriptions) {
        this.descriptions = descriptions;
    }

    public String[] getNames() {
        return names;
    }

    public void setNames(String[] names) {
        this.names = names;
    }

    public String[] getStats() {
        return stats;
    }

    public void setStats(String[] stats) {
        this.stats = stats;
    }

    public String getToMail() {
        return toMail;
    }

    public void setToMail(String toMail) {
        this.toMail = toMail;
    }

    public String getCcMail() {
        return ccMail;
    }

    public void setCcMail(String ccMail) {
        this.ccMail = ccMail;
    }

    public String[] getToMails() {
        return toMails;
    }

    public void setToMails(String[] toMails) {
        this.toMails = toMails;
    }

    public String[] getCcMails() {
        return ccMails;
    }

    public void setCcMails(String[] ccMails) {
        this.ccMails = ccMails;
    }
    
}