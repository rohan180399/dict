<%--
    Document   : vehicleAvailabilityForEmptyTrip
    Created on : Dec 5, 2013, 12:31:54 PM
    Author     : admin
--%>


<%@page import="java.text.SimpleDateFormat" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">


            function viewCurrentLocation(vehicleRegNo) {
                var url = 'http://ivts.noviretechnologies.com/IVTS/reportEngServ.do?username=serviceuser&password=7C53C003126C10E1091C73F4F945FEB4&companyId=759&reportRef=ref_currentStatusMapServ&param0=759&param1=ref_currentStatusMapServ&param2=&param3=&param4=&param5=&param6=&param7=&param8=&param9=&param10=&param11=' + vehicleRegNo;
                //alert(url);
                window.open(url, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
            }

            function viewTripDetails(vehicleId, vehicleNo) {
                $("#vehicleId").val(vehicleId);
                $("#vehicleNo").val(vehicleNo);
                document.vehicleAvailable.action = '/throttle/createEmptyTrip.do';
                document.vehicleAvailable.submit();
            }


            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>


        <script type="text/javascript">

            function show_src() {
                document.getElementById('exp_table').style.display = 'none';
            }

            function show_exp() {
                document.getElementById('exp_table').style.display = 'block';
            }

            function show_close() {
                document.getElementById('exp_table').style.display = 'none';
            }
        </script>
        <script type="text/javascript">
            var httpRequest;
            function getLocation() {
                var zoneid = document.getElementById("zoneId").value;
                if (zoneid != '') {

                    // Use the .autocomplete() method to compile the list based on input from user
                    $('#cityFrom').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getLocationName.do",
                                dataType: "json",
                                data: {
                                    cityId: request.term,
                                    zoneId: document.getElementById('zoneId').value
                                },
                                success: function(data, textStatus, jqXHR) {
                                    var items = data;
                                    response(items);
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 1,
                        select: function(event, ui) {
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $('#cityFromId').val(tmp[0]);
                            $('#cityFrom').val(tmp[1]);
                            return false;
                        }
                    }).data("autocomplete")._renderItem = function(ul, item) {
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[1] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };

                }
            }

        </script>
        
    </head>
    <% String menuPath = "Operations >>  Empty Trip Planning";
                request.setAttribute("menuPath", menuPath);
    %>
    <body onload="setValues();">
        <form name="vehicleAvailable">
            


            <br>
            <br>
            <a href="content/common/menuResponsive.jsp">menu</a>
            <br>
            <c:if test="${vehicleNos == null }" >
                <br>
                <center><font color="red" size="2"> no records found </font></center>
                </c:if>
                <c:if test = "${vehicleNos != null}">
            <table  border="1" align="center" cellpadding="0" cellspacing="0" id="table" class="sortable" style="text-align: center">
                <thead>
                <input type="hidden" name="vehicleId" id="vehicleId" value=""/>
                <input type="hidden" name="vehicleNo" id="vehicleNo" value=""/>
                <tr >
                    <th  class="contentsub" style="text-align: center">Vehicle No</th>
                    <th  class="contentsub" style="text-align: center">Primary Driver</th>
                    <th  class="contentsub" style="text-align: center">Create Trip</th>
                </tr>
                </thead>
                
                    <% int sno = 0;%>   
                    <c:forEach items="${vehicleNos}" var="val">
                        <%
                                    sno++;
                                    String className = "text1";
                                    if ((sno % 1) == 0) {
                                        className = "text1";
                                    } else {
                                        className = "text2";
                                    }
                        %>
                        <input type="hidden" name="respStatus" value="Y" />
                            <c:if test="${val.primaryDriverName != null}">
                            <tr>
                                <td class="<%=className%>"  align="left" width="90"> <c:out value="${val.vehicleNo}"/></td>
                                <td class="<%=className%>"  align="left" width="90"> <c:out value="${val.primaryDriverName}"/></td>
                                <c:if test="${val.primaryDriverId == '0' && val.primaryDriverName == 'Driver Not Mapped'}">

                                <td class="<%=className%>"  align="left" width="90">Please Map Driver</td>
                                </c:if>
                                <c:if test="${val.primaryDriverId != '0'}">
                                <td class="<%=className%>"  align="left">
                                     <a href="#" onclick="viewTripDetails('<c:out value="${val.vehicleId}"/>','<c:out value="${val.vehicleNo}"/>');">Create Empty Trip</a>
                                </td>
                                </c:if>
                            </tr>
                            </c:if>
                    </c:forEach>
                    </tbody>
                    <input type="hidden" name="count" id="count" value="<%=sno%>" />
            </table>
            </c:if>



            <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>