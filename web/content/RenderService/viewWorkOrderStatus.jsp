
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="/throttle/js/validate.js"></script>  
<%@ page import="ets.domain.operation.business.OperationTO" %>      
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
</head>


<body>
<form name="workOrder" method="post">
    
<%@ include file="/content/common/path.jsp" %>

<!-- pointer table -->

<!-- message table -->

<%@ include file="/content/common/message.jsp"%>

    <c:if test = "${vehicleDetails != null}" >    
<table align="center" border="0" cellpadding="0" cellspacing="0" width="400" id="bg" class="border">
    
<tr>
<td class="contenthead" colspan="4" align="center" height="30"><div class="contenthead">Vehicle Details</div></td>
</tr>
<c:forEach items="${vehicleDetails}" var="fservice"> 
  
<tr>
<td class="text2" height="30">Vehicle Number</td>
<td class="text2" height="30"></td>
<td class="text2" height="30"><c:out value="${fservice.regno}"/></td>
<td class="text2" height="30"></td>
</tr>

<tr>
<td class="text1" height="30">Vehicle Type:</td>
<td class="text1" height="30"><c:out value="${fservice.vehicleTypeName}"/></td>
<td class="text1" height="30">MFG:</td>
<td class="text1" height="30"><c:out value="${fservice.mfrName}"/></td>
</tr>

<tr>
<td class="text2" height="30">Use Type:</td>
<td class="text2" height="30"><c:out value="${fservice.usageName}"/></td>
<td class="text2" height="30">Model:</td>
<td class="text2" height="30"><c:out value="${fservice.modelName}"/></td>
</tr>

<tr>
<td class="text1" height="30">Engine No:</td>
<td class="text1" height="30"><c:out value="${fservice.engineNo}"/></td>
<td class="text1" height="30">Chasis No:</td>
<td class="text1" height="30"><c:out value="${fservice.chassNo}"/></td>
</tr>
</c:forEach>
</table>
</c:if>


      <c:if test = "${workOrderDetails != null}" >      
<table align="center" border="0" cellpadding="0" cellspacing="0" width="400" id="bg" class="border">
<tr>
<td colspan="2" class="contenthead" height="30"><div class="contenthead">Work Order</div></td>
</tr>
<c:forEach items="${workOrderDetails}" var="service"> 
<tr>
<td class="text2" height="30">Work Order No:</td>
<input name="workOrderId" type="hidden" value='<%=request.getAttribute("workOrderId")%>'>
<td class="text2" height="30"><%=request.getAttribute("workOrderId")%></td>
</tr>

<tr>
<td class="text2" height="30">Date of Issue</td>
<td class="text2" height="30"><c:out value="${service.dateOfIssue}"/></td>
</tr>
<tr>
<td class="text1" height="30">Place of Issue</td>
<td class="text1" height="30"><c:out value="${service.compName}"/></td>
</tr>
<tr>
<td class="text2" height="30">Required Date/Time of delivery</td>
<td class="text2" height="30"><c:out value="${service.reqDate}"/></td>
</tr>
<tr>
<td class="text1" height="30">Km Reading Before leaving</td>
<td class="text1" height="30"><c:out value="${service.kmReading}"/></td>
</tr>
<tr>
<td class="text2" height="30">Service Location</td>
<td class="text2" height="30"><%=session.getAttribute("companyName")%></td>
</tr>
<tr>
<td class="text1" height="30">Driver </td>
<td class="text1" height="30"><c:out value="${service. driverName}"/></td>
</tr>

</c:forEach>
</table
></c:if>
<br>
<%
    
    int index=0;
    %>
<c:if test = "${workOrderProblemDetails != null}" >      
<table align="center"  border="0" cellpadding="0" cellspacing="0" width="600" >
<tr>
<td colspan="6" align="center" class="contenthead" height="30">Complaints</td>
</tr>

<tr>
<td  height="30" class="contentsub">S.No</td>
<td  height="30" class="contentsub">Section</td>
<td  height="30" class="contentsub">Fault</td>
<td  height="30" class="contentsub">Description</td>
<td  height="30" class="contentsub">Symptoms</td>
<td  height="30" class="contentsub">Severity</td>
</tr>
<c:forEach items="${workOrderProblemDetails}" var="prob"> 
<%  
String classText = "";
int oddEven = index % 2;
if (oddEven > 0) {
classText = "text1";
} else {
classText = "text2";
}
%>
  <tr>
      <td height="30" class="<%=classText%>"><%=index+1%></td>
      <td height="30" class="<%=classText%>"><c:out value="${prob.secName}"/></td>
      <td height="30" class="<%=classText%>"><c:out value="${prob.probName}"/></td>
    <td height="30" class="<%=classText%>"><c:out value="${prob.desc}"/></td>
    <td height="30" class="<%=classText%>"><c:out value="${prob.symptoms}"/></td>
    <c:if test = "${prob.severity ==1}" >      
    <td height="30" class="<%=classText%>">Low</td>
    </c:if>      
    <c:if test = "${prob.severity ==2}" >
    <td height="30" class="<%=classText%>">Medium</td>
    </c:if>      
    <c:if test = "${prob.severity ==3}" >
    <td height="30" class="<%=classText%>">High</td>
    </c:if>      
  </tr>
  <%
  index++;
  %>
</c:forEach>

</table>
</c:if>
<br>


<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
<br>
</body>
</html>
