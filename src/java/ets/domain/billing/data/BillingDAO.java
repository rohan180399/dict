/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.billing.data;

/**
 *
 * @author vinoth
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import ets.domain.util.FPLogUtils;
import ets.arch.exception.FPRuntimeException;
import ets.domain.billing.business.BillingTO;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.ibatis.sqlmap.client.SqlMapClient;

public class BillingDAO extends SqlMapClientDaoSupport {

    private final int errorStatus = 4;
    private final static String CLASS = "LoginDAO";

    public BillingDAO() {
    }

    public ArrayList getOrdersForBilling(BillingTO billingTO) {
        Map map = new HashMap();
        ArrayList clisedTrips = null;

        try {
            map.put("fromDate", billingTO.getFromDate());
            map.put("toDate", billingTO.getToDate());
            if (!"".equals(billingTO.getCustomerName())) {
                billingTO.setCustomerName(billingTO.getCustomerName() + "%");
            }
            map.put("custName", billingTO.getCustomerName());
            map.put("userId", billingTO.getUserId());
            map.put("customerId", billingTO.getCustomerId());
            map.put("billOfEntry", billingTO.getBillOfEntryNo());
            map.put("shippingBillNo", billingTO.getShipingBillNo());
            map.put("billingType", billingTO.getBillingType());
            map.put("movementType", billingTO.getMovementType());
            System.out.println("closed Trip map is::" + map);
//            if(billingTO.getCustomerId() != null){
//            }
            clisedTrips = new ArrayList();
            clisedTrips = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getOrdersForBilling", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getClosedTrips Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getClosedTrips List", sqlException);
        }
        return clisedTrips;
    }

    public ArrayList getOrdersForSupplementBilling(BillingTO billingTO) {
        Map map = new HashMap();
        ArrayList clisedTrips = null;

        try {
            map.put("customerId", billingTO.getCustomerId());
            map.put("fromDate", billingTO.getFromDate());
            map.put("toDate", billingTO.getToDate());
            map.put("tripType", billingTO.getBillingType());
            if (billingTO.getShipingBillNo() != null && !"".equals(billingTO.getShipingBillNo())) {
                map.put("billNo", "%" + billingTO.getShipingBillNo() + "%");
            } else {
                map.put("billNo", "");
            }
            if (billingTO.getBillOfEntryNo() != null && !"".equals(billingTO.getBillOfEntryNo())) {
                map.put("grNo", "%" + billingTO.getBillOfEntryNo() + "%");
            } else {
                map.put("grNo", "");
            }
            System.out.println("getClosedbill Value map" + map);
            System.out.println("closed Trip map is::" + map);
            clisedTrips = new ArrayList();
            clisedTrips = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getOrdersForSupplementBilling", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getClosedTrips Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getClosedTrips List", sqlException);
        }
        return clisedTrips;
    }

    public ArrayList getRepoOrdersForBilling(BillingTO billingTO) {
        Map map = new HashMap();
        ArrayList clisedTrips = null;

        try {
            map.put("fromDate", billingTO.getFromDate());
            map.put("toDate", billingTO.getToDate());
            if (!"".equals(billingTO.getCustomerName())) {
                billingTO.setCustomerName(billingTO.getCustomerName() + "%");
            }
            map.put("custName", billingTO.getCustomerName());
            map.put("userId", billingTO.getUserId());
            map.put("customerId", billingTO.getCustomerId());
            map.put("billOfEntry", billingTO.getBillOfEntryNo());
            map.put("shippingBillNo", billingTO.getShipingBillNo());
            map.put("billingType", billingTO.getBillingType());
            map.put("movementType", billingTO.getMovementType());
            if (!"".equals(billingTO.getGrNo())) {
                billingTO.setGrNo("%" + billingTO.getGrNo() + "%");
            }
            map.put("grNo", billingTO.getGrNo());
            System.out.println("closed Trip map is::" + map);
//            if(billingTO.getCustomerId() != null){
//            }
            clisedTrips = new ArrayList();
            clisedTrips = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getRepoOrdersForBilling", map);
            System.out.println("clisedTrips : " + clisedTrips.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getClosedTrips Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getClosedTrips List", sqlException);
        }
        return clisedTrips;
    }

    public ArrayList getOrdersToBeBillingDetails(BillingTO billingTO) {
        Map map = new HashMap();

        String[] tripId = billingTO.getTripSheetId().split(",");
        List tripIds = new ArrayList(tripId.length);
        for (int i = 0; i < tripId.length; i++) {
            System.out.println("value:" + tripId[i]);
            tripIds.add(tripId[i]);
        }
        map.put("tripIds", tripIds);
        ArrayList orderBillingDetails = new ArrayList();
        try {
            orderBillingDetails = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getOrdersToBeBillingDetails", map);
            System.out.println("tripDetails size=" + orderBillingDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return orderBillingDetails;
    }

    public ArrayList getOrdersToBeSupplementBillingDetails(BillingTO billingTO) {
        Map map = new HashMap();

        String[] tripId = billingTO.getTripSheetId().split(",");
        List tripIds = new ArrayList(tripId.length);
        for (int i = 0; i < tripId.length; i++) {
            System.out.println("value:" + tripId[i]);
            tripIds.add(tripId[i]);
        }
        map.put("tripIds", tripIds);
        System.out.println("tripIds======++++"+tripIds);
        ArrayList orderBillingDetails = new ArrayList();
        try {
            orderBillingDetails = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getOrdersToBeSupplementBillingDetails", map);
            System.out.println("tripDetails size=" + orderBillingDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return orderBillingDetails;
    }

    public ArrayList getOrdersToBeBillingDetails1(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();

        String[] tripId = billingTO.getTripSheetId().split(",");
        List tripIds = new ArrayList(tripId.length);
        for (int i = 0; i < tripId.length; i++) {
            System.out.println("value:" + tripId[i]);
            tripIds.add(tripId[i]);
        }
        map.put("tripIds", tripIds);
        ArrayList orderBillingDetails = new ArrayList();
        try {
            orderBillingDetails = (ArrayList) session.queryForList("billing.getOrdersToBeBillingDetails1", map);
            System.out.println("tripDetails size=" + orderBillingDetails.size());

//  int invoiceUpdate = (Integer) getSqlMapClientTemplate().update("billing.invoiceUpdate", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return orderBillingDetails;
    }

    public ArrayList getOrdersToBeSuppBillingDetails1(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();

        String[] tripId = billingTO.getTripSheetId().split(",");
        List tripIds = new ArrayList(tripId.length);
        for (int i = 0; i < tripId.length; i++) {
            System.out.println("value:" + tripId[i]);
            tripIds.add(tripId[i]);
        }
        map.put("tripIds", tripIds);
        ArrayList orderBillingDetails = new ArrayList();
        try {
            orderBillingDetails = (ArrayList) session.queryForList("billing.getOrdersToBeSuppBillingDetails1", map);
            System.out.println("tripDetails size=" + orderBillingDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return orderBillingDetails;
    }
//rohan

    public String getCustGtaType(BillingTO billingTO) {
        Map map = new HashMap();
        String getCustGtaType = " ";

        try {
            map.put("custId", billingTO.getCustomerId());
            System.out.println("map--------------------------" + map);
            getCustGtaType = (String) getSqlMapClientTemplate().queryForObject("billing.getCustGtaType", map);
            System.out.println("getCustGtaType size=----------------------------------------" + getCustGtaType);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return getCustGtaType;
    }

    public String getBillingTotalRevenue(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();

        String[] tripId = billingTO.getTripSheetId().split(",");
        List tripIds = new ArrayList(tripId.length);
        for (int i = 0; i < tripId.length; i++) {
            System.out.println("value:" + tripId[i]);
            tripIds.add(tripId[i]);
        }
        map.put("tripIds", tripIds);
        String totalRevenue = "";
        try {
            totalRevenue = (String) session.queryForObject("billing.getBillingTotalRevenue", map);
            System.out.println("tripDetails size=" + totalRevenue);
            if (totalRevenue == null) {
                totalRevenue = "0";
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return totalRevenue;
    }

    public String getBillingTotalExpense(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();

        String[] tripId = billingTO.getTripSheetId().split(",");
        List tripIds = new ArrayList(tripId.length);
        for (int i = 0; i < tripId.length; i++) {
            System.out.println("value:" + tripId[i]);
            tripIds.add(tripId[i]);
        }
        map.put("tripIds", tripIds);
        String totalExpense = "";
        try {
            totalExpense = (String) session.queryForObject("billing.getBillingTotalExpense", map);
            System.out.println("tripDetails size=" + totalExpense);
            if (totalExpense == null) {
                totalExpense = "0";
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return totalExpense;
    }

    public String getSuppBillingTotalExpense(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();

        String[] tripId = billingTO.getTripSheetId().split(",");
        List tripIds = new ArrayList(tripId.length);
        for (int i = 0; i < tripId.length; i++) {
            System.out.println("value:" + tripId[i]);
            tripIds.add(tripId[i]);
        }
        map.put("tripIds", tripIds);
        String totalExpense = "";
        try {
            totalExpense = (String) session.queryForObject("billing.getSuppBillingTotalExpense", map);
            System.out.println("getSuppBillingTotalExpense size=" + totalExpense);
            if (totalExpense == null) {
                totalExpense = "0";
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return totalExpense;
    }

    public ArrayList getOrderOtherExpenseDetails(BillingTO billingTO) {
        Map map = new HashMap();

        map.put("tripId", billingTO.getTripId());

        String[] tripId = billingTO.getTripSheetId().split(",");
        List tripIds = new ArrayList(tripId.length);
        for (int i = 0; i < tripId.length; i++) {
            System.out.println("value:" + tripId[i]);
            tripIds.add(tripId[i]);
        }
        map.put("tripIds", tripIds);

        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getOrderOtherExpenseDetails", map);
            System.out.println("tripDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getOrdersSupplementOtherExpenseDetails(BillingTO billingTO) {
        Map map = new HashMap();

        map.put("tripId", billingTO.getTripId());

        String[] tripId = billingTO.getTripSheetId().split(",");
        List tripIds = new ArrayList(tripId.length);
        for (int i = 0; i < tripId.length; i++) {
            System.out.println("value:" + tripId[i]);
            tripIds.add(tripId[i]);
        }
        map.put("tripIds", tripIds);

        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getOrdersSupplementOtherExpenseDetails", map);
            System.out.println("tripDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getOrderOtherExpenseDetails1(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();

        map.put("tripId", billingTO.getTripId());
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) session.queryForList("billing.getOrderOtherExpenseDetails1", map);
            System.out.println("tripDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getOrderOtherExpenseSupplementaryDetails(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();

        map.put("tripId", billingTO.getTripId());
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) session.queryForList("billing.getOrderOtherExpenseSupplementaryDetails", map);
            System.out.println("tripDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getTripDetails(BillingTO billingTO) {
        Map map = new HashMap();
        map.put("invoiceId", billingTO.getInvoiceId());
        ArrayList tripDetails = new ArrayList();
        System.out.println("map:---" + map);
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getTripDetails", map);
            System.out.println("tripDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return tripDetails;
    }

    public int saveBillHeader(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();

        map.put("fromDate", billingTO.getFromDate());
        map.put("billingPartyId", billingTO.getBillingPartyId());
        map.put("userId", billingTO.getUserId());
        map.put("invoiceCode", billingTO.getInvoiceCode());
        map.put("invoiceNo", billingTO.getInvoiceNo());
        map.put("customerId", billingTO.getCustomerId());
        map.put("billType", "1");
        map.put("noOfTrips", billingTO.getNoOfTrips());
        map.put("grandTotal", billingTO.getGrandTotal());
        map.put("totalRevenue", billingTO.getTotalRevenue());
        map.put("totalExpToBeBilled", billingTO.getTotalExpToBeBilled());
        map.put("orderCount", billingTO.getOrderCount());
        map.put("remarks", billingTO.getRemarks());
        map.put("totalTax", billingTO.getTotalTax());
        map.put("customerAddress", billingTO.getCustomerAddress());
        //   map.put("grandTotal", billingTO.getGrandTotal());
        map.put("otherExpense", billingTO.getOtherExpense());
        map.put("gstNo", billingTO.getGstNo());
        map.put("panNo", billingTO.getPanNo());
        map.put("gstType", billingTO.getGstType());
        System.out.println("the saveBillHeader:" + map);
        int status = 0;
        try {
            status = (Integer) session.insert("billing.saveBillHeader", map);
            System.out.println("saveBillHeader size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveBillHeader Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveBillHeader List", sqlException);
        }

        return status;
    }

    public int saveSupplementBillHeader(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();
        map.put("invoiceId", billingTO.getInvoiceId());
        map.put("fromDate", billingTO.getFromDate());
        map.put("billingPartyId", billingTO.getBillingPartyId());
        map.put("userId", billingTO.getUserId());
        map.put("suppInvoiceCode", billingTO.getSuppInvoiceNo());
        map.put("invoiceNo", billingTO.getInvoiceNo());
        map.put("customerId", billingTO.getCustomerId());
        map.put("billType", "1");
        map.put("noOfTrips", billingTO.getNoOfTrips());
        map.put("grandTotal", billingTO.getGrandTotal());
        map.put("totalRevenue", billingTO.getTotalRevenue());
        map.put("totalExpToBeBilled", billingTO.getTotalExpToBeBilled());
        map.put("orderCount", billingTO.getOrderCount());
        map.put("remarks", billingTO.getRemarks());
        map.put("totalTax", billingTO.getTotalTax());
        map.put("customerAddress", billingTO.getCustomerAddress());
        //   map.put("grandTotal", billingTO.getGrandTotal());
        map.put("otherExpense", billingTO.getOtherExpense());
        map.put("gstNo", billingTO.getGstNo());
        map.put("panNo", billingTO.getPanNo());
        map.put("gstType", billingTO.getGstType());
        System.out.println("the saveSupplementBillHeader:" + map);
        int status = 0;
        try {
            status = (Integer) session.insert("billing.saveSupplementBillHeader", map);
            System.out.println("saveSupplementBillHeader size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveBillHeader Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveBillHeader List", sqlException);
        }

        return status;
    }

    public int saveBillDetails(BillingTO billingTO, BillingTO billingTO1, SqlMapClient session) {
        Map map = new HashMap();

        map.put("userId", billingTO.getUserId());
        map.put("invoiceNo", billingTO.getInvoiceNo());
        map.put("invoiceCode", billingTO.getInvoiceCode());
        map.put("invoiceId", billingTO.getInvoiceId());
        map.put("tripId", billingTO.getTripId());
        map.put("customerId", billingTO.getCustomerId());
        map.put("totalRevenue", billingTO.getEstimatedRevenue());
        map.put("totalExpToBeBilled", billingTO.getExpenseToBeBilledToCustomer());
        map.put("grandTotal", String.valueOf(Double.parseDouble(billingTO.getEstimatedRevenue()) + Double.parseDouble(billingTO.getExpenseToBeBilledToCustomer())));
        map.put("otherExpense", billingTO.getOtherExpense());

        System.out.println("the saveBillDetails:" + map);
        int status = 0;
        double taxPercent = 5.00;
        double taxPercentTmp = 0.00;
        double grTotal = 0.00;
        String gTotal = String.valueOf(Double.parseDouble(billingTO.getEstimatedRevenue()) + Double.parseDouble(billingTO.getExpenseToBeBilledToCustomer()));
        try {
            status = (Integer) session.insert("billing.saveBillDetails", map);
            System.out.println("saveBillDetails size=" + status);
            System.out.println("billingTO1.getOrganizationId()-Detail-" + billingTO1.getOrganizationId());
            System.out.println("billingTO.getGstNo()--Detail--" + billingTO1.getGstNo());
            System.out.println("billingTO.getBillingState()--Detail--" + billingTO1.getBillingState());
            System.out.println("billingTO.billingTO.getGstType()--Detail--" + billingTO1.getGstType());

            if ("Y".equals(billingTO1.getGstType()) && "1".equalsIgnoreCase(billingTO1.getOrganizationId())) {
                if ("1".equalsIgnoreCase(billingTO1.getOrganizationId()) && "".equalsIgnoreCase(billingTO1.getGstNo()) && "9".equalsIgnoreCase(billingTO1.getBillingState())) {

                    taxPercentTmp = taxPercent / 2;
                    map.put("gstType", "CGST");
                    map.put("cgstTaxAmount", ((Double.parseDouble(gTotal))) * (taxPercentTmp / 100));
                    map.put("taxPercentage", taxPercentTmp);
                    System.out.println("the tax map ccgst:" + map);

                    map.put("gstType", "SGST");
                    map.put("sgstTaxAmount", ((Double.parseDouble(gTotal))) * (taxPercentTmp / 100));
                    map.put("taxPercentage", taxPercentTmp);
                    map.put("igstTaxAmount", "0.00");

                    grTotal = (Double.parseDouble(gTotal)) + (Double.parseDouble(gTotal)) * (taxPercent / 100);
                    map.put("grTotal", grTotal);
                    System.out.println("the tax map sgst:" + map);
                    status = (Integer) session.update("billing.updateInvDetail", map);

                    System.out.println("updateInvDetail----" + status);

                } else if ("1".equalsIgnoreCase(billingTO1.getOrganizationId()) && "".equalsIgnoreCase(billingTO1.getGstNo()) && !"9".equalsIgnoreCase(billingTO1.getBillingState())) {

                    map.put("gstType", "IGST");
                    map.put("cgstTaxAmount", "0.00");
                    map.put("sgstTaxAmount", "0.00");
                    map.put("igstTaxAmount", ((Double.parseDouble(gTotal))) * (taxPercent / 100));
                    map.put("taxPercentage", taxPercent);

                    grTotal = (Double.parseDouble(gTotal)) + (Double.parseDouble(gTotal)) * (taxPercent / 100);
                    map.put("grTotal", grTotal);
                    System.out.println("the tax map:" + map);
                    status = (Integer) session.update("billing.updateInvDetail", map);
                    System.out.println("updateInvDetail----" + status);

                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveBillDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveBillDetails List", sqlException);
        }

        return status;
    }

    public int saveSupplementBillDetails(BillingTO billingTO, BillingTO billingTO1, SqlMapClient session) {
        Map map = new HashMap();

        map.put("userId", billingTO.getUserId());
        map.put("invoiceNo", billingTO.getInvoiceNo());
        map.put("invoiceCode", billingTO.getInvoiceCode());
        map.put("invoiceId", billingTO.getInvoiceId());
        map.put("tripId", billingTO.getTripId());
        map.put("customerId", billingTO.getCustomerId());
        map.put("totalRevenue", billingTO.getTotalRevenue());
        map.put("totalExpToBeBilled", billingTO.getExpenseToBeBilledToCustomer());
        map.put("grandTotal", billingTO.getTotalRevenue());
        map.put("otherExpense", billingTO.getOtherExpense());

        System.out.println("the saveBillDetails:" + map);
        int status = 0;
        double taxPercent = 5.00;
        double taxPercentTmp = 0.00;
        double grTotal = 0.00;
        String gTotal = billingTO.getTotalRevenue();
        System.out.println("the saveSupplementBillDetails:" + map);
        try {
            status = (Integer) session.insert("billing.saveSupplementBillDetails", map);
            System.out.println("saveSupplementBillDetails size=" + status);

            System.out.println("saveBillDetails size=" + status);
            System.out.println("billingTO1.getOrganizationId()-Detail-" + billingTO1.getOrganizationId());
            System.out.println("billingTO.getGstNo()--Detail--" + billingTO1.getGstNo());
            System.out.println("billingTO.getBillingState()--Detail--" + billingTO1.getBillingState());
            System.out.println("billingTO.billingTO.getGstType()--Detail--" + billingTO1.getGstType());

            if ("Y".equals(billingTO1.getGstType()) && "1".equalsIgnoreCase(billingTO1.getOrganizationId())) {
                if ("1".equalsIgnoreCase(billingTO1.getOrganizationId()) && "".equalsIgnoreCase(billingTO1.getGstNo()) && "9".equalsIgnoreCase(billingTO1.getBillingState())) {

                    taxPercentTmp = taxPercent / 2;
                    map.put("gstType", "CGST");
                    map.put("cgstTaxAmount", ((Double.parseDouble(gTotal))) * (taxPercentTmp / 100));
                    map.put("taxPercentage", taxPercentTmp);
                    System.out.println("the tax map ccgst:" + map);

                    map.put("gstType", "SGST");
                    map.put("sgstTaxAmount", ((Double.parseDouble(gTotal))) * (taxPercentTmp / 100));
                    map.put("taxPercentage", taxPercentTmp);
                    map.put("igstTaxAmount", "0.00");

                    grTotal = (Double.parseDouble(gTotal)) + (Double.parseDouble(gTotal)) * (taxPercent / 100);
                    map.put("grTotal", grTotal);
                    System.out.println("the tax map sgst:" + map);
                    status = (Integer) session.update("billing.updateSuppInvDetail", map);

                    System.out.println("updateSuppInvDetail----" + status);

                } else if ("1".equalsIgnoreCase(billingTO1.getOrganizationId()) && "".equalsIgnoreCase(billingTO1.getGstNo()) && !"9".equalsIgnoreCase(billingTO1.getBillingState())) {

                    map.put("gstType", "IGST");
                    map.put("cgstTaxAmount", "0.00");
                    map.put("sgstTaxAmount", "0.00");
                    map.put("igstTaxAmount", ((Double.parseDouble(gTotal))) * (taxPercent / 100));
                    map.put("taxPercentage", taxPercent);

                    grTotal = (Double.parseDouble(gTotal)) + (Double.parseDouble(gTotal)) * (taxPercent / 100);
                    map.put("grTotal", grTotal);
                    System.out.println("the tax map:" + map);
                    status = (Integer) session.update("billing.updateSuppInvDetail", map);
                    System.out.println("updateSuppInvDetail----" + status);

                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveBillDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveBillDetails List", sqlException);
        }

        return status;
    }

    public int saveBillTax(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", billingTO.getUserId());
        map.put("invoiceId", billingTO.getInvoiceId());

        System.out.println("billingTO.getOrganizationId()--" + billingTO.getOrganizationId());
        System.out.println("billingTO.getGstNo()--" + billingTO.getGstNo());
        System.out.println("billingTO.getBillingState()--" + billingTO.getBillingState());
        System.out.println("billingTO.billingTO.getGstType()--" + billingTO.getGstType());
        Double taxPercent = 5.00;
        Double taxPercentTmp = 0.00;
        String gTotal = "0";
        if ("".equals(billingTO.getGrandTotal()) || billingTO.getGrandTotal() == null) {
            gTotal = "0.00";
        } else {
            gTotal = billingTO.getGrandTotal();
        }
        try {
            System.out.println("taxPercent-----" + taxPercent);
            System.out.println("billingTO.getGrandTotal()-----" + billingTO.getGrandTotal());
            System.out.println("Double.parseDouble(gTotal+taxPercent)-----" + Double.parseDouble(gTotal));
            map.put("gTotal", Double.parseDouble(gTotal));
            if ("Y".equals(billingTO.getGstType()) && "1".equalsIgnoreCase(billingTO.getOrganizationId())) {
                if ("1".equalsIgnoreCase(billingTO.getOrganizationId()) && "".equalsIgnoreCase(billingTO.getGstNo()) && "9".equalsIgnoreCase(billingTO.getBillingState())) {

                    map.put("gstType", "CGST");
                    taxPercentTmp = taxPercent / 2;
                    map.put("taxAmount", ((Double.parseDouble(gTotal))) * (taxPercentTmp / 100));
                    map.put("taxPercentage", taxPercentTmp);
                    System.out.println("the tax map ccgst:" + map);
                    status = (Integer) session.update("billing.saveInvoiceTax", map);
                    map.put("gstType", "SGST");
                    map.put("taxAmount", ((Double.parseDouble(gTotal))) * (taxPercentTmp / 100));
                    map.put("taxPercentage", taxPercentTmp);
                    System.out.println("the tax map sgst:" + map);
                    status = (Integer) session.update("billing.saveInvoiceTax", map);

                    map.put("taxAmount", ((Double.parseDouble(gTotal))) * (taxPercent / 100));
                    status = (Integer) session.update("billing.updateInvHeader", map);
                    System.out.println("updateInvHeader----" + status);

                } else if ("1".equalsIgnoreCase(billingTO.getOrganizationId()) && "".equalsIgnoreCase(billingTO.getGstNo()) && !"9".equalsIgnoreCase(billingTO.getBillingState())) {

                    map.put("gstType", "IGST");
                    map.put("taxAmount", billingTO.getTotalTax());

                    map.put("taxPercentage", taxPercent);
                    System.out.println("the tax map:" + map);
                    status = (Integer) session.update("billing.saveInvoiceTax", map);
                    status = (Integer) session.update("billing.updateInvHeader", map);
                    System.out.println("updateInvHeader----" + status);

                }

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveBillDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveBillDetails List", sqlException);
        }

        return status;
    }

    public int saveSupplementBillTax(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", billingTO.getUserId());
        map.put("invoiceId", billingTO.getInvoiceId());
        System.out.println("billingTO.getOrganizationId()--" + billingTO.getOrganizationId());
        System.out.println("billingTO.getGstNo()--" + billingTO.getGstNo());
        System.out.println("billingTO.getBillingState()--" + billingTO.getBillingState());
        System.out.println("billingTO.billingTO.getGstType()--" + billingTO.getGstType());
        Double taxPercent = 5.00;
        Double taxPercentTmp = 0.00;
        String gTotal = "0";
        if ("".equals(billingTO.getGrandTotal()) || billingTO.getGrandTotal() == null) {
            gTotal = "0.00";
        } else {
            gTotal = billingTO.getGrandTotal();
        }
        try {

            System.out.println("taxPercent-----" + taxPercent);
            System.out.println("billingTO.getGrandTotal()-----" + billingTO.getGrandTotal());
            System.out.println("Double.parseDouble(gTotal+taxPercent)-----" + Double.parseDouble(gTotal));
            map.put("gTotal", Double.parseDouble(gTotal));
            if ("Y".equals(billingTO.getGstType()) && "1".equalsIgnoreCase(billingTO.getOrganizationId())) {
                if ("1".equalsIgnoreCase(billingTO.getOrganizationId()) && "".equalsIgnoreCase(billingTO.getGstNo()) && "9".equalsIgnoreCase(billingTO.getBillingState())) {

                    map.put("gstType", "CGST");
                    taxPercentTmp = taxPercent / 2;
                    map.put("taxAmount", ((Double.parseDouble(gTotal))) * (taxPercentTmp / 100));
                    map.put("taxPercentage", taxPercentTmp);
                    System.out.println("the tax map ccgst:" + map);
                    status = (Integer) session.update("billing.saveSupplementInvoiceTax", map);
                    map.put("gstType", "SGST");
                    map.put("taxAmount", ((Double.parseDouble(gTotal))) * (taxPercentTmp / 100));
                    map.put("taxPercentage", taxPercentTmp);
                    System.out.println("the tax map sgst:" + map);
                    status = (Integer) session.update("billing.saveSupplementInvoiceTax", map);

                    map.put("taxAmount", ((Double.parseDouble(gTotal))) * (taxPercent / 100));
                    status = (Integer) session.update("billing.updateSuppInvHeader", map);
                    System.out.println("updateSuppInvHeader----" + status);

                } else if ("1".equalsIgnoreCase(billingTO.getOrganizationId()) && "".equalsIgnoreCase(billingTO.getGstNo()) && !"9".equalsIgnoreCase(billingTO.getBillingState())) {

                    map.put("gstType", "IGST");
                    map.put("taxAmount", ((Double.parseDouble(gTotal))) * (taxPercent / 100));
                    map.put("taxPercentage", taxPercent);
                    System.out.println("the tax map:" + map);
                    status = (Integer) session.update("billing.saveSupplementInvoiceTax", map);
                    status = (Integer) session.update("billing.updateSuppInvHeader", map);
                    System.out.println("updateSuppInvHeader----" + status);

                }

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveBillDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveBillDetails List", sqlException);
        }

        return status;
    }

    public int saveBillDetailExpense(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();

        map.put("userId", billingTO.getUserId());
        map.put("invoiceCode", billingTO.getInvoiceCode());
        map.put("invoiceDetailId", billingTO.getInvoiceDetailId());
        map.put("tripId", billingTO.getTripId());
        map.put("expenseId", billingTO.getExpenseId());
        map.put("expenseName", billingTO.getExpenseName() + "; " + billingTO.getExpenseRemarks());
        map.put("marginValue", billingTO.getMarginValue());
        map.put("taxPercentage", billingTO.getTaxPercentage());
        map.put("expenseValue", billingTO.getExpenseValue());
        Float totalValue = 0.00F;
        Float taxValue = 0.00F;
        Float nettValue = 0.00F;
        String marginValue = "0";
        String expenseValue = "0";
        String taxPercentage = "0";
        if (billingTO.getMarginValue() == null || "".equals(billingTO.getMarginValue())) {
            marginValue = "0";
        } else {
            marginValue = billingTO.getMarginValue();
        }
        if (billingTO.getTaxPercentage() == null || "".equals(billingTO.getTaxPercentage())) {
            taxPercentage = "0";
        } else {
            taxPercentage = billingTO.getTaxPercentage();
        }
        if (billingTO.getExpenseValue() == null || "".equals(billingTO.getExpenseValue())) {
            expenseValue = "0";
        } else {
            expenseValue = billingTO.getExpenseValue();
        }
        totalValue = (Float.parseFloat(expenseValue) + Float.parseFloat(marginValue));
        taxValue = ((Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
                * Float.parseFloat(taxPercentage) / 100);
        nettValue = (Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
                + ((Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
                * Float.parseFloat(taxPercentage) / 100);
        map.put("taxValue", taxValue);
        map.put("totalValue", totalValue);
        map.put("nettValue", nettValue);
        System.out.println("the saveBillDetailExpense:" + map);
        int status = 0;
        try {
            status = (Integer) session.update("billing.saveBillDetailExpense", map);
            System.out.println("saveBillDetailExpense size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveBillDetailExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveBillDetailExpense List", sqlException);
        }

        return status;
    }

    public int saveSuppBillDetailExpense(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();

        map.put("userId", "100");
        map.put("invoiceCode", billingTO.getInvoiceCode());
        map.put("invoiceDetailId", billingTO.getInvoiceDetailId());
        map.put("tripId", billingTO.getTripId());
        map.put("expenseId", billingTO.getExpenseId());
        map.put("expenseName", billingTO.getExpenseName() + "; " + billingTO.getExpenseRemarks());
        map.put("marginValue", billingTO.getMarginValue());
        map.put("taxPercentage", billingTO.getTaxPercentage());
        map.put("expenseValue", billingTO.getExpenseValue());
        Float totalValue = 0.00F;
        Float taxValue = 0.00F;
        Float nettValue = 0.00F;
        String marginValue = "0";
        String expenseValue = "0";
        String taxPercentage = "0";
        if (billingTO.getMarginValue() == null || "".equals(billingTO.getMarginValue())) {
            marginValue = "0";
        } else {
            marginValue = billingTO.getMarginValue();
        }
        if (billingTO.getTaxPercentage() == null || "".equals(billingTO.getTaxPercentage())) {
            taxPercentage = "0";
        } else {
            taxPercentage = billingTO.getTaxPercentage();
        }
        if (billingTO.getExpenseValue() == null || "".equals(billingTO.getExpenseValue())) {
            expenseValue = "0";
        } else {
            expenseValue = billingTO.getExpenseValue();
        }
        totalValue = (Float.parseFloat(expenseValue) + Float.parseFloat(marginValue));
        taxValue = ((Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
                * Float.parseFloat(taxPercentage) / 100);
        nettValue = (Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
                + ((Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
                * Float.parseFloat(taxPercentage) / 100);
        map.put("taxValue", taxValue);
        map.put("totalValue", totalValue);
        map.put("nettValue", nettValue);
        System.out.println("the saveSuppBillDetailExpense:" + map);
        int status = 0;
        try {
            status = (Integer) session.update("billing.saveSuppBillDetailExpense", map);
            System.out.println("saveBillDetailExpense size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveBillDetailExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveBillDetailExpense List", sqlException);
        }

        return status;
    }

    public int updateStatus(BillingTO billingTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        map.put("tripSheetId", billingTO.getTripSheetId());
        map.put("tripId", billingTO.getTripSheetId());
        map.put("userId", userId);
        map.put("statusId", billingTO.getStatusId());
        System.out.println("the updateEndTripSheet" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        int tripVehicleCount = 0;
        int tripClosureCount = 0;
        try {

            map.put("KM", 0);
            map.put("HM", 0);
            map.put("remarks", "system update");
            status = (Integer) session.update("billing.insertTripStatus", map);
//            status = (Integer) session.update("billing.updateTripStatus", map);

            int updateBillStatus = (Integer) session.update("billing.updateBillStatus", map);

            String ConsignmentIdList = (String) session.queryForObject("billing.getTripConsignment", map);
            System.out.println("the ConsignmentIdList size :" + ConsignmentIdList.length());
            String[] ConsignmentId = ConsignmentIdList.split(",");
            for (int i = 0; i < ConsignmentId.length; i++) {
                map.put("consignmentId", ConsignmentId[i]);
                map.put("consignmentStatus", "13");
                System.out.println("the updated map:" + map);
                status = (Integer) session.update("billing.updateConsignmentStatus", map);
                System.out.println("the status1:" + status);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStatus List", sqlException);
        }

        return status;
    }

    public ArrayList viewBillForSubmission(String customerId, String fromDate, String toDate, String tripType, String submitStatus, String billNo, String grNo, String containerNo) {
        Map map = new HashMap();
        ArrayList closedBillList = null;
        try {
            map.put("customerId", customerId);
            if (fromDate == null) {
                fromDate = "";
            }
            if (toDate == null) {
                toDate = "";
            }
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("tripType", tripType);
            map.put("submitStatus", submitStatus);
            if (billNo != null && !"".equals(billNo)) {
                map.put("billNo", "%" + billNo + "%");
            } else {
                map.put("billNo", "");
            }
            if (grNo != null && !"".equals(grNo)) {
                map.put("grNo", "%" + grNo + "%");
            } else {
                map.put("grNo", "");
            }
            if (containerNo != null && !"".equals(containerNo)) {
                map.put("containerNo", "%" + containerNo + "%");
            } else {
                map.put("containerNo", "");
            }
            System.out.println("getClosedbill Value map" + map);
            closedBillList = (ArrayList) getSqlMapClientTemplate().queryForList("billing.viewBillForSubmission", map);
            System.out.println("getbil details size is ::::" + closedBillList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return closedBillList;
    }

    public ArrayList viewSuppBillForSubmission(String customerId, String fromDate, String toDate, String tripType, String submitStatus, String billNo, String grNo) {
        Map map = new HashMap();
        ArrayList closedBillList = null;
        try {
            map.put("customerId", customerId);
            if (fromDate == null) {
                fromDate = "";
            }
            if (toDate == null) {
                toDate = "";
            }
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("tripType", tripType);
            map.put("submitStatus", submitStatus);
            if (billNo != null && !"".equals(billNo)) {
                map.put("billNo", "%" + billNo + "%");
            } else {
                map.put("billNo", "");
            }
            if (grNo != null && !"".equals(grNo)) {
                map.put("grNo", "%" + grNo + "%");
            } else {
                map.put("grNo", "");
            }
            System.out.println("getClosedbill Value map" + map);
            closedBillList = (ArrayList) getSqlMapClientTemplate().queryForList("billing.viewSuppBillForSubmission", map);
            System.out.println("getbil details size is ::::" + closedBillList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return closedBillList;
    }

    public int submitBill(String invoiceId, int userId) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("invoiceId", invoiceId);
            map.put("userId", userId);
            System.out.println("map::::" + map);
            status = (Integer) getSqlMapClientTemplate().update("billing.updateTripStatusForBillingSubmission", map);
            ArrayList billingTripIds = new ArrayList();
            billingTripIds = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getInvoiceTripIds", map);
            if (billingTripIds.size() > 0) {
                Iterator itr = billingTripIds.iterator();
                BillingTO billingTO = new BillingTO();
                while (itr.hasNext()) {
                    billingTO = (BillingTO) itr.next();
                    map.put("tripId", billingTO.getTripId());
                    status = (Integer) getSqlMapClientTemplate().update("billing.saveTripStatusDetailsForBillingSubmission", map);
                }
            }
            status = (Integer) getSqlMapClientTemplate().update("billing.updateBillSubmitStatusFinanceHeader", map);

            System.out.println("status = " + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("submitBill Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "submitBill List", sqlException);
        }
        return status;
    }

    public int updateCancel(String billingNo, int userId) {
        Map map = new HashMap();
        map.put("billingNo", billingNo);
        map.put("userId", userId);
        System.out.println("the updateCancel" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("billing.updateInvoiceHeader", map);
            System.out.println("statusInvoiceHeaderUpdate" + status);

            status = (Integer) getSqlMapClientTemplate().update("billing.updateInvoiceDetail", map);
            System.out.println("statusInvoiceDetailsUpdate" + status);

            System.out.println("updateStatus size=" + tripDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStatus List", sqlException);
        }

        return status;
    }

    public ArrayList getPreviewHeader(BillingTO billingTO) {
        Map map = new HashMap();
        ArrayList getPreviewHeader = null;
        try {
            String[] tripId = billingTO.getTripSheetIds().split(",");
            List tripIds = new ArrayList(tripId.length);
            for (int i = 0; i < tripId.length; i++) {
                System.out.println("value:" + tripId[i]);
                tripIds.add(tripId[i]);
            }

            map.put("tripSheetIds", tripIds);

            System.out.println("getPreviewHeader Value map" + map);
            getPreviewHeader = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getPreviewHeader", map);
            System.out.println("getPreviewHeader size is ::::" + getPreviewHeader.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return getPreviewHeader;
    }

    public ArrayList getPreviewSupplementHeader(BillingTO billingTO) {
        Map map = new HashMap();
        ArrayList getPreviewHeader = null;
        try {
            String[] tripId = billingTO.getTripSheetIds().split(",");
            List tripIds = new ArrayList(tripId.length);
            for (int i = 0; i < tripId.length; i++) {
                System.out.println("value:" + tripId[i]);
                tripIds.add(tripId[i]);
            }

            map.put("tripSheetIds", tripIds);

            System.out.println("getPreviewHeader Value map" + map);
            getPreviewHeader = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getPreviewSupplementHeader", map);
            System.out.println("getPreviewHeader size is ::::" + getPreviewHeader.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return getPreviewHeader;
    }

    public ArrayList getBillingDetailsForPreview(BillingTO billingTO) {
        Map map = new HashMap();
        ArrayList getBillingDetailsForPreview = null;
        try {
            String[] tripId = billingTO.getTripSheetIds().split(",");
            List tripIds = new ArrayList(tripId.length);
            for (int i = 0; i < tripId.length; i++) {
                System.out.println("value:" + tripId[i]);
                tripIds.add(tripId[i]);
            }

            map.put("tripSheetIds", tripIds);

            System.out.println("getBillingDetailsForPreview map" + map);
            getBillingDetailsForPreview = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getBillingDetailsForPreview", map);
            System.out.println("getBillingDetailsForPreview size is ::::" + getBillingDetailsForPreview.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return getBillingDetailsForPreview;
    }

    public ArrayList getSupplementBillingDetailsForPreview(BillingTO billingTO) {
        Map map = new HashMap();
        ArrayList getBillingDetailsForPreview = null;
        try {
            String[] tripId = billingTO.getTripSheetIds().split(",");
            List tripIds = new ArrayList(tripId.length);
            for (int i = 0; i < tripId.length; i++) {
                System.out.println("value:" + tripId[i]);
                tripIds.add(tripId[i]);
            }

            map.put("tripSheetIds", tripIds);

            System.out.println("getBillingDetailsForPreview map" + map);
            getBillingDetailsForPreview = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getSupplementBillingDetailsForPreview", map);
            System.out.println("getBillingDetailsForPreview size is ::::" + getBillingDetailsForPreview.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return getBillingDetailsForPreview;
    }

    public String getInvoiceCodeSequence(SqlMapClient session) {
        Map map = new HashMap();
        int codeSequence = 0;
        String invoiceCode = "";
        try {
            codeSequence = (Integer) session.insert("trip.getInvoiceCodeSequence", map);
            invoiceCode = codeSequence + "";
            if (invoiceCode.length() == 1) {
                invoiceCode = "000000" + invoiceCode;
                System.out.println("invoice no 1.." + invoiceCode);
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "00000" + invoiceCode;
                System.out.println("invoice lenght 2.." + invoiceCode);
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 4) {
                invoiceCode = "000" + invoiceCode;
            } else if (invoiceCode.length() == 5) {
                invoiceCode = "00" + invoiceCode;
            } else if (invoiceCode.length() == 6) {
                invoiceCode = "0" + invoiceCode;
            }
            System.out.println("getInvoiceCodeSequence=" + codeSequence);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return invoiceCode;
    }

    public String getInvoiceCodeSequence1718(SqlMapClient session, int gstCheck) {
        Map map = new HashMap();
        int codeSequence = 0;
        String invoiceCode = "";
        try {
            if (gstCheck == 2) {
                codeSequence = (Integer) session.insert("billing.getInvoiceCodeSequence1718", map);
            } else {
                codeSequence = (Integer) session.insert("billing.getInvoiceCodeSequenceGST", map);
            }
            invoiceCode = codeSequence + "";
            if (invoiceCode.length() == 1) {
                invoiceCode = "000000" + invoiceCode;
                System.out.println("invoice no 1.." + invoiceCode);
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "00000" + invoiceCode;
                System.out.println("invoice lenght 2.." + invoiceCode);
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 4) {
                invoiceCode = "000" + invoiceCode;
            } else if (invoiceCode.length() == 5) {
                invoiceCode = "00" + invoiceCode;
            } else if (invoiceCode.length() == 6) {
                invoiceCode = "0" + invoiceCode;
            }

            System.out.println("getInvoiceCodeSequence=" + invoiceCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return invoiceCode;
    }

    public String getInvoiceCodeSequence1819(SqlMapClient session, int gstCheck) {
        Map map = new HashMap();
        int codeSequence = 0;
        String invoiceCode = "";
        int lastInvoiceCode = 0;
        try {
            lastInvoiceCode = (Integer) session.queryForObject("billing.getLastInvoiceCode1819", map);
            //  int lastCodes = Integer.parseInt(lastInvoiceCode.substring(7));
            //  codeSequence = lastCodes +1 ;
            map.put("invoiceCode", lastInvoiceCode + 1);
            codeSequence = (Integer) session.insert("billing.getInvoiceCodeSequenceGST1819", map);
            codeSequence = (Integer) session.queryForObject("billing.getLastInvoiceCode1819", map);
            System.out.println("codeSequence-----" + codeSequence);
            invoiceCode = codeSequence + "";
            if (invoiceCode.length() == 1) {
                invoiceCode = "000000" + invoiceCode;
                System.out.println("invoice no 1.." + invoiceCode);
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "00000" + invoiceCode;
                System.out.println("invoice lenght 2.." + invoiceCode);
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 4) {
                invoiceCode = "000" + invoiceCode;
            } else if (invoiceCode.length() == 5) {
                invoiceCode = "00" + invoiceCode;
            } else if (invoiceCode.length() == 6) {
                invoiceCode = "0" + invoiceCode;
            }

            System.out.println("getInvoiceCodeSequence=" + invoiceCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return invoiceCode;
    }

    public String getInvoiceCodeSequence1920(SqlMapClient session, int gstCheck) {
        Map map = new HashMap();
        int codeSequence = 0;
        String invoiceCode = "";
        int lastInvoiceCode = 0;
        try {
            lastInvoiceCode = (Integer) session.queryForObject("billing.getLastInvoiceCode", map);

            //  int lastCodes = Integer.parseInt(lastInvoiceCode.substring(7));
            //  codeSequence = lastCodes +1 ;
            map.put("invoiceCode", lastInvoiceCode + 1);
            codeSequence = (Integer) session.insert("billing.getInvoiceCodeSequenceGST1920", map);
            codeSequence = (Integer) session.queryForObject("billing.getLastInvoiceCode1920", map);
            System.out.println("codeSequence-----" + codeSequence);
            invoiceCode = codeSequence + "";
            if (invoiceCode.length() == 1) {
                invoiceCode = "000000" + invoiceCode;
                System.out.println("invoice no 1.." + invoiceCode);
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "00000" + invoiceCode;
                System.out.println("invoice lenght 2.." + invoiceCode);
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 4) {
                invoiceCode = "000" + invoiceCode;
            } else if (invoiceCode.length() == 5) {
                invoiceCode = "00" + invoiceCode;
            } else if (invoiceCode.length() == 6) {
                invoiceCode = "0" + invoiceCode;
            }

            System.out.println("getInvoiceCodeSequence=" + invoiceCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return invoiceCode;
    }

    public String getInvoiceCodeSequence2021(SqlMapClient session, int gstCheck) {
        Map map = new HashMap();
        int codeSequence = 0;
        String invoiceCode = "";
        int lastInvoiceCode = 0;
        try {
            lastInvoiceCode = (Integer) session.queryForObject("billing.getLastInvoiceCode2021", map);
//            
//            //  int lastCodes = Integer.parseInt(lastInvoiceCode.substring(7));
//            //  codeSequence = lastCodes +1 ;
            map.put("invoiceCode", lastInvoiceCode + 1);
            System.out.println("lastInvoiceCode + 1 -------" + lastInvoiceCode + 1);
            codeSequence = (Integer) session.insert("billing.getInvoiceCodeSequenceGST2021", map);
            codeSequence = (Integer) session.queryForObject("billing.getLastInvoiceCode2021", map);
            System.out.println("codeSequence-----" + codeSequence);
            invoiceCode = codeSequence + "";
            if (invoiceCode.length() == 1) {
                invoiceCode = "000000" + invoiceCode;
                System.out.println("invoice no 1.." + invoiceCode);
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "00000" + invoiceCode;
                System.out.println("invoice lenght 2.." + invoiceCode);
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 4) {
                invoiceCode = "000" + invoiceCode;
            } else if (invoiceCode.length() == 5) {
                invoiceCode = "00" + invoiceCode;
            } else if (invoiceCode.length() == 6) {
                invoiceCode = "0" + invoiceCode;
            }

            System.out.println("getInvoiceCodeSequence=" + invoiceCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return invoiceCode;
    }

    public String getInvoiceCodeSequence2122(SqlMapClient session, int gstCheck) {
        Map map = new HashMap();
        int codeSequence = 0;
        String invoiceCode = "";
        int lastInvoiceCodeTmp = 0;
        int lastInvoiceCode = 0;
        try {
            lastInvoiceCodeTmp = (Integer) session.queryForObject("billing.getLastInvoiceCode", map);
            System.out.println("lastInvoiceCodeTmp -------" + lastInvoiceCodeTmp);

            //  int lastCodes = Integer.parseInt(lastInvoiceCode.substring(7));
            //  codeSequence = lastCodes +1 ;
            lastInvoiceCode = lastInvoiceCodeTmp + 1;
            System.out.println("lastInvoiceCode + 1 -------" + lastInvoiceCode);
            map.put("invoiceCode", lastInvoiceCode);
            codeSequence = (Integer) session.insert("billing.getInvoiceCodeSequenceGST2122", map);
            System.out.println("codeSequence1---" + codeSequence);
            codeSequence = (Integer) session.queryForObject("billing.getLastInvoiceCode", map);
            System.out.println("codeSequence2-----" + codeSequence);
            invoiceCode = codeSequence + "";
            if (invoiceCode.length() == 1) {
                invoiceCode = "000000" + invoiceCode;
                System.out.println("invoice no 1.." + invoiceCode);
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "00000" + invoiceCode;
                System.out.println("invoice lenght 2.." + invoiceCode);
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 4) {
                invoiceCode = "000" + invoiceCode;
            } else if (invoiceCode.length() == 5) {
                invoiceCode = "00" + invoiceCode;
            } else if (invoiceCode.length() == 6) {
                invoiceCode = "0" + invoiceCode;
            }

            System.out.println("getInvoiceCodeSequence=" + invoiceCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return invoiceCode;
    }

    public String getSuppInvoiceCodeSequence1819(SqlMapClient session, int gstCheck) {
        Map map = new HashMap();
        int codeSequence = 0;
        String invoiceCode = "";
        try {
            codeSequence = (Integer) session.insert("billing.getSuppInvoiceCodeSequenceGST1819", map);
            invoiceCode = codeSequence + "";
            if (invoiceCode.length() == 1) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice no 1.." + invoiceCode);
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "000" + invoiceCode;
                System.out.println("invoice lenght 2.." + invoiceCode);
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "00" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 4) {
                invoiceCode = "0" + invoiceCode;
            }

            System.out.println("getSuppInvoiceCodeSequenceGST1819=" + invoiceCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return invoiceCode;
    }

    public String getSuppInvoiceCodeSequence1920(SqlMapClient session, int gstCheck) {
        Map map = new HashMap();
        int codeSequence = 0;
        String invoiceCode = "";
        try {
            codeSequence = (Integer) session.insert("billing.getSuppInvoiceCodeSequenceGST1920", map);
            invoiceCode = codeSequence + "";
            if (invoiceCode.length() == 1) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice no 1.." + invoiceCode);
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "000" + invoiceCode;
                System.out.println("invoice lenght 2.." + invoiceCode);
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "00" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 4) {
                invoiceCode = "0" + invoiceCode;
            }

            System.out.println("getSuppInvoiceCodeSequence1920=" + invoiceCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return invoiceCode;
    }

//    public String getSuppInvoiceCodeSequence2021(SqlMapClient session, int gstCheck) {
//        Map map = new HashMap();
//        int codeSequence = 0;
//        String invoiceCode = "";
//        try {
//            codeSequence = (Integer) session.insert("billing.getSuppInvoiceCodeSequenceGST2021", map);
//            invoiceCode = codeSequence + "";
//            if (invoiceCode.length() == 1) {
//                invoiceCode = "0000" + invoiceCode;
//                System.out.println("invoice no 1.." + invoiceCode);
//            } else if (invoiceCode.length() == 2) {
//                invoiceCode = "000" + invoiceCode;
//                System.out.println("invoice lenght 2.." + invoiceCode);
//            } else if (invoiceCode.length() == 3) {
//                invoiceCode = "00" + invoiceCode;
//                System.out.println("invoice lenght 3.." + invoiceCode);
//            } else if (invoiceCode.length() == 4) {
//                invoiceCode = "0" + invoiceCode;
//            }
//
//            System.out.println("getSuppInvoiceCodeSequence1920=" + invoiceCode);
//        } catch (Exception sqlException) {
//            sqlException.printStackTrace();
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
//        }
//        return invoiceCode;
//    }
//    public String getSuppInvoiceCodeSequence2022(SqlMapClient session, int gstCheck) {
//        Map map = new HashMap();
//        int codeSequence = 0;
//        String invoiceCode = "";
//        try {
//            codeSequence = (Integer) session.insert("billing.getSuppInvoiceCodeSequenceGST2122", map);
//            invoiceCode = codeSequence + "";
//            if (invoiceCode.length() == 1) {
//                invoiceCode = "0000" + invoiceCode;
//                System.out.println("invoice no 1.." + invoiceCode);
//            } else if (invoiceCode.length() == 2) {
//                invoiceCode = "000" + invoiceCode;
//                System.out.println("invoice lenght 2.." + invoiceCode);
//            } else if (invoiceCode.length() == 3) {
//                invoiceCode = "00" + invoiceCode;
//                System.out.println("invoice lenght 3.." + invoiceCode);
//            } else if (invoiceCode.length() == 4) {
//                invoiceCode = "0" + invoiceCode;
//            }
//
//            System.out.println("getSuppInvoiceCodeSequence1920=" + invoiceCode);
//        } catch (Exception sqlException) {
//            sqlException.printStackTrace();
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
//        }
//        return invoiceCode;
//    }
    public ArrayList getGSTTaxDetails(BillingTO billingTO) {
        Map map = new HashMap();

        map.put("tripId", billingTO.getTripId());

        String[] tripId = billingTO.getTripSheetId().split(",");
        List tripIds = new ArrayList(tripId.length);
        for (int i = 0; i < tripId.length; i++) {
            System.out.println("value:" + tripId[i]);
            tripIds.add(tripId[i]);
        }
        map.put("tripIds", tripIds);

        ArrayList gstDetails = new ArrayList();
        try {
            gstDetails = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getGSTTaxDetails", map);
            System.out.println("gstDetails size=" + gstDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return gstDetails;
    }

    public ArrayList getInvoiceGSTTaxDetails(BillingTO billingTO) {
        Map map = new HashMap();

        map.put("invoiceId", billingTO.getInvoiceId());

        ArrayList gstDetails = new ArrayList();
        try {
            gstDetails = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getInvoiceGSTTaxDetails", map);
            System.out.println("gstDetails size=" + gstDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return gstDetails;
    }

    public ArrayList getInvoiceSupplementGSTTaxDetails(BillingTO billingTO) {
        Map map = new HashMap();

        map.put("invoiceId", billingTO.getInvoiceId());

        ArrayList gstDetails = new ArrayList();
        try {
            gstDetails = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getInvoiceSupplementGSTTaxDetails", map);
            System.out.println("gstDetails size=" + gstDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return gstDetails;
    }

    public int updateTripGst(String invoiceId, String[] tripIds, String[] tripIdIGSTAmount, String[] tripIdCGSTAmount, String[] tripIdSGSTAmount) {
        Map map = new HashMap();
        map.put("invoiceId", invoiceId);

        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            for (int i = 0; i < tripIds.length; i++) {
                map.put("tripId", tripIds[i]);
                map.put("tripIdIGSTAmount", tripIdIGSTAmount[i]);
                map.put("tripIdCGSTAmount", tripIdCGSTAmount[i]);
                map.put("tripIdSGSTAmount", tripIdSGSTAmount[i]);
                System.out.println("map...." + map);
                status = (Integer) getSqlMapClientTemplate().update("billing.updateInvoiceTripGST", map);
                System.out.println("updateInvoiceTripGST.." + status);

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStatus List", sqlException);
        }

        return status;
    }

    public ArrayList getInvoiceCreditNoteDetails(String creditNoteId) {
        Map map = new HashMap();

        map.put("creditNoteId", creditNoteId);

        ArrayList creditDetails = new ArrayList();
        try {
            creditDetails = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getInvoiceCreditNoteDetails", map);
            System.out.println("getInvoiceCreditNoteDetails size=" + creditDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return creditDetails;
    }

    public ArrayList getCustomerInvoiceList(BillingTO billingTO) {
        Map map = new HashMap();

        map.put("fromDate", billingTO.getFromDate());
        map.put("toDate", billingTO.getToDate());
        map.put("customerId", billingTO.getCustomerId());
        System.out.println("map for customerInvoiceList:" + map);
        ArrayList creditDetails = new ArrayList();
        try {
            creditDetails = (ArrayList) getSqlMapClientTemplate().queryForList("billing.customerInvoiceList", map);
            System.out.println("customerInvoiceList size=" + creditDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return creditDetails;
    }

    public int saveInvoicePayment(String[] invoiceIds, String[] InvoiceCodes, String[] invoiceDates, String[] invoiceAmounts, String[] modes, String[] modeNos, String[] bankNames, String[] paidAmounts, int userId) {
        Map map = new HashMap();
        int status = 0;

        try {
            for (int i = 0; i < invoiceIds.length; i++) {
                if (!"".equalsIgnoreCase(paidAmounts[i])) {
                    map.put("InvoiceCode", InvoiceCodes[i]);
                    map.put("invoiceDate", invoiceDates[i]);
                    map.put("invoiceAmount", invoiceAmounts[i]);
                    map.put("mode", modes[i]);
                    map.put("modeNos", modeNos[i]);
                    map.put("bankNames", bankNames[i]);
                    map.put("paidAmounts", paidAmounts[i]);
                    map.put("userId", userId);
                    System.out.println("map...." + map);
                    status = (Integer) getSqlMapClientTemplate().insert("billing.saveInvoicePayment", map);
                    System.out.println("saveInvoicePayment.." + status);
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStatus List", sqlException);
        }

        return status;
    }

    public int saveCustomerPayment(String customerId, String receiptCode, String modes, String modeNos, String bankNames, String payDate, String paidAmounts, String custType, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;

        try {
            if (!"".equalsIgnoreCase(paidAmounts)) {
                map.put("customerId", customerId);
                map.put("receiptCode", receiptCode);
                map.put("mode", modes);
                map.put("modeNos", modeNos);
                map.put("bankNames", bankNames);
                map.put("payDate", payDate);
                map.put("paidAmounts", paidAmounts);
                map.put("userId", userId);
                map.put("custType", custType);

                map.put("consignmentOrderId", "0");
                map.put("outstandingAmount", "0");
                map.put("creditNoteInvoiceId", "0");
                map.put("supplymentryInvoiceId", "0");
                map.put("totalFreightAmount", "0");
                map.put("supplementrycreditId", 0);
                map.put("invoiceId", 0);
                map.put("outStandingAmount", paidAmounts);

                System.out.println("map...." + map);
                status = (Integer) session.insert("billing.saveCustomerPayment", map);
                map.put("paymentId", status);

                int updateCnoteCount, updateOutstanding, updateCreditLimit, updateUsedLimit, updateOutstandingLog = 0;
                String availBlockedAmount = "";
                String availBlockedAmountTemp[] = null;
                map.put("transactionName", "Receipt");

                if ("1".equals(custType)) {
                    status = (Integer) session.update("billing.updateCreditLimit", map);
                    updateOutstanding = (Integer) session.update("operation.updateOutstandingPayment", map);
                    availBlockedAmount = (String) session.queryForObject("operation.getAvailandBlockedAmount", map);
                    System.out.println("availBlockedAmount------" + availBlockedAmount);
                    availBlockedAmountTemp = availBlockedAmount.split("~");
                    map.put("fixedLimit", availBlockedAmountTemp[0]);
                    map.put("availAmount", availBlockedAmountTemp[1]);
                    map.put("blockedAmount", availBlockedAmountTemp[2]);
                    map.put("usedLimit", availBlockedAmountTemp[3]);

                    map.put("transactionType", "Credit");   // credit
                    map.put("debitAmount", "0.00");
                    map.put("creditAmount", paidAmounts);

                    updateOutstandingLog = (Integer) session.update("operation.updateOutstandingLog", map);

                } else if ("2".equals(custType)) {

                    availBlockedAmount = (String) session.queryForObject("operation.getAvailandBlockedAmount", map);
                    System.out.println("availBlockedAmount------" + availBlockedAmount);
                    availBlockedAmountTemp = availBlockedAmount.split("~");

                    System.out.println("used Limit :" + availBlockedAmountTemp[3]);
                    System.out.println("paidAmounts :" + paidAmounts);

                    double usedlimit = Double.parseDouble(availBlockedAmountTemp[3]) - Double.parseDouble(paidAmounts);
                    System.out.println("new usedlimit :" + usedlimit);

                    if (usedlimit < 0) {
                        map.put("usedLimit", 0);
                    } else {
                        map.put("usedLimit", usedlimit);
                    }

                    status = (Integer) session.update("billing.updatePDALimit", map);

                    availBlockedAmount = (String) session.queryForObject("operation.getAvailandBlockedAmount", map);
                    System.out.println("availBlockedAmount------" + availBlockedAmount);
                    availBlockedAmountTemp = availBlockedAmount.split("~");
                    map.put("usedLimit", availBlockedAmountTemp[3]);
                    map.put("fixedLimit", availBlockedAmountTemp[0]);
                    map.put("availAmount", availBlockedAmountTemp[1]);
                    map.put("blockedAmount", availBlockedAmountTemp[2]);
                    updateOutstanding = (Integer) session.update("operation.updateOutstandingPayment", map);
                    System.out.println("status------" + status);
//                    status = (Integer) session.update("billing.updateCreditLimit", map);

                    map.put("transactionType", "Credit");   // credit
                    map.put("debitAmount", "0.00");
                    map.put("creditAmount", paidAmounts);
                    updateOutstandingLog = (Integer) session.update("operation.updateOutstandingLog", map);
                }
                System.out.println("saveCustomerPayment.." + status);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStatus List", sqlException);
        }

        return status;
    }

    public ArrayList getCustomerPaymentHistory(BillingTO billingTO) {
        Map map = new HashMap();

        map.put("customerId", billingTO.getCustomerId());
        System.out.println("map for customerPaymentHistory:" + map);
        ArrayList paymentHistory = new ArrayList();
        try {
            paymentHistory = (ArrayList) getSqlMapClientTemplate().queryForList("billing.customerPaymentHistory", map);
            System.out.println("customerPaymentHistory size=" + paymentHistory.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return paymentHistory;
    }

    public ArrayList getCustomerStatementHistory(BillingTO billingTO) {
        Map map = new HashMap();

        map.put("customerId", billingTO.getCustomerId());
        map.put("fromDate", billingTO.getFromDate());
        map.put("toDate", billingTO.getToDate());
        System.out.println("map for paymentStatementHistory:" + map);
        ArrayList paymentHistory = new ArrayList();
        try {
            paymentHistory = (ArrayList) getSqlMapClientTemplate().queryForList("billing.paymentStatementHistory", map);
            System.out.println("paymentStatementHistory size=" + paymentHistory.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("paymentStatementHistory Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "paymentStatementHistory List", sqlException);
        }

        return paymentHistory;
    }

    public String getReceiptSequence(SqlMapClient session) {
        Map map = new HashMap();
        int codeSequence = 0;
        String receiptCode = "";
        try {

            codeSequence = (Integer) session.insert("billing.getReceiptCodeSequence", map);
            codeSequence = (Integer) session.queryForObject("billing.getLastReceiptCode", map);
            System.out.println("codeSequence-----" + codeSequence);
            receiptCode = codeSequence + "";
            if (receiptCode.length() == 1) {
                receiptCode = "000000" + receiptCode;
                System.out.println("invoice no 1.." + receiptCode);
            } else if (receiptCode.length() == 2) {
                receiptCode = "00000" + receiptCode;
                System.out.println("invoice lenght 2.." + receiptCode);
            } else if (receiptCode.length() == 3) {
                receiptCode = "0000" + receiptCode;
                System.out.println("invoice lenght 3.." + receiptCode);
            } else if (receiptCode.length() == 4) {
                receiptCode = "000" + receiptCode;
            } else if (receiptCode.length() == 5) {
                receiptCode = "00" + receiptCode;
            } else if (receiptCode.length() == 6) {
                receiptCode = "0" + receiptCode;
            }

            System.out.println("receiptCodeSequence=" + receiptCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getReceiptCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getReceiptCodeSequence", sqlException);
        }
        return receiptCode;
    }

    public String getTempReceiptSequence(SqlMapClient session) {
        Map map = new HashMap();
        int codeSequence = 0;
        String receiptCode = "";
        try {

            codeSequence = (Integer) session.insert("billing.getTempReceiptCodeSequence", map);
            //  codeSequence = (Integer) session.queryForObject("billing.getLastReceiptCode", map);
            System.out.println("codeSequence-----" + codeSequence);
            receiptCode = codeSequence + "";
            if (receiptCode.length() == 1) {
                receiptCode = "000000" + receiptCode;
                System.out.println("invoice no 1.." + receiptCode);
            } else if (receiptCode.length() == 2) {
                receiptCode = "00000" + receiptCode;
                System.out.println("invoice lenght 2.." + receiptCode);
            } else if (receiptCode.length() == 3) {
                receiptCode = "0000" + receiptCode;
                System.out.println("invoice lenght 3.." + receiptCode);
            } else if (receiptCode.length() == 4) {
                receiptCode = "000" + receiptCode;
            } else if (receiptCode.length() == 5) {
                receiptCode = "00" + receiptCode;
            } else if (receiptCode.length() == 6) {
                receiptCode = "0" + receiptCode;
            }

            System.out.println("getTempReceiptCodeSequence=" + receiptCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTempReceiptCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTempReceiptCodeSequence", sqlException);
        }
        return receiptCode;
    }

    public String getPdaReceiptSequence(SqlMapClient session) {
        Map map = new HashMap();
        int codeSequence = 0;
        String receiptCode = "";
        try {

            codeSequence = (Integer) session.insert("billing.getPdaReceiptCodeSequence", map);
            //     codeSequence = (Integer) session.queryForObject("billing.getLastReceiptCode", map);
            System.out.println("codeSequence-----" + codeSequence);
            receiptCode = codeSequence + "";
            if (receiptCode.length() == 1) {
                receiptCode = "000000" + receiptCode;
                System.out.println("invoice no 1.." + receiptCode);
            } else if (receiptCode.length() == 2) {
                receiptCode = "00000" + receiptCode;
                System.out.println("invoice lenght 2.." + receiptCode);
            } else if (receiptCode.length() == 3) {
                receiptCode = "0000" + receiptCode;
                System.out.println("invoice lenght 3.." + receiptCode);
            } else if (receiptCode.length() == 4) {
                receiptCode = "000" + receiptCode;
            } else if (receiptCode.length() == 5) {
                receiptCode = "00" + receiptCode;
            } else if (receiptCode.length() == 6) {
                receiptCode = "0" + receiptCode;
            }

            System.out.println("receiptCodeSequence=" + receiptCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPdaReceiptCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPdaReceiptCodeSequence", sqlException);
        }
        return receiptCode;
    }

    public String getCreditAdjReceiptSequence(SqlMapClient session) {
        Map map = new HashMap();
        int codeSequence = 0;
        String receiptCode = "";
        try {

            codeSequence = (Integer) session.insert("billing.getCreditAdjReceiptSequence", map);
            //  codeSequence = (Integer) session.queryForObject("billing.getLastReceiptCode", map);
            System.out.println("codeSequence-----" + codeSequence);
            receiptCode = codeSequence + "";
            if (receiptCode.length() == 1) {
                receiptCode = "000000" + receiptCode;
                System.out.println("invoice no 1.." + receiptCode);
            } else if (receiptCode.length() == 2) {
                receiptCode = "00000" + receiptCode;
                System.out.println("invoice lenght 2.." + receiptCode);
            } else if (receiptCode.length() == 3) {
                receiptCode = "0000" + receiptCode;
                System.out.println("invoice lenght 3.." + receiptCode);
            } else if (receiptCode.length() == 4) {
                receiptCode = "000" + receiptCode;
            } else if (receiptCode.length() == 5) {
                receiptCode = "00" + receiptCode;
            } else if (receiptCode.length() == 6) {
                receiptCode = "0" + receiptCode;
            }

            System.out.println("getAdjReceiptSequence=" + receiptCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdjReceiptSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getAdjReceiptSequence", sqlException);
        }
        return receiptCode;
    }

    public String getAdjReceiptSequence(SqlMapClient session) {
        Map map = new HashMap();
        int codeSequence = 0;
        String receiptCode = "";
        try {

            codeSequence = (Integer) session.insert("billing.getAdjReceiptCodeSequence", map);
            //  codeSequence = (Integer) session.queryForObject("billing.getLastReceiptCode", map);
            System.out.println("codeSequence-----" + codeSequence);
            receiptCode = codeSequence + "";
            if (receiptCode.length() == 1) {
                receiptCode = "000000" + receiptCode;
                System.out.println("invoice no 1.." + receiptCode);
            } else if (receiptCode.length() == 2) {
                receiptCode = "00000" + receiptCode;
                System.out.println("invoice lenght 2.." + receiptCode);
            } else if (receiptCode.length() == 3) {
                receiptCode = "0000" + receiptCode;
                System.out.println("invoice lenght 3.." + receiptCode);
            } else if (receiptCode.length() == 4) {
                receiptCode = "000" + receiptCode;
            } else if (receiptCode.length() == 5) {
                receiptCode = "00" + receiptCode;
            } else if (receiptCode.length() == 6) {
                receiptCode = "0" + receiptCode;
            }

            System.out.println("getAdjReceiptSequence=" + receiptCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdjReceiptSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getAdjReceiptSequence", sqlException);
        }
        return receiptCode;
    }

    public int saveTempCreditlimit(String receiptCode, BillingTO billingTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        map.put("customerId", billingTO.getCustomerId());
        map.put("validFrom", billingTO.getFromDate());
        map.put("validTo", billingTO.getToDate());
        map.put("creidtLimit", billingTO.getCreditAmount());
        map.put("availLimit", billingTO.getCreditAmount());
        map.put("userId", userId);
        map.put("receiptCode", receiptCode);
        System.out.println("map for custtempcreditlimit:" + map);
        int creditlimit = 0;
        try {
            creditlimit = (Integer) session.insert("billing.saveTempCreditlimit", map);
            String availBlockedAmount = (String) session.queryForObject("operation.getAvailandBlockedAmount", map);
            System.out.println("availBlockedAmount------" + availBlockedAmount);
            String[] availBlockedAmountTemp = availBlockedAmount.split("~");

            double tot = Double.parseDouble(availBlockedAmountTemp[1]) + Double.parseDouble(billingTO.getCreditAmount());

            map.put("fixedLimit", availBlockedAmountTemp[0]);
            map.put("availAmount", tot);
            map.put("blockedAmount", "0");
            map.put("usedLimit", "0");
            map.put("paymentId", 0);
            map.put("tempId", creditlimit);
            map.put("transactionName", "TempCredit");

            map.put("transactionType", "Credit");  //debit
            map.put("debitAmount", 0);
            map.put("custType", 1);
            map.put("creditAmount", billingTO.getCreditAmount());
            map.put("outStandingAmount", billingTO.getCreditAmount());
            System.out.println(" consignment insert map ::::::::" + map);

            int updateOutstandingLog = (Integer) session.update("operation.updateOutstandingLog", map);

            int status = (Integer) session.update("billing.updateTempCreditLimit", map);

            System.out.println("custTempcreditLimit size=" + creditlimit);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
        }
        return creditlimit;
    }

    public ArrayList getTempCreditDetails(BillingTO billingTO) {
        Map map = new HashMap();
        ArrayList tempCreditDetails = new ArrayList();
        map.put("customerId", billingTO.getCustomerId());
        System.out.println("map for getTempCreditDetails:" + map);
        try {
            tempCreditDetails = (ArrayList) getSqlMapClientTemplate().queryForList("billing.tempCreditDetails", map);
            System.out.println("tempCreditDetails size=" + tempCreditDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("tempCreditDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "tempCreditDetails List", sqlException);
        }

        return tempCreditDetails;
    }

    public ArrayList getInvoiceSuppCreditNoteDetails(String creditNoteId) {
        Map map = new HashMap();

        map.put("creditNoteId", creditNoteId);

        ArrayList creditDetails = new ArrayList();
        try {
            creditDetails = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getInvoiceSuppCreditNoteDetails", map);
            System.out.println("getInvoiceCreditNoteDetails size=" + creditDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return creditDetails;
    }

    public ArrayList getSuppInvoiceGSTTaxDetails(BillingTO billingTO) {
        Map map = new HashMap();

        map.put("invoiceId", billingTO.getInvoiceId());

        ArrayList gstDetails = new ArrayList();
        try {
            gstDetails = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getSuppInvoiceGSTTaxDetails", map);
            System.out.println("gstDetails size=" + gstDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return gstDetails;
    }

    public int saveAdjustPayment(String customerId, String receiptCode, String transactionType, String balAmt, String availAmount, String adjustRemark, String adjustAmount, String custType, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;

        try {

            map.put("customerId", customerId);
            map.put("receiptCode", receiptCode);
            map.put("transactionType", transactionType);
            map.put("balAmt", balAmt);
            map.put("availAmount", availAmount);
            map.put("adjustAmount", adjustAmount);
            map.put("userId", userId);
            map.put("custType", custType);
            map.put("adjustRemark", adjustRemark);
            System.out.println("map...." + map);
            status = (Integer) session.insert("billing.saveAdjustPayment", map);
            map.put("paymentId", 0);
            map.put("tempId", status);
            map.put("transactionName", "PdaAdjust");

            String availBlockedAmount = (String) session.queryForObject("operation.getAvailandBlockedAmount", map);
            System.out.println("availBlockedAmount------" + availBlockedAmount);
            String[] availBlockedAmountTemp = availBlockedAmount.split("~");

            map.put("fixedLimit", availBlockedAmountTemp[0]);
            map.put("blockedAmount", "0");
            map.put("usedLimit", "0");

            if (transactionType.equalsIgnoreCase("CREDIT")) {
                double tot = Double.parseDouble(availBlockedAmountTemp[1]) + Double.parseDouble(adjustAmount);
                map.put("availAmount", tot);
                map.put("transactionType", "Credit");
                map.put("creditAmount", adjustAmount);
                map.put("debitAmount", 0);
            } else {
                double tot = Double.parseDouble(availBlockedAmountTemp[1]) - Double.parseDouble(adjustAmount);
                map.put("availAmount", tot);
                map.put("transactionType", "Debit");
                map.put("creditAmount", 0);
                map.put("debitAmount", adjustAmount);
            }

            map.put("outStandingAmount", adjustAmount);
            System.out.println(" consignment insert map ::::::::" + map);

            int updateOutstandingLog = (Integer) session.update("operation.updateOutstandingLog", map);

            status = (Integer) session.update("billing.updateTempCreditLimit", map);

            //creditlimit = (Integer) getSqlMapClientTemplate().update("billing.updateTempCreditStatus", map);
            System.out.println(" pda update size=" + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStatus List", sqlException);
        }

        return status;
    }

    public int saveCreditAdjustPayment(String customerId, String receiptCode, String transactionType, String balAmt, String availAmount, String adjustRemark, String adjustAmount, String custType, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;

        try {

            map.put("customerId", customerId);
            map.put("receiptCode", receiptCode);
            map.put("transactionType", transactionType);
            map.put("balAmt", balAmt);
            map.put("availAmount", availAmount);
            map.put("adjustAmount", adjustAmount);
            map.put("userId", userId);
            map.put("custType", custType);
            map.put("adjustRemark", adjustRemark);
            System.out.println("map...." + map);
            status = (Integer) session.insert("billing.saveCreditAdjustPayment", map);
            map.put("paymentId", 0);
            map.put("tempId", status);
            map.put("transactionName", "CreditAdjust");

            String availBlockedAmount = (String) session.queryForObject("operation.getAvailandBlockedAmount", map);
            System.out.println("availBlockedAmount------" + availBlockedAmount);
            String[] availBlockedAmountTemp = availBlockedAmount.split("~");

            map.put("fixedLimit", availBlockedAmountTemp[0]);
            map.put("blockedAmount", "0");
            map.put("usedLimit", "0");

            if (transactionType.equalsIgnoreCase("CREDIT")) {
                double tot = Double.parseDouble(availBlockedAmountTemp[1]) + Double.parseDouble(adjustAmount);
                map.put("availAmount", tot);
                map.put("transactionType", "Credit");
                map.put("creditAmount", adjustAmount);
                map.put("debitAmount", 0);
            } else {
                double tot = Double.parseDouble(availBlockedAmountTemp[1]) - Double.parseDouble(adjustAmount);
                map.put("availAmount", tot);
                map.put("transactionType", "Debit");
                map.put("creditAmount", 0);
                map.put("debitAmount", adjustAmount);
            }

            map.put("outStandingAmount", adjustAmount);
            System.out.println(" consignment insert map ::::::::" + map);

            int updateOutstandingLog = (Integer) session.update("operation.updateOutstandingLog", map);

            status = (Integer) session.update("billing.updateTempCreditLimit", map);

            //creditlimit = (Integer) getSqlMapClientTemplate().update("billing.updateTempCreditStatus", map);
            System.out.println(" credit update size=" + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStatus List", sqlException);
        }

        return status;
    }

    public ArrayList getCustomerAdjustmentHistory(BillingTO billingTO) {
        Map map = new HashMap();

        map.put("customerId", billingTO.getCustomerId());
        System.out.println("map for getCustomerAdjustmentHistory:" + map);
        ArrayList customerAdjustmentHistory = new ArrayList();
        try {
            customerAdjustmentHistory = (ArrayList) getSqlMapClientTemplate().queryForList("billing.customerAdjustmentHistory", map);
            System.out.println("customerAdjustmentHistory size=" + customerAdjustmentHistory.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("customerAdjustmentHistory Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "customerAdjustmentHistory List", sqlException);
        }

        return customerAdjustmentHistory;
    }

    public int approveInvoice(BillingTO billingTO) {
        Map map = new HashMap();
        int status = 0;
        String[] invoiceIds = billingTO.getInvoiceIds();
        List invIds = new ArrayList(invoiceIds.length);
        for (int j = 0; j < invoiceIds.length; j++) {
            System.out.println("cn value:" + invoiceIds[j]);
            invIds.add(invoiceIds[j]);
        }
        map.put("invoiceIds", invIds);
        System.out.println("map for trip det:" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("billing.approveInvoice", map);
            System.out.println("Approve Invoice =" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return status;
    }

    public ArrayList getCreditAdjustmentHistory(BillingTO billingTO) {
        Map map = new HashMap();

        map.put("customerId", billingTO.getCustomerId());
        System.out.println("map for getCreditAdjustmentHistory:" + map);
        ArrayList customerAdjustmentHistory = new ArrayList();
        try {
            customerAdjustmentHistory = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getCreditAdjustmentHistory", map);
            System.out.println("getCreditAdjustmentHistory size=" + customerAdjustmentHistory.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("customerAdjustmentHistory Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "customerAdjustmentHistory List", sqlException);
        }

        return customerAdjustmentHistory;
    }

    public ArrayList getOrdersForAutoBilling() {
        Map map = new HashMap();
        ArrayList clisedTrips = new ArrayList();

        try {
            System.out.println(" i am in dao");
            clisedTrips = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getOrdersForAutoBilling", map);
            System.out.println(" i am in dao" + clisedTrips.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getClosedTrips Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getClosedTrips List", sqlException);
        }
        return clisedTrips;
    }

//     <!--NEW E Invoice Starts-->
    public ArrayList viewInvoiceList(String customerId, String fromDate, String toDate, String tripType, String submitStatus, String billNo, String grNo, String containerNo) {
        Map map = new HashMap();
        ArrayList closedBillList = null;
        try {
            map.put("customerId", customerId);
            if (fromDate == null) {
                fromDate = "";
            }
            if (toDate == null) {
                toDate = "";
            }
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("tripType", tripType);
            map.put("submitStatus", submitStatus);
            if (billNo != null && !"".equals(billNo)) {
                map.put("billNo", "%" + billNo + "%");
            } else {
                map.put("billNo", "");
            }
            if (grNo != null && !"".equals(grNo)) {
                map.put("grNo", "%" + grNo + "%");
            } else {
                map.put("grNo", "");
            }
            if (containerNo != null && !"".equals(containerNo)) {
                map.put("containerNo", "%" + containerNo + "%");
            } else {
                map.put("containerNo", "");
            }
            System.out.println("getClosedbill Value map" + map);
            closedBillList = (ArrayList) getSqlMapClientTemplate().queryForList("billing.viewInvoiceList", map);
            System.out.println("getbil details size is ::::" + closedBillList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return closedBillList;
    }

    public ArrayList viewSupplyInvoiceList(BillingTO billingTO) {
        Map map = new HashMap();
        ArrayList clisedTrips = null;

        try {
            map.put("customerId", billingTO.getCustomerId());
            map.put("fromDate", billingTO.getFromDate());
            map.put("toDate", billingTO.getToDate());
            map.put("tripType", billingTO.getBillingType());
            if (billingTO.getShipingBillNo() != null && !"".equals(billingTO.getShipingBillNo())) {
                map.put("billNo", "%" + billingTO.getShipingBillNo() + "%");
            } else {
                map.put("billNo", "");
            }
            if (billingTO.getBillOfEntryNo() != null && !"".equals(billingTO.getBillOfEntryNo())) {
                map.put("grNo", "%" + billingTO.getBillOfEntryNo() + "%");
            } else {
                map.put("grNo", "");
            }
            System.out.println("getClosedbill Value map" + map);
            System.out.println("closed Trip map is::" + map);
            clisedTrips = new ArrayList();
            clisedTrips = (ArrayList) getSqlMapClientTemplate().queryForList("billing.viewSupplyInvoiceList", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getClosedTrips Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getClosedTrips List", sqlException);
        }
        return clisedTrips;
    }

    public int eInvoiceGenerate(BillingTO billingTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        int headerStatus = 0;
        int detailStatus = 0;
        String[] invoiceIds = billingTO.getInvoiceIds();
        ArrayList getSellerBuyerDetails = new ArrayList();
        ArrayList getEinvoiceDetails = new ArrayList();
        try {
            List invIds = new ArrayList(invoiceIds.length);
            System.out.println("invoiceIds.length;----" + invoiceIds.length);
            for (int j = 0; j < invoiceIds.length; j++) {
                System.out.println("cn#### value:" + invoiceIds[j]);
                map.put("invoiceId", invoiceIds[j]);
                System.out.println("map for trip det:" + map);
                getSellerBuyerDetails = (ArrayList) session.queryForList("billing.getSellerBuyerDetails", map);
                System.out.println("getSellerBuyerDetails.size()---" + getSellerBuyerDetails.size());
                Iterator itr = getSellerBuyerDetails.iterator();
                BillingTO billTO = new BillingTO();
                while (itr.hasNext()) {
                    billTO = (BillingTO) itr.next();
                    map.put("invoiceId", billTO.getInvoiceId());
                    map.put("invoiceCode", billTO.getInvoiceCode());
                    map.put("invoiceDate", billTO.getInvoiceDate());
                    map.put("invType", billTO.getInvType());
                    map.put("billingCustId", billTO.getBillingCustId());
                    map.put("billingCustName", billTO.getBillingCustName());
                    map.put("billingAddress", billTO.getBillingAddress());
                    map.put("billingPanNo", billTO.getBillingPanNo());
                    map.put("billingPincode", billTO.getBillingPincode());
                    map.put("billingPos", billTO.getBillingPos());
                    map.put("billingLocation", billTO.getBillingLocation());
                    map.put("billingGst", billTO.getBillingGstin());
                    map.put("billingState", billTO.getBillingState());
                    map.put("grandTotal", billTO.getGrandTotal());
                    double cgstPercent = 0.0;
                    double sgstPercent = 0.0;
                    double igstPercent = 0.0;
                    double cgst = 0.0;
                    double sgst = 0.0;
                    double igst = 0.0;
                    double netAmount = 0.0;
                    if ("6".equals(billTO.getBillingState())) {
                        cgstPercent = 0.0;
                        sgstPercent = 0.0;
                        igstPercent = 0.0;
                        cgst = (Double.parseDouble(billTO.getGrandTotal()) * cgstPercent) / 100;
                        sgst = (Double.parseDouble(billTO.getGrandTotal()) * sgstPercent) / 100;
                        igst = (Double.parseDouble(billTO.getGrandTotal()) * igstPercent) / 100;
                        netAmount = (Double.parseDouble(billTO.getGrandTotal())) + igst + cgst + sgst;
                        map.put("cgstPercent", cgstPercent);
                        map.put("sgstPercent", sgstPercent);
                        map.put("igstPercent", igstPercent);
                        map.put("cgst", cgst);
                        map.put("sgst", sgst);
                        map.put("igst", igst);
                    } else {
                        cgstPercent = 0.0;
                        sgstPercent = 0.0;
                        igstPercent = 0.0;
                        cgst = (Double.parseDouble(billTO.getGrandTotal()) * cgstPercent) / 100;
                        sgst = (Double.parseDouble(billTO.getGrandTotal()) * sgstPercent) / 100;
                        igst = (Double.parseDouble(billTO.getGrandTotal()) * igstPercent) / 100;

                        netAmount = (Double.parseDouble(billTO.getGrandTotal())) + igst + cgst + sgst;
                        map.put("cgstPercent", cgstPercent);
                        map.put("sgstPercent", sgstPercent);
                        map.put("igstPercent", igstPercent);
                        map.put("cgst", cgst);
                        map.put("sgst", sgst);
                        map.put("igst", igst);
                    }

                    map.put("netAmount", netAmount);
                    map.put("version", "1.03");
                    map.put("igstOnIntra", "N");
                    map.put("regRev", "Y");
                    map.put("taxSch", "GST");
                    map.put("supTyp", "B2B");

                    String sellerDetailsTmp = billTO.getSellerDetails();
                    String sellerDetails[] = sellerDetailsTmp.split("~");
                    map.put("compName", sellerDetails[0]);
                    map.put("compAddress", sellerDetails[1]);
                    map.put("compLocation", sellerDetails[2]);
                    map.put("compState", sellerDetails[3]);
                    map.put("compPincode", sellerDetails[4]);
                    map.put("compGst", sellerDetails[5]);

                    System.out.println("MAP--insertEinvoiceHeader-----" + map);

                    headerStatus = (Integer) session.insert("billing.insertEinvoiceHeader", map);
                    System.out.println("insertEinvoiceHeader---" + headerStatus);
                }
                map.put("eInvoiceId", headerStatus);

                System.out.println("MAP HEADER---" + map);
                map.put("invoiceId", invoiceIds[j]);

                getEinvoiceDetails = (ArrayList) session.queryForList("billing.getEinvoiceDetails", map);
                System.out.println("getEinvoiceDetails.size()---" + getEinvoiceDetails.size());
                Iterator itr1 = getEinvoiceDetails.iterator();
                BillingTO billTO1 = new BillingTO();
                int sno = 0;
                while (itr1.hasNext()) {
                    billTO1 = (BillingTO) itr1.next();
                    sno = sno + 1;
                    map.put("sno", sno);
                    map.put("isServc", billTO1.getIsServc());
                    map.put("prdDesc", billTO1.getPrdDesc());
                    System.out.println("billTO1.getHsnCode()-----" + billTO1.getHsnCode());
                    map.put("hsnCode", billTO1.getHsnCode());
                    map.put("qty", billTO1.getQty());
                    map.put("unit", billTO1.getUnit());
                    map.put("unitPrice", billTO1.getUnitPrice());
                    map.put("totAmount", billTO1.getTotAmount());
                    map.put("assAmt", billTO1.getAssAmt());
                    double cgstPercent = 0.0;
                    double sgstPercent = 0.0;
                    double igstPercent = 0.0;
                    double cgst = 0.0;
                    double sgst = 0.0;
                    double igst = 0.0;
                    double totalValue = 0.0;
                    if ("6".equals(billTO1.getBillingState())) {
                        cgstPercent = 0.0;
                        sgstPercent = 0.0;
                        igstPercent = 0.0;
                        cgst = (Double.parseDouble(billTO1.getUnitPrice()) * cgstPercent) / 100;
                        sgst = (Double.parseDouble(billTO1.getUnitPrice()) * sgstPercent) / 100;
                        igst = (Double.parseDouble(billTO1.getUnitPrice()) * igstPercent) / 100;
                        totalValue = (Double.parseDouble(billTO1.getUnitPrice())) + igst + cgst + sgst;
                        map.put("cgstPercent", cgstPercent);
                        map.put("sgstPercent", sgstPercent);
                        map.put("igstPercent", igstPercent);
                        map.put("cgst", cgst);
                        map.put("sgst", sgst);
                        map.put("igst", igst);
                    } else {
                        cgstPercent = 0.0;
                        sgstPercent = 0.0;
                        igstPercent = 0.0;
                        cgst = (Double.parseDouble(billTO1.getUnitPrice()) * cgstPercent) / 100;
                        sgst = (Double.parseDouble(billTO1.getUnitPrice()) * sgstPercent) / 100;
                        igst = (Double.parseDouble(billTO1.getUnitPrice()) * igstPercent) / 100;

                        totalValue = (Double.parseDouble(billTO1.getUnitPrice())) + igst + cgst + sgst;
                        map.put("cgstPercent", cgstPercent);
                        map.put("sgstPercent", sgstPercent);
                        map.put("igstPercent", igstPercent);
                        map.put("cgst", cgst);
                        map.put("sgst", sgst);
                        map.put("igst", igst);
                    }

                    map.put("totalValue", totalValue);
                    map.put("expenseDesc", billTO1.getExpenseDesc());
                    map.put("tripId", billTO1.getTripId());
                    map.put("expenseId", billTO1.getExpenseId());

                    System.out.println("MAP--insertEInvoiceDetails-----" + map);
                    detailStatus = (Integer) session.update("billing.insertEInvoiceDetails", map);
                    System.out.println("eInvoiceDetailGenerate----" + detailStatus);
                }

                System.out.println(" Invoice =" + status);
                map.put("custId", billingTO.getCustomerId());
                if (headerStatus > 0 && detailStatus > 0) {
                    status = (Integer) session.update("billing.updateInvoiceHeaderPostStatus", map);
                    System.out.println("updateCustomerPostStatus =" + status);

                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return status;
    }

    public ArrayList viewEInvoiceList(String customerId, String fromDate, String toDate, String tripType, String submitStatus, String billNo, String grNo, String containerNo) {
        Map map = new HashMap();
        ArrayList closedBillList = null;
        try {
            map.put("customerId", customerId);
            if (fromDate == null) {
                fromDate = "";
            }
            if (toDate == null) {
                toDate = "";
            }
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("tripType", tripType);
            map.put("submitStatus", submitStatus);
            if (billNo != null && !"".equals(billNo)) {
                map.put("billNo", "%" + billNo + "%");
            } else {
                map.put("billNo", "");
            }
            if (grNo != null && !"".equals(grNo)) {
                map.put("grNo", "%" + grNo + "%");
            } else {
                map.put("grNo", "");
            }
            if (containerNo != null && !"".equals(containerNo)) {
                map.put("containerNo", "%" + containerNo + "%");
            } else {
                map.put("containerNo", "");
            }
            System.out.println("getClosedbill Value map" + map);
            closedBillList = (ArrayList) getSqlMapClientTemplate().queryForList("billing.viewEInvoiceList", map);
            System.out.println("getbil details size is ::::" + closedBillList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return closedBillList;
    }

    public ArrayList viewClosedEInvoiceList(String custId, String invoiceId) {
        Map map = new HashMap();
        ArrayList viewClosedEInvoiceList = null;
        try {
            map.put("customerId", custId);
            map.put("invoiceId", invoiceId);
            System.out.println("getClosedbill Value map" + map);
            viewClosedEInvoiceList = (ArrayList) getSqlMapClientTemplate().queryForList("billing.viewClosedEInvoiceList", map);
            System.out.println("getbil details size is ::::" + viewClosedEInvoiceList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return viewClosedEInvoiceList;
    }

    public ArrayList insertEInvoiceUploadTemp(ArrayList shippingLineNoList, String invoiceId) {
        Map map = new HashMap();
        int deleteSBillTemp = 0;
        int status = 0;
        int insertOrder = 0;
        ArrayList getSbillNoListTemp = new ArrayList();
        BillingTO billTO = new BillingTO();

        try {

            map.put("invoiceId", invoiceId);
            deleteSBillTemp = (Integer) getSqlMapClientTemplate().update("billing.deleteEinvoiceUpload", map);
            System.out.println("deleteSBillTemp------" + deleteSBillTemp);

            Iterator itr = shippingLineNoList.iterator();
            while (itr.hasNext()) {
                billTO = (BillingTO) itr.next();
                map.put("document_date", billTO.getDocumentDate());
                map.put("document_number", billTO.getDocumentNumber());
                map.put("document_type", billTO.getDocumentType());

                map.put("sellerGst", billTO.getSellerGst());
                map.put("buyerGst", billTO.getBuyerGst());
                map.put("status", billTO.getStatus());
                map.put("acknowledgementNumber", billTO.getAccNum());
                map.put("acknowledgementDate", billTO.getAccDate());

                map.put("irn", billTO.getIrn());
                map.put("SignedQrCode", billTO.getSignedQr());
                map.put("ewbNumber", billTO.getEwbNum());
                map.put("ewbDate", billTO.getEwbDate());
                map.put("ewbValidTill", billTO.getEwbValid());
                map.put("info", billTO.getInfo());
                map.put("error", billTO.getError());

                System.out.println("map value is: after flag set" + map);
                insertOrder = (Integer) getSqlMapClientTemplate().update("billing.insertEinvoiceUploadTemp", map);
                System.out.println("insertOrder--" + insertOrder);
            }

            if (insertOrder > 0) {
                getSbillNoListTemp = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getinsertEinvoiceUpload", map);
                System.out.println(" getSbillNoListTemp" + getSbillNoListTemp.size());
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertShippingLineNoList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertShippingLineNoList", sqlException);
        }
        return getSbillNoListTemp;
    }

    public ArrayList getEInvoiveUploadData(String invoiceId) {
        Map map = new HashMap();

        ArrayList getSbillNoListTemp = new ArrayList();
        BillingTO billTO = new BillingTO();

        try {

            map.put("invoiceId", invoiceId);

            getSbillNoListTemp = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getinsertEinvoiceUpload", map);
            System.out.println(" getSbillNoListTemp" + getSbillNoListTemp.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertShippingLineNoList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertShippingLineNoList", sqlException);
        }
        return getSbillNoListTemp;
    }

    public int insertEInvoiceUpload(BillingTO billTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        int uploadStatus = 0;
        int uploadEInvoiceStatus = 0;
        try {

            map.put("invoiceId", billTO.getInvoiceId());
            map.put("document_date", billTO.getDocumentDate());
            map.put("document_number", billTO.getDocumentNumber());
            map.put("document_type", billTO.getDocumentType());

            map.put("sellerGst", billTO.getSellerGst());
            map.put("buyerGst", billTO.getBuyerGst());
            map.put("status", billTO.getStatus());
            map.put("acknowledgementNumber", billTO.getAccNum());
            map.put("acknowledgementDate", billTO.getAccDate());

            map.put("irn", billTO.getIrn());
            map.put("SignedQrCode", billTO.getSignedQr());
            map.put("ewbNumber", billTO.getEwbNum());
            map.put("ewbDate", billTO.getEwbDate());
            map.put("ewbValidTill", billTO.getEwbValid());
            map.put("info", billTO.getInfo());
            map.put("error", billTO.getError());

            System.out.println("map after====" + map);

            status = (Integer) getSqlMapClientTemplate().update("billing.insertEinvoiceUpload", map);
            uploadStatus = (Integer) getSqlMapClientTemplate().update("billing.updateUploadStatus", map);
            uploadEInvoiceStatus = (Integer) getSqlMapClientTemplate().update("billing.updateEInvoiceUploadStatus", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertEInvoiceUpload Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertEInvoiceUpload List", sqlException);
        }

        return status;
    }

//  supp invoice starts
    public int eSuppInvoiceGenerate(BillingTO billingTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        int headerStatus = 0;
        int detailStatus = 0;
        String[] invoiceIds = billingTO.getInvoiceIds();
        ArrayList getSellerBuyerDetails = new ArrayList();
        ArrayList getEinvoiceDetails = new ArrayList();
        try {
            List invIds = new ArrayList(invoiceIds.length);
            for (int j = 0; j < invoiceIds.length; j++) {
                System.out.println("cn value:" + invoiceIds[j]);
                map.put("invoiceId", invoiceIds[j]);
                System.out.println("map for trip det:" + map);
                getSellerBuyerDetails = (ArrayList) session.queryForList("billing.getSuppSellerBuyerDetails", map);
                System.out.println("getSellerBuyerDetails.size()---" + getSellerBuyerDetails.size());
                Iterator itr = getSellerBuyerDetails.iterator();
                BillingTO billTO = new BillingTO();
                while (itr.hasNext()) {
                    billTO = (BillingTO) itr.next();
                    map.put("invoiceId", billTO.getInvoiceId());
                    map.put("invoiceCode", billTO.getInvoiceCode());
                    map.put("invoiceDate", billTO.getInvoiceDate());
                    map.put("invType", billTO.getInvType());
                    map.put("billingCustId", billTO.getBillingCustId());
                    map.put("billingCustName", billTO.getBillingCustName());
                    map.put("billingAddress", billTO.getBillingAddress());
                    map.put("billingPanNo", billTO.getBillingPanNo());
                    map.put("billingPincode", billTO.getBillingPincode());
                    map.put("billingPos", billTO.getBillingPos());
                    map.put("billingLocation", billTO.getBillingLocation());
                    map.put("billingGst", billTO.getBillingGstin());
                    map.put("billingState", billTO.getBillingState());
                    map.put("grandTotal", billTO.getGrandTotal());
                    double cgstPercent = 0.0;
                    double sgstPercent = 0.0;
                    double igstPercent = 0.0;
                    double cgst = 0.0;
                    double sgst = 0.0;
                    double igst = 0.0;
                    double netAmount = 0.0;
                    if ("6".equals(billTO.getBillingState())) {
                        cgstPercent = 0.0;
                        sgstPercent = 0.0;
                        igstPercent = 0.0;
                        cgst = (Double.parseDouble(billTO.getGrandTotal()) * cgstPercent) / 100;
                        sgst = (Double.parseDouble(billTO.getGrandTotal()) * sgstPercent) / 100;
                        igst = (Double.parseDouble(billTO.getGrandTotal()) * igstPercent) / 100;
                        netAmount = (Double.parseDouble(billTO.getGrandTotal())) + igst + cgst + sgst;
                        map.put("cgstPercent", cgstPercent);
                        map.put("sgstPercent", sgstPercent);
                        map.put("igstPercent", igstPercent);
                        map.put("cgst", cgst);
                        map.put("sgst", sgst);
                        map.put("igst", igst);
                    } else {
                        cgstPercent = 0.0;
                        sgstPercent = 0.0;
                        igstPercent = 0.0;
                        cgst = (Double.parseDouble(billTO.getGrandTotal()) * cgstPercent) / 100;
                        sgst = (Double.parseDouble(billTO.getGrandTotal()) * sgstPercent) / 100;
                        igst = (Double.parseDouble(billTO.getGrandTotal()) * igstPercent) / 100;

                        netAmount = (Double.parseDouble(billTO.getGrandTotal())) + igst + cgst + sgst;
                        map.put("cgstPercent", cgstPercent);
                        map.put("sgstPercent", sgstPercent);
                        map.put("igstPercent", igstPercent);
                        map.put("cgst", cgst);
                        map.put("sgst", sgst);
                        map.put("igst", igst);
                    }

                    map.put("netAmount", netAmount);
                    map.put("version", "1.03");
                    map.put("igstOnIntra", "N");
                    map.put("regRev", "Y");
                    map.put("taxSch", "GST");
                    map.put("supTyp", "B2B");

                    String sellerDetailsTmp = billTO.getSellerDetails();
                    String sellerDetails[] = sellerDetailsTmp.split("~");
                    map.put("compName", sellerDetails[0]);
                    map.put("compAddress", sellerDetails[1]);
                    map.put("compLocation", sellerDetails[2]);
                    map.put("compState", sellerDetails[3]);
                    map.put("compPincode", sellerDetails[4]);
                    map.put("compGst", sellerDetails[5]);

                    System.out.println("MAP--insertEinvoiceHeader-----" + map);

                    headerStatus = (Integer) session.insert("billing.insertESuppInvoiceHeader", map);
                    System.out.println("insertEinvoiceHeader---" + headerStatus);
                }
                map.put("eInvoiceId", headerStatus);

                System.out.println("MAP HEADER---" + map);
                map.put("invoiceId", invoiceIds[j]);

                getEinvoiceDetails = (ArrayList) session.queryForList("billing.getESuppInvoiceDetails", map);
                System.out.println("getEinvoiceDetails.size()---" + getEinvoiceDetails.size());
                Iterator itr1 = getEinvoiceDetails.iterator();
                BillingTO billTO1 = new BillingTO();
                int sno = 0;
                while (itr1.hasNext()) {
                    billTO1 = (BillingTO) itr1.next();
                    sno = sno + 1;
                    map.put("sno", sno);
                    map.put("isServc", billTO1.getIsServc());
                    map.put("prdDesc", billTO1.getPrdDesc());
                    System.out.println("billTO1.getHsnCode()-----" + billTO1.getHsnCode());
                    map.put("hsnCode", billTO1.getHsnCode());
                    map.put("qty", billTO1.getQty());
                    map.put("unit", billTO1.getUnit());
                    map.put("unitPrice", billTO1.getUnitPrice());
                    map.put("totAmount", billTO1.getTotAmount());
                    map.put("assAmt", billTO1.getAssAmt());
                    double cgstPercent = 0.0;
                    double sgstPercent = 0.0;
                    double igstPercent = 0.0;
                    double cgst = 0.0;
                    double sgst = 0.0;
                    double igst = 0.0;
                    double totalValue = 0.0;
                    if ("6".equals(billTO1.getBillingState())) {
                        cgstPercent = 0.0;
                        sgstPercent = 0.0;
                        igstPercent = 0.0;
                        cgst = (Double.parseDouble(billTO1.getUnitPrice()) * cgstPercent) / 100;
                        sgst = (Double.parseDouble(billTO1.getUnitPrice()) * sgstPercent) / 100;
                        igst = (Double.parseDouble(billTO1.getUnitPrice()) * igstPercent) / 100;
                        totalValue = (Double.parseDouble(billTO1.getUnitPrice())) + igst + cgst + sgst;
                        map.put("cgstPercent", cgstPercent);
                        map.put("sgstPercent", sgstPercent);
                        map.put("igstPercent", igstPercent);
                        map.put("cgst", cgst);
                        map.put("sgst", sgst);
                        map.put("igst", igst);
                    } else {
                        cgstPercent = 0.0;
                        sgstPercent = 0.0;
                        igstPercent = 0.0;
                        cgst = (Double.parseDouble(billTO1.getUnitPrice()) * cgstPercent) / 100;
                        sgst = (Double.parseDouble(billTO1.getUnitPrice()) * sgstPercent) / 100;
                        igst = (Double.parseDouble(billTO1.getUnitPrice()) * igstPercent) / 100;

                        totalValue = (Double.parseDouble(billTO1.getUnitPrice())) + igst + cgst + sgst;
                        map.put("cgstPercent", cgstPercent);
                        map.put("sgstPercent", sgstPercent);
                        map.put("igstPercent", igstPercent);
                        map.put("cgst", cgst);
                        map.put("sgst", sgst);
                        map.put("igst", igst);
                    }

                    map.put("totalValue", totalValue);
                    map.put("expenseDesc", billTO1.getExpenseDesc());
                    map.put("tripId", billTO1.getTripId());
                    map.put("expenseId", billTO1.getExpenseId());

                    System.out.println("MAP--insertEInvoiceDetails-----" + map);
                    detailStatus = (Integer) session.update("billing.insertESuppInvoiceDetails", map);
                    System.out.println("eInvoiceDetailGenerate----" + detailStatus);
                }
                System.out.println(" Invoice =" + status);
                map.put("custId", billingTO.getCustomerId());
                if (headerStatus > 0 && detailStatus > 0) {
                    status = (Integer) session.update("billing.updateSuppInvoiceHeaderPostStatus", map);
                    System.out.println("updateCustomerPostStatus =" + status);

                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return status;
    }

    public ArrayList viewESuppInvoiceList(String customerId, String fromDate, String toDate, String tripType, String submitStatus, String billNo, String grNo, String containerNo) {
        Map map = new HashMap();
        ArrayList closedBillList = null;
        try {
            map.put("customerId", customerId);
            if (fromDate == null) {
                fromDate = "";
            }
            if (toDate == null) {
                toDate = "";
            }
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("tripType", tripType);
            map.put("submitStatus", submitStatus);
            if (billNo != null && !"".equals(billNo)) {
                map.put("billNo", "%" + billNo + "%");
            } else {
                map.put("billNo", "");
            }
            if (grNo != null && !"".equals(grNo)) {
                map.put("grNo", "%" + grNo + "%");
            } else {
                map.put("grNo", "");
            }
            if (containerNo != null && !"".equals(containerNo)) {
                map.put("containerNo", "%" + containerNo + "%");
            } else {
                map.put("containerNo", "");
            }
            System.out.println("getClosedbill Value map" + map);
            closedBillList = (ArrayList) getSqlMapClientTemplate().queryForList("billing.viewESuppInvoiceList", map);
            System.out.println("getbil details size is ::::" + closedBillList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return closedBillList;
    }

    public ArrayList viewClosedESuppInvoiceList(String custId, String invoiceId) {
        Map map = new HashMap();
        ArrayList viewClosedEInvoiceList = null;
        try {
            map.put("customerId", custId);
            map.put("invoiceId", invoiceId);
            System.out.println("getClosedbill Value map" + map);
            viewClosedEInvoiceList = (ArrayList) getSqlMapClientTemplate().queryForList("billing.viewClosedESuppInvoiceList", map);
            System.out.println("getbil details size is ::::" + viewClosedEInvoiceList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return viewClosedEInvoiceList;
    }

    public ArrayList insertESuppInvoiceUploadTemp(ArrayList shippingLineNoList, String invoiceId) {
        Map map = new HashMap();
        int deleteSBillTemp = 0;
        int status = 0;
        int insertOrder = 0;
        ArrayList getSbillNoListTemp = new ArrayList();
        BillingTO billTO = new BillingTO();

        try {

            map.put("invoiceId", invoiceId);
            deleteSBillTemp = (Integer) getSqlMapClientTemplate().update("billing.deleteESuppInvoiceUpload", map);
            System.out.println("deleteSBillTemp------" + deleteSBillTemp);

            Iterator itr = shippingLineNoList.iterator();
            while (itr.hasNext()) {
                billTO = (BillingTO) itr.next();
                map.put("document_date", billTO.getDocumentDate());
                map.put("document_number", billTO.getDocumentNumber());
                map.put("document_type", billTO.getDocumentType());

                map.put("sellerGst", billTO.getSellerGst());
                map.put("buyerGst", billTO.getBuyerGst());
                map.put("status", billTO.getStatus());
                map.put("acknowledgementNumber", billTO.getAccNum());
                map.put("acknowledgementDate", billTO.getAccDate());

                map.put("irn", billTO.getIrn());
                map.put("SignedQrCode", billTO.getSignedQr());
                map.put("ewbNumber", billTO.getEwbNum());
                map.put("ewbDate", billTO.getEwbDate());
                map.put("ewbValidTill", billTO.getEwbValid());
                map.put("info", billTO.getInfo());
                map.put("error", billTO.getError());

                System.out.println("map value is: after flag set" + map);
                insertOrder = (Integer) getSqlMapClientTemplate().update("billing.insertESuppInvoiceUploadTemp", map);
                System.out.println("insertOrder--" + insertOrder);
            }

            if (insertOrder > 0) {
                getSbillNoListTemp = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getInsertESuppInvoiceUpload", map);
                System.out.println(" getSbillNoListTemp" + getSbillNoListTemp.size());
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertShippingLineNoList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertShippingLineNoList", sqlException);
        }
        return getSbillNoListTemp;
    }

    public ArrayList getESuppInvoiveUploadData(String invoiceId) {
        Map map = new HashMap();

        ArrayList getSbillNoListTemp = new ArrayList();
        BillingTO billTO = new BillingTO();

        try {

            map.put("invoiceId", invoiceId);

            getSbillNoListTemp = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getInsertESuppInvoiceUpload", map);
            System.out.println(" getSbillNoListTemp" + getSbillNoListTemp.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertShippingLineNoList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertShippingLineNoList", sqlException);
        }
        return getSbillNoListTemp;
    }

    public int insertESuppInvoiceUpload(BillingTO billTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        int uploadStatus = 0;
        int uploadEInvoiceStatus = 0;
        try {

            map.put("invoiceId", billTO.getInvoiceId());
            map.put("document_date", billTO.getDocumentDate());
            map.put("document_number", billTO.getDocumentNumber());
            map.put("document_type", billTO.getDocumentType());

            map.put("sellerGst", billTO.getSellerGst());
            map.put("buyerGst", billTO.getBuyerGst());
            map.put("status", billTO.getStatus());
            map.put("acknowledgementNumber", billTO.getAccNum());
            map.put("acknowledgementDate", billTO.getAccDate());

            map.put("irn", billTO.getIrn());
            map.put("SignedQrCode", billTO.getSignedQr());
            map.put("ewbNumber", billTO.getEwbNum());
            map.put("ewbDate", billTO.getEwbDate());
            map.put("ewbValidTill", billTO.getEwbValid());
            map.put("info", billTO.getInfo());
            map.put("error", billTO.getError());

            System.out.println("map after====" + map);

            status = (Integer) getSqlMapClientTemplate().update("billing.insertESuppInvoiceUpload", map);
            uploadStatus = (Integer) getSqlMapClientTemplate().update("billing.updateSuppUploadStatus", map);
            uploadEInvoiceStatus = (Integer) getSqlMapClientTemplate().update("billing.updateESuppInvoiceUploadStatus", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertESuppInvoiceUpload Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertESuppInvoiceUpload List", sqlException);
        }

        return status;
    }

    //EInvoice starts
    public ArrayList getEcreditNoteList(BillingTO billingTO) {
        Map map = new HashMap();
        ArrayList creditNoteSearchList = new ArrayList();
        map.put("customerId", billingTO.getCustomerId());
        map.put("fromDate", billingTO.getFromDate());
        map.put("toDate", billingTO.getToDate());
        map.put("billNo", billingTO.getBillNo());
        try {
            System.out.println("creditNoteSearchList map= " + map);
            creditNoteSearchList = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getEcreditNoteList", map);
            System.out.println("getCreditNoteSearchList DAO= " + creditNoteSearchList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCreditNoteSearchList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getCreditNoteSearchList", sqlException);
        }
        return creditNoteSearchList;
    }

    public ArrayList getEsupplyCreditNoteList(BillingTO billingTO) {
        Map map = new HashMap();
        ArrayList creditNoteSearchList = new ArrayList();
        map.put("customerId", billingTO.getCustomerId());
        map.put("fromDate", billingTO.getFromDate());
        map.put("toDate", billingTO.getToDate());
        map.put("billNo", billingTO.getBillNo());
        try {
            System.out.println("creditNoteSearchList map= " + map);
            creditNoteSearchList = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getEsupplyCreditNoteList", map);
            System.out.println("getCreditNoteSearchList DAO= " + creditNoteSearchList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCreditNoteSearchList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getCreditNoteSearchList", sqlException);
        }
        return creditNoteSearchList;
    }

//       credit e-invoice
    public int eCreditInvoiceGenerate(BillingTO billingTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        int headerStatus = 0;
        int detailStatus = 0;
        String[] invoiceIds = billingTO.getInvoiceIds();
        ArrayList getSellerBuyerDetails = new ArrayList();
        ArrayList getEinvoiceDetails = new ArrayList();
        try {
            List invIds = new ArrayList(invoiceIds.length);
            for (int j = 0; j < invoiceIds.length; j++) {
                System.out.println("cn value:" + invoiceIds[j]);
                map.put("invoiceId", invoiceIds[j]);
                System.out.println("map for trip det:" + map);
                getSellerBuyerDetails = (ArrayList) session.queryForList("billing.getCreditSellerBuyerDetails", map);
                System.out.println("getSellerBuyerDetails.size()---" + getSellerBuyerDetails.size());
                Iterator itr = getSellerBuyerDetails.iterator();
                BillingTO billTO = new BillingTO();
                while (itr.hasNext()) {
                    billTO = (BillingTO) itr.next();
                    map.put("invoiceId", billTO.getInvoiceId());
                    map.put("invoiceCode", billTO.getInvoiceCode());
                    map.put("invoiceDate", billTO.getInvoiceDate());
                    map.put("refInvoicecode", billTO.getRefInvoicecode());
                    map.put("refInvoiceDate", billTO.getRefInvoiceDate());
                    map.put("invType", billTO.getInvType());
                    map.put("billingCustId", billTO.getBillingCustId());
                    map.put("billingCustName", billTO.getBillingCustName());
                    map.put("billingAddress", billTO.getBillingAddress());
                    map.put("billingPanNo", billTO.getBillingPanNo());
                    map.put("billingPincode", billTO.getBillingPincode());
                    map.put("billingPos", billTO.getBillingPos());
                    map.put("billingLocation", billTO.getBillingLocation());
                    map.put("billingGst", billTO.getBillingGstin());
                    map.put("billingState", billTO.getBillingState());
                    map.put("grandTotal", billTO.getGrandTotal());
                    double cgstPercent = 0.0;
                    double sgstPercent = 0.0;
                    double igstPercent = 0.0;
                    double cgst = 0.0;
                    double sgst = 0.0;
                    double igst = 0.0;
                    double netAmount = 0.0;
                    if ("6".equals(billTO.getBillingState())) {
                        cgstPercent = 0.0;
                        sgstPercent = 0.0;
                        igstPercent = 0.0;
                        cgst = (Double.parseDouble(billTO.getGrandTotal()) * cgstPercent) / 100;
                        sgst = (Double.parseDouble(billTO.getGrandTotal()) * sgstPercent) / 100;
                        igst = (Double.parseDouble(billTO.getGrandTotal()) * igstPercent) / 100;
                        netAmount = (Double.parseDouble(billTO.getGrandTotal())) + igst + cgst + sgst;
                        map.put("cgstPercent", cgstPercent);
                        map.put("sgstPercent", sgstPercent);
                        map.put("igstPercent", igstPercent);
                        map.put("cgst", cgst);
                        map.put("sgst", sgst);
                        map.put("igst", igst);
                    } else {
                        cgstPercent = 0.0;
                        sgstPercent = 0.0;
                        igstPercent = 0.0;
                        cgst = (Double.parseDouble(billTO.getGrandTotal()) * cgstPercent) / 100;
                        sgst = (Double.parseDouble(billTO.getGrandTotal()) * sgstPercent) / 100;
                        igst = (Double.parseDouble(billTO.getGrandTotal()) * igstPercent) / 100;

                        netAmount = (Double.parseDouble(billTO.getGrandTotal())) + igst + cgst + sgst;
                        map.put("cgstPercent", cgstPercent);
                        map.put("sgstPercent", sgstPercent);
                        map.put("igstPercent", igstPercent);
                        map.put("cgst", cgst);
                        map.put("sgst", sgst);
                        map.put("igst", igst);
                    }

                    map.put("netAmount", netAmount);
                    map.put("version", "1.03");
                    map.put("igstOnIntra", "N");
                    map.put("regRev", "Y");
                    map.put("taxSch", "GST");
                    map.put("supTyp", "B2B");

                    String sellerDetailsTmp = billTO.getSellerDetails();
                    String sellerDetails[] = sellerDetailsTmp.split("~");
                    map.put("compName", sellerDetails[0]);
                    map.put("compAddress", sellerDetails[1]);
                    map.put("compLocation", sellerDetails[2]);
                    map.put("compState", sellerDetails[3]);
                    map.put("compPincode", sellerDetails[4]);
                    map.put("compGst", sellerDetails[5]);

                    System.out.println("MAP--insertEinvoiceHeader-----" + map);

                    headerStatus = (Integer) session.insert("billing.insertECreditInvoiceHeader", map);
                    System.out.println("insertEinvoiceHeader---" + headerStatus);
                }
                map.put("eInvoiceId", headerStatus);

                System.out.println("MAP HEADER---" + map);
                map.put("invoiceId", invoiceIds[j]);

                getEinvoiceDetails = (ArrayList) session.queryForList("billing.getECreditInvoiceDetails", map);
                System.out.println("getEinvoiceDetails.size()---" + getEinvoiceDetails.size());
                Iterator itr1 = getEinvoiceDetails.iterator();
                BillingTO billTO1 = new BillingTO();
                int sno = 0;
                while (itr1.hasNext()) {
                    billTO1 = (BillingTO) itr1.next();
                    sno = sno + 1;
                    map.put("sno", sno);
                    map.put("isServc", billTO1.getIsServc());
                    map.put("prdDesc", billTO1.getPrdDesc());
                    System.out.println("billTO1.getHsnCode()-----" + billTO1.getHsnCode());
                    map.put("hsnCode", billTO1.getHsnCode());
                    map.put("qty", billTO1.getQty());
                    map.put("unit", billTO1.getUnit());
                    map.put("unitPrice", billTO1.getUnitPrice());
                    map.put("totAmount", billTO1.getTotAmount());
                    map.put("assAmt", billTO1.getAssAmt());
                    double cgstPercent = 0.0;
                    double sgstPercent = 0.0;
                    double igstPercent = 0.0;
                    double cgst = 0.0;
                    double sgst = 0.0;
                    double igst = 0.0;
                    double totalValue = 0.0;
                    if ("6".equals(billTO1.getBillingState())) {
                        cgstPercent = 0.0;
                        sgstPercent = 0.0;
                        igstPercent = 0.0;
                        cgst = (Double.parseDouble(billTO1.getUnitPrice()) * cgstPercent) / 100;
                        sgst = (Double.parseDouble(billTO1.getUnitPrice()) * sgstPercent) / 100;
                        igst = (Double.parseDouble(billTO1.getUnitPrice()) * igstPercent) / 100;
                        totalValue = (Double.parseDouble(billTO1.getUnitPrice())) + igst + cgst + sgst;
                        map.put("cgstPercent", cgstPercent);
                        map.put("sgstPercent", sgstPercent);
                        map.put("igstPercent", igstPercent);
                        map.put("cgst", cgst);
                        map.put("sgst", sgst);
                        map.put("igst", igst);
                    } else {
                        cgstPercent = 0.0;
                        sgstPercent = 0.0;
                        igstPercent = 0.0;
                        cgst = (Double.parseDouble(billTO1.getUnitPrice()) * cgstPercent) / 100;
                        sgst = (Double.parseDouble(billTO1.getUnitPrice()) * sgstPercent) / 100;
                        igst = (Double.parseDouble(billTO1.getUnitPrice()) * igstPercent) / 100;

                        totalValue = (Double.parseDouble(billTO1.getUnitPrice())) + igst + cgst + sgst;
                        map.put("cgstPercent", cgstPercent);
                        map.put("sgstPercent", sgstPercent);
                        map.put("igstPercent", igstPercent);
                        map.put("cgst", cgst);
                        map.put("sgst", sgst);
                        map.put("igst", igst);
                    }

                    map.put("totalValue", totalValue);
                    map.put("expenseDesc", billTO1.getExpenseDesc());
                    map.put("tripId", billTO1.getTripId());
                    map.put("expenseId", billTO1.getExpenseId());

                    System.out.println("MAP--insertEInvoiceDetails-----" + map);
                    detailStatus = (Integer) session.update("billing.insertECreditInvoiceDetails", map);
                    System.out.println("eInvoiceDetailGenerate----" + detailStatus);
                }
                System.out.println(" Invoice =" + detailStatus);
                map.put("custId", billingTO.getCustomerId());
                if (headerStatus > 0 && detailStatus > 0) {
                    status = (Integer) session.update("billing.updateCreditInvoiceHeaderPostStatus", map);
                    System.out.println("updateCustomerPostStatus =" + status);

                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return status;
    }

    public int eCreditSuppInvoiceGenerate(BillingTO billingTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        int headerStatus = 0;
        int detailStatus = 0;
        String[] invoiceIds = billingTO.getInvoiceIds();
        ArrayList getSellerBuyerDetails = new ArrayList();
        ArrayList getEinvoiceDetails = new ArrayList();
        try {
            List invIds = new ArrayList(invoiceIds.length);
            for (int j = 0; j < invoiceIds.length; j++) {
                System.out.println("cn value:" + invoiceIds[j]);
                map.put("invoiceId", invoiceIds[j]);
                System.out.println("map for trip det:" + map);
                getSellerBuyerDetails = (ArrayList) session.queryForList("billing.getCreditSuppSellerBuyerDetails", map);
                System.out.println("getSellerBuyerDetails.size()---" + getSellerBuyerDetails.size());
                Iterator itr = getSellerBuyerDetails.iterator();
                BillingTO billTO = new BillingTO();
                while (itr.hasNext()) {
                    billTO = (BillingTO) itr.next();
                    map.put("invoiceCode", billTO.getInvoiceCode());
                    map.put("invoiceDate", billTO.getInvoiceDate());
                    map.put("refInvoicecode", billTO.getRefInvoicecode());
                    map.put("refInvoiceDate", billTO.getRefInvoiceDate());
                    map.put("invType", billTO.getInvType());
                    map.put("billingCustId", billTO.getBillingCustId());
                    map.put("billingCustName", billTO.getBillingCustName());
                    map.put("billingAddress", billTO.getBillingAddress());
                    map.put("billingPanNo", billTO.getBillingPanNo());
                    map.put("billingPincode", billTO.getBillingPincode());
                    map.put("billingPos", billTO.getBillingPos());
                    map.put("billingLocation", billTO.getBillingLocation());
                    map.put("billingGst", billTO.getBillingGstin());
                    map.put("billingState", billTO.getBillingState());
                    map.put("grandTotal", billTO.getGrandTotal());
                    map.put("grandTotal", billTO.getGrandTotal());
                    double cgstPercent = 0.0;
                    double sgstPercent = 0.0;
                    double igstPercent = 0.0;
                    double cgst = 0.0;
                    double sgst = 0.0;
                    double igst = 0.0;
                    double netAmount = 0.0;
                    if ("6".equals(billTO.getBillingState())) {
                        cgstPercent = 0.0;
                        sgstPercent = 0.0;
                        igstPercent = 0.0;
                        cgst = (Double.parseDouble(billTO.getGrandTotal()) * cgstPercent) / 100;
                        sgst = (Double.parseDouble(billTO.getGrandTotal()) * sgstPercent) / 100;
                        igst = (Double.parseDouble(billTO.getGrandTotal()) * igstPercent) / 100;
                        netAmount = (Double.parseDouble(billTO.getGrandTotal())) + igst + cgst + sgst;
                        map.put("cgstPercent", cgstPercent);
                        map.put("sgstPercent", sgstPercent);
                        map.put("igstPercent", igstPercent);
                        map.put("cgst", cgst);
                        map.put("sgst", sgst);
                        map.put("igst", igst);
                    } else {
                        cgstPercent = 0.0;
                        sgstPercent = 0.0;
                        igstPercent = 0.0;
                        cgst = (Double.parseDouble(billTO.getGrandTotal()) * cgstPercent) / 100;
                        sgst = (Double.parseDouble(billTO.getGrandTotal()) * sgstPercent) / 100;
                        igst = (Double.parseDouble(billTO.getGrandTotal()) * igstPercent) / 100;

                        netAmount = (Double.parseDouble(billTO.getGrandTotal())) + igst + cgst + sgst;
                        map.put("cgstPercent", cgstPercent);
                        map.put("sgstPercent", sgstPercent);
                        map.put("igstPercent", igstPercent);
                        map.put("cgst", cgst);
                        map.put("sgst", sgst);
                        map.put("igst", igst);
                    }

                    map.put("netAmount", netAmount);
                    map.put("version", "1.03");
                    map.put("igstOnIntra", "N");
                    map.put("regRev", "Y");
                    map.put("taxSch", "GST");
                    map.put("supTyp", "B2B");

                    String sellerDetailsTmp = billTO.getSellerDetails();
                    String sellerDetails[] = sellerDetailsTmp.split("~");
                    map.put("compName", sellerDetails[0]);
                    map.put("compAddress", sellerDetails[1]);
                    map.put("compLocation", sellerDetails[2]);
                    map.put("compState", sellerDetails[3]);
                    map.put("compPincode", sellerDetails[4]);
                    map.put("compGst", sellerDetails[5]);

                    System.out.println("MAP--insertEinvoiceHeader-----" + map);

                    headerStatus = (Integer) session.insert("billing.insertECreditSuppInvoiceHeader", map);
                    System.out.println("insertEinvoiceHeader---" + headerStatus);
                }
                map.put("eInvoiceId", headerStatus);

                System.out.println("MAP HEADER---" + map);
                map.put("invoiceId", invoiceIds[j]);

                getEinvoiceDetails = (ArrayList) session.queryForList("billing.getECreditSuppInvoiceDetails", map);
                System.out.println("getEinvoiceDetails.size()---" + getEinvoiceDetails.size());
                Iterator itr1 = getEinvoiceDetails.iterator();
                BillingTO billTO1 = new BillingTO();
                int sno = 0;
                while (itr1.hasNext()) {
                    billTO1 = (BillingTO) itr1.next();
                    sno = sno + 1;
                    map.put("sno", sno);
                    map.put("isServc", billTO1.getIsServc());
                    map.put("prdDesc", billTO1.getPrdDesc());
                    System.out.println("billTO1.getHsnCode()-----" + billTO1.getHsnCode());
                    map.put("hsnCode", billTO1.getHsnCode());
                    map.put("qty", billTO1.getQty());
                    map.put("unit", billTO1.getUnit());
                    map.put("unitPrice", billTO1.getUnitPrice());
                    map.put("totAmount", billTO1.getTotAmount());
                    map.put("assAmt", billTO1.getAssAmt());
                    double cgstPercent = 0.0;
                    double sgstPercent = 0.0;
                    double igstPercent = 0.0;
                    double cgst = 0.0;
                    double sgst = 0.0;
                    double igst = 0.0;
                    double totalValue = 0.0;
                    if ("6".equals(billTO1.getBillingState())) {
                        cgstPercent = 0.0;
                        sgstPercent = 0.0;
                        igstPercent = 0.0;
                        cgst = (Double.parseDouble(billTO1.getUnitPrice()) * cgstPercent) / 100;
                        sgst = (Double.parseDouble(billTO1.getUnitPrice()) * sgstPercent) / 100;
                        igst = (Double.parseDouble(billTO1.getUnitPrice()) * igstPercent) / 100;
                        totalValue = (Double.parseDouble(billTO1.getUnitPrice())) + igst + cgst + sgst;
                        map.put("cgstPercent", cgstPercent);
                        map.put("sgstPercent", sgstPercent);
                        map.put("igstPercent", igstPercent);
                        map.put("cgst", cgst);
                        map.put("sgst", sgst);
                        map.put("igst", igst);
                    } else {
                        cgstPercent = 0.0;
                        sgstPercent = 0.0;
                        igstPercent = 0.0;
                        cgst = (Double.parseDouble(billTO1.getUnitPrice()) * cgstPercent) / 100;
                        sgst = (Double.parseDouble(billTO1.getUnitPrice()) * sgstPercent) / 100;
                        igst = (Double.parseDouble(billTO1.getUnitPrice()) * igstPercent) / 100;

                        totalValue = (Double.parseDouble(billTO1.getUnitPrice())) + igst + cgst + sgst;
                        map.put("cgstPercent", cgstPercent);
                        map.put("sgstPercent", sgstPercent);
                        map.put("igstPercent", igstPercent);
                        map.put("cgst", cgst);
                        map.put("sgst", sgst);
                        map.put("igst", igst);
                    }

                    map.put("totalValue", totalValue);
                    map.put("expenseDesc", billTO1.getExpenseDesc());
                    map.put("tripId", billTO1.getTripId());
                    map.put("expenseId", billTO1.getExpenseId());

                    System.out.println("MAP--insertEInvoiceDetails-----" + map);
                    detailStatus = (Integer) session.update("billing.insertECreditSuppInvoiceDetails", map);
                    System.out.println("eInvoiceDetailGenerate----" + detailStatus);
                }
                System.out.println(" Invoice =" + status);
                map.put("custId", billingTO.getCustomerId());
                if (headerStatus > 0 && detailStatus > 0) {
                    status = (Integer) session.update("billing.updateCreditSuppInvoiceHeaderPostStatus", map);
                    System.out.println("updateCustomerPostStatus =" + status);

                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return status;
    }

    public ArrayList viewECreditInvoiceList(String customerId, String fromDate, String toDate, String tripType, String submitStatus, String billNo, String grNo, String containerNo) {
        Map map = new HashMap();
        ArrayList closedBillList = null;
        try {
            map.put("customerId", customerId);
            if (fromDate == null) {
                fromDate = "";
            }
            if (toDate == null) {
                toDate = "";
            }
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("tripType", tripType);
            map.put("submitStatus", submitStatus);
            if (billNo != null && !"".equals(billNo)) {
                map.put("billNo", "%" + billNo + "%");
            } else {
                map.put("billNo", "");
            }
            if (grNo != null && !"".equals(grNo)) {
                map.put("grNo", "%" + grNo + "%");
            } else {
                map.put("grNo", "");
            }
            if (containerNo != null && !"".equals(containerNo)) {
                map.put("containerNo", "%" + containerNo + "%");
            } else {
                map.put("containerNo", "");
            }
            System.out.println("getClosedbill Value map" + map);
            closedBillList = (ArrayList) getSqlMapClientTemplate().queryForList("billing.viewECreditInvoiceList", map);
            System.out.println("getbil details size is ::::" + closedBillList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return closedBillList;
    }

    public ArrayList viewECreditSuppInvoiceList(String customerId, String fromDate, String toDate, String tripType, String submitStatus, String billNo, String grNo, String containerNo) {
        Map map = new HashMap();
        ArrayList closedBillList = null;
        try {
            map.put("customerId", customerId);
            if (fromDate == null) {
                fromDate = "";
            }
            if (toDate == null) {
                toDate = "";
            }
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("tripType", tripType);
            map.put("submitStatus", submitStatus);
            if (billNo != null && !"".equals(billNo)) {
                map.put("billNo", "%" + billNo + "%");
            } else {
                map.put("billNo", "");
            }
            if (grNo != null && !"".equals(grNo)) {
                map.put("grNo", "%" + grNo + "%");
            } else {
                map.put("grNo", "");
            }
            if (containerNo != null && !"".equals(containerNo)) {
                map.put("containerNo", "%" + containerNo + "%");
            } else {
                map.put("containerNo", "");
            }
            System.out.println("getClosedbill Value map" + map);
            closedBillList = (ArrayList) getSqlMapClientTemplate().queryForList("billing.viewECreditSuppInvoiceList", map);
            System.out.println("getbil details size is ::::" + closedBillList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return closedBillList;
    }

    public ArrayList viewClosedECreditInvoiceList(String custId, String invoiceId) {
        Map map = new HashMap();
        ArrayList viewClosedEInvoiceList = null;
        try {
            map.put("customerId", custId);
            map.put("invoiceId", invoiceId);
            System.out.println("getClosedbill Value map" + map);
            viewClosedEInvoiceList = (ArrayList) getSqlMapClientTemplate().queryForList("billing.viewClosedECreditInvoiceList", map);
            System.out.println("getbil details size is ::::" + viewClosedEInvoiceList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return viewClosedEInvoiceList;
    }

    public ArrayList viewClosedECreditSuppInvoiceList(String custId, String invoiceId) {
        Map map = new HashMap();
        ArrayList viewClosedEInvoiceList = null;
        try {
            map.put("customerId", custId);
            map.put("invoiceId", invoiceId);
            System.out.println("getClosedbill Value map" + map);
            viewClosedEInvoiceList = (ArrayList) getSqlMapClientTemplate().queryForList("billing.viewClosedECreditSuppInvoiceList", map);
            System.out.println("getbil details size is ::::" + viewClosedEInvoiceList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return viewClosedEInvoiceList;
    }

    public int approveSuppInvoice(BillingTO billingTO) {
        Map map = new HashMap();
        int status = 0;
        String[] invoiceIds = billingTO.getInvoiceIds();
        List invIds = new ArrayList(invoiceIds.length);
        for (int j = 0; j < invoiceIds.length; j++) {
            System.out.println("cn value:" + invoiceIds[j]);
            invIds.add(invoiceIds[j]);
        }
        map.put("invoiceIds", invIds);
        System.out.println("map for trip det:" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("billing.approveSuppInvoice", map);
            System.out.println("Approve Invoice =" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return status;
    }

    public String getInvoiceCodeSequenceBos2021(SqlMapClient session, int gstCheck) {
        Map map = new HashMap();
        int codeSequence = 0;
        String invoiceCode = "";
        int lastInvoiceCode = 0;
        try {
            lastInvoiceCode = (Integer) session.queryForObject("billing.getLastInvoiceCodeBos2021", map);
//            
//            //  int lastCodes = Integer.parseInt(lastInvoiceCode.substring(7));
//            //  codeSequence = lastCodes +1 ;
            map.put("invoiceCode", lastInvoiceCode + 1);
            codeSequence = (Integer) session.insert("billing.getInvoiceCodeSequenceBos2021", map);
            codeSequence = (Integer) session.queryForObject("billing.getLastInvoiceCodeBos2021", map);
            System.out.println("codeSequence-----" + codeSequence);
            invoiceCode = codeSequence + "";
            if (invoiceCode.length() == 1) {
                invoiceCode = "000000" + invoiceCode;
                System.out.println("invoice no 1.." + invoiceCode);
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "00000" + invoiceCode;
                System.out.println("invoice lenght 2.." + invoiceCode);
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 4) {
                invoiceCode = "000" + invoiceCode;
            } else if (invoiceCode.length() == 5) {
                invoiceCode = "00" + invoiceCode;
            } else if (invoiceCode.length() == 6) {
                invoiceCode = "0" + invoiceCode;
            }

            System.out.println("getInvoiceCodeSequence=" + invoiceCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return invoiceCode;
    }

    public String getInvoiceCodeSequenceBos2122(SqlMapClient session, int gstCheck) {
        Map map = new HashMap();
        int codeSequence = 0;
        String invoiceCode = "";
        int lastInvoiceCodeTmp = 0;
        int lastInvoiceCode = 0;
        try {
            lastInvoiceCodeTmp = (Integer) session.queryForObject("billing.getLastInvoiceCodeBos", map);
            System.out.println("lastInvoiceCodeTmp -------" + lastInvoiceCodeTmp);

            //  int lastCodes = Integer.parseInt(lastInvoiceCode.substring(7));
            //  codeSequence = lastCodes +1 ;
            lastInvoiceCode = lastInvoiceCodeTmp + 1;
            System.out.println("lastInvoiceCode + 1 -------" + lastInvoiceCode);
            map.put("invoiceCode", lastInvoiceCode);
            System.out.println("lastInvoiceCode + 1-----" + lastInvoiceCode);
            codeSequence = (Integer) session.insert("billing.getInvoiceCodeSequenceBos2122", map);
            System.out.println("codeSequence1-----" + codeSequence);
            codeSequence = (Integer) session.queryForObject("billing.getLastInvoiceCodeBos", map);
            System.out.println("codeSequence2-----" + codeSequence);
            invoiceCode = codeSequence + "";
            if (invoiceCode.length() == 1) {
                invoiceCode = "000000" + invoiceCode;
                System.out.println("invoice no 1.." + invoiceCode);
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "00000" + invoiceCode;
                System.out.println("invoice lenght 2.." + invoiceCode);
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 4) {
                invoiceCode = "000" + invoiceCode;
            } else if (invoiceCode.length() == 5) {
                invoiceCode = "00" + invoiceCode;
            } else if (invoiceCode.length() == 6) {
                invoiceCode = "0" + invoiceCode;
            }

            System.out.println("getInvoiceCodeSequence=" + invoiceCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return invoiceCode;
    }

//    public String getSuppInvoiceCodeSequenceBos2021(SqlMapClient session, int gstCheck) {
//        Map map = new HashMap();
//        int codeSequence = 0;
//        String invoiceCode = "";
//        try {
//            codeSequence = (Integer) session.insert("billing.getSuppInvoiceCodeSequenceBos2021", map);
//            invoiceCode = codeSequence + "";
//            if (invoiceCode.length() == 1) {
//                invoiceCode = "0000" + invoiceCode;
//                System.out.println("invoice no 1.." + invoiceCode);
//            } else if (invoiceCode.length() == 2) {
//                invoiceCode = "000" + invoiceCode;
//                System.out.println("invoice lenght 2.." + invoiceCode);
//            } else if (invoiceCode.length() == 3) {
//                invoiceCode = "00" + invoiceCode;
//                System.out.println("invoice lenght 3.." + invoiceCode);
//            } else if (invoiceCode.length() == 4) {
//                invoiceCode = "0" + invoiceCode;
//            }
//
//            System.out.println("getSuppInvoiceCodeSequence1920=" + invoiceCode);
//        } catch (Exception sqlException) {
//            sqlException.printStackTrace();
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
//        }
//        return invoiceCode;
//    }
    public String getSuppInvoiceCodeSequence2021(SqlMapClient session, int gstCheck) {
        Map map = new HashMap();
        int codeSequence = 0;
        String invoiceCode = "";
        int lastInvoiceCodeTmp = 0;
        int lastInvoiceCode = 0;
        try {
            lastInvoiceCodeTmp = (Integer) session.queryForObject("billing.getLastSuppInvoiceCode2021", map);
            System.out.println("lastInvoiceCodeTmp -------" + lastInvoiceCodeTmp);

            //  int lastCodes = Integer.parseInt(lastInvoiceCode.substring(7));
            //  codeSequence = lastCodes +1 ;
            lastInvoiceCode = lastInvoiceCodeTmp + 1;
            System.out.println("lastInvoiceCode + 1 -------" + lastInvoiceCode);
            map.put("invoiceCode", lastInvoiceCode);
            codeSequence = (Integer) session.insert("billing.getSuppInvoiceCodeSequenceGST2021", map);
            System.out.println("codeSequence1---" + codeSequence);
            codeSequence = (Integer) session.queryForObject("billing.getLastSuppInvoiceCode2021", map);
            System.out.println("codeSequence2-----" + codeSequence);
            invoiceCode = codeSequence + "";
            if (invoiceCode.length() == 1) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "000" + invoiceCode;
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "00" + invoiceCode;
            } else if (invoiceCode.length() == 1) {
                invoiceCode = "0" + invoiceCode;
            }

            System.out.println("getInvoiceCodeSequence=" + invoiceCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return invoiceCode;
    }

    public String getSuppInvoiceCodeSequence2022(SqlMapClient session, int gstCheck) {
        Map map = new HashMap();
        int codeSequence = 0;
        String invoiceCode = "";
        int lastInvoiceCodeTmp = 0;
        int lastInvoiceCode = 0;
        try {
            lastInvoiceCodeTmp = (Integer) session.queryForObject("billing.getLastSuppInvoiceCode2122", map);
            System.out.println("lastInvoiceCodeTmp -------" + lastInvoiceCodeTmp);

            //  int lastCodes = Integer.parseInt(lastInvoiceCode.substring(7));
            //  codeSequence = lastCodes +1 ;
            lastInvoiceCode = lastInvoiceCodeTmp + 1;
            System.out.println("lastInvoiceCode + 1 -------" + lastInvoiceCode);
            map.put("invoiceCode", lastInvoiceCode);
            codeSequence = (Integer) session.insert("billing.getSuppInvoiceCodeSequenceGST2122", map);
            System.out.println("codeSequence1---" + codeSequence);
            codeSequence = (Integer) session.queryForObject("billing.getLastSuppInvoiceCode2122", map);
            System.out.println("codeSequence2-----" + codeSequence);
            invoiceCode = codeSequence + "";
            if (invoiceCode.length() == 1) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "000" + invoiceCode;
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "00" + invoiceCode;
            } else if (invoiceCode.length() == 4) {
                invoiceCode = "0" + invoiceCode;
            }

            System.out.println("getInvoiceCodeSequence=" + invoiceCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return invoiceCode;
    }

    public String getSuppInvoiceCodeSequenceBos2021(SqlMapClient session, int gstCheck) {
        Map map = new HashMap();
        int codeSequence = 0;
        String invoiceCode = "";
        int lastInvoiceCodeTmp = 0;
        int lastInvoiceCode = 0;
        try {
            lastInvoiceCodeTmp = (Integer) session.queryForObject("billing.getLastSupplementInvoiceCodeBos2021", map);
            System.out.println("lastInvoiceCodeTmp -------" + lastInvoiceCodeTmp);

            //  int lastCodes = Integer.parseInt(lastInvoiceCode.substring(7));
            //  codeSequence = lastCodes +1 ;
            lastInvoiceCode = lastInvoiceCodeTmp + 1;
            System.out.println("lastInvoiceCode + 1 -------" + lastInvoiceCode);
            map.put("invoiceCode", lastInvoiceCode);
            System.out.println("lastInvoiceCode + 1-----" + lastInvoiceCode);
            codeSequence = (Integer) session.insert("billing.getSuppInvoiceCodeSequenceBos2021", map);
            System.out.println("codeSequence1-----" + codeSequence);
            codeSequence = (Integer) session.queryForObject("billing.getLastSupplementInvoiceCodeBos2021", map);
            System.out.println("codeSequence2-----" + codeSequence);
            invoiceCode = codeSequence + "";
            if (invoiceCode.length() == 1) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "000" + invoiceCode;
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "00" + invoiceCode;
            } else if (invoiceCode.length() == 1) {
                invoiceCode = "0" + invoiceCode;
            }

            System.out.println("getSuppInvoiceCodeSequence1920=" + invoiceCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return invoiceCode;
    }

    public String getSuppInvoiceCodeSequenceBos2022(SqlMapClient session, int gstCheck) {
        Map map = new HashMap();
        int codeSequence = 0;
        String invoiceCode = "";
        int lastInvoiceCodeTmp = 0;
        int lastInvoiceCode = 0;
        try {
            lastInvoiceCodeTmp = (Integer) session.queryForObject("billing.getLastSupplementInvoiceCodeBos2122", map);
            System.out.println("lastInvoiceCodeTmp -------" + lastInvoiceCodeTmp);

            //  int lastCodes = Integer.parseInt(lastInvoiceCode.substring(7));
            //  codeSequence = lastCodes +1 ;
            lastInvoiceCode = lastInvoiceCodeTmp + 1;
            System.out.println("lastInvoiceCode + 1 -------" + lastInvoiceCode);
            map.put("invoiceCode", lastInvoiceCode);
            System.out.println("lastInvoiceCode + 1-----" + lastInvoiceCode);
            codeSequence = (Integer) session.insert("billing.getSuppInvoiceCodeSequenceBos2122", map);
            System.out.println("codeSequence1-----" + codeSequence);
            codeSequence = (Integer) session.queryForObject("billing.getLastSupplementInvoiceCodeBos2122", map);
            System.out.println("codeSequence2-----" + codeSequence);
            invoiceCode = codeSequence + "";
            if (invoiceCode.length() == 1) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "000" + invoiceCode;
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "00" + invoiceCode;
            } else if (invoiceCode.length() == 4) {
                invoiceCode = "0" + invoiceCode;
            }

            System.out.println("getSuppInvoiceCodeSequence1920=" + invoiceCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return invoiceCode;
    }

    public String getInvoiceCodeSequenceBos2223(SqlMapClient session, int gstCheck) {
        Map map = new HashMap();
        int codeSequence = 0;
        String invoiceCode = "";
        int lastInvoiceCodeTmp = 0;
        int lastInvoiceCode = 0;
        try {
            lastInvoiceCodeTmp = (Integer) session.queryForObject("billing.getLastInvoiceCodeBos2223", map);
            System.out.println("lastInvoiceCodeTmp -------" + lastInvoiceCodeTmp);

            //  int lastCodes = Integer.parseInt(lastInvoiceCode.substring(7));
            //  codeSequence = lastCodes +1 ;
            lastInvoiceCode = lastInvoiceCodeTmp + 1;
            System.out.println("lastInvoiceCode + 1 -------" + lastInvoiceCode);
            map.put("invoiceCode", lastInvoiceCode);
            System.out.println("lastInvoiceCode + 1-----" + lastInvoiceCode);
            codeSequence = (Integer) session.insert("billing.insertInvoiceCodeSequenceBos2223", map);
            System.out.println("codeSequence1-----" + codeSequence);
            codeSequence = (Integer) session.queryForObject("billing.getLastInvoiceCodeBos2223", map);
            System.out.println("codeSequence2-----" + codeSequence);
            invoiceCode = codeSequence + "";
            if (invoiceCode.length() == 1) {
                invoiceCode = "000000" + invoiceCode;
                System.out.println("invoice no 1.." + invoiceCode);
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "00000" + invoiceCode;
                System.out.println("invoice lenght 2.." + invoiceCode);
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 4) {
                invoiceCode = "000" + invoiceCode;
            } else if (invoiceCode.length() == 5) {
                invoiceCode = "00" + invoiceCode;
            } else if (invoiceCode.length() == 6) {
                invoiceCode = "0" + invoiceCode;
            }

            System.out.println("getInvoiceCodeSequence=" + invoiceCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return invoiceCode;
    }

    public String getInvoiceCodeSequenceBos2324(SqlMapClient session, int gstCheck) {
        Map map = new HashMap();
        int codeSequence = 0;
        String invoiceCode = "";
        int lastInvoiceCodeTmp = 0;
        int lastInvoiceCode = 0;
        try {
            lastInvoiceCodeTmp = (Integer) session.queryForObject("billing.getLastInvoiceCodeBos2324", map);
            System.out.println("lastInvoiceCodeTmp -------" + lastInvoiceCodeTmp);

            //  int lastCodes = Integer.parseInt(lastInvoiceCode.substring(7));
            //  codeSequence = lastCodes +1 ;
            lastInvoiceCode = lastInvoiceCodeTmp + 1;
            System.out.println("lastInvoiceCode + 1 -------" + lastInvoiceCode);
            map.put("invoiceCode", lastInvoiceCode);
            System.out.println("lastInvoiceCode + 1-----" + lastInvoiceCode);
            codeSequence = (Integer) session.insert("billing.insertInvoiceCodeSequenceBos2324", map);
            System.out.println("codeSequence1-----" + codeSequence);
            codeSequence = (Integer) session.queryForObject("billing.getLastInvoiceCodeBos2324", map);
            System.out.println("codeSequence2-----" + codeSequence);
            invoiceCode = codeSequence + "";
            if (invoiceCode.length() == 1) {
                invoiceCode = "000000" + invoiceCode;
                System.out.println("invoice no 1.." + invoiceCode);
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "00000" + invoiceCode;
                System.out.println("invoice lenght 2.." + invoiceCode);
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 4) {
                invoiceCode = "000" + invoiceCode;
            } else if (invoiceCode.length() == 5) {
                invoiceCode = "00" + invoiceCode;
            } else if (invoiceCode.length() == 6) {
                invoiceCode = "0" + invoiceCode;
            }

            System.out.println("getInvoiceCodeSequence=" + invoiceCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return invoiceCode;
    }

    public String getInvoiceCodeSequence2223(SqlMapClient session, int gstCheck) {
        Map map = new HashMap();
        int codeSequence = 0;
        String invoiceCode = "";
        int lastInvoiceCodeTmp = 0;
        int lastInvoiceCode = 0;
        try {
            lastInvoiceCodeTmp = (Integer) session.queryForObject("billing.getLastInvoiceCode2223", map);
            System.out.println("lastInvoiceCodeTmp -------" + lastInvoiceCodeTmp);

            //  int lastCodes = Integer.parseInt(lastInvoiceCode.substring(7));
            //  codeSequence = lastCodes +1 ;
            lastInvoiceCode = lastInvoiceCodeTmp + 1;
            System.out.println("lastInvoiceCode + 1 -------" + lastInvoiceCode);
            map.put("invoiceCode", lastInvoiceCode);
            codeSequence = (Integer) session.insert("billing.insertInvoiceCodeSequenceGST2122", map);
            System.out.println("codeSequence1---" + codeSequence);
            codeSequence = (Integer) session.queryForObject("billing.getLastInvoiceCode2223", map);
            System.out.println("codeSequence2-----" + codeSequence);
            invoiceCode = codeSequence + "";
            if (invoiceCode.length() == 1) {
                invoiceCode = "000000" + invoiceCode;
                System.out.println("invoice no 1.." + invoiceCode);
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "00000" + invoiceCode;
                System.out.println("invoice lenght 2.." + invoiceCode);
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 4) {
                invoiceCode = "000" + invoiceCode;
            } else if (invoiceCode.length() == 5) {
                invoiceCode = "00" + invoiceCode;
            } else if (invoiceCode.length() == 6) {
                invoiceCode = "0" + invoiceCode;
            }

            System.out.println("getInvoiceCodeSequence=" + invoiceCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return invoiceCode;
    }

    public String getInvoiceCodeSequence2324(SqlMapClient session, int gstCheck) {
        Map map = new HashMap();
        int codeSequence = 0;
        String invoiceCode = "";
        int lastInvoiceCodeTmp = 0;
        int lastInvoiceCode = 0;
        try {
            lastInvoiceCodeTmp = (Integer) session.queryForObject("billing.getLastInvoiceCode2324", map);
            System.out.println("lastInvoiceCodeTmp -------" + lastInvoiceCodeTmp);

            //  int lastCodes = Integer.parseInt(lastInvoiceCode.substring(7));
            //  codeSequence = lastCodes +1 ;
            lastInvoiceCode = lastInvoiceCodeTmp + 1;
            System.out.println("lastInvoiceCode + 1 -------" + lastInvoiceCode);
            map.put("invoiceCode", lastInvoiceCode);
            codeSequence = (Integer) session.insert("billing.insertInvoiceCodeSequenceGST2324", map);
            System.out.println("codeSequence1---" + codeSequence);
            codeSequence = (Integer) session.queryForObject("billing.getLastInvoiceCode2324", map);
            System.out.println("codeSequence2-----" + codeSequence);
            invoiceCode = codeSequence + "";
            if (invoiceCode.length() == 1) {
                invoiceCode = "000000" + invoiceCode;
                System.out.println("invoice no 1.." + invoiceCode);
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "00000" + invoiceCode;
                System.out.println("invoice lenght 2.." + invoiceCode);
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 4) {
                invoiceCode = "000" + invoiceCode;
            } else if (invoiceCode.length() == 5) {
                invoiceCode = "00" + invoiceCode;
            } else if (invoiceCode.length() == 6) {
                invoiceCode = "0" + invoiceCode;
            }

            System.out.println("getInvoiceCodeSequence=" + invoiceCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return invoiceCode;
    }

    public String getSuppInvoiceCodeSequenceBos2223(SqlMapClient session, int gstCheck) {
        Map map = new HashMap();
        int codeSequence = 0;
        String invoiceCode = "";
        int lastInvoiceCodeTmp = 0;
        int lastInvoiceCode = 0;
        try {
            lastInvoiceCodeTmp = (Integer) session.queryForObject("billing.getLastSupplementInvoiceCodeBos2223", map);
            System.out.println("lastInvoiceCodeTmp -------" + lastInvoiceCodeTmp);

            //  int lastCodes = Integer.parseInt(lastInvoiceCode.substring(7));
            //  codeSequence = lastCodes +1 ;
            lastInvoiceCode = lastInvoiceCodeTmp + 1;
            System.out.println("lastInvoiceCode + 1 -------" + lastInvoiceCode);
            map.put("invoiceCode", lastInvoiceCode);
            System.out.println("lastInvoiceCode + 1-----" + lastInvoiceCode);
            codeSequence = (Integer) session.insert("billing.getSuppInvoiceCodeSequenceBos2223", map);
            System.out.println("codeSequence1-----" + codeSequence);
            codeSequence = (Integer) session.queryForObject("billing.getLastSupplementInvoiceCodeBos2223", map);
            System.out.println("codeSequence2-----" + codeSequence);
            invoiceCode = codeSequence + "";
            if (invoiceCode.length() == 1) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "000" + invoiceCode;
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "00" + invoiceCode;
            } else if (invoiceCode.length() == 4) {
                invoiceCode = "0" + invoiceCode;
            }

            System.out.println("getSuppInvoiceCodeSequence1920=" + invoiceCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return invoiceCode;
    }

    public String getSuppInvoiceCodeSequenceBos2324(SqlMapClient session, int gstCheck) {
        Map map = new HashMap();
        int codeSequence = 0;
        String invoiceCode = "";
        int lastInvoiceCodeTmp = 0;
        int lastInvoiceCode = 0;
        try {
            lastInvoiceCodeTmp = (Integer) session.queryForObject("billing.getLastSupplementInvoiceCodeBos2324", map);
            System.out.println("lastInvoiceCodeTmp -------" + lastInvoiceCodeTmp);

            //  int lastCodes = Integer.parseInt(lastInvoiceCode.substring(7));
            //  codeSequence = lastCodes +1 ;
            lastInvoiceCode = lastInvoiceCodeTmp + 1;
            System.out.println("lastInvoiceCode + 1 -------" + lastInvoiceCode);
            map.put("invoiceCode", lastInvoiceCode);
            System.out.println("lastInvoiceCode + 1-----" + lastInvoiceCode);
            codeSequence = (Integer) session.insert("billing.getSuppInvoiceCodeSequenceBos2324", map);
            System.out.println("codeSequence1-----" + codeSequence);
            codeSequence = (Integer) session.queryForObject("billing.getLastSupplementInvoiceCodeBos2324", map);
            System.out.println("codeSequence2-----" + codeSequence);
            invoiceCode = codeSequence + "";
            if (invoiceCode.length() == 1) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "000" + invoiceCode;
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "00" + invoiceCode;
            } else if (invoiceCode.length() == 4) {
                invoiceCode = "0" + invoiceCode;
            }

            System.out.println("getSuppInvoiceCodeSequence1920=" + invoiceCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return invoiceCode;
    }

    public String getSuppInvoiceCodeSequence2223(SqlMapClient session, int gstCheck) {
        Map map = new HashMap();
        int codeSequence = 0;
        String invoiceCode = "";
        int lastInvoiceCodeTmp = 0;
        int lastInvoiceCode = 0;
        try {
            lastInvoiceCodeTmp = (Integer) session.queryForObject("billing.getLastSuppInvoiceCode2223", map);
            System.out.println("lastInvoiceCodeTmp -------" + lastInvoiceCodeTmp);

            //  int lastCodes = Integer.parseInt(lastInvoiceCode.substring(7));
            //  codeSequence = lastCodes +1 ;
            lastInvoiceCode = lastInvoiceCodeTmp + 1;
            System.out.println("lastInvoiceCode + 1 -------" + lastInvoiceCode);
            map.put("invoiceCode", lastInvoiceCode);
            codeSequence = (Integer) session.insert("billing.getSuppInvoiceCodeSequenceGST2223", map);
            System.out.println("codeSequence1---" + codeSequence);
            codeSequence = (Integer) session.queryForObject("billing.getLastSuppInvoiceCode2223", map);
            System.out.println("codeSequence2-----" + codeSequence);
            invoiceCode = codeSequence + "";
            if (invoiceCode.length() == 1) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "000" + invoiceCode;
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "00" + invoiceCode;
            } else if (invoiceCode.length() == 4) {
                invoiceCode = "0" + invoiceCode;
            }

            System.out.println("getInvoiceCodeSequence=" + invoiceCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return invoiceCode;
    }

    public String getSuppInvoiceCodeSequence2324(SqlMapClient session, int gstCheck) {
        Map map = new HashMap();
        int codeSequence = 0;
        String invoiceCode = "";
        int lastInvoiceCodeTmp = 0;
        int lastInvoiceCode = 0;
        try {
            lastInvoiceCodeTmp = (Integer) session.queryForObject("billing.getLastSuppInvoiceCode2324", map);
            System.out.println("lastInvoiceCodeTmp -------" + lastInvoiceCodeTmp);

            //  int lastCodes = Integer.parseInt(lastInvoiceCode.substring(7));
            //  codeSequence = lastCodes +1 ;
            lastInvoiceCode = lastInvoiceCodeTmp + 1;
            System.out.println("lastInvoiceCode + 1 -------" + lastInvoiceCode);
            map.put("invoiceCode", lastInvoiceCode);
            codeSequence = (Integer) session.insert("billing.getSuppInvoiceCodeSequenceGST2324", map);
            System.out.println("codeSequence1---" + codeSequence);
            codeSequence = (Integer) session.queryForObject("billing.getLastSuppInvoiceCode2324", map);
            System.out.println("codeSequence2-----" + codeSequence);
            invoiceCode = codeSequence + "";
            if (invoiceCode.length() == 1) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "000" + invoiceCode;
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "00" + invoiceCode;
            } else if (invoiceCode.length() == 4) {
                invoiceCode = "0" + invoiceCode;
            }

            System.out.println("getInvoiceCodeSequence=" + invoiceCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return invoiceCode;
    }

    public int resendInvoice(BillingTO billingTO) {
        Map map = new HashMap();
        int status = 0;
        String[] invoiceIds = billingTO.getInvoiceIds();
        List invIds = new ArrayList(invoiceIds.length);
        for (int j = 0; j < invoiceIds.length; j++) {
            System.out.println("cn value:" + invoiceIds[j]);
            invIds.add(invoiceIds[j]);
        }
        map.put("invoiceIds", invIds);
        System.out.println("map for trip det:" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("billing.resendInvoice", map);
            System.out.println("Approve Invoice =" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return status;
    }

    public int eInvoiceGenerateAuto(BillingTO billingTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        int headerStatus = 0;
        int detailStatus = 0;
        String invoiceIds = billingTO.getInvoiceId();
        ArrayList getSellerBuyerDetails = new ArrayList();
        ArrayList getEinvoiceDetails = new ArrayList();
        try {
//            List invIds = new ArrayList(invoiceIds.length);
//            System.out.println("invoiceIds.length;----" + invoiceIds.length);
//            for (int j = 0; j < invoiceIds.length; j++) {
//                System.out.println("cn#### value:" + invoiceIds[j]);
            map.put("invoiceId", billingTO.getInvoiceId());
            System.out.println("map for trip det:" + map);
            getSellerBuyerDetails = (ArrayList) session.queryForList("billing.getSellerBuyerDetails", map);
            System.out.println("getSellerBuyerDetails.size()---" + getSellerBuyerDetails.size());
            Iterator itr = getSellerBuyerDetails.iterator();
            BillingTO billTO = new BillingTO();
            while (itr.hasNext()) {
                billTO = (BillingTO) itr.next();
                map.put("invoiceId", billTO.getInvoiceId());
                map.put("invoiceCode", billTO.getInvoiceCode());
                map.put("invoiceDate", billTO.getInvoiceDate());
                map.put("invType", billTO.getInvType());
                map.put("billingCustId", billTO.getBillingCustId());
                map.put("billingCustName", billTO.getBillingCustName());
                map.put("billingAddress", billTO.getBillingAddress());
                map.put("billingPanNo", billTO.getBillingPanNo());
                map.put("billingPincode", billTO.getBillingPincode());
                map.put("billingPos", billTO.getBillingPos());
                map.put("billingLocation", billTO.getBillingLocation());
                map.put("billingGst", billTO.getBillingGstin());
                map.put("billingState", billTO.getBillingState());
                map.put("grandTotal", billTO.getGrandTotal());
                double cgstPercent = 0.0;
                double sgstPercent = 0.0;
                double igstPercent = 0.0;
                double cgst = 0.0;
                double sgst = 0.0;
                double igst = 0.0;
                double netAmount = 0.0;
                if ("6".equals(billTO.getBillingState())) {
                    cgstPercent = 0.0;
                    sgstPercent = 0.0;
                    igstPercent = 0.0;
                    cgst = (Double.parseDouble(billTO.getGrandTotal()) * cgstPercent) / 100;
                    sgst = (Double.parseDouble(billTO.getGrandTotal()) * sgstPercent) / 100;
                    igst = (Double.parseDouble(billTO.getGrandTotal()) * igstPercent) / 100;
                    netAmount = (Double.parseDouble(billTO.getGrandTotal())) + igst + cgst + sgst;
                    map.put("cgstPercent", cgstPercent);
                    map.put("sgstPercent", sgstPercent);
                    map.put("igstPercent", igstPercent);
                    map.put("cgst", cgst);
                    map.put("sgst", sgst);
                    map.put("igst", igst);
                } else {
                    cgstPercent = 0.0;
                    sgstPercent = 0.0;
                    igstPercent = 0.0;
                    cgst = (Double.parseDouble(billTO.getGrandTotal()) * cgstPercent) / 100;
                    sgst = (Double.parseDouble(billTO.getGrandTotal()) * sgstPercent) / 100;
                    igst = (Double.parseDouble(billTO.getGrandTotal()) * igstPercent) / 100;

                    netAmount = (Double.parseDouble(billTO.getGrandTotal())) + igst + cgst + sgst;
                    map.put("cgstPercent", cgstPercent);
                    map.put("sgstPercent", sgstPercent);
                    map.put("igstPercent", igstPercent);
                    map.put("cgst", cgst);
                    map.put("sgst", sgst);
                    map.put("igst", igst);
                }

                map.put("netAmount", netAmount);
                map.put("version", "1.03");
                map.put("igstOnIntra", "N");
                map.put("regRev", "Y");
                map.put("taxSch", "GST");
                map.put("supTyp", "B2B");

                String sellerDetailsTmp = billTO.getSellerDetails();
                String sellerDetails[] = sellerDetailsTmp.split("~");
                map.put("compName", sellerDetails[0]);
                map.put("compAddress", sellerDetails[1]);
                map.put("compLocation", sellerDetails[2]);
                map.put("compState", sellerDetails[3]);
                map.put("compPincode", sellerDetails[4]);
                map.put("compGst", sellerDetails[5]);

                System.out.println("MAP--insertEinvoiceHeader-----" + map);

                headerStatus = (Integer) session.insert("billing.insertEinvoiceHeader", map);
                System.out.println("insertEinvoiceHeader---" + headerStatus);
            }
            map.put("eInvoiceId", headerStatus);

            System.out.println("MAP HEADER---" + map);
            map.put("invoiceId", billingTO.getInvoiceId());

            getEinvoiceDetails = (ArrayList) session.queryForList("billing.getEinvoiceDetails", map);
            System.out.println("getEinvoiceDetails.size()---" + getEinvoiceDetails.size());
            Iterator itr1 = getEinvoiceDetails.iterator();
            BillingTO billTO1 = new BillingTO();
            int sno = 0;
            while (itr1.hasNext()) {
                billTO1 = (BillingTO) itr1.next();
                sno = sno + 1;
                map.put("sno", sno);
                map.put("isServc", billTO1.getIsServc());
                map.put("prdDesc", billTO1.getPrdDesc());
                System.out.println("billTO1.getHsnCode()-----" + billTO1.getHsnCode());
                map.put("hsnCode", billTO1.getHsnCode());
                map.put("qty", billTO1.getQty());
                map.put("unit", billTO1.getUnit());
                map.put("unitPrice", billTO1.getUnitPrice());
                map.put("totAmount", billTO1.getTotAmount());
                map.put("assAmt", billTO1.getAssAmt());
                double cgstPercent = 0.0;
                double sgstPercent = 0.0;
                double igstPercent = 0.0;
                double cgst = 0.0;
                double sgst = 0.0;
                double igst = 0.0;
                double totalValue = 0.0;
                if ("6".equals(billTO1.getBillingState())) {
                    cgstPercent = 0.0;
                    sgstPercent = 0.0;
                    igstPercent = 0.0;
                    cgst = (Double.parseDouble(billTO1.getUnitPrice()) * cgstPercent) / 100;
                    sgst = (Double.parseDouble(billTO1.getUnitPrice()) * sgstPercent) / 100;
                    igst = (Double.parseDouble(billTO1.getUnitPrice()) * igstPercent) / 100;
                    totalValue = (Double.parseDouble(billTO1.getUnitPrice())) + igst + cgst + sgst;
                    map.put("cgstPercent", cgstPercent);
                    map.put("sgstPercent", sgstPercent);
                    map.put("igstPercent", igstPercent);
                    map.put("cgst", cgst);
                    map.put("sgst", sgst);
                    map.put("igst", igst);
                } else {
                    cgstPercent = 0.0;
                    sgstPercent = 0.0;
                    igstPercent = 0.0;
                    cgst = (Double.parseDouble(billTO1.getUnitPrice()) * cgstPercent) / 100;
                    sgst = (Double.parseDouble(billTO1.getUnitPrice()) * sgstPercent) / 100;
                    igst = (Double.parseDouble(billTO1.getUnitPrice()) * igstPercent) / 100;

                    totalValue = (Double.parseDouble(billTO1.getUnitPrice())) + igst + cgst + sgst;
                    map.put("cgstPercent", cgstPercent);
                    map.put("sgstPercent", sgstPercent);
                    map.put("igstPercent", igstPercent);
                    map.put("cgst", cgst);
                    map.put("sgst", sgst);
                    map.put("igst", igst);
                }

                map.put("totalValue", totalValue);
                map.put("expenseDesc", billTO1.getExpenseDesc());
                map.put("tripId", billTO1.getTripId());
                map.put("expenseId", billTO1.getExpenseId());
                map.put("userId", userId);

                System.out.println("MAP--insertEInvoiceDetails-----" + map);
                detailStatus = (Integer) session.update("billing.insertEInvoiceDetails", map);
                System.out.println("eInvoiceDetailGenerate----" + detailStatus);
            }

            System.out.println(" Invoice =" + status);
            map.put("custId", billingTO.getCustomerId());
            if (headerStatus > 0 && detailStatus > 0) {
                status = (Integer) session.update("billing.updateInvoiceHeaderPostStatus", map);
                System.out.println("updateCustomerPostStatus =" + status);

            }
//            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return status;
    }

    public int eSuppInvoiceGenerateAuto(BillingTO billingTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        int headerStatus = 0;
        int detailStatus = 0;
        String invoiceIds = billingTO.getInvoiceId();
        ArrayList getSellerBuyerDetails = new ArrayList();
        ArrayList getEinvoiceDetails = new ArrayList();
        try {
//            List invIds = new ArrayList(invoiceIds.length);
//            for (int j = 0; j < invoiceIds.length; j++) {
//                System.out.println("cn value:" + invoiceIds[j]);
            map.put("invoiceId", billingTO.getInvoiceId());
            System.out.println("map for trip det:" + map);
            getSellerBuyerDetails = (ArrayList) session.queryForList("billing.getSuppSellerBuyerDetails", map);
            System.out.println("getSellerBuyerDetails.size()---" + getSellerBuyerDetails.size());
            Iterator itr = getSellerBuyerDetails.iterator();
            BillingTO billTO = new BillingTO();
            while (itr.hasNext()) {
                billTO = (BillingTO) itr.next();
                map.put("invoiceId", billTO.getInvoiceId());
                map.put("invoiceCode", billTO.getInvoiceCode());
                map.put("invoiceDate", billTO.getInvoiceDate());
                map.put("invType", billTO.getInvType());
                map.put("billingCustId", billTO.getBillingCustId());
                map.put("billingCustName", billTO.getBillingCustName());
                map.put("billingAddress", billTO.getBillingAddress());
                map.put("billingPanNo", billTO.getBillingPanNo());
                map.put("billingPincode", billTO.getBillingPincode());
                map.put("billingPos", billTO.getBillingPos());
                map.put("billingLocation", billTO.getBillingLocation());
                map.put("billingGst", billTO.getBillingGstin());
                map.put("billingState", billTO.getBillingState());
                map.put("grandTotal", billTO.getGrandTotal());
                double cgstPercent = 0.0;
                double sgstPercent = 0.0;
                double igstPercent = 0.0;
                double cgst = 0.0;
                double sgst = 0.0;
                double igst = 0.0;
                double netAmount = 0.0;
                if ("6".equals(billTO.getBillingState())) {
                    cgstPercent = 0.0;
                    sgstPercent = 0.0;
                    igstPercent = 0.0;
                    cgst = (Double.parseDouble(billTO.getGrandTotal()) * cgstPercent) / 100;
                    sgst = (Double.parseDouble(billTO.getGrandTotal()) * sgstPercent) / 100;
                    igst = (Double.parseDouble(billTO.getGrandTotal()) * igstPercent) / 100;
                    netAmount = (Double.parseDouble(billTO.getGrandTotal())) + igst + cgst + sgst;
                    map.put("cgstPercent", cgstPercent);
                    map.put("sgstPercent", sgstPercent);
                    map.put("igstPercent", igstPercent);
                    map.put("cgst", cgst);
                    map.put("sgst", sgst);
                    map.put("igst", igst);
                } else {
                    cgstPercent = 0.0;
                    sgstPercent = 0.0;
                    igstPercent = 0.0;
                    cgst = (Double.parseDouble(billTO.getGrandTotal()) * cgstPercent) / 100;
                    sgst = (Double.parseDouble(billTO.getGrandTotal()) * sgstPercent) / 100;
                    igst = (Double.parseDouble(billTO.getGrandTotal()) * igstPercent) / 100;

                    netAmount = (Double.parseDouble(billTO.getGrandTotal())) + igst + cgst + sgst;
                    map.put("cgstPercent", cgstPercent);
                    map.put("sgstPercent", sgstPercent);
                    map.put("igstPercent", igstPercent);
                    map.put("cgst", cgst);
                    map.put("sgst", sgst);
                    map.put("igst", igst);
                }

                map.put("netAmount", netAmount);
                map.put("version", "1.03");
                map.put("igstOnIntra", "N");
                map.put("regRev", "Y");
                map.put("taxSch", "GST");
                map.put("supTyp", "B2B");

                String sellerDetailsTmp = billTO.getSellerDetails();
                String sellerDetails[] = sellerDetailsTmp.split("~");
                map.put("compName", sellerDetails[0]);
                map.put("compAddress", sellerDetails[1]);
                map.put("compLocation", sellerDetails[2]);
                map.put("compState", sellerDetails[3]);
                map.put("compPincode", sellerDetails[4]);
                map.put("compGst", sellerDetails[5]);

                System.out.println("MAP--insertEinvoiceHeader-----" + map);

                headerStatus = (Integer) session.insert("billing.insertESuppInvoiceHeader", map);
                System.out.println("insertEinvoiceHeader---" + headerStatus);
            }
            map.put("eInvoiceId", headerStatus);

            System.out.println("MAP HEADER---" + map);
            map.put("invoiceId", billingTO.getInvoiceId());

            getEinvoiceDetails = (ArrayList) session.queryForList("billing.getESuppInvoiceDetails", map);
            System.out.println("getEinvoiceDetails.size()---" + getEinvoiceDetails.size());
            Iterator itr1 = getEinvoiceDetails.iterator();
            BillingTO billTO1 = new BillingTO();
            int sno = 0;
            while (itr1.hasNext()) {
                billTO1 = (BillingTO) itr1.next();
                sno = sno + 1;
                map.put("sno", sno);
                map.put("isServc", billTO1.getIsServc());
                map.put("prdDesc", billTO1.getPrdDesc());
                System.out.println("billTO1.getHsnCode()-----" + billTO1.getHsnCode());
                map.put("hsnCode", billTO1.getHsnCode());
                map.put("qty", billTO1.getQty());
                map.put("unit", billTO1.getUnit());
                map.put("unitPrice", billTO1.getUnitPrice());
                map.put("totAmount", billTO1.getTotAmount());
                map.put("assAmt", billTO1.getAssAmt());
                double cgstPercent = 0.0;
                double sgstPercent = 0.0;
                double igstPercent = 0.0;
                double cgst = 0.0;
                double sgst = 0.0;
                double igst = 0.0;
                double totalValue = 0.0;
                if ("6".equals(billTO1.getBillingState())) {
                    cgstPercent = 0.0;
                    sgstPercent = 0.0;
                    igstPercent = 0.0;
                    cgst = (Double.parseDouble(billTO1.getUnitPrice()) * cgstPercent) / 100;
                    sgst = (Double.parseDouble(billTO1.getUnitPrice()) * sgstPercent) / 100;
                    igst = (Double.parseDouble(billTO1.getUnitPrice()) * igstPercent) / 100;
                    totalValue = (Double.parseDouble(billTO1.getUnitPrice())) + igst + cgst + sgst;
                    map.put("cgstPercent", cgstPercent);
                    map.put("sgstPercent", sgstPercent);
                    map.put("igstPercent", igstPercent);
                    map.put("cgst", cgst);
                    map.put("sgst", sgst);
                    map.put("igst", igst);
                } else {
                    cgstPercent = 0.0;
                    sgstPercent = 0.0;
                    igstPercent = 0.0;
                    cgst = (Double.parseDouble(billTO1.getUnitPrice()) * cgstPercent) / 100;
                    sgst = (Double.parseDouble(billTO1.getUnitPrice()) * sgstPercent) / 100;
                    igst = (Double.parseDouble(billTO1.getUnitPrice()) * igstPercent) / 100;

                    totalValue = (Double.parseDouble(billTO1.getUnitPrice())) + igst + cgst + sgst;
                    map.put("cgstPercent", cgstPercent);
                    map.put("sgstPercent", sgstPercent);
                    map.put("igstPercent", igstPercent);
                    map.put("cgst", cgst);
                    map.put("sgst", sgst);
                    map.put("igst", igst);
                }

                map.put("totalValue", totalValue);
                map.put("expenseDesc", billTO1.getExpenseDesc());
                map.put("tripId", billTO1.getTripId());
                map.put("expenseId", billTO1.getExpenseId());
                map.put("userId", userId);

                System.out.println("MAP--insertEInvoiceDetails-----" + map);
                detailStatus = (Integer) session.update("billing.insertESuppInvoiceDetails", map);
                System.out.println("eInvoiceDetailGenerate----" + detailStatus);
            }
            System.out.println(" Invoice =" + status);
            map.put("custId", billingTO.getCustomerId());
            if (headerStatus > 0 && detailStatus > 0) {
                status = (Integer) session.update("billing.updateSuppInvoiceHeaderPostStatus", map);
                System.out.println("updateCustomerPostStatus =" + status);

            }
//            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return status;
    }

    public int eCreditInvoiceGenerateAuto(BillingTO billingTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        int headerStatus = 0;
        int detailStatus = 0;
        String invoiceIds = billingTO.getInvoiceId();
        ArrayList getSellerBuyerDetails = new ArrayList();
        ArrayList getEinvoiceDetails = new ArrayList();
        try {
//            List invIds = new ArrayList(invoiceIds.length);
//            for (int j = 0; j < invoiceIds.length; j++) {
//                System.out.println("cn value:" + invoiceIds[j]);
            map.put("invoiceId", billingTO.getInvoiceId());
            System.out.println("map for trip det:" + map);
            getSellerBuyerDetails = (ArrayList) session.queryForList("billing.getCreditSellerBuyerDetails", map);
            System.out.println("getSellerBuyerDetails.size()---" + getSellerBuyerDetails.size());
            Iterator itr = getSellerBuyerDetails.iterator();
            BillingTO billTO = new BillingTO();
            while (itr.hasNext()) {
                billTO = (BillingTO) itr.next();
                map.put("invoiceId", billTO.getInvoiceId());
                map.put("invoiceCode", billTO.getInvoiceCode());
                map.put("invoiceDate", billTO.getInvoiceDate());
                map.put("refInvoicecode", billTO.getRefInvoicecode());
                map.put("refInvoiceDate", billTO.getRefInvoiceDate());
                map.put("invType", billTO.getInvType());
                map.put("billingCustId", billTO.getBillingCustId());
                map.put("billingCustName", billTO.getBillingCustName());
                map.put("billingAddress", billTO.getBillingAddress());
                map.put("billingPanNo", billTO.getBillingPanNo());
                map.put("billingPincode", billTO.getBillingPincode());
                map.put("billingPos", billTO.getBillingPos());
                map.put("billingLocation", billTO.getBillingLocation());
                map.put("billingGst", billTO.getBillingGstin());
                map.put("billingState", billTO.getBillingState());
                map.put("grandTotal", billTO.getGrandTotal());
                double cgstPercent = 0.0;
                double sgstPercent = 0.0;
                double igstPercent = 0.0;
                double cgst = 0.0;
                double sgst = 0.0;
                double igst = 0.0;
                double netAmount = 0.0;
                if ("6".equals(billTO.getBillingState())) {
                    cgstPercent = 0.0;
                    sgstPercent = 0.0;
                    igstPercent = 0.0;
                    cgst = (Double.parseDouble(billTO.getGrandTotal()) * cgstPercent) / 100;
                    sgst = (Double.parseDouble(billTO.getGrandTotal()) * sgstPercent) / 100;
                    igst = (Double.parseDouble(billTO.getGrandTotal()) * igstPercent) / 100;
                    netAmount = (Double.parseDouble(billTO.getGrandTotal())) + igst + cgst + sgst;
                    map.put("cgstPercent", cgstPercent);
                    map.put("sgstPercent", sgstPercent);
                    map.put("igstPercent", igstPercent);
                    map.put("cgst", cgst);
                    map.put("sgst", sgst);
                    map.put("igst", igst);
                } else {
                    cgstPercent = 0.0;
                    sgstPercent = 0.0;
                    igstPercent = 0.0;
                    cgst = (Double.parseDouble(billTO.getGrandTotal()) * cgstPercent) / 100;
                    sgst = (Double.parseDouble(billTO.getGrandTotal()) * sgstPercent) / 100;
                    igst = (Double.parseDouble(billTO.getGrandTotal()) * igstPercent) / 100;

                    netAmount = (Double.parseDouble(billTO.getGrandTotal())) + igst + cgst + sgst;
                    map.put("cgstPercent", cgstPercent);
                    map.put("sgstPercent", sgstPercent);
                    map.put("igstPercent", igstPercent);
                    map.put("cgst", cgst);
                    map.put("sgst", sgst);
                    map.put("igst", igst);
                }

                map.put("netAmount", netAmount);
                map.put("version", "1.03");
                map.put("igstOnIntra", "N");
                map.put("regRev", "Y");
                map.put("taxSch", "GST");
                map.put("supTyp", "B2B");

                String sellerDetailsTmp = billTO.getSellerDetails();
                String sellerDetails[] = sellerDetailsTmp.split("~");
                map.put("compName", sellerDetails[0]);
                map.put("compAddress", sellerDetails[1]);
                map.put("compLocation", sellerDetails[2]);
                map.put("compState", sellerDetails[3]);
                map.put("compPincode", sellerDetails[4]);
                map.put("compGst", sellerDetails[5]);

                System.out.println("MAP--insertEinvoiceHeader-----" + map);

                headerStatus = (Integer) session.insert("billing.insertECreditInvoiceHeader", map);
                System.out.println("insertEinvoiceHeader---" + headerStatus);
            }
            map.put("eInvoiceId", headerStatus);

            System.out.println("MAP HEADER---" + map);
            map.put("invoiceId", billingTO.getInvoiceId());

            getEinvoiceDetails = (ArrayList) session.queryForList("billing.getECreditInvoiceDetails", map);
            System.out.println("getEinvoiceDetails.size()---" + getEinvoiceDetails.size());
            Iterator itr1 = getEinvoiceDetails.iterator();
            BillingTO billTO1 = new BillingTO();
            int sno = 0;
            while (itr1.hasNext()) {
                billTO1 = (BillingTO) itr1.next();
                sno = sno + 1;
                map.put("sno", sno);
                map.put("isServc", billTO1.getIsServc());
                map.put("prdDesc", billTO1.getPrdDesc());
                System.out.println("billTO1.getHsnCode()-----" + billTO1.getHsnCode());
                map.put("hsnCode", billTO1.getHsnCode());
                map.put("qty", billTO1.getQty());
                map.put("unit", billTO1.getUnit());
                map.put("unitPrice", billTO1.getUnitPrice());
                map.put("totAmount", billTO1.getTotAmount());
                map.put("assAmt", billTO1.getAssAmt());
                double cgstPercent = 0.0;
                double sgstPercent = 0.0;
                double igstPercent = 0.0;
                double cgst = 0.0;
                double sgst = 0.0;
                double igst = 0.0;
                double totalValue = 0.0;
                if ("6".equals(billTO1.getBillingState())) {
                    cgstPercent = 0.0;
                    sgstPercent = 0.0;
                    igstPercent = 0.0;
                    cgst = (Double.parseDouble(billTO1.getUnitPrice()) * cgstPercent) / 100;
                    sgst = (Double.parseDouble(billTO1.getUnitPrice()) * sgstPercent) / 100;
                    igst = (Double.parseDouble(billTO1.getUnitPrice()) * igstPercent) / 100;
                    totalValue = (Double.parseDouble(billTO1.getUnitPrice())) + igst + cgst + sgst;
                    map.put("cgstPercent", cgstPercent);
                    map.put("sgstPercent", sgstPercent);
                    map.put("igstPercent", igstPercent);
                    map.put("cgst", cgst);
                    map.put("sgst", sgst);
                    map.put("igst", igst);
                } else {
                    cgstPercent = 0.0;
                    sgstPercent = 0.0;
                    igstPercent = 0.0;
                    cgst = (Double.parseDouble(billTO1.getUnitPrice()) * cgstPercent) / 100;
                    sgst = (Double.parseDouble(billTO1.getUnitPrice()) * sgstPercent) / 100;
                    igst = (Double.parseDouble(billTO1.getUnitPrice()) * igstPercent) / 100;

                    totalValue = (Double.parseDouble(billTO1.getUnitPrice())) + igst + cgst + sgst;
                    map.put("cgstPercent", cgstPercent);
                    map.put("sgstPercent", sgstPercent);
                    map.put("igstPercent", igstPercent);
                    map.put("cgst", cgst);
                    map.put("sgst", sgst);
                    map.put("igst", igst);
                }

                map.put("totalValue", totalValue);
                map.put("expenseDesc", billTO1.getExpenseDesc());
                map.put("tripId", billTO1.getTripId());
                map.put("expenseId", billTO1.getExpenseId());
                map.put("userId", userId);

                System.out.println("MAP--insertEInvoiceDetails-----" + map);
                detailStatus = (Integer) session.update("billing.insertECreditInvoiceDetails", map);
                System.out.println("eInvoiceDetailGenerate----" + detailStatus);
            }
            System.out.println(" Invoice =" + detailStatus);
            map.put("custId", billingTO.getCustomerId());
            if (headerStatus > 0 && detailStatus > 0) {
                status = (Integer) session.update("billing.updateCreditInvoiceHeaderPostStatus", map);
                System.out.println("updateCustomerPostStatus =" + status);

            }
//            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return status;
    }

    public int eCreditSuppInvoiceGenerateAuto(BillingTO billingTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        int headerStatus = 0;
        int detailStatus = 0;
        String invoiceIds = billingTO.getInvoiceId();
        ArrayList getSellerBuyerDetails = new ArrayList();
        ArrayList getEinvoiceDetails = new ArrayList();
        try {
//            List invIds = new ArrayList(invoiceIds.length);
//            for (int j = 0; j < invoiceIds.length; j++) {
            System.out.println("cn value:" + billingTO.getInvoiceId());
            map.put("invoiceId", billingTO.getInvoiceId());
            System.out.println("map for trip det:" + map);
            getSellerBuyerDetails = (ArrayList) session.queryForList("billing.getCreditSuppSellerBuyerDetails", map);
            System.out.println("getSellerBuyerDetails.size()---" + getSellerBuyerDetails.size());
            Iterator itr = getSellerBuyerDetails.iterator();
            BillingTO billTO = new BillingTO();
            while (itr.hasNext()) {
                billTO = (BillingTO) itr.next();
                map.put("invoiceCode", billTO.getInvoiceCode());
                map.put("invoiceDate", billTO.getInvoiceDate());
                map.put("refInvoicecode", billTO.getRefInvoicecode());
                map.put("refInvoiceDate", billTO.getRefInvoiceDate());
                map.put("invType", billTO.getInvType());
                map.put("billingCustId", billTO.getBillingCustId());
                map.put("billingCustName", billTO.getBillingCustName());
                map.put("billingAddress", billTO.getBillingAddress());
                map.put("billingPanNo", billTO.getBillingPanNo());
                map.put("billingPincode", billTO.getBillingPincode());
                map.put("billingPos", billTO.getBillingPos());
                map.put("billingLocation", billTO.getBillingLocation());
                map.put("billingGst", billTO.getBillingGstin());
                map.put("billingState", billTO.getBillingState());
                map.put("grandTotal", billTO.getGrandTotal());
                map.put("grandTotal", billTO.getGrandTotal());
                double cgstPercent = 0.0;
                double sgstPercent = 0.0;
                double igstPercent = 0.0;
                double cgst = 0.0;
                double sgst = 0.0;
                double igst = 0.0;
                double netAmount = 0.0;
                if ("6".equals(billTO.getBillingState())) {
                    cgstPercent = 0.0;
                    sgstPercent = 0.0;
                    igstPercent = 0.0;
                    cgst = (Double.parseDouble(billTO.getGrandTotal()) * cgstPercent) / 100;
                    sgst = (Double.parseDouble(billTO.getGrandTotal()) * sgstPercent) / 100;
                    igst = (Double.parseDouble(billTO.getGrandTotal()) * igstPercent) / 100;
                    netAmount = (Double.parseDouble(billTO.getGrandTotal())) + igst + cgst + sgst;
                    map.put("cgstPercent", cgstPercent);
                    map.put("sgstPercent", sgstPercent);
                    map.put("igstPercent", igstPercent);
                    map.put("cgst", cgst);
                    map.put("sgst", sgst);
                    map.put("igst", igst);
                } else {
                    cgstPercent = 0.0;
                    sgstPercent = 0.0;
                    igstPercent = 0.0;
                    cgst = (Double.parseDouble(billTO.getGrandTotal()) * cgstPercent) / 100;
                    sgst = (Double.parseDouble(billTO.getGrandTotal()) * sgstPercent) / 100;
                    igst = (Double.parseDouble(billTO.getGrandTotal()) * igstPercent) / 100;

                    netAmount = (Double.parseDouble(billTO.getGrandTotal())) + igst + cgst + sgst;
                    map.put("cgstPercent", cgstPercent);
                    map.put("sgstPercent", sgstPercent);
                    map.put("igstPercent", igstPercent);
                    map.put("cgst", cgst);
                    map.put("sgst", sgst);
                    map.put("igst", igst);
                }

                map.put("netAmount", netAmount);
                map.put("version", "1.03");
                map.put("igstOnIntra", "N");
                map.put("regRev", "Y");
                map.put("taxSch", "GST");
                map.put("supTyp", "B2B");

                String sellerDetailsTmp = billTO.getSellerDetails();
                String sellerDetails[] = sellerDetailsTmp.split("~");
                map.put("compName", sellerDetails[0]);
                map.put("compAddress", sellerDetails[1]);
                map.put("compLocation", sellerDetails[2]);
                map.put("compState", sellerDetails[3]);
                map.put("compPincode", sellerDetails[4]);
                map.put("compGst", sellerDetails[5]);

                System.out.println("MAP--insertEinvoiceHeader-----" + map);

                headerStatus = (Integer) session.insert("billing.insertECreditSuppInvoiceHeader", map);
                System.out.println("insertEinvoiceHeader---" + headerStatus);
            }
            map.put("eInvoiceId", headerStatus);

            System.out.println("MAP HEADER---" + map);
            map.put("invoiceId", billingTO.getInvoiceId());

            getEinvoiceDetails = (ArrayList) session.queryForList("billing.getECreditSuppInvoiceDetails", map);
            System.out.println("getEinvoiceDetails.size()---" + getEinvoiceDetails.size());
            Iterator itr1 = getEinvoiceDetails.iterator();
            BillingTO billTO1 = new BillingTO();
            int sno = 0;
            while (itr1.hasNext()) {
                billTO1 = (BillingTO) itr1.next();
                sno = sno + 1;
                map.put("sno", sno);
                map.put("isServc", billTO1.getIsServc());
                map.put("prdDesc", billTO1.getPrdDesc());
                System.out.println("billTO1.getHsnCode()-----" + billTO1.getHsnCode());
                map.put("hsnCode", billTO1.getHsnCode());
                map.put("qty", billTO1.getQty());
                map.put("unit", billTO1.getUnit());
                map.put("unitPrice", billTO1.getUnitPrice());
                map.put("totAmount", billTO1.getTotAmount());
                map.put("assAmt", billTO1.getAssAmt());
                double cgstPercent = 0.0;
                double sgstPercent = 0.0;
                double igstPercent = 0.0;
                double cgst = 0.0;
                double sgst = 0.0;
                double igst = 0.0;
                double totalValue = 0.0;
                if ("6".equals(billTO1.getBillingState())) {
                    cgstPercent = 0.0;
                    sgstPercent = 0.0;
                    igstPercent = 0.0;
                    cgst = (Double.parseDouble(billTO1.getUnitPrice()) * cgstPercent) / 100;
                    sgst = (Double.parseDouble(billTO1.getUnitPrice()) * sgstPercent) / 100;
                    igst = (Double.parseDouble(billTO1.getUnitPrice()) * igstPercent) / 100;
                    totalValue = (Double.parseDouble(billTO1.getUnitPrice())) + igst + cgst + sgst;
                    map.put("cgstPercent", cgstPercent);
                    map.put("sgstPercent", sgstPercent);
                    map.put("igstPercent", igstPercent);
                    map.put("cgst", cgst);
                    map.put("sgst", sgst);
                    map.put("igst", igst);
                } else {
                    cgstPercent = 0.0;
                    sgstPercent = 0.0;
                    igstPercent = 0.0;
                    cgst = (Double.parseDouble(billTO1.getUnitPrice()) * cgstPercent) / 100;
                    sgst = (Double.parseDouble(billTO1.getUnitPrice()) * sgstPercent) / 100;
                    igst = (Double.parseDouble(billTO1.getUnitPrice()) * igstPercent) / 100;

                    totalValue = (Double.parseDouble(billTO1.getUnitPrice())) + igst + cgst + sgst;
                    map.put("cgstPercent", cgstPercent);
                    map.put("sgstPercent", sgstPercent);
                    map.put("igstPercent", igstPercent);
                    map.put("cgst", cgst);
                    map.put("sgst", sgst);
                    map.put("igst", igst);
                }

                map.put("totalValue", totalValue);
                map.put("expenseDesc", billTO1.getExpenseDesc());
                map.put("tripId", billTO1.getTripId());
                map.put("expenseId", billTO1.getExpenseId());
                map.put("userId", userId);

                System.out.println("MAP--insertEInvoiceDetails-----" + map);
                detailStatus = (Integer) session.update("billing.insertECreditSuppInvoiceDetails", map);
                System.out.println("eInvoiceDetailGenerate----" + detailStatus);
            }
            System.out.println(" Invoice =" + status);
            map.put("custId", billingTO.getCustomerId());
            if (headerStatus > 0 && detailStatus > 0) {
                status = (Integer) session.update("billing.updateCreditSuppInvoiceHeaderPostStatus", map);
                System.out.println("updateCustomerPostStatus =" + status);

            }
//            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return status;
    }
    
    public ArrayList viewcombinedBillList(String customerId, String fromDate, String toDate, String tripType, String submitStatus, String billNo, String grNo, String containerNo) {
        Map map = new HashMap();
        ArrayList closedBillList = null;
        try {
            map.put("customerId", customerId);
            if (fromDate == null) {
                fromDate = "";
            }
            if (toDate == null) {
                toDate = "";
            }
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("tripType", tripType);
            map.put("submitStatus", submitStatus);
            if (billNo != null && !"".equals(billNo)) {
                map.put("billNo", "%" + billNo + "%");
            } else {
                map.put("billNo", "");
            }
            if (grNo != null && !"".equals(grNo)) {
                map.put("grNo", "%" + grNo + "%");
            } else {
                map.put("grNo", "");
            }
            if (containerNo != null && !"".equals(containerNo)) {
                map.put("containerNo", "%" + containerNo + "%");
            } else {
                map.put("containerNo", "");
            }
            System.out.println("getClosedbill Value map" + map);
            closedBillList = (ArrayList) getSqlMapClientTemplate().queryForList("billing.viewcombinedBillList", map);
            System.out.println("getbil details size is ::::" + closedBillList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return closedBillList;
    }

    
     public ArrayList viewWorkOrderSubmission(String invoiceId) {
        Map map = new HashMap();
        ArrayList closedBillList = null;
        try {
            map.put("invoiceId", invoiceId);
           
            System.out.println("getClosedbill Value map" + map);
            closedBillList = (ArrayList) getSqlMapClientTemplate().queryForList("billing.viewWorkOrderForSubmission", map);
            System.out.println("getbil details size is ::::" + closedBillList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return closedBillList;
    }
     
     
      public int insertWorkOrderDetails(String invoiceId,String workOrderNumber) {
        Map map = new HashMap();
        int status = 0;
        
       
        map.put("invoiceId", invoiceId);
        map.put("workOrderNumber", workOrderNumber);
        System.out.println("map for trip det:" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("billing.updateWorkOrder", map);
            System.out.println("Approve Invoice =" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return status;
    }


      
       public ArrayList viewWorkOrderTwoSubmission(String invoiceId) {
        Map map = new HashMap();
        ArrayList viewWorkOrderTwoSubmission = null;
        try {
            map.put("invoiceId", invoiceId);
           
            System.out.println("getClosedbill Value map" + map);
            viewWorkOrderTwoSubmission = (ArrayList) getSqlMapClientTemplate().queryForList("billing.viewWorkOrderTwoForSubmission", map);
            System.out.println("getbil viewWorkOrderTwoSubmission size is ::::" + viewWorkOrderTwoSubmission.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return viewWorkOrderTwoSubmission;
    }
       
       
        public int insertWorkOrderTwoDetails(String invoiceId,String workOrderNumber) {
        Map map = new HashMap();
        int status = 0;
        
       
        map.put("invoiceId", invoiceId);
        map.put("workOrderNumber", workOrderNumber);
        System.out.println("map for trip det:" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("billing.updateWorkOrderSupp", map);
            System.out.println("Approve Invoice =" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return status;
    }

    public String getPriviewPrintBilling(String billingPartyId) {
        Map map = new HashMap();
        String getPriviewPrintBilling = "";
        try {

            map.put("billingPartyId", billingPartyId);
                getPriviewPrintBilling = (String) getSqlMapClientTemplate().queryForObject("billing.getPriviewPrintBilling", map);
            System.out.println("getPriviewPrintBilling=" + getPriviewPrintBilling);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return getPriviewPrintBilling;
    }


}
