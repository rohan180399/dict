
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="ets.domain.report.business.ReportTO" %>
        <%@ page import="java.util.*" %>
         <SCRIPT LANGUAGE="Javascript" SRC="/throttle/js/FusionCharts.js"></SCRIPT>
    </head>
    
<script language="javascript">
        function show_src() {
            document.getElementById('exp_table').style.display='none';
        }
        function show_exp() {
            document.getElementById('exp_table').style.display='block';
        }
        function show_close() {
            document.getElementById('exp_table').style.display='none';
        }
       function submitPage(){
            var chek=validation();
           if(chek=='true'){
           document.vehicleGraph.action='/throttle/vehicleAgeComparisionReport.do';
           document.vehicleGraph.submit();
           }
           }
            function validation(){
               if(textValidation(document.vehicleGraph.fromDate,'From Date')){
                   
                   document.vehicleGraph.fromDate.focus();
                   return 'false';
                   }
                  
                    return 'true';
               }
        function setValues(){
           if('<%=request.getAttribute("date")%>'!='null'){
            document.vehicleGraph.fromDate.value='<%=request.getAttribute("date")%>';
            
           document.vehicleGraph.mfrId.value='<%=request.getAttribute("mfrId")%>';
                }
            }
                        
    </script>
    <body onload="setValues();">
        <form  name="vehicleGraph" method="post">
            <%@ include file="/content/common/path.jsp" %>                 
            <%@ include file="/content/common/message.jsp" %>

<table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
<tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
</h2></td>
<td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
</tr>
<tr id="exp_table" >
<td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
    <div class="tabs" align="left" style="width:850;">
<ul class="tabNavigation">
        <li style="background:#76b3f1">Vehicle Comparision Chart</li>
</ul>
<div id="first">
<table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
<tr>
<td><font color="red">*</font>As On Date</td>
                <td><input name="fromDate" class="form-control" type="text" value="" size="20">
                <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.vehicleGraph.fromDate,'dd-mm-yyyy',this)"/></td>
               <td height="25"><font color="red">*</font>Mfr</td>
<td height="25"><select name="mfrId" style="width:125px">
<c:if test = "${MfrList != null}" >
        <c:forEach items="${MfrList}" var="mfr">
            <option value="<c:out value="${mfr.mfrId}"/>"><c:out value="${mfr.mfrName}"/></option>
        </c:forEach>
    </c:if>
</select></td>

<td><input type="button" class="button" value="Search" onclick="submitPage();" /> </td>
</tr>
</table>
</div></div>
</td>
</tr>
</table>

       
            <table  align="Right" width="250" class="border" border="0" cellspacing="0" cellpadding="0">
               

    <%
  
ArrayList comparisionList = (ArrayList) request.getAttribute("VehicleList");
System.out.println(comparisionList.size());
 if(comparisionList.size()!=0 ){
int size = comparisionList.size(); 
String[][] arrData = new String[size][2];

int i = 0;   
Iterator itr = comparisionList.iterator();
ReportTO repTO = new ReportTO();
while(itr.hasNext()){
repTO = (ReportTO) itr.next();
arrData[i][0] = repTO.getYear();
arrData[i][1] = repTO.getVehCount();

i++;
}

String  strXML = "<graph caption='Vehicle Age Comparision'  YAxisName='Total Vehicles' showNames='1' rotateNames='1' XAxisName='Year' showAlternateHGridColor='1' AlternateHGridColor='ff5904' divLineColor='ff5904' divLineAlpha='20' alternateHGridAlpha='5' formatNumberScale='0' decimalPrecision='0'>";

//Initialize <categories> element - necessary to generate a stacked chart
String strCategories = "<categories>";

//Initiate <dataset> elements
String strDataProdA = "<dataset seriesName='Total Vehicle' color='BF9900'>";


//Iterate through the data	
for(int j=0;j<arrData.length;j++){
//Append <category name='...' /> to strCategories
strCategories += "<category name='" + arrData[j][0] + "' />";
//Add <set value='...' /> to both the datasets

strDataProdA += "<set value='" + arrData[j][1] + "' />";

   

}

//Close <categories> element
strCategories += "</categories>";

//Close <dataset> elements
strDataProdA += "</dataset>";


//Assemble the entire XML now
strXML += strCategories + strDataProdA + "</graph>";

%>

        <table align="center" style="margin-left:10px;">
            <tr>
                <td >
                      <jsp:include page="FusionChartsRenderer.jsp" flush="true"> 
                <jsp:param name="chartSWF" value="/throttle/swf/FCF_StackedColumn2D.swf" /> 
                <jsp:param name="strURL" value="" /> 
                <jsp:param name="strXML" value="<%=strXML %>" /> 
                <jsp:param name="chartId" value="productSales" /> 
                <jsp:param name="chartWidth" value="800" /> 
                <jsp:param name="chartHeight" value="450" />
                <jsp:param name="debugMode" value="false" /> 	
                <jsp:param name="registerWithJS" value="false" /> 
        </jsp:include>
                </td>
            </tr>
            
        </table>



<%}%>

</table>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
