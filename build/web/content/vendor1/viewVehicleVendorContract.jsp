<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.customer.business.CustomerTO" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->

        <script  type="text/javascript" src="js/jq-ac-script.js"></script>




    </head>
    <script language="javascript">
        $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#custName').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCustomerName.do",
                        dataType: "json",
                        data: {
                            custName: request.term
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {
                                $('#customerId').val('');
                                $('#custName').val('');
                            }
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $('#customerId').val(tmp[0]);
                    $('#custName').val(tmp[1]);
                    return false;
                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[1] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };

        });
        $(document).ready(function() {
            $('#customerCode').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCustomerCode.do",
                        dataType: "json",
                        data: {
                            customerCode: request.term
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {
                                $('#customerId').val('');
                                $('#customerCode').val('');
                            }
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $('#customerId').val(tmp[0]);
                    $('#customerCode').val(tmp[1]);
                    return false;
                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[1] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
        });



        function checkVehicle() {
            document.vehicleVendorContract.action = '/throttle/createVehicleVendorContract.do';
            document.vehicleVendorContract.submit();
        }
        function submitPage() {
//            var value=document.getElementById("contractType").value;
//            if (value == '2') {
//              document.getElementById("fixedAmount").value=document.getElementById("fixedAmountAK").value;
//              document.getElementById("rateExtraKm").value=document.getElementById("rateExtraKmAK").value;
//              document.getElementById("fuleExpenseby").value=document.getElementById("fuleExpensebyAK").value;
//              document.getElementById("fuleExpenseby").value=document.getElementById("fuleExpensebyAK").value;
//            }
            document.vehicleVendorContract.action = '/throttle/saveVehicleVendorContract.do';
            document.vehicleVendorContract.submit();
        }
        function checkContractType() {
            
          var  value=document.getElementById("contractType").value;
            if (value == '1') {
                $("#fixedkmtable").show();
                $("#fixedKmTr").show();
                $("#fixedKmTr1").show();
                $("#actualKmTr").hide();
                $("#actualkmtable").hide();
            } else {
                $("#actualkmtable").show();
                $("#fixedkmtable").hide();
                $("#fixedKmTr").hide();
                $("#fixedKmTr1").hide();
                $("#actualkmtable").show();
            }
        }
    </script>
    <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });



        });

        $(function() {
            //	alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });

        });
    </script>

    <body onload="checkContractType();">
        <form name="vehicleVendorContract" method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>

            <table width="980" align="center" border="0" id="table2" >
                <tr>
                    <td class="contenthead" colspan="4" >View/Alter Vehicle Vendor</td>
                </tr>
                <c:if test = "${viewVendorVehicleContract != null}" >
                    <c:forEach items="${viewVendorVehicleContract}" var="vendorContract">
                        <tr>
                            <td height="30"><font color="red">*</font>Start Date</td>
                            <td><input type="text" name="startDate" id="startDate" value="<c:out value="${vendorContract.startDate}" />" class="datepicker"></td>
                        <input type="hidden" name="vendorId" id="vendorId" value="<c:out value="${vendorContract.vendorId}" />" >
                        <input type="hidden" name="contractId" id="contractId" value="<c:out value="${vendorContract.contractId}" />" >
                        </td>
                        <td height="30"><font color="red">*</font>End Date</td>
                        <td><input type="text" name="endDate" id="endDate" value="<c:out value="${vendorContract.endDate}" />" class="datepicker"></td>
                        </tr>
                        <tr>
                            <td height="30"><font color="red">*</font>Vehicle Type</td>
                            <td>
                                <select class="form-control" name="vehicleTypeId" id="vehicleTypeId">
                                    <option value="0">---Select---</option>
                                            <option value='<c:out value="${vendorContract.vehicleTypeId}" />'><c:out value="${vendorContract.vehicleTypeName}" /></option>
                                    <c:if test = "${TypeList != null}" >
                                        <c:forEach items="${TypeList}" var="Type">
                                        </c:forEach >
                                        <script>
                                            document.getElementById('vehicleTypeId').value = '<c:out value="${vendorContract.vehicleTypeId}"/>'
                                        </script>
                                    </c:if>
                                </select>
                            </td>
<!--                            <td height="30"><font color="red">*</font>Vehicle No</td>
                            <td>
                                <select name="vehicleId" id="vehicleId" class="form-control">
                                    <option value="0">-select-</option>
                                    <c:if test = "${vehicleList != null}" >
                                        <c:forEach items="${vehicleList}" var="reg">
                                            <option value='<c:out value="${reg.vehicleId}" />'> <c:out value="${reg.vehicleNo}" /></option>
                                        </c:forEach>
                                    </c:if>
                                    <script>
                                        document.getElementById('vehicleId').value = '<c:out value="${vendorContract.vehicleId}"/>'
                                    </script>          
                                </select>
                            </td>-->
                        </tr>
                        <tr>
                            <td height="30"><font color="red">*</font>Contract Type</td>
                            <td>
                                <select name="contractType" id="contractType" class="form-control" onchange="checkContractType(this.value);">
                                    <!--<option value="0" selected>--Select--</option>-->
                                    <option value="1" >Fixed Km</option>
                                    <option value="2" >Actual Km</option>
                                    <script>
                                         document.getElementById('contractType').value = '<c:out value="${vendorContract.contractType}"/>'
                                    </script>       
                                </select>
                            </td>
                            <td height="30"><font color="red">*</font>Operation Type</td>
                            <td>
                                <select name="operationType" id="operationType" class="form-control">
                                    <option value="1" >Primary</option>
                                    <option value="2" >Secondary</option>
                                    <script>
                                         document.getElementById('operationType').value = '<c:out value="${vendorContract.operationType}"/>'
                                    </script> 
                                </select>
                            </td>

                        </tr>
                        <tr>
                            <!--                <div id="fixedkmtable">-->
                            <td class="contenthead" id="fixedkmtable" colspan="4" >Terms & Conditions (if fixed km)</td> 
                            <!--</div>-->
                            <!--<div id="actualkmtable">-->
                            <td class="contenthead" id="actualkmtable" colspan="4" >Terms & Conditions (if Actual km)</td> 
                            <!--</div>-->
                        </tr>
                        <tr id="fixedKmTr">
                            <td height="30"><font color="red">*</font>Fixed km</td>
                            <td><input type="text" name="fixedKm" id="fixedKm" value="<c:out value="${vendorContract.fixedKm}"/>" class="form-control"></td>
                            <td height="30"><font color="red">*</font>Fixed Amount</td>
                            <td><input type="text" name="fixedAmount" id="fixedAmount" value="<c:out value="${vendorContract.fixedAmount}"/>" class="form-control"></td>
                        </tr> 
                        <tr id="fixedKmTr1">
                            <td height="30"><font color="red">*</font>Rate/Extra Km</td>
                            <td><input type="text" name="rateExtraKm" id="rateExtraKm" value="<c:out value="${vendorContract.ratePerExtraKm}"/>" class="form-control"></td>
                            <td height="30">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr id="actualKmtr">
                            <td height="30"><font color="red">*</font>Rate per Km</td>
                            <td><input type="text" name="ratePerKm" id="ratePerKm" value="<c:out value="${vendorContract.ratePerKm}"/>" class="form-control"></td>
                            <td height="30">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td height="30"><font color="red">*</font>Driver Reponsibility</td>
                            <td>
                                <select name="driverReponsibility" id="driverReponsibility" class="form-control">
                                    <option value="1" >Interem</option>
                                    <option value="2" >vendor</option>
                                    <script>
                                         document.getElementById('driverReponsibility').value = '<c:out value="${vendorContract.driverResponsibility}"/>'
                                    </script> 
                                </select> 
                            </td>
                            <td height="30">Fuel & Expense By</td>
                            <td> 
                                <select name="fuleExpenseby" id="fuleExpenseby" class="form-control">
                                    <option value="1" >Interem</option>
                                    <option value="2" >vendor</option>
                                    <script>
                                         document.getElementById('fuelExpenseResponsibility').value = '<c:out value="${vendorContract.fuelExpenseResponsibility}"/>'
                                    </script> 
                                </select> 
                            </td>
                        </tr>
                        <tr>
                            <td height="30"><font color="red">*</font>Payment Type</td>
                            <td>
                                <select name="paymentType" id="paymentType" class="form-control">
                                    <option value="1" >Trip Wise</option>
                                    <option value="2" >Monthly</option>
                                    <script>
                                         document.getElementById('paymentType').value = '<c:out value="${vendorContract.paymentType}"/>'
                                    </script> 
                                </select> 
                            </td>
                            <td height="30">Payment Schedule Days</td>
                            <td> 
                                <input type="text" name="paymentScheduleDays" id="paymentScheduleDays" value="<c:out value="${vendorContract.paymentScheduleDays}"/>" class="form-control">
                            </td>
                        </tr>
                        <tr>
                            <td height="30"><font color="red">*</font>Payment Type</td>
                            <td>
                                <select name="activeInd" id="activeInd" class="form-control">
                                    <option value="Y" >Active</option>
                                    <option value="N" >InActive</option>
                                    <script>
                                         document.getElementById('activeInd').value = '<c:out value="${vendorContract.activeInd}"/>'
                                    </script> 
                                </select> 
                            </td>
                            <td height="30">&nbsp;</td>
                            <td> 
                                &nbsp;
                            </td>
                        </tr>
                    </c:forEach> 
                </c:if>
                <tr>
                    <td colspan="4" align="center">
                        <input type="button"   value="SAVE" class="button" name="save" onClick="submitPage();">
                    </td>
                </tr>
            </table>
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
