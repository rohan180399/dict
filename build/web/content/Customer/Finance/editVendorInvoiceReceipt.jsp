<%-- 
    Document   : editVendorInvoiceReceipt
    Created on : 3 Feb, 2017, 3:26:11 PM
    Author     : pavithra
--%>


<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {

        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            dateFormat: 'dd-mm-yy',
            changeMonth: true, changeYear: true
        });

    });
</script>
<script type="text/javascript">
    var httpReq;
    function setVendor(vendorTypeid) { //alert(str);
        var temp = "";
//                                alert(vendorTypeid);
        $.ajax({
            url: "/throttle/vendorPaymentsJson.do",
            dataType: "json",
            data: {
                vendorType: vendorTypeid
            },
            success: function(temp) {
//                                         alert(temp);
                if (temp != '') {
                    $('#vendorName').empty();
                    $('#vendorName').append(
                            $('<option style="width:150px"></option>').val(0 + "~" + 0).html('---Select----')
                            )
                    $.each(temp, function(i, data) {
                        $('#vendorName').append(
                                $('<option value="' + data.vendorId + "~" + data.ledgerId + '" style="width:150px"></option>').val(data.vendorId + "~" + data.ledgerId).html(data.vendorName)
                                )
                    });
                } else {
                    $('#vendorName').empty();
                }
            }
        });

    }
</script>


<script type="text/javascript">
    var httpReq;
    function getLedgerList(levelId) { //alert(str);
        var temp = "";
//                                alert(levelId);
        $.ajax({
            url: "/throttle/getLedgerListforGroup.do",
            dataType: "json",
            data: {
                levelId: levelId
            },
            success: function(temp) {
//                                         alert(temp);
                if (temp != '') {
                    $('#debitLedgerId').empty();
                    $('#debitLedgerId').append(
                            $('<option></option>').val(0).html('---Select----')
                            )
                    $.each(temp, function(i, data) {
                        $('#debitLedgerId').append(
                                $('<option value="' + data.ledgerID + '" ></option>').val(data.ledgerID).html(data.ledgerName)
                                )
                    });
                } else {
                    $('#debitLedgerId').empty();
                }
            }
        });

    }
</script>
<script type="text/javascript">
    function searchPage(value) {
        var vendorTypeId = document.getElementById("vendorType").value;
        var vendorNameId = document.getElementById("vendorName").value;
        var invoiceDate = document.getElementById("invoiceDate").value;
        if (vendorTypeId == null || vendorTypeId == "" || vendorTypeId == "0") {
            alert('Please Select vendorType');
        } else if (vendorNameId == null || vendorNameId == "" || vendorNameId == "0~0") {
            alert('Please Select Vendor');
        } else if (invoiceDate == null || invoiceDate == "") {
            alert('Please Select Invoice Date');
        } else {
            document.payment.action = '/throttle/addVendorInvoiceReceipt.do?param=' + value;
            document.payment.submit();
        }
    }
      function submitPage(value)
    {

        var count = document.getElementById("count").value;
        var contractTypeId = document.getElementById("contractTypeId").value;
        var invoiceAmount = document.getElementById("invoiceAmount").value;
          var vendorNameId = document.getElementById("vendorName").value;
        var invoiceDate = document.getElementById("invoiceDate").value;
        var invoiceNo = document.getElementById("invoiceNo").value;

        var form = document.getElementById("payment");
        if (contractTypeId == 2) {
            var totalAmount = document.getElementById("totalAmtHire").value;
            if (parseFloat(invoiceAmount).toFixed(2) == parseFloat(totalAmount).toFixed(2)) {
                if (vendorNameId == null || vendorNameId == "" || vendorNameId == "0~0") {
                        alert('Please Select Vendor');
                    } else if (invoiceDate == null || invoiceDate == "") {
                        alert('Please Select Invoice Date');
                    } else if (invoiceNo == null || invoiceNo == "") {
                        alert('Please Select Invoice No');
                    } else {
                        form.setAttribute("enctype", "multipart/form-data");
                        document.payment.action = '/throttle/insertVendorInvoiceReceipt.do?count=' + count;
                        document.payment.submit();
                    }
                } else {
                    alert("Invoice Amount doesn't match");
                }
            } else if (contractTypeId == 1) {
                var totalAmount = document.getElementById("totalAmtDedicate").value;
                if (parseFloat(invoiceAmount).toFixed(2) == parseFloat(totalAmount).toFixed(2)) {
                    form.setAttribute("enctype", "multipart/form-data");
                    document.payment.action = '/throttle/insertVendorInvoiceReceiptDedicate.do?count=' + count;
                    document.payment.submit();
                } else {
                    alert("Invoice Amount doesn't match");
                }
            }
        }

    function setActiveIndHire(sno) {
        if (document.getElementById("selectedIndexHire" + sno).checked) {
            document.getElementById("activeIndHire" + sno).value = 'Y';
            document.getElementById("totAmttHire" + sno).value = document.getElementById("totAmtHire" + sno).value;
            //document.getElementById("noOfTripssHire" + sno).value = document.getElementById("noOfTripsHire" + sno).value;

        } else {
//                alert("hai");
            document.getElementById("activeIndHire" + sno).value = 'N';
            document.getElementById("totAmttHire" + sno).value = '';
        }
        setValueHire();
    }
    function setValueHire() {

        // document.getElementById("totalNoOfTripsHire").innerHTML = '';
        var temp = 0;
        var temp1 = 0;
        var cntr = 0;
        var totAmtt = document.getElementsByName('totAmttHire');
        //var noOfTripss = document.getElementsByName('noOfTripssHire');
        var activeInd = document.getElementsByName('activeIndHire');
        for (var i = 0; i < totAmtt.length; i++) {
            if (activeInd[i].value == 'Y') {
                if (cntr == 0) {
                    temp = totAmtt[i].value;
                    // temp1 = noOfTripss[i].value;
                    //   alert(temp);
                } else {
                    temp = +temp + +totAmtt[i].value;
                    //  temp1 = +temp1 + +noOfTripss[i].value;
                    // alert(temp);
                }
                cntr++;


            }

        }
        document.getElementById("totalAmtHire").value = parseFloat(temp).toFixed(2);
        document.getElementById("totalAmountHire").innerHTML = '';
        document.getElementById("totalAmountHire").innerHTML = parseFloat(temp).toFixed(2);
        var rtgsHire = 0;
        var serviceTaxHire = 0;
        // rtgsHire = document.getElementById("rtgsHire").value;
        // serviceTaxHire = document.getElementById("serviceTaxHire").value;

        // document.getElementById("totalInvoiceAmountHire").innerHTML = parseFloat(parseFloat(temp) + parseFloat(serviceTaxHire) + parseFloat(rtgsHire)).toFixed(2);
        // document.getElementById("actualInvoiceAmountHire").value = parseFloat(parseFloat(temp) + parseFloat(serviceTaxHire) + parseFloat(rtgsHire)).toFixed(2);

        // document.getElementById("totalNoOfTripsHire").innerHTML = temp1;
    }
    function setActiveIndDedicate(sno) {
        if (document.getElementById("selectedIndexDedicate" + sno).checked) {
            document.getElementById("activeIndDedicate" + sno).value = 'Y';
            document.getElementById("totAmttDedicate" + sno).value = document.getElementById("totAmtDedicate" + sno).value;

        } else {
            document.getElementById("activeIndDedicate" + sno).value = 'N';
            document.getElementById("totAmttDedicate" + sno).value = '';
        }
        setValueDedicate();
    }
    function setValueDedicate() {

        var temp = 0;
        var temp1 = 0;
        var cntr = 0;
        var totAmtt = document.getElementsByName('totAmttDedicate');
        var activeInd = document.getElementsByName('activeIndDedicate');
        for (var i = 0; i < totAmtt.length; i++) {
            if (activeInd[i].value == 'Y') {
                if (cntr == 0) {
                    temp = totAmtt[i].value;
                } else {
                    temp = +temp + +totAmtt[i].value;
                }
                cntr++;


            }

        }
        document.getElementById("totalAmtDedicate").value = parseFloat(temp).toFixed(2);
        document.getElementById("totalAmountDedicate").innerHTML = '';
        document.getElementById("totalAmountDedicate").innerHTML = parseFloat(temp).toFixed(2);
    }
</script>
<style>
    #index td {
        color:white;
    }
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i>Edit Vendor Invoice Receipt</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance"  text="default text"/></a></li>
            <li class="active">Edit Vendor Invoice Receipt</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="payment" id="payment" class="form-horizontal form-bordered" method="post">
                    <input type="hidden" name="virCode" id="virCode" value="<c:out value="${virCode}"/>"/>
                    <input type="hidden" name="virId" id="virId" value="<c:out value="${virId}"/>"/>
                    <font style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; ">
                    <div align="center" id="status1">&nbsp;&nbsp;
                    </div>
                    </font>
                    <c:if test="${VIRdetailList != null}">
                        <c:forEach items="${VIRdetailList}" var="pd">
                            <c:set var="invoiceNo" value="${pd.invoiceNo}"/>
                            <c:set var="invoiceAmount" value="${pd.invoiceAmount}"/>
                            <c:set var="invoiceDate" value="${pd.invoiceDate}"/>
                            <c:set var="remarks" value="${pd.headerNarration}"/>
                            <c:set var="virId" value="${pd.virId}"/>
                            <c:set var="vendorType" value="${pd.vendorTypeId}"/>
                            <c:set var="vendorId" value="${pd.vendorId}"/>
                            <c:set var="vendorLedgerId" value="${pd.vendorLedgerId}"/>
                            <c:set var="fileName" value="${pd.fileName}"/>
                        </c:forEach>
                    </c:if>
                     <select type="hidden" name="contractTypeId" id="contractTypeId"  onchange="checkContractType(this.value);" style="visibility:hidden">
                                    <option value="2" selected>Hired</option>
                                </select>
                                <script>
                                 //   document.getElementById("contractTypeId").value = '<c:out value="${contractTypeId}"/>';
                                </script>
                                <select  name="vendorType" id="vendorType" onchange="setVendor(this.value);" style="visibility:hidden">
                                    <option value='0'>--Select--</option>
                                    <c:forEach items="${VendorTypeList}" var="vendor">
                                        <option value='<c:out value="${vendor.vendorTypeId}"/>'><c:out value="${vendor.vendorTypeValue}"/></option>
                                    </c:forEach>
                                </select>
                                <script>
                                    document.getElementById("vendorType").value = '<c:out value="${vendorType}"/>';
                                    document.getElementById("vendorName").value = '<c:out value="${vendorName}"/>';
                                </script>
                    <table class="table table-info mb30 table-hover">
                        <thead>
                            <tr height="30" id="index">
                                <th colspan="6" ><b>Edit Vendor Invoice Receipt</b></th>
                            </tr>
                        </thead>
                        <tr height="40">
                            
                            <td  ><font color=red>*</font><spring:message code="finance.label.Vendor"  text="default text"/></td>
                            <td  >
                                <select class="form-control" name="vendorName" id="vendorName" style="width:220px;height:40px;">
                                    <c:if test="${vendorList != null}">
                                        <c:forEach items="${vendorList}" var="vendor">
                                            <option value='<c:out value="${vendor.vendorId}"/>~<c:out value="${vendor.ledgerId}"/>'><c:out value="${vendor.vendorName}"/></option>
                                            <script>
                                                document.getElementById("vendorName").value = '<c:out value="${vendorId}"/>~<c:out value="${vendorLedgerId}"/>';
                                            </script>
                                        </c:forEach>
                                    </c:if>

                                </select>

                            </td>
                            <td><font color=red>*</font><spring:message code="finance.label.InvoiceDate"  text="default text" /></td>
                            <td>
                                <input type="textbox" name="invoiceDate" id="invoiceDate" value="<c:out value="${invoiceDate}"/>" autocomplete="off" class="datepicker" style="width:220px;height:40px;"  onchange="searchPage('search')"/>
                            </td>
                            <td   ><font color=red>*</font><spring:message code="finance.label.InvoiceNo"  text="default text"/></td>
                            <td   >
                                <input type="textbox" name="invoiceNo" id="invoiceNo" value="<c:out value="${invoiceNo}"/>" autocomplete="off" class="form-control" style="width:220px;height:40px;"/>
                            </td>
                        </tr>

                        <tr height="40">
                            <td  ><font color=red>*</font><spring:message code="finance.label.InvoiceAmount"  text="default text"/></td>
                            <td >
                                <input type="textbox" name="invoiceAmount" id="invoiceAmount" value="<c:out value="${invoiceAmount}"/>" autocomplete="off" onKeyPress="return onKeyPressBlockCharacters(event);" class="form-control" style="width:220px;height:40px;"/>
                            </td>
                            <td ><spring:message code="finance.label.Narration"  text="default text"/></td>
                            <td >
                                <textarea type="textbox"   name="remarks" id="remarks" value=""  class="form-control" style="width:220px;height:40px;"/><c:out value="${remarks}"/></textarea>
                            </td>

                            <td ><spring:message code="finance.label.Attach"  text="default text"/></td>
                            <td >
                                <input type="file" name="file" size="45"  multiple/>
                                <c:out value ="${fileName}"/>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <br>
                    <c:if test = "${contractTypeId == '2'}" >
                        <c:if test="${vendorInvoiceTripDetailsList != null}">
                            <div>
                                <input type="hidden" name="totalAmtHire" id="totalAmtHire" value="0" />
                                <table class="table table-info mb30 table-hover" border="1" >
                                    <thead>
                                        <tr align="center" id="index" height="30">
                                            <th height="30">S.No</th>
                                            <th height="30">Trip Id</th>
                                            <th height="30">Vehicle No</th>
                                            <th height="30">Vehicle Type</th>
                                            <th height="30">Container Type</th>
                                            <th height="30">Container Qty</th>
                                            <th height="30">Container Name</th>
                                            <th height="30">Route</th>
                                            <th height="30">Amount</th>
                                            <th height="30">Bill Status</th>
                                            <th height="30">Select</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <% int sno = 0;
                            int index = 1;%>
                                        <c:set var="totalAmtHire" value="${0}"/>
                                        <c:forEach items="${vendorInvoiceTripDetailsList}" var="list"> 
                                            <c:set var="totalAmtHire" value="${totalAmtHire + list.invoiceAmount}"/>
                                            <%

                                                String classText = "";
                                                int oddEven = index % 2;
                                                if (oddEven > 0) {
                                                    classText = "text2";
                                                } else {
                                                    classText = "text1";
                                                }
                                            %>

                                            <tr  width="208" height="40" > 
                                                <td height="20"><%=index%></td>
                                                <td height="20">
                                                    <input type="hidden" name="tripIdHire" id="tripIdHire<%=index%>" value="<c:out value="${list.tripId}"/>"/>
                                                    <c:out value="${list.tripId}"/>
                                                </td>
                                                <td height="20">
                                                    <input type="hidden" name="vehicleIdHire"  value="<c:out value="${list.vehicleNo}"/>"/>
                                                    <c:out value="${list.vehicleNo}"/>
                                                </td>
                                                <td height="20">
                                                    <input type="hidden" name="vehicleTypeIdHire"  value="<c:out value="${list.vehicleTypeId}"/>"/>
                                                    <c:out value="${list.vehicleTypeName}"/>
                                                </td>
                                               
                                                <td height="20" align="right">
                                                    <input type="hidden" name="containerHire"  value="<c:out value="${list.containerType}"/>"/>
                                                    <c:out value="${list.containerType}"/>
                                                </td>
                                                <td height="20" align="right">
                                                    <input type="hidden" name="containerQtyHire" id="containerQtyHire<%=index%>" value="<c:out value="${list.containerQuty}"/>"/>
                                                    <c:out value="${list.containerQuty}"/>
                                                </td>
                                                <td height="20" align="right">
                                                    <input type="hidden" name="containerNo"  value="<c:out value="${list.containerNo}"/>"/>
                                                    <c:out value="${list.containerNo}"/>
                                                </td>
                                                 <td height="20">
                                                    <input type="hidden" name="routeHire"  value="<c:out value="${list.routeIdFullTruck}"/>"/>
                                                    <c:out value="${list.routeIdFullTruck}"/>
                                                </td>
                                                <td height="20" align="right">
                                                    <input type="hidden" name="totAmtHire" id="totAmtHire<%=index%>" value="<c:out value="${list.invoiceAmount}"/>"/>
                                                    <c:out value="${list.invoiceAmount}"/>
                                                </td>
                                                <td height="20" align="right">
                                                    <c:if test = "${list.statusName == 0}" ><label style="color:red">Not Ready to Bill</label></c:if>
                                                    <c:if test = "${list.statusName == 1}" ><label style="color:green">Ready to Bill</label></c:if>

                                                    </td>
                                                    <td height="20" align="right">

                                                        <input type="checkbox" name="selectedIndexHire" id="selectedIndexHire<%=index%>" onclick="setActiveIndHire('<%=index%>');" checked/>
                                                    <input type="hidden" name ="activeIndHire" id="activeIndHire<%=index%>" value="N"/>
                                                    <input type="hidden" name="totAmttHire" id="totAmttHire<%=index%>" value=""/>
                                                </td>
                                            </tr>
                                        <script>setActiveIndHire(<%=index%>);</script>
                                        <%
                                            index++;
                                            sno++;
                                        %>
                                    </c:forEach>
                                    <input type="hidden" name="count" id="count" value="<%=sno%>"/>
                                    <tr>
                                        <td colspan="9" align="right" ><span id="totalAmountHire" style="color:black">
                                                <fmt:formatNumber pattern="##0.00" value="${totalAmtHire}"/>
                                            </span> 
                                        </td>
                                        <td colspan="2" align="right">&emsp;

                                        </td>

                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </c:if>
                    </c:if>
                    
                    <br>
                    <br>
                    <br>
                    <center>
                        <input type="button" value="Update" class="btn btn-success" id="saveButton" onClick="submitPage(this.value);" style="width:100px;height:35px;">
                        &emsp;<input type="reset" class="btn btn-success" value="<spring:message code="finance.label.Clear"  text="default text"/>" style="width:100px;height:35px;">
                    </center>
                    <br>
                    <br>
                    <br>
                    <br>

                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

