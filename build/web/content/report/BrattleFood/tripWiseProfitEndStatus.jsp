<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<style type="text/css" title="currentStyle">
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">


<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<script type="text/javascript">
    function submitPage(value) {
        if (document.getElementById('fromDate').value == '') {
            alert("Please select from Date");
            document.getElementById('fromDate').focus();
        } else if (document.getElementById('toDate').value == '') {
            alert("Please select to Date");
            document.getElementById('toDate').focus();
        } else {
            if (value == 'ExportExcel') {
                document.tripSheet.action = '/throttle/handleTripWiseProfitEndStatus.do?param=ExportExcel';
                document.tripSheet.submit();
            } else {
                document.tripSheet.action = '/throttle/handleTripWiseProfitEndStatus.do?param=Search';
                document.tripSheet.submit();
            }
        }
    }
    function setValue() {
        if ('<%=request.getAttribute("page")%>' != 'null') {
            var page = '<%=request.getAttribute("page")%>';
            if (page == 1) {
                submitPage('search');
            }
        }
        if ('<%=request.getAttribute("tripId")%>' != 'null') {
            document.getElementById('tripId').value = '<%=request.getAttribute("tripId")%>';
        }
        if ('<%=request.getAttribute("fleetCenterId")%>' != 'null') {
            document.getElementById('fleetCenterId').value = '<%=request.getAttribute("fleetCenterId")%>';
        }
    }
    function viewVehicleDetails(vehicleId) {
        //alert(vehicleId);
        window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    function viewTripDetails(tripId) {
        //alert(tripId);
        window.open('/throttle/viewTripSheetDetails.do?tripId=' + tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
</script>

<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
                  setValues();
                  getVehicleNos();">
    </c:if>

    <!--	  <span style="float: right">
                    <a href="?paramName=en">English</a>
                    |
                    <a href="?paramName=ar">Ø§Ù„Ø¹Ø±Ø¨ÙŠØ©</a>
              </span>-->
    <style>
        #index td {
            color:white;
        }
    </style>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i><spring:message code="operations.reports.label.TripWiseProfitability" text="default text"/></h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="operations.reports.label.Report" text="default text"/></a></li>
                <li class="active"><spring:message code="operations.reports.label.TripWiseProfitability" text="default text"/></li>
            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="setValue();
            sorter.size(20);">
                    <form name="tripSheet" method="post">



<!--                        <table class="table table-bordered" id="report"  >
                            <tr height="30" id="index" >
                                <td colspan="4"  style="background-color:#5BC0DE;">
                                    <b><spring:message code="operations.reports.label.TripWiseProfitability" text="default text"/></b>
                                </td> </tr>

                            <div id="first">
                                <td style="border-color:#5BC0DE;padding:16px;">-->
                                    <table class="table table-info mb30 table-hover" >
                                        <thead><tr><th colspan="4"><spring:message code="operations.reports.label.TripWiseProfitability" text="default text"/></b></th></tr></thead>
                                        <tr>
                                            <td align="center"><font color="red"></font><spring:message code="operations.reports.label.TripCode" text="default text"/>
                                            </td>
                                            <td height="30">
                                                <input type="hidden" name="vehicleId" id="vehicleId" value="<c:out value="${vehicleId}"/>"/>
                                                <input type="text" name="tripId" style="width:260px;height:40px;" id="tripId" value="" class="form-control""/>
                                            </td>
                                            <td ><spring:message code="operations.reports.label.FleetCenter" text="default text"/></td>
                                            <td height="30"> <select name="fleetCenterId" id="fleetCenterId" class="form-control" style="width:260px;height:40px;" >
                                                    <c:if test="${companyList != null}">
                                                        <option value="" selected>--<spring:message code="operations.reports.label.Select" text="default text"/>--</option>
                                                        <c:forEach items="${companyList}" var="companyList">
                                                            <option value='<c:out value="${companyList.cmpId}"/>'><c:out value="${companyList.name}"/></option>
                                                        </c:forEach>
                                                    </c:if>
                                                </select></td>
                                        </tr>
                                        <tr>
                                            <td align="center"><font color="red">*</font><spring:message code="operations.reports.label.FromDate" text="default text"/></td>
                                            <td height="30"><input name="fromDate" id="fromDate"  autocomplete="off" style="width:260px;height:40px;" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                            <td><font color="red">*</font><spring:message code="operations.reports.label.ToDate" text="default text"/></td>
                                            <td height="30"><input name="toDate" id="toDate"  autocomplete="off" style="width:260px;height:40px;" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                                        </tr>
                                    </table>
                                        <tr align="center">
                                        <center><td><input type="button" class="btn btn-success" name="ExportExcel"   value="<spring:message code="operations.reports.label.EXPORTEXCEL" text="default text"/>" onclick="submitPage(this.name);">&nbsp;&nbsp;</td>
                                            <td><input type="button" class="btn btn-success" name="Search"  id="Search"  value="<spring:message code="operations.reports.label.SEARCH" text="default text"/>" onclick="submitPage(this.name);"></td></center>
                                        </tr>
                                        <br>
<!--                            </div></div>
                            </td>
                            </tr>
                        </table>-->
<br>
                        <c:if test="${tripDetails != null}">
                            <table class="table table-info mb30 table-hover sortable" id="table"  width="100%">
                                <thead>
                                    <tr >
                                        <th rowspan="2"><spring:message code="operations.reports.label.SNo" text="default text"/></th>
                                        <th rowspan="2"><spring:message code="operations.reports.label.VehicleNo" text="default text"/> </th>
                                        <th rowspan="2"><spring:message code="operations.reports.label.FleetCenter" text="default text"/> </th>
                                        <th rowspan="2"><spring:message code="operations.reports.label.TripCode" text="default text"/></th>
                                        <th rowspan="2"><spring:message code="operations.reports.label.ConsignmentNo" text="default text"/></th>
                                        <th rowspan="2"><spring:message code="operations.reports.label.CustomerName" text="default text"/> </th>
                                        <th rowspan="2"><spring:message code="operations.reports.label.CustomerType" text="default text"/></th>
                                        <th rowspan="2"><spring:message code="operations.reports.label.BillingType" text="default text"/> </th>
                                        <th rowspan="2"><spring:message code="operations.reports.label.Route" text="default text"/> </th>
                                        <th rowspan="2"><spring:message code="operations.reports.label.VehicleType" text="default text"/> </th>
                                        <th rowspan="2"><spring:message code="operations.reports.label.Reefer" text="default text"/> </th>
                                        <th rowspan="2"><spring:message code="operations.reports.label.DriverName" text="default text"/></th>
                                        <th rowspan="2"><spring:message code="operations.reports.label.EstimatedRevenue" text="default text"/> </th>
                                        <th rowspan="2"><spring:message code="operations.reports.label.RequestedAdvance" text="default text"/></th>
                                        <th rowspan="2"><spring:message code="operations.reports.label.BPCLAllocation" text="default text"/></th>
                                        <th rowspan="2"><spring:message code="operations.reports.label.Profit" text="default text"/> </th>
                                        <th rowspan="2"><spring:message code="operations.reports.label.Profit" text="default text"/> %</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% int index = 1;%>
                                    <c:set var="totalTrips" value="0" />
                                    <c:set var="totalEarnings" value="0" />
                                    <c:set var="requestedAdvance" value="0" />
                                    <c:set var="earnings" value="0" />
                                    <c:set var="expense" value="0" />
                                    <c:forEach items="${tripDetails}" var="tripDetails">
                                        <%
                                                       String classText = "";
                                                       int oddEven = index % 2;
                                                       if (oddEven > 0) {
                                                           classText = "text2";
                                                       } else {
                                                           classText = "text1";
                                                       }
                                        %>

                                        <c:set var="totalTrips" value="${totalTrips + 1}"/>
                                        <c:set var="totalEarnings" value="${totalEarnings + tripDetails.revenue}"/>
                                        <c:set var="requestedAdvance" value="${requestedAdvance + tripDetails.requestedAdvance}"/>
                                        <c:set var="totalBpclAllocated" value="${totalBpclAllocated + tripDetails.paidAdvance}"/>
                                        <tr >
                                            <td class="<%=classText%>"  align="center"><%=index++%></td>
                                            <td width="150" class="<%=classText%>" align="center" ><a href="#" onclick="viewVehicleDetails('<c:out value="${tripDetails.vehicleId}"/>')"><c:out value="${tripDetails.vehicleNo}"/></a></td>
                                            <td class="<%=classText%>"  align="center" ><c:out value="${tripDetails.companyName}"/></td>
                                            <td class="<%=classText%>"  align="center">
                                                <a href="#" onclick="viewTripDetails('<c:out value="${tripDetails.tripSheetId}"/>');"><c:out value="${tripDetails.tripCode}"/></a>
                                            </td>

                                            <td class="<%=classText%>"  align="center"><c:out value="${tripDetails.consignmentName}"/></td>
                                            <td class="<%=classText%>"  align="center"><c:out value="${tripDetails.customerName}"/></td>
                                            <td class="<%=classText%>"  align="center"><c:out value="${tripDetails.customerTypeId}"/></td>
                                            <td class="<%=classText%>"  align="center"><c:out value="${tripDetails.billingType}"/></td>
                                            <td class="<%=classText%>"  align="center"><c:out value="${tripDetails.routeName}"/></td>
                                            <td class="<%=classText%>"  align="center"><c:out value="${tripDetails.vehicleTypeName}"/></td>
                                            <td class="<%=classText%>"  align="center"><c:out value="${tripDetails.reeferRequired}"/></td>
                                            <td class="<%=classText%>"  align="center"><c:out value="${tripDetails.driverName}"/></td>
                                            <td class="<%=classText%>"  align="center"><fmt:formatNumber pattern="##0.00" value="${tripDetails.revenue}"/></td>
                                            <td class="<%=classText%>"  align="center"><fmt:formatNumber pattern="##0.00" value="${tripDetails.requestedAdvance}"/></td>
                                            <td class="<%=classText%>"  align="center"><fmt:formatNumber pattern="##0.00" value="${tripDetails.paidAdvance}"/></td>
                                            <%--                            <td class="<%=classText%>"  align="center"><fmt:formatNumber pattern="##0.00" value="${tripDetails.fuelAmount}"/></td>
                                                                        <td class="<%=classText%>"  align="center"><fmt:formatNumber pattern="##0.00" value="${tripDetails.vehicleDriverSalary}"/></td>
                                                                        <td class="<%=classText%>"  align="center"><fmt:formatNumber pattern="##0.00" value="${tripDetails.driverExpense}"/></td>
                                                                        <td class="<%=classText%>"  align="center"><fmt:formatNumber pattern="##0.00" value="${tripDetails.totlalOperationExpense}"/></td>--%>
                                            <c:set var="earnings" value="${tripDetails.revenue}" />
                                            <c:set var="expense" value="${tripDetails.requestedAdvance}" />
                                            <c:set var="profit" value="${earnings - expense}" />
                                            <td class="<%=classText%>"  align="right">
                                                <c:if test="${profit <= 0 && tripDetails.customerName != 'Empty Trip'}">
                                                    <font color="red"><fmt:formatNumber pattern="##0.00"  value="${profit}" /></font>
                                                </c:if>
                                                <c:if test="${profit gt 0}">
                                                    <font color="green"><fmt:formatNumber pattern="##0.00"  value="${profit}" /></font>
                                                </c:if></td>

                                            <c:set var="profitValue" value="0" />
                                            <c:set var="percentageprofit" value="0" />
                                            <c:if test="${earnings != '0.00' }">
                                                <c:set var="profitValue" value="${profit/earnings}" />
                                                <c:set var="percentageprofit" value="${profitValue*100}" />
                                            </c:if>
                                            <c:if test="${earnings == '0.00' }">
                                                <c:set var="profitValue" value="0" />
                                                <c:set var="percentageprofit" value="0" />
                                            </c:if>

                                            <td class="<%=classText%>"  align="right">
                                                <c:if test="${percentageprofit <= 0 && tripDetails.customerName != 'Empty Trip'}">
                                                    <font color="red"><fmt:formatNumber pattern="##0.00"  value="${percentageprofit}" />&nbsp;%</font>
                                                </c:if>
                                                <c:if test="${percentageprofit > 0}">
                                                    <font color="green"><fmt:formatNumber pattern="##0.00"  value="${percentageprofit}" />&nbsp;%</font>
                                                </c:if>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </c:if>


                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5" >5</option>
                                    <option value="10">10</option>
                                    <option value="20" selected="selected">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span><spring:message code="operations.reports.label.EntriesPerPage" text="default text"/></span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text"><spring:message code="operations.reports.label.DisplayingPage" text="default text"/> <span id="currentpage"></span> <spring:message code="operations.reports.label.of" text="default text"/> <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 0);
                        </script>
                        <c:if test="${tripDetailsSize != '0'}">
                            <table border="2" style="border: 1px solid #666666;"  align="center"  cellpadding="0" cellspacing="1" >
                                <c:set var="profitper" value="0" />
                                <c:set var="profitper" value="${totalEarnings - requestedAdvance}"/>
                                <tr height="25">
                                    <td style="background-color: #6374AB; color: #ffffff"><spring:message code="operations.reports.label.TotalTripsCarriedOut" text="default text"/></td>
                                    <td width="150"><c:out value="${totalTrips}"/></td>
                                </tr>
                                <tr height="25">
                                    <td style="background-color: #6374AB; color: #ffffff"><spring:message code="operations.reports.label.TotalIncome" text="default text"/></td>
                                    <td width="150"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalEarnings}" /></td>
                                </tr>
                                <tr height="25">
                                    <td style="background-color: #6374AB; color: #ffffff">Total Advance Request</td>
                                    <td width="150"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${requestedAdvance}" /> </td>
                                </tr>
                                <tr height="25">
                                    <td style="background-color: #6374AB; color: #ffffff"><spring:message code="operations.reports.label.TotalAdvancePaid" text="default text"/></td>
                                    <td width="150"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalBpclAllocated}" /> </td>
                                </tr>
                                <tr height="25">
                                    <td style="background-color: #6374AB; color: #ffffff"><spring:message code="operations.reports.label.Profit" text="default text"/> </td>
                                    <td width="150">
                                        <c:if test="${profitper gt 1}"><font color="green">
                                            <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalEarnings - requestedAdvance}" /></font>
                                        </c:if>
                                        <c:if test="${profitper lt 1}"><font color="red">
                                            <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalEarnings - requestedAdvance}" /></font>
                                        </c:if>
                                    </td>
                                </tr>

                                <tr height="25">
                                    <td style="background-color: #6374AB; color: #ffffff"><spring:message code="operations.reports.label.Profit" text="default text"/>  % <c:out value="${profitper}"/></td>
                                    <c:if test="${profitper gt 1}">
                                        <td width="150"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${((totalEarnings - requestedAdvance) * 100 )/ totalEarnings }" />% </font></td>
                                        </c:if>
                                        <c:if test="${profitper lt 1}">
                                        <td width="150"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${((totalEarnings - requestedAdvance) * 100 )/ totalEarnings }" />% </font></td>
                                        </c:if>
                                </tr>
                            </table>
                        </c:if>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>
