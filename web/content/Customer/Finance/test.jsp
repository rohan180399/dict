<html >
<head>

  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="images/favicon.png" type="image/png">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <title>Throttle - Leading Transport Management System</title>

  <link href="content/NewDesign/css/style.default.css" rel="stylesheet">
  <link rel="stylesheet" href="/throttle/css/jquery-ui.css">


  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->





</head>

<body>
<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<section>

  <div class="leftpanel">

    <div class="logopanel">
<!--        <h1><span>[</span> bracket <span>]</span></h1>-->
        <img src="/throttle/images/Throttle_Logo.png" alt="throttle Logo" width="210" height="30" />
    </div><!-- logopanel -->

    <div class="leftpanelinner">

        <!-- This is only visible to small devices -->
        <div class="visible-xs hidden-sm hidden-md hidden-lg">
            <div class="media userlogged">
                <img alt="" src="content/NewDesign/images/photos/loggeduser.png" class="media-object">
                <div class="media-body">
                    <h4>John Doe</h4>
                    <span>"Life is so..."</span>
                </div>
            </div>

            <h5 class="sidebartitle actitle">Account</h5>
            <ul class="nav nav-pills nav-stacked nav-bracket mb30">
              <li><a href="profile.html"><i class="fa fa-user"></i> <span>Profile</span></a></li>
              <li><a href=""><i class="fa fa-cog"></i> <span>Account Settings</span></a></li>
              <li><a href=""><i class="fa fa-question-circle"></i> <span>Help</span></a></li>
              <li><a href="signout.html"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
            </ul>
        </div>


      <h5 class="sidebartitle">Navigation</h5>
      <ul class="nav nav-pills nav-stacked nav-bracket">
        <li class="nav-parent active"><a href="index.html"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
            <ul class="children">
                <li><a href="/throttle/dashboardOperation.do"><i class="fa fa-cog"></i> Operations</a></li>
                <li><a href="/throttle/dashboardWorkshop.do"><i class="fa fa-cog"></i> Workshop</a></li>
              </ul>
        </li>

        <li class="nav-parent"><a href=""><i class="fa fa-user"></i> <span>HRMS</span></a>
          <ul class="children">
            <li><a href="/throttle/viewCompany1.do"><i class="fa fa-cog"></i> Company</a></li>
            <li><a href="/throttle/manageDepartment.do"><i class="fa fa-cog"></i> Department</a></li>
            <li><a href="/throttle/viewDesign.do"><i class="fa fa-cog"></i> Designation</a></li>
            <li><a href="/throttle/handleEmpViewPage.do"><i class="fa fa-cog"></i> Employees</a></li>
          </ul>
        </li>
        <li class="nav-parent"><a href=""><i class="fa fa-gears"></i> <span>Settings</span></a>
          <ul class="children">
            <li><a href="general-forms.html"><i class="fa fa-cog"></i> User</a></li>
            <li><a href="form-layouts.html"><i class="fa fa-cog"></i> Roles</a></li>
            <li><a href="form-validation.html"><i class="fa fa-cog"></i> Role Functions</a></li>
            <li><a href="form-wizards.html"><i class="fa fa-cog"></i> Change Password</a></li>
          </ul>
        </li>
        <li class="nav-parent"><a href="index.html"><i class="fa fa-bus"></i> <span>Trucks</span></a>
            <ul class="children">
                <li><a href="../charts/chartjs.html"><i class="fa fa-cog"></i> Axle Master</a></li>
                <li><a href="../charts/flot.html"><i class="fa fa-cog"></i> Vehicle Type</a></li>
                <li><a href="../charts/morris.html"><i class="fa fa-cog"></i> Make</a></li>
                <li><a href="../charts/inline.html"><i class="fa fa-cog"></i> Model</a></li>
                <li><a href="../charts/inline.html"><i class="fa fa-cog"></i> Veicles</a></li>
                <li><a href="../charts/inline.html"><i class="fa fa-cog"></i> Reset Vehicle KM</a></li>
                <li><a href="../charts/inline.html"><i class="fa fa-cog"></i> Vehicle Purchase </a></li>
                <li><a href="../charts/inline.html"><i class="fa fa-cog"></i> Vehicle Insurance</a></li>
                <li><a href="../charts/inline.html"><i class="fa fa-cog"></i> Vehicle Registration</a></li>
                <li><a href="../charts/inline.html"><i class="fa fa-cog"></i> Vehicle Inspection</a></li>
                <li><a href="../charts/inline.html"><i class="fa fa-cog"></i> Vehicle Road Permit</a></li>
              </ul>
        </li>
        <li class="nav-parent"><a href="index.html"><i class="fa fa-truck"></i> <span>Trailers</span></a>
            <ul class="children">
                <li><a href="../charts/flot.html"><i class="fa fa-cog"></i> Trailer Type</a></li>
                <li><a href="../charts/morris.html"><i class="fa fa-cog"></i> Make</a></li>
                <li><a href="../charts/inline.html"><i class="fa fa-cog"></i> Model</a></li>
                <li><a href="../charts/inline.html"><i class="fa fa-cog"></i> Trailers</a></li>
                <li><a href="../charts/inline.html"><i class="fa fa-cog"></i> Trailer Purchase </a></li>
              </ul>
        </li>
        <li class="nav-parent"><a href="index.html"><i class="fa fa-retweet"></i> <span>Accidents</span></a>
            <ul class="children">
                <li><a href="../charts/flot.html"><i class="fa fa-cog"></i> Accidents</a></li>
                <li><a href="../charts/morris.html"><i class="fa fa-cog"></i> Accident Details</a></li>
                <li><a href="../charts/inline.html"><i class="fa fa-cog"></i> Accident History</a></li>
              </ul>
        </li>
        <li class="nav-parent"><a href="index.html"><i class="fa fa-users"></i> <span>Vendors</span></a>
            <ul class="children">
                <li><a href="../UI/general.html"><i class="fa fa-cog"></i> Vendor Type</a></li>
                <li><a href="/throttle/manageVendorPage.do"><i class="fa fa-cog"></i> Vendors</a></li>
                <li><a href="/throttle/manageVendorItemConfigPage.do"><i class="fa fa-cog"></i> Inventory Vendor Contract </a></li>
                <li><a href="/throttle/manageFleetVendorPage.do"><i class="fa fa-cog"></i> Fleet Vendor Contract</a></li>
              </ul>
        </li>
        <li class="nav-parent"><a href="index.html"><i class="fa fa-angle-double-up"></i> <span>Sales</span></a>
            <ul class="children">
                <li><a href="../UI/general.html"><i class="fa fa-cog"></i> Quotation</a></li>
                <li><a href="/throttle/handleViewCustomer.do"><i class="fa fa-cog"></i> Customers</a></li>
                <li><a href="/throttle/handleViewAddCustomer.do"><i class="fa fa-cog"></i> Customers Add</a></li>
                <li><a href="#"><i class="fa fa-cog"></i> C Note Create</a></li>
                <li><a href="#"><i class="fa fa-cog"></i> C Note View</a></li>
              </ul>
        </li>
        <li class="nav-parent"><a href="index.html"><i class="fa fa-wrench"></i> <span>Service</span></a>
            <ul class="children">
                <li><a href="/throttle/createJobcard.do"><i class="fa fa-cog"></i> Create JobCard</a></li>
                <li><a href="/throttle/vehicleComplaintHist.do"><i class="fa fa-cog"></i>Veh Compliant History</a></li>
                <li><a href="/throttle/supervisorView.do"><i class="fa fa-cog"></i> View JobCard</a></li>
                <li><a href="/throttle/chooseJobCard.do"><i class="fa fa-cog"></i> Generate MRS</a></li>
                <li><a href="/throttle/closeJobCardView.do"><i class="fa fa-cog"></i> Close JobCard</a></li>
                <li><a href="/throttle/ViewJobCardsForBilling.do"><i class="fa fa-cog"></i> Generate Bill</a></li>
              </ul>
        </li>
        <li class="nav-parent"><a href="index.html"><i class="fa fa-building"></i> <span>Stores</span></a>
            <ul class="children">
                <li><a href="/throttle/manageRacks.do"><i class="fa fa-cog"></i> Rack</a></li>
                <li><a href="/throttle/manageSubRack.do"><i class="fa fa-cog"></i> Subrack</a></li>
                <li><a href="/throttle/manageParts.do"><i class="fa fa-cog"></i> Parts</a></li>
                <li><a href="/throttle/MRSApproval.do"><i class="fa fa-cog"></i> MRS Approval</a></li>
                <li><a href="/throttle/MRSList.do"><i class="fa fa-cog"></i> View/Issue MRS</a></li>
                <li><a href="/throttle/generateDirectMprPage.do"><i class="fa fa-cog"></i> Generate MPR</a></li>
                <li><a href="/throttle/mprApprovalList.do"><i class="fa fa-cog"></i> MPR Approval</a></li>
                <li><a href="/throttle/storesMprList.do"><i class="fa fa-cog"></i> MPR List</a></li>
                <li><a href="/throttle/modifyPO.do"><i class="fa fa-cog"></i> Modify PO</a></li>
                <li><a href="/throttle/requiredItems.do"><i class="fa fa-cog"></i> Required Items</a></li>
                <li><a href="/throttle/receiveInvoice.do"><i class="fa fa-cog"></i> Receive DC</a></li>
                <li><a href="/throttle/modifyGrnPage.do"><i class="fa fa-cog"></i> Receive DC Invoice</a></li>
              </ul>
        </li>
        <li class="nav-parent"><a href="index.html"><i class="fa fa-refresh"></i> <span>Operations</span></a>
            <ul class="children">

                <li><a href="index.html"><i class="fa fa-cog"></i> <span>Primary Operations</span></a>
                    <ul class="children">
                        <li><a href="/throttle/createJobcard.do"><i class="fa fa-cog"></i> Trip Planning</a></li>
                        <li><a href="#"><i class="fa fa-cog"></i> Vehicle Availability</a></li>
                        <li><a href="#"><i class="fa fa-cog"></i> Start Trip</a></li>
                        <li><a href="#"><i class="fa fa-cog"></i> Trip In Progress</a></li>
                      </ul>
                </li>

                <li><a href="/throttle/createJobcard.do"><i class="fa fa-cog"></i> Leasing Operations</a></li>

              </ul>
        </li>



      </ul>



    </div><!-- leftpanelinner -->
  </div><!-- leftpanel -->

  <div class="mainpanel">

    <div class="headerbar">

      <a class="menutoggle"><i class="fa fa-bars"></i></a>

<!--      <form class="searchform" action="index.html" method="post">
        <input type="text" class="form-control" name="keyword" placeholder="Search here..." />
      <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>-->


      <div class="header-right">
        <ul class="headermenu">

          <li>
            <div class="btn-group">
                <br>
                <span >
                    <a href="/throttle/languageChangeLoginRequest.do?paramName=en">english</a> &nbsp;|&nbsp;
                        <a href="/throttle/languageChangeLoginRequest.do?paramName=ar">???????</a>
                </span>

                <br>
            </div>
          </li>
          <li>
            <div class="btn-group">
              <button class="btn btn-default dropdown-toggle tp-icon" data-toggle="dropdown">
                <i class="glyphicon glyphicon-globe"></i>
                <span class="badge">5</span>
              </button>
              <div class="dropdown-menu dropdown-menu-head pull-right">
                <h5 class="title">You Have 5 New Notifications</h5>
                <ul class="dropdown-list gen-list">
                  <li class="new">
                    <a href="">
                    <span class="thumb"><img src="images/photos/user4.png" alt="" /></span>
                    <span class="desc">
                      <span class="name">Zaham Sindilmaca <span class="badge badge-success">new</span></span>
                      <span class="msg">is now following you</span>
                    </span>
                    </a>
                  </li>
                  <li class="new">
                    <a href="">
                    <span class="thumb"><img src="images/photos/user5.png" alt="" /></span>
                    <span class="desc">
                      <span class="name">Weno Carasbong <span class="badge badge-success">new</span></span>
                      <span class="msg">is now following you</span>
                    </span>
                    </a>
                  </li>
                  <li class="new">
                    <a href="">
                    <span class="thumb"><img src="images/photos/user3.png" alt="" /></span>
                    <span class="desc">
                      <span class="name">Veno Leongal <span class="badge badge-success">new</span></span>
                      <span class="msg">likes your recent status</span>
                    </span>
                    </a>
                  </li>
                  <li class="new">
                    <a href="">
                    <span class="thumb"><img src="images/photos/user3.png" alt="" /></span>
                    <span class="desc">
                      <span class="name">Nusja Nawancali <span class="badge badge-success">new</span></span>
                      <span class="msg">downloaded your work</span>
                    </span>
                    </a>
                  </li>
                  <li class="new">
                    <a href="">
                    <span class="thumb"><img src="images/photos/user3.png" alt="" /></span>
                    <span class="desc">
                      <span class="name">Nusja Nawancali <span class="badge badge-success">new</span></span>
                      <span class="msg">send you 2 messages</span>
                    </span>
                    </a>
                  </li>
                  <li class="new"><a href="">See All Notifications</a></li>
                </ul>
              </div>
            </div>
          </li>
          <li>
            <div class="btn-group">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                <img src="/throttle/content/NewDesign/images/photos/loggeduser.png" alt="" />
                Srinivasan R
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                <li><a href="profile.html"><i class="glyphicon glyphicon-user"></i> My Profile</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-cog"></i> Account Settings</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-question-sign"></i> Help</a></li>
                <li><a href="signin.html"><i class="glyphicon glyphicon-log-out"></i> Log Out</a></li>
              </ul>
            </div>
          </li>

        </ul>
      </div><!-- header-right -->

    </div><!-- headerbar -->











        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javasckript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>





             <link rel="stylesheet" href="/throttle/css/jquery-ui.css">
        <script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
        <script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>





<script type="text/javascript">


            $(document).ready(function() {

                $("#datepicker").datepicker({
                    showOn: "button",
                    format: "dd-mm-yyyy",
                    autoclose: true,
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function() {
                $(".datepicker").datepicker({
                    format: "dd-mm-yyyy",
                    autoclose: true
                });

            });


        </script>



    <SCRIPT>

        function openPopup(jobCardId, probId){
            var url = '/throttle/handleTechnicianPopup.do?jobCardId='+jobCardId+'&probId='+probId;
            window.open( url, 'PopupPage', 'height=500,width=600,scrollbars=yes,resizable=yes');
        }
        var httpRequest1;
        function getProblems(secId,sno) {

            if(secId!='null' ){

                var list1=document.getElementsByName("probId");
                while (list1[sno].childNodes[0]) {
                    list1[sno].removeChild(list1[sno].childNodes[0]);
                }

                var url='/throttle/getProblems.do?secId='+secId;

                if (window.ActiveXObject)


                    {
                        httpRequest1 = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)
                        {
                            httpRequest1 = new XMLHttpRequest();
                        }
                        httpRequest1.open("POST", url, true);
                        httpRequest1.onreadystatechange = function() {go(secId,sno); } ;
                        httpRequest1.send(null);
                    }
                }

                function go(secId,sno) {



                    if (httpRequest1.readyState == 4) {
                        if (httpRequest1.status == 200) {
                            var response = httpRequest1.responseText;
                            //alert(response);
                            //  response=0+'-'+'select'+response;
                            //alert(response);
                            var list=document.getElementsByName("probId");
                            var details=response.split(',');
                            for (i=1; i <details.length; i++) {
                                var temp = details[i].split('-');
                                var probId= temp[0];
                                var  probName = temp[1];
                                var  x=document.createElement('option');
                                var  name=document.createTextNode(probName);
                                x.appendChild(name);
                                x.setAttribute('value',probId+'-'+probName)
                                list[sno].appendChild(x);

                            }

                            if(secId==1016 || secId==1040) {

                                document.getElementById("technicianId"+sno).style.visibility = 'hidden';
                            }else{

                            document.getElementById("technicianId"+sno).style.visibility = 'visible';
                        }




                    }
                }


            }


            var rowCount=2;
            var sno=0;
            var index=0;
            var style="text1";
            var cntr = 0;

            function addRow(value,ind)
            {

                if(rowCount%2==0){
                    style="text2";
                }else{
                style="text1";
            }





            var tab = document.getElementById("addRows");
            var lastElement = tab.rows.length;
            var newrow = tab.insertRow(lastElement);

            if(index==0){
            index=ind;
            //sno=parseInt(lastElement)-2;
            if(value==0){
            sno++;
            }else{
            sno=parseInt(value)+1;
            }
            }else{
            index++;
            sno++;
            }


            var cell = newrow.insertCell(0);
            var cell0 = "<td height='25' ><input type='hidden' name='validate' value='0'> "+sno+"</td>";
            cell.setAttribute("className",style);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(1);
            var cell1 = "<td class='text1' height='25'><select class='form-control' id='secId"+index+"'  name='secId' style='width:100px;' onchange='getProblems(this.value,"+index+")'><option  selected value='0'>---Select ---</option><option  value='1091'>AC ENGINE<option  value='1069'>AIR BRAKE SYSTEM<option  value='1090'>AIR DOOR<option  value='1071'>AUDIO &amp; VIDEO<option  value='1100'>BATTERY<option  value='1040'>BODY WORK<option  value='1092'>BRAKE SYSTEM<option  value='1072'>CLUTCH<option  value='1016'>CONTRACTOR<option  value='1073'>COOLING SYSTEMS<option  value='1097'>DRIVER LEFT<option  value='1074'>ELECTRICAL<option  value='1075'>ENGINE<option  value='1076'>FRONT AXLE<option  value='1077'>FUEL FEED SYSTEM<option  value='1078'>GEAR BOX<option  value='1079'>GENERAL MAINTENANCE<option  value='1080'>HYDRAULIC BRAKE SYSTEM<option  value='1081'>INTAKE &amp; EXHAUST SYSTEM<option  value='1082'>JOINT<option  value='1083'>LUBRICATING SYSTEM<option  value='1096'>NON R &amp; M<option  value='1099'>PAINTING WORKS<option  value='1084'>REAR AXLE<option  value='1088'>RETARDER BRAKE (ELECTRICAL)<option  value='1089'>RETARDER BRAKE (HYDRAULIC)<option  value='1085'>STEERING<option  value='1086'>SUSPENSION SYSTEM<option  value='1093'>TAG AXLE (DEAD REAR AXLE)<option  value='1094'>TANK WORK<option  value='1098'>VEHICLE PASSING<option  value='1095'>WHEEL ALIGNMENT<option  value='1087'>WHEELS &amp; TYRES </select></td>";
            cell.setAttribute("className",style);
            cell.innerHTML = cell1;

            cell = newrow.insertCell(2);
            var cell2 = "<td class='text1' height='25'><select class='form-control' id='probId"+index+"' style='width:200px;'  name='probId'><option  selected value='0'>---Select ---</option></select></td>";
            cell.setAttribute("className",style);
            cell.innerHTML = cell2;

            cell = newrow.insertCell(3);
            var cell3 = "<td class='text1' height='25'><textarea  name='symptoms' style='width:120px;' onchange='setSelectbox("+index+")' ></textarea></td>";
            cell.setAttribute("className",style);
            cell.innerHTML = cell3;

            cell = newrow.insertCell(4);
            var cell4 = " <td class='text1'  height='25'><select name='severity' onchange='setSelectbox("+index+")'  class='text2'><option  selected value='0'>-Select -</option><option value='1'>Low</option><option value='2' >Medium</option><option value='3'>High</option></select></td>";
            cell.setAttribute("className",style);
            cell.innerHTML = cell4;

            cell = newrow.insertCell(5);
//            var cell5 = "<td class='text1' height='25'><select name='technicianId' id='technicianId"+index+"'   class='text2' style='width:80px;' onChange='setSelectbox("+index+");newWO1(this.value,"+index+",1);'><option value='0'>--select--</option><option  value='1201049'>TECHINICIAN ONE<option  value='1201051'>TECHINICIAN THREE<option  value='1201050'>TECHINICIAN TWO </select></td>";
            var cell5 = "<td class='text1' height='25'><input type='hidden' name='technicianId' value='1005'>&nbsp;</td>";
            cell.setAttribute("className",style);
            cell.innerHTML = cell5;



            cell = newrow.insertCell(6);
            var cell5 = "<td class='text1' height='25' width='30'><input type='text'  size='9'  onchange='setSelectbox("+index+")'   readonly name='scheduledDate' id='scheduledDate"+index+"'  class='form-control pull-right datepicker'> </td>";
            cell.setAttribute("className",style);
            cell.innerHTML = cell5;


            cell = newrow.insertCell(7);
            var cell7 = "<td class='text1' height='25'><select class='text2' onchange='setSelectbox("+index+")'  name='status' style='width:80px;'><option value='U'>default text</option><option value='P'>Planned</option><option value='C' > Completed</option></select></td>";
            cell.setAttribute("className",style);
            cell.innerHTML = cell7;

            cell = newrow.insertCell(8);
            cell7 = "<td class='text1' height='25'><textarea name='cremarks' style='width:127px;'  onkeyup='maxlength(this.value,300)' class='form-control'></textarea></td>";
            cell.setAttribute("className",style);
            cell.innerHTML = cell7;
            //var temp=rowCount-1;

            cell = newrow.insertCell(9);
            var cell7 = "<td class='text1' height='30'> <input type='checkbox'  name='selectedindex' value='"+index+"'>  </td>";
            cell.setAttribute("className",style);
            cell.innerHTML = cell7;



            //index++;
            rowCount++;
        }




/////////////////////////
            var rowCount1=1;
            var sno1=0;
            var index1=0;
            var styl="text1";
            var rowIndex = 0;
            function addNonGracePeriod()
         {
                    if(parseInt(rowCount1) %2==0)
                        {
                            styl="text2";
                        }else{
                        styl="text1";
                    }
                    sno1++;

                    //alert('test');
                    //if( parseInt(rowIndex) > 0 && document.getElementsByName("mfrCode")[rowIndex].value!= '')
                    var tab = document.getElementById("nonGracePeriodServices");
                    var newrow = tab.insertRow(rowCount1);

                    var cell = newrow.insertCell(0);
                    var cell0 = "<td class='text1' height='25' >  "+sno1+"</td>";
                    cell.setAttribute("className",styl);
                    cell.innerHTML = cell0;

                    var cell = newrow.insertCell(1);
                    var cell0 = "<td class='text1' height='25'><select class='form-control'   name='NonGraceServiceId' onchange='getDueKmHm("+rowIndex+");setSelectbox1("+rowIndex+");'><option  selected value='0'>---Select---</option> </select></td>";
                    cell.setAttribute("className",styl);
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(2);
                    var cell1 = "<td class='text1' height='25'><input name='NonGraceDueKm' readonly class='form-control' type='text'></td>";
                    cell.setAttribute("className",styl);
                    cell.innerHTML = cell1;

                    cell = newrow.insertCell(3);
                    var cell2 = "<td class='text1' height='25'><input name='NonGraceBalKm' readonly class='form-control'    type='text'></td>";
                    cell.setAttribute("className",styl);
                    cell.innerHTML = cell2;

                    cell = newrow.insertCell(4);
                    var cell3 = " <td class='text1' height='25'><input name='NonGraceDueHm' size='5' readonly class='form-control'  type='text'></td>";
                    cell.setAttribute("className",styl);
                    cell.innerHTML = cell3;

                    cell = newrow.insertCell(5);
                    var cell4 = " <td class='text1' height='25'><input name='NonGraceBalHm' readonly  size='5'  class='form-control'  type='text'></td>";
                    cell.setAttribute("className",styl);
                    cell.innerHTML = cell4;

                    cell = newrow.insertCell(6);
                    var cell5 = " <td class='text1' height='25'><input name='NonGraceDate'  size='10'  class='form-control pull-right datepicker' id='NonGraceDate"+rowCount1+"' value='' type='text'>  </td>";
                    cell.setAttribute("className",styl);
                    cell.innerHTML = cell5;

                    cell = newrow.insertCell(7);
                    var cell6 = " <td class='text1' height='25'><select name='NonGraceTech' class='text2' onChange='setSelectbox1("+rowIndex+");' style='width:80px;' ><option value='0'>--select--</option><option  value='1201049'>TECHINICIAN ONE<option  value='1201051'>TECHINICIAN THREE<option  value='1201050'>TECHINICIAN TWO </select></td>";
                    cell.setAttribute("className",styl);
                    cell.innerHTML = cell6;

                    cell = newrow.insertCell(8);
                    var cell1 = "<td class='text1' height='25'><select class='text2' name='nonGracePeriodStatus' onChange='setSelectbox1("+rowIndex+");' style='width:80px;'><option value='U'>UnPlanned</option><option value='P'>Planned</option><option value='C' > Completed</option></select><input type='checkbox' value='"+(rowCount1-1)+"' name='nonGraceSelectInd' > </td>";
                    cell.setAttribute("className","text2");
                    cell.innerHTML = cell1;

                    rowIndex++;
                    rowCount1++;
        }


var httpRequestHmKm;
function getDueKmHm(ind)
{
    var serviceId = document.getElementsByName('NonGraceServiceId');
    var vehicleId = document.workOrder.vehicleId.value;
    var km = document.workOrder.km.value;
    var hm = document.workOrder.hm.value;
    var url = '/throttle/getServiceHmKm.do?serviceId='+serviceId[ind].value+'&vehicleId='+vehicleId;
    url = url + '&km='+km+'&hm='+hm;

        if (window.ActiveXObject)
        {
        httpRequestHmKm = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else if (window.XMLHttpRequest)
        {
        httpRequestHmKm = new XMLHttpRequest();
        }
    httpRequestHmKm.open("GET", url, true);
    httpRequestHmKm.onreadystatechange = function() { processKmHm(ind); } ;
    httpRequestHmKm.send(null);
}


function processKmHm(ind)
{
    var dueKm = document.getElementsByName("NonGraceDueKm");
    var dueHm = document.getElementsByName("NonGraceDueHm");
    var balKm = document.getElementsByName("NonGraceBalKm");
    var balHm = document.getElementsByName("NonGraceBalHm");
    if (httpRequestHmKm.readyState == 4)
    {
    if(httpRequestHmKm.status == 200)
    {
        if(httpRequestHmKm.responseText.valueOf()!=""){
             var splt = httpRequestHmKm.responseText.valueOf().split('~');
             dueKm[ind].value = splt[0];
             dueHm[ind].value = splt[1];
             balKm[ind].value = splt[2];
             balHm[ind].value = splt[3];
        }else{
             dueKm[ind].value = '';
             dueHm[ind].value = '';
             balKm[ind].value = '';
             balHm[ind].value = '';
        }
    }
    else
    {
    alert("Error loading page\n"+ httpRequestHmKm.status +":"+ httpRequestHmKm.statusText);
    }
    }
}


function getWoDetail(woId){

window.open('/throttle/woDetail.do?woId='+woId, 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');


}




    </SCRIPT>


    <script type="text/javascript">


    function setFocus()
    {
        var date='08-06-2016'
        if(date!='null'){
            document.workOrder.compDate.value=date;
        }
        var jRemarks='-';
        var newjRemarks = "<table border='0'>";
        if(jRemarks !='null'){
            //document.workOrder.jremarks.value=jRemarks;
            var jr = jRemarks.split("@");
            for(var j=0;j<jr.length; j++){
                newjRemarks = newjRemarks + "<tr><td align='left' class='text2'>"+jr[j]+"</td></tr>";
            }
            newjRemarks=newjRemarks+"</table>";
            document.getElementById("jobCardRemarks").innerHTML=newjRemarks;
        }

    }

    </script>
   <div class="pageheader">
      <h2><i class="fa fa-edit"></i> View JobCard </h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You Are Here:</span>
        <ol class="breadcrumb">
          <li><a href="index.html">Home</a></li>
          <li><a href="general-forms.html">Service</a></li>
          <li class="active">View JobCard</li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">

    <body onload="setFocus();addNonGracePeriod();">
        <form name="workOrder" method="post">


            <!-- message table -->


<table  cellpadding="0" cellspacing="0" align="center" border="0" height="20">
<tr>

                </tr>
                <tr> <td>&nbsp; </td></tr>
                <tr>


</tr>
</table>





            <table class="table table-info mb30 table-hover">
                    <thead>

			<tr>
                             <th colspan="6" ><b>Job Card &nbsp;&nbsp; :&nbsp;&nbsp; 16061957 </b></th>


			</tr>
                    </thead>
                <tr>
                    <input type="hidden"  name="jobcardId"  value='1957'>
                    <td><b>Work Order No</b></td>
                    <input type="hidden" name="workOrderId" value='0'>
                    <input type="hidden" name="km" value='6200'>
                    <input type="hidden" name="hm" value='0'>
                    <input type="hidden" name="jcMYFormatNo" value='16061957'>

                    <td>0</td>
                    <td><b>Km</b></td>
                    <td>6200</td>
                    <td><b>Hour Meter</b></td>
                    <td>0</td>
                </tr>

                <input type="hidden" name="vehicleId" value='94'>
                <input type="hidden" name="reqDate" value='05-06-2016 4:00PM'>


                        <tr>
                            <td><b>Vehicle No</b></td>
                            <td>AXO0987</td>
                            <td><b>Committed Delivery</b></td>
                            <td>05-06-2016 4:00PM</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td><b>MFR</b></td>
                            <td>BENZ</td>
                            <td><b>Model</b></td>
                            <td>20FT</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>






                <tr>
                    <tr>
                        <td><b>Completion Schedule</b></td>
                        <td> <input name="compDate" type="text" class="form-control pull-right datepicker" value="">
<!--                        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.workOrder.compDate,'dd-mm-yyyy',this)"/>-->

                        </td>
                        <td colspan="4" align="left">&nbsp;</td>
                    </tr>
                </tr>
            </table>

            <td>
            <br>

            <table align="center" >









             <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns">
                        <a href="" class="panel-close">&times;</a>
                        <a href="" class="minimize">&minus;</a>
                    </div><!-- panel-btns -->
                    <h5 class="panel-title">Last 5 Problems</h5>
                </div>
            <div class="panel-body">
                    <table class="table table-info mb30 table-hover" >
                    <thead>

                        <tr>
                            <th  >SNo</th>
                            <th  >default text</th>
                            <th  >default text</th>
                             <th >Symptoms</th>
                        </tr>
                    </thead>


                            <tr>
                                <td >1</td>
                                <td>Accelerator cable cut</td>
                                <td>Accelerator cable cut</td>
                                   <td align="center">asfd</td>
                            </tr>



                            <tr>
                                <td >2</td>
                                <td>Gear box no power transmitted</td>
                                <td>Gear box no power transmitted</td>
                                   <td align="center">asdf</td>
                            </tr>



                            <tr>
                                <td >3</td>
                                <td>Clutch servo cylinder bracket mounting bolt cut</td>
                                <td>Clutch servo cylinder bracket mounting bolt cut</td>
                                   <td align="center">llk</td>
                            </tr>



                            <tr>
                                <td >4</td>
                                <td>Air door not working</td>
                                <td>Air door not working</td>
                                   <td align="center">sadfds</td>
                            </tr>



                            <tr>
                                <td >5</td>
                                <td>Clutch hard</td>
                                <td>Clutch hard</td>
                                   <td align="center">sdfasdf</td>
                            </tr>


                    </table>
            </div>
             </div>



            </table>


            <br>



                    <table class="table table-info mb30 table-hover">
                    <thead>



                        <tr>
                        <th colspan="10" align="center"><strong>Job Card Complaints </strong></th>
                        </tr>

                        <tr>
                            <th ><div >SNo</div></th>
                            <th ><div >Section </div></th>
                            <th ><div >Complaints </div></th>
                            <th ><div >Symptoms</div></th>
                            <th ><div >Severity</div></th>
                            <th ><div >Technician </div></th>
                            <th ><div >scheduled Date </div> </th>
                            <th ><div >Status</div></th>
                            <th ><div >Remarks</div></th>
                            <th colspan="6" ><div >Select </div></th>
                         </tr>
                    </thead>


                            <tr>
                                <input type='hidden' name='validate' value='1'>
                                <input name="probId" id="probId0" type="hidden" value='1288'>
                                <input name="secId" id="secId0" type="hidden" value='1071'>
                                <input name="secName" type="hidden" value='AUDIO &amp; VIDEO'>
                                <input name="probName" id="probName0" type="hidden" value='FM NOT WORKING'>
                                <input name="symptoms" type="hidden" value='CHECK'>
                                <input name="severity" type="hidden" value='3'>
                                <td>1</td>
                                <td>AUDIO &amp; VIDEO</td>

                                <td>FM NOT WORKING</td>
                                <td>CHECK</td>



                                    <td>High</td>



                                    <td>
                                            <input type="hidden" name="technicianId" value="1005" >

    <a href="" onclick="openPopup(1957,1288);" >Technicians</a>
                                    </td>


                                <td><input type="text"  size="10" id="scheduledDate0" name="scheduledDate" value='' onchange="setSelectbox(0)" class="form-control pull-right datepicker"></td>
                                <td ><select name="status" >

                                            <option selected value='U'>Not planned </option>
                                            <option  value='P'>Planned</option>
                                            <option  value='C' >Completed </option>



                                </select></td>
                                <td >
<textarea name="cremarks"  style="width:127px;"  onkeyup="maxlength(this.form.remark,300)" ></textarea>
                                </td>
                                <td ><input type="checkbox" name="selectedindex" value='0'></td>
                                <input type="hidden"  name="cause"  value="">
                                <input name="remark" type="hidden"  value="">


                            </tr>



                            <tr>
                                <input type='hidden' name='validate' value='1'>
                                <input name="probId" id="probId1" type="hidden" value='1257'>
                                <input name="secId" id="secId1" type="hidden" value='1074'>
                                <input name="secName" type="hidden" value='ELECTRICAL'>
                                <input name="probName" id="probName1" type="hidden" value='HEAD LIGHT NOT WORKING'>
                                <input name="symptoms" type="hidden" value='GONE FUSE'>
                                <input name="severity" type="hidden" value='3'>
                                <td>2</td>
                                <td>ELECTRICAL</td>

                                <td>HEAD LIGHT NOT WORKING</td>
                                <td>GONE FUSE</td>



                                    <td>High</td>



                                    <td>
                                            <input type="hidden" name="technicianId" value="1005" >

    <a href="" onclick="openPopup(1957,1257);" >Technicians</a>
                                    </td>


                                <td><input type="text"  size="10" id="scheduledDate1" name="scheduledDate" value='' onchange="setSelectbox(1)" class="form-control pull-right datepicker"></td>
                                <td ><select name="status" >

                                            <option selected value='U'>Not planned </option>
                                            <option  value='P'>Planned</option>
                                            <option  value='C' >Completed </option>



                                </select></td>
                                <td >
<textarea name="cremarks"  style="width:127px;"  onkeyup="maxlength(this.form.remark,300)" ></textarea>
                                </td>
                                <td ><input type="checkbox" name="selectedindex" value='1'></td>
                                <input type="hidden"  name="cause"  value="">
                                <input name="remark" type="hidden"  value="">


                            </tr>



                            <tr>
                                <input type='hidden' name='validate' value='1'>
                                <input name="probId" id="probId2" type="hidden" value='1031'>
                                <input name="secId" id="secId2" type="hidden" value='1075'>
                                <input name="secName" type="hidden" value='ENGINE'>
                                <input name="probName" id="probName2" type="hidden" value='LOSS OF POWER'>
                                <input name="symptoms" type="hidden" value='WHILE DRIVING OVER BRIDGE'>
                                <input name="severity" type="hidden" value='3'>
                                <td>3</td>
                                <td>ENGINE</td>

                                <td>LOSS OF POWER</td>
                                <td>WHILE DRIVING OVER BRIDGE</td>



                                    <td>High</td>



                                    <td>
                                            <input type="hidden" name="technicianId" value="1005" >

    <a href="" onclick="openPopup(1957,1031);" >Technicians</a>
                                    </td>


                                <td><input type="text"  size="10" id="scheduledDate2" name="scheduledDate" value='' onchange="setSelectbox(2)" class="form-control pull-right datepicker"></td>
                                <td ><select name="status" >

                                            <option selected value='U'>Not planned </option>
                                            <option  value='P'>Planned</option>
                                            <option  value='C' >Completed </option>



                                </select></td>
                                <td >
<textarea name="cremarks"  style="width:127px;"  onkeyup="maxlength(this.form.remark,300)" ></textarea>
                                </td>
                                <td ><input type="checkbox" name="selectedindex" value='2'></td>
                                <input type="hidden"  name="cause"  value="">
                                <input name="remark" type="hidden"  value="">


                            </tr>



                </table>

            <br>




            <table  align="center" id="addRows" border="0" cellpadding="0" cellspacing="0" width="100%" class="border" >
                <!--DWLayoutTable-->
                <tbody><tr>

                        <td colspan="10" align="center"><strong>Identified Complaints</strong></td>

                    </tr>




                        <tr>
                            <td   >SNo</td>
                            <td  >Section </td>
                            <td   >Problem </td>
                            <td   >Symptoms</td>
                            <td   >Severity</td>
                            <td   >Technician </td>
                            <td   >scheduled Date </td>
                            <td   >Status</td>
                            <td   >Remarks</td>
                            <td   >Select </td>


                        </tr>


                        <tr>



                        <tr>



                        <tr>







            </tbody></table>

            <br>

            <center>
                <input type="button" class="button" value="ADDROW" onClick="addRow('0',3)">

                <br>
                <br>
                <div class="text"><b>Job Card Remarks  </b></div>
                <div id="jobCardRemarks" align="center" class="text"></div>
                <textarea name="jremarks"  style="width:250px;height:60px;"  onkeyup="maxlength(this.form.remark,300)"></textarea>
                <br>
                <br>
                <input type="button" class="button" value="Save " onclick="submitPage()">
                <input type="button" class="button" value="Print" onclick="printPage()">

                <input type="hidden" name="buttonClick" value="">
            </center>


        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    <script type="text/javascript">
        var woCount=0
        function newWindow(jobcardId,workorderId){

            window.open('/throttle/previousJobCard.do?jobcardId='+jobcardId+"&workOrderId="+workorderId, 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
        }
        function newWO(){
            window.open('/throttle/content/RenderService/bodyParts.html', 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
            window.close();
        }
        function newWO1(tech,index,check){

            var jobcardId=document.workOrder.jobcardId.value;
            if(check==1){
                var temp=document.getElementById("probId"+index).value;
                var p=temp.split('-');
                var probId=p[0];
                var probName=p[1];
            }else{


            var probId=document.getElementById("probId"+index).value;
            var probName=document.getElementById("probName"+index).value;
        }

        if(tech==1 ){
            window.open('/throttle/externalTech.do?probId='+probId+"&jobcardId="+jobcardId+"&probName="+probName, 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
        }

        setSelectbox(index);
    }


    function nonGraceServiceValidate()
    {
        var serviceId = document.getElementsByName("NonGraceServiceId");
        var selectedInd = document.getElementsByName("nonGraceSelectInd");
        var serviceDate = document.getElementsByName("NonGraceDate");
        var tech = document.getElementsByName("NonGraceTech");

        for(var i=0;i<selectedInd.length;i++){
            if(selectedInd[i].checked==true ){
                if(serviceId[i].value=='0'){
                    alert("Please Select Service");
                    serviceId[i].focus();
                    return 'fail';
                }
                else if(serviceDate[i].value==''){
                    alert("Please Enter Schedule Date");
                    return 'fail';
                }
                else if(tech[i].value=='0'){
                    alert("Please Select Technician");
                    tech[i].focus();
                    return 'fail';
                }
            }
        }
        return 'pass';
    }


    function submitPage()
    {
        var x=0;
        var index = document.getElementsByName("selectedindex");
        var serIndex=document.getElementsByName("selectedServices");
        var nonGraceIndex = document.getElementsByName("nonGraceSelectInd");

        if(nonGraceServiceValidate() == 'fail'){
            return;
        }

        for(var i=0;(i<index.length && index.length!=0);i++){
            if(index[i].checked){
                x++;
            }
        }

        for(var i=0;(i<nonGraceIndex.length && nonGraceIndex.length!=0);i++){
            if(nonGraceIndex[i].checked){
                x++;
            }
        }

        for(var i=0;(i<serIndex.length && serIndex.length!=0);i++){
            if(serIndex[i].checked){
                x++;
            }
        }
        //var checValidate = selectedItemValidation();
        //    if(checValidate == 'SubmitForm'){
        if(x!=0){
            if(confirm("Have you entered Technician Effort Details")){
                document.workOrder.buttonClick.value="save";
                document.workOrder.action='/throttle/scheduleJobCard.do';
                document.workOrder.submit();
            }
        }else{
        alert("Please Select any One And then Proceed");
    }
    //}
}

function printPage(){
    document.workOrder.action='/throttle/printJobCard.do';
            document.workOrder.submit();
    }
function generateWO()
{
    var wo=0;
    var index = document.getElementsByName("selectedindex");

    var j=1;
    for(var i=0;(i<index.length && index.length!=0);i++){
        if(index[i].checked==true){
            var secId =document.getElementsByName("secId");
            if(secId[i].value=='1016' || secId[i].value == '1040'){
                wo++;
            }
            j++;
        }
    }
    if(wo!=0){
        document.workOrder.buttonClick.value="generateWO";
        document.workOrder.action='/throttle/scheduleJobCard.do';
        document.workOrder.submit();
    }else{
    alert("select Any One Body related problem");
}
}


function selectedItemValidation(){

    var index = document.getElementsByName("selectedindex");

    var chec=0;
    var mess = "SubmitForm";
    for(var i=0;(i<index.length && index.length!=0);i++){
        var secId =document.workOrder.secId[i];
        var probId =document.workOrder.probId[i];
        var symptoms =document.workOrder.symptoms[i];
        var severity =document.workOrder.severity[i];
        if(index[i].checked){
            chec++;

            if(isSelect(secId,'section')){
                return 'notSubmit';
            }else if(isSelect(probId,"Fault")){
            return 'notSubmit';
        }else if(textValidation(symptoms,"symptoms")){
        return 'notSubmit';
    }else if(isSelect(severity,"severity")){
    return 'notSubmit';
}
}
j++;
}
if(chec == 0){
    //alert("Please Select Any One And Then Proceed");
    //km[0].focus();
    return 'notSubmit';
}
return 'SubmitForm';
}

function getDetails(val,con)
{
    var x=0;

    if(con==1){



        document.getElementById("tech"+val).disabled=true;
        document.getElementById("serviceStatus"+val).disabled=true;
        document.getElementById("selectedServices"+val).checked=0;

        var serviceId=document.getElementById("serviceId"+val).value;
        var serviceName=document.getElementById("serviceName"+val).value;
        var vehicleId=document.workOrder.vehicleId.value;
        window.open('/throttle/serviceDone.do?serviceId='+serviceId+"&serviceName="+serviceName+"&vehicleId="+vehicleId, 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');


    }else
    {



        document.getElementById("servicesDone"+val).checked=0;
        document.getElementById("tech"+val).disabled=false;
        document.getElementById("serviceStatus"+val).disabled=false;


    }

}

function show(status,ind)
{
    if(status==2){
        document.getElementById("shows1").style.visibility="visible";
        document.getElementById("shows2").style.visibility="visible";
        document.getElementById("shows3").style.visibility="visible";
        document.getElementById("shows4").style.visibility="visible";
        document.getElementById("show"+ind).style.visibility="visible";
        document.getElementById("showRemark"+ind).style.visibility="visible";

    }else
    {
        document.getElementById("shows1").style.visibility="hidden";
        document.getElementById("shows2").style.visibility="hidden";
        document.getElementById("show"+ind).style.visibility="hidden";
        document.getElementById("showRemark"+ind).style.visibility="hidden";

    }
    setSelectbox(ind);
}


function setSchedule(i)
{
    var selected=document.getElementsByName("selectedServices") ;
    selected[i].checked = 1;
}
function setSelectbox(i)
{


    var selected=document.getElementsByName("selectedindex") ;

    selected[i].checked = 1;
}

function setSelectbox1(i)
{
    var selected=document.getElementsByName("nonGraceSelectInd") ;
    selected[i].checked = 1;
}
    </script>

        </div>


        </div>
        </div>




</div>
</section>


<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-migrate-1.2.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script src="content/NewDesign/js/bootstrap.min.js"></script>
<script src="content/NewDesign/js/modernizr.min.js"></script>
<script src="content/NewDesign/js/jquery.sparkline.min.js"></script>
<script src="content/NewDesign/js/toggles.min.js"></script>
<script src="content/NewDesign/js/retina.min.js"></script>
<script src="content/NewDesign/js/jquery.cookies.js"></script>

<script src="content/NewDesign/js/flot/jquery.flot.min.js"></script>
<script src="content/NewDesign/js/flot/jquery.flot.resize.min.js"></script>
<script src="content/NewDesign/js/flot/jquery.flot.spline.min.js"></script>
<script src="content/NewDesign/js/morris.min.js"></script>
<script src="content/NewDesign/js/raphael-2.1.0.min.js"></script>

<script src="content/NewDesign/js/custom.js"></script>
<script src="content/NewDesign/js/dashboard.js"></script>
<script type="text/javascript">


            $(document).ready(function() {

                $("#datepicker").datepicker({
                    showOn: "button",
                    format: "dd-mm-yyyy",
                    autoclose: true,
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function() {
                $(".datepicker").datepicker({
                    format: "dd-mm-yyyy",
                    autoclose: true
                });

            });


        </script>

</body>
</html>
 <script>
function addRows(val) {
                                    //alert(val);
                                    var loadCnt = val;
                                    //    alert(loadCnt)
                                    //alert("loadCnt");
                                    //var loadCnt1 = ve;
                                    var routeInfoSize = $('#routeDetails1' + loadCnt + ' tr').size();
                                    // alert(routeInfoSize);
                                    var routeInfoSizeSub = routeInfoSize - 2;
                                    var addRouteDetails = "addRouteDetailsFullTruck1" + loadCnt;
                                    var routeInfoDetails = "routeDetails1" + loadCnt;
                                    $('#routeDetails1' + loadCnt + ' tr').last().after('<tr><td>' + routeInfoSizeSub + '</td>\n\
                        <%--   <td><input type="text" name="vehicleTypeIddeDTemp" id="vehicleTypeIddeDTemp' + loadCnt + '" /></td>--%>\n\
                                   <td><select ype="text" name="vehicleTypeIdDedicate" id="vehicleTypeIdDedicate' + routeInfoSizeSub + '" onchange="onSelectVal(this.value,' + routeInfoSizeSub + ');"><option value="0">--<spring:message code="vendors.label.Select"  text="default text"/>--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                                   <td><input type="text" name="vehicleUnitsDedicate" id="vehicleUnitsDedicate' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                   <td><select ype="text" name="trailorTypeDedicate" id="trailorTypeDedicate' + routeInfoSizeSub + '"><option value="0">--<spring:message code="vendors.label.Select"  text="default text"/>--</option><c:if test="${trailerTypeList != null}"><c:forEach items="${trailerTypeList}" var="veh"><option value="<c:out value="${veh.trailerId}"/>"><c:out value="${veh.seatCapacity}"/></option></c:forEach></c:if></select></td>\n\
                                   <td><input type="text" name="trailorUnitsDedicate" id="trailorUnitsDedicate' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                    <td><select ype="text" name="contractCategory" id="contractCategory' + routeInfoSizeSub + '" onchange="setTextBoxEnable(this.value,' + routeInfoSizeSub + ');"><option value="">--<spring:message code="vendors.label.Select"  text="default text"/>--</option>\n\
                                     <option value="1" ><spring:message code="vendors.label.Fixed"  text="default text"/> </option>\n\
                                     <option value="2" ><spring:message code="vendors.label.Actual"  text="default text"/>  </option></select></td>\n\
                                   <td><input type="text" name="fixedCost" id="fixedCost' + routeInfoSizeSub + '" onchange="calculateTotalFixedCost(' + routeInfoSizeSub + ')"  value="0" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                                   <td width="10px;"><select ype="text" name="fixedHrs" id="fixedHrs' + routeInfoSizeSub + '" class="textbox">\n\
                                    <option value="00">00</option>\n\
                                    <option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option></select>\n\
                                   </td>\n\
                                    <td width="260px;"><select ype="text" name="fixedMin" id="fixedMin' + routeInfoSizeSub + '" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option></select>\n\
                                   </td>\n\
                                    <td><input type="text" name="totalFixedCost" id="totalFixedCost' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" readonly/></td>\n\
                                    <td><input type="text" name="rateCost" id="rateCost' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" readonly/></td>\n\
                                    <td><input type="text" name="rateLimit" id="rateLimit' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" readonly/></td>\n\
                                   <td><input type="text" name="maxAllowableKM" id="maxAllowableKM' + routeInfoSizeSub + '" value="0"  onKeyPress="return onKeyPressBlockCharacters(event);" readonly/></td>\n\
                                    <td><input type="text" name="workingDays" id="workingDays' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                    <td><input type="text" name="holidays" id="holidays' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                    <td><input type="text" name="addCostDedicate" id="addCostDedicate' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                  </tr>');
                                    // alert("loadCnt = "+loadCnt)
                                    loadCnt++;
                                    //   alert("loadCnt = +"+loadCnt)
                                }
                                
                                 var contain = "";
                                $(document).ready(function() {
                                    var iCnt = 1;
                                    var rowCnt = 1;
                                    // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                                    contain = $($("#routedeD")).css({
                                        padding: '5px', margin: '20px', width: '100%', border: '0px dashed',
                                        borderTopColor: '#999', borderBottomColor: '#999',
                                        borderLeftColor: '#999', borderRightColor: '#999'
                                    });
                                    $(contain).last().after('<table id="mainTableFullTruck" width="100%"><tr></td>\n\
               <table class="contenthead" style="border-spacing: 40px 10px;" cellspacing="10" id="routeDetails' + iCnt + '" border="1">\n\
                   <tr style="display:none"><td style="padding-left:10px;padding-right:10px;"><spring:message code="vendors.label.AgreedFuelPrice"  text="default text"/></td>\n\
                    <td style="margin-left:-200px;">\n\
                    <input type="text" width="20" name="agreedFuelPrice" id="agreedFuelPrice"  onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                       <td style="width:3px;">\n\
                          <select ype="text" name="uom" id="uom" style="width:85px;">\n\
                                      <option value="0">--<spring:message code="vendors.label.Select"  text="default text"/>--</option>\n\
                                      <option value="1"><spring:message code="vendors.label.Litre"  text="default text"/></option>\n\
                                      <option value="2"><spring:message code="vendors.label.Gallon"  text="default text"/></option>\n\
                                      <option value="3"><spring:message code="vendors.label.Kilogram"  text="default text"/></option>\n\
                                      </select></td>\n\
                                        <td style="padding-left:10px;padding-right:10px;">Fuel Hike % for Revising Cost</td>\n\
                    <td><input type="text" name="hikeFuelPrice" id="hikeFuelPrice"  onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                                  </tr></table><br><br>\n\
                                    <table  class="contenthead" id="routeDetails1' + iCnt + '"  border="1">\n\
                                    <tr>\n\
                                    <td><spring:message code="vendors.label.SNo"  text="default text"/></td>\n\
                                    <td><center><spring:message code="vendors.label.VehicleType"  text="default text"/></center></td>\n\
                                    <td><center><spring:message code="vendors.label.VehicleUnits"  text="default text"/></center></td>\n\
                                    <td><center><spring:message code="vendors.label.TrailerType"  text="default text"/></center></td>\n\
                                    <td><center><spring:message code="vendors.label.TrailerUnits"  text="default text"/></center></td>\n\
                                    <td><center><spring:message code="vendors.label.ContractCategory"  text="default text"/></center></td>\n\
                                   <td><center><spring:message code="vendors.label.FixedCostPerVehicle&Month"  text="default text"/></center></td>\n\
                                    <td colspan="2"><br><center><spring:message code="vendors.label.FixedDuration"  text="default text"/> <br><spring:message code="vendors.label.Perday"  text="default text"/></center><br>\n\
                                    <table class="contenthead" border="1">\n\
                                    <tr><td width="235px;"><center><spring:message code="vendors.label.Hours"  text="default text"/></center></td><td width="206px;"><center><spring:message code="vendors.label.Minutes"  text="default text"/></center></td></tr></table></td>\n\
                                    <td><center><spring:message code="vendors.label.TotalFixedCostPerMonth"  text="default text"/></center></td>\n\
                                    <td><center><spring:message code="vendors.label.RatePerKM"  text="default text"/></center></td>\n\
                                    <td><center><spring:message code="vendors.label.RateExceedsLimitKM"  text="default text"/></center></td>\n\
                                    <td><center><spring:message code="vendors.label.MaxAllowableKMPerMonth"  text="default text"/></center></td>\n\
                                    <td colspan="2"><br><br><center><spring:message code="vendors.label.OverTimeCostPerHR"  text="default text"/></center><br><br>\n\
                                    <table class="contenthead" border="1">\n\
                                    <tr><td width="245px;"><center><spring:message code="vendors.label.WorkDays"  text="default text"/></center></td><td width="216px;height="20px;"><center><spring:message code="vendors.label.Holidays"  text="default text"/></center></td></tr></table></td>\n\
                                    <td><center><spring:message code="vendors.label.AdditionalCost"  text="default text"/></center></td></tr>\n\
                                     <tr>\n\
                                   <td> ' + iCnt + ' </td>\n\
                                   <td><select type="text" name="vehicleTypeIdDedicate" id="vehicleTypeIdDedicate' + iCnt + '" onchange="onSelectVal(this.value,' + iCnt + ');"><option value="0">--<spring:message code="vendors.label.Select"  text="default text"/>--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                                   <td><input type="text" name="vehicleUnitsDedicate" id="vehicleUnitsDedicate' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                   <td><select ype="text" name="trailorTypeDedicate" id="trailorTypeDedicate' + iCnt + '"><option value="0">--<spring:message code="vendors.label.Select"  text="default text"/>--</option><c:if test="${trailerTypeList  != null}"><c:forEach items="${trailerTypeList }" var="trailer"><option value="<c:out value="${trailer.trailerId}"/>"><c:out value="${trailer.seatCapacity}"/></option></c:forEach></c:if></select></td>\n\
                                   <td><input type="text" name="trailorUnitsDedicate" id="trailorUnitsDedicate' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                    <td><select ype="text" name="contractCategory" id="contractCategory' + iCnt + '" onchange="setTextBoxEnable(this.value,' + iCnt + ')"><option value="0">--<spring:message code="vendors.label.Select"  text="default text"/>--</option>\n\
                                         <option value="1" ><spring:message code="vendors.label.Fixed"  text="default text"/>  </option>\n\
                                            <option value="2" ><spring:message code="vendors.label.Actual"  text="default text"/> </option></select></td>\n\
                                   <td><input type="text" name="fixedCost" id="fixedCost' + iCnt + '" value="0" onchange="calculateTotalFixedCost(' + iCnt + ')" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                                   <td width="10px;"><select ype="text" name="fixedHrs" id="fixedHrs' + iCnt + '" class="textbox">\n\
                                              <option value="00">00</option>\n\
                                            <option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option></select>\n\
                                   </td>\n\
                                    <td width="260px;"><select ype="text" name="fixedMin" id="fixedMin' + iCnt + '" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option></select>\n\
                                   </td>\n\
                                    <td><input type="text" name="totalFixedCost" id="totalFixedCost' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" readonly/></td>\n\
                                    <td><input type="text" name="rateCost" id="rateCost' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" readonly/></td>\n\
                                    <td><input type="text" name="rateLimit" id="rateLimit' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" readonly/></td>\n\
                                    <td><input type="text" name="maxAllowableKM" id="maxAllowableKM' + iCnt + '" value="0"  onKeyPress="return onKeyPressBlockCharacters(event);" readonly/></td>\n\
                                    <td><input type="text" name="workingDays" id="workingDays' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                    <td><input type="text" name="holidays" id="holidays' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                    <td><input type="text" name="addCostDedicate" id="addCostDedicate' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                     </tr>\n\
                                    </table>\n\
                                   <table border="" width=""><tr>\n\
                                    <td><input class="button" type="button" name="addRouteDetailsFullTruck1" id="addRouteDetailsFullTruck1' + iCnt + rowCnt + '" value="<spring:message code="vendors.label.Add"  text="default text"/>" onclick="addRows(' + iCnt + ')" />\n\
                                    <input class="button" type="button" name="removeRouteDetailsFullTruck1" id="removeRouteDetailsFullTruck1' + iCnt + rowCnt + '" value="<spring:message code="vendors.label.Remove"  text="default text"/>"  onclick="deleteRows(' + iCnt + ')" />\n\
                               </tr></table></td></tr></table><br><br>');
                        
                        var container = "";
                        $(document).ready(function() {
                            var iCnt = 1;
                            var rowCnt = 1;
                            // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                            container = $($("#routeFullTruck"))
//                                    .css({
//                                padding: '5px', margin: '20px', width: '170px', border: '0px dashed',
//                                borderTopColor: '#999', borderBottomColor: '#999',
//                                borderLeftColor: '#999', borderRightColor: '#999'
//                            });
                            $(container).last().after('<table id="mainTableFullTruck" width="100%"><tr></td>\
                            <table  class="contenthead" id="routeDetails' + iCnt + '"  border="1">\n\
                            <tr><td><spring:message code="vendors.label.SNo"  text="default text"/></td>\n\
                            <td><spring:message code="vendors.label.Origin"  text="default text"/></td>\n\
                            <td><spring:message code="vendors.label.Destination"  text="default text"/></td>\n\
                            <td><spring:message code="vendors.label.TravelKm"  text="default text"/></td>\n\
                            <td><spring:message code="vendors.label.TravelHour"  text="default text"/></td>\n\
                            <td><spring:message code="vendors.label.TravelMin"  text="default text"/></td></tr>\n\
                            <tr><td><spring:message code="vendors.label.Route"  text="default text"/>&nbsp; ' + iCnt + '</td>\n\
                            <td><input type="hidden" name="originIdFullTruck" id="originIdFullTruck' + iCnt + '" value="" />\n\
                            <input type="text" name="originNameFullTruck" id="originNameFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="iCnt" id="iCnt' + iCnt + '" value="' + iCnt + '"/></td>\n\
                            <td><input type="hidden" name="destinationIdFullTruck" id="destinationIdFullTruck' + iCnt + '" value="" />\n\
                            <input type="text" name="destinationNameFullTruck" id="destinationNameFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockNumbers(event);"/>\n\</td>\n\
                            <td><input type="text" name="travelKmFullTruck" id="travelKmFullTruck' + iCnt + '" value=""  onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            <td><input type="text" name="travelHourFullTruck" id="travelHourFullTruck' + iCnt + '" value=""  onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            <td><input type="text" name="travelMinuteFullTruck" id="travelMinuteFullTruck' + iCnt + '" value=""  onKeyPress="return onKeyPressBlockCharacters(event);"/> <input type="hidden" name="routeIdFullTruck" id="routeIdFullTruck' + iCnt + '" value="" /></td>\n\
                           </tr>\n\
                            </table>\n\
                            <table class="contentsub" id="routeInfoDetailsFullTruck' + iCnt + rowCnt + '" border="1">\n\
                            <tr><td><spring:message code="vendors.label.SNo"  text="default text"/></td><td><spring:message code="vendors.label.VehicleType"  text="default text"/></td><td><spring:message code="vendors.label.VehicleUnits"  text="default text"/></td><td><spring:message code="vendors.label.TrailerType"  text="default text"/></td><td><spring:message code="vendors.label.TrailerUnits"  text="default text"/></td><td><spring:message code="vendors.label.SpotCostPerTrip"  text="default text"/></td><td><spring:message code="vendors.label.AdditionalCost"  text="default text"/></td></tr>\n\
                            <tr>\n\
                            <td>' + rowCnt + '<input type="hidden" name="iCnt1" id="iCnt1' + iCnt + '" value="' + iCnt + '"/></td>\n\
                            <td><select ype="text" name="vehicleTypeId" id="vehicleTypeId' + iCnt + '"><option value="0">--<spring:message code="vendors.label.Select"  text="default text"/>--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                            <td><input type="text" name="vehicleUnits" id="vehicleUnits' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            <td><select type="text" name="trailerType" id="trailerType' + iCnt + '"><option value="0">--<spring:message code="vendors.label.Select"  text="default text"/>--</option><c:if test="${trailerTypeList != null}"><c:forEach items="${trailerTypeList}" var="trailer"><option value="<c:out value="${trailer.trailerId}"/>"><c:out value="${trailer.seatCapacity}"/></option></c:forEach></c:if></select></td>\n\
                            <td><input type="text" name="trailorTypeUnits" id="trailorTypeUnits' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            <td><input type="text" name="spotCost" id="spotCost' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            <td><input type="text" name="additionalCost" id="additionalCost' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            </tr></table>\n\
                            <table border="" width=><tr>\n\
                            <td><input class="button" type="button" name="addRouteDetailsFullTruck" id="addRouteDetailsFullTruck' + iCnt + rowCnt + '" value="<spring:message code="vendors.label.Add"  text="default text"/>" onclick="addRow(' + iCnt + rowCnt + ',' + rowCnt + ')" />\n\
                            <input class="button" type="button" name="removeRouteDetailsFullTruck" id="removeRouteDetailsFullTruck' + iCnt + rowCnt + '" value="<spring:message code="vendors.label.Remove"  text="default text"/>"  onclick="deleteRow(' + iCnt + rowCnt + ')" /></td>\n\
                           </tr></table></td></tr></table><br><br>');
                            callOriginAjaxFullTruck(iCnt);
                            callDestinationAjaxFullTruck(iCnt);
                            $('#btAdd').click(function() {
                                iCnt = iCnt + 1;
                                $(container).last().after('<table id="mainTableFullTruck" ><tr><td>\
                            <table  class="contenthead" id="routeDetailsFullTruck' + iCnt + '" border="1" width="100%">\n\
                            <tr><td><spring:message code="vendors.label.SNo"  text="default text"/></td>\n\
                            <td><spring:message code="vendors.label.Origin"  text="default text"/></td>\n\
                            <td><spring:message code="vendors.label.Destination"  text="default text"/></td>\n\
                            <td><spring:message code="vendors.label.TravelKm"  text="default text"/></td>\n\
                            <td><spring:message code="vendors.label.TravelHour"  text="default text"/></td>\n\
                            <td><spring:message code="vendors.label.TravelMin"  text="default text"/></td></tr>\n\
                            <td>' + iCnt + '</td>\n\
                            <td><input type="hidden" name="originIdFullTruck" id="originIdFullTruck' + iCnt + '" value="" /><input type="hidden" name="iCnt" id="iCnt' + iCnt + '" value="' + iCnt + '"/>\n\
                            <input type="text" name="originNameFullTruck" id="originNameFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockNumbers(event);"/></td>\n\
                            <td><input type="hidden" name="destinationIdFullTruck" id="destinationIdFullTruck' + iCnt + '" value="" />\n\
                            <input type="text" name="destinationNameFullTruck" id="destinationNameFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockNumbers(event);"/>\n\
                            </td>\n\
                             <td><input type="text" name="travelKmFullTruck" id="travelKmFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            <td><input type="text" name="travelHourFullTruck" id="travelHourFullTruck' + iCnt + '" value=""  onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            <td><input type="text" name="travelMinuteFullTruck" id="travelMinuteFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);"/>\n\
                            <input type="hidden" name="routeIdFullTruck" id="routeIdFullTruck' + iCnt + '" value="" /></td>\n\
                              </tr>\n\
                            </table>\n\
                            <table class="contentsub" id="routeInfoDetailsFullTruck' + iCnt + rowCnt + '" border="1" width="100%">\n\
                            <tr><td><spring:message code="vendors.label.SNo"  text="default text"/></td><td><spring:message code="vendors.label.VehicleType"  text="default text"/></td><td><spring:message code="vendors.label.VehicleUnits"  text="default text"/></td><td><spring:message code="vendors.label.TrailerType"  text="default text"/></td><td><spring:message code="vendors.label.TrailerUnits"  text="default text"/></td><td><spring:message code="vendors.label.SpotCostPerTrip"  text="default text"/></td><td><spring:message code="vendors.label.AdditionalCost"  text="default text"/></td></tr>\n\
                            <tr>\n\
                            <td>' + rowCnt + '<input type="hidden" name="iCnt1" id="iCnt1' + iCnt + '" value="' + iCnt + '"/></td>\n\
                            <td><select ype="text" name="vehicleTypeId" id="vehicleTypeId' + iCnt + '"><option value="0">--<spring:message code="vendors.label.Select"  text="default text"/>--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                            <td><input type="text" name="vehicleUnits" id="vehicleUnits' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            <td><select type="text" name="trailerType" id="trailerType' + iCnt + '"><option value="0">--<spring:message code="vendors.label.Select"  text="default text"/>--</option><c:if test="${trailerTypeList != null}"><c:forEach items="${trailerTypeList}" var="veh"><option value="<c:out value="${veh.trailerId}"/>"><c:out value="${veh.seatCapacity}"/></option></c:forEach></c:if></select></td>\n\
                            <td><input type="text" name="trailorTypeUnits" id="trailorTypeUnits' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            <td><input type="text" name="spotCost" id="spotCost' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            <td><input type="text" name="additionalCost" id="additionalCost' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            </tr></table>\n\
                            <table border="" width=""><tr>\n\
                            <td><input class="button"  type="button" name="addRouteDetailsFullTruck" id="addRouteDetailsFullTruck' + iCnt + rowCnt + '" value="<spring:message code="vendors.label.Add"  text="default text"/>" onclick="addRow(' + iCnt + rowCnt + ','+ iCnt +')" />\n\
                            <input class="button"  type="button" name="removeRouteDetailsFullTruck" id="removeRouteDetailsFullTruck' + iCnt + rowCnt + '" value="<spring:message code="vendors.label.Remove"  text="default text"/>" onclick="deleteRow(' + iCnt + rowCnt + ')"  /></td>\n\
                           </tr></table></td></tr></table><br><br>');
                                callOriginAjaxFullTruck(iCnt);
                                callDestinationAjaxFullTruck(iCnt);
                                $('#mainFullTruck').after(container);
                            });
                            $('#btRemove').click(function() {
//                                alert($('#mainTableFullTruck tr').size());
                                if ($(container).size() > 1) {
                                    $(container).last().remove();
                                    iCnt = iCnt - 1;
                                }
                            });
                        });
        </script>
         <input type="button" id="AddRow" value="AddRow" class="btn btn-success" onClick="fullTruckAddRow();" style="width:100px;height:35px;">&emsp;
                <input type="button" id="DeleteRow1" value="DeleteRow" class="btn btn-success" onClick="DeleteRow();" style="width:100px;height:35px;">&emsp;
                
          <script>
                        var container = "";
                        $(document).ready(function() {
                            var iCnt = 1;
                            var rowCnt = 1;
                            // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                            container = $($("#routeFullTruck"))
//                                    .css({
//                                padding: '5px', margin: '20px', width: '170px', border: '0px dashed',
//                                borderTopColor: '#999', borderBottomColor: '#999',
//                                borderLeftColor: '#999', borderRightColor: '#999'
//                            });
                            $(container).last().after('<table id="mainTableFullTruck" width="100%"><tr></td>\
                            <table  class="contenthead" id="routeDetails' + iCnt + '"  border="1">\n\
                            <tr><td><spring:message code="vendors.label.SNo"  text="default text"/></td>\n\
                            <td><spring:message code="vendors.label.Origin"  text="default text"/></td>\n\
                            <td><spring:message code="vendors.label.Destination"  text="default text"/></td>\n\
                            <td><spring:message code="vendors.label.TravelKm"  text="default text"/></td>\n\
                            <td><spring:message code="vendors.label.TravelHour"  text="default text"/></td>\n\
                            <td><spring:message code="vendors.label.TravelMin"  text="default text"/></td></tr>\n\
                            <tr><td><spring:message code="vendors.label.Route"  text="default text"/>&nbsp; ' + iCnt + '</td>\n\
                            <td><input type="hidden" name="originIdFullTruck" id="originIdFullTruck' + iCnt + '" value="" />\n\
                            <input type="text" name="originNameFullTruck" id="originNameFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="iCnt" id="iCnt' + iCnt + '" value="' + iCnt + '"/></td>\n\
                            <td><input type="hidden" name="destinationIdFullTruck" id="destinationIdFullTruck' + iCnt + '" value="" />\n\
                            <input type="text" name="destinationNameFullTruck" id="destinationNameFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockNumbers(event);"/>\n\</td>\n\
                            <td><input type="text" name="travelKmFullTruck" id="travelKmFullTruck' + iCnt + '" value=""  onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            <td><input type="text" name="travelHourFullTruck" id="travelHourFullTruck' + iCnt + '" value=""  onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            <td><input type="text" name="travelMinuteFullTruck" id="travelMinuteFullTruck' + iCnt + '" value=""  onKeyPress="return onKeyPressBlockCharacters(event);"/> <input type="hidden" name="routeIdFullTruck" id="routeIdFullTruck' + iCnt + '" value="" /></td>\n\
                           </tr>\n\
                            </table>\n\
                            <table class="contentsub" id="routeInfoDetailsFullTruck' + iCnt + rowCnt + '" border="1">\n\
                            <tr><td><spring:message code="vendors.label.SNo"  text="default text"/></td><td><spring:message code="vendors.label.VehicleType"  text="default text"/></td><td><spring:message code="vendors.label.VehicleUnits"  text="default text"/></td><td><spring:message code="vendors.label.TrailerType"  text="default text"/></td><td><spring:message code="vendors.label.TrailerUnits"  text="default text"/></td><td><spring:message code="vendors.label.SpotCostPerTrip"  text="default text"/></td><td><spring:message code="vendors.label.AdditionalCost"  text="default text"/></td></tr>\n\
                            <tr>\n\
                            <td>' + rowCnt + '<input type="hidden" name="iCnt1" id="iCnt1' + iCnt + '" value="' + iCnt + '"/></td>\n\
                            <td><select ype="text" name="vehicleTypeId" id="vehicleTypeId' + iCnt + '"><option value="0">--<spring:message code="vendors.label.Select"  text="default text"/>--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                            <td><input type="text" name="vehicleUnits" id="vehicleUnits' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            <td><select type="text" name="trailerType" id="trailerType' + iCnt + '"><option value="0">--<spring:message code="vendors.label.Select"  text="default text"/>--</option><c:if test="${trailerTypeList != null}"><c:forEach items="${trailerTypeList}" var="trailer"><option value="<c:out value="${trailer.trailerId}"/>"><c:out value="${trailer.seatCapacity}"/></option></c:forEach></c:if></select></td>\n\
                            <td><input type="text" name="trailorTypeUnits" id="trailorTypeUnits' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            <td><input type="text" name="spotCost" id="spotCost' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            <td><input type="text" name="additionalCost" id="additionalCost' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            </tr></table>\n\
                            <table border="" width=><tr>\n\
                            <td><input class="button" type="button" name="addRouteDetailsFullTruck" id="addRouteDetailsFullTruck' + iCnt + rowCnt + '" value="<spring:message code="vendors.label.Add"  text="default text"/>" onclick="addRow(' + iCnt + rowCnt + ',' + rowCnt + ')" />\n\
                            <input class="button" type="button" name="removeRouteDetailsFullTruck" id="removeRouteDetailsFullTruck' + iCnt + rowCnt + '" value="<spring:message code="vendors.label.Remove"  text="default text"/>"  onclick="deleteRow(' + iCnt + rowCnt + ')" /></td>\n\
                           </tr></table></td></tr></table><br><br>');
                            callOriginAjaxFullTruck(iCnt);
                            callDestinationAjaxFullTruck(iCnt);
                            $('#btAdd').click(function() {
                                iCnt = iCnt + 1;
                                $(container).last().after('<table id="mainTableFullTruck" ><tr><td>\
                            <table  class="contenthead" id="routeDetailsFullTruck' + iCnt + '" border="1" width="100%">\n\
                            <tr><td><spring:message code="vendors.label.SNo"  text="default text"/></td>\n\
                            <td><spring:message code="vendors.label.Origin"  text="default text"/></td>\n\
                            <td><spring:message code="vendors.label.Destination"  text="default text"/></td>\n\
                            <td><spring:message code="vendors.label.TravelKm"  text="default text"/></td>\n\
                            <td><spring:message code="vendors.label.TravelHour"  text="default text"/></td>\n\
                            <td><spring:message code="vendors.label.TravelMin"  text="default text"/></td></tr>\n\
                            <td>' + iCnt + '</td>\n\
                            <td><input type="hidden" name="originIdFullTruck" id="originIdFullTruck' + iCnt + '" value="" /><input type="hidden" name="iCnt" id="iCnt' + iCnt + '" value="' + iCnt + '"/>\n\
                            <input type="text" name="originNameFullTruck" id="originNameFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockNumbers(event);"/></td>\n\
                            <td><input type="hidden" name="destinationIdFullTruck" id="destinationIdFullTruck' + iCnt + '" value="" />\n\
                            <input type="text" name="destinationNameFullTruck" id="destinationNameFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockNumbers(event);"/>\n\
                            </td>\n\
                             <td><input type="text" name="travelKmFullTruck" id="travelKmFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            <td><input type="text" name="travelHourFullTruck" id="travelHourFullTruck' + iCnt + '" value=""  onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            <td><input type="text" name="travelMinuteFullTruck" id="travelMinuteFullTruck' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);"/>\n\
                            <input type="hidden" name="routeIdFullTruck" id="routeIdFullTruck' + iCnt + '" value="" /></td>\n\
                              </tr>\n\
                            </table>\n\
                            <table class="contentsub" id="routeInfoDetailsFullTruck' + iCnt + rowCnt + '" border="1" width="100%">\n\
                            <tr><td><spring:message code="vendors.label.SNo"  text="default text"/></td><td><spring:message code="vendors.label.VehicleType"  text="default text"/></td><td><spring:message code="vendors.label.VehicleUnits"  text="default text"/></td><td><spring:message code="vendors.label.TrailerType"  text="default text"/></td><td><spring:message code="vendors.label.TrailerUnits"  text="default text"/></td><td><spring:message code="vendors.label.SpotCostPerTrip"  text="default text"/></td><td><spring:message code="vendors.label.AdditionalCost"  text="default text"/></td></tr>\n\
                            <tr>\n\
                            <td>' + rowCnt + '<input type="hidden" name="iCnt1" id="iCnt1' + iCnt + '" value="' + iCnt + '"/></td>\n\
                            <td><select ype="text" name="vehicleTypeId" id="vehicleTypeId' + iCnt + '"><option value="0">--<spring:message code="vendors.label.Select"  text="default text"/>--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                            <td><input type="text" name="vehicleUnits" id="vehicleUnits' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            <td><select type="text" name="trailerType" id="trailerType' + iCnt + '"><option value="0">--<spring:message code="vendors.label.Select"  text="default text"/>--</option><c:if test="${trailerTypeList != null}"><c:forEach items="${trailerTypeList}" var="veh"><option value="<c:out value="${veh.trailerId}"/>"><c:out value="${veh.seatCapacity}"/></option></c:forEach></c:if></select></td>\n\
                            <td><input type="text" name="trailorTypeUnits" id="trailorTypeUnits' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            <td><input type="text" name="spotCost" id="spotCost' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            <td><input type="text" name="additionalCost" id="additionalCost' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            </tr></table>\n\
                            <table border="" width=""><tr>\n\
                            <td><input class="button"  type="button" name="addRouteDetailsFullTruck" id="addRouteDetailsFullTruck' + iCnt + rowCnt + '" value="<spring:message code="vendors.label.Add"  text="default text"/>" onclick="addRow(' + iCnt + rowCnt + ','+ iCnt +')" />\n\
                            <input class="button"  type="button" name="removeRouteDetailsFullTruck" id="removeRouteDetailsFullTruck' + iCnt + rowCnt + '" value="<spring:message code="vendors.label.Remove"  text="default text"/>" onclick="deleteRow(' + iCnt + rowCnt + ')"  /></td>\n\
                           </tr></table></td></tr></table><br><br>');
                                callOriginAjaxFullTruck(iCnt);
                                callDestinationAjaxFullTruck(iCnt);
                                $('#mainFullTruck').after(container);
                            });
                            $('#btRemove').click(function() {
//                                alert($('#mainTableFullTruck tr').size());
                                if ($(container).size() > 1) {
                                    $(container).last().remove();
                                    iCnt = iCnt - 1;
                                }
                            });
                        });

                        // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                        var divValue, values = '';
                        function GetTextValue() {
                            $(divValue).empty();
                            $(divValue).remove();
                            values = '';
                            $('.input').each(function() {
                                divValue = $(document.createElement('div')).css({
                                    padding: '5px', width: '200px'
                                });
                                values += this.value + '<br />'
                            });
                            $(divValue).append('<p><b>Your selected values</b></p>' + values);
                            $('body').append(divValue);
                        }

                        function addRow(val,v1) {
//                            alert(val);
//                            alert(v1);
                            var loadCnt = val;
                            var loadCnt1 = v1;
                            var routeInfoSize = $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').size();
                            //alert(routeInfoSize);
                            var addRouteDetails = "addRouteDetailsFullTruck" + loadCnt;
                            var routeInfoDetails = "routeInfoDetailsFullTruck" + loadCnt;
                            $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').last().after('<tr><td>' + routeInfoSize + '<input type="hidden" name="iCnt1" id="iCnt1' + loadCnt + '" value="' + loadCnt1 + '"/></td><td><select type="text" name="vehicleTypeId" id="vehicleTypeId' + loadCnt + '"><option value="0">--<spring:message code="vendors.label.Select"  text="default text"/>--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td><td><input type="text" name="vehicleUnits" id="vehicleUnits' + loadCnt + '" value="0"/></td><td><select type="text" name="trailerType" id="trailerType' + loadCnt + '"><option value="0">--<spring:message code="vendors.label.Select"  text="default text"/>--</option><c:if test="${trailerTypeList  != null}"><c:forEach items="${trailerTypeList}" var="veh"><option value="<c:out value="${veh.trailerId}"/>"><c:out value="${veh.seatCapacity}"/></option></c:forEach></c:if></select></td><td><input type="text" name="trailorTypeUnits" id="trailorTypeUnits' + loadCnt + '" value="0"/></td><td><input type="text" name="spotCost" id="spotCost' + loadCnt + '" value="0"/></td><td><input type="text" name="additionalCost" id="additionalCost' + loadCnt + '" value="0" /></td></tr>');
                            loadCnt++;
                        }
                        function deleteRow(val) {
                            var loadCnt = val;

                            var addRouteDetails = "addRouteDetailsFullTruck" + loadCnt;
                            var routeInfoDetails = "routeInfoDetailsFullTruck" + loadCnt;
                            if ($('#routeInfoDetailsFullTruck' + loadCnt + ' tr').size() > 2) {
                                $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').last().remove();
                                loadCnt = loadCnt - 1;
                            } else {
                                alert('One row should be present in table');
                            }
                        }
                            </script>

                            <script>
                                function callOriginAjaxFullTruck(val) {
                                    // Use the .autocomplete() method to compile the list based on input from user
                                    //alert(val);
                                    //       var pointNameId = 'originNameFullTruck' + val;
                                    var pointNameId = 'originNameFullTruck' + val;
                                    var pointId = 'originIdFullTruck' + val;
                                    var desPointName = 'destinationNameFullTruck' + val;


                                    //alert(prevPointId);
                                    $('#' + pointNameId).autocomplete({
                                        source: function(request, response) {
                                            $.ajax({
                                                url: "/throttle/getCityFromList.do",
                                                dataType: "json",
                                                data: {
//                                                    cityName: request.term,
                                                    cityName: $("#" + pointNameId).val(),
//                                                    cityName: $("#" + pointNameId).val(),
                                                    textBox: 1
                                                },
                                                success: function(data, textStatus, jqXHR) {
                                                    var items = data;
                                                    response(items);
                                                    //alert("asdfasdf")
                                                },
                                                error: function(data, type) {
                                                }
                                            });
                                        },
                                        minLength: 1,
                                        select: function(event, ui) {
                                            var value = ui.item.Value;
                                            //alert("value ="+value);
                                            var temp = value.split("-");
                                            var textId = temp[0];
                                            var textName = temp[1];
                                            var id = ui.item.Id;
                                            //alert("id ="+id);
                                            //alert(id+" : "+value);
                                            $('#' + pointId).val(textId);
                                            $('#' + pointNameId).val(textName);
                                            $('#' + desPointName).focus();
                                            //validateRoute(val,value);

                                            return false;
                                        }

                                        // Format the list menu output of the autocomplete
                                    }).data("autocomplete")._renderItem = function(ul, item) {
                                        //alert(item);
                                        //var temp [] = "";
                                        var itemVal = item.Value;
                                        var temp = itemVal.split("-");
                                        var textId = temp[0];
                                        var t2 = temp[1];
//                                        alert("t1 = "+t1)
//                                        alert("t2 = "+t2)
                                        //alert("itemVal = "+itemVal)
                                        t2 = '<font color="green">' + t2 + '</font>';
                                        return $("<li></li>")
                                                .data("item.autocomplete", item)
                                                .append("<a>" + t2 + "</a>")
                                                .appendTo(ul);
                                    };


                                }


//
//
//                                }
                                function callDestinationAjaxFullTruck(val) {
                                    // Use the .autocomplete() method to compile the list based on input from user
                                    //alert(val);
                                    var pointNameId = 'destinationNameFullTruck' + val;
                                    var pointIdId = 'destinationIdFullTruck' + val;
                                    var originPointId = 'originIdFullTruck' + val;
                                    var truckRouteId = 'routeIdFullTruck' + val;
                                    var travelKm = 'travelKmFullTruck' + val;
                                    var travelHour = 'travelHourFullTruck' + val;
                                    var travelMinute = 'travelMinuteFullTruck' + val;


//                                    if($("#" + originPointId).val() == ""){
//                                        alert("id inside condition ==")
//                                    }else{
//                                        alert("else inside condition ==")
//                                    }
                                    $('#' + pointNameId).autocomplete({
                                        source: function(request, response) {
                                            $.ajax({
                                                url: "/throttle/getCityToList.do",
                                                dataType: "json",
                                                data: {
                                                    cityName: $("#" + pointNameId).val(),
                                                    originCityId: $("#" + originPointId).val(),
                                                    textBox: 1
                                                },
                                                success: function(data, textStatus, jqXHR) {
                                                    var items = data;
                                                    response(items);
                                                },
                                                error: function(data, type) {

                                                    //console.log(type);
                                                }
                                            });
                                        },
                                        minLength: 1,
                                        select: function(event, ui) {
                                            var value = ui.item.cityName;
                                            var id = ui.item.cityId;
//                                            alert(id+" : "+value);
                                            $('#' + pointIdId).val(id);
                                            $('#' + pointNameId).val(value);
                                            $('#' + travelKm).val(ui.item.Distance);
                                            $('#' + travelHour).val(ui.item.TravelHour);
                                            $('#' + travelMinute).val(ui.item.TravelMinute);
                                            $('#' + truckRouteId).val(ui.item.RouteId);
                                            //validateRoute(val,value);

                                            return false;
                                        }

                                        // Format the list menu output of the autocomplete
                                    }).data("autocomplete")._renderItem = function(ul, item) {
                                        //alert(item);
                                        var itemVal = item.cityName;
                                        itemVal = '<font color="green">' + itemVal + '</font>';
                                        return $("<li></li>")
                                                .data("item.autocomplete", item)
                                                .append("<a>" + itemVal + "</a>")
                                                .appendTo(ul);
                                    };
                                }
                            </script>