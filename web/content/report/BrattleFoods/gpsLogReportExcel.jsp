<%-- 
    Document   : gpsLogReportExcel
    Created on : Dec 23, 2013, 3:59:10 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

        </style>
    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "GPSLogReport-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
          <c:if test="${logDetailsSize != '0'}">
                  <table align="center" border="1" id="table" class="sortable" width="100%" >
                    <thead>
                        <tr height="50">
                            <th align="center"><h3>S.No</h3></th>
                           <th align="center"><h3>Vehicle No</h3></th>
                           <th align="center"><h3>Trip Code</h3></th>
                           <th align="center"><h3>Trip Date</h3></th>
                           <th align="center"><h3>Current Location</h3></th>
                            <th align="center"><h3>Distance Travelled</h3></th>
                            <th align="center"><h3>Current Temperature</h3></th>
                            <th align="center"><h3>Log Date</h3></th>
                        </tr>
                    </thead>
                            <tbody>
                    <% int index = 1;%>
                            <c:forEach items="${logDetails}" var="logDetails">
                                    <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                                    %>
                                        <tr>
                                            <td class="<%=classText%>"><%=index++%></td>
                                            <td  width="90" class="<%=classText%>">
                                              <c:out value="${logDetails.vehicleNo}"/></td>
                                          <td width="90" class="<%=classText%>">
                                             <c:out value="${logDetails.tripCode}"/>
                                       </td>
                                            <td class="<%=classText%>" ><c:out value="${logDetails.tripDate}"/></td>
                                            <td height="50" class="<%=classText%>"  ><c:out value="${logDetails.location}"/></td>
                                            <td width="140" class="<%=classText%>"  ><c:out value="${logDetails.distance}"/></td>
                                            <td width="140" class="<%=classText%>"  ><c:out value="${logDetails.currentTemperature}"/></td>
                                            <td class="<%=classText%>"  ><c:out value="${logDetails.logDate}"/></td>
                                        </tr>

                            </c:forEach>
                </tbody>
                </table>
          </c:if>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
