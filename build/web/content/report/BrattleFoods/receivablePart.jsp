
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
        <style type="text/css">
            .container {width: 960px; margin: 0 auto; overflow: hidden;}
            .content {width:800px; margin:0 auto; padding-top:50px;}
            .contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

            /* STOP ANIMATION */

            /* Second Loadin Circle */

            .circle1 {
                background-color: rgba(0,0,0,0);
                border:5px solid rgba(100,183,229,0.9);
                opacity:.9;
                border-left:5px solid rgba(0,0,0,0);
                /*	border-right:5px solid rgba(0,0,0,0);*/
                border-radius:50px;
                /*box-shadow: 0 0 15px #2187e7; */
                /*	box-shadow: 0 0 15px blue;*/
                width:40px;
                height:40px;
                margin:0 auto;
                position:relative;
                top:-50px;
                -moz-animation:spinoffPulse 1s infinite linear;
                -webkit-animation:spinoffPulse 1s infinite linear;
                -ms-animation:spinoffPulse 1s infinite linear;
                -o-animation:spinoffPulse 1s infinite linear;
            }

            @-moz-keyframes spinoffPulse {
                0% { -moz-transform:rotate(0deg); }
                100% { -moz-transform:rotate(360deg);  }
            }
            @-webkit-keyframes spinoffPulse {
                0% { -webkit-transform:rotate(0deg); }
                100% { -webkit-transform:rotate(360deg);  }
            }
            @-ms-keyframes spinoffPulse {
                0% { -ms-transform:rotate(0deg); }
                100% { -ms-transform:rotate(360deg);  }
            }
            @-o-keyframes spinoffPulse {
                0% { -o-transform:rotate(0deg); }
                100% { -o-transform:rotate(360deg);  }
            }
        </style>
        <script>
            $(document).ready(function() {
                $('.ball, .ball1').removeClass('stop');
                $('.trigger').click(function() {
                    $('.ball, .ball1').toggleClass('stop');
                });
            });

        </script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>

        <script type="text/javascript">
            function submitPage(value) {
                //  alert(value);
//                if (document.getElementById('fromDate').value == '') {
//                    alert("Please select from Date");
//                    document.getElementById('fromDate').focus();
//                } else if (document.getElementById('toDate').value == '') {
//                    alert("Please select to Date");
//                    document.getElementById('toDate').focus();
//                } else {
//                    if (value == 'ExportExcel') {
//                        document.accountReceivable.action = '/throttle/handleReceivableReport.do?param=ExportExcel';
//                        document.accountReceivable.submit();
//                    } 
                    if (value == 'ExportExcel') {
                        document.accountReceivable.action = '/throttle/handleReceivableReport.do?param=ExportExcel';
                        document.accountReceivable.submit();
                    } 
                }
            
            function setValue() {
                if ('<%=request.getAttribute("page")%>' != 'null') {
                    var page = '<%=request.getAttribute("page")%>';
                    if (page == 1) {
                        submitPage('search');
                    }
                }
            }
//
//            function viewCustomerProfitDetails(tripIds) {
////            alert(tripIds);
//                window.open('/throttle/viewGrReportExpenseDetails.do?tripId=' + tripIds + "&param=Search", 'PopupPage', 'height = 500, width = 1150, scrollbars = yes, resizable = yes');
//            }
        </script>
     <div class="pageheader">
    <h2><i class="fa fa-edit"></i> Report</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Report</a></li>
            <li class="active">Receivable Report</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body>
        <!--<body>-->
        <form name="accountReceivable" action=""  method="post">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>
                             <table style="width:100%" align="center" border="0" class="tabouterborder" >
                                    <tr height="30"   ><td colSpan="6" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">Receivable Part Report</td></tr>
                                    <%--  <tr height="40">
                                           <td class="tabtext"><font color="red">*</font>From Date</td>
                                        <td class="tabtext" height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" style="width:180px;height:25px;color:black" ></td>
                                        <td class="tabtext"><font color="red">*</font>To Date</td>
                                        <td class="tabtext" height="30"><input name="toDate" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>" style="width:180px;height:25px;color:black"></td>
                                        <td class="tabtext" ><input type="button" class="btn btn-info" name="Search"  id="Search"  value="Search" onclick="submitPage(this.name);" style=" width:90px;height:27px;font-weight: bold;padding: 1px;"></td>
<!--                                    </tr>  --%>
                                    <tr>
                                        <td class="tabtext"  align="right"><center><input type="button" class="btn btn-info" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);" style=" width:90px;height:27px;font-weight: bold;padding: 1px;"></center>&nbsp;&nbsp;</td>
                                    </tr>
                                </table>  
           
<!--                <table>
                    <center>
                        <font size="4" color="Black"><b>GR Wise Profitability - TT</b></font>
                    </center>
                </table>-->
                <br>
                 <table  id="table" class="table table-info mb30 table-hover" style="width:100%;" >
                    <thead height="30">
                       
                        <tr >
                            <th >Particulars</th>
                            <th >Status</th>
                            <th >Gr status</th>
                            <th >Opening GR of the Day</th>
                            <th >GR Available for billing during the day</th>
                            <th >Invoice raised</th>
                            <th >Net GR Pending for Billing </th>
                            <th >Amount of invoices raised</th>
                            <th >Pending Revenue</th>
                            <th >Pending from < 7 Days</th>
                            
                            
                            
                        </tr>
                    </thead>
                    <% int index = 0, sno = 1;%>
                   
                        <c:set var="totOpenGR" value="${0}"/>    
                        <c:set var="totGRissued" value="${0}"/>    
                        <c:set var="totGRBilled" value="${0}"/>    
                        <c:set var="totNetGRPending" value="${0}"/>    
                        <c:set var="totInvAmtBillToParty" value="${0}"/>    
                        <c:set var="totInvAmtBillNotToParty" value="${0}"/>    
                        <c:set var="totPendingRevenueBillToParty" value="${0}"/>    
                        <c:set var="totPendingRevenueBillNotToParty" value="${0}"/>    
                        <c:set var="totpendingGRless7" value="${0}"/>    
                        

                            <tr  height="30">
                            <td align="center" class="text1" rowspan="10">GR Closed but not Billed</td>
                            <td align="center" class="text1" rowspan="4">Bill to Party</td>
                            <td align="center" class="text1">Loaded</td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-billing-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-billing-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-grBilled']}"/></td>
                             <c:set var="netGrPending1" value="${tripMap['own-load-billing-openGR'] + tripMap['own-load-billing-issueGR'] - tripMap['own-load-grBilled']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending1}"/></td>
                            <td align="center" class="text1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${tripMap['own-load-totInvAmt']}" /></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-pendingInvToParty']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-grlessthn7']}"/></td>
                            
                            </tr>
                            <tr  height="30">
                            
                            <td align="center" class="text1">Loaded-DSO</td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-billing-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-billing-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-grBilled']}"/></td>
                             <c:set var="netGrPending2" value="${tripMap['own-dso-billing-openGR'] + tripMap['own-dso-billing-issueGR'] - tripMap['own-dso-grBilled']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending2}"/></td>
                            <td align="center" class="text1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${tripMap['own-dso-totInvAmt']}" /></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-pendingInvToParty']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-grlessthn7']}"/></td>
                            
                            </tr>
                            </tr>
                            <tr  height="30">
                            
                            <td align="center" class="text1">Empty</td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-billing-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-billing-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-grBilled']}"/></td>
                             <c:set var="netGrPending3" value="${tripMap['own-empty-billing-openGR'] + tripMap['own-empty-billing-issueGR'] - tripMap['own-empty-grBilled']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending3}"/></td>
                            <td align="center" class="text1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${tripMap['own-empty-totInvAmt']}" /></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-pendingInvToParty']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-grlessthn7']}"/></td>
                            
                            </tr>
                           
                            <tr  height="30">
                            
                            <td align="center" class="text1">Back To Town</td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-billing-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-billing-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-grBilled']}"/></td>
                             <c:set var="netGrPending4" value="${tripMap['own-btt-billing-openGR'] + tripMap['own-btt-billing-issueGR'] - tripMap['own-btt-grBilled']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending4}"/></td>
                            <td align="center" class="text1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${tripMap['own-btt-totInvAmt']}" /></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-pendingInvToParty']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-grlessthn7']}"/></td>
                            
                            </tr>
                           
                           
                            
                            <tr>
                        <c:set var="totOpenGR" value="${tripMap['own-load-billing-openGR'] + tripMap['own-dso-billing-openGR'] + tripMap['own-empty-billing-openGR'] + tripMap['own-btt-billing-openGR']}"/>   
                        <c:set var="totGRissued" value="${tripMap['own-btt-load-issueGR'] + tripMap['own-dso-billing-issueGR'] + tripMap['own-empty-billing-issueGR'] + tripMap['own-btt-billing-issueGR']}"/>   
                        <c:set var="totGRBilled" value="${tripMap['own-load-grBilled'] + tripMap['own-dso-grBilled'] + tripMap['own-empty-grBilled'] + tripMap['own-btt-grBilled']}"/>       
                        <c:set var="totNetGRPending" value="${netGrPending1 + netGrPending2 + netGrPending3+ netGrPending4}"/>       
                        <c:set var="totInvAmtBillToParty" value="${tripMap['own-load-totInvAmt'] + tripMap['own-dso-totInvAmt'] + tripMap['own-empty-totInvAmt'] + tripMap['own-btt-totInvAmt']}"/>       
                        <c:set var="totPendingRevenueBillToParty" value="${tripMap['own-load-pendingInvToParty'] + tripMap['own-dso-pendingInvToParty'] + tripMap['own-empty-pendingInvToParty'] + tripMap['own-btt-pendingInvToParty']}"/>      
                        <c:set var="totpendingGRless7" value="${tripMap['own-empty-grlessthn7'] + tripMap['own-dso-grlessthn7'] + tripMap['own-load-grlessthn7'] + tripMap['own-btt-grlessthn7']}"/>      
                        
                                 
                            <td align="center" class="text1">Total</td>
                            <td align="center" class="text1"></td>
                            <td align="center" class="text1"><c:out value="${totOpenGR}"/></td>
                            <td align="center" class="text1"><c:out value="${totGRissued}"/></td>
                            <td align="center" class="text1"><c:out value="${totGRBilled}"/></td>
                            <td align="center" class="text1"><c:out value="${totNetGRPending}"/></td>
                            <td align="center" class="text1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totInvAmtBillToParty}" /></td>
                            <td align="center" class="text1"><c:out value="${totPendingRevenueBillToParty}"/></td>
                            <td align="center" class="text1"><c:out value="${totpendingGRless7}"/></td>
                            
                            </tr>
                            <tr>
                             <td align="center" class="text1" rowspan="4">Not to be billed to party</td>
                            <td align="center" class="text1">Loaded</td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-billing-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-billing-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-grBilled']}"/></td>
                             <c:set var="netGrPending5" value="${tripMap['own-load-billing-openGR'] + tripMap['own-load-billing-issueGR'] - tripMap['own-load-grBilled']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending5}"/></td>
                            <td align="center" class="text1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${tripMap['own-load-totInvAmtNotToParty']}" /></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-pendingInvNotToParty']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-load-grlessthn7']}"/></td>
                            
                            </tr>
                            
                            </tr>
                            <tr>
                             <td align="center" class="text1">Loaded DSO</td>
                          <td align="center" class="text1"><c:out value="${tripMap['own-dso-billing-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-billing-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-grBilled']}"/></td>
                             <c:set var="netGrPending6" value="${tripMap['own-dso-billing-openGR'] + tripMap['own-dso-billing-issueGR'] - tripMap['own-dso-grBilled']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending6}"/></td>
                            <td align="center" class="text1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${tripMap['own-dso-totInvAmtNotToParty']}" /></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-pendingInvNotToParty']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-dso-grlessthn7']}"/></td>
                            
                            </tr>
                            <tr>
                             <td align="center" class="text1">Empty</td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-billing-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-billing-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-grBilled']}"/></td>
                             <c:set var="netGrPending7" value="${tripMap['own-empty-billing-openGR'] + tripMap['own-empty-billing-issueGR'] - tripMap['own-empty-grBilled']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending7}"/></td>
                            <td align="center" class="text1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${tripMap['own-empty-totInvAmtNotToParty']}" /></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-pendingInvNotToParty']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-empty-grlessthn7']}"/></td>
                           
                            </tr>
                            <tr>
                             <td align="center" class="text1">Back To Town</td>
                           <td align="center" class="text1"><c:out value="${tripMap['own-btt-billing-openGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-billing-issueGR']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-grBilled']}"/></td>
                             <c:set var="netGrPending7" value="${tripMap['own-btt-billing-openGR'] + tripMap['own-btt-billing-issueGR'] - tripMap['own-btt-grBilled']}"/> 
                            <td align="center" class="text1"><c:out value="${netGrPending7}"/></td>
                            <td align="center" class="text1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${tripMap['own-btt-totInvAmtNotToParty']}" /></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-pendingInvNotToParty']}"/></td>
                            <td align="center" class="text1"><c:out value="${tripMap['own-btt-grlessthn7']}"/></td>
                           
                           
                            </tr>
                          <c:set var="totOpenGR2" value="${tripMap['own-load-billing-openGR'] + tripMap['own-dso-billing-openGR'] + tripMap['own-empty-billing-openGR'] + tripMap['own-btt-billing-openGR']}"/>   
                         <c:set var="totGRissued2" value="${tripMap['own-btt-load-issueGR'] + tripMap['own-dso-billing-issueGR'] + tripMap['own-empty-billing-issueGR'] + tripMap['own-btt-billing-issueGR']}"/>   
                        <c:set var="totGRBilled2" value="${tripMap['own-load-grBilled'] + tripMap['own-dso-grBilled'] + tripMap['own-empty-grBilled'] + tripMap['own-btt-grBilled']}"/>       
                        <c:set var="totNetGRPending2" value="${netGrPending5 + netGrPending6 + netGrPending7+ netGrPending8}"/>       
                       <c:set var="totInvAmtBillNotToParty" value="${tripMap['own-load-totInvAmtNotToParty'] + tripMap['own-dso-totInvAmtNotToParty'] + tripMap['own-empty-totInvAmtNotToParty'] + tripMap['own-btt-totInvAmtNotToParty']}"/>       
                        <c:set var="totPendingRevenueBillNotToParty" value="${tripMap['own-load-pendingInvNotToParty'] + tripMap['own-dso-pendingInvNotToParty'] + tripMap['own-empty-pendingInvNotToParty'] + tripMap['own-btt-pendingInvNotToParty']}"/>      
                        <c:set var="totpendingGRless7" value="${tripMap['own-empty-grlessthn7'] + tripMap['own-dso-grlessthn7'] + tripMap['own-load-grlessthn7'] + tripMap['own-btt-grlessthn7']}"/>      
                            <tr>
                            <td align="center" class="text1">Total</td>
                            <td align="center" class="text1"></td>
                            <td align="center" class="text1"><c:out value="${totOpenGR2}"/></td>
                            <td align="center" class="text1"><c:out value="${totGRissued2}"/></td>
                            <td align="center" class="text1"><c:out value="${totGRBilled2}"/></td>
                            <td align="center" class="text1"><c:out value="${totNetGRPending2}"/></td>
                            <td align="center" class="text1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totInvAmtBillNotToParty}" /></td>
                            <td align="center" class="text1"><c:out value="${totPendingRevenueBillNotToParty}"/></td>
                            <td align="center" class="text1"><c:out value="${totpendingGRless7}"/></td>
                           
                            </tr>
                            
                            <c:set var="grandTotOpenGr" value="${totOpenGR }"/>       
                            <c:set var="grandTotGRissued" value="${totGRissued }"/>       
                            <c:set var="grandTotGRBilled" value="${totGRBilled }"/>       
                            <c:set var="grandtotNetGRPending" value="${totNetGRPending }"/>       
                            <c:set var="grandtotInvAmtBill" value="${totInvAmtBillNotToParty + totInvAmtBillToParty }"/>       
                            <c:set var="grandtotPendingRevenue" value="${totPendingRevenueBillNotToParty + totPendingRevenueBillToParty }"/>       
                            <c:set var="grandtottotpendingGRless7" value="${totpendingGRless7 }"/>       
                            
                             <tr  height="30">
                            <td align="center" class="text1" >Grand Total</td>
                            <td align="center" class="text1" ></td>
                            <td align="center" class="text1"></td>
                            <td align="center" class="text1"><c:out value="${grandTotOpenGr}"/></td>
                            <td align="center" class="text1"><c:out value="${grandTotGRissued}"/></td>
                            <td align="center" class="text1"><c:out value="${grandTotGRBilled}"/></td>
                            <td align="center" class="text1"><c:out value="${grandtotNetGRPending}"/></td>
                            <td align="center" class="text1"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${grandtotInvAmtBill}" /></td>
                            <td align="center" class="text1"><c:out value="${grandtotPendingRevenue}"/></td>
                            <td align="center" class="text1"><c:out value="${grandtottotpendingGRless7}"/></td>
                          
                            </tr>
                       
                        <%
                            index++;
                            sno++;
                        %>
                   
                </table>
           
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>    
</div>
            </div>
        </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
