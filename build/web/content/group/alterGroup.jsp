<%-- 
    Document   : alterGroup
    Created on : Jun 22, 2010, 1:33:21 PM
    Author     : Hari
--%>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

    
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="ets.domain.racks.business.RackTO" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
</head>
<script>
    function submitPage()
    {
         var checValidate = selectedItemValidation();
    }
    function setSelectbox(i){
    var selected=document.getElementsByName("selectedIndex") ;
    selected[i].checked = 1;
}
function selectedItemValidation(){
var index = document.getElementsByName("selectedIndex");
var groupName = document.getElementsByName("groupNameList");
var groupDescription = document.getElementsByName("groupDescriptionList");
var activeInd = document.getElementsByName("groupStatusList");
var chec=0;
for(var i=0;(i<index.length && index.length!=0);i++){
if(index[i].checked){
chec++;
if(textValidation(groupName[i],'Group name')){
        return;
   }
 if(textValidation(groupDescription[i],'Group Description')){
        return;
   }
 if(textValidation(activeInd[i],'Rack Status')){
        return;
   }
   document.alterGroups.action='/throttle/handleAlterGroup.do';
   document.alterGroups.submit();
}
}
if(chec == 0){
alert("Please Select Any One And Then Proceed");
rackName[0].focus();
}
}
</script>


<script>
   function changePageLanguage(langSelection){
   if(langSelection== 'ar'){
   document.getElementById("pAlign").style.direction="rtl";
   }else if(langSelection== 'en'){
   document.getElementById("pAlign").style.direction="ltr";
   }
   }
 </script>

  <c:if test="${jcList != null}">
  <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
  </c:if>
      
  <span style="float: right">
	<a href="?paramName=en">English</a>
	|
	<a href="?paramName=ar">Arabic</a>
  </span>
        



<body>
<form name="alterGroups" method="post" >
     <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
<tr>
<td >
<%@ include file="/content/common/path.jsp" %>
</td></tr></table>
<!-- pointer table -->

<!-- message table -->
<table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
<tr>
<td >
<%@ include file="/content/common/message.jsp" %>
</td></tr></table>
<table width="524" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">

<tr>
<td width="57" height="30" class="contentsub"><div class="contentsub"><spring:message code="stores.label.GroupId"  text="default text"/>
</div></td>
<td width="144" height="30" class="contentsub"><div class="contentsub"><spring:message code="stores.label.GroupName"  text="default text"/>
</div></td>
<td width="194" height="30" class="contentsub"><div class="contentsub"><spring:message code="stores.label.GroupDescription"  text="default text"/>
</div></td>
<td width="92" height="30" class="contentsub"><div class="contentsub"><spring:message code="stores.label.Status"  text="default text"/>
</div></td>
<td width="37" height="30" class="contentsub"><div class="contentsub"><spring:message code="stores.label.Select"  text="default text"/>
</div></td>
</tr>
<% int index=0; %>
<c:if test = "${groupList != null}" >
 <c:forEach items="${groupList}" var="group">
  <%
    String classText = "";
    int oddEven = index % 2;
    if (oddEven > 0) {
    classText = "text2";
    } else {
    classText = "text1";
    }
    %>
<tr>
<td class="<%=classText %>" height="30"><input type="hidden" class="textbox" name="groupIdList" value="<c:out value='${group.groupId}'/>"><c:out value='${group.groupId}'/></td>
<td class="<%=classText %>" height="30"><input type="text" class="textbox" name="groupNameList" value="<c:out value='${group.groupName}'/>" onchange="setSelectbox('<%= index %>');"></td>
<td class="<%=classText %>" height="30"><textarea class="textbox" name="groupDescriptionList" onchange="setSelectbox('<%= index %>');" > <c:out value='${group.description}'/></textarea></td>
<td class="<%=classText %>" height="30"><select class="textbox" name="groupStatusList" onchange="setSelectbox('<%= index %>');">
<c:if test="${(group.activeInd=='n') || (group.activeInd=='N')}" >
 <option value="Y" ><spring:message code="stores.label.Active"  text="default text"/></option><option value="N" selected><spring:message code="stores.label.InActive"  text="default text"/></option>
 </c:if>
<c:if test="${(group.activeInd=='y') || (group.activeInd=='Y')}" >
 <option value="Y" selected><spring:message code="stores.label.Active"  text="default text"/></option><option value="N"><spring:message code="stores.label.InActive"  text="default text"/></option>
</c:if>
</select></td>
<td width="37" height="30" class="<%=classText %>"><input type="checkbox" name="selectedIndex" value='<%= index %>'></td>
</tr>
 <%
    index++;
  %>
</c:forEach>
  </c:if>
</tr>
</table>
<br>
<center>
<input type="button" value="<spring:message code="stores.label.Save"  text="default text"/>
" class="button" onclick="submitPage();">
</center>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>

