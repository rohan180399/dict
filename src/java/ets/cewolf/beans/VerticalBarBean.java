package ets.cewolf.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import de.laures.cewolf.DatasetProduceException;
import de.laures.cewolf.DatasetProducer;
import de.laures.cewolf.links.CategoryItemLinkGenerator;
import de.laures.cewolf.tooltips.CategoryToolTipGenerator;

public class VerticalBarBean implements DatasetProducer,
		CategoryToolTipGenerator, CategoryItemLinkGenerator, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private static final Log log = LogFactory.getLog(VerticalBarBean.class);


/* Both  String arrays seriesNames and seriesToolTip are of fixed size and can accomidate from 0-24 entries
 * Since the methods generateLink and generateToolTip have return type as String[]*/
	private String[] seriesNames = new String[25];
	private String[] seriesToolTip = new String[25];

	/**
	 * Produces the dataset required to generate the graph.
	 */
	public Object produceDataset(Map params) throws DatasetProduceException
	 {
		  // Dataset can either be populated by hard-coded or else from a datasource
		DefaultCategoryDataset dataset = new DefaultCategoryDataset()
		{

			/**
			 *
			 */
			private static final long serialVersionUID = 1L;

			protected void finalize() throws Throwable {
				super.finalize();
				log.debug(this + " finalized.");
			}
		};

		   dataset.addValue(10,"raja","raja1");
		   dataset.addValue(15, "C++","C++");
		   dataset.addValue(30, "Java","Java");
		   dataset.addValue(15, ".NET",".Net");
		   dataset.addValue(10, "Perl","Perl");
		   dataset.addValue(15, "Python","Python");


				/*assinging values to a new Array series Tool Tip used to generate toool tip*/
		   seriesToolTip[1]="C";
		   seriesToolTip[2]="C++";
		   seriesToolTip[3]="Java";
		   seriesToolTip[4]=".NET";
		   seriesToolTip[5]="Perl";
		   seriesToolTip[6]="Python";

		   seriesNames[1]="C";
		   seriesNames[2]="C++";
		   seriesNames[3]="Java";
		   seriesNames[4]=".NET";
		   seriesNames[5]="Perl";
		   seriesNames[6]="Python";
		 return dataset;

	}

	/**
	 * This producer's data is invalidated after 5 seconds. By this method the
	 * producer can influence Cewolf's caching behaviour the way it wants to.
	 */
	public boolean hasExpired(Map params, Date since) {
		log.debug(getClass().getName() + "hasExpired()");
		return (System.currentTimeMillis() - since.getTime()) > 5000;
	}

	/**
	 * Returns a unique ID for this DatasetProducer
	 */
	public String getProducerId() {
		return "PageViewCountData DatasetProducer";
	}

	/**
	 * Returns a link target for a special data item.
	 */

	public String generateLink(Object data, int series, Object category) {

		return seriesNames[series];
		// return category.toString();
	}

	/**
	 * @see java.lang.Object#finalize()
	 */
	protected void finalize() throws Throwable {
		super.finalize();
		log.debug(this + " finalized.");
	}

	/**
	 * @see org.jfree.chart.tooltips.CategoryToolTipGenerator#generateToolTip(CategoryDataset,
	 *      int, int)
	 */
	public String generateToolTip(CategoryDataset arg0, int series, int arg2) {
		return seriesToolTip[series];
	}


}