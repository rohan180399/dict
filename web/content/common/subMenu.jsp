<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-EN">
    <head>
        <%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
        <%@page import="java.util.Locale"%>
        <script src="/throttle/js/jquery-1.4.2.min.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/menu.css" />

        <!--[if lt IE 8]>
   <style type="text/css">
   li a {display:inline-block;}
   li a {display:block;}
   </style>
   <![endif]-->
        <style>
            .high{
                background: #ff0000;
            }
        </style>
        <script type="text/javascript">
            $.noConflict();

            var tempObj;
            (function ($) {
                jQuery.fn.initMenu = function () {
                    return this.each(function () {
                        var theMenu = $(this).get(0);
                        $('.acitem', this).hide();
                        $('li.expand > .acitem', this).show();
                        $('li.expand > .acitem', this).prev().addClass('active');
                        $('li a', this).click(
                                function (e) {
                                    if (tempObj != null) {
                                        tempObj.removeClass('high');
                                    }
                                    tempObj = $(this);
                                    e.stopImmediatePropagation();
                                    var theElement = $(this).next();
                                    $(this).prev().removeClass('active');

                                    var parent = this.parentNode.parentNode;
                                    if ($(parent).hasClass('noaccordion')) {
                                        if (theElement[0] === undefined) {
                                            window.location.href = this.href;
                                        }
                                        $(theElement).slideToggle('normal', function () {
                                            if ($(this).is(':visible')) {
                                                $(this).prev().addClass('active');
                                            }
                                            else {
                                                $(this).prev().removeClass('active');
                                            }
                                        });
                                        return false;
                                    }
                                    else {
                                        if (theElement.hasClass('acitem') && theElement.is(':visible')) {
                                            if ($(parent).hasClass('collapsible')) {
                                                $('.acitem:visible', parent).first().slideUp('normal',
                                                        function () {
                                                            $(this).prev().removeClass('active');
                                                        }
                                                );

                                                return false;
                                            }

                                            return false;
                                        }
                                        if (theElement.hasClass('acitem') && !theElement.is(':visible')) {
                                            $('.acitem:visible', parent).first().slideUp('normal', function () {
                                                $(this).prev().removeClass('active');
                                            });
                                            theElement.slideDown('normal', function () {
                                                $(this).prev().addClass('active');
                                            });
                                            return false;
                                        }
                                    }
                                    $(this).addClass('high');

                                }
                        );
                    });
                };

                $(document).ready(function () {
                    $('.menu').initMenu();
                });


            })(jQuery);





            function  curSelected() {
                document.getElementById('pivot').className = 'current';
            }




        </script>
    </head>
    <body>
        <% Object rolId = session.getAttribute("RoleId");%>
        <div id="hrmsM" style="display: none;">
            <table id="hrmsMenu" style="display: block;" cellpadding="0" cellspacing="0" align="center" border="0">
                <tr>
                    <td>
                        <c:if test = "${menu.hrms != null}" >
                            <ul class="menu">
                                <c:forEach items="${menu.hrms}" var="hrms">
                                    <c:if test = "${hrms.subMenu =='Company'}">
                                        <li><a href="/throttle/viewCompany1.do" target="frame">&nbsp;Company</a></li>
                                    </c:if>
                                    <c:if test = "${hrms.subMenu =='Department'}">
                                        <li><a href="/throttle/manageDepartment.do" target="frame">&nbsp;Department</a></li>
                                    </c:if>
                                    <c:if test = "${hrms.subMenu =='Designation'}">
                                        <li><a href="/throttle/viewDesign.do" target="frame">&nbsp;Designation</a></li>
                                    </c:if>
                                    <c:if test = "${hrms.subMenu =='Employee'}">
                                        <li><a href="/throttle/handleEmpViewPage.do" target="frame">&nbsp;Manage Employee</a></li>
                                    </c:if>
                                </c:forEach>
                            </ul>
                        </c:if>
                    </td>
                </tr>
            </table>
        </div>

        <div id="settingsM" style="display: none;">
            <table id="settingsMenu"  cellpadding="0" cellspacing="0" align="center" border="0">
                <tr>
                    <td>

                        <c:if test="${menu.users!=null}">
                            <ul class="menu">
                                <c:forEach items="${menu.users}" var="users">

                                    <%if (rolId.equals(1030) || rolId.equals(1033) || rolId.equals(1032) || rolId.equals(1031) || rolId.equals(1036) || rolId.equals(1035) || rolId.equals(1037) || rolId.equals(1040) || rolId.equals(1041) || rolId.equals(1044)) {%>
                                    <c:if test = "${hrms.subMenu =='RecoverPassword'}">
                                        <li><a href="/throttle/alterPassword.do" target="frame">&nbsp;Change Password</a></li>
                                    </c:if>
                                    <% } else if (rolId.equals(1023)) {%>
                                    <c:if test = "${hrms.subMenu =='ManagePassword'}">
                                        <li><a href="/throttle/recoverPassword.do" target="frame">&nbsp;Manage Password</a></li>
                                    </c:if>
                                    <c:if test = "${hrms.subMenu =='RecoverPassword'}">
                                        <li><a href="/throttle/alterPassword.do" target="frame">&nbsp;Change Password</a></li>
                                    </c:if>
                                    <c:if test = "${users.subMenu =='ManageUser'}">
                                        <li><a href="/throttle/viewuser.do" target="frame">&nbsp;Manage Users</a></li>
                                    </c:if>
                                    <c:if test = "${hrms.subMenu =='ManageRole'}">
                                        <li><a href="/throttle/allroles.do" target="frame">&nbsp;Manage Roles</a></li>
                                    </c:if>

                                    <c:if test = "${hrms.subMenu =='ManageUserRole'}">
                                        <li><a href="/throttle/userroles.do" target="frame">&nbsp;Manage User Roles</a></li>
                                    </c:if>
                                    <c:if test = "${hrms.subMenu =='ManageRoleFunction'}">
                                        <li><a href="/throttle/activerole.do" target="frame">&nbsp;Manage Role Functions</a></li>
                                    </c:if>
                                    <% } else {%>
                                    <c:if test = "${users.subMenu =='ManageUser'}">
                                        <li><a href="/throttle/viewuser.do" target="frame">&nbsp;Manage Users</a></li>
                                    </c:if>
                                    <c:if test = "${hrms.subMenu =='ManageRole'}">
                                        <li><a href="/throttle/allroles.do" target="frame">&nbsp;Manage Roles</a></li>
                                    </c:if>

                                    <c:if test = "${hrms.subMenu =='ManageUserRole'}">
                                        <li><a href="/throttle/userroles.do" target="frame">&nbsp;Manage User Roles</a></li>
                                    </c:if>
                                    <c:if test = "${hrms.subMenu =='ManageRoleFunction'}">
                                        <li><a href="/throttle/activerole.do" target="frame">&nbsp;Manage Role Functions</a></li>
                                    </c:if>
                                    <% }%>
                                </c:forEach>
                            </ul>
                        </c:if>
                    </td>
                </tr>
            </table>
        </div>

        <div id="vehicleM" style="display: none;">
            <table id="vehicleMenu" style="display: block;" cellpadding="0" cellspacing="0" align="center" border="0">
                <tr>
                    <td>
                        <ul class="menu">
                            <c:if test = "${menu.vehicles != null}">
                                <c:forEach items="${menu.vehicles}" var="vehicles">
                                    <%if (rolId.equals(1030)) {%>
                                    <%} else if (rolId.equals(1032)) {%>
                                    <%} else if (rolId.equals(1032)) {%>
                                    <%} else if (rolId.equals(1031)) {%>
                                    <%} else if (rolId.equals(1036)) {%>
                                    <%} else if (rolId.equals(1035)) {%>
                                    <%} else if (rolId.equals(1046) || rolId.equals(1033)) {%>
                                    <li><a href="/throttle/handleViewAxle.do" target="frame">&nbsp;Axle Master</a></li>
                     <!--//                <li><a href="/throttle/vehicleAxleTyreMaster.do" target="frame">&nbsp;Axle Master</a></li>-->
                                    <c:if test = "${vehicles.subMenu =='MFR'}">
                                        <li><a href="/throttle/viewMfr.do" target="frame">&nbsp;MFR</a></li>
                                    </c:if>
                                    <c:if test = "${vehicles.subMenu =='VehicleType'}">
                                        <li><a href="/throttle/viewType.do" target="frame">&nbsp;Vehicle Type</a></li>
                                    </c:if>
                                    <c:if test = "${vehicles.subMenu =='Model'}">
                                        <li><a href="/throttle/viewModelPage.do" target="frame">&nbsp;Model</a></li>
                                    </c:if>
                                    <c:if test = "${vehicles.subMenu =='Vehicle'}">
                                        <li><a href="/throttle/searchVehiclePage.do?fleetTypeId=1&listId=" target="frame">&nbsp;Manage Vehicle</a></li>
                                                <li><a href="/throttle/resetVehicleKM.do" target="frame">&nbsp;Reset Vehicle KM</a></li>
                                                <li><a href="/throttle/handleVehicleFinance.do" target="frame">&nbsp;Vehicle Purchase</a></li>
                                                 <li><a href="/throttle/searchVehiclePage.do?listId=1" target="frame">&nbsp;Vehicle Insurance </a></li>
                                                <!--<li><a href="/throttle/searchVehicleInsurance.do" target="frame">&nbsp;Vehicle Insurance Update</a></li>-->
<!--                                                <li><a href="/throttle/searchVehiclePage.do?listId=5" target="frame">&nbsp;Vehicle AMC Update</a></li>-->
                                                <!--<li><a href="/throttle/searchVehicleAMC.do" target="frame">&nbsp;Vehicle AMC Update</a></li>-->
<!--                                                <li><a href="/throttle/searchVehiclePage.do?listId=2" target="frame">&nbsp;Vehicle Road Tax Update</a></li>-->
                                                <!--<li><a href="/throttle/searchVehicleRoadTax.do" target="frame">&nbsp;Vehicle Road Tax Update</a></li>-->
                                                <li><a href="/throttle/searchVehiclePage.do?listId=3" target="frame">&nbsp;Vehicle Registration Renewal</a></li>
                                                <!--<li><a href="/throttle/searchVehicleFc.do" target="frame">&nbsp;Vehicle Fc Update</a></li>-->
<!--                                                <li><a href="/throttle/searchVehiclePage.do?listId=4" target="frame">&nbsp;Vehicle Permit Update</a></li>-->
                                                <!--<li><a href="/throttle/searchVehiclePermit.do" target="frame">&nbsp;Vehicle Permit Update</a></li>-->
<!--                                                <li><a href="/throttle/viewVehicleProfile.do" target="frame">&nbsp;Vehicle Profile</a></li>-->
<!--                                                <li><a href="/throttle/vehicleReplacementPage.do" target="frame"> &nbsp;Vehicle Replacement</a></li>-->
<!--                                                <li><a href="/throttle/vehicleAxleTyreMaster.do" target="frame">&nbsp;Vehicle AxleTyre Master</a></li>-->
                                                <li><a href="/throttle/handleVehicleAccident.do" target="frame">&nbsp;Vehicle Accident</a></li>
                                                <li><a href="/throttle/vehicleAccidentDetails.do" target="frame">&nbsp;Vehicle Accident Details</a></li>
                                                <li><a href="/throttle/vehicleAccidentHistory.do" target="frame">&nbsp;Vehicle Accident History</a></li>
                                    </c:if>
                                    <% } else if (rolId.equals(1023)) {%>
                                    <li><a href="">&nbsp;Vehicle</a>
                                        <ul class="acitem">
                                            <li><a href="/throttle/handleViewAxle.do" target="frame">&nbsp;Axle Master</a></li>
                                    <!--<li><a href="/throttle/vehicleAxleTyreMaster.do" target="frame">&nbsp;Axle Master</a></li>-->
                                            <c:if test = "${vehicles.subMenu =='MFR'}">
                                                <li><a href="/throttle/viewMfr.do?fleetType=1" target="frame">&nbsp;MFR</a></li>
                                            </c:if>
                                            <c:if test = "${vehicles.subMenu =='VehicleType'}">
                                                <li><a href="/throttle/viewType.do" target="frame">&nbsp;Vehicle Type</a></li>
                                            </c:if>
                                            <c:if test = "${vehicles.subMenu =='Model'}">
                                                <li><a href="/throttle/viewModelPage.do?fleetType=1" target="frame">&nbsp;Model</a></li>
                                            </c:if>
                                            <c:if test = "${vehicles.subMenu =='Vehicle'}">
                                                <li><a href="/throttle/searchVehiclePage.do?fleetTypeId=1&listId=" target="frame">&nbsp;Manage Vehicle</a></li>
                                                <li><a href="/throttle/resetVehicleKM.do" target="frame">&nbsp;Reset Vehicle KM</a></li>
                                                <li><a href="/throttle/handleVehicleFinance.do" target="frame">&nbsp;Vehicle Purchase</a></li>
                                                 <li><a href="/throttle/searchVehiclePage.do?listId=1" target="frame">&nbsp;Vehicle Insurance </a></li>
                                                <!--<li><a href="/throttle/searchVehicleInsurance.do" target="frame">&nbsp;Vehicle Insurance Update</a></li>-->
<!--                                                <li><a href="/throttle/searchVehiclePage.do?listId=5" target="frame">&nbsp;Vehicle AMC Update</a></li>-->
                                                <!--<li><a href="/throttle/searchVehicleAMC.do" target="frame">&nbsp;Vehicle AMC Update</a></li>-->
<!--                                                <li><a href="/throttle/searchVehiclePage.do?listId=2" target="frame">&nbsp;Vehicle Road Tax Update</a></li>-->
                                                <!--<li><a href="/throttle/searchVehicleRoadTax.do" target="frame">&nbsp;Vehicle Road Tax Update</a></li>-->
                                                <li><a href="/throttle/searchVehiclePage.do?listId=3" target="frame">&nbsp;Vehicle Registration/Renewal</a></li>
                                                <!--<li><a href="/throttle/searchVehicleFc.do" target="frame">&nbsp;Vehicle Fc Update</a></li>-->
<!--                                                <li><a href="/throttle/searchVehiclePage.do?listId=4" target="frame">&nbsp;Vehicle Permit Update</a></li>-->
                                                <!--<li><a href="/throttle/searchVehiclePermit.do" target="frame">&nbsp;Vehicle Permit Update</a></li>-->
<!--                                                <li><a href="/throttle/viewVehicleProfile.do" target="frame">&nbsp;Vehicle Profile</a></li>-->
<!--                                                <li><a href="/throttle/vehicleReplacementPage.do" target="frame"> &nbsp;Vehicle Replacement</a></li>-->
<!--                                                <li><a href="/throttle/vehicleAxleTyreMaster.do" target="frame">&nbsp;Vehicle AxleTyre Master</a></li>-->
                                                <li><a href="/throttle/handleVehicleAccident.do" target="frame">&nbsp;Vehicle Accident</a></li>
                                                <li><a href="/throttle/vehicleAccidentDetails.do" target="frame">&nbsp;Vehicle Accident Details</a></li>
                                                <li><a href="/throttle/vehicleAccidentHistory.do" target="frame">&nbsp;Vehicle Accident History</a></li>
<!--                                                <li><a href="/throttle/manageVessel.do" target="frame">&nbsp;Manage Vessel</a></li>
                                                <li><a href="/throttle/dischargeVessel.do" target="frame">&nbsp;Discharge Vessel</a></li>-->
                                            </c:if>
                                        </ul>
                                    </li>
                                    <li><a href="">&nbsp;Trailer</a>
                                        <ul class="acitem">
                                            <c:if test = "${vehicles.subMenu =='MFR'}">
                                                <li><a href="/throttle/viewMfr.do?fleetType=2" target="frame">&nbsp;MFR</a></li>
                                            </c:if>
                                            <c:if test = "${vehicles.subMenu =='VehicleType'}">
                                                <li><a href="/throttle/handleViewTrailerTypePage.do" target="frame">&nbsp;Trailer Type</a></li>
                                            </c:if>
                                            <c:if test = "${vehicles.subMenu =='Model'}">
                                                <li><a href="/throttle/viewModelPage.do?fleetType=2" target="frame">&nbsp;Model</a></li>
                                            </c:if>
                                            <c:if test = "${vehicles.subMenu =='Vehicle'}">
                                                <li><a href="/throttle/searchTrailerPage.do?fleetTypeId=2&listId=" target="frame"> Manage Trailer</a></li>
<!--                                                <li><a href="/throttle/resetVehicleKM.do" target="frame">&nbsp;Reset Vehicle KM</a></li>-->
                                                <li><a href="/throttle/handleVehicleFinance.do" target="frame">&nbsp;Trailer Purchase</a></li>
                                                 <li><a href="/throttle/searchTrailerPage.do?listId=1" target="frame">&nbsp;Trailer Insurance </a></li>
                                                <!--<li><a href="/throttle/searchVehicleInsurance.do" target="frame">&nbsp;Vehicle Insurance Update</a></li>-->
<!--                                                <li><a href="/throttle/searchVehicleAMC.do" target="frame">&nbsp;Vehicle AMC Update</a></li>-->
<!--                                                <li><a href="/throttle/searchVehicleRoadTax.do" target="frame">&nbsp;Vehicle Road Tax Update</a></li>-->
                                                <li><a href="/throttle/searchTrailerPage.do?listId=3" target="frame">&nbsp;Trailer Registration/Renewal</a></li>
<!--                                                <li><a href="/throttle/handleVehicleAccident.do" target="frame">&nbsp;Vehicle Accident Update</a></li>
                                                <li><a href="/throttle/searchVehiclePermit.do" target="frame">&nbsp;Vehicle Permit Update</a></li>
                                                <li><a href="/throttle/viewVehicleProfile.do" target="frame">&nbsp;Vehicle Profile</a></li>
                                                <li><a href="/throttle/vehicleReplacementPage.do" target="frame"> &nbsp;Vehicle Replacement</a></li>-->
<!--                                                <li><a href="/throttle/vehicleAxleTyreMaster.do" target="frame">&nbsp;Vehicle AxleTyre Master</a></li>-->
<!--                                                <li><a href="/throttle/vehicleAccidentDetails.do" target="frame">&nbsp;Vehicle Accident Details</a></li>
                                                <li><a href="/throttle/vehicleAccidentHistory.do" target="frame">&nbsp;Vehicle Accident History</a></li>
                                                <li><a href="/throttle/manageVessel.do" target="frame">&nbsp;Manage Vessel</a></li>
                                                <li><a href="/throttle/dischargeVessel.do" target="frame">&nbsp;Discharge Vessel</a></li>-->
                                            </c:if>
                                        </ul>
                                    </li>

                                    <% } else {%>
                                    <li><a href="/throttle/handleViewAxle.do" target="frame">&nbsp;Axle Master</a></li>
                                    <!--<li><a href="/throttle/vehicleAxleTyreMaster.do" target="frame">&nbsp;Axle Master</a></li>-->
                                    <c:if test = "${vehicles.subMenu =='MFR'}">
                                        <li><a href="/throttle/viewMfr.do" target="frame">&nbsp;MFR</a></li>
                                    </c:if>
                                    <c:if test = "${vehicles.subMenu =='Model'}">
                                        <li><a href="/throttle/viewModelPage.do" target="frame">&nbsp;Model</a></li>
                                    </c:if>
                                    <c:if test = "${vehicles.subMenu =='VehicleType'}">
                                        <li><a href="/throttle/viewType.do" target="frame">&nbsp;Vehicle Type</a></li>
                                    </c:if>
                                    <c:if test = "${vehicles.subMenu =='Vehicle'}">
                                        <li><a href="/throttle/searchVehiclePage.do" target="frame">&nbsp;Manage Vehicle</a></li>
                                        <li><a href="/throttle/resetVehicleKM.do" target="frame">&nbsp;Reset Vehicle KM</a></li>
                                        <li><a href="/throttle/handleVehicleFinance.do" target="frame">&nbsp;Vehicle Financing Details</a></li>
                                        <li><a href="/throttle/searchVehiclePage.do?" target="frame">&nbsp;Vehicle Insurance Update</a></li>
                                        <!--<li><a href="/throttle/searchVehicleInsurance.do" target="frame">&nbsp;Vehicle Insurance Update</a></li>-->
                                        <li><a href="/throttle/searchVehicleAMC.do" target="frame">&nbsp;Vehicle AMC Update</a></li>
                                        <li><a href="/throttle/searchVehicleRoadTax.do" target="frame">&nbsp;Vehicle Road Tax Update</a></li>
                                        <li><a href="/throttle/searchVehicleFc.do" target="frame">&nbsp;Vehicle Fc Update</a></li>
                                        <li><a href="/throttle/handleVehicleAccident.do" target="frame">&nbsp;Vehicle Accident Update</a></li>
                                        <li><a href="/throttle/searchVehiclePermit.do" target="frame">&nbsp;Vehicle Permit Update</a></li>
                                        <li><a href="/throttle/viewVehicleProfile.do" target="frame">&nbsp;Vehicle Profile</a></li>
                                        <li><a href="/throttle/vehicleAccidentDetails.do" target="frame">&nbsp;Vehicle Accident Details</a></li>
                                        <li><a href="/throttle/vehicleAccidentHistory.do" target="frame">&nbsp;Vehicle Accident History</a></li>
                                        <li><a href="/throttle/manageVessel.do" target="frame">&nbsp;Manage Vessel</a></li>
                                        <li><a href="/throttle/dischargeVessel.do" target="frame">&nbsp;Discharge Vessel</a></li>
                                    </c:if>
                                    <% }%>
                                </c:forEach>
                            </c:if>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>



        <div id="customerM" style="display: none;">
            <table id="customerMenu"  cellpadding="0" cellspacing="0" align="center" border="0">
                <tr>
                    <td>
                        <ul class="menu">
                            <c:if test = "${menu.vehicles != null}">
                                <c:if test = "${menu.customer != null}">
                                    <c:forEach items="${menu.customer}" var="customer">
                                        <c:if test = "${customer.subMenu =='CustDetails'}">
                                            <li><a href="/throttle/handleViewCustomer.do" target="frame">&nbsp;Customer Details</a></li>
                                        </c:if>
                                        <c:if test = "${customer.subMenu =='Contract'}">
                                            <!--<li><a href="/throttle/viewContracts.do" target="frame">&nbsp;Contract Details</a></li>-->
                                            <!--                                            <li><a href="/throttle/viewContractDetails.do" target="frame">&nbsp;Customer Contract</a></li>-->

                                        </c:if>
                                    </c:forEach>
                                </c:if>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>

        <div id="storesM" style="display: none;">
            <c:if test="${menu.stores!=null}">
                <ul class="menu">
                    <c:forEach items="${menu.stores}" var="stores">
                        <c:if test = "${stores.subMenu =='ManageRack'}">
                            <li><a href="/throttle/manageRacks.do" target="frame">&nbsp;Manage Rack</a></li>
                        </c:if>
                        <c:if test = "${stores.subMenu =='ManageSubRack'}" >
                            <li><a href="/throttle/manageSubRack.do" target="frame">&nbsp;Manage Subrack</a></li>
                        </c:if>
                        <c:if test = "${stores.subMenu =='ManageParts'}" >
                            <li><a href="/throttle/manageParts.do" target="frame">&nbsp;Manage Parts</a></li>
                        </c:if>
                        <c:if test = "${stores.subMenu =='MRS Approval'}" >
                            <li><a href="/throttle/handleAlterVatPage.do" target="frame">&nbsp;Manage VAT</a></li>
                            <li><a href="/throttle/handleGroupPage.do" target="frame">&nbsp;Manage Group</a></li>
                            <li><a href="/throttle/MRSApproval.do" target="frame">&nbsp;MRS Approval</a></li>
                        </c:if>
                        <c:if test = "${stores.subMenu =='View MRS'}" >
                            <li><a href="/throttle/MRSList.do" target="frame">&nbsp;View/Issue MRS</a></li>
<!--                            <li><a href="/throttle/handleGenerateCounterPage.do" target="frame">&nbsp;Generate Counter</a></li>-->
                        </c:if>
                        <c:if test = "${stores.subMenu =='GenerateMPR'}">
                            <li><a href="/throttle/generateDirectMprPage.do" target="frame">&nbsp;Generate MPR</a></li>
                        </c:if>
                        <c:if test = "${stores.subMenu =='MprApproval'}">
                            <li><a href="/throttle/mprApprovalList.do" target="frame">&nbsp;MPR Approval</a></li>
                        </c:if>
                        <c:if test = "${stores.subMenu =='MprList'}">
                            <li><a href="/throttle/storesMprList.do" target="frame">&nbsp;MPR List</a></li>
                        </c:if>
                        <c:if test = "${stores.subMenu =='MprApproval'}" >
                            <li><a href="/throttle/modifyPO.do" target="frame">&nbsp;Modify PO</a></li>
                        </c:if>
                        <c:if test = "${stores.subMenu =='RequiredItems'}" >
                            <li><a href="/throttle/requiredItems.do" target="frame">&nbsp;Required Items</a></li>
                        </c:if>
                        <c:if test = "${stores.subMenu =='MPR Approval'}" >
                            <li><a href="/throttle/receiveInvoice.do" target="frame">&nbsp;Receive DC</a></li>
                        </c:if>
                        <c:if test = "${stores.subMenu =='ModifyGRN'}" >
                            <li><a href="/throttle/modifyGrnPage.do" target="frame">&nbsp;Receive DC Invoice </a></li>
                        </c:if>
                        <c:if test = "${stores.subMenu =='ModifyGRN'}" >
                            <!--<li><a href="/throttle/handleAlterGrnHeaderInfoPage.do" target="frame">&nbsp;Modify GRN </a></li>-->
                        </c:if>

                        <c:if test = "${stores.subMenu =='StockTransfer'}" >
                            <li><a href="">&nbsp;Stock Transfer</a>
                                <ul class="acitem">
                                    <li><a href="/throttle/generateStRequestPage.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;New Request </a></li>
                                    <li><a href="/throttle/search.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Approve Request</a></li>
                                    <li><a href="/throttle/issueStockTransfer.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Transfer Stock</a></li>
                                    <li><a href="/throttle/receiveStock.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Receive Stock</a></li>
                                    <li><a href="/throttle/handleRcGoodsTransfer.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Move RC Stock to Redhills</a></li>
                                </ul>
                            </li>
                        </c:if>

                        <c:if test = "${stores.subMenu =='Recondition'}" >
                            <li><a href="">&nbsp;Reconditioning</a>
                                <ul class="acitem">
<!--                                    <li><a href="/throttle/generateRcZoneWo.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Int. RC WO</a></li>-->
<!--                                    <li><a href="/throttle/faultItems.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Ext. RC WO</a></li>-->
                                    <!--      <li><a href="/throttle/faultTyreItems.do" target="frame">&nbsp;Fault Tyre Items</a></li>-->
                                    <li><a href="/throttle/rcQueue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;RC Queue</a></li>
                                    <li><a href="/throttle/modifyWO.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Modify RC WO</a></li>
<!--                                    <li><a href="/throttle/generateRCMrs.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Generate MRS</a></li>-->
<!--                                    <li><a href="/throttle/alterRcWo.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Alter RC WO</a></li>-->
                                    <li><a href="/throttle/receiveRcParts.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Receive RC</a></li>
                                </ul>
                            </li>
                        </c:if>

                        <c:if test = "${stores.subMenu =='ManageScrap'}" >
                            <li><a href="">&nbsp;Manage Scrap</a>
                                <ul class="acitem">
                                    <li><a href="/throttle/viewScrapPage.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;View Scrap</a></li>
                                    <li><a href="/throttle/manageDisposePage.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Dispose Scrap</a></li>
                                    <li><a href="/throttle/handleAddScrapItemsPage.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Add Scrap</a></li>
                                    <li><a href="/throttle/scrapApprovalPage.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Approve Scrap Items</a></li>
                                </ul>
                            </li>
                        </c:if>
                        </c:if>
                    </c:forEach>
                </ul>
            </c:if>
        </div>


        <div id="securityM" style="display: none;">
            <table id="securityMenu"  cellpadding="0" cellspacing="0" align="center" border="0">
                <tr>
                    <td>
                        <c:if test = "${menu.name=='Security'}">
                            <ul class="menu">
                                <c:if test = "${menu.security != null}" >
                                    <c:forEach items="${menu.security}" var="security">
                                        <c:if test = "${security.subMenu =='VehiclesIO'}" >
                                            <li><a href="/throttle/manageSecurityPage.do" target="frame">&nbsp;Vehicle In/Out</a></li>
                                        </c:if>
                                        <c:if test = "${security.subMenu =='GoodsIO'}" >
                                            <li><a href="/throttle/manageGoodsPage.do" target="frame">&nbsp;Goods In/Out</a></li>
                                        </c:if>
                                    </c:forEach>
                                </c:if>
                            </ul>
                        </c:if>
                    </td>
                </tr>
            </table>
        </div>

        <div id="serviceplanM" style="display: none;">
            <table id="serviceplanMenu"  cellpadding="0" cellspacing="0" align="center" border="0">
                <tr>
                    <td>
                        <c:if test = "${menu.name=='Service'}" >
                            <ul class="menu">
                                <c:if test = "${menu.service != null}" >
                                    <c:forEach items="${menu.service}" var="service">
                                        <%if (rolId.equals(1030)) {%>
                                        <%} else if (rolId.equals(1031)) {%>
                                        <%} else if (rolId.equals(1036)) {%>
                                        <%} else if (rolId.equals(1041)) {%>
                                        <%} else if (rolId.equals(1035)) {%>
                                        <c:if test = "${service.subMenu =='ManageSection'}" >
                                            <li><a href="/throttle/manageService.do" target="frame">&nbsp;Manage Service Type</a></li>
                                        </c:if>
                                        <c:if test = "${service.subMenu =='ManageSection'}" >
                                            <li><a href="/throttle/manageSection.do" target="frame">&nbsp;Manage Section</a></li>
                                        </c:if>
                                        <c:if test = "${service.subMenu =='ManageProblem'}" >
                                            <li><a href="/throttle/manageProblemAct.do" target="frame">&nbsp;Manage Problem</a></li>
                                        </c:if>
                                        <c:if test = "${service.subMenu =='ProblemActivity'}" >
                                            <li><a href="/throttle/manageProblemActivity.do" target="frame">&nbsp;Activities</a></li>
                                        </c:if>
                                        <c:if test = "${service.subMenu =='ProblemActivity'}" >
                                            <!--                 <li><a href="/throttle/manageProblemCause.do" target="frame">&nbsp;Prob Causes</a></li>-->
                                            <!-- <li><a href="/throttle/manageProblemActivityCause.do" target="frame">&nbsp;Prob Activity Causes</a></li> -->
                                        </c:if>
                                        <c:if test = "${service.subMenu =='ConfigRate'}" >
                                            <!--          <li class="lineDiffer" style="margin-top:2px; "><a href="/throttle/configLabourRate.do" target="frame">Config Labour</a></li>-->
                                        </c:if>
                                        <c:if test = "${service.subMenu =='PeriodicService'}" >
                                            <li><a href="/throttle/manageMaintenance.do" target="frame">&nbsp;Periodic Service</a></li>
                                        </c:if>
                                        <c:if test = "${service.subMenu =='ConfigFreeService'}" >
                                            <li><a href="/throttle/searchFreeService.do" target="frame">&nbsp;Config Free Service</a></li>
                                        </c:if>
                                        <% } else {%>
                                        <c:if test = "${service.subMenu =='ManageSection'}" >
                                            <li><a href="/throttle/manageService.do" target="frame">&nbsp;Manage Service Type</a></li>
                                        </c:if>
                                        <c:if test = "${service.subMenu =='ManageSection'}" >
                                            <li><a href="/throttle/manageSection.do" target="frame">&nbsp;Manage Section</a></li>
                                        </c:if>
                                        <c:if test = "${service.subMenu =='ManageProblem'}" >
                                            <li><a href="/throttle/manageProblemAct.do" target="frame">&nbsp;Manage Problem</a></li>
                                        </c:if>
                                        <c:if test = "${service.subMenu =='ProblemActivity'}" >
                                            <li><a href="/throttle/manageProblemActivity.do" target="frame">&nbsp;Activities</a></li>
                                        </c:if>
                                        <c:if test = "${service.subMenu =='ProblemActivity'}" >
                                            <!--                 <li><a href="/throttle/manageProblemCause.do" target="frame">&nbsp;Prob Causes</a></li>-->
                                            <!-- <li><a href="/throttle/manageProblemActivityCause.do" target="frame">&nbsp;Prob Activity Causes</a></li> -->
                                        </c:if>
                                        <c:if test = "${service.subMenu =='ConfigRate'}" >
                                            <!--          <li class="lineDiffer" style="margin-top:2px; "><a href="/throttle/configLabourRate.do" target="frame">Config Labour</a></li>-->
                                        </c:if>
                                        <c:if test = "${service.subMenu =='PeriodicService'}" >
                                            <li><a href="/throttle/manageMaintenance.do" target="frame">&nbsp;Periodic Service</a></li>
                                        </c:if>
                                        <c:if test = "${service.subMenu =='ConfigFreeService'}" >
                                            <li><a href="/throttle/searchFreeService.do" target="frame">&nbsp;Config Free Service</a></li>
                                        </c:if>
                                        <%}%>
                                    </c:forEach>
                                </c:if>
                            </ul>
                        </c:if>
                    </td>
                </tr>
            </table>
        </div>

        <div id="vendorsM" style="display: none;">
            <table id="vendorsMenu"  cellpadding="0" cellspacing="0" align="center" border="0">
                <tr>
                    <td>
                        <c:if test = "${menu.name=='Vendor'}" >
                            <ul class="menu">
                                <c:if test="${menu.vendor!=null}">
                                    <c:forEach items="${menu.vendor}" var="vendor">
                                        <%if (rolId.equals(1030)) {%>
                                        <c:if test = "${vendor.subMenu =='ManageVendor'}" >
                                            <li><a href="/throttle/manageVendorPage.do" target="frame">&nbsp;Manage Vendor</a></li>
                                        </c:if>

                                        <%} else if (rolId.equals(1032)) {%>
                                        <%} else if (rolId.equals(1031)) {%>
                                        <%} else if (rolId.equals(1041)) {%>
                                        <%} else if (rolId.equals(1036) || rolId.equals(1033)) {%>
                                        <c:if test = "${vendor.subMenu =='ManageVendor'}" >
                                            <li><a href="/throttle/manageVendorPage.do" target="frame">&nbsp;Manage Vendor</a></li>
                                            <li><a href="/throttle/manageFleetVendorPage.do" target="frame">&nbsp;Fleet Vendor Contract</a></li>
                                        </c:if>
                                        <c:if test = "${vendor.subMenu =='ConfigVendor'}" >
                                            <!--   <li><a href="/throttle/handleSearchVendorCatPage.do" target="frame">&nbsp;Config Vendor Category</a></li>-->
                                            <!--<li><a href="/throttle/manageVendorConfigPage.do" target="frame">&nbsp;Config Vendor Items</a></li>-->
                                            <li><a href="/throttle/manageVendorItemConfigPage.do" target="frame">&nbsp;Config Vendor Items</a></li>
                                        </c:if>
                                        <%} else if (rolId.equals(1035)) {%>
                                        <c:if test = "${vendor.subMenu =='ConfigVendor'}" >
                                            <c:if test = "${vendor.subMenu =='ManageVendor'}" >
                                                <li><a href="/throttle/manageVendorPage.do" target="frame">&nbsp;Manage Vendor</a></li>
                                            </c:if>
                                            <c:if test = "${vendor.subMenu =='ConfigVendor'}" >
                                                <!--   <li><a href="/throttle/handleSearchVendorCatPage.do" target="frame">&nbsp;Config Vendor Category</a></li>-->
                                                <!--<li><a href="/throttle/manageVendorConfigPage.do" target="frame">&nbsp;Config Vendor Items</a></li>-->
                                                <li><a href="/throttle/manageVendorItemConfigPage.do" target="frame">&nbsp;Config Vendor Items</a></li>
                                            </c:if>
<!--                                            <li><a href="/throttle/handleVendorPaymentPage.do" target="frame">&nbsp;Vendor Payment</a></li>-->
                                        </c:if>
                                        <%} else {%>
                                        <c:if test = "${vendor.subMenu =='ConfigVendor'}" >
                                            <c:if test = "${vendor.subMenu =='ManageVendor'}" >
                                                <li><a href="/throttle/manageVendorPage.do" target="frame">&nbsp;Manage Vendor</a></li>
                                            </c:if>
                                            <c:if test = "${vendor.subMenu =='ConfigVendor'}" >
                                                <!--   <li><a href="/throttle/handleSearchVendorCatPage.do" target="frame">&nbsp;Config Vendor Category</a></li>-->
                                                <!--<li><a href="/throttle/manageVendorConfigPage.do" target="frame">&nbsp;Config Vendor Items</a></li>-->
                                                <li><a href="/throttle/manageVendorItemConfigPage.do" target="frame">&nbsp;Config Vendor Items</a></li>
                                            </c:if>
<!--                                            <li><a href="/throttle/handleVendorPaymentPage.do" target="frame">&nbsp;Vendor Payment</a></li>-->
                                            <li><a href="/throttle/manageFleetVendorPage.do" target="frame">&nbsp;Fleet Vendor Contract</a></li>
                                        </c:if>
                                        <%}%>
                                    </c:forEach>
                                </c:if>
                            </ul>
                        </c:if>
                    </td>
                </tr>
            </table>
        </div>

        <div id="operationsM" style="display: none;">
            <table id="operationsMenu"  cellpadding="0" cellspacing="0" align="center" border="0">
                <tr>
                    <td>
                        <ul class="menu">
                            <c:if test="${menu.operation!=null}">
                                <c:forEach items="${menu.operation}" var="operation">
                                    <%if (rolId.equals(1030)) {%>
                                    <li><a href="">&nbsp;Sales/Ops</a>
                                        <ul class="acitem">
                                            <li><a href="/throttle/handleViewCustomer.do" target="frame">&nbsp;Customer Master</a></li>
                                            <li><a href="/throttle/consignmentNote.do" target="frame">&nbsp;C-Note Create</a></li>
                                            <li><a href="/throttle/consignmentNoteView.do" target="frame">&nbsp;C-Note View</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="">&nbsp;Operation</a>
                                        <li><a href="/throttle/viewTripSheets.do?statusId=10&admin=No" target="frame">&nbsp;View Trip Sheet</a></li>
                                    </li>
                                    <%} else if (rolId.equals(1032)) {%>
                                    <li><a href="">&nbsp;Sales/Ops</a>
                                        <ul class="acitem">
                                            <li><a href="/throttle/handleViewCustomer.do" target="frame">&nbsp;Customer Master</a></li>
                                            <li><a href="/throttle/consignmentNote.do" target="frame">&nbsp;C-Note Create</a></li>
                                            <li><a href="/throttle/consignmentNoteView.do" target="frame">&nbsp;C-Note View</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="">&nbsp;Operation</a>
                                        <ul class="acitem">
                                            <li><a href="/throttle/viewProductCategory.do" target="frame">&nbsp;Product Category Master</a></li>
                                            <li><a href="/throttle/viewCityMaster.do" target="frame">&nbsp;&nbsp;City Master</a></li>
                                            <li><a href="/throttle/viewRouteDetails.do" target="frame">&nbsp;Route Master</a></li>
                                            <li><a href="/throttle/handleViewPrimaryDriver.do" target="frame">&nbsp;Manage Driver</a></li>
                                            <li><a href="/throttle/vehicleDriverPlanning.do" target="frame">&nbsp;Vehicle &amp; Driver Mapping</a></li>
                                            <li><a href="/throttle/vehicleBPCLCardMapping.do" target="frame">&nbsp;Vehicle &amp; Cash Card Mapping</a></li>
                                            <li><a href="/throttle/viewVehicleAvailability.do?tripType=1" target="frame">&nbsp;&nbsp;Vehicle Availability</a></li>
                                            <li><a href="/throttle/tripPlanning.do?tripType=1" target="frame">&nbsp;Trip Planning</a></li>
                                            <li><a href="/throttle/viewTripPlannedVehicle.do" target="frame">&nbsp;Trip Creation (XLS Upload)</a></li>
                                            <li><a href="/throttle/emptyTripMerging.do" target="frame">&nbsp;Empty Trip Merging</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=10&tripType=1&admin=No" target="frame">&nbsp;View Trip Sheet</a></li>
                                            <li><a href="/throttle/handlefinanceRequest.do" target="frame">&nbsp;Finance Advice Approval</a></li>
                                            <li><a href="/throttle/handleTripClosureRequest.do" target="frame">&nbsp;Trip Closure Approval</a></li>
                                            <li><a href="/throttle/manualEmptyTripApproval.do" target="frame">&nbsp;Empty Trip Approval</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=6&admin=No" target="frame">&nbsp;Trip Assign Vehicle</a></li>
                                            <!--                                            <li><a href="/throttle/viewTripSheets.do?statusId=7" target="frame">&nbsp;Trip Freeze</a></li>-->
                                            <li><a href="/throttle/viewTripSheets.do?statusId=8&admin=No" target="frame">&nbsp;Trip Un Freeze</a></li>
                                            <!--                                            <li><a href="/throttle/viewTripSheets.do?statusId=8" target="frame">&nbsp;Trip Pre Start</a></li>-->
                                            <li><a href="/throttle/viewTripSheets.do?statusId=8&admin=No&tripType=1" target="frame">&nbsp;Trip Start</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=12&tripType=1&admin=No" target="frame">&nbsp;Trip Ended</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=10&admin=No&tripType=1" target="frame">&nbsp;Trip End</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=100&admin=No&tripType=1" target="frame">&nbsp;Trip POD</a></li>
                                            <li><a href="/throttle/viewTripSheetsForChallan.do?tripType=1&admin=No&documentRequired=Y" target="frame">&nbsp;Upload Expense Bill Copy</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=12&admin=No&tripType=1" target="frame">&nbsp;Trip Closure</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=12&admin=Yes&tripType=1" target="frame">&nbsp;Trip Closure Admin</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=13&admin=No&tripType=1" target="frame">&nbsp;Trip Settlement</a></li>
                                            <li><a href="/throttle/emptyTripMerging.do" target="frame">&nbsp;Empty Trip Merging</a></li>
                                            <li><a href="/throttle/handlePrimaryDriverSettlement.do" target="frame">&nbsp;Driver Settlement</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="">&nbsp;Reports</a>
                                        <ul class="acitem">
                                            <li><a href="/throttle/handleVehicleWiseProfitReport.do?param=search" target="frame">&nbsp;Vehicle Wise Profit</a></li>
                                            <li><a href="/throttle/handleVehicleUtilizationReport.do?param=search" target="frame">&nbsp; Vehicle Utilization Report</a></li>
                                            <li><a href="/throttle/handleGPSLogReport.do?param=search" target="frame">&nbsp; GPS Log Report</a></li>
                                            <li><a href="/throttle/handleDriverSettlementDetails.do?param=search" target="frame">&nbsp; Driver Settlement Report</a></li>
                                            <li><a href="/throttle/handleViewTripSheet.do?param=search" target="frame">&nbsp;Trip Sheet Report</a></li>
                                            <li><a href="/throttle/handleFCWiseTripSummary.do?param=search" target="frame">&nbsp;FC Trip Summary</a></li>
                                            <li><a href="/throttle/handleOrderExpenseRevenueReport.do?param=search" target="frame">&nbsp;Order Expense Revenue Report</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="">&nbsp;MIS</a>
                                        <ul class="acitem">
                                            <li><a href="/throttle/handlecustomerrevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Customer Wise Revenue</a></li>
                                            <li><a href="/throttle/customerrevenuebillwise.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Bill Wise Revenue </a></li>
                                            <li><a href="/throttle/revenuefcwise.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp; FC Wise Revenue </a></li>
                                            <li><a href="/throttle/vehiclerevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Vehicle Wise Revenue </a></li>
                                            <li><a href="/throttle/vehicletyperevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Vehicle Type Wise Revenue</a></li>
                                            <li><a href="/throttle/accountreceivablerevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Customer Wise A.R</a></li>
                                            <li><a href="/throttle/accountreceivablebillwiserevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Bill Wise A.R</a></li>
                                            <li><a href="/throttle/totaloperationcost.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Total Operation Cost</a></li>
                                            <li><a href="/throttle/totaloperationcostperbill.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Total Operation Cost (Bill)</a></li>
                                            <li><a href="/throttle/fuelcostperbill.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Fuel Cost</a></li>
                                            <li><a href="/throttle/customerwiseprofit.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Customer Wise Profit</a></li>
                                            <li><a href="/throttle/vehiclewiseprofit.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Vehicle Wise Profit</a></li>
                                            <li><a href="/throttle/fcwiseprofit.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;FC Wise Profit</a></li>
                                            <li><a href="/throttle/consolidatedreportpermonth.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Month Wise Consolidated</a></li>
                                            <li><a href="/throttle/consolidatedreportperbill.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Month Wise Consolidated (Bill)</a></li>
                                        </ul>
                                    </li>
                                    <%} else if (rolId.equals(1042)) {%>
                                    <li><a href="">&nbsp;Operation</a>
                                        <ul class="acitem">
                                            <li><a href="/throttle/viewTripSheets.do?statusId=12&tripType=1&admin=No" target="frame">&nbsp;Trip Closure</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=12&tripType=1&admin=Yes" target="frame">&nbsp;Trip Closure Admin</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=13&tripType=1&admin=No" target="frame">&nbsp;Trip Settlement</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=12&tripType=1&admin=No" target="frame">&nbsp;Trip Ended</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=14&tripType=1&admin=No" target="frame">&nbsp;View Settled Trips</a></li>
                                            <li><a href="/throttle/handlePrimaryDriverSettlement.do" target="frame">&nbsp;Driver Settlement</a></li>
                                            <li><a href="/throttle/viewTripSheetsForChallan.do?tripType=1&admin=No&documentRequired=Y" target="frame">&nbsp;Upload Expense Bill Copy</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=12&tripType=1&admin=Yes" target="frame">&nbsp;Trip Closure Admin</a></li>

                                            <li><a href="/throttle/emptyTripMerging.do" target="frame">&nbsp;Empty Trip Merging</a></li>

                                            <li><a href="/throttle/vehicleBPCLCardMapping.do" target="frame">&nbsp;Vehicle &amp; Cash Card Mapping</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="">&nbsp;Reports</a>
                                        <ul class="acitem">
                                            <li><a href="/throttle/handleViewTripSheet.do?param=search" target="frame">&nbsp;Trip Sheet Report</a></li>

                                        </ul>
                                    </li>

                                    <%} else if (rolId.equals(1029)) {%>

                                    <li><a href="">&nbsp;Sales/Ops</a>
                                        <ul class="acitem">
                                            <li><a href="/throttle/handleViewCustomer.do" target="frame">&nbsp;Customer Master</a></li>

                                            <li><a href="/throttle/consignmentNoteView.do" target="frame">&nbsp;C-Note View</a></li>
                                        </ul>
                                    </li>

                                    <li><a href="">&nbsp;Operation</a>
                                        <ul class="acitem">
                                            <li><a href="/throttle/viewVehicleAvailability.do?tripType=1" target="frame">&nbsp;&nbsp;Vehicle Availability</a></li>
                                            <li><a href="/throttle/viewTripSheets.do?statusId=10&admin=No" target="frame">&nbsp;View Trip Sheet</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="">&nbsp;Finance</a>
                                        <ul class="acitem">


                                            <li><a href="/throttle/viewbillpage.do" target="frame">&nbsp; View Bill </a></li>
                                        </ul>
                                    </li>
                                    <li><a href="">&nbsp;Reports</a>
                                        <ul class="acitem">
                                            <li><a href="/throttle/handleAccountsReceivableReport.do?param=search" target="frame">&nbsp;Accounts Receivable</a></li>
                                            <li><a href="/throttle/handleVehicleWiseProfitReport.do?param=search" target="frame">&nbsp;Vehicle Wise Profit</a></li>
                                            <li><a href="/throttle/handleCustomerWiseProfitReport.do?param=search" target="frame">&nbsp; Customer wise Profit</a></li>
                                            <li><a href="/throttle/handleMarginWiseReport.do?param=search" target="frame">&nbsp;Margin Trip Summary</a></li>
                                            <li><a href="/throttle/handleMonthWiseEmptyRunSummary.do?param=search" target="frame">&nbsp;Empty Run Summary</a></li>
                                            <li><a href="/throttle/handleJobcardSumary.do?param=search" target="frame">&nbsp;Jobcard Summary</a></li>
                                            <li><a href="/throttle/handleTripBudgetReport.do?param=search" target="frame">&nbsp;FC Performance Report</a></li>
                                            <li><a href="/throttle/handleAccountMgrPerformanceReport.do?param=search" target="frame">&nbsp;A/C Mgr Performance Report</a></li>
                                            <li><a href="/throttle/handleRNMExpenseReport.do?param=search" target="frame">&nbsp;R&M Spent Analysis Report</a></li>
                                            <li><a href="/throttle/handleTyerExpenseReport.do?param=search" target="frame">&nbsp;Tyre  Analysis Report</a></li>
                                            <li><a href="/throttle/handleOrderExpenseRevenueReport.do?param=search" target="frame">&nbsp;Order Expense Revenue Report</a></li>
                                            <!-- <li><a href="/throttle/handleTripWiseProfitReport.do" target="frame">&nbsp; Trip wise Profit</a></li>
                                            <li><a href="/throttle/handleTripWiseProfit1.do" target="frame">&nbsp; Trip wise Profit-1</a></li> -->
                                        </ul>
                                    </li>
                                    <li><a href="">&nbsp;MIS</a>
                                        <ul class="acitem">
                                            <li><a href="/throttle/handlecustomerrevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Customer Wise Revenue</a></li>
                                            <li><a href="/throttle/customerrevenuebillwise.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Bill Wise Revenue </a></li>
                                            <li><a href="/throttle/revenuefcwise.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp; FC Wise Revenue </a></li>
                                            <li><a href="/throttle/vehiclerevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Vehicle Wise Revenue </a></li>
                                            <li><a href="/throttle/vehicletyperevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Vehicle Type Wise Revenue</a></li>
                                            <li><a href="/throttle/accountreceivablerevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Customer Wise A.R</a></li>
                                            <li><a href="/throttle/accountreceivablebillwiserevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Bill Wise A.R</a></li>
                                            <li><a href="/throttle/totaloperationcost.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Total Operation Cost</a></li>
                                            <li><a href="/throttle/totaloperationcostperbill.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Total Operation Cost (Bill)</a></li>
                                            <li><a href="/throttle/fuelcostperbill.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Fuel Cost</a></li>
                                            <li><a href="/throttle/customerwiseprofit.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Customer Wise Profit</a></li>
                                            <li><a href="/throttle/vehiclewiseprofit.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Vehicle Wise Profit</a></li>
                                            <li><a href="/throttle/fcwiseprofit.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;FC Wise Profit</a></li>
                                            <li><a href="/throttle/consolidatedreportpermonth.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Month Wise Consolidated</a></li>
                                            <li><a href="/throttle/consolidatedreportperbill.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Month Wise Consolidated (Bill)</a></li>
                                        </ul>
                                    </li>
                                    <%} else if (rolId.equals(1033)) {%>
                                    <li><a href="">&nbsp;Sales/Ops</a>
                                        <ul class="acitem">

                                            <li><a href="/throttle/consignmentNoteView.do" target="frame">&nbsp;C-Note View</a></li>
                                        </ul>

                                        <li><a href="">&nbsp;Operation</a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/tripPlanning.do?tripType=1" target="frame">&nbsp;Trip Planning</a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=8&admin=No" target="frame">&nbsp;Trip To Start</a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=10&tripType=1&admin=No" target="frame">&nbsp;Trip End</a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=12&admin=No" target="frame">&nbsp;Trip Closure</a></li>
                                                <li><a href="/throttle/viewRouteDetails.do" target="frame">&nbsp;Route Master</a></li>
                                                <li><a href="/throttle/vehicleDriverPlanning.do" target="frame">&nbsp;Vehicle &amp; Driver Mapping</a></li>
                                                <li><a href="/throttle/viewVehicleAvailability.do?tripType=1" target="frame">&nbsp;&nbsp;Vehicle Availability</a></li>
                                                <li><a href="/throttle/viewTripPlannedVehicle.do" target="frame">&nbsp;Trip Creation (XLS Upload)</a></li>
                                                <li><a href="/throttle/emptyTripPlanning.do?tripType=1" target="frame">&nbsp;Empty Trip</a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=10&admin=No&tripType=1" target="frame">&nbsp;View Trip Sheet</a></li>
                                                <li><a href="/throttle/viewVehicleDriverAdvance.do?usageTypeId=2" target="frame">&nbsp;VehicleDriverAdvance</a></li>
                                                <li><a href="/throttle/handleTripClosureRequest.do" target="frame">&nbsp;Trip Closure Approval</a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=6&admin=No" target="frame">&nbsp;Trip Assign Vehicle</a></li>
                                                <!--                                            <li><a href="/throttle/viewTripSheets.do?statusId=7" target="frame">&nbsp;Trip Freeze</a></li>
                                                                                            <li><a href="/throttle/viewTripSheets.do?statusId=8" target="frame">&nbsp;Trip Un Freeze</a></li>
                                                                                            <li><a href="/throttle/viewTripSheets.do?statusId=8" target="frame">&nbsp;Trip Pre Start</a></li>
                                                                                            <li><a href="/throttle/viewTripSheets.do?statusId=9&admin=No" target="frame">&nbsp;Trip Start</a></li>-->


                                                <li><a href="/throttle/viewTripSheets.do?statusId=18&admin=No&tripType=1" target="frame">&nbsp;Trip In WFU</a></li>

                                                <li><a href="/throttle/viewTripSheets.do?statusId=12&tripType=1&admin=No" target="frame">&nbsp;Trip Ended</a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=100&admin=No" target="frame">&nbsp;Trip POD</a></li>

                                                <li><a href="/throttle/viewTripSheets.do?statusId=12&tripType=1&admin=Yes" target="frame">&nbsp;Trip FC Closed</a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=13&admin=No" target="frame">&nbsp;Trip Settlement</a></li>
                                                <li><a href="/throttle/WOstatus.do" target="frame">&nbsp;Work Order</a></li>
                                            </ul>
                                        </li>

                                        <%} else if (rolId.equals(1041)) {%>
                                        <li><a href="">&nbsp;Secondary Operation</a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/handleViewCustomer.do?customerType=2" target="frame">&nbsp;Customers </a></li>
                                                <li><a href="/throttle/handleEditViewCustomer.do" target="frame">&nbsp;Routes</a></li>
                                                <li><a href="/throttle/vehicleDriverPlanning.do" target="frame">&nbsp;Vehicle &amp; Driver Mapping</a></li>
                                                <li><a href="/throttle/secondaryTripSheduleView.do" target="frame">&nbsp;Schedule Trip</a></li>
                                                <li><a href="/throttle/emptyTripPlanning.do?tripType=2" target="frame">&nbsp;Empty Trip</a></li>
                                                <li><a href="/throttle/secondaryDriverSettlement.do" target="frame">&nbsp; Secondary Driver Settlement</a></li>
                                                <li><a href="/throttle/viewSecondaryDriverSettlement.do" target="frame">&nbsp; View Secondary Settlement</a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=10&tripType=2&admin=No" target="frame">&nbsp;View Trip Sheet</a></li>
                                                <li><a href="/throttle/viewVehicleDriverAdvance.do?usageTypeId=1" target="frame">&nbsp;VehicleDriverAdvance</a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=6&tripType=2&admin=No" target="frame">&nbsp;Trip Assign Vehicle</a></li>
                                                <!--						<li><a href="/throttle/viewTripSheets.do?statusId=7&tripType=2" target="frame">&nbsp;Trip Freeze</a></li>-->
                                                <li><a href="/throttle/viewTripSheets.do?statusId=8&tripType=2&admin=No" target="frame">&nbsp;Trip Un Freeze</a></li>
                                                <!--						<li><a href="/throttle/viewTripSheets.do?statusId=8&tripType=2" target="frame">&nbsp;Trip Pre Start</a></li>-->
                                                <li><a href="/throttle/viewTripSheets.do?statusId=8&tripType=2&admin=No" target="frame">&nbsp;Trip Start</a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=10&tripType=2&admin=No" target="frame">&nbsp;Trip End</a></li>
                                                <!--<li><a href="/throttle/viewTripSheets.do?statusId=100&tripType=2" target="frame">&nbsp;Trip POD</a></li>-->
                                                <li><a href="/throttle/viewTripSheets.do?statusId=12&tripType=2&admin=No" target="frame">&nbsp;Trip Closure</a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=12&tripType=2&admin=Yes" target="frame">&nbsp;Trip FC Closed</a></li>

                                                <li><a href="/throttle/WOstatus.do" target="frame">&nbsp;Work Order</a></li>
                                            </ul>
                                        </li>

                                        <%} else if (rolId.equals(1035)) {%>
                                        <li><a href="">&nbsp;Operation</a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/vehicleDriverPlanning.do" target="frame">&nbsp;Vehicle &amp; Driver Mapping</a></li>
                                                <li><a href="/throttle/viewVehicleAvailability.do?tripType=1" target="frame">&nbsp;&nbsp;Vehicle Availability</a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=10&admin=No" target="frame">&nbsp;View Trip Sheet</a></li>
                                                <li><a href="/throttle/manageSecurityPage.do" target="frame">&nbsp;Security Action</a></li>
                                                <li><a href="/throttle/manageGoodsPage.do" target="frame">&nbsp;Goods</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;Reports</a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/handleRNMExpenseReport.do?param=search" target="frame">&nbsp;R&M Spent Analysis Report</a></li>
                                                <li><a href="/throttle/handleTyerExpenseReport.do?param=search" target="frame">&nbsp;Tyre  Analysis Report</a></li>
                                                <li><a href="/throttle/handleAccountsReceivableReport.do?param=search" target="frame">&nbsp;Accounts Receivable</a></li>
                                                <li><a href="/throttle/handleVehicleWiseProfitReport.do?param=search" target="frame">&nbsp;Vehicle Wise Profit</a></li>

                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;MIS</a>
                                            <ul class="acitem">

                                                <li><a href="/throttle/vehiclewiseprofit.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Vehicle Wise Profit</a></li>
                                                <li><a href="/throttle/fcwiseprofit.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;FC Wise Profit</a></li>
                                                <li><a href="/throttle/consolidatedreportpermonth.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Month Wise Consolidated</a></li>
                                                <li><a href="/throttle/consolidatedreportperbill.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Month Wise Consolidated (Bill)</a></li>



                                            </ul>
                                        </li>
                                        <%} else if (rolId.equals(1037)) {%>
                                        <li><a href="">&nbsp;Sales/Ops</a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/handleViewCustomer.do" target="frame">&nbsp;Customer Master</a></li>

                                                <li><a href="/throttle/consignmentNoteView.do" target="frame">&nbsp;C-Note View</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;Operation</a>
                                            <ul class="acitem">

                                                <li><a href="/throttle/viewVehicleAvailability.do?tripType=1" target="frame">&nbsp;&nbsp;Vehicle Availability</a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=10&admin=No" target="frame">&nbsp;View Trip Sheet</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;Finance</a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/viewclosedtrip.do" target="frame">&nbsp;Generate Billing</a></li>
                                                <li><a href="/throttle/viewbillpageforsubmit.do" target="frame">&nbsp; Submit Bill </a></li>
                                                <li><a href="/throttle/viewbillpage.do" target="frame">&nbsp; View Bill </a></li>
                                                <li><a href="/throttle/customerBillPayment.do" target="frame">&nbsp; Customer Bill Payment</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;Reports</a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/handleAccountsReceivableReport.do?param=search" target="frame">&nbsp;Accounts Receivable</a></li>
                                                <li><a href="/throttle/handleToPayCustomerTripDetails.do?param=search" target="frame">&nbsp;To pay customer trip Details Report</a></li>
                                                <li><a href="/throttle/handleVehicleWiseProfitReport.do?param=search" target="frame">&nbsp;Vehicle Wise Profit</a></li>
                                                <li><a href="/throttle/handleTripVmrReport.do?param=search" target="frame">&nbsp;Trip VMR Report</a></li>
                                                <li><a href="/throttle/handleToPayCustomerTripDetails.do?param=search" target="frame">&nbsp;To pay customer trip Details Report</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;MIS</a>
                                            <ul class="acitem">


                                                <li><a href="/throttle/vehiclerevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Vehicle Wise Revenue </a></li>
                                                <li><a href="/throttle/vehicletyperevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Vehicle Type Wise Revenue</a></li>

                                                <li><a href="/throttle/totaloperationcost.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Total Operation Cost</a></li>
                                                <li><a href="/throttle/totaloperationcostperbill.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Total Operation Cost (Bill)</a></li>

                                                <li><a href="/throttle/vehiclewiseprofit.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Vehicle Wise Profit</a></li>
                                                <li><a href="/throttle/fcwiseprofit.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;FC Wise Profit</a></li>
                                                <li><a href="/throttle/consolidatedreportpermonth.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Month Wise Consolidated</a></li>
                                                <li><a href="/throttle/consolidatedreportperbill.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Month Wise Consolidated (Bill)</a></li>
                                            </ul>
                                        </li>
                                        <%} else if (rolId.equals(1044)) {%>
                                        <li><a href="">&nbsp;Operation</a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/viewTripSheets.do?statusId=100&tripType=1&admin=No" target="frame">&nbsp;Trip POD</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;Reports</a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/handleViewTripSheet.do?param=search" target="frame"> Trip Sheet Report</a></li>
                                            </ul>
                                        </li>




                                        <%} else if (rolId.equals(1040)) {%>
                                        <li><a href="">&nbsp;Operation</a>
                                            <ul class="acitem">

                                                <li><a href="/throttle/viewVehicleAvailability.do?tripType=1" target="frame">&nbsp;&nbsp;Vehicle Availability</a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=10&admin=No" target="frame">&nbsp;View Trip Sheet</a></li>
                                                <li><a href="/throttle/handlefinanceAdvice.do?tripType=1" target="frame">&nbsp;Primary Finance Advice (Adv)</a></li>
                                                <li><a href="/throttle/handlefinanceAdvice.do?tripType=2" target="frame">&nbsp;Sec Finance Advice (Adv)</a></li>
                                                <li><a href="/throttle/vehicleBPCLCardMapping.do" target="frame">&nbsp;Vehicle &amp; Cash Card Mapping</a></li>
                                                <li><a href="/throttle/uploadBPCLData.do" target="frame">&nbsp;Upload Cash Card Txn</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;Reports</a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/handleBPCLTransactionDetails.do?param=search" target="frame">&nbsp; BPCL Transaction Report</a></li>
                                                <li><a href="/throttle/handleTripWiseProfitReportEndStatus.do?param=search" target="frame">&nbsp; Trip Wise Profitability Report(End)</a></li>
                                            </ul>
                                        </li>
                                        <%} else if (rolId.equals(1036)) {%>
                                        <li><a href="">&nbsp;Sales/Ops</a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/handleViewCustomer.do" target="frame">&nbsp;Customer Master</a></li>

                                                <li><a href="/throttle/consignmentNoteView.do" target="frame">&nbsp;C-Note View</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;Operation</a>
                                            <ul class="acitem">

                                                <li><a href="/throttle/viewVehicleAvailability.do?tripType=1" target="frame">&nbsp;&nbsp;Vehicle Availability</a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=10&admin=No" target="frame">&nbsp;View Trip Sheet</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;Finance</a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/viewclosedtrip.do" target="frame">&nbsp;Generate Billing</a></li>
                                                <li><a href="/throttle/viewbillpageforsubmit.do" target="frame">&nbsp; Submit Bill </a></li>
                                                <li><a href="/throttle/viewbillpage.do" target="frame">&nbsp; View Bill </a></li>
                                                <li><a href="/throttle/customerBillPayment.do" target="frame">&nbsp; Customer Bill Payment</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;Reports</a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/handleAccountsReceivableReport.do?param=search" target="frame">&nbsp;Accounts Receivable</a></li>
                                                <li><a href="/throttle/handleVehicleWiseProfitReport.do?param=search" target="frame">&nbsp;Vehicle Wise Profit</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;MIS</a>
                                            <ul class="acitem">


                                                <li><a href="/throttle/vehiclerevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Vehicle Wise Revenue </a></li>
                                                <li><a href="/throttle/vehicletyperevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Vehicle Type Wise Revenue</a></li>

                                                <li><a href="/throttle/totaloperationcost.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Total Operation Cost</a></li>
                                                <li><a href="/throttle/totaloperationcostperbill.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Total Operation Cost (Bill)</a></li>

                                                <li><a href="/throttle/vehiclewiseprofit.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Vehicle Wise Profit</a></li>
                                                <li><a href="/throttle/fcwiseprofit.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;FC Wise Profit</a></li>
                                                <li><a href="/throttle/consolidatedreportpermonth.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Month Wise Consolidated</a></li>
                                                <li><a href="/throttle/consolidatedreportperbill.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Month Wise Consolidated (Bill)</a></li>
                                            </ul>
                                        </li>
                                        <%} else if (rolId.equals(1038)) {%>
                                        <li><a href="">&nbsp;Sales/Ops</a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/handleViewCustomer.do" target="frame">&nbsp;Customer Master</a></li>

                                                <li><a href="/throttle/consignmentNoteView.do" target="frame">&nbsp;C-Note View</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;Operation</a>
                                            <ul class="acitem">

                                                <li><a href="/throttle/viewVehicleAvailability.do?tripType=1" target="frame">&nbsp;&nbsp;Vehicle Availability</a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=10&admin=No" target="frame">&nbsp;View Trip Sheet</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;Finance</a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/viewclosedtrip.do" target="frame">&nbsp;Generate Billing</a></li>
                                                <li><a href="/throttle/viewbillpageforsubmit.do" target="frame">&nbsp; Submit Bill </a></li>
                                                <li><a href="/throttle/viewbillpage.do" target="frame">&nbsp; View Bill </a></li>
                                                <li><a href="/throttle/customerBillPayment.do" target="frame">&nbsp; Customer Bill Payment</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;Reports</a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/handleAccountsReceivableReport.do?param=search" target="frame">&nbsp;Accounts Receivable</a></li>
                                                <li><a href="/throttle/handleVehicleWiseProfitReport.do?param=search" target="frame">&nbsp;Vehicle Wise Profit</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;MIS</a>
                                            <ul class="acitem">


                                                <li><a href="/throttle/vehiclerevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Vehicle Wise Revenue </a></li>
                                                <li><a href="/throttle/vehicletyperevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Vehicle Type Wise Revenue</a></li>

                                                <li><a href="/throttle/totaloperationcost.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Total Operation Cost</a></li>
                                                <li><a href="/throttle/totaloperationcostperbill.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Total Operation Cost (Bill)</a></li>

                                                <li><a href="/throttle/vehiclewiseprofit.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Vehicle Wise Profit</a></li>
                                                <li><a href="/throttle/fcwiseprofit.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;FC Wise Profit</a></li>
                                                <li><a href="/throttle/consolidatedreportpermonth.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Month Wise Consolidated</a></li>
                                                <li><a href="/throttle/consolidatedreportperbill.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Month Wise Consolidated (Bill)</a></li>
                                            </ul>
                                        </li>
                                        <%} else if (rolId.equals(1023)) {%>
                                        <li><a href="">&nbsp;<spring:message code="subMenu.label.Sales/Ops"  text="default text"/></a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/handleViewCustomer.do?customerType=1" target="frame">&nbsp;<spring:message code="subMenu.label.Customers"  text="default text"/> </a></li>
                                                <li><a href="/throttle/handleViewCustomer.do?customerType=1" target="frame">&nbsp;<spring:message code="subMenu.label.TransportCustomers"  text="default text"/> </a></li>
                                                <li><a href="/throttle/consignmentNote.do" target="frame">&nbsp;<spring:message code="subMenu.label.C-NoteCreate"  text="default text"/></a></li>
                                                <li><a href="/throttle/consignmentNoteView.do" target="frame">&nbsp;<spring:message code="subMenu.label.C-NoteView"  text="default text"/></a></li>
                                            </ul>
                                        </li>


                                        <li><a href="">&nbsp;<spring:message code="subMenu.label.PrimaryOperation"  text="default text"/></a>
                                            <ul class="acitem">

                                                <!--<li><a href="/throttle/manageSecurityPage.do" target="frame">&nbsp;Security</a></li>
                                                <li><a href="/throttle/freeServiceDues.do" target="frame">&nbsp;Free Services Dues</a></li>-->
                                                <li><a href="/throttle/tripPlanning.do?tripType=1" target="frame">&nbsp;<spring:message code="subMenu.label.TripPlanning"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=8&tripType=1&admin=No" target="frame">&nbsp;<spring:message code="subMenu.label.TripStart"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=10&tripType=1&admin=No" target="frame">&nbsp;<spring:message code="subMenu.label.TripEnd"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=12&tripType=1&admin=No" target="frame">&nbsp;<spring:message code="subMenu.label.TripClosure"  text="default text"/></a></li>
                                                <li><a href="/throttle/getTollGateDetail.do" target="frame">&nbsp;<spring:message code="subMenu.label.TollGate"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewRouteDetails.do" target="frame">&nbsp;<spring:message code="subMenu.label.Routes"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewVehicleAvailability.do?tripType=1" target="frame">&nbsp;<spring:message code="subMenu.label.VehicleAvailability"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewTrailerAvailability.do?tripType=1" target="frame">&nbsp;<spring:message code="subMenu.label.TrailerAvailability"  text="default text"/></a></li>
                                                <li><a href="/throttle/getContainerVisibility.do?tripType=1" target="frame">&nbsp;<spring:message code="subMenu.label.ContainerVisibility"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewTripPlannedVehicle.do" target="frame">&nbsp;<spring:message code="subMenu.label.TripCreation(XLSUpload)"  text="default text"/></a></li>
                                                <li><a href="/throttle/emptyTripPlanning.do?tripType=1" target="frame">&nbsp;<spring:message code="subMenu.label.EmptyTrip"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=10&tripType=1&admin=No" target="frame">&nbsp;<spring:message code="subMenu.label.ViewTripSheet"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewVehicleDriverAdvance.do?usageTypeId=2" target="frame">&nbsp;<spring:message code="subMenu.label.VehicleDriverAdvance"  text="default text"/></a></li>
                                                <li><a href="/throttle/handlefinanceAdvice.do?tripType=1" target="frame">&nbsp;<spring:message code="subMenu.label.FinanceAdvice(Adv)"  text="default text"/></a></li>
                                                <li><a href="/throttle/handlefinanceRequest.do" target="frame">&nbsp;<spring:message code="subMenu.label.FinanceAdviceApproval"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleTripClosureRequest.do" target="frame">&nbsp;<spring:message code="subMenu.label.TripClosureApproval"  text="default text"/></a></li>
                                                <li><a href="/throttle/manualEmptyTripApproval.do" target="frame">&nbsp;<spring:message code="subMenu.label.EmptyTripApproval"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=6&tripType=1&admin=No" target="frame">&nbsp;<spring:message code="subMenu.label.TripAssignVehicle"  text="default text"/></a></li>
                                                <!--<li><a href="/throttle/viewTripSheets.do?statusId=7&tripType=1" target="frame">&nbsp;Trip Freeze</a></li>-->
                                                <li><a href="/throttle/viewTripSheets.do?statusId=8&tripType=1&admin=No" target="frame">&nbsp;<spring:message code="subMenu.label.TripUnFreeze"  text="default text"/></a></li>
                                                <!--<li><a href="/throttle/viewTripSheets.do?statusId=8&tripType=1" target="frame">&nbsp;Trip Pre Start</a></li>-->
                                                <li><a href="/throttle/viewTripSheets.do?statusId=23&tripType=1&admin=No" target="frame">&nbsp;<spring:message code="subMenu.label.TripAtPoint"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=18&tripType=1&admin=No" target="frame">&nbsp;<spring:message code="subMenu.label.TripWFU"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=12&tripType=1&admin=No" target="frame">&nbsp;<spring:message code="subMenu.label.TripEnded"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=100&tripType=1&admin=No" target="frame">&nbsp;<spring:message code="subMenu.label.TripPOD"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewTripSheetsForChallan.do?tripType=1&admin=No&documentRequired=Y" target="frame">&nbsp;<spring:message code="subMenu.label.UploadExpenseBillCopy"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=12&tripType=1&admin=Yes" target="frame">&nbsp;<spring:message code="subMenu.label.TripClosure(Admin)"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=13&tripType=1&admin=No" target="frame">&nbsp;<spring:message code="subMenu.label.TripSettlement"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=14&tripType=1&admin=No" target="frame">&nbsp;<spring:message code="subMenu.label.SettledTrips"  text="default text"/></a></li>
                                                <li><a href="/throttle/handlePrimaryDriverSettlement.do" target="frame">&nbsp;<spring:message code="subMenu.label.DriverSettlement"  text="default text"/></a></li>
                                                <li><a href="/throttle/emptyTripMerging.do" target="frame">&nbsp;<spring:message code="subMenu.label.EmptyTripMerging"  text="default text"/></a></li>
                                                <li><a href="/throttle/userCustomer.do" target="frame">&nbsp;<spring:message code="subMenu.label.UserCustomer"  text="default text"/></a></li>
                                                <li><a href="/throttle/vendorPayments.do" target="frame">&nbsp;<spring:message code="subMenu.label.VendorPayment"  text="default text"/></a></li>
                                                <!--<li><a href="/throttle/marketVehicleDetail.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Market Vehicle Settlement</a></li>-->

                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;<spring:message code="subMenu.label.SecondaryOperation"  text="default text"/></a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/handleViewCustomer.do?customerType=2" target="frame">&nbsp;<spring:message code="subMenu.label.Customers"  text="default text"/> </a></li>
                                                <li><a href="/throttle/handleEditViewCustomer.do" target="frame">&nbsp;<spring:message code="subMenu.label.Routes"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewVehicleAvailability.do?tripType=2" target="frame">&nbsp;<spring:message code="subMenu.label.VehicleAvailability"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewVehicleDriverAdvance.do?usageTypeId=1" target="frame">&nbsp;<spring:message code="subMenu.label.VehicleDriverAdvance"  text="default text"/></a></li>
                                                <li><a href="/throttle/secondaryTripSheduleView.do?tripType=2" target="frame">&nbsp;<spring:message code="subMenu.label.ScheduleTrip"  text="default text"/></a></li>
                                                <li><a href="/throttle/emptyTripPlanning.do?tripType=2" target="frame">&nbsp;<spring:message code="subMenu.label.EmptyTrip"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=10&tripType=2&admin=No" target="frame"><spring:message code="subMenu.label.ViewTripSheet"  text="default text"/></a></li>
                                                <li><a href="/throttle/handlefinanceAdvice.do?tripType=2" target="frame">&nbsp;<spring:message code="subMenu.label.FinanceAdvice(Adv)"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=6&tripType=2&admin=No" target="frame">&nbsp;<spring:message code="subMenu.label.TripAssignVehicle"  text="default text"/></a></li>
                                                <!-- <li><a href="/throttle/viewTripSheets.do?statusId=7&tripType=2" target="frame">&nbsp;Trip Freeze</a></li>-->
                                                <li><a href="/throttle/viewTripSheets.do?statusId=8&tripType=2&admin=No" target="frame">&nbsp;<spring:message code="subMenu.label.TripUnFreeze"  text="default text"/></a></li>
                                                <!--<li><a href="/throttle/viewTripSheets.do?statusId=8&tripType=2" target="frame">&nbsp;Trip Pre Start</a></li>-->
                                                <li><a href="/throttle/viewTripSheets.do?statusId=8&tripType=2&admin=No" target="frame">&nbsp;<spring:message code="subMenu.label.TripStart"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=10&tripType=2&admin=No" target="frame">&nbsp;<spring:message code="subMenu.label.TripEnd"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=12&tripType=2&admin=No" target="frame">&nbsp;<spring:message code="subMenu.label.TripClosure"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=12&tripType=2&admin=Yes" target="frame">&nbsp;<spring:message code="subMenu.label.TripFCClosed"  text="default text"/></a></li>
                                                <li><a href="/throttle/secondaryDriverSettlement.do" target="frame">&nbsp;<spring:message code="subMenu.label.SecondaryDriverSettlement"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewSecondaryDriverSettlement.do" target="frame">&nbsp;<spring:message code="subMenu.label.ViewSecondarySettlement"  text="default text"/></a></li>
                                                <li><a href="/throttle/secondaryCustomerApprovalMailId.do?" target="frame">&nbsp;<spring:message code="subMenu.label.SecondaryApprovalMail"  text="default text"/></a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;<spring:message code="subMenu.label.LeasingOperation"  text="default text"/></a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/handleViewCustomer.do?customerType=1" target="frame">&nbsp;<spring:message code="subMenu.label.Customers"  text="default text"/></a></li>
                                                <!--                                                <li><a href="/throttle/handleEditViewCustomer.do" target="frame">&nbsp;Routes</a></li>
                                                                                                <li><a href="/throttle/viewVehicleAvailability.do?tripType=2" target="frame">&nbsp;&nbsp;Vehicle Availability</a></li>
                                                                                                <li><a href="/throttle/viewVehicleDriverAdvance.do?usageTypeId=1" target="frame">&nbsp;VehicleDriverAdvance</a></li>-->
                                                <li><a href="/throttle/secondaryLeasingSheduleView.do?tripType=2" target="frame">&nbsp;<spring:message code="subMenu.label.VehicleSchedule"  text="default text"/></a></li>
                                                <!--                                                <li><a href="/throttle/emptyTripPlanning.do?tripType=2" target="frame">&nbsp;Empty Trip</a></li>-->
                                                <li><a href="/throttle/viewTripSheets.do?statusId=8&tripType=3&admin=No" target="frame">&nbsp;<spring:message code="subMenu.label.TripStart"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=10&tripType=3&admin=No" target="frame">&nbsp;<spring:message code="subMenu.label.ViewTripSheet"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=10&tripType=3&admin=No" target="frame">&nbsp;<spring:message code="subMenu.label.TripEnd"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=12&tripType=3&admin=No" target="frame">&nbsp;<spring:message code="subMenu.label.TripClosure"  text="default text"/></a></li>
                                                <li><a href="/throttle/handlefinanceAdvice.do?tripType=3" target="frame">&nbsp;<spring:message code="subMenu.label.FinanceAdvice(Adv)"  text="default text"/></a></li>
                                                 <li><a href="/throttle/handlefinanceRequest.do" target="frame">&nbsp;<spring:message code="subMenu.label.FinanceAdviceApproval"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=13&tripType=3&admin=No" target="frame">&nbsp;<spring:message code="subMenu.label.TripSettlement"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=14&tripType=3&admin=No" target="frame">&nbsp;<spring:message code="subMenu.label.SettledTrips"  text="default text"/></a></li>
                                                <!--                                                <li><a href="/throttle/viewTripSheets.do?statusId=6&tripType=2&admin=No" target="frame">&nbsp;Trip Assign Vehicle</a></li>-->
                                                <!-- <li><a href="/throttle/viewTripSheets.do?statusId=7&tripType=2" target="frame">&nbsp;Trip Freeze</a></li>-->
                                                <!--                                                <li><a href="/throttle/viewTripSheets.do?statusId=8&tripType=2&admin=No" target="frame">&nbsp;Trip Un Freeze</a></li>-->
                                                <!--<li><a href="/throttle/viewTripSheets.do?statusId=8&tripType=2" target="frame">&nbsp;Trip Pre Start</a></li>-->
                                                <!--                                                <li><a href="/throttle/viewTripSheets.do?statusId=12&tripType=2&admin=Yes" target="frame">&nbsp;Trip FC Closed</a></li>-->
                                                <li><a href="/throttle/secondaryDriverSettlement.do" target="frame">&nbsp;<spring:message code="subMenu.label.DriverSettlement"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewSecondaryDriverSettlement.do" target="frame">&nbsp;<spring:message code="subMenu.label.ViewSettlement"  text="default text"/> </a></li>
                                                <!--                                                <li><a href="/throttle/secondaryCustomerApprovalMailId.do?" target="frame">&nbsp;Approval Mail</a></li>-->
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;<spring:message code="subMenu.label.PrimaryBilling"  text="default text"/></a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/uploadCustomeroutStanding.do" target="frame"><spring:message code="subMenu.label.UploadCustomerOutstanding"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewclosedtrip.do" target="frame"><spring:message code="subMenu.label.GenerateBilling"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewbillpageforsubmit.do?tripType=1" target="frame"><spring:message code="subMenu.label.ViewBill"  text="default text"/> </a></li>
                                                <!--                                            <li><a href="/throttle/viewbillpage.do" target="frame">&nbsp; View Bill </a></li>-->
                                                <li><a href="/throttle/customerBillPayment.do" target="frame"><spring:message code="subMenu.label.CustomerBillPayment"  text="default text"/></a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;<spring:message code="subMenu.label.LeasingBilling"  text="default text"/></a>
                                            <ul class="acitem">
                                                <!--                                                <li><a href="/throttle/uploadCustomeroutStanding.do" target="frame">&nbsp;Upload Customer Outstanding</a></li>-->
                                                <li><a href="/throttle/viewclosedLeasedtrip.do" target="frame">&nbsp;<spring:message code="subMenu.label.GenerateBilling"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewbillpageforsubmit.do?tripType=1" target="frame">&nbsp;<spring:message code="subMenu.label.ViewBill"  text="default text"/>  </a></li>
                                                <!--                                            <li><a href="/throttle/viewbillpage.do" target="frame">&nbsp; View Bill </a></li>-->
                                                <li><a href="/throttle/customerBillPayment.do" target="frame">&nbsp;<spring:message code="subMenu.label.CustomerBillPayment"  text="default text"/> </a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;<spring:message code="subMenu.label.SecondaryBilling"  text="default text"/></a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/viewClosedTripForSecondaryBilling.do" target="frame">&nbsp;<spring:message code="subMenu.label.GenerateSecondaryBilling"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewbillpageforsubmit.do?tripType=2" target="frame">&nbsp;<spring:message code="subMenu.label.ViewBill"  text="default text"/> </a></li>
                                                <!--                                            <li><a href="/throttle/viewbillpage.do" target="frame">&nbsp; View Bill </a></li>-->
                                                <li><a href="/throttle/customerBillPayment.do" target="frame">&nbsp;<spring:message code="subMenu.label.CustomerBillPayment"  text="default text"/> </a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;<spring:message code="subMenu.label.CashCardTxn"  text="default text"/></a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/vehicleBPCLCardMapping.do" target="frame">&nbsp;<spring:message code="subMenu.label.VehicleCashCardMapping"  text="default text"/></a></li>
                                                <li><a href="/throttle/uploadBPCLData.do" target="frame">&nbsp;<spring:message code="subMenu.label.UploadCashCardTxn"  text="default text"/></a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;<spring:message code="subMenu.label.VehicleSchedule"  text="default text"/></a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/handleLatestUpdates.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.LatestUpdates"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewTicketingDetails.do" target="frame">&nbsp;<spring:message code="subMenu.label.Ticketing"  text="default text"/></a></li>
                                                <li><a href="/throttle/mailDelivered.do" target="frame">&nbsp;<spring:message code="subMenu.label.MailDelivered"  text="default text"/></a></li>
                                                <li><a href="/throttle/mailNotDelivered.do" target="frame">&nbsp;<spring:message code="subMenu.label.MailNotDelivered"  text="default text"/></a></li>
                                            </ul>
                                        </li>

                                        <li><a href="">&nbsp;<spring:message code="subMenu.label.FleetMgmt"  text="default text"/></a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/periodicServiceDues.do" target="frame">&nbsp;<spring:message code="subMenu.label.PeriodicServiceDues"  text="default text"/></a></li>
                                                <li><a href="/throttle/WOstatus.do" target="frame">&nbsp;<spring:message code="subMenu.label.CreateServiceWorkOrder"  text="default text"/></a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;<spring:message code="subMenu.label.Drivers"  text="default text"/></a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/vehicleDriverPlanning.do" target="frame">&nbsp;<spring:message code="subMenu.label.Vehicle&DriverMapping"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleViewPrimaryDriver.do" target="frame">&nbsp;<spring:message code="subMenu.label.ManageDriver"  text="default text"/></a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;<spring:message code="subMenu.label.Alerts&EmailSettings"  text="default text"/></a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/tripEmailSettings.do" target="frame">&nbsp;<spring:message code="subMenu.label.TripEmailSettings"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewAlerts.do" target="frame">&nbsp;<spring:message code="subMenu.label.Alerts"  text="default text"/></a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;<spring:message code="subMenu.label.Masters"  text="default text"/></a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/viewStandardCharge.do" target="frame">&nbsp;<spring:message code="subMenu.label.StandardChargeMaster"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewCheckList.do" target="frame">&nbsp;<spring:message code="subMenu.label.CheckListMaster"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewProductCategory.do" target="frame">&nbsp;<spring:message code="subMenu.label.ProductCategoryMaster"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewConfigMaster.do" target="frame">&nbsp;<spring:message code="subMenu.label.ConfigMaster"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewCityMaster.do" target="frame">&nbsp;&nbsp;<spring:message code="subMenu.label.CityMaster"  text="default text"/></a></li>
                                                <!--gulshan-16/12/15-->
                                                <li><a href="/throttle/countryMaster.do" target="frame">&nbsp;&nbsp;<spring:message code="subMenu.label.CountryMaster"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewCurrencySymbolMaster.do" target="frame">&nbsp;&nbsp;<spring:message code="subMenu.label.CurrencyMaster"  text="default text"/></a></li>
                                                <li><a href="/throttle/viewCurrencyRateofExchange.do" target="frame">&nbsp;&nbsp;<spring:message code="subMenu.label.CurrencyRateOfExchange"  text="default text"/></a></li>
                                                <!--gulshan-10/12/15-->
                                                <li><a href="/throttle/zoneMaster.do" target="frame">&nbsp;&nbsp;<spring:message code="subMenu.label.ZoneMaster"  text="default text"/></a></li>
                                                <li><a href="/throttle/slabMaster.do" target="frame">&nbsp;&nbsp;<spring:message code="subMenu.label.SlabMaster"  text="default text"/></a></li>
                                                <li><a href="/throttle/expenseMaster.do" target="frame">&nbsp;&nbsp;<spring:message code="subMenu.label.ExpenseMaster"  text="default text"/></a></li>
                                            </ul>
                                        </li>

                                        <li><a href="">&nbsp;<spring:message code="subMenu.label.Reports"  text="default text"/></a>
                                            <ul class="acitem">
                                                <!--<li><a href="/throttle/handleLatestUpdates.do?param=search" target="frame">&nbsp;Latest Updates</a></li>-->
                                                <li><a href="/throttle/handleAccountsReceivableReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.AccountsReceivable"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleVehicleWiseProfitReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.VehicleWiseProfit"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleCustomerWiseProfitReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.CustomerwiseProfit"  text="default text"/> </a></li>
                                                <li><a href="/throttle/handleVehicleUtilizationReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.VehicleUtilizationReport"  text="default text"/> </a></li>
                                                <li><a href="/throttle/handleTripWiseProfitReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.TripWiseProfitabilityReport(Closure)"  text="default text"/> </a></li>
                                                <li><a href="/throttle/handleTripWiseProfitReportEndStatus.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.TripWiseProfitabilityReport(End)"  text="default text"/> </a></li>
                                                <li><a href="/throttle/handleGPSLogReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.GPSLogReport"  text="default text"/> </a></li>
                                                <li><a href="/throttle/handleDriverSettlementDetails.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.DriverSettlementReport"  text="default text"/> </a></li>
                                                <li><a href="/throttle/handleBPCLTransactionDetails.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.BPCLTransactionReport"  text="default text"/> </a></li>
                                                <li><a href="/throttle/viewFinanceAdviceDetais.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.FinanceAdviceReport"  text="default text"/> </a></li>
                                                <li><a href="/throttle/viewGpsStatusDetais.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.GPSStatusReport"  text="default text"/> </a></li>
                                                <li><a href="/throttle/viewWFLWFUDetais.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.WFL&WFUMailSent"  text="default text"/></a></li>
                                                <li><a href="/throttle/dashboardReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.Dashboard"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleViewTripSheet.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.TripSheetReport"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleViewTripSheetWfl.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.TripSheetReportWithWfl"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleFCWiseTripSummary.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.FCTripSummary"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleMarginWiseReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.MarginTripSummary"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleMonthWiseEmptyRunSummary.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.EmptyRunSummary"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleJobcardSumary.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.JobcardSummary"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleTripMergingDetails.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.TripMergingReport"  text="default text"/> </a></li>
                                                <li><a href="/throttle/handleVehicleOdometerReading.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.VehicleCurrentOdometerReadingReport"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleCustomerWiseMergingProfitReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.CustomerWiseProfitMerging"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleTripExtraExpenseReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.TripExtraExpenseReport"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleVehicleDriverAdvance.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.VehicleDriverAdvanceReport"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleToPayCustomerTripDetails.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.TopaycustomertripDetailsReport"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleTripVmrReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.TripVMRReport"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleTripBudgetReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.FCPerformanceReport"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleAccountMgrPerformanceReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.A/CMgrPerformanceReport"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleRNMExpenseReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.R&MSpentAnalysisReport"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleTyerExpenseReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.TyreAnalysisReport"  text="default text"/></a></li>
                                                <li><a href="/throttle/report.do" target="frame">&nbsp;<spring:message code="subMenu.label.TruckDriverMappingReport"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleOrderExpenseRevenueReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.OrderExpenseRevenueReport"  text="default text"/></a></li>
                                                <li><a href="/throttle/handletripTrailerProfitReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.TripTrailerProfitReport"  text="default text"/></a></li>

                                                <!--<li><a href="/throttle/handleTripWiseProfitReport.do" target="frame">&nbsp; Trip wise Profit</a></li>
                                               <li><a href="/throttle/handleTripWiseProfit1.do" target="frame">&nbsp; Trip wise Profit-1</a></li>-->
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;<spring:message code="subMenu.label.MIS"  text="default text"/></a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/handlecustomerrevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;<spring:message code="subMenu.label.CustomerWiseRevenue"  text="default text"/></a></li>
                                                <li><a href="/throttle/customerrevenuebillwise.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;<spring:message code="subMenu.label.BillWiseRevenue"  text="default text"/> </a></li>
                                                <li><a href="/throttle/revenuefcwise.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp; <spring:message code="subMenu.label.FCWiseRevenue"  text="default text"/> </a></li>
                                                <li><a href="/throttle/vehiclerevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;<spring:message code="subMenu.label.VehicleWiseRevenue"  text="default text"/></a></li>
                                                <li><a href="/throttle/vehicletyperevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;<spring:message code="subMenu.label.VehicleTypeWiseRevenue"  text="default text"/></a></li>
                                                <li><a href="/throttle/accountreceivablerevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;<spring:message code="subMenu.label.CustomerWiseA.R"  text="default text"/></a></li>
                                                <li><a href="/throttle/accountreceivablebillwiserevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;<spring:message code="subMenu.label.BillWiseA.R"  text="default text"/></a></li>
                                                <li><a href="/throttle/totaloperationcost.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;<spring:message code="subMenu.label.TotalOperationCost"  text="default text"/></a></li>
                                                <li><a href="/throttle/totaloperationcostperbill.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;<spring:message code="subMenu.label.TotalOperationCost(Bill)"  text="default text"/></a></li>
                                                <li><a href="/throttle/fuelcostperbill.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;<spring:message code="subMenu.label.FuelCost"  text="default text"/></a></li>
                                                <li><a href="/throttle/customerwiseprofit.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;<spring:message code="subMenu.label.CustomerWiseProfit"  text="default text"/></a></li>
                                                <li><a href="/throttle/vehiclewiseprofit.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;<spring:message code="subMenu.label.VehicleWiseProfit"  text="default text"/></a></li>
                                                <li><a href="/throttle/fcwiseprofit.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;<spring:message code="subMenu.label.FCWiseProfit"  text="default text"/></a></li>
                                                <li><a href="/throttle/consolidatedreportpermonth.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;<spring:message code="subMenu.label.MonthWiseConsolidated"  text="default text"/></a></li>
                                                <li><a href="/throttle/consolidatedreportperbill.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;<spring:message code="subMenu.label.MonthWiseConsolidated(Bill)"  text="default text"/></a></li>
                                            </ul>
                                        </li>
                                        <%} else if (rolId.equals(1048)) {%>
                                        <li><a href="">&nbsp;Primary Operation</a>
                                            <ul class="acitem">

                                                <!--<li><a href="/throttle/manageSecurityPage.do" target="frame">&nbsp;Security</a></li>
                                                <li><a href="/throttle/freeServiceDues.do" target="frame">&nbsp;Free Services Dues</a></li>-->
                                                <li><a href="/throttle/tripPlanning.do?tripType=1" target="frame">&nbsp;Trip Planning</a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=8&tripType=1&admin=No" target="frame">&nbsp;Trip Start</a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=10&tripType=1&admin=No" target="frame">&nbsp;Trip End</a></li>
                                                <li><a href="/throttle/viewTripSheets.do?statusId=12&tripType=1&admin=No" target="frame">&nbsp;Trip Closure</a></li>
                                            </ul>
                                        </li>


                                        <li><a href="">&nbsp;Fleet Mgmt</a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/periodicServiceDues.do" target="frame">&nbsp;Periodic Service Dues</a></li>
                                                <li><a href="/throttle/WOstatus.do" target="frame">&nbsp;Create Service Work Order</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;Drivers</a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/vehicleDriverPlanning.do" target="frame">&nbsp;Vehicle &amp; Driver Mapping</a></li>
                                                <li><a href="/throttle/handleViewPrimaryDriver.do" target="frame">&nbsp;Manage Driver</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;Alerts & Email Settings</a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/tripEmailSettings.do" target="frame">&nbsp;TripEmailSettings</a></li>
                                                <li><a href="/throttle/viewAlerts.do" target="frame">&nbsp;Alerts</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;Masters</a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/viewStandardCharge.do" target="frame">&nbsp;StandardCharge Master</a></li>
                                                <li><a href="/throttle/viewCheckList.do" target="frame">&nbsp;Check List Master</a></li>
                                                <li><a href="/throttle/viewProductCategory.do" target="frame">&nbsp;Product Category Master</a></li>
                                                <li><a href="/throttle/viewConfigMaster.do" target="frame">&nbsp;Config Master</a></li>
                                                <li><a href="/throttle/viewCityMaster.do" target="frame">&nbsp;&nbsp;City Master</a></li>
                                                <!--gulshan-16/12/15-->
                                                <li><a href="/throttle/countryMaster.do" target="frame">&nbsp;&nbsp;Country Master</a></li>
                                                <li><a href="/throttle/viewCurrencySymbolMaster.do" target="frame">&nbsp;&nbsp;Currency Master</a></li>
                                                <li><a href="/throttle/viewCurrencyRateofExchange.do" target="frame">&nbsp;&nbsp;Currency Rate of Exchange</a></li>
                                                <!--gulshan-10/12/15-->
                                                <li><a href="/throttle/zoneMaster.do" target="frame">&nbsp;&nbsp;Zone Master</a></li>
                                                <li><a href="/throttle/slabMaster.do" target="frame">&nbsp;&nbsp;Slab Master</a></li>



                                            </ul>
                                        </li>
                                        <%}%>

                                </c:forEach>
                            </c:if>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>

        <div id="tyresM" style="display: none;">
            <table id="tyresMenu"  cellpadding="0" cellspacing="0" align="center" border="0">
                <tr>
                    <td>
                        <ul class="menu">
                            <%if (rolId.equals(1030)) {%>
                            <% } else if (rolId.equals(1033)) {%>
                            <% } else if (rolId.equals(1032)) {%>
                            <% } else if (rolId.equals(1031)) {%>
                            <% } else if (rolId.equals(1036)) {%>
                            <% } else if (rolId.equals(1035)) {%>
                            <li><a href="/throttle/manageParts.do" target="frame">&nbsp;Manage Tyres</a></li>
                            <!--<li><a href="/throttle/handleVehicleList.do" target="frame">&nbsp;Tyres Rotation</a></li>  -->
                            <li><a href="/throttle/handleTyresRotationsList.do" target="frame">&nbsp;Tyres Rotation</a></li>
                            <li><a href="/throttle/tyresReportByVehicleNo.do" target="frame">&nbsp;Tyres Report By Vehicle</a></li>
                            <li><a href="/throttle/tyresReportByTyreNo.do" target="frame">&nbsp;Tyres Report By Tyre No</a></li>
                            <li><a href="/throttle/stockPurchaseRep.do" target="frame">&nbsp;Tyres Purchase Report</a></li>
                            <li><a href="/throttle/stockIssueRep.do" target="frame">&nbsp;Tyres Issue Report</a></li>
                            <li><a href="/throttle/stockWorthPage.do" target="frame">&nbsp;Tyres Stock Worth Report</a></li>
                                <% } else {%>
                            <li><a href="/throttle/manageParts.do" target="frame">&nbsp;Manage Tyres</a></li>
                            <!--<li><a href="/throttle/handleVehicleList.do" target="frame">&nbsp;Tyres Rotation</a></li>  -->
                            <li><a href="/throttle/handleTyresRotationsList.do" target="frame">&nbsp;Tyres Rotation</a></li>
                            <li><a href="/throttle/tyresReportByVehicleNo.do" target="frame">&nbsp;Tyres Report By Vehicle</a></li>
                            <li><a href="/throttle/tyresReportByTyreNo.do" target="frame">&nbsp;Tyres Report By Tyre No</a></li>
                            <li><a href="/throttle/stockPurchaseRep.do" target="frame">&nbsp;Tyres Purchase Report</a></li>
                            <li><a href="/throttle/stockIssueRep.do" target="frame">&nbsp;Tyres Issue Report</a></li>
                            <li><a href="/throttle/stockWorthPage.do" target="frame">&nbsp;Tyres Stock Worth Report</a></li>
                                <%}%>
                        </ul>


                    </td>
                </tr>
            </table>
        </div>

        <div id="renderserviceM" style="display: none;">
            <table id="renderserviceMenu"  cellpadding="0" cellspacing="0" align="center" border="0">
                <tr>
                    <td>
                        <c:if test = "${menu.name=='RenderService'}">
                            <ul class="menu">

                                <c:if test="${menu.renderService!=null}">
                                    <!--
                                    <li class="imatm"  style="width:90px; padding-right:0px;"><a href=""><span class="imea imeam"><span></span></span>Other Vehicle</a>


        <div class="imsc" style="padding-right:0px; "><div class="imsubc" style="width:95px;top:0px;left:0px; padding-right:0px;"><ul style="">
                        <c:forEach items="${menu.renderService}" var="renderService">
                            <c:if test = "${renderService.subMenu =='CreateJobCard'}" >
                                <li class="lineDiffer" style="margin-top:2px; "><a href="/throttle/vehicleStatusPage.do" target="frame">Vehicle Status</a></li>
                                <li class="lineDiffer" style="margin-top:2px; "><a href="/throttle/createDirectJobcard.do" target="frame">Create Jobcard</a></li>
                            </c:if>
                            <c:if test = "${renderService.subMenu =='WOApproval'}" >
                                <li class="lineDiffer" style="margin-top:2px; "><a href="/throttle/wMpermission.do" target="frame">Work Plan</a></li>
                            </c:if>
                        </c:forEach>
                    </c:if>

                                        </ul></div></div></li>
                                    -->

                                    <!--RAJ KUMAR-->

                                    <%if (rolId.equals(1030)) {%>
                                    <%} else if (rolId.equals(1031)) {%>
                                    <%} else if (rolId.equals(1036)) {%>
                                    <%} else if (rolId.equals(1035) || rolId.equals(1042)) {%>
                                    <c:if test="${menu.renderService!=null}">
                                        <c:forEach items="${menu.renderService}" var="renderService">
                                            <c:if test = "${renderService.subMenu =='WOApproval'}" >
                                                <li><a href="/throttle/workManagerApprove.do" target="frame">&nbsp;WO Approval</a></li>
                                            </c:if>
                                            <c:if test = "${renderService.subMenu =='WorkOrderSchedule'}" >
                                                <!--<li><a href="/throttle/serviceEstimate.do" target="frame">&nbsp;Estimate</a></li>-->
                                                <li><a href="/throttle/checkWorkOrder.do?scheduleDate=<%=session.getAttribute("currentDate")%>" target="frame">&nbsp;SP Schedule</a></li>
                                            </c:if>
                                            <c:if test = "${renderService.subMenu =='ViewJobCard'}" >
                                                <li><a href="/throttle/supervisorView.do" target="frame">&nbsp;View Job Card</a></li>
                                            </c:if>
                                            <c:if test = "${renderService.subMenu =='CreateJobCard'}" >
                                                <li><a href="/throttle/vehicleComplaintHist.do" target="frame">&nbsp;Veh Compliant History</a></li>
                                                <li><a href="/throttle/createJobcard.do" target="frame">&nbsp;Create Job Card</a></li>
                                            </c:if>
                                            <c:if test = "${renderService.subMenu =='GenerateMrs'}" >
                                                <li><a href="/throttle/chooseJobCard.do" target="frame">&nbsp;Generate MRS</a></li>
                                            </c:if>
                                            <c:if test = "${renderService.subMenu =='CloseJobCard'}" >
                                                <li><a href="/throttle/closeJobCardView.do" target="frame">&nbsp;Close Job Card</a></li>
                                            </c:if>
                                            <c:if test = "${renderService.subMenu =='GenerateBill'}" >
                                                <li><a href="/throttle/ViewJobCardsForBilling.do" target="frame">&nbsp;Generate-Bill</a></li>
                                                <li><a href="/throttle/receiveBodyWOBill.do" target="frame">&nbsp;Receive Contract WO Bill</a></li>
                                            </c:if>
                                            <% } else {%>
                                            <c:if test="${menu.renderService!=null}">
                                                <c:forEach items="${menu.renderService}" var="renderService">
                                                    <c:if test = "${renderService.subMenu =='WOApproval'}" >
                                                        <li><a href="/throttle/workManagerApprove.do" target="frame">&nbsp;WO Approval</a></li>
                                                    </c:if>
                                                    <c:if test = "${renderService.subMenu =='WorkOrderSchedule'}" >
                                                        <!--<li><a href="/throttle/serviceEstimate.do" target="frame">&nbsp;Estimate</a></li>-->
                                                        <li><a href="/throttle/checkWorkOrder.do?scheduleDate=<%=session.getAttribute("currentDate")%>" target="frame">&nbsp;SP Schedule</a></li>
                                                    </c:if>
                                                    <c:if test = "${renderService.subMenu =='ViewJobCard'}" >
                                                        <li><a href="/throttle/supervisorView.do" target="frame">&nbsp;View Job Card</a></li>
                                                    </c:if>
                                                    <c:if test = "${renderService.subMenu =='CreateJobCard'}" >
                                                        <li><a href="/throttle/vehicleComplaintHist.do" target="frame">&nbsp;Veh Compliant History</a></li>
                                                        <li><a href="/throttle/createJobcard.do" target="frame">&nbsp;Create Job Card</a></li>
                                                    </c:if>
                                                    <c:if test = "${renderService.subMenu =='GenerateMrs'}" >
                                                        <li><a href="/throttle/chooseJobCard.do" target="frame">&nbsp;Generate MRS</a></li>
                                                    </c:if>
                                                    <c:if test = "${renderService.subMenu =='CloseJobCard'}" >
                                                        <li><a href="/throttle/closeJobCardView.do" target="frame">&nbsp;Close Job Card</a></li>
                                                    </c:if>
                                                    <c:if test = "${renderService.subMenu =='GenerateBill'}" >
                                                        <!--                                                        <li><a href="/throttle/viewSubGroup.do" target="frame">&nbsp;Manage Bill SubGroup</a></li>-->
                                                        <li><a href="/throttle/ViewJobCardsForBilling.do" target="frame">&nbsp;Generate-Bill</a></li>
                                                        <li><a href="/throttle/ViewJobCardsBilling.do" target="frame">&nbsp;View-Bill</a></li>
                                                        <!--                                                         <li><a href="/throttle/viewTyerDetails.do" target="frame">&nbsp;Tyre Details</a></li>-->
                                                        <!--                                                        <li><a href="/throttle/receiveBodyWOBill.do" target="frame">&nbsp;Receive Contract WO Bill</a></li>-->
                                                    </c:if>
                                                    <%}%>
                                                </c:forEach>
                                            </c:if>
                                            </ul>
                                    </c:if>


                                    </td>
                                    </tr>
                                    </table>
                                    </div>

                                    <div id="reportM" style="display: none;">
                                        <table id="reportMenu"  cellpadding="0" cellspacing="0" align="center" border="0">
                                            <tr>
                                                <td>
                                                    <c:if test = "${menu.name=='Report'}" >
                                                        <ul class="menu">
                                                            <c:if test = "${companyId !='1011'}" >
                                                                 <li><a href="">&nbsp;Operational Reports</a>
                                            <ul class="acitem">
                                                <!--<li><a href="/throttle/handleLatestUpdates.do?param=search" target="frame">&nbsp;Latest Updates</a></li>-->
                                                <li><a href="/throttle/handleAccountsReceivableReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.AccountsReceivable"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleVehicleWiseProfitReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.VehicleWiseProfit"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleCustomerWiseProfitReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.CustomerwiseProfit"  text="default text"/> </a></li>
                                                <li><a href="/throttle/handleVehicleUtilizationReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.VehicleUtilizationReport"  text="default text"/> </a></li>
                                                <li><a href="/throttle/handleTripWiseProfitReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.TripWiseProfitabilityReport(Closure)"  text="default text"/> </a></li>
                                                <li><a href="/throttle/handleTripWiseProfitReportEndStatus.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.TripWiseProfitabilityReport(End)"  text="default text"/> </a></li>
                                                <li><a href="/throttle/handleGPSLogReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.GPSLogReport"  text="default text"/> </a></li>
                                                <li><a href="/throttle/handleDriverSettlementDetails.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.DriverSettlementReport"  text="default text"/> </a></li>
                                                <li><a href="/throttle/handleBPCLTransactionDetails.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.BPCLTransactionReport"  text="default text"/> </a></li>
                                                <li><a href="/throttle/viewFinanceAdviceDetais.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.FinanceAdviceReport"  text="default text"/> </a></li>
                                                <li><a href="/throttle/viewGpsStatusDetais.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.GPSStatusReport"  text="default text"/> </a></li>
                                                <li><a href="/throttle/viewWFLWFUDetais.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.WFL&WFUMailSent"  text="default text"/></a></li>
                                                <li><a href="/throttle/dashboardReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.Dashboard"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleViewTripSheet.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.TripSheetReport"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleViewTripSheetWfl.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.TripSheetReportWithWfl"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleFCWiseTripSummary.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.FCTripSummary"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleMarginWiseReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.MarginTripSummary"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleMonthWiseEmptyRunSummary.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.EmptyRunSummary"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleJobcardSumary.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.JobcardSummary"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleTripMergingDetails.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.TripMergingReport"  text="default text"/> </a></li>
                                                <li><a href="/throttle/handleVehicleOdometerReading.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.VehicleCurrentOdometerReadingReport"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleCustomerWiseMergingProfitReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.CustomerWiseProfitMerging"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleTripExtraExpenseReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.TripExtraExpenseReport"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleVehicleDriverAdvance.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.VehicleDriverAdvanceReport"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleToPayCustomerTripDetails.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.TopaycustomertripDetailsReport"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleTripVmrReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.TripVMRReport"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleTripBudgetReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.FCPerformanceReport"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleAccountMgrPerformanceReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.A/CMgrPerformanceReport"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleRNMExpenseReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.R&MSpentAnalysisReport"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleTyerExpenseReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.TyreAnalysisReport"  text="default text"/></a></li>
                                                <li><a href="/throttle/report.do" target="frame">&nbsp;<spring:message code="subMenu.label.TruckDriverMappingReport"  text="default text"/></a></li>
                                                <li><a href="/throttle/handleOrderExpenseRevenueReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.OrderExpenseRevenueReport"  text="default text"/></a></li>
                                                <li><a href="/throttle/handletripTrailerProfitReport.do?param=search" target="frame">&nbsp;<spring:message code="subMenu.label.TripTrailerProfitReport"  text="default text"/></a></li>

                                                <!--<li><a href="/throttle/handleTripWiseProfitReport.do" target="frame">&nbsp; Trip wise Profit</a></li>
                                               <li><a href="/throttle/handleTripWiseProfit1.do" target="frame">&nbsp; Trip wise Profit-1</a></li>-->
                                            </ul>
                                        </li>
                                        <li><a href="">&nbsp;Operational MIS  </a>
                                            <ul class="acitem">
                                                <li><a href="/throttle/handlecustomerrevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;<spring:message code="subMenu.label.CustomerWiseRevenue"  text="default text"/></a></li>
                                                <li><a href="/throttle/customerrevenuebillwise.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;<spring:message code="subMenu.label.BillWiseRevenue"  text="default text"/> </a></li>
                                                <li><a href="/throttle/revenuefcwise.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp; <spring:message code="subMenu.label.FCWiseRevenue"  text="default text"/> </a></li>
                                                <li><a href="/throttle/vehiclerevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;<spring:message code="subMenu.label.VehicleWiseRevenue"  text="default text"/></a></li>
                                                <li><a href="/throttle/vehicletyperevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;<spring:message code="subMenu.label.VehicleTypeWiseRevenue"  text="default text"/></a></li>
                                                <li><a href="/throttle/accountreceivablerevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;<spring:message code="subMenu.label.CustomerWiseA.R"  text="default text"/></a></li>
                                                <li><a href="/throttle/accountreceivablebillwiserevenue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;<spring:message code="subMenu.label.BillWiseA.R"  text="default text"/></a></li>
                                                <li><a href="/throttle/totaloperationcost.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;<spring:message code="subMenu.label.TotalOperationCost"  text="default text"/></a></li>
                                                <li><a href="/throttle/totaloperationcostperbill.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;<spring:message code="subMenu.label.TotalOperationCost(Bill)"  text="default text"/></a></li>
                                                <li><a href="/throttle/fuelcostperbill.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;<spring:message code="subMenu.label.FuelCost"  text="default text"/></a></li>
                                                <li><a href="/throttle/customerwiseprofit.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;<spring:message code="subMenu.label.CustomerWiseProfit"  text="default text"/></a></li>
                                                <li><a href="/throttle/vehiclewiseprofit.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;<spring:message code="subMenu.label.VehicleWiseProfit"  text="default text"/></a></li>
                                                <li><a href="/throttle/fcwiseprofit.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;<spring:message code="subMenu.label.FCWiseProfit"  text="default text"/></a></li>
                                                <li><a href="/throttle/consolidatedreportpermonth.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;<spring:message code="subMenu.label.MonthWiseConsolidated"  text="default text"/></a></li>
                                                <li><a href="/throttle/consolidatedreportperbill.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;<spring:message code="subMenu.label.MonthWiseConsolidated(Bill)"  text="default text"/></a></li>
                                            </ul>
                                        </li>
                                                                <li><a href="">&nbsp;FMS Vehicle</a>
                                                                    <ul class="acitem">
                                                                        <li><a href="/throttle/handleBillListPage.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Bill Report</a></li>
                                                                        <!--<li><a href="/throttle/vehicleComplaintHist.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Mechanic Performace</a></li>-->
                                                                        <li><a href="/throttle/handleBodyBillPage.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Contractors Report</a></li>
                                                                        <!--<li><a href="/throttle/contractorActivityPage.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Contractors Activity Report</a></li>-->
                                                                        <li><a href="/throttle/periodicServiceListPage.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Service Dues</a></li>
                                                                        <li><a href="/throttle/vehicleFCDue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;FC Dues</a></li>
                                                                        <li><a href="/throttle/vehicleInsuranceDue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Insurance Dues</a></li>
                                                                        <li><a href="/throttle/vehicleRoadTaxDue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Road Tax Dues</a></li>
                                                                        <li><a href="/throttle/vehiclePermitDue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Permit Dues</a></li>
                                                                        <li><a href="/throttle/vehicleAMCDue.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;AMC Dues</a></li>
                                                                        <!--<li><a href="/throttle/vehicleServiceCost.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Service Cost Analyst</a></li>
                                                    <li><a href="/throttle/vehicleTaxServiceCost.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;TaxWise Service Cost Analyst</a></li>-->
                                                                    </ul>
                                                                </li>
                                                                <li><a href="">&nbsp;FMS Stores</a>
                                                                    <ul class="acitem">
                                                                        <li><a href="/throttle/categorySearch.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Categorywise Report</a></li>
                                                                        <li><a href="/throttle/stkAvbPage.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Stock Availability</a></li>
                                                                        <!--<li><a href="/throttle/handleTaxWiseItemsPage.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Taxwise Items</a></li>
                                                <li><a href="/throttle/reqItemsPage.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Required Items</a></li>-->
                                                                        <li><a href="/throttle/handleOrderListPage.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;WO/PO Report</a></li>
                                                                        <li><a href="/throttle/stockWorthPage.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Stock Worth</a></li>
                                                                        <li><a href="/throttle/stockPurchaseRep.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Purchase Report</a></li>
                                                                        <!--<li><a href="/throttle/stockIssueRep.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Issue Report</a></li>
                                                <li><a href="/throttle/storesEff.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Stores Efficiency</a></li>
                                                <li><a href="/throttle/movingAve.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Moving Average</a></li>-->
                                                                        <li><a href="/throttle/receivedStockRpt.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Receive Stock</a></li>
                                                                        <!--<li><a href="/throttle/stList.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Stock Transfer </a></li>
                                                <li><a href="/throttle/handleStockPurchasePage.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Stock Purchase Report </a></li>
                                                <li><a href="/throttle/taxwisePOreport.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;TaxWise Purchase Report</a></li>-->
                                                                    </ul>
                                                                </li>
                                                                <li><a href="">&nbsp;FMS RC Parts</a>
                                                                    <ul class="acitem">
                                                                        <li><a href="/throttle/rcItemPage.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;RC Parts Report</a></li>
                                                                        <!--<li><a href="/throttle/handleRcBillList.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;RC Work Order Bills</a></li>-->
                                                                    </ul>
                                                                </li>
                                                                <li><a href="">&nbsp;FMS Tyres</a>
                                                                    <ul class="acitem">
                                                                        <li><a href="/throttle/tyrePoWoPage.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Tyre Po/Wo Report</a></li>
                                                                        <!--  <li><a href="/throttle/handleTyreLifeCyclePage.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Tyre Life Cycle</a></li>  -->

                                                                    </ul>
                                                                </li>
                                                                <li><a href="">&nbsp;FMS Render Service</a>
                                                                    <ul class="acitem">
                                                                        <li><a href="/throttle/serviceSummaryPage.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Service Summary</a></li>
                                                                        <!--<li><a href="/throttle/serEff.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Service Efficiency</a></li>-->
                                                                        <li><a href="/throttle/vehicleComplaintHist.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Veh Compliant History</a></li>
                                                                    </ul>
                                                                </li>
                                                                <!--<li><a href="">&nbsp;Operation Point</a>
                                            <ul class="acitem">
                                                    <li><a href="/throttle/workOrderList.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Workorder Status</a></li>
                                                    <li><a href="/throttle/periodicServiceListPage.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Service Dues</a></li>
                                                    <li><a href="/throttle/FreeServicePage.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Warranty Service Dues</a></li>
                                        </ul>
                                    </li>-->
                                                                <li><a href="">&nbsp;FMS MIS</a>
                                                                    <ul class="acitem">
                                                                        <li><a href="/throttle/handleServiceDailyMIS.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Jobcard Rep1</a></li>
                                                                        <li><a href="/throttle/handleServiceGraphData.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Jobcard Rep2</a></li>
                                                                        <!--<li><a href="/throttle/vehicleComparisionReportPage.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Manufacturer Comparison</a></li>-->
                                                                        <li><a href="/throttle/vehicleAgeComparisionReportPage.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Vehicle Age Distribution</a></li>
                                                                        <!--<li><a href="/throttle/salesTrendSearch.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Sales Bill Trend</a></li>-->
                                                                        <li><a href="/throttle/stockWorthSearch.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Stock Worth</a></li>
                                                                        <!--<li><a href="/throttle/vendorTrendSearch.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Vendor Supply Trend</a></li>-->
                                                                        <li><a href="/throttle/problemDistributionSearch.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Problem Distribution</a></li>
                                                                        <!--<li><a href="/throttle/externalLabourBillGraphData.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;External Labour Bill Trend</a></li>-->
                                                                        <%--<li><a href="/throttle/frame/report/Valume.html" target="frame" >Volume of Vehicle Services</a></li>--%>
                                                                        <li><a href="/throttle/topProblemSearch.do" target="frame" >&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Top Problem</a></li>
                                                                            <%--<li><a href="/throttle/frame/report/consumption.html" target="frame" >Stock Consumption</a></li>--%>
                                                                        <!--<li><a href="/throttle/scheduleDeviation.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Schedule Deviation</a></li>
                                                    <li><a href="/throttle/scrapGraphData.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Scarp Value</a></li>-->
                                                                        <li><a href="/throttle/rcTrendGraphData.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;RC Trend</a></li>
                                                                            <%--<li><a href="/throttle/frame/report/spareLife.html" target="frame">Spare Life</a></li>--%>
                                                                            <%--<li><a href="/throttle/frame/report/RCspareLife.html" target="frame">RC-Spare Life</a></li>--%>
                                                                        <li><a href="/throttle/RCexpense.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;RC-Expense </a></li>
                                                                        <!--<li><a href="/throttle/RCsaving.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Saving By RC</a></li>-->
                                                                        <li><a href="/throttle/mileageReport.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Mileage Report</a></li>
                                                                        <!--<li><a href="/throttle/vehicleServiceCostGraphData.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Cost of Service</a></li>-->
                                                                        <%--bala--%>
                                                                        <%-- <li><a href="/throttle/usageTypewisereport.do" target="frame">Usage Typewise Report</a></li>--%>
                                                                        <%--<li><a href="/throttle/frame/report/vehicleDeviation.html" target="frame">Vehicle Deviation </a></li>--%>
                                                                    </ul>
                                                                </li>
                                                            </c:if>
                                                            <!--
                                     <c:if test = "${companyId==1011}" >
                                            <li><a href="">Vehicle</a>
                                                    <ul class="acitem">
                                                            <li><a href="/throttle/handleBillListPage.do" target="frame">Bill Report</a></li>
                                                            <li><a href="/throttle/periodicServiceListPage.do" target="frame">Service Dues</a></li>
                                                            <li><a href="/throttle/vehicleFCDue.do" target="frame">FC Dues</a></li>
                                                            <li><a href="/throttle/vehicleServiceCost.do" target="frame">Service Cost Analyst</a></li>
                                                    </ul>
                                            </li>
                                    </c:if> -->
                                                            <!--<li><a href="/throttle/viewTripSheet.do" target="frame">&nbsp;View Trip Sheet</a></li> -->

                                                            <!--<li><a href="/throttle/content/report/TripMileageReport.jsp" target="frame">&nbsp;Mileage Report</a></li>
                                                            <li><a href="/throttle/content/report/TripFuelReport.jsp" target="frame">&nbsp;Fuel Report</a></li>-->
                                                            <li><a href="">&nbsp;Trip Analytics</a>
                                                                <ul class="acitem">
                                                                    <li><a href="/throttle/content/RenderService/tripMisReport.jsp" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />Trip MIS Report</a></li>
                                                                    <!--      <li><a href="/throttle/content/report/driverSettlementReport.jsp" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Driver Settlement </a></li>
                                                                          <li><a href="/throttle/vehicleCurrentStatus.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Vehicle Status</a></li>-->
                                                                    <li><a href="/throttle/companyWiseReportPage.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Company Wise Trip </a></li>
                                                                    <!--<!-- <li><a href="/throttle/vehiclePerformancePage.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Vehicle Performance</a></li>
                                                                         <li><a href="/throttle/driverPerformancePage.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Driver Performance</a></li>    -->
                                                                    <!--                                        <li><a href="/throttle/lpsReport.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;LPS</a></li>-->
                                                                    <!--<li><a href="/throttle/vehicleProfitAndLossReport.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Vehicle Profit And Loss Report</a></li> -->
                                                                    <li><a href="/throttle/tripWisePandLReport.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Trip Wise Profit And Loss  </a></li>
                                                                    <li><a href="/throttle/VehicleWisePandL.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Vehicle Wise Profit And Loss  </a></li>
                                                                    <li><a href="/throttle/content/RenderService/ProfitAndLossReport.jsp" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Profit And Loss  </a></li>
                                                                    <li><a href="/throttle/districtWiseReport.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;District Wise Report </a></li>
                                                                    <li><a href="/throttle/tripSheetDetailsReport.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;TripSheetDetails </a></li>
                                                                    <li><a href="/throttle/consigneeWiseDetailsReport.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Consignee Wise Details </a></li>
                                                                    <li><a href="/throttle/tripSettlementDetailsReport.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Trip Settlement Details </a></li>
                                                                    <li><a href="/throttle/dieselReport.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Diesel Report </a></li>
                                                                </ul>
                                                            </li>
                                                            <li><a href="/throttle/BrattleFoods/printConsignmentNote.jsp" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Print Consignment Note</a></li>
                                                            <li><a href="/throttle/BrattleFoods/printTripSheet.jsp" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Print Trip Sheet</a></li>
                                                            <!--<li><a href="">&nbsp;Route Optimization</a>
                                                                   <ul class="acitem">
                                                                   <li><a href="/throttle/content/report/routeOptimization/openTripReport.html" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Open Trips</a></li>
                                                                   <li><a href="/throttle/content/report/routeOptimization/closedTripReport.html" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Closed Trip</a></li>
                                                                   <li><a href="/throttle/content/report/routeOptimization/vehicleCurrentLocationReport.html" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Vehicle Current Location</a></li>
                                                                   <li><a href="/throttle/content/report/routeOptimization/vehicleProductivityReport.html" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Vehicle Productivity</a></li>
                                                            </ul>
                                                           </li>-->

                                                        </ul>
                                                    </c:if>

                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                    <div id="fuelM" style="display: none;">
                                        <table id="fuelMenu"  cellpadding="0" cellspacing="0" align="center" border="0">
                                            <tr>
                                                <td>
                                                    <c:if test = "${menu.name=='FuelManagement'}" >
                                                        <!--Fuel Management -->
                                                        <ul class="menu">
                                                            <c:if test="${menu.fuelManagement!=null}">
                                                                <c:forEach items="${menu.fuelManagement}" var="fuelManagement">
                                                                    <%if (rolId.equals(1030)) {%>
                                                                    <%} else if (rolId.equals(1033)) {%>
                                                                    <%} else if (rolId.equals(1032)) {%>
                                                                    <%} else if (rolId.equals(1031)) {%>
                                                                    <%} else if (rolId.equals(1036)) {%>
                                                                    <%} else if (rolId.equals(1035)) {%>
                                                                    <!--<c:if test = "${fuelManagement.subMenu =='VehicleCompany'}" >
                                                            <li><a href="/throttle/viewUsingCompany.do" target="frame">&nbsp;Manage Using Company</a></li>
                                                            <li><a href="/throttle/viewVehicleCompany.do" target="frame">&nbsp;Config Vehicle Company</a></li>
                                                        </c:if>-->
                                                                    <c:if test = "${fuelManagement.subMenu =='MilleageConfig'}" >
                                                                        <li><a href="/throttle/viewMfrMilleageAgeing.do" target="frame" >&nbsp;Milleage Ageing</a></li>
                                                                        <li><a href="/throttle/viewMfrMilleageConfiguration.do" target="frame" >&nbsp;Milleage Configuration</a></li>
                                                                        <!--                                                                        <li><a href="/throttle/viewVehicleMilleage.do" target="frame" >&nbsp;Milleage Configuration</a></li>-->
                                                                    </c:if>
                                                                    <c:if test = "${fuelManagement.subMenu =='TankDetail'}" >
                                                                        <!--<li><a href="/throttle/generateDirectMprPage.do" target="frame">&nbsp;Generate MPR</a></li>
                                                                    <li><a href="/throttle/mprApprovalList.do" target="frame">&nbsp;MPR Approval</a></li>
                                                                    <li><a href="/throttle/storesMprList.do" target="frame">&nbsp;MPR List</a></li>
                                                                    <li><a href="/throttle/receiveInvoice.do" target="frame">&nbsp;Receive Invoice</a></li>-->
                                                                        <!--<li><a href="/throttle/viewTankDetail.do" target="frame" >&nbsp;Manage Tank Details</a></li>-->
                                                                    </c:if>
                                                                    <c:if test = "${fuelManagement.subMenu =='FuelPrice'}" >
                                                                        <!--<li><a href="/throttle/viewFuelPrice.do" target="frame" >&nbsp;Fuel Price</a></li>-->
                                                                        <li><a href="/throttle/viewFuelPriceMaster.do" target="frame" >&nbsp;Fuel Price</a></li>
                                                                        <li><a href="/throttle/vehicleTypeOperationgCostMaster.do" target="frame" >&nbsp;VehicleTypeOperationgCost
                                                                                Master</a></li>                         
                                                                    </c:if>
                                                                    <!--<c:if test = "${fuelManagement.subMenu =='FuelFilling'}" >
                                                                    <li><a href="/throttle/addFuelFillingPage.do" target="frame" >&nbsp;Manage Fuel Filing</a></li>
                                                        </c:if>-->
                                                                    <c:if test = "${fuelManagement.subMenu =='FuelReport'}" >
                                                                        <!--<li><a href="/throttle/fuelReportPage.do" target="frame" >&nbsp;Fuel Report</a></li>-->
                                                                    </c:if>
                                                                    <c:if test = "${fuelManagement.subMenu =='MilleageReport'}" >
                                                                        <!--<li><a href="/throttle/milleageReportPage.do" target="frame" >&nbsp;Milleage Report</a></li>-->
                                                                    </c:if>
                                                                    <c:if test = "${fuelManagement.subMenu =='VehicleReport'}" >
                                                                        <!--<li><a href="/throttle/vehicleReportPage.do" target="frame" >&nbsp;Vehicle Report</a></li>-->
                                                                    </c:if>
                                                                    <c:if test = "${fuelManagement.subMenu =='MonthlyAvgReport'}" >
                                                                        <!--<li><a href="/throttle/monthlyAvgReportPage.do" target="frame" >&nbsp;Monthly Avg Report</a></li>-->
                                                                    </c:if>
                                                                    <% } else {%>
                                                                    <!--<c:if test = "${fuelManagement.subMenu =='VehicleCompany'}" >
                                                            <li><a href="/throttle/viewUsingCompany.do" target="frame">&nbsp;Manage Using Company</a></li>
                                                            <li><a href="/throttle/viewVehicleCompany.do" target="frame">&nbsp;Config Vehicle Company</a></li>
                                                        </c:if>-->
                                                                    <c:if test = "${fuelManagement.subMenu =='MilleageConfig'}" >
                                                                        <li><a href="/throttle/viewMfrMilleageAgeing.do" target="frame" >&nbsp;Milleage Ageing</a></li>
                                                                        <li><a href="/throttle/viewMfrMilleageConfiguration.do" target="frame" >&nbsp;Milleage Configuration</a></li>
                                                                        <!--<li><a href="/throttle/viewVehicleMilleage.do" target="frame" >&nbsp;Milleage Configuration</a></li>-->
                                                                    </c:if>
                                                                    <c:if test = "${fuelManagement.subMenu =='TankDetail'}" >
                                                                        <!--<li><a href="/throttle/generateDirectMprPage.do" target="frame">&nbsp;Generate MPR</a></li>
                                                                    <li><a href="/throttle/mprApprovalList.do" target="frame">&nbsp;MPR Approval</a></li>
                                                                    <li><a href="/throttle/storesMprList.do" target="frame">&nbsp;MPR List</a></li>
                                                                    <li><a href="/throttle/receiveInvoice.do" target="frame">&nbsp;Receive Invoice</a></li>-->
                                                                        <!--<li><a href="/throttle/viewTankDetail.do" target="frame" >&nbsp;Manage Tank Details</a></li>-->
                                                                    </c:if>
                                                                    <c:if test = "${fuelManagement.subMenu =='FuelPrice'}" >
                                                                        <!--<li><a href="/throttle/viewFuelPrice.do" target="frame" >&nbsp;Fuel Price</a></li>-->
                                                                        <li><a href="/throttle/viewFuelPriceMaster.do" target="frame" >&nbsp;Fuel Price</a></li>
                                                                        <li><a href="/throttle/vehicleTypeOperationgCostMaster.do" target="frame" >&nbsp;VehicleTypeOperationgCost
                                                                                Master</a></li>                         

                                                                    </c:if>
                                                                    <!--<c:if test = "${fuelManagement.subMenu =='FuelFilling'}" >
                                                                    <li><a href="/throttle/addFuelFillingPage.do" target="frame" >&nbsp;Manage Fuel Filing</a></li>
                                                        </c:if>-->
                                                                    <c:if test = "${fuelManagement.subMenu =='FuelReport'}" >
                                                                        <!--<li><a href="/throttle/fuelReportPage.do" target="frame" >&nbsp;Fuel Report</a></li>-->
                                                                    </c:if>
                                                                    <c:if test = "${fuelManagement.subMenu =='MilleageReport'}" >
                                                                        <!--<li><a href="/throttle/milleageReportPage.do" target="frame" >&nbsp;Milleage Report</a></li>-->
                                                                    </c:if>
                                                                    <c:if test = "${fuelManagement.subMenu =='VehicleReport'}" >
                                                                        <!--<li><a href="/throttle/vehicleReportPage.do" target="frame" >&nbsp;Vehicle Report</a></li>-->
                                                                    </c:if>
                                                                    <c:if test = "${fuelManagement.subMenu =='MonthlyAvgReport'}" >
                                                                        <!--<li><a href="/throttle/monthlyAvgReportPage.do" target="frame" >&nbsp;Monthly Avg Report</a></li>-->
                                                                    </c:if>
                                                                    <%}%>
                                                                </c:forEach>
                                                            </c:if>
                                                        </ul>
                                                    </c:if>
                                                    </c:forEach>
                                                    </c:if>

                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div id="finM" style="display: none;">
                                        <table id="financeMenu"  cellpadding="0" cellspacing="0" align="center" border="0">
                                            <tr>
                                                <td>
                                                    <c:if test = "${menu.name=='Finance'}" >
                                                        <!--Fuel Management -->
                                                        <ul class="menu">
                                                            <c:if test="${menu.Finance!=null}">
                                                                <c:forEach items="${menu.Finance}" var="Finance">
                                                                    <c:if test = "${Finance.subMenu =='Finance'}" >
                                                                        <!--<li><a href="/throttle/viewCompany1.do" target="frame">&nbsp;Company</a></li>-->
                                                                        <li><a href="/throttle/financeYear.do" target="frame">&nbsp;Financial Year</a></li>
                                                                        <li><a href="/throttle/financePrimaryBank.do" target="frame">&nbsp;Bank Master</a></li>
                                                                        <li><a href="/throttle/financeBank.do" target="frame">&nbsp;Bank Branch Master</a></li> 
                                                                        <!--   <li><a href="/throttle/financeCountry.do" target="frame">&nbsp;Country</a></li>
                                                                                <li><a href="/throttle/financeState.do" target="frame">&nbsp;State</a></li>
                                                                                <li><a href="/throttle/financeDistrict.do" target="frame">&nbsp;District</a></li>-->
                                                                        <li><a href="/throttle/financeGroup.do" target="frame">&nbsp;Primary Group</a></li>
                                                                        <li><a href="/throttle/levelMaster.do" target="frame">&nbsp;Group Master</a></li>
                                                                        <li><a href="/throttle/financeLedger.do" target="frame">&nbsp;Ledger Master</a></li>
                                                                        <!-- <li><a href="/throttle/content/sample.jsp" target="frame">&nbsp;Group Type</a></li>-->
                                                                        <!--				                                                <li><a href="/throttle/financeVoucher.do" target="frame">&nbsp;Voucher Type</a></li>
                                                                                                                <li><a href="/throttle/financeAccountEntryType.do" target="frame">&nbsp;Account Entry Type Master</a></li>-->
                                                                        <li><a href="/throttle/financeTax.do" target="frame">&nbsp;Tax Master</a></li>
                                                                        <!--                                                                        <li><a href="/throttle/chargeCodeTax.do" target="frame">&nbsp;Charge Code Tax Map</a></li>-->
                                                                        <!--                                            <li><a href="/throttle/financeTax.do" target="frame">&nbsp;Tax Master</a></li>-->
                                                                        <!--                                            <li><a href="/throttle/contraEntry.do" target="frame">&nbsp;Contra Entry</a></li>-->
                                                                        <!--                                            <li><a href="/throttle/journalEntry.do" target="frame">&nbsp;Journal Entry</a></li>-->



                                                                        <li><a href="">&nbsp;Payments</a>
                                                                            <ul class="acitem">
                                                                                <li><a href="/throttle/handleCRJPayment.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Creditor Journal Voucher(CRJ) </a></li>
                                                                                <li><a href="/throttle/handlePaymentDetailsReport.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Payments </a></li>
                                                                                <li><a href="/throttle/handlePaymentPendingReport.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Payments Pending Report</a></li>
                                                                                <li><a href="/throttle/handlePaymentPaidReport.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Payments Paid Report</a></li>
                                                                            </ul>
                                                                        </li>
                                                                        <li><a href="">&nbsp;Invoice</a>
                                                                            <ul class="acitem">
                                                                                <li><a href="/throttle/invoiceReceipts.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Invoice Receipts</a></li>
                                                                                <li><a href="/throttle/invoicePendingReceipts.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Invoice Pending Receipts </a></li>
                                                                                <li><a href="/throttle/invoiceReceived.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Invoice Received</a></li>
                                                                            </ul>
                                                                        </li>
                                                                        <li><a href="">&nbsp;Credit</a>
                                                                            <ul class="acitem">
                                                                                <li><a href="/throttle/handleCreditNote.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Credit Note</a></li>
                                                                                <li><a href="/throttle/handleCreditSearch.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Search</a></li>
                                                                            </ul>
                                                                        </li>
                                                                        <li><a href="">&nbsp;Debit</a>
                                                                            <ul class="acitem">
                                                                                <li><a href="/throttle/handleDebitNote.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Debit Note</a></li>
                                                                                <li><a href="/throttle/handleDebitSearch.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Search</a></li>
                                                                            </ul>
                                                                        </li>


                                                                        <li><a href="">&nbsp;Entries</a>
                                                                            <ul class="acitem">
                                                                                <li><a href="/throttle/paymentEntry.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Cash Payments</a></li>
                                                                                <li><a href="/throttle/bankPaymentEntry.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Bank Payments</a></li>
                                                                                <!--<li><a href="/throttle/receiptEntry.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Cash Receipts</a></li>-->
                                                                                <!--<li><a href="/throttle/bankReceiptEntry.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Bank Receipts</a></li>-->
                                                                                <li><a href="/throttle/contraEntry.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Contra Entry</a></li>
                                                                                <li><a href="/throttle/journalEntry.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Journal Entry</a></li>
                                                                                <!--                                                <li><a href="/throttle/creditNote.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Credit Note</a></li>
                                                                                                                                                                            <li><a href="/throttle/debitNote.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Debit Note</a></li>-->
                                                                                <!--                                                <li><a href="/throttle/vatMaster.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;VAT Master</a></li>-->
                                                                                <!--                                                <li><a href="/throttle/content/sample.jsp" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Credit Note</a></li>
                                                                                                                                                                            <li><a href="/throttle/content/sample.jsp" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Debit Note</a></li>-->
                                                                            </ul>
                                                                        </li>
                                                                        <li><a href="">&nbsp;Bank Transaction</a>
                                                                            <ul class="acitem">
                                                                                <li><a href="/throttle/bankPaymentClearance.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Bank Transaction</a></li>
                                                                                <li><a href="/throttle/bankReconciliationStatement.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Bank Reconciliation Statement</a></li>
                                                                            </ul>
                                                                        </li>
                                                                        <li><a href="">&nbsp;Reports</a>
                                                                            <ul class="acitem">
                                                                                <li><a href="/throttle/ledgerReport.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Ledger Report</a></li>
                                                                                <!--                                                    <li><a href="/throttle/ledgerReportOld.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Ledger Report Old</a></li>-->
                                                                                <li><a href="/throttle/dayBook.do?param=search" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Day Book</a></li>
                                                                                <li><a href="/throttle/dayBookSummary.do?param=search" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Day Book Summary</a></li>
                                                                                <li><a href="/throttle/trialBalanceNew.do?param=search" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Trial Balance</a></li>

                                                                                <!--<li><a href="/throttle/trialBalance.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Trial Balance</a></li>-->
                                                                               <li><a href="/throttle/profitAndLoss.do?param=search" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;P & L</a></li>
                                                                                <li><a href="/throttle/balanceSheet.do?param=search" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Balance Sheet</a></li>
                                                                                <li><a href="/throttle/entriesReport.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Entries Report</a></li>
                                                                                <li><a href="/throttle/costCenterReport.do" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;Cost Center Report</a></li>
                                                                                <li><a href="/throttle/bankReconciliationReport.do?param=view" target="frame">&nbsp;<img src="/throttle/images/icon_stand.png" alt="" />&nbsp;BRS Report</a></li>
                                                                            </ul>
                                                                        </li>
                                                                        <!--                                                                         <li><a href="/throttle/financeOperation.do" target="frame">&nbsp;Finance Operation</a></li>-->

                                                                    </c:if>
                                                                </c:forEach>
                                                            </c:if>
                                                        </ul>
                                                    </c:if>
                                                    </c:forEach>
                                                    </c:if>

                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                    </body>
                                    </html>
