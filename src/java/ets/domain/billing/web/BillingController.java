/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.billing.web;

/**
 *
 * @author vinoth
 */
import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.ParamPart;
import com.oreilly.servlet.multipart.Part;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.arch.web.BaseController;
import ets.domain.util.FPLogUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import javax.servlet.http.HttpServletRequest;
import ets.domain.billing.business.BillingBP;
import ets.domain.billing.business.BillingTO;
import ets.domain.finance.business.FinanceTO;
import ets.domain.finance.web.FinanceCommand;
import ets.domain.operation.business.OperationBP;
import ets.domain.operation.business.OperationTO;
import ets.domain.users.business.LoginBP;
import ets.domain.trip.business.TripBP;
import ets.domain.trip.business.TripTO;
import ets.domain.trip.web.TripCommand;
import ets.domain.util.ParveenErrorConstants;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.ModelAndView;
import java.util.Calendar;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

public class BillingController extends BaseController {

    BillingCommand billingCommand;
    BillingBP billingBP;
    OperationBP operationBP;
    TripBP tripBP;
    LoginBP loginBP;
    TripCommand tripCommand;

    public BillingBP getBillingBP() {
        return billingBP;
    }

    public void setBillingBP(BillingBP billingBP) {
        this.billingBP = billingBP;
    }

    public OperationBP getOperationBP() {
        return operationBP;
    }

    public void setOperationBP(OperationBP operationBP) {
        this.operationBP = operationBP;
    }

    public TripBP getTripBP() {
        return tripBP;
    }

    public void setTripBP(TripBP tripBP) {
        this.tripBP = tripBP;
    }

    public BillingCommand getBillingCommand() {
        return billingCommand;
    }

    public void setBillingCommand(BillingCommand billingCommand) {
        this.billingCommand = billingCommand;
    }

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        //////System.out.println("request.getRequestURI() = " + request.getRequestURI());
        binder.closeNoCatch();
        initialize(request);

    }

    public ModelAndView viewClosedOrderForBilling(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        billingCommand = command;
        String menuPath = "";
        BillingTO billingTO = new BillingTO();
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  Display Closed Trips";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String customerName = "";
        String customerId = "";
        String fromDate = "";
        String toDate = "";
        String billOfEntry = "";
        String shippingBillNo = "";
        String type = "";
        String movementType = "";
        try {

            path = "content/billing/viewClosedOrder.jsp";
            int userId = (Integer) session.getAttribute("userId");
            fromDate = request.getParameter("fromDate");
            toDate = request.getParameter("toDate");
            customerName = request.getParameter("Customer");
            customerId = request.getParameter("customerId");
            billOfEntry = request.getParameter("billOfEntryNo");
            shippingBillNo = request.getParameter("shipingBillNo");
            type = request.getParameter("type");
            movementType = request.getParameter("movementType");
            request.setAttribute("customerName", customerName);
            request.setAttribute("customerId", customerId);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("billOfEntryNo", billOfEntry);
            request.setAttribute("shippingBillNo", shippingBillNo);
            request.setAttribute("type", type);
            request.setAttribute("movementType", movementType);
            billingTO.setUserId(userId + "");
            billingTO.setFromDate(fromDate);
            billingTO.setToDate(toDate);
            billingTO.setCustomerName(customerName);
            billingTO.setCustomerId(customerId);
            billingTO.setBillOfEntryNo(billOfEntry);
            billingTO.setShipingBillNo(shippingBillNo);
            billingTO.setBillingType(type);
            billingTO.setMovementType(movementType);
//            if (!"0".equals(customerName)) {}
            ArrayList movementTypeList = new ArrayList();
            movementTypeList = operationBP.getMovementTypeList();
            request.setAttribute("movementTypeList", movementTypeList);
            ArrayList customerList = new ArrayList();
            customerList = operationBP.getTripCustomerList();
            request.setAttribute("customerList", customerList);
            System.out.println("customerList Size Is :::" + customerList.size());

            ArrayList closedTrips = new ArrayList();
            closedTrips = billingBP.getOrdersForBilling(billingTO);
            request.setAttribute("closedTrips", closedTrips);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleSupplementInvoice(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        billingCommand = command;
        String menuPath = "";
        BillingTO billingTO = new BillingTO();
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  Display Closed Trips";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String customerName = "";
        String customerId = "";
        String fromDate = "";
        String toDate = "";
        String billOfEntry = "";
        String shippingBillNo = "";
        String type = "";
        String movementType = "";
        try {

            path = "content/billing/viewClosedOrderForSupplementInv.jsp";
            int userId = (Integer) session.getAttribute("userId");

            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            String param = request.getParameter("param");
            customerId = request.getParameter("customerId");
            fromDate = request.getParameter("fromDate");
            toDate = request.getParameter("toDate");
            String submitStatus = request.getParameter("submitStatus");
            String tripType = request.getParameter("tripType");
            String grNo = request.getParameter("grNo");
            String billNo = request.getParameter("billNo");

            request.setAttribute("customerId", customerId);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("tripType", tripType);
            request.setAttribute("submitStatus", submitStatus);
            request.setAttribute("billNo", billNo);
            request.setAttribute("grNo", grNo);

            billingTO.setUserId(userId + "");
            billingTO.setFromDate(fromDate);
            billingTO.setToDate(toDate);
            billingTO.setCustomerName(customerName);
            billingTO.setCustomerId(customerId);
            billingTO.setBillOfEntryNo(grNo);
            billingTO.setShipingBillNo(billNo);
            billingTO.setBillingType(tripType);
            billingTO.setMovementType(movementType);
//            if (!"0".equals(customerName)) {}
            ArrayList movementTypeList = new ArrayList();
            movementTypeList = operationBP.getMovementTypeList();
            request.setAttribute("movementTypeList", movementTypeList);
            ArrayList customerList = new ArrayList();
            customerList = operationBP.getTripCustomerList();
            request.setAttribute("customerList", customerList);
            System.out.println("customerList Size Is :::" + customerList.size());

            ArrayList closedTrips = new ArrayList();
            closedTrips = billingBP.getOrdersForSupplementBilling(billingTO);
            request.setAttribute("closedTrips", closedTrips);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewClosedRepoOrderForBilling(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        System.out.println("viewClosedRepoOrderForBilling");
        String path = "";
        HttpSession session = request.getSession();
        billingCommand = command;
        String menuPath = "";
        BillingTO billingTO = new BillingTO();
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  Display Closed Trips";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute("menuPath", menuPath);
        String customerName = "";
        String customerId = "";
        String fromDate = "";
        String toDate = "";
        String billOfEntry = "";
        String shippingBillNo = "";
        String type = "";
        String movementType = "";
        String grNo = "";
        try {
            path = "content/billing/viewClosedRepoOrder.jsp";
            int userId = ((Integer) session.getAttribute("userId")).intValue();

            Date dNow = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(dNow);
            cal.add(5, 0);
            dNow = cal.getTime();

            Date dNow1 = new Date();
            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(dNow1);
            cal1.add(2, -1);
            dNow1 = cal1.getTime();

            SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy");
            String today = ft.format(dNow);
            String fromday = ft.format(dNow1);
            System.out.println("the fromday date is " + fromday);
            System.out.println("the today date is " + today);
            if (request.getParameter("checkDate1").equals("1")) {
                if (request.getParameter("fromDate") != null && request.getParameter("fromDate") != "") {
                    fromDate = request.getParameter("fromDate");
                }
                if (request.getParameter("toDate") != null && request.getParameter("toDate") != "") {
                    toDate = request.getParameter("toDate");
                }
            } else {

                fromDate = fromday;
                toDate = today;
            }

            if (request.getParameter("grNo") != null) {
                grNo = request.getParameter("grNo");
            }
            customerName = request.getParameter("Customer");
            customerId = request.getParameter("billingParty");
            billOfEntry = request.getParameter("billOfEntryNo");
            shippingBillNo = request.getParameter("shipingBillNo");
            type = request.getParameter("type");
            movementType = request.getParameter("movementType");
            request.setAttribute("customerName", customerName);
            request.setAttribute("customerId", customerId);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("billOfEntryNo", billOfEntry);
            request.setAttribute("shippingBillNo", shippingBillNo);
            request.setAttribute("type", type);
            request.setAttribute("movementType", movementType);
            request.setAttribute("grNo", grNo);

            billingTO.setUserId(userId + "");
            billingTO.setFromDate(fromDate);
            billingTO.setToDate(toDate);
            billingTO.setCustomerName(customerName);
            billingTO.setCustomerId(customerId);
            billingTO.setBillOfEntryNo(billOfEntry);
            billingTO.setShipingBillNo(shippingBillNo);
            billingTO.setBillingType(type);
            billingTO.setMovementType(movementType);
            billingTO.setGrNo(grNo);

            ArrayList movementTypeList = new ArrayList();
            movementTypeList = operationBP.getMovementTypeList();
            request.setAttribute("movementTypeList", movementTypeList);
            ArrayList customerList = new ArrayList();
            customerList = operationBP.getTripCustomerList();
            request.setAttribute("customerList", customerList);
            System.out.println("customerList Size Is :::" + customerList.size());

            ArrayList closedTrips = new ArrayList();
            closedTrips = billingBP.getRepoOrdersForBilling(billingTO);
            request.setAttribute("closedTrips", closedTrips);
            System.out.println("closedTrips.size()=" + closedTrips.size());

        } catch (FPRuntimeException exception) {

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView gererateOrderBill(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Finance  >>  Generate Bill ";
        String customerId = "", tripSheetId = "";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            String billList = "Billing";
            customerId = request.getParameter("custId");
            tripSheetId = request.getParameter("tripSheetId");

            System.out.println("tripSheetId in controller:" + tripSheetId);
            System.out.println("custId in controller:" + customerId);

            request.setAttribute("customerId", customerId);
            request.setAttribute("pageTitle", pageTitle);
            request.setAttribute("tripSheetId", tripSheetId);

            BillingTO billingTO = new BillingTO();

            String consignmentOrderId = request.getParameter("consignmentOrderId");
            request.setAttribute("consignmentOrderIds", consignmentOrderId);
            System.out.println("consignmentOrderId" + consignmentOrderId);
            String reeferRequired = request.getParameter("reeferRequired");

            ArrayList comodityDetails = new ArrayList();
            comodityDetails = tripBP.getComodityDetails();
            request.setAttribute("comodityDetails", comodityDetails);
            System.out.println("hi=========================>>>>>" + comodityDetails);

            billingTO.setConsignmentOrderId(consignmentOrderId);
            billingTO.setTripSheetId(tripSheetId);
            ArrayList ordersToBeBillingDetails = billingBP.getOrdersToBeBillingDetails(billingTO);
            ArrayList tripsOtherExpenseDetails = billingBP.getOrdersOtherExpenseDetails(billingTO);
            ArrayList gsttaxDetails = billingBP.getGSTTaxDetails(billingTO);
            if (gsttaxDetails.size() > 0) {
                Iterator it = gsttaxDetails.iterator();
                while (it.hasNext()) {
                    BillingTO billTO = (BillingTO) it.next();
                    if ("CGST".equalsIgnoreCase(billTO.getGstName())) {
                        System.out.println("CGST:" + billTO.getGstPercentage());
                        request.setAttribute("CGST", billTO.getGstPercentage());
                    } else if ("SGST".equalsIgnoreCase(billTO.getGstName())) {
                        System.out.println("SGST:" + billTO.getGstPercentage());
                        request.setAttribute("SGST", billTO.getGstPercentage());
                    } else if ("IGST".equalsIgnoreCase(billTO.getGstName())) {
                        System.out.println("IGST:" + billTO.getGstPercentage());
                        request.setAttribute("IGST", billTO.getGstPercentage());
                    }
                }
            }
            boolean articleFlag = false;
            if (ordersToBeBillingDetails.size() > 0) {
                Iterator itr1 = ordersToBeBillingDetails.iterator();
                while (itr1.hasNext()) {
                    BillingTO billTO1 = (BillingTO) itr1.next();
//               request.setAttribute("gstType", billTO1.getGstType());
//                 System.out.println("gstType"+billTO1.getGstType());
                    System.out.println("billTO1.getCommodityCategory()" + billTO1.getCommodityCategory());
                    if ("General".equalsIgnoreCase(billTO1.getCommodityCategory())) {
                        articleFlag = true;

                    }
                    break;
                }
            }
            System.out.println("articleFlag:" + articleFlag);
            request.setAttribute("articleFlag", articleFlag);

            if (ordersToBeBillingDetails.size() > 0) {
                BillingTO billingTO1 = (BillingTO) ordersToBeBillingDetails.get(0);
                request.setAttribute("billingParty", billingTO1.getCustomerName());
                request.setAttribute("billingPartyAddress", billingTO1.getCustomerAddress());
                request.setAttribute("billingPartyId", billingTO1.getCustomerId());
                request.setAttribute("customerId", billingTO1.getCustomerId());
                request.setAttribute("movementType", billingTO1.getMovementType());
                request.setAttribute("billList", billList);
                request.setAttribute("billOfEntryNo", billingTO1.getBillOfEntryNo());
                request.setAttribute("shipingBillNo", billingTO1.getShipingBillNo());
                request.setAttribute("organizationId", billingTO1.getOrganizationId());
                request.setAttribute("billingState", billingTO1.getBillingState());
                request.setAttribute("GSTNo", billingTO1.getGstNo());
                request.setAttribute("grDate", billingTO1.getGrDate());
                request.setAttribute("panNo", billingTO1.getPanNo());
                request.setAttribute("companyType", billingTO1.getCompanyType());
                request.setAttribute("commodityName", billingTO1.getArticleName());
                request.setAttribute("gstType", billingTO1.getGstType());
                System.out.println("commodityName" + billingTO1.getArticleName());
                System.out.println("commodityId" + billingTO1.getCommodityId());
                System.out.println("gstType" + billingTO1.getGstType());
                System.out.println("companyType" + billingTO1.getCompanyType());
                System.out.println("grDate:" + billingTO1.getGrDate());
                System.out.println("movementType:" + billingTO1.getMovementType());
                System.out.println("shipingBillNo:" + billingTO1.getShipingBillNo());
                System.out.println("billOfEntryNo:" + billingTO1.getBillOfEntryNo());
                System.out.println("billingState:" + billingTO1.getBillingState());
                System.out.println("organizationId:" + billingTO1.getOrganizationId());
                System.out.println("GSTNo:" + billingTO1.getGstNo());
            }

            request.setAttribute("tripsToBeBilledDetails", ordersToBeBillingDetails);
            request.setAttribute("tripsOtherExpenseDetails", tripsOtherExpenseDetails);
            request.setAttribute("reeferRequired", reeferRequired);
            request.setAttribute("consignmentOrderId", consignmentOrderId);
            String revenueApprovalStatus = "0";
            if (consignmentOrderId != null) {
                request.setAttribute("consignmentOrderCount", consignmentOrderId.split(",").length);
                revenueApprovalStatus = tripBP.getRevenueApprovalStatus(customerId, "1");//1 indicates gps non usage approval request
            }
            System.out.println("revenueApprovalStatus:" + revenueApprovalStatus);
            request.setAttribute("revenueApprovalStatus", revenueApprovalStatus);
            path = "content/billing/viewOrderBillDetails.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView gererateOrderSupplementBill(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Finance  >>  Generate Bill ";
        String customerId = "", tripSheetId = "", invoiceId = "";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            String billList = "Billing";
            customerId = request.getParameter("custId");
            tripSheetId = request.getParameter("tripSheetId");
            invoiceId = request.getParameter("invoiceId");
            System.out.println("tripSheetId in controller:" + tripSheetId);
            System.out.println("invoiceId in controller:" + invoiceId);
            System.out.println("custId in controller:" + customerId);
            request.setAttribute("customerId", customerId);
            request.setAttribute("pageTitle", pageTitle);
            request.setAttribute("tripSheetId", tripSheetId);

            BillingTO billingTO = new BillingTO();

            String consignmentOrderId = request.getParameter("consignmentOrderId");
            request.setAttribute("invoiceId", request.getParameter("invoiceId"));
            request.setAttribute("consignmentOrderIds", consignmentOrderId);
            System.out.println("consignmentOrderId" + consignmentOrderId);

            ArrayList comodityDetails = new ArrayList();
            comodityDetails = tripBP.getComodityDetails();
            request.setAttribute("comodityDetails", comodityDetails);
            System.out.println("hi=========================>>>>>" + comodityDetails);

            String reeferRequired = request.getParameter("reeferRequired");
            billingTO.setConsignmentOrderId(consignmentOrderId);
            billingTO.setTripSheetId(tripSheetId);
            ArrayList ordersToBeBillingDetails = billingBP.getOrdersToBeSupplementBillingDetails(billingTO);
            ArrayList tripsOtherExpenseDetails = billingBP.getOrdersSupplementOtherExpenseDetails(billingTO);
            ArrayList gsttaxDetails = billingBP.getGSTTaxDetails(billingTO);
            if (gsttaxDetails.size() > 0) {
                Iterator it = gsttaxDetails.iterator();
                while (it.hasNext()) {
                    BillingTO billTO = (BillingTO) it.next();
                    if ("CGST".equalsIgnoreCase(billTO.getGstName())) {
                        System.out.println("CGST:" + billTO.getGstPercentage());
                        request.setAttribute("CGST", billTO.getGstPercentage());
                    } else if ("SGST".equalsIgnoreCase(billTO.getGstName())) {
                        System.out.println("SGST:" + billTO.getGstPercentage());
                        request.setAttribute("SGST", billTO.getGstPercentage());
                    } else if ("IGST".equalsIgnoreCase(billTO.getGstName())) {
                        System.out.println("IGST:" + billTO.getGstPercentage());
                        request.setAttribute("IGST", billTO.getGstPercentage());
                    }
                }
            }
            boolean articleFlag = false;
            if (ordersToBeBillingDetails.size() > 0) {
                Iterator itr1 = ordersToBeBillingDetails.iterator();
                while (itr1.hasNext()) {
                    BillingTO billTO1 = (BillingTO) itr1.next();
//               request.setAttribute("gstType", billTO1.getGstType());
//                 System.out.println("gstType"+billTO1.getGstType());
                    System.out.println("billTO1.getCommodityCategory()" + billTO1.getCommodityCategory());
                    if ("General".equalsIgnoreCase(billTO1.getCommodityCategory())) {
                        articleFlag = true;

                    }
                    break;
                }
            }
            System.out.println("articleFlag:" + articleFlag);
            request.setAttribute("articleFlag", articleFlag);

            String billingParty = "";
            String billingPartyAddress = "";
            String billingPartyId = "";
            String customerIds = "";
            String GSTNo = "";
            String panNo = "";
            String companyType = "";
            String organizationId = "";
            String billingState = "";

            Iterator it = ordersToBeBillingDetails.iterator();

            while (it.hasNext()) {

                BillingTO billingTO1 = (BillingTO) it.next();
                double detention = Double.parseDouble(billingTO1.getDetaintionCharge());
                double toll = Double.parseDouble(billingTO1.getTollCharge());
                double green = Double.parseDouble(billingTO1.getGreenTax());
                double other = Double.parseDouble(billingTO1.getOtherExpense());
                double weight = Double.parseDouble(billingTO1.getWeightmentCharge());
                System.out.println("detention charge " + detention);
                System.out.println("toll charge " + toll);
                System.out.println("green charge " + green);

                if ((detention + toll + green + other + weight) > 0) {
                    System.out.println("supple part " + (detention + toll + green + other + weight));
                    billingParty = billingTO1.getCustomerName();
                    billingPartyAddress = billingTO1.getCustomerAddress();
                    billingPartyId = billingTO1.getCustomerId();
                    customerIds = billingTO1.getCustomerId();
                    GSTNo = billingTO1.getGstNo();
                    panNo = billingTO1.getPanNo();
                    companyType = billingTO1.getCompanyType();
                    organizationId = billingTO1.getOrganizationId();
                    billingState = billingTO1.getBillingState();
                }

                request.setAttribute("billingParty", billingTO1.getCustomerName());
                request.setAttribute("billingPartyAddress", billingTO1.getCustomerAddress());
                request.setAttribute("billingPartyId", billingTO1.getCustomerId());
                request.setAttribute("customerId", billingTO1.getCustomerId());
                request.setAttribute("GSTNo", billingTO1.getGstNo());
                request.setAttribute("panNo", billingTO1.getPanNo());
                request.setAttribute("companyType", billingTO1.getCompanyType());
                request.setAttribute("organizationId", billingTO1.getOrganizationId());
                request.setAttribute("billingState", billingTO1.getBillingState());

                request.setAttribute("movementType", billingTO1.getMovementType());
                request.setAttribute("billList", billList);
                request.setAttribute("billOfEntryNo", billingTO1.getBillOfEntryNo());
                request.setAttribute("shipingBillNo", billingTO1.getShipingBillNo());
                request.setAttribute("grDate", billingTO1.getGrDate());

                System.out.println("companyType" + billingTO1.getCompanyType());
                System.out.println("grDate:" + billingTO1.getGrDate());
                System.out.println("movementType:" + billingTO1.getMovementType());
                System.out.println("shipingBillNo:" + billingTO1.getShipingBillNo());
                System.out.println("billOfEntryNo:" + billingTO1.getBillOfEntryNo());
                System.out.println("billingState:" + billingTO1.getBillingState());
                System.out.println("organizationId:" + billingTO1.getOrganizationId());
                System.out.println("GSTNo:" + billingTO1.getGstNo());
            }

            if (!billingParty.equals("")) {
                request.setAttribute("billingParty", billingParty);
                request.setAttribute("billingPartyAddress", billingPartyAddress);
                request.setAttribute("billingPartyId", billingPartyId);
                request.setAttribute("customerId", customerIds);
                request.setAttribute("GSTNo", GSTNo);
                request.setAttribute("panNo", panNo);
                request.setAttribute("companyType", companyType);
                request.setAttribute("organizationId", organizationId);
                request.setAttribute("billingState", billingState);
            }

            request.setAttribute("tripsToBeBilledDetails", ordersToBeBillingDetails);
            request.setAttribute("tripsOtherExpenseDetails", tripsOtherExpenseDetails);
            request.setAttribute("reeferRequired", reeferRequired);
            request.setAttribute("consignmentOrderId", consignmentOrderId);

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getTripCustomerList();
            request.setAttribute("customerList", customerList);
            System.out.println("customerList Size Is :::" + customerList.size());

            System.out.println("customerList Size Is :::" + customerList.size());

            String revenueApprovalStatus = "0";
            if (consignmentOrderId != null) {
                request.setAttribute("consignmentOrderCount", consignmentOrderId.split(",").length);
                revenueApprovalStatus = tripBP.getRevenueApprovalStatus(customerId, "1");//1 indicates gps non usage approval request
            }
            System.out.println("revenueApprovalStatus:" + revenueApprovalStatus);
            request.setAttribute("revenueApprovalStatus", revenueApprovalStatus);
            path = "content/billing/viewOrderSupplementBillDetails.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView gererateRepoOrderBill(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Finance  >>  Generate Bill ";
        String customerId = "", tripSheetId = "", tripSheetIds = "";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            customerId = request.getParameter("custId");
            tripSheetId = request.getParameter("tripSheetId");
            tripSheetIds = request.getParameter("tripIds");
            System.out.println(" tripSheetId:" + tripSheetId);
            request.setAttribute("customerId", customerId);
            request.setAttribute("pageTitle", pageTitle);
            request.setAttribute("tripSheetId", tripSheetIds);

            ArrayList comodityDetails = new ArrayList();
            comodityDetails = tripBP.getComodityDetails();
            request.setAttribute("comodityDetails", comodityDetails);
            System.out.println("hi=========================>>>>>" + comodityDetails);

            BillingTO billingTO = new BillingTO();
            String billList = "RepoBilling";
            String consignmentOrderId = request.getParameter("consignmentOrderId");
            System.out.println("consignmentOrderId" + consignmentOrderId);
            String reeferRequired = request.getParameter("reeferRequired");
            billingTO.setConsignmentOrderId(consignmentOrderId);
            billingTO.setTripSheetId(tripSheetIds);
            ArrayList ordersToBeBillingDetails = billingBP.getOrdersToBeBillingDetails(billingTO);
            ArrayList tripsOtherExpenseDetails = billingBP.getOrdersOtherExpenseDetails(billingTO);

            ArrayList gsttaxDetails = billingBP.getGSTTaxDetails(billingTO);
            if (gsttaxDetails.size() > 0) {
                Iterator it = gsttaxDetails.iterator();
                while (it.hasNext()) {
                    BillingTO billTO = (BillingTO) it.next();
                    if ("CGST".equalsIgnoreCase(billTO.getGstName())) {
                        System.out.println("CGST:" + billTO.getGstPercentage());
                        request.setAttribute("CGST", billTO.getGstPercentage());
                    } else if ("SGST".equalsIgnoreCase(billTO.getGstName())) {
                        System.out.println("SGST:" + billTO.getGstPercentage());
                        request.setAttribute("SGST", billTO.getGstPercentage());
                    } else if ("IGST".equalsIgnoreCase(billTO.getGstName())) {
                        System.out.println("IGST:" + billTO.getGstPercentage());
                        request.setAttribute("IGST", billTO.getGstPercentage());
                    }
                }
            }
            boolean articleFlag = true;
            if (ordersToBeBillingDetails.size() > 0) {
                Iterator itr1 = ordersToBeBillingDetails.iterator();
                while (itr1.hasNext()) {
                    BillingTO billTO1 = (BillingTO) itr1.next();
                    request.setAttribute("gstType", billTO1.getGstType());
                    System.out.println("gstType" + billTO1.getGstType());
                    if (billTO1.getArticleName() != "Rice" && billTO1.getArticleName() != "rice" && billTO1.getArticleName() != "RICE") {
                        articleFlag = false;

                    }
                    break;
                }
            }
            request.setAttribute("articleFlag", articleFlag);
            if (ordersToBeBillingDetails.size() > 0) {
                BillingTO billingTO1 = (BillingTO) ordersToBeBillingDetails.get(0);
                request.setAttribute("billingParty", billingTO1.getCustomerName());
                request.setAttribute("billingPartyAddress", billingTO1.getCustomerAddress());
                request.setAttribute("billingPartyId", billingTO1.getCustomerId());
                request.setAttribute("customerId", billingTO1.getCustomerId());
                request.setAttribute("movementType", billingTO1.getMovementType());
                request.setAttribute("billList", billList);
                request.setAttribute("billOfEntryNo", billingTO1.getBillOfEntryNo());
                request.setAttribute("shipingBillNo", billingTO1.getShipingBillNo());
                request.setAttribute("organizationId", billingTO1.getOrganizationId());
                request.setAttribute("billingState", billingTO1.getBillingState());
                request.setAttribute("GSTNo", billingTO1.getGstNo());
                request.setAttribute("grDate", billingTO1.getGrDate());
                request.setAttribute("panNo", billingTO1.getPanNo());
                System.out.println("grDate:" + billingTO1.getGrDate());
                System.out.println("movementType:" + billingTO1.getMovementType());
                System.out.println("shipingBillNo:" + billingTO1.getShipingBillNo());
                System.out.println("billOfEntryNo:" + billingTO1.getBillOfEntryNo());
                System.out.println("billingState:" + billingTO1.getBillingState());
                System.out.println("organizationId:" + billingTO1.getOrganizationId());
                System.out.println("GSTNo:" + billingTO1.getGstNo());
            }
            request.setAttribute("tripsToBeBilledDetails", ordersToBeBillingDetails);
            request.setAttribute("tripsOtherExpenseDetails", tripsOtherExpenseDetails);
            request.setAttribute("reeferRequired", reeferRequired);
            request.setAttribute("consignmentOrderId", consignmentOrderId);
            if (consignmentOrderId != null) {
                if (consignmentOrderId.contains(",")) {
                    request.setAttribute("consignmentOrderCount", consignmentOrderId.split(",").length);
                } else {
                    request.setAttribute("consignmentOrderCount", 1);
                }
            }
            String revenueApprovalStatus = tripBP.getRevenueApprovalStatus(customerId, "1");//1 indicates gps non usage approval request
            System.out.println("revenueApprovalStatus:" + revenueApprovalStatus);
            request.setAttribute("revenueApprovalStatus", revenueApprovalStatus);
            path = "content/billing/viewOrderRepoBillDetails.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView updateTollAndDetaintionForBilling(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        //tripCommand = command;
        billingCommand = command;
        String menuPath = "Operation  >>  View start Trip Sheet ";
        int insertStatus = 0;
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

//            String consignmentOrderIds[]=request.getParameterValues("consignmentOrderIds");
            String billingParty = request.getParameter("billingParty");
            String detaintionRemarks = request.getParameter("detaintionRemarks");
            String billingPartyOld = request.getParameter("billingPartyOld");
            System.out.println("billingPartyIdOld=" + billingPartyOld);
            String billingPartyId = request.getParameter("billingPartyId");
            System.out.println("billingPartyId=" + billingPartyId);
            String billList = request.getParameter("billList");
            System.out.println("billList=" + billList);
            request.setAttribute("billList", billList);
            String movementType = request.getParameter("movementType");
            String grNo[] = request.getParameterValues("grNo");
            String consignmentOrderIds[] = request.getParameterValues("consignmentOrderIds");
            String tripId[] = request.getParameterValues("tripId");
            String tripIds = request.getParameter("tripIds");
            String tollOld[] = request.getParameterValues("tollChargeOld");
            String toll[] = request.getParameterValues("tollCharge");
            String detaintionOld[] = request.getParameterValues("detaintionChargeOld");
            String detaintion[] = request.getParameterValues("detaintionCharge");
            String vehicleId[] = request.getParameterValues("vehicleId");
            String driverId[] = request.getParameterValues("driverId");
            String containerNoOld[] = request.getParameterValues("containerNoOld");
            String containerNo[] = request.getParameterValues("containerNo");
            String tripContainerId[] = request.getParameterValues("tripContainerId");
            String consignmentConatinerId[] = request.getParameterValues("consignmentConatinerId");
            String greenTaxOld[] = request.getParameterValues("greenTaxOld");
            String greenTax[] = request.getParameterValues("greenTax");
            String otherExpense[] = request.getParameterValues("otherExpense");
            String otherExpenseOld[] = request.getParameterValues("otherExpenseOld");
            String weightment[] = request.getParameterValues("weightmentCharge");
            String weightmentOld[] = request.getParameterValues("weightmentChargeOld");
            String expenseType[] = request.getParameterValues("expenseType"); //expense type for weightment
            String shippingBillNoOld[] = request.getParameterValues("shippingBillNoOld");
            String shippingBillNo[] = request.getParameterValues("shippingBillNo");
            String billOfEntry[] = request.getParameterValues("billOfEntry");
            String billOfEntryOld[] = request.getParameterValues("billOfEntryOld");

            String commodityId = request.getParameter("commodityIds");
            String commodityName = request.getParameter("commodityNames");
            String commodityGstType = request.getParameter("commodityGstType");
            System.out.println("commodityId================" + commodityId);
            System.out.println("commodityName================" + commodityName);
            request.setAttribute("commodityId", commodityId);
            insertStatus = tripBP.updateTollAndDetaintionForBilling(grNo, tripId, toll, tollOld, detaintion, detaintionOld, vehicleId, driverId, containerNo, containerNoOld, tripContainerId,
                    consignmentConatinerId, greenTax, greenTaxOld, shippingBillNo, shippingBillNoOld, billOfEntry, billOfEntryOld, billList, movementType, billingPartyOld, billingParty, otherExpense, otherExpenseOld, weightment, weightmentOld, expenseType, userId, detaintionRemarks, commodityId, commodityName, commodityGstType);

//            insertStatus = tripBP.updateTollAndDetaintionForBilling(grNo, tripId, toll, tollOld, detaintion, detaintionOld, vehicleId, driverId, containerNo, containerNoOld, tripContainerId,
//                    consignmentConatinerId, greenTax, greenTaxOld, shippingBillNo, shippingBillNoOld, billOfEntry, billOfEntryOld, billList, movementType, billingPartyOld, billingParty, otherExpense, otherExpenseOld, weightment, weightmentOld, expenseType, userId, detaintionRemarks);
            //insertStatus = tripBP.updateBillingPartyForBilling(tripId,consignmentOrderIds,billOfEntry,billingParty,billingPartyId,movementType,userId);
            System.out.println("insertStatus" + insertStatus);
            request.setAttribute("sessionPageParam", session.getAttribute("sessionPageParam"));
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Trip  Details Updated Successfully....");
            mv = gererateOrderBill(request, response, command);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        // return new ModelAndView(path);
        return mv;
    }

    public ModelAndView updateTollAndDetaintionForSupplemetBilling(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        //tripCommand = command;
        billingCommand = command;
        String menuPath = "Operation  >>  View start Trip Sheet ";
        int insertStatus = 0;
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

//            String consignmentOrderIds[]=request.getParameterValues("consignmentOrderIds");
            String billingParty = request.getParameter("billingParty");
            String detaintionRemarks = request.getParameter("detaintionRemarks");
            String billingPartyOld = request.getParameter("billingPartyOld");
            System.out.println("billingPartyIdOld=" + billingPartyOld);
            String billingPartyId = request.getParameter("billingPartyId");
            System.out.println("billingPartyId=" + billingPartyId);
            String billList = request.getParameter("billList");
            System.out.println("billList=" + billList);
            request.setAttribute("billList", billList);
            String movementType = request.getParameter("movementType");
            String grNo[] = request.getParameterValues("grNo");
            String consignmentOrderIds[] = request.getParameterValues("consignmentOrderIds");
            String tripId[] = request.getParameterValues("tripId");
            String tripIds = request.getParameter("tripIds");
            String tollOld[] = request.getParameterValues("tollChargeOld");
            String toll[] = request.getParameterValues("tollCharge");
            String detaintionOld[] = request.getParameterValues("detaintionChargeOld");
            String detaintion[] = request.getParameterValues("detaintionCharge");
            String vehicleId[] = request.getParameterValues("vehicleId");
            String driverId[] = request.getParameterValues("driverId");
            String containerNoOld[] = request.getParameterValues("containerNoOld");
            String containerNo[] = request.getParameterValues("containerNo");
            String tripContainerId[] = request.getParameterValues("tripContainerId");
            String consignmentConatinerId[] = request.getParameterValues("consignmentConatinerId");
            String greenTaxOld[] = request.getParameterValues("greenTaxOld");
            String greenTax[] = request.getParameterValues("greenTax");
            String otherExpense[] = request.getParameterValues("otherExpense");
            String otherExpenseOld[] = request.getParameterValues("otherExpenseOld");
            String weightment[] = request.getParameterValues("weightmentCharge");
            String weightmentOld[] = request.getParameterValues("weightmentChargeOld");
            String expenseType[] = request.getParameterValues("expenseType"); //expense type for weightment
            String shippingBillNoOld[] = request.getParameterValues("shippingBillNoOld");
            String shippingBillNo[] = request.getParameterValues("shippingBillNo");
            String billOfEntry[] = request.getParameterValues("billOfEntry");
            String billOfEntryOld[] = request.getParameterValues("billOfEntryOld");

            System.out.println("weightment 1 :" + weightment[0]);

            String commodityId = request.getParameter("commodityIds");
            String commodityName = request.getParameter("commodityNames");
            String commodityGstType = request.getParameter("commodityGstType");

            System.out.println("weightment 1 :" + weightment[0]);

            insertStatus = tripBP.updateTollAndDetaintionForSupplemetBilling(grNo, tripId, toll, tollOld, detaintion, detaintionOld, vehicleId, driverId, containerNo, containerNoOld, tripContainerId,
                    consignmentConatinerId, greenTax, greenTaxOld, shippingBillNo, shippingBillNoOld, billOfEntry, billOfEntryOld, billList, movementType, billingPartyOld, billingPartyId, otherExpense, otherExpenseOld, weightment, weightmentOld, expenseType, userId, detaintionRemarks, commodityId, commodityName, commodityGstType);
            System.out.println("updateTollAndDetaintionForSupplemetBilling----" + insertStatus);

//            insertStatus = tripBP.updateTollAndDetaintionForSupplemetBilling(grNo, tripId, toll, tollOld, detaintion, detaintionOld, vehicleId, driverId, containerNo, containerNoOld, tripContainerId,
//                    consignmentConatinerId, greenTax, greenTaxOld, shippingBillNo, shippingBillNoOld, billOfEntry, billOfEntryOld, billList, movementType, billingPartyOld, billingPartyId, otherExpense, otherExpenseOld, weightment, weightmentOld, expenseType, userId, detaintionRemarks);
//            System.out.println("updateTollAndDetaintionForSupplemetBilling----" + insertStatus);
//            insertStatus = tripBP.updateBillingPartyForBilling(tripId,consignmentOrderIds,billOfEntry,billingParty,billingPartyId,movementType,userId);
//            System.out.println("updateBillingPartyForBilling----"+insertStatus);   
            System.out.println("insertStatus" + insertStatus);
            request.setAttribute("sessionPageParam", session.getAttribute("sessionPageParam"));
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Trip  Details Updated Successfully....");
            mv = gererateOrderSupplementBill(request, response, command);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        // return new ModelAndView(path);
        return mv;
    }

    public ModelAndView updateTollAndDetaintionForRepoBilling(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        System.out.println("updateTollAndDetaintionForBillingNEW");

        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        //tripCommand = command;
        billingCommand = command;
        String menuPath = "Operation  >>  View start Trip Sheet ";
        int insertStatus = 0;
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            String billingPartyId = request.getParameter("billingPartyId");
            String billingParty = request.getParameter("billingParty");
            String billingPartyOld = request.getParameter("billingPartyOld");
            String consignmentOrderIds[] = request.getParameterValues("consignmentOrderIds");

            String billList = request.getParameter("billList");
            System.out.println("billList1====" + billList);
            request.setAttribute("billList", billList);
            String movementType = request.getParameter("movementType");
            String grNo[] = request.getParameterValues("grNo");
            String tripId[] = request.getParameterValues("tripId");
            String tripIds = request.getParameter("tripIds");
            System.out.println("tripIdsREPO" + tripIds);
            String tollOld[] = request.getParameterValues("tollChargeOld");
            String toll[] = request.getParameterValues("tollCharge");
            String detaintionOld[] = request.getParameterValues("detaintionChargeOld");
            String detaintion[] = request.getParameterValues("detaintionCharge");
            String vehicleId[] = request.getParameterValues("vehicleId");
            String driverId[] = request.getParameterValues("driverId");
            String containerNoOld[] = request.getParameterValues("containerNoOld");
            String containerNo[] = request.getParameterValues("containerNo");
            String tripContainerId[] = request.getParameterValues("tripContainerId");
            String consignmentConatinerId[] = request.getParameterValues("consignmentConatinerId");
            String greenTaxOld[] = request.getParameterValues("greenTaxOld");
            String greenTax[] = request.getParameterValues("greenTax");
            String otherExpense[] = request.getParameterValues("otherExpense");
            String otherExpenseOld[] = request.getParameterValues("otherExpenseOld");
            String weightment[] = request.getParameterValues("weightmentCharge");
            String weightmentOld[] = request.getParameterValues("weightmentChargeOld");
            String expenseType[] = request.getParameterValues("expenseType"); //expense type for weightment

            String commodityId = request.getParameter("commodityIds");
            String commodityName = request.getParameter("commodityNames");
            String commodityGstType = request.getParameter("commodityGstType");
            System.out.println("commodityId================" + commodityId);
            System.out.println("commodityName================" + commodityName);
            request.setAttribute("commodityId", commodityId);

            insertStatus = tripBP.updateTollAndDetaintionForRepoBilling(grNo, tripId, toll, tollOld, detaintion, detaintionOld, vehicleId, driverId, containerNo, containerNoOld, tripContainerId,
                    consignmentConatinerId, greenTax, greenTaxOld, billList, movementType, billingPartyOld, billingParty, otherExpense, otherExpenseOld, weightment, weightmentOld, expenseType, userId, commodityId, commodityName, commodityGstType);
            insertStatus = tripBP.updateBillingPartyForRepoBilling(tripId, consignmentOrderIds, billingParty, billingPartyId, movementType, userId);

//            System.out.println("Raj  Here For Path Test");
//            path = "gererateRepoOrderBill.do?tripIds="+tripIds;
//            mv = new ModelAndView(path);
            System.out.println("insertStatus" + insertStatus);
            request.setAttribute("sessionPageParam", session.getAttribute("sessionPageParam"));
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Trip  Details Updated Successfully....");
            mv = gererateRepoOrderBill(request, response, command);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        // return new ModelAndView(path);
        return mv;
    }
//    public ModelAndView saveOrderBill(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/login.jsp");
//        }
//        HttpSession session = request.getSession();
//        String path = "";
//
//        String menuPath = "Finance  >>  Generate Bill ";
//        ModelAndView mv = null;
//        try {
//            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            int userId = (Integer) session.getAttribute("userId");
////            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
////                path = "content/common/NotAuthorized.jsp";
////            } else {
//            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            String pageTitle = "View Trip Sheet";
//            request.setAttribute("pageTitle", pageTitle);
//
//            BillingTO billingTO = new BillingTO();
//
//            String consignmentOrderId = request.getParameter("consignmentOrderId");
//            String tripSheetId = request.getParameter("tripSheetId");
//            String finYear = request.getParameter("finYear");
//            billingTO.setConsignmentOrderId(consignmentOrderId);
//            billingTO.setTripSheetId(tripSheetId);
//            String commodityName = request.getParameter("commodityName");
//            String gstType = request.getParameter("gstType");
//            String invoiceNo = request.getParameter("invoiceNo");
//            String customerId = request.getParameter("customerId");
//            String customerAddress = request.getParameter("billingPartyAddress");
//            String totalRevenue = request.getParameter("totalRevenue");
//            //  String estimatedRevenue = request.getParameter("estimatedRevenue");
//            String totalExpToBeBilled = request.getParameter("totalExpToBeBilled");
//            //String grandTotal = request.getParameter("grandTotal");
//            String noOfTrips = request.getParameter("noOfTrips");
//            String noOfOrders = request.getParameter("noOfOrders");
//            String billingPartyId = request.getParameter("billingPartyId");
//            String billingParty = request.getParameter("billingParty");
//            String remarks = request.getParameter("detaintionRemarks");
//            String grandTotal = request.getParameter("netAmount");
//            String repoOrder = request.getParameter("repoOrder");
//            String totalTax = request.getParameter("totalTax");
//            String cgstAmount = request.getParameter("cgstAmount");
//            String sgstAmount = request.getParameter("sgstAmount");
//            String igstAmount = request.getParameter("igstAmount");
//            String organizationId = request.getParameter("organizationId");
//            String billingState = request.getParameter("billingState");
//            String GSTNo = request.getParameter("GSTNo");
//            String PANNo = request.getParameter("panNo");
//            String igstPercentage = request.getParameter("IGST");
//            String cgstPercentage = request.getParameter("CGST");
//            String sgstPercentage = request.getParameter("SGST");
//
//            String otherExpense = request.getParameter("totOtherExpense");
//            billingTO.setIgstPercentage(igstPercentage);
//            billingTO.setCgstPercentage(cgstPercentage);
//            billingTO.setSgstPercentage(sgstPercentage);
//            billingTO.setIgstAmount(igstAmount);
//            billingTO.setCgstAmount(cgstAmount);
//            billingTO.setSgstAmount(sgstAmount);
//            billingTO.setTotalTax(totalTax);
//            billingTO.setOrganizationId(organizationId);
//            billingTO.setBillingState(billingState);
//            billingTO.setGstNo(GSTNo);
//            billingTO.setPanNo(PANNo);
//            billingTO.setOrderCount(noOfOrders);
//            billingTO.setNoOfTrips(noOfTrips);
//            billingTO.setUserId(userId + "");
//            billingTO.setCustomerId(customerId);
//            billingTO.setTotalRevenue(totalRevenue);
//            billingTO.setTotalExpToBeBilled(totalExpToBeBilled);
//            billingTO.setCustomerAddress(customerAddress);
//            billingTO.setBillingPartyId(billingPartyId);
//            billingTO.setBillingParty(billingParty);
//            billingTO.setRemarks(remarks);
//            billingTO.setGrandTotal(grandTotal);
//            billingTO.setFinYear(finYear);
//            billingTO.setCommodityName(commodityName);
//            billingTO.setGstType(gstType);
//            System.out.println("gstType invoice controller---"+gstType);
//
//            billingTO.setOtherExpense(otherExpense);
//            //generate tripcode
//            String[] tripIds = request.getParameterValues("tripId");
//            String[] tripIdIGSTAmount = request.getParameterValues("tripIGSTAmount");
//            String[] tripIdCGSTAmount = request.getParameterValues("tripCGSTAmount");
//            String[] tripIdSGSTAmount = request.getParameterValues("tripSGSTAmount");
//
//            int existStatus = tripBP.getTripExistInBilling(tripIds);
//            System.out.println("existStatus----" + existStatus);
//            int insertStatus = 0;
//            if (existStatus == 0) {
//                
////                String getCustGtaType=billingBP.getCustGtaType(billingTO);
//////                rohan
////                
////                   if( "Y".equals(getCustGtaType)){
////                
////                insertStatus = billingBP.saveOrderBillForGta(billingTO, userId, finYear);
////
////                
////                
////                int tripGst = billingBP.updateTripGst(String.valueOf(userId), tripIds,
////                        tripIdIGSTAmount, tripIdCGSTAmount, tripIdSGSTAmount);
////           
////                
////                }  else{
//                       
//                        insertStatus = billingBP.saveOrderBill(billingTO, userId, finYear);
//
//                
//                
//                int tripGst = billingBP.updateTripGst(String.valueOf(userId), tripIds,
//                        tripIdIGSTAmount, tripIdCGSTAmount, tripIdSGSTAmount);
//              
//                       
//                       
////                   }
//                     request.setAttribute("consignmentOrderId", consignmentOrderId);
//                request.setAttribute("invoiceNo", invoiceNo);
//                request.setAttribute("invoiceId", insertStatus);
//
//                System.out.println("request sessionPageParam:" + request.getParameter("sessionPageParam"));
//                System.out.println("session sessionPageParam:" + session.getAttribute("sessionPageParam"));
//                request.setAttribute("sessionPageParam", session.getAttribute("sessionPageParam"));
//
//                path = "viewOrderBillDetailsForSubmit.do?invoiceId=" + insertStatus + "&repoOrder=" + repoOrder + "&submitStatus=" + 0 + "&cancelStatus=1&sessionPageParam=" + session.getAttribute("sessionPageParam");
//
//                System.out.println("path:" + path);
//                       
//                       
//                
//            } else {
////<<<<<<< HEAD
//            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            String pageTitle = "View Trip Sheet";
//            request.setAttribute("pageTitle", pageTitle);
//
//            BillingTO billingTO = new BillingTO();
//
//            String consignmentOrderId = request.getParameter("consignmentOrderId");
//            String tripSheetId = request.getParameter("tripSheetId");
//            String finYear = request.getParameter("finYear");
//            billingTO.setConsignmentOrderId(consignmentOrderId);
//            billingTO.setTripSheetId(tripSheetId);
//            String commodityName = request.getParameter("commodityName");
//            String gstType = request.getParameter("gstType");
//            String invoiceNo = request.getParameter("invoiceNo");
//            String customerId = request.getParameter("customerId");
//            String customerAddress = request.getParameter("billingPartyAddress");
//            String totalRevenue = request.getParameter("totalRevenue");
//            //  String estimatedRevenue = request.getParameter("estimatedRevenue");
//            String totalExpToBeBilled = request.getParameter("totalExpToBeBilled");
//            //String grandTotal = request.getParameter("grandTotal");
//            String noOfTrips = request.getParameter("noOfTrips");
//            String noOfOrders = request.getParameter("noOfOrders");
//            String billingPartyId = request.getParameter("billingPartyId");
//            String billingParty = request.getParameter("billingParty");
//            String remarks = request.getParameter("detaintionRemarks");
//            String grandTotal = request.getParameter("netAmount");
//            String repoOrder = request.getParameter("repoOrder");
//            String totalTax = request.getParameter("totalTax");
//            String cgstAmount = request.getParameter("cgstAmount");
//            String sgstAmount = request.getParameter("sgstAmount");
//            String igstAmount = request.getParameter("igstAmount");
//            String organizationId = request.getParameter("organizationId");
//            String billingState = request.getParameter("billingState");
//            String GSTNo = request.getParameter("GSTNo");
//            String PANNo = request.getParameter("panNo");
//            String igstPercentage = request.getParameter("IGST");
//            String cgstPercentage = request.getParameter("CGST");
//            String sgstPercentage = request.getParameter("SGST");
//
//            String otherExpense = request.getParameter("totOtherExpense");
//            billingTO.setIgstPercentage(igstPercentage);
//            billingTO.setCgstPercentage(cgstPercentage);
//            billingTO.setSgstPercentage(sgstPercentage);
//            billingTO.setIgstAmount(igstAmount);
//            billingTO.setCgstAmount(cgstAmount);
//            billingTO.setSgstAmount(sgstAmount);
//            billingTO.setTotalTax(totalTax);
//            billingTO.setOrganizationId(organizationId);
//            billingTO.setBillingState(billingState);
//            billingTO.setGstNo(GSTNo);
//            billingTO.setPanNo(PANNo);
//            billingTO.setOrderCount(noOfOrders);
//            billingTO.setNoOfTrips(noOfTrips);
//            billingTO.setUserId(userId + "");
//            billingTO.setCustomerId(customerId);
//            billingTO.setTotalRevenue(totalRevenue);
//            billingTO.setTotalExpToBeBilled(totalExpToBeBilled);
//            billingTO.setCustomerAddress(customerAddress);
//            billingTO.setBillingPartyId(billingPartyId);
//            billingTO.setBillingParty(billingParty);
//            billingTO.setRemarks(remarks);
//            billingTO.setGrandTotal(grandTotal);
//            billingTO.setFinYear(finYear);
//            billingTO.setCommodityName(commodityName);
//            billingTO.setGstType(gstType);
//            System.out.println("gstType invoice controller---"+gstType);
//
//            billingTO.setOtherExpense(otherExpense);
//            //generate tripcode
//            String[] tripIds = request.getParameterValues("tripId");
//            String[] tripIdIGSTAmount = request.getParameterValues("tripIGSTAmount");
//            String[] tripIdCGSTAmount = request.getParameterValues("tripCGSTAmount");
//            String[] tripIdSGSTAmount = request.getParameterValues("tripSGSTAmount");
//
//            int existStatus = tripBP.getTripExistInBilling(tripIds);
//            System.out.println("existStatus----" + existStatus);
//            int insertStatus = 0;
//            if (existStatus == 0) {
//                
////                String getCustGtaType=billingBP.getCustGtaType(billingTO);
////                rohan
//                
////                   if( "Y".equals(getCustGtaType)){
////                
////                insertStatus = billingBP.saveOrderBillForGta(billingTO, userId, finYear);
////
////                
////                
////                int tripGst = billingBP.updateTripGst(String.valueOf(userId), tripIds,
////                        tripIdIGSTAmount, tripIdCGSTAmount, tripIdSGSTAmount);
////                request.setAttribute("consignmentOrderId", consignmentOrderId);
////                request.setAttribute("invoiceNo", invoiceNo);
////                request.setAttribute("invoiceId", insertStatus);
////
////                System.out.println("request sessionPageParam:" + request.getParameter("sessionPageParam"));
////                System.out.println("session sessionPageParam:" + session.getAttribute("sessionPageParam"));
////                request.setAttribute("sessionPageParam", session.getAttribute("sessionPageParam"));
////
////                path = "viewOrderBillDetailsForSubmit.do?invoiceId=" + insertStatus + "&repoOrder=" + repoOrder + "&submitStatus=" + 0 + "&cancelStatus=1&sessionPageParam=" + session.getAttribute("sessionPageParam");
////                System.out.println("path:" + path);
////                
////                }  else{
//                       
//                        insertStatus = billingBP.saveOrderBill(billingTO, userId, finYear);
//
//                
//                
//                int tripGst = billingBP.updateTripGst(String.valueOf(userId), tripIds,
//                        tripIdIGSTAmount, tripIdCGSTAmount, tripIdSGSTAmount);
//                request.setAttribute("consignmentOrderId", consignmentOrderId);
//                request.setAttribute("invoiceNo", invoiceNo);
//                request.setAttribute("invoiceId", insertStatus);
//
//                System.out.println("request sessionPageParam:" + request.getParameter("sessionPageParam"));
//                System.out.println("session sessionPageParam:" + session.getAttribute("sessionPageParam"));
//                request.setAttribute("sessionPageParam", session.getAttribute("sessionPageParam"));
//
//                path = "viewOrderBillDetailsForSubmit.do?invoiceId=" + insertStatus + "&repoOrder=" + repoOrder + "&submitStatus=" + 0 + "&cancelStatus=1&sessionPageParam=" + session.getAttribute("sessionPageParam");
//
//                System.out.println("path:" + path);
//                       
//                       
//                       
//                       
////                   }
//                
//            } else {
//                path = "gererateOrderBill.do?sessionPageParam=" + session.getAttribute("sessionPageParam");
//            }
//
//        } catch (FPRuntimeException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            return new ModelAndView("content/common/error.jsp");
//        } catch (FPBusinessException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
//                    exception.getErrorMessage());
//        } catch (Exception exception) {
//            exception.printStackTrace();
//            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        }
//        return new ModelAndView(path);
//    }

//                path = "gererateOrderBill.do?sessionPageParam=" + session.getAttribute("sessionPageParam");
//            }
//
//        } catch (FPRuntimeException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            return new ModelAndView("content/common/error.jsp");
//        } catch (FPBusinessException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
//                    exception.getErrorMessage());
//        } catch (Exception exception) {
//            exception.printStackTrace();
//            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        }
//        return new ModelAndView(path);
//    }
//>>>>>>> be1ec7e6c6c777b2c5aa6e65856f5dc714087c42
    public ModelAndView saveOrderSupplementBill(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Finance  >>  Generate Bill ";
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            BillingTO billingTO = new BillingTO();

            String consignmentOrderId = request.getParameter("consignmentOrderId");
            String tripSheetId = request.getParameter("tripSheetId");
            billingTO.setConsignmentOrderId(consignmentOrderId);
            billingTO.setTripSheetId(tripSheetId);
            String invoiceNo = request.getParameter("invoiceNo");
            String finYear = request.getParameter("finYear");
            String customerId = request.getParameter("customerId");
            String customerAddress = request.getParameter("billingPartyAddress");
            String totalRevenue = request.getParameter("totalRevenue");
            //  String estimatedRevenue = request.getParameter("estimatedRevenue");
            String totalExpToBeBilled = request.getParameter("totalExpToBeBilled");
            //String grandTotal = request.getParameter("grandTotal");
            String noOfTrips = request.getParameter("noOfTrips");
            String noOfOrders = request.getParameter("noOfOrders");
            String billingPartyId = request.getParameter("billingPartyId");
            String billingParty = request.getParameter("billingParty");
            String remarks = request.getParameter("detaintionRemarks");
            String grandTotal = request.getParameter("netAmount");
            String repoOrder = request.getParameter("repoOrder");
            String totalTax = request.getParameter("totalTax");
            String cgstAmount = request.getParameter("cgstAmount");
            String sgstAmount = request.getParameter("sgstAmount");
            String igstAmount = request.getParameter("igstAmount");
            String organizationId = request.getParameter("organizationId");
            String billingState = request.getParameter("billingState");
            String GSTNo = request.getParameter("GSTNo");
            String PANNo = request.getParameter("panNo");
            String igstPercentage = request.getParameter("IGST");
            String cgstPercentage = request.getParameter("CGST");
            String sgstPercentage = request.getParameter("SGST");
            String invoiceId = request.getParameter("invoiceId");
            String otherExpense = request.getParameter("totOtherExpense");
            String gstType = request.getParameter("gstType");
            billingTO.setInvoiceNo(invoiceId);
            billingTO.setIgstPercentage(igstPercentage);
            billingTO.setCgstPercentage(cgstPercentage);
            billingTO.setSgstPercentage(sgstPercentage);
            billingTO.setIgstAmount(igstAmount);
            billingTO.setCgstAmount(cgstAmount);
            billingTO.setSgstAmount(sgstAmount);
            billingTO.setTotalTax(totalTax);
            billingTO.setOrganizationId(organizationId);
            billingTO.setBillingState(billingState);
            billingTO.setGstNo(GSTNo);
            billingTO.setPanNo(PANNo);
            billingTO.setOrderCount(noOfOrders);
            billingTO.setNoOfTrips(noOfTrips);
            billingTO.setUserId(userId + "");
            billingTO.setCustomerId(customerId);
            billingTO.setTotalRevenue(totalRevenue);
            billingTO.setTotalExpToBeBilled(totalExpToBeBilled);
            billingTO.setCustomerAddress(customerAddress);
            billingTO.setBillingPartyId(billingPartyId);
            billingTO.setBillingParty(billingParty);
            billingTO.setRemarks(remarks);
            billingTO.setGrandTotal(grandTotal);
            billingTO.setGstType(gstType);

            billingTO.setOtherExpense(otherExpense);
            //generate tripcode
            String[] supplementsCharges = request.getParameterValues("supplementCharge");
            String[] detaintionCharge = request.getParameterValues("detaintionCharge");
            String[] tollCharge = request.getParameterValues("tollCharge");
            String[] greenTax = request.getParameterValues("greenTax");
            String[] otherExpenses = request.getParameterValues("otherExpense");
            String[] weightmentCharge = request.getParameterValues("weightmentCharge");
            String[] tripIds = request.getParameterValues("tripId");
            String[] tripIdIGSTAmount = request.getParameterValues("tripIGSTAmount");
            String[] tripIdCGSTAmount = request.getParameterValues("tripCGSTAmount");
            String[] tripIdSGSTAmount = request.getParameterValues("tripSGSTAmount");

            int existStatus = tripBP.getTripExistInBilling(tripIds);
            int insertStatus = 0;
            System.out.println("eeeeeeeeeeeeeeeeeeee" + existStatus);
            if (existStatus == 0) {
//                String getCustGtaType=billingBP.getCustGtaType(billingTO);
////
//                if("Y".equals(getCustGtaType)){
//                    System.out.println("inside GTA");
//                insertStatus = billingBP.saveOrderSupplementBillForGta(billingTO, supplementsCharges, detaintionCharge, tollCharge, greenTax, otherExpenses, weightmentCharge, tripIds, userId, finYear);
//                }else{
                    
                insertStatus = billingBP.saveOrderSupplementBill(billingTO, supplementsCharges, detaintionCharge, tollCharge, greenTax, otherExpenses, weightmentCharge, tripIds, userId, finYear);
//            } 

                int tripGst = billingBP.updateTripGst(String.valueOf(userId), tripIds,
                        tripIdIGSTAmount, tripIdCGSTAmount, tripIdSGSTAmount);
                request.setAttribute("consignmentOrderId", consignmentOrderId);
                request.setAttribute("invoiceNo", invoiceNo);
                request.setAttribute("invoiceId", insertStatus);

//            path = "content/trip/responsePage.jsp";
//            path = "viewOrderBillDetailsForSubmit.do?invoiceId="+invoiceId+"&submitStatus="+0+"&cancelStatus="+1;
                System.out.println("request sessionPageParam:" + request.getParameter("sessionPageParam"));
                System.out.println("session sessionPageParam:" + session.getAttribute("sessionPageParam"));
                request.setAttribute("sessionPageParam", session.getAttribute("sessionPageParam"));

                path = "viewOrderSuppBillDetailsForSubmit.do?invoiceId=" + insertStatus + "&repoOrder=" + repoOrder + "&submitStatus=" + 0 + "&cancelStatus=1&sessionPageParam=" + session.getAttribute("sessionPageParam");
//            mv = viewOrderBillDetailsForSubmit(request,response,command);
                System.out.println("path:" + path);

//                }
            } 
            else {
                System.out.println("pathelseeeeeeeeeeeeeeeeeee:" + path);
                path = "generateOrderSupplementBill.do?sessionPageParam=" + session.getAttribute("sessionPageParam");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewOrderBillForSubmit(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        int userId = 0;
        HttpSession session = null;
        billingCommand = command;
        String menuPath = "";
        BillingTO billingTO = null;
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  View Bill For Submission";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String fromDate = "";
        String toDate = "";
        String customerId = "";
        String tripType = "";
        String submitStatus = "";
        String param = "";
        String billNo = "";
        String grNo = "";
        String containerNo = "";
        try {
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            param = request.getParameter("param");
            customerId = request.getParameter("customerId");
            fromDate = request.getParameter("fromDate");
            toDate = request.getParameter("toDate");
            submitStatus = request.getParameter("submitStatus");
            tripType = request.getParameter("tripType");
            billNo = request.getParameter("billNo");
            grNo = request.getParameter("grNo");
            containerNo = request.getParameter("containerNo");

            request.setAttribute("customerId", customerId);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("tripType", tripType);
            request.setAttribute("submitStatus", submitStatus);
            request.setAttribute("billNo", billNo);
            request.setAttribute("grNo", grNo);
            request.setAttribute("containerNo", containerNo);

            int status = 0;
            String cancelStatus = request.getParameter("cancelStatus");
            String billingNo = request.getParameter("billingNo");

            if (cancelStatus != null) {
                status = billingBP.updateCancel(billingNo, userId);
                System.out.println("status" + status);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, billingNo + " billingNo  cancelled Successfully....");
            }

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getTripCustomerList();
            request.setAttribute("customerList", customerList);
            System.out.println("customerList Size Is :::" + customerList.size());

            ArrayList closedBillList = new ArrayList();
            //closedBillList = operationBP.getClosedBilledList(customerId, fromDate, toDate);
            closedBillList = billingBP.viewBillForSubmission(customerId, fromDate, toDate, tripType, submitStatus, billNo, grNo, containerNo);
            request.setAttribute("closedBillList", closedBillList);
            System.out.println("closedBillList Size Is :::" + closedBillList.size());

            if (!"ExcelExport".equals(param)) {
                path = "content/billing/viewOrderBillForSubmission.jsp";
            } else {
                path = "content/billing/viewOrderBillForSubmissionExport.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewOrderSuppBillForSubmit(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        int userId = 0;
        HttpSession session = null;
        billingCommand = command;
        String menuPath = "";
        BillingTO billingTO = null;
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  View Bill For Submission";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String fromDate = "";
        String toDate = "";
        String customerId = "";
        String tripType = "";
        String submitStatus = "";
        String param = "";
        String billNo = "";
        String grNo = "";
        try {
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            param = request.getParameter("param");
            customerId = request.getParameter("customerId");
            fromDate = request.getParameter("fromDate");
            toDate = request.getParameter("toDate");
            submitStatus = request.getParameter("submitStatus");
            tripType = request.getParameter("tripType");
            grNo = request.getParameter("grNo");
            billNo = request.getParameter("billNo");

            request.setAttribute("customerId", customerId);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("tripType", tripType);
            request.setAttribute("submitStatus", submitStatus);
            request.setAttribute("billNo", billNo);
            request.setAttribute("grNo", grNo);

            int status = 0;
            String cancelStatus = request.getParameter("cancelStatus");
            String billingNo = request.getParameter("billingNo");

            if (cancelStatus != null) {
                status = billingBP.updateCancel(billingNo, userId);
                System.out.println("status" + status);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, billingNo + " billingNo  cancelled Successfully....");
            }

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getTripCustomerList();
            request.setAttribute("customerList", customerList);
            System.out.println("customerList Size Is :::" + customerList.size());

            ArrayList closedBillList = new ArrayList();
            //closedBillList = operationBP.getClosedBilledList(customerId, fromDate, toDate);
            closedBillList = billingBP.viewSuppBillForSubmission(customerId, fromDate, toDate, tripType, submitStatus, billNo, grNo);
            request.setAttribute("closedBillList", closedBillList);
            System.out.println("closedBillList Size Is :::" + closedBillList.size());

            if (!"ExcelExport".equals(param)) {
                path = "content/billing/viewOrderSuppBillForSubmission.jsp";
            } else {
                path = "content/billing/viewOrderSuppBillForSubmissionExport.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewOrderBillDetailsForSubmit(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        int userId = 0;
        HttpSession session = null;
        billingCommand = command;
        String menuPath = "";
        BillingTO billingTO = new BillingTO();
        TripTO tripTO = new TripTO();
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  View Bill For Submission";
        request.setAttribute("pageTitle", pageTitle);
        String status = "1";
        request.setAttribute("status", status);

        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            session = request.getSession();
            int roleId = (Integer) session.getAttribute("RoleId");
            System.out.println("roleId=@@@@......" + roleId);
            request.setAttribute("roleId", roleId);

            //for cancell the invoice after generate the bill
            String cancel = "0";
            request.setAttribute("cancelStatus", cancel);
            String cancelStatus = request.getParameter("cancelStatus");
            String repoOrder = request.getParameter("repoOrder");
            request.setAttribute("repoOrder", repoOrder);
            String invoiceId = request.getParameter("invoiceId");
            tripTO.setInvoiceId(invoiceId);
            String[] tempRemarks = null;
            //for cancell the invoice after generate the bill
            if ("1".equals(cancelStatus)) {
                path = "content/billing/viewOrderBillDetailsForSubmissionPrintPreview.jsp";
                System.out.println("if!!!!----------");
            } else {
                String invoiceType = "1";
                String checkInvoiceType = tripBP.checkInvoiceType(invoiceId, invoiceType);
                System.out.println("checkInvoiceType===$$$$=" + checkInvoiceType);
                tripTO.setInvoiceType(invoiceType);

                System.out.println("inside=11==$$$$=--" + checkInvoiceType);

                if ("Y".equals(checkInvoiceType)) {
                    ArrayList eInvoiceHeader = tripBP.getOrderEInvoiceHeader(tripTO);
                    System.out.println("eInvoiceHeader ilist111---" + eInvoiceHeader.size());
                    Iterator itr = eInvoiceHeader.iterator();
                    TripTO tpTO = new TripTO();
                    while (itr.hasNext()) {
                        tpTO = (TripTO) itr.next();
                        request.setAttribute("billingParty", tpTO.getBillingParty());
                        System.out.println("billingParty:" + tpTO.getBillingParty());
                        request.setAttribute("creditNoteNo", tpTO.getCreditNoteNo());
                        request.setAttribute("creditNoteDate", tpTO.getCreditNoteDate());
                        request.setAttribute("createdBy", tpTO.getCreatedBy());
                        request.setAttribute("customerName", tpTO.getCustomerName());
                        request.setAttribute("taxValue", tpTO.getTaxValue());
                        request.setAttribute("customerAddress", tpTO.getCustomerAddress());
                        request.setAttribute("panNo", tpTO.getPanNo());
                        request.setAttribute("gstNo", tpTO.getGstNo());
                        request.setAttribute("cityName", tpTO.getCityName());
                        request.setAttribute("state", tpTO.getState());
                        request.setAttribute("invoicecode", tpTO.getInvoiceCode());
                        request.setAttribute("consignmentOrderNos", tpTO.getcNotes());
                        request.setAttribute("consignmentOrderDate", tpTO.getConsignmentOrderDate());
                        request.setAttribute("billDate", tpTO.getBillDate());
                        request.setAttribute("movementType", tpTO.getMovementType());
                        request.setAttribute("containerTypeName", tpTO.getContainerTypeName());
                        request.setAttribute("billingType", tpTO.getBillingType());
                        request.setAttribute("userName", tpTO.getUserName());
                        request.setAttribute("billingState", tpTO.getBillingState());
                        request.setAttribute("irn", tpTO.getIrn());
                        request.setAttribute("signedQr", tpTO.getSignedQr());
                        request.setAttribute("workOrderNumber", tpTO.getWorkOrderNumber());
                        System.out.println("irn:" + tpTO.getIrn());
                        System.out.println("signedQR:" + tpTO.getSignedQr());
                        System.out.println("remaarks:" + tpTO.getRemarks());
                        if (!"".equals(tpTO.getRemarks())) {
                            tempRemarks = tpTO.getRemarks().split(",");
                            if (tempRemarks.length >= 1) {
                                request.setAttribute("firstRemarks", tempRemarks[0]);
                                System.out.println("firstRemarks:" + tempRemarks[0]);
                            }
                            if (tempRemarks.length >= 2) {
                                request.setAttribute("secondRemarks", tempRemarks[1]);
                            }
                            if (tempRemarks.length >= 3) {
                                request.setAttribute("thirdRemarks", tempRemarks[2]);
                            }
                        }

                    }
                    System.out.println("SSSSSS");
                    path = "content/billing/viewOrderBillDetailsForEInvoiceSubmission.jsp";
                } else {
                    path = "content/billing/viewOrderBillDetailsForSubmission.jsp";

                    System.out.println("else!!!!!----------");
                }
            }

            billingTO.setInvoiceId(invoiceId);

            request.setAttribute("invoiceId", invoiceId);

            ArrayList invoiceHeader = tripBP.getOrderInvoiceHeader(tripTO);
            ArrayList invoiceDetails = tripBP.getInvoiceDetails(tripTO);
            ArrayList invoiceDetailExpenses = tripBP.getInvoiceDetailExpenses(tripTO);
            ArrayList invoiceTaxDetails = billingBP.getInvoiceGSTTaxDetails(billingTO);
            request.setAttribute("invoiceHeader", invoiceHeader);
            request.setAttribute("invoiceTaxDetails", invoiceTaxDetails);
            request.setAttribute("invoiceTaxCheck", "1");
            System.out.println("invoiceTaxDetailsSize:" + invoiceTaxDetails.size());
            request.setAttribute("invoiceTaxDetailsSize", invoiceTaxDetails.size());

            Iterator itr = invoiceHeader.iterator();
            TripTO tpTO = new TripTO();
            while (itr.hasNext()) {
                tpTO = (TripTO) itr.next();
                request.setAttribute("billingParty", tpTO.getBillingParty());
                System.out.println("billingParty:" + tpTO.getBillingParty());
                request.setAttribute("creditNoteNo", tpTO.getCreditNoteNo());
                request.setAttribute("creditNoteDate", tpTO.getCreditNoteDate());
                request.setAttribute("createdBy", tpTO.getCreatedBy());
                request.setAttribute("customerName", tpTO.getCustomerName());
                request.setAttribute("taxValue", tpTO.getTaxValue());
                request.setAttribute("customerAddress", tpTO.getCustomerAddress());
                request.setAttribute("panNo", tpTO.getPanNo());
                request.setAttribute("gstNo", tpTO.getGstNo());
                request.setAttribute("gstType", tpTO.getGstType());
                request.setAttribute("cityName", tpTO.getCityName());
                request.setAttribute("state", tpTO.getState());
                request.setAttribute("invoicecode", tpTO.getInvoiceCode());
                request.setAttribute("consignmentOrderNos", tpTO.getcNotes());
                request.setAttribute("consignmentOrderDate", tpTO.getConsignmentOrderDate());
                request.setAttribute("billDate", tpTO.getBillDate());
                request.setAttribute("movementType", tpTO.getMovementType());
                request.setAttribute("containerTypeName", tpTO.getContainerTypeName());
                request.setAttribute("billingType", tpTO.getBillingType());
                request.setAttribute("userName", tpTO.getUserName());
                request.setAttribute("billingState", tpTO.getBillingState());
                request.setAttribute("workOrderNumber", tpTO.getWorkOrderNumber());
//                request.setAttribute("irn", tpTO.getIrn());
//                request.setAttribute("signedQr", tpTO.getSignedQr());
                System.out.println("remaarks:" + tpTO.getRemarks());
                if (!"".equals(tpTO.getRemarks())) {
                    tempRemarks = tpTO.getRemarks().split(",");
                    if (tempRemarks.length >= 1) {
                        request.setAttribute("firstRemarks", tempRemarks[0]);
                        System.out.println("firstRemarks:" + tempRemarks[0]);
                    }
                    if (tempRemarks.length >= 2) {
                        request.setAttribute("secondRemarks", tempRemarks[1]);
                    }
                    if (tempRemarks.length >= 3) {
                        request.setAttribute("thirdRemarks", tempRemarks[2]);
                    }
                }

            }
            request.setAttribute("invoiceDetails", invoiceDetails);
            if (invoiceDetailExpenses.size() > 0) {
                request.setAttribute("invoiceDetailExpenses", invoiceDetailExpenses);
            }
            ArrayList invoiceDetailsList = new ArrayList();
            invoiceDetailsList = tripBP.getOrderInvoiceDetailsList(tripTO);
            request.setAttribute("invoiceDetailsList", invoiceDetailsList);
            itr = invoiceDetailsList.iterator();

            while (itr.hasNext()) {
                tpTO = (TripTO) itr.next();
                request.setAttribute("remarks", tpTO.getRemarks());
            }
            ArrayList tripDetails = new ArrayList();
            tripDetails = billingBP.getTripDetails(billingTO);
            request.setAttribute("tripDetails", tripDetails);
            request.setAttribute("invoiceId", invoiceId);
            request.setAttribute("submitStatus", request.getParameter("submitStatus"));
            System.out.println("invoiceDetailsList.size():" + invoiceDetailsList.size());
            request.setAttribute("invoiceDetailsListSize", invoiceDetailsList.size());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewOrderSuppBillDetailsForSubmit(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        int userId = 0;
        HttpSession session = null;
        billingCommand = command;
        String menuPath = "";
        BillingTO billingTO = new BillingTO();
        TripTO tripTO = new TripTO();
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  View Bill For Submission";
        request.setAttribute("pageTitle", pageTitle);
        String status = "1";
        request.setAttribute("status", status);

        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            session = request.getSession();
            int roleId = (Integer) session.getAttribute("RoleId");
            System.out.println("roleId=@@@@......" + roleId);
            request.setAttribute("roleId", roleId);

            //for cancell the invoice after generate the bill
            String cancel = "0";
            String tempRemarks[] = null;
            request.setAttribute("cancelStatus", cancel);
            String cancelStatus = request.getParameter("cancelStatus");
            String repoOrder = request.getParameter("repoOrder");
            request.setAttribute("repoOrder", repoOrder);

            String invoiceId = request.getParameter("invoiceId");
            billingTO.setInvoiceId(invoiceId);
            tripTO.setInvoiceId(invoiceId);
            request.setAttribute("invoiceId", invoiceId);

            //for cancell the invoice after generate the bill
            if ("1".equals(cancelStatus)) {
                path = "content/billing/viewOrderBillDetailsForSubmissionSupplement.jsp";
            } else {
                String invoiceType = "2";
                String checkInvoiceType = tripBP.checkInvoiceType(invoiceId, invoiceType);
                System.out.println("checkInvoiceType===$$$$=" + checkInvoiceType);
                tripTO.setInvoiceType(invoiceType);
                if ("Y".equals(checkInvoiceType)) {
                    System.out.println("inside===$$$$=" + checkInvoiceType);
                    ArrayList eInvoiceHeader = tripBP.getOrderEInvoiceHeader(tripTO);
                    System.out.println("eInvoiceHeader ilist00---" + eInvoiceHeader.size());
                    Iterator itr = eInvoiceHeader.iterator();
                    TripTO tpTO = new TripTO();
                    while (itr.hasNext()) {
                        tpTO = (TripTO) itr.next();
                        request.setAttribute("billingParty", tpTO.getBillingParty());
                        System.out.println("billingParty:" + tpTO.getBillingParty());
                        request.setAttribute("creditNoteNo", tpTO.getCreditNoteNo());
                        request.setAttribute("creditNoteDate", tpTO.getCreditNoteDate());
                        request.setAttribute("createdBy", tpTO.getCreatedBy());
                        request.setAttribute("customerName", tpTO.getCustomerName());
                        request.setAttribute("taxValue", tpTO.getTaxValue());
                        request.setAttribute("customerAddress", tpTO.getCustomerAddress());
                        request.setAttribute("panNo", tpTO.getPanNo());
                        request.setAttribute("gstNo", tpTO.getGstNo());
                        request.setAttribute("gstType", tpTO.getGstType());
                        System.out.println("gstType=----" + tpTO.getGstType());
                        request.setAttribute("cityName", tpTO.getCityName());
                        request.setAttribute("state", tpTO.getState());
                        request.setAttribute("invoicecode", tpTO.getInvoiceCode());
                        request.setAttribute("consignmentOrderNos", tpTO.getcNotes());
                        request.setAttribute("consignmentOrderDate", tpTO.getConsignmentOrderDate());
                        request.setAttribute("billDate", tpTO.getBillDate());
                        request.setAttribute("movementType", tpTO.getMovementType());
                        request.setAttribute("containerTypeName", tpTO.getContainerTypeName());
                        request.setAttribute("billingType", tpTO.getBillingType());
                        request.setAttribute("userName", tpTO.getUserName());
                        request.setAttribute("billingState", tpTO.getBillingState());
                        request.setAttribute("irn", tpTO.getIrn());
                        request.setAttribute("signedQr", tpTO.getSignedQr());
                         request.setAttribute("workOrderNumber", tpTO.getWorkOrderNumber());
                        System.out.println("irn:" + tpTO.getIrn());
                        System.out.println("signedQR:" + tpTO.getSignedQr());
                        System.out.println("remaarks:" + tpTO.getRemarks());
                        if (!"".equals(tpTO.getRemarks())) {
                            tempRemarks = tpTO.getRemarks().split(",");
                            if (tempRemarks.length >= 1) {
                                request.setAttribute("firstRemarks", tempRemarks[0]);
                                System.out.println("firstRemarks:" + tempRemarks[0]);
                            }
                            if (tempRemarks.length >= 2) {
                                request.setAttribute("secondRemarks", tempRemarks[1]);
                            }
                            if (tempRemarks.length >= 3) {
                                request.setAttribute("thirdRemarks", tempRemarks[2]);
                            }
                        }

                    }
                    System.out.println("SSSSSS");
                    path = "content/billing/viewOrderBillDetailsForESuppInvoiceSubmission.jsp";
                } else {
                    request.setAttribute("gstType", checkInvoiceType);  //gsttype
                    path = "content/billing/viewOrderSuppBillDetailsForSubmission.jsp";

                    System.out.println("else!!!!!----------");
                }

//                    path = "content/billing/viewOrderSuppBillDetailsForSubmission.jsp";
            }

            ArrayList invoiceHeader = tripBP.getOrderSuppInvoiceHeader(tripTO);
            ArrayList invoiceDetails = tripBP.getInvoiceDetails(tripTO);
            ArrayList invoiceDetailExpenses = tripBP.getInvoiceSupplementDetailExpenses(tripTO);
            ArrayList invoiceTaxDetails = billingBP.getInvoiceSupplementGSTTaxDetails(billingTO);
            request.setAttribute("invoiceHeader", invoiceHeader);
            request.setAttribute("invoiceTaxDetails", invoiceTaxDetails);
            request.setAttribute("invoiceTaxCheck", "1");
            System.out.println("invoiceTaxDetailsSize:" + invoiceTaxDetails.size());
            request.setAttribute("invoiceTaxDetailsSize", invoiceTaxDetails.size());
            Iterator itr = invoiceHeader.iterator();
            TripTO tpTO = new TripTO();
            while (itr.hasNext()) {
                tpTO = (TripTO) itr.next();
                request.setAttribute("billingParty", tpTO.getBillingParty());
                System.out.println("billingParty:" + tpTO.getBillingParty());
                request.setAttribute("creditNoteNo", tpTO.getCreditNoteNo());
                request.setAttribute("creditNoteDate", tpTO.getCreditNoteDate());
                request.setAttribute("createdBy", tpTO.getCreatedBy());
                request.setAttribute("customerName", tpTO.getCustomerName());
                request.setAttribute("taxValue", tpTO.getTaxValue());
                request.setAttribute("customerAddress", tpTO.getCustomerAddress());
                request.setAttribute("panNo", tpTO.getPanNo());
                request.setAttribute("gstNo", tpTO.getGstNo());
                request.setAttribute("gstType", tpTO.getGstType());
                System.out.println("gstType******" + tpTO.getGstType());
                request.setAttribute("cityName", tpTO.getCityName());
                request.setAttribute("state", tpTO.getState());
                request.setAttribute("invoicecode", tpTO.getInvoiceCode());
                request.setAttribute("consignmentOrderNos", tpTO.getcNotes());
                request.setAttribute("consignmentOrderDate", tpTO.getConsignmentOrderDate());
                request.setAttribute("billDate", tpTO.getBillDate());
                request.setAttribute("movementType", tpTO.getMovementType());
                request.setAttribute("containerTypeName", tpTO.getContainerTypeName());
                request.setAttribute("billingType", tpTO.getBillingType());
                request.setAttribute("userName", tpTO.getUserName());
                 request.setAttribute("workOrderNumber", tpTO.getWorkOrderNumber());
                System.out.println("remaarks:" + tpTO.getRemarks());
                if (!"".equals(tpTO.getRemarks())) {
                    tempRemarks = tpTO.getRemarks().split(",");
                    if (tempRemarks.length >= 1) {
                        request.setAttribute("firstRemarks", tempRemarks[0]);
                        System.out.println("firstRemarks:" + tempRemarks[0]);
                    }
                    if (tempRemarks.length >= 2) {
                        request.setAttribute("secondRemarks", tempRemarks[1]);
                    }
                    if (tempRemarks.length >= 3) {
                        request.setAttribute("thirdRemarks", tempRemarks[2]);
                    }
                }

            }
            request.setAttribute("invoiceDetails", invoiceDetails);
            if (invoiceDetailExpenses.size() > 0) {
                request.setAttribute("invoiceDetailExpenses", invoiceDetailExpenses);
            }
            ArrayList invoiceDetailsList = new ArrayList();
            invoiceDetailsList = tripBP.getOrderSuppInvoiceDetailsList(tripTO);
            request.setAttribute("invoiceDetailsList", invoiceDetailsList);
            ArrayList tripDetails = new ArrayList();
            tripDetails = billingBP.getTripDetails(billingTO);
            request.setAttribute("tripDetails", tripDetails);
            request.setAttribute("invoiceId", invoiceId);
            request.setAttribute("submitStatus", request.getParameter("submitStatus"));
            System.out.println("invoiceDetailsList.size():" + invoiceDetailsList.size());
            request.setAttribute("invoiceDetailsListSize", invoiceDetailsList.size());
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewCreditNotePrint(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        int userId = 0;
        HttpSession session = null;
        billingCommand = command;
        String menuPath = "";
        BillingTO billingTO = new BillingTO();
        TripTO tripTO = new TripTO();
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  View Bill For Submission";
        request.setAttribute("pageTitle", pageTitle);
        String status = "1";
        request.setAttribute("status", status);

        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            session = request.getSession();
            int roleId = (Integer) session.getAttribute("RoleId");
            System.out.println("roleId=@@@@......" + roleId);
            request.setAttribute("roleId", roleId);

            //for cancell the invoice after generate the bill
            String cancel = "0";
            request.setAttribute("cancelStatus", cancel);
            String cancelStatus = request.getParameter("cancelStatus");
            String repoOrder = request.getParameter("repoOrder");
            request.setAttribute("repoOrder", repoOrder);
            //for cancell the invoice after generate the bill

            String invoiceId = request.getParameter("invoiceId");
            String creditNoteId = request.getParameter("creditNoteId");
            String[] tempRemarks = null;

            if ("1".equals(cancelStatus)) {
                path = "content/billing/viewCreditNotePrint.jsp";
                System.out.println("if!!!!----------");
            } else {
                String invoiceType = "3";
                String checkInvoiceType = tripBP.checkInvoiceType(creditNoteId, invoiceType);
                System.out.println("checkInvoiceType===$$$$=" + checkInvoiceType);
                tripTO.setInvoiceType(invoiceType);
                tripTO.setInvoiceId(creditNoteId);

                System.out.println("inside=11==$$$$=--" + checkInvoiceType);

                if ("Y".equals(checkInvoiceType)) {
                    ArrayList eInvoiceHeader = tripBP.getOrderEInvoiceHeader(tripTO);
                    System.out.println("eInvoiceHeader ilist111---" + eInvoiceHeader.size());
                    Iterator itr1 = eInvoiceHeader.iterator();
                    TripTO tpTO1 = new TripTO();
                    while (itr1.hasNext()) {
                        tpTO1 = (TripTO) itr1.next();
                        request.setAttribute("billingParty", tpTO1.getBillingParty());
                        System.out.println("billingParty:" + tpTO1.getBillingParty());
                        request.setAttribute("creditNoteNo", tpTO1.getCreditNoteNo());
                        request.setAttribute("creditNoteDate", tpTO1.getCreditNoteDate());
                        request.setAttribute("createdBy", tpTO1.getCreatedBy());
                        request.setAttribute("customerName", tpTO1.getCustomerName());
                        request.setAttribute("taxValue", tpTO1.getTaxValue());
                        request.setAttribute("customerAddress", tpTO1.getCustomerAddress());
                        request.setAttribute("panNo", tpTO1.getPanNo());
                        request.setAttribute("gstNo", tpTO1.getGstNo());
                        request.setAttribute("cityName", tpTO1.getCityName());
                        request.setAttribute("state", tpTO1.getState());
                        request.setAttribute("invoicecode", tpTO1.getInvoiceCode());
                        request.setAttribute("consignmentOrderNos", tpTO1.getcNotes());
                        request.setAttribute("consignmentOrderDate", tpTO1.getConsignmentOrderDate());
                        request.setAttribute("billDate", tpTO1.getBillDate());
                        request.setAttribute("movementType", tpTO1.getMovementType());
                        request.setAttribute("containerTypeName", tpTO1.getContainerTypeName());
                        request.setAttribute("billingType", tpTO1.getBillingType());
                        request.setAttribute("userName", tpTO1.getUserName());
                        request.setAttribute("billingState", tpTO1.getBillingState());
                        request.setAttribute("irn", tpTO1.getIrn());
                        request.setAttribute("signedQr", tpTO1.getSignedQr());
                        System.out.println("irn:" + tpTO1.getIrn());
                        System.out.println("signedQR:" + tpTO1.getSignedQr());
                        System.out.println("remaarks:" + tpTO1.getRemarks());
                        if (!"".equals(tpTO1.getRemarks())) {
                            tempRemarks = tpTO1.getRemarks().split(",");
                            if (tempRemarks.length >= 1) {
                                request.setAttribute("firstRemarks", tempRemarks[0]);
                                System.out.println("firstRemarks:" + tempRemarks[0]);
                            }
                            if (tempRemarks.length >= 2) {
                                request.setAttribute("secondRemarks", tempRemarks[1]);
                            }
                            if (tempRemarks.length >= 3) {
                                request.setAttribute("thirdRemarks", tempRemarks[2]);
                            }
                        }

                    }
                    System.out.println("SSSSSS");
                    path = "content/billing/viewOrderBillDetailsForECreditInvoiceSubmission.jsp";
                } else {
                    path = "content/billing/viewCreditNotePrint.jsp";

                    System.out.println("else!!!!!----------");
                }
            }

            billingTO.setInvoiceId(invoiceId);
            tripTO.setInvoiceId(invoiceId);
            tripTO.setCreditNoteId(creditNoteId);
            request.setAttribute("invoiceId", invoiceId);
            ArrayList invoiceHeader = tripBP.getOrderInvoiceHeader(tripTO);
            ArrayList invoiceDetails = tripBP.getInvoiceDetails(tripTO);
            ArrayList invoiceDetailExpenses = tripBP.getInvoiceDetailExpenses(tripTO);
            ArrayList invoiceTaxDetails = billingBP.getInvoiceGSTTaxDetails(billingTO);
            ArrayList invoiceCeditDetails = billingBP.getInvoiceCreditNoteDetails(creditNoteId);
            Iterator itrCredit = invoiceCeditDetails.iterator();
            BillingTO tpCRTO = new BillingTO();
            while (itrCredit.hasNext()) {
                tpCRTO = (BillingTO) itrCredit.next();
                request.setAttribute("creditNoteNo", tpCRTO.getInvoiceCode());
                request.setAttribute("creditAmount", tpCRTO.getCreditAmount());
                request.setAttribute("creditRemarks", tpCRTO.getRemarks());
                request.setAttribute("creditReason", tpCRTO.getReason());
                request.setAttribute("creditNoteDate", tpCRTO.getCreditNoteDate());
                request.setAttribute("invoiceGrandTotal", tpCRTO.getGrandTotal());
                request.setAttribute("userName", tpCRTO.getUserId());
            }

            request.setAttribute("invoiceHeader", invoiceHeader);
            request.setAttribute("invoiceTaxDetails", invoiceTaxDetails);
            request.setAttribute("invoiceTaxCheck", "1");
            System.out.println("invoiceTaxDetailsSize:" + invoiceTaxDetails.size());
            request.setAttribute("invoiceTaxDetailsSize", invoiceTaxDetails.size());

            Iterator itr = invoiceHeader.iterator();
            TripTO tpTO = new TripTO();
            while (itr.hasNext()) {
                tpTO = (TripTO) itr.next();
                request.setAttribute("billingParty", tpTO.getBillingParty());
                System.out.println("billingParty:" + tpTO.getBillingParty());
                request.setAttribute("customerName", tpTO.getCustomerName());
                request.setAttribute("taxValue", tpTO.getTaxValue());
                request.setAttribute("customerAddress", tpTO.getCustomerAddress());
                request.setAttribute("panNo", tpTO.getPanNo());
                request.setAttribute("gstNo", tpTO.getGstNo());
                request.setAttribute("cityName", tpTO.getCityName());
                request.setAttribute("state", tpTO.getState());
                request.setAttribute("invoicecode", tpTO.getInvoiceCode());
                request.setAttribute("consignmentOrderNos", tpTO.getcNotes());
                request.setAttribute("consignmentOrderDate", tpTO.getConsignmentOrderDate());
                request.setAttribute("billDate", tpTO.getBillDate());
                request.setAttribute("movementType", tpTO.getMovementType());
                request.setAttribute("containerTypeName", tpTO.getContainerTypeName());
                request.setAttribute("billingType", tpTO.getBillingType());
//                request.setAttribute("userName", tpTO.getUserName());
                System.out.println("remaarks:" + tpTO.getRemarks());
                System.out.println("tpTO.getPanNo():" + tpTO.getPanNo());
                System.out.println("gstNo:" + tpTO.getGstNo());
                if (!"".equals(tpTO.getRemarks())) {
                    tempRemarks = tpTO.getRemarks().split(",");
                    if (tempRemarks.length >= 1) {
                        request.setAttribute("firstRemarks", tempRemarks[0]);
                        System.out.println("firstRemarks:" + tempRemarks[0]);
                    }
                    if (tempRemarks.length >= 2) {
                        request.setAttribute("secondRemarks", tempRemarks[1]);
                    }
                    if (tempRemarks.length >= 3) {
                        request.setAttribute("thirdRemarks", tempRemarks[2]);
                    }
                }

            }
            request.setAttribute("invoiceDetails", invoiceDetails);
            if (invoiceDetailExpenses.size() > 0) {
                request.setAttribute("invoiceDetailExpenses", invoiceDetailExpenses);
            }
            ArrayList invoiceDetailsList = new ArrayList();
            invoiceDetailsList = tripBP.getCreditOrderInvoiceDetailsList(tripTO);
            request.setAttribute("invoiceDetailsList", invoiceDetailsList);
            ArrayList tripDetails = new ArrayList();
            tripDetails = billingBP.getTripDetails(billingTO);
            request.setAttribute("tripDetails", tripDetails);
            request.setAttribute("invoiceId", invoiceId);
            request.setAttribute("submitStatus", request.getParameter("submitStatus"));
            System.out.println("invoiceDetailsList.size():" + invoiceDetailsList.size());
            request.setAttribute("invoiceDetailsListSize", invoiceDetailsList.size());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView submitOrderBill(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Finance  >>  Generate Bill ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            BillingTO billingTO = new BillingTO();
            TripTO tripTO = new TripTO();

            String invoiceId = request.getParameter("invoiceId");
            billingTO.setInvoiceId(invoiceId);
            tripTO.setInvoiceId(invoiceId);
            request.setAttribute("invoiceId", invoiceId);
            int userId = (Integer) session.getAttribute("userId");
            int status = billingBP.submitBill(invoiceId, userId);

            ArrayList invoiceHeader = tripBP.getInvoiceHeader(tripTO);
            ArrayList invoiceDetails = tripBP.getInvoiceDetails(tripTO);
            ArrayList invoiceDetailExpenses = tripBP.getInvoiceDetailExpenses(tripTO);

            request.setAttribute("invoiceHeader", invoiceHeader);
            request.setAttribute("invoiceDetails", invoiceDetails);
            if (invoiceDetailExpenses.size() > 0) {
                request.setAttribute("invoiceDetailExpenses", invoiceDetailExpenses);
            }
            ArrayList tripDetails = new ArrayList();
            tripDetails = tripBP.getTripDetails(tripTO);
            request.setAttribute("tripDetails", tripDetails);
//            path = "content/billing/orderBillingInvoice.jsp";
//session.getAttribute("sessionPageParam")
            request.setAttribute("sessionPageParam", session.getAttribute("sessionPageParam"));
            path = "viewOrderBillDetailsForSubmit.do?invoiceId=" + invoiceId + "&submitStatus=1";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView printPreviewForBilling(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Finance  >>  Generate Bill ";
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            String status = "0";
            request.setAttribute("status", status);

            BillingTO billingTO = new BillingTO();

            String tripSheetIds = request.getParameter("tripSheetIds");
            String taxCheck = request.getParameter("taxCheck");
            String igstAmount = request.getParameter("igstAmount");
            String sgstAmount = request.getParameter("sgstAmount");
            String cgstAmount = request.getParameter("cgstAmount");
            String netAmount = request.getParameter("netAmount");
            String IGST = request.getParameter("IGST");
            String CGST = request.getParameter("CGST");
            String SGST = request.getParameter("SGST");
            String GSTNo = request.getParameter("GSTNo");
            String panNo = request.getParameter("panNo");
            String finYear = request.getParameter("finYear");

            request.setAttribute("panNo", panNo);
            request.setAttribute("GSTNo", GSTNo);
            request.setAttribute("taxCheck", taxCheck);
            request.setAttribute("igstAmount", igstAmount);
            request.setAttribute("sgstAmount", sgstAmount);
            request.setAttribute("cgstAmount", cgstAmount);
            request.setAttribute("netAmount", netAmount);
            request.setAttribute("IGST", IGST);
            request.setAttribute("CGST", CGST);
            request.setAttribute("SGST", SGST);
            billingTO.setTripSheetIds(tripSheetIds);
            billingTO.setTripSheetId(tripSheetIds);

            ArrayList invoiceHeader = new ArrayList();
            invoiceHeader = billingBP.getPreviewHeader(billingTO);
            request.setAttribute("invoiceHeader", invoiceHeader);
            System.out.println("getPreviewHeader.size():" + invoiceHeader.size());

            ArrayList tripsOtherExpenseDetails = billingBP.getOrdersOtherExpenseDetails(billingTO);
            request.setAttribute("tripsOtherExpenseDetails", tripsOtherExpenseDetails);

            String[] tempRemarks = null;
            Iterator itr = invoiceHeader.iterator();
            TripTO tpTO = new TripTO();
            while (itr.hasNext()) {
                tpTO = (TripTO) itr.next();
                request.setAttribute("billingParty", tpTO.getBillingParty());
                System.out.println("billingParty:" + tpTO.getBillingParty());
                request.setAttribute("customerName", tpTO.getCustomerName());
                request.setAttribute("customerAddress", tpTO.getCustomerAddress());
                request.setAttribute("cityName", tpTO.getCityName());
                request.setAttribute("state", tpTO.getState());
                request.setAttribute("invoicecode", tpTO.getInvoiceCode());
                request.setAttribute("consignmentOrderNos", tpTO.getcNotes());
                request.setAttribute("consignmentOrderDate", tpTO.getConsignmentOrderDate());
                request.setAttribute("gstType", tpTO.getGstType());

                if (finYear.equalsIgnoreCase("2023")) {
                    request.setAttribute("billDate", tpTO.getBillDate());
                } else {
                    request.setAttribute("billDate", "31-03-2022");
                }

                request.setAttribute("movementType", tpTO.getMovementType());
                request.setAttribute("containerTypeName", tpTO.getContainerTypeName());
                request.setAttribute("billingType", tpTO.getBillingType());
                request.setAttribute("userName", tpTO.getUserName());
                request.setAttribute("billingState", tpTO.getBillingState());
                System.out.println("remaarks:" + tpTO.getRemarks());
                if (!"".equals(tpTO.getRemarks())) {
                    tempRemarks = tpTO.getRemarks().split(",");
                    if (tempRemarks.length >= 1) {
                        request.setAttribute("firstRemarks", tempRemarks[0]);
                        System.out.println("firstRemarks:" + tempRemarks[0]);
                    }
                    if (tempRemarks.length >= 2) {
                        request.setAttribute("secondRemarks", tempRemarks[1]);
                    }
                    if (tempRemarks.length >= 3) {
                        request.setAttribute("thirdRemarks", tempRemarks[2]);
                    }
                }

            }

            ArrayList invoiceDetailsList = new ArrayList();
            invoiceDetailsList = billingBP.getBillingDetailsForPreview(billingTO);
            request.setAttribute("invoiceDetailsList", invoiceDetailsList);
            request.setAttribute("invoiceTaxCheck", "2");
            request.setAttribute("invoiceDetailsListSize", invoiceDetailsList.size());
            System.out.println("getBillingDetailsForPreview.size():" + invoiceDetailsList.size());

//          path = "content/trip/responsePage.jsp";
            path = "content/billing/viewOrderBillDetailsForSubmissionPreview.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            return new ModelAndView("content/common/error.jsp");
//
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView printPreviewForSupplementBilling(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Finance  >>  Generate Bill ";
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            String status = "0";
            request.setAttribute("status", status);

            BillingTO billingTO = new BillingTO();

            String tripSheetIds = request.getParameter("tripSheetIds");
            String taxCheck = request.getParameter("taxCheck");
            String igstAmount = request.getParameter("igstAmount");
            String sgstAmount = request.getParameter("sgstAmount");
            String cgstAmount = request.getParameter("cgstAmount");
            String netAmount = request.getParameter("netAmount");
            String IGST = request.getParameter("IGST");
            String CGST = request.getParameter("CGST");
            String SGST = request.getParameter("SGST");
            String GSTNo = request.getParameter("GSTNo");
            String panNo = request.getParameter("panNo");
            String finYear = request.getParameter("finYear");
            String billingPartyId = request.getParameter("billingPartyId");

            request.setAttribute("panNo", panNo);
            request.setAttribute("GSTNo", GSTNo);
            request.setAttribute("taxCheck", taxCheck);
            request.setAttribute("igstAmount", igstAmount);
            request.setAttribute("sgstAmount", sgstAmount);
            request.setAttribute("cgstAmount", cgstAmount);
            request.setAttribute("netAmount", netAmount);
            request.setAttribute("IGST", IGST);
            request.setAttribute("CGST", CGST);
            request.setAttribute("SGST", SGST);
            billingTO.setTripSheetIds(tripSheetIds);

            ArrayList invoiceHeader = new ArrayList();
            invoiceHeader = billingBP.getPreviewSupplementHeader(billingTO);
            request.setAttribute("invoiceHeader", invoiceHeader);
            System.out.println("getPreviewHeader.size():" + invoiceHeader.size());
            String[] tempRemarks = null;
            Iterator itr = invoiceHeader.iterator();
            TripTO tpTO = new TripTO();
            while (itr.hasNext()) {
                tpTO = (TripTO) itr.next();
                request.setAttribute("billingParty", tpTO.getBillingParty());
                System.out.println("billingParty:==" + tpTO.getBillingParty());
                request.setAttribute("customerName", tpTO.getCustomerName());
                request.setAttribute("customerAddress", tpTO.getCustomerAddress());
                request.setAttribute("cityName", tpTO.getCityName());
                request.setAttribute("state", tpTO.getState());
                request.setAttribute("invoicecode", tpTO.getInvoiceCode());
                request.setAttribute("consignmentOrderNos", tpTO.getcNotes());
                request.setAttribute("consignmentOrderDate", tpTO.getConsignmentOrderDate());
                request.setAttribute("gstType", tpTO.getGstType());

                if (finYear.equalsIgnoreCase("2023")) {
                    request.setAttribute("billDate", tpTO.getBillDate());
                } else {
                    request.setAttribute("billDate", "31-03-2022");
                }

                request.setAttribute("movementType", tpTO.getMovementType());
                request.setAttribute("containerTypeName", tpTO.getContainerTypeName());
                request.setAttribute("billingType", tpTO.getBillingType());
                request.setAttribute("userName", tpTO.getUserName());
                System.out.println("remaarks:" + tpTO.getRemarks());
                if (!"".equals(tpTO.getRemarks())) {
                    tempRemarks = tpTO.getRemarks().split(",");
                    if (tempRemarks.length >= 1) {
                        request.setAttribute("firstRemarks", tempRemarks[0]);
                        System.out.println("firstRemarks:" + tempRemarks[0]);
                    }
                    if (tempRemarks.length >= 2) {
                        request.setAttribute("secondRemarks", tempRemarks[1]);
                    }
                    if (tempRemarks.length >= 3) {
                        request.setAttribute("thirdRemarks", tempRemarks[2]);
                    }
                }

            }

            String priviewPrintBilling = billingBP.getPriviewPrintBilling(billingPartyId);
            //System.out.println("priviewPrintBilling==="+priviewPrintBilling);

            if (priviewPrintBilling != null && !priviewPrintBilling.equals("")) {
                String[] temp = priviewPrintBilling.split("~");
                String billingPartyPreview = temp[0];
                request.setAttribute("billingPartyPreview", billingPartyPreview);
                String customerAddressPreview = temp[1];
                request.setAttribute("customerAddressPreview", customerAddressPreview);
                String panNoPreview = temp[2];
                request.setAttribute("panNoPreview", panNoPreview);
                String GSTNoPreview = temp[3];
                request.setAttribute("GSTNoPreview", GSTNoPreview);
            } else {

                request.setAttribute("billingPartyPreview", "");
                request.setAttribute("customerAddressPreview", "");
                request.setAttribute("panNoPreview", "");
                request.setAttribute("GSTNoPreview", "");

            }

            ArrayList invoiceDetailsList = new ArrayList();
            invoiceDetailsList = billingBP.getSupplementBillingDetailsForPreview(billingTO);
            request.setAttribute("invoiceDetailsList", invoiceDetailsList);
            request.setAttribute("invoiceTaxCheck", "2");
            request.setAttribute("invoiceDetailsListSize", invoiceDetailsList.size());
            System.out.println("getBillingDetailsForPreview.size():" + invoiceDetailsList.size());

//          path = "content/trip/responsePage.jsp";
            path = "content/billing/viewOrderSupplementBillDetailsForSubmissionPreview.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            return new ModelAndView("content/common/error.jsp");
//
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleCustomerPayment(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Finance  >>  Customer Payment";
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            String status = "0";
            String custType = request.getParameter("custType");
            request.setAttribute("status", status);

            BillingTO billingTO = new BillingTO();
            String[] customerIds = null;
            String customerId = null;
            if (request.getParameter("customerId") != null) {
                customerIds = request.getParameter("customerId").split("~");
                customerId = customerIds[0];
            }
            System.out.println("customerId : " + customerId);
            billingTO.setCustomerId(customerId);

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getCreditCustomerList();
            request.setAttribute("customerList", customerList);
            System.out.println("customerList---" + customerList.size());
            request.setAttribute("custType", custType);

            ArrayList paymentHistory = new ArrayList();
            if (customerId != null) {
                paymentHistory = billingBP.getCustomerPaymentHistory(billingTO);
            }

            request.setAttribute("paymentHistory", paymentHistory);
            request.setAttribute("customerId", request.getParameter("customerId"));

            ArrayList bankList = new ArrayList();
            bankList = operationBP.getBankList();
            request.setAttribute("bankList", bankList);

            if ("1".equals(custType)) {
                path = "content/billing/customerPayment.jsp";
            } else {
                path = "content/billing/customerPDAPayment.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            return new ModelAndView("content/common/error.jsp");
            //
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveCustomerPayment(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Finance  >>  Generate Bill ";
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);
            BillingTO billingTO = new BillingTO();
            String custType = request.getParameter("custType");
            String modes = request.getParameter("mode");
            String modeNos = request.getParameter("modeNo");
            String bankNames = request.getParameter("bankName");
            String payDate = request.getParameter("payDate");
            String paidAmounts = request.getParameter("paidAmount");
            String customerIds[] = request.getParameter("customerId").split("~");
            String customerId = customerIds[0];
            System.out.println("custType----" + custType);

            int insertStatus = billingBP.saveCustomerPayment(customerId, modes, modeNos, bankNames, payDate, paidAmounts, custType, userId);
            if (insertStatus > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Invoice payment added Successfully....");
            }

            String custIds = request.getParameter("customerId");
            ArrayList customerList = new ArrayList();
            customerList = operationBP.getCreditCustomerList();
            request.setAttribute("custType", custType);
            request.setAttribute("customerList", customerList);
            request.setAttribute("customerId", request.getParameter("customerId"));
            System.out.println("path:" + path);

            billingTO.setCustomerId(customerId);

            ArrayList paymentHistory = new ArrayList();
            if (customerId != null) {
                paymentHistory = billingBP.getCustomerPaymentHistory(billingTO);
            }
            request.setAttribute("paymentHistory", paymentHistory);

            if ("1".equals(custType)) {
                path = "content/billing/customerPayment.jsp";
            } else {
                path = "content/billing/customerPDAPayment.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveInvoicePayment(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Finance  >>  Generate Bill ";
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            //generate tripcode
            String[] invoiceIds = request.getParameterValues("invoiceId");
            String[] InvoiceCodes = request.getParameterValues("invoiceCode");
            String[] invoiceDates = request.getParameterValues("invoiceDate");
            String[] invoiceAmounts = request.getParameterValues("invoiceAmount");
            String[] modes = request.getParameterValues("mode");
            String[] modeNos = request.getParameterValues("modeNo");
            String[] bankNames = request.getParameterValues("bankName");
            String[] paidAmounts = request.getParameterValues("paidAmount");

            int insertStatus = billingBP.saveInvoicePayment(invoiceIds, InvoiceCodes, invoiceDates, invoiceAmounts, modes, modeNos, bankNames, paidAmounts, userId);
//            mv = viewOrderBillDetailsForSubmit(request,response,command);
            System.out.println("path:" + path);

            path = "content/billing/invoicePayment.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleCustomerPaymentHistory(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Customer Payment ";
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "Customer Payment";
            request.setAttribute("pageTitle", pageTitle);

            String status = "0";
            request.setAttribute("status", status);
            BillingTO billingTO = new BillingTO();

            String customerId = request.getParameter("customerId");
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            String param = request.getParameter("param");
            request.setAttribute("customerId", customerId);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            System.out.println("customerId : " + customerId);
            System.out.println("param---" + param);

            if (customerId != null) {
                billingTO.setCustomerId(customerId);
            }
            if (fromDate != null) {
                billingTO.setFromDate(fromDate);
            }
            if (toDate != null) {
                billingTO.setToDate(toDate);
            }

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getCreditCustomerList();
            request.setAttribute("customerList", customerList);

            ArrayList customerStatementHistory = new ArrayList();

            if (customerId != null) {
                customerStatementHistory = billingBP.getCustomerStatementHistory(billingTO);
                System.out.println("customerStatementHistory : " + customerStatementHistory.size());
                request.setAttribute("customerStatementHistory", customerStatementHistory);
            }

            if ("Excel".equals(param)) {
                if (customerId != null) {
                    customerStatementHistory = billingBP.getCustomerStatementHistory(billingTO);
                    request.setAttribute("customerStatementHistory", customerStatementHistory);
                    System.out.println("customerStatementHistory-----" + customerStatementHistory.size());
                }
                path = "content/billing/customerPaymentExcel.jsp";
            } else {
                path = "content/billing/customerPaymentHistory.jsp";
            }

            Iterator itr = customerStatementHistory.iterator();

            while (itr.hasNext()) {
                billingTO = (BillingTO) itr.next();
                request.setAttribute("custName", billingTO.getCustomerName());
                request.setAttribute("custCode", billingTO.getCustCode());
                System.out.println("custName " + billingTO.getCustomerName());
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            return new ModelAndView("content/common/error.jsp");
            //
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleCustomerPaymentPrint(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Finance  >>  Generate Bill ";
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            String status = "0";
            request.setAttribute("status", status);

            BillingTO billingTO = new BillingTO();

            String customerName = request.getParameter("customerName");
            String paymentDate = request.getParameter("paymentDate");
            String paymentMode = request.getParameter("paymentMode");
            String voucherNo = request.getParameter("voucherNo");
            String bankName = request.getParameter("bankName");
            String amount = request.getParameter("amount");
            String receiptCode = request.getParameter("receiptCode");
//            String fromDate = request.getParameter("fromDate");
//            String toDate = request.getParameter("toDate");
//            
//            System.out.println("customerId:"+customerId+"fromDate:"+fromDate+"toDate:"+toDate);
            request.setAttribute("receiptCode", receiptCode);
            request.setAttribute("customerName", customerName);
            request.setAttribute("paymentDate", paymentDate);
            request.setAttribute("paymentMode", paymentMode);
            request.setAttribute("voucherNo", voucherNo);
            request.setAttribute("bankName", bankName);
            request.setAttribute("amount", amount);

            path = "content/billing/customerPaymentPrint.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            return new ModelAndView("content/common/error.jsp");
//
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView tempCustomerCreditLimit(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Finance  >>  Generate Bill ";
        ModelAndView mv = null;
        try {
            ArrayList tempCreditDetails = new ArrayList();
            BillingTO billingTO = new BillingTO();
            int userId = (Integer) session.getAttribute("userId");
            String customerId = request.getParameter("customerId");
            request.setAttribute("customerId", customerId);
            billingTO.setCustomerId(customerId);
            String custType = "1";
            request.setAttribute("custType", custType);

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getCreditCustomerList();
            request.setAttribute("customerList", customerList);
            request.setAttribute("customerId", request.getParameter("customerId"));

            if (customerId != null || "".equals(customerId)) {
                tempCreditDetails = billingBP.getTempCreditDetails(billingTO);
                request.setAttribute("tempCreditDetails", tempCreditDetails);
                System.out.println("tempCreditDetails---" + tempCreditDetails.size());
            }

            path = "content/billing/custTempCreditLimit.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            return new ModelAndView("content/common/error.jsp");
            //
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveTempCustomerCreditLimit(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Finance  >>  Generate Bill ";
        ModelAndView mv = null;
        try {
            ArrayList tempCreditDetails = new ArrayList();
            BillingTO billingTO = new BillingTO();
            int userId = (Integer) session.getAttribute("userId");

            String customerId = request.getParameter("customerId");
            String tempCreditLimit = request.getParameter("tempCreditLimit");
            String validFrom = request.getParameter("validFrom");
            String validTo = request.getParameter("validTo");

            request.setAttribute("customerId", customerId);
            request.setAttribute("tempCreditLimit", tempCreditLimit);
            request.setAttribute("validfrom", validFrom);
            request.setAttribute("validTo", validTo);

            billingTO.setCustomerId(customerId);
            billingTO.setFromDate(validFrom);
            billingTO.setToDate(validTo);
            billingTO.setCreditAmount(tempCreditLimit);

            System.out.println("validfrom : " + validFrom);
            System.out.println("tempCreditLimit : " + tempCreditLimit);
            System.out.println("validto : " + validTo);

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getCreditCustomerList();
            request.setAttribute("customerList", customerList);
            request.setAttribute("customerId", request.getParameter("customerId"));

            int status = 0;
            status = billingBP.saveTempCreditLimit(billingTO, userId);
            System.out.println("insert status " + status);

            if (customerId != null || !"".equals(customerId)) {
                tempCreditDetails = billingBP.getTempCreditDetails(billingTO);
                request.setAttribute("tempCreditDetails", tempCreditDetails);
                System.out.println("tempCreditDetails---" + tempCreditDetails.size());
            }

            path = "content/billing/custTempCreditLimit.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            return new ModelAndView("content/common/error.jsp");
            //
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewSuppCreditNotePrint(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        int userId = 0;
        HttpSession session = null;
        billingCommand = command;
        String menuPath = "";
        BillingTO billingTO = new BillingTO();
        TripTO tripTO = new TripTO();
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  View Bill For Submission";
        request.setAttribute("pageTitle", pageTitle);
        String status = "1";
        request.setAttribute("status", status);

        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            session = request.getSession();
            int roleId = (Integer) session.getAttribute("RoleId");
            System.out.println("roleId=@@@@......" + roleId);
            request.setAttribute("roleId", roleId);

            //for cancell the invoice after generate the bill
            String cancel = "0";
            request.setAttribute("cancelStatus", cancel);
            String cancelStatus = request.getParameter("cancelStatus");
            String repoOrder = request.getParameter("repoOrder");
            request.setAttribute("repoOrder", repoOrder);
            //for cancell the invoice after generate the bill

            String invoiceId = request.getParameter("invoiceId");
            String creditNoteId = request.getParameter("creditNoteId");
            String[] tempRemarks = null;

            if ("1".equals(cancelStatus)) {
                path = "content/billing/viewSuppCreditNotePrint.jsp";
                System.out.println("if!!!!----------");
            } else {
                String invoiceType = "4";
                String checkInvoiceType = tripBP.checkInvoiceType(creditNoteId, invoiceType);
                System.out.println("checkInvoiceType===$$$$=" + checkInvoiceType);
                tripTO.setInvoiceType(invoiceType);
                tripTO.setInvoiceId(creditNoteId);

                System.out.println("inside=11==$$$$=--" + checkInvoiceType);

                if ("Y".equals(checkInvoiceType)) {
                    ArrayList eInvoiceHeader = tripBP.getOrderEInvoiceHeader(tripTO);
                    System.out.println("eInvoiceHeader ilist111---" + eInvoiceHeader.size());
                    Iterator itr = eInvoiceHeader.iterator();
                    TripTO tpTO = new TripTO();
                    while (itr.hasNext()) {
                        tpTO = (TripTO) itr.next();
                        request.setAttribute("billingParty", tpTO.getBillingParty());
                        System.out.println("billingParty:" + tpTO.getBillingParty());
                        request.setAttribute("creditNoteNo", tpTO.getCreditNoteNo());
                        request.setAttribute("creditNoteDate", tpTO.getCreditNoteDate());
                        request.setAttribute("createdBy", tpTO.getCreatedBy());
                        request.setAttribute("customerName", tpTO.getCustomerName());
                        request.setAttribute("taxValue", tpTO.getTaxValue());
                        request.setAttribute("customerAddress", tpTO.getCustomerAddress());
                        request.setAttribute("panNo", tpTO.getPanNo());
                        request.setAttribute("gstNo", tpTO.getGstNo());
                        request.setAttribute("cityName", tpTO.getCityName());
                        request.setAttribute("state", tpTO.getState());
                        request.setAttribute("invoicecode", tpTO.getInvoiceCode());
                        request.setAttribute("consignmentOrderNos", tpTO.getcNotes());
                        request.setAttribute("consignmentOrderDate", tpTO.getConsignmentOrderDate());
                        request.setAttribute("billDate", tpTO.getBillDate());
                        request.setAttribute("movementType", tpTO.getMovementType());
                        request.setAttribute("containerTypeName", tpTO.getContainerTypeName());
                        request.setAttribute("billingType", tpTO.getBillingType());
                        request.setAttribute("userName", tpTO.getUserName());
                        request.setAttribute("billingState", tpTO.getBillingState());
                        request.setAttribute("irn", tpTO.getIrn());
                        request.setAttribute("signedQr", tpTO.getSignedQr());
                        System.out.println("irn:" + tpTO.getIrn());
                        System.out.println("signedQR:" + tpTO.getSignedQr());
                        System.out.println("remaarks:" + tpTO.getRemarks());
                        if (!"".equals(tpTO.getRemarks())) {
                            tempRemarks = tpTO.getRemarks().split(",");
                            if (tempRemarks.length >= 1) {
                                request.setAttribute("firstRemarks", tempRemarks[0]);
                                System.out.println("firstRemarks:" + tempRemarks[0]);
                            }
                            if (tempRemarks.length >= 2) {
                                request.setAttribute("secondRemarks", tempRemarks[1]);
                            }
                            if (tempRemarks.length >= 3) {
                                request.setAttribute("thirdRemarks", tempRemarks[2]);
                            }
                        }

                    }
                    System.out.println("SSSSSS");
                    path = "content/billing/viewOrderBillDetailsForECreditInvoiceSubmission.jsp";
                } else {
                    path = "content/billing/viewSuppCreditNotePrint.jsp";

                    System.out.println("else!!!!!----------");
                }
            }
            billingTO.setInvoiceId(invoiceId);
            tripTO.setInvoiceId(invoiceId);
            tripTO.setCreditNoteId(creditNoteId);
            request.setAttribute("invoiceId", invoiceId);
            ArrayList invoiceHeader = tripBP.getOrderSuppInvoiceHeader(tripTO);
            ArrayList invoiceDetails = tripBP.getSuppInvoiceDetails(tripTO);
            ArrayList invoiceDetailExpenses = tripBP.getSuppInvoiceDetailExpenses(tripTO);
            ArrayList invoiceTaxDetails = billingBP.getSuppInvoiceGSTTaxDetails(billingTO);
            ArrayList invoiceCeditDetails = billingBP.getInvoiceSuppCreditNoteDetails(creditNoteId);
            Iterator itrCredit = invoiceCeditDetails.iterator();
            BillingTO tpCRTO = new BillingTO();
            while (itrCredit.hasNext()) {
                tpCRTO = (BillingTO) itrCredit.next();
                request.setAttribute("creditNoteNo", tpCRTO.getInvoiceCode());
                request.setAttribute("creditAmount", tpCRTO.getCreditAmount());
                request.setAttribute("creditRemarks", tpCRTO.getRemarks());
                request.setAttribute("creditReason", tpCRTO.getReason());
                request.setAttribute("creditNoteDate", tpCRTO.getCreditNoteDate());
                request.setAttribute("invoiceGrandTotal", tpCRTO.getGrandTotal());
                request.setAttribute("userName", tpCRTO.getUserId());
            }

            request.setAttribute("invoiceHeader", invoiceHeader);
            request.setAttribute("invoiceTaxDetails", invoiceTaxDetails);
            request.setAttribute("invoiceTaxCheck", "1");
            System.out.println("invoiceTaxDetailsSize:" + invoiceTaxDetails.size());
            request.setAttribute("invoiceTaxDetailsSize", invoiceTaxDetails.size());

            Iterator itr = invoiceHeader.iterator();
            TripTO tpTO = new TripTO();
            while (itr.hasNext()) {
                tpTO = (TripTO) itr.next();
                request.setAttribute("billingParty", tpTO.getBillingParty());
                System.out.println("billingParty:" + tpTO.getBillingParty());
                request.setAttribute("customerName", tpTO.getCustomerName());
                request.setAttribute("taxValue", tpTO.getTaxValue());
                request.setAttribute("customerAddress", tpTO.getCustomerAddress());
                request.setAttribute("panNo", tpTO.getPanNo());
                request.setAttribute("gstNo", tpTO.getGstNo());
                request.setAttribute("cityName", tpTO.getCityName());
                request.setAttribute("state", tpTO.getState());
                request.setAttribute("invoicecode", tpTO.getInvoiceCode());
                request.setAttribute("consignmentOrderNos", tpTO.getcNotes());
                request.setAttribute("consignmentOrderDate", tpTO.getConsignmentOrderDate());
                request.setAttribute("billDate", tpTO.getBillDate());
                request.setAttribute("movementType", tpTO.getMovementType());
                request.setAttribute("containerTypeName", tpTO.getContainerTypeName());
                request.setAttribute("billingType", tpTO.getBillingType());
                //                request.setAttribute("userName", tpTO.getUserName());
                System.out.println("remaarks:" + tpTO.getRemarks());
                System.out.println("tpTO.getPanNo():" + tpTO.getPanNo());
                System.out.println("gstNo:" + tpTO.getGstNo());
                if (!"".equals(tpTO.getRemarks())) {
                    tempRemarks = tpTO.getRemarks().split(",");
                    if (tempRemarks.length >= 1) {
                        request.setAttribute("firstRemarks", tempRemarks[0]);
                        System.out.println("firstRemarks:" + tempRemarks[0]);
                    }
                    if (tempRemarks.length >= 2) {
                        request.setAttribute("secondRemarks", tempRemarks[1]);
                    }
                    if (tempRemarks.length >= 3) {
                        request.setAttribute("thirdRemarks", tempRemarks[2]);
                    }
                }

            }
            request.setAttribute("invoiceDetails", invoiceDetails);
            if (invoiceDetailExpenses.size() > 0) {
                request.setAttribute("invoiceDetailExpenses", invoiceDetailExpenses);
            }
            ArrayList invoiceDetailsList = new ArrayList();
            invoiceDetailsList = tripBP.getSuppCreditOrderInvoiceDetailsList(tripTO);
            request.setAttribute("invoiceDetailsList", invoiceDetailsList);
            ArrayList tripDetails = new ArrayList();
            tripDetails = billingBP.getTripDetails(billingTO);
            request.setAttribute("tripDetails", tripDetails);
            request.setAttribute("invoiceId", invoiceId);
            request.setAttribute("submitStatus", request.getParameter("submitStatus"));
            System.out.println("invoiceDetailsList.size():" + invoiceDetailsList.size());
            request.setAttribute("invoiceDetailsListSize", invoiceDetailsList.size());
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handlePDAadjustment(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Finance  >>  Generate Bill ";
        ModelAndView mv = null;
        try {

            BillingTO billingTO = new BillingTO();
            String status = "0";
            String custType = request.getParameter("custType");
            request.setAttribute("custType", custType);
            request.setAttribute("status", status);

            String customerId = request.getParameter("customerId");
            System.out.println("customerId : " + customerId);
            request.setAttribute("customerId", customerId);
            billingTO.setCustomerId(customerId);

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getCreditCustomerList();
            request.setAttribute("customerList", customerList);
            System.out.println("customerList---" + customerList.size());

            ArrayList adjustmentHistory = new ArrayList();

            if (customerId != null) {
                billingTO.setCustomerId(customerId);
                adjustmentHistory = billingBP.getCustomerAdjustmentHistory(billingTO);
            }

            request.setAttribute("adjustmentHistory", adjustmentHistory);
            path = "content/billing/PDAadjustment.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            return new ModelAndView("content/common/error.jsp");
            //
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveAdjustPayment(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        BillingTO billingTO = new BillingTO();
        String menuPath = "Finance  >>  Generate Bill ";
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            String custType = request.getParameter("custType");
            String balAmt = request.getParameter("balAmt");
            String adjustAmount = request.getParameter("adjustAmount");
            String transactionType = request.getParameter("mode");
            String availAmount = request.getParameter("availAmount");
            String adjustRemark = request.getParameter("adjustRemark");
            String customerIds[] = request.getParameter("customerId").split("~");
            String customerId = customerIds[0];
            System.out.println("custType----" + custType);

            int insertStatus = billingBP.saveAdjustPayment(customerId, transactionType, balAmt, availAmount, adjustRemark, adjustAmount, custType, userId);
            if (insertStatus > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Adjust Amount Saved Successfully....");
            }

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getCreditCustomerList();
            request.setAttribute("custType", custType);
            request.setAttribute("customerList", customerList);
            request.setAttribute("customerId", request.getParameter("customerId"));
            System.out.println("path:" + path);

            ArrayList adjustmentHistory = new ArrayList();
            if (customerId != null) {
                billingTO.setCustomerId(customerId);
                adjustmentHistory = billingBP.getCustomerAdjustmentHistory(billingTO);
            }
            request.setAttribute("adjustmentHistory", adjustmentHistory);

            path = "content/billing/PDAadjustment.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView invoiceApproval(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Finance  >>  Invoice Approval ";

        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);
            BillingTO billingTO = new BillingTO();
            String[] invoiceIds = request.getParameterValues("selectedIndex");
            billingTO.setInvoiceIds(invoiceIds);
            int status = billingBP.approveInvoice(billingTO);
            System.out.println("status =" + status);

            path = "content/billing/viewOrderBillForSubmission.jsp";

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Invoice Rejected....");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleCreditAdjustment(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Finance  >>  Generate Bill ";
        ModelAndView mv = null;
        try {

            BillingTO billingTO = new BillingTO();
            String status = "0";
            String custType = request.getParameter("custType");
            request.setAttribute("custType", custType);
            request.setAttribute("status", status);

            String customerId = request.getParameter("customerId");
            System.out.println("customerId : " + customerId);
            request.setAttribute("customerId", customerId);
            billingTO.setCustomerId(customerId);

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getCreditCustomerList();
            request.setAttribute("customerList", customerList);
            System.out.println("customerList---" + customerList.size());

            ArrayList adjustmentHistory = new ArrayList();

            if (customerId != null) {
                billingTO.setCustomerId(customerId);
                adjustmentHistory = billingBP.getCreditAdjustmentHistory(billingTO);
            }

            request.setAttribute("adjustmentHistory", adjustmentHistory);
            path = "content/billing/CreditAdjustment.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            return new ModelAndView("content/common/error.jsp");
            //
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveCreditAdjust(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        BillingTO billingTO = new BillingTO();
        String menuPath = "Finance  >>  Generate Bill ";
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            String custType = request.getParameter("custType");
            String balAmt = request.getParameter("balAmt");
            String adjustAmount = request.getParameter("adjustAmount");
            String transactionType = request.getParameter("mode");
            String availAmount = request.getParameter("availAmount");
            String adjustRemark = request.getParameter("adjustRemark");
            String customerIds[] = request.getParameter("customerId").split("~");
            String customerId = customerIds[0];
            System.out.println("custType----" + custType);

            int insertStatus = billingBP.saveCreditAdjustPayment(customerId, transactionType, balAmt, availAmount, adjustRemark, adjustAmount, custType, userId);
            if (insertStatus > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Adjust Amount Saved Successfully....");
            }

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getCreditCustomerList();
            request.setAttribute("custType", custType);
            request.setAttribute("customerList", customerList);
            request.setAttribute("customerId", request.getParameter("customerId"));
            System.out.println("path:" + path);

            ArrayList adjustmentHistory = new ArrayList();
            if (customerId != null) {
                billingTO.setCustomerId(customerId);
                adjustmentHistory = billingBP.getCreditAdjustmentHistory(billingTO);
            }
            request.setAttribute("adjustmentHistory", adjustmentHistory);

            path = "content/billing/CreditAdjustment.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleEinvoiceList(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        int userId = 0;
        HttpSession session = null;
        billingCommand = command;
        String menuPath = "";
        BillingTO billingTO = null;
        String pageTitle = "View Invoice List";
        menuPath = "Operation >>  View Invoice List";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String fromDate = "";
        String toDate = "";
        String customerId = "";
        String tripType = "";
        String submitStatus = "";
        String param = "";
        String billNo = "";
        String grNo = "";
        String containerNo = "";
        try {
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            param = request.getParameter("param");
            customerId = request.getParameter("customerId");
            fromDate = request.getParameter("fromDate");
            toDate = request.getParameter("toDate");

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            System.out.println("fromDtae---" + fromDate);
            System.out.println("toDate---" + toDate);
            if ("".equals(fromDate) || fromDate == null) {
                fromDate = startDate;
            }
            if ("".equals(toDate) || toDate == null) {
                toDate = endDate;
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            submitStatus = request.getParameter("submitStatus");
            tripType = request.getParameter("tripType");
            billNo = request.getParameter("billNo");
            grNo = request.getParameter("grNo");
            containerNo = request.getParameter("containerNo");

            request.setAttribute("customerId", customerId);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("tripType", tripType);
            request.setAttribute("submitStatus", submitStatus);
            request.setAttribute("billNo", billNo);
            request.setAttribute("grNo", grNo);
            request.setAttribute("containerNo", containerNo);

            int status = 0;
            String cancelStatus = request.getParameter("cancelStatus");
            String billingNo = request.getParameter("billingNo");

            if (cancelStatus != null) {
                status = billingBP.updateCancel(billingNo, userId);
                System.out.println("status" + status);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, billingNo + " billingNo  cancelled Successfully....");
            }

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getTripCustomerList();
            request.setAttribute("customerList", customerList);
            System.out.println("customerList Size Is :::" + customerList.size());

            ArrayList closedBillList = new ArrayList();
            //closedBillList = operationBP.getClosedBilledList(customerId, fromDate, toDate);
            closedBillList = billingBP.viewInvoiceList(customerId, fromDate, toDate, tripType, submitStatus, billNo, grNo, containerNo);
            request.setAttribute("closedBillList", closedBillList);
            System.out.println("closedBillList Size Is :::" + closedBillList.size());

            if (!"ExcelExport".equals(param)) {
                path = "content/billing/viewInvoiceList.jsp";
            } else {
                path = "content/billing/viewInvoiceListExport.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView eInvoiceGenerate(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Finance  >>  Invoice Approval ";

        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);
            BillingTO billingTO = new BillingTO();
            String[] invoiceIds = request.getParameterValues("selectedIndex");
            String custId = request.getParameter("custId");
            billingTO.setInvoiceIds(invoiceIds);
            billingTO.setCustomerId(custId);
            int status = billingBP.eInvoiceGenerate(billingTO, userId);
            System.out.println("status =" + status);

            path = "content/billing/viewInvoiceList.jsp";

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "E-Invoice Generated Successfully....");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

//    download upload
    public ModelAndView handleEInvoiceListForDownloadUpload(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        int userId = 0;
        HttpSession session = null;
        billingCommand = command;
        String menuPath = "";
        BillingTO billingTO = null;
        String pageTitle = "View Invoice List";
        menuPath = "Operation >>  View Invoice List";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String fromDate = "";
        String toDate = "";
        String customerId = "";
        String custId = "";
        String tripType = "";
        String submitStatus = "";
        String param = "";
        String billNo = "";
        String grNo = "";
        String containerNo = "";
        String gstNo = "";
        String invoiceId = "";
        try {
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            param = request.getParameter("param");
            customerId = request.getParameter("customerId");
            invoiceId = request.getParameter("invoiceId");
            gstNo = request.getParameter("gstNo");
            custId = request.getParameter("custId");
            fromDate = request.getParameter("fromDate");
            toDate = request.getParameter("toDate");
            submitStatus = request.getParameter("submitStatus");
            tripType = request.getParameter("tripType");
            billNo = request.getParameter("billNo");
            System.out.println("BILLNO------" + billNo);
            grNo = request.getParameter("grNo");
            containerNo = request.getParameter("containerNo");

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            System.out.println("fromDtae---" + fromDate);
            System.out.println("toDate---" + toDate);
            if ("".equals(fromDate) || fromDate == null) {
                fromDate = startDate;
            }
            if ("".equals(toDate) || toDate == null) {
                toDate = endDate;
            }

            request.setAttribute("customerId", customerId);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("tripType", tripType);
            request.setAttribute("submitStatus", submitStatus);
            request.setAttribute("billNo", billNo);
            request.setAttribute("grNo", grNo);
            request.setAttribute("containerNo", containerNo);
            request.setAttribute("gstNo", gstNo);
            request.setAttribute("supplierGstNo", "06AACCB8054G1ZT");

            int status = 0;
            String cancelStatus = request.getParameter("cancelStatus");
            String billingNo = request.getParameter("billingNo");

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getTripCustomerList();
            request.setAttribute("customerList", customerList);
            System.out.println("customerList Size Is :::" + customerList.size());

            //closedBillList = operationBP.getClosedBilledList(customerId, fromDate, toDate);
            ArrayList closedBillList = new ArrayList();
            closedBillList = billingBP.viewEInvoiceList(customerId, fromDate, toDate, tripType, submitStatus, billNo, grNo, containerNo);
            request.setAttribute("closedBillList", closedBillList);
            System.out.println("closedBillList Size Is :::" + closedBillList.size());

            ArrayList closedEBillList = new ArrayList();

            if ("ExcelExport".equals(param)) {
                path = "content/billing/viewInvoiceListExport.jsp";
            } else if ("FETCHDATA".equals(param)) {
                path = "content/billing/viewEInvoiceList.jsp";
            } else if ("download".equals(param)) {
                closedEBillList = billingBP.viewClosedEInvoiceList(custId, invoiceId);
                request.setAttribute("closedEBillList", closedEBillList);
                System.out.println("closedEBillList Size Is :::" + closedEBillList.size());
                path = "content/billing/viewEInvoiceListDownload.jsp";
            } else {
                path = "content/billing/viewEInvoiceList.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView eInvoiceUpload(HttpServletRequest request, HttpServletResponse response, TripCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        FileInputStream fis = null;
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        String custId = "";
        String param = "";
        String gstNo = "";
        String invoiceId = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        int userId = 0;
        String pageTitle = "Shipping Bill No Uploading ";
        menuPath = "Operation >> Shipping Bill No Uploading";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        String customerName = "", billingParty = "", consignee = "", movementType = "", shippingLine = "", vehicleType = "", iso_dso = "", fs_ms_repo = "",
                pickupLocation = "", handling = "", cargo = "", toLocation = "", todays_booking_20 = "", todays_booking_40 = "", s_no = "", loadType = "",
                point1 = "", point2 = "", point3 = "", point4 = "";

//        String containerNo = "", containerSize = "", containerType = "", shippingBillNo = "", requestNo = "", 
//                shippingBillDate = "", gateInDate = "", vehicleNo = "", noOfContainer = "";
        String document_date = "", document_number = "", document_type = "", sellerGst = "", buyerGst = "", status = "", acknowledgementNumber = "",
                acknowledgementDate = "", irn = "", SignedQrCode = "", ewbNumber = "", ewbDate = "", ewbValidTill = "", info = "", error = "";
        try {
            tripCommand = command;
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            path = "content/billing/viewEInvoiceListUpload.jsp";
//            param = request.getParameter("param");
//            invoiceId = request.getParameter("invoiceId");
//            gstNo = request.getParameter("gstNo");
//            custId = request.getParameter("custId");
            isMultipart = ServletFileUpload.isMultipartContent(request);
            ArrayList shippingBillNoList = new ArrayList();
            if (isMultipart) {
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();

                        if (!"".equals(uploadedFileName) && uploadedFileName != null) {

                            String[] splitFileName = uploadedFileName.split("\\.");
                            fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath = tempServerFilePath + "\\" + fileSavedAs;
                            actualFilePath = actualServerFilePath + "\\" + uploadedFileName;
                            System.out.println("tempPath..." + tempFilePath);
                            System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                            System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
                            System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath));
                            System.out.println("tempPath = " + tempFilePath);
                            System.out.println("actPath = " + actualFilePath);
                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
                            String part1 = parts.replace("\\", "");

                            WorkbookSettings ws = new WorkbookSettings();
                            ws.setLocale(new Locale("en", "EN"));
                            Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                            Sheet s = workbook.getSheet(0);
                            System.out.println("rows" + userId + s.getRows());

                            BillingTO billTO = new BillingTO();
                            customerName = s.getCell(1, 1).getContents();
                            System.out.println("customerName=================" + customerName);
                            request.setAttribute("customerName", customerName);
                            for (int i = 1; i < s.getRows(); i++) {
                                System.out.println(" iiiiiii" + i);
                                billTO = new BillingTO();

                                document_date = s.getCell(0, i).getContents();
                                System.out.println("document_date==" + document_date);
                                billTO.setDocumentDate(document_date);

                                document_number = s.getCell(1, i).getContents();
                                System.out.println("document_number=================" + document_number);
                                billTO.setDocumentNumber(document_number);

                                document_type = s.getCell(2, i).getContents();
                                billTO.setDocumentType(document_type);
                                System.out.println("document_type=================" + document_type);

                                sellerGst = s.getCell(3, i).getContents();
                                billTO.setSellerGst(sellerGst);
                                System.out.println("sellerGSt=================" + sellerGst);

                                buyerGst = s.getCell(4, i).getContents();
                                System.out.println("buyerGst=================" + buyerGst);
                                billTO.setBuyerGst(buyerGst);

                                status = s.getCell(5, i).getContents();
                                System.out.println("statuse=================" + status);
                                billTO.setStatus(status);

                                acknowledgementNumber = s.getCell(6, i).getContents();
                                System.out.println("acknowledgementNumber---" + acknowledgementNumber);
                                billTO.setAccNum(acknowledgementNumber);

                                acknowledgementDate = s.getCell(7, i).getContents();
                                System.out.println("acknowledgementDate---" + acknowledgementDate);
                                billTO.setAccDate(acknowledgementDate);

                                irn = s.getCell(8, i).getContents();
                                System.out.println("fs_ms_repo---" + irn);
                                billTO.setIrn(irn);

                                SignedQrCode = s.getCell(9, i).getContents();
                                System.out.println("SignedQrCode---" + SignedQrCode);
                                billTO.setSignedQr(SignedQrCode);

                                ewbNumber = s.getCell(10, i).getContents();
                                System.out.println("ewbNumber---" + ewbNumber);
                                billTO.setEwbNum(ewbNumber);

                                ewbDate = s.getCell(11, i).getContents();
                                System.out.println("ewbDate---" + ewbDate);
                                billTO.setEwbDate(ewbDate);

                                ewbValidTill = s.getCell(12, i).getContents();
                                System.out.println("ewbValidTill---" + ewbValidTill);
                                billTO.setEwbValid(ewbValidTill);

                                info = s.getCell(13, i).getContents();
                                System.out.println("info---" + info);
                                billTO.setInfo(info);

                                error = s.getCell(14, i).getContents();
                                System.out.println("error---" + error);
                                billTO.setError(error);

                                shippingBillNoList.add(billTO);
                            }

                        }
                    } else if (partObj.isParam()) {
                        if (partObj.getName().equals("invoiceId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            invoiceId = paramPart.getStringValue();
                            System.out.println("invoiceId===" + invoiceId);
                        }
                    }
                }
            }

            System.out.println("shippingBillNoList----" + shippingBillNoList.size());
            param = request.getParameter("param");
            ArrayList getOrderListTemp = new ArrayList();

            if ("Submit".equals(param)) {
                getOrderListTemp = billingBP.insertEInvoiceUploadTemp(shippingBillNoList, invoiceId);
                System.out.println("getOrderListTemp---------####-----" + getOrderListTemp.size());

                if (getOrderListTemp.size() == 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Orders not Exist");
                }
            }

            request.setAttribute("orderUploadList", getOrderListTemp);
            request.setAttribute("orderUploadListSize", getOrderListTemp.size());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    public ModelAndView saveEInvoiceUpload(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Billing  >>  EInvoice Uploading";

        try {

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "Create Order Sheet";
            request.setAttribute("pageTitle", pageTitle);
            OperationTO operationTO = new OperationTO();
            BillingTO billTO = new BillingTO();

            String invoiceId = request.getParameter("invoiceCode");
            System.out.println("invoiceId===" + invoiceId);

            ArrayList OrderUploadData = billingBP.getEInvoiveUploadData(invoiceId);
            System.out.println("OrderUploadData : " + OrderUploadData);
            Iterator itr = OrderUploadData.iterator();
            int insertOrder = 0;

            while (itr.hasNext()) {
                billTO = (BillingTO) itr.next();
                billTO.setInvoiceId(billTO.getInvoiceCode());
                billTO.setDocumentDate(billTO.getDocumentDate());
                billTO.setDocumentNumber(billTO.getDocumentNumber());
                billTO.setDocumentType(billTO.getDocumentType());
                billTO.setSellerGst(billTO.getSellerGst());
                billTO.setBuyerGst(billTO.getBuyerGst());
                billTO.setAccNum(billTO.getAccNum());
                billTO.setAccDate(billTO.getAccDate());
                billTO.setIrn(billTO.getIrn());
                billTO.setSignedQr(billTO.getSignedQr());
                billTO.setEwbNum(billTO.getEwbNum());
                billTO.setEwbDate(billTO.getEwbDate());
                billTO.setEwbValid(billTO.getEwbValid());
                billTO.setInfo(billTO.getInfo());
                billTO.setError(billTO.getError());

                insertOrder = billingBP.insertEInvoiceUpload(billTO);

            }

            if (insertOrder > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "e-Invoice Uploaded Sucessfully");
            } else {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "e-Invoice Not Uploaded");
            }

            path = "content/billing/viewEInvoiceListUpload.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

//suppInvoice starts
    public ModelAndView handleEsupplementryInvoiceList(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        billingCommand = command;
        String menuPath = "";
        BillingTO billingTO = new BillingTO();
        String pageTitle = "View SupplymentryInvoiceList";
        menuPath = "View SupplymentryInvoiceList";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String customerName = "";
        String customerId = "";
        String fromDate = "";
        String toDate = "";
        String billOfEntry = "";
        String shippingBillNo = "";
        String type = "";
        String movementType = "";
        try {

            path = "content/billing/viewSupplyInvoiceList.jsp";
            int userId = (Integer) session.getAttribute("userId");

            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            String param = request.getParameter("param");
            customerId = request.getParameter("customerId");
            String submitStatus = request.getParameter("submitStatus");
            String tripType = request.getParameter("tripType");
            String grNo = request.getParameter("grNo");
            String billNo = request.getParameter("billNo");

            fromDate = request.getParameter("fromDate");
            toDate = request.getParameter("toDate");

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            System.out.println("fromDtae---" + fromDate);
            System.out.println("toDate---" + toDate);
            if ("".equals(fromDate) || fromDate == null) {
                fromDate = startDate;
            }
            if ("".equals(toDate) || toDate == null) {
                toDate = endDate;
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            request.setAttribute("customerId", customerId);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("tripType", tripType);
            request.setAttribute("submitStatus", submitStatus);
            request.setAttribute("billNo", billNo);
            request.setAttribute("grNo", grNo);

            billingTO.setUserId(userId + "");
            billingTO.setFromDate(fromDate);
            billingTO.setToDate(toDate);
            billingTO.setCustomerName(customerName);
            billingTO.setCustomerId(customerId);
            billingTO.setBillOfEntryNo(grNo);
            billingTO.setShipingBillNo(billNo);
            billingTO.setBillingType(tripType);
            billingTO.setMovementType(movementType);
//            if (!"0".equals(customerName)) {}
            ArrayList movementTypeList = new ArrayList();
            movementTypeList = operationBP.getMovementTypeList();
            request.setAttribute("movementTypeList", movementTypeList);
            ArrayList customerList = new ArrayList();
            customerList = operationBP.getTripCustomerList();
            request.setAttribute("customerList", customerList);
            System.out.println("customerList Size Is :::" + customerList.size());

            ArrayList closedTrips = new ArrayList();
            closedTrips = billingBP.viewSupplyInvoiceList(billingTO);
            request.setAttribute("closedBillList", closedTrips);

//             ArrayList closedBillList = new ArrayList();
//            //closedBillList = operationBP.getClosedBilledList(customerId, fromDate, toDate);
//            closedBillList = billingBP.viewSuppBillForSubmission(customerId, fromDate, toDate, tripType, submitStatus, billNo, grNo);
//            request.setAttribute("closedBillList", closedBillList);
//            System.out.println("closedBillList Size Is :::" + closedBillList.size());
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView eSuppInvoiceGenerate(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Finance  >>  Invoice Approval ";

        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);
            BillingTO billingTO = new BillingTO();
            String[] invoiceIds = request.getParameterValues("selectedIndex");
            String custId = request.getParameter("custId");
            billingTO.setInvoiceIds(invoiceIds);
            billingTO.setCustomerId(custId);
            int status = billingBP.eSuppInvoiceGenerate(billingTO, userId);
            System.out.println("status =" + status);
            path = "content/billing/viewSupplyInvoiceList.jsp";

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "E-Invoice Generated Successfully....");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

//    download upload
    public ModelAndView handleESuppInvoiceListForDownloadUpload(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        int userId = 0;
        HttpSession session = null;
        billingCommand = command;
        String menuPath = "";
        BillingTO billingTO = null;
        String pageTitle = "View Supp Invoice List";
        menuPath = "Operation >>  View Supp Invoice List";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String fromDate = "";
        String toDate = "";
        String customerId = "";
        String custId = "";
        String tripType = "";
        String submitStatus = "";
        String param = "";
        String billNo = "";
        String grNo = "";
        String containerNo = "";
        String gstNo = "";
        String invoiceId = "";
        try {
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            param = request.getParameter("param");
            customerId = request.getParameter("customerId");
            invoiceId = request.getParameter("invoiceId");
            gstNo = request.getParameter("gstNo");
            custId = request.getParameter("custId");
            fromDate = request.getParameter("fromDate");
            toDate = request.getParameter("toDate");
            submitStatus = request.getParameter("submitStatus");
            tripType = request.getParameter("tripType");
            billNo = request.getParameter("billNo");
            System.out.println("BILLNO------" + billNo);
            grNo = request.getParameter("grNo");
            containerNo = request.getParameter("containerNo");

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            System.out.println("fromDtae---" + fromDate);
            System.out.println("toDate---" + toDate);
            if ("".equals(fromDate) || fromDate == null) {
                fromDate = startDate;
            }
            if ("".equals(toDate) || toDate == null) {
                toDate = endDate;
            }

            request.setAttribute("customerId", customerId);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("tripType", tripType);
            request.setAttribute("submitStatus", submitStatus);
            request.setAttribute("billNo", billNo);
            request.setAttribute("grNo", grNo);
            request.setAttribute("containerNo", containerNo);
            request.setAttribute("gstNo", gstNo);
            request.setAttribute("supplierGstNo", "06AACCB8054G1ZT");

            int status = 0;
            String cancelStatus = request.getParameter("cancelStatus");
            String billingNo = request.getParameter("billingNo");

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getTripCustomerList();
            request.setAttribute("customerList", customerList);
            System.out.println("customerList Size Is :::" + customerList.size());

            //closedBillList = operationBP.getClosedBilledList(customerId, fromDate, toDate);
            ArrayList closedBillList = new ArrayList();
            closedBillList = billingBP.viewESuppInvoiceList(customerId, fromDate, toDate, tripType, submitStatus, billNo, grNo, containerNo);
            request.setAttribute("closedBillList", closedBillList);
            System.out.println("closedBillList Size Is :::" + closedBillList.size());

            ArrayList closedEBillList = new ArrayList();

            if ("ExcelExport".equals(param)) {
                path = "content/billing/viewSuppInvoiceListExport.jsp";
            } else if ("FETCHDATA".equals(param)) {
                path = "content/billing/viewESuppInvoiceList.jsp";
            } else if ("download".equals(param)) {
                closedEBillList = billingBP.viewClosedESuppInvoiceList(custId, invoiceId);
                request.setAttribute("closedEBillList", closedEBillList);
                System.out.println("closedEBillList Size Is :::" + closedEBillList.size());
                path = "content/billing/viewESuppInvoiceListDownload.jsp";
            } else {
                path = "content/billing/viewESuppInvoiceList.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

//   upload
    public ModelAndView eSuppInvoiceUpload(HttpServletRequest request, HttpServletResponse response, TripCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        FileInputStream fis = null;
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        String custId = "";
        String param = "";
        String gstNo = "";
        String invoiceId = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        int userId = 0;
        String pageTitle = "eSuppInvoiceUpload ";
        menuPath = "Operation >> eSuppInvoiceUpload";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        String customerName = "", billingParty = "", consignee = "", movementType = "", shippingLine = "", vehicleType = "", iso_dso = "", fs_ms_repo = "",
                pickupLocation = "", handling = "", cargo = "", toLocation = "", todays_booking_20 = "", todays_booking_40 = "", s_no = "", loadType = "",
                point1 = "", point2 = "", point3 = "", point4 = "";

//        String containerNo = "", containerSize = "", containerType = "", shippingBillNo = "", requestNo = "", 
//                shippingBillDate = "", gateInDate = "", vehicleNo = "", noOfContainer = "";
        String document_date = "", document_number = "", document_type = "", sellerGst = "", buyerGst = "", status = "", acknowledgementNumber = "",
                acknowledgementDate = "", irn = "", SignedQrCode = "", ewbNumber = "", ewbDate = "", ewbValidTill = "", info = "", error = "";
        try {
            tripCommand = command;
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            path = "content/billing/viewESuppInvoiceListUpload.jsp";
//            param = request.getParameter("param");
//            invoiceId = request.getParameter("invoiceId");
//            gstNo = request.getParameter("gstNo");
//            custId = request.getParameter("custId");
            isMultipart = ServletFileUpload.isMultipartContent(request);
            ArrayList shippingBillNoList = new ArrayList();
            if (isMultipart) {
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();

                        if (!"".equals(uploadedFileName) && uploadedFileName != null) {

                            String[] splitFileName = uploadedFileName.split("\\.");
                            fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath = tempServerFilePath + "\\" + fileSavedAs;
                            actualFilePath = actualServerFilePath + "\\" + uploadedFileName;
                            System.out.println("tempPath..." + tempFilePath);
                            System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                            System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
                            System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath));
                            System.out.println("tempPath = " + tempFilePath);
                            System.out.println("actPath = " + actualFilePath);
                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
                            String part1 = parts.replace("\\", "");

                            WorkbookSettings ws = new WorkbookSettings();
                            ws.setLocale(new Locale("en", "EN"));
                            Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                            Sheet s = workbook.getSheet(0);
                            System.out.println("rows" + userId + s.getRows());

                            BillingTO billTO = new BillingTO();
                            customerName = s.getCell(1, 1).getContents();
                            System.out.println("customerName=================" + customerName);
                            request.setAttribute("customerName", customerName);
                            for (int i = 1; i < s.getRows(); i++) {
                                System.out.println(" iiiiiii" + i);
                                billTO = new BillingTO();

                                document_date = s.getCell(0, i).getContents();
                                System.out.println("document_date==" + document_date);
                                billTO.setDocumentDate(document_date);

                                document_number = s.getCell(1, i).getContents();
                                System.out.println("document_number=================" + document_number);
                                billTO.setDocumentNumber(document_number);

                                document_type = s.getCell(2, i).getContents();
                                billTO.setDocumentType(document_type);
                                System.out.println("document_type=================" + document_type);

                                sellerGst = s.getCell(3, i).getContents();
                                billTO.setSellerGst(sellerGst);
                                System.out.println("sellerGSt=================" + sellerGst);

                                buyerGst = s.getCell(4, i).getContents();
                                System.out.println("buyerGst=================" + buyerGst);
                                billTO.setBuyerGst(buyerGst);

                                status = s.getCell(5, i).getContents();
                                System.out.println("statuse=================" + status);
                                billTO.setStatus(status);

                                acknowledgementNumber = s.getCell(6, i).getContents();
                                System.out.println("acknowledgementNumber---" + acknowledgementNumber);
                                billTO.setAccNum(acknowledgementNumber);

                                acknowledgementDate = s.getCell(7, i).getContents();
                                System.out.println("acknowledgementDate---" + acknowledgementDate);
                                billTO.setAccDate(acknowledgementDate);

                                irn = s.getCell(8, i).getContents();
                                System.out.println("fs_ms_repo---" + irn);
                                billTO.setIrn(irn);

                                SignedQrCode = s.getCell(9, i).getContents();
                                System.out.println("SignedQrCode---" + SignedQrCode);
                                billTO.setSignedQr(SignedQrCode);

                                ewbNumber = s.getCell(10, i).getContents();
                                System.out.println("ewbNumber---" + ewbNumber);
                                billTO.setEwbNum(ewbNumber);

                                ewbDate = s.getCell(11, i).getContents();
                                System.out.println("ewbDate---" + ewbDate);
                                billTO.setEwbDate(ewbDate);

                                ewbValidTill = s.getCell(12, i).getContents();
                                System.out.println("ewbValidTill---" + ewbValidTill);
                                billTO.setEwbValid(ewbValidTill);

                                info = s.getCell(13, i).getContents();
                                System.out.println("info---" + info);
                                billTO.setInfo(info);

                                error = s.getCell(14, i).getContents();
                                System.out.println("error---" + error);
                                billTO.setError(error);

                                shippingBillNoList.add(billTO);
                            }

                        }
                    } else if (partObj.isParam()) {
                        if (partObj.getName().equals("invoiceId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            invoiceId = paramPart.getStringValue();
                            System.out.println("invoiceId===" + invoiceId);
                        }
                    }
                }
            }

            System.out.println("shippingBillNoList----" + shippingBillNoList.size());
            param = request.getParameter("param");
            ArrayList getOrderListTemp = new ArrayList();

            if ("Submit".equals(param)) {
                getOrderListTemp = billingBP.insertESuppInvoiceUploadTemp(shippingBillNoList, invoiceId);
                System.out.println("getOrderListTemp---------####-----" + getOrderListTemp.size());

                if (getOrderListTemp.size() == 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Orders not Exist");
                }
            }

            request.setAttribute("orderUploadList", getOrderListTemp);
            request.setAttribute("orderUploadListSize", getOrderListTemp.size());

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);
    }

    public ModelAndView saveESuppInvoiceUpload(HttpServletRequest request, HttpServletResponse response, TripCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Billing  >>  EInvoice Uploading";

        try {

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "Create Order Sheet";
            request.setAttribute("pageTitle", pageTitle);
            OperationTO operationTO = new OperationTO();
            BillingTO billTO = new BillingTO();

            String invoiceId = request.getParameter("invoiceCode");
            System.out.println("invoiceId===" + invoiceId);

            ArrayList OrderUploadData = billingBP.getESuppInvoiveUploadData(invoiceId);
            System.out.println("OrderUploadData : " + OrderUploadData);
            Iterator itr = OrderUploadData.iterator();
            int insertOrder = 0;

            while (itr.hasNext()) {
                billTO = (BillingTO) itr.next();
                billTO.setInvoiceId(billTO.getInvoiceCode());
                billTO.setDocumentDate(billTO.getDocumentDate());
                billTO.setDocumentNumber(billTO.getDocumentNumber());
                billTO.setDocumentType(billTO.getDocumentType());
                billTO.setSellerGst(billTO.getSellerGst());
                billTO.setBuyerGst(billTO.getBuyerGst());
                billTO.setAccNum(billTO.getAccNum());
                billTO.setAccDate(billTO.getAccDate());
                billTO.setIrn(billTO.getIrn());
                billTO.setSignedQr(billTO.getSignedQr());
                billTO.setEwbNum(billTO.getEwbNum());
                billTO.setEwbDate(billTO.getEwbDate());
                billTO.setEwbValid(billTO.getEwbValid());
                billTO.setInfo(billTO.getInfo());
                billTO.setError(billTO.getError());

                insertOrder = billingBP.insertESuppInvoiceUpload(billTO);

            }

            if (insertOrder > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "e-SuppInvoice Uploaded Sucessfully");
            } else {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "e-SuppInvoice Not Uploaded");
            }

            path = "content/billing/viewESuppInvoiceListUpload.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

//    New EInvoiceCredit starts
    public ModelAndView handleEcreditNoteList(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bankPaymentLedgerList = new ArrayList();

        HttpSession session = request.getSession();
        billingCommand = command;
        String menuPath = "";
        String pageTitle = "E-Credit Search";
        menuPath = "Finance  >> E- Credit Search";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        BillingTO billingTO = new BillingTO();
        String billNo = "";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/billing/viewCreditInvoiceList.jsp";
            if (billingCommand.getCustomerId() != null && billingCommand.getCustomerId() != "") {
                billingTO.setCustomerId(billingCommand.getCustomerId());
                request.setAttribute("customerId", billingCommand.getCustomerId());
            }

//            if (billingCommand.getFromDate() != null && billingCommand.getFromDate() != "") {
//                billingTO.setFromDate(billingCommand.getFromDate());
//                request.setAttribute("fromDate", billingCommand.getFromDate());
//            }
//            if (billingCommand.getToDate() != null && billingCommand.getToDate() != "") {
//                billingTO.setToDate(billingCommand.getToDate());
//                request.setAttribute("toDate", billingCommand.getToDate());
//            }
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            System.out.println("fromDtae---" + fromDate);
            System.out.println("toDate---" + toDate);
            if ("".equals(fromDate) || fromDate == null) {
                fromDate = startDate;
            }
            if ("".equals(toDate) || toDate == null) {
                toDate = endDate;
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            billingTO.setFromDate(fromDate);
            billingTO.setToDate(toDate);

            billNo = request.getParameter("billNo");
            System.out.println("BILLNO------" + billNo);
            billingTO.setBillNo(billNo);
            request.setAttribute("billNo", billNo);

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getTripCustomerList();
            request.setAttribute("customerList", customerList);

            ArrayList creditNoteSearchList = new ArrayList();
            creditNoteSearchList = billingBP.getEcreditNoteList(billingTO);//0 indicates all customers
            System.out.println("creditNoteSearchList --" + creditNoteSearchList.size());
            if (creditNoteSearchList.size() > 0) {
                request.setAttribute("closedBillList", creditNoteSearchList);
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleEsupplementryCreditNoteList(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bankPaymentLedgerList = new ArrayList();

        HttpSession session = request.getSession();
        billingCommand = command;
        String menuPath = "";
        String pageTitle = "Credit Search";
        menuPath = "Finance  >> Credit Search";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        BillingTO billingTO = new BillingTO();
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/billing/viewCreditSuppInvoiceList.jsp";
            if (billingCommand.getCustomerId() != null && billingCommand.getCustomerId() != "") {
                billingTO.setCustomerId(billingCommand.getCustomerId());
                request.setAttribute("customerId", billingCommand.getCustomerId());
            }
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            System.out.println("fromDtae---" + fromDate);
            System.out.println("toDate---" + toDate);
            if ("".equals(fromDate) || fromDate == null) {
                fromDate = startDate;
            }
            if ("".equals(toDate) || toDate == null) {
                toDate = endDate;
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            String billNo = request.getParameter("billNo");
            System.out.println("BILLNO------" + billNo);
            billingTO.setBillNo(billNo);
            request.setAttribute("billNo", billNo);
            ArrayList customerList = new ArrayList();
            customerList = operationBP.getTripCustomerList();
            request.setAttribute("customerList", customerList);
            ArrayList creditNoteSearchList = new ArrayList();
            creditNoteSearchList = billingBP.getEsupplyCreditNoteList(billingTO);//0 indicates all customers
            System.out.println("creditNoteSearchList --" + creditNoteSearchList.size());
            if (creditNoteSearchList.size() > 0) {
                request.setAttribute("closedBillList", creditNoteSearchList);
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Entry List --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView eCreditInvoiceGenerate(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Finance  >>  Invoice Approval ";

        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);
            BillingTO billingTO = new BillingTO();
            String[] invoiceIds = request.getParameterValues("selectedIndex");
            String custId = request.getParameter("custId");
            billingTO.setInvoiceIds(invoiceIds);
            billingTO.setCustomerId(custId);
            int status = billingBP.eCreditInvoiceGenerate(billingTO, userId);
            System.out.println("status =" + status);
            path = "content/billing/viewCreditInvoiceList.jsp";

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "E-CreditInvoice Generated Successfully....");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView eSuppCreditInvoiceGenerate(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Finance  >>  Invoice Approval ";

        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);
            BillingTO billingTO = new BillingTO();
            String[] invoiceIds = request.getParameterValues("selectedIndex");
            String custId = request.getParameter("custId");
            billingTO.setInvoiceIds(invoiceIds);
            billingTO.setCustomerId(custId);
            int status = billingBP.eCreditSuppInvoiceGenerate(billingTO, userId);
            System.out.println("status =" + status);
            path = "content/billing/viewCreditSuppInvoiceList.jsp";

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "E-SuppCreditInvoice Generated Successfully....");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleECreditInvoiceListForDownloadUpload(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        int userId = 0;
        HttpSession session = null;
        billingCommand = command;
        String menuPath = "";
        BillingTO billingTO = null;
        String pageTitle = "View Invoice List";
        menuPath = "Operation >>  View Invoice List";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String fromDate = "";
        String toDate = "";
        String customerId = "";
        String custId = "";
        String tripType = "";
        String submitStatus = "";
        String param = "";
        String billNo = "";
        String grNo = "";
        String containerNo = "";
        String gstNo = "";
        String invoiceId = "";
        try {
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            param = request.getParameter("param");
            customerId = request.getParameter("customerId");
            invoiceId = request.getParameter("invoiceId");
            gstNo = request.getParameter("gstNo");
            custId = request.getParameter("custId");
            fromDate = request.getParameter("fromDate");
            toDate = request.getParameter("toDate");
            submitStatus = request.getParameter("submitStatus");
            tripType = request.getParameter("tripType");
            billNo = request.getParameter("billNo");
            System.out.println("BILLNO------" + billNo);
            grNo = request.getParameter("grNo");
            containerNo = request.getParameter("containerNo");

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            System.out.println("fromDtae---" + fromDate);
            System.out.println("toDate---" + toDate);
            if ("".equals(fromDate) || fromDate == null) {
                fromDate = startDate;
            }
            if ("".equals(toDate) || toDate == null) {
                toDate = endDate;
            }

            request.setAttribute("customerId", customerId);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("tripType", tripType);
            request.setAttribute("submitStatus", submitStatus);
            request.setAttribute("billNo", billNo);
            request.setAttribute("grNo", grNo);
            request.setAttribute("containerNo", containerNo);
            request.setAttribute("gstNo", gstNo);
            request.setAttribute("supplierGstNo", "06AACCB8054G1ZT");

            int status = 0;
            String cancelStatus = request.getParameter("cancelStatus");
            String billingNo = request.getParameter("billingNo");

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getTripCustomerList();
            request.setAttribute("customerList", customerList);
            System.out.println("customerList Size Is :::" + customerList.size());

            //closedBillList = operationBP.getClosedBilledList(customerId, fromDate, toDate);
            ArrayList closedBillList = new ArrayList();
            closedBillList = billingBP.viewECreditInvoiceList(customerId, fromDate, toDate, tripType, submitStatus, billNo, grNo, containerNo);
            request.setAttribute("closedBillList", closedBillList);
            System.out.println("closedBillList Size Is :::" + closedBillList.size());

            ArrayList closedEBillList = new ArrayList();

            if ("ExcelExport".equals(param)) {
                path = "content/billing/viewInvoiceListExport.jsp";
            } else if ("FETCHDATA".equals(param)) {
                path = "content/billing/viewECreditInvoiceList.jsp";
            } else if ("download".equals(param)) {
                closedEBillList = billingBP.viewClosedECreditInvoiceList(custId, invoiceId);
                request.setAttribute("closedEBillList", closedEBillList);
                System.out.println("closedEBillList Size Is :::" + closedEBillList.size());
                path = "content/billing/viewECreditInvoiceListDownload.jsp";
            } else {
                path = "content/billing/viewECreditInvoiceList.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleECreditSuppInvoiceListForDownloadUpload(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        int userId = 0;
        HttpSession session = null;
        billingCommand = command;
        String menuPath = "";
        BillingTO billingTO = null;
        String pageTitle = "View Invoice List";
        menuPath = "Operation >>  View Invoice List";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String fromDate = "";
        String toDate = "";
        String customerId = "";
        String custId = "";
        String tripType = "";
        String submitStatus = "";
        String param = "";
        String billNo = "";
        String grNo = "";
        String containerNo = "";
        String gstNo = "";
        String invoiceId = "";
        try {
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            param = request.getParameter("param");
            customerId = request.getParameter("customerId");
            invoiceId = request.getParameter("invoiceId");
            gstNo = request.getParameter("gstNo");
            custId = request.getParameter("custId");
            fromDate = request.getParameter("fromDate");
            toDate = request.getParameter("toDate");
            submitStatus = request.getParameter("submitStatus");
            tripType = request.getParameter("tripType");
            billNo = request.getParameter("billNo");
            System.out.println("BILLNO------" + billNo);
            grNo = request.getParameter("grNo");
            containerNo = request.getParameter("containerNo");

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "01" + "-" + temp[1] + "-" + temp[2];
            }
            System.out.println("fromDtae---" + fromDate);
            System.out.println("toDate---" + toDate);
            if ("".equals(fromDate) || fromDate == null) {
                fromDate = startDate;
            }
            if ("".equals(toDate) || toDate == null) {
                toDate = endDate;
            }

            request.setAttribute("customerId", customerId);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("tripType", tripType);
            request.setAttribute("submitStatus", submitStatus);
            request.setAttribute("billNo", billNo);
            request.setAttribute("grNo", grNo);
            request.setAttribute("containerNo", containerNo);
            request.setAttribute("gstNo", gstNo);
            request.setAttribute("supplierGstNo", "06AACCB8054G1ZT");

            int status = 0;
            String cancelStatus = request.getParameter("cancelStatus");
            String billingNo = request.getParameter("billingNo");

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getTripCustomerList();
            request.setAttribute("customerList", customerList);
            System.out.println("customerList Size Is :::" + customerList.size());

            //closedBillList = operationBP.getClosedBilledList(customerId, fromDate, toDate);
            ArrayList closedBillList = new ArrayList();
            closedBillList = billingBP.viewECreditSuppInvoiceList(customerId, fromDate, toDate, tripType, submitStatus, billNo, grNo, containerNo);
            request.setAttribute("closedBillList", closedBillList);
            System.out.println("closedBillList Size Is :::" + closedBillList.size());

            ArrayList closedEBillList = new ArrayList();

            if ("ExcelExport".equals(param)) {
                path = "content/billing/viewInvoiceListExport.jsp";
            } else if ("FETCHDATA".equals(param)) {
                path = "content/billing/viewECreditSuppInvoiceList.jsp";
            } else if ("download".equals(param)) {
                closedEBillList = billingBP.viewClosedECreditSuppInvoiceList(custId, invoiceId);
                request.setAttribute("closedEBillList", closedEBillList);
                System.out.println("closedEBillList Size Is :::" + closedEBillList.size());
                path = "content/billing/viewECreditSuppInvoiceListDownload.jsp";
            } else {
                path = "content/billing/viewECreditSuppInvoiceList.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView approveSuppInvoice(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Finance  >>  Invoice Approval ";

        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);
            BillingTO billingTO = new BillingTO();
            String[] invoiceIds = request.getParameterValues("selectedIndex");
            billingTO.setInvoiceIds(invoiceIds);
            int status = billingBP.approveSuppInvoice(billingTO);
            System.out.println("status =" + status);

            path = "content/billing/viewOrderSuppBillForSubmission.jsp";

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Invoice Approved Successfully....");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } /*
         * run time exception has occurred. Directed to error page.
         */ catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

// public ModelAndView viewOrderSuppBillForSubmit(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
//        String path = "";
//        int userId = 0;
//        HttpSession session = null;
//        billingCommand = command;
//        String menuPath = "";
//        BillingTO billingTO = null;
//        String pageTitle = "View Route Details";
//        menuPath = "Operation >>  View Bill For Submission";
//        request.setAttribute("pageTitle", pageTitle);
//        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//        String fromDate = "";
//        String toDate = "";
//        String customerId = "";
//        String tripType = "";
//        String submitStatus = "";
//        String param = "";
//        String billNo = "";
//        String grNo = "";
//        try {
//            session = request.getSession();
//            userId = (Integer) session.getAttribute("userId");
//            param = request.getParameter("param");
//            customerId = request.getParameter("customerId");
//            fromDate = request.getParameter("fromDate");
//            toDate = request.getParameter("toDate");
//            submitStatus = request.getParameter("submitStatus");
//            tripType = request.getParameter("tripType");
//            grNo = request.getParameter("grNo");
//            billNo = request.getParameter("billNo");
//
//            request.setAttribute("customerId", customerId);
//            request.setAttribute("fromDate", fromDate);
//            request.setAttribute("toDate", toDate);
//            request.setAttribute("tripType", tripType);
//            request.setAttribute("submitStatus", submitStatus);
//            request.setAttribute("billNo", billNo);
//            request.setAttribute("grNo", grNo);
//
//            int status = 0;
//            String cancelStatus = request.getParameter("cancelStatus");
//            String billingNo = request.getParameter("billingNo");
//
//            if (cancelStatus != null) {
//                status = billingBP.updateCancel(billingNo, userId);
//                System.out.println("status" + status);
//                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, billingNo + " billingNo  cancelled Successfully....");
//            }
//
//            ArrayList customerList = new ArrayList();
//            customerList = operationBP.getTripCustomerList();
//            request.setAttribute("customerList", customerList);
//            System.out.println("customerList Size Is :::" + customerList.size());
//
//            ArrayList closedBillList = new ArrayList();
//            //closedBillList = operationBP.getClosedBilledList(customerId, fromDate, toDate);
//            closedBillList = billingBP.viewSuppBillForSubmission(customerId, fromDate, toDate, tripType, submitStatus, billNo, grNo);
//            request.setAttribute("closedBillList", closedBillList);
//            System.out.println("closedBillList Size Is :::" + closedBillList.size());
//
//            if (!"ExcelExport".equals(param)) {
//                path = "content/billing/viewOrderSuppBillForSubmission.jsp";
//            } else {
//                path = "content/billing/viewOrderSuppBillForSubmissionExport.jsp";
//            }
//
//        } catch (FPRuntimeException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        } catch (Exception exception) {
//            exception.printStackTrace();
//            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        }
//        return new ModelAndView(path);
//    }
    public ModelAndView saveOrderBill(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Finance  >>  Generate Bill ";
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            BillingTO billingTO = new BillingTO();

            String consignmentOrderId = request.getParameter("consignmentOrderId");
            String tripSheetId = request.getParameter("tripSheetId");
            String finYear = request.getParameter("finYear");
            billingTO.setConsignmentOrderId(consignmentOrderId);
            billingTO.setTripSheetId(tripSheetId);
            String commodityName = request.getParameter("commodityName");
            String gstType = request.getParameter("gstType");
            String invoiceNo = request.getParameter("invoiceNo");
            String customerId = request.getParameter("customerId");
            String customerAddress = request.getParameter("billingPartyAddress");
            String totalRevenue = request.getParameter("totalRevenue");
            //  String estimatedRevenue = request.getParameter("estimatedRevenue");
            String totalExpToBeBilled = request.getParameter("totalExpToBeBilled");
            //String grandTotal = request.getParameter("grandTotal");
            String noOfTrips = request.getParameter("noOfTrips");
            String noOfOrders = request.getParameter("noOfOrders");
            String billingPartyId = request.getParameter("billingPartyId");
            String billingParty = request.getParameter("billingParty");
            String remarks = request.getParameter("detaintionRemarks");
            String grandTotal = request.getParameter("netAmount");
            String repoOrder = request.getParameter("repoOrder");
            String totalTax = request.getParameter("totalTax");
            String cgstAmount = request.getParameter("cgstAmount");
            String sgstAmount = request.getParameter("sgstAmount");
            String igstAmount = request.getParameter("igstAmount");
            String organizationId = request.getParameter("organizationId");
            String billingState = request.getParameter("billingState");
            String GSTNo = request.getParameter("GSTNo");
            String PANNo = request.getParameter("panNo");
            String igstPercentage = request.getParameter("IGST");
            String cgstPercentage = request.getParameter("CGST");
            String sgstPercentage = request.getParameter("SGST");

            String otherExpense = request.getParameter("totOtherExpense");
            billingTO.setIgstPercentage(igstPercentage);
            billingTO.setCgstPercentage(cgstPercentage);
            billingTO.setSgstPercentage(sgstPercentage);
            billingTO.setIgstAmount(igstAmount);
            billingTO.setCgstAmount(cgstAmount);
            billingTO.setSgstAmount(sgstAmount);
            billingTO.setTotalTax(totalTax);
            billingTO.setOrganizationId(organizationId);
            billingTO.setBillingState(billingState);
            billingTO.setGstNo(GSTNo);
            billingTO.setPanNo(PANNo);
            billingTO.setOrderCount(noOfOrders);
            billingTO.setNoOfTrips(noOfTrips);
            billingTO.setUserId(userId + "");
            billingTO.setCustomerId(customerId);
            billingTO.setTotalRevenue(totalRevenue);
            billingTO.setTotalExpToBeBilled(totalExpToBeBilled);
            billingTO.setCustomerAddress(customerAddress);
            billingTO.setBillingPartyId(billingPartyId);
            billingTO.setBillingParty(billingParty);
            billingTO.setRemarks(remarks);
            billingTO.setGrandTotal(grandTotal);
            billingTO.setFinYear(finYear);
            billingTO.setCommodityName(commodityName);
            billingTO.setGstType(gstType);
            System.out.println("gstType invoice controller---" + gstType);

            billingTO.setOtherExpense(otherExpense);
            //generate tripcode
            String[] tripIds = request.getParameterValues("tripId");
            String[] tripIdIGSTAmount = request.getParameterValues("tripIGSTAmount");
            String[] tripIdCGSTAmount = request.getParameterValues("tripCGSTAmount");
            String[] tripIdSGSTAmount = request.getParameterValues("tripSGSTAmount");

            int existStatus = tripBP.getTripExistInBilling(tripIds);

            int noOfBillInHeader = tripBP.getNOfBillInHeader(tripIds);
            int gstTypesInTrip = tripBP.getGstTypesInTrip(tripIds);

            System.out.println("noOfBillInHeader----" + noOfBillInHeader);
            int insertStatus = 0;
            if (noOfBillInHeader == 0 && gstTypesInTrip == 0) {
//                   String getCustGtaType=billingBP.getCustGtaType(billingTO);
//////                rohan
                
//                   if( "Y".equals(getCustGtaType)){
//                
//                insertStatus = billingBP.saveOrderBillForGta(billingTO, userId, finYear);
//                
//                
//                   }else{
                
                insertStatus = billingBP.saveOrderBill(billingTO, userId, finYear);

//                   }
                int tripGst = billingBP.updateTripGst(String.valueOf(userId), tripIds,
                        tripIdIGSTAmount, tripIdCGSTAmount, tripIdSGSTAmount);
                request.setAttribute("consignmentOrderId", consignmentOrderId);
                request.setAttribute("invoiceNo", invoiceNo);
                request.setAttribute("invoiceId", insertStatus);

//            path = "content/trip/responsePage.jsp";
//            path = "viewOrderBillDetailsForSubmit.do?invoiceId="+invoiceId+"&submitStatus="+0+"&cancelStatus="+1;
                System.out.println("request sessionPageParam:" + request.getParameter("sessionPageParam"));
                System.out.println("session sessionPageParam:" + session.getAttribute("sessionPageParam"));
                request.setAttribute("sessionPageParam", session.getAttribute("sessionPageParam"));
                System.out.println("xxxxxxxxxxxxxxxxx-------------------------------------");
                path = "viewOrderBillDetailsForSubmit.do?invoiceId=" + insertStatus + "&repoOrder=" + repoOrder + "&submitStatus=" + 0 + "&cancelStatus=1&sessionPageParam=" + session.getAttribute("sessionPageParam");
//            mv = viewOrderBillDetailsForSubmit(request,response,command);
                System.out.println("path:" + path);
           
            } else {
                path = "gererateOrderBill.do?sessionPageParam=" + session.getAttribute("sessionPageParam");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView resendInvoice(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Finance  >>  Invoice Approval ";

        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);
            BillingTO billingTO = new BillingTO();
            String[] invoiceIds = request.getParameterValues("selectedIndex");
            billingTO.setInvoiceIds(invoiceIds);
            int status = billingBP.resendInvoice(billingTO);
            System.out.println("status =" + status);

            path = "content/billing/viewOrderBillForSubmission.jsp";

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Invoice Updated Successfully....");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    // combined bill
    public ModelAndView ViewCombinedBills(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        int userId = 0;
        HttpSession session = null;
        billingCommand = command;
        String menuPath = "";
        BillingTO billingTO = null;
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  View Bill For Submission";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String fromDate = "";
        String toDate = "";
        String customerId = "";
        String tripType = "";
        String submitStatus = "";
        String param = "";
        String billNo = "";
        String grNo = "";
        String containerNo = "";
        try {
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            param = request.getParameter("param");
            customerId = request.getParameter("customerId");
            fromDate = request.getParameter("fromDate");
            toDate = request.getParameter("toDate");
            submitStatus = request.getParameter("submitStatus");
            tripType = request.getParameter("tripType");
            billNo = request.getParameter("billNo");
            grNo = request.getParameter("grNo");
            containerNo = request.getParameter("containerNo");

            request.setAttribute("customerId", customerId);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("tripType", tripType);
            request.setAttribute("submitStatus", submitStatus);
            request.setAttribute("billNo", billNo);
            request.setAttribute("grNo", grNo);
            request.setAttribute("containerNo", containerNo);

            int status = 0;
            String cancelStatus = request.getParameter("cancelStatus");
            String billingNo = request.getParameter("billingNo");

            if (cancelStatus != null) {
                status = billingBP.updateCancel(billingNo, userId);
                System.out.println("status" + status);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, billingNo + " billingNo  cancelled Successfully....");
            }

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getTripCustomerList();
            request.setAttribute("customerList", customerList);
            System.out.println("customerList Size Is :::" + customerList.size());

            ArrayList combinedBillList = new ArrayList();
            combinedBillList = billingBP.viewcombinedBillList(customerId, fromDate, toDate, tripType, submitStatus, billNo, grNo, containerNo);
            request.setAttribute("closedBillList", combinedBillList);
            System.out.println("combinedBillList Size Is :::" + combinedBillList.size());

            if (!"ExcelExport".equals(param)) {
                path = "content/billing/viewCombinedBillDetails.jsp";
            } else {
                path = "content/billing/viewOrderBillForSubmissionExport.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

  
   public ModelAndView viewWorkOrderDetailsForSubmit(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        int userId = 0;
        HttpSession session = null;
        billingCommand = command;
        String menuPath = "";
        BillingTO billingTO = null;
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  View Bill For Submission";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String invoiceId = "";
        String param = "";
       
        try {
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            param = request.getParameter("param");
            invoiceId = request.getParameter("invoiceId");
            

            request.setAttribute("invoiceId", invoiceId);
           

//            int status = 0;
//            String cancelStatus = request.getParameter("cancelStatus");
//            String billingNo = request.getParameter("billingNo");
//
//            if (cancelStatus != null) {
//                status = billingBP.updateCancel(billingNo, userId);
//                System.out.println("status" + status);
//                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, billingNo + " billingNo  cancelled Successfully....");
//            }
//
//            ArrayList customerList = new ArrayList();
//            customerList = operationBP.getTripCustomerList();
//            request.setAttribute("customerList", customerList);
//            System.out.println("customerList Size Is :::" + customerList.size());

            ArrayList viewWorkOrderSubmission = new ArrayList();
            //closedBillList = operationBP.getClosedBilledList(customerId, fromDate, toDate);
            viewWorkOrderSubmission = billingBP.viewWorkOrderSubmission(invoiceId);
            request.setAttribute("viewWorkOrderSubmission", viewWorkOrderSubmission);
            System.out.println("viewWorkOrderSubmission Size Is :::" + viewWorkOrderSubmission.size());

            
                path = "content/billing/viewWorkOrderSubmission.jsp";
           

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
   
   
    public ModelAndView saveWorkOrderDetailsForSubmit(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        int userId = 0;
        HttpSession session = null;
        billingCommand = command;
        String menuPath = "";
        BillingTO billingTO = null;
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  View Bill For Submission";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String invoiceId = "";
        String param = "";
        String workOrderNumber = "";
       
        try {
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            param = request.getParameter("param");
            invoiceId = request.getParameter("invoiceId");
            workOrderNumber =request.getParameter("workOrderNumber");
            

            request.setAttribute("invoiceId", invoiceId);
            int insertStatus=0;
 insertStatus = billingBP.insertWorkOrderDetails(invoiceId,workOrderNumber);
                    
       
   ArrayList viewWorkOrderSubmission = new ArrayList();
            
            viewWorkOrderSubmission = billingBP.viewWorkOrderSubmission(invoiceId);
            request.setAttribute("viewWorkOrderSubmission", viewWorkOrderSubmission);
            System.out.println("viewWorkOrderSubmission Size Is :::" + viewWorkOrderSubmission.size());
            
              path = "content/billing/viewWorkOrderSubmission.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    
     public ModelAndView viewWorkOrderTwoDetailsForSubmit(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        int userId = 0;
        HttpSession session = null;
        billingCommand = command;
        String menuPath = "";
        BillingTO billingTO = null;
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  View Bill For Submission";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String invoiceId = "";
        String param = "";
       
        try {
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            param = request.getParameter("param");
            invoiceId = request.getParameter("invoiceId");
            

            request.setAttribute("invoiceId", invoiceId);
           



            ArrayList viewWorkOrderTwoSubmission = new ArrayList();
            viewWorkOrderTwoSubmission = billingBP.viewWorkOrderTwoSubmission(invoiceId);
            request.setAttribute("viewWorkOrderTwoSubmission", viewWorkOrderTwoSubmission);
            System.out.println("viewWorkOrderTwoSubmission Size Is :::" + viewWorkOrderTwoSubmission.size());

            
                path = "content/billing/viewWorkOrderTwoSubmission.jsp";
           

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
   
     
    
     
       public ModelAndView saveWorkOrderTwoDetailsForSubmit(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        int userId = 0;
        HttpSession session = null;
        billingCommand = command;
        String menuPath = "";
        BillingTO billingTO = null;
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  View Bill For Submission";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String invoiceId = "";
        String param = "";
        String workOrderNumber = "";
       
        try {
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            param = request.getParameter("param");
            invoiceId = request.getParameter("invoiceId");
            workOrderNumber =request.getParameter("workOrderNumber");
            

            request.setAttribute("invoiceId", invoiceId);
            int insertStatus=0;
            insertStatus = billingBP.insertWorkOrderTwoDetails(invoiceId,workOrderNumber);
                  
      
             ArrayList viewWorkOrderTwoSubmission = new ArrayList();
            viewWorkOrderTwoSubmission = billingBP.viewWorkOrderTwoSubmission(invoiceId);
            request.setAttribute("viewWorkOrderTwoSubmission", viewWorkOrderTwoSubmission);
            System.out.println("viewWorkOrderTwoSubmission Size Is :::" + viewWorkOrderTwoSubmission.size());

            
                path = "content/billing/viewWorkOrderTwoSubmission.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
}
