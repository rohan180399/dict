package ets.cewolf.beans;

import java.awt.Color;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.chart.plot.CompassPlot;
import org.jfree.data.general.DefaultValueDataset;
import org.jfree.data.general.ValueDataset;
import de.laures.cewolf.DatasetProduceException;
import de.laures.cewolf.DatasetProducer;


public class MeterBean implements DatasetProducer, Serializable {

	    /**
	 *
	 */
		private static final long serialVersionUID = 1L;
		private static final Log log = LogFactory.getLog(MeterBean.class);

	     // These values would normally not be hard coded but produced by
	    // some kind of data source like a database or a file


		/**
		 *  Produces some random data.
		 */
	    public Object produceDataset(Map params) throws DatasetProduceException {
	    	log.debug("producing data.");

	    	  // Dataset can either be populated by hard-coded or else from a datasource

	    	final ValueDataset dataset = new DefaultValueDataset(new Double(45.0));
	    	CompassPlot plot = new CompassPlot(dataset);
	        plot.setSeriesNeedle(7);
	        plot.setSeriesPaint(0, Color.red);
	        plot.setSeriesOutlinePaint(0, Color.red);

	        return dataset;


	    }

	    /**
	     * This producer's data is invalidated after 5 seconds. By this method the
	     * producer can influence Cewolf's caching behaviour the way it wants to.
	     */
		public boolean hasExpired(Map params, Date since) {
	        log.debug(getClass().getName() + "hasExpired()");
			return (System.currentTimeMillis() - since.getTime())  > 5000;
		}

		/**
		 * Returns a unique ID for this DatasetProducer
		 */
		public String getProducerId() {
			return "PageViewCountData DatasetProducer";
		}


		/**
		 * @see java.lang.Object#finalize()
		 */
		protected void finalize() throws Throwable {
			super.finalize();
			log.debug(this + " finalized.");
		}


}





