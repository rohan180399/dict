<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<!--<title>PAPL</title>-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->

<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });


</script>

        
        <script>
            function submitPag(){
                document.mpr.dat1.value = dateFormat(document.mpr.dat1);
                document.mpr.dat2.value = dateFormat(document.mpr.dat2);
                //alert(document.mpr.dat1.value);
                document.mpr.action="/throttle/mprApprovalList.do";
                document.mpr.submit();
            }            
            
            function viewPO(ind)
            {
                //alert("am herere:"+ind);
                var poId = document.getElementsByName('poIds');
                //alert("am herere:"+poId[ind].value);
                //document.mpr.action="/throttle/handlePoDetail.do?poId="+poId[ind].value;
                document.mpr.action="/throttle/handlePoDetail.do?poId="+poId[ind].value;
                //alert("am herere:"+document.mpr.action);
                document.mpr.submit();                
            }    
            
            function cancelPO(ind)
            {
                var poId = document.getElementsByName('poIds');
                if(confirm("Please Ensure to Cancel Purchase Order = "+poId[ind].value,'parveen' )){
                document.mpr.action="/throttle/handleCancelPo.do?poId="+poId[ind].value;
                document.mpr.submit();                
                }
            }    
            
            function setDates()
            {
             if('<%= request.getAttribute("fromDate") %>' != 'null' ){
                 document.mpr.fromDate.value = '<%= request.getAttribute("fromDate") %>' ;
             }
             if('<%= request.getAttribute("toDate") %>' != 'null' ){
                 document.mpr.toDate.value = '<%= request.getAttribute("toDate") %>' ;
             }                
            }
            
            function getData(){
                if(document.mpr.toDate.value==''){
                    alert("Please Enter From Date");
                }else if(document.mpr.fromDate.value==''){
                    alert("Please Enter to Date");
                }                 
                document.mpr.action = "/throttle/storesMprList.do";
                document.mpr.submit();                
            }    
            
            
        </script>
        
             
             <script>
   function changePageLanguage(langSelection){
   if(langSelection== 'ar'){
   document.getElementById("pAlign").style.direction="rtl";
   }else if(langSelection== 'en'){
   document.getElementById("pAlign").style.direction="ltr";
   }
   }
 </script>

             <div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.MPR"  text="MPR"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="stores.label.stores"  text="Stores"/></a></li>
          <li class="active"><spring:message code="stores.label.MPR"  text="MPR"/></li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">
<body onLoad="setDates();">
        
             
        <form name="mpr"  method="post" >                    
<!--            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns">
                        <a href="" class="panel-close">&times;</a>
                        <a href="" class="minimize">&minus;</a>
                    </div> panel-btns 
                    <h5 class="panel-title">Search Filters</h5>
                </div>
            <div class="panel-body">
          -->
            <!-- message table -->           
            <%@ include file="/content/common/message.jsp" %>    


            <table class="table table-info mb30 table-hover">
                <thead>
                    <tr>
                        <th colspan="4"><spring:message code="stores.label.MPR"  text="MPR"/></th>
                    </tr>
                </thead>
                <tr>
                    <td ><spring:message code="stores.label.FromDate"  text="default text"/>
                    </td>
                    <td ><input readonly name="fromDate" style="width:260px;height:40px;" class="form-control pull-right datepicker" type="text" value="" size="20">
<!--                    <span class="text2"><img src="/throttle/images/cal.gif" name="Calendar"  readonly  onclick="displayCalendar(document.mpr.fromDate,'dd-mm-yyyy',this)"/></span></td>-->

                    <td ><spring:message code="stores.label.TODate"  text="default text"/></td>
                    <td ><input name="toDate" style="width:260px;height:40px;" class="form-control pull-right datepicker"  type="text" value="" size="20">
<!--                    <span class="text2"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.mpr.toDate,'dd-mm-yyyy',this)"/></span></td>-->
                </tr>
                <tr align="center">
                    <!--<td></td>-->
                    <!--<td></td>-->
                    <td colspan="4" > <input type="button" class="btn btn-success" value="<spring:message code="stores.label.SEARCH"  text="default text"/>" name="search" onClick="getData();"> </td>
                    <!--<td></td>-->
                
                </tr>    
            </table>
             <br>
    
            <table class="table table-info mb30 table-hover" id="table">
            <thead>
               
                <tr>
                    <th><spring:message code="stores.label.Sno"  text="default text"/></th>
                    <th><spring:message code="stores.label.mpr/lprNo"  text="default text"/></th>
                    <th><spring:message code="stores.label.PONo"  text="default text"/></th>
                    <th><spring:message code="stores.label.VendorName"  text="default text"/></th>
                    <th><spring:message code="stores.label.RequestedDate"  text="default text"/></th>
                    <th><spring:message code="stores.label.Status"  text="default text"/></th>
                    <th><spring:message code="stores.label.ApprovedBy"  text="default text"/></th>
                    <th><spring:message code="stores.label.Remarks"  text="default text"/></th>
                    <th><spring:message code="stores.label.GeneratePO"  text="default text"/></th>
                    <th><spring:message code="stores.label.View"  text="default text"/></th>
                    <th><spring:message code="stores.label.Cancel"  text="default text"/></th>
                </tr>
            </thead>
                <% int index = 0;
            String classText = "";
            int oddEven = 0;
                %>
                <c:if test = "${mprList != null}" >
                    <c:forEach items="${mprList}" var="mpr"> 
                        <%

            oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr>
                            <td > <%= index+1 %></td>
                            <td ><c:out value="${mpr.mprId}"/></td>
                            <td >
                            <c:if test="${mpr.poId!='0'}" >
                            <c:out value="${mpr.poId}"/>
                            </c:if>
                            <c:if test="${mpr.poId=='0'}" >
                            <spring:message code="stores.label.NotAvailable"  text="default text"/>
                            </c:if>
                            </td>
                            <td ><c:out value="${mpr.vendorName}"/></td>                       
                            <td ><c:out value="${mpr.mprDate}"/></td>
                            <td ><c:out value="${mpr.status}"/></td>                                                                        
                            <td ><c:out value="${mpr.approvedBy}"/></td>
                            <td ><c:out value="${mpr.desc}"/></td> 
                            <c:if test="${mpr.status=='APPROVED' && mpr.poId!='0'}" >
                                <td > <span class="label label-primary">Completed</span> </td>
                            </c:if>                            
                            <c:if test="${mpr.status=='APPROVED' && mpr.poId=='0'}" >
                            <td >
                                <span class="label label-success">
                                    <a href="/throttle/generatePoTaxDetails.do?mprId=<c:out value='${mpr.mprId}'/>" > <font color="white"><spring:message code="stores.label.GeneratePO"  text="default text"/> </font></a>
                                </span>
                                </td>
                            </c:if>
                            <c:if test="${mpr.status=='REJECTED'}" >
                            <td ><span class="label label-danger"><spring:message code="stores.label.Rejected"  text="default text"/></span></td>
                            </c:if>
                            <c:if test="${mpr.status=='PENDING'}" >
                                <td ><span class="label label-warning"><a href="" disabled > <font color="white"><spring:message code="stores.label.Pending"  text="default text"/>  </font></a> </span></td>
                            </c:if>
                            
                            
                            <c:if test="${mpr.poId=='0'}" >
                                <td > <span class="label label-info"><a href="" disabled > <font color="white"><spring:message code="stores.label.View"  text="default text"/> </a> </span></td>
                                <td > <span class="label label-warning"><a href="" disabled onClick="cancelPO('<%= index %>')" > <font color="white"><spring:message code="stores.label.Cancel"  text="default text"/> </font></a> </span></td>
                                <input type="hidden" name="poIds" value='<c:out value="${mpr.poId}"/>' > 
                            </c:if>
                            <c:if test="${mpr.poId!='0'}" >
                                <td > <span class="label label-info"><a href="" onClick="viewPO('<%= index %>')" > <font color="white"><spring:message code="stores.label.View"  text="default text"/> </font></a> </span></td>
                                <td > <span class="label label-warning"><a href="" onClick="cancelPO('<%= index %>')" > <font color="white"><spring:message code="stores.label.Cancel"  text="default text"/> </font></a> </span></td>
                                <input type="hidden" name="poIds" value='<c:out value="${mpr.poId}"/>' > 
                            </c:if>
                                                                                     
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach>
                </c:if>                  
            </table>                        
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
 </div>
    </div>
    </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>




