<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>FusionCharts Free Documentation</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="ets.domain.report.business.ReportTO" %>
        <%@ page import="java.util.*" %>
        <SCRIPT LANGUAGE="Javascript" SRC="/throttle/js/FusionCharts.js"></SCRIPT>
    </head>
    <script language="javascript">
function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}


        function show(){
        
            document.getElementById("table1").style.visibility="visible";
        }

        function submitSearch()
        {
            if(isEmpty(document.rcExpense.fromDate.value))
                {
                    alert("Select From Date");
                    document.rcExpense.fromDate.focus();
                    return false;
                }
           else if(isEmpty(document.rcExpense.toDate.value))
                {
                    alert("Select End Date");
                    document.rcExpense.toDate.focus();
                    return false;
                }
               
                else
                    {
                        document.rcExpense.action='/throttle/rcExpenseGraphData.do';
                        document.rcExpense.submit();
                    }
        }
          Date.fromDDMMYYYY = function (s) {return (/^(\d\d?)\D(\d\d?)\D(\d{4})$/).test(s) ? new Date(RegExp.$3, RegExp.$2 - 1, RegExp.$1) : new Date (s)}
            
            function StartDateCheck(field){
                var val = field.value;
                
                var startDate = Date.fromDDMMYYYY (val);
                var currentDate = new Date();
                currentDate.setHours(0);
                currentDate.setMinutes(0);
                currentDate.setSeconds(0);
                currentDate.setMilliseconds(0);

                if (startDate < currentDate){
                    alert("Start date should current date or feature date!!");
                    field.value="";
                    field.focus();
                }
            }

function setValues(){
    if('<%= request.getAttribute("fromDate") %>' != 'null'){
        document.rcExpense.fromDate.value='<%= request.getAttribute("fromDate") %>';
    }
    if('<%= request.getAttribute("toDate") %>' != 'null'){
        document.rcExpense.toDate.value='<%= request.getAttribute("toDate") %>';
    }
    if('<%= request.getAttribute("vendorId") %>' != 'null'){
        document.rcExpense.vendorId.value='<%= request.getAttribute("vendorId") %>';
    }
}


<%--onchange="StartDateCheck(this);"--%>

    </script>   
<body onload="setValues();">
             <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
        <form name="rcExpense">
            <c:if  test="${rcVendorList!=null}">

<table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
<tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
</h2></td>
<td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
</tr>
<tr id="exp_table" >
<td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
    <div class="tabs" align="left" style="width:850;">
<ul class="tabNavigation">
        <li style="background:#76b3f1">RC Expenses</li>
</ul>
<div id="first">
<table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
<tr>
        <td height="20"><font color="red">*</font>Start Date </td>
        <td height="20"><input type="text"  name="fromDate"   readonly class="form-control"/>
            <img src="/throttle/images/cal.gif" name="Calendar"   onClick="displayCalendar(document.rcExpense.fromDate,'dd-mm-yyyy',this)"/></td>
        <td height="20"><font color="red">*</font>End Date </td>
        <td height="20"><input type="text" name="toDate" readonly   class="form-control" onchange="StartDateCheck(this);"/>
            <img src="/throttle/images/cal.gif" name="Calendar" onClick="displayCalendar(document.rcExpense.toDate,'dd-mm-yyyy',this)"/></td>

        <td height="20">RC-Vendor Name</td>
        <td height="20"><select style="width:150px" name="vendorId" class="form-control">
                <option   value="0">--Select--</option>
                <c:forEach items="${rcVendorList}" var="vendr">
                    <option value='<c:out value="${vendr.vendorId}"/>'><c:out value="${vendr.vendorName}"/></option>
                </c:forEach>
            </select>
        </td>
        <td height="20"><input type="button" class="button" name="search" value="search" onclick="submitSearch()"></td>

    </tr>
</table>
</div></div>
</td>
</tr>
</table>


                
                
            </c:if>
            <br/>
 <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            <br/><br/><br/><br/>
<%
if(request.getAttribute("rcExpenseSummary") != null){

ArrayList rcExpenseSummary = (ArrayList) request.getAttribute("rcExpenseSummary");
System.out.println("rcExpenseSummary"+rcExpenseSummary.size());

int siz = rcExpenseSummary.size();
String[][] arrData = new String[siz][2];

int i1 = 0;

Iterator itr = rcExpenseSummary.iterator();
ReportTO reportTO = new ReportTO();
while(itr.hasNext()){
reportTO = (ReportTO) itr.next();
arrData[i1][0] = reportTO.getMonthName();
arrData[i1][1] = reportTO.getReportAmount();

System.out.println("arrData----"+arrData[i1][0]+"-"+arrData[i1][1]);
i1++;
}



String  strXML = "<graph caption='RC Expense Report' XAxisName='Month' showAlternateHGridColor='1' AlternateHGridColor='ff5904' divLineColor='ff5904' divLineAlpha='20' alternateHGridAlpha='5' formatNumberScale='0' decimalPrecision='0'>";

//Initialize <categories> element - necessary to generate a stacked chart
String strCategories = "<categories>";

//Initiate <dataset> elements
String strDataProdA = "<dataset seriesName='' color='FF0000' showValues='0'>";
String strDataProdB = "<dataset seriesName='' color='8BBA00' showValues='0' parentYAxis='S' >";


//Iterate through the data
for(int i=0;i<arrData.length;i++){
//Append <category name='...' /> to strCategories
strCategories += "<category name='" + arrData[i][0] + "' />";
//Add <set value='...' /> to both the datasets
strDataProdA += "<set value='" + arrData[i][1] + "' />";
strDataProdB += "<set value='" + arrData[i][1] + "' />";

}

//Close <categories> element
strCategories += "</categories>";

//Close <dataset> elements
strDataProdA += "</dataset>";
strDataProdB += "</dataset>";

//Assemble the entire XML now
strXML += strCategories + strDataProdA + strDataProdB + "</graph>";
System.out.println("strXML-->"+strXML);

%>
            <p>
            <table align="center" style="margin-left:10px;" >
            <tr>
                <td >
                 <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                <jsp:param name="chartSWF" value="/throttle/swf/FCF_MSColumn3DLineDY.swf" />
                <jsp:param name="strURL" value="" />
                <jsp:param name="strXML" value="<%=strXML %>" />
                <jsp:param name="chartId" value="productSales" />
                <jsp:param name="chartWidth" value="800" />
                <jsp:param name="chartHeight" value="450" />
                <jsp:param name="debugMode" value="false" />
                <jsp:param name="registerWithJS" value="false" />
        </jsp:include>
                </td>
            </tr>

        </table>


    </p>
    <%
}
%>
       
    </body>
</html>
