

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
    </head>
    <script>
    function show_src() {
        document.getElementById('exp_table').style.display='none';
    }
    function show_exp() {
        document.getElementById('exp_table').style.display='block';
    }
    function show_close() {
        document.getElementById('exp_table').style.display='none';
    }



        function submitPage(){
            var chek = validation1();
            if(chek== 'pass' ){
                document.receivedStock.action='/throttle/handleVendorPaymentDetails.do';
                document.receivedStock.submit();
            }
        }
        function addPayment(){
            var chek = validation();            
            if(chek=='pass'){
                if(confirm("Sure to submit","parveen")) {
                document.receivedStock.action='/throttle/handleAddVendorPayments.do';
                document.receivedStock.submit();
                }
            }
        }

        function validation1()
        {
            if( document.receivedStock.vendorId.value == '0' ){
                alert("Please select vendor name");
                document.receivedStock.vendorId.focus();
                return 'fail';
            }else if( document.receivedStock.fromDate.value == '' ){
                alert("Please enter from date");
                return 'fail';
            }else if( document.receivedStock.toDate.value == '' ){
                alert("Please enter to date");
                return 'fail';
            }
            return 'pass';
        }

        function validation(){
            var selectedIndex = document.getElementsByName("selectedInd");
            var invoiceAmnt = document.getElementsByName("invoiceAmounts");
            var paidAmnt = document.getElementsByName("paidAmounts");
            var paidDate = document.getElementsByName("payDates");
            var remarks = document.getElementsByName("remarkss");
            var cntr =0;
            for(var i=0; i<selectedIndex.length; i++){
                if(selectedIndex[i].checked == 1){
                    if( paidAmnt[i].value == '' ){
                        alert("Please enter paid amount");
                        paidAmnt[i].focus();
                        return 'fail';
                    }
                    if( isFloat(paidAmnt[i].value) ){
                        alert("Please enter valid paid amount");
                        paidAmnt[i].select();
                        paidAmnt[i].focus();
                        return 'fail';
                    }
                    if( parseFloat(invoiceAmnt[i].value) < parseFloat(paidAmnt[i].value) ){
                        alert("Pay amount should not exceed invoice amount");
                        paidAmnt[i].focus();
                        paidAmnt[i].select();
                        return 'fail';
                    }
                    if( trim(remarks[i].value) == '' ){
                        alert("Please enter Remarks");
                        remarks[i].focus();
                        return 'fail';
                    }
                    if( paidDate[i].value == '' ){
                        alert("Please enter paid date");
                        return 'fail';
                    }
                cntr++;
                }
            }
            
            if( parseInt(cntr) == 0 )  {
                alert("Please select any grn and proceed");
                return 'fail';
            }
            return 'pass';
        }





        function newWindow(indx){
            var supplyId=document.getElementsByName("grnIds");
            window.open('/throttle/viewGRN.do?supplyId='+supplyId[indx].value, 'PopupPage', 'height=450,width=700,scrollbars=yes,resizable=yes');
        }


        function detail(indx){
            reqId=document.getElementsByName("grnIds");
            var url = '/throttle/handleRcBillDetail.do?billNo='+reqId[indx].value;
            window.open(url , 'PopupPage', 'height=450,width=650,scrollbars=yes,resizable=yes');
        }


        function setValues(){
            if('<%=request.getAttribute("fromDate")%>'!='null'){
                document.receivedStock.fromDate.value='<%=request.getAttribute("fromDate")%>';
            }
            if('<%=request.getAttribute("toDate")%>'!='null'){
                document.receivedStock.toDate.value='<%=request.getAttribute("toDate")%>';
            }
            if('<%=request.getAttribute("vendorId")%>'!='null'){
                document.receivedStock.vendorId.value='<%=request.getAttribute("vendorId")%>';
            }

        }
            
            
        function selectBill(val){
            var selectedInd = document.getElementsByName("selectedInd");
            selectedInd[val].checked = 1;
        }

        function paidDate(ind)
        {
            var dat = document.getElementsByName("payDates");
            selectBill(ind);
            return dat[ind];
        }
            
            
    </script>
    <body onload="setValues();">
        <form name="receivedStock" method="post">
            <%@ include file="/content/common/path.jsp" %>           
            <%@ include file="/content/common/message.jsp" %>

            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:750;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">Vendor Payments</li>
    </ul>
    <div id="first">
    <table width="800" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
    <tr>
    <td><font color="red">*</font>Vendor</td><td><select name="vendorId" class="form-control" style="width:125px">
                            <option value="0">-select-</option>
                            <c:if test = "${vendorList != null}" >
                                <c:forEach items="${vendorList}" var="vendor">
                                    <option value='<c:out value="${vendor.vendorId}"/>' > <c:out value="${vendor.vendorName}"/> </option>
                                </c:forEach>
                            </c:if>
                        </select></td>
    <td  height="30" class="text1"><font color="red">*</font>From Date</td>
                    <td  class="text1"><input name="fromDate" type="text" class="form-control" value="" size="20">
                        <img src="/throttle/images/cal.gif" name="Calendar"  onClick="displayCalendar(document.receivedStock.fromDate,'dd-mm-yyyy',this)"/></td>
                    <td  class="text1"><font color="red">*</font>To Date</td>
                    <td  height="30" class="text1">
                        <input name="toDate" type="text" class="form-control" value="" size="20">
                        <img src="/throttle/images/cal.gif" name="Calendar"  onClick="displayCalendar(document.receivedStock.toDate,'dd-mm-yyyy',this)"/></td>

    <td><input type="button" name="" value="Search" class="button" height="30" onclick="submitPage();" > </td>
    </tr>
    </table>
    </div></div>
    </td>
    </tr>
    </table>

            <c:set var="Amount" value="0" />
            <c:if test = "${invoiceList != null}" >
                <table width="900" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" height="30" class="border">
                    <!--DWLayoutTable-->
                    <tr>
                        <td class="contenthead" height="30" colspan="11" align="center"  >Vendor Payments </td>
                    </tr>
                    <tr>
                        <td height="30"  class="contentsub" >Sno</td>
                        <td height="30"  class="contentsub" >Invoice Date</td>
                        <td height="30"  class="contentsub" >GRN No</td>
                        <td  height="30"  class="contentsub" >Invoice  No</td>
                        <td height="30"  class="contentsub" >Order Type</td>
                        <td height="30"  class="contentsub" >Vendor Name </td>
                        <td height="30"  class="contentsub" >Invoice Amount</td>
                        <td  height="30"  class="contentsub" >Pay Date</td>
                        <td height="30"  class="contentsub" >Amount Paid </td>
                        <td  height="30"  class="contentsub" >Remarks</td>
                        <td colspan="3"  class="contentsub" >&nbsp;</td>
                    </tr>
                    <% int index = 0;%>
                    <c:forEach items="${invoiceList}" var="vend">
                        <c:set var="vendorId1" value='<c:out value="${vend.vendorId}"/>' />
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>	

                        <tr>
                            <td class="<%=classText%>" height="30" > <%= index + 1%> </td>
                            <td class="<%=classText%>" height="30" ><c:out value="${vend.inVoiceDate}"/></td>
                            <td class="<%=classText%>" height="30" >
                                <c:choose>
                                    <c:when test="${vend.orderType=='PURCHASE ORDER'}" >
                                    <input type="hidden" name="grnIds" value="<c:out value='${vend.supplyId}'/>"><a href="" onClick="newWindow(<%=index%>);"><c:out value="${vend.supplyId}"/></a>
                                    </c:when>
                                    <c:otherwise>
                                    <input type="hidden" name="grnIds" value="<c:out value='${vend.supplyId}'/>"><a href="" onClick="detail(<%=index%>);"><c:out value="${vend.supplyId}"/></a>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <td class="<%=classText%>" height="30" > 
                            <c:out value="${vend.inVoiceNumber}"/> </td>
                            <td class="<%=classText%>" height="30" > <input type="hidden" name="orderTypes" value='<c:out value="${vend.orderType}" />'  > <c:out value="${vend.orderType}"/></td>
                            <td class="<%=classText%>" height="30" ><c:out value="${vend.vendorName}"/></td>
                            <td class="<%=classText%>" height="30" ><input type="hidden" class="form-control" name="invoiceAmounts" value='<c:out value="${vend.inVoiceAmount}"/>' ><c:out value="${vend.inVoiceAmount}"/> </td>
                            <td class="<%=classText%>" height="30" width="100" >
                                <input type="text" size="9" name="payDates" readonly class="form-control" value='<c:out value="${vend.payDate}"/>' >
                            <img src="/throttle/images/cal.gif" name="Calendar"  onClick="displayCalendar(paidDate('<%= index%>'),'dd-mm-yyyy',this)"/>
                            </td>
                            <td class="<%=classText%>" height="30" ><input type="text" name="paidAmounts" class="form-control" value='<c:out value="${vend.paidAmnt}"/>' > </td>
                            <td class="<%=classText%>" height="30" ><input type="text" name="remarkss" class="form-control"  value='<c:out value="${vend.remarks}"/>'  > </td>
                            <td class="<%=classText%>" height="30" > 
                                <input type="checkbox" name="selectedInd" value='<%= index%>' >
                                <input type="hidden" name="paymentIds" value='<c:out value="${vend.paymentId}"/>' >
                            </td>
                        </tr>
                        <% index++;%>
                    </c:forEach>

                </table>
                    <input type="hidden" name="vendorId1" value='<c:out value="vendorId1" />' >
        <br>
        <center> <input type="button" class="button" name="save" value="save" onClick="addPayment();" > </center>
        </c:if>
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
