<%-- 
    Document   : ProfitAndLossReport
    Created on : Aug 28, 2012, 8:09:20 PM
    Author     : admin
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page contentType="text/html" import="java.sql.*,java.text.DecimalFormat,java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Profit & Loss Report</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>


        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {

                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });
        </script>
        <script language="javascript">

            function show_src() {
                document.getElementById('exp_table').style.display='none';
            }
            function show_exp() {
                document.getElementById('exp_table').style.display='block';
            }
            function show_close() {
                document.getElementById('exp_table').style.display='none';
            }
            function submitpage(){
                var errStr = "";
                if(document.getElementById("tripFromDate").value == "") {
                    errStr = errStr+"From Date is not filled\n"
                }
                if(document.getElementById("tripToDate").value == "") {
                    errStr = errStr+"To Date is not filled\n"
                }
                if(errStr == "") {
                    document.viewTripSheet.action="ProfitAndLossReport.jsp";
                    document.viewTripSheet.method = "post";
                    document.viewTripSheet.submit();
                } else {
                    alert(errStr);
                }
            }
        </script>
        <style>
            .text1{
                border-right: 1px solid #666666;
            }
            .text2{
                border-right: 1px solid #666666;
            }
            .contenthead{
                border-right: 1px solid #666666;
                border-bottom: 1px solid #666666;


            }
        </style>
    </head>
    <body>

        <form name="viewTripSheet" >

            <%!    DecimalFormat df2 = new DecimalFormat("#0.00");

                double getFleetExpenses(Statement stm, String vehicleNo, String fromDate, String toDate) {
                    double fleetExpenses = 0;
                    try {
                        System.out.println("------>select c.total_amount  as fleetExp "
                                + "from papl_vehicle_master a, papl_direct_jobcard b, papl_jobcard_bill_master c, "
                                + "papl_vehicle_reg_no d where a.vehicle_id = b.vehicle_id and  "
                                + "a.vehicle_id = d.vehicle_id and d.active_ind='Y' and b.job_card_id = c.job_card_id  "
                                + " and c.Created_On  >= '" + fromDate + "'"
                                + " and c.Created_On  <= '" + toDate + "'"
                                + " and d.reg_no in ('" + vehicleNo + "')");
                        ResultSet res = stm.executeQuery("select c.total_amount  as fleetExp "
                                + "from papl_vehicle_master a, papl_direct_jobcard b, papl_jobcard_bill_master c, "
                                + "papl_vehicle_reg_no d where a.vehicle_id = b.vehicle_id and  "
                                + "a.vehicle_id = d.vehicle_id and d.active_ind='Y' and b.job_card_id = c.job_card_id  "
                                + " and c.Created_On  >= '" + fromDate + "'"
                                + " and c.Created_On  <= '" + toDate + "'"
                                + " and d.reg_no in ('" + vehicleNo + "')");
                        while (res.next()) {
                            fleetExpenses = res.getDouble("fleetExp");
                        }
                    } catch (Exception e) {
                        System.out.println("Exception in getting Fleet Amount" + e.getMessage());
                    }
                    return fleetExpenses;
                }
                String tripExpenses(Statement stm, String vehicleId, String fromDate, String toDate) {
                    String returnData = "0~0~0~0~0~0~0~0";
                    double fuel = 0, toll = 0, bata = 0, others = 0,coolie = 0,mamool =0,IcExpenses =0,LoadUnload=0;
                    try {
                        String qry1 = "SELECT stageid as typ, sum(amount) as amt FROM trip_expenses_details ed,ra_trip_open_close_epos ts"
                                + " WHERE ts.tripid = ed.tripsheetid AND ts.vehicleid = '" + vehicleId + "' AND "
                                + "ts.tripdate >= '" + fromDate + "' AND ts.tripdate <= '" + toDate + "' AND active_ind = 'Y' group by stageid UNION "
                                + "SELECT 'Fuel' as typ,sum(totalamount) as amt  FROM trip_expenses_details ed,ra_trip_open_close_epos ts WHERE "
                                + "ts.tripid = ed.tripsheetid AND ts.vehicleid = '" + vehicleId + "' AND "
                                + "ts.tripdate >= '" + fromDate + "' AND ts.tripdate <= '" + toDate + "'  AND active_ind = 'Y'";
                        System.out.println("qry1::::::::::::---->" + qry1);
                        ResultSet res = stm.executeQuery(qry1);
                        while (res.next()) {
                            if (res.getString("typ").equalsIgnoreCase("Diesel Expenses")) {
                                fuel = res.getDouble("amt");
                            }
                            if (res.getString("typ").equalsIgnoreCase("Drivar Batta")) {
                                bata = res.getDouble("amt");
                            }
                            if (res.getString("typ").equalsIgnoreCase("Toll Expenses")) {
                                toll = res.getDouble("amt");
                            }
                            if (res.getString("typ").equalsIgnoreCase("other") || res.getString("typ").equalsIgnoreCase("others")) {
                                others = res.getDouble("amt");
                            }
                            if (res.getString("typ").equalsIgnoreCase("Coolie")) {
                                coolie = res.getDouble("amt");
                            }
                            if (res.getString("typ").equalsIgnoreCase("Mamool")) {
                                mamool = res.getDouble("amt");
                            }
                            if (res.getString("typ").equalsIgnoreCase("IC Expenses")) {
                                IcExpenses = res.getDouble("amt");
                            }
                            if (res.getString("typ").equalsIgnoreCase("Loading & Unloading")) {
                                LoadUnload = res.getDouble("amt");
                            }
                        }
                        returnData = fuel + "~" + bata + "~" + toll + "~" + others +"~"+ coolie+"~"+ mamool+"~"+ IcExpenses+"~"+ LoadUnload;
                    } catch (Exception e) {
                        //  System.out.println("Exception in getting Fleet Amount"+e.getMessage());
                    }
                    return returnData;

                }


            %>


            <%
                        Class.forName("com.mysql.jdbc.Driver").newInstance();
                        Connection connThtottle = DriverManager.getConnection("jdbc:mysql://localhost:3306/throttleclplnew1", "root", "admin");
                        Statement stm = connThtottle.createStatement();
                        Statement stmfun = connThtottle.createStatement();

                        ResultSet resVehicle = stm.executeQuery("SELECT distinct vr.vehicle_id,vr.reg_no FROM papl_vehicle_reg_no vr WHERE  vr.active_ind='Y' ORDER BY vr.reg_no");
                        System.out.println("resVehicle--->" + resVehicle);
            %>

            <table width="800" align="center" cellpadding="0" cellspacing="0" border="0" id="report" style="margin-top:0px;">

                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">P&amp;L Report</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                                    <tr>
                                        <td>Vehicle No</td><td>
                                            <select name="vehicleId"  id="vehicleId" class="form-control" style="width:120px;">
                                                <option value=""> -Select- </option>
                                                <%
                                                            while (resVehicle.next()) {
                                                %>
                                                <option value="<%=resVehicle.getString("vr.vehicle_id")%>"><%=resVehicle.getString("vr.reg_no")%></option>
                                                <%
                                                            }
                                                %>
                                            </select>
                                        </td>

                                        <td><font color="red">*</font>From Date</td><td><input name="tripFromDate" id="tripFromDate" readonly class="datepicker"  type="text" value="" size="20"></td>
                                        <td><font color="red">*</font>To Date</td><td><input name="tripToDate" id="tripToDate" readonly class="datepicker"  type="text" value="" size="20"></td>
                                        <td><input type="button" name="search" value="View Report" onClick="submitpage(this.name)" class="button" /></td>
                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>
            <%
                        String tripFromDate = request.getParameter("tripFromDate");
                        if (tripFromDate == null) {
                            tripFromDate = "";
                        }

                        String tripToDate = request.getParameter("tripToDate");
                        if (tripToDate == null) {
                            tripToDate = "";
                        }

                        String vehicleId = request.getParameter("vehicleId");
                        if (vehicleId == null) {
                            vehicleId = "";
                        }


                        String queryConcat = "";
                        if (vehicleId.trim().length() > 0) {
                            queryConcat = queryConcat + " AND  vehicleid = '" + vehicleId + "'";
                        }

                        if (tripFromDate.length() > 0 && tripToDate.length() > 0) {

            //out.println(tripFromDate);
            //out.println(tripToDate);
                            String tripFromDateSQL = tripFromDate.split("-")[2] + "-" + tripFromDate.split("-")[1] + "-" + tripFromDate.split("-")[0];
                            String tripToDateSQL = tripToDate.split("-")[2] + "-" + tripToDate.split("-")[1] + "-" + tripToDate.split("-")[0];
                            DateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                            Date startDt = (java.util.Date) sdf.parse(tripFromDate);
                            Date endDt = (java.util.Date) sdf.parse(tripToDate);

                            Calendar startCal, endCal;
                            startCal = Calendar.getInstance();
                            startCal.setTime(startDt);
                            endCal = Calendar.getInstance();
                            endCal.setTime(endDt);
                            int workDays = 0;

            //Just in case the dates were transposed this prevents infinite loop
                            if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
                                startCal.setTime(endDt);
                                endCal.setTime(startDt);
                            }
                            startCal.add(Calendar.DATE, -1);
                            do {
                                startCal.add(Calendar.DAY_OF_MONTH, 1);
                                ++workDays;
                            } while (startCal.getTimeInMillis() < endCal.getTimeInMillis());

                            System.out.println(workDays);




                            String vehicleQry = "SELECT vm.vehicle_id,vm.chassis_no,ven.engine_no,vr.reg_no, mfr.mfr_name,ml.Model_name,"
                                    + " count(*) as total_trips, sum(ts.revenue) as revenue,sum(ts.totalexpenses) as expenses,ins.premium_amount as insur,"
                                    + "fc.fc_amount,rtax.road_tax_amount as rtax FROM "
                                    + "papl_vehicle_reg_no vr, papl_mfr_master mfr, "
                                    + "papl_vehicle_engine_details ven, papl_model_master ml,papl_vehicle_master vm LEFT JOIN ra_trip_open_close_epos ts ON ts.vehicleid = vm.vehicle_id"
                                    + " LEFT JOIN papl_vehicle_insurance ins ON vm.vehicle_id = ins.vehicle_id LEFT JOIN papl_vehicle_fc_detail fc "
                                    + "ON vm.vehicle_id = fc.vehicle_id LEFT JOIN papl_vehicle_road_tax rtax ON vm.vehicle_id = rtax.vehicle_id where vm.vehicle_id = vr.vehicle_id AND ml.model_id = vm.model_id "
                                    + "AND mfr.mfr_id = vm.mfr_id AND ven.vehicle_id = vm.vehicle_id AND vm.active_ind='Y' AND vr.active_ind='Y'"
                                    + " AND mfr.active_ind='Y' AND ven.active_ind='Y' AND ml.active_ind='Y' AND ts.vehicleid = vm.vehicle_id AND ts.status = 'Close' AND vm.Ownership='1' AND ts.tripdate between str_to_date('" + tripFromDate + "','%d-%m-%Y') and str_to_date('" + tripToDate + "','%d-%m-%Y')  " + queryConcat + "  GROUP BY vm.vehicle_id";

            //out.println("vehicleQry===>"+vehicleQry);
                            ResultSet res = stm.executeQuery(vehicleQry);
            %>

            <script language="javascript">
    document.getElementById("tripFromDate").value = "<%=tripFromDate%>";
    document.getElementById("tripToDate").value = "<%=tripToDate%>";
    document.getElementById("vehicleId").value = "<%=vehicleId%>";    
            </script>



            <table width="110%" cellpadding="3" cellspacing="0" style="border: 1px solid #666666; border-bottom: 0px;">
                <tr>
                    <td rowspan="2" class="contenthead">S.No</td>
                    <td rowspan="2" class="contenthead">Vehicle No</td>
                    <td rowspan="2" class="contenthead">Vehicle Model</td>
                    <td rowspan="2" class="contenthead">MFR Name</td>
                    <td rowspan="2" class="contenthead">Trips</td>
                    <td rowspan="2" class="contenthead">Earnings</td>

                    <td colspan="3" class="contenthead" style="text-align:center">Fixed Expenses </td>
                    <td rowspan="2" class="contenthead" style="text-align:center">Per Day<br> Fixed Expenses </td>
                    <td rowspan="2" class="contenthead" style="text-align:center">Fixed Expenses <br>for the Report Period</td>
                    <td colspan="8" class="contenthead" style="text-align:center">Operation Expenses</td>
                    <td rowspan="2" class="contenthead">Maint <br>Expenses</td>
                    <td rowspan="2" class="contenthead">Nett <br>Expenses</td>
                    <td rowspan="2" class="contenthead">Profit</td>
                </tr>
                <tr>
                    <td class="contenthead">Insurance</td>
                    <td class="contenthead">Road Tax </td>
                    <td class="contenthead">FC Amount </td>
                    <td class="contenthead">Fuel</td>
                    <td class="contenthead">Toll</td>
                    <td class="contenthead">Bata</td>
                    <td class="contenthead">Coolie</td>
                    <td class="contenthead">Mamool</td>
                    <td class="contenthead"> IC </td>
                    <td class="contenthead">Loading & UnLoading</td>
                    <td class="contenthead">Others</td>
                </tr>


                <%
                    String tripExp = "0~0~0~0";
                    String vehicleIdRs = "", vehicleNo = "", vehicleModel = "", mfrName = "";
                    int sno = 0, totalTrips = 0;
                    double earnings = 0, expenses = 0, profit = 0, insurance = 0, fcAmount = 0, roadTax = 0, daysFixedExp = 0;
                    double earningsOverAll = 0, expensesOverAll = 0, fixedExpensesOverAll = 0, operationExpensesOverAll = 0, fleetExpensOverAll = 0;
                    double fuel = 0, toll = 0, bata = 0, other = 0, coolie = 0, mamool =0, IcExpenses =0, LoadUnload=0, driverSalary = 0, fleetExpens = 0;


                    double fuelOverAll = 0, tollOverAll = 0, bataOverAll = 0, otherOverAll = 0,coolieOverAll=0, driverSalaryOverAll = 0;
                    String rowClass = "text1";
                    String profitColor = "";//#00FF00";
                    while (res.next()) {
                        sno++;
                        if ((sno % 2) == 0) {
                            rowClass = "text2";
                        } else {
                            rowClass = "text1";
                        }

                        vehicleIdRs = res.getString("vm.vehicle_id");
                        vehicleNo = res.getString("vr.reg_no");
                        vehicleModel = res.getString("ml.Model_name");
                        mfrName = res.getString("mfr.mfr_name");

                        totalTrips = res.getInt("total_trips");
                        earnings = res.getDouble("revenue");
                        earningsOverAll += earnings;

                        insurance = res.getDouble("insur");
                        fcAmount = res.getDouble("fc.fc_amount");
                        roadTax = res.getDouble("rtax");


                        fleetExpens = getFleetExpenses(stmfun, vehicleNo, tripFromDateSQL, tripToDateSQL);
                        fleetExpensOverAll += fleetExpens;
                        tripExp = tripExpenses(stmfun, vehicleIdRs, tripFromDateSQL, tripToDateSQL);
                        String tripEpen[] = tripExp.split("~");

                        fuel = Double.parseDouble(tripEpen[0]);
                        bata = Double.parseDouble(tripEpen[1]);
                        toll = Double.parseDouble(tripEpen[2]);
                        other = Double.parseDouble(tripEpen[3]);
                        coolie = Double.parseDouble(tripEpen[4]);
                        mamool = Double.parseDouble(tripEpen[5]);
                        IcExpenses = Double.parseDouble(tripEpen[6]);
                        LoadUnload = Double.parseDouble(tripEpen[7]);
                        operationExpensesOverAll += (fuel + bata + toll + other + coolie+ mamool+ IcExpenses+ LoadUnload);

                        daysFixedExp = ((roadTax + fcAmount + insurance) / 365) * workDays;
                        //expenses = res.getDouble("expenses");
                        expenses = (fuel + bata + toll + other + coolie + mamool + IcExpenses + LoadUnload + daysFixedExp + fleetExpens);
                        expensesOverAll += expenses;
                        profit = earnings - expenses;
                        if (profit <= 0) {
                            profitColor = "#FF0000";
                        } else {
                            profitColor = "#207B30";
                        }
                        fixedExpensesOverAll += daysFixedExp;
                %>

                <tr>
                    <td class="<%=rowClass%>"><%=sno%></td>
                    <td class="<%=rowClass%>"><%=vehicleNo%></td>
                    <td class="<%=rowClass%>"><%=vehicleModel%></td>
                    <td class="<%=rowClass%>"><%=mfrName%></td>
                    <td class="<%=rowClass%>" align="right"><%=totalTrips%></td>
                    <td class="<%=rowClass%>" align="right"><%=df2.format(earnings)%></td>
                    <td class="<%=rowClass%>" align="right"><%=df2.format(insurance)%></td>
                    <td class="<%=rowClass%>" align="right"><%=df2.format(roadTax)%></td>
                    <td class="<%=rowClass%>" align="right"><%=df2.format(fcAmount)%></td>

                    <td class="<%=rowClass%>" align="right"><%=df2.format((roadTax + fcAmount + insurance) / 365)%></td>
                    <td class="<%=rowClass%>" align="right"><%=df2.format(((roadTax + fcAmount + insurance) / 365) * workDays)%></td>

                    <td class="<%=rowClass%>" align="right"><%=df2.format(fuel)%></td>
                    <td class="<%=rowClass%>" align="right"><%=df2.format(toll)%></td>
                    <td class="<%=rowClass%>" align="right"><%=df2.format(bata)%></td>
                    <td class="<%=rowClass%>" align="right"><%=df2.format(coolie)%></td>
                    <td class="<%=rowClass%>" align="right"><%=df2.format(mamool)%></td>
                    <td class="<%=rowClass%>" align="right"><%=df2.format(IcExpenses)%></td>
                    <td class="<%=rowClass%>" align="right"><%=df2.format(LoadUnload)%></td>
                    <td class="<%=rowClass%>" align="right"><%=df2.format(other)%></td>
                    <td class="<%=rowClass%>" align="right"><%=df2.format(fleetExpens)%></td>
                    <td class="<%=rowClass%>" align="right"><%=df2.format(expenses)%></td>
                    <td class="<%=rowClass%>" style="color:<%=profitColor%>" align="right"><%=df2.format(profit)%></td>
                </tr>


                <%
                    }

                    if (connThtottle != null) {
                        connThtottle.close();
                    }
                %>
            </table>



            <br>
            <br>
            <table width="800" cellpadding="5" cellspacing="0" style="border: 1px solid #666666; border-bottom: 0px;border-right: 0px;">
                <tr>
                    <td class="contenthead">Total Earnings</td>
                    <td class="contenthead">Total Expenses</td>
                    <td class="contenthead">Profit</td>
                    <td class="contenthead">Total Fixed Expenses</td>
                    <td class="contenthead">Total Operation Expenses</td>
                    <td class="contenthead">Total Maint Expenses</td>
                </tr>

                <tr>
                    <td class="text1" align="right"><%=df2.format(earningsOverAll)%></td>
                    <td class="text1" align="right"><%=df2.format(expensesOverAll)%></td>
                    <td class="text1" align="right"><%=df2.format(earningsOverAll - expensesOverAll)%></td>
                    <td class="text1" align="right"><%=df2.format(fixedExpensesOverAll)%></td>
                    <td class="text1" align="right"><%=df2.format(operationExpensesOverAll)%></td>
                    <td class="text1" align="right"><%=df2.format(fleetExpensOverAll)%></td>
                </tr>
            </table>

            <%
                        }
            %>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

    </body>
</html>

