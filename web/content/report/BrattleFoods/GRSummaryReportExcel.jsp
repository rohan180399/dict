<%-- 
    Document   : GRSummaryReportExcel
    Created on : Feb 15, 2016, 2:09:44 PM
    Author     : hp
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<html>
    <head>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
            <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "GRSummaryReport-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
            <c:if test = "${GRSummaryList != null}" >
                <table border="1"  align="center" width="100%" cellpadding="0" cellspacing="1" >
                    <thead>
                        <tr>
                            <th rowspan="2" >S.No</th>
                                    <th rowspan="2" >Booking Office</th>
                                    <th rowspan="2" >Empty Pick Up</th>
                                    <th rowspan="2" >Movement Type</th>
                                    <th rowspan="2" >G.R No.</th>
                                      <th rowspan="2" >Shipping Bill No.</th>
                                    <th rowspan="2" >BOE</th>
                                     <th rowspan="2" >Trip Start</th>
                                    <th rowspan="2" >Trip End</th>
                          
                                    <th rowspan="2" >Trip Code</th>
                                     <th rowspan="2" >Date of Issue</th>
                                     <th rowspan="2" >GR Submission Date</th>
                                     <th rowspan="2" >Trip Status</th>
                                     <th rowspan="2" >Trip Sheet Submit</th>
                                     <th rowspan="2" >TPT Name</th>
                                     <th rowspan="2" >Diesel Qtty.</th>
                                     <th rowspan="2" >Amount</th>
                                     <th colspan="15"  style="text-align:center">Trip Expenses</th>
                                     <th rowspan="2" >Total Other Expenses</th>
                                     <th rowspan="2" >Total Trip Expense</th>


                                    <th colspan="3"  style="text-align:center">Shipper Detail</th>
                                    <th colspan="2"  style="text-align:center">Movement </td>
                                    <th colspan="6"  style="text-align:center">Conatiner Details </th>
                                    <th colspan="6"  style="text-align:center">Revenue </th>
                                     <th rowspan="2">Total Revenue</th>
                        </tr>
                        <tr>
                                     <th  class="contenthead">cashDiseslAmount</th>
                                    <th class="contenthead" >toll</th>
                                    <th  class="contenthead">Tea/Food</th>
                                    <th class="contenthead" >R&M</th>
                                    <th class="contenthead" >weightment</th>
                                    <th class="contenthead" >octroi</th>
                                    <th class="contenthead" >bhati</th>
                                    <th class="contenthead" >Trip Expense</th>
                                    <th class="contenthead" >misc</th>
                                    <th class="contenthead" >Extra Fooding</th>
                                    <th class="contenthead" >Green Tax</th>
                                    <th  class="contenthead">Detaintion</th>
                                    <th  class="contenthead">Other Expense</th>
                                    <th class="contenthead" >Driver Batta</th>
                                    <th class="contenthead" >Dala</th>
                            <td class="contenthead">Consignee</td>
                            <td class="contenthead">Consignor </td>
                            <td class="contenthead">Billing Party </td>

                            <td class="contenthead">Origin </td>
                            <td class="contenthead">Destination </td>

                            <td class="contenthead">Conatiner No</td>
                            <td class="contenthead">20' </td>
                            <td class="contenthead">40' </td>
                            <td class="contenthead">Type L/E </td>
                            <td class="contenthead">S.liNe </td>
                            <td class="contenthead">Lorry No. </td>

                            <td class="contenthead">Bill No.</td>
                            <td class="contenthead">Freight Charges</td>
                           <th class="contenthead">Toll/GreenTax Charges</th>
                                    <th class="contenthead">Detention Charges </th>
                                    <th class="contenthead">Weightment Charges</th>
                                    <th class="contenthead">Other Charges</th>
                           

                        </tr></tr>
                    </thead>
                    <% int index = 0,sno = 1;%>
                   
                            <c:forEach items="${GRSummaryList}" var="csList">
                                 <c:set var="masterId" value="${csList.masterId}"/>
                            <c:set var="detailId" value="${csList.detailId}"/>
                                 <c:set var="routeInfo" value="${csList.origin}"/>
                                <tr>
                                              <%
String origin="";
                                        String   routeInformation = "" + (String)pageContext.getAttribute("routeInfo");
                                       String[] routes = null;
                                          routes = routeInformation.split("-");
                                        String   emptyPickup = "" + routes[0] ;
                                        int z= routes.length;
                                  if( z == 2){
                                          origin = "" + routes[0] ;

                                    }else{

                                          origin = "" + routes[1] ;
}
                                       
                                    %>

                                   <td align="center"><%=sno%></td>
                             <td align="left">DICT(Sonepat)</td>
                            <td align="left" id="td1" ><%=emptyPickup%></td>
                            <td align="left"><c:out value="${csList.movementType}"/></td>

                            <td align="left"><c:out value="${csList.grNo}"/></td>
                              <td align="left"><c:out value="${csList.shippingBillNo}"/></td>
                                    <td align="left"><c:out value="${csList.billOfEntryNo}"/></td>
                            <td align="left"><c:out value="${csList.startDate}"/></td>
                            <td align="left"><c:out value="${csList.endDate}"/></td>
                            <td align="left"><c:out value="${csList.tripSheetId}"/></td>
                            <td align="left"><c:out value="${csList.grDate}"/></td>
                             <td align="left"><c:out value=""/></td>
                            <c:if test = "${csList.activeInd == 'Y'}">
                                 
                                 <c:if test = "${masterId > detailId}">
                             <td align="left"><c:out value="${csList.statusName}"/></td>
                             </c:if>
                                 <c:if test = "${detailId > masterId}">
                             <td align="left"><c:out value="${csList.detailStatusName}"/></td>
                             </c:if>
                                 <c:if test = "${detailId == masterId}">
                             <td align="left"><c:out value="${csList.detailStatusName}"/></td>
                             </c:if>
                             
                             </c:if>
                             <c:if test = "${csList.activeInd == 'N'}">
                             <td align="left">Trip Cancelled</td>
                             </c:if>
                             <td align="left"><c:out value=""/></td>
                            <td align="left"><c:out value="${csList.transporter}"/></td>
                           
                           <td align="right"><c:out value="${csList.liters}"/></td>
                            
                            <td align="right"><c:out value="${csList.tripFuelAmount}"/></td>
                            
                            
                          


                            <td align="left"><c:out value="${csList.dieselCost}"/></td>
                            <td align="left"><c:out value="${csList.tollAmount}"/></td>
                            <td align="left"><c:out value="${csList.foodCost}"/></td>
                            <td align="left"><c:out value="${csList.rnmAdvance}"/></td>
                            <td align="left"><c:out value="${csList.weightment}"/></td>
                            <td align="left"><c:out value="${csList.octroiAmount}"/></td>
                            <td align="left"><c:out value="${csList.bhati}"/></td>
                            <td align="left"><c:out value="${csList.tripExpense}"/></td>
                            <td align="left"><c:out value="${csList.miscAmount}"/></td>
                            <td align="left"><c:out value="${csList.extraFoodingAmount}"/></td>
                            <td align="left"><c:out value="${csList.greenTaxAmount}"/></td>
                            <td align="left"><c:out value="${csList.detaintionAmount}"/></td>
                            <td align="left"><c:out value="${csList.otherExpense}"/></td>
                            <td align="left"><c:out value="${csList.driverBatta}"/></td>
                            <td align="left"><c:out value="${csList.dalaAmount}"/></td>
                         <td align="left"> <c:out value="${csList.dieselCost + csList.tollAmount+csList.foodCost+csList.rnmAdvance+csList.weightment+csList.octroiAmount+csList.bhati+csList.tripExpense+csList.miscAmount+csList.extraFoodingAmount+csList.greenTaxAmount+csList.detaintionAmount+csList.otherExpense+csList.driverBatta+csList.dalaAmount}"/> </td>
                         <td align="left"><c:out value="${csList.dieselCost + csList.tollAmount+csList.foodCost+csList.rnmAdvance+csList.weightment+csList.octroiAmount+csList.bhati+csList.tripExpense+csList.miscAmount+csList.extraFoodingAmount+csList.greenTaxAmount+csList.detaintionAmount+csList.otherExpense+csList.driverBatta+csList.dalaAmount +csList.tripFuelAmount}"/></td>
                            <td align="left"><c:out value="${csList.consigneeName}"/></td>
                            <td align="left"><c:out value="${csList.consignorName}"/></td>
                            <td align="left"><c:out value="${csList.billingParty}"/></td>
                            <td align="left">
                                <c:if test = "${csList.movementType == 'Import' || csList.movementType == 'Import DSO' }">
                                    <c:out value="${csList.destination}"/>
                                </c:if>
                                <c:if test = "${csList.movementType == 'Export' || csList.movementType == 'Export DSO' || csList.movementType == 'Repo' }">
                                 <%=origin%>
                                </c:if>
                            </td>
                            <td align="left">
                                <c:if test = "${csList.movementType == 'Import' || csList.movementType == 'Import DSO' }">
                                   <%=origin%>
                                </c:if>
                                <c:if test = "${csList.movementType == 'Export' || csList.movementType == 'Export DSO' || csList.movementType == 'Repo' }">
                                    <c:out value="${csList.destination}"/>
                                </c:if>
                            </td>
                            <%--<td align="left"><c:out value="${csList.destination}"/></td>--%>
                            <td align="left"><c:out value="${csList.containerNo}"/></td>
                            <td align="left"><c:out value="${csList.twentyFT}"/></td>
                            <td align="left"><c:out value="${csList.fortyFT}"/></td>
                            <%--<td align="left"><c:out value="${csList.containerType}"/></td>--%>
                            <td align="center">
                                <c:if test = "${csList.movementType == 'Export' || csList.movementType == 'Import'|| csList.movementType == 'Export DSO'|| csList.movementType == 'Import DSO' }" >
                                    L
                                </c:if>
                                <c:if test = "${csList.movementType == 'Repo'}">
                                    E
                                </c:if>
                            </td>
                            <td align="left"><c:out value="${csList.linerName}"/></td>
                            <td align="left"><c:out value="${csList.vehicleNo}"/></td>
                            <td align="left"><c:out value="${csList.invoiceCode}"/></td>


                            <td align="right"><c:out value="${csList.freightAmount}"/></td>
                             <td align="right"><c:out value="${csList.revenueTollAmount}"/></td>
                            <td align="right"><c:out value="${csList.revenueDetentionAmount}"/></td>
                            <td align="right"><c:out value="${csList.revenueWeightMent}"/></td>
                            <td align="right"><c:out value="${csList.revenueOtherExpense}"/></td>

                            <td align="right"><c:out value="${csList.freightAmount + csList.revenueTollAmount + csList.revenueDetentionAmount + csList.revenueWeightMent + csList.revenueOtherExpense}"/></td>
                                </tr>
                        <%
                   index++;
                     sno++;
                        %>

                    </c:forEach>

                </table>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
