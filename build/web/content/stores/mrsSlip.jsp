

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    

    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ page import="ets.domain.mrs.business.MrsTO" %> 
    <%@ page import="java.util.*" %> 
    <title>MRS SLIP</title>




</head>
<body>


<script>
    function submitPage(val){
        if(val=='approve'){
            document.mpr.status.value="APPROVED"
            document.mpr.action="/throttle/approveMpr.do"     
            document.mpr.submit();
        }else if(val=='reject'){
        document.mpr.status.value="REJECTED"                
        document.mpr.action="/throttle/approveMpr.do"                
        document.mpr.submit();
    }
}            

    
    function print(ind)
    {       
        var DocumentContainer = document.getElementById(ind);
        var WindowObject = window.open('', "TrackHistoryData", 
            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        //WindowObject.close();   
    }            


</script>

<form name="mpr"  method="post" >                        
    <br>
    

<%
int index=0;
ArrayList vehicleDetails = new ArrayList();
ArrayList itemDetails = new ArrayList();
vehicleDetails = (ArrayList) request.getAttribute("vehicleDetails");
itemDetails = (ArrayList) request.getAttribute("mrsDetails");


MrsTO purch = new MrsTO();
int listSize=10;
int itemNameLimit = 30;
int mfrCodeLimit = 10;

MrsTO headContent = new MrsTO();
System.out.println("itemDetails size in jsp="+itemDetails.size());
headContent = (MrsTO)vehicleDetails.get(0);

String itemName = "";
String mfrCode = "";


for(int i=0; i<itemDetails.size(); i=i+listSize) {
%>



<div id="print<%= i %>" >
<style type="text/css">
.header {font-family:Arial;
font-size:15px;
color:#000000;
text-align:center;
 padding-top:10px;
 font-weight:bold;
}
.border {border:1px;
border-color:#000000;
border-style:solid;
}
.text1 {
font-family:Arial, Helvetica, sans-serif;
font-size:14px;
color:#000000;
}
.text2 {
font-family:Arial, Helvetica, sans-serif;
font-size:16px;
color:#000000;
}
.text3 {
font-family:Arial, Helvetica, sans-serif;
font-size:18px;
color:#000000;
}

</style>    


<table width="700" align="center" border="0" cellpadding="0" cellspacing="0" >
<tr>
<td valign="top" height="30" align="center" class="border"><strong>MATERIAL REQUISITION SLIP</strong></td>
</tr>

<tr>
<td height="70" valign="top">
    
                <table width="700" height="60"  border="0" cellpadding="0" cellspacing="0" class="border">
                <tr>
                <Td colspan="2" height="60" width="600" class="text3" align="center">
                    <strong> GATI. </strong>
                </Td>
                <td colspan="2" height="30" class="text3"  align="right" style="padding-right:10px; " >MRS NO.&nbsp;<strong><%= headContent.getMrsNumber() %></strong>
                </td>
                </tr>

                <tr>
                <Td colspan="4" align="center" class="text2" style="padding-left:10px; ">
                Daruhera
                25583919, 26721087
                </Td>
                </tr>

                <tr>
                <Td colspan="4" align="center" class="text2" style="padding-left:10px; ">&nbsp;
                </Td>
                </tr>

                <tr>
                <Td align="left" width="170" class="text2" style="padding-left:10px; ">Jobcard No</td>
                <Td align="right" width="170"  class="text2" style="padding-right:10px; "> <strong><%= headContent.getJcMYFormatNo() %></strong> </td>
                <Td align="left" width="100"  class="text2" style="padding-left:10px; ">Date
                <Td align="right" width="240" class="text2" style="padding-right:10px; "> <strong><%= headContent.getMrsCreatedDate() %></strong></Td>
                </tr>

                <tr>
                <Td align="left" width="170" class="text2" style="padding-left:10px; ">Reg No</td>
                    <Td align="right"width="170"  class="text2" style="padding-right:10px; "> <strong><%= headContent.getMrsVehicleNumber() %> </strong></td>
                </Td>
                <Td align="left"width="170"  class="text2" style="padding-left:10px; ">Vehicle Km</td>
                <Td align="right" width="170" class="text2" style="padding-right:10px; "> <strong><%= headContent.getMrsVehicleKm() %> </strong></td>
                </tr>

                <tr>
                    <Td align="left" width="170"  class="text2" style="padding-left:10px; ">Vehicle Make</td>
                    <Td align="right" width="170"  class="text2" style="padding-right:10px; "> <strong><%= headContent.getMrsVehicleMfr() %> </strong> </td>    
                    <Td align="left"  width="170" class="text2" style="padding-left:10px; ">Mechanic</td>
                    <Td align="right" width="170"  class="text2" style="padding-right:10px; "><strong><%= headContent.getMrsTechnician() %> </strong></td>
                </tr>


                <tr>
                <Td colspan="4" align="center" class="text2" style="padding-left:10px; ">&nbsp;
                </Td>
                </tr>
                </table>

</td>
</tr>


<tr>
<Td valign="top" colspan="2">
       
        <table width="700" align="center" border="0" cellpadding="0" cellspacing="0" class="border" style="margin-bottom:25px; ">

        
        <tr>
        <Td class="text2" width="25" height="28" valign="top" class="border" style="padding-left:10px; border:1px; border-color:#000000; border-style:solid;"> <strong>S.NO</strong></Td>
        <Td class="text2" valign="top" width="90" style="padding-left:10px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>FOLIO NO</strong></Td>
        <Td class="text2"  valign="top" width="220" style="padding-left:10px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>NOMENCLATURE</strong></Td>
        <Td  class="text2" valign="top" width="60" style="padding-left:10px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>UOM</strong></Td>
        <Td class="text2"  valign="top" width="110" style="padding-left:10px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>ISSUED QTY.</strong></Td>
        </tr> 
            











<%	index = 0;
	for(int j=i; ( j < (listSize+i) ) && (j<itemDetails.size() ) ; j++){	
		purch = new MrsTO();	
		purch = (MrsTO)itemDetails.get(j);
                mfrCode = purch.getMrsPaplCode();
                itemName  = purch.getMrsItemName();
                if(purch.getMfrCode().length() > mfrCodeLimit ){
                    mfrCode = mfrCode.substring(0,mfrCodeLimit-1);
                }if(purch.getItemName().length() > itemNameLimit ){
                    itemName = itemName.substring(0,itemNameLimit-1);
                }
                System.out.println("j=="+j);		
%>       
    
                <tr>
                    <Td valign="top" HEIGHT="20" class="text1" width="25"  style="border:1px;  border-right-style:solid; border-left-style:solid;" align="right"   > <%= j+1 %> </td>                                     
                    <Td valign="top" HEIGHT="20"  class="text1" width="90"  style="border:1px;  border-right-style:solid; border-left-style:solid;" align="center"> <%= mfrCode %> &nbsp; </td>                                     
                    <Td valign="top" HEIGHT="20"  class="text1" width="400"  style="border:1px; border-right-style:solid; border-left-style:solid;" align="left"> <%= itemName %> </td>
                    <Td valign="top" HEIGHT="20"  class="text1" width="60"  style="border:1px;  border-right-style:solid; border-left-style:solid;" align="center"> <%= purch.getUomName() %> </td>
                    <Td valign="top" HEIGHT="20"  class="text1" width="60"  style="border:1px;  border-right-style:solid; border-left-style:solid;" align="right"> <%= purch.getTotalIssued() %> </td>                
                </tr>
                
                <%  index++; }  %>

            <% while(index <= listSize ){ %>

	        <tr>
                    <Td valign="top" HEIGHT="20"  class="text1" width="25"  style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;"  > &nbsp; </td>                                     
                    <Td valign="top" HEIGHT="20"  class="text1" width="90"  style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="center">   &nbsp;</td>                                     
                    <Td valign="top" HEIGHT="20"  class="text1" width="400"  style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="left"> &nbsp;</td>
                    <Td valign="top" HEIGHT="20"  class="text1" width="60"  style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="center"> &nbsp;</td>
                    <Td valign="top" HEIGHT="20"  class="text1" width="60"  style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right"> &nbsp; </td>                            
                </tr>


            <% index++; } %>
                                            
            </table>
</td>
</tr>

<Tr>
<Td valign="top" colspan="3">
            <table width="700" height="50" align="center" border="0" cellpadding="0" cellspacing="0" class="border">

            <Tr>
            <Td width="110" height="30" class="text1" ><strong>NOTE</strong></Td>
            <Td width="336" height="30" >&nbsp;</Td>
            <Td width="154" height="30"  class="text1" align="right" > &nbsp; </Td>
            </tr>
            <Tr>
            <Td width="110" class="text1" >&nbsp;</Td>
            <Td width="336">&nbsp;</Td>
            <Td width="154" class="text1" align="right" >&nbsp;</Td>
            </Tr>
            </table>
</td>
</tr>

<Tr>
<Td valign="top" colspan="3">
            <table width="700" height="30" align="center" border="0" cellpadding="0" cellspacing="0" class="border">

            <Tr>
            <Td  class="text1" height="30" > &nbsp; </Td>
            <Td  class="text1" height="30" >&nbsp;</Td>
            <Td class="text1" height="30" >&nbsp;</Td>
            </Tr>

            <Tr>
            <Td width="200" class="text1"><strong>Supervisor Signature</strong></Td>
            <Td  class="text1">&nbsp;</Td>
            <Td class="text1"><strong>&nbsp;</strong></Td>
            </Tr>
            </table>
</Td>
</Tr>

</table>
</div>
    <center>   
        <input type="button" class="button" name="Print" value="Print" onClick="print('print<%= i %>');" > &nbsp;        
    </center>
<br>    
<br>    
<br>    
<% } %>    

<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
