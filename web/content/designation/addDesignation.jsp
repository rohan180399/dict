<%-- 
Document   : addDesig
Created on : Nov 03, 2008, 2:57:39 PM
Author     : Vijay
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
   <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>PAPL</title>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>

<script language="javascript">

function validate(value){
if(document.addDesignation.searchStatus.value == '1'){
alert("Designation Name Already Exist");
document.addDesignation.desigName.focus();
}else if(isEmpty(document.addDesignation.desigName.value)){
alert("Designation Name Field Should Not Be Empty");
document.addDesignation.desigName.focus();
}else if(isSpecialCharacter(document.addDesignation.desigName.value)){
alert("Special Characters are not Allowed");
document.addDesignation.desigName.focus();
}else if(isChar(document.addDesignation.desigName.value)){
alert("Enter Valid Character ");
document.addDesignation.desigName.focus();
}else if(textValidation(document.addDesignation.desigName,'Designation Name')){
               return;
           }else if(textValidation(document.addDesignation.description,'Discription')){
               return;
           }
else if(value == 'add'){    
document.addDesignation.action = '/throttle/desigAdd.do';
document.addDesignation.submit();
}
}

    
    
function setFocus(){
document.addDesignation.desigName.focus();
}
window.onLoad = setFocus;

function isChar(s){
if(!(/^-?\d+$/.test(s))){
return false;
}
return true;
}

var httpRequest;
function getProfile(desigName)
{

var url = '/throttle/checkDesigName.do?desigName='+ desigName;
if (window.ActiveXObject)
{
httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
}
else if (window.XMLHttpRequest)
{
httpRequest = new XMLHttpRequest();
}

httpRequest.open("GET", url, true);

httpRequest.onreadystatechange = function() { processRequest(); } ;

httpRequest.send(null);
}


function processRequest()
{
if (httpRequest.readyState == 4)
{
if(httpRequest.status == 200)
{  
    
if(trim(httpRequest.responseText)!="") {   
document.addDesignation.desigName.focus();
document.addDesignation.searchStatus.value = "1";
document.getElementById("userNameStatus").innerHTML=httpRequest.responseText+" Already Exsts";
}else {
document.addDesignation.searchStatus.value = "0";
document.getElementById("userNameStatus").innerHTML="";
}


}
else
{
alert("Error loading page\n"+ httpRequest.status +":"+ httpRequest.statusText);
}
}
}


</script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">


<!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/ 
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->
<script>
	   function changePageLanguage(langSelection){
	   if(langSelection== 'ar'){
	   document.getElementById("pAlign").style.direction="rtl";
	   }else if(langSelection== 'en'){
	   document.getElementById("pAlign").style.direction="ltr";
	   }
	   }
	 </script>

	  <c:if test="${jcList != null}">
	  <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
	  </c:if>

<div class="pageheader">
      <h2><i class="fa fa-edit"></i><spring:message code="hrms.label.AddDesignation" text="default text"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="default text"/>:</span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.HRMS" text="default text"/></a></li>
          <li class=""><spring:message code="hrms.label.Designation" text="default text"/></li>
          <li class=""><spring:message code="hrms.label.AddDesignation" text="default text"/></li>



        </ol>
      </div>
      </div>
<div class="contentpanel">
            <div class="panel panel-default">
             <div class="panel-body">
<body onLoad="document.addDesignation.desigName.focus()">
<form name="addDesignation" method="post">
<font color="#FF0033" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; "><div align="center" id="userNameStatus" style="height:25px; "></div></font>   
 <table class="table table-info mb30 table-hover" >
<!--     <thead>
     <tr height="30">
                    <th colspan="5" class="contenthead"> <spring:message code="hrms.label.AddDesignation" text="default text"/></th>
                    </tr></thead>-->
<tr>
    <td width="150"  height="40"><font color=red>*</font><spring:message code="hrms.label.DesignationName" text="default text"/></td>
<td width="150"  height="40"><input type="text" name="desigName" class="form-control" style="width:220px;height:40px;" maxlength="100" onChange="getProfile(this.value)"></td>
<!--</tr>
<tr>-->
<td width="150"  height="40"><font color=red>*</font><spring:message code="hrms.label.Description" text="default text"/></td>
<td width="150"  height="40"><textarea name="description" class="form-control" style="width:220px;height:40px;"></textarea> </td>
</tr>
</table>
<center>
<br style="height:30px;">
<input type="button" value="<spring:message code="hrms.label.ADD" text="default text"/>" name="add" onClick="validate(this.name)" class="btn btn-success">
&emsp;<input type="reset" class="btn btn-success" value="<spring:message code="hrms.label.CLEAR" text="default text"/>">
<input type="hidden" name="searchStatus" value="">
</center>
<br>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
