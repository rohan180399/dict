package ets.cewolf.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.data.xy.MatrixSeriesCollection;
import org.jfree.data.xy.NormalizedMatrixSeries;
import de.laures.cewolf.DatasetProduceException;
import de.laures.cewolf.DatasetProducer;


public class BubbleBean implements DatasetProducer, Serializable {

	    /**
	 *
	 */
	private static final long serialVersionUID = 1L;

		private static final Log log = LogFactory.getLog(BubbleBean.class);

	    private  final int    SIZE  = 10;


		/**
		 *  Produces some random data.
		 */
	    public Object produceDataset(Map params) throws DatasetProduceException {
	    	log.debug("producing data.");

	    	// Dataset can either be populated by hard-coded or else from a datasource
	    	final NormalizedMatrixSeries newSeries =
                new NormalizedMatrixSeries("Sample Grid 1", SIZE, SIZE);

        // seed a few random bubbles
        for (int count = 0; count < SIZE * 3; count++) {
            final int i = (int) (Math.random() * SIZE);
            final int j = (int) (Math.random() * SIZE);

            final double mij = Math.random() * 1;
            newSeries.update(i, j, mij);
        }

        newSeries.setScaleFactor(newSeries.getItemCount());
        final MatrixSeriesCollection dataset = new MatrixSeriesCollection(newSeries);
        return dataset;

	    }

	    /**
	     * This producer's data is invalidated after 5 seconds. By this method the
	     * producer can influence Cewolf's caching behaviour the way it wants to.
	     */
		public boolean hasExpired(Map params, Date since) {
	        log.debug(getClass().getName() + "hasExpired()");
			return (System.currentTimeMillis() - since.getTime())  > 5000;
		}

		/**
		 * Returns a unique ID for this DatasetProducer
		 */
		public String getProducerId() {
			return "PageViewCountData DatasetProducer";
		}


		/**
		 * @see java.lang.Object#finalize()
		 */
		protected void finalize() throws Throwable {
			super.finalize();
			log.debug(this + " finalized.");
		}


}





