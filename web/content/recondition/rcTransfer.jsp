
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

    <html>
    <head>
        <%@page language="java" contentType="text/html; charset=UTF-8"%>
    <%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
    <%@page import="java.util.Locale"%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ page import="ets.domain.mrs.business.MrsTO" %> 
        <%@ page import="ets.domain.recondition.business.ReconditionTO" %> 
        <title>RC Queue</title>
    </head>
    <body>


    <script>

        function submitPage1(){  
            if(validate() == 'pass'){
            document.rcQueue.action="/throttle/handleSetSenderStatus.do"     
            document.rcQueue.submit();                
            }
        }  


    function validate()
    {
        var selectedIndex = document.getElementsByName("selectedIndex");
        var cntr = 0;

        for(var i=0;i<selectedIndex.length;i++){
            if(selectedIndex[i].checked == true ){
                cntr++;
            }                        
        }
        if(cntr == 0){
            alert("Please select any one item");
            return 'fail';
        }
            return 'pass';
    }




    function checkItems(val)
    {
        var items = document.getElementsByName("selectedIndex");
        items[val].checked=true;
    }
    </script>


         <script>
       function changePageLanguage(langSelection){
       if(langSelection== 'ar'){
       document.getElementById("pAlign").style.direction="rtl";
       }else if(langSelection== 'en'){
       document.getElementById("pAlign").style.direction="ltr";
       }
       }
     </script>

      <c:if test="${jcList != null}">
      <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
      </c:if>

      <span style="float: right">
            <a href="?paramName=en">English</a>
            |
            <a href="?paramName=ar">Arabic</a>
      </span>

    <form name="rcQueue" method="post">                    
    <%@ include file="/content/common/path.jsp" %>            
    <!-- pointer table -->

    <!-- message table -->           
                <%@ include file="/content/common/message.jsp" %>    

    <% int index = 0;%>                

    <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" id="bg" class="border">              
    <%index = 0;
                String classText = "";
                int counter = 0;
                int counter1 = 0;
                int oddEven = 0;
    %>
    <c:set var="mont" value="(Months)" />

    <c:if test = "${rcQueue != null}" >

    <tr>
        <td colspan="9" align="center" class="text2" height="30"><strong><spring:message code="stores.label.RCQueue"  text="default text"/> </strong></td>
    </tr>  

    <tr>
        <td class="contenthead" height="30"><div class="contenthead"><spring:message code="stores.label.MfrCode"  text="default text"/>
    </div></td>
        <td class="contenthead" height="30"><div class="contenthead"><spring:message code="stores.label.CompanyCode"  text="default text"/></div></td>                       
        <td class="contenthead" height="30"><div class="contenthead"><spring:message code="stores.label.ItemName"  text="default text"/></div></td>
        <td class="contenthead" height="30"><div class="contenthead"><spring:message code="stores.label.RCCode"  text="default text"/></div></td>
        <td class="contenthead" height="30"><div class="contenthead"><spring:message code="stores.label.CreatedDate"  text="default text"/></div></td>  
        <td class="contenthead" height="30"><div class="contenthead"><spring:message code="stores.label.Select"  text="default text"/></div></td>                                                                                                  
    </tr>                

    <c:forEach items="${rcQueue}" var="rc"> 

    <%

                oddEven = index % 2;
                if (oddEven > 0) {
                    classText = "text2";
                } else {
                    classText = "text1";
                }
    %>
    <tr>
    <td class="<%=classText %>" height="30"><c:out value="${rc.mfrCode}"/></td>
    <td class="<%=classText %>" height="30"><c:out value="${rc.paplCode}"/></td>                       
    <td class="<%=classText %>" height="30"><c:out value="${rc.itemName}"/></td>

    <td class="<%=classText %>" height="30">  

        <!-- For Tyre Items  -->       
        <c:if test="${rc.categoryId!='1011'}" >  
                <c:out value="${rc.rcNumber}" />
                <input type="hidden" readonly name="rcItemId" value="<c:out value="${rc.rcNumber}"/>" >
        </c:if> 

        <!-- For general Items  -->
        <c:if test="${rc.categoryId=='1011'}">  
                <c:out value="${rc.tyreNo}" />
                <input type="hidden" readonly name="rcItemId" value="<c:out value="${rc.tyreNo}"/>" >
        </c:if>  


    </td>

    <td class="<%=classText %>" height="30"><c:out value="${rc.createdDate}"/></td>

    <td class="<%=classText %>" height="30"> <input type="checkbox" name="selectedIndex" value="<%= index %>" > </td>                                                        
    <input type="hidden" name="itemIds" value=<c:out value="${rc.itemId}"/> > 
    <input type="hidden" name="rcQueueIds" value=<c:out value="${rc.rcQueueId}"/> >                             
    <input type="hidden" name="categoryId" value=<c:out value="${rc.categoryId}"/> >                             

    </tr>
    <%
                index++;
    %>

    </c:forEach>                 
    </table>                            
    <br>
    <div class="text2" align="center"> <spring:message code="stores.label.Remarks"  text="default text"/> &nbsp;&nbsp;:&nbsp;&nbsp;<textarea class="form-control" name="remarks" ></textarea> </div>
    <center>  
        <br>
        <br>    
        <input type="button" class="button" name="update" value="<spring:message code="stores.label.TRANSFERRCGOODS"  text="default text"/>" onClick="submitPage1();" >

    </center>           
    </c:if>             
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    </html>
