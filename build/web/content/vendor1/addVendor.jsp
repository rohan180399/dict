<%-- 
    Document   : addVendor
    Created on : Mar 8, 2009, 12:48:39 PM
    Author     : karudaiyar Subramaniam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="ets.domain.vendor.business.VendorTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
 <script language="javascript" src="/throttle/js/validate.js"></script>   

</head>
<script>
    function setSettlementDiv(){
        var vendorTypeId = document.dept.vendorTypeId.value;
        alert(vendorTypeId);
        if(vendorTypeId == 1001){
            document.getElementById("settlementDiv").style.display="block";
            document.getElementById("settlementDiv1").style.display="block";
        }else{
            document.getElementById("settlementDiv").style.display="none";
            document.getElementById("settlementDiv1").style.display="none";
        }
    }
    function submitpage()
    {
        if(textValidation(document.dept.vendorName,"vendorName")){
           return;
           }else if(textValidation(document.dept.tinNo,"Tin No")){
           return;
       }else if(isSelect(document.dept.vendorTypeId,"vendorType")){
           return;
       }else if(textValidation(document.dept.vendorAddress,"vendorAddress")){
           return;
       
       }else if(textValidation(document.dept.creditDays,"creditDays")){
           return;

       }else if(textValidation(document.dept.vendorPhoneNo,"vendorPhoneNo")){
           return;
       
       }else if(textValidation(document.dept.vendorMailId,"vendorMailId")){
           return;
       
       }          
        else{
            
            if(document.dept.vendorTypeId.value == 1011 &&
                document.dept.priceType.value == 0 ){
                alert("Please select price type");
                document.dept.priceType.focus();
                return;
            }

          var index = document.getElementsByName("selectedIndex");
          var mfrNames = document.getElementsByName("mfrNames");
           var chec=0;

        for(var i=0;(i<index.length && index.length!=0);i++){
        if(index[i].checked){
        chec++;
        // alert("chk"+chec);
        }
       }
       document.dept.action= "/throttle/addVendor.do";
       document.dept.submit();
    }
    }
 

</script>

<body>
 <%@ include file="/content/common/path.jsp" %>                   


<%@ include file="/content/common/message.jsp" %>

<form name="dept"  method="post" >
<table align="center" border="0" cellpadding="0" cellspacing="0" width="70%" id="bg" class="border">
<tr>
<td colspan="4" class="contentsub" height="30"><div class="contentsub">Add Vendor</div></td>
</tr>
<tr>
<td class="text1" height="30">Vendor Name </td>
   
<td class="text1" height="30">
    <input type="hidden" name="vendorId" value='<c:out value="${list.vendorId}"/>'>
    <input name="vendorName" style="width:124px;" type="text" class="form-control" value=""></td>

<td class="text1" height="30">TIN No</td>

<td class="text1" height="30">
    <input name="tinNo" style="width:124px;" type="text" class="form-control" value=""></td>
</tr>

<tr>  
<td class="text2"  >Vendor Type</td>
<td class="text2"  >
    <select class="form-control" name="vendorTypeId" style="width:129px" onChange="setSettlementDiv();" >
<option value="0">---Select---</option>
<c:if test = "${VendorTypeList != null}" >
<c:forEach items="${VendorTypeList}" var="Type"> 
<option value='<c:out value="${Type.vendorTypeId}" />'><c:out value="${Type.vendorTypeValue}" /></option>
</c:forEach >
</c:if>  	
</select></td>
<!--<div id="settlementDiv" style="display: none;">-->

<td class="text2" height="30"><div id="settlementDiv" style="display: none;">Settlement Type</div></td>
    <td class="text2" height="30">
        <div id="settlementDiv1" style="display: none;">
        <select class="form-control" name="settlementType" style="width:129px" >
            <option value="1">Cash Settlement</option>
            <option value="2">Credit Settlement</option>
        </select>
        </div>
    </td>

<input type="hidden" name="priceType" value="M"/>
<!--<td class="text2" height="30">-->
<!--    <select class="form-control" name="priceType" style="width:129px" >
        <option value="0">NA</option>
        <option value="M">MRP</option>
        <option value="L">List Price</option>
    </select>-->
<!--</td>-->
</tr>
<tr>
<td class="text1" height="30">Vendor Address</td>
<td class="text1" height="30"><textarea name="vendorAddress" style="width:125px;" rows="2" cols="20" class="form-control"></textarea></td>
<td class="text1" height="30">Vendor Phone No/Fax No</td>
<td class="text1" height="30"><input name="vendorPhoneNo" style="width:125px;" maxlength="45" type="text" class="form-control" value=""></td>
</tr>
<tr>
<td class="text2" height="30">Vendor Mail Id</td>
<td class="text2" height="30"><input name="vendorMailId" style="width:125px;" type="text" class="form-control" value=""></td>
<td class="text2" height="30">Credit (Days)</td>
<td class="text2" height="30"><input name="creditDays" style="width:125px;" type="text" class="form-control" value=""></td>
</tr>
 
 
 </table>
<br>
     
   <% int index = 0;  %>    
            <br>
            <div id="mfrList" style="display:none;">
            <c:if test = "${MfrList != null}" >
                <table width="400" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
                    
                    <tr align="center">
                        <td height="30" class="contentsub"><div class="contentsub">S.No</div></td>
                        <td height="30" class="contentsub"><div class="contentsub">Manufacture Name</div></td>
                        <td height="30" class="contentsub"><div class="contentsub">Select</div></td>
                         
                    </tr>
                    <%

                    %>
                    
                    <c:forEach items="${MfrList}" var="list"> 	
                        <%

            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr  width="208" height="40" > 
                            
                            
                                                 
                                
                                    <td class="<%=classText %>" height="30"><%=index + 1%></td>
                                    <td class="<%=classText %>" height="30"><input type="hidden" name="mfrids" value='<c:out value="${list.mfrId}"/>'> <c:out value="${list.mfrName}"/></td>
                                     <td width="77" height="30" class="<%=classText %>"><input type="checkbox" name="selectedIndex" value='<%= index %>'></td>
                                   
                                 
                                 
                            
                             
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach >
                    
                </table>
            </c:if>
            </div>
     
     
<center>
<input type="button" value="Add" class="button" onClick="submitpage();">
&emsp;<input type="reset" class="button" value="Clear">
</center>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>

