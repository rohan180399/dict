<%-- 
    Document   : addVendor
    Created on : Mar 8, 2009, 12:48:39 PM
    Author     : karudaiyar Subramaniam
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
    <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="ets.domain.problemActivities.business.ProblemActivitiesTO" %>

<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>   
    
</head>
<script>
    function submitpage()
    {
         if(isSelect(document.dept.sectionId,"sectionName")){           
       } else if(textValidation(document.dept.problemName,"Problem Name")){               
               } else if(textValidation(document.dept.discription,"Vendor Address")){     
     }else{
         document.dept.action= "/throttle/alterProblem.do";
        document.dept.submit();
    }
    }
   

</script>
<div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="serviceplan.label.AlterProblem" text="default text"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="serviceplan.label.ServicePlan" text="default text"/></a></li>
          <li class=""><spring:message code="serviceplan.label.ManageProblem" text="default text"/></li>
          <li class=""><spring:message code="serviceplan.label.AlterProblem" text="default text"/></li>

        </ol>
      </div>
      </div>
 <div class="contentpanel">
<div class="panel panel-default">
 <div class="panel-body">
<body   >
<form name="dept"  method="post"     >

     

<%@ include file="/content/common/message.jsp" %>

    <table class="table table-info mb30 table-hover" >

    <c:if test="${GetProblemDetailUnique != null}">
        <c:forEach items="${GetProblemDetailUnique}" var="vnd" >
            <thead>
           <tr >
           <th colspan="5" ><spring:message code="serviceplan.label.AlterProblem" text="default text"/></th>
            </tr>
            </thead>
            <tr>  
           <td  height="30"><spring:message code="serviceplan.label.Section" text="default text"/><c:out value="${comp.sectionId}" /></td>
                            <td   ><select style="width:260px;height:40px;"  class="form-control" name="sectionId" style="width:125px" >
                                    <option value="0">---<spring:message code="serviceplan.label.Select" text="default text"/>---</option>
                                    <c:if test = "${SectionList != null}" >
                                        <c:forEach items="${SectionList}" var="Dept"> 
                                            <c:choose>
                                                <c:when test="${vnd.sectionId == Dept.sectionId}" >
                                                    <option selected value='<c:out value="${Dept.sectionId}" />'><c:out value="${Dept.sectionName}" /></option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value='<c:out value="${Dept.sectionId}" />'><c:out value="${Dept.sectionName}" /></option>
                                                </c:otherwise>
                                            </c:choose>                                            
                                        </c:forEach >
                                    </c:if>  	
                            </select></td>
           
                <td  height="30"><spring:message code="serviceplan.label.ProblemName" text="default text"/> </td>
                <td  height="30">
                    <input type="hidden" name="problemId" value="<%=request.getAttribute("problemId")%>"  >
                       <input name="problemName" type="text" style="width:260px;height:40px;"  class="form-control" value="<c:out value="${vnd.problemName}"/>"  ></td>
            </tr>
            
            <tr>
                <td  height="30"><spring:message code="serviceplan.label.Description" text="default text"/></td>
                <td  height="30"><textarea style="width:260px;height:40px;"  class="form-control" name="discription"><c:out value="${vnd.discription}" /></textarea></td>
           
                <td  height="30"><spring:message code="serviceplan.label.Status" text="default text"/> </td>
                <td  height="30">
                        <select name="activeInd" style="width:260px;height:40px;"  class="form-control"style="width:125px" >
                        <c:if test="${(vnd.activeInd=='y') || (vnd.activeInd=='Y')}" >
                        <option value="Y" selected><spring:message code="serviceplan.label.Active" text="default text"/></option><option value="N"><spring:message code="serviceplan.label.InActive" text="default text"/></option>
                        </c:if>
                        <c:if test="${(vnd.activeInd=='n') || (vnd.activeInd=='N')}" >
                        <option value="Y" ><spring:message code="serviceplan.label.Active" text="default text"/></option><option value="N" selected><spring:message code="serviceplan.label.InActive" text="default text"/></option>                           
                        </c:if>                            
                        </select>                    
                    
                </td>
            </tr>
   </c:forEach>  
    </c:if> 
            <td></td>
            <td></td>
</table>
    </td>




 


<center>
    <input type="button" value="<spring:message code="serviceplan.label.ALTER" text="default text"/>" class="btn btn-success"  onClick="submitpage();">
</center>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

