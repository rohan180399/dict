<%--
    Document   : Accounts Receivable
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

        </style>
    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "VehicleWiseProfitability-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
                <c:if test="${containerAvailability != null }">
                <table border="1"  align="center" width="100%" cellpadding="0" cellspacing="1" >
                    <thead>
                        <tr>
                            <th width="100"  style="text-align: center">ContainerNo&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th width="150"  style="text-align: center">ContainerType</th>
                                <th width="150"  style="text-align: center">TripCode</th>
                                <th width="150"  style="text-align: center">OriginDestination</th>
                                <th width="150"  style="text-align: center">Status</th>
                                <th width="150"  style="text-align: center">Trip Start Date</th>
                                <th width="150"  style="text-align: center">Trip End Date</th>
                                <th width="150"  style="text-align: center">GPS Location</th>
                                <th width="150"  style="text-align: center">Order No</th>
                                <th width="150"  style="text-align: center">Movement Type</th>
                        </tr>

                    </thead>
                   
                    <c:forEach items="${containerAvailability}" var="val">
                         <% int index = 0,sno = 1;
                    String className = "text1";
                                            if ((sno % 1) == 0) {
                                                className = "text1";
                                            } else {
                                                className = "text2";
                                            }%>
                         <tr>
                                    <td class="<%=className%>"  align="left" width="90"> <c:out value="${val.containerNo}"/></td>
                                    <td class="<%=className%>"  align="left" width="90"> <c:out value="${val.containerTypeName}"/></td>
                                    <td class="<%=className%>"  align="left" width="90"> <c:out value="${val.tripCode}"/></td>
                                    <td class="<%=className%>"  align="left" width="90"> <c:out value="${val.origin}"/></td>
                                    <td class="<%=className%>"  align="left" width="90"> <c:out value="${val.status}"/></td>
                                    <td class="<%=className%>"  align="left" width="90"> <c:out value="${val.tripStartDate}"/></td>
                                    <td class="<%=className%>"  align="left" width="90"> <c:out value="${val.tripEndDate}"/></td>
                                    <c:if test = "${val.status eq 'Trip Started / Trip In Progress' ||  val.status eq 'Trip Started / Trip In Progress'}">
                                    <td class="<%=className%>"  align="left" width="90"> <c:out value="${val.gpsLocation}"/></td>
                                    </c:if>
                                    <c:if test = "${val.status ne 'Trip Started / Trip In Progress' ||  val.status  ne 'Trip Started / Trip In Progress'}">
                                    <td class="<%=className%>"  align="left" width="90"> NA</td>
                                    </c:if>
                                    <td class="<%=className%>"  align="left" width="90"> <c:out value="${val.consignmentOrderNos}"/></td>
                                    <td class="<%=className%>"  align="left" width="90"> <c:out value="${val.movementType}"/></td>
                                </tr>
                    </c:forEach>
                  
                                        
                </table>
            </c:if>
           
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
