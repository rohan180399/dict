<%--
    Document   : newfuelprice
    Created on : Oct 27, 2013, 5:04:09 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<!--<script language="javascript" src="/throttle/js/validate.js"></script>-->
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body >
        <form name="fuleprice"  method="post" >
            <br>
            <br>
            <table class="table table-info mb30 table-hover" id="table" >
                <thead>
                <tr height="30">
                <th><h3>Sno</h3></th>
                <th><h3>Model Name(MfrName)</h3></th>
                <th><h3>VehicleType</h3></th>
                <th><h3>Action</h3></th>
                </tr>
                </thead>
                <tbody>
                    <% int sno = 0;%>
                    <c:if test = "${viewMfrModelList != null}" >
                        <%
                                            sno++;
                                           String className = "text1";
                                           if ((sno % 1) == 0) {
                                               className = "text1";
                                           } else {
                                               className = "text2";
                                           }
                        %>
                        <c:forEach items="${viewMfrModelList}" var="viewMfrModelList">
                            <tr height="30">
                                <td class="<%=className%>"  height="30"> <%= sno++%></td>
                                <td class="<%=className%>" align="left" ><c:out value="${viewMfrModelList.modelName}"/> (<c:out value="${viewMfrModelList.mfrName}"/>)</td>
                                <td class="<%=className%>" align="left" ><c:out value="${viewMfrModelList.vehicleTypeName}"/></td>
                                <td class="<%=className%>" align="left" >
                                    <button type="button" class="btn btn-success" onclick="viewConfigDetails('<c:out value="${viewMfrModelList.mfrId}"/>','<c:out value="${viewMfrModelList.modelId}"/>','<c:out value="${viewMfrModelList.vehicleTypeId}"/>');">CONFIG</button>
                                </td>
                                
                            </tr>
                        </c:forEach>
                        <script>
                            function  viewConfigDetails(mfrId,modelId,vehicleTypeId) {
                            var url = './viewMfrMilleageConfigurationDetails.do?mfrId='+mfrId+'&modelId='+modelId+'&vehicleTypeId='+vehicleTypeId;
                            jQuery.ajax({
                                url: url,
                                type: "get",
                                success: function (data)
                                {
                                    $('#details').html(data);
                                }
                            });
                        }
                        </script>   
                    </c:if>
                            
                </tbody>
            </table>
                           
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);

                
                function upateMfrMilleage(value,ageingId,vehicleTypeId,modelId,mfrId){
                    url = './upateMfrMilleage.do';
                    $.ajax({
                        url: url,
                        data: {kmPerLitter: value,ageingId: ageingId,vehicleTypeId: vehicleTypeId,modelId: modelId,mfrId: mfrId
                        },
                        type: "GET",
                        success: function(response) {
                            
                        },
                        error: function(xhr, status, error) {
                        }
                    });
                }
            </script>
                    <div id="details" style="align-content: center"></div> 
            
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

