<%-- 
Document   : modifyDesig
Created on : Nov 03, 2008, 6:14:28 PM
Author     : Vijay
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
 
<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="ets.domain.vehicle.business.VehicleTO" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>PAPL</title>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
                        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
                        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
<script language="javascript">

function submitpage(value)
{
 

var checValidate = selectedItemValidation();
var splt = checValidate.split("-");
if(splt[0]=='SubmitForm' && splt[1]!=0 ){
    

document.modify.action='/throttle/modifyMfr.do';
document.modify.submit();
}
}

function setSelectbox(i)
{
var selected=document.getElementsByName("selectedIndex") ;
selected[i].checked = 1;
}

function selectedItemValidation(){
var index = document.getElementsByName("selectedIndex");
var mfrNames = document.getElementsByName("mfrNames");
var percentage = document.getElementsByName("percentage");
var desc = document.getElementsByName("descriptions");
var chec=0;

for(var i=0;(i<index.length && index.length!=0);i++){
        if(index[i].checked){
        chec++;
        if(textValidation(mfrNames[i],"Manufacturer Name")){
            return;
        }else if(textValidation(desc[i],"Desription")){
            return;                            
        }else if( floatValidation(percentage[i],'Percentage') ){
            return;                    
        }
        }
}
if(chec == 0){
alert("Please Select Any One And Then Proceed");
desigName[0].focus();
//break;
}
document.modify.action='/throttle/modifyMfr.do';
document.modify.submit();
}

function isChar(s){
if(!(/^-?\d+$/.test(s))){
return false;
}
return true;
}
</script>

</head>


<!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/ 
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->

<body>
<!--<body onload="document.modify.desigNames[0].focus();">-->
<form method="post" name="modify"> 
 <%@ include file="/content/common/path.jsp" %>
     
<%@ include file="/content/common/message.jsp" %>
<br>
 <% int index = 0; %>        

<table width="500" align="center" id="bg" cellpadding="0" cellspacing="0" class="border">
 <c:if test = "${MfrList != null}" >   

<tr>
 <td class="contentsub" height="30"> </td>
<td height="30" class="contentsub"><div class="contentsub">S.No</div></td>  
<td class="contentsub" height="30"><div class="contentsub">Manufacturer Name</div> </td> 
<td class="contentsub" height="30"><div class="contentsub">Activity Group</div> </td>
<td class="contentsub" height="30"><div class="contentsub">Body Works Group</div></td>
<td class="contentsub" height="30"><div class="contentsub">Description</div></td>
<td class="contentsub" height="30"><div class="contentsub">Status</div></td>
<td class="contentsub" height="30"><div class="contentsub">Select</div></td>

</tr>


</c:if>
<c:if test = "${MfrList != null}" >
<c:forEach items="${MfrList}" var="mfr"> 		
<%

String classText = "";
int oddEven = index % 2;
if (oddEven > 0) {
classText = "text2";
} else {
classText = "text1";
}
%>
<tr>
 <td class="<%=classText %>" height="30"><input type="hidden" name="mfrIds" value='<c:out value="${mfr.mfrId}"/>'>  </td>
<td class="<%=classText %>" height="30"><%=index + 1%></td>
<td class="<%=classText %>" height="30"><input type="text" class="form-control" name="mfrNames" value="<c:out value="${mfr.mfrName}"/>" onchange="setSelectbox(<%= index %>)"></td>
<td class="<%=classText %>" height="30">
    <select  style='width:200px;' class="form-control" id="groupIds"  name="groupIds"  onchange="setSelectbox(<%= index %>)">
        <c:choose>
        <c:when test="${mfr.activityGroupId == 1}" >
            <option selected value=1>Volvo Group</option>
            <option  value=2>Non Volvo Group</option>
        </c:when>
        <c:when test="${mfr.activityGroupId == 2}" >
            <option  value=1>Volvo Group</option>
            <option selected value=2>Non Volvo Group</option>
        </c:when>
        <c:otherwise>
            <option selected value=0>--select--</option>
            <option  value=1>Volvo Group</option>
            <option  value=2>Non Volvo Group</option>
        </c:otherwise>
        </c:choose>

    </select>
</td>
<td class="<%=classText %>" height="30">
    <select  style='width:200px;' class="form-control" id="bodyGroupIds"  name="bodyGroupIds"  onchange="setSelectbox(<%= index %>)">

        <c:choose>
        <c:when test = "${mfr.bodyWorkGroupId == 3}" >
            <option selected value=1>Cargo</option>
            <option  value=2>High End (Luxury)</option>
            <option  value=3>Ordinary</option>
        </c:when>
        <c:when test = "${mfr.bodyWorkGroupId == 4}" >
            <option  value=1>Cargo</option>
            <option selected value=2>High End (Luxury)</option>
            <option  value=3>Ordinary</option>
        </c:when>
        <c:when test = "${mfr.bodyWorkGroupId == 5}" >
            <option  value=1>Cargo</option>
            <option  value=2>High End (Luxury)</option>
            <option selected value=3>Ordinary</option>
        </c:when>
        <c:otherwise>
            <option selected value=0>--select--</option>
            <option  value=3>Cargo</option>
            <option  value=4>High End (Luxury)</option>
            <option  value=5>Ordinary</option>
        </c:otherwise>
        </c:choose>


    </select>
</td>
<input type="hidden" class="form-control" name="percentage" value="<c:out value="${mfr.percentage}"/>" onchange="setSelectbox(<%= index %>)">
<td class="<%=classText %>" height="30"><input type="text" class="form-control" name="descriptions" value="<c:out value="${mfr.description}"/>" onchange="setSelectbox(<%= index %>)"></td>
<td height="30" class="<%=classText %>"> <div align="center"><select name="activeInds" class="form-control" onchange="setSelectbox(<%= index %>)">
<c:choose>
<c:when test="${mfr.activeInd == 'Y'}">
<option value="Y" selected>Active</option>
<option value="N">InActive</option>
</c:when>
<c:otherwise>
<option value="Y">Active</option>
<option value="N" selected>InActive</option>
</c:otherwise>
</c:choose>
</select>
</div> 
</td>
<td width="77" height="30" class="<%=classText %>"><input type="checkbox" name="selectedIndex" value='<%= index %>'></td>
</tr>


<!--<tr id="dispaly">
   <td class="<%=classText %>" height="30">
   </td>
   <td class="<%=classText %>" height="30">vehicle </td>
    <td class="<%=classText %>" height="30">
         <c:choose>
        <c:when test = "${mfr.vehicleMfr == 'vehicle'}" >
            <input type="checkbox" name="vehicleMfr" id="vehicleMfr<%= index %>" value="<c:out value="${mfr.vehicleMfr}"/>" checked/>
        </c:when>
        <c:otherwise>
            <input type="checkbox" name="vehicleMfr" id="vehicleMfr<%= index %>" value="vehicle"  />
        </c:otherwise>
        </c:choose>
    </td>
    
    <td class="<%=classText %>" height="30">tyre</td>
    <td class="<%=classText %>" height="30">
         <c:choose>
        <c:when test = "${mfr.tyreMfr == 'tyre'}" >
            <input type="checkbox" name="tyreMfr" id="tyreMfr<%= index %>" value="<c:out value="${mfr.tyreMfr}"/>"  checked />
        </c:when>
        <c:otherwise>
            <input type="checkbox" name="tyreMfr" id="tyreMfr<%= index %>" value="tyre"  />
        </c:otherwise>
        </c:choose>
    </td>
    <td class="<%=classText %>" height="30">trailer</td>
    <td class="<%=classText %>" height="30">
         <c:choose>
        <c:when test = "${mfr.tyreMfr == 'trailer'}" >
            <input type="checkbox" name="trailerMfr" id="tyreMfr<%= index %>" value="<c:out value="${mfr.trailer}"/>"  checked />
        </c:when>
        <c:otherwise>
            <input type="checkbox" name="trailerMfr" id="trailerMfr<%= index %>" value="trailer"  />
        </c:otherwise>
        </c:choose>
    </td>
    
    
    <td class="<%=classText %>" height="30">Battery</td>
    <td class="<%=classText %>" height="30">
         <c:choose>
        <c:when test = "${mfr.tyreMfr == 'trailer'}" >
            <input type="checkbox" name="batteryMfr" id="batteryMfr<%= index %>" value="<c:out value="${mfr.batteryMfr}"/>"  checked />
        </c:when>
        <c:otherwise>
            <input type="checkbox" name="batteryMfr" id="batteryMfr<%= index %>" value="batteryMfr"  />
        </c:otherwise>
        </c:choose>
    </td>
    
</tr>-->


<%
index++;
%>
</c:forEach >
</c:if> 
</table>
<center>
<br>
<input type="button" name="save" value="Save" onClick="submitpage(this.name)" class="button" />
<input type="hidden" name="reqfor" value="designa" />
</center>
<br>
    <br>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
