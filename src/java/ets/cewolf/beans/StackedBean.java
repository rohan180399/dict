package ets.cewolf.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import de.laures.cewolf.DatasetProduceException;
import de.laures.cewolf.DatasetProducer;
import de.laures.cewolf.links.CategoryItemLinkGenerator;
import de.laures.cewolf.tooltips.CategoryToolTipGenerator;

import ets.domain.report.business.ReportBP;
import ets.domain.report.business.ReportTO;


public class StackedBean implements DatasetProducer, CategoryToolTipGenerator, CategoryItemLinkGenerator,Serializable  {

	private static final long serialVersionUID = 1L;
		private static final Log log = LogFactory.getLog(StackedBean.class);
	    String startDate;
	    String endDate;
	    String strIOU;
            ReportTO serviceRepTO = new ReportTO();
            ArrayList serviceSummary = new ArrayList();

            String region;
	    String[] seriesNames = new String[10];
	    String[][] linkNames = new String[10][10];
	    String[][] links = new String[10][10];
	    int counter = 0;
	    int count = 0;
            int siz = 0;
            int cntr = 0;
            int i = 0,j=0;
	    /**
		 *  Produces some random data.
		 */

	    public Object produceDataset(Map params) throws DatasetProduceException {
	    	log.debug("producing data.");
                ReportBP repBP = new ReportBP();
                
                //////System.out.println("serviceData size="+serviceSummary.size());
                siz = (serviceSummary.size())/3;
                Iterator service = serviceSummary.iterator();
                ReportTO repTO = new ReportTO();

	    	  // Dataset can either be populated by hard-coded or else from a datasource
	    	DefaultCategoryDataset dataset = new DefaultCategoryDataset(){
				/**
				 *
				 */
				private static final long serialVersionUID = 1L;

				/**
				 * @see java.lang.Object#finalize()
				 */
				protected void finalize() throws Throwable {
					super.finalize();
					log.debug(this +" finalized.");
				}
	        };                
                while(service.hasNext()){
                    repTO = new ReportTO();
                    repTO = (ReportTO) service.next();                    
                    if( j >= (3) ){
                        j=0;
                        i++;
                    }
                    linkNames[i][j]=repTO.getCompanyName()+"-"+repTO.getTotalIssued();    
                    //links[i][j]="";                    
                    j++;                    
                    cntr++;
                    dataset.addValue( Integer.parseInt(repTO.getTotalIssued() ) ,repTO.getStatus() ,repTO.getCompanyName() );
                }
                
                                            /*
					   seriesNames[0]="Madhavaram";
					   seriesNames[1]="CTS";
					   seriesNames[2]="Nokia";

                                          
					   links[0][0]="/index.jsp";
					   links[0][1]="/index.jsp";
					   links[0][2]="/index.jsp";
                                           links[0][3]="/index.jsp";
					   links[1][0]="/index.jsp";
					   links[1][1]="/index.jsp";
					   links[1][2]="/index.jsp";
                                           links[1][3]="/index.jsp";
					   links[2][0]="/index.jsp";
					   links[2][1]="/index.jsp";
					   links[2][2]="/index.jsp";
                                           links[2][3]="/index.jsp"; 
                                           */
                                           /* 
					   linkNames[0][0]="Madhavaram-30";
					   linkNames[0][1]="Madhavaram-25";
					   linkNames[0][2]="Madhavaram-55";
                                           linkNames[0][3]="Madhavaram-90";
					   linkNames[1][0]="CTS";
					   linkNames[1][1]="CTS";
					   linkNames[1][2]="CTS";
                                           linkNames[1][3]="CTS";
					   linkNames[2][0]="Nokia";
					   linkNames[2][1]="Nokia";
					   linkNames[2][2]="Nokia";
                                           linkNames[2][3]="Nokia";

                                           dataset.addValue(30, "Vehicles In","Madhavaram");
					   dataset.addValue(25, "Vehicles Out","Madhavaram");
					   dataset.addValue(55, "Completely Serviced","Madhavaram");
                                           dataset.addValue(90, "Partially Serviced","Madhavaram");

					   dataset.addValue(50, "Vehicles In","CTS");
					   dataset.addValue(35, "Vehicles Out","CTS");
					   dataset.addValue(15, "Completely Serviced","CTS");
                                           dataset.addValue(90, "Partially Serviced","CTS");

					   dataset.addValue(30, "Vehicles In","Nokia");
					   dataset.addValue(55, "Vehicles Out","Nokia");
					   dataset.addValue(15, "Completely Serviced","Nokia");
                                           dataset.addValue(90, "Partially Serviced","Nokia");
                                           */


			      return dataset;

	    }

	    /**
	     * This producer's data is invalidated after 5 seconds. By this method the
	     * producer can influence Cewolf's caching behaviour the way it wants to.
	     */
		public boolean hasExpired(Map params, Date since) {
	        log.debug(getClass().getName() + "hasExpired()");
			return (System.currentTimeMillis() - since.getTime())  > 5000;
		}

		/**
		 * Returns a unique ID for this DatasetProducer
		 */
		public String getProducerId() {
			return "PageViewCountData DatasetProducer";
		}


		/**
		 * @see java.lang.Object#finalize()
		 */
		protected void finalize() throws Throwable {
			super.finalize();
			log.debug(this + " finalized.");
		}

		/**
		 * @see org.jfree.chart.tooltips.CategoryToolTipGenerator#generateToolTip(CategoryDataset, int, int)
		 */
		public String generateToolTip(CategoryDataset arg0, int series, int arg2) {

			return linkNames[arg2][series];
		}

		/**
	     * Returns a link target for a special data item.
	     */
	    public String generateLink(Object data, int series, Object category) {

	    	if(series == 0)
	    	{
		    	count = counter++;                        
	    		return links[count][series];
	    	}
	    	else {
	    		return links[count][series];
	    	}
	    }

    public ArrayList getServiceSummary() {
        return serviceSummary;
    }

    public void setServiceSummary(ArrayList serviceSummary) {
        this.serviceSummary = serviceSummary;
    }


	}


