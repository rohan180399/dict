<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BUS</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script><!-- External script -->
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    </head>
    <script language="javascript">
        function submitPage(value)
        {
            if (value=='add') {
                 document.desigDetail.action ='/throttle/handleRouteAddPage.do';
            }/*else if(value == 'modify'){
                document.desigDetail.action ='/throttle/handleRouteAlterPage.do';
            }*/
            document.desigDetail.submit();
        }
    </script>    
    <body>        
        <form method="post" name="desigDetail">
        <%@ include file="/content/common/path.jsp" %>
        <%@ include file="/content/common/message.jsp" %>
        <br>
        <center><input type="button" name="add" value="Add" onClick="submitPage(this.name)" class="button"></center>
<!--        <div style="height:360px;overflow:scroll;overflow-y:scroll;overflow-x:hidden;">-->
        <% int index = 0; %>
            <c:if test = "${RouteList != null}" >
                <table width="1000" align="center" cellpadding="0" cellspacing="0" id="mngRoute" class="border">
                    <tr align="center">
                        <td height="30" class="contentsub">S.No</td>
                        <td height="30" class="contentsub">Route Code</td>
                        <td height="30" class="contentsub">Customer</td>
                        <td height="30" class="contentsub">Route Name</td>
                        <td height="30" class="contentsub">State</td>
                        <td height="30" class="contentsub">District / City</td>
                        <td height="30" class="contentsub">Via Route</td>
                        <td height="30" class="contentsub">Freight / Ton</td>
                        <td height="30" class="contentsub">Freight/Ton for Market</td>
                        <td height="30" class="contentsub">Km</td>
                        <td height="30" class="contentsub">SLA (in Hrs)</td>
                        <td height="30" class="contentsub">Toll Amount</td>                        
                        <td height="30" class="contentsub">Driver Bata / per Ton</td>
                        <td height="30" class="contentsub">Market Vehicle Margin %age</td>
                        <td height="30" class="contentsub">&nbsp;</td>
                    </tr>
                    <c:forEach items="${RouteList}" var="list">
                        <%
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }
                        %>
                        <tr  width="208" height="40" > 
                            <td class="<%=classText %>" height="30"><%=index + 1%></td>
                            <td class="<%=classText %>" height="30"><c:out value="${list.routeId}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${list.custName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${list.routeName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${list.state}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${list.toCity}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${list.viaRoute}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${list.tonnageRate}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${list.tonnageRateMarket}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${list.km}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${list.sla}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${list.tollAmount}"/></td>                            
                            <td class="<%=classText %>" height="30"><c:out value="${list.driverBata}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${list.marketVehiclePercentage}"/></td>
                            <td class="<%=classText %>" height="30">
                                <a href="/throttle/alterRoutes.do?routeId=<c:out value="${list.routeId}"/>"> Edit</a>
                            </td>
                        </tr>
                        <%
                            index++;
                        %>
                    </c:forEach>                    
                </table>
            </c:if>
<!--            </div>-->
            <center>
                <br>
<!--                <input type="button" name="add" value="Add" onClick="submitPage(this.name)" class="button">-->
                <c:if test = "${RouteList != null}" >
                    <!--<input type="button" value="Alter" name="modify" onClick="submitPage(this.name)" class="button">-->
                </c:if>
                <input type="hidden" name="reqfor" value="">
            </center>
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    <script language="javascript" type="text/javascript">
        setFilterGrid("mngRoute");
    </script>
    </body>
</html>
