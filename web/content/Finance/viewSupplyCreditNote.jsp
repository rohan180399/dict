
<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">

<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>

<script type="text/javascript">
    function setInvoiceDetails() {
       // alert("cal");
        var invoiceName = document.getElementById('invoiceIdTemp').options[document.getElementById('invoiceIdTemp').selectedIndex].text;

        var invoiceNames = document.getElementsByName('invoiceNames');

        var errStatus = false;
        for (var i = 0; i < invoiceNames.length; i++) {
            if (invoiceNames[i].value == invoiceName) {
                alert("adjustment for this po no is already done. please select different po no");
                errStatus = true;
            }
        }
        if (!errStatus) {
            var invoiceId = document.getElementById("invoiceIdTemp").value;
            var grNos = document.getElementsByName("grNo").value;
           // alert("grNos.length" + invoiceId);
            var temp = invoiceId.split("~");
            for (var i = 0; i < grNos.length; i++) {
                document.getElementById("invoiceId" + i).value = temp[0];
                document.getElementById("grandTotal" + i).value = temp[1];
                document.getElementById("payAmount" + i).value = 0;
                document.getElementById("pendingAmount" + i).value = temp[3];
                document.getElementById("discountAmount" + i).value = temp[4];
                document.getElementById("invoiceCode" + i).value = temp[5];
                document.getElementById("invoiceDate" + i).value = temp[6];
            }
            //alert(temp[4]);
        }
    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
</script>

<script type="text/javascript">
    function submitPage() {
        var finYear = document.getElementById("finYear").value;
        if ($('#receiptDate').val() == '') {
            alert("Please select date");
            $('#receiptDate').focus();
        } else if ($('#remarks').val() == '') {
            alert("Please enter the remarks");
            $('#remarks').focus();
        } else if ($('#invoiceIdTemp').val() == 0) {
            alert("Please select invoice");
            $('#invoiceIdTemp').focus();
        } else if ($('#payAmount').val() == 0 || $('#payAmount').val() == '') {
            alert("Please enter credit amount");
            $('#payAmount').focus();
        } else if ($('#receiptAmount').val() < 0) {
            alert("Please check credit amount should not negative");
            $('#payAmount').focus();
        } else if ($('#totalCreditAmount').val() <= 0 || $('#totalCreditAmount').val() == '') {
            alert("Please check credit amount should not be 0");
            $('#totalCreditAmount').focus();
        } else if ($('#creditReason').val() <= 0 || $('#creditReason').val() == '') {
            alert("Please enter the credit reason ");
            $('#creditReason').focus();
        } else if(finYear==""){
            alert("Please selct the financal Year.");
            document.getElementById("finYear").focus(); 
            return false;
        }        
        else {
            $("#saveCredit").hide();  
            document.manufacturer.action = "/throttle/saveSupplyCreditNote.do";
            document.manufacturer.submit();
        }
    }
    
    
    
    window.onload = function ()
    {
        var currentDate = new Date();
        var day = currentDate.getDate();
        var month = currentDate.getMonth() + 1;
        var year = currentDate.getFullYear();
        var myDate = day + "-" + month + "-" + year;
        document.getElementById("receiptDate").value = myDate;
    }
    function setValues() {
        if ('<%=request.getAttribute("invoiceId")%>' != 'null') {
            document.getElementById('invoiceIdTemp').value = '<%=request.getAttribute("invoiceId")%>';
        }
        if ('<%=request.getAttribute("invoiceDate")%>' != 'null') {
            document.getElementById('invoiceDate').value = '<%=request.getAttribute("invoiceDate")%>';
        }
        if ('<%=request.getAttribute("invId")%>' != 'null') {
            document.getElementById('invoiceId').value = '<%=request.getAttribute("invId")%>';
        }
        if ('<%=request.getAttribute("totalInvoiceAmount")%>' != 'null') {
            document.getElementById('totInvoiceAmt').value = '<%=request.getAttribute("totalInvoiceAmount")%>';
            //   $('totalInvoiceAmount').text = '<%=request.getAttribute("totalInvoiceAmount")%>';
            $('#totalInvoiceAmount').text('<%=request.getAttribute("totalInvoiceAmount")%>');
        }

    }

</script>



<style>
    #index td {
        color:white;
    }
</style>    

<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="finance.label.SupplyCreditNoteDetails"  text="Supply CreditNote Details"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance"  text="default text"/></a></li>
            <li class="active"><spring:message code="finance.label.SupplyCreditNoteDetails"  text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">


            <body onload="setValues();">
                <%
              Date today = new Date();
              SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
              String startDate = sdf.format(today);
                %>
                <form name="manufacturer" method="post" >

                    <table class="table table-info mb30 table-hover">
                        <thead>
                            <tr colspan="6">
                                <th   accesskey="" style="background-color:#5BC0DE;" colspan="4"  height="30">
                                    <b><spring:message code="finance.label.SupplyCreditNoteDetails"  text="Supply CreditNote Details"/></b></th>
                            </tr>

                            <tr>
                                <th colspan="6"  align="center">
                                    <b><spring:message code="finance.label.CustomerName"  text="default text"/>:&nbsp;&nbsp; </b><b><c:out value="${customerName}"/></b>
                                    <input type="hidden" name="customerId" id="customerId" value="<c:out value="${customerId}"/>"/> 
                                    <input type="hidden" name="customerName" id="customerName" value="<c:out value="${customerName}"/>"/> </th>
                            </tr>
                        </thead>

                        <tr  height="40">
                            <td align="center"><font color="red">*</font>Supply Invoice No</td>
                        <input type="hidden" name="invoiceId" id="invoiceId" value=""/>
                        <td> <select name="invoiceIdTemp" id="invoiceIdTemp" class="form-control" onchange="setGRNoList(this.value);" style="width:170px;height:40px;">
                                <c:if test="${invoiceList != null}">
                                    <option value="0" selected>--<spring:message code="finance.label.Select"  text="default text"/>--</option>
                                    <c:forEach items="${invoiceList}" var="invoiceList">
                                        <option value='<c:out value="${invoiceList.invoiceId}"/>~<c:out value="${invoiceList.grandTotal}"/>~<c:out value="${invoiceList.payAmount}"/>~<c:out value="${invoiceList.pendingAmount}"/>~<c:out value="${invoiceList.creditNoteAmount}"/>~<c:out value="${invoiceList.invoiceCode}"/>~<c:out value="${invoiceList.invoiceDate}"/>~<c:out value="${invoiceList.gstType}"/>'><c:out value="${invoiceList.invoiceCode}"/></option>
                                    </c:forEach>
                                </c:if>
                            </select>
                            <input type="hidden" name="invoiceDate" id="invoiceDate" readonly class="form-control" value="" style="width:140px;height:40px;">
                            <input type="hidden" name="gstType" id="gstType" readonly class="form-control" value='<c:out value="${gstType}"/>' style="width:140px;height:40px;">
                        </td>
                        <td><font color="red">*</font>Supply Credit Note Date:</td>
                        <td>
                            <input type="text" name="receiptDate" id="receiptDate" readonly class="form-control" value="<%=startDate%>" style="width:260px;height:40px;">
                        </td>
                        <td><font color="red">*</font>Remarks:</td>
                        <td>
                            <textarea  name="remarks" id="remarks" style="width:260px;height:55px;"></textarea>
                        </td>

                        </tr>
                        <tr>
                            <td>Total Invoice Amount</td>
                            <td><span id="totalInvoiceAmount"></span>
                                <input  type ="hidden" id="totInvoiceAmt" name="totInvoiceAmt" value=""  class="form-control" style="width:140px;height:40px;"></td>
                            <td>Reason</td>
                            <td>
                                <select id="creditReason" name="creditReason" class="form-control">
                                    <option value="0">----Select----</option>
                                    <option value="Sales Return/ Service reversed">Sales Return/ Service reversed</option>
                                    <option value="Post Sale Discount / Volume Discount">Post Sale Discount / Volume Discount</option>
                                    <option value="Deficiency in services">Deficiency in services</option>
                                    <option value="Correction in Invoice">Correction in Invoice</option>
                                    <option value="Change in POS/Change in Place of Supply">Change in POS/Change in Place of Supply</option>
                                    <option value="Finalization of Provisional assessment">Finalization of Provisional assessment</option>
                                    <option value="Others">Others</option>
                                </select>
                            </td>
                            <td >Credit Note No</td>
                            <td ><c:out value="${creditNoteNo}"/></td>
                        </tr>

                    </table>
                    <br/>
                    
                    <c:if test="${invoiceDetailsList != null}">
                        <table class="table table-info">
                            <tr height="30" id="index">
                                <td colspan="9"  height="30" style="background-color:#5BC0DE;">
                                    &nbsp;&nbsp;Supply Invoice Details</td>
                            </tr>

                            <tr>

                                <td align="center"><font color="red">*</font>GR No</td>
                                <td align="center"><font color="red">*</font>Freight Amount</td>
                                <td align="center"><font color="red">*</font>Detaintion</td>
                                <td align="center"><font color="red">*</font>Toll</td>
                                <td align="center"><font color="red">*</font>Weighment</td>
                                <td align="center"><font color="red">*</font>Other</td>
                                <td align="center"><font color="red">*</font>Total Credit Amount</td>
                            </tr>
                             <% int i=1; %>
                            <c:forEach items="${invoiceDetailsList}" var="invoiceC">
                                <tr>
                                    <td align="center"> <c:out value="${invoiceC.grNo}"/></td> 
                                    <td align="center"> <c:out value="${invoiceC.freightAmount}"/></td> 
                                    <td align="center"> <c:out value="${invoiceC.detaintion}"/></td> 
                                    <td align="center"> <c:out value="${invoiceC.tollTax}"/></td> 
                                    <td align="center"> <c:out value="${invoiceC.weighment}"/></td> 
                                    <td align="center"> <c:out value="${invoiceC.otherCharge}"/></td> 
                                    <td align="center"> <c:out value="${invoiceC.invoiceAmount}"/></td> 
                                </tr>
                            </c:forEach>
                        </table>
                    <br/>
                    <br/>
                        </c:if>
                    <c:if test="${creditNoteDetailsSize > 0}">
                        <table class="table table-info">
                            <tr height="30" id="index">
                                <td colspan="9"  height="30" style="background-color:#5BC0DE;">
                                    &nbsp;&nbsp;Supply Created Credit</td>
                            </tr>

                            <tr>

                                <td align="center"><font color="red">*</font>GR No</td>
                                <td align="center"><font color="red">*</font>Freight Amount</td>
                                <td align="center"><font color="red">*</font>Detaintion</td>
                                <td align="center"><font color="red">*</font>Toll</td>
                                <td align="center"><font color="red">*</font>Weighment</td>
                                <td align="center"><font color="red">*</font>Other</td>
                                <td align="center"><font color="red">*</font>Total Credit Amount</td>
                            </tr>
                             <% int i=1; %>
                            <c:forEach items="${creditNoteDetails}" var="invoiceC">
                                <tr>
                                    <td align="center"> <c:out value="${invoiceC.grNo}"/></td> 
                                    <td align="center"> <c:out value="${invoiceC.freightAmount}"/></td> 
                                    <td align="center"> <c:out value="${invoiceC.detaintion}"/></td> 
                                    <td align="center"> <c:out value="${invoiceC.tollTax}"/></td> 
                                    <td align="center"> <c:out value="${invoiceC.weighment}"/></td> 
                                    <td align="center"> <c:out value="${invoiceC.otherCharge}"/></td> 
                                    <td align="center"> <c:out value="${invoiceC.creditAmount}"/></td> 
                                </tr>
                            </c:forEach>
                        </table>
                            
                    <br/>
                    <br/>
                        </c:if>
                    <c:if test="${invoiceDetails !=null}">
                        <table class="table table-info">
                            <tr height="30" id="index">
                                <td colspan="9"  height="30" style="background-color:#5BC0DE;">
                                    &nbsp;&nbsp;<spring:message code="finance.label.Credit"  text="default text"/></td>
                            </tr>

                            <tr>

                                <td align="center"><font color="red">*</font>GR No</td>
                                <td align="center"><font color="red">*</font><spring:message code="finance.label.InvoiceValue"  text="default text"/></td>
                                <td align="center"><font color="red">*</font>Freight Amount</td>
                                <td align="center"><font color="red">*</font>Detaintion</td>
                                <td align="center"><font color="red">*</font>Toll</td>
                                <td align="center"><font color="red">*</font>Weighment</td>
                                <td align="center"><font color="red">*</font>Other</td>
                                <td align="center"><font color="red">*</font><spring:message code="finance.label.CreditAmount"  text="default text"/></td>
                            </tr>
                            <% int i=1; %>
                            <c:forEach items="${invoiceDetails}" var="invoiceD">
                                <tr>

                                    <td> <c:out value="${invoiceD.grNo}"/><input  type ="hidden" id="grNo<%=i%>" name="grNo" value="<c:out value="${invoiceD.grNo}"/>"  class="form-control" style="width:140px;height:40px;">
                                        <input  type ="hidden" id="tripId<%=i%>" name="tripId" value="<c:out value="${invoiceD.tripId}"/>"  class="form-control" >
                                    </td>
                                    <td ><input type="text" name="grandTotal" onKeyPress="return onKeyPressBlockCharacters(event);" id="grandTotal<%=i%>" readonly  class="form-control" value="<c:out value="${invoiceD.invoiceAmount}"/>" style="width:140px;height:40px;"></td>
                                    <td >
                                        <input type="hidden" name="freightAmountOld" id="freightAmountOld<%=i%>"    class="form-control" value="<c:out value="${invoiceD.freightAmount }"/>" style="width:140px;height:40px;">
                                        <input type="text" name="freightAmount" onKeyPress="return onKeyPressBlockCharacters(event);" id="freightAmount<%=i%>" onChange="checkGreaterAmount('freightAmountOld','freightAmount',<%=i%>,this.value)"   class="form-control" value="0" style="width:140px;height:40px;"></td>
                                    <td >
                                        <input type="hidden" name="detaintionOld" id="detaintionOld<%=i%>"  class="form-control" value="<c:out value="${invoiceD.detaintion}"/>" style="width:140px;height:40px;">
                                        <input type="text" name="detaintion" onKeyPress="return onKeyPressBlockCharacters(event);" id="detaintion<%=i%>"  onChange="checkGreaterAmount('detaintionOld','detaintion',<%=i%>,this.value)" class="form-control" value="0" style="width:140px;height:40px;"></td>
                                    <td >
                                        <input type="hidden" name="tollTaxOld" id="tollTaxOld<%=i%>"  class="form-control" value="<c:out value="${invoiceD.tollTax}"/>" style="width:140px;height:40px;">
                                        <input type="text" name="toll" onKeyPress="return onKeyPressBlockCharacters(event);" id="toll<%=i%>"  class="form-control" onChange="checkGreaterAmount('tollTaxOld','toll',<%=i%>,this.value)" value="0" style="width:140px;height:40px;"></td>
                                    <td>   
                                <input type="hidden" name="weighmentOld" id="weighmentOld<%=i%>"  class="form-control" value="<c:out value="${invoiceD.weighment}"/>" style="width:140px;height:40px;"> 
                                        <input type="text" name="weighment" onKeyPress="return onKeyPressBlockCharacters(event);" id="weighment<%=i%>"  class="form-control" onChange="checkGreaterAmount('weighmentOld','weighment',<%=i%>,this.value)" value="0" style="width:140px;height:40px;"></td>
                                    <td >
                                        <input type="hidden" name="otherOld" id="otherOld<%=i%>"  class="form-control" value="<c:out value="${invoiceD.otherCharge }"/>" style="width:140px;height:40px;">
                                        <input type="text" name="other" onKeyPress="return onKeyPressBlockCharacters(event);" id="other<%=i%>" onChange="checkGreaterAmount('otherOld','other',<%=i%>,this.value)" class="form-control" value="0" style="width:140px;height:40px;"></td>
                                    
                                    <input type="hidden" name="pendingAmount" id="pendingAmount<%=i%>" readonly class="form-control" value="<c:out value="${invoiceD.invoiceAmount}"/>" style="width:140px;height:40px;">
                                    <td ><input type="text" name="payAmount" onKeyPress="return onKeyPressBlockCharacters(event);" id="payAmount<%=i%>" readonly class="form-control" value="0" onchange="setAmount();" onKeyPress='return onKeyPressBlockCharacters(event);' style="width:140px;height:40px;"></td>
                                </tr>
                                <% i++;%>
                            </c:forEach>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>Total Credit Amount</td>
                                <td><input type="text" readonly id="totalCreditAmount" name="totalCreditAmount" value="0.00"/></td>
                            </tr>
                        </table>

                        <script type="text/javascript">
                            function checkGreaterAmount(id,idNew,sno,value) {
                               var oldAmount = document.getElementById(id+sno).value;
                               var newAmount = document.getElementById(idNew+sno).value;
                               if(newAmount == ''){
                                 document.getElementById(idNew+sno).value = 0;  
                                 newAmount = document.getElementById(idNew+sno).value;
                               }
                               if(parseFloat(newAmount) > parseFloat(oldAmount)){
                                   alert("The "+idNew+" amount should not be greater than invoice amount");
                                   document.getElementById(idNew+sno).value = oldAmount;
                               }
                               callSumAmount(sno);
                                
                            }
                            function callSumAmount(sno){
                                var totalAMount = parseFloat(document.getElementById("freightAmount"+sno).value)
                                + parseFloat(document.getElementById("detaintion"+sno).value) + parseFloat(document.getElementById("toll"+sno).value)
                                + parseFloat(document.getElementById("weighment"+sno).value)
                                + parseFloat(document.getElementById("other"+sno).value);
                                document.getElementById("payAmount"+sno).value = parseFloat(totalAMount).toFixed(2);
                                setAmount();
                            }
                        </script>
                        
                    </c:if>
                    <script type="text/javascript">
                        function setAmount() {
                            //   var value1 = document.getElementById("pendingAmount"+i).value;
                            var pay = document.getElementsByName("payAmount");
                            // var value2 = document.getElementById("payAmount"+i).value;

                            var totalCreditAmount = 0.00;
                            for (var j = 1; j <= pay.length; j++) {
                                if (document.getElementById("payAmount" + j).value != "") {
                                    totalCreditAmount += parseFloat(document.getElementById("payAmount" + j).value);
                                }
                            }

                            document.getElementById("totalCreditAmount").value = totalCreditAmount;
                            if(parseFloat(document.getElementById("totalCreditAmount").value) > parseFloat(document.getElementById("totInvoiceAmt").value)){
                                   alert("The total credit amount should not be greater than invoice amount");
                                   document.getElementById("totalCreditAmount").value = document.getElementById("totInvoiceAmt").value;
                               }
                                $("#saveCredit").show();
                                if(parseFloat(document.getElementById("totalCreditAmount").value) <= 0)
                                {
                                  $("#saveCredit").hide();  
                                }

                        }


                        function setGRNoList(val) {
                            var tempInvoice = document.getElementById("invoiceIdTemp").value;
                            var invoice = tempInvoice.split("~");
                            document.getElementById("gstType").value = invoice[7];
                            document.manufacturer.action = "/throttle/supplyCreditNoteDetails.do?totalInvoiceAmount=" + invoice[1] + "&invoiceDate=" + invoice[6] + "&invoiceId=" + invoice[0] + "&gstType="+ invoice[7];
                            document.manufacturer.submit();
//                var temp=val.split("~");
//                $.ajax({
//                    url: '/throttle/getInvoiceGRList.do',
//                    data: {invoiceId: temp[0]},
//                    dataType: 'json',
//                    success: function (data) {
////                        $.each(data, function (i, data) {
////                            $('#grNo').append(
////                                    $('<option style="width:150px"></option>').val(data.Id).html(data.Name)
////                                    )
////                        });
//                    }
//                });

                        }
                    </script>
                    
                      <br>
                             <br>
                             <div><center>
                                 Financial Year : 
                                 <select name="finYear" id="finYear"  class="form-control" style="width:240px">
                                     <!--<option value="">-Select Any One -</option>-->
                                     <!--<option value="2122">2021-22</option>-->
                                     <!--<option value="2223">2022-23</option>-->
                                     <option value="2324" selected>2023-24</option>
                                     <!--<option value="2021">2020-21</option>-->
                                     <!--<option value="1920">2019-20</option>-->
                                     <!--<option value="1819">2018-19</option>-->
                                 </select>
                                 </center>
                            </div>
                            <br>
                            <br>
                    
                    <center>
                        <input type="button" class="btn btn-success" id="saveCredit" name="saveCredit" style="width:100px;height:35px;padding:2px; " value="saveCredit" onclick="submitPage();" />
                    </center>   

            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
