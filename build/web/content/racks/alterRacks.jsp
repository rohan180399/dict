<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
     <%@page language="java" contentType="text/html; charset=UTF-8"%>
 <%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ page import="ets.domain.racks.business.RackTO" %>  
<script language="javascript" src="/throttle/js/validate.js"></script>

<script>
    function submitPage()
    {
         var checValidate = selectedItemValidation();                
    }
    function setSelectbox(i){
    var selected=document.getElementsByName("selectedIndex") ;
    selected[i].checked = 1;
} 
function selectedItemValidation(){
var index = document.getElementsByName("selectedIndex");
var rackName = document.getElementsByName("rackNameList");
var rackDescription = document.getElementsByName("rackDescriptionList");
var rackStatus = document.getElementsByName("rackStatusList");
var chec=0;
for(var i=0;(i<index.length && index.length!=0);i++){
if(index[i].checked){
chec++;
if(textValidation(rackName[i],'Rack name')){       
        return;
   }     
 if(textValidation(rackDescription[i],'Rack Description')){ 
        return;
   }     
 if(textValidation(rackStatus[i],'Rack Status')){ 
        return;
   }  
   document.alterracks.action='/throttle/handleUpdateRack.do';
   document.alterracks.submit();
}
}
if(chec == 0){
alert("Please Select Any One And Then Proceed");
rackName[0].focus();
}
}
</script>

 <script>
   function changePageLanguage(langSelection){

            if(langSelection== 'ar'){
            document.getElementById("pAlign").style.direction="rtl";
            }else if(langSelection== 'en'){
            document.getElementById("pAlign").style.direction="ltr";
            }
        }

    </script>

        
        
 <div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.Racks"  text="Racks"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="general.label.stores"  text="Stores"/></a></li>
          <li class="active"><spring:message code="stores.label.Racks"  text="Racks"/></li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">
<body>
<form name="alterracks" method="post" >


<table class="table table-info mb30 table-hover" >
    <thead>
<tr>
<th>
    <spring:message code="stores.label.RackId"  text="default text"/>
    </th>
    <th ><spring:message code="stores.label.RackName"  text="default text"/></th>
    <th ><spring:message code="stores.label.RackDescription"  text="default text"/></th>
    <th ><spring:message code="stores.label.Status"  text="default text"/></th>
    <th ><spring:message code="stores.label.Select"  text="default text"/></th>
</tr>
    </thead>
<% int index=0; %>
<c:if test = "${rackLists != null}" >
 <c:forEach items="${rackLists}" var="rack"> 
  <%
    String classText = "";
    int oddEven = index % 2;
    if (oddEven > 0) {
    classText = "text2";
    } else {
    classText = "text1";
    }
    %>
<tr>
<td  height="30"><input type="hidden" class="form-control" style="width:260px;height:40px;" name="rackIdList" value="<c:out value='${rack.rackId}'/>"><c:out value='${rack.rackId}'/></td>
<td  height="30"><input type="text" class="form-control" style="width:260px;height:40px;" name="rackNameList" value="<c:out value='${rack.rackName}'/>" onchange="setSelectbox('<%= index %>');"></td>
<td  height="30"><textarea class="form-control" style="width:260px;height:40px;" name="rackDescriptionList" onchange="setSelectbox('<%= index %>');" > <c:out value='${rack.rackDescription}'/></textarea></td>
<td  height="30"><select class="form-control" style="width:260px;height:40px;" name="rackStatusList" onchange="setSelectbox('<%= index %>');">
<c:if test="${(rack.rackStatus=='n') || (rack.rackStatus=='N')}" >
 <option value="Y" ><spring:message code="stores.label.Active"  text="default text"/>
</option><option value="N" selected><spring:message code="stores.label.InActive"  text="default text"/>
</option>                           
 </c:if>   
<c:if test="${(rack.rackStatus=='y') || (rack.rackStatus=='Y')}" >
 <option value="Y" selected><spring:message code="stores.label.Active"  text="default text"/></option><option value="N"><spring:message code="stores.label.InActive"  text="default text"/></option>
</c:if>
</select></td>
<td width="37" height="30" ><input type="checkbox" name="selectedIndex" value='<%= index %>'></td>
</tr>
 <%
    index++;
  %>
</c:forEach>
  </c:if> 
</tr>
</table>
<br>
<center>
<input type="button" value="<spring:message code="stores.label.Save"  text="default text"/>" class="btn btn-success" onclick="submitPage();">
</center>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>

 </div>


    </div>
    </div>





<%@ include file="/content/common/NewDesign/settings.jsp" %>