

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
    </head>
    
    <script>
  function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}
       function saveExcel(){
           document.receivedStock.action='/throttle/handleStockPurchaseExcelRep.do';
           document.receivedStock.submit();                      
       }           
        
       function saveRcExcel(){
           document.receivedStock.action='/throttle/handleRcBillExcelRep.do';
           document.receivedStock.submit();                      
       }           
        
        
       function submitPage(){
            var chek=validation();
            
            if(chek!='true'){  
                return;
            }    
            if(document.receivedStock.orderType.value == 'PO'){  
               document.receivedStock.action='/throttle/handleStockPurchase.do';
           }
           else if(document.receivedStock.orderType.value == 'WO'){  
               document.receivedStock.action='/throttle/handleRcBillsReport.do';
           }    
           document.receivedStock.submit();           
           }
             function validation(){
           
            if(textValidation(document.receivedStock.fromDate,'From Date')){
                return 'false';
            }
            else  if(textValidation(document.receivedStock.toDate,'TO Date')){
                return'false';
            }
                
            return 'true';
        }
        function newWindow(indx){
            var supplyId=document.getElementsByName("supplyId");
            window.open('/throttle/viewGRN.do?supplyId='+supplyId[indx].value, 'PopupPage', 'height=450,width=700,scrollbars=yes,resizable=yes');
        }
         function setValues(){           
           
                 if('<%=request.getAttribute("vendorId")%>'!='null'){
                document.receivedStock.vendorId.value='<%=request.getAttribute("vendorId")%>';                                
                }           
                 if('<%=request.getAttribute("fromDate")%>'!='null'){
                document.receivedStock.fromDate.value='<%=request.getAttribute("fromDate")%>';                                
                }
                 if('<%=request.getAttribute("toDate")%>'!='null'){
                document.receivedStock.toDate.value='<%=request.getAttribute("toDate")%>';                                
                }
                 if('<%=request.getAttribute("billType")%>'!='null'){
                document.receivedStock.billType.value='<%=request.getAttribute("billType")%>';                                
                }
                 if('<%=request.getAttribute("orderType")%>'!='null'){
                document.receivedStock.orderType.value='<%=request.getAttribute("orderType")%>';                                
                }
                 if('<%=request.getAttribute("companyId")%>'!='null'){
                document.receivedStock.companyId.value='<%=request.getAttribute("companyId")%>';                                
                }
                 if('<%=request.getAttribute("purpose")%>'!='null'){
                document.receivedStock.purpose.value='<%=request.getAttribute("purpose")%>';                                
                }
            }
            
            
    function printPag()
    {       
        var DocumentContainer = document.getElementById('print');
        var WindowObject = window.open('', "TrackHistoryData", 
            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        WindowObject.close();   
    }                        
            
            
    </script>
    <body onload="setValues();">
        <form name="receivedStock" method="post">
            <%@ include file="/content/common/path.jsp" %>           
            <%@ include file="/content/common/message.jsp" %>
            
            
<table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:850;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">Received Stock Report</li>
    </ul>
    <div id="first">
    <table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
  <tr>
                    <td>&nbsp;&nbsp;Vendor</td>
                    <td><select name="vendorId" class="form-control" style="width:125px">
                            <option value="0">-select-</option>
                            <c:if test = "${VendorLists != null}" >
                                <c:forEach items="${VendorLists}" var="vendor">
                                    <option value="<c:out value="${vendor.vendorId}"/>"><c:out value="${vendor.vendorName}"/></option>
                                </c:forEach>
                            </c:if>

                    </select></td>
                  <td  height="30"  >&nbsp;&nbsp;Bill Type</td>
                    <td  height="30"  >
                        <select name="billType" class="form-control" >
                            <option value=''> Select </option>
                            <option value='DC'> DC</option>
                        </select>
                    </td>
                     <td>Order Type</td>
                  <td  height="30"><select name="orderType" class="form-control" >
                          <option value="PO" > Purchase Order </option>
                          <option value="WO" > Work Order </option>
                  </select>
                  </td>
                <td >Purpose</td>
                      <td  height="30"><select name="purpose" class="form-control" >
                              <option value="0" > -Select-</option>
                              <option value="STORE" > STORE</option>
                              <option value="VEHICLE" > VEHICLE </option>
                      </select>
                      </td>

                </tr>
                <tr>
                    <td  height="30"><font color="red">*</font>From Date</td>
                    <td ><input name="fromDate" type="text" class="form-control" value="" size="20">
                    <span><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.receivedStock.fromDate,'dd-mm-yyyy',this)"/></span></td>
                    <td ><font color="red">*</font>To Date</td>
                    <td  height="30"><input name="toDate" type="text" class="form-control" value="" size="20">
                    <span><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.receivedStock.toDate,'dd-mm-yyyy',this)"/></span></td>
               
                  
                    <td height="30"><font color="red">*</font>Service Point</td>
                    <td height="30">
                            <select class='form-control' id='companyId'  name='companyId' >
                            <option selected   value=''>---Select---</option>
                            <c:if test = "${servicePointList != null}" >
                            <c:forEach items="${servicePointList}" var="spList">
                                <option  value='<c:out value="${spList.spId}" />'><c:out value="${spList.spName}" /> </option>
                            </c:forEach >
                            </c:if>
                            </select>
                    </td>
                    <td colspan="2"><input type="button" name="" value="Search" class="button" height="30" onclick="submitPage();" ></td>
                </tr>
               
    </table>
    </div></div>
    </td>
    </tr>
    </table>                  
            <br>
                 <c:if test = "${itemList != null}" >
            <table width="900" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" height="30" class="border">
                   <!--DWLayoutTable-->
                   <tr>
                    <td class="contenthead" height="30" colspan="8" align="center"  >Received Stock Report</td>
                </tr>
                <tr>
                    <td  class="contentsub" height="30" >SNO</td>
                    <td  class="contentsub" height="30" >PO No</td>
                    <td  class="contentsub" height="30" >PO DATE</td>
                    <td  class="contentsub" height="30" >INVOICE NO</td>
                    <td  class="contentsub" height="30" >INVOICE DATE</td>
                    <td  class="contentsub" height="30" >VENDOR</td>                                             
                    <td  class="contentsub" height="30" >DC NO</td>                    
                    <td  class="contentsub" height="30" >ITEM NAME</td>    
                    <td  class="contentsub" height="30" >REQ QTY</td>                                                            
                    <td  class="contentsub" height="30" >RCVD QTY</td>                                                            
                    <td  class="contentsub" height="30" >PRICE</td>                                                                         
                    <td  class="contentsub" height="30" >TOTAL AMOUNT</td>
                    <td  class="contentsub" height="30" >Vehicle No</td>
                </tr>  
                  <% int index = 0;%> 
                    <c:set var="poId" value="0" />
                    <c:forEach items="${itemList}" var="stock"> 
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>	
                    

                        
                    <c:if test="${stock.poId == poId}" >                       
                <tr>
                    <td class="<%=classText%>" height="30" > 
                    &nbsp; <input type="hidden" name="sno" value='' >  </td>
                    <td class="<%=classText%>" height="30" >
                    &nbsp; <input type="hidden" name="poId" value='' > </td>
                    
                     <input type="hidden" name="companyName" value='' > 
                    <td class="<%=classText%>" height="30" >
                    &nbsp; <input type="hidden" name="invoiceNo" value='' > </td>
                    <td class="<%=classText%>" height="30" >
                    &nbsp; <input type="hidden" name="dcNo" value='' > </td>
                    <td class="<%=classText%>" height="30" >
                    &nbsp; <input type="hidden" name="poDate" value='' > </td>
                    <td class="<%=classText%>" height="30" >
                    &nbsp; <input type="hidden" name="billDate" value='' > </td>
                    <td class="<%=classText%>" height="30" >
                    &nbsp; <input type="hidden" name="vendorName" value='' > </td>
                    
                    
                     <input type="hidden" name="paplCode" value='<c:out value="${stock.paplCode}"/>' > 
                    
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.itemName}"/>  <input type="hidden" name="itemName" value='<c:out value="${stock.itemName}"/>' > </td>
                    
                    
                      <input type="hidden" name="uom" value='<c:out value="${stock.uomName}"/>' > 
                    
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.quantity}"/>  <input type="hidden" name="rqty" value='<c:out value="${stock.quantity}"/>' > </td>                    
                    
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.aqty}"/> <input type="hidden" name="aqty" value='<c:out value="${stock.aqty}"/>' > </td>
                    
                    
                  
                    
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.itemPrice}"/> <input type="hidden" name="price" value='<c:out value="${stock.itemPrice}"/>' > </td>
                 
                    <td class="<%=classText%>" height="30" >
                    &nbsp; <input type="hidden" name="totalAmount" value='' > </td>
                    <td class="<%=classText%>" height="30" >
                     &nbsp; <input type="hidden" name="remarks" value='' > </td>
                 
                </tr>
                
                    </c:if>




                     <c:if test="${ (stock.poId != poId)  }" >   
                <tr>
                    
                    <c:if test="${ (stock.supplyId == '-')  }" >   
                    <td class="<%=classText%>" height="30" > 
                    <img width="15px" height="15px" src="/throttle/images/flag2.gif" align="middle" border="0"> &nbsp; <%= index+1 %> <input type="hidden" name="sno" value='<%= index+1 %>' >  </td>
                    </c:if>
                    <c:if test="${ (stock.supplyId != '-')  }" >   
                    <td class="<%=classText%>" height="30" > 
                    <%= index+1 %> <input type="hidden" name="sno" value='<%= index+1 %>' >  </td>
                    </c:if>
                    
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.poId}"/> <input type="hidden" name="poId" value='<c:out value="${stock.poId}"/>' > </td>
                    
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.purDate}"/><input type="hidden" name="poDate" value='<c:out value="${stock.purDate}"/>' > </td>
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.invoiceId}"/> <input type="hidden" name="invoiceNo" value='<c:out value="${stock.invoiceId}"/>' > </td>
                    
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.billDate}"/><input type="hidden" name="billDate" value='<c:out value="${stock.billDate}"/>' > </td>
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.vendorName}"/> <input type="hidden" name="vendorName" value='<c:out value="${stock.vendorName}"/>' > </td>
                    
                   
                    <input type="hidden" name="companyName" value='<c:out value="${stock.companyName}"/>' > 
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.dcNo}"/> <input type="hidden" name="dcNo" value='<c:out value="${stock.dcNo}"/>' > </td>
                    
                    <input type="hidden" name="paplCode" value='<c:out value="${stock.paplCode}"/>' > 
                    
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.itemName}"/>  <input type="hidden" name="itemName" value='<c:out value="${stock.itemName}"/>' > </td>
                    
                    <input type="hidden" name="uom" value='<c:out value="${stock.uomName}"/>' > 
                    
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.quantity}"/>  <input type="hidden" name="rqty" value='<c:out value="${stock.quantity}"/>' > </td>                    
                    
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.aqty}"/> <input type="hidden" name="aqty" value='<c:out value="${stock.aqty}"/>' > </td>
                    
                     
                     
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.itemPrice}"/> <input type="hidden" name="price" value='<c:out value="${stock.itemPrice}"/>' > </td>
                    
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.billAmount}"/> <input type="hidden" name="totalAmount" value='<c:out value="${stock.billAmount}"/>' > </td>                    
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.remarks}"/> <input type="hidden" name="remarks" value='<c:out value="${stock.remarks}"/> ' > </td>
                 
                </tr>
                    <% index++;%>
                    </c:if>
                                                           
                    
             <c:set var="poId" value="${stock.poId}" />
                    </c:forEach>
                </table>                
                       
<br>
            <center> <input type="button" name="print1" value="Generate Excel" class="button" onClick="saveExcel();" > </center>
            </c:if>
            <br>              
<!--  ***************************************************************** -->


                 <c:if test = "${rcItemList != null}" >

            <table width="900" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" height="30" class="border">
                   <!--DWLayoutTable-->
                   <tr>
                    <td class="contenthead" height="30" colspan="8" align="center"  >Received Stock Report</td>
                </tr>
                <tr>
                    <td  class="contentsub" height="30" >SNO</td>
                    <td  class="contentsub" height="30" >WO No</td>
                    <td  class="contentsub" height="30" >CREATED DATE</td>
                    <td  class="contentsub" height="30" >INVOICE NO</td>
                    <td  class="contentsub" height="30" >INVOICE DATE</td>
                    <td  class="contentsub" height="30" >VEHICLE NO</td>                    
                    <td  class="contentsub" height="30" >VENDOR</td>                                                                                                  
                    <td  class="contentsub" height="30" >ITEM NAME</td>                                                                                           
                    <td  class="contentsub" height="30" >QTY</td>                                                                                                                
                    <td  class="contentsub" height="30" >REF NO</td>
                    <td  class="contentsub" height="30" >Mat Cost Int</td>
                    <td  class="contentsub" height="30" >Mat Cost Ext</td>
                    <td  class="contentsub" height="30" >Labor Charge</td>
                    <td  class="contentsub" height="30" >PRICE</td>                                        
                    <td  class="contentsub" height="30" >TOTAL</td>
                    
                </tr>  
                  <% int index = 0;%>
                    <c:set var="supplyId" value="0" />
                    <c:forEach items="${rcItemList}" var="stock"> 
                        <%
         String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>	
                    

                        
                    <c:if test="${stock.rcWoId == rcWoId }" >                       
                <tr>
                    <td class="<%=classText%>" height="30" >
                    <input type="hidden" name="sno" value='' >  </td>
                    <td class="<%=classText%>" height="30" >
                    <input type="hidden" name="rcWoId" value='' > </td>
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.createdDate}"/> <input type="hidden" name="createdDate" value='<c:out value="${stock.createdDate}"/>' > </td>
                    <td class="<%=classText%>" height="30" >
                    <input type="hidden" name="invoiceNo" value='' > </td>
                    <td class="<%=classText%>" height="30" >
                    <input type="hidden" name="billDate" value='' > </td>
                     <td class="<%=classText%>" height="30" >
                    <input type="hidden" name="vehicleNo" value='' > </td>
                   
                    <td class="<%=classText%>" height="30" >
                    <input type="hidden" name="vendorName" value='' > </td>
                    
                     <input type="hidden" name="paplCode" value='<c:out value="${stock.paplCode}"/>' > 
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.itemName}"/>  <input type="hidden" name="itemName" value='<c:out value="${stock.itemName}"/>' > </td>
                    
                      <input type="hidden" name="uom" value='<c:out value="${stock.uomName}"/>' > 
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.aqty}"/> <input type="hidden" name="aqty" value='<c:out value="${stock.aqty}"/>' > </td>
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.tyreNo}"/> <input type="hidden" name="tyreNo" value='<c:out value="${stock.tyreNo}"/>' > </td>
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.materialCostInternal}"/> <input type="hidden" name="materialCostInternal" value='<c:out value="${stock.materialCostInternal}"/>' > </td>
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.materialCostExternal}"/> <input type="hidden" name="materialCostExternal" value='<c:out value="${stock.materialCostExternal}"/>' > </td>
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.laborCharge}"/> <input type="hidden" name="laborCharge" value='<c:out value="${stock.laborCharge}"/>' > </td>
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.itemPrice}"/> <input type="hidden" name="price" value='<c:out value="${stock.itemPrice}"/>' > </td>
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.totalAmount}"/> <input type="hidden" name="totalAmount" value='<c:out value="${stock.totalAmount}"/>' > </td>
                    
                </tr>                
                    </c:if>




                     <c:if test="${ (stock.rcWoId != rcWoId )  }" >   
                <tr>
                    
                    <td class="<%=classText%>" height="30" >
                    <%= index+1 %>
                    <input type="hidden" name="sno" value='<%= index+1 %>' >  </td>
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.rcWoId}"/> <input type="hidden" name="rcWoId" value='<c:out value="${stock.rcWoId}"/>' > </td>
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.createdDate}"/> <input type="hidden" name="createdDate" value='<c:out value="${stock.createdDate}"/>' > </td>
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.invoiceId}"/> <input type="hidden" name="invoiceNo" value='<c:out value="${stock.invoiceId}"/>' > </td>
                   
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.billDate}"/><input type="hidden" name="billDate" value='<c:out value="${stock.billDate}"/>' > </td>
                     <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.vehicleNo}"/> <input type="hidden" name="vehicleNo" value='<c:out value="${stock.vehicleNo}"/>' > </td>
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.vendorName}"/> <input type="hidden" name="vendorName" value='<c:out value="${stock.vendorName}"/>' > </td>
                    
                     <input type="hidden" name="paplCode" value='<c:out value="${stock.paplCode}"/>' > 
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.itemName}"/>  <input type="hidden" name="itemName" value='<c:out value="${stock.itemName}"/>' > </td>
                    
                      <input type="hidden" name="uom" value='<c:out value="${stock.uomName}"/>' > 
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.aqty}"/> <input type="hidden" name="aqty" value='<c:out value="${stock.aqty}"/>' > </td>
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.tyreNo}"/> <input type="hidden" name="tyreNo" value='<c:out value="${stock.tyreNo}"/>' > </td>
                    
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.materialCostInternal}"/> <input type="hidden" name="materialCostInternal" value='<c:out value="${stock.materialCostInternal}"/>' > </td>
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.materialCostExternal}"/> <input type="hidden" name="materialCostExternal" value='<c:out value="${stock.materialCostExternal}"/>' > </td>
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.laborCharge}"/> <input type="hidden" name="laborCharge" value='<c:out value="${stock.laborCharge}"/>' > </td>
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.itemPrice}"/> <input type="hidden" name="price" value='<c:out value="${stock.itemPrice}"/>' > </td>
                    <td class="<%=classText%>" height="30" >
                    <c:out value="${stock.totalAmount}"/> <input type="hidden" name="totalAmount" value='<c:out value="${stock.totalAmount}"/>' > </td>
                    
                </tr>
                    <% index++;%>
                    </c:if>
                                                           
                    
             <c:set var="rcWoId" value="${stock.rcWoId}" />
                    </c:forEach>
                </table>                
                       
<br>
            <center> <input type="button" name="print2" value="Generate Excel" class="button" onClick="saveRcExcel();" > </center>
            </c:if>
            
            
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
