<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>-->

<script type="text/javascript">
    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
</script>

<style>

    .loading {

        border:10px solid red;

        display:none;

    }

    .button1{border: 1px black solid ; color:#000 !important;  }



    .spinner{

        position: fixed;

        top: 50%;

        left: 50%;

        margin-left: -50px; /* half width of the spinner gif */

        margin-top: -50px; /* half height of the spinner gif */

        text-align:center;

        z-index:1234;

        overflow: auto;

        width: 300px; /* width of the spinner gif */

        height: 300px; /*hight of the spinner gif +2px to fix IE8 issue */

    }

    .ui-loader-background {

        width:100%;

        height:100%;

        top:0;

        padding: 0;

        margin: 0;

        background: rgba(0, 0, 0, 0.3);

        display:none;

        position: fixed;

        z-index:100;

    }

    .ui-loader-background {

        display:block;



    }



</style>
<script type="text/javascript">
//google Map Plot Script
    var rendererOptions = {
        draggable: true
    };
    var directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);
    ;
    var directionsService = new google.maps.DirectionsService();
    var map;



//var alkhail = new google.maps.LatLng(25.15, 55.26);

    function initialize() {
//alert("hi...1");
        var mapOptions = {
            zoom: 7
//    center: alkhail
        };
        map = new google.maps.Map(document.getElementById('mapdiv'), mapOptions);
        directionsDisplay.setMap(map);
        directionsDisplay.setPanel(document.getElementById('directionsPanel'));

        google.maps.event.addListener(directionsDisplay, 'directions_changed', function () {
            computeTotalDistance(directionsDisplay.getDirections());
        });

        calcRoute();

    }





    function calcRoute() {
        var routePoint = document.getElementById("routePoints").value;

        var routePoints = routePoint.split('@');
        var routePointsLength = routePoints.length;

        var waypts = [];
        for (var j = 1; j <= routePoints.length - 2; j++) {
            //alert('"'+routePoints[j].split("~")[1],routePoints[j].split("~")[2] +'"');
            if ('"' + routePoints[j].split("~")[1] + ',' + routePoints[j].split("~")[2] + '"' != '') {
                waypts.push({
                    location: new google.maps.LatLng(routePoints[j].split("~")[1], routePoints[j].split("~")[2]),
                    stopover: true});
            }
        }



        //alert('"'+routePoints[0].split("~")[1]+','+routePoints[0].split("~")[2] +'"');
        //alert('"'+routePoints[routePointsLength-1].split("~")[1]+','+routePoints[routePointsLength-1].split("~")[2] +'"');
        //var start = '"'+routePoints[0].split("~")[1]+', '+routePoints[0].split("~")[2] +'"';
        //var end = '"'+routePoints[routePointsLength-1].split("~")[1]+', '+routePoints[routePointsLength-1].split("~")[2] +'"';
        var start = new google.maps.LatLng(routePoints[0].split("~")[1], routePoints[0].split("~")[2]);
        var end = new google.maps.LatLng(routePoints[routePointsLength - 1].split("~")[1], routePoints[routePointsLength - 1].split("~")[2]);
        //alert(start);
        //alert(end);
        var request = {
            origin: start,
            destination: end,
            waypoints: waypts,
            optimizeWaypoints: false,
            travelMode: google.maps.TravelMode.DRIVING
        };
        directionsService.route(request, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
            }
        });
    }

    function computeTotalDistance(result) {
        var total = 0;
        var myroute = result.routes[0];
        for (var i = 0; i < myroute.legs.length; i++) {
            total += myroute.legs[i].distance.value;
        }
        total = total / 1000.0;
        //document.getElementById('mapdistance').innerHTML = 'Total distance in km' + total;
        //document.getElementById('totalRouteKm').innerHTML ='Total Route Km: '+total;
        document.getElementById("googleDistance").value = total;

    }




// Script End

    function setRCMForVehicleType() {
        var travelKm = document.getElementById("googleDistance").value;
        alert(travelKm);
        var vehicleTypeId = document.getElementsByName("vehicleTypeId");
        for (var m = 0; m <= vehicleTypeId.length; m++) {
            alert("im here11");
            var url = "/throttle/getRcmForVehicleType.do?travelKm=" + travelKm + "&vehicleTypeId=" + document.getElementById("vehicleTypeId" + m).value;
            alert("im here");
            if (window.ActiveXObject)
            {
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest)
            {
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function () {
                processAjax();
            };
            httpReq.send(null);

        }
    }
    function processAjax()
    {
        if (httpReq.readyState == 4)
        {
            if (httpReq.status == 200)
            {
                temp = httpReq.responseText.valueOf();

                document.getElementById("vehicleTypeExpense").innerHTML = parseFloat(temp);

            } else
            {
                alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
            }

        }



    }


    function viewConsignmentDetails(orderId) {
        window.open('/throttle/getConsignmentDetails.do?consignmentOrderId=' + orderId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }

    function viewConsignmentContainerDetails(orderId) {
        window.open('/throttle/viewConsignmentContainerDetails.do?consignmentOrderId=' + orderId, 'PopupPage', 'height = 800, width = 1400, scrollbars = yes, resizable = yes');
    }







//        function viewExpenseDetails(vehicleTypeName, vehicleTypeId, routeDistance, noOfVehicles){
//            //alert('am here');
//            //alert('am here...'+'/throttle/viewExpenseDetails.do?vehicleTypeId='+vehicleTypeId+'&noOfVehicles='+noOfVehicles+'&routeDistance='+routeDistance+'&vehicleTypeName='+vehicleTypeName);
//            window.open('/throttle/viewExpenseDetails.do?vehicleTypeId='+vehicleTypeId+'&noOfVehicles='+noOfVehicles+'&routeDistance='+routeDistance+'&vehicleTypeName='+vehicleTypeName, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
//        }

    function viewExpenseDetails(vehicleTypeName, vehicleTypeId, routeDistance, noOfVehicles) {
        //alert('am here');
        //alert('am here...'+'/throttle/viewExpenseDetails.do?vehicleTypeId='+vehicleTypeId+'&noOfVehicles='+noOfVehicles+'&routeDistance='+routeDistance+'&vehicleTypeName='+vehicleTypeName);
        var temp = "";
        var selectedConsignment = document.getElementsByName('selectedIndex');
        var consignmentOrder = document.getElementsByName('consignmentOrderId');
        var cntr = 0;
        for (var i = 0; i < selectedConsignment.length; i++) {
            if (selectedConsignment[i].checked == true) {
                if (cntr == 0) {
                    temp = consignmentOrder[i].value;
                    // alert(temp);
                } else {
                    temp = temp + "," + consignmentOrder[i].value;
                    //   alert(temp);
                }
                cntr++;
            }
        }

        window.open('/throttle/viewExpenseDetails.do?vehicleTypeId=' + vehicleTypeId + '&noOfVehicles=' + noOfVehicles + '&routeDistance=' + routeDistance + '&vehicleTypeName=' + vehicleTypeName + '&temp=' + temp, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }

</script>

<script>
    $(document).ready(function () {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#cityTo').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getCityNameList.do",
                    dataType: "json",
                    data: {
                        cityName: request.term
                    },
                    success: function (data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function (data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                $("#cityTo").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var Name = ui.item.Name;
                var Id = ui.item.Id;
                $itemrow.find('#destinationPoint').val(Id);
                $itemrow.find('#cityTo').val(Name);
                return false;
            }
        }).data("autocomplete")._renderItem = function (ul, item) {
            var itemVal = item.Name;
            var itemId = item.Id;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    });


    function resetCityId() {
        if (document.getElementById('destinationPoint').value == '') {
            document.getElementById('cityTo').value = '';
        }
    }
</script>



<script type="text/javascript">
    function setPickupType() {
        var pickupType = document.getElementById("pickupType").value;
        if (parseInt(pickupType) == 1) {
            $("#pickupRadiusName").hide();
            $("#pickupRadiusId").hide();
        } else if (parseInt(pickupType) == 2) {
            $("#pickupRadiusName").show();
            $("#pickupRadiusId").show();
        } else if (parseInt(pickupType) == 0) {
            $("#pickupRadiusName").hide();
            $("#pickupRadiusId").hide();
        } else {
            $("#pickupRadiusName").hide();
            $("#pickupRadiusId").hide();
        }
    }
    function setDestinationType() {
        var destinationType = document.getElementById("destinationType").value;
        if (parseInt(destinationType) == 1) {
            $("#destinationRadiusName").hide();
            $("#destinationRadiusId").hide();
        } else if (parseInt(destinationType) == 2) {
            $("#destinationRadiusName").show();
            $("#destinationRadiusId").show();
        } else if (parseInt(destinationType) == 0) {
            $("#destinationRadiusName").hide();
            $("#destinationRadiusId").hide();
        } else {
            $("#destinationRadiusName").hide();
            $("#destinationRadiusId").hide();
        }
    }
    function setProductId() {
        var productCategory = document.getElementById("productCategory").value;
        var temp = productCategory.split("~");
        document.getElementById("productCategoryId").value = temp[0];
    }
    function setValue() {
        document.getElementById('pickupRadius').value = "";
        $("#pickupRadiusName").hide();
        $("#pickupRadiusId").hide();
        $("#destinationRadiusName").hide();
        $("#destinationRadiusId").hide();
        document.getElementById('destinationRadius').value = "";
        if ('<%=request.getAttribute("cityTo")%>' != 'null') {
            document.getElementById('cityTo').value = '<%=request.getAttribute("cityTo")%>';
        }
        if ('<%=request.getAttribute("cityFrom")%>' != 'null') {
            document.getElementById('cityFrom').value = '<%=request.getAttribute("cityFrom")%>';
        }
        if ('<%=request.getAttribute("zoneId")%>' != 'null') {
            document.getElementById('zoneId').value = '<%=request.getAttribute("zoneId")%>';
        }
        if ('<%=request.getAttribute("summaryOption")%>' != 'null') {
            document.getElementById('summaryOption').value = '<%=request.getAttribute("summaryOption")%>';
        }
        if ('<%=request.getAttribute("pickupType")%>' != 'null') {
            document.getElementById('pickupType').value = '<%=request.getAttribute("pickupType")%>';
        }
        if ('<%=request.getAttribute("pickPoint")%>' != 'null') {
            document.getElementById('pickPoint').value = '<%=request.getAttribute("pickPoint")%>';
        }
        if ('<%=request.getAttribute("pickupDate")%>' != 'null') {
            document.getElementById('pickupDate').value = '<%=request.getAttribute("pickupDate")%>';
        }
        if ('<%=request.getAttribute("pickupDays")%>' != 'null') {
            document.getElementById('pickupDays').value = '<%=request.getAttribute("pickupDays")%>';
        }

        if ('<%=request.getAttribute("pickupRadius")%>' != 'null' && '<%=request.getAttribute("pickupRadius")%>' != '') {
            document.getElementById('pickupRadius').value = '<%=request.getAttribute("pickupRadius")%>';
            $("#pickupRadiusName").show();
            $("#pickupRadiusId").show();
        }

        if ('<%=request.getAttribute("destinationType")%>' != 'null') {
            document.getElementById('destinationType').value = '<%=request.getAttribute("destinationType")%>';
        }
        if ('<%=request.getAttribute("destinationPoint")%>' != 'null') {
            document.getElementById('destinationPoint').value = '<%=request.getAttribute("destinationPoint")%>';
        }
        if ('<%=request.getAttribute("arrivalDate")%>' != 'null') {
            document.getElementById('arrivalDate').value = '<%=request.getAttribute("arrivalDate")%>';
        }
        if ('<%=request.getAttribute("destinationDays")%>' != 'null') {
            document.getElementById('destinationDays').value = '<%=request.getAttribute("destinationDays")%>';
        }
        if ('<%=request.getAttribute("productCategoryId")%>' != 'null') {
            document.getElementById('productCategoryId').value = '<%=request.getAttribute("productCategoryId")%>';
        }
        if ('<%=request.getAttribute("productCategory")%>' != 'null') {
            document.getElementById('productCategory').value = '<%=request.getAttribute("productCategory")%>';
        }

        if ('<%=request.getAttribute("destinationRadius")%>' != 'null' && '<%=request.getAttribute("destinationRadius")%>' != '') {
            document.getElementById('destinationRadius').value = '<%=request.getAttribute("destinationRadius")%>';
            $("#destinationRadiusName").show();
            $("#destinationRadiusId").show();
        }

    }



</script>

<script type="text/javascript">
    var httpRequest;
    function getLocation() {
        var zoneid = document.getElementById("zoneId").value;
        if (zoneid != '') {

            // Use the .autocomplete() method to compile the list based on input from user
            $('#cityFrom').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "/throttle/getLocationName.do",
                        dataType: "json",
                        data: {
                            cityId: request.term,
                            zoneId: document.getElementById('zoneId').value
                        },
                        success: function (data, textStatus, jqXHR) {
                            var items = data;
                            response(items);
                        },
                        error: function (data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function (event, ui) {
                    var value = ui.item.Name;
                    //  alert(value);
                    var tmp = value.split('-');
                    $('#pickPoint').val(tmp[0]);
                    $('#cityFrom').val(tmp[1]);
                    return false;
                }
            }).data("autocomplete")._renderItem = function (ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[1] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };

        }
    }

</script>



<script type="text/javascript">
    $(document).ready(function () {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#customerName').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getCustomerDetails.do",
                    dataType: "json",
                    data: {
                        customerName: request.term
                    },
                    success: function (data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function (data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                $("#customerName").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('~');
                $('#customerId').val(tmp[0]);
                $('#customerName').val(tmp[1]);
                return false;
            }
        }).data("autocomplete")._renderItem = function (ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('~');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });
</script>
<script>
    $(document).on("click", ".show-page-loading-msg", function () {
        var $this = $(this),
                theme = $this.jqmData("theme") || $.mobile.loader.prototype.options.theme,
                msgText = $this.jqmData("msgtext") || $.mobile.loader.prototype.options.text,
                textVisible = $this.jqmData("textvisible") || $.mobile.loader.prototype.options.textVisible,
                textonly = !!$this.jqmData("textonly");
        html = $this.jqmData("html") || "";
        $.mobile.loading('show', {
            text: msgText,
            textVisible: textVisible,
            theme: theme,
            textonly: textonly,
            html: html
        });
    })
            .on("click", ".hide-page-loading-msg", function () {
                $.mobile.loading("hide");
            });
</script>
<script>


    function submitPageForTripGeneration() {
//      document.getElementById("spinner").style.display = "block";
//      document.getElementById("loader").style.display = "block";
//      document.getElementById('loader').className +=  'ui-loader-background';
        var temp = "";
        var containertemp = "";
        var ismultiPle = document.getElementsByName('multiPleTrip');
        var selectedConsignment = document.getElementsByName('selectedIndex');
        var containerId = document.getElementsByName('containerId');

        var consignmentOrder = document.getElementsByName('consignmentOrderId');
        var tripType = $("#tripType").val();
        var conOrderId = "";
        var flag = 0;
        //alert("1:"+selectedConsignment.length);
        for (var j = 0; j < ismultiPle.length; j++) {
            if (ismultiPle[j].checked == true) {
                conOrderId = document.getElementById('multiPleTrip' + j).value;
                flag = 1;
                break;
            }
        }

        var cntr = 0;
        for (var i = 0; i < selectedConsignment.length; i++) {
            if (selectedConsignment[i].checked == true) {
                if (cntr == 0) {
                    temp = consignmentOrder[i].value;
                } else {
                    temp = temp + "," + consignmentOrder[i].value;
                }
                cntr++;
            }
        }
        //for  conatiner Id
        var cntr1 = 0;
        for (var i = 0; i < selectedConsignment.length; i++) {
            if (selectedConsignment[i].checked == true) {
                if (cntr1 == 0) {
                    containertemp = containerId[i].value;
                } else {
                    containertemp = containertemp + "," + containerId[i].value;
                }
                cntr1++;
            }
        }
        //end
        if (cntr > 0) {
            if (flag == 1) {
                if (temp == conOrderId) {
                    //alert("am here..");
                    document.CNoteSearch.action = "/throttle/createTripSheet.do?consignmentOrderNos=" + temp + "&tripType=" + tripType + "&containerType=" + containertemp;
                    //alert("am here..1:"+document.CNoteSearch.action);
                    document.CNoteSearch.submit();
                } else {
                    alert("Please select Same checkBox");
                }
            } else
            {
                document.CNoteSearch.action = "/throttle/createTripSheet.do?consignmentOrderNos=" + temp + "&tripType=" + tripType + "&containerType=" + containertemp;
                //alert("am here..1:"+document.CNoteSearch.action);
                document.CNoteSearch.submit();
            }
        } else {
            alert("Please select consignment order and proceed");
        }
    }
    function searchPage(val) {
        var temp = "";
        var selectedConsignment = document.getElementsByName('selectedIndex');
        var consignmentOrder = document.getElementsByName('consignmentOrderId');
//        var pickupType = document.getElementById('pickupType').value;
//        var destinationType = document.getElementById('destinationType').value;
        var tripType = $("#tripType").val();
        //alert("1:"+selectedConsignment.length);
        var cntr = 0;
        for (var i = 0; i < selectedConsignment.length; i++) {
            //alert(selectedConsarignment[i].checked);
            if (selectedConsignment[i].checked == true) {
                if (cntr == 0) {
                    temp = consignmentOrder[i].value;
                } else {
                    temp = temp + "," + consignmentOrder[i].value;
                }
                cntr++;
            }
        }

        var ord = 1;
        if (cntr > 0 && val == 'ExcelExport' && ord == 0) {
            document.CNoteSearch.action = "/throttle/tripPlanning.do?param=" + val + "&consignmentOrderIds=" + temp;
            document.CNoteSearch.submit();
        } else if (cntr == 0 && val == 'ExcelExport' && ord == 0) {
            alert("Please select consignment order and proceed");
        } else if (cntr == 0 && val == 'Search' && ord == 0) {
            document.CNoteSearch.action = "/throttle/tripPlanning.do?param=" + val + "&consignmentOrderIds=" + temp;
            document.CNoteSearch.submit();
        } else if (cntr == 0 && val == 'Search' && ord > 0) {
            if (isEmpty(document.getElementById("fromDate").value)) {
                alert('please  select the fromDate');
                document.getElementById("fromDate").focus();
                return;
            }
            if (isEmpty(document.getElementById("toDate").value)) {
                alert('please  select the toDate');
                document.getElementById("toDate").focus();
                return;
            }
            document.getElementById("summaryOption").value = 1;
            document.CNoteSearch.action = "/throttle/tripPlanning.do?param=" + ord + "&consignmentOrderIds=" + temp;
            document.CNoteSearch.submit();
        }
    }



    function importExcel() {
        document.CNoteSearch.action = '/throttle/BrattleFoods/tripPlanningImport.jsp';
        document.CNoteSearch.submit();
    }

    function viewDistanceMap() {
        document.CNoteSearch.action = '/throttle/tripPlanning.do?param=view';
        document.CNoteSearch.submit();
    }


    function importExcel() {
        document.CNoteSearch.action = '/throttle/BrattleFoods/tripPlanningImport.jsp';
        document.CNoteSearch.submit();
    }
    function checkShowTable() {
        var value = document.getElementById("summaryOption").value;
        if (parseInt(value) == 1) {
            $("#summaryTable").show();
        } else {
            $("#summaryTable").hide();
        }
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Trip Planning</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Operation</a></li>
            <li class="active">TRIP PLANNING</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="setValue();
                    checkShowTable();
                    getLocation();">
                <%
                            String menuPath = "Operation >> Trip Planning";
                            request.setAttribute("menuPath", menuPath);
                %>
                <form name="CNoteSearch" method="post" enctype="multipart">


                    <table class="table table-info mb30 table-hover" style="width:60%">
                        <tr   ><td colSpan="8" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">CONSIGNMENT ORDERS FOR TRIP PLANNING</td></tr>
                        <!--                                <tr>
                                                            <td  >Pickup Zone </td>
                                                            <td  > <select name="zoneId" id="zoneId" class="form-control" style="width:180px;height:40px;" onchange="getLocation();" >
                        <c:if test="${zoneList != null}">
                            <option value="0" selected>--Select--</option>
                            <c:forEach items="${zoneList}" var="zoneList">
                                <option value='<c:out value="${zoneList.zoneId}"/>'><c:out value="${zoneList.zoneName}"/></option>
                            </c:forEach>
                        </c:if>
                    </select></td>

            <td >ProductCategory&nbsp;<font color="red">*</font></td>
                <td><input type="hidden" name="productCategoryId" id="productCategoryId" value=""/>
                    <select name="productCategory" id="productCategory" class="form-control" style="width:180px;height:40px;"  onchange="setProductId()" >
                        <c:if test="${productCategoryList != null}">
                            <option value="" selected>--Select--</option>
                            <c:forEach items="${productCategoryList}" var="product">
                                <option value='<c:out value="${product.productCategoryId}"/>'><c:out value="${product.customerTypeName}"/></option>
                            </c:forEach>
                        </c:if>
                    </select>
                </td>
                <tr>-->


                        <td><font color="red">*</font>From Date</td>
                        <td><input name="fromDate" id="fromDate" type="text" class="datepicker" style="width:180px;height:40px;"  onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>"></td>
                        <td><font color="red">*</font>To Date</td>
                        <td><input name="toDate" id="toDate" type="text" class="datepicker" style="width:180px;height:40px;" onclick="ressetDate(this);" value="<c:out value="${toDate}"/>"></td>

                        <td  align="left">
                            <input type="button" class="btn btn-info"   value="Search" onclick="searchPage('Search')">
                        </td>
                        </tr>

                        <!--                                <tr>
                                                            <td >PickUp</td>
                                                            <td>
                                                                <select name="pickupType"  id="pickupType" onclick="setPickupType();" class="form-control" style="width:180px;height:40px;"  >
                                                                    <option value="0">--Select--</option>
                                                                    <option value="1">Exact</option>
                                                                    <option value="2">Dynamic</option>
                                                                </select>
                                                            </td>
                        
                                                            <td>Pickup Point</td>
                                                            <td>
                                                                <input type="text" name="cityFrom" id="cityFrom" class="form-control" style="width:180px;height:40px;"  onchange="setProductId()" />
                                                                <input type="hidden" name="pickPoint" id="pickPoint" class="textbox" style="width:180px;height:40px;"  onchange="setProductId()" />
                        
                                                            </td>
                                                            <td >Pickup Date</td>
                                                            <td><input type="text" class="datepicker" name="pickupDate" id="pickupDate" style="width:180px;height:40px;"  value=""/></td>
                                                            <td >+/-(hours)</td>
                                                            <td><input type="totalDays" class="form-control" name="pickupDays" id="pickupDays" style="width:180px;height:40px;"  value="0"/></td>
                        
                        
                        
                                                            <td id="pickupRadiusName" style="display: none">Radius(KM)</td>
                                                            <td id="pickupRadiusId" style="display: none"><input type="text" class="form-control" style="width:180px;height:40px;"  name="pickupRadius" id="pickupRadius" value=""/></td>
                        
                                                        </tr>
                                                        <tr>
                                                            <td >Destination</td>
                                                            <td>
                                                                <select name="destinationType"  id="destinationType" onChange="setDestinationType();" class="form-control" style="width:180px;height:40px;"  >
                                                                    <option value="0">--Select--</option>
                                                                    <option value="1">Exact</option>
                                                                    <option value="2">Dynamic</option>
                                                                </select>
                                                            </td>
                        
                                                            <td >Destination Point</td>
                                                            <td>
                                                                <input type="text" name="cityTo" id="cityTo" class="form-control" style="width:180px;height:40px;"  onchange="setProductId()" />
                                                                <input type="hidden" name="destinationPoint" id="destinationPoint" class="form-control" style="width:240px;height:40px;"  onchange="setProductId()" />
                        
                        
                                                                                                        <select name="destinationPoint" id="destinationPoint" class="textbox" style="height:20px; width:112px;" onchange="setProductId()" >
                        <c:if test="${cityList != null}">
                            <option value="" selected>--Select--</option>
                            <c:forEach items="${cityList}" var="city">
                                <option value='<c:out value="${city.cityId}"/>'><c:out value="${city.cityName}"/></option>
                            </c:forEach>
                        </c:if>
                    </select>
                    </td>
                    <td >Estimated Arrival Date</td>
                    <td><input type="text" class="datepicker" name="arrivalDate" id="arrivalDate" style="width:180px;height:40px;"  value=""/></td>
                    <td align="right">+/-(hours)</td>
                    <td><input type="text" class="form-control" name="destinationDays" id="destinationDays" class="form-control" style="width:180px;height:40px;"  value="0"/></td>
                    <td id="destinationRadiusName" style="display: none">Radius(Km)</td>
                    <td id="destinationRadiusId" style="display: none"><input type="text" name="destinationRadius" id="destinationRadius" class="form-control" style="width:240px;height:40px;"  value=""/></td>
                </tr>-->
                    </table>


                    <input type="hidden" name="routePoints" id="routePoints" value='<c:out value="${routePoints}"/>' />
                    <input type="hidden" name="consignmentListSize" id="routePoints" value='<c:out value="${consignmentListSize}"/>' />
                    <input type="hidden" name="orderedUniquePickPoints" id="routePoints" value='<c:out value="${orderedUniquePickPoints}"/>' />



                    <c:if test = "${consignmentList != null}" >

                        <table width="60%" cellpadding="0" cellspacing="0" border="0" align="left" class="table5">
                            <tr>
                                <td class="bottom" align="left"><img src="images/left_status.jpg" alt=""  /></td>
                                <td class="bottom" height="35" align="right"><img src="images/icon_notstart.png" alt="" /></td>
                                <td class="bottom">&nbsp;<span style="font-size:16px; color:#F30;" align="left"><c:out value="${consignmentListSize}"/></span></td>
                                <td class="bottom" height="35" align="right"><img src="images/icon_active.png" alt="" /></td>
                                <td class="bottom">&nbsp;<span style="font-size:16px; color:#3C0;" align="left">0</span></td>
                                <td class="bottom" align="right"><img src="images/icon_closed.png" alt="" /></td>
                                <td class="bottom">&nbsp;<span style="font-size:16px; color:#F30;">0</span></td>
                                <td class="bottom" align="right"><img src="images/icon_completed.png" alt="" /></td>
                                <td class="bottom">&nbsp;<span style="font-size:16px; color:#39C;">0</span></td>
                                <td align="center"><h2>Total  <c:out value="${consignmentListSize}"/></h2></td>
                            </tr>

                        </table>
                    </c:if>

                    <!--        <div id="spinner" class="spinner" style="display:none;" >
                                               <img id="img-spinner" src="images/page-loader2.gif" alt="Loading" />
                                                        <div style="margin-top: 10px; color: white">
                                                        <b>Please wait...</b>
                                                    </div>
                                                                   </div>-->


                    <c:if test = "${consignmentList != null}" >
                        <table class="table table-info mb30 table-hover" id="table" width="100%" >
                            <thead>
                                <tr >
                                    <th>Sno</th>
                                    <th>Select</th>
                                    <th>Consignment No</th>
                                    <th>Movement Type</th>
                                    <th>Consignment Date</th>
                                    <th>Customer Name </th>
                                    <!--<th>Customer Id </th>-->
                                    <th>Billing Party Name </th>
                                    <th>Consignor Name </th>
                                    <th>Consignee Name </th>
                                    <th>Bill of enty </th>
                                    <th>Shipping bill no </th>
                                    
                                    <th>Route </th>
                                    <th>Product Category </th>
                                        <%--                        <th>Weight(Kg) </th>
                                                                <th>Volume(CBM) </th>--%>
                                    <th>Container Type(s) </th>
                                    <th>Status </th>
                                    <th>Vehicle Required On </th>
                                    <th>TimeToStart </th>
                                    <th>Created By</th>


                                </tr>
                            </thead>
                            <% int index = 0;
                                        int sno = 1;
                            %>
                            <tbody>
                                <c:set var="totalVolume" value="${0 }"/>
                                <c:set var="totalWeight" value="${0 }"/>
                                <c:forEach items="${consignmentList}" var="cnl">
                                    <c:set var="totalVolume" value="${totalVolume + cnl.totalVolume }"/>
                                    <c:set var="totalWeight" value="${totalWeight + cnl.totalWeights }"/>
                                    <%-- <input type="hidden" name="consignmentOrderId" id="consignmentOrderId<%=index%>" value="<c:out value="${cnl.consignmentOrderId}"/>"/><c:out value="${cnl.consignmentNoteNo}"/>--%>
                                    <tr>
                                        <td align="left" ><%=sno%></td>
                                        <td align="left" >
                                            <%if(request.getAttribute("productCategoryId") != null){%>
                                            <input type="checkbox" name="selectedIndex" id="selectedIndex<%=sno - 1%>" readonly  value="<c:out value="${cnl.consignmentOrderId}"/>" onclick="checkSelectedItem('<%=sno - 1%>', '<c:out value="${cnl.customerId}"/>,<c:out value="${cnl.billingPartyId}"/>,<c:out value="${cnl.consigneeId}"/>')" />
                                            <%}else{%>
                                            <input type="checkbox" name="selectedIndex" id="selectedIndex<%=sno - 1%>" value="<c:out value="${cnl.consignmentOrderId}"/>" onclick="checkSelectedItem('<%=sno - 1%>', '<c:out value="${cnl.customerId}"/>,<c:out value="${cnl.billingPartyId}"/>,<c:out value="${cnl.consigneeId}"/>')" />
                                            <%}%>
                                        </td>
                                        <td align="left" >
                                            <input type="hidden" name="consignmentOrderId" id="consignmentOrderId<%=index%>" value="<c:out value="${cnl.consignmentOrderId}"/>"/>
                                            <input type="hidden" name="containerId" id="containerId<%=index%>" value="<c:out value="${cnl.containerTypeId}"/>"/>

                                            <a href="#" onclick="viewConsignmentDetails('<c:out value="${cnl.consignmentOrderId}"/>');"><c:out value="${cnl.consignmentNoteNo}"/></a>
                                        </td>
                                        <td align="left" ><c:out value="${cnl.movementType}"/></td>
                                        <td align="left" ><c:out value="${cnl.createdOn}"/></td>

                                        <td align="left" ><c:out value="${cnl.billingParty}"/></td>
                                        <td align="left" ><c:out value="${cnl.customerName}"/></td>
                                        <td align="left" ><c:out value="${cnl.consignorName}"/></td>
                                        <td align="left" ><c:out value="${cnl.consigneeName}"/></td>
                                        <td align="left" ><c:out value="${cnl.billOfEntry}"/></td>
                                        <td align="left" ><c:out value="${cnl.shipingLineNo}"/></td>
                                        

                                        <td align="left" ><c:out value="${cnl.routeName}"/></td>
                                        <%--                            <td align="left" ><c:out value="${cnl.consigmentOrigin}"/> &nbsp;
                                                                    (<c:out value="${cnl.originLat}"/>/<c:out value="${cnl.originLong}"/>)</td>
                                                                    <td align="left" ><c:out value="${cnl.consigmentDestination}"/> &nbsp;
                                                                    (<c:out value="${cnl.destLat}"/>/<c:out value="${cnl.destLong}"/>)</td>--%>

                                        <td align="left" ><c:out value="${cnl.productCategoryName}"/></td>
                                        <%--                            <td align="right" ><c:out value="${cnl.totalWeights}"/></td>
                                                                    <td align="right" ><c:out value="${cnl.totalVolume}"/></td>--%>
                                        <td align="right" >
                                            <a href="#" onclick="viewConsignmentContainerDetails('<c:out value="${cnl.consignmentOrderId}"/>');">
                                                <c:out value="${cnl.containerTypeName}"/>
                                            </a>
                                        </td>



                                        <td align="left" >
                                            <input type="hidden" name="originId" id="originId<%=index%>" value="<c:out value="${cnl.origin}"/>"/>
                                            <input type="hidden" name="destinationId" id="destinationId<%=index%>" value="<c:out value="${cnl.destination}"/>"/>

                                            <input type="hidden" name="consignmentOrderStatus" id="consignmentOrderStatus<%=index%>" value="<c:out value="${cnl.consginmentOrderStatus}"/>"/>

                                            <c:if test="${(cnl.consginmentOrderStatus=='5')}" >
                                                Order Created
                                            </c:if>

                                            <c:if test="${(cnl.consginmentOrderStatus=='25')}" >
                                                First Leg completed
                                            </c:if>
                                            <c:if test="${(cnl.consginmentOrderStatus=='28')}" >
                                                Second Leg completed
                                            </c:if>

                                        </td>
                                        <td align="left" ><c:out value="${cnl.tripScheduleDate}"/>&nbsp;
                                            <c:out value="${cnl.tripScheduleTime}"/></td>
                                        <td align="left" >
                                            <c:if test="${cnl.timeLeftValue > 0}">
                                                <font color="green"><c:out value="${cnl.timeLeft}"/></font>
                                            </c:if>
                                            <c:if test="${cnl.timeLeftValue <= 0}">
                                                <font color="red"><c:out value="${cnl.timeLeft}"/></font>
                                            </c:if>
                                            <input type="hidden" name="count" id="count" value="<%=sno%>"/>
                                        </td>
                                        <td align="left" ><c:out value="${cnl.userName}"/></td>
                                    </tr>
                                    <%index++;%>
                                    <%sno++;%>
                                </c:forEach>
                            </tbody>
                        </table>
                        <br/>
                        <c:if test="${totalWeight != '0'}">
                            <!--                <table width="500px" align="center" border="0" id="summaryTable" class="sortable"  style="width:1150px;" >
                                                <tr height="40">
                                                    <td class="contentsub"> Total Weight&nbsp;(Kg) &emsp;&emsp;<c:out value="${totalWeight}"/></td>
                                                    <td class="contentsub" > Total Volume&nbsp;(CBM) &emsp;&emsp;<c:out value="${totalVolume}"/></td>
                                                    <td class="contentsub" >
                            
                                                        <a href="#" onclick="initialize();"><font color="white">View On Map</font></a>
                                                    </td>
                                                    <td class="contentsub" id="totalRouteKm" >Total Route Km &emsp;&emsp; <%=request.getAttribute("routeDistance")%></td>
                                                <input type="hidden" name="routeDistance" id="routeDistance" value='<%=request.getAttribute("routeDistance")%>' />
                                                </tr>
                                                <tr height="40">
                                                    <td class="contentsub" align="center" colspan="4"> &emsp;&emsp; &emsp;&emsp; &emsp;&emsp; Recommendation</td>
                                                </tr>
                                                <tr height="40">
                                                    <td class="contentsub">Vehicle Type</td>
                                                    <td class="contentsub">No of Vehicle Required</td>
                                                    <td class="contentsub">Vehicle Utilization</td>
                                                    <td class="contentsub">Estimated Route Expense</td>
                                                </tr>
                            <c:forEach items="${vehicleTypeList}" var="vehicle">
                                <tr height="40">
                                    <td><c:out value="${vehicle.vehicleTypeName}"/></td>
                                <input type="hidden"  id="vehicleTypeId" name="vehicleTypeId" value='<c:out value="${vehicle.vehicleTypeId}"/>'/>
                                <c:set var="totalVehicle" value="${totalWeight / (vehicle.vehicleTonnage*1000)}"/>
                                <c:set var="totalVehicles" value="${(totalVehicle - ( totalVehicle % 1)) +1 }" />
                                <td >
                                <fmt:formatNumber type="number"  maxFractionDigits="2" value="${totalVehicles }" />
                            </td>
                            <td >
                                <fmt:formatNumber type="number"  maxFractionDigits="2" value="${(totalWeight / (vehicle.vehicleTonnage*1000 * totalVehicles )) * 100    }" />&nbsp;%
                            </td>
                            <td id="vehicleTypeExpense">
    
                                <a href="#" onClick="viewExpenseDetails('<c:out value="${vehicle.vehicleTypeName}"/>', '<c:out value="${vehicle.vehicleTypeId}"/>', '<c:out value="${routeDistance}"/>', '<c:out value="${totalVehicles}"/>');" > view</a>
                                
                                <fmt:formatNumber type="number"  maxFractionDigits="2" value="${vehicle.costPerKm * routeDistance  * totalVehicles }" />
                                
                            </td>
    
                            </tr>
                            </c:forEach>
                        </table>-->



                        </c:if>
                    </c:if>
                    <input type="hidden" name="customerId" id="customerId" value=""/>
                    <input type="hidden" name="summaryOption" id="summaryOption" value="0"/>
                    <input type="hidden" name="ismultiple" id="ismultiple" value="0"/>
                    <input type="hidden" name="googleDistance" id="googleDistance" value="0"/>
                    <script language="javascript" type="text/javascript">



                        function setTripType(v) {
                            var ismultiPle = document.getElementsByName("multiPleTrip");
                            var flag = 0;
                            for (var j = 0; j < ismultiPle.length; j++) {

                                if (ismultiPle[j].checked == true) {
                                    flag = 1;

                                    if (j == v) {
                                        document.getElementById("ismultiple").value = "1";
                                    } else {
                                        document.getElementById("multiPleTrip" + j).checked = false;
                                        document.getElementById("ismultiple").value = "1";
                                    }
                                } else {
                                    if (flag == 0) {
                                        document.getElementById("ismultiple").value = "0";
                                    }
                                }
                            }
                        }

                        function checkSelectedItem(sno, customerId) {
                            //                 alert(customerId);
                            var index;
                            if (document.getElementById("selectedIndex" + sno).checked) {
                                var cust = document.getElementById('customerId').value = customerId;
                                //                    alert(cust)

                                var selectedConsignment = document.getElementsByName('selectedIndex');
                                //      for(var i=0; i<selectedConsignment.length; i++){
                                //                    if(sno != i){
                                //                        document.getElementById("selectedIndex"+i).checked = false;
                                //
                                //                    }
                                document.getElementById("orderSequence" + sno).value = ParseInt(sno) + 1;
                                index = document.getElementById("orderSequence" + sno).value

                            } else {
                                document.getElementById('customerId').value = "";
                            }
                            index++;

                        }

                    </script>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 1);
                    </script>
                    <br/>
                    <center>
                        &nbsp;&nbsp;<input type="button"  class="btn btn-info" value="Generate Excel" name="ExcelExport" onClick="searchPage(this.name)">

                        &nbsp;&nbsp;<input type="button" class="btn btn-info" name="trip"  value="Trip Planning" onclick="submitPageForTripGeneration()"/>

                    </center>

                    <table>
                        <tr>
                            <td>
                                <div id="mapdiv"   style="width:550px; height:400px">

                                </div>
                                <div id="mapdistance"   style="width:550px; height:400px">

                                </div>
                            </td>
                            <td valign="top" >&nbsp;
                                <%if(request.getAttribute("routePoints") != null){%>
                                <table width="100%" align="center" border="0" id="table1" class="sortable" style="width:360px;" >
                                    <thead>
                                        <tr>
                                            <td>Sno</td>
                                            <td>Legend</td>
                                            <td>Point Name</td>
                                            <td>Point Type</td>
                                            <td>Planned Date Time</td>
                                        </tr>
                                    </thead>

                                    <%
                                    String routeDetails = (String)request.getAttribute("routePoints");
                                    String[] routeData = routeDetails.split("@");
                                    String[] tempData = null;
                                    int cntr = 0;
                                    String tempLegend = "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z";
                                    String[] tempLegendArray = tempLegend.split(",");
                                    for(int j=0; j< routeData.length; j++){
                                        cntr++;
                                        tempData = routeData[j].split("~");
                                        if(tempData.length > 4){// pickup
                                    %>
                                    <tr>
                                        <td><%=cntr%></td>
                                        <td><%=tempLegendArray[j]%></td>
                                        <td><%=tempData[3]%></td>
                                        <td>PickUp</td>
                                        <td><%=tempData[4]%></td>
                                    </tr>
                                    <%
                                }else{
                                    %>
                                    <tr>
                                        <td><%=cntr%></td>
                                        <td><%=tempLegendArray[j]%></td>
                                        <td><%=tempData[3]%></td>
                                        <td>Drop</td>
                                        <td>-</td>
                                    </tr>
                                    <%
                                }

                            }
                                    %>

                                </table>
                                <%}%>
                            </td>
                    </table>


                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                <div id="loader"></div>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
