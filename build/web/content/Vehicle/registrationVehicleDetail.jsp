<%-- 
    Document   : registrationVehicleDetail
    Created on : 16 Jun, 2016, 11:25:39 AM
    Author     : pavithra
--%>

<%@ page import="ets.domain.util.ThrottleConstants" %>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">-->

<script type="text/javascript">
    $(document).ready(function () {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function () {
        $(".datepicker").datepicker({
            changeMonth: true, changeYear: true,
            dateFormat: 'dd-mm-yy'
        });

    });
  

</script>






<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        $("#tabs").tabs();

        $("#insurance").focus();
        $("#vehicleRegistration").focus();
    });

    function saveRegistrationDetails() {

        var truckNo = document.getElementById("truckNo").value;
        var vehicleId = $("#vehicleId").val();
        var transportionType = document.getElementById("transportionType").value;
        var ownerName = document.getElementById("ownerName").value;
        var ownerId = document.getElementById("ownerId").value;
        var dateOfReg = document.getElementById("dateOfReg").value;
        var noOfPersons = document.getElementById("noOfPersons").value;
        var color = document.getElementById("color").value;
        if (document.addVehicle.truckNo.value == '') {
            alert('Please Enter Truck No  ');
            $("#truckNo").focus();
            return;
        } else if (document.addVehicle.transportionType.value == '0') {
            alert('Please Enter  Transportion Type');
            $("#transportionType").focus();
            return;
        } else if (document.addVehicle.ownerName.value == '') {
            alert('Please Enter  Owner Name');
            $("#ownerName").focus();
            return;
        } else if (document.addVehicle.ownerId.value == '') {
            alert('Please Enter  Owner Id');
            $("#ownerId").focus();
            return;
        } else if (document.addVehicle.dateOfReg.value == '') {
            alert('Please Enter Vehicle DateOfReg');
            $("#dateOfReg").focus();
            return;
        } else if (document.addVehicle.noOfPersons.value == '') {
            alert('Please Enter  NoOfPersons ');
            $("#noOfPersons").focus();
            return;
        } else if (document.addVehicle.color.value == '') {
            alert('Please Enter Vehicle color');
            $("#color").focus();
            return;
        } else if (document.addVehicle.vehicleId.value == '') {
            alert('Please Enter vehicle No');
            return;
        }

        var url = '';
        var insertStatus = 0;
        url = './saveRegistrationDetails.do';
        $.ajax({
            url: url,
            data: {truckNo: truckNo,transportionType:transportionType, ownerName: ownerName, ownerId: ownerId, 
                dateOfReg: dateOfReg, noOfPersons: noOfPersons,color:color,vehicleId:vehicleId
            },
            type: "GET",
            success: function (response) {
                // alert(response);
                insertStatus = response.toString().trim();
                if (insertStatus == 0) {
                    var str = "Vehicle Registration added failed ";
                    $("#status1").text(str).css("color", "red");
                } else {
                    var str = "Vehicle Registration added sucessfully ";
                    $("#status1").text(str).css("color", "green");

                }
            },
            error: function (xhr, status, error) {
            }
        });
    }
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.VehicleRegistration"  text="Vehicle"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.Vehicle"  text="Vehicle"/></a></li>
            <li class="active"><spring:message code="stores.label.VehicleRegistration"  text="Vehicle"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">


            <body>

                <style>
                    body {
                        font:13px verdana;
                        font-weight:normal;
                    }
                </style>
                <form name="addVehicle" method="post">
                    <%@ include file="/content/common/path.jsp" %>


                    <%@ include file="/content/common/message.jsp" %>
                    <br>
                  
                    <font style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; ">
                    <div align="center" id="status1">&nbsp;&nbsp;
                    </div>
                    </font>
                  

                    <br>
                    <input type="hidden" name="fleetTypeId" id="fleetTypeId" value="1" />
                    <div id="tabs">
                        <ul>
                            <li id="insurance"><a href="#vehicleRegistration"><span>Registration</span></a></li>
                            <li id="tab1"><a href="#vehicleInfo"><span>Vehicle</span></a></li>
                            <li id="tab2"><a href="#vehicleOem"><span>OEM</span></a></li>
                            <li id="tab3"><a href="#vehicleDepreciationTab"><span>Depreciation</span></a></li>
                            <li><a href="#vehicleFileAttachments"><span>File Attachments</span></a></li>
                        </ul>

<div id="vehicleRegistration" class="tabContent">
                <table class="table table-info mb30 table-hover">
                    <thead>
                    <tr>
                        <th colspan="6"  height="30">
                            <div  align="center">Add Registration</div>
                        </th>
                    </tr>
                    </thead>
                    <tr>
                        <td height="30">Truck No(in Govt. Sys)</td>
                        <td height="30">
                            <input type="text" name="truckNo" id="truckNo" class="form-control" value='' style="width:250px"></td>
                        <td height="30">Transportion Type</td>
                    <td  colspan="3"> <select class="form-control" name="transportionType" id="transportionType" style="width:250px"  >
                             <option value="0" selected>---select---</option>
                                <option value="1">Private</option>
                                <option value="2">Public</option>
                            </select></td> 
                    </tr>
                    <tr>
                        <td height="30">Owner Name</td>
                        <td height="30">
                            <input type="text" name="ownerName" id="ownerName" class="form-control" value='' style="width:250px"></td>
                        <td height="30">Owner Id</td>
                        <td height="30">
                            <input type="text" name="ownerId" id="ownerId" class="form-control" value='' style="width:250px"></td>
                    </tr>
                    <tr>
                        <td height="30">Date Of Reg</td>
                        <td height="30">
                            <input type="text" name="dateOfReg" id="dateOfReg" class="form-control datepicker" value='' style="width:250px"></td>
                        <td height="30">No. of persons in Cabin</td>
                        <td height="30">
                            <input type="text" name="noOfPersons" id="noOfPersons" class="form-control" value='' style="width:250px"></td>
                    </tr>
                    <tr>
                        <td height="30">color</td>
                        <td height="30">
                            <input type="text" name="color" id="color" class="form-control" value='' style="width:250px"></td>
                        <td height="30"></td>
                        <td height="30">
                            </td>
                    </tr>
                    
                    
                </table>
                <center>
                    
                    <input type="button" class="button" value="Save" name="Save" id="insuranceNext" onClick="saveRegistrationDetails();" style="visibility: block"/>
                        <a  class="nexttab" ><input type="button" class="button" value="Next" name="Next" /></a>

                </center>

            </div>
                        <div id="vehicleInfo">
                                                                   <input type="hidden" name="regNoCheck" id="regNoCheck" value='' >
                                                                   <table border="0" class="border" align="center" width="1000" cellpadding="0" cellspacing="0" id="bg">
                                                                       <tr>
                                                                           <td colspan="6">
                                                                               <table class="table table-info mb30 table-hover">
                                                                                   <thead>
                                                                                   <th colspan="6"  height="30">
                                                                                   <div  align="center">Vehicle </div>
                                                                                   </th>
                                                                                   </thead>
                                                                                   <tr width="100%">
                                                                                   <div id="groupInfo" style="display: none;">

                                                                                       <td height="30" width="20%"><font color="red" size="4">*</font><font style="bold">Vehicle OwnerShip</font></td>
                                                                                       <td width="20%">  <select class="form-control" name="ownerShips" id="ownerShips"  style="width:250px;" disabled>
                                                                                               <option value="0">---select---</option>
                                                                                               <option value="1" >Own</option>
                                                                                               <option value="2" >Dedicate</option>
                                                                                               <option value="3" >Hire </option>
                                                                                               <option value="4" >Replacement</option>
                                                                                               <input type="hidden" name="groupNo" id="groupNo" class="form-control" value="1">
                                                                                           </select>
                                                                                           <script>
                                                                    document.getElementById("ownerShips").value = '<c:out value="${ownership}"/>';
                                                                                           </script>
                                                                                           <input type="hidden" name="vehicleId" id="vehicleId" value="<c:out value="${vehicleId}"/>" /></td>
                                                                                   </div>

                                                                                   <input type="hidden" name="asset" id="asset" class="form-control" value="1">
                                                                                   <td  colspan="4">
                                                                                       <div id="attachInfo" style="display: none">
                                                                                           <table width="100%" cellpadding="0" cellspacing="2"  id="vandornameId" >
                                                                                               <tr>
                                                                                                   <td height="30%" width="20%"><font color="red">*</font>Vendor Name</td>

                                                                                                   <td width="10%">
                                                                                                       <c:if test = "${vendorNameList != null}" >
                                                                                                           <c:forEach items="${vendorNameList}" var="vnl">
                                                                                                               <input type="text" id ="vendorId" value='<c:out value="${vnl.vendorId}"/>' style="width:250px;"/>
                                                                                                               <input type="text" id ="vendorName" value='<c:out value="${vnl.vendorName}"/>' style="width:250px;"/>
                                                                                                           </c:forEach >
                                                                                                       </c:if>
                                                                                                       <c:if test = "${leasingCustList != null}" >
                                                                                                           &emsp;&emsp;&emsp;&emsp;&emsp;&emsp; <select class="form-control" name="vendorId" id="vId"  style="width:250px;" disabled>
                                                                                                               <option value="0" selected>--Select--</option>
                                                                                                               <c:forEach items="${leasingCustList}" var="LCList">
                                                                                                                   <option value='<c:out value="${LCList.vendorId}" />'><c:out value="${LCList.vendorName}" /></option>
                                                                                                               </c:forEach >
                                                                                                           </c:if>
                                                                                                       </select>
                                                                                                       <script>
                                                                                    document.getElementById("vId").value = '<c:out value="${vendorId}"/>';
                                                                                                       </script>        
                                                                                                   </td>
                                                                                               </tr>
                                                                                           </table>
                                                                                       </div>
                                                                                   </td>
                                                                       </tr>
                                                                       <tr>
                                                                           <td height="30"><font color="red">*</font>Vehicle Number</td>
                                                                           <td height="30"><input maxlength='13'  name="regNo" type="text" class="form-control" value="<c:out value="${regNo}"/>" readonly style="width:250px;"></td>


                                                                           <td height="30" width="20%"><font color="red">*</font>MFRs</td>
                                                                           <td  width="20%" style="border-right-color:#5BC0DE;"><select class="form-control" name="mfrId" id="mfrId"  style="width:250px;" disabled>
                                                                                   <option value="0">---Select---</option>
                                                                                   <c:if test = "${MfrList != null}" >
                                                                                       <c:forEach items="${MfrList}" var="Dept">
                                                                                           <option value='<c:out value="${Dept.mfrId}" />'><c:out value="${Dept.mfrName}" /></option>
                                                                                       </c:forEach >
                                                                                   </c:if>
                                                                               </select>
                                                                               <script>
                                                                        document.getElementById("mfrId").value = '<c:out value="${mfrId}"/>';
                                                                               </script>
                                                                           </td>


                                                                       </tr>
                                                                       <tr>
                                                                           <td height="30" width="20%"><font color="red">*</font>Vehicle Type</td>
                                                                           <td  width="20%">
                                                                               <input type="hidden" name="typeId" id="typeId" value="<c:out value="${typeId}"/>" />
                                                                               <input type="hidden" name="axleTypeId" id="axleTypeId" value="<c:out value="${axleTypeId}"/>" />
                                                                               <select class="form-control" name="vehicleTypeId" id="vehicleTypeId" disabled style="width:250px;">
                                                                                   <option value="0">---Select---</option>
                                                                                   <c:if test = "${TypeList != null}" >
                                                                                       <c:forEach items="${TypeList}" var="Type">
                                                                                           <option value='<c:out value="${Type.typeId}" />~<c:out value="${Type.axleTypeId}" />'><c:out value="${Type.typeName}" /></option>
                                                                                       </c:forEach >
                                                                                   </c:if>
                                                                               </select>
                                                                               <c:if test="${vehicleId != null}">
                                                                                   <script>
                                                                            document.getElementById("vehicleTypeId").value = '<c:out value="${typeId}"/>~<c:out value="${axleTypeId}"/>';
                                                                                getModelAndTonnage('<c:out value="${typeId}"/>~<c:out value="${axleTypeId}"/>');
                                                                                   </script>
                                                                               </c:if>
                                                                           </td>

                                                                           <td height="30" width="20%"><font color="red">*</font>Model </td>
                                                                           <td style="border-right-color:#5BC0DE;" height="30" >
                                                                               <select class="form-control" name="modelId" id="modelId" style="width:250px;" disabled>
                                                                                   <option value="0">---Select---</option>
                                                                               </select>
                                                                               <script>
                                                                        document.getElementById("modelId").value = '<c:out value="${modelId}"/>'
                                                                               </script>
                                                                           </td>
                                                                       </tr>
                                                                   </table>
                                                                   </td></tr>

                                                                   <tr>
                                                                       <td colspan="6">

                                                                           <div id="hiredGroup" >
                                                                               <table class="table table-info mb30 table-hover">
                                                                                   <tr  width="20%">
                                                                                       <td height="30" width="20%"><font color="red">*</font>Registration Date</td>

                                                                                       <td height="30" width="20%"><input name="dateOfSale" type="text" class="datepicker" value="<c:out value="${dateOfSale}" />" size="20" style="width:250px;" disabled>

                                                                                       </td>
                                                                                       <!--                    <td></td>-->

                                                                                       <td height="30"><font color="red">*</font>Fleet Center</td>
                                                                                       <td  >
                                                                                           <select class="form-control" name="opId" id="opId"  style="width:250px;" disabled>
                                                                                               <c:if test = "${OperationPointList != null}" >
                                                                                                   <c:forEach items="${OperationPointList}" var="Dept">
                                                                                                       <option value='<c:out value="${Dept.opId}" />'> <c:out value="${Dept.opName}" /> </option>
                                                                                                   </c:forEach >
                                                                                               </c:if>
                                                                                           </select>
                                                                                           <script>
                                                                                    document.getElementById("opId").value = '<c:out value="${opId}"/>';
                                                                                           </script>
                                                                                       </td>
                                                                                   </tr>

                                                                                   <tr width="20%">
                                                                                       <td height="30" ><font color="red">*</font>Engine No</td>
                                                                                       <td height="30" ><input maxlength='20' name="engineNo" type="text" class="form-control"readonly value="<c:out value="${engineNo}"/>" size="20" style="width:250px;"></td>
                                                                                       <td height="30" width="20%"><font color="red">*</font>Chassis No</td>
                                                                                       <td height="30" width="20%"><input maxlength='20'  type="text" name="chassisNo" size="20" readonly class="form-control" value="<c:out value="${chassisNo}"/>" style="width:250px;"> </td>
                                                                                   </tr>
                                                                                   <tr>
                                                                                       <td height="30" style="display: none"><font color="red">*</font>No Of Axles</td>
                                                                                       <td height="30" style="display: none"><select name="axles" class="form-control" style="width:250px;" disabled>
                                                                                               <option value="2">Double Axle</option>
                                                                                               <option value="3">Triple Axle</option>
                                                                                               <option value="3">Four Axle</option>
                                                                                               <option value="3">Five Axle</option>
                                                                                               <option value="3">Six Axle</option>
                                                                                               <option value="3">More Than 6 Axle</option>
                                                                                           </select></td>
                                                                                       <td height="30">Tonnage (MT)</td>
                                                                                       <td height="30"><input name="seatCapacity" id="seatCapacity" type="text" readonly class="form-control" value="<c:out value="${seatCapacity}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="15" value="0" size="20" style="width:250px;"></td>
                                                                                       <td colspan="2">&nbsp;</td>
                                                                                   </tr>
                                                                                   <tr>
                                                                                       <td height="30"><font color="red">*</font>Warranty Date</td>
                                                                                       <td height="30"><input name="warrantyDate" id="warrantyDate" type="text" disabled class="datepicker" value="<c:out value="${warrantyDate}"/>" size="20" onchange="CalWarrantyDate(this.value)" style="width:250px;"></td>
                                                                                       <td height="30">Warranty (days)</td>
                                                                                       <td height="30"><input name="war_period" id="war_period" type="text" readonly  class="form-control" value="<c:out value="${warPeriod}"/>" size="20"  onKeyPress="return onKeyPressBlockCharacters(event);" style="width:250px;"></td>
                                                                                   </tr>
                                                                                   <tr>
                                                                                       <td height="30">Vehicle Cost As On Date</td>
                                                                                       <td  ><input type="text" name="vehicleCost" class="form-control" maxlength="15" readonly value="<c:out value="${vehicleCost}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" onkeyup="setVehicleCost(this.value);" style="width:250px;"/></td>
                                                                                       <td height="30">Depreciation(%)</td>
                                                                                       <td  ><input type="text" name="vehicleDepreciation" id="vehicleDepreciation" readonly class="form-control" maxlength="15" value="<c:out value="${vehicleDepreciation}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" style="width:250px;"/></td>
                                                                                   </tr>
                                                                                   <script>
                                                                                       function setVehicleCost(value) {
                                                                                           $("#vehicleDepreciationCost").val(value);
                                                                                       }
                                                                                   </script>
                                                                                   <tr>
                                                                                       <td height="30"><font color="red">*</font>Cold Storage</td>
                                                                                       <td  ><select class="form-control" name="classId" id="classId" style="width:250px;" disabled>
                                                                                               <c:if test = "${ClassList != null}" >
                                                                                                   <c:forEach items="${ClassList}" var="Dept">
                                                                                                       <option value="<c:out value="${Dept.classId}" />"><c:out value="${Dept.className}" /></option>
                                                                                                   </c:forEach >
                                                                                               </c:if>
                                                                                           </select>
                                                                                           <script>
                                                                                               document.getElementById("classId").value = "1012";
                                                                                           </script>
                                                                                       </td>
                                                                                       <td height="30"> <font color="red">*</font>KM Reading</td>
                                                                                       <td  ><input maxlength='20'  name="kmReading" type="text" class="form-control" readonly value="<c:out value="${kmReading}"/>" size="20" onKeyPress="return onKeyPressBlockCharacters(event);" style="width:250px;"></td>
                                                                                   </tr>

                                                                                   <tr>
                                                                                       <td height="30"><font color="red">*</font>Daily Run KM</td>
                                                                                       <td height="30"> <input  type="text"  id="dailyKm"  name="dailyKm" readonly class="form-control" maxlength="10" value="<c:out value="${dailyKm}"/>" size="20"  onKeyPress="return onKeyPressBlockCharacters(event);" style="width:250px;"> </td>
                                                                                       <td height="30"> <font color="red">*</font>Daily Run HM</td>
                                                                                       <td  > <input type="text" id="dailyHm" name="dailyHm"  class="form-control" readonly value="<c:out value="${dailyHm}"/>" maxlength="10" size="20" onKeyPress="return onKeyPressBlockCharacters(event);" style="width:250px;"></td>
                                                                                   </tr>
                                                                                   <tr>
                                                                                       <td height="30"> <font color="red">*</font>Vehicle Color</td>
                                                                                       <td><input type="text"  name="vehicleColor" id="vehicleColor" class="form-control" readonly maxlength="20" value="<c:out value="${vehicleColor}"/>" onKeyPress="return onKeyPressBlockNumbers(event);" style="width:250px;"></td>
                                                                                       <td height="30"><font color="red">*</font>Vehicle Usage</td>
                                                                                       <td  ><select class="form-control" name="usageId"  style="width:250px;" disabled>
                                                                                               <option value="1" >Short Trip</option>
                                                                                               <option value="2" selected>Long Trip</option>
                                                                                           </select></td>
                                                                                   </tr>
                                                                                   <tr>

                                                                                       <td height="30"> <font color="red">*</font>GPS Tracking System</td>
                                                                                       <td  >
                                                                                           <select class="form-control" name="gpsSystem" id="gpsSystem" onchange="openGpsInfo(this.value)" style="width:250px;" disabled>
                                                                                               <option value='Yes' >Yes</option>
                                                                                               <option value='No' selected>No</option>
                                                                                           </select>
                                                                                           <script type="text/javascript">
                                                                                               function openGpsInfo(val) {
                                                                                                   if (val == 'Yes') {
                                                                                                       document.getElementById("trckingId").style.display = "block";
                                                                                                   } else {
                                                                                                       document.getElementById("trckingId").style.display = "none";
                                                                                                   }
                                                                                               }
                                                                                           </script>
                                                                                       </td>
                                                                                       <td> &nbsp;<input type="hidden" name="groupId" value="0"/>Status</td> 
                                                                                       <td>
                                                                                           <select name="activeInd" id="activeInd" style="width:250px;"  class="form-control" disabled>
                                                                                               <option value="Y">Active</option>
                                                                                               <option value="N">In-Active</option>
                                                                                           </select>
                                                                                       </td>

                                                                                   </tr>

                                                                                   <tr>
                                                                                       <td> Remarks:</td>
                                                                                       <td  colspan="3"><textArea id="description" name="description" cols="5" rows="5" style="width:250px;" readonly><c:out value="${description}" /></textArea></td>
                                                                        </tr>



                                <script type="text/javascript">


                                    function ChangeDropdowns(value) {

                                        if (value == "1") {
                                            document.getElementById("attachInfo").style.display = "none";
                                        }

                                        if (value == "2" || value == "3" || value == "4") {

                                            document.getElementById("attachInfo").style.display = "block";
                                        }
                                        //
                                        if (value == "1") {
                                            document.getElementById("hiredGroup").style.display = "block";
                                            //                         document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "2" || value == "3" || value == "4") {
                                            document.getElementById("hiredGroup").style.display = "none";
                                        }
                                        if (value == "1") {
                                            document.getElementById("hiredGroup").style.display = "block";
                                            //                         document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "3" || value == "4" || value == "2") {
                                            document.getElementById("hiredGroup").style.display = "none";
                                        }
                                        if (value == "2" || value == "3" || value == "4") {

                                            document.getElementById("attachInfo").style.display = "block";
                                        }

                                        //    function hideAdd(value){

                                        if (value == "3" || value == "4" || value == "2") {
                                            document.getElementById("add").style.visibility = 'hidden';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "1") {
                                            document.getElementById("add").style.visibility = 'visible';
                                            //   document.getElementById('vendor').style.display='none';
                                        }

                                        //    function hideAdds(value){


                                        if (value == "1" || value == "2") {
                                            document.getElementById("adds").style.visibility = 'hidden';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "3" || value == "4") {
                                            document.getElementById("adds").style.visibility = 'visible';
                                            //   document.getElementById('vendor').style.display='none';
                                        }

                                        //       function hideAdds1(value){


                                        if (value == "1" || value == "3" || value == "4") {
                                            document.getElementById("adds1").style.visibility = 'hidden';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "2") {
                                            document.getElementById("adds1").style.visibility = 'visible';
                                            //   document.getElementById('vendor').style.display='none';
                                        }


                                        if (value == "2") {
                                            document.getElementById("groupNo").style.display = "block";

                                        }
                                        if (value == "1") {
                                            document.getElementById("groupNo").style.display = "block";
                                        }

                                        if (value == "4") {
                                            ocument.getElementById("groupNo").style.display = "block";
                                        }

                                        if (value == "3") {
                                            document.getElementById("groupNo").style.display = "block";
                                        }

                                        $('#vandornameId').show();

                                        if (value != "1") {
                                            document.getElementById("bankPayment1").style.display = "block";
                                            document.getElementById("bankPayment2").style.display = "block";
                                            document.getElementById("bankPayment3").style.display = "block";
                                            document.getElementById("bankPayment4").style.display = "block";
                                        }
                                        else {
                                            document.getElementById("bankPayment1").style.display = "none";
                                            document.getElementById("bankPayment2").style.display = "none";
                                            document.getElementById("bankPayment3").style.display = "none";
                                            document.getElementById("bankPayment4").style.display = "none";
                                        }

                                    }

                                    function hideAdd(value) {

                                        if (value == "3" || value == "4" || value == "2") {
                                            document.getElementById("add").style.visibility = 'hidden';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "1") {
                                            document.getElementById("add").style.visibility = 'visible';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                    }
                                    function hideAdds(value) {


                                        if (value == "1" || value == "2") {
                                            document.getElementById("adds").style.visibility = 'hidden';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "3" || value == "4") {
                                            document.getElementById("adds").style.visibility = 'visible';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                    }
                                    function hideAdds1(value) {


                                        if (value == "1" || value == "3" || value == "4") {
                                            document.getElementById("adds1").style.visibility = 'hidden';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "2") {
                                            document.getElementById("adds1").style.visibility = 'visible';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                    }


                                                                                                                                                 </script>
</table></div>
</td>
</tr>
<tr>
                                <td colspan="6">
                                    <div id="trckingId" style="display: none;">
                                        <table width="50%" cellpadding="0" cellspacing="2">
                                            <tr>
                                                <td height="30"><font color="red">*</font>GPS System Id</td>
                                                <td align="center">
                                                    <select class="form-control" name="gpsSystemId" style="width:250px;">
                                                        <option value="1" selected>0001</option>
                                                        <option value="2" >0002</option>
                                                        <option value="3" >0003</option>
                                                    </select>
                                                </td>
                                                <td height="30">&nbsp;</td>
                                                <td>&nbsp;</td>

                                            </tr>
                                        </table>
                        </div> </td>
</tr>
</table>
                    <center>
                            <a  class="nexttab" ><input type="button" class="button" value="Next" name="Next" /></a>

                    </center>
            </div>
            <div id="vehicleOem">
                 <table class="table table-info mb30 table-hover" id="positionViewTable">
                <thead>
                    <tr>
                        <th>Axle Type</th>
                        <th>Left</th>
                        <th>Right</th>
                        <th></th>
                        <input type="hidden" name="selectedRowCount" id="selectedRowCount" value="0"/>
                    </tr>
                    </thead>
                </table>

                <br> <br>
                  <script type="text/javascript">
                                function getVehAxleName(str) {
                                    var axleTypeId = str.split("~")[1];
                                    var typeId = axleTypeId;
                                    $("#axleTypeId").val(axleTypeId);
                                    //                        var typeId=document.getElementById("typeId").value;

                                    var axleTypeName = "";
                                    var leftSideTyreCount = 0;
                                    var rightSideTyreCount = 0;
                                    var axleDetailId = 0;
                                    var count = 1;

                                    $.ajax({
                                        url: '/throttle/getVehAxleName.do',
                                        // alert(url);
                                        data: {typeId: typeId},
                                        dataType: 'json',
                                        success: function (data) {
                                            if (data !== '') {
                                                $.each(data, function (i, data) {
                                                    axleTypeName = data.AxleTypeName;
                                                    leftSideTyreCount = data.LeftSideTyreCount;
                                                    rightSideTyreCount = data.RightSideTyreCount;
                                                    axleDetailId = data.AxleDetailId;
                                                    positionAddRow(axleTypeName, leftSideTyreCount, rightSideTyreCount, count, axleDetailId);
                                                    count++;
                                                });
                                            }
                                        }
                                    });
                                }


                                function positionAddRow(axleTypeName, leftSideTyreCount, rightSideTyreCount, rowCount, axleDetailId) {
                                    var rowCount = "";
                                    var style = "text2";

                                    rowCount = document.getElementById('selectedRowCount').value;
                                    rowCount++;
                                    var tab = document.getElementById("positionViewTable");
                                    var newrow = tab.insertRow(rowCount);
                                    newrow.id = 'rowId' + rowCount;

                                    cell = newrow.insertCell(0);
                                    var cell2 = "<td class='text1' height='30'><input type='hidden' name='vehicleAxleDetailId' id='vehicleAxleDetailId" + rowCount + "' value='" + axleDetailId + "'  class='form-control' /><input type='hidden' name='vehicleFrontAxle' id='vehicleFrontAxle" + rowCount + "' value='" + axleTypeName + "'  class='form-control' /><label name='vehicleFronts' id='vehicleFronts" + rowCount + "'>" + axleTypeName + "</label></td>";
                                    //                        var cell2 = "<td class='text1' height='30'><input type='text' name='vehicleFrontAxle' id='vehicleFrontAxle" + rowCount + "' value='"+ axleTypeName +"'  class='form-control' /><label name='vehicleFronts' id='vehicleFronts" + rowCount + "'>" + axleTypeName + "</label></td>";
                                    cell.setAttribute("className", style);
                                    cell.innerHTML = cell2;

                                    cell = newrow.insertCell(1);
                                    var cell3 = "<td height='30' class='tex1' name='leftTyreIds'  id='leftTyreIds2' colspan=" + leftSideTyreCount + ">";
                                    for (var i = 0; i < leftSideTyreCount; i++) {
                                        cell3 += parseInt(i + 1) + ":<input type='text'  id='tyreNoLeft" + axleDetailId + parseInt(i + 1) + "'/>Depth<input type='text' maxlength='3'  name='treadDepthLeft' id='treadDepthLeft" + axleDetailId + parseInt(i + 1) + "' style='width:25px;' onkeypress='return blockNonNumbers(this, event, true, false);'/>mm<select name='leftItemIds' id='tyreMfrLeft" + axleDetailId + parseInt(i + 1) + "' class='form-control' onchange='updateVehicleTyreDetails(" + axleDetailId + ",0," + i + ",this.value,1);'><option value='0'>--Select--</option><c:if test = "${tyreItemList != null}" ><c:forEach items="${tyreItemList}" var="Type"><option value='<c:out value="${Type.itemId}" />'><c:out value="${Type.itemName}" /></option></c:forEach > </c:if></select><br>";
                                    }
                                    cell.setAttribute("className", style);
                                    cell.innerHTML = cell3 + "</td>";
                                    for (var i = 0; i < leftSideTyreCount; i++) {
                                        fillVehicleTyreDetails(axleDetailId, 0, i);
                                    }

                                    cell = newrow.insertCell(2);
                                    var cell4 = "<td class='text1' height='30' name='rightTyreIds' id='rightTyreIds2' colspan=" + rightSideTyreCount + ">";
                                    for (var j = 0; j < rightSideTyreCount; j++) {
                                        cell4 += parseInt(j + 1) + ":<input type='text'  id='tyreNoRight" + axleDetailId + parseInt(j + 1) + "' />Depth<input type='text' maxlength='3' name='treadDepthRight' id='treadDepthRight" + axleDetailId + parseInt(j + 1) + "' style='width:25px;' onkeypress='return blockNonNumbers(this, event, true, false);'/>mm<select name='rightItemIds' id='tyreMfrRight" + axleDetailId + parseInt(j + 1) + "' class='form-control' onchange='updateVehicleTyreDetails(" + axleDetailId + ",1," + j + ",this.value,1);'><option value='0'>--Select--</option><c:if test = "${tyreItemList != null}" ><c:forEach items="${tyreItemList}" var="Type"><option value='<c:out value="${Type.itemId}" />'><c:out value="${Type.itemName}" /></option></c:forEach > </c:if></select><br>";
                                    }
                                    cell.setAttribute("className", style);
                                    cell.innerHTML = cell4 + "</td>";
                                    for (var j = 0; j < rightSideTyreCount; j++) {
                                        fillVehicleTyreDetails(axleDetailId, 1, j);
                                    }


                                    cell = newrow.insertCell(3);
                                    var cell5 = "<td class='text1' height='30'><input type='hidden' name='position' id='position'  value='" + parseInt(i) + "' ></td>";
                                    cell.setAttribute("className", style);
                                    cell.innerHTML = cell5;

                                    document.getElementById('selectedRowCount').value = rowCount;


                                }

                                function fillVehicleTyreDetails(axleDetailId, positionName, positionNo) {
                                    var vehicleId = $("#vehicleId").val();
                                    var axleTypeId = $("#axleTypeId").val();
                                    if (vehicleId != 0) {
                                        positionNo = parseInt(positionNo + 1);
                                        if (positionName == '0') {
                                            positionName = 'Left';
                                        } else {
                                            positionName = 'Right';
                                        }
                                        $.ajax({
                                            url: '/throttle/getVehAxleTyreNo.do',
                                            // alert(url);
                                            data: {axleDetailId: axleDetailId, positionName: positionName,
                                                positionNo: positionNo, vehicleId: vehicleId, axleTypeId: axleTypeId},
                                            dataType: 'json',
                                            success: function (data) {
                                                if (data !== '') {
                                                    $.each(data, function (i, data) {
                                                        //                                                axleTypeName = data.AxleTypeName;
                                                        document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).value = data.TyreNo;
                                                        document.getElementById("treadDepth" + positionName + axleDetailId + positionNo).value = data.TyreDepth;
                                                        document.getElementById("tyreMfr" + positionName + axleDetailId + positionNo).value = data.TyreMfr;
                                                    });
                                                }
                                            }
                                        });
                                    }
                                    //                            alert(document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).value);
                                    //                            document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).value = 100;
                                }
                                function updateVehicleTyreDetails(axleDetailId, positionName, positionNo, val, updateType) {
                                    var vehicleId = $("#vehicleId").val();
                                    positionNo = parseInt(positionNo + 1);
                                    var url = '';
                                    if (positionName == '0') {
                                        positionName = 'Left';
                                    } else {
                                        positionName = 'Right';
                                    }
                                    if (updateType == 1) {

                                    }
                                    var treadDepth = document.getElementById("treadDepth" + positionName + axleDetailId + positionNo).value;
                                    var tyreNo = document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).value;
                                    if (treadDepth == '') {
                                        alert("Please fill tread depth for " + positionName + " " + positionNo);
                                        document.getElementById("treadDepth" + positionName + axleDetailId + positionNo).focus();
                                        document.getElementById("tyreMfr" + positionName + axleDetailId + positionNo).value = 0;
                                        return;
                                    } else if (tyreNo == '') {
                                        alert("Please fill tyre no for " + positionName + " " + positionNo);
                                        document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).focus();
                                        document.getElementById("tyreMfr" + positionName + axleDetailId + positionNo).value = 0;
                                        return;
                                    } else {
                                        url = './updateVehicleTyreNo.do';
                                        $.ajax({
                                            url: url,
                                            data: {
                                                axleDetailId: axleDetailId, positionName: positionName, positionNo: positionNo,
                                                updateValue: val, depthVal: treadDepth, updateType: updateType, vehicleId: vehicleId,
                                                tyreNo: tyreNo
                                            },
                                            type: "GET",
                                            success: function (response) {
                                                if (response.toString().trim() == 0) {
                                                    document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).value = '';
                                                    document.getElementById("treadDepth" + positionName + axleDetailId + positionNo).value = '';
                                                    document.getElementById("tyreMfr" + positionName + axleDetailId + positionNo).value = 0;
                                                    alert("Tyre No Already Mapped Please Enter Valid Tyre No");
                                                    document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).focus();
                                                }
                                            },
                                            error: function (xhr, status, error) {
                                            }
                                        });
                                    }

                                }

                                //jp End Ajex insCompny list
                                                                                      </script>

                    <table class="table table-info mb30 table-hover"  id="oEMtable">
                    <thead>
                    <tr >
                        <th  align="center" height="30" >Sno</th>
                        <th   align="center" height="30">Stepney/Battery</th>
                        <th   align="center" height="30">Details</th>
                    </tr>
                    </thead>

                    
                 <input type="hidden" name="oemIdCount" id="oemIdCount" value="<c:out value="${oemListSize}"/>" />
                <input type="hidden" name="rowCounted" id="rowCounted" value=""/>
               <script>
                                    function vehicleStepneyDetails(val, sno) {

                                        if (val == 1) {

                                            $("#oemMfr" + sno).show();
                                            $("#oemDepth" + sno).show();
                                            $("#oemTyreNo" + sno).show();
                                            $("#oemBattery" + sno).hide();
                                            //                        
                                        } else if (val == 2) {
                                            $("#oemBattery" + sno).show();
                                            $("#oemMfr" + sno).hide();
                                            $("#oemDepth" + sno).hide();
                                            $("#oemTyreNo" + sno).hide();

                                        }
                                    }

                                                                                                 </script>
               <script>

                                    function insertVehicleAxle() {

                                        var vehicleId = $("#vehicleId").val();
                                        var url = '';
                                        var oemId = [];
                                        var oemDetailsId = [];
                                        var oemMfr = [];
                                        var oemDepth = [];
                                        var oemTyreNo = [];
                                        var oemBattery = [];
                                        var oemDetailsIds = document.getElementsByName("oemDetailsId");
                                        var oemMfrs = document.getElementsByName("oemMfr");
                                        var oemDepths = document.getElementsByName("oemDepth");
                                        var oemTyreNos = document.getElementsByName("oemTyreNo");
                                        var oemBatterys = document.getElementsByName("oemBattery");
                                        var oemIds = document.getElementsByName("oemId");
                                        for (var i = 0; i < oemDetailsIds.length; i++) {
                                            oemDetailsId.push(oemDetailsIds[i].value);
                                            oemMfr.push(oemMfrs[i].value);
                                            oemTyreNo.push(oemTyreNos[i].value);
                                            oemDepth.push(oemDepths[i].value);
                                            oemBattery.push(oemBatterys[i].value);
                                            oemId.push(oemIds[i].value);
                                            if (oemDetailsIds[i].value == 1) {
                                                if (oemMfrs[i].value == '') {
                                                    alert('Please select MFR ');
                                                    return;
                                                } else if (oemTyreNos[i].value == '') {
                                                    alert('Please Enter The TyreNo ');
                                                    return;
                                                } else if (oemDepths[i].value == '') {
                                                    alert('Please Enter The Depth ');
                                                    return;
                                                }
                                            } else if (oemDetailsIds[i].value == 2) {
                                                if (oemBattery[i].value == '') {
                                                    alert('Please select battty ');
                                                    return;
                                                }
                                            }

                                        }
                                        var insertStatus = 0;
                                        var oemIdResponse = "";
                                        url = './insertOemDetails.do';
                                        $("#oEmNext").hide();
                                        $.ajax({
                                            url: url,
                                            data: {oemDetailsIdVal: oemDetailsId, vehicleId: vehicleId, oemMfrVal: oemMfr, oemTyreNoVal: oemTyreNo, oemDepthVal: oemDepth,
                                                oemBatteryVal: oemBattery, oemIdVal: oemId
                                            },
                                            type: "GET",
                                            dataType: 'json',
                                            success: function (data) {
                                                var r = 0;
                                                $.each(data, function (i, data) {
                 //                                alert(data.OemId);
                                                    oemIds[r].value = data.OemId;
                                                    r++;
                                                });
                                                $("#Status").text("Vehicle OEM added sucessfully ");
                                                $("#oEmNext").show();
                                            },
                                            error: function (xhr, status, error) {
                                            }
                                        });
                                        //                        $("#oEmNext").hide();
                                    }

                                    function addOem(sno, oemDetailsId, batteryNo, tyreNo, depth, mfrId, oemId, addType) {
                                        var rowCount = sno;
                                        var style = "text2";
                                        rowCount = document.getElementById('rowCounted').value;
                                        rowCount++;


                                        var tab = document.getElementById("oEMtable");
                                        var newrow = tab.insertRow(rowCount);
                                        newrow.id = 'rowId' + rowCount;

                                        cell = newrow.insertCell(0);
                                        var cell0 = "<td class='text1' height='25' ><input type='hidden' name='serNO' id='serNO" + rowCount + "' value='" + rowCount + "'  class='form-control' />" + rowCount + "</td>";
                                        cell.setAttribute("className", style);
                                        cell.innerHTML = cell0;

                                        cell = newrow.insertCell(1);
                                        var cell1 = "";
                                        var oemDetailsName = "";
                                        if (oemDetailsId == 1) {
                                            oemDetailsName = "Stepney";
                                        } else {
                                            oemDetailsName = "Battery";
                                        }
                                        if (addType == 1) {
                                            cell1 = "<td class='text1' height='30'><input type='hidden' id='oemDetailsId" + rowCount + "' style='width:125px'  name='oemDetailsId' value='" + oemDetailsId + "'>'" + oemDetailsName + "'</td>";
                                        } else {
                                            cell1 = "<td class='text1' height='30'><select class='form-control' id='oemDetailsId" + rowCount + "' style='width:125px'  name='oemDetailsId' value='" + oemDetailsId + "' onchange='batteryTextHideShow(this.value," + rowCount + ");'><option value='1'>Stepney</option><option value='2'>battery</option></select></td>";
                                        }
                                        cell.setAttribute("className", style);
                                        cell.innerHTML = cell1;

                                        cell = newrow.insertCell(2);
                                        var cell2 = "<td  height='30' class='tex1' ><span id='batterySpan" + rowCount + "' style='display:none'>Bat:<input type='text'  name='oemBattery' id='oemBattery" + rowCount + "' maxlength='13' placeholder='BatteryNo'  size='20' class='form-control' value='" + batteryNo + "'  /></span><span id='tyreSpan" + rowCount + "'>Mfr:<select name='oemMfr' id='oemMfr" + rowCount + "' class='form-control' style='width:124px'  ><option value=0>--Select--</option><c:if test = "${tyreItemList != null}" ><c:forEach items="${tyreItemList}" var="Type"><option value='<c:out value="${Type.itemId}" />'><c:out value="${Type.itemName}" /></option></c:forEach > </c:if></select>\n\
                                                                                                                       TyreNo:<input type='text'  name='oemTyreNo' id='oemTyreNo" + rowCount + "' placeholder='TyreNo'   size='20' class='form-control' value='" + tyreNo + "'  />Depth:<input type='text'  name='oemDepth' id='oemDepth" + rowCount + "' maxlength='13'   size='20' class='form-control' placeholder='TyreDepth' value='" + depth + "' />mm</span></td>";
                                        cell.setAttribute("className", style);
                                        cell.innerHTML = cell2;
                                        $("#oemMfr" + rowCount).val(mfrId);
                                        if (addType == 1) {
                                            if (oemDetailsId == 2) {
                                                $("#batterySpan" + rowCount).show();
                                                $("#tyreSpan" + rowCount).hide();
                                            }
                                        }
                                        cell = newrow.insertCell(3);
                                        var cell3 = "<td class='text1' height='25' ><input type='hidden' name='oemId' id='oemId" + rowCount + "' value='" + oemId + "'  class='form-control' /></td>";
                                        cell.setAttribute("className", style);
                                        cell.innerHTML = cell3;
                                        document.getElementById('rowCounted').value = rowCount;
                                    }

                                    function batteryTextHideShow(value, rowCount) {
                                        if (value == 1) {
                                            $("#batterySpan" + rowCount).hide();
                                            $("#tyreSpan" + rowCount).show();
                                        } else {
                                            $("#batterySpan" + rowCount).show();
                                            $("#tyreSpan" + rowCount).hide();
                                        }
                                    }

                                     </script>  
                                <%int addRowCount = 1;%>    
                                <c:if test="${oemList != null}">
                                    <c:forEach items="${oemList}" var="item">
                        <script>
                            addOem('<%=addRowCount%>', '<c:out value="${item.oemDetailsId}"/>', '<c:out value="${item.oemBattery}"/>', '<c:out value="${item.oemTyreNo}"/>', '<c:out value="${item.oemDepth}"/>', '<c:out value="${item.oemMfr}"/>', '<c:out value="${item.oemId}"/>', 1);
                        </script>
                                    </c:forEach>
                                </c:if>                     
                       
                  </table>  
                            

                <br>
                <center>
                            <td><a  class="nexttab" ><input type="button" class="button" value="Next" name="Next" /></a>

                </center>


            </div>
                   

            <div id="vehicleDepreciationTab">
                <table class="table table-info mb30 table-hover" >
                    <tr>
                        <td >Vehicle Cost</td>
                        <td ><input type="text" name="vehicleDepreciationCost" id="vehicleDepreciationCost" class="form-control" value='<c:out value="${vehicleCost}"/>' style="width:250px;" readonly></td>

                        <td >Depreciation Year</td>
                        <td >

                            <select name="depreciationType" id="depreciationType" class="form-control"  style="width:250px;" onChange="removeRowDetails();" disabled>
                                <option value="0">Select</option>
                                <option value="1">1year</option>
                                <option value="2">2year</option>
                                <option value="3">3year</option>
                                <option value="4">4year</option>
                                <option value="5">5year</option>
                            </select>
                        </td>
                    <script>
        document.getElementById("depreciationType").value = '<c:out value="${depreciationSize}"/>';
                    </script>
                    </tr>

                </table>
                <table class="table table-info" >
                     <thead>
                        <th  align="center" height="30" >Sno</th>
                        <th   align="center" height="30">Year</th>
                        <th  height="30" >Depreciation(%)</th>
                        <th  height="30" >Annual Depreciation Cost</th>
                        <th  height="30" >Monthly Depreciation Cost</th>
                        </thead>
                    
                    <%int rcount = 1;%>
                    <c:if test="${depreciationList != null}">
                        <c:forEach items="${depreciationList}" var="dep">
                            <tr>
                                <td  height='25' ><input type='hidden' name='serNO' id='serNO<%=rcount%>' value='<%=rcount%>'  class='form-control' /><%=rcount%></td>
                                <td  height='30'>
                                    <input type='hidden' name='vehicleYear' id='vehicleYear<%=rcount%>' value='<%=rcount%>'  class='form-control' />
                                    <label name='vehicleYears' id='vehicleYears<%=rcount%>'><%=rcount%>-Year</label>
                                </td>
                                <td height='30' >
                                    <input type='text' value='<c:out value="${dep.depreciation}"/>' name='depreciation' id='depreciation<%=rcount%>' maxlength='13'   size='20' class='form-control'  onchange="vehicleCostCalculation('<%=rcount%>');" readonly=/>
                                </td>
                                <td  height='30'><input type='hidden' value='<c:out value="${dep.yearCost}"/>' name='yearCost' id='yearCost<%=rcount%>' maxlength='13'   size='20' class='form-control'    /><span id='yearCostSpan<%=rcount%>' ><c:out value="${dep.yearCost}"/></span></td>
                                <td height='30' ><input type='hidden' value='<c:out value="${dep.perMonth}"/>' name='perMonth' id='perMonth<%=rcount%>' maxlength='13'   size='20' class='form-control' onchange='perMonthCalculation(<%=rcount%>);'  /><span id='perMonthSpan<%=rcount%>'><c:out value="${dep.perMonth}"/></span></td>
                            </tr>
                            <%rcount++;%>
                        </c:forEach>
                    </c:if>


                </table>
                <br>
                <br>
                <center>
                    <input type="button" class="button" name="Save" id="depreciationSave" value="Save" onclick="saveVehicleDepreciation(this.name);" style="display: none"/>&nbsp;
                    <a  class="nexttab" ><input type="button" class="button" value="Next" name="Next" /></a>
                </center>
                <script type="text/javascript">
                    function saveVehicleDepreciation() {
                        var depreciationType = document.getElementById("depreciationType").value;
                        var vehicleYear = document.getElementsByName("vehicleYear");
                        var depreciation = document.getElementsByName("depreciation");
                        var yearCost = document.getElementsByName("yearCost");
                        var perMonth = document.getElementsByName("perMonth");
                        var status = 0;
                        for (var i = 1; i <= depreciationType; i++) {
                            if ($("#depreciation" + i).val() == '') {
                                var vehicleYear = $("#vehicleYear" + i).val();
                                alert("Please enter the depreciation for Year " + vehicleYear);
                                $("#depreciation" + i).focus();
                                return;
                            } else {
                                if (i == 1) {
                                    vehicleYear = $("#vehicleYear" + i).val();
                                    depreciation = $("#depreciation" + i).val();
                                    yearCost = $("#yearCost" + i).val();
                                    perMonth = $("#perMonth" + i).val();
                                } else {
                                    vehicleYear = vehicleYear + "~" + $("#vehicleYear" + i).val();
                                    depreciation = depreciation + "~" + $("#depreciation" + i).val();
                                    yearCost = yearCost + "~" + $("#yearCost" + i).val();
                                    perMonth = perMonth + "~" + $("#perMonth" + i).val();
                                }
                                status = 1;
                            }
                            //alert("i == " + i);
                        }
                        if (status == 1) {
                            var responseStatus = 0;
                            var vehicleId = $("#vehicleId").val();
                            var url = '';
                            url = './saveVehicleDepreciation.do';
                            $.ajax({
                                url: url,
                                data: {vehicleYear: vehicleYear, depreciation: depreciation, yearCost: yearCost, perMonth: perMonth, vehicleId: vehicleId
                                },
                                type: "GET",
                                success: function (response) {
                                    responseStatus = response.toString().trim();
                                    if (responseStatus == 0) {
                                        $("#StatusMsg").text("Vehicle Premit added failed ");
                                    } else {
                                        $("#StatusMsg").text("Vehicle Permit added sucessfully ");
                                    }

                                },
                                error: function (xhr, status, error) {
                                }
                            });
                        }

                    }
                    function removeRowDetails() {
                        var tab = document.getElementById("vehicleCostTable");
                        //find current no of rows
                        var rowCountLength = document.getElementById('vehicleCostTable').rows.length;
                        rowCountLength = parseInt(rowCountLength);
                        for (var s = rowCountLength; s > 1; s--) {
                            $('#vehicleCostTable tr:last-child').remove();
                        }
                        vehicleCostAddRow();
                    }

                    function vehicleCostAddRow() {
                        var rowCount = "";
                        var style = "text2";
                        var axleCount = document.getElementById('depreciationType').value;
                        for (var i = 0; i < parseInt(axleCount); i++) {

                            var tab = document.getElementById("vehicleCostTable");
                            var rowCount = document.getElementById("vehicleCostTable").rows.length;
                            var newrow = tab.insertRow(rowCount);
                            newrow.id = 'rowId' + rowCount;

                            var cell = newrow.insertCell(0);
                            var cell1 = "<td  height='25' ><input type='hidden' name='serNO' id='serNO" + rowCount + "' value='" + rowCount + "'  class='form-control' />" + rowCount + "</td>";
                            cell.setAttribute("className", style);
                            cell.innerHTML = cell1;

                            cell = newrow.insertCell(1);
                            var cell2 = "<td  height='30'><input type='hidden' name='vehicleYear' id='vehicleYear" + rowCount + "' value='" + rowCount + "'  class='form-control' /><label name='vehicleYears' id='vehicleYears" + rowCount + "'>" + (rowCount) + "-Year</label></td>";
                            cell.setAttribute("className", style);
                            cell.innerHTML = cell2;

                            cell = newrow.insertCell(2);
                            var cell3 = "";
                            if (parseInt(axleCount - 2) == parseInt(i)) {
                                cell3 = "<td height='30' ><input type='text' value = '' name='depreciation' id='depreciation" + rowCount + "' maxlength='13'   size='20' class='form-control'  onchange='vehicleCostCalculation(" + rowCount + ");lastRowPercent(" + rowCount + ")' /></td>";
                            } else if (parseInt(axleCount - 1) == parseInt(i)) {
                                cell3 = "<td height='30' ><input type='text' value = '' name='depreciation' id='depreciation" + rowCount + "' maxlength='13'   size='20' class='form-control'  onchange='vehicleCostCalculation(" + rowCount + ");' readonly /></td>";
                            } else {
                                cell3 = "<td height='30' ><input type='text' value = '' name='depreciation' id='depreciation" + rowCount + "' maxlength='13'   size='20' class='form-control'  onchange='vehicleCostCalculation(" + rowCount + ");' /></td>";
                            }
                            cell.setAttribute("className", style);
                            cell.innerHTML = cell3;

                            cell = newrow.insertCell(3);
                            var cell4 = "<td  height='30'><input type='hidden' value = '' name='yearCost' id='yearCost" + rowCount + "' maxlength='13'   size='20' class='form-control'    /><span id='yearCostSpan" + rowCount + "'>0.00</span></td>";
                            cell.setAttribute("className", style);
                            cell.innerHTML = cell4;

                            cell = newrow.insertCell(4);
                            var cell5 = "";
                            if (parseInt(axleCount - 1) == parseInt(i)) {
                                cell5 = "<td height='30' ><input type='hidden' value = '' name='perMonth' id='perMonth" + rowCount + "' maxlength='13'   size='20' class='form-control'  onchange='perMonthCalculation(" + rowCount + ");'  /><span id='perMonthSpan" + rowCount + "'>0.00</span></td>";
                            } else if (parseInt(axleCount) == parseInt(i) + 1) {
                                cell5 = "<td height='30' ><input type='hidden' value = '' name='perMonth' id='perMonth" + rowCount + "' maxlength='13'   size='20' class='form-control'  onchange='perMonthCalculation(" + rowCount + ");'  /><span id='perMonthSpan" + rowCount + "'>0.00</span></td>";
                            } else {
                                cell5 = "<td height='30' ><input type='hidden' value = '' name='perMonth' id='perMonth" + rowCount + "' maxlength='13'   size='20' class='form-control' onchange='perMonthCalculation(" + rowCount + ");'  /><span id='perMonthSpan" + rowCount + "'>0.00</span></td>";
                            }
                            cell.setAttribute("className", style);
                            cell.innerHTML = cell5;

                        }

                    }


                    function lastRowPercent(rowCount) {
                        var depreciation = document.getElementsByName("depreciation");
                        var yearCost = document.getElementsByName("yearCost");
                        var perMonth = document.getElementsByName("perMonth");
                        var depreciationLength = parseInt(depreciation.length - 1);
                        var percentSum = 0;
                        var row = parseInt(rowCount + 1);
                        for (var i = 0; i < depreciationLength; i++) {
                            percentSum += parseInt(depreciation[i].value);
                        }
                        var balancePercent = parseInt(100 - percentSum);
                        if (balancePercent <= 0) {
                            alert("Depreciation planning is incorrect please plan again");
                            for (var i = 1; i <= depreciationLength; i++) {
                                $("#depreciation" + i).val('');
                                $("#yearCost" + i).val('');
                                $("#yearCostSpan" + i).text('0.00');
                                $("#perMonth" + i).val('');
                                $("#perMonthSpan" + i).text('0.00');
                            }
                            $("#depreciation" + row).val('');
                            $("#yearCost" + row).val('');
                            $("#yearCostSpan" + row).text('0.00');
                            $("#perMonth" + row).val('');
                            $("#perMonthSpan" + row).text('0.00');
                        } else {
                            var row = parseInt(rowCount + 1);
                            $("#depreciation" + row).val(balancePercent);
                            vehicleCostCalculation(row);
                            $("#depreciation" + 1).focus();
                        }
                    }
                    function vehicleCostCalculation(rowCount) {
                        for (var i = 0; i < parseInt(rowCount); i++) {
                            var depCost = document.getElementById('depreciation' + rowCount).value;
                            var vehicleCost = document.getElementById('vehicleDepreciationCost').value;
                            var depCostPercent = parseInt(depCost) / 100;
                            var depCostPerYear = parseFloat(depCostPercent * parseInt(vehicleCost)).toFixed(2);
                            document.getElementById('yearCost' + rowCount).value = depCostPerYear
                            $('#yearCostSpan' + rowCount).text(depCostPerYear);
                            var perMothCost = parseFloat(depCostPerYear / 12);
                            document.getElementById('perMonth' + rowCount).value = perMothCost.toFixed(2);
                            $('#perMonthSpan' + rowCount).text(perMothCost.toFixed(2));
                        }
                    }



                </script>
            </div>            
            <div id="vehicleFileAttachments">
                            <%int img = 0;%>

                            <c:if test="${vehicleUploadsDetails != null}">
                                                                                                <table class="table table-info"   style="width: 950px;" align="center">
                                                                                                     <thead>
                         <th>SNo</th>
                         <th>&emsp;&emsp;&emsp;File</th>
                         <th>&emsp;&emsp;&emsp;&emsp;File Name</th>
                         <th>&nbsp;Remarks</th>
                </thead>
                                    <c:forEach items="${vehicleUploadsDetails}" var="cust">
                                                                                                        <tr>
                                                                                                            <td>&nbsp;<%=img+1%></td>
                                                                                                            <td style="width: 100px;"> 
                                                                                                                <img src="/throttle/displayVehicleLogoBlobData.do?uploadId=<c:out value ="${cust.uploadId}"/>"  style="width: 90px;height: 40px" data-toggle="modal" data-target="#myModal<%=img%>" title="<c:out value ="${cust.remarks}"/>"/>
                                                                                                            </td>
                                                                                                        <td>&emsp;&emsp;<c:out value ="${cust.fileName}"/></td>
                                                                                                        <td><c:out value ="${cust.remarks}"/></td>
                                                                                                        </tr>
                                                                                                        
                                                                                                                  <div class="modal fade" id="myModal<%=img%>" role="dialog" style="width: 100%;height: 100%">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" onclick="resetTheSlabDetails()">&times;</button>
                            <h4 class="modal-title"><c:out value ="${cust.fileName}"/></h4>
                        </div>
                        <div class="modal-body" id="slabRateListSet" style="width: 100%;height: 80%">
  <img src="/throttle/displayVehicleLogoBlobData.do?uploadId=<c:out value ="${cust.uploadId}"/>" style="width: 95%;height: 75%" class="img-responsive" title="<c:out value ="${cust.remarks}"/>">
                        </div>
                        <div class="modal-footer">


                        </div>
                    </div>
                </div>
            </div>
                                                                                                        <input  type="hidden" id="uploadId" name="uploadId" value="<c:out value ="${cust.uploadId}"/>"/>
                                        <%img++;%>
                                    </c:forEach>
                               <script>
                function resetTheSlabDetails() {
                    $('#slabRateListSet').html('');
                }

            </script>                                                                  </table>
                            </c:if>
                

            </div>

        </div>

                    <script>

                        $(".nexttab").click(function () {
                            var selected = $("#tabs").tabs("option", "selected");
                            $("#tabs").tabs("option", "selected", selected + 1);
                        });
                        $(".pretab").click(function () {
                            var selected = $("#tabs").tabs("option", "selected");
                            $("#tabs").tabs("option", "selected", selected - 1);
                        });


                        </script>
                    <c:if test="${vehicleId > '0' && editStatus == '1'}">
        <script>
                            $("#vehicleDetailUpdateButton").show();
                            $("#vehicleDetailSaveButton").hide();
                            $("#tyreSave").show();
                            $("#depreciationSave").hide();
                            $("#oEmNext").show();
                            $("#insuranceNext").show();
                            $("#roadTaxNext").show();
                            $("#fcNext").show();
                            $("#permitNext").show();
                                                                                                                                                        </script>

                    </c:if>       
                    <c:if test="${vehicleId > '0' && editStatus == '0'}">
        <script>
                            $("#vehicleDetailUpdateButton").hide();
                            $("#vehicleDetailSaveButton").hide();
                            $("#tyreSave").hide();
                            $("#depreciationSave").hide();
                            $("#oEmNext").hide();
                            $("#insuranceNext").hide();
                            $("#roadTaxNext").hide();
                            $("#fcNext").hide();
                            $("#permitNext").hide();
                        </script>

                    </c:if>  
    </div>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</div>
    </div>
    </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>