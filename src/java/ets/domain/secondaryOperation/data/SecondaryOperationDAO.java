
package ets.domain.secondaryOperation.data;
import com.ibatis.sqlmap.client.SqlMapClient;
import ets.arch.exception.FPRuntimeException;
import ets.domain.secondaryOperation.business.SecondaryOperationTO;
import ets.domain.util.FPLogUtils;
import ets.domain.util.ThrottleConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

/**
 *
 * @author Arul
 */
public class SecondaryOperationDAO extends SqlMapClientDaoSupport {

    private final int errorStatus = 4;
    private final static String CLASS = "SecondaryOperationDAO";

    public ArrayList getCity(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        map.put("cityName", operationTO.getCityName() + "%");
        ArrayList cityList = new ArrayList();
        try {

            cityList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getCity", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCity Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getCity", sqlException);
        }
        return cityList;

    }

    public ArrayList getCustomerPoints(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        map.put("cityName", operationTO.getCityName() + "%");
        map.put("customerId", operationTO.getCustomerId());
        ArrayList cityList = new ArrayList();
        try {

            cityList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getCustomerPoints", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCity Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getCity", sqlException);
        }
        return cityList;

    }

    public ArrayList getSecondaryCustomerDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        map.put("customerName", operationTO.getCustomerName() + "%");
        ArrayList customerName = new ArrayList();
        try {

            customerName = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryCustomerDetails", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getSecondaryCustomerDetails", sqlException);
        }
        return customerName;

    }

    public String getConfigDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        String configDetails = "";
        try {
            configDetails = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getConfigDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConfigDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConfigDetails", sqlException);
        }

        return configDetails;
    }

    public int insertContractRouteMater(SecondaryOperationTO operationTO) {
        Map map = new HashMap();

        int insertContractRouteMater = 0;
        try {
            map.put("routeId", insertContractRouteMater);
            String routeCode = "";
            String[] temp = null;
            routeCode = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getSecondaryRouteCode", map);
            if (routeCode == null) {
                routeCode = "SRC-0001";
            } else if (routeCode.length() == 1) {
                routeCode = "SRC-000" + routeCode;
            } else if (routeCode.length() == 2) {
                routeCode = "SRC-00" + routeCode;
            } else if (routeCode.length() == 3) {
                routeCode = "SRC-0" + routeCode;
            }
            map.put("routeCode", routeCode);
            map.put("routeName", operationTO.getRouteName());
            map.put("customerId", operationTO.getCustomerId());
            map.put("fromDate", operationTO.getRouteValidFrom());
            map.put("toDate", operationTO.getRouteValidTo());
            map.put("fuelCost", operationTO.getFuelCost());
            map.put("totalKm", operationTO.getDistance());
            map.put("totalHours", operationTO.getTotalHours());
            map.put("totalMinutes", operationTO.getTotalMinutes());
            map.put("totalReeferHours", operationTO.getTotalReeferHours());
            map.put("totalReeferMinutes", operationTO.getTotalReeferMinutes());
            map.put("fixedKm", operationTO.getFixedKmPerMonth());
            map.put("fixedReeferHours", operationTO.getFixedReeferHours());
            map.put("fixedReeferMinutes", operationTO.getFixedReeferMinutes());
            map.put("totalWaitMinutes", operationTO.getTotalWaitMinutes());
            map.put("averageKM", operationTO.getAverageKM());
            map.put("userId", operationTO.getUserId());
            map.put("totalReferMin", operationTO.getTotalReferMin());
            ////System.out.println("map = " + map);
            insertContractRouteMater = (Integer) getSqlMapClientTemplate().insert("secondaryOperation.insertContractRouteMater", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractRouteMater Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertContractRouteMater", sqlException);
        }

        return insertContractRouteMater;
    }

    public int insertContractMaster(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        int response = 0;
        try {

            map.put("customerId", operationTO.getCustomerId());
            map.put("fromDate", operationTO.getRouteValidFrom());
            map.put("toDate", operationTO.getRouteValidTo());
            map.put("extraKmCalculation", operationTO.getExtraKmCalculation());
            map.put("contractCngCost", operationTO.getContractCngCost());
            map.put("contractDieselCost", operationTO.getContractDieselCost());
            map.put("rateChangeOfCng", operationTO.getRateChangeOfCng());
            map.put("rateChangeOfDiesel", operationTO.getRateChangeOfDiesel());
            map.put("userId", operationTO.getUserId());
            ////System.out.println("map = " + map);
            response = (Integer) getSqlMapClientTemplate().insert("secondaryOperation.insertContractMaster", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertContractMaster", sqlException);
        }

        return response;
    }

    public int insertContractDetails(SecondaryOperationTO operationTO, int contractId) {
        Map map = new HashMap();
        int response = 0;
        try {

            map.put("contractId", contractId);
            map.put("vehicleTypeId", operationTO.getVehTypeId());
            map.put("noOfVehicle", operationTO.getNoOfVehicle());
            map.put("fixedKm", operationTO.getFixedKm());
            map.put("fixedKmCharge", operationTO.getFixedKmCharge());
            map.put("extraKmCharge", operationTO.getExtraKmCharge());
            map.put("userId", operationTO.getUserId());
            ////System.out.println("map = " + map);
            response = (Integer) getSqlMapClientTemplate().insert("secondaryOperation.insertContractDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertContractDetails", sqlException);
        }

        return response;
    }

    public int insertContractRouteDetails(SecondaryOperationTO operationTO, int insertContractRouteMater) {
        Map map = new HashMap();
        int insertContractRouteDetails = 0;
        try {
            map.put("secondaryRouteId", insertContractRouteMater);
            map.put("pointId", operationTO.getCityId());
            map.put("pointName", operationTO.getCityName());
            map.put("pointType", operationTO.getPointType());
            map.put("pointSequence", operationTO.getPointSequence());
            map.put("pointAddress", operationTO.getPointAddresss());
            map.put("latitude", operationTO.getLatitude());
            map.put("longitude", operationTO.getLongitude());
            map.put("travelKm", operationTO.getTravelKm());
            map.put("travelHours", operationTO.getTravelHour());
            map.put("travelMinutes", operationTO.getTravelMinute());
            map.put("reeferHour", operationTO.getReeferHour());
            map.put("reeferMinute", operationTO.getReeferMinute());
            map.put("waitMinute", operationTO.getWaitMinute());
            map.put("userId", operationTO.getUserId());
            ////System.out.println("map = " + map);
            insertContractRouteDetails = (Integer) getSqlMapClientTemplate().update("secondaryOperation.insertContractRouteDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractRouteDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertContractRouteDetails", sqlException);
        }

        return insertContractRouteDetails;
    }

    public int insertContractRouteCostDetails(SecondaryOperationTO operationTO, int insertContractRouteMater) {
        Map map = new HashMap();
        int insertContractRouteCostDetails = 0;
        try {
            map.put("secondaryRouteId", insertContractRouteMater);
            map.put("vehTypeId", operationTO.getVehTypeId());
            map.put("fuelCostPerKms", operationTO.getFuelCostPerKms());
            map.put("tollAmounts", operationTO.getTollAmounts());
            map.put("addlTollAmounts", operationTO.getAddlTollAmounts());
            map.put("miscCostKm", operationTO.getMiscCostKm());
            map.put("totExpense", operationTO.getTotExpense());
            map.put("parkingCost", operationTO.getParkingCost());
            map.put("userId", operationTO.getUserId());
            map.put("fuelCostPerMin", operationTO.getFuelCostPerMin());
            map.put("fuelCostPerMin", operationTO.getFuelCostPerMin());
            ////System.out.println("map = " + map);
            insertContractRouteCostDetails = (Integer) getSqlMapClientTemplate().update("secondaryOperation.insertContractRouteCostDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractRouteCostDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertContractRouteCostDetails", sqlException);
        }

        return insertContractRouteCostDetails;
    }

    public String getSecondaryRouteCode(int insertContractRouteMater) {
        Map map = new HashMap();
        String secondaryRouteCode = "";
        map.put("routeId", insertContractRouteMater);
        try {
            secondaryRouteCode = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getSecondaryRouteCode", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryRouteCode Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryRouteCode", sqlException);
        }

        return secondaryRouteCode;
    }

    public ArrayList getSecondaryContractRouteMasterlist(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList SecondaryContractRouteList = new ArrayList();
        map.put("customerId", operationTO.getCustomerId());
        ////System.out.println("map = " + map);
        try {

            SecondaryContractRouteList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryContractRouteMaster", map);
            ////System.out.println("SecondaryContractRouteList.size() = " + SecondaryContractRouteList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "MfrList", sqlException);
        }
        return SecondaryContractRouteList;

    }

    public int saveSecondaryTripSchedule(SecondaryOperationTO operationTO, int userId, String passedValue) {
        Map map = new HashMap();
        int routeId = 0;
        int insertStatus = 0;
        try {
            String[] temp1 = null;
            String[] temp2 = null;
            map.put("customerId", operationTO.getCustomerId());
            map.put("tripStatusId", 19);
            map.put("userId", userId);
            String checkSecondaryTripSchedule = "";
            if (passedValue.contains(",")) {
                temp1 = passedValue.split(",");
                for (int i = 0; i < temp1.length; i++) {
                    if (temp1[i].contains("~")) {
                        temp2 = temp1[i].split("~");
                        map.put("scheduleDate", temp2[0]);
                        map.put("routeId", temp2[1]);
                        map.put("status", temp2[2]);
                        ////System.out.println("map====" + map);
                        checkSecondaryTripSchedule = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.checkSecondaryTripSchedule", map);
                        if (checkSecondaryTripSchedule == null) {
                            insertStatus += (Integer) getSqlMapClientTemplate().update("secondaryOperation.insertSecondaryTripSchedule", map);
                        } else {
                            insertStatus += (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateSecondaryTripSchedule", map);
                        }
                    }
                }
            }
        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveRoute Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveRoute", sqlException);
        }

        return insertStatus;
    }

    public ArrayList getSecondaryTripCustomer(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList secondaryTripCustomerList = new ArrayList();
        map.put("customerId", operationTO.getCustomerId());
        try {

            secondaryTripCustomerList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryTripCustomer", map);

            ////System.out.println("secondaryTripCustomerList size=" + secondaryTripCustomerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "bunkList", sqlException);
        }
        return secondaryTripCustomerList;
    }

    public ArrayList getSecondaryTripScheduleList(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList secondaryTripScheduleList = new ArrayList();
        map.put("customerId", operationTO.getCustomerId());
        map.put("fromDate", operationTO.getFromDate());
        map.put("toDate", operationTO.getToDate());
        ////System.out.println("map = " + map);
        try {

            secondaryTripScheduleList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryTripScheduleList", map);

            ////System.out.println("getSecondaryTripScheduleList size=" + secondaryTripScheduleList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryTripScheduleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getSecondaryTripScheduleList", sqlException);
        }
        return secondaryTripScheduleList;
    }

    public String getTripScheduleStatus(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        String secondaryTripSchedule = "";
        map.put("customerId", operationTO.getCustomerId());
        map.put("secondaryRouteId", operationTO.getSecondaryRouteId());
        map.put("scheduleDate", operationTO.getDate());
        ////System.out.println("map = " + map);
        try {

            secondaryTripSchedule = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getTripScheduleStatus", map);

            ////System.out.println("getSecondaryTripScheduleList size=" + secondaryTripSchedule);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripScheduleStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getTripScheduleStatus", sqlException);
        }
        return secondaryTripSchedule;
    }

    public String getDateWiseTripScheduleStatus(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        String secondaryTripSchedule = "";
        map.put("customerId", operationTO.getCustomerId());
        map.put("secondaryRouteId", operationTO.getSecondaryRouteId());
        map.put("scheduleDate", operationTO.getDate());
        ////System.out.println("map = " + map);
        try {

            secondaryTripSchedule = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getDateWiseTripScheduleStatus", map);

            ////System.out.println("getSecondaryTripScheduleList size=" + secondaryTripSchedule);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripScheduleStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getTripScheduleStatus", sqlException);
        }
        return secondaryTripSchedule;
    }

    public ArrayList getSecondaryContract() {
        Map map = new HashMap();
        ArrayList getSecondaryContractView = new ArrayList();

        try {
            getSecondaryContractView = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryContract", map);
            ////System.out.println(" getTyreMfr Size :: :: " + getSecondaryContractView.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryContract Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryContract", sqlException);
        }
        return getSecondaryContractView;
    }

    public ArrayList getContractRouteDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList routeDetails = new ArrayList();
        map.put("routeId", operationTO.getSecondaryRouteId());
        map.put("customerId", operationTO.getCustomerId());
        try {
            routeDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getContractRouteDetails", map);
            ////System.out.println(" getContractRouteDetails Size :: :: " + routeDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractRouteDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getContractRouteDetails", sqlException);
        }
        return routeDetails;
    }

    public ArrayList getContractRateCostDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList routeDetails = new ArrayList();
        map.put("routeId", operationTO.getSecondaryRouteId());
        try {
            routeDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getContractRateCostDetails", map);
            ////System.out.println(" getContractRateCostDetails Size :: :: " + routeDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractRateCostDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getContractRateCostDetails", sqlException);
        }
        return routeDetails;
    }

    public ArrayList getContractRateCostDetailsEdit(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList routeDetails = new ArrayList();
        map.put("routeId", operationTO.getSecondaryRouteId());
        map.put("customerId", operationTO.getCustomerId());
        ////System.out.println("map in the dao = " + map);
        try {
            routeDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getContractRateCostDetailsEdit", map);
            ////System.out.println(" getContractRateCostDetails Size :: :: " + routeDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractRateCostDetailsEdit Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getContractRateCostDetailsEdit", sqlException);
        }
        return routeDetails;
    }

    public int updateSecondaryRateCost(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        int updateSecondaryRateCost = 0;
        map.put("secondaryRouteCostId", operationTO.getSecondaryRouteCostId());
        map.put("secondaryRouteId", operationTO.getSecondaryRouteId());
        map.put("vehicleTypeId", operationTO.getVehicleTypeId());
        map.put("perKmFuelCost", operationTO.getFuelCostPerKms());
        map.put("perKmFuelCost", operationTO.getFuelCostPerKms());
        map.put("totalExpense", operationTO.getTotalExpense());
        map.put("userId", operationTO.getUserId());
        ////System.out.println("map in the dao= " + map);
        try {
            updateSecondaryRateCost = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateSecondaryRateCost", map);
            ////System.out.println("updateSecondaryRateCost = " + updateSecondaryRateCost);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateSecondaryRateCost Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateSecondaryRateCost", sqlException);
        }

        return updateSecondaryRateCost;
    }

    public ArrayList getRouteContractDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList routeDetails = new ArrayList();
        map.put("routeId", operationTO.getSecondaryRouteId());
        try {
            routeDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getRouteContractDetails", map);
            ////System.out.println(" getRouteContractDetails Size :: :: " + routeDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRouteContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRouteContractDetails", sqlException);
        }
        return routeDetails;
    }

    public ArrayList getRoutePointDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList routePointDetails = new ArrayList();
        map.put("routeId", operationTO.getSecondaryRouteId());
        ////System.out.println("map = " + map);
        try {
            routePointDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getRoutePointDetails", map);
            ////System.out.println(" getRouteContractDetails Size :: :: " + routePointDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRouteContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRouteContractDetails", sqlException);
        }
        return routePointDetails;
    }

    public int updateSecondaryTripSchedule(SecondaryOperationTO operationTO, int userId, String passedValue) {
        Map map = new HashMap();
        int routeId = 0;
        int insertStatus = 0;
        try {
            String[] temp1 = null;
            String[] temp2 = null;
            map.put("customerId", operationTO.getCustomerId());
            map.put("tripStatusId", 19);
            map.put("userId", userId);
            String checkSecondaryTripSchedule = "";
            if (passedValue.contains(",")) {
                temp1 = passedValue.split(",");
                for (int i = 0; i < temp1.length; i++) {
                    if (temp1[i].contains("~")) {
                        temp2 = temp1[i].split("~");
                        map.put("scheduleDate", temp2[0]);
                        map.put("routeId", temp2[1]);
                        map.put("status", temp2[2]);
                        ////System.out.println("map====" + map);
                        checkSecondaryTripSchedule = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.checkSecondaryTripSchedule", map);
                        if (checkSecondaryTripSchedule == null) {
                            insertStatus += (Integer) getSqlMapClientTemplate().update("secondaryOperation.insertSecondaryTripSchedule", map);
                        } else {
                            insertStatus += (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateSecondaryTripSchedule", map);
                        }
                    }
                }
            }
        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveRoute Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveRoute", sqlException);
        }

        return insertStatus;
    }

    public int updateContractRouteMater(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        int insertContractRouteMater = 0;
        try {
            map.put("routeId", insertContractRouteMater);
            String routeCode = "";
            String[] temp = null;
            routeCode = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getSecondaryRouteCode", map);
            if (routeCode == null) {
                routeCode = "SRC-0001";
            } else {
                routeCode = "SRC-0001";
            }
            map.put("secondaryRouteId", operationTO.getSecondaryRouteId());
            map.put("routeCode", routeCode);
            map.put("routeName", operationTO.getRouteName());
            map.put("customerId", operationTO.getCustomerId());
            map.put("fromDate", operationTO.getRouteValidFrom());
            map.put("toDate", operationTO.getRouteValidTo());
            map.put("fuelCost", operationTO.getFuelCost());
            map.put("totalKm", operationTO.getDistance());
            map.put("totalHours", operationTO.getTotalHours());
            map.put("totalMinutes", operationTO.getTotalMinutes());
            map.put("totalReeferHours", operationTO.getTotalReeferHours());
            map.put("totalReeferMinutes", operationTO.getTotalReeferMinutes());
            map.put("fixedKm", operationTO.getFixedKmPerMonth());
            map.put("fixedReeferHours", operationTO.getFixedReeferHours());
            map.put("fixedReeferMinutes", operationTO.getFixedReeferMinutes());
            map.put("totalWaitMinutes", operationTO.getTotalWaitMinutes());
            map.put("averageKM", operationTO.getAverageKM());
            map.put("totalReferMin", operationTO.getTotalReferMin());

            map.put("userId", operationTO.getUserId());
            ////System.out.println("map = " + map);
            insertContractRouteMater = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateContractRouteMater", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateContractRouteMater Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateContractRouteMater", sqlException);
        }

        return insertContractRouteMater;
    }

    public int updateContractRouteDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        int insertContractRouteDetails = 0;
        try {
            map.put("secondaryRouteDetailId", operationTO.getSecondaryRouteDetailId());
            map.put("pointId", operationTO.getCityId());
            map.put("pointName", operationTO.getCityName());
            map.put("pointType", operationTO.getPointType());
            map.put("pointSequence", operationTO.getPointSequence());
            map.put("pointAddress", operationTO.getPointAddresss());
            map.put("latitude", operationTO.getLatitude());
            map.put("longitude", operationTO.getLongitude());
            map.put("travelKm", operationTO.getTravelKm());
            map.put("travelHours", operationTO.getTravelHour());
            map.put("travelMinutes", operationTO.getTravelMinute());
            map.put("reeferHour", operationTO.getReeferHour());
            map.put("reeferMinute", operationTO.getReeferMinute());
            map.put("waitMinute", operationTO.getWaitMinute());
            map.put("activeStatus", operationTO.getActiveStatus());
            map.put("userId", operationTO.getUserId());
            ////System.out.println("map = " + map);
            insertContractRouteDetails = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateContractRouteDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateContractRouteDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateContractRouteDetails", sqlException);
        }

        return insertContractRouteDetails;
    }

    public int updateContractRouteCostDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        int insertContractRouteCostDetails = 0;
        try {
            map.put("secondaryRouteCostId", operationTO.getSecondaryRouteCostId());
            map.put("vehTypeId", operationTO.getVehTypeId());
            map.put("fuelCostPerKms", operationTO.getFuelCostPerKms());
            map.put("parkingCost", operationTO.getParkingCost());
            map.put("tollAmounts", operationTO.getTollAmounts());
            map.put("addlTollAmounts", operationTO.getAddlTollAmounts());
            map.put("miscCostKm", operationTO.getMiscCostKm());
            map.put("totExpense", operationTO.getTotExpense());
            map.put("userId", operationTO.getUserId());
            map.put("fuelCostPerMin", operationTO.getFuelCostPerMin());
            ////System.out.println("map = " + map);
            insertContractRouteCostDetails = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateContractRouteCostDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateContractRouteCostDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateContractRouteCostDetails", sqlException);
        }

        return insertContractRouteCostDetails;
    }

    public ArrayList getSecondaryVehicleList(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList routePointDetails = new ArrayList();
        map.put("customerName", operationTO.getCustomerName());
        ////System.out.println("map = " + map);
        try {

            routePointDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryVehicleList", map);
            ////System.out.println(" getRouteContractDetails Size :: :: " + routePointDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRouteContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRouteContractDetails", sqlException);
        }
        return routePointDetails;
    }

    public String getVehicleTypeId(String vehicleId) {
        Map map = new HashMap();
        String vehicleTypeId = "";
        map.put("vehicleId", vehicleId);
        try {
            vehicleTypeId = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getVehicleTypeId", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleTypeId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleTypeId", sqlException);
        }

        return vehicleTypeId;
    }

    public int updateTripScheduleStatus(int tripid, String customerId, String secondaryRouteId, String tripScheduleDate, int userId) {
        Map map = new HashMap();
        int updateTripScheduleStatus = 0;
        try {
            map.put("tripStatusId", "6");
            map.put("tripId", tripid);
            map.put("userId", userId);
            map.put("secondaryRouteId", secondaryRouteId);
            map.put("tripScheduleDate", tripScheduleDate);
            map.put("customerId", customerId);
            ////System.out.println("map = " + map);
            updateTripScheduleStatus = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateTripScheduleStatus", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTripScheduleStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateTripScheduleStatus", sqlException);
        }

        return updateTripScheduleStatus;
    }
     public int updateLeasingTripScheduleStatus(String vehicleId, String customerId, String sechduleMonth,  int userId) {
        Map map = new HashMap();
        int updateTripScheduleStatus = 0;
        try {

            map.put("vehicleId", vehicleId);
            map.put("userId", userId);
            map.put("customerId", customerId);
            map.put("scheduleMonth", sechduleMonth);

            System.out.println("map for Update Status = " + map);
            updateTripScheduleStatus = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateLeasingTripScheduleStatus", map);
            
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTripScheduleStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateTripScheduleStatus", sqlException);
        }

        return updateTripScheduleStatus;
    }

    /**
     * This method used to Get Route List.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getSecondaryCustomerPoints(SecondaryOperationTO operationTO, String customerId) {
        Map map = new HashMap();
        ArrayList SecondaryCustomerPointsList = new ArrayList();
        map.put("customerId", customerId);
        ////System.out.println("map = " + map);
        try {
            ////System.out.println("this is City Master");
            SecondaryCustomerPointsList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryCustomerPoints", map);
            ////System.out.println(" SecondaryCustomerPointsList =" + SecondaryCustomerPointsList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getcityMaster List", sqlException);
        }

        return SecondaryCustomerPointsList;
    }

    /**
     * This method used to Modify standardCharge .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int updateSecondaryRoute(SecondaryOperationTO operationTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int userid = userId;

        map.put("pointName", operationTO.getPointName());
        map.put("customerId", operationTO.getCustomerId());
        map.put("pointType", operationTO.getPointType());
        map.put("pointAddress", operationTO.getPointAddress());
        map.put("cityId", operationTO.getCityId());
        map.put("pointId", operationTO.getPointId());
        map.put("latitudePosition", operationTO.getLatitudePosition());
        map.put("longitudePosition", operationTO.getLongitudePosition());
        map.put("status", operationTO.getStatus());
        ////System.out.println("updateSecondaryRoute = " + map);
        try {

            status = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateSecondaryRoute", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateCityMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateCityMaster", sqlException);
        }

        return status;
    }

    public int saveSecondaryRoutes(SecondaryOperationTO operationTO, int userId) {
        Map map = new HashMap();
        int insertContractRouteMater = 0;
        map.put("pointName", operationTO.getPointName());
        map.put("customerId", operationTO.getCustomerId());
        map.put("pointType", operationTO.getPointType());
        map.put("pointAddress", operationTO.getPointAddress());
        map.put("cityId", operationTO.getCityId());
        map.put("latitudePosition", operationTO.getLatitudePosition());
        map.put("longitudePosition", operationTO.getLongitudePosition());
        map.put("userId", userId);
        try {
            ////System.out.println("map 1= " + map);
            insertContractRouteMater = (Integer) getSqlMapClientTemplate().update("secondaryOperation.saveSecondaryCustomerPoints", map);
//               ////System.out.println("map 1= " + insertContractRouteMater);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractRouteMater Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveSecondaryCustomerPoints", sqlException);
        }

        return insertContractRouteMater;
    }

    public ArrayList getSecondarySettlementDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList settlementDetails = new ArrayList();
        map.put("fromDate", operationTO.getFromDate());
        map.put("toDate", operationTO.getToDate());
        map.put("driverId", operationTO.getPrimaryDriverId());
        ////System.out.println("map = " + map);
        try {
            settlementDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondarySettlementDetails", map);
            ////System.out.println(" getSecondarySettlementDetails Size :: :: " + settlementDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondarySettlementDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondarySettlementDetails", sqlException);
        }
        return settlementDetails;
    }

    public ArrayList getTotalExpenseDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList totalExpense = new ArrayList();
        map.put("fromDate", operationTO.getFromDate());
        map.put("toDate", operationTO.getToDate());
        map.put("driverId", operationTO.getPrimaryDriverId());
        ////System.out.println("map = " + map);
        try {
            totalExpense = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getTotalExpenseDetails", map);
            ////System.out.println(" getTotalExpenseDetails Size :: :: " + totalExpense.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTotalExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTotalExpenseDetails", sqlException);
        }
        return totalExpense;
    }

    public String getDriverIncentiveAmount() {
        Map map = new HashMap();
        String tollAmount = "";
        try {
            tollAmount = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getDriverIncentive", map);
            ////System.out.println("getDriverIncentive size=" + tollAmount);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
           public String  */
            FPLogUtils.fpDebugLog("getDriverIncentive Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverIncentive List", sqlException);
        }

        return tollAmount;
    }

    public String getDriverBataAmount() {
        Map map = new HashMap();
        String tollAmount = "";
        try {
            tollAmount = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getDriverBatta", map);
            ////System.out.println("getDriverBataAmount size=" + tollAmount);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverBataAmount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverBataAmount List", sqlException);
        }

        return tollAmount;
    }

    public String getFuelPrice() {
        Map map = new HashMap();
        String fuelPrice = "";
        try {
            fuelPrice = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getFuelPrice", map);
            ////System.out.println("getFuelPrice size=" + fuelPrice);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFuelPrice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFuelPrice List", sqlException);
        }

        return fuelPrice;
    }

    public String getTollAmount() {
        Map map = new HashMap();
        String tollAmount = "";
        try {
            tollAmount = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getTollAmount", map);
            ////System.out.println("getTollAmount size=" + tollAmount);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("tollAmount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "tollAmount List", sqlException);
        }

        return tollAmount;
    }

    public String getMiscCost() {
        Map map = new HashMap();
        String tollAmount = "";
        try {
            tollAmount = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getMiscCost", map);
            ////System.out.println("getTollAmount size=" + tollAmount);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("tollAmount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "tollAmount List", sqlException);
        }

        return tollAmount;
    }

    public String getFactor() {
        Map map = new HashMap();
        String tollAmount = "";
        try {
            tollAmount = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getFactor", map);
            ////System.out.println("getTollAmount size=" + tollAmount);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("tollAmount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "tollAmount List", sqlException);
        }

        return tollAmount;
    }

    public ArrayList getTripExpenses(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList settlementDetails = new ArrayList();
        map.put("fromDate", operationTO.getFromDate());
        map.put("toDate", operationTO.getToDate());
        map.put("driverId", operationTO.getPrimaryDriverId());
        ////System.out.println("map = " + map);
        try {
            settlementDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getTripExpenses", map);
            ////System.out.println(" getSecondarySettlementDetails Size :: :: " + settlementDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondarySettlementDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondarySettlementDetails", sqlException);
        }
        return settlementDetails;
    }

    public ArrayList getViewSecondaryCustomerVehicle(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList SecondaryCustomerVehicleList = new ArrayList();
        map.put("customerId", operationTO.getCustomerId());
        ////System.out.println("map = " + map);
        try {
            ////System.out.println("this is getViewSecondaryCustomerVehicle");
            SecondaryCustomerVehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getViewSecondaryCustomer", map);
            ////System.out.println(" SecondaryCustomerVehicleList =" + SecondaryCustomerVehicleList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getViewSecondaryCustomerVehicle List", sqlException);
        }

        return SecondaryCustomerVehicleList;
    }

    public ArrayList getViewSecondaryCustomerdetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList SecondaryCustomerList = new ArrayList();
        map.put("customerId", operationTO.getCustomerId());
        ////System.out.println("map = " + map);
        try {
            ////System.out.println("this is getViewSecondaryCustomerdetails");
            SecondaryCustomerList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getViewSecondaryCustomerdetails", map);
            ////System.out.println(" getViewSecondaryCustomerdetails =" + SecondaryCustomerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getViewSecondaryCustomerdetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getViewSecondaryCustomerdetails List", sqlException);
        }

        return SecondaryCustomerList;
    }

    public ArrayList getTripAdvances(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList settlementDetails = new ArrayList();
        map.put("fromDate", operationTO.getFromDate());
        map.put("toDate", operationTO.getToDate());
        map.put("driverId", operationTO.getPrimaryDriverId());
        ////System.out.println("map = " + map);
        try {
            settlementDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getTripAdvances", map);
            ////System.out.println(" getSecondarySettlementDetails Size :: :: " + settlementDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondarySettlementDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondarySettlementDetails", sqlException);
        }
        return settlementDetails;
    }

    public ArrayList getCustomerPaymentDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList settlementDetails = new ArrayList();
        map.put("fromDate", operationTO.getFromDate());
        map.put("toDate", operationTO.getToDate());
        map.put("customerId", operationTO.getCustomerId());
        ////System.out.println("map = " + map);
        try {
            settlementDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getCustomerPaymentDetails", map);
            ////System.out.println(" getCustomerPaymentDetails Size :: :: " + settlementDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerPaymentDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerPaymentDetails", sqlException);
        }
        return settlementDetails;
    }

    public ArrayList getCustomerCollectBillDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList collectBillAmountDetails = new ArrayList();
        map.put("fromDate", operationTO.getFromDate());
        map.put("toDate", operationTO.getToDate());
        map.put("customerId", operationTO.getCustomerId());
        ////System.out.println("map = " + map);
        try {
            collectBillAmountDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getCustomerCollectBillDetails", map);
            ////System.out.println(" getCustomerCollectBillDetails Size :: :: " + collectBillAmountDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerPaymentDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerPaymentDetails", sqlException);
        }
        return collectBillAmountDetails;
    }

    public ArrayList getHumanResourceList() {
        Map map = new HashMap();
        ArrayList humanResourceList = new ArrayList();
        ////System.out.println("map = " + map);
        try {
            humanResourceList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getHumanResourceList", map);
            ////System.out.println(" getHumanResourceList Size :: :: " + humanResourceList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getHumanResourceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getHumanResourceList", sqlException);
        }
        return humanResourceList;
    }

    public int insertHumanResourceDetails(SecondaryOperationTO operationTO, int contractId) {
        Map map = new HashMap();
        int response = 0;
        try {

            map.put("contractId", contractId);
            map.put("hrId", operationTO.getHrId());
            map.put("noOfPersons", operationTO.getNoOfPersons());
            map.put("fixedAmount", operationTO.getFixedAmount());
            map.put("userId", operationTO.getUserId());
            ////System.out.println("map = " + map);
            response = (Integer) getSqlMapClientTemplate().update("secondaryOperation.insertHumanResourceDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertHumanResourceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertHumanResourceDetails", sqlException);
        }

        return response;
    }

    public ArrayList getViewHumanResourceDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList humanResourceList = new ArrayList();
        map.put("customerId", operationTO.getCustomerId());
        ////System.out.println("map = " + map);
        try {
            ////System.out.println("this is getViewHumanResourceDetails");
            humanResourceList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getViewHumanResourceDetails", map);
            ////System.out.println(" getViewHumanResourceDetails =" + humanResourceList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getViewHumanResourceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getViewHumanResourceDetails List", sqlException);
        }

        return humanResourceList;
    }

    public ArrayList getTripClosureDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList closureDetails = new ArrayList();
        map.put("fromDate", operationTO.getFromDate());
        map.put("toDate", operationTO.getToDate());
        map.put("driverId", operationTO.getPrimaryDriverId());
        ////System.out.println("map = " + map);
        try {
            ////System.out.println("this is getTripClosureDetails");
            closureDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getTripClosureDetails", map);
            ////System.out.println(" getTripClosureDetails =" + closureDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripClosureDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripClosureDetails List", sqlException);
        }

        return closureDetails;
    }

    public int insertSecondaryDriverSettlement(SecondaryOperationTO operationTO, int userId) {
        Map map = new HashMap();
        int insertSecondaryDriverSettlement = 0;
        try {
            map.put("payAmount", operationTO.getPayAmount());
            map.put("cfAmount", operationTO.getCfAmount());
            map.put("primaryDriverId", operationTO.getPrimaryDriverId());
            map.put("primaryDriverId", operationTO.getPrimaryDriverId());
            map.put("totalRcmAllocation", operationTO.getTotalRcmAllocation());
            map.put("totalBpclAllocation", operationTO.getTotalBpclAllocation());
            map.put("totalExtraExpense", operationTO.getTotalExtraExpense());
            map.put("totalMiscellaneousExpense", operationTO.getTotalMiscellaneousExpense());
            map.put("totalDriverBhatta", operationTO.getTotalDriverBhatta());
            map.put("totalExpenses", operationTO.getTotalExpenses());
            map.put("balanceAmount", operationTO.getBalanceAmount());
            map.put("startingBalance", operationTO.getStartingBalance());
            map.put("endingBalance", operationTO.getEndingBalance());
            map.put("paymentMode", operationTO.getPaymentMode());
            map.put("settlementRemarks", operationTO.getSettlementRemarks());
            map.put("userId", userId);
            ////System.out.println("map = " + map);
            insertSecondaryDriverSettlement = (Integer) getSqlMapClientTemplate().insert("secondaryOperation.insertSecondaryDriverSettlement", map);
            ////System.out.println("insertSecondaryDriverSettlement value is = " + insertSecondaryDriverSettlement);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractRouteCostDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertContractRouteCostDetails", sqlException);
        }

        return insertSecondaryDriverSettlement;
    }

    public String getStartingBalance(String driverId) {
        Map map = new HashMap();
        String startingBalance = "";
        map.put("driverId", driverId);
        ////System.out.println("map = " + map);
        try {
            startingBalance = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getStartingBalance", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConfigDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConfigDetails", sqlException);
        }

        return startingBalance;
    }

    public int insertSecondaryDriverSettlementDetails(SecondaryOperationTO operationTO, int insertDriverSettlement) {
        Map map = new HashMap();
        int insertSecondaryDriverSettlement = 0;
        try {
            map.put("insertDriverSettlement", insertDriverSettlement);
            map.put("tripId", operationTO.getTripId());
            map.put("fuelCost", operationTO.getFuelCost());
            map.put("totalKm", operationTO.getTotalKm());
            map.put("totalHours", operationTO.getTotalHours());
            map.put("fuelConsumption", operationTO.getFuelConsumption());
            if ("".equals(operationTO.getRcmExpense())) {
                map.put("rcmExpense", 0);
            } else {
                map.put("rcmExpense", operationTO.getRcmExpense());
            }
            map.put("advanceAmount", operationTO.getAdvanceAmount());
            map.put("extraExpense", operationTO.getExtraExpense());
            map.put("miscExpense", operationTO.getMiscExpense());
            map.put("driverBatta", operationTO.getDriverBatta());
            map.put("totalExpense", operationTO.getTotalExpense());
            map.put("userId", operationTO.getUserId());
            map.put("balanceAmount", operationTO.getTripBalance());
            map.put("paymentMode", operationTO.getPaymentMode());
            map.put("settlementRemarks", operationTO.getSettlementRemarks());
            ////System.out.println("map = " + map);
            insertSecondaryDriverSettlement = (Integer) getSqlMapClientTemplate().update("secondaryOperation.insertSecondaryDriverSettlementDetails", map);
            ////System.out.println("insertSecondaryDriverSettlementDetails value is = " + insertSecondaryDriverSettlement);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertSecondaryDriverSettlementDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertSecondaryDriverSettlementDetails", sqlException);
        }

        return insertSecondaryDriverSettlement;
    }

    public ArrayList getSecondaryDriverSettlementDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList settlementDetails = new ArrayList();
        map.put("fromDate", operationTO.getFromDate());
        map.put("toDate", operationTO.getToDate());
        map.put("driverId", operationTO.getPrimaryDriverId());
        map.put("settlementId", operationTO.getTripSettlementId());
        ////System.out.println("map = " + map);
        try {
            ////System.out.println("this is getSecondaryDriverSettlementDetails");
            settlementDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryDriverSettlementDetails", map);
            ////System.out.println(" getSecondaryDriverSettlementDetails =" + settlementDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryDriverSettlementDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryDriverSettlementDetails List", sqlException);
        }

        return settlementDetails;
    }

    public ArrayList getSecondaryDriverTripSettlementDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList settlementDetails = new ArrayList();
        map.put("settlementId", operationTO.getTripSettlementId());
        ////System.out.println("map = " + map);
        try {
            ////System.out.println("this is getSecondaryDriverTripSettlementDetails");
            settlementDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryDriverTripSettlementDetails", map);
            ////System.out.println(" getSecondaryDriverTripSettlementDetails =" + settlementDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryDriverSettlementDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryDriverSettlementDetails List", sqlException);
        }

        return settlementDetails;
    }

    public int updateContractMaster(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        int response = 0;
        try {
            map.put("contractId", operationTO.getContractId());
            map.put("customerId", operationTO.getCustomerId());
            map.put("fromDate", operationTO.getRouteValidFrom());
            map.put("toDate", operationTO.getRouteValidTo());
            map.put("extraKmCalculation", operationTO.getExtraKmCalculation());
            map.put("contractCngCost", operationTO.getContractCngCost());
            map.put("contractDieselCost", operationTO.getContractDieselCost());
            map.put("rateChangeOfCng", operationTO.getRateChangeOfCng());
            map.put("rateChangeOfDiesel", operationTO.getRateChangeOfDiesel());
            map.put("userId", operationTO.getUserId());
            ////System.out.println("map = " + map);
            response = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateContractMaster", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertContractMaster", sqlException);
        }

        return response;
    }

    public int updateContractDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        int response = 0;
        try {

            map.put("contractId", operationTO.getContractId());
            map.put("id", operationTO.getId());
            map.put("vehicleTypeId", operationTO.getVehTypeId());
            map.put("noOfVehicle", operationTO.getNoOfVehicle());
            map.put("fixedKm", operationTO.getFixedKm());
            map.put("fixedKmCharge", operationTO.getFixedKmCharge());
            map.put("extraKmCharge", operationTO.getExtraKmCharge());
            map.put("userId", operationTO.getUserId());
            ////System.out.println("map = " + map);
            response = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateContractDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertContractDetails", sqlException);
        }

        return response;
    }

    public int updateHumanResourceDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        int response = 0;
        try {

            map.put("pointId", operationTO.getPointId());
            map.put("contractId", operationTO.getContractId());
            map.put("hrId", operationTO.getHrId());
            map.put("noOfPersons", operationTO.getNoOfPersons());
            map.put("fixedAmount", operationTO.getFixedAmount());
            map.put("userId", operationTO.getUserId());
            ////System.out.println("map = " + map);
            response = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateHumanResourceDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertHumanResourceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertHumanResourceDetails", sqlException);
        }

        return response;
    }

    public String getCngPrice() {
        Map map = new HashMap();
        String cngPrice = "";
        try {
            cngPrice = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getCngPrice", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCngPrice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCngPrice", sqlException);
        }

        return cngPrice;
    }

    public String getCustomer(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        String customerDetails = "";
        try {
            map.put("customerId", operationTO.getCustomerId());
            customerDetails = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getCustomer", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomer Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomer", sqlException);
        }

        return customerDetails;
    }
    public String getLeasingCustomer(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        String customerDetails = "";
        try {
            map.put("customerId", operationTO.getCustomerId());
            customerDetails = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getLeasingCustomer", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomer Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomer", sqlException);
        }

        return customerDetails;
    }

    public boolean checkPointName(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        boolean checkStatus = false;
        String pointName = "";
        try {
            map.put("pointName", operationTO.getPointName());
            map.put("customerId", operationTO.getCustomerId());
            ////System.out.println("map = " + map);
            pointName = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.checkPointName", map);
            if (pointName != null) {
                checkStatus = true;
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomer Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomer", sqlException);
        }

        return checkStatus;
    }

    public boolean checkRouteName(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        boolean checkStatus = false;
        String pointName = "";
        try {
            map.put("routeName", operationTO.getRouteName());
            map.put("customerId", operationTO.getCustomerId());
            ////System.out.println("map = " + map);
            pointName = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.checkRouteName", map);
            if (pointName != null) {
                checkStatus = true;
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomer Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomer", sqlException);
        }

        return checkStatus;
    }

    public ArrayList getMileageConfigList(String customerId) {
        Map map = new HashMap();
        map.put("customerId", customerId);
        ArrayList mileageConfigList = new ArrayList();
        try {
            mileageConfigList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getMilleageConfigList", map);
            ////System.out.println("mileageConfigList " + mileageConfigList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMileageConfigList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getMileageConfigList List", sqlException);
        }

        return mileageConfigList;
    }

    public ArrayList getSecondaryCustomerList() {
        Map map = new HashMap();
        ArrayList secondaryCustomerList = new ArrayList();
        try {
            secondaryCustomerList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryCustomerList", map);
            ////System.out.println("mileageConfigList " + secondaryCustomerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryCustomerList List", sqlException);
        }

        return secondaryCustomerList;
    }

    public ArrayList getSecondaryTripNotSettledList(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList secondaryTripNotSettledList = new ArrayList();
        map.put("customerId", operationTO.getCustomerId());
        map.put("billTimePeriod", operationTO.getBilledMonth() + "-" + operationTO.getBillYear());
        map.put("vehicleTypeId", operationTO.getVehicleTypeId());
        try {
            ////System.out.println("map in the secondary controller = " + map);
            secondaryTripNotSettledList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryTripNotSettledList", map);
            ////System.out.println("secondaryTripNotSettledList " + secondaryTripNotSettledList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryTripNotSettledList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryTripNotSettledList List", sqlException);
        }

        return secondaryTripNotSettledList;
    }

    public ArrayList getSecondaryBillingList(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList secondaryBillingList = new ArrayList();
        map.put("customerId", operationTO.getCustomerId());
        map.put("billTimePeriod", operationTO.getBilledMonth() + "-" + operationTO.getBillYear());
        map.put("vehicleTypeId", operationTO.getVehicleTypeId());
        try {
            ////System.out.println("map in the secondary controller = " + map);
            secondaryBillingList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryBillingList", map);
            ////System.out.println("secondaryBillingList " + secondaryBillingList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryBillingList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryBillingList List", sqlException);
        }

        return secondaryBillingList;
    }

    public ArrayList getSecondaryBillingTripList(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList secondaryBillingTripList = new ArrayList();
        map.put("vehicleTypeId", operationTO.getVehicleTypeId());
        map.put("customerId", operationTO.getCustomerId());
        map.put("billTimePeriod", operationTO.getBilledMonth() + "-" + operationTO.getBillYear());
        try {
            ////System.out.println("map in the  trip bill list = " + map);
            secondaryBillingTripList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryBillingTripList", map);
            ////System.out.println("secondaryBillingList " + secondaryBillingTripList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryBillingTripList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryBillingTripList List", sqlException);
        }

        return secondaryBillingTripList;
    }

    public String checkTripClosure(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        String checkTripClosure = "";
        try {
            map.put("customerId", operationTO.getCustomerId());
            map.put("billTimePeriod", operationTO.getBilledMonth() + "-" + operationTO.getBillYear());
            ////System.out.println("map = " + map);
            checkTripClosure = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.checkTripClosure", map);
            ////System.out.println("checkTripClosure = " + checkTripClosure);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkTripClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkTripClosure", sqlException);
        }

        return checkTripClosure;
    }

    public String getVehicleTypeFuelDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        String vehicleTypeFuelDetails = "";
        try {
            map.put("vehicleTypeId", operationTO.getVehicleTypeId());
            map.put("customerId", operationTO.getCustomerId());
            map.put("billTimePeriod", operationTO.getBilledMonth() + "-" + operationTO.getBillYear());
            ////System.out.println("map = " + map);
            vehicleTypeFuelDetails = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getVehicleTypeFuelDetails", map);
            ////System.out.println("checkTripClosure = " + vehicleTypeFuelDetails);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleTypeFuelDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleTypeFuelDetails", sqlException);
        }

        return vehicleTypeFuelDetails;
    }

    public String getVehicleTypeCngDeviation(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        String vehicleTypeCngDeviation = "";
        try {
            map.put("vehicleTypeId", operationTO.getVehicleTypeId());
            map.put("fuelTypeId", operationTO.getFuelTypeId());
            map.put("fuelPrice", operationTO.getFuelPrice());
            map.put("customerId", operationTO.getCustomerId());
            map.put("billTimePeriod", operationTO.getBilledMonth() + "-" + operationTO.getBillYear());
            ////System.out.println("map = " + map);
            if (operationTO.getFuelTypeId().equals("1002")) {
                vehicleTypeCngDeviation = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getVehicleTypeDieselDeviation", map);
            } else if (operationTO.getFuelTypeId().equals("1003")) {
                vehicleTypeCngDeviation = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getVehicleTypeCngDeviation", map);
            }
            ////System.out.println("vehicleTypeCngDeviation = " + vehicleTypeCngDeviation);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleTypeCngDeviation Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleTypeCngDeviation", sqlException);
        }

        return vehicleTypeCngDeviation;
    }

    public String getContractFuelAmount(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        String contractFuelAmount = "";
        try {
            map.put("vehicleTypeId", operationTO.getVehicleTypeId());
            map.put("fuelTypeId", operationTO.getFuelTypeId());
            map.put("fuelPrice", operationTO.getFuelPrice());
            map.put("customerId", operationTO.getCustomerId());
            map.put("billTimePeriod", operationTO.getBilledMonth() + "-" + operationTO.getBillYear());
            ////System.out.println("map = " + map);
            if (operationTO.getFuelTypeId().equals("1002")) {
                contractFuelAmount = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getContractDieselAmount", map);
            } else if (operationTO.getFuelTypeId().equals("1003")) {
                contractFuelAmount = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getContractCngAmount", map);
            }
            ////System.out.println("contractFuelAmount = " + contractFuelAmount);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractFuelAmount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getContractFuelAmount", sqlException);
        }

        return contractFuelAmount;
    }

    public ArrayList getSecondaryDriverName(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList driverName = new ArrayList();
        map.put("driverName", operationTO.getDriverName() + "%");
        try {
            if (getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryDriverName", map) != null) {
                driverName = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryDriverName", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryDriverName Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryDriverName", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryDriverName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryDriverName", sqlException);
        }
        return driverName;
    }

    public ArrayList getPrimaryDriverName(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList driverName = new ArrayList();
        map.put("driverName", operationTO.getDriverName() + "%");
        try {
            if (getSqlMapClientTemplate().queryForList("secondaryOperation.getPrimaryDriverName", map) != null) {
                driverName = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getPrimaryDriverName", map);
            }
        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPrimaryDriverName Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPrimaryDriverName", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPrimaryDriverName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPrimaryDriverName", sqlException);
        }
        return driverName;
    }

    public ArrayList getSecondaryFleet(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList fleetName = new ArrayList();

        try {

            fleetName = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryFleet", map);

        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryDriverName Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryDriverName", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryDriverName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryDriverName", sqlException);
        }
        return fleetName;
    }

    public ArrayList getSecondaryDriverNameForSettelment(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        map.put("companyName", operationTO.getCompanyName());
        ArrayList driverName = new ArrayList();

        ////System.out.println("map for driver" + map);

        try {

            driverName = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSecondaryDriverNameforSettelment", map);

        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryDriverName Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryDriverName", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryDriverName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryDriverName", sqlException);
        }
        return driverName;
    }

    public String getSecondaryRouteApprovalPerson(String customerId) {
        Map map = new HashMap();
        map.put("customerId", customerId);

        ////System.out.println("map = " + map);
        try {
            customerId = (String) getSqlMapClientTemplate().queryForObject("operation.getSecondaryRouteApprovalPerson", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryApprovalPerson Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getSecondaryApprovalPerson", sqlException);
        }
        return customerId;

    }

    public String getSecRouteApprovalBy(String routeName) {
        Map map = new HashMap();
        map.put("routeName", routeName);

        // ArrayList driverName = new ArrayList();
        String status = "";
        ////System.out.println("map for RouteApproval:" + map);

        try {

            status = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getSecRouteApprovalBy", map);

        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryDriverName Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryDriverName", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryDriverName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryDriverName", sqlException);
        }
        return status;
    }

    public int UpdateSecondaryRouteApproval(String routeName, String mailId) {
        Map map = new HashMap();
        map.put("routeName", routeName);
        map.put("mailId", mailId);
        // ArrayList driverName = new ArrayList();
        int status = 0;
        ////System.out.println("map for RouteApproval:" + map);

        try {

            status = (Integer) getSqlMapClientTemplate().update("secondaryOperation.UpdateSecondaryRouteApproval", map);

        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryDriverName Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryDriverName", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryDriverName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryDriverName", sqlException);
        }
        return status;
    }

    public int UpdateSecondaryRouteRejection(String routeName, String mailId) {
        Map map = new HashMap();
        map.put("routeName", routeName);
        map.put("mailId", mailId);
        // ArrayList driverName = new ArrayList();
        int status = 0;
        ////System.out.println("map for Route Rejection:" + map);

        try {

            status = (Integer) getSqlMapClientTemplate().update("secondaryOperation.UpdateSecondaryRouteRejection", map);

        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryDriverName Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryDriverName", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryDriverName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryDriverName", sqlException);
        }
        return status;
    }

    public ArrayList getSlabTypeList() {
        Map map = new HashMap();
        ArrayList slabTypeList = new ArrayList();
        try {
            slabTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSlabTypeList", map);
            ////System.out.println("slabTypeListSize    " + slabTypeList.size());
        } catch (NullPointerException ne) {
            ne.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("slabTypeList Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "slabTypeList", ne);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("slabTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "slabTypeList", sqlException);
        }
        return slabTypeList;
    }

    public ArrayList getvehicleSlabrateList(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        map.put("vehicleTypeId", operationTO.getVehicleTypeId());
        map.put("routeId", operationTO.getRouteId());
        ArrayList vehicleSlabrateList = new ArrayList();

        ////System.out.println("map for driver" + map);

        try {

            vehicleSlabrateList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSlabRateDetailsList", map);

        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getvehicleSlabrateList Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getvehicleSlabrateList", ne);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getvehicleSlabrateList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getvehicleSlabrateList", sqlException);
        }
        return vehicleSlabrateList;
    }

    public int saveSlapRate(SecondaryOperationTO operationTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("userId", userId);
            map.put("slabRateId", operationTO.getSlabRateId());
            map.put("slabRate", operationTO.getSlabRate());
            map.put("status", operationTO.getStatus());
            map.put("routeCostId", operationTO.getRouteCostId());
            map.put("slabId", operationTO.getSlabId());
            map.put("vehicleTypeId", operationTO.getVehicleTypeId());
            map.put("routeId", operationTO.getRouteId());
            map.put("customerId", operationTO.getCustomerId());
            ////System.out.println("map = " + map);
            if (operationTO.getSlabRateId() != null && !"".equals(operationTO.getSlabRateId())) {
                status = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateSlapRate", map);
                ////System.out.println("updatestatus saveSlapRate = " + status);
            } else {
                status = (Integer) getSqlMapClientTemplate().update("secondaryOperation.saveSlapRate", map);
                ////System.out.println("insertstatus saveSlapRate = " + status);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkTripClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkTripClosure", sqlException);
        }

        return status;
    }

    public ArrayList getVehicleTransitPeriodList(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        map.put("tripSheetId", operationTO.getTripSheetId());
        ArrayList vehicleTransitPeriodList = new ArrayList();

        ////System.out.println("map for getVehicleTransitPeriodList" + map);

        try {

            vehicleTransitPeriodList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getVehicleTransitPeriodList", map);

        } catch (NullPointerException ne) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleTransitPeriodList Error" + ne.toString());
            FPLogUtils.fpErrorLog("sqlException" + ne);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleTransitPeriodList", ne);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleTransitPeriodList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleTransitPeriodList", sqlException);
        }
        return vehicleTransitPeriodList;
    }

    public int saveVehicleTransitPeriod(SecondaryOperationTO operationTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("userId", userId);           
            map.put("status", operationTO.getStatus());
            map.put("vehicleId", operationTO.getVehicleId());
            map.put("tripSheetId", operationTO.getTripSheetId());
            map.put("tripType", operationTO.getTripType());
            map.put("vehicleNo", operationTO.getVehicleNo());
            map.put("tripCode", operationTO.getTripCode());
            map.put("freezeInDate", operationTO.getFreezeInDate());
            map.put("freezeInTime", operationTO.getFreezeInTime());
            map.put("freezeOutDate", operationTO.getFreezeOutDate());
            map.put("freezeOutTime", operationTO.getFreezeOutTime());
            map.put("incidentId", operationTO.getIncidentId());
            map.put("currentLocation", operationTO.getCurrentLocation());
            map.put("remarks", operationTO.getRemarks());
            ////System.out.println("map saveVehicleTransitPeriod = " + map);
            status = (Integer) getSqlMapClientTemplate().update("secondaryOperation.saveVehicleTransitPeriod", map);
            ////System.out.println("insertstatus saveVehicleTransitPeriod = " + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveVehicleTransitPeriod Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveVehicleTransitPeriod", sqlException);
        }
        return status;
    }
    
     public ArrayList getIncident(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList getIncident = new ArrayList();
        try {

            getIncident = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getIncident", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getIncident Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getIncident", sqlException);
        }
        return getIncident;

    }
    //Toll gate Master Start
   
      public ArrayList tollGateMasterList(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList tollGateMasterList = new ArrayList();
        map.put("tollId", operationTO.getTollId());
        System.out.println("map:::" + map);
        try {
            tollGateMasterList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.tollGateMasterList", map);
            System.out.println("tollGateMasterList " + tollGateMasterList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTollgatDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTollgatDetailsList List", sqlException);
        }

        return tollGateMasterList;
    }
      public ArrayList getTollgatDetailsList(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList getTollgatDetailsList = new ArrayList();
        map.put("tollId", operationTO.getTollId());
        //////System.out.println("map:::" + map);
        try {
            getTollgatDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getTollgatDetailsList", map);
            //////System.out.println("routeDetailsList " + routeDetailsList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTollgatDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTollgatDetailsList List", sqlException);
        }

        return getTollgatDetailsList;
    }
      
      public ArrayList getCityTollList(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList zoneList = new ArrayList();
        map.put("cityName", operationTO.getCityName() + "%");
        System.out.println("getZoneList=="+map);
        try {
            zoneList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getCityTollList", map);
            //////System.out.println(" getZoneList =" + zoneList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityTollList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCityTollList List", sqlException);
        }

        return zoneList;
    }
    
     public String cityNameList(String cityName) {
        Map map = new HashMap();
        String CityName = "";
        try {
            map.put("cityName", cityName);
            System.out.println("cityName=="+map);
            CityName = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.cityNameList", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkVehicleTyre Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleDetails", sqlException);
        }
        return CityName;
    }
     
     public int insertTollgetDetails(SecondaryOperationTO operationTO, int userId, int madhavaramSp) {
        Map map = new HashMap();
        int tollId = 0;
        try {
//            map.put("tollId", operationTO.getTollId());
            map.put("tollName", operationTO.getTollName());
            map.put("cityId", operationTO.getCityId());
            map.put("zoneId", operationTO.getZoneId());
            map.put("countryId", operationTO.getCountryId());
            map.put("googleCityName", operationTO.getGoogleCityName());
            map.put("latitude", operationTO.getLatitude());
            map.put("longitude", operationTO.getLongitude());
            map.put("fromDate", operationTO.getFromDate());
            map.put("toDate", operationTO.getToDate());
            map.put("currencyCode", operationTO.getCurrencyCode());
            map.put("userId", userId);
            map.put("companyId", madhavaramSp);
            System.out.println("map 1 = " + map);
            if (!"0".equals(operationTO.getTollId())) {
                System.out.println("ifffffff");
                map.put("tollId", operationTO.getTollId());
                System.out.println("map Second:::::: = " + map);
                tollId = (Integer) getSqlMapClientTemplate().update("secondaryOperation.upadeteTollgetDetails", map);
                System.out.println("tollId11111 = " + tollId);
            } else {
                 System.out.println("elssssseeeeeeeeee");
                tollId = (Integer) getSqlMapClientTemplate().insert("secondaryOperation.insertTollgetDetails", map);
                System.out.println("tollId"+tollId);
                
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("updateVehicleTyreDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "updateVehicleTyreDetails", sqlException);
        }
        return tollId;
    }
     public int insertTollgetRateDetails(SecondaryOperationTO operationTO, int userId, String axleName,String vehTypeId,String monthlyPass,String singleJourney,String returnJourney,String localVehicleCost,String tollRateId,int lastInsertedId) {
        Map map = new HashMap();
        int tollRateId1 = 0;
        int tollId = 0;
        int updateinsuranceId = 0;
        try {
            map.put("tollId", lastInsertedId);
            map.put("axleName", axleName);
            map.put("vehTypeId", vehTypeId);
            map.put("monthlyPass", monthlyPass);
            map.put("singleCost", singleJourney);
            map.put("returnCost", returnJourney);
            map.put("localVehicleCost", localVehicleCost);
            map.put("userId", userId);
            System.out.println("map insertTollgetRateDetails 2 = " + map);
            if (!"0".equals(tollRateId)) {
                map.put("tollRateId", tollRateId);
                System.out.println("map Second:::::: = " + map);
                tollRateId1 = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateTollgetRateDetails", map);
                System.out.println("tollrateId11111 = " + tollId);
            } else {
                tollRateId1 = (Integer) getSqlMapClientTemplate().insert("secondaryOperation.insertTollgetRateDetails", map);
                System.out.println("tollRateId"+tollRateId);
                
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("insertTollgetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-02", CLASS,
                    "insertTollgetDetails", sqlException);
        }
        return tollRateId1;
    }

     public String getTransportCustomer(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        String customerDetails = "";
        try {
            map.put("customerId", operationTO.getCustomerId());
            System.out.println("map:"+map);
            customerDetails = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getTranportCustomer", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomer Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomer", sqlException);
        }

        return customerDetails;
    }

         public int processInsertTransportCustomerContract(SecondaryOperationTO operationTO, int UserId) {
        Map map = new HashMap();
        int contractId = 0;

        try {
            map.put("userId", UserId);

          //  if (!"".equals(operationTO.getCustomerId()) && !"".equals(operationTO.getStartDate()) && !"".equals(operationTO.getEndDate()) && !"".equals(operationTO.getBillingTypeId()) && !"".equals(operationTO.getBillingKmCalculationId())) {

                map.put("customerId", operationTO.getCustomerId());
                map.put("startDate", operationTO.getStartDate());
                map.put("endDate", operationTO.getEndDate());
                map.put("billingType", operationTO.getBillingTypeId());
                map.put("billingKmCalculationId", operationTO.getBillingKmCalculationId());
                map.put("maxAllowedKm", operationTO.getMaxAllowedKm());
                map.put("extraRunKmCost", operationTO.getExtraRunKmCost());
                map.put("driverResponsibility", operationTO.getDriverResponsibility());
                map.put("fuelResponsibilty", operationTO.getFuelResponsibility());
                System.out.println("Inser tTransport Customer Contract map ===================== " + map);
                contractId = (Integer) getSqlMapClientTemplate().insert("secondaryOperation.InsertTransportCustomerContract", map);
                System.out.println("status1fgtfgdf = " + contractId);
         //   }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVendorMfrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return contractId;

    }

         public int processInsertTransportCustomerDailyContract(SecondaryOperationTO operationTO, int UserId) {
        Map map = new HashMap();
        int contractId = 0;

        try {
            map.put("userId", UserId);

          //  if (!"".equals(operationTO.getCustomerId()) && !"".equals(operationTO.getStartDate()) && !"".equals(operationTO.getEndDate()) && !"".equals(operationTO.getBillingTypeId()) && !"".equals(operationTO.getBillingKmCalculationId())) {

                map.put("customerId", operationTO.getCustomerId());
                map.put("startDate", operationTO.getStartDate());
                map.put("endDate", operationTO.getEndDate());
                
                System.out.println("Inser tTransport Customer Contract map ===================== " + map);
                contractId = (Integer) getSqlMapClientTemplate().insert("secondaryOperation.InsertTransportCustomerDailyContract", map);
                System.out.println("status1fgtfgdf = " + contractId);
         //   }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVendorMfrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return contractId;

    }
public int processUpdateCustomerId(SecondaryOperationTO operationTO, int UserId) {
        Map map = new HashMap();
        int update = 0;

        try {
            map.put("userId", UserId);

          //  if (!"".equals(operationTO.getCustomerId()) && !"".equals(operationTO.getStartDate()) && !"".equals(operationTO.getEndDate()) && !"".equals(operationTO.getBillingTypeId()) && !"".equals(operationTO.getBillingKmCalculationId())) {

                map.put("customerId", operationTO.getCustomerId());
                map.put("startDate", operationTO.getStartDate());
                map.put("endDate", operationTO.getEndDate());
                map.put("quotationId", operationTO.getQuotationId());

                System.out.println("Update quotation cust id Contract map ===================== " + map);
                update = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateQuotationCustomerId", map);
                System.out.println("status1fgtfgdf = " + update);
         //   }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVendorMfrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return update;

    }
         public int processUpdateTransportCustomerContract(SecondaryOperationTO operationTO, int UserId) {
        Map map = new HashMap();
        int contractId = 0;

        try {
            map.put("userId", UserId);

          //  if (!"".equals(operationTO.getCustomerId()) && !"".equals(operationTO.getStartDate()) && !"".equals(operationTO.getEndDate()) && !"".equals(operationTO.getBillingTypeId()) && !"".equals(operationTO.getBillingKmCalculationId())) {

                map.put("customerId", operationTO.getCustomerId());
                map.put("startDate", operationTO.getStartDate());
                map.put("endDate", operationTO.getEndDate());
                map.put("billingType", operationTO.getBillingTypeId());
                map.put("billingKmCalculationId", operationTO.getBillingKmCalculationId());
                map.put("maxAllowedKm", operationTO.getMaxAllowedKm());
                map.put("extraRunKmCost", operationTO.getExtraRunKmCost());
                map.put("driverResponsibility", operationTO.getDriverResponsibility());
                map.put("fuelResponsibilty", operationTO.getFuelResponsibility());
                map.put("contractId", operationTO.getContractId());
                System.out.println("Update tTransport Customer Contract map ===================== " + map);
                contractId = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateTransportCustomerContract", map);
                System.out.println("status1fgtfgdf = " + contractId);
         //   }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVendorMfrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return contractId;

    }
         public int processUpdateQuotation(SecondaryOperationTO operationTO, int UserId) {
        Map map = new HashMap();
        int contractId = 0;

        try {
            map.put("userId", UserId);

          //  if (!"".equals(operationTO.getCustomerId()) && !"".equals(operationTO.getStartDate()) && !"".equals(operationTO.getEndDate()) && !"".equals(operationTO.getBillingTypeId()) && !"".equals(operationTO.getBillingKmCalculationId())) {

                map.put("quotationId", operationTO.getQuotationId());
                map.put("quotationStatus", operationTO.getQuotationStatus());
                map.put("customerName", operationTO.getCustomerName());
                map.put("customerAddress", operationTO.getCustomerAddress());
                map.put("salesManager", operationTO.getSalesManager());
                map.put("email", operationTO.getEmail());
                map.put("quotationType", operationTO.getQuotationType());
                map.put("validity", operationTO.getValidity());
                
                System.out.println("Update tTransport Customer Contract map ===================== " + map);
                contractId = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateQuotation", map);
                System.out.println("status1fgtfgdf = " + contractId);
         //   }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVendorMfrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return contractId;

    }
         public int processUpdateTransportCustomerDailyContract(SecondaryOperationTO operationTO, int UserId) {
        Map map = new HashMap();
        int contractId = 0;

        try {
            map.put("userId", UserId);

          //  if (!"".equals(operationTO.getCustomerId()) && !"".equals(operationTO.getStartDate()) && !"".equals(operationTO.getEndDate()) && !"".equals(operationTO.getBillingTypeId()) && !"".equals(operationTO.getBillingKmCalculationId())) {

                map.put("customerId", operationTO.getCustomerId());
                map.put("startDate", operationTO.getStartDate());
                map.put("endDate", operationTO.getEndDate());
                
                map.put("contractId", operationTO.getContractId());
                System.out.println("Update tTransport Customer Contract map ===================== " + map);
                contractId = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateTransportCustomerDailyContract", map);
                System.out.println("status1fgtfgdf = " + contractId);
         //   }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVendorMfrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return contractId;

    }
         public int processInsertTransportCustomerVehicleContract(SecondaryOperationTO operationTO, int userId,int contractId, String vehicleTypeIds, String vehicleUnit,String contractTypes,String fixedCostPerVehicles,String totalFixedCosts,String extraVehicleRunKm) {
        Map map = new HashMap();


        try {
            map.put("userId", userId);

            if ( !"".equals(vehicleTypeIds) && !"".equals(vehicleUnit) ) {

                map.put("contractId", contractId);
                map.put("vehicleTypeId", vehicleTypeIds);
                map.put("vehicleUnit", vehicleUnit);
                map.put("contractType",contractTypes);
                map.put("fixedCostPerVehicle", fixedCostPerVehicles);
                map.put("totalFixedCost", totalFixedCosts);
                map.put("extraVehicleRunKm", extraVehicleRunKm);
                
                System.out.println("Inser tTransport Customer vehicle Contract map ===================== " + map);
                contractId = (Integer) getSqlMapClientTemplate().insert("secondaryOperation.InsertTransportCustomerVehicleContract", map);
                System.out.println("status1fgtfgdf = " + contractId);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVendorMfrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return contractId;

    }
         public int processInsertTransportCustomerDailyVehicleContract(SecondaryOperationTO operationTO, int userId,int contractId, String vehicleTypeIds, String trailerTypeId,String fixedCostPerVehicles) {
        Map map = new HashMap();


        try {
            map.put("userId", userId);

            if ( !"".equals(vehicleTypeIds)  ) {

                map.put("contractId", contractId);
                map.put("vehicleTypeId", vehicleTypeIds);
                map.put("trailerTypeId", trailerTypeId);
                map.put("fixedCostPerVehicle", fixedCostPerVehicles);
               

                System.out.println("Inser Transport Customer Daily vehicle Contract map ===================== " + map);
                contractId = (Integer) getSqlMapClientTemplate().insert("secondaryOperation.InsertTransportCustomerDailyVehicleContract", map);
                System.out.println("status Inser Transport = " + contractId);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVendorMfrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return contractId;

    }
         public int processInsertTransportCustomerTrailerContract(SecondaryOperationTO operationTO, int userId,int contractId, String trailerTypeIds, String trailerUnit,String contractTypes,String fixedCostPerTrailers,String totalFixedCosts) {
        Map map = new HashMap();


        try {
            map.put("userId", userId);

            if ( !"".equals(trailerTypeIds) && !"".equals(trailerUnit) ) {

                map.put("contractId", contractId);
                map.put("trailerTypeId", trailerTypeIds);
                map.put("trailerUnit", trailerUnit);
                map.put("contractType",contractTypes);
                map.put("fixedCostPerTrailer", fixedCostPerTrailers);
                map.put("totalFixedCost", totalFixedCosts);
              
                System.out.println("Inser tTransport Customer trailer Contract map ===================== " + map);
                contractId = (Integer) getSqlMapClientTemplate().insert("secondaryOperation.InsertTransportCustomerTrailerContract", map);
                System.out.println("status1fgtfgdf = " + contractId);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVendorMfrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return contractId;

    }
         public int processInsertTransportCustomerDriverCompensation(SecondaryOperationTO operationTO, int userId,int contractId, String TypeIds, String amount) {
        Map map = new HashMap();


        try {
            map.put("userId", userId);

            if ( !"".equals(TypeIds) && !"".equals(amount) ) {

                map.put("contractId", contractId);
                map.put("typeId", TypeIds);
                map.put("amount", amount);
               
                System.out.println("Inser tTransport Customer driver compesation map ===================== " + map);
                contractId = (Integer) getSqlMapClientTemplate().insert("secondaryOperation.InsertTransportCustomerDriverCompensation", map);
                System.out.println("status1fgtfgdf = " + contractId);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVendorMfrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return contractId;

    }

         public ArrayList getTransportCustomerContractDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList contractDetails = new ArrayList();
        map.put("customerId", operationTO.getCustomerId());
        System.out.println("map..=="+map);
        try {
            contractDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getTransportCustomerContractDetails", map);
          System.out.println(" contractDetails =" + contractDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityTollList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTransportCustomerContractDetails List", sqlException);
        }

        return contractDetails;
    }
         public ArrayList getTransportCustomerDailyContractDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList contractDetails = new ArrayList();
        map.put("customerId", operationTO.getCustomerId());
        System.out.println("map..=="+map);
        try {
            contractDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getTransportCustomerDailyContractDetails", map);
          System.out.println(" contractDetails =" + contractDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityTollList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTransportCustomerContractDetails List", sqlException);
        }

        return contractDetails;
    }
         public ArrayList getTransportCustomerVehicleDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList contractVehicleDetails = new ArrayList();
        map.put("customerId", operationTO.getCustomerId());
        System.out.println("map..=="+map);
        try {
            contractVehicleDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getTransportCustomerVehicleDetails", map);
          System.out.println(" contractVehicleDetails =" + contractVehicleDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityTollList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTransportCustomerVehicleDetails List", sqlException);
        }

        return contractVehicleDetails;
    }
         public ArrayList getTransportCustomerDailyFlatRate(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList contractVehicleDetails = new ArrayList();
        map.put("customerId", operationTO.getCustomerId());
        System.out.println("map..=="+map);
        try {
            contractVehicleDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getTransportCustomerDailyFlatRate", map);
          System.out.println(" contractVehicleDetails =" + contractVehicleDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityTollList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTransportCustomerVehicleDetails List", sqlException);
        }

        return contractVehicleDetails;
    }
         public ArrayList getTransportCustomerTrailerDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList contractTrailerDetails = new ArrayList();
        map.put("customerId", operationTO.getCustomerId());
        System.out.println("map..=="+map);
        try {
            contractTrailerDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getTransportCustomerTrailerDetails", map);
          System.out.println(" contractTrailerDetails =" + contractTrailerDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityTollList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTransportCustomerTrailerDetails List", sqlException);
        }

        return contractTrailerDetails;
    }
         public ArrayList getTransportCustomerDriverCompensationDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList driverCompensationDetails = new ArrayList();
        map.put("customerId", operationTO.getCustomerId());
        System.out.println("map..=="+map);
        try {
            driverCompensationDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getTransportCustomerDriverCompensationDetails", map);
          System.out.println(" getTransportCustomerDriverCompensationDetails =" + driverCompensationDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityTollList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTransportCustomerDriverCompensationDetails List", sqlException);
        }

        return driverCompensationDetails;
    }

       public int updateTransportCustomerVehicleContract(String[] vehicleContractId,String[] fixedCostPerVehicleE, String[] totalFixedCostE,int userId) {
        Map map = new HashMap();

      int status=0;
        try {
            map.put("userId", userId);
              System.out.println("vehicleContractId.length:"+vehicleContractId.length);
              if (vehicleContractId.length > 0) {
                for(int i=0;i<vehicleContractId.length;i++){

                map.put("vehicleContractId", vehicleContractId[i]);
                map.put("fixedCostPerVehicle", fixedCostPerVehicleE[i]);
                map.put("totalFixedCost", totalFixedCostE[i]);
                System.out.println("update Transport Customer vehicle contract map ===================== " + map);
                status = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateTransportCustomerVehicleContract", map);
                System.out.println("status1fgtfgdf = " + status);
                }


            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVendorMfrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return status;

    }

public int updateTransportCustomerDailyVehicleContract(String[] vehicleContractId,String[] fixedCostPerVehicleE,int userId) {
        Map map = new HashMap();

      int status=0;
        try {
            map.put("userId", userId);
              System.out.println("vehicleContractId.length:"+vehicleContractId.length);
              if (vehicleContractId.length > 0) {
                for(int i=0;i<vehicleContractId.length;i++){

                map.put("vehicleContractId", vehicleContractId[i]);
                map.put("fixedCostPerVehicle", fixedCostPerVehicleE[i]);
                
                System.out.println("update Transport Customer Daily vehicle contract map ===================== " + map);
                status = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateTransportCustomerDailyVehicleContract", map);
                System.out.println("status1fgtfgdf = " + status);
                }


            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVendorMfrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return status;

    }
          public int updateTransportCustomerTrailerContract(String[] trailerContractId,String[] fixedCostPerTrailerE, String[] totalTrailerFixedCostE,int userId) {
        Map map = new HashMap();

      int status=0;
        try {
            map.put("userId", userId);

              if (trailerContractId.length > 0) {
                for(int i=0;i<trailerContractId.length;i++){

                map.put("trailerContractId", trailerContractId[i]);
                map.put("fixedCostPerTrailer", fixedCostPerTrailerE[i]);
                map.put("totalFixedCost", totalTrailerFixedCostE[i]);
                System.out.println("update Transport Customer Trailer contract map ===================== " + map);
                 status = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateTransportCustomerTrailerContract", map);
                System.out.println("status1 = " + status);
                }


            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVendorMfrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return status;

    }

        public int processInsertVehicleConf(SecondaryOperationTO operationTO, int userId) {
            Map map = new HashMap();
           int status=0;
    
            try {
                map.put("userId", userId);
                map.put("vehicleTypeId", operationTO.getVehicleTypeId());
                map.put("transCustomerId", operationTO.getCustId());
    
                 String temp[] = operationTO.getVehicleRegNo().split(",");
                 String[] vehicleRegNo = operationTO.getVehicleRegNo().split(",");
                String[] agreedDate = operationTO.getAgreedDate().split(",");
                String[] mfr = operationTO.getMfr().split(",");
                String[] model = operationTO.getModel().split(",");
                String[] remarks = operationTO.getRemarks().split(",");
                String[] vehicleId = operationTO.getVehicleId().split(",");
                String[] mfrId = operationTO.getMfrId().split(",");
                String[] modelId = operationTO.getModelId().split(",");
                String[] id = operationTO.getId().split(",");
                String[] activeInd = operationTO.getActiveInd().split(",");
                String[] startReading = operationTO.getStartReading().split(",");
    
                 System.out.println("temp.length = " + temp.length);
    
                    map.put("contractId", operationTO.getContractId());
                    if (operationTO.getVehicleRegNo() != null && !"".equals(operationTO.getVehicleRegNo())) {
                     if (temp.length > 0) {
                         for (int i = 0; i < temp.length; i++) {
    
                    map.put("model", model[i]);
                    map.put("mfr", mfr[i]);
                    map.put("agreedDate", agreedDate[i]);
                    map.put("activeInd",activeInd[i]);
                    map.put("vehicleNo", vehicleRegNo[i]);
                    map.put("remarks", remarks[i]);
                    map.put("vehicleId", vehicleId[i]);
                    map.put("mfrId", mfrId[i]);
                    map.put("modelId", modelId[i]);
                    map.put("startReading", startReading[i]);
                    map.put("id", id[i]);
                     if(!"".equals(id[i]) && id[i]!=null && !"0".equals(id[i])){
                        System.out.println("update the vehicles");
                           System.out.println("update trailer map ===================== " + map);
                         status = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateVehicleConf", map);
                         status = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateVehicleTransportCustomerId", map);
                          System.out.println("status  of vehicle update = " + status);
                    }else{
                    System.out.println("Inser vehicle map ===================== " + map);
                    status = (Integer) getSqlMapClientTemplate().insert("secondaryOperation.InsertVehicleConf", map);
                    status = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateVehicleTransportCustomerId", map);
                    System.out.println("status  of vehicle = " + status);
                    }
                         }
    
                     }
                    }
    
        
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("insertVendorMfrDetails Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-SYS-02", CLASS,
                        "insertMfr", sqlException);
            }
            return status;
    
        }
            public int processInsertTrailerConf(SecondaryOperationTO operationTO, int userId) {
            Map map = new HashMap();
           int status=0;
    
            try {
                map.put("userId", userId);
                map.put("transCustomerId", operationTO.getCustId());
                map.put("trailerTypeId", operationTO.getTrailerTypeId());
    
                 String temp[] = operationTO.getTrailerNo().split(",");
                 String[] trailerNo = operationTO.getTrailerNo().split(",");
                String[] agreedDate = operationTO.getAgreedDate().split(",");
    
                String[] remarks = operationTO.getTrailerRemarks().split(",");
                String[] activeInd = operationTO.getActiveInd().split(",");
                String[] trailerId = operationTO.getTrailerId().split(",");
                String[] id = operationTO.getId().split(",");
                
                 System.out.println("temp.length = " + temp.length);
    
                    map.put("contractId", operationTO.getContractId());
                    if (operationTO.getTrailerNo() != null && !"".equals(operationTO.getTrailerNo())) {
                     if (temp.length > 0) {
                         for (int i = 0; i < temp.length; i++) {
    
    
                    map.put("agreedDate", agreedDate[i]);
                    map.put("activeInd", activeInd[i]);
                    map.put("trailerNo", trailerNo[i]);
                    map.put("remarks", remarks[i]);
                    map.put("trailerId", trailerId[i]);
                    map.put("id", id[i]);
                    if(!"".equals(id[i]) && id[i]!=null && !"0".equals(id[i])){
                        System.out.println("update the vehicles");
                           System.out.println("update trailer map ===================== " + map);
                         status = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateTrailerConf", map);
                         status = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateTrailerTransportCustomerId", map);
                    }else{
    
                    System.out.println("Inser trailer map ===================== " + map);
                    status = (Integer) getSqlMapClientTemplate().insert("secondaryOperation.InsertTrailerConf", map);
                    status = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateTrailerTransportCustomerId", map);
                    System.out.println("status  of trailer = " + status);
                    }
                         }
    
                     }
                    }
    
    
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("insertVendorMfrDetails Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-SYS-02", CLASS,
                        "insertMfr", sqlException);
            }
            return status;
    
        }
    
             public ArrayList getVehicleRegNoForTransportCustomer(SecondaryOperationTO operationTO) {
            Map map = new HashMap();
            ArrayList regNo = new ArrayList();
            map.put("vehicleTypeId", operationTO.getVehicleTypeId());
            map.put("vehicleNo", operationTO.getVehicleNo()+ "%");
            System.out.println("map..=="+map);
            try {
                regNo = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getVehicleRegNoForTransportCustomer", map);
              System.out.println(" regNoDetails =" + regNo.size());
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getCityTollList Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-MRS-01", CLASS, "regNoDetailsDetails List", sqlException);
            }
    
            return regNo;
        }
    
             public ArrayList getTrailerNoForTransportCustomer(SecondaryOperationTO operationTO) {
            Map map = new HashMap();
            ArrayList regNo = new ArrayList();
            map.put("trailerTypeId", operationTO.getTrailerTypeId());
            map.put("trailerNo", operationTO.getTrailerNo()+ "%");
            System.out.println("map..=="+map);
            try {
                regNo = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getTrailerNoForTransportCustomer", map);
              System.out.println(" regNoDetails =" + regNo.size());
            } catch (Exception sqlException) {
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getCityTollList Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-MRS-01", CLASS, "regNoDetailsDetails List", sqlException);
            }
    
            return regNo;
        }
             public ArrayList getVehicleDetails(SecondaryOperationTO operationTO) {
            Map map = new HashMap();
            ArrayList regNo = new ArrayList();
            map.put("contractId", operationTO.getContractId());
            map.put("customerId", operationTO.getCustId());
            map.put("vehicleTypeId", operationTO.getVehicleTypeId());
            System.out.println("map..=="+map);
            try {
                regNo = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getVehicleDetails", map);
              System.out.println(" regNoDetails =" + regNo.size());
            } catch (Exception sqlException) {
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getCityTollList Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-MRS-01", CLASS, "regNoDetailsDetails List", sqlException);
            }
    
            return regNo;
        }
             public ArrayList getTrailerDetails(SecondaryOperationTO operationTO) {
            Map map = new HashMap();
            ArrayList regNo = new ArrayList();
            map.put("contractId", operationTO.getContractId());
            map.put("customerId", operationTO.getCustId());
            map.put("trailerTypeId", operationTO.getTrailerTypeId());
            System.out.println("map..=="+map);
            try {
                regNo = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getTrailerDetails", map);
              System.out.println(" trailerNoDetails =" + regNo.size());
            } catch (Exception sqlException) {
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getCityTollList Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-MRS-01", CLASS, "regNoDetailsDetails List", sqlException);
            }
    
            return regNo;
    }
             
    public ArrayList getCustomerContractVehicleList(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList customerContractVehielcList = new ArrayList();
        map.put("customerId", operationTO.getCustomerId());
        ////System.out.println("map = " + map);
        try {

            customerContractVehielcList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getCustomerContractVehicleList", map);
            System.out.println("customerContractVehielcList.size() = " + customerContractVehielcList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerContractVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getCustomerContractVehicleList", sqlException);
        }
        return customerContractVehielcList;

    }

    public int saveVehicleTripSchedule(SecondaryOperationTO operationTO, int userId, String passedValue) {
        Map map = new HashMap();
        int routeId = 0;
        int insertStatus = 0, count=0;
        try {
            String[] temp1 = null;
            String[] temp2 = null;
            map.put("customerId", operationTO.getCustomerId());
            map.put("tripStatusId", 19);
            map.put("userId", userId);
            String checkSecondaryTripSchedule = "";
           // if (passedValue.contains(",")) {
                temp1 = passedValue.split(",");
                for (int i = 0; i < temp1.length; i++) {
                    if (temp1[i].contains("~")) {
                        temp2 = temp1[i].split("~");
                        map.put("scheduleMonth", temp2[0]);
                        map.put("vehicleId", temp2[1]);
                        map.put("status", temp2[2]);
                       System.out.println("map====" + map);
                        count = (Integer) getSqlMapClientTemplate().queryForObject("secondaryOperation.checkCount", map);
                        if(count==0){   // insert Table
                        if("jan".equals(temp2[0])){
                            insertStatus += (Integer) getSqlMapClientTemplate().update("secondaryOperation.insertVehicleTripScheduleJan", map);
                        }
                        if("feb".equals(temp2[0])){
                            insertStatus += (Integer) getSqlMapClientTemplate().update("secondaryOperation.insertVehicleTripScheduleFeb", map);
                        }
                        if("mar".equals(temp2[0])){
                            insertStatus += (Integer) getSqlMapClientTemplate().update("secondaryOperation.insertVehicleTripScheduleMar", map);
                        }
                        if("apr".equals(temp2[0])){
                            insertStatus += (Integer) getSqlMapClientTemplate().update("secondaryOperation.insertVehicleTripScheduleApr", map);
                        }
                        if("may".equals(temp2[0])){
                            insertStatus += (Integer) getSqlMapClientTemplate().update("secondaryOperation.insertVehicleTripScheduleMay", map);
                        }
                        if("jun".equals(temp2[0])){
                            insertStatus += (Integer) getSqlMapClientTemplate().update("secondaryOperation.insertVehicleTripScheduleJun", map);
                        }
                        if("jul".equals(temp2[0])){
                            insertStatus += (Integer) getSqlMapClientTemplate().update("secondaryOperation.insertVehicleTripScheduleJul", map);
                        }
                        if("aug".equals(temp2[0])){
                            insertStatus += (Integer) getSqlMapClientTemplate().update("secondaryOperation.insertVehicleTripScheduleAug", map);
                        }
                        if("sep".equals(temp2[0])){
                            insertStatus += (Integer) getSqlMapClientTemplate().update("secondaryOperation.insertVehicleTripScheduleSep", map);
                        }
                        if("oct".equals(temp2[0])){
                            insertStatus += (Integer) getSqlMapClientTemplate().update("secondaryOperation.insertVehicleTripScheduleOct", map);
                        }
                        if("nov".equals(temp2[0])){
                            insertStatus += (Integer) getSqlMapClientTemplate().update("secondaryOperation.insertVehicleTripScheduleNov", map);
                        }
                        if("dec".equals(temp2[0])){
                            insertStatus += (Integer) getSqlMapClientTemplate().update("secondaryOperation.insertVehicleTripScheduleDec", map);
                        }
                        }else{    // update Table
                        insertStatus += (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateVehicleTripSchedule", map);
                        }
//                        
                    }
                }
           // }
                System.out.println("insertStatus:"+insertStatus);
        } catch (Exception sqlException) {
             sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveRoute Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveRoute", sqlException);
        }

        return insertStatus;
    }

     public ArrayList getLeasingTripCustomer(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList secondaryTripCustomerList = new ArrayList();
        map.put("customerId", operationTO.getCustomerId());
         System.out.println("map:"+map);
        try {

            secondaryTripCustomerList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getLeasingTripCustomer", map);

            System.out.println("secondaryTripCustomerList size=" + secondaryTripCustomerList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "bunkList", sqlException);
        }
        return secondaryTripCustomerList;
    }

      public ArrayList getLeasingRoutePointDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        ArrayList routePointDetails = new ArrayList();
        map.put("routeId", operationTO.getSecondaryRouteId());
        ////System.out.println("map = " + map);
        try {
            routePointDetails = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getLeasingRoutePointDetails", map);
            ////System.out.println(" getRouteContractDetails Size :: :: " + routePointDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRouteContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRouteContractDetails", sqlException);
        }
        return routePointDetails;
    }
      //exp
      public ArrayList getExpenselist() {
            Map map = new HashMap();
            ArrayList getExpenselist = new ArrayList();
            try {
                getExpenselist = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getExpenselist", map);
              System.out.println(" getExpenselist =" + getExpenselist.size());
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getCityTollList Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-MRS-01", CLASS, "regNoDetailsDetails List", sqlException);
            }
    
            return getExpenselist;
    }
     public int insertExpenseMaster(SecondaryOperationTO operationTO,int userId,SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        String groupCode = "";
        String code = "";
        String delimiter = "-";
        String[] temp;

        try {
            map.put("expenseName", operationTO.getExpenseName());
            map.put("status", operationTO.getStatus());
            map.put("userId", userId);
            code = (String) session.queryForObject("secondaryOperation.getLedgerCode", map);
            temp = code.split(delimiter);
            //////System.out.println("temp[1] = " + temp[1]);
            int codeval = Integer.parseInt(temp[1]);
            int codev = codeval + 1;
            //////System.out.println("codev = " + codev);
            String ledgercode = "LEDGER-" + codev;
            //////System.out.println("ledgercode = " + ledgercode);
            map.put("ledgercode", ledgercode);
            map.put("groupId", ThrottleConstants.expenseGroupCode);
//            groupCode = (String) session.queryForObject("secondaryOperation.getLedgerGroupCode", map);
            map.put("levellD", ThrottleConstants.expenseLevelId);
            System.out.println("mapfffff" + map);
            status = (Integer) session.insert("secondaryOperation.insertFinanceLedger", map);
            System.out.println("statussdf" + status);
            map.put("ledgerId", status);
            status = (Integer) session.insert("secondaryOperation.insertExpenseMaster", map);
            System.out.println("stat2" + status);
                
           

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVendorMfrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return status;

    }
     public int updateExpenseMaster(SecondaryOperationTO operationTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        String groupCode = "";
        String code = "";
        String delimiter = "-";
        String[] temp;

        try {
            map.put("expenseId", operationTO.getExpenseId());
            map.put("expenseName", operationTO.getExpenseName());
            map.put("status", operationTO.getStatus());
            map.put("userId", userId);
            map.put("levellD", operationTO.getLevelID());
            map.put("ledgerId", operationTO.getLedgerId());
            System.out.println("mapfffff" + map);
            status = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateFinanceLedger", map);
            System.out.println("statussdf" + status);
            status = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateExpenseMaster", map);
            System.out.println("stat2" + status);
                
           

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVendorMfrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return status;

    }
     public String getDailyContractedRate(SecondaryOperationTO operationTO) {
        Map map = new HashMap();
        String rate = "";
        try {
             map.put("vehicleTypeId", operationTO.getVehicleTypeId());
            map.put("trailerTypeId", operationTO.getTrailerTypeId());
            map.put("customerId", operationTO.getCustomerId());

            rate = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getDailyContractedRate", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCngPrice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCngPrice", sqlException);
        }

        return rate;
    }
public int processInsertQuotation(SecondaryOperationTO operationTO, int UserId) {
        Map map = new HashMap();
        int contractId = 0;

        try {
            map.put("userId", UserId);

          //  if (!"".equals(operationTO.getCustomerId()) && !"".equals(operationTO.getStartDate()) && !"".equals(operationTO.getEndDate()) && !"".equals(operationTO.getBillingTypeId()) && !"".equals(operationTO.getBillingKmCalculationId())) {

                map.put("customerName", operationTO.getCustomerName());
                map.put("customerAddress", operationTO.getCustomerAddress());
                map.put("quotationDate", operationTO.getQuotationDate());
                map.put("quotationStatus", operationTO.getQuotationStatus());
                map.put("quotationType", operationTO.getQuotationType());
                map.put("email", operationTO.getEmail());
                map.put("salesManager", operationTO.getSalesManager());
                map.put("validity", operationTO.getValidity());
                map.put("quotationNo", operationTO.getQuotationNo());
                map.put("dept", operationTO.getDept());
                map.put("contactPerson", operationTO.getContactPerson());
                map.put("driverResponsibility", operationTO.getDriverResponsibility());
                map.put("fuelResponsibility", operationTO.getFuelResponsibility());
                map.put("conditions", operationTO.getConditions());
                map.put("tariffType", operationTO.getTariffType());
                map.put("customerId", operationTO.getCustomerId());
                System.out.println("Inser Quotation map ===================== " + map);
                contractId = (Integer) getSqlMapClientTemplate().insert("secondaryOperation.InsertQuotation", map);
                System.out.println("Quotation = " + contractId);
         //   }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVendorMfrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return contractId;

    }
public int processInsertQuotationDetails(SecondaryOperationTO operationTO, int userId,int quotationId, String vehicleTypeIds,String fixedCostPerVehicles,String trailerTypeIds,String qtys,String perKmCostValues,String totalFixedCosts) {
        Map map = new HashMap();
                int status=0;

        try {
            map.put("userId", userId);

            if ( !"".equals(vehicleTypeIds)) {

                map.put("quotationId", quotationId);
                map.put("vehicleTypeId", vehicleTypeIds);
                map.put("fixedCostPerVehicle", fixedCostPerVehicles);
                map.put("trailerTypeId", trailerTypeIds);
                map.put("qty", qtys);
                map.put("perKmCostValue", perKmCostValues);
                map.put("totalFixedCost", totalFixedCosts);


                System.out.println("Inser Quotation Deatils map ===================== " + map);
                status = (Integer) getSqlMapClientTemplate().insert("secondaryOperation.InsertQuotationDetails", map);
                System.out.println("Quotation details = " + status);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVendorMfrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return status;

    }
 public int getQuotationSequenceCode() {
        Map map = new HashMap();
        int code = 0;
        try {
            code = (Integer) getSqlMapClientTemplate().insert("secondaryOperation.getQuotationSequence", map);
            ////System.out.println("getDriverIncentive size=" + tollAmount);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
           public String  */
            FPLogUtils.fpDebugLog("getDriverIncentive Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverIncentive List", sqlException);
        }

        return code;
    }

 public ArrayList getQuotationViewList(SecondaryOperationTO operationTO) {
        Map map = new HashMap();

        ArrayList quotationList = new ArrayList();
        try {

            quotationList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.viewQuotationList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCity Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getCity", sqlException);
        }
        return quotationList;

    }

 public ArrayList getQuotationDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();

        ArrayList quotationList = new ArrayList();
        try {
             map.put("quotationId",operationTO.getQuotationId());
            quotationList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.viewQuotationDetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCity Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getCity", sqlException);
        }
        return quotationList;

    }
 public ArrayList getQuotationVehicleDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();

        ArrayList quotationList = new ArrayList();
        try {
             map.put("quotationId",operationTO.getQuotationId());
             System.out.println("map for quotation view:"+map);
            quotationList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getQuotationVehicleDetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCity Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getCity", sqlException);
        }
        return quotationList;

    }
 public ArrayList getAccountManager(SecondaryOperationTO operationTO) {
        Map map = new HashMap();

        ArrayList accountManager = new ArrayList();
        try {

            accountManager = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getAccountManager", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCity Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getCity", sqlException);
        }
        return accountManager;

    }

 public int updateQuotationDetails(String[] quotationDetailsId,String[] fixedCostPerVehicleE,String[] totalCostE,String[] costPerKmE,int userId) {
        Map map = new HashMap();

      int status=0;
        try {
            map.put("userId", userId);
              System.out.println("quotationDetailsId.length:"+quotationDetailsId.length);
              if (quotationDetailsId.length > 0) {
                for(int i=0;i<quotationDetailsId.length;i++){

                map.put("quotationDetailsId", quotationDetailsId[i]);
                map.put("fixedCostPerVehicle", fixedCostPerVehicleE[i]);
                map.put("totalCost", totalCostE[i]);
                map.put("costPerKm", costPerKmE[i]);

                System.out.println("update Quotation Details map ===================== " + map);
                status = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateQuotationDetails", map);
                System.out.println("status1fgtfgdf = " + status);
                }


            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVendorMfrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return status;

    }
    public ArrayList getEmp(SecondaryOperationTO operationTO) {
        Map map = new HashMap();

        ArrayList accountManager = new ArrayList();
        try {

            accountManager = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getEmp", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCity Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getCity", sqlException);
        }
        return accountManager;

    }

    public int insertSafetyAlert(String alertFor, String vehicleId, String trailerId, String empId, String other, String description, String driverId, int UserId) {
        Map map = new HashMap();
        int contractId = 0;

        try {
            map.put("userId", UserId);

              //  if (!"".equals(operationTO.getCustomerId()) && !"".equals(operationTO.getStartDate()) && !"".equals(operationTO.getEndDate()) && !"".equals(operationTO.getBillingTypeId()) && !"".equals(operationTO.getBillingKmCalculationId())) {
            map.put("vehicleId", vehicleId);
            map.put("trailerId", trailerId);
            map.put("empId", empId);
            map.put("other", other);
            map.put("alertFor", alertFor);
            map.put("description", description);
            map.put("driverId", driverId);

            System.out.println("Insert Alert map ===================== " + map);
            contractId = (Integer) getSqlMapClientTemplate().update("secondaryOperation.insertSafetyAlert", map);
            System.out.println("status1fgtfgdf = " + contractId);
             //   }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVendorMfrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return contractId;

    }

    public ArrayList getSafetyAlertDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();

        ArrayList alertList = new ArrayList();
        try {

            alertList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getSafetyAlertDetails", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCity Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getCity", sqlException);
        }
        return alertList;

    }
    public ArrayList getQuotationPrintDetails(SecondaryOperationTO operationTO) {
            Map map = new HashMap();
    
            ArrayList alertList = new ArrayList();
            try {
                 map.put("quotationId",operationTO.getQuotationId());
                alertList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.quotationPrintDetails", map);

            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getCity Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-MFR-01", CLASS, "getCity", sqlException);
}
            return alertList;

    }
    public ArrayList getQuotationHeadertDetails(SecondaryOperationTO operationTO) {
            Map map = new HashMap();

            ArrayList alertList = new ArrayList();
            try {
                 map.put("quotationId",operationTO.getQuotationId());
                 System.out.println("map..for header"+map);
                alertList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.quotationHeader", map);

            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getCity Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-MFR-01", CLASS, "getCity", sqlException);
            }
            return alertList;

        }
      public String getQuotationEmailId(SecondaryOperationTO operationTO) {
            Map map = new HashMap();
            String email = "";
            try {
                 map.put("quotationId", operationTO.getQuotationId());


                email = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getQuotationEmailId", map);
            } catch (Exception sqlException) {
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getCngPrice Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-MRS-01", CLASS, "getCngPrice", sqlException);
            }

            return email;
    }
      public String getDailyContractId(SecondaryOperationTO operationTO) {
            Map map = new HashMap();
            String contract_id = "";
            try {
                 map.put("customerId", operationTO.getCustomerId());


                contract_id = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getDailyContractId", map);
                System.out.println("contract_id"+ contract_id);
            } catch (Exception sqlException) {
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("getCngPrice Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-MRS-01", CLASS, "getCngPrice", sqlException);
            }

            return contract_id;
    }

       public ArrayList getDailyOrderQuotationNo(SecondaryOperationTO operationTO) {
        Map map = new HashMap();

        ArrayList quotationList = new ArrayList();
        try {
            System.out.println("Dao");
            quotationList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getDailyOrderQuotationNo", map);
               System.out.println("quotationList:"+quotationList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCity Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getCity", sqlException);
        }
        return quotationList;

    }
       public ArrayList getDailyCustomerDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();

        ArrayList quotationList = new ArrayList();
        try {
            map.put("quotationId", operationTO.getQuotationId());
        map.put("userId", operationTO.getUserId());


            System.out.println("Dao");
            quotationList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getDailyContratcedCustomerNameDetails", map);
               System.out.println("quotationList:"+quotationList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCity Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getCity", sqlException);
        }
        return quotationList;

    }
       public ArrayList getQuotationTrailerType(SecondaryOperationTO operationTO) {
        Map map = new HashMap();

        ArrayList quotationList = new ArrayList();
        try {
            map.put("quotationId", operationTO.getQuotationId());
        map.put("userId", operationTO.getUserId());


            System.out.println("Map for Trailer Type:"+map);
            quotationList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getQuotationTrailerType", map);
               System.out.println("trailertypeList:"+quotationList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCity Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getCity", sqlException);
        }
        return quotationList;

    }
       public ArrayList getQuotationVehicleType(SecondaryOperationTO operationTO) {
        Map map = new HashMap();

        ArrayList quotationList = new ArrayList();
        try {
            map.put("quotationId", operationTO.getQuotationId());
        map.put("userId", operationTO.getUserId());


            System.out.println("map for vehicle type:"+map);
            quotationList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getQuotationVehicleType", map);
               System.out.println("vehicleTypelist:"+quotationList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCity Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getCity", sqlException);
        }
        return quotationList;

    }

       public int insertCustomerDistanceContract(String[] refernceName,String[] vehicleTypeIds,String[] containerTypeId,String[] containerQty,String[] fromDistance,String[] toDistance,String[] rateWithReeferDistance,String[] rateWithoutReeferDistance, String[] loadTypeId,String companyId, int userId) {
        Map map = new HashMap();
        int contractId = 0;

        try {
            map.put("userId", userId);
            map.put("companyId", companyId);
           if(refernceName !=null && refernceName.length >0){
             for(int i=0;i< vehicleTypeIds.length;i++){

            map.put("vehicleTypeId", vehicleTypeIds[i]);
            map.put("refernceName", refernceName[i]);
            map.put("containerTypeId", containerTypeId[i]);
            map.put("containerQty", containerQty[i]);
            map.put("fromDistance", fromDistance[i]);
            map.put("toDistance", toDistance[i]);
            map.put("rateWithReeferDistance", rateWithReeferDistance[i]);
            map.put("rateWithoutReeferDistance", rateWithoutReeferDistance[i]);
            map.put("loadType", loadTypeId[i]);
           
            System.out.println("Insert insertCustomerDistanceContract  map ===================== " + map);
            if(refernceName[i] != null && !"".equalsIgnoreCase(refernceName[i]) && vehicleTypeIds[i] != null && !"".equalsIgnoreCase(vehicleTypeIds[i]) && containerTypeId[i] != null && !"".equalsIgnoreCase(containerTypeId[i]) ){
            contractId = (Integer) getSqlMapClientTemplate().update("secondaryOperation.insertDistanceRouteTariff", map);
            System.out.println("insertCustomerDistanceContract = " + contractId);
            }
 }
            }     //   }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVendorMfrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return contractId;

    }

      public ArrayList getCustomerDistanceContractDetails(SecondaryOperationTO operationTO) {
        Map map = new HashMap();

        ArrayList contractList = new ArrayList();
        try {
            map.put("customerId", operationTO.getCustomerId());
            map.put("companyId", operationTO.getCompanyId());

            System.out.println("map for getCustomerDistanceContractDetails:"+map);
            contractList = (ArrayList) getSqlMapClientTemplate().queryForList("secondaryOperation.getCustomerDistanceContractDetails", map);
               System.out.println("vehicleTypelist:"+contractList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCity Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getCity", sqlException);
        }
        return contractList;

    }


       public int updateCustomerDistanceContract(String[] distanceContractIds,String[] rateWithReefer1,String[] rateWithoutReefer1,String[] validStatus) {
        Map map = new HashMap();
        int contractId = 0;

        try {
           
             for(int i=0;i< distanceContractIds.length;i++){

            map.put("distanceContractId", distanceContractIds[i]);
            map.put("rateWithReefer", rateWithReefer1[i]);
            map.put("rateWithoutReefer", rateWithoutReefer1[i]);
            map.put("validStatus", validStatus[i]);
           
            System.out.println(" updateCustomerDistanceContract  map ===================== " + map);
             if(distanceContractIds[i] != null && !"".equalsIgnoreCase(distanceContractIds[i]) && rateWithoutReefer1[i] != null && !"".equalsIgnoreCase(rateWithoutReefer1[i]) ){
            contractId = (Integer) getSqlMapClientTemplate().update("secondaryOperation.updateDistanceRouteTariff", map);
            System.out.println("updateCustomerDistanceContract = " + contractId);
                 }
 }
             //   }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVendorMfrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-02", CLASS,
                    "insertMfr", sqlException);
        }
        return contractId;

    }

 public String validateContract(String origin,String pointId1,String pointId2,String destination,String customerId,String vehicleTypeId,String containerTypeId,String loadTypeId) {
        Map map = new HashMap();
        String check = "";
             map.put("origin", origin);
            map.put("pointId1", pointId1);
            map.put("pointId2", pointId2);
            map.put("destination",destination);
            map.put("customerId",customerId);
            map.put("vehicleTypeId",vehicleTypeId);
            map.put("containerTypeId",containerTypeId);
            map.put("loadTypeId",loadTypeId);
            System.out.println("map validate.."+map);
        try {
            check = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.validateContract", map);
            if(check==null){
              check="0~0";
            }
             System.out.println("check.."+check);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConfigDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConfigDetails", sqlException);
        }

        return check;
    }

  public String getContractId(SecondaryOperationTO operationTO) {
        Map map = new HashMap();

        String contractId ="";
        try {
            map.put("customerId", operationTO.getCustomerId());
            map.put("companyId", operationTO.getCompanyId());

            System.out.println("map for contractId:"+map);
            contractId = (String) getSqlMapClientTemplate().queryForObject("secondaryOperation.getContractId", map);
               System.out.println("contractId:"+contractId);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCity Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getCity", sqlException);
        }
        return contractId;

    }
}
