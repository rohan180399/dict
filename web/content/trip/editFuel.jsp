
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@page import="java.text.DecimalFormat"%>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>

<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });
    $(function() {
        $(".datepicker").datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true, changeYear: true
        });
    });
</script>       
<script type="text/javascript">


    function replaceSpecialCharacters()
    {
        var content = document.getElementById("requestremarks").value;

        //alert(content.replace(/[^a-zA-Z0-9]/g,'_'));
        // content=content.replace(/[^a-zA-Z0-9]/g,'');
        content = content.replace(/[@&\/\\#,+()$~%'":*?<>{}]/g, ' ');
        document.getElementById("requestremarks").value = content;
        //alert("Repalced");

    }
</script>

<script language="javascript">
    function submitPage() {

        document.approve.action = '/throttle/updateTripFuel.do';
        document.approve.submit();
    }
    function setFocus() {
        document.approve.advancerequestamt.select();
    }
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> PrimaryOperation</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">PrimaryOperation</a></li>
            <li class="active">Edit Fuel</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body onload="setFocus();">
                <form name="approve"  method="post" >
                    <%
                                request.setAttribute("menuPath", "Fuel >> Edit Fuel");
                                String tripid = request.getParameter("tripid");
                                String type = "M";
                    %>
                    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>


                    <c:if test="${tripDieselDetails != null}">
                        <input type="hidden" id="tripId" name="tripId" value="<c:out value="${tripId}" />"/>
                        <table class="table table-info mb30" style="width:100%" id="fuelTBL" name="fuelTBL">
                            <!--<table style="width: 70%; left: 30px;" class="border" cellpadding="0"  cellspacing="0" align="center" border="0" id="fuelTBL" name="fuelTBL">-->
                            <thead style="background-color:#5BC0DE;width:100%;height:30px;color:white;text-align: left;font-size: 13px;"> 
                                <tr height="30px">
                                    <th>S No</th>
                                    <th>Bunk Name</th>
                                    <th>Vehicle(Hire)</th>
                                    <th>Slip No</th>
                                    <th>Date</th>
                                    <th>Ltrs</th>
                                    <th>Amount</th>
                                    <th>Person</th>
                                    <th>Remarks</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>   
                            <%int index1=1;%>
                            <script type="text/javascript">
                                function changeRate(sno) {
                                    var bunkLocat = document.getElementById("bunkName" + sno).value;
                                    var temp = bunkLocat.split("~");
                                    var fuelLters = document.getElementById("fuelLtrs" + sno).value;
                                    document.getElementById("fuelAmount" + sno).value = (temp[3] * fuelLters).toFixed(2);
                                }
                            </script>
                            <c:forEach items="${tripDieselDetails}" var="tripDiesel">
                                <tr height="30px">
                                    <td><%=index1++%></td>
                                    <td>

                                        <input name="uniqueId" type="hidden" class="textbox" id="uniqueId" value="<c:out value="${tripDiesel.uniqueId}" />" />
                                        <select style="width:200px;height:30px;"  id='bunkName1' name='bunkName' onchange="changeRate(1)" class="textbox">
                                            <option   value="0">---Select---</option>
                                            <c:if test = "${bunkList != null}" >
                                                <c:forEach items="${bunkList}" var="bunk">
                                                    <option selected value='<c:out value="${bunk.bunkId}" />'> <c:out value="${bunk.bunkName}" />
                                                    </c:forEach >
                                                </c:if>
                                        </select>


                                    </td>
                                    <td>
                                        <input name="vehicleNo" type="text" class="textbox" id="vehicleNo" value="<c:out value="${tripDiesel.vehicleNo}"/>" style="width:120px;height:30px;"/>
                                        <input type="hidden" id="vehicleId" name="vehicleId" value="<c:out value="${tripDiesel.vehicleId}" />"/>
                                        <input type="hidden" id="bunkId" name="bunkId" value="<c:out value="${tripDiesel.bunkId}" />"/>
                                    </td>
                                <script type="text/javascript">
                                    var bunkloc = document.getElementById("bunkId").value;
                                    <c:if test = "${bunkList != null}" ><c:forEach items="${bunkList}" var="bunk">
                                    var tempBunk = '<c:out value="${bunk.bunkId}" />';
                                    var tempVal = tempBunk.split("~");
                                    if (bunkloc == tempVal[0]) {
                                        $('#bunkName1').val(tempBunk);
                                    }
                                        </c:forEach ></c:if>
                                    </script>
                                    <td>
                                        <input name="slipNo" type="text" class="textbox" id="slipNo" value="<c:out value="${tripDiesel.ticketNo}"/>" style="width:120px;height:30px;"/>
                                </td>

                                <td>
                                    <input name="bunkPlace" type="hidden" class="textbox" id="bunkPlace"  />
                                    <input name="fuelDate" id="fuelDate" type="text" class="datepicker" value='<c:out value="${tripDiesel.fuelDate}"/>' id="fuelDate"  style="width:120px;height:30px;" />
                                </td>
                                <td>
                                    <input name="fuelLtrs" type="text" class="textbox" value="<c:out value="${tripDiesel.dieselUsed}"/>" id="fuelLtrs1"  onblur="sumFuel();" onchange="fuelPrice1(1);" style="width:120px;height:30px;" />
                                </td>
                                <td>
                                    <input name="fuelAmount" readonly type="text" class="textbox" value="<c:out value="${tripDiesel.dieselCost}"/>" id="fuelAmount1"  onblur="sumFuel();"  style="width:120px;height:30px;" readonly />
                                </td>

                                <td>
                                    <input name="fuelFilledName" type="text" class="textbox" id="fuelFilledName" value="<%= session.getAttribute("userName")%>" style="width:120px;height:30px;"/>
                                    <input name="fuelFilledBy" type="hidden" class="textbox" id="fuelFilledBy" value="<%= session.getAttribute("userId")%>" />
                                </td>
                                <td>
                                    <textarea name='fuelRemarks' id='fuelRemarks' class='textbox' type='text' rows='2' cols='10' style="width:130px;"></textarea>
                                    <input type="hidden" name="HfId" id="HfId" />
                                </td>
                                <td>&nbsp;</td>
                                </tr>
                            </c:forEach>
                            <tr height="50px;">
                                <td colspan="10" align="center">
                                    <!--		                                                    <input type="button" name="add" value="Add" onclick="addFuelRow()" id="add" class="button" />-->
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="button" name="delete" value="Delete" onclick="delFuelRow()" id="delete" class="btn btn-success" style="width:80px;height:30px;"/>
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="button" name="save" value="Save" onclick="submitPage()" id="save" class="btn btn-success" style="width:80px;height:30px;"/>
                                    <input type="hidden" name="totalFuelLtrs" id="totalFuelLtrs" value=""/>
                                    <input type="hidden" name="totalKms" id="totalKms" value=""/>
                                    <input type="hidden" name="totalFuelAmount" id="totalFuelAmount" value=""/>
                                </td>
                            </tr>

                            <script>
                                var poItems = 0;
                                var rowCount = '';
                                var sno = '';
                                var snumber = '';

                                function addFuelRow()
                                {
                                    var currentDate = new Date();
                                    var day = currentDate.getDate();
                                    var month = currentDate.getMonth() + 1;
                                    var year = currentDate.getFullYear();
                                    var myDate = day + "-" + month + "-" + year;

                                    if (sno < 9) {
                                        sno++;
                                        var tab = document.getElementById("fuelTBL");
                                        var rowCount = tab.rows.length;

                                        snumber = parseInt(rowCount) - 1;
                                        //                    if(snumber == 1) {
                                        //                        snumber = parseInt(rowCount);
                                        //                    }else {
                                        //                        snumber++;
                                        //                    }

                                        var newrow = tab.insertRow(parseInt(rowCount) - 1);
                                        newrow.height = "30px";
                                        // var temp = sno1-1;
                                        var cell = newrow.insertCell(0);
                                        var cell0 = "<td><input type='hidden'  name='fuelItemId' /> " + snumber + "</td>";
                                        //cell.setAttribute(cssAttributeName,"text1");
                                        cell.innerHTML = cell0;

                                        cell = newrow.insertCell(1);
                                        //                                        cell0 = "<td class='text2'><input name='bunkName' type='text' class='textbox'     id='bunkName' class='Textbox' /></td>";
                                        cell0 = "<td class='text2'><select class='textbox' style='width:100px;' id='bunkName" + snumber + "' style='width:123px'  name='bunkName' onchange='changeRate(" + snumber + ");><option selected value=0>---Select---</option><c:if test = "${bunkList != null}" ><c:forEach items="${bunkList}" var="bunk"><option  value='<c:out value="${bunk.bunkId}" />'><c:out value="${bunk.bunkName}" /> </c:forEach ></c:if> </select> <input type='hidden' id='uniqueId" + snumber + "' name='uniqueId' value=''/><input type='hidden' id='bunkPlace" + snumber + "' name='bunkPlace' value=''/></td>";
                                        //cell.setAttribute(cssAttributeName,"text1");
                                        cell.innerHTML = cell0;

                                        cell = newrow.insertCell(2);
                                        cell0 = "<td class='text1'><input name='slipNo' id='slipNo" + snumber + "' type='text'  style='width:120px;' value='' class='Textbox' /></td>";
                                        //cell.setAttribute(cssAttributeName,"text1");
                                        cell.innerHTML = cell0;

                                        cell = newrow.insertCell(3);
                                        cell0 = "<td class='text1'><input name='fuelDate' id='fuelDate" + snumber + "' type='text' class='datepicker' style='width:120px;' value='" + myDate + "' class='Textbox' /></td>";
                                        //cell.setAttribute(cssAttributeName,"text1");
                                        cell.innerHTML = cell0;

                                        cell = newrow.insertCell(4);
                                        cell0 = "<td class='text1'><input name='fuelLtrs' onBlur='sumFuel();' onchange='fuelPrice1(" + snumber + ");' type='text' class='textbox' id='fuelLtrs" + snumber + "' value='0' style='width:80px;' class='Textbox' /></td>";
                                        //cell.setAttribute(cssAttributeName,"text1");
                                        cell.innerHTML = cell0;

                                        cell = newrow.insertCell(5);
                                        cell0 = "<td class='text1'><input name='fuelAmount' readonly onBlur='sumFuel();' type='text' class='textbox' value='0' style='width:80px;' id='fuelAmount" + snumber + "' class='Textbox' /></td>";
                                        //cell.setAttribute(cssAttributeName,"text1");
                                        cell.innerHTML = cell0;



                                        cell = newrow.insertCell(6);
                                        cell0 = "<td class='text1'><input name='fuelFilledBy'  type='hidden' class='textbox' id='fuelFilledBy'  value='<%= session.getAttribute("userId")%>' /><input name='fuelFilledName' type='text' style='width:55px;' class='textbox' id='fuelFilledName' class='Textbox' value='<%= session.getAttribute("userName")%>' /></td>";
                                        //cell.setAttribute(cssAttributeName,"text1");
                                        cell.innerHTML = cell0;

                                        cell = newrow.insertCell(7);
                                        cell0 = "<td class='text1'><textarea name='fuelRemarks' id='fuelRemarks' class='textbox' type='text' rows='2' cols='10'></textarea></td>";
                                        //cell.setAttribute(cssAttributeName,"text1");
                                        cell.innerHTML = cell0;

                                        cell = newrow.insertCell(8);
                                        var cell1 = "<td><input type='checkbox' name='deleteItem' value='" + snumber + "'/> </td>";
                                        //cell.setAttribute(cssAttributeName,"text1");
                                        cell.innerHTML = cell1;
                                        // rowCount++;


                                        $(".datepicker").datepicker({
                                            /*altField: "#alternate",
                                             altFormat: "DD, d MM, yy"*/
                                            changeMonth: true, changeYear: true
                                        });
                                    }
                                }
                                function fuelPrice1(rowVal)
                                {
                                    //  alert("----->"+rowVal);
                                    var fuelRate = document.getElementById('bunkName' + rowVal).value;
                                    //alert("=====>"+fuelRate);
                                    var temp = fuelRate.split('~');
                                    fuelRate = temp[3];
                                    //  alert(fuelRate);

                                    var fuelPrice = fuelRate;
                                    var fuelLters = document.getElementById("fuelLtrs" + rowVal).value;
                                    //  alert("fuelLters==>"+fuelLters);
                                    var fuelAmount = (fuelPrice * fuelLters).toFixed(2);
                                    //  alert("fuelAmount==>"+fuelAmount);
                                    //                                                        document.tripSheet.fuelAmount.value = fuelAmount;
                                    document.getElementById("fuelAmount" + rowVal).value = fuelAmount;
                                    // sumFuel();
                                }

                                function delFuelRow() {
                                    try {
                                        var table = document.getElementById("fuelTBL");
                                        rowCount = table.rows.length - 1;
                                        for (var i = 2; i < rowCount; i++) {
                                            var row = table.rows[i];
                                            var checkbox = row.cells[7].childNodes[0];
                                            if (null != checkbox && true == checkbox.checked) {
                                                if (rowCount <= 1) {
                                                    alert("Cannot delete all the rows");
                                                    break;
                                                }
                                                table.deleteRow(i);
                                                rowCount--;
                                                i--;
                                                sno--;
                                                // snumber--;
                                            }
                                        }
                                        sumFuel();
                                    } catch (e) {
                                        alert(e);
                                    }
                                }


                                function sumFuel() {
                                    //alert("this s ctesting");
                                    var totFuel = 0;
                                    var totltr = 0;
                                    var totAmount = 0;
                                    var totAmt = 0;
                                    totFuel = document.getElementsByName('fuelLtrs');

                                    totAmount = document.getElementsByName('fuelAmount');
                                    for (i = 0; i < totFuel.length; i++) {
                                        totltr = +totltr.toFixed(2) + +totFuel[i].value;
                                    }
                                    document.getElementById('totalFuelLtrs').value = totltr.toFixed(2);
                                    for (i = 0; i < totAmount.length; i++) {
                                        totAmt = +totAmt.toFixed(2) + +totAmount[i].value;
                                    }
                                    document.getElementById('totalFuelAmount').value = totAmt.toFixed(2);
                                    // sumExpenses();
                                    //setBalance();
                                }


                                function totalKmsFunc() {
                                    //document.getElementById('totalKms').value=parseInt(document.getElementById('kmsIn').value)-parseInt(document.getElementById('kmsOut').value);
                                    document.getElementById('totalKms').value = 0;
                                }

                                function sumExpenses() {
                                    var sumAmt = 0;
                                    sumAmt = parseInt(document.getElementById('totalAllowance').value) + parseInt(document.getElementById('totalFuelAmount').value);
                                    document.getElementById('totalExpenses').value = parseInt(sumAmt);
                                }
                                function setBalance() {
                                    var sumAmt = 0;
                                    sumAmt = parseInt(document.getElementById('totalAllowance').value) - parseInt(document.getElementById('totalFuelAmount').value);
                                    document.getElementById('balanceAmount').value = parseInt(sumAmt);
                                }


                            </script>

                        </table>

                    </c:if>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
