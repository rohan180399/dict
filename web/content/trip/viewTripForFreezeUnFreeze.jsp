
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>


<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>
<script type="text/javascript">
    function computeExpense() {

        var distance = document.trip.preStartLocationDistance.value;
        var vehicleMileage = document.trip.vehicleMileage.value;
        var tollRate = document.trip.tollRate.value;
        var fuelPrice = document.trip.fuelPrice.value;

        if (distance.trim() != '' && distance.trim() != '0') {
            var fuelLtrs = parseFloat(distance) / parseFloat(vehicleMileage);
            var fuelCost = (parseFloat(fuelPrice) * fuelLtrs);
            var tollCost = parseFloat(distance) * parseFloat(tollRate);
            var totalCost = (fuelCost + tollCost).toFixed(2);
            document.trip.preStartRouteExpense.value = totalCost;
            document.trip.preStartLocationDurationHrs.value = 0;
            document.trip.preStartLocationDurationMins.value = 0;
        }

    }

    var httpReq;
    var temp = "";




    function fetchRouteInfo() {
        var preStartLocationId = document.trip.preStartLocationId.value;
        var originId = document.trip.originId.value;
        var vehicleTypeId = document.trip.vehicleTypeId.value;
        if (preStartLocationId != '') {
            var url = "/throttle/checkPreStartRoute.do?preStartLocationId=" + preStartLocationId + "&originId=" + originId + "&vehicleTypeId=" + vehicleTypeId;

            if (window.ActiveXObject)
            {
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest)
            {
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function() {
                processFetchRouteCheck();
            };
            httpReq.send(null);
        }

    }

    function processFetchRouteCheck()
    {
        if (httpReq.readyState == 4)
        {
            if (httpReq.status == 200)
            {
                temp = httpReq.responseText.valueOf();
                //alert(temp);
                if (temp != '' && temp != null && temp != 'null') {
                    var tempVal = temp.split('-');
                    document.trip.preStartLocationDistance.value = tempVal[0];
                    document.trip.preStartLocationDurationHrs.value = tempVal[1];
                    document.trip.preStartLocationDurationMins.value = tempVal[2];
                    document.trip.preStartRouteExpense.value = tempVal[3];
                    document.getElementById("preStartLocationDistance").readOnly = true;
                    document.getElementById("preStartLocationDurationHrs").readOnly = true;
                    document.getElementById("preStartLocationDurationMins").readOnly = true;
                    document.getElementById("preStartRouteExpense").readOnly = true;
                } else {
                    alert('valid route does not exists between pre start location and trip start location');
                    document.getElementById("preStartLocationDistance").readOnly = false;
                    document.getElementById("preStartLocationDurationHrs").readOnly = false;
                    document.getElementById("preStartLocationDurationMins").readOnly = false;
                    document.getElementById("preStartRouteExpense").readOnly = false;
                }
//                        if(tempVal[0] == 0){
//                            alert("no match found");
//                        }
            }
            else
            {
                alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
            }
        }
    }
    function processPreStartCheckBox() {

        var tempVal = document.getElementById("preStartLocationCheckbox").checked;
        if (tempVal) {
            document.trip.preStartLocationCheckbox.value = 1;
            $("#preStartDetailDiv").hide();
        } else {
            document.trip.preStartLocationCheckbox.value = 0;
            $("#preStartDetailDiv").show();
        }
    }
    function setButtons() {
        var temp = document.trip.actionName.value;
        if (temp != '0') {
            if (temp == '1') {
                $("#actionDiv").show();
//                        $("#preStartDiv").show();
            } else {
//                        $("#preStartDiv").hide();

            }
        } else {
//                    $("#preStartDiv").hide();
        }
    }

    //start ajax for pre location Nos
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#preStartLocation').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCityFromName.do",
                    dataType: "json",
                    data: {
                        cityFrom: request.term,
                        textBox: 1
                    },
                    success: function(data, textStatus, jqXHR) {
//                                alert(data);
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        //console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#preStartLocationId').val(tmp[0]);
                $('#preStartLocation').val(tmp[1]);
                return false;
            }
            // Format the list menu output of the autocomplete
        }).data("autocomplete")._renderItem = function(ul, item) {
            //alert(item);
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    });
    //end ajax for pre location
    function saveAction() {
        var statusCheck = true;
        var temp = document.trip.actionName.value;
        //alert(temp);
        if (temp != '0') {
            if (temp == '1') { //freeze selected
//                        var tempVal = document.getElementById("preStartLocationCheckbox").checked;
                //alert(tempVal);
//                        if(!tempVal){//same checkbox is not selected
//                            //check for distance and route expense;
//                            var distance = document.trip.preStartLocationDistance.value;
//                            //alert(distance);
//                            if(distance == '' || distance == '0'){
//                                alert('please enter pre start distance');
//                                document.trip.preStartLocationDistance.focus();
//                                statusCheck = false;
//                            }
//                        }
                statusCheck = true;
            }
        } else if (temp == 0) {
            alert('please select your action');
            statusCheck = false;
        }
//                 if(statusCheck && !tempVal && temp == '1'){
//                    if(document.trip.preStartLocationPlanDate.value == ''){
//                        alert('please enter pre start plan date');
//                        document.trip.preStartLocationPlanDate.focus();
//                        statusCheck = false;
//                    }
//                }

        if (statusCheck) {
            document.trip.action = "/throttle/saveTripUpdate.do?";
            document.trip.submit();
        }
    }
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });



</script>
<script type="text/javascript">
    function calculateDate() {
        var endDates = document.getElementById("tripScheduleDate").value;
        var tempDate1 = endDates.split("-");
        var stDates = document.getElementById("preStartLocationPlanDate").value;
        var tempDate2 = stDates.split("-");
        var prevTime = new Date(tempDate2[2], tempDate2[1], tempDate2[0]);  // Feb 1, 2011
        var thisTime = new Date(tempDate1[2], tempDate1[1], tempDate1[0]);              // now
        var difference = thisTime.getTime() - prevTime.getTime();   // now - Feb 1
        if (prevTime.getTime() < thisTime.getTime()) {
            if (difference > 0) {
                alert(" Selected Date is greater than scheduled date ");
                document.getElementById("preStartLocationPlanDate").value = document.getElementById("todayDate").value;
            }
        }
    }
    function calculateTime() {
        var endDates = document.getElementById("tripScheduleDate").value;
        var endTimeIds = document.getElementById("tripScheduleTime").value;
        var tempDate1 = endDates.split("-");
        var tempTime3 = endTimeIds.split(" ");
        var tempTime1 = tempTime3[0].split(":");
        if (tempTime3[1] == "PM") {
            tempTime1[0] = 12 + parseInt(tempTime1[0]);
        }
        var stDates = document.getElementById("preStartLocationPlanDate").value;
        var hour = document.getElementById("preStartLocationPlanTimeHrs").value;
        var minute = document.getElementById("preStartLocationPlanTimeMins").value;
        var stTimeIds = hour + ":" + minute + ":" + "00";
        var tempDate2 = stDates.split("-");
        var tempTime2 = stTimeIds.split(":");
        var prevTime = new Date(tempDate2[2], tempDate2[1], tempDate2[0], tempTime2[0], tempTime2[1]);  // Feb 1, 2011
        var thisTime = new Date(tempDate1[2], tempDate1[1], tempDate1[0], parseInt(tempTime1[0]), tempTime1[1]);              // now
        var difference = thisTime.getTime() - prevTime.getTime();   // now - Feb 1
        var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
        if (prevTime.getTime() < thisTime.getTime()) {
            if (hoursDifference > 0) {
                alert("Selected Time is greater than scheduled Time ");
                document.getElementById("preStartLocationPlanTimeHrs").focus();
            }
        }
    }
</script>
<script type="text/javascript" language="javascript">




    function getDriverName() {
        var oTextbox = new AutoSuggestControl(document.getElementById("driName"), new ListSuggestions("driName", "/throttle/handleDriverSettlement.do?"));

    }
</script>
<script language="">
    function print(val)
    {
        var DocumentContainer = document.getElementById(val);
        var WindowObject = window.open('', "TrackHistoryData",
                "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        WindowObject.close();
    }



</script>

<script type="text/javascript">

    function computeVehicleCapUtil() {
        var orderWeight = document.trip.totalWeight.value;
        var vehicleCapacity = document.trip.vehicleTonnage.value;
        if (vehicleCapacity > 0) {
            var utilPercent = (orderWeight / vehicleCapacity) * 100;
            document.trip.vehicleCapUtil.value = utilPercent.toFixed(2);


            var e = document.getElementById("vehicleCheck");
            e.style.display = 'block';
        }
    }
    //start ajax for vehicle Nos
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#vehicleNo').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getVehicleRegNos.do",
                    dataType: "json",
                    data: {
                        vehicleNo: request.term,
                        textBox: 1
                    },
                    success: function(data, textStatus, jqXHR) {
//                                alert(data);
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        //console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#vehicleId').val(tmp[0]);
                $('#vehicleNo').val(tmp[1]);
                $('#vehicleTonnage').val(tmp[2]);
                $('#driver1Id').val(tmp[3]);
                $('#driver1Name').val(tmp[4]);
                $('#driver2Id').val(tmp[5]);
                $('#driver2Name').val(tmp[6]);
                $('#driver3Id').val(tmp[7]);
                $('#driver3Name').val(tmp[8]);
                //$itemrow.find('#vehicleId').val(tmp[0]);
                //$itemrow.find('#vehicleNo').val(tmp[1]);
                return false;
            }
            // Format the list menu output of the autocomplete
        }).data("autocomplete")._renderItem = function(ul, item) {
            //alert(item);
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    //.append( "<a>"+ item.Name + "</a>" )
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    });
    //end ajax for vehicle Nos



</script>

<style>
    #index th {
        color:white;
        font-weight: bold;
        background-color:#5BC0DE;
        font-size:14px;
    }
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> PrimaryOperation</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">PrimaryOperation</a></li>
            <li class="active">Update Trip Details</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body >

                <form name="trip" method="post">
                    <br>

                    <%@ include file="/content/common/message.jsp"%>
                    <%
            Date today = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String startDate = sdf.format(today);
                    %>

                    <%
                    String vehicleMileageAndTollRate = "";
                    String vehicleMileage = "0";
                    String tollRate = "0";
                    String fuelPrice = "0";
                    String[] temp = null;
                    if(request.getAttribute("vehicleMileageAndTollRate") != null){
                        vehicleMileageAndTollRate = (String)request.getAttribute("vehicleMileageAndTollRate");
                        temp = vehicleMileageAndTollRate.split("-");
                        vehicleMileage = temp[0];
                        tollRate = temp[1];
                        fuelPrice = temp[2];
                    }
                    %>
                    <input type="hidden" name="vehicleMileage" value="<%=vehicleMileage%>" />
                    <input type="hidden" name="tollRate" value="<%=tollRate%>" />
                    <input type="hidden" name="fuelPrice" value="<%=fuelPrice%>" />
                    <input type="hidden" name="tripType" value="<c:out value="${tripType}"/>" />
                    <input type="hidden" name="statusId" value="<c:out value="${statusId}"/>" />
                    <table width="300" cellpadding="0" cellspacing="0" align="right" border="0" id="report" style="margin-top:0px;">

                        <tr id="exp_table" >
                            <td colspan="8" bgcolor="#5BC0DE" style="padding:10px;" align="left">
                                <div class="tabs" align="left" style="width:300;">

                                    <div id="first">
                                        <c:if test = "${tripDetails != null}" >
                                            <c:forEach items="${tripDetails}" var="trip">
                                                <table width="300" cellpadding="0" cellspacing="1" border="0" align="center">
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Expected Revenue:</b></font></td>
                                                        <td> <font color="white"><b><c:out value="${trip.orderRevenue}" /></b></font></td>

                                                    </tr>
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Projected Expense:</b></font></td>
                                                        <td><font color="white"><b> <c:out value="${trip.orderExpense}" /></b></font></td>

                                                    </tr>
                                                    <c:set var="profitMargin" value="" />
                                                    <c:set var="orderRevenue" value="${trip.orderRevenue}" />
                                                    <c:set var="orderExpense" value="${trip.orderExpense}" />

                                                    <input type="hidden" name="orderExpense" Id="orderExpense" class="textbox" value='<c:out value="${trip.orderExpense}" />' >
                                                    <c:set var="profitMargin" value="${orderRevenue - orderExpense}" />
                                                    <%
                                                    String profitMarginStr = "" + (Double)pageContext.getAttribute("profitMargin");
                                                    String revenueStr = "" + (String)pageContext.getAttribute("orderRevenue");
                                                    float profitPercentage = 0.00F;
                                                    if(!"".equals(revenueStr) && !"".equals(profitMarginStr)){
                                                        profitPercentage = Float.parseFloat(profitMarginStr)*100/Float.parseFloat(revenueStr);
                                                    }


                                                    %>
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Profit Margin:</b></font></td>
                                                        <td> <font color="white"><b> <%=new DecimalFormat("#0.00").format(profitPercentage)%>(%)</b></font>
                                                            <input type="hidden" name="profitMargin" value='<c:out value="${profitMargin}" />'>
                                                        <td>

                                                        <td>
                                                    </tr>
                                                </table>
                                            </c:forEach>
                                        </c:if>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>



                    <div id="tabs" >
                        <ul class="nav nav-tabs">

                            <li class="active" data-toggle="tab"><a href="#action"><span>Action</span></a></li>
                            <li data-toggle="tab"><a href="#tripDetail"><span>Trip Details </span></a></li>
                            <li data-toggle="tab"><a href="#routeDetail"><span>Consignment Note(s) / Route Course / Trip Plan</span></a></li>
                            <!--                    <li><a href="#driverDetail"><span>Driver</span></a></li>
                        <li><a href="#cleanerDetail"><span>Cleaner</span></a></li>-->
                            <!--                    <li><a href="#advDetail"><span>Advance</span></a></li>-->
                            <!--<li><a href="#expDetail"><span>Expense Details</span></a></li>-->
                            <!--                    <li><a href="#summary"><span>Remarks</span></a></li>-->

                        </ul>

                        <div id="action" class="tab-pane active" >
                            <br>
                            <table border="0" style=" outline: 3px solid #5BC0DE;"  align="center" width="980" cellpadding="0" cellspacing="0" >
                                <!--<table border="1" frame="box" style="outline-color: red;outline-width: thick;" class="border" align="center" width="980" cellpadding="0" cellspacing="0" >-->
                                <tr height="40">

                                    <td class="text1">Action: </td>
                                    <td  class="text1"> <input type="hidden" onclick="processPreStartCheckBox();"  name="preStartLocationCheckbox"  id="preStartLocationCheckbox" value="1"  />
                                        <select name="actionName" id="actionName" onChange="setButtons();" style="width:130px;height:22px;" class="textbox">
                                            <option value="0">--select--</option>
                                            <option value="1">Freeze</option>
                                            <option value="2">UnFreeze</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr height="60">
                                    <td  class="text2">Remarks</td>
                                    <td  class="text2"><textarea name="actionRemarks" rows="3" cols="100"></textarea> </td>
                                </tr>
                                <tr height="40">
                                    <td  class="text1">Do you want to intimate?</td>
                                    <td  class="text1">
                                        Client&nbsp;<input type="checkbox" name="client" value="1" /> &nbsp;&nbsp;
                                        A/C Mgr&nbsp;<input type="checkbox" name="manager" value="1" /> &nbsp;&nbsp;
                                        Fleet Mgr&nbsp;<input type="checkbox" name="fleetManager" value="1" /> &nbsp;&nbsp;
                                    </td>
                                </tr>

                            </table>
                            <br>
                            <br>
                            <table border="0"  align="center" width="980" cellpadding="0" cellspacing="0" >
                                <tr>
                                    <td align="center">
                                        <div id="actionDiv" style="display:block;align:center">
                                            <input type="button" class="btn btn-success" value="Save Action" name="Next" onclick="saveAction();" style="width:110px;height:30px;padding:2px; "/>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;
                                        <div id="preStartDiv" style="display:none;">
                                            <table border="0" width="500" align="left" cellpadding="0" cellspacing="0" >
                                                <tr>
                                                    <td class="text1">Pre Start Location </td>
                                                    <td class="text1" align="left"  >
                                                        <!--                                                    <input type="checkbox" onclick="processPreStartCheckBox();" style="width:10px;" name="preStartLocationCheckbox"  id="preStartLocationCheckbox" value="0" />same as trip start location-->
                                                    </td>
                                                </tr>


                                            </table>
                                            <div id="preStartDetailDiv" style="display:block;">
                                                <table border="0" width="500" align="left" cellpadding="0" cellspacing="0" >
                                                    <tr>
                                                        <td class="text1">Pre Start Location  </td>
                                                        <td class="text1" align="left"  >
                                                            <input  type="text" name="preStartLocation" id="preStartLocation" onBlur="fetchRouteInfo();" value="" class="textbox" />
                                                            <input  type="hidden" name="preStartLocationId" id="preStartLocationId" value="" class="textbox" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text1">preStartLocationPlanDate:</td>
                                                        <td class="text1"><input  type="text" name="preStartLocationPlanDate"  id="preStartLocationPlanDate"  value='<%=startDate%>' class="datepicker" onchange="calculateDate();" class="datepicker"/>
                                                            <input type="hidden" name="todayDate" id="todayDate" value='<%=startDate%>'/></td>
                                                    </tr>
                                                    <tr>

                                                        <td class="text1">preStartLocationPlanTime:</td>
                                                        <td class="text1">HH: <select name="preStartLocationPlanTimeHrs"  id="preStartLocationPlanTimeHrs" onchange="calculateTime();">
                                                                <option value='00'>00</option>
                                                                <option value='01'>01</option>
                                                                <option value='02'>02</option>
                                                                <option value='03'>03</option>
                                                                <option value='04'>04</option>
                                                                <option value='05'>05</option>
                                                                <option value='06'>06</option>
                                                                <option value='07'>07</option>
                                                                <option value='08'>08</option>
                                                                <option value='09'>09</option>
                                                                <option value='10'>10</option>
                                                                <option value='11'>11</option>
                                                                <option value='12'>12</option>
                                                                <option value='13'>13</option>
                                                                <option value='14'>14</option>
                                                                <option value='15'>15</option>
                                                                <option value='16'>16</option>
                                                                <option value='17'>17</option>
                                                                <option value='18'>18</option>
                                                                <option value='19'>19</option>
                                                                <option value='20'>20</option>
                                                                <option value='21'>21</option>
                                                                <option value='22'>22</option>
                                                                <option value='23'>23</option>


                                                            </select>
                                                            MI: <select name="preStartLocationPlanTimeMins"  id="preStartLocationPlanTimeMins" >
                                                                <option value='00'>00</option>
                                                                <option value='01'>01</option>
                                                                <option value='02'>02</option>
                                                                <option value='03'>03</option>
                                                                <option value='04'>04</option>
                                                                <option value='05'>05</option>
                                                                <option value='06'>06</option>
                                                                <option value='07'>07</option>
                                                                <option value='08'>08</option>
                                                                <option value='09'>09</option>
                                                                <option value='10'>10</option>
                                                                <option value='11'>11</option>
                                                                <option value='12'>12</option>
                                                                <option value='13'>13</option>
                                                                <option value='14'>14</option>
                                                                <option value='15'>15</option>
                                                                <option value='16'>16</option>
                                                                <option value='17'>17</option>
                                                                <option value='18'>18</option>
                                                                <option value='19'>19</option>
                                                                <option value='20'>20</option>
                                                                <option value='21'>21</option>
                                                                <option value='22'>22</option>
                                                                <option value='23'>23</option>
                                                                <option value='24'>24</option>
                                                                <option value='25'>25</option>
                                                                <option value='26'>26</option>
                                                                <option value='27'>27</option>
                                                                <option value='28'>28</option>
                                                                <option value='29'>29</option>
                                                                <option value='30'>30</option>
                                                                <option value='31'>31</option>
                                                                <option value='32'>32</option>
                                                                <option value='33'>33</option>
                                                                <option value='34'>34</option>
                                                                <option value='35'>35</option>
                                                                <option value='36'>36</option>
                                                                <option value='37'>37</option>
                                                                <option value='38'>38</option>
                                                                <option value='39'>39</option>
                                                                <option value='40'>40</option>
                                                                <option value='41'>41</option>
                                                                <option value='42'>42</option>
                                                                <option value='43'>43</option>
                                                                <option value='44'>44</option>
                                                                <option value='45'>45</option>
                                                                <option value='46'>46</option>
                                                                <option value='47'>47</option>
                                                                <option value='48'>48</option>
                                                                <option value='49'>49</option>
                                                                <option value='50'>50</option>
                                                                <option value='51'>51</option>
                                                                <option value='52'>52</option>
                                                                <option value='53'>53</option>
                                                                <option value='54'>54</option>
                                                                <option value='55'>55</option>
                                                                <option value='56'>56</option>
                                                                <option value='57'>57</option>
                                                                <option value='58'>58</option>
                                                                <option value='59'>59</option>
                                                            </select>
                                                        </td>

                                                    </tr>

                                                    <tr>
                                                        <td class="text1">preStartLocationDistance</td>
                                                        <td class="text1"><input  type="text" name="preStartLocationDistance"  id="preStartLocationDistance"  onchange="computeExpense();"  value="" class="textbox" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text1">preStartLocationDurationHrs:</td>
                                                        <td class="text1"><input  type="text" name="preStartLocationDurationHrs"  id="preStartLocationDurationHrs"  value="" class="textbox" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text1">preStartLocationDurationMins:</td>
                                                        <td class="text1"><input  type="text" name="preStartLocationDurationMins"  id="preStartLocationDurationMins"  value="" class="textbox" /></td>
                                                    </tr>

                                                    <tr>
                                                        <td class="text1">preStartRouteExpense</td>
                                                        <td class="text1"><input  type="text" name="preStartRouteExpense"  id="preStartRouteExpense"  value="" class="textbox" /></td>

                                                    </tr>

                                                </table>
                                            </div>
                                        </div>

                                    </td>
                                </tr>


                            </table>
                            <br/>


                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="<spring:message code="vendors.label.NEXT" text="default text"/>" name="Next" style="width:100px;height:35px;font-weight: bold;"/></a>
                            </center>

                        </div>
                        <div id="tripDetail" class="tab-pane">
                            <table   class="table table-info mb30"  id="bg">
                                <thead>
                                    <tr  id="index" height="30px;">
                                        <th colspan="6">Trip Details</th>
                                    </tr>
                                </thead>

                                <c:if test = "${tripDetails != null}" >
                                    <c:forEach items="${tripDetails}" var="trip">


                                        <tr>
                                            <!--                            <td class="text1"><font color="red">*</font>Trip Sheet Date</td>
                                                                        <td class="text1"><input type="text" name="tripDate" class="datepicker" value=""></td>-->
                                            <td class="text1">CNote No(s)</td>
                                            <td class="text1">
                                                <c:out value="${trip.cNotes}" />
                                            </td>
                                            <td class="text1">Billing Type</td>
                                            <td class="text1">
                                                <c:out value="${trip.billingType}" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <!--                            <td class="text2">Customer Code</td>
                                                                        <td class="text2">BF00001</td>-->
                                            <td class="text2">Customer Name</td>
                                            <td class="text2">
                                                <c:out value="${trip.customerName}" />
                                                <input type="hidden" name="customerName" Id="customerName" class="textbox" value='<c:out value="${customerName}" />'>
                                                <input type="hidden" name="tripId" Id="tripId" class="textbox" value='<c:out value="${trip.tripId}" />'>
                                                <input type="hidden" name="originId" Id="tripId" class="textbox" value='<c:out value="${trip.originId}" />'>
                                                <input type="hidden" name="vehicleTypeId" Id="tripId" class="textbox" value='<c:out value="${trip.vehicleTypeId}" />'>
                                                <input type="hidden" name="vehicleId" Id="vehicleId" class="textbox" value="">
                                                <input type="hidden" name="driver1Id" Id="driver1Id" class="textbox" value=""  >
                                            </td>
                                            <td class="text2">Customer Type</td>
                                            <td class="text2" colspan="3" >
                                                <c:out value="${trip.customerType}" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text1">Route Name</td>
                                            <td class="text1">
                                                <c:out value="${trip.routeInfo}" />
                                            </td>
                                            <!--                            <td class="text1">Route Code</td>
                                                                        <td class="text1" >DL001</td>-->
                                            <td class="text1">Reefer Required</td>
                                            <td class="text1" >
                                                <c:out value="${trip.reeferRequired}" />
                                            </td>
                                            <td class="text1">Order Est Weight (MT)</td>
                                            <td class="text1" >
                                                <c:out value="${trip.totalWeight}" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text2">Vehicle Type</td>
                                            <td class="text2">
                                                <c:out value="${trip.vehicleTypeName}" />
                                            </td>
                                            <td class="text2"><font color="red">*</font>Vehicle No</td>
                                            <td class="text2">
                                                <c:out value="${trip.vehicleNo}" />

                                            </td>
                                            <td class="text2">Vehicle Capacity (MT)</td>
                                            <td class="text2">
                                                <c:out value="${trip.vehicleTonnage}" />

                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="text1">Veh. Cap [Util%]</td>
                                            <td class="text1">
                                                <c:out value="${trip.vehicleCapUtil}" />
                                            </td>
                                            <td class="text1">Special Instruction</td>
                                            <td class="text1">-</td>
                                            <td class="text1">Trip Schedule</td>
                                            <td class="text1"><c:out value="${trip.tripScheduleDate}" />  <c:out value="${trip.tripScheduleTime}" />
                                                <input type="hidden" name="tripScheduleDate"  id="tripScheduleDate" value='<c:out value="${trip.tripScheduleDate}" />'>
                                                <input type="hidden" name="tripScheduleTime" id="tripScheduleTime" value='<c:out value="${trip.tripScheduleTime}" />'></td>
                                        <input type="hidden" name="totalHours" id="totalHours" value='<c:out value="${trip.estimatedTransitHours}" />'> </td>
                                        </tr>


                                        <tr>
                                            <td class="text2"><font color="red">*</font>Driver </td>
                                            <td class="text2" colspan="5" >
                                                <c:out value="${trip.driverName}" />
                                            </td>

                                        </tr>
                                    </c:forEach>
                                </c:if>
                            </table>
                            <br/>
                            <c:if test = "${expiryDateDetails != null}" >
                                <table  class="table table-info mb30" id="bg">
                                    <thead>
                                        <tr  id="index" height="30px;">
                                            <th colspan="4">Vehicle Compliance Check</th>
                                        </tr>
                                    </thead>
                                    <c:forEach items="${expiryDateDetails}" var="expiryDate">
                                        <tr>
                                            <td class="text2">Vehicle FC Valid UpTo</td>
                                            <td class="text2"><label><font color="green"><c:out value="${expiryDate.fcExpiryDate}" /></font></label></td>
                                        </tr>
                                        <tr>
                                            <td class="text1">Vehicle Insurance Valid UpTo</td>
                                            <td class="text1"><label><font color="green"><c:out value="${expiryDate.insuranceExpiryDate}" /></font></label></td>
                                        </tr>
                                        <tr>
                                            <td class="text2">Vehicle Permit Valid UpTo</td>
                                            <td class="text2"><label><font color="green"><c:out value="${expiryDate.permitExpiryDate}" /></font></label></td>
                                        </tr>
                                        <tr>
                                            <td class="text2">Road Tax Valid UpTo</td>
                                            <td class="text2"><label><font color="green"><c:out value="${expiryDate.roadTaxExpiryDate}" /></font></label></td>
                                        </tr>
                                    </c:forEach>
                                </table>
                                <br/>
                                <br/>
                            </c:if>
                            <br/>
                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="<spring:message code="vendors.label.NEXT" text="default text"/>" name="Next" style="width:100px;height:35px;font-weight: bold;"/></a>
                                <a><input type="button" class="btn btn-success btnPrevious" value="<spring:message code="vendors.label.PREVIOUS" text="default text"/>" name="Previous" style="width:100px;height:35px;font-weight: bold;"/></a>
                            </center>
                            <br>
                            <br>
                        </div>
                        <div id="routeDetail" class="tab-pane">
                            <c:if test = "${tripPointDetails != null}" >
                                <table class="table table-info mb30" >
                                    <thead>
                                        <tr id="index" height="30px;">
                                            <th height="30" >Point Name</th>
                                            <th height="30" >Type</th>
                                            <th height="30" >Route Order</th>
                                            <th height="30" >Address</th>
                                            <th height="30" >Planned Date</th>
                                            <th height="30" >Planned Time</th>
                                        </tr>
                                    </thead>
                                    <c:forEach items="${tripPointDetails}" var="tripPoint">
                                        <tr>
                                            <td class="text1" height="30" ><c:out value="${tripPoint.pointName}" /></td>
                                            <td class="text1" height="30" ><c:out value="${tripPoint.pointType}" /></td>
                                            <td class="text1" height="30" ><c:out value="${tripPoint.pointSequence}" /></td>
                                            <td class="text1" height="30" ><c:out value="${tripPoint.pointAddress}" /></td>
                                            <td class="text1" height="30" ><c:out value="${tripPoint.pointPlanDate}" /></td>
                                            <td class="text1" height="30" ><c:out value="${tripPoint.pointPlanTime}" /></td>
                                        </tr>
                                    </c:forEach >
                                </table>
                                <br>

                                <table class="table table-info mb30"  >
                                    <c:if test = "${tripDetails != null}" >
                                        <c:forEach items="${tripDetails}" var="trip">
                                            <tr>
                                                <td class="text1" width="150"> Estimated KM</td>
                                                <td class="text1" width="120" > <c:out value="${trip.estimatedKM}" />&nbsp;</td>
                                                <td class="text1" colspan="4">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="text2" width="150"> Estimated Reefer Hour</td>
                                                <td class="text2" width="120"> <c:out value="${trip.estimatedTransitHours * 60 / 100}" />&nbsp;</td>
                                                <td class="text2" colspan="4">&nbsp;</td>
                                            </tr>

                                        </c:forEach>
                                    </c:if>
                                </table>


                            </c:if>
                            <center>
                                <br>
                                <br>
                                <a><input type="button" class="btn btn-success btnPrevious" value="<spring:message code="vendors.label.PREVIOUS" text="default text"/>" name="Previous" style="width:100px;height:35px;font-weight: bold;"/></a>
                            </center>
                        </div>
                        <br>
                        <br>
                        <br>
                    </div>
                    <script>
                        $('.btnNext').click(function() {
                            $('.nav-tabs > .active').next('li').find('a').trigger('click');
                        });
                        $('.btnPrevious').click(function() {
                            $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                        });
                        //                                   
                    </script>

        </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
        </body>
    </div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>