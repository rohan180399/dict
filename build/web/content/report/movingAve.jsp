

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
    </head>
    <script language="javascript">

function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}


        var httpReq;
        var temp = "";
        function ajaxData()
        {
           if(document.IssueReport.mfrId.value !='0'){ 
            var url = "/throttle/getModels1.do?mfrId="+document.IssueReport.mfrId.value;    
            if (window.ActiveXObject)
                {
                    httpReq = new ActiveXObject("Microsoft.XMLHTTP");
                }
                else if (window.XMLHttpRequest)
                    {
                        httpReq = new XMLHttpRequest();
                    }
                    httpReq.open("GET", url, true);
                    httpReq.onreadystatechange = function() { processAjax(); } ;
                    httpReq.send(null);
                }
                }
                function processAjax()
                {
                    if (httpReq.readyState == 4)
                        {
                            if(httpReq.status == 200)
                                {
                                    temp = httpReq.responseText.valueOf();                 
                                    setOptions(temp,document.IssueReport.modelId);        
                                    if('<%= request.getAttribute("modelId")%>' != 'null' ){
                                        document.IssueReport.modelId.value = <%= request.getAttribute("modelId")%>;
                                    }
                                }
                                else
                                    {
                                        alert("Error loading page\n"+ httpReq.status +":"+ httpReq.statusText);
                                    }
                                }
                            }
                            
                            function submitPage(){
                                var chek=validation();
                                if(chek=='true'){                              
                                    document.IssueReport.action = '/throttle/movingAveReport.do';
                                    document.IssueReport.submit();
                                }
                            }
                            function validation(){
                                if(document.IssueReport.companyId.value==0){
                                    alert("Please Select Data for Company Name");
                                    document.IssueReport.companyId.focus();
                                    return 'false';
                                }           
                                else  if(textValidation(document.IssueReport.fromDate,'From Date')){
                                    return 'false';
                                }
                                else  if(textValidation(document.IssueReport.toDate,'TO Date')){
                                    return'false';
                                }
                                return 'true';
                            }
                            
                            function setValues(){
                                var poId='<%=request.getAttribute("poId")%>';
                                if('<%=request.getAttribute("companyId")%>'!='null'){
                                    document.IssueReport.companyId.value='<%=request.getAttribute("companyId")%>';                                   
                                    document.IssueReport.fromDate.value='<%=request.getAttribute("fromDate")%>';
                                    document.IssueReport.toDate.value='<%=request.getAttribute("toDate")%>';
                                }
                                if('<%=request.getAttribute("categoryId")%>'!='null'){
                                    document.IssueReport.categoryId.value='<%=request.getAttribute("categoryId")%>';
                                }
                                if('<%=request.getAttribute("mfrId")%>'!='null'){
                                    document.IssueReport.mfrId.value='<%=request.getAttribute("mfrId")%>';
                                }
                                if('<%=request.getAttribute("modelId")%>'!='null'){
                                    ajaxData();
                                    document.IssueReport.modelId.value='<%=request.getAttribute("modelId")%>';
                                }
                                if('<%=request.getAttribute("mfrCode")%>'!='null'){
                                    document.IssueReport.mfrCode.value='<%=request.getAttribute("mfrCode")%>';
                                } 
                                if('<%=request.getAttribute("paplCode")%>'!='null'){
                                    document.IssueReport.paplCode.value='<%=request.getAttribute("paplCode")%>';
                                }
                                if('<%=request.getAttribute("itemName")%>'!='null'){
                                    document.IssueReport.itemName.value='<%=request.getAttribute("itemName")%>';
                                }
                                
                            }
                            function getItemNames(){
                                var oTextbox = new AutoSuggestControl(document.getElementById("itemName"),new ListSuggestions("itemName","/throttle/handleItemSuggestions.do?"));
                                
                            } 
    </script>
    <body onload="setValues();getItemNames();">
        <form name="IssueReport" method="post">
            <%@ include file="/content/common/path.jsp" %>           
            <%@ include file="/content/common/message.jsp" %>



            <table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:850;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">Material Issue Report</li>
    </ul>
    <div id="first">
    <table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
  <tr>
                    <td width="114" height="30">&nbsp;&nbsp;Category</td>
                    <td width="172" height="30"><select name="categoryId" class="form-control" style="width:125px">
                            <option value="0">-select-</option>
                            <c:if test = "${categoryList != null}" >
                                <c:forEach items="${categoryList}" var="category">
                                    <option value="<c:out value="${category.categoryId}"/>"><c:out value="${category.categoryName}"/></option>
                                </c:forEach>
                            </c:if>

                    </select></td>
                    <td height="30">&nbsp;&nbsp;MFR</td>
                    <td height="30"><select name="mfrId" onchange="ajaxData();" class="form-control">
                            <option value="0">-select-</option>
                            <c:if test = "${MfrLists != null}" >
                                <c:forEach items="${MfrLists}" var="mfr">
                                    <option value="<c:out value="${mfr.mfrId}"/>"><c:out value="${mfr.mfrName}"/></option>
                                </c:forEach>
                            </c:if>
                    </select></td>
                    <td height="30">&nbsp;&nbsp;Model</td>
                    <td height="30"><select name="modelId" class="form-control" style="width:125px">
                            <option value="0">-select-</option>
                            <c:if test = "${ModelLists != null}" >
                                <c:forEach items="${ModelLists}" var="model">
                                    <option value="<c:out value="${model.modelId}"/>"><c:out value="${model.modelName}"/></option>
                                </c:forEach>
                            </c:if>
                    </select></td>
                </tr>
                <tr>

                    <td height="30"><font color="red">*</font>Location</td>
                    <td height="30"><select  class="form-control" name="companyId" style="width:125px">
                            <option value="0">-select-</option>
                            <c:if test = "${LocationLists != null}" >
                                <c:forEach items="${LocationLists}" var="company">
                                    <c:choose>
                                        <c:when test="${company.companyTypeId==1012}" >

                                            <option value="<c:out value="${company.cmpId}"/>"><c:out value="${company.name}"/></option>
                                        </c:when>
                                    </c:choose>
                                </c:forEach>
                            </c:if>

                    </select></td>
                     <td width="114" height="30">&nbsp;&nbsp;Item Name</td>
                    <td width="172" height="30"><input name="itemName" id="itemName" type="text"  class="form-control" value=""></td>
                    <td width="111" height="30">&nbsp;&nbsp;Mfr Item Code</td>
                    <td width="151" height="30"><input name="mfrCode" type="text" class="form-control" value="" size="20"></td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;PAPL Code</td>
                    <td height="30"><input name="paplCode" type="text" class="form-control" value="" size="20"></td>

                    <td width="80" height="30"><font color="red">*</font>From Date</td>
                    <td width="182"><input name="fromDate" type="text" class="form-control" value="" size="20">
                    <span><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.IssueReport.fromDate,'dd-mm-yyyy',this)"/></span></td>
                    <td width="148"><font color="red">*</font>To Date</td>
                    <td width="172" height="30"><input name="toDate" type="text" class="form-control" value="" size="20">
                    <span><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.IssueReport.toDate,'dd-mm-yyyy',this)"/></span></td>
                </tr>
                <tr><td colspan="6" align="center"><input type="button"  value="Fetch Data" class="button" name="Fetch Data" onClick="submitPage()">
                <input type="hidden" value="" name="reqfor"></td></tr>
    </table>
    </div></div>
    </td>
    </tr>
    </table>


            
            <br>
            
            <c:if test = "${stokIssueList != null}" >
                <table width="786" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
                    <tr class="contenthead">
                        <td width="51" class="contentsub">Sno</td>
                        <td width="80" height="30" class="contentsub">Manufacturer</td>
                        <td width="82" class="contentsub">Model</td>
                        <%--<td width="104" height="30" class="contentsub">Mfr Item Code </td>--%>
                        <td width="82" class="contentsub">PAPL Code </td>
                        <td width="82" height="30" class="contentsub">Item Name</td>
                        <td width="78" height="30" class="contentsub">Category</td>                        
                        <blockquote>&nbsp;</blockquote>                        
                        <td width="80" height="30" class="contentsub">Total Issued</td>
                        <td width="80" height="30" class="contentsub">Available Balance</td>
                        <td width="80" height="30" class="contentsub">Moving Avg</td>
                        <td width="80" height="30" class="contentsub">Month Avg</td>
                        <td width="80" height="30" class="contentsub">Days(1-15)</td>
                        <td width="80" height="30" class="contentsub">Days(15-31)</td>
                        <td width="80" height="30" class="contentsub">Price</td>
                        <td width="80" height="30" class="contentsub">Nett AMount</td>
                        
                    </tr>
                    <c:set var="issued" value="0" />
                    <c:set var="vendorPurAmount" value="0" />
                    <% int index = 0;%> 
                    <c:forEach items="${stokIssueList}" var="issue"> 
                    
                    
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>	
                        
                        <tr>                             
                            <td class="<%=classText%>" height="30"><%=index+1%></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.mfrName}"/></td>
                            <td class="<%=classText%>"><c:out value="${issue.modelName}" /></td>
                            <%--<td class="<%=classText%>" height="30"><c:out value="${issue.mfrCode}"/></td>--%>
                            <td class="<%=classText%>"><c:out value="${issue.paplCode}" /></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.itemName}"/></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.categoryName}"/></td>                                                                                                                                                                                                                                        
                            <td class="<%=classText%>" height="30"><c:out value="${issue.totalIssued}"/></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.itemQty}"/></td>
                            <td class="<%=classText%>" height="30">
                            <c:out value="${issue.movingAverage}"/>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.monthAverage}"/></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.firstHalfDays}"/></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.lastHalfDays}"/></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.price}"/></td>
                            <c:set var="totalAmount" value="${issue.monthAverage * issue.price}"/>
                            <td class="<%=classText%>" height="30"><fmt:formatNumber value="${totalAmount}" pattern="##.00"/></td>
                            
                        </td>
                        </tr>
                     
                        <% index++;%>
                    </c:forEach>
                  
                </table>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    
</html>

