<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/parveencss/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ page import="ets.domain.mrs.business.MrsTO" %> 

<script>

    function addRow(val) {
        if (document.getElementById(val).innerHTML == "") {
            document.getElementById(val).innerHTML = "<input type='text' size='7' class='form-control' >";
        } else {
            document.getElementById(val).innerHTML = "";
        }
    }
    function newWO(val) {
        window.open('/throttle/OtherServiceStockAvailability.do?itemId=' + val, 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
    }
    function back() {
        document.spareIssue.action = '/throttle/MRSApproval.do';
        document.spareIssue.submit();
    }
</script>


<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.MRS"  text="MRS"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.stores"  text="Stores"/></a></li>
            <li class="active"><spring:message code="stores.label.MRS"  text="MRS"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="spareIssue"  method="post"  action="/throttle/content/stores/MRSApproval.jsp">
                    <%@ include file="/content/common/message.jsp" %>
                    <table class="table table-info mb30 table-hover" >
                        <thead>
                            <tr>
                                <th colspan="4" align="center" ><spring:message code="stores.label.VehicleDetails"  text="MRS"/></th>
                            </tr>
                        </thead>


                        <c:if test = "${vehicleDetails != null}" >
                            <c:forEach items="${vehicleDetails}" var="vehicle"> 

                                <tr>
                                    <td ><spring:message code="stores.label.VehicleNumber"  text="MRS"/></td>
                                    <td ><c:out value="${vehicle.mrsVehicleNumber}"/></td>
                                    <td ><spring:message code="stores.label.KM"  text="MRS"/></td>
                                    <td ><c:out value="${vehicle.mrsVehicleKm}"/></td>
                                </tr>

                                <tr>
                                    <td ><spring:message code="stores.label.VehicleType"  text="MRS"/></td>
                                    <td ><c:out value="${vehicle.mrsVehicleType}"/></td>
                                    <td ><spring:message code="stores.label.MFR"  text="MRS"/></td>
                                    <td ><c:out value="${vehicle.mrsVehicleMfr}"/></td>
                                </tr>

                                <tr>
                                    <td ><spring:message code="stores.label.UseType"  text="MRS"/></td>
                                    <td ><c:out value="${vehicle.mrsVehicleUsageType}"/></td>
                                    <td ><spring:message code="stores.label.Model"  text="MRS"/></td>
                                    <td ><c:out value="${vehicle.mrsVehicleModel}"/></td>
                                </tr>

                                <tr>
                                    <td ><spring:message code="stores.label.EngineNo"  text="MRS"/></td>
                                    <td ><c:out value="${vehicle.mrsVehicleNumber}"/></td>
                                    <td ><spring:message code="stores.label.ChasisNo"  text="MRS"/></td>
                                    <td ><c:out value="${vehicle.mrsVehicleChassisNumber}"/></td>
                                </tr>

                                <tr>
                                    <td ><spring:message code="stores.label.Technician"  text="MRS"/></td>
                                    <td ><c:out value="${vehicle.mrsTechnician}"/></td>
                                    <td ><spring:message code="stores.label.JobCardNo"  text="MRS"/>.</td>
                                    <td ><c:out value="${vehicle.mrsJobCardNumber}"/></td>
                                </tr>

                            </c:forEach>
                        </c:if>
                    </table>
                    <br>
                    <br>
                    <c:if test = "${mrsDetails != null}" >
                        <table class="table table-info mb30 table-hover">
                            <thead>
                                <tr>
                                    <th colspan="10" align="center" ><spring:message code="stores.label.MRSDetails"  text="MRS"/> </th>
                                </tr>
                                <tr>
                                    <th  ><spring:message code="stores.label.Sno"  text="MRS"/></th>
                                    <th  ><spring:message code="stores.label.ItemCode"  text="MRS"/> </th>
                                    <th   ><div align="left"><spring:message code="stores.label.PAPLCode"  text="MRS"/></div></th>
                            <th><spring:message code="stores.label.ItemName"  text="MRS"/></th>
                            <th colspan="2" ><spring:message code="stores.label.AvailableStock"  text="MRS"/></th>
                            <th><spring:message code="stores.label.StockAtotherServicePoint"  text="MRS"/></th>
                            <th><spring:message code="stores.label.RequiredQty"  text="MRS"/></th>
                            <th><spring:message code="stores.label.POQty"  text="MRS"/></th>
                            <th  colspan="4" rowspan="5" >&nbsp;</th>
                            </tr>

                            <tr>
                                <th >&nbsp;</th>
                                <th >&nbsp;</th>
                                <th  ><div align="left"></div></th>
                            <th  >&nbsp;</th>
                            <th  ><spring:message code="stores.label.New"  text="MRS"/></th>
                            <th  ><spring:message code="stores.label.RC"  text="MRS"/></th>
                            <th  >&nbsp;</th>
                            <th  >&nbsp;</th>
                            <th  >&nbsp;</th>
                            </tr>
                            </thead>
                            <% int index = 0; %>
                            <c:forEach items="${mrsDetails}" var="mrs"> 
                                <%
                                   String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                    classText = "text2";
                                    } else {
                                    classText = "text1";
                                    }
                                %>
                                <tr>
                                    <td ><%=index+1%></td>
                                    <td ><c:out value="${mrs.mrsItemMfrCode}"/></td>
                                    <td  ><div align="left"><c:out value="${mrs.mrsPaplCode}"/></div></td>
                                    <td  ><c:out value="${mrs.mrsItemName}"/></td>
                                    <td  ><div align="center"><c:out value="${mrs.mrsLocalServiceItemSum}"/></div></td>
                                    <td  ><div align="center"><c:out value="${mrs.qty}"/></div></td>
                                    <td  ><div align="center"><a href="" onClick="newWO(<c:out value="${mrs.mrsItemId}"/>)"><c:out value="${mrs.mrsOtherServiceItemSum}"/></a></div></td>
                                    <td  ><div align="center"><c:out value="${mrs.mrsRequestedItemNumber}"/></div></td>    
                                    <td  ><div align="center"><c:out value="${mrs.poQuantity}"/></div></td> 
                                    <td></td>
                                    <!--<td></td>-->
                                </tr>
                                <% index++; %>
                            </c:forEach>
                        </c:if>
                    </table>
                    <br>
                    <center>
                        <input type="button" value="<spring:message code="stores.label.Back"  text="MRS"/>" class="btn btn-success" onclick="back();">
                    </center>
                    <br>
                    <br>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>


