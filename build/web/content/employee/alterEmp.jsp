<%-- 
    Document   : alterLecture
    Created on : Nov 7, 2008, 11:34:46 AM
    Author     : vijay
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<title>PAPL</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script> 
<script language="javascript" src="/throttle/js/validate.js"></script>
<script language="javascript">
window.onload = nameSearch;
function nameSearch(){
    //alert("value");
    var oTextbox = new AutoSuggestControl(document.getElementById("staffName"),  
                        new ListSuggestions("staffName","/throttle/getStaffName.do?"));
} 

function submitPage(value){
if(value == "search"){        
document.lecture.action = '/throttle/getAlterEmpPage.do';
document.lecture.submit();
}else if(isEmpty(document.lecture.staffNames.value)){
alert("Enter Valid Character for Employee Name ");
document.lecture.staffNames.focus();
}else if(isEmpty(document.lecture.qualification.value)){
alert("Qualification Field Should Not Be Empty");
document.lecture.qualification.focus();
}else if(isChar(document.lecture.qualification.value)){
alert("Enter Valid Character for Qualification ");
document.lecture.qualification.focus();
}else if(isEmpty(document.lecture.dateOfBirth.value)){
alert("Date of Birth Field Should Not Be Empty");
document.lecture.dateOfBirth.focus();
}else if(isEmpty(document.lecture.dateOfJoining.value)){
alert("Date Of Joining Field Should Not Be Empty");
document.lecture.dateOfJoining.focus();
}else if(isEmpty(document.lecture.bloodGrp.value)){
alert("Blood Group Field Should Not Be Empty");
document.lecture.bloodGrp.focus();
}else if(isEmpty(document.lecture.fatherName.value)){
alert("Father Field Should Not Be Empty");
document.lecture.fatherName.focus();
}else if(isEmpty(document.lecture.mobile.value)){
alert("Mobile Field Should Not Be Empty");
document.lecture.mobile.focus();
}else if(isDigit(document.lecture.mobile.value)){
alert("Mobile Field Should Be Digit");
document.lecture.mobile.focus();
}else if(isEmpty(document.lecture.phone.value)){
alert("Phone No Field Should Be Digit");
document.lecture.phone.focus();
}else if(isEmpty(document.lecture.email.value)){
alert("Email Field Should Not Be Empty");
document.lecture.email.focus();
}else if(isEmail(document.lecture.email.value)){
document.lecture.email.focus();
}else if(isEmpty(document.lecture.addr.value)){
alert("Present Address Field Should Not Be Empty");
document.lecture.addr.focus();
}else if(isEmpty(document.lecture.city.value)){
alert("Present City Field Should Not Be Empty");
document.lecture.city.focus();
}else if(isEmpty(document.lecture.state.value)){
alert("Present State Field Should Not Be Empty");
document.lecture.state.focus();
}else if(isEmpty(document.lecture.pincode.value)){
alert("Present Pincode Field Should Not Be Empty");
document.lecture.pincode.focus();
}else if(isEmpty(document.lecture.addr1.value)){
alert("Permanant Address Field Should Not Be Empty");
document.lecture.addr1.focus();
}else if(isEmpty(document.lecture.city1.value)){
alert("Permanant City Field Should Not Be Empty");
document.lecture.city1.focus();
}else if(isEmpty(document.lecture.state1.value)){
alert("Permanant State Field Should Not Be Empty");
document.lecture.state1.focus();
}else if(isEmpty(document.lecture.pincode1.value)){
alert("Permanant Pincode Field Should Not Be Empty");
document.lecture.pincode1.focus();
}else if(document.lecture.deptid.value == "0"){
alert("Select any one Department");
document.lecture.deptid.focus();
}else if(document.lecture.designationId.value == "0"){
alert("Select any one Designation");
document.lecture.designationId.focus();
}else if(document.lecture.gradeId.value == "0"){
alert("Select any one Grade");
document.lecture.gradeId.focus();
}else if(value=='save')
{
document.lecture.action = '/throttle/alterEmployee.do';    
document.lecture.submit();
}


}
function copyAddress(value)
{
if(value == true){
var addAddr;
var addCity;
var addState;
var pinCode;

addAddr = document.lecture.addr.value ;
addCity = document.lecture.city.value;
addState = document.lecture.state.value;
pinCode = document.lecture.pincode.value;

document.lecture.addr1.value = addAddr;
document.lecture.city1.value = addCity;  
document.lecture.state1.value = addState;
document.lecture.pincode1.value  = pinCode;
}
else 
{
document.lecture.addr1.value  = "";
document.lecture.city1.value = "";  
document.lecture.state1.value = "";
document.lecture.pincode1.value   = "";

}
}

function setValues(id,name){


if(id != 'null' && id != 0){
    
document.lecture.staffId.value = id;
}
if(name != 'null'){
    
document.lecture.name.value = name;
}
}


function isChar(s){
if(!(/^-?\d+$/.test(s))){
return false;
}
return true;
}

function isEmail(s)
{

if(/[^@]+@[^@]+\.(com)|(co.in)$/.test(s))
return false;
alert("Email not in valid form!");
return true;	
}

function initCs() {
var desig=document.getElementById('desigId');
//alert(desig.value);
desig.onchange=function() {
if(this.value!="") {
var list=document.getElementById("gradeId");
while (list.childNodes[0]) {
list.removeChild(list.childNodes[0])
}
fillSelect(this.value);
}
}

fillSelect(document.getElementById('desigId').value);
}

function go() {

if (httpRequest.readyState == 4) {
if (httpRequest.status == 200) {
var response = httpRequest.responseText;
var list=document.getElementById("gradeId");
var grade=response.split(',');
for (i=1; i<grade.length; i++) {
var gradeDetails =  grade[i].split('-');
var gradeId = gradeDetails[0];
var gradeName = gradeDetails[1];
var x=document.createElement('option');
var name=document.createTextNode(gradeName);
x.appendChild(name);
x.setAttribute('value',gradeId)
list.appendChild(x);
}
}
}
}


function fillSelect(desigId) {
//alert(desigId +" design Id ");
var url='/throttle/getGradeForDesig.do?desigId='+desigId;

if (window.ActiveXObject)
{
httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
}
else if (window.XMLHttpRequest)
{
httpRequest = new XMLHttpRequest();
}

httpRequest.open("POST", url, true);
httpRequest.onreadystatechange = function() {go(); } ;
httpRequest.send(null);
}





var httpRequest;
function getProfile(userName)
{

var url = '/throttle/checkUserName.do?userName='+ userName;

if (window.ActiveXObject)
{
httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
}
else if (window.XMLHttpRequest)
{
httpRequest = new XMLHttpRequest();
}

httpRequest.open("GET", url, true);

httpRequest.onreadystatechange = function() { processRequest(); } ;

httpRequest.send(null);
}


function processRequest()
{
if (httpRequest.readyState == 4)
{
if(httpRequest.status == 200)
{  
if(trim(httpRequest.responseText)!="") {    

document.getElementById("userNameStatus").innerHTML=httpRequest.responseText;
document.addLect.password.disabled=true;
document.addLect.password.disabled=true;
document.addLect.userName.select();
}else {
document.getElementById("userNameStatus").innerHTML="";
document.addLect.password.disabled=false;
document.addLect.password.focus();
}
}
else
{
alert("Error loading page\n"+ httpRequest.status +":"+ httpRequest.statusText);
}
}
}



function viewPage()
{
    document.addLect.action = '/throttle/EmpViewSearchPage.do';
    document.addLect.submit();
}



            function showTab() {
                if (document.addLect.userStatus.checked == true) {
                    document.getElementById("userStat").style.visibility = "visible";
                    document.getElementById("userStat1").style.visibility = "visible";
                    document.getElementById("userStatus1").value = 1;
                } else {
                    document.getElementById("userStat").style.visibility = "hidden";
                    document.getElementById("userStat1").style.visibility = "hidden";
                    document.getElementById("userStatus1").value = "";
                }
            }






</script>
</head>

<!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/ 
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->

<body onLoad="setValues('<%= request.getAttribute("staffId") %>','<%= request.getAttribute("staffName") %>');document.lecture.staffId.focus();">
<form name="lecture" method="post">
<!-- copy there from end -->
<div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
<div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
<!-- pointer table -->
<table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
<tr>
<td >
<%@ include file="/content/common/path.jsp" %>
</td></tr></table>
<!-- pointer table -->
</div>
</div>
<br>
<br>
<!-- message table -->
<table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
<tr>
<td >
<%@ include file="/content/common/message.jsp" %>
</td></tr></table>
<!-- message table -->
<!-- copy there  end -->
<div id="fixme" class="box">
<table  border="0" align="center" width="500" cellpadding="0" cellspacing="0" class="border" bgcolor="#FFFFFF" class="border"  >
<tr>
<td class="text2" height="30">Employee Id</td>
<td class="text2" height="30"><input type="text" class="textbox" maxlength="7" name="staffId" ></td>
<td class="text2" height="30">Employee Name</td>
<td class="text2" height="30"><input type="text" class="textbox" id="staffName" maxlength="20" name="staffName" onKeyPress="nameSearch();" onKeyPress="return onKeyPressBlockNumbers(event);"></td>
<td class="text2" height="30"><input type="button" class="button" value="Search" name="search" onClick="submitPage(this.name)"></td>
</tr>
</table>
</div>
<br>
<center>

<input type="hidden" value="" name="reqfor">
</center>
<br>
<%
if(request.getAttribute("StaffList") != null) {               
%>
<table width="800" align="center" border="0" cellpadding="0" cellspacing="0">    
<%--<c:choose>   
<c:when test = "${StaffList != null}">--%>
<c:if test = "${DeptList != null}">
    
<c:forEach items="${StaffList}" var="lec"> 

  
<tr>
<td class="contenthead" height="30" colspan="4"><b>Personal Details</b></td>
 <input type="text" name="userId" value="<c:out value="${lec.userId}"/>" />
</tr>


<tr>
<td class="text1" height="30">Staff Id</td>
<td class="text1" height="30"><input type="hidden" name="staffid" value='<c:out value="${lec.staffId}"/>'><c:out value="${lec.staffId}"/> </td>
<td class="text1" height="30">Name</td>
<td class="text1" height="30"><input type="text" name="staffNames" maxlength="35" class="textbox" value='<c:out value="${lec.name}"/>' onKeyPress="return onKeyPressBlockCharacters(event);"></td>
</tr>

<tr>
<td class="text2" height="30">Qualification </td>
<td class="text2" height="30"><input type="text" name="qualification" maxlength="10" size="20" class="textbox" value="<c:out value="${lec.qualification}"/>"></td>
<td class="text2" height="30">Date Of Birth</td>
<td class="text2" height="30"><input type="text" name="dateOfBirth" value='<c:out value="${lec.dateOfBirth}"/>' readonly="readOnly" size="20" class="textbox"> <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.lecture.dateOfBirth,'dd-mm-yyyy',this)"  style="cursor:default; "/></td>
</tr>

<tr>

<td class="text1" height="30">Date Of Joining </td>
<td class="text1" height="30"><input type="text" name="dateOfJoining" value='<c:out value="${lec.DOJ}"/>' size="20" readonly="readOnly" class="textbox"> <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.lecture.dateOfJoining,'dd-mm-yyyy',this)"  style="cursor:default; "/></td>   
<td height="30" class="text1">Gender</td>
<td height="30" class="text1">
<c:choose>
<c:when test="${lec.gender== 'Male'}">
<input type="radio" name="gender" checked value="Male">Male <input type="radio" name="gender" value="Female">Female</td>
</c:when>
<c:otherwise>
<input type="radio" name="gender" checked value="Female">Female <input type="radio" name="gender" value="Male">Male</td>
</c:otherwise>
</c:choose>    
</tr>

<tr>
<td class="text2" height="30">Blood Group</td>
<td class="text2" height="30"><input type="text" name="bloodGrp" size="20" maxlength="7" class="textbox" value="<c:out value="${lec.bloodGrp}"/>"></td>
<td height="30" class="text2">Marital Status</td>
<td class="text2" height="30">

<c:if test="${martialStatus =='Married'}">
<input type="radio" name="martial_Status" checked value="Married"> Married <input type="radio" name="martial_Status" value="UnMarried">UnMarried <input type="radio" name="martial_Status" value="Widow">Widow</td>
</c:if>
<c:if test="${martialStatus =='UnMarried'}">
<input type="radio" name="martial_Status" value="Married"> Married <input type="radio" name="martial_Status" checked value="UnMarried">UnMarried <input type="radio" name="martial_Status" value="Widow">Widow</td>
</c:if>
<c:if test="${martialStatus =='Widow'}">
<input type="radio" name="martial_Status" value="Married"> Married <input type="radio" name="martial_Status" value="UnMarried">UnMarried <input type="radio" checked name="martial_Status" value="Widow">Widow</td>
</c:if>
</tr>

<tr>
<td class="text1" height="30">Father Name</td>
<td class="text1" height="30"><input name="fatherName" type="text" maxlength="35" class="textbox" size="20" value="<c:out value="${lec.fatherName}"/>" onKeyPress="return onKeyPressBlockNumbers(event);"></td>
<td class="text1" height="30">Mobile No </td>
<td class="text1" height="30"><input type="text" name="mobile" size="20" maxlength="10" class="textbox" value="<c:out value="${lec.mobile}"/>" onKeyPress="return onKeyPressBlockCharacters(event);"></td>
</tr>

<tr>

<td class="text2" height="30">Phone No </td>
<td class="text2" height="30"><input type="text" name="phone" size="20" maxlength="12" class="textbox" value="<c:out value="${lec.phone}"/>" onKeyPress="return onKeyPressBlockCharacters(event);"></td>
<td class="text2" height="30">Email-Id</td>
<td class="text2" height="30" colspan="4"><input type="text" name="email" maxlength="35" size="20" class="textbox" value="<c:out value="${lec.email}"/>"></td>
</tr>
<tr>
<td class="contenthead" height="30" colspan="4"><b>Present Address</b></td>
</tr>
<tr >
<td class="text1" height="30">Address</td>
<td class="text1" height="30"> <input type="text" name="addr" size="20" maxlength="35" class="textbox" value="<c:out value="${lec.addr}"/>" onKeyPress="return onKeyPressBlockNumbers(event);"></td>
<td class="text1" height="30">City</td>
<td class="text1" height="30"><input type="text" name="city" size="20" maxlength="20" class="textbox" value="<c:out value="${lec.city}"/>" onKeyPress="return onKeyPressBlockNumbers(event);"></td>
</tr>
<tr>
<td class="text2" height="30">State</td>
<td class="text2" height="30"><input type="text" name="state" size="20" maxlength="20" class="textbox" value="<c:out value="${lec.state}"/>" onKeyPress="return onKeyPressBlockNumbers(event);"></td>
<td class="text2" height="30">Pincode</td>
<td class="text2" height="30"><input type="text" name="pincode" size="20" maxlength="6" class="textbox" value="<c:out value="${lec.pincode}"/>" onKeyPress="return onKeyPressBlockCharacters(event);"></td>
</tr>
<tr>
<td class="contenthead" height="30" colspan="2"><b>Permanent Address</b> </td>
<td colspan="2" class="contenthead" height="30"><input type="checkbox" name="chec" onClick="copyAddress(this.checked)"> 
If  Temporary Address is Same As Permanent Address Click Here </td>
</tr>
<tr >
<td class="text2" height="30">Address </td>
<td class="text2" height="30"> <input type="text" name="addr1" size="20" maxlength="35" class="textbox" value="<c:out value="${lec.addr1}"/>" ></td>
<td class="text2" height="30">City</td>
<td class="text2" height="30"><input type="text" name="city1" size="20" class="textbox" maxlength="20" value="<c:out value="${lec.city1}"/>" onKeyPress="return onKeyPressBlockNumbers(event);" ></td>
</tr>
<tr>    
<td class="text1" height="30">State</td>
<td class="text1" height="30"><input type="text" name="state1" size="20" class="textbox" maxlength="20" value="<c:out value="${lec.state1}"/>" onKeyPress="return onKeyPressBlockNumbers(event);"></td>
<td class="text1" height="30">Pincode</td>
<td class="text1" height="30"><input type="text" name="pincode1" size="20" class="textbox" maxlength="6" value="<c:out value="${lec.pincode1}"/>" onKeyPress="return onKeyPressBlockCharacters(event);"></td>
</tr>

<tr>
<td colspan="4" height="30" class="contenthead"><B>Official Details</B></td>
</tr>

<tr>
    
    
<td class="text2" height="30">Department Name</td>
<td class="text2" height="30"><select class="textbox" name="deptid" style="width:125px;">
        <option>---Select---</option>
        <c:if test = "${DeptList != null}" >
        <c:forEach items="${DeptList}" var="Dept"> 
        <c:choose>
        <c:when test="${Dept.deptId==lec.deptId}">
        <option selected value='<c:out value="${Dept.deptId}" />'><c:out value="${Dept.deptName}" /> 
        </c:when>
        <c:otherwise>
        <option value='<c:out value="${Dept.deptId}" />'><c:out value="${Dept.deptName}" /></option>
        </c:otherwise>
        </c:choose>
        </c:forEach >
        </c:if>  
        </select></td>
        
<td class="text2" height="30">Designation Name </td>
<td class="text2" height="30"><select class="textbox" id="desigId" name="designationId" maxlength="20" onChange="initCs()" style="width:125px;">
<option value="0">---Select---</option>
<c:if test = "${DesignaList != null}" >
<c:forEach  items="${DesignaList}" var="desig"> 
    <c:choose>
<c:when test="${lec.desigId==desig.desigId}">
<option selected value='<c:out value="${desig.desigId}" />'><c:out value="${desig.desigName}" /></option>
</c:when>
<c:otherwise>
<option  value='<c:out value="${desig.desigId}" />'><c:out value="${desig.desigName}" /></option>    
</c:otherwise>
</c:choose>
</c:forEach >
</c:if>  	
</select></td>
</tr>

<tr>  
<td class="text1" height="30">Grade Name </td>
<td class="text1" height="30"><select class="textbox" id="gradeId" name="gradeId" style="width:125px;" >
<option value="1">---Select---</option>
<c:if test = "${GradeList != null}" >
<c:forEach  items="${GradeList}" var="Grade"> 
    <c:choose>
<c:when test="${lec.gradeId==Grade.gradeId}">
<option selected value='<c:out value="${Grade.gradeId}" />'><c:out value="${Grade.gradeName}" /></option>
</c:when>
<c:otherwise>
<option value='<c:out value="${Grade.gradeId}" />'><c:out value="${Grade.gradeName}" /></option>    
</c:otherwise>
</c:choose>
</c:forEach >
</c:if>  	
</select></td>
<td height="30" class="text1">Status</td>
<td height="30" class="text1"><select name="activeInd" class="textbox" style="width:125px;">
<c:choose>
<c:when test="${lec.status == 'Y'}">
<option value="Y" selected>Active</option>
<option value="N">InActive</option>
</c:when>
<c:otherwise>
<option value="Y">Active</option>
<option value="N" selected>InActive</option>
</c:otherwise>
</c:choose>
</select>
 

</td>

</tr>

<tr>
<td class="text2" height="30">Company </td>
<td class="text2" height="30"><select class="textbox" name="cmpId" style="width:125px;">
<option value="0">-Select-</option>
<c:if test = "${companyList!= null}" >
<c:forEach  items="${companyList}" var="comp"> 
<c:choose>
<c:when test="${lec.cmpId == comp.cmpId}">
<option selected value='<c:out value="${comp.cmpId}" />'><c:out value="${comp.name}" />(<c:out value="${comp.companyType}" />)</option>
</c:when>
<c:otherwise>
<option value='<c:out value="${comp.cmpId}" />'><c:out value="${comp.name}" />(<c:out value="${comp.companyType}" />)</option>
</c:otherwise>
</c:choose>
</c:forEach >
</c:if>    
</select></td>
</tr>


<c:if test="${lec.userId != 0}" >

<tr >
<td height="30" class="text2">Click here if need user privilege</td>
<td height="30" class="text2"><input type="checkbox" class="textbox" name="userStatus" onClick="showTab();" /></td>
<td height="30" class="text2">&nbsp;</td>
<td height="30" class="text2">&nbsp;</td>
<td>
</tr>


<tr id="userStat" style="visibility:hidden;">
<td height="30" class="text2"><font color="red">*</font>User Name :</td>
<td height="30" class="text2"><input type="text" class="textbox" name="userName" maxlength="20" onchange="getProfile(this.value)" /></td>
<td height="30" class="text2"><font color="red">*</font> Password :</td>
<td height="30" class="text2"><input type="password" class="textbox" maxlength="10" name="password" maxlength="18"  /></td>
<td>
</tr>
<tr id="userStat1"  style="visibility:hidden;">
<td class="text1" height="30"><font color="red">*</font>Employee Role</td>
<script type="text/javascript">
    function getvalues(){
        alert("dhfjhdsjfksd");
      alert(document.getElementById("roleId").value);
    }
    </script>
<td class="text1" height="30"><select class="textbox" id="roleId" name="roleId" style="width:125px;" onchange="getvalues();" >
<c:if test = "${roleList != null}" >
<c:forEach  items="${roleList}" var="role"> 
<c:choose>
<c:when test="${lec.roleId == role.roleId}">
<option selected value='<c:out value="${role.roleId}" />'><c:out value="${role.roleName}" /></option>
</c:when>
<c:otherwise>
<option value='<c:out value="${role.roleId}" />'><c:out value="${role.roleName}" /></option>
</c:otherwise>
</c:choose>
</c:forEach >
</c:if>    
</select></td>
</tr>
</c:if>


</c:forEach>
</c:if>


</table>
<br>
<center>
<input type="text" class="textbox" id="userStatus1" name="userStatus1"  value="0" />      

<input type="button" class="button" value="Save" name="save" onClick="submitPage(this.name)">
<input type="button" class="button" value="View" name="View" onClick="viewPage()">
<input type="hidden" value="" name="reqfor">
</center>
<% 
}
%>
<bR>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
