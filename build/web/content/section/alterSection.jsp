<%-- 
Document   : modifyDesig
Created on : Nov 03, 2008, 6:14:28 PM
Author     : Vijay
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
    <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
 
<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="ets.domain.racks.business.RackTO" %>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>PAPL</title>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script language="javascript">

function submitpage(value)
{
 

var checValidate = selectedItemValidation();
var splt = checValidate.split("-");
if(splt[0]=='SubmitForm' && splt[1]!=0 ){
    

document.modify.action='/throttle/alterSection.do';
document.modify.submit();
}
}

function setSelectbox(i)
{
var selected=document.getElementsByName("selectedIndex") ;
selected[i].checked = 1;
}

function selectedItemValidation(){
var index = document.getElementsByName("selectedIndex");
var sectionNames = document.getElementsByName("sectionNames");
var sectionCodes = document.getElementsByName("sectionCodes");
var desc = document.getElementsByName("descriptions");
var chec=0;

for(var i=0;(i<index.length && index.length!=0);i++){
        if(index[i].checked){
        chec++;
        if(textValidation(sectionNames[i],"Section Name")){
            return;
        }else if(textValidation(desc[i],"Description")){
            return;                    
        }else if(textValidation(sectionCodes[i],"Section Code")){
            return;
        }
        }
}
if(chec == 0){
alert("Please Select Any One And Then Proceed");
desigName[0].focus();
//break;
}
document.modify.action='/throttle/alterSection.do';
document.modify.submit();
}

function isChar(s){
if(!(/^-?\d+$/.test(s))){
return false;
}
return true;
}
</script>

<div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="serviceplan.label.AlterSection" text="default text"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="serviceplan.label.ServicePlan" text="default text"/></a></li>
          <li class=""><spring:message code="serviceplan.label.ManageSection" text="default text"/></li>
          <li class="active"><spring:message code="serviceplan.label.AlterSection" text="default text"/></li>
        </ol>
      </div>
      </div>
 <div class="contentpanel">
<div class="panel panel-default">
 <div class="panel-body">


<!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/ 
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->

<body onload="document.modify.desigNames[0].focus();">
<form method="post" name="modify"> 

      

<%@ include file="/content/common/message.jsp" %>

 <% int index = 0; %>        

 <table class="table table-info mb30 table-hover" >

<c:if test = "${SectionList != null}" >
    <thead>
<tr>
<th height="30" ></th>
<th height="30" ><spring:message code="serviceplan.label.SNo" text="default text"/></th>
<th  height="30" ><spring:message code="serviceplan.label.SectionCode" text="default text"/> </th>
<th  height="30"><spring:message code="serviceplan.label.Section" text="default text"/> </th>
<th  height="30"><spring:message code="serviceplan.label.Description" text="default text"/></th>
<th  height="30"><spring:message code="serviceplan.label.ServiceType" text="default text"/></th>
<th  height="30"><spring:message code="serviceplan.label.Status" text="default text"/></th>
<th  height="30"><spring:message code="serviceplan.label.Select" text="default text"/></th>
</tr>
</thead>
</c:if>
<c:if test = "${SectionList != null}" >
<c:forEach items="${SectionList}" var="sec"> 		
<%

String classText = "";
int oddEven = index % 2;
if (oddEven > 0) {
classText = "text2";
} else {
classText = "text1";
}
%>
<tr>
 <td  height="30"><input type="hidden" name="sectionIds" value='<c:out value="${sec.sectionId}"/>'>  </td>
<td  height="30"><%=index + 1%></td>
<td  height="30"><input type="text" style="width:220px;height:40px;"  class="form-control" name="sectionNames" value="<c:out value="${sec.sectionName}"/>" onchange="setSelectbox(<%= index %>)"></td>
<td  height="30"><input type="text" style="width:220px;height:40px;"  class="form-control" name="sectionCodes" value="<c:out value="${sec.sectionCode}"/>" onchange="setSelectbox(<%= index %>)"></td>
<td  height="30"><input type="text" style="width:220px;height:40px;"  class="form-control" name="descriptions" value="<c:out value="${sec.description}"/>" onchange="setSelectbox(<%= index %>)"></td>
<td  height="30">
    <c:if test="${serviceTypeList != null}">
    <select name="serviceTypeIds" id="serviceTypeId<%= index %>" style="width:220px;height:40px;"  class="form-control" onchange="setSelectbox(<%= index %>)">
            <option value="0">--<spring:message code="serviceplan.label.Select" text="default text"/>--</option>
        <c:forEach items="${serviceTypeList}" var="service">
            <option value="<c:out value="${service.serviceTypeId}"/>"><c:out value="${service.serviceTypeName}"/></option>
        </c:forEach>
    </select>
    <script>
        document.getElementById("serviceTypeId"+<%= index %>).value = '<c:out value="${sec.serviceTypeId}"/>';
    </script>
    </c:if>
</td>
<td height="30" > <div align="center"><select name="activeInds" style="width:220px;height:40px;"  class="form-control" onchange="setSelectbox(<%= index %>)">
<c:choose>
<c:when test="${sec.activeInd == 'Y'}">
<option value="Y" selected><spring:message code="serviceplan.label.Active" text="default text"/></option>
<option value="N"><spring:message code="serviceplan.label.InActive" text="default text"/></option>
</c:when>
<c:otherwise>
<option value="Y"><spring:message code="serviceplan.label.Active" text="default text"/></option>
<option value="N" selected><spring:message code="serviceplan.label.InActive" text="default text"/></option>
</c:otherwise>
</c:choose>
</select>
</div> 
</td>
<td width="77" height="30" ><input type="checkbox" name="selectedIndex" value='<%= index %>'></td>
</tr>
<%
index++;
%>
</c:forEach >
</c:if> 
</table>
<center>
<br>
<input type="button" name="save" value="<spring:message code="serviceplan.label.SAVE" text="default text"/>" onClick="submitpage(this.name)" class="btn btn-success"  />
<input type="hidden" name="reqfor" value="designa" />
</center>
<br>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
