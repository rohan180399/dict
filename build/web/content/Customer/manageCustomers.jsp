<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<script language="javascript">
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#custName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCustomerNameDetails.do",
                    dataType: "json",
                    data: {
                        custName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            $('#customerId').val('');
                            $('#custName').val('');
                        }
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#customerId').val(tmp[0]);
                $('#custName').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    });
    $(document).ready(function() {
        $('#customerCode').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCustomerCode.do",
                    dataType: "json",
                    data: {
                        customerCode: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            $('#customerId').val('');
                            $('#customerCode').val('');
                        }
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('~');
                $('#customerId').val(tmp[0]);
                $('#customerCode').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('~');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });


    //    function checkValue(value,id){
    //                if(value == '' && id=='custName'){
    //                   $('#customerCode').attr('readonly', true);
    //                   document.getElementById('customerId').value = '';
    //                }
    //                if(value == '' && id=='customerCode'){
    //                   $('#custName').attr('readonly', true);
    //                   document.getElementById('customerCode').value = '';
    //                }
    //            }
    //

    function submitPage(value) {
        if (value == "add") {
            document.manufacturer.action = '/throttle/handleViewAddCustomer.do';
            document.manufacturer.submit();
        } else if (value == 'search') {
            document.manufacturer.action = '/throttle/handleViewCustomer.do';
            document.manufacturer.submit();
        }
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.Sales"  text="Sales"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.Sales"  text="Sales"/></a></li>
            <li class="active"><spring:message code="sales.subMenus.label.Customers"  text="Customers"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
                <form name="manufacturer" method="post" >
                    <%@ include file="/content/common/message.jsp" %>
                    <table class="table table-info mb30 table-hover">
                        <thead>
                            <tr>
                                <th colspan="6"><spring:message code="sales.label.customer.header"  text="View Customer Details"/></th>
                            </tr>
                        </thead>
                        <tr>
                            <td><spring:message code="sales.label.CustomerCode"  text="default text"/></td>
                            <td><input type="text" name="customerCode" id="customerCode" value="<c:out value="${customerCode}"/>" class="form-control" onclick="checkValue(this.value, this.id)"></td>
                            <td><spring:message code="sales.label.CustomerName"  text="default text"/></td>
                            <td><input type="hidden" name="customerId" id="customerId" value="<c:out value="${customerId}"/>"><input type="text" name="custName" id="custName" value="<c:out value="${custName}"/>" class="form-control" onclick="checkValue(this.value, this.id)"></td>
                            <td><spring:message code="sales.label.CustomerType"  text="default text"/></td>
                            <td>

                                <select name="contractType" id="contractType"  class="form-control" style="width:260px;height:40px;" onchange="showPartPayment(this.value)">
                                    <option value="" selected>--<spring:message code="sales.label.Select"  text="Select"/>---</option>
                                    <option value="1" ><spring:message code="sales.label.customer.primaryContract"  text="Primary Contract"/></option>
                                    <option value="2" ><spring:message code="sales.label.customer.secondaryContract"  text="Secondary Contract"/></option>
                                </select>
                            </td>
                        <tr>
                            <td colspan="6" align="center">
                                <input type="button" class="btn btn-info" name="add" value="<spring:message code="sales.label.customer.add"  text="default text"/>" onClick="submitPage('add')" style="width:100px;height:35px;">&nbsp;&nbsp;
                                <input type="button" class="btn btn-info" name="Search" value="<spring:message code="sales.label.customer.search"  text="default text"/>" onClick="submitPage('search')" style="width:100px;height:35px;">&nbsp;&nbsp;
                        </tr>
                    </table>

                    <c:if test="${CustomerLists == null }" >
                        <center><font color="red" size="2"><spring:message code="trucks.label.NoRecordsFound"  text="default text"/>  </font></center>
                      </c:if>
                            <c:if test = "${CustomerLists != null}" >
                        <table class="table table-info mb30 table-hover" id="table" >
                            <thead>
                                <tr>
                                    <th><spring:message code="sales.label.Sno"  text="default text"/></th>
                                    <th><spring:message code="sales.label.CustomerCode"  text="default text"/></th>
                                    <th><spring:message code="sales.label.CustomerName"  text="default text"/></th>
                                    <th><spring:message code="sales.label.CustomerType"  text="default text"/></th>
                                    <th><spring:message code="sales.label.PrimaryBillingType"  text="PrimaryBillingType"/></th>
                                    <th><spring:message code="sales.label.Location"  text="Location"/></th>
                                    <th><spring:message code="sales.label.EnrolledDate"  text="EnrolledDate"/></th>
                                    <th><spring:message code="sales.label.CreatedBy"  text="Created By"/></th>
                                    <th><spring:message code="sales.label.Status"  text="Status"/></th>
                                    <th><spring:message code="sales.label.customerView/Edit"  text="Credit View / Edit"/></th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int index = 0, sno = 1;%>
                                <c:forEach items="${CustomerLists}" var="customer">
                                    <tr>
                                        <td><%=sno%></td>
                                        <td><c:out value="${customer.customerCode}"/> </td>
                                        <td><c:out value="${customer.custName}"/> </td>
                                        <td><c:out value="${customer.customerTypeName}"/></td>
                                        <td><c:out value="${customer.billingTypeName}"/></td>
                                        <td><c:out value="${customer.custCity}"/></td>
                                        <td><c:out value="${customer.enrollDate}"/></td>
                                        <td><c:out value="${customer.createdBy}"/></td>
                                        <td>
                                            <c:if test="${(customer.custStatus=='n') || (customer.custStatus=='N')}" >
                                                InActive
                                            </c:if>
                                            <c:if test="${(customer.custStatus=='y') || (customer.custStatus=='Y')}" >
                                                Active
                                            </c:if>
                                        </td>
                                        <td>
                                            <a href="/throttle/editCustomerOutstandingCredit.do?customerId=<c:out value="${customer.custId}"/>&customerName=<c:out value="${customer.custName}"/>">
                                                <span class="label label-warning"><spring:message code="trucks.label.ViewEdit"  text="view/edit"/> </span> 
                                            </a>
                                        </td>

                                    </tr>
                                    <%
                                        index++;
                                        sno++;
                                    %>
                                </c:forEach>

                            </tbody>
                        </table>
                    </c:if>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                        <div id="controls" style="width:auto;" >
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5"  selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span><spring:message code="trucks.label.EntriesPerPage"  text="default text"/></span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text"><spring:message code="trucks.label.DisplayingPage"  text="default text"/> <span id="currentpage"></span> <spring:message code="trucks.label.of"  text="default text"/> <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 1);
                    </script>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>
